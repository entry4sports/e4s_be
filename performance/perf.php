<?php
include_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_updateCompetitionPb($obj) {
    $entryId = checkFieldForXSS($obj, 'entryid:Entry ID' . E4S_CHECKTYPE_NUMERIC);
    $value = checkFieldForXSS($obj, 'value:PB');
    $track = checkFieldForXSS($obj, 'track:track PB');
	if ( is_null($track) ) {
		$track = false;
	}

    $entryObj = new entryClass($entryId);
    $entryPbObj = $entryObj->updatePB($value, $track);
    if (!is_null($entryPbObj)){
        $payload = new stdClass();
        $payload->entryId = $entryId;
        $payload->value = $value;

        e4s_sendSocketInfo($entryPbObj->compId, $payload, R4S_SOCKET_ENTRY_PB);
        Entry4UISuccess();
    }
    Entry4UIError(4142, 'Unable to update the Performance');
}

// update Open PBs from the Athlete maint
function e4s_updatePBFromAthleteMaintV2($obj) {
	include_once E4S_FULL_PATH . 'entries/entries.php';
	e4s_updateEntryPerf($obj);
    Entry4UISuccess('');
}
function e4s_updatePBFromAthleteMaint($obj) {
    $aid = checkFieldForXSS($obj, 'aid:Athlete ID' . E4S_CHECKTYPE_NUMERIC);
    $athleteObj = athleteClass::withID($aid);
    if (is_null($athleteObj->athleteRow)) {
        Entry4UIError(3010, 'Invalid Athlete', 200, '');
    }
    $athleteObj->updatePBFromPayload($obj);
    Entry4UISuccess('');
}

function e4s_updateAthletesPof10($obj){
	$urn = checkFieldForXSS($obj, 'urn:URN');
	$force = checkFieldForXSS($obj, 'force:Force update');
	if ( !is_null($force) ) {
		if ( $force === true or $force === 1 or $force === 'true' or $force === '1' ){
			$force = true;
		} else {
			$force = false;
		}
	}else{
		$force = false;
	}
	$obj = new pof10V2Class($urn);
	$obj->updateDb($force);
	$athleteId = $obj->getAthleteId();
	$perfObj = new perfAthlete();
	$perfs = $perfObj->getPerformances($athleteId);
	$data = new stdClass();
	foreach($perfs as $athleteId => $perf) {
		$data->athleteId = $athleteId;
		$data->perfs = $perf;
		// should only be 1 element anyway
		break;
	}

	$data->rankings = $perfObj->getRankings($athleteId);
	Entry4UISuccess($data);
}
function e4s_updatePBsFromPof10($obj) {

    $urn = checkFieldForXSS($obj, 'urn:Athlete URN');
    if (!_e4s_isPof10Available($obj)) {
        Entry4UISuccess('');
    }

    $data = null;
    $athleteObj = athleteClass::withURN(E4S_AOCODE_EA, $urn);
    if (!is_null($athleteObj)) {
        $athleteObj->updatePBsFromPof10Info();
        $data = $athleteObj->getRow(TRUE);
    }
    Entry4UISuccess('
        "data": ' . json_encode($data, JSON_NUMERIC_CHECK));
}

function e4s_getPof10Info($obj) {

    $urn = checkFieldForXSS($obj, 'urn:Athlete URN');
    if (!_e4s_isPof10Available($obj)) {
        return null;
    }
    $pof10Obj = new pof10Class('EA', $urn);

    return $pof10Obj->getPerformanceRows(TRUE);
}