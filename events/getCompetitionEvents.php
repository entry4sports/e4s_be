<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'events/commonEvent.php';
include_once E4S_FULL_PATH . 'entries/commonEntries.php';

//$compid = $_GET['compid'];
//$athleteId = $_GET['aid'];
//$clubid
$compid = (int)$compid;
$athleteId = (int)$athleteId;

if (is_null($clubId) or $clubId === 'undefined') {
    $clubId = -1;
} else {
    $clubId = (int)$clubId;
}
$ruleObj = '{
    "id":0,
    "options" : {}
}';
$schedInfo = new stdClass();
$schedInfo->title = 'Schedule information';
$currentAge = '0';
$config = e4s_getConfig();
$compObj = e4s_GetCompObj($compid);
$athleteSQL = 'select a.*, c.id clubId, c.Clubname club, s.Clubname school, ar_county.id countyid, ar_region.id regionid, ar_country.id countryid
               from ' . E4S_TABLE_ATHLETE . ' a left join
                    ' . E4S_TABLE_CLUBS . ' c on a.clubid = c.id left join
                    ' . E4S_TABLE_AREA . ' ar_county on c.areaid = ar_county.id left join ' . E4S_TABLE_AREA . ' ar_region on ar_county.parentid = ar_region.id left join ' . E4S_TABLE_AREA . ' ar_country on ar_region.parentid = ar_country.id left join
                    ' . E4S_TABLE_CLUBS . " s on a.schoolid = s.id
               where a.id = $athleteId";

$athleteResult = e4s_queryNoLog($athleteSQL);
$athleteRecord = $athleteResult->fetch_assoc();
if ( is_null($athleteRecord) ){
    Entry4UIError(9440, 'Invalid Athlete');
}
$aOptions = e4s_getOptionsAsObj($athleteRecord['options']);
if (isset($aOptions->noEntryReason) and $aOptions->noEntryReason !== '') {
    $schedInfo->noEntryReason = $aOptions->noEntryReason;
    e4s_returnNoEvents($currentAge, $schedInfo, $ruleObj);
    exit();
}
$override = FALSE;
if (isset($aOptions->compOverride)) {
    $override = $aOptions->compOverride === $compid;
}
$athleteObj = new athleteClass(FALSE);
$athleteRecord = $athleteObj->checkAndUpdateExtAthlete($athleteRecord, $compObj->getDate());

$athleteRecord['firstName'] = formatAthleteFirstname($athleteRecord['firstName']);
$athleteRecord['surName'] = formatAthleteSurname($athleteRecord['surName']);
$urn = $athleteRecord['URN'];
if (is_null($urn)) {
    $urn = '';
}
if ((int)$clubId > 0 and (int)$clubId !== (int)$athleteRecord['clubid']) {
    // using 2nd Claim club
    $claimSQL = 'select *
                 from ' . E4S_TABLE_CLUBS . '
                 where id = ' . $clubId;
    $claimresult = e4s_queryNoLog($claimSQL);
    if ($claimresult->num_rows === 1) {
        $claimClub = $claimresult->fetch_assoc();
        $athleteRecord['club'] = $claimClub['Clubname'];
    }
    $athleteRecord['clubid'] = $clubId;
}
$gender = $athleteRecord['gender'];
if ($gender === '') {
    $gender = E4S_GENDER_MALE;
}
$dob = $athleteRecord['dob'];
// Get Competition

$compRow = $compObj->getRow();
$cOptions = $compObj->getOptions();
$isE4S = isE4SUser();
$isOrg = $compObj->isOrganiser();

$compClosed = FALSE;
$limitReason = '';
if (isset($cOptions->compLimits)) {
    $counters = $compObj->getEntryCounts();
    $limits = $cOptions->compLimits;
    /*
        relayAthleteCnt: athletes in teams
        relayCnt: team entries
        totalCnt: indiv entries
        uniqueCnt: unique Athlete Cnt
    */
    if (isset($limits->entries)) {
        if ($counters['totalCnt'] >= $limits->entries and $limits->entries > 0) {
            $compClosed = TRUE;
            $limitReason = 'Maximum entries for competition reached : ' . $limits->entries;
        }
    }
    if (isset($limits->athletes)) {
        if ($counters['uniqueCnt'] >= $limits->athletes and $limits->athletes > 0) {
            $compClosed = TRUE;
            $limitReason = 'Maximum athletes for competition reached : ' . $limits->athletes;
        }
    }
    if (isset($limits->teams)) {
        if ($counters['relayCnt'] >= $limits->teams and $limits->teams > 0) {
            $compClosed = TRUE;
            $limitReason = 'Maximum teams for competition reached : ' . $limits->teams;
        }
    }
}

$isRegistered = isAthleteRegistered($athleteRecord, false);
$allowUnregistered = TRUE;
$allowRegistered = TRUE;
$regExpired = FALSE;
$registrationIssue = FALSE;

$allowUnregistered = $compObj->allowUnRegisteredAthletes($cOptions);
$allowRegistered = $compObj->allowRegisteredAthletes($cOptions);
$allowInternational = $compObj->allowInternationalAthletes($cOptions);

if (is_null($athleteRecord['URN'])) {
    $athleteRecord['URN'] = '';
}
$u11Age = '';
$allCompDOBs = getAllCompDOBs($compid);
foreach ($allCompDOBs as $ageInfo) {
    if ($ageInfo['MaxAge'] < 11) {
        $checkDate = date_create($ageInfo['fromDate']);
        if (gettype($u11Age) === 'string') {
            $u11Age = $checkDate;
        } elseif ($checkDate < $u11Age) {
            $u11Age = $checkDate;
        }
    }
}
e4s_addDebugForce('Registerdd : ' . $isRegistered);
if ($isRegistered) {
    $nowStr = date('Y-m-d');
    $compDate = $compRow['Date'];
    $regDate = $athleteRecord['activeEndDate'];
    // Registered Athlete
    if ( !$isOrg ) {
        if (!$compObj->allowExpiredRegistration()) {
            if ((is_null($regDate) || $regDate < $compDate)) {
                $regExpired = TRUE;

                if ($allowUnregistered === FALSE) {
                    $registrationIssue = TRUE;
                }
            }
        }
        if ($allowRegistered === FALSE) {
            $registrationIssue = TRUE;
        }
    }
} elseif ($allowUnregistered === FALSE) {
    $registrationIssue = TRUE;
}
if (E4S_CURRENT_DOMAIN !== E4S_AAI_DOMAIN and $registrationIssue) {
    $athleteDob = date_create($athleteRecord['dob']);
    if ($athleteDob > $u11Age) {
        $registrationIssue = FALSE;
        $regExpired = FALSE;
    }
}
$useAgeGroupsInfo = getAgeGroupInfo($allCompDOBs, $athleteRecord['dob']);
$useAgeGroup = $useAgeGroupsInfo['ageGroup'];
$ageGroups = $useAgeGroupsInfo['ageGroups'];
$useAgeGroupIds = '0';
$useAgeGroupIdsArr = array();
foreach ($ageGroups as $value) {
    if (isset($value['agid'])) {
        $useAgeGroupIds .= ',' . $value['agid'];
        $useAgeGroupIdsArr[] = $value['agid'];
    }
}
$currentAge = $useAgeGroupsInfo['currentAge'];
$athleteSecurity = e4s_getAthleteSecurityForComp($compObj, $athleteRecord);
if (!$athleteSecurity->allowed or $useAgeGroup === null or (int)$useAgeGroup['id'] === 0 or isset($useAgeGroup['agid']) === FALSE or ($regExpired and $allowUnregistered === FALSE) or $registrationIssue) {
    if ($registrationIssue) {
        $urnMessage = '';
        if ($urn === '') {
            $urnMessage = '!!! Missing URN number in athletes profile. (Click the i icon next to athletes name )';
        }
        if ($allowRegistered) {
            $schedInfo->shortDescription = 'Competition only open to Active Registered athletes. ' . $urnMessage;
        } elseif ($allowUnregistered) {
            $schedInfo->shortDescription = 'Competition only open to Un-Registered athletes.';
        } else {
            $schedInfo->shortDescription = 'Issue with athletes registration';
        }
    } elseif (!$athleteSecurity->inArea and $athleteSecurity->inArea !== E4S_DEFAULT_ATHLETE_SECURITY) {
        $schedInfo->shortDescription = 'Athletes County/Region/Country not accepted for this competition.';
    } elseif (!$athleteSecurity->inClub and $athleteSecurity->inClub !== E4S_DEFAULT_ATHLETE_SECURITY) {
        $schedInfo->shortDescription = 'Athletes Club is not accepted for this competition.';
    } else {
        $schedInfo->shortDescription = 'No individual events available. Please contact the Competition organiser if there is an issue with the available events.';
    }

    if ($useAgeGroup === null or (int)$useAgeGroup['id'] === 0 or !isset($useAgeGroup['agid'])) {
        //        Athlete has no agegroup, get out irrelevant of user
        e4s_returnNoEvents($currentAge, $schedInfo, $ruleObj);
        exit();
    }
    if (!$isOrg and !$override) {
        e4s_returnNoEvents($currentAge, $schedInfo, $ruleObj);
        exit();
    }
}

$eventTeamsAvailable = FALSE;
$useVetAgeGroup = $useAgeGroupsInfo['vetAgeGroup'];
$ageGroupId = $useAgeGroup['agid'];
$useVetAgeGroupId = 0;
$useVetAgeGroupName = '';
if ($useVetAgeGroup !== null) {
    $useVetAgeGroupId = $useVetAgeGroup['id'];
    $useVetAgeGroupName = $useVetAgeGroup['Name'];
}

$useAgeGroup['Name'] = e4s_getVetDisplay($useAgeGroup['Name'], $gender);
$useVetAgeGroupName = e4s_getVetDisplay($useVetAgeGroupName, $gender);

// Get existing entries for the athlete
$entrySql = 'SELECT e.*, u.display_name userName, u.user_email userEmail,ev.name Name
             FROM ' . E4S_TABLE_ENTRIES . ' as e, 
                  ' . E4S_TABLE_EVENTS . ' ev,
                  ' . E4S_TABLE_COMPEVENTS . ' as ce,
                  ' . E4S_TABLE_USERS . " u
             where e.compeventid = ce.id 
             and   ce.compid = {$compid}
             and   e.athleteid = {$athleteId}
             and   ce.eventid = ev.id
             and   e.userid = u.id";
$entryResult = e4s_queryNoLog('Get Existing Events' . E4S_SQL_DELIM . $entrySql);
$compEntries = $entryResult->fetch_all(MYSQLI_ASSOC);

$eventSql = "SELECT ce.id as ceid
                  , ce.AgeGroupID ageGroupId
                  , ag.Name ageGroupName
                  , p.description
                  , date_format(eg.startdate,'" . ISO_MYSQL_DATE . "') startdate
                  , ce.IsOpen
                  , e.tf
                  , u.uomtype uomType
                  , u.uomoptions
                  , p.id priceID
                  , p.saleprice
                  , p.price
                  , p.description pricedescription
                  , p.salefee
                  , p.fee stdfee
                  , p.options priceoptions
                  , IFNULL(date_format(p.saleenddate,'" . ISO_MYSQL_DATE . "'),'') saledate
                  , p.name
                  , e.id eventid
                  , e.Name
                  , eg.name eventGroup
                  , eg.options egOptions
                  , ce.split
                  , ce.maxgroup
                  , ce.maxathletes
                  , ce.options ceoptions
                  , e.options eoptions
             from 
                  " . E4S_TABLE_COMPEVENTS . ' as ce,
                  ' . E4S_TABLE_AGEGROUPS . ' as ag,
                  ' . E4S_TABLE_EVENTGROUPS . ' as eg,
                  ' . E4S_TABLE_EVENTPRICE . ' as p,
                  ' . E4S_TABLE_EVENTS . ' as e left join ' . E4S_TABLE_UOM . ' u on u.id = e.uomid 
             where e.ID = ce.EventID
             and   ce.agegroupid = ag.id 
             and   ce.maxgroup = eg.id
             and   ce.PriceID = p.id
             and   (AgeGroupID in(' . $useAgeGroupIds . ')
             ';

foreach ($useAgeGroupIdsArr as $agid) {
    $eventSql .= " or ce.options like '%\"ageGroup\":" . $agid . "%' ";
}
//or ce.options like '%\"ageGroup\":" . $ageGroupId . "%'
$eventSql .= ")
             and ce.CompID = $compid
             and e.Gender = '$gender' 
             order by eg.startdate, eg.eventno, e.name";
//and (AgeGroupID = 0 or AgeGroupID = $ageGroupId or ce.options like '%\"ageGroup\":" . $ageGroupId . "%')

$eventResult = e4s_queryNoLog('Get Events' . E4S_SQL_DELIM . $eventSql);
$eventData = array();
$maxGroups = array();
$defaultUserObj = array();
$defaultUserObj['userId'] = 0;
$defaultUserObj['userName'] = '';
$defaultUserObj['userEmail'] = '';
$priceObjs = array();
$orderObj = new e4sOrders($compid);
$countObj = e4s_getEntryCountObj($compid);
$eventObj = new e4sEvents($compid);
while ($row = mysqli_fetch_array($eventResult, TRUE)) {
    $row['startdate'] .= e4s_getOffset($row['startdate']);
    $row['saledate'] .= e4s_getOffset($row['saledate']);
    $row['ageGroupID'] = $row['ageGroupId'];
    $row['ageGroup'] = e4s_getAgeGroupInfo($row)['ageGroup'];
    $ceoptions = e4s_mergeAndAddDefaultCEOptions($row['ceoptions'], $row['eoptions'], E4S_OPTIONS_OBJECT);
    $egOptions = e4s_addDefaultEventGroupV2Options($row['egOptions']);
    $ceoptions = $eventObj->populateOptions($ceoptions, (int)$row['maxgroup']);
    if ($compClosed) {
        e4s_markUnavailable($row, $limitReason, true);
        $ceoptions = $row['ceoptions']; // Not sure why this is here ??
    }

    if (!e4s_checkEGSecurity($athleteRecord, $egOptions)) {
        continue;
    }
    // ignore if set as a team event or a league event
    if (isset($ceoptions->isTeamEvent) and $ceoptions->isTeamEvent) {
        continue;
    }

    // Check if not for registered/unregistered athletes
    $allowEventRegistered = TRUE;
    $allowEventUnRegistered = TRUE;
    // CE Event options take priority
    if (isset($ceoptions->registeredAthletes)) {
        $allowEventRegistered = $ceoptions->registeredAthletes;
    }

    if (isset($ceoptions->unregisteredAthletes)) {
        $allowEventUnRegistered = $ceoptions->unregisteredAthletes;
    }

    if (!$allowEventRegistered and $isRegistered) {
        if (!$override) {
            continue;
        }
    }
    if (!$allowEventUnRegistered and !$isRegistered) {
        if (!$override) {
            continue;
        }
    }
    if (!$isOrg) {
        // Check if CE has security
        $userObj = e4s_getUserObj(FALSE, TRUE);
        $ceEntitylevel = -1;
        $ceEntityid = -1;
        if (isset($userObj->clubs)) {
            $userclubs = $userObj->clubs;
        } else {
            $userclubs = array();
        }

        if (sizeof($userclubs) > 0) {
            $ceEntitylevel = 1;
            $ceEntityid = (int)$userclubs[0]->id;
        }

        if (isset($userObj->areas)) {
            $userareas = $userObj->areas;
        } else {
            $userareas = array();
        }
        if (sizeof($userareas) > 0) {
            $ceEntitylevel = (int)$userareas[0]->entitylevel;
            $ceEntityid = (int)$userareas[0]->areaid;
        }
        $entity = new stdClass();
        $entity->level = $ceEntitylevel;
        $entity->id = $ceEntityid;
        $entity->school = FALSE;

        $securityReason = e4s_checkTeamEventSecurity($ceoptions, $entity);
        if ($securityReason !== '') {
//            e4s_addDebug($securityReason);
//            continue;
            e4s_markUnavailable($row, 'Secure Event. You do not have the required permissions.', true);
        }
    }

    $row['entered'] = FALSE;
    $row['entryId'] = 0;
    $row['paid'] = E4S_ENTRY_NOT_PAID;
    $row['athleteid'] = $athleteId;
//    $oldobj = e4s_getEntryInformation( $athleteId, $row['ceid']);
    $obj = $countObj->getAthleteInfoForCE($athleteId, $row['ceid']);
//    if ( $obj->paidCount !== $oldobj->paidCount){
//        Entry4UIError(9000,"Count failure");
//    }
    $row['entryInfo'] = $obj;
    // Awaiting Nick to confirm this can go
    $row['entrycnt'] = $obj->paidCount;
    $row['compName'] = $compRow['Name'];
    $row['user'] = $defaultUserObj;
    if (!in_array($row['maxgroup'], $maxGroups)) {
        $maxGroups[] = $row['maxgroup'];
    }

    $row['order'] = $orderObj->eventOrderInfo();

    foreach ($compEntries as $compEntry) {
        //echo "compEvent = $compEventID     ceid = " . $row['ceid'] . " <br>";

        if ($row['ceid'] === $compEntry['compEventID']) {
            $row['entered'] = TRUE;
            $row['entryId'] = $compEntry['id'];
            $compEntry['isE4S'] = $isE4S;
            $compEntry['isOrganiser'] = $isOrg;
            $row['order'] = $orderObj->eventOrderInfo($compEntry);
            $row['paid'] = $compEntry['paid'];
            if ($compEntry['teambibno'] === '0' or is_null($compEntry['teambibno'])) {
                $compEntry['teambibno'] = '';
            }
            $row['teamBibNo'] = $compEntry['teambibno'];
            $userObj = array();
            $userObj['userId'] = $compEntry['userid'];
            $userObj['userName'] = $compEntry['userName'];
            $userObj['userEmail'] = $compEntry['userEmail'];
            $row['user'] = $userObj;
            unset($row['userid']);
            unset($row['userName']);
            unset($row['userEmail']);
        }
    }

    $row['firstName'] = $athleteRecord['firstName'];
    $row['surName'] = $athleteRecord['surName'];
    $row['club'] = $athleteRecord['club'];
    $row['clubId'] = $athleteRecord['clubid'];
    $row['eoptions'] = e4s_getOptionsAsObj($row['eoptions']);
    $row['ceoptions'] = $ceoptions;

    // price object
    $priceObj = array();
    $priceObj['id'] = $row['priceID'];
    $priceObj['priceName'] = $row['name'];
    $priceObj['stdPrice'] = $row['price'];
    $priceObj['curPrice'] = $row['price'];
    $priceObj['curFee'] = $row['stdfee'];
    $priceObj['salePrice'] = $row['saleprice'];
    $priceObj['saleDate'] = $row['saledate'];
    if ($priceObj['saleDate'] !== '') {
        $saleDate = strtotime($priceObj['saleDate']);
        $now = time();
        if ($saleDate > $now) {
            $priceObj['curPrice'] = $priceObj['salePrice'];
            $priceObj['curFee'] = $row['salefee'];
        }
    }
    $row['price'] = $priceObj;
    $priceObj['description'] = $row['pricedescription'];
    $priceObj['options'] = $row['priceoptions'];
    $priceObj['salefee'] = $row['salefee'];
    $priceObj['stdfee'] = $row['stdfee'];
    $priceObjs[$priceObj['priceName']] = $priceObj;

    unset($row['priceID']);
    unset($row['name']);
    unset($row['saleprice']);
    unset($row['saledate']);
    unset($row['pricedescription']);
    unset($row['priceoptions']);
    unset($row['salefee']);
    unset($row['stdfee']);

    // Split
    getEventNameWithSplit($row, 'Name');

    // TODO Check correct isTeam Event method
    if (isset($eoptions->eventTeam) || isset($ceoptions->eventTeam)) {
        // Can you get into here ???? See Line 233

//        Get the number of athletes entered by this user to the event
//        TO DO What if another user starts off the entries. How can this work. 1 user adds some to a team and another is allowed to create a team for the same area ????
        $checkSql = 'Select count(*) cnt
                 from ' . E4S_TABLE_ENTRIES . ' 
                 where compEventID = ' . $row['ceid'] . '
                 and   athleteid in (
                    select a.id 
                    from ' . E4S_TABLE_USERATHLETES . ' a
                    where userid = ' . e4s_getUserID() . '
                 )';

        $countResult = e4s_queryNoLog($checkSql);
        $countRow = mysqli_fetch_array($countResult, TRUE);

        if (isset($row['eoptions']->eventTeam)) {
            if (!is_null($row['eoptions']->eventTeam)) {
                $row['eoptions']->eventTeam->currCount = $countRow['cnt'];
            }
        }

        if (isset($row['ceoptions']->eventTeam)) {
            if (!is_null($row['ceoptions']->eventTeam)) {
                $row['ceoptions']->eventTeam->currCount = (int)$countRow['cnt'];
            }
        }
    }

    $addRow = TRUE;
    if (isset($ceoptions->mindob)) {
        if ($dob > $ceoptions->mindob) {
            $addRow = FALSE;
        }
    }

    $orgHelpText = ' Event Closed : Organiser Access';
    // default availableTo to the event start date if there is not one available
    if (!isset($ceoptions->availableTo) or $ceoptions->availableTo === '') {
        $ceoptions->availableTo = $row['startdate'];
    }

    if (!isset($ceoptions->availableToStatus)) {
        $ceoptions->availableToStatus = E4S_EVENT_HIDDEN;
    }
    // if there is an expiry on this event/item, check and close if expired
    $availableTo = e4s_iso_to_sql($ceoptions->availableTo);
    $expiryDate = DateTime::createFromFormat('Y-m-d G:i:s', $availableTo, $tz);

    $now = new DateTime();
    if ($now > $expiryDate) {
        $row['IsOpen'] = E4S_EVENT_CLOSED;
        if (!$isOrg){
            if ($ceoptions->availableToStatus === E4S_EVENT_HIDDEN ) {
                $addRow = FALSE;
            }
        }else{
            $orgHelpText = ' Event No Longer Available : Organiser Access';
        }
    }

    if (isset($ceoptions->availableFrom) and $ceoptions->availableFrom !== '') {
        if (!isset($ceoptions->availableFromStatus)) {
            $ceoptions->availableFromStatus = E4S_EVENT_HIDDEN;
        }
        // if there is an availableFrom on this event/item, check and dont show if not yet to be made available
        $availableFrom = e4s_iso_to_sql($ceoptions->availableFrom);

        $availableOnDate = DateTime::createFromFormat('Y-m-d G:i:s', $availableFrom, $tz);
        $now = new DateTime();
        if ($now < $availableOnDate) {
            $row['IsOpen'] = E4S_EVENT_CLOSED;
            if (!$isOrg){
                if ($ceoptions->availableFromStatus === E4S_EVENT_HIDDEN ) {
                    $addRow = FALSE;
                }
            }else{
                $orgHelpText = ' Event Not Yet Available: Organiser Access';
            }
        }
    }

    if ($isOrg) {
        // allow E4S/Organiser to always enter events. Waiting list position ?
        // Not sure why this will have changed but for now, check both
        $maxAthletes = 0;
        if ( isset($egOptions->maxAthletes)){
            $maxAthletes = $egOptions->maxAthletes;
        }else{
            $maxAthletes = eventGroup::getMaxAthletes($egOptions);
        }
        if ((int)$row['IsOpen'] !== E4S_EVENT_OPEN and $maxAthletes > -1) {
//            e4s_addDebugForce("Open Event");
            $row['IsOpen'] = E4S_EVENT_OPEN;
            $row['ceoptions']->helpText .= $orgHelpText;
            $row['ceoptions']->rowOptions->autoExpandHelpText = TRUE;
        }
    }
    $athleteSecurity = e4s_getAthleteSecurityForComp($compObj, $athleteRecord);
    if (!$athleteSecurity->allowed and !$isOrg) {
        e4s_markUnavailable($row, 'Not available to the athletes club');
        $addRow = FALSE;
    }

    if (!e4s_athleteClassificationOk($athleteRecord, $row)) {
//        Entry4UIError(1003,"Classifcation");
        $addRow = FALSE;
    }
    if ($addRow or $override) {
        $eventData[] = $row;
    }
}

$pbSql = 'Select eventid, pb
          from ' . E4S_TABLE_ATHLETEPB . "
          where athleteid = {$athleteId}";

$pbResults = e4s_queryNoLog($pbSql);
$pbs = mysqli_fetch_all($pbResults, MYSQLI_ASSOC);

// get Comp Rules for ageGroup
$ruleSQL = 'select * from ' . E4S_TABLE_COMPRULES . " 
            where compid = $compid 
            and (ageGroupID = " . $useAgeGroup['id'] . ' or ageGroupID = 0 )
            order by ageGroupID';
$result = e4s_queryNoLog($ruleSQL);
while ($row = mysqli_fetch_array($result, TRUE)) {
    // if more than 1 row return, only interested in last row
    $ruleObj = '{
        "id": ' . $row['id'] . ',
        "options" : ' . ($row['options']) . '
    }';
}

// CompSchedInfo
$discSql = 'select * 
            from ' . E4S_TABLE_COMPDISCOUNTS . '
            where compid = ' . $compid . '
            and agegroupid in (0,' . $useAgeGroup['id'] . ')
            order by agegroupid desc';
$discResults = e4s_queryNoLog(basename(__FILE__) . '' . E4S_SQL_DELIM . $discSql);
$priceDiscInfoObj = '';

// TODO allocate a discount to a price
// This assumes all discounts have the same sale date
// ALSO this will apply to ALL PRICES which is wrong.
// You may NOT want to apply a discount to a specific event ( Heptathlon e.t.c )
$useSalePrice = FALSE;

$eventTeamMessage = '';
if ($eventTeamsAvailable === TRUE) {
    $eventTeamMessage = ', however there are team events available ' . $athleteRecord['firstName'] . '.';
}

$timetable = '';
if (isset($cOptions->timetable)) {
    if (strtolower($cOptions->timetable) === 'provisional') {
        $timetable = 'The Timetable is ' . strtoupper($cOptions->timetable) . ' and subject to change. ';
    }
}

$schedInfoDetails = array();
$schedInfo->autoExpand = FALSE;
$schedInfo->shortDescription = 'No individual events available.' . $eventTeamMessage . ' Please contact the Competition organiser if there is an issue with the available events.';
if (isset($cOptions->helpText)) {
    if (isset($cOptions->helpText->schedule) and trim($cOptions->helpText->schedule) !== '') {
        $title = 'General';
        $schedText = $cOptions->helpText->schedule;
        if (strpos($schedText, E4S_TEXT_TITLE_DELIM) > 0) {
            $schedText = explode(E4S_TEXT_TITLE_DELIM, $schedText);
            $title = $schedText[0];
            $schedText = $schedText[1];
        }
        $schedInfo->autoExpand = FALSE;
        if ($title . $schedText !== '') {
            $compSchedInfoObj = new stdClass();
            $compSchedInfoObj->title = $title;
            $compSchedInfoObj->body = $schedText;
            $schedInfoDetails[] = $compSchedInfoObj;
        }
    }
}
/*
 * TO DO : Check if there are any manadatory perfs required
$compSchedInfoObj = new stdClass();
$compSchedInfoObj->title = "Mandatory";
$compSchedInfoObj->body = "Fill in your Performance";
$schedInfoDetails[] = $compSchedInfoObj;
*/
if (!empty($eventData)) {
    $schedInfo->autoExpand = getUserSchedInfoExpand();
    $schedInfo->rowOptions = '{"showPrice": false}';
    $schedInfo->shortDescription = $timetable . 'This schedule is bespoke to ' . $athleteRecord['firstName'] . '. Please use Contact Organiser if there is an issue with the available events.';
}
$schedInfo->schedInfoDetails = $schedInfoDetails;

Entry4UISuccess('
 "events" : ' . json_encode($eventData, JSON_NUMERIC_CHECK) . ',
 "ageInfo" : {  
   "currentAge" : ' . $currentAge . ',
   "ageGroupId" : ' . $useAgeGroup['id'] . ',
   "ageGroup" : "' . $useAgeGroup['Name'] . '",
   "vetAgeGroupId" : ' . $useVetAgeGroupId . ',
   "vetAgeGroup" : "' . $useVetAgeGroupName . '"
 }, 
 "schedInfo" : ' . json_encode($schedInfo, JSON_NUMERIC_CHECK) . ',
 "rules" : ' . $ruleObj . ',
 "subscriptionText":"' . e4s_getSubscriptionTandC() . '"
');

function e4s_checkEGSecurity($athleteRecord, $egOptions) {
    if (isE4SUser()) {
        return TRUE;
    }
    $clubs = $egOptions->athleteSecurity->clubs;
    if (empty($clubs)) {
        return TRUE;
    }
    // eventGroup has athleteSecurity so check athlete club is in it
    foreach ($clubs as $club) {
        if ($club->id === (int)$athleteRecord['clubid'] or $club->id === (int)$athleteRecord['club2id'] or $club->id === (int)$athleteRecord['schoolid']) {
            return TRUE;
        }
    }
    return FALSE;
}

function e4s_markUnavailable(&$row, $reason, $markClosed = false) {
    $ceoptions = e4s_getOptionsAsObj($row['ceoptions']);
    $ceoptions->isNotAvailable = E4S_EVENT_NOTAVAILABLE;
    if (!isset($ceoptions->rowOptions)) {
        $ceoptions->rowOptions = new stdClass();
    }
    $ceoptions->rowOptions->autoExpandHelpText = TRUE;
    $text = '';
    if (isset($ceoptions->helpText)) {
        $text = $ceoptions->helpText;
    }
    if ($text !== '') {
        $text .= ', ';
    }
    $text .= $reason;
    $ceoptions->helpText = $text;
    $row['ceoptions'] = $ceoptions;
    if ( $markClosed ){
        $row['IsOpen'] = E4S_EVENT_CLOSED;
    }
}

function e4s_athleteClassificationOk($athleteRecord, $eventRow) {
    $athleteClass = (int)$athleteRecord['classification'];
//    $ceOptions = $eventRow['ceoptions'];
    $eOptions = $eventRow['eoptions'];

    if (!isset($eOptions->class)) {
        return TRUE;
    }

    $eventClassifications = preg_split('~,~', $eOptions->class);
    foreach ($eventClassifications as $eventClassification) {

        $classRange = preg_split('~-~', $eventClassification);
        $min = (int)$classRange[0];
        $max = (int)$classRange[0];
        if (sizeof($classRange) > 1) {
            $max = (int)$classRange[1];
        }
        if ($athleteClass >= $min and $athleteClass <= $max) {
            return TRUE;
        }
    }
    return FALSE;
}

function e4s_returnNoEvents($currentAge, $schedInfo, $ruleObj) {
    Entry4UISuccess('
             "events" : [],
             "athletepbs" : [],
             "entryCnts" : [],
             "ageInfo" : {
               "currentAge" : "' . $currentAge . '",
               "ageGroupId" : 0,
               "ageGroup" : "",
               "vetAgeGroupId" : 0,
               "vetAgeGroup" : ""
             },
             "schedInfo" : ' . json_encode($schedInfo) . ',
             "rules" : ' . $ruleObj . '
           ');
}
