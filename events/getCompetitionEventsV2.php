<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'events/commonEvent.php';
include_once E4S_FULL_PATH . 'entries/commonEntries.php';

$compId = (int)$compId;
$athleteId = (int)$athleteId;

$ruleObj = '{
    "id":0,
    "options" : {}
}';
$schedInfo = new stdClass();
$schedInfo->title = 'Schedule information';
$currentAge = '0';
$config = e4s_getConfig();
$compObj = e4s_GetCompObj($compId);
$athleteSQL = 'select a.*, c.Clubname club, c2.Clubname club2, s.Clubname school, ar_county.id countyid, ar_region.id regionid, ar_country.id countryid
               from ' . E4S_TABLE_ATHLETE . ' a left join
                    ' . E4S_TABLE_CLUBS . ' c on a.clubid = c.id left join
                    ' . E4S_TABLE_CLUBS . ' c2 on a.club2id = c2.id left join
                    ' . E4S_TABLE_AREA . ' ar_county on c.areaid = ar_county.id left join ' . E4S_TABLE_AREA . ' ar_region on ar_county.parentid = ar_region.id left join ' . E4S_TABLE_AREA . ' ar_country on ar_region.parentid = ar_country.id left join
                    ' . E4S_TABLE_CLUBS . " s on a.schoolid = s.id
               where a.id = $athleteId";

$athleteResult = e4s_queryNoLog($athleteSQL);
$athleteRecord =  e4s_normaliseAthleteRecord($athleteResult->fetch_assoc());

$aOptions = $athleteRecord['options'];
if (isset($aOptions->noEntryReason) and $aOptions->noEntryReason !== '') {
    $schedInfo->noEntryReason = $aOptions->noEntryReason;
    e4s_returnNoEvents($currentAge, $schedInfo, $ruleObj);
    exit();
}

$urn = $athleteRecord['URN'];

$gender = $athleteRecord['gender'];

$dob = $athleteRecord['dob'];
// Get Competition

$compRow = $compObj->getRow();

$cOptions = $compObj->getOptions();
$isE4S = isE4SUser();
$isOrg = $compObj->isOrganiser();
$override = $isOrg;
if (isset($aOptions->compOverride)) {
	$override = $aOptions->compOverride === $compId;
}
$compClosed = FALSE;
$limitReason = '';
if (isset($cOptions->compLimits)) {
    $counters = $compObj->getEntryCounts();
    $limits = $cOptions->compLimits;
    /*
        relayAthleteCnt: athletes in teams
        relayCnt: team entries
        totalCnt: indiv entries
        uniqueCnt: unique Athlete Cnt
    */
    if (isset($limits->entries)) {
        if ($counters['totalCnt'] >= $limits->entries and $limits->entries > 0) {
            $compClosed = TRUE;
            $limitReason = 'Maximum entries for competition reached : ' . $limits->entries;
        }
    }
    if (isset($limits->athletes)) {
        if ($counters['uniqueCnt'] >= $limits->athletes and $limits->athletes > 0) {
            $compClosed = TRUE;
            $limitReason = 'Maximum athletes for competition reached : ' . $limits->athletes;
        }
    }
    if (isset($limits->teams)) {
        if ($counters['relayCnt'] >= $limits->teams and $limits->teams > 0) {
            $compClosed = TRUE;
            $limitReason = 'Maximum teams for competition reached : ' . $limits->teams;
        }
    }
}
$compObj->setClubComp(true);
if ( $compObj->clubCompFullForUser() ){
    $compClosed = TRUE;
    $limitReason = 'Maximum entries for competition reached';
}

$isRegistered = isAthleteRegistered($athleteRecord, false);
$isIntAthlete = e4s_isAthleteInternational($athleteRecord);
$regExpired = FALSE;
$registrationIssue = FALSE;

$allowUnregistered = $compObj->allowUnregistered();
$allowRegistered = $compObj->allowRegistered();
$allowInternational = $compObj->allowInternational();

$u11Age = '';
$allCompDOBs = getAllCompDOBs($compId);
foreach ($allCompDOBs as $ageInfo) {
    if ($ageInfo['MaxAge'] < 11) {
        $checkDate = date_create($ageInfo['fromDate']);
        if (gettype($u11Age) === 'string') {
            $u11Age = $checkDate;
        } elseif ($checkDate < $u11Age) {
            $u11Age = $checkDate;
        }
    }
}

if ($isRegistered) {
    $nowStr = date(E4S_SHORT_DATE);
    $compDate = $compRow['Date'];
    $regDate = $athleteRecord['activeEndDate'];
    // Registered Athlete
    if(!$isOrg) {
        if (!$compObj->allowExpiredRegistration()) {
            if ((is_null($regDate) || $regDate < $compDate)) {
                $regExpired = TRUE;

                if ($allowUnregistered === FALSE) {
                    $registrationIssue = TRUE;
                }
            }
        }

        if ($allowRegistered === FALSE) {
            $registrationIssue = TRUE;
        }
    }
} else{

	// allowUnregistered is true means auto allow international athletes
	if ($allowUnregistered === FALSE) {
		if (!( $allowInternational and $isIntAthlete)){
			$registrationIssue = TRUE;
		}
	}
}
if (E4S_CURRENT_DOMAIN !== E4S_AAI_DOMAIN and $registrationIssue) {
    $athleteDob = date_create($athleteRecord['dob']);
    if ($athleteDob > $u11Age) {
        $registrationIssue = FALSE;
        $regExpired = FALSE;
    }
}
$useAgeGroupsInfo = getAgeGroupInfo($allCompDOBs, $athleteRecord['dob']);
$useAgeGroup = $useAgeGroupsInfo['ageGroup'];
$ageGroups = $useAgeGroupsInfo['ageGroups'];
$useAgeGroupIds = '0';
$useAgeGroupIdsArr = array();
foreach ($ageGroups as $value) {
    if (isset($value['agid'])) {
        $useAgeGroupIds .= ',' . $value['agid'];
        $useAgeGroupIdsArr[] = $value['agid'];
    }
}
$currentAge = $useAgeGroupsInfo['currentAge'];
$athleteSecurity = e4s_getAthleteSecurityForComp($compObj, $athleteRecord);
if (!$athleteSecurity->allowed or $useAgeGroup === null or (int)$useAgeGroup['id'] === 0 or isset($useAgeGroup['agid']) === FALSE or ($regExpired and $allowUnregistered === FALSE) or $registrationIssue) {
    if ($registrationIssue) {
        $urnMessage = '';
        if ($urn === '') {
            $urnMessage = '!!! Missing URN number in athletes profile. (Click the i icon next to athletes name)';
        }
        if ($allowRegistered) {
            $schedInfo->shortDescription = 'Competition only open to Active Registered athletes. ' . $urnMessage;
        } elseif ($allowUnregistered) {
            $schedInfo->shortDescription = 'Competition only open to Un-Registered athletes.';
        } else {
            $schedInfo->shortDescription = 'Issue with athletes registration';
        }
    } elseif (!$athleteSecurity->inArea and $athleteSecurity->inArea !== E4S_DEFAULT_ATHLETE_SECURITY) {
        $schedInfo->shortDescription = 'Athletes County/Region/Country not accepted for this competition.';
    } elseif (!$athleteSecurity->inClub and $athleteSecurity->inClub !== E4S_DEFAULT_ATHLETE_SECURITY) {
        $schedInfo->shortDescription = 'Athletes Club is not accepted for this competition.';
    } else {
        $schedInfo->shortDescription = 'No individual events available. Please contact the Competition organiser if there is an issue with the available events.';
    }

    if ($useAgeGroup === null or (int)$useAgeGroup['id'] === 0 or !isset($useAgeGroup['agid'])) {
        //        Athlete has no agegroup, get out irrelevant of user
        e4s_returnNoEvents($currentAge, $schedInfo, $ruleObj);
        exit();
    }
    if (!$isOrg and !$override) {
        e4s_returnNoEvents($currentAge, $schedInfo, $ruleObj);
        exit();
    }
}

$eventTeamsAvailable = FALSE;
$useVetAgeGroup = $useAgeGroupsInfo['vetAgeGroup'];
$ageGroupId = $useAgeGroup['agid'];
$useVetAgeGroupId = 0;
$useVetAgeGroupName = '';
if ($useVetAgeGroup !== null) {
    $useVetAgeGroupId = $useVetAgeGroup['id'];
    $useVetAgeGroupName = $useVetAgeGroup['Name'];
}

$useAgeGroup['Name'] = e4s_getVetDisplay($useAgeGroup['Name'], $gender);
$useVetAgeGroupName = e4s_getVetDisplay($useVetAgeGroupName, $gender);

// Get existing entries for the athlete
$entrySql = "SELECT e.*, u.display_name userName, u.user_email userEmail,ev.name Name
             FROM " . E4S_TABLE_ENTRIES . " as e, 
                  " . E4S_TABLE_EVENTS . " ev,
                  " . E4S_TABLE_COMPEVENTS . " as ce,
                  " . E4S_TABLE_USERS . " u
             where e.compeventid = ce.id 
             and   ce.compid = {$compId}
             and   e.athleteid = {$athleteId}
             and   ce.eventid = ev.id
             and   e.userid = u.id";

$entryResult = e4s_queryNoLog('Get Existing Events' . E4S_SQL_DELIM . $entrySql);
$compEntries = $entryResult->fetch_all(MYSQLI_ASSOC);
$useClub = $athleteRecord['club'];
$useClubId = $athleteRecord['clubid'];

foreach ($compEntries as $compEntry) {
    $compEntry = e4s_normaliseCompEntry($compEntry);
    switch ($compEntry['clubid']) {
        case $athleteRecord['club2id']:
            $useClubId = $athleteRecord['club2id'];
            $useClub = $athleteRecord['club2'];
            break;
        case $athleteRecord['schoolid']:
            $useClubId = $athleteRecord['schoolid'];
            $useClub = $athleteRecord['school'];
            break;
    }
}

$ceIds = [];
$eventSql = "SELECT ce.id as ceid
                  , ce.AgeGroupID ageGroupId
                  , ag.Name ageGroupName
                  , p.description
                  , date_format(eg.startdate,'" . ISO_MYSQL_DATE . "') startdate
                  , ce.IsOpen
                  , e.tf
                  , u.uomtype uomType
                  , u.uomoptions uomOptions
                  , p.id priceID
                  , p.saleprice
                  , p.price
                  , p.description pricedescription
                  , p.salefee
                  , p.fee stdfee
                  , p.options priceoptions
                  , IFNULL(date_format(p.saleenddate,'" . ISO_MYSQL_DATE . "'),'') saledate
                  , p.name
                  , e.id eventid
                  , e.Name
                  , e.min min
                  , e.max max
                  , eg.name eventGroup
                  , eg.options egOptions
                  , ce.split
                  , ce.within
                  , ce.split2
                  , ce.maxgroup
                  , ce.maxathletes
                  , ce.options ceoptions
                  , e.options eoptions
             from 
                  " . E4S_TABLE_COMPEVENTS . ' as ce,
                  ' . E4S_TABLE_AGEGROUPS . ' as ag,
                  ' . E4S_TABLE_EVENTGROUPS . ' as eg,
                  ' . E4S_TABLE_EVENTPRICE . ' as p,
                  ' . E4S_TABLE_EVENTS . ' as e left join ' . E4S_TABLE_UOM . ' u on u.id = e.uomid 
             where e.ID = ce.EventID
             and   ce.agegroupid = ag.id 
             and   ce.maxgroup = eg.id
             and   ce.PriceID = p.id
             and   (AgeGroupID in(' . $useAgeGroupIds . ')
             ';

if ( $compObj->hideScheduledOnly() ){
	$eventSql .= " and maxathletes > -1";
}
foreach ($useAgeGroupIdsArr as $agid) {
    $eventSql .= " or ce.options like '%\"ageGroup\":" . $agid . "%' ";
}
//or ce.options like '%\"ageGroup\":" . $ageGroupId . "%'
$eventSql .= ")
             and ce.CompID = $compId
             and e.Gender = '$gender' 
             order by eg.startdate, eg.eventno, e.name";
//and (AgeGroupID = 0 or AgeGroupID = $ageGroupId or ce.options like '%\"ageGroup\":" . $ageGroupId . "%')

$eventResult = e4s_queryNoLog('Get Events' . E4S_SQL_DELIM . $eventSql);
$eventData = array();
$maxGroups = array();
$defaultUserObj = array();
$defaultUserObj['userId'] = 0;
$defaultUserObj['userName'] = '';
$defaultUserObj['userEmail'] = '';
$priceObjs = array();
$orderObj = new e4sOrders($compId);
$countObj = e4s_getEntryCountObj($compId);
$eventObj = new e4sEvents($compId);
// CHECK THIS for new performances if within is set
$athleteObj = athleteClass::withID($athleteId);

$pbVersion = 2;
$pbs = $athleteObj->getPBsV2($compObj);
$eventRows = [];
while ($eventRow = mysqli_fetch_array($eventResult, TRUE)) {
	$ceIds[] = $eventRow['ceid'];
	$eventRows[] = $eventRow;
}
foreach($eventRows as $eventRow){
    $eventRow = e4s_getEventRowNormalised($eventRow);
    $ceOptions = $eventRow['ceoptions'];
    $egOptions = $eventRow['egOptions'];
    if ( $egOptions->entriesFrom->id > 0 ){
        $entriesFrom = $egOptions->entriesFrom;
        $srcEg = $compObj->eventGroups[$entriesFrom->id];
        $entriesFrom->eventNo = $srcEg->eventNo;
        $entriesFrom->typeNo = $srcEg->typeNo;
        $entriesFrom->name = $srcEg->name;
        $egOptions->entriesFrom = $entriesFrom;
        $ceOptions->entriesFrom = $entriesFrom;
    }
    $ceOptions = $eventObj->populateOptions($ceOptions, (int)$eventRow['maxgroup']);
    $eventRow['ceid'] = (int)$eventRow['ceid'];
    $eventRow['maxgroup'] = (int)$eventRow['maxgroup'];
    if ($compClosed) {
        e4s_markUnavailable($eventRow, $limitReason, true);
        $ceOptions = $eventRow['ceoptions']; // Not sure why this is here ??
    }

    if (!e4s_checkEGSecurity($athleteRecord, $egOptions)) {
        continue;
    }
    // ignore if set as a team event or a league event
    if (isset($ceOptions->isTeamEvent) and $ceOptions->isTeamEvent) {
        continue;
    }

    if ( $compObj->clubCompEGFullForUser($eventRow['maxgroup'])){
        e4s_markUnavailable($eventRow, 'Maximum athletes in this event');
    }
    // Check if not for registered/unregistered athletes
    $allowEventRegistered = TRUE;
    $allowEventUnRegistered = TRUE;
    $allowEventInternational = TRUE;
    // CE Event options take priority
    if (isset($ceOptions->registeredAthletes)) {
        $allowEventRegistered = $ceOptions->registeredAthletes;
    }
    if (isset($ceOptions->unregisteredAthletes)) {
        $allowEventUnRegistered = $ceOptions->unregisteredAthletes;
    }
	if (isset($ceOptions->internationalAthletes)) {
		$allowEventInternational = $ceOptions->internationalAthletes;
    }

    if (!$allowEventRegistered and $isRegistered) {
        if (!$override) {
            continue;
        }
    }
    if (!$allowEventUnRegistered and !$isRegistered) {
        if (!$override) {
            continue;
        }
    }

    if (!$isOrg) {
        // Check if CE has security
        $userObj = e4s_getUserObj(FALSE, TRUE);
        $ceEntitylevel = -1;
        $ceEntityid = -1;
        if (isset($userObj->clubs)) {
            $userclubs = $userObj->clubs;
        } else {
            $userclubs = array();
        }

        if (sizeof($userclubs) > 0) {
            $ceEntitylevel = 1;
            $ceEntityid = (int)$userclubs[0]->id;
        }

        if (isset($userObj->areas)) {
            $userareas = $userObj->areas;
        } else {
            $userareas = array();
        }
        if (sizeof($userareas) > 0) {
		    $maxEntity = $userareas[0];
		    foreach ($userareas as $area) {
		        if ($area->entitylevel > $maxEntity->entitylevel) {
		            $maxEntity = $area;
		        }
		    }
		    $ceEntitylevel = (int)$maxEntity->entitylevel;
		    $ceEntityid = (int)$maxEntity->areaid;
		}

        $entity = new stdClass();
        $entity->level = $ceEntitylevel;
        $entity->id = $ceEntityid;
        $entity->school = FALSE;

        $securityReason = e4s_checkTeamEventSecurity($ceOptions, $entity);
        if ($securityReason !== '') {
            e4s_markUnavailable($eventRow, 'Secure Event. You do not have the required permissions.', true);
        }
    }

    $eventRow['entered'] = FALSE;
    $eventRow['entryId'] = 0;
    $eventRow['paid'] = E4S_ENTRY_NOT_PAID;
    $eventRow['athleteid'] = $athleteId;

    $obj = $countObj->getAthleteInfoForCE($athleteId, $eventRow['ceid']);

    $eventRow['entryInfo'] = $obj;
    // Awaiting Nick to confirm this can go
    $eventRow['entrycnt'] = $obj->paidCount;
    $eventRow['compName'] = $compRow['Name'];
    $eventRow['user'] = $defaultUserObj;
    if (!in_array($eventRow['maxgroup'], $maxGroups)) {
        $maxGroups[] = $eventRow['maxgroup'];
    }

    $eventRow['order'] = $orderObj->eventOrderInfo();
	$eventType = $eventRow['tf'];

    $perfInfo           = new stdClass();
    $perfInfo->perf     = 0;
	if (array_key_exists($eventRow['eventid'], $pbs)) {
		$perfInfo            = $pbs[$eventRow['eventid']];
		if ( $perfInfo->sb > 0 ) {
			$perfInfo->perf = $perfInfo->sb;
		}else{
			$perfInfo->perf = $perfInfo->pb;
		}
		$perfInfo->trackedPB = true;
	}

    foreach ($compEntries as $compEntry) {
        $compEntry = e4s_normaliseCompEntry($compEntry);
        //echo "compEvent = $compEventID     ceid = " . $row['ceid'] . " <br>";
        if ((int)$eventRow['ceid'] === $compEntry['compEventID']) {
            $eventRow['entered'] = TRUE;
            e4s_markAvailable($eventRow);
            $eventRow['entryId'] = $compEntry['id'];
            $compEntry['isE4S'] = $isE4S;
            $compEntry['isOrganiser'] = $isOrg;
            $eventRow['order'] = $orderObj->eventOrderInfo($compEntry);
            $eventRow['paid'] = $compEntry['paid'];
            if ($compEntry['teambibno'] === '0' or is_null($compEntry['teambibno'])) {
                $compEntry['teambibno'] = '';
            }
	        $perfInfo->perf = $compEntry['pb'];
			$perfInfo->trackedPB = (int)$compEntry['trackedPB'] === 1;

            $eventRow['teamBibNo'] = $compEntry['teambibno'];
            $userObj = array();
            $userObj['userId'] = $compEntry['userid'];
            $userObj['userName'] = $compEntry['userName'];
            $userObj['userEmail'] = $compEntry['userEmail'];
            $eventRow['user'] = $userObj;
            unset($eventRow['userid']);
            unset($eventRow['userName']);
            unset($eventRow['userEmail']);
            break;
        }
    }
    $eventRow['clubId'] = $useClubId;
    $eventRow['clubName'] = $useClub;

    $eventRow['firstName'] = $athleteRecord['firstName'];
    $eventRow['surName'] = $athleteRecord['surName'];
    $eventRow['eoptions'] = e4s_getOptionsAsObj($eventRow['eoptions']);
    $eventRow['ceoptions'] = $ceOptions;

    // price object
    $priceObj = array();
    $priceObj['id'] = $eventRow['priceID'];
    $priceObj['priceName'] = $eventRow['name'];
    $priceObj['stdPrice'] = $eventRow['price'];
    $priceObj['curPrice'] = $eventRow['price'];
    $priceObj['curFee'] = $eventRow['stdfee'];
    $priceObj['salePrice'] = $eventRow['saleprice'];
    $priceObj['saleDate'] = $eventRow['saledate'];
    if ($priceObj['saleDate'] !== '') {
        $saleDate = strtotime($priceObj['saleDate']);
        $now = time();
        if ($saleDate > $now) {
            $priceObj['curPrice'] = $priceObj['salePrice'];
            $priceObj['curFee'] = $eventRow['salefee'];
        }
    }
    $eventRow['price'] = $priceObj;
    $priceObj['description'] = $eventRow['pricedescription'];
    $priceObj['options'] = $eventRow['priceoptions'];
    $priceObj['salefee'] = $eventRow['salefee'];
    $priceObj['stdfee'] = $eventRow['stdfee'];
    $priceObjs[$priceObj['priceName']] = $priceObj;

    unset($eventRow['priceID']);
    unset($eventRow['name']);
    unset($eventRow['saleprice']);
    unset($eventRow['saledate']);
    unset($eventRow['pricedescription']);
    unset($eventRow['priceoptions']);
    unset($eventRow['salefee']);
    unset($eventRow['stdfee']);

    // Split
    getEventNameWithSplit($eventRow, 'eventGroup');
	$perfInfo->limits = new stdClass();
	$perfInfo->limits->min = (int)$eventRow['min'];
	$perfInfo->limits->max = (int)$eventRow['max'];

	$perfInfo->uom = $eventRow['uom'];

	unset($perfInfo->options);
	unset($perfInfo->optionsSet);
	unset($perfInfo->uomOptions);

	if ( !isset($perfInfo->ageGroup) ){
		// no Pof10 record for this event
		$perfInfo->ageGroup    = $eventRow['ageGroupName'];
		$perfInfo->curAgeGroup = $eventRow['ageGroupName'];
		$perfInfo->athleteId   = $eventRow['athleteid'];
		$perfInfo->eventId     = $eventRow['eventid'];
		$perfInfo->eventName   = $eventRow['Name'];
		$perfInfo->eventType   = $eventRow['uomType'];
		$perfInfo->id          = 0;
		$perfInfo->info        = '';
		$perfInfo->min         = $eventRow['min'];
		$perfInfo->max         = $eventRow['max'];
		$perfInfo->pbAchieved  = '';
		$perfInfo->pb          = $perfInfo->perf;
		$perfInfo->sbAchieved  = '';
		$perfInfo->sb          = $perfInfo->perf;
		$perfInfo->tf          = $eventRow['tf'];
		$perfInfo->wind        = 0;
	}
	unset($eventRow['min']);
	unset($eventRow['max']);

	$perfInfo->perfText = e4s_ensureString($perfInfo->perf);
	$perfInfo->pbText = e4s_ensureString($perfInfo->pb);
	$perfInfo->sbText = e4s_ensureString($perfInfo->sb);
	if ( $eventRow['tf'] === E4S_EVENT_TRACK) {
		if ( $perfInfo->perf > 0 ) {
			$perfInfo->perfText = e4s_ensureString(resultsClass::getResultFromSeconds( $perfInfo->perf ));
		}
		if ( $perfInfo->sb > 0 ) {
			$perfInfo->sbText = e4s_ensureString(resultsClass::getResultFromSeconds( $perfInfo->sb ));
		}
		if ( $perfInfo->pb > 0 ) {
			$perfInfo->pbText = e4s_ensureString(resultsClass::getResultFromSeconds( $perfInfo->pb ));
		}
	}

	if ( is_null($perfInfo->pbAchieved) ){
		$perfInfo->pbAchieved = '';
	}if ( is_null($perfInfo->sbAchieved) ){
		$perfInfo->sbAchieved = '';
	}

	$eventRow['perfInfo'] = e4s_deepCloneToObject($perfInfo);

    // TODO Check correct isTeam Event method
    if (isset($eoptions->eventTeam) || isset($ceOptions->eventTeam)) {
//        Get the number of athletes entered by this user to the event
//        TO DO What if another user starts off the entries. How can this work. 1 user adds some to a team and another is allowed to create a team for the same area ????
	    if ( !array_key_exists(E4S_CACHE_ENTRY_CNT, $GLOBALS) ) {
		    $GLOBALS[ E4S_CACHE_ENTRY_CNT ] = [];
			foreach($ceIds as $ceid){
				$GLOBALS[ E4S_CACHE_ENTRY_CNT ][ $ceid ] = 0;
			}
		    $checkSql = 'Select count(*) cnt, compEventId
                 from ' . E4S_TABLE_ENTRIES . ' 
                 where compEventID in (' . implode(',', $ceIds) . ')
                 and   athleteid in (
                    select a.athleteid
                    from ' . E4S_TABLE_USERATHLETES . ' a
                    where userid = ' . e4s_getUserID() . '
                 )
                 group by compEventId';

		    $countResult = e4s_queryNoLog( $checkSql );
		    while ($countRow = $countResult->fetch_object()){
			    $GLOBALS[ E4S_CACHE_ENTRY_CNT ][ (int)$countRow->compEventId ] = (int)$countRow->cnt;
		    }
	    }
	    $cntToUse = $GLOBALS[ E4S_CACHE_ENTRY_CNT ][ $eventRow['ceid'] ];
        if (isset($eventRow['eoptions']->eventTeam)) {
            if (!is_null($eventRow['eoptions']->eventTeam)) {
                $eventRow['eoptions']->eventTeam->currCount = $cntToUse;
            }
        }

        if (isset($eventRow['ceoptions']->eventTeam)) {
            if (!is_null($eventRow['ceoptions']->eventTeam)) {
                $eventRow['ceoptions']->eventTeam->currCount = $cntToUse;
            }
        }
    }

    $addRow = TRUE;
    if (isset($ceOptions->mindob)) {
        if ($dob > $ceOptions->mindob) {
            $addRow = FALSE;
        }
    }
	if (!isset($ceOptions->availableTo) or $ceOptions->availableTo === '') {
		$ceOptions->availableTo = $eventRow['startdate'];
	}

    // if there is an expiry on this event/item, check and close if expired
    $availableTo = e4s_iso_to_sql($ceOptions->availableTo);
    $expiryDate = DateTime::createFromFormat('Y-m-d G:i:s', $availableTo, $tz);

    $now = new DateTime();
    if ($now > $expiryDate) {
        $eventRow['IsOpen'] = E4S_EVENT_CLOSED;
    }

    if ($isOrg and $egOptions->entriesFrom->id === 0 ) {
        // allow E4S/Organiser to always enter events. Waiting list position ? if not part of multi-event
        if ((int)$eventRow['IsOpen'] !== E4S_EVENT_OPEN) {
            $eventRow['IsOpen'] = E4S_EVENT_OPEN;
            $eventRow['ceoptions']->helpText .= ' Event Closed : Organiser Access';
            $eventRow['ceoptions']->rowOptions->autoExpandHelpText = TRUE;
        }
    }

    if (isset($ceOptions->availableFrom) and $ceOptions->availableFrom !== '') {
        // if there is an availableon on this event/item, check and dont show if not yet to be made available
        $availableFrom = e4s_iso_to_sql($ceOptions->availableFrom);

        $availableOnDate = DateTime::createFromFormat('Y-m-d G:i:s', $availableFrom, $tz);
        $now = new DateTime();
        if ($now < $availableOnDate) {
            $addRow = FALSE;
        }
    }

    $athleteSecurity = e4s_getAthleteSecurityForComp($compObj, $athleteRecord);
    if (!$athleteSecurity->allowed and !$isOrg) {
        e4s_markUnavailable($eventRow, 'Not available to the athletes club');
        $addRow = FALSE;
    }

    if (!e4s_athleteClassificationOk($athleteRecord, $eventRow)) {
//        Entry4UIError(1003,"Classifcation");
        $addRow = FALSE;
    }
    if ($addRow or $override) {
        $eventData[] = $eventRow;
    }
}

// get Comp Rules for ageGroup
$ruleSQL = 'select * from ' . E4S_TABLE_COMPRULES . " 
            where compid = $compId 
            and (ageGroupID = " . $useAgeGroup['id'] . ' or ageGroupID = 0 )
            order by ageGroupID';
$result = e4s_queryNoLog($ruleSQL);
while ($eventRow = mysqli_fetch_array($result, TRUE)) {
    // if more than 1 row return, only interested in last row
    $ruleObj = '{
        "id": ' . $eventRow['id'] . ',
        "options" : ' . ($eventRow['options']) . '
    }';
}

// CompSchedInfo
$discSql = 'select * 
            from ' . E4S_TABLE_COMPDISCOUNTS . '
            where compid = ' . $compId . '
            and agegroupid in (0,' . $useAgeGroup['id'] . ')
            order by agegroupid desc';
$discResults = e4s_queryNoLog(basename(__FILE__) . '' . E4S_SQL_DELIM . $discSql);
$priceDiscInfoObj = '';

// TODO allocate a discount to a price
// This assumes all discounts have the same sale date
// ALSO this will apply to ALL PRICES which is wrong.
// You may NOT want to apply a discount to a specific event ( Heptathlon e.t.c )
$useSalePrice = FALSE;

$eventTeamMessage = '';
if ($eventTeamsAvailable === TRUE) {
    $eventTeamMessage = ', however there are team events available ' . $athleteRecord['firstName'] . '.';
}

$timetable = '';
if (isset($cOptions->timetable)) {
    if (strtolower($cOptions->timetable) === 'provisional') {
        $timetable = 'The Timetable is ' . strtoupper($cOptions->timetable) . ' and subject to change. ';
    }
}

$schedInfoDetails = array();
$schedInfo->autoExpand = FALSE;
$schedInfo->shortDescription = 'No individual events available.' . $eventTeamMessage . ' Please contact the Competition organiser if there is an issue with the available events.';
if (isset($cOptions->helpText)) {
    if (isset($cOptions->helpText->schedule) and trim($cOptions->helpText->schedule) !== '') {
        $title = 'General';
        $schedText = $cOptions->helpText->schedule;
        if (strpos($schedText, E4S_TEXT_TITLE_DELIM) > 0) {
            $schedText = explode(E4S_TEXT_TITLE_DELIM, $schedText);
            $title = $schedText[0];
            $schedText = $schedText[1];
        }
        $schedInfo->autoExpand = FALSE;
        if ($title . $schedText !== '') {
            $compSchedInfoObj = new stdClass();
            $compSchedInfoObj->title = $title;
            $compSchedInfoObj->body = $schedText;
            $schedInfoDetails[] = $compSchedInfoObj;
        }
    }
}
/*
 * TO DO : Check if there are any manadatory perfs required
$compSchedInfoObj = new stdClass();
$compSchedInfoObj->title = "Mandatory";
$compSchedInfoObj->body = "Fill in your Performance";
$schedInfoDetails[] = $compSchedInfoObj;
*/
if (!empty($eventData)) {
    $schedInfo->autoExpand = getUserSchedInfoExpand();
    $schedInfo->rowOptions = '{"showPrice": false}';
    $schedInfo->shortDescription = $timetable . 'This schedule is bespoke to ' . $athleteRecord['firstName'] . '. Please use Contact Organiser if there is an issue with the available events.';
}
$schedInfo->schedInfoDetails = $schedInfoDetails;
$athleteRecord = e4s_cleanUpAthleteRecord($athleteRecord);

//$compClubData = new stdClass();
Entry4UISuccess('
 "events" : ' . json_encode($eventData, JSON_NUMERIC_CHECK) . ',
 "ageInfo" : {  
   "currentAge" : ' . $currentAge . ',
   "ageGroupId" : ' . $useAgeGroup['id'] . ',
   "ageGroup" : "' . $useAgeGroup['Name'] . '",
   "vetAgeGroupId" : ' . $useVetAgeGroupId . ',
   "vetAgeGroup" : "' . $useVetAgeGroupName . '"
 }, 
 "athlete" : ' . json_encode($athleteRecord, JSON_NUMERIC_CHECK) . ',
 "schedInfo" : ' . json_encode($schedInfo, JSON_NUMERIC_CHECK) . ',
 "rules" : ' . $ruleObj . ',
 "subscriptionText":"' . e4s_getSubscriptionTandC() . '",
 "clubCompInfo":' . json_encode($compObj->getClubCompData(), JSON_NUMERIC_CHECK) . '
');

function e4s_checkEGSecurity($athleteRecord, $egOptions) {
	if (isE4SUser()) {
        return TRUE;
	}
	$clubs = $egOptions->athleteSecurity->clubs;
	$counties = $egOptions->athleteSecurity->counties;
	$regions = $egOptions->athleteSecurity->regions;

	if ( empty($clubs) and empty($counties) and empty($regions) ){
		// no security set
		return true;
	}

	// eventGroup has athleteSecurity so check athlete is in it
	foreach ( $clubs as $club ) {
		if ( $club->id === (int) $athleteRecord['clubid'] or $club->id === (int) $athleteRecord['club2id'] or $club->id === (int) $athleteRecord['schoolid'] ) {
			return TRUE;
		}
	}
	foreach ( $counties as $county ) {
		if ( (int)$county->id === (int) $athleteRecord['countyId'] ) {
			return TRUE;
		}
	}
	foreach ( $regions as $region ) {
		if ( (int)$region->id === (int) $athleteRecord['regionId'] ) {
			return TRUE;
		}
	}
	return FALSE;
}
function e4s_markAvailable(&$row){
    $ceoptions = e4s_getOptionsAsObj($row['ceoptions']);
    unset($ceoptions->isNotAvailable);
    $row['ceoptions'] = $ceoptions;
    $row['IsOpen'] = E4S_EVENT_OPEN;
}
function e4s_markUnavailable(&$row, $reason) {
    $ceoptions = e4s_getOptionsAsObj($row['ceoptions']);
    $ceoptions->isNotAvailable = E4S_EVENT_NOTAVAILABLE;
    if (!isset($ceoptions->rowOptions)) {
        $ceoptions->rowOptions = new stdClass();
    }
    $ceoptions->rowOptions->autoExpandHelpText = TRUE;
    $text = '';
    if (isset($ceoptions->helpText)) {
        $text = $ceoptions->helpText;
    }
    if ($text !== '') {
        $text .= ', ';
    }
    $text .= $reason;
    $ceoptions->helpText = $text;
    $row['ceoptions'] = $ceoptions;
}

function e4s_athleteClassificationOk($athleteRecord, $eventRow) {
    $athleteClass = (int)$athleteRecord['classification'];
//    $ceOptions = $eventRow['ceoptions'];
    $eOptions = $eventRow['eoptions'];

    if (!isset($eOptions->class)) {
        return TRUE;
    }

    $eventClassifications = preg_split('~,~', $eOptions->class);
    foreach ($eventClassifications as $eventClassification) {
		if ( strpos($eventClassification, '-') > 0) {
			$classRange = preg_split( '~-~', $eventClassification );
			$min        = (int) $classRange[0];
			$max        = (int) $classRange[0];
			if ( sizeof( $classRange ) > 1 ) {
				$max = (int) $classRange[1];
			}
			if ( $athleteClass >= $min and $athleteClass <= $max ) {
				return true;
			}
		}else{
			if ( $athleteClass === (int)$eventClassification ) {
				return true;
			}
		}
    }
    return FALSE;
}

function e4s_returnNoEvents($currentAge, $schedInfo, $ruleObj) {
    Entry4UISuccess('
             "events" : [],
             "athletepbs" : [],
             "entryCnts" : [],
             "ageInfo" : {
               "currentAge" : "' . $currentAge . '",
               "ageGroupId" : 0,
               "ageGroup" : "",
               "vetAgeGroupId" : 0,
               "vetAgeGroup" : ""
             },
             "schedInfo" : ' . json_encode($schedInfo) . ',
             "rules" : ' . $ruleObj . '
           ');
}
function e4s_normaliseAthleteRecord($athleteRecord){
    $athleteRecord['id'] = (int)$athleteRecord['id'];
    if (is_null($athleteRecord['URN'])) {
        $athleteRecord['URN'] = '';
    }else{
        $athleteRecord['URN'] = (int)$athleteRecord['URN'];
    }
    if ($athleteRecord['gender'] === '') {
        $athleteRecord['gender'] = E4S_GENDER_MALE;
    }
    $athleteRecord['classification'] = (int)$athleteRecord['classification'];
    $athleteRecord['clubid'] = (int)$athleteRecord['clubid'];
    $athleteRecord['club2id'] = (int)$athleteRecord['club2id'];
    $athleteRecord['schoolid'] = (int)$athleteRecord['schoolid'];
    $athleteRecord['clubId'] = (int)$athleteRecord['clubid'];
    $athleteRecord['club2Id'] = (int)$athleteRecord['club2id'];
    $athleteRecord['schoolId'] = (int)$athleteRecord['schoolid'];
    $athleteRecord['countyId'] = (int)$athleteRecord['countyid'];
    $athleteRecord['regionId'] = (int)$athleteRecord['regionid'];
    $athleteRecord['countryId'] = (int)$athleteRecord['countryid'];
    $athleteRecord['pof10Id'] = $athleteRecord['pof10id'];
    $athleteRecord['options'] = e4s_getOptionsAsObj($athleteRecord['options']);
    $athleteRecord['firstName'] = formatAthleteFirstname($athleteRecord['firstName']);
    $athleteRecord['surName'] = formatAthleteSurname($athleteRecord['surName']);
    return $athleteRecord;
}
function e4s_cleanUpAthleteRecord($athleteRecord){
    unset($athleteRecord['pof10id']);
    unset($athleteRecord['clubid']);
    unset($athleteRecord['club2id']);
    unset($athleteRecord['schoolid']);
    unset($athleteRecord['regionid']);
    unset($athleteRecord['countryid']);
    unset($athleteRecord['countyid']);
    return $athleteRecord;
}
function e4s_normaliseCompEntry($compEntry){

    $compEntry['id'] = (int)$compEntry['id'];
    $compEntry['compEventID'] = (int)$compEntry['compEventID'];
    $compEntry['athleteid'] = (int)$compEntry['athleteid'];
    $compEntry['bibno'] = (int)$compEntry['bibno'];
    $compEntry['pb'] = (float)$compEntry['pb'];
    $compEntry['variationID'] = (int)$compEntry['variationID'];
    $compEntry['clubid'] = (int)$compEntry['clubid'];
    $compEntry['orderid'] = (int)$compEntry['orderid'];
    $compEntry['paid'] = (int)$compEntry['paid'];
    $compEntry['price'] = (float)$compEntry['id'];
    $compEntry['discountid'] = (int)$compEntry['discountid'];
    $compEntry['ageGroupID'] = (int)$compEntry['ageGroupID'];
    $compEntry['vetAgeGroupID'] = (int)$compEntry['vetAgeGroupID'];
    $compEntry['teamid'] = (int)$compEntry['teamid'];
    $compEntry['userid'] = (int)$compEntry['userid'];
    $compEntry['options'] = e4s_getOptionsAsObj($compEntry['options']);
    $compEntry['waitingPos'] = (int)$compEntry['waitingPos'];
    $compEntry['entryPos'] = (int)$compEntry['entryPos'];
    $compEntry['checkedin'] = (int)$compEntry['checkedin'];
    $compEntry['present'] = (int)$compEntry['present'];

    return $compEntry;
}
function e4s_getEventRowNormalised($eventRow){
    $eventRow['ceoptions'] = e4s_mergeAndAddDefaultCEOptions($eventRow['ceoptions'], $eventRow['eoptions'], E4S_OPTIONS_OBJECT);
    $eventRow['egOptions'] = e4s_addDefaultEventGroupV2Options($eventRow['egOptions']);
    $eventRow['eoptions'] = e4s_addDefaultEventOptions($eventRow['eoptions']);
    $eventRow['startdate'] .= e4s_getOffset($eventRow['startdate']);
    $eventRow['saledate'] .= e4s_getOffset($eventRow['saledate']);

    $eventRow['ageGroupId'] = (int)$eventRow['ageGroupId'];

    $eventRow['IsOpen'] = (int)$eventRow['IsOpen'];
    $eventRow['priceID'] = (int)$eventRow['priceID'];
    $eventRow['saleprice'] = (float)$eventRow['saleprice'];

    $eventRow['price'] = (float)$eventRow['price'];
    $eventRow['salefee'] = (float)$eventRow['salefee'];
    $eventRow['stdfee'] = (float)$eventRow['stdfee'];
    $eventRow['eventid'] = (int)$eventRow['eventid'];
    $eventRow['eventGroupId'] = (int)$eventRow['maxgroup'];
    $eventRow['maxathletes'] = (int)$eventRow['maxathletes'];
    $eventRow['priceoptions'] = e4s_getOptionsAsObj($eventRow['priceoptions']);

    return $eventRow;
//    $eventRow['ageGroup'] = e4s_getAgeGroupInfo($eventRow)['ageGroup'];
//    $eventRow['ceid'] = (int)$eventRow['ceid'];
//    return $eventRow;
}
