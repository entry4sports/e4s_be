<?php
include_once E4S_FULL_PATH . 'admin/commonSecurity.php';
include_once E4S_FULL_PATH . 'eventteams/commonEventTeam.php';

function e4s_getAgeGroupInfo(&$row) {
    $metaModel = new stdClass();
    $metaModel->id = (int)$row['ageGroupID'];
    if ($metaModel->id !== 0) {
        include_once E4S_FULL_PATH . 'builder/ageCRUD.php';
        $row['ageGroup'] = e4s_readAge($metaModel, FALSE);
    } else {
        $row['ageGroup'] = $metaModel;
    }

    unset($row['ageGroupID']);

    return $row;
}