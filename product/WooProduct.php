<?php
/**
 * Created by PhpStorm.
 * User: pauld
 * Date: 08/09/2018
 * Time: 07:40
 */

function wooProductString($productObj, $priceRow) {
    $prodId = 0;
    if ($productObj !== null) {
        if (is_array($productObj)) {
            $prodId = $productObj['productid'];
        } else {
            $prodId = $productObj;
        }
    }
    $price = 0;
    if ($priceRow !== null) {
        if (array_key_exists('usePrice', $priceRow)) {
            $price = $priceRow['usePrice'];
        } else {
            $price = $priceRow['price'];
        }
    }
    return '{
                "productId" :' . $prodId . ',
                "productPrice" :' . $price . '}';
}

function wooProductObj($productArr, $priceRow) {
    return json_decode(wooProductString($productArr, $priceRow));
}

function createWooProductPurchaseNote($eventTeamArr, $wooProductObj) {
    if (!isset($wooProductObj->productId)) {
        return;
    }
    $productId = $wooProductObj->productId;
    if (!array_key_exists('athletes', $eventTeamArr)) {
        return;
    }
    $athletesArr = $eventTeamArr['athletes'];

    $athleteids = '';
    $athletesep = '';
    $athleteNames = '';
    foreach ($athletesArr as $athlete) {
        if (array_key_exists('athlete', $athlete)) {
            $athleteNames .= $athletesep . $athlete['athlete'];
        } else {
            $athleteids .= $athletesep . $athlete['id'];
        }
        $athletesep = ',';
    }
    $athletesep = '';
    $athleteNames = 'Athletes : ' . $athleteNames;
    if ($athleteids !== '') {
        $sql = "select concat(firstname, ' ', surname) athlete
            from " . E4S_TABLE_ATHLETE . '
            where id in (' . $athleteids . ')';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return;
        }
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        foreach ($rows as $row) {
            $athleteNames .= $athletesep . addslashes($row['athlete']);
            $athletesep = ', ';
        }
    }

    // incase there is already one, delete it
    $sql = 'delete from ' . E4S_TABLE_POSTMETA . '
            where post_id = ' . $productId . "
            and meta_key = '" . E4S_POSTMETA_PURCHASE_NOTE . "'";
    e4s_queryNoLog($sql);
    $sql = 'insert into ' . E4S_TABLE_POSTMETA . '( post_id ,meta_key, meta_value)
            values (' . $productId . ",'" . E4S_POSTMETA_PURCHASE_NOTE . "','" . addslashes($athleteNames) . "')";
    e4s_queryNoLog($sql);
}

function createWooProductWithPriceObj($compid, $postTitle, $desc, $priceObj) {
    global $conn;
    $productArr = createWooProduct($conn, $compid, $postTitle, $desc, $priceObj);
    return wooProductObj($productArr, $priceObj);
}

function createWooProduct($conn, $compid, $postTitle, $desc, &$priceRow) {

    $regPrice = $priceRow['usePrice'];
    $salePrice = $priceRow['saleprice'];
    $saleDate = $priceRow['saleenddate'];
    $usePrice = $regPrice;

//    Entry4_StartTransaction();
    if ($saleDate === '' || is_null($saleDate)) {
        // No sale date set so use regular price
        $salePrice = $regPrice;
    }

    if ($saleDate !== '' and !is_null($saleDate)) {
        $saleDate = strtotime($saleDate);
        $now = time();

        if ($saleDate > $now) {
            $usePrice = $salePrice;
        }
    }

// check if it exists first
    $prodResult = e4s_getWcProduct(addslashes($postTitle));

    if ($prodResult->num_rows === 0) {
        $sql = 'INSERT INTO ' . E4S_TABLE_POSTS . " ( post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_Status, ping_status, post_password, post_name,to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count)
        VALUES (2, UTC_TIMESTAMP(), UTC_TIMESTAMP(), '{" . addslashes($desc) . "}', '" . addslashes($postTitle) . "','compid:" . $compid . "','" . WC_POST_PUBLISH . "','closed','closed','', concat('" . $compid . "',date_format(UTC_TIMESTAMP(),'%d%m%Y%H%i%s')),'','', UTC_TIMESTAMP(), UTC_TIMESTAMP(),'',0,'',0,'product','',0) ";

        if (e4s_queryNoLog($sql) === TRUE) {
            $productId = $conn->insert_id;

//            $terms = array( 'exclude-from-catalog', 'exclude-from-search' );
            $terms = array();
            wp_set_object_terms($productId, $terms, 'product_visibility');

            // Create the postmeta records from the base Product
            $baseVirtualProductID = getBaseVirtualProduct($conn);
            $sql = 'SELECT * FROM ' . E4S_TABLE_POSTMETA . ' WHERE post_id = ' . $baseVirtualProductID;
            $baseProdResult = e4s_queryNoLog($sql);

            if ($baseProdResult->num_rows > 0) {
                $updateSql = 'INSERT INTO ' . E4S_TABLE_POSTMETA . ' (post_id,  meta_key, meta_value ) VALUES ';
				$sep = '';
                while ($row = $baseProdResult->fetch_assoc()) {
                    $rowKey = $row['meta_key'];
                    $rowValue = $row['meta_value'];
                    $sendNull = FALSE;

                    switch ($rowKey) {
                        case '_sku':
                            $rowValue = $productId;
                            break;
                        case '_sale_price':
                            $rowValue = $salePrice;
                            //$rowValue = 0;
                            break;
                        case '_sale_price_dates_to':
                            $rowValue = $saleDate;
                            //$rowValue = 0;
                            break;
                        case '_manage_stock':
                            $rowValue = 'no';
                            break;
                        case '_stock':
                            $rowValue = '1';
                            break;
                        case '_regular_price':
                            $rowValue = $regPrice;
                            //$rowValue = 0;
                            break;
                        case '_price':
                            $rowValue = $usePrice;
                            //$rowValue = 0;
                            break;
                    }
                    $updateSql .= $sep;
                    if ($sendNull === TRUE) {
                        $updateSql .= "(" . $productId . ",'" . $rowKey . "' ,NULL)";
                    } else {
                        $updateSql .= "(" . $productId . ",'" . $rowKey . "','" . $rowValue . "')";
                    }
                    $sep = ',';
                }
                if (e4s_queryNoLog($updateSql) === FALSE) {
	                Entry4UIError(1001, $conn->error, 400, '');
                }
            }
        } else {
            Entry4UIError(1003, $conn->error, 400, '');
        }
        $prodResult = e4s_getWcProduct($productId, 'ID');

        $compObj = e4s_GetCompObj($compid);
        $compObj->add_Category_to_Product($productId);
    }
    $fetchedRow = $prodResult->fetch_assoc();
	$productId = (int)$fetchedRow['ID'];
    $fetchedRow['productid'] = $productId;

	$guid = 'https://' . E4S_CURRENT_DOMAIN . '/?post_type=product&p=' . $productId;
    $sql = 'update ' . E4S_TABLE_POSTS . "
            set post_status = '" . WC_POST_PUBLISH . "',
                guid = '" . $guid . "'
            where id = ". $productId;
    e4s_queryNoLog($sql);

//    Entry4_Commit();
    return $fetchedRow;
}

function e4s_getWcProduct($fieldValue, $fieldName = 'post_title'){
    $sql = 'select *
                from ' . E4S_TABLE_POSTS . '
                where ' . $fieldName . ' = "' . $fieldValue . '"';

    return e4s_queryNoLog($sql);
}
function updateWooProduct($id, $postTitle, $desc) {
    $sql = 'update ' . E4S_TABLE_POSTS . "
            set post_title = '" . addslashes($postTitle) . "',
            post_content = '" . addslashes($desc) . "',
                post_modified_gmt = UTC_TIMESTAMP
            where id = " . $id;
    e4s_queryNoLog($sql);
}