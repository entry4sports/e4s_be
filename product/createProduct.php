<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'product/WooProduct.php';
include_once E4S_FULL_PATH . 'entries/createEntry.php';
//function e4s_createProduct($ceId, $athleteId, $useClubId, $teamId, $sourceId = 0, $exit = TRUE) {
function e4s_createProduct($productObj, $exit = TRUE) {
    $ceId = $productObj->ceId;
    $athleteId = $productObj->athleteId;
    if ( !isset($productObj->sourceId )){
        $productObj->sourceId = 0;
    }
    $sql = "Select ce.*, date_format(eg.startdate,'%H:%i') as prettyStartTime, date_format(eg.startdate,'%D %b %Y') as prettyStartDate , ag.Name ageGroupName, ag.minAge minage,eg.eventNo, eg.name eventName
        from " . E4S_TABLE_COMPEVENTS . ' ce,
             ' . E4S_TABLE_AGEGROUPS . ' ag,
              ' . E4S_TABLE_EVENTGROUPS . ' eg               
        where ce.id = ' . $ceId . '
        and   ce.maxgroup = eg.id
        and   ag.id = ce.AgeGroupID';
    $result = e4s_queryNoLog($sql);

    $desc = '';
    if ($result->num_rows < 1) {
        Entry4UIError(1000, 'Failed to get the competition event', 400, '');
    }
    $ceRow = $result->fetch_assoc();
    $productObj->ceRow = $ceRow;
    $compId = (int)$ceRow['CompID'];
    $egId = (int)$ceRow['maxGroup'];
    $compObj = e4s_getCompObj($compId);

    // if club Comp, override club id
    $clubComp = $compObj->checkClubComp();
    if ( $clubComp > 0 ){
        $clubId = $compObj->getClubId();
        if ( $clubId > 0 ){
            $productObj->useClubId = $clubId;
            $productObj->entity = e4s_getEntityKey(1 ,$clubId);
        }
    }

//    $egObjOrig = eventGroup::getEgObj($egId);
    $egObj = $compObj->getEventGroupByEgId($egId);
//    $event = $eventRow['Name'];
    $event = $egObj->name;

    $sql = 'select * from ' . E4S_TABLE_ATHLETE . ' where id = ' . $athleteId;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows > 0) {
        $athleteRow = $result->fetch_assoc();
    } else {
        // raise error
    }

    $athleteRow['firstName'] = formatAthleteFirstname($athleteRow['firstName']);
    $athleteRow['surName'] = formatAthleteSurname($athleteRow['surName']);
    $athlete = $athleteRow['firstName'] . ' ' . $athleteRow['surName'];

    $urn = $athleteRow['URN'];
    $productObj->athleteRow = $athleteRow;
    $compRow = $compObj->getFullRow();
    $ceObj= $compObj->getCEByCeID($ceId);
    $ceOptions = $ceObj->options;
    $productObj->ceOptions = $ceOptions;
// check if PB is mandatory
    if ($ceOptions->mandatoryPB) {
        if ( is_null($productObj->perf) or $productObj->perf == 0 ) {
			Entry4UIError(1001, 'A Performance is mandatory for this event', 200, '');
        }
    }
    if ( !isset($productObj->cOptions)){
        $productObj->cOptions = $compObj->getOptions();
    }
    $cOptions = $productObj->cOptions ;
// Check for singleAge
    $ignoreCompOptions = FALSE;
    if ($ceOptions->singleAge === FALSE) {
        // ce is to ignore comp singleAge
        $ignoreCompOptions = TRUE;
    }

    if ($ignoreCompOptions === FALSE) {
        if ($cOptions->singleAge === TRUE) {
            $currentAgeGroupID = $ceRow['AgeGroupID'];
            $sql = 'select e.agegroupid ageGroupID, eventagegroup, ce.options ceoptions, ag.minAge minage
			        from ' . E4S_TABLE_ENTRIES . ' e,
			             ' . E4S_TABLE_COMPEVENTS . ' ce,
			             ' . E4S_TABLE_AGEGROUPS . ' ag
			        where ce.id = e.compeventid
			        and e.athleteid = ' . $athleteRow['id'] . '
			        and ag.id = ce.agegroupid
			        and ce.compid = ' . $compId;
            $singleResults = e4s_queryNoLog($sql);
            $existingRows = $singleResults->fetch_all(MYSQLI_ASSOC);
            foreach ($existingRows as $existingRow) {
                if ($existingRow['ceoptions'] === '') {
                    continue;
                }
                if ($existingRow['ageGroupID'] === $currentAgeGroupID) {
                    continue;
                }
                // both athlete and event in the Vet age range
                if ($existingRow['minage'] >= 35 and $ceRow['minage'] >= 35) {
                    continue;
                }
                $existing_ceoptions = e4s_getOptionsAsObj($existingRow['ceoptions']);

                if (isset($existing_ceoptions->singleAge)) {
                    if ($existing_ceoptions->singleAge === FALSE) {
                        continue;
                    }
                }
                Entry4UIError(1002, $athleteRow['firstName'] . ' has already been entered into the ' . $existingRow['eventagegroup'] . ' age group. Competition is configured for single age group only.', 200, '');
            }
        }
    }

    $teamName = '';
    $teamId = 0;
    if ( isset($productObj->teamId)) {
        $teamId = $productObj->teamId;
    }
	$autoEntry = 0;
    if ( isset($productObj->autoEntry)) {
	    $autoEntry = $productObj->autoEntry ? 1 : 0;
    }
    if ($teamId !== 0) {
        $sql = 'select name
                from ' . E4S_TABLE_COMPTEAM . '
                where id = ' . $teamId;
        $teamResult = e4s_queryNoLog($sql);
        $teamRow = $teamResult->fetch_assoc();
        $teamName = $teamRow['name'] . ' - ';
    }

    $desc .= '"compid":' . $compId;
    $compName = $compObj->getName();
    $compName = str_replace('"', "'", $compName);
    $desc .= ',"compname":"' . addslashes($compName) . '"';
    $desc .= ',"athleteid":' . $athleteId;
    $desc .= ',"athlete":"' . $athlete . '"';
    if (isAdminUser()) {
        $desc .= ',"ignore":true';
    }
    $desc .= ',"autoEntry":' . $autoEntry;
    $desc .= ',"ceid":' . $ceId;
    $desc .= ',"event":"' . $event . '"';
    $desc .= ',"compclubid":' . $compRow['compOrgId'];
    $desc .= ',"locationid":' . $compRow['location']->id;
    $desc .= ',"teamid":' . $teamId;

    $ageGroupName = $ceRow['ageGroupName'];
    $xbText = ' - ' . str_replace('Under ', 'U', $ageGroupName);

    if (isset($ceOptions->xbText)) {
        $xbText = $ceOptions->xbText;
    }
    $urnDisplay = '. ';
	if ( is_null($urn)){
		$urn = '';
	}
    if (trim($urn) !== '') {
        $urnDisplay = ' (' . $urn . '). ';
    }
    $postTitle = $ceId . ' : ' . $compObj->getName() . ' ' . $ceRow['prettyStartDate'] . '. ' . $teamName . $athlete . $urnDisplay . $event . $xbText;
    if ($ceRow['prettyStartTime'] !== '00:00') {
        $postTitle .= ' at ' . $ceRow['prettyStartTime'];
    }

// get Price info
    $priceRow = getUseEventPrice($ceRow['PriceID']);

    $productArr = createWooProduct($GLOBALS['conn'], $compId, $postTitle, $desc, $priceRow);

    $compCountObj = e4s_getEntryCountObj($compId);
    $productObj->wcProduct = $productArr;
    $productObj->productId = $productArr['productid'];
    $productObj->ceOptions = $ceOptions;
    $productObj->priceRow = $priceRow;
    $productObj->compId = $compId;

    $entryInfoObj = e4s_createEntry($productObj, $exit);

    $priceRow['usePrice'] = $GLOBALS['useProdPrice'];
    $data = wooProductObj($productArr, $priceRow);
    $data->entryInfo = $compCountObj->getAthleteInfoForCE($athleteId, $ceId);
    $data->entryInfo->entryId = $entryInfoObj->entryId;
    $data->entryInfo->paid = $entryInfoObj->paid;
    $data->entryInfo->orderId = 0;
    if ($exit) {
        // if OTD then send socket Message
        if (isset($entryInfoObj->meta)) {
            if (isset($entryInfoObj->meta->eOptions)) {
                if ($entryInfoObj->meta->eOptions->otd) {
                    e4s_buildOTDEntrySocket($compId, $entryInfoObj);
                }
            }
            if (!is_null($entryInfoObj->meta->bibInfo)) {
                $bibInfo = $entryInfoObj->meta->bibInfo;
                // new Bib number has been allocated
                if ((int)$bibInfo->teamBibNo !== 0 or (!$bibInfo->exists and $bibInfo->bibNo > 0)) {
                    // Been given a bibNo on a free entry
                    $bibNo = $bibInfo->bibNo;
                    if ((int)$bibInfo->teamBibNo !== 0) {
                        $bibNo = $bibInfo->teamBibNo;
                    }
                    Entry4UIError(200, 'Allocated Bib ' . $bibNo);
                }
            }
        }
        $meta = new stdClass();
        $meta->clubCompInfo = $compObj->getClubCompData();
        Entry4UISuccess('
        "data":' . json_encode($data, JSON_NUMERIC_CHECK) . ',
        "meta":' . json_encode($meta, JSON_NUMERIC_CHECK));

    }
    return $data;
}

function e4s_buildOTDEntrySocket($compId, $obj) {
    $socket = new socketClass($compId);
    unset($obj->meta->athleteRecord->dob);
    unset($obj->meta->postRecord);
    $entry = new stdClass();
    $entry->id = $obj->entryId;
    $entry->paid = $obj->paid;
    $entry->options = e4s_getOptionsAsObj(e4s_getOptionsAsString($obj->meta->eOptions));
    unset($obj->meta->eOptions);
    $obj->meta->entryInfo = $entry;
    $options = $obj->meta->ceRecord['options'];
    $obj->meta->ceRecord['options'] = e4s_getOptionsAsObj($options);
    $ageGroup = $obj->meta->ageGroup;
    if (array_key_exists('ageGroup', $ageGroup)) {
        $ag = $ageGroup['ageGroup'];
        $options = e4s_getOptionsAsObj($ag['options']);
        $obj->meta->ageGroup['ageGroup']['options'] = $options;
    }
    $socket->setPayload($obj->meta);
    $socket->sendMsg(R4S_SOCKET_ENTRIES_ADD_ENTRY);
}

function checkCouponValidity($productArr, $athleteRow, $compRow, $eventRow) {

    // loop through any coupons to see if the new product should be allowed
    $couponSql = 'Select *
                  from ' . E4S_TABLE_COUPONRULES;
    $result = e4s_queryNoLog($couponSql);
    if ($result->num_rows === 0) {
        exit();
    }
    $couponRules = mysqli_fetch_all($result, MYSQLI_ASSOC);

    foreach ($couponRules as $couponRow) {
        $operator = $couponRow['operator'];
        $addproduct = FALSE;
        $and = ($operator === 1);
        $or = ($operator === 0);

        $athleteResult = checkAndAddCoupon($couponRow['athlete'], $athleteRow);
        $compResult = checkAndAddCoupon($couponRow['competition'], $compRow);
        $eventResult = checkAndAddCoupon($couponRow['event'], $eventRow);

        if ($and) {
            $addproduct = TRUE;
            if ($athleteResult === FALSE or $compResult === FALSE or $eventResult === FALSE) {
                $addproduct = FALSE;
            }
        }
        if ($or) {
            $addproduct = FALSE;
            if ($athleteResult === TRUE or $compResult === TRUE or $eventResult === TRUE) {
                $addproduct = TRUE;
            }
        }

        if ($addproduct) {
            $sql = 'select *
                    from ' . E4S_TABLE_POSTMETA . '
                    where post_id = ' . $couponRow['coupon'] . "
                    and   meta_key = 'product_ids'";
            $result = e4s_queryNoLog($sql);
            $productIds = '';
            $updateSql = '';
            if ($result->num_rows === 1) {
                $metaRow = $result->fetch_assoc();
                if ($metaRow['meta_value'] !== null) {
                    $productIds = $metaRow['meta_value'];
                }
                if ($productIds === '' or strpos($productArr['productid'], $productIds) === FALSE) {
                    if ($productIds !== '') {
                        $productIds .= ',';
                    }
                    $productIds .= $productArr['productid'];
                    $updateSql = 'update ' . E4S_TABLE_POSTMETA . "
                              set meta_value = '" . $productIds . "'
                              where post_id = " . $couponRow['coupon'] . "
                              and   meta_key = 'product_ids'";
                }
            } else {
                $updateSql = 'Insert into ' . E4S_TABLE_POSTMETA . ' ( post_id, meta_key, meta_value)
                              values (' . $couponRow['coupon'] . ",'product_ids','" . $productIds . "')";
            }
            if ($updateSql !== '') {
                e4s_queryNoLog($updateSql);
            }
        }
    }
}

function checkAndAddCoupon($couponTxt, $data) {
    if (is_null($couponTxt) or $couponTxt === '') {
        return TRUE;
    }
    $params = explode(':', $couponTxt);
    $pos = strpos($params[0], ',');
    $checkValue = '';

    if ($pos === FALSE) {
        $checkValue = $data[$params[0]];
    } else {
        // combine fields eg firstName,surName
        $fields = explode(',', $params[0]);
        foreach ($fields as $index => $name) {
            $checkValue .= $data[$name];
        }
    }
    $pos = strpos($params[1], ',');
    if ($pos === FALSE) {
        if (fnmatch($params[1], $checkValue)) {
            return TRUE;
        }
    } else {
        $values = explode(',', $params[1]);
        foreach ($values as $index => $value) {
            if (fnmatch($value, $checkValue)) {
                return TRUE;
            }
        }
    }
    return FALSE;
}
