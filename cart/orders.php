<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'product/commonProduct.php';
function updateOrder($obj) {
    /*
     * !!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!
     * Ensure the trigger on the _posts table is matched
     * DROP TRIGGER IF EXISTS `Update entry paid`;
     * CREATE DEFINER=`dbo754251800`@`%` TRIGGER `Update entry paid`
     * AFTER UPDATE ON `Entry4_posts`
     * FOR EACH ROW
     * begin
     *   DECLARE usePaid integer;
     *   set usePaid := 0;
     *   if NEW.post_status = 'wc-completed' then
     *     set usePaid := 1;
     *   end if;
     *   if NEW.post_status = 'wc-on-hold' then
     *     set usePaid := 2;
     *   end if;
     *   if NEW.post_type = '" . WC_POST_ORDER . "' then
     *     update Entry4_Entries
     *       set paid = usepaid
     *       where orderid = NEW.ID;
     *     select row_Count() into @rowCount;
     *     if (@rowCount < 1) then
     *       update Entry4_EventTeamEntries
     *         set paid = usepaid
     *         where orderid = NEW.ID;
     *    end if;
     *   END IF;
     * END
     */
    hasChqPermission();
    $orderids = checkJSONObjForXSS($obj, 'orderIds:Order Ids');
    $paidStatus = checkFieldForXSS($obj, 'paid:Paid Status');
    foreach ($orderids as $orderid) {
        $id = $orderid['id'];
        $sql = 'select post_status status
                from ' . E4S_TABLE_POSTS . '
                where id = ' . $id;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(9031, 'Invalid Order ID : ' . $id, 400, '');
        }
        $currentRow = $result->fetch_assoc();
        if (is_null($paidStatus)) {
            $setStatus = e4s_toggleStatus($currentRow['status']);
        } else {
            $setStatus = e4s_getOrderStatus($paidStatus);
            if ($paidStatus === E4S_ENTRY_NOT_PAID) {
                // delete the post/postmeta and update the Entries
                e4s_markEntryNotPaid($id);
                Entry4UISuccess('');
            }
        }
        $updateSql = 'update ' . E4S_TABLE_POSTS . "
                      set post_status = '" . $setStatus . "'
                      where id = " . $id;
        e4s_queryNoLog($updateSql);
    }
    Entry4UISuccess('');
}

function e4s_markEntryNotPaid($orderId) {
    // delete the WordPress/WooCommerce data
    e4s_removeWCData($orderId);
    $sql = 'update ' . E4S_TABLE_ENTRIES . '
            set orderid = 0,
                paid = ' . E4S_ENTRY_NOT_PAID . '
            where orderid = ' . $orderId;
    e4s_queryNoLog($sql);
}

function e4s_getOrderStatus($paidStatus) {
    switch ($paidStatus) {
        case E4S_ENTRY_NOT_PAID:
            return WC_ORDER_PENDING;
            break;
        case E4S_ENTRY_AWAITING_PAYMENT:
            return WC_ORDER_ONHOLD;
            break;
        case E4S_ENTRY_PAID:
            return WC_ORDER_PAID;
            break;
    }
}

function e4s_toggleStatus($currentStatus) {
    $setStatus = WC_ORDER_PAID;
    if ($currentStatus === $setStatus) {
        $setStatus = WC_ORDER_ONHOLD;
    }
    return $setStatus;
}

function getOnHoldOrders() {
    hasChqPermission();
    $sql = 'select p.id, pm.meta_key, pm.meta_value
            from ' . E4S_TABLE_POSTS . ' p,
                 ' . E4S_TABLE_POSTMETA . " pm
            where post_status = '" . WC_ORDER_ONHOLD . "'
                  and p.id = pm.post_id
                  and pm.meta_key in ('_order_total','_customer_user','_billing_email')
                  order by p.id";

    $sql = "select p.id,p.post_status,DATE_FORMAT(post_date_gmt,'" . ISO_MYSQL_DATE . "') orderdate ,pm.meta_key, pm.meta_value
            from " . E4S_TABLE_POSTS . ' p,
                 ' . E4S_TABLE_POSTMETA . " pm
            where post_status in ('" . WC_ORDER_ONHOLD . "','" . WC_ORDER_PAID . "')
                  and p.id = pm.post_id
                  and pm.meta_key in ('_order_total','_customer_user','_billing_email')
                  and p.id in (select p2.post_id from " . E4S_TABLE_POSTMETA . " p2 where meta_key = '_payment_method' and meta_value = 'cheque')
                  order by p.id";
    $results = e4s_queryNoLog($sql);
    if ($results->num_rows === 0) {
        Entry4UIError(9032, 'No orders found for cheques.', 200, '');
//        Entry4UISuccess('
//        "data":[]'
//        );
    }

    $rows = $results->fetch_all(MYSQLI_ASSOC);

    $orders = array();
    $order = array();
    $lastid = 0;
    foreach ($rows as $row) {
        $rowid = (int)$row['id'];
        if ($lastid !== $rowid and $lastid !== 0) {
            $orders[] = $order;
            $order = array();
        }
        $lastid = $rowid;
        $order['id'] = $row['id'];
        $order['competition'] = e4s_getCompForChq($row['id']);
        $order['orderDate'] = $row['orderdate'] .= e4s_getOffset($row['orderdate']);
        $status = $row['post_status'];
        if ($status === WC_ORDER_PAID) {
            $status = 'Cheque Received';
        }
        if ($status === WC_ORDER_ONHOLD) {
            $status = 'Awaiting Cheque';
        }
        $order['status'] = $status;
        switch ($row['meta_key']) {
            case '_order_total':
                $order['total'] = number_format($row['meta_value'], 2);
                break;
            case '_customer_user':
                $order['userId'] = $row['meta_value'];
                break;
            case '_billing_email':
                $order['userEmail'] = $row['meta_value'];
                break;
        }
    }
    $orders[] = $order;

    Entry4UISuccess('
        "data":' . json_encode($orders));
}

function e4s_getCompForChq($orderID) {
    $sql = 'select c.id, c.name
             from ' . E4S_TABLE_COMPETITON . ' c,
                  ' . E4S_TABLE_COMPEVENTS . ' ce,
                  ' . E4S_TABLE_ENTRIES . " e
             where e.orderid = {$orderID}
             and   e.compeventid = ce.id
             and   c.id = ce.compid
     ";
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows === 0) {
        return array();
    }
    return $result->fetch_assoc();
}

function hasChqPermission() {
//    e4s_getUserID() = -1;
    if (isAppAdmin() or userHasPermission(PERM_FINANCE, 0, 0)) {
        return;
    }
    Entry4UIError(9033, 'Unauthorised', 401, '');
}