<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'eventteams/commonEventTeam.php';
include_once E4S_FULL_PATH . 'stripe/e4sStripe.php';

const E4S_CART_MESSAGE_TYPE = 'notice';
const E4S_CART_ERROR_TYPE = 'error';
function getWCCart() {
//    WC()->frontend_includes();
    $userId = e4s_getUserID();
    $obj = new stdClass();
    $obj->url = wc_get_cart_url();
    $obj->currency = get_woocommerce_currency_symbol();

    $obj->items = array();
    if ($GLOBALS[E4S_LOCAL_HOST] === TRUE) {
        // Get an instance of the WC_Session_Handler Object
        $session_handler = new WC_Session_Handler();
// Get the user session from its user ID:
        $session = $session_handler->get_session($GLOBALS[E4S_USER_ID]);
// Get cart items array
//        $useCart = maybe_unserialize($session['cart']);
        Entry4UISuccess();
    } else {
        $useCart = WC()->cart->get_cart();
    }

    if (is_null($useCart)) {
        Entry4UIError(8300, 'Failed to get cart');
    }
    $productIds = array();
    $e4sEntries = array();
    foreach ($useCart as $cart_item_key => $cart_item) {
        $productId = $cart_item['product_id'];
        $productIds[$productId] = $productId;
    }
    if (!empty($productIds)) {
        $e4sEntries = getE4SEntriesFromProdIds($productIds);
    }
    foreach ($useCart as $cart_item_key => $cart_item) {
        $item = new stdClass();
        $item->productId = $cart_item['product_id'];
        $product = wc_get_product($item->productId);
        $metaData = e4s_getDataAsType($product->get_description(), E4S_OPTIONS_OBJECT);
        $item->athleteId = 0;
        if (isset($metaData->athleteid)) {
            $item->athleteId = $metaData->athleteid;
        }
        if (array_key_exists($item->productId, $e4sEntries)) {
            $entryObj = $e4sEntries[$item->productId];
            if ($entryObj->paid === E4S_ENTRY_PAID or $entryObj->userId !== $userId) {
                // paid or for another user (e4s switch user) so remove from cart
                try {
                    e4sProduct::removeFromWCCart($item->productId);
                } catch (Exception $err) {

                }
                continue;
            }
        }
        $item->title = $product->get_title();
        $item->variationId = $cart_item['variation_id'];
        $wcvariation = $cart_item['variation'];
        $variation = array();
        foreach ($wcvariation as $key => $value) {
            if (strpos($key, E4S_ATTRIB_PREFIX) !== FALSE) {
                $key = str_replace(E4S_ATTRIB_PREFIX, '', $key);
            }
            $variation[$key] = $value;
        }
        $item->variation = $variation;
        $item->qty = $cart_item['quantity'];
        $item->lineValue = $cart_item['line_total'];
        $obj->items[] = $item;
    }
    return $obj;
}

function getE4SEntriesFromProdIds($productIds): array {
    $e4sEntries = array();
    $sql = 'select variationid productId, paid, userId
                    from ' . E4S_TABLE_ENTRIES . '
                    where variationId in (' . implode(',', $productIds) . ')';
    $result = e4s_queryNoLog($sql);
    while ($obj = $result->fetch_object()) {
        $obj->productId = (int)$obj->productId;
        $obj->paid = (int)$obj->paid;
        $obj->userId = (int)$obj->userId;
        $e4sEntries[$obj->productId] = $obj;
    }

    $sql = 'select productId, paid, userId
                    from ' . E4S_TABLE_EVENTTEAMENTRIES . '
                    where productId in (' . implode(',', $productIds) . ')';
    $result = e4s_queryNoLog($sql);
    while ($obj = $result->fetch_object()) {
        $obj->productId = (int)$obj->productId;
        $obj->paid = (int)$obj->paid;
        $obj->userId = (int)$obj->userId;
        $e4sEntries[$obj->productId] = $obj;
    }
    return $e4sEntries;
}

function getActiveUserCart($exit = TRUE) {
    $userid = e4s_getUserID();
    $data = getCartForUserID($userid);
    $ticketData = e4sSeatingClass::getAllocationForUser();
    if (is_null($data)) {
        $data = array();
    }
    //    get WC Cart info
    $wcObj = new stdClass();
    $wcObj->wcCart = getWCCart();

    if (sizeof($data) + sizeof($ticketData) !== sizeof($wcObj->wcCart->items)) {
        $cartItems = $wcObj->wcCart->items;

        foreach ($data as $entry) {
            $addToCart = TRUE;
            $entryProductId = (int)$entry['prodId'];
            foreach ($cartItems as $cartItem) {
                if ((int)$cartItem->productId === $entryProductId) {
                    $addToCart = FALSE;
                }
            }
            if ($addToCart) {
                WC()->cart->add_to_cart($entryProductId, 1);
            }
        }

        $wcObj->wcCart = getWCCart();
    }
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($data, JSON_NUMERIC_CHECK) . ',
        "meta":' . json_encode($wcObj, JSON_NUMERIC_CHECK));
    }
}

function getActiveUserMiniCart() {
    ob_start();
    woocommerce_mini_cart();
    $mini_cart = ob_get_clean();

    if (strpos($mini_cart, 'No products in the basket') > 1) {
        echo $mini_cart;
    }else {
	    $items = preg_split( '~woocommerce-mini-cart-item mini_cart_item~', $mini_cart );
	    // add in clear cart
	    $mini_cart = preg_split( '~' . e4s_getCartButtonDiv() . '~', $mini_cart );
	    e4s_showCartButtons();
	    echo $mini_cart[0];
	    if ( sizeof( $items ) > 3 ) {
		    e4s_showCartButtons();
	    }
    }
    exit();
}

// not required. done in UI
function e4s_miniCartHeader() {
    echo '<div>';
    echo '<p class="e4s_cart_message">Entries are not confirmed until paid for.</p>';
    echo '</div>';
}

function e4s_getCartButtonDiv() {
    return '<p class="woocommerce-mini-cart__buttons buttons">';
}

function e4s_showCartButtons() {
    $emptyCart = '<button class="btn waves-effect waves green public-comp-list--enter-button" style="padding: 1px !important;padding-left: 0 !important;"><i style="margin-left: 10px;" class="material-icons normal">delete_forever</i>
          <a href="/basket/?empty-cart=clearcart" class="button" style="margin-right: 1em;color:white !important">Empty basket</a>
        </button>';
//    $emptyCart = '<a href="/basket/?empty-cart=clearcart" class="button" style="margin-right: 2em;">Empty basket</a>';
    $viewBasket = '<button class="btn waves-effect waves green public-comp-list--enter-button" style="float:right; padding: 1px !important;padding-left: 0 !important;"><i style="margin-left: 10px;" class="material-icons normal">shopping_cart</i>
          <a href="/basket" class="button" style="margin-right: 1em;color:white !important">View basket</a>
        </button>';
    $checkout = '<button class="btn waves-effect waves green public-comp-list--enter-button" style="margin-right:5px; float:right; padding: 1px !important;padding-left: 0px !important;"><i style="margin-left: 10px;" class="material-icons normal">payment</i>
          <a href="/checkout" class="button" style="margin-right: 1em;color:white !important">Checkout</a>
        </button>';

    echo e4s_getCartButtonDiv();
    echo $emptyCart;
    echo $viewBasket;
    echo $checkout;
    echo '</p>';
}

function getCartForUserID($userid) {
    return getEntriesForUserID($userid, E4S_ENTRY_NOT_PAID);
}

function getOrdersForUserID($userid) {
    return getEntriesForUserID($userid, E4S_ENTRY_PAID);
}

function getEntriesForUserID($userid, $paid) {
    if ($userid === E4S_USER_ANON_ID) {
        return array();
    }
//    $x = e4s_getUserID() === 19828 or e4s_getUserID() === 1;
//    if ( $x and e4s_isDevDomain()) {
//        e4s_addDebugForce('No CART for Nick or Admin in DEV !!!!!!!!!');
//        return array();
//    }
	$GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] = [];
    $sql                                    = "select e.* 
        , date_format(e.periodStart,'" . ISO_MYSQL_DATE . "') timeSelected
        , a.id athleteid
        , a.firstname firstName
        , a.surname surName
        , a.dob
        , ce.id ceid
        , ce.compid
        , comp.Name compName
        , ce.options ceoptions
        , ce.isopen IsOpen
        , ce.maxathletes
        , ce.maxgroup
        , c.Clubname club
        , ag.Name ageGroup
        , ev.Name
        , ev.options eoptions
        , ev.tf
        , ce.split split
        , date_format(eg.startdate,'" . ISO_MYSQL_DATE . "') startdate
        , ag2.Name vetAgeGroup
        , uom.uomtype uomType
        , uom.uomoptions
        , p.id priceID
        , p.saleprice
        , p.price
        , e.price eventprice
        , e.orderid
        , post.post_date
        , prod.post_status 
        , prod.post_title
        , IFNULL(date_format(p.saleenddate,'" . ISO_MYSQL_DATE . "'),'') saledate
        , p.name
        from " . E4S_TABLE_ATHLETE . ' a,
             ' . E4S_TABLE_CLUBS . ' c,
             ' . E4S_TABLE_AGEGROUPS . ' ag,
             ' . E4S_TABLE_COMPEVENTS . ' ce,
             ' . E4S_TABLE_EVENTGROUPS . ' eg,
             ' . E4S_TABLE_COMPETITON . ' comp,
             ' . E4S_TABLE_EVENTS . ' ev,
             ' . E4S_TABLE_UOM . ' uom,
             ' . E4S_TABLE_EVENTPRICE . ' as p,
             ' . E4S_TABLE_POSTS . ' prod,
             ' . E4S_TABLE_ENTRIES . ' e
        left join ' . E4S_TABLE_AGEGROUPS . ' ag2 on ag2.id = e.vetAgeGroupID
        left join ' . E4S_TABLE_POSTS . ' post on e.orderid = post.id
        where paid = ' . $paid . '
        and userid = ' . $userid . '
        and e.athleteid = a.id
        and e.clubid = c.id
        and ce.agegroupid = ag.id
        and ce.maxgroup = eg.id
        and e.compeventid = ce.id
        and comp.id = ce.CompID
        and ev.id = ce.eventid
        and ce.PriceID = p.id
        and e.variationid = prod.id
        and uom.id = ev.uomid';

    if ($paid === E4S_ENTRY_PAID) {
        $sql .= ' order by e.periodStart desc limit 0,200';
    }

    $result = e4s_queryNoLog('GetOrders' . E4S_SQL_DELIM . $sql);

    $data = array();
    if ($result->num_rows !== 0) {
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $compIds = [];
        foreach($rows as $row){
            $compIds[(int)$row['compid']] = (int)$row['compid'];
        }

	    $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_IDS] = $compIds;
        $maxGroups                                                        = array();
	    foreach($rows as $row){
            if ( $row['post_status'] === WC_POST_DELETED ){
                $row['variationID'] = e4s_correctProductId($row);
            }
            if ( $row['variationID'] !== 0 ) {
                $row['startdate'] .= e4s_getOffset($row['startdate']);
                $row['saledate'] .= e4s_getOffset($row['saledate']);
                $row['timeSelected'] .= e4s_getOffset($row['timeSelected']);
                $row['firstName'] = formatAthleteFirstname($row['firstName']);
                $row['surName'] = formatAthleteSurname($row['surName']);
                $row['prodId'] = $row['variationID'];
                $row['ceid'] = $row['compEventID'];
                $row['ageGroupId'] = $row['ageGroupID'];
                unset($row['ageGroupID']);
                unset($row['compEventID']);
                unset($row['variationID']);

                if ($row['vetAgeGroup'] === null) {
                    $row['vetAgeGroup'] = '';
                }
                unset($row['eventAgeGroup']);
                unset($row['clubname']);
                unset($row['athlete']);
                // this can return object or array !!!!!!!!
                $athleteAG = getAgeGroupForCeId($row['ageGroupId'], $row['compid'], $row['dob']);
                $useAthleteAG = array();
                if (gettype($athleteAG['ageGroup']) === 'array') {
                    $useAthleteAG['ageGroupId'] = $athleteAG['ageGroup']['id'];
                } else {
                    $useAthleteAG['ageGroupId'] = $athleteAG['ageGroup']->id;
                }

                $row['ageInfo'] = $useAthleteAG;
                $row['eventid'] = $row['id'];
                unset($row['id']);

                getEventNameWithSplit($row, 'Name');

                $row['entered'] = TRUE;
                $row['entrycnt'] = 0;
                $row['eoptions'] = json_decode($row['eoptions']);
                $row['ceoptions'] = json_decode($row['ceoptions']);

                if (isset($row['ceoptions']->xiText)) {
                    $row['Name'] .= $row['ceoptions']->xiText;
                }
                // price object
                $priceObj = array();
                $priceObj['id'] = $row['priceID'];
                $priceObj['priceName'] = $row['name'];
                $priceObj['stdPrice'] = $row['price'];
                $priceObj['curPrice'] = $row['price'];
                $priceObj['salePrice'] = $row['saleprice'];
                $priceObj['saleDate'] = $row['saledate'];
                $priceObj['actualPrice'] = $row['eventprice'];
                if ($priceObj['saleDate'] !== '') {
                    $saleDate = strtotime($priceObj['saleDate']);
                    $now = time();
                    if ($saleDate > $now) {
                        $priceObj['curPrice'] = $priceObj['salePrice'];
                    }
                }

                $row['price'] = $priceObj;
                unset($row['priceID']);
                unset($row['name']);
                unset($row['saleprice']);
                unset($row['saledate']);
                unset($row['eventprice']);
                if (!in_array($row['maxgroup'], $maxGroups)) {
                    $maxGroups[] = $row['maxgroup'];
                }

                $row['description'] = ''; // What is this ?
                $data[] = $row;
            }
        }
    }

// Event Team Objects
    $sql = "Select 
                e.Name
            ,   ce.IsOpen
            ,   ce.id ceid
            ,   ce.AgeGroupID ageGroupId
            ,   ag.Name ageGroup
            ,   ce.options ceoptions
            ,   ce.compid 
            ,   ce.maxgroup
            ,   ce.maxathletes
            ,   ce.split
            ,   c.Name compName
            ,   e.id eventid
            ,   e.options eoptions
            ,   e.tf 
            ,   evt.name surName
            ,   evt.paid
            ,   evt.orderid
            ,   evt.productid prodId
            ,   evt.id teamid
            ,   date_format(eg.startdate,'" . ISO_MYSQL_DATE . "') startdate
            ,   date_format(evt.created,'" . ISO_MYSQL_DATE . "') timeSelected
            ,   IFNULL(date_format(p.saleenddate,'" . ISO_MYSQL_DATE . "'),'') saledate
            ,   p.id priceID
            ,   p.saleprice
            ,   p.price
            ,   p.name priceName
            ,   evt.price eventprice
        from 
       " . E4S_TABLE_EVENTTEAMENTRIES . ' evt ,
       ' . E4S_TABLE_COMPEVENTS . ' ce,
       ' . E4S_TABLE_EVENTGROUPS . ' eg,
       ' . E4S_TABLE_COMPETITON . ' c,
       ' . E4S_TABLE_AGEGROUPS . ' ag,
       ' . E4S_TABLE_EVENTPRICE . ' p,
       ' . E4S_TABLE_EVENTS . ' e  
        where evt.userid = ' . $userid . '
        and   ce.id = evt.ceid
        and   evt.paid = ' . $paid . '
        and   ce.compid = c.id
        and   ce.eventid = e.id
        and   ce.PriceID = p.id
        and   ce.agegroupid = ag.id
        and   ce.maxgroup = eg.id
        and   evt.productId > 0
        ';
    if ($paid === E4S_ENTRY_PAID) {
        $sql .= ' order by evt.created desc 
                  limit 0,50';
    }

    $result = e4s_queryNoLog($sql);

    if ($result->num_rows > 0) {
        while ($row = mysqli_fetch_array($result, TRUE)) {
            $row['startdate'] .= e4s_getOffset($row['startdate']);
            $row['saledate'] .= e4s_getOffset($row['saledate']);
            $row['timeSelected'] .= e4s_getOffset($row['timeSelected']);
            $row['firstName'] = 'Team :';
            $row['athleteid'] = '0';
            $row['clubid'] = '0';
            $row['club'] = '';
            $row['description'] = '';
            $row['dob'] = '';
            $row['entered'] = TRUE;
            $row['entrycnt'] = 0;
            $row['pb'] = 0;
            $row['vetAgeGroup'] = '';
            $row['vetAgeGroupID'] = 0;

            $ceoptions = e4s_getOptionsAsObj($row['ceoptions']);

            $useAthleteAG = array();
            $useAthleteAG['ageGroupId'] = $row['ageGroupId'];
            $row['ageInfo'] = $useAthleteAG;

            // price object
            $priceObj = array();
            $priceObj['id'] = $row['priceID'];
            $priceObj['priceName'] = $row['priceName'];
            $priceObj['stdPrice'] = $row['price'];
            $priceObj['curPrice'] = $row['price'];
            $priceObj['salePrice'] = $row['saleprice'];
            $priceObj['saleDate'] = $row['saledate'];
            $priceObj['actualPrice'] = $row['eventprice'];
            if ($priceObj['saleDate'] != '') {
                $saleDate = strtotime($priceObj['saleDate']);
                $now = time();
                if ($saleDate > $now) {
                    $priceObj['curPrice'] = $priceObj['salePrice'];
                }
            }

            $row['price'] = $priceObj;
            unset($row['priceID']);
            unset($row['priceName']);
            unset($row['saleprice']);
            unset($row['saledate']);

            if (e4s_getTeamType($ceoptions) === E4S_TEAM_TYPE_SCHOOL) {
                // If a school, dont show athletes on cart ( could be a lot ! )
                $row['athletes'] = array();
            } else {
                $sql = "select concat(firstname, ' ', surname ) athlete, urn, eta.pos
                from 
                " . E4S_TABLE_ATHLETE . ' a,
                ' . E4S_TABLE_EVENTTEAMATHLETE . ' eta
                where eta.teamentryid = ' . $row['teamid'] . '
                and   a.id = eta.athleteid
                order by eta.pos';
                $athleteResults = e4s_queryNoLog($sql);
                $athleteRows = $athleteResults->fetch_all(MYSQLI_ASSOC);

                $row['athletes'] = $athleteRows;
            }

            $data[] = $row;
        }
    }

    return $data;
}

// Called when building cart as an Entry has a "trash" product
// Try to find valid product
function e4s_correctProductId($row){
    $origId = $row['variationID'];
    $retVal = $origId;
    // see if there is ONE valid product based on title
    $sql = '
        select id
        from ' . E4S_TABLE_POSTS . "
        where post_title = '" . addslashes($row['post_title']) . "'
        and post_status = '" . WC_POST_PUBLISH  . "'
    ";
    $result = e4s_queryNoLog($sql);

    if ( $result->num_rows === 1 ){
        $obj = $result->fetch_object();
        $retVal = $obj->id;
        $sql = 'update ' . E4S_TABLE_ENTRIES . '
                set variationid = ' . $retVal . '
                where variationid = ' . $origId;
        e4s_queryNoLog($sql);
    }else{
//        Entry4UIError(9335,$result->num_rows . ":Cart Error. Support have been notified.(" . $origId . "/" . $retVal . ")");
        $retVal = 0;
    }
    return $retVal;
}
// Called when admin add an entry to another users backet
function e4s_addToBasket($obj) {
    $productId = checkJSONObjForXSS($obj, 'productid:Product Id');
    $userId = checkJSONObjForXSS($obj, 'userid:User Id');
    $force = checkJSONObjForXSS($obj, 'force:force');
    if (is_null($force)) {
        $force = TRUE;
    }
    $isTeamEvent = checkJSONObjForXSS($obj, 'isteamevent:Team Event');
    if (is_null($isTeamEvent)) {
        $isTeamEvent = FALSE;
    }
    $paid = checkJSONObjForXSS($obj, 'paid:Paid');
    if (is_null($paid)) {
        $paid = E4S_ENTRY_NOT_PAID;
    }
    $reason = checkJSONObjForXSS($obj, 'reason:Reason');
    if (is_null($reason)) {
        $reason = '';
    }
    $entryObj = new entryClass();
    $entryObj->putEntryInUsersAccount($productId, $userId, $paid, $isTeamEvent, $reason, $force);
    Entry4UISuccess();
}

// duplicate of above
function e4s_updatePaid($obj) {
    $productIdArray = checkJSONObjForXSS($obj, 'prodIds:Event Product Ids');
    $paid = checkFieldForXSS($obj, 'paid:Paid');
    $reason = checkFieldForXSS($obj, 'message:Reason for Toggle');

    $entryObj = new entryClass();
    foreach ($productIdArray as $productId) {
        $entryObj->putEntryInUsersAccount($productId, 0, $paid, FALSE, $reason, TRUE);
    }
    Entry4UISuccess('');
}

function e4s_addCartMessage($msg, $type) {
    $filler = '';
    if ($type === E4S_CART_ERROR_TYPE) {
        $filler = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    }
    wc_add_notice($filler . $msg, $type);
}

function e4s_CartSummary() {
    // generated when cart was validated
    if (!array_key_exists('E4S_CART_ENTRIES', $GLOBALS)) {
        $entries = e4s_getCartEntries();
    } else {
        $entries = $GLOBALS['E4S_CART_ENTRIES'];
    }

    $config = e4s_getConfig();
    if (isset($config['options']->cartMessage)) {
        echo '<div id="e4sCartMessage">' . $config['options']->cartMessage . '</div>';
    }
    if (sizeof($entries) < 5) {
        return;
    }
    $counts = array();

    foreach ($entries as $entry) {
        if (!array_key_exists($entry->compId, $counts)) {
            $countObj = new stdClass();
            $countObj->indivCount = 0;
            $countObj->teamCount = 0;
            $counts[$entry->compId] = $countObj;
        }
        $countObj = $counts[$entry->compId];
        if ($entry->isTeamEvent) {
            $countObj->teamCount += 1;
        } else {
            $countObj->indivCount += 1;
        }
    }
    ?>
    <table cellspacing="0" class="shop_table shop_table_responsive">
        <tbody>
        <tr class="cart-subtotal">
            <th colspan="2" style="text-align: center;">Cart Summary</th>
            <td data-title="Cart Summary" colspan="2"></td>
        </tr>
        <?php
        foreach ($counts as $compId => $countObj) {
            $compObj = e4s_GetCompObj($compId);
            ?>
            <tr class="cart-subtotal">
                <th colspan="2"
                    style="font-style: italic;"><?php echo $compObj->getID() . ' : ' . $compObj->getName() ?></th>
                <td data-title="CompName" colspan="2"></td>
            </tr>
            <?php
            if ($countObj->indivCount > 0) {
                ?>
                <tr>
                    <th style="font-size: small;">Individual entries</th>
                    <td data-title="Individual entries">
                        <span class="woocommerce-Price-amount amount"><bdi><?php echo $countObj->indivCount ?></bdi></span>
                    </td>
                </tr>
                <?php
            }

            if ($countObj->teamCount > 0) {
                ?>
                <tr>
                    <th style="font-size: small;">Team entries</th>
                    <td data-title="Team entries">
                        <span class="woocommerce-Price-amount amount"><bdi><?php echo $countObj->teamCount ?></bdi></span>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>
    <?php
}

// if checkout is true, also create YITH summaries ( if required )
function e4s_validateCart($checkout) {
    $pricesAltered = FALSE;
    $ceSChecked = [];
    $entries = e4s_getCartEntries();
    $messages = array();
//    $yithObjs = array();
    foreach (WC()->cart->get_cart() as $cart_item_key => $values) {
        $product = $values['data'];
        $productId = $product->get_id();
//        $compId = 0;
        // if it does NOT exist, its a ticket
        if (array_key_exists($productId, $entries)) {
            $entryObj = $entries[$productId];
            $eOptions = e4s_getOptionsAsObj($entryObj->options);

            $e4sEntryObj = new entryClass();
            $force = $e4sEntryObj->getForceBasket($eOptions);
            $ignorePriceChange = false;
            if ( isset($eOptions->ignorePriceChange) ){
                $ignorePriceChange = $eOptions->ignorePriceChange;
            }
            $compId = $entryObj->compId;
            $compObj = e4s_GetCompObj($compId);
//            if ( !array_key_exists($compId, $yithObjs) ){
//                $yithObjs[$compId] = array();
//            }
            $messageKey = $compId . '_1';
            // Check if the competition has closed
            if (!$force) {
                if (!array_key_exists($messageKey, $messages)) {
                    if ($compObj->haveEntriesClosed()) {
                        $messages[$messageKey] = TRUE;
                        $msg = 'Entries for Competition ' . $compObj->getFormattedName() . ' have closed.';
                        if ( $compObj->isOrganiser()){
                            e4s_addCartMessage( $msg . ' As the organiser, this is a warning and you may continue.', E4S_CART_MESSAGE_TYPE);
                        }else{
                            e4s_addCartMessage( $msg . ' Please remove any entry for this competition to enable checkout.', E4S_CART_ERROR_TYPE);
                        }
                    }
                }

                // check limits
                $limits = $compObj->getCompLimits();
                if ($limits->entries > 0 or $limits->athletes > 0) {
                    $currentLimits = $compObj->getLiveCompLimits();
                    if ($limits->entries > 0) {
                        if ($currentLimits->entries >= $limits->entries) {
                            $messageKey = $entryObj->compId . '_2';
                            if (!array_key_exists($messageKey, $messages)) {
                                $messages[$messageKey] = TRUE;
                                $msg = 'Entries for Competition ' . $compObj->getFormattedName() . ' have reached their limit. ';
                                if ( $compObj->isOrganiser()){
                                    e4s_addCartMessage($msg . 'This is a warning and you may continue.', E4S_CART_MESSAGE_TYPE);
                                }else{
                                    e4s_addCartMessage($msg . 'Please remove any entry for this competition to enable checkout.', E4S_CART_ERROR_TYPE);
                                }
                            }
                        }
                    }
                    if ($limits->athletes > 0) {
                        if ($currentLimits->athletes >= $limits->athletes) {
                            $messageKey = $entryObj->compId . '_3';
                            if (!array_key_exists($messageKey, $messages)) {
                                $messages[$messageKey] = TRUE;
                                $msg = 'Athletes entered for Competition ' . $compObj->getFormattedName() . ' have reached their limit. ';
                                if ( $compObj->isOrganiser()){
                                    e4s_addCartMessage($msg . 'This is a warning and you may continue.', E4S_CART_MESSAGE_TYPE);
                                }else{
                                    e4s_addCartMessage($msg . 'Please remove all athlete entries for this competition to enable checkout.', E4S_CART_ERROR_TYPE);
                                }
                            }
                        }
                    }
                }
            }
            // Check if the event has closed
            if (!array_key_exists($entryObj->ceId, $ceSChecked)) {
                if ((int)$entryObj->isOpen === E4S_EVENT_CLOSED and !$compObj->isWaitingListEnabled() and !$force) {
                    $messageKey = $entryObj->compId . '_4' . $entryObj->ceId;
                    if (!array_key_exists($messageKey, $messages)) {
                        $messages[$messageKey] = TRUE;
                        $msg = $entryObj->eventName . ' entries on ' . $entryObj->startDate . ' at ' . $compObj->getFormattedName() . ' have closed. ';
                        if ( $compObj->isOrganiser()){
                            e4s_addCartMessage( $msg . ' As the organiser, this is a warning and you may continue.', E4S_CART_MESSAGE_TYPE);
                        }else{
                            e4s_addCartMessage( $msg . ' Please remove any entry for this competition to enable checkout.', E4S_CART_ERROR_TYPE);
                        }
                    }
                }
                $ceSChecked[$entryObj->ceId] = TRUE;
            }

            // check if the comp is Cash/BACCS only
            $bacsMsg = $compObj->bacsOnly();
            if ($bacsMsg !== '') {
                $messageKey = $entryObj->compId . '_5';
                $messages[$messageKey] = TRUE;
                e4s_addCartMessage($bacsMsg, E4S_CART_ERROR_TYPE);
            }
            $productPrice = (float)$product->get_price();
            $e4sPrice = $entryObj->price;
            $entryObj->activePrice = $e4sPrice;

            // Now check price to see if it has gone up since adding to basket
            // TODO NEEDS MORE THAN JUST DISCOUNTID = 0
            if (!$force and !is_null($entryObj->saleEndDate) and $entryObj->discountId === 0) {
                $useSalePrice = FALSE;
                $saleDate = strtotime($entryObj->saleEndDate);
//            e4s_addCartMessage("Product early bird price : " . $product->get_date_on_sale_to(), E4S_CART_MESSAGE_TYPE);
                $now = time();
                if ($saleDate > $now) {
//                    e4s_addCartMessage("Inside early bird price", E4S_CART_MESSAGE_TYPE);
                    $useSalePrice = TRUE;
                    $e4sPrice = $entryObj->salePrice;
                    $entryObj->activePrice = $entryObj->salePrice;
                }

                if ($productPrice !== $e4sPrice and !$ignorePriceChange) {
//                    var_dump($entryObj);
                    e4s_amendPriceFromCart($entryObj, $product, $useSalePrice);
                    $pricesAltered = TRUE;
                    e4s_addCartMessage(trim($entryObj->entryName) . ' / ' . trim($entryObj->eventName) . ' : Price amended since adding to basket.', E4S_CART_MESSAGE_TYPE);
                    $productPrice = $e4sPrice;
                }
            }

        } else {
            $descObj = e4s_getWCProductDescObj($product);
            if (isset($descObj->ticket)) {
                // Handle a ticket
            }
        }
    }

    if ($pricesAltered) {
        WC()->cart->calculate_totals();
    }
    $hasMessages = !empty($messages);
//    if ( $checkout ) {
//        if (!$hasMessages and e4s_useStripeConnect() ) {
//            // create YITH Records
//            foreach($yithObjs as $compId=>$yithOrderItemObj){
//                $compObj = e4s_GetCompObj($compId);
//                $compObj->createYITHConsolidatedReceivers($yithOrderItemObj);
//            }
//        }
//    }
    return $hasMessages;
}

function e4s_amendPriceFromCart($entryObj, $product, $useSalePrice) {
//    $product->set_regular_price(20.00);
//    $product->set_sale_price(6.00);
//    $product = wc_get_product(1);

    $e4sPrice = $entryObj->price;
    if ($useSalePrice) {
        $e4sPrice = $entryObj->salePrice;
    }
    $product->set_price($e4sPrice);
    $product->set_regular_price($e4sPrice);
    $product->set_sale_price($e4sPrice);
    $product->set_date_on_sale_to(null);
    $product->save();

    // alter entry price
    if ($e4sPrice !== $entryObj->entryPrice) {
        e4s_amendEntryPrice($entryObj, $e4sPrice);
    }
}

function e4s_amendEntryPrice($entryObj, $e4sPrice) {
    $sql = 'update ';
    if (!$entryObj->isTeamEvent) {
        $sql .= E4S_TABLE_ENTRIES;
    } else {
        $sql .= E4S_TABLE_EVENTTEAMENTRIES;
    }

    $sql .= '
        set price = ' . $e4sPrice . '
        where id = ' . $entryObj->id;
    e4s_queryNoLog($sql);
}

function e4s_getCartEntries(): array {
    $entries = [];
    $userId = e4s_getUserID();
    if ($userId > E4S_USER_NOT_LOGGED_IN) {
        $sql = "
        select ete.id,
               ete.ceId,
               ete.price entryPrice,
               ete.productId,
               ete.name entryName,
               ete.options,
               0 discountId,
               ce.compId,
               ce.priceId,
               ce.isOpen,
               p.price,
               p.salePrice,
               p.fee,
               p.saleFee,
               p.saleEndDate,
               eg.name eventName,
               date_format(eg.startDate,'" . E4S_MYSQL_PRETTY_DATE . "') startDate,
               true isTeamEvent
        from " . E4S_TABLE_EVENTTEAMENTRIES . ' ete,
             ' . E4S_TABLE_COMPEVENTS . ' ce,
             ' . E4S_TABLE_EVENTGROUPS . ' eg,
             ' . E4S_TABLE_EVENTPRICE . ' p
        where ete.userid = ' . $userId . '
        and   ete.ceid = ce.ID
        and   eg.id = ce.maxGroup
        and   ce.PriceID = p.id
        and   ete.productId > 0
        and   paid = ' . E4S_ENTRY_NOT_PAID . "
        union
        select e.entryid id,
               e.ceId,
               e.price entryPrice,
               e.productId,
               concat(e.firstname,' ',e.surname) entryName,
               e.eoptions options,
               e.discountId discountId,
               e.compId,
               e.priceId,
               e.isOpen,
               p.price,
               p.salePrice,
               p.fee,
               p.saleFee,
               p.saleEndDate,
               e.eventname eventName,
               date_format(e.startDate,'" . E4S_MYSQL_PRETTY_DATE . "') startDate,
               false isTeamEvent
        from " . E4S_TABLE_ENTRYINFO . ' e,
             ' . E4S_TABLE_EVENTPRICE . ' p
        where e.userid = ' . $userId . '
        and   e.PriceID = p.id
        and   paid = ' . E4S_ENTRY_NOT_PAID . '
    ';
        $result = e4s_queryNoLog($sql);
        while ($obj = $result->fetch_object()) {
            $obj->id = (int)$obj->id;
            $obj->discountId = (int)$obj->discountId;
            $obj->entryPrice = (float)$obj->entryPrice;
            $obj->price = (float)$obj->price;
            $obj->salePrice = (float)$obj->salePrice;
            $obj->fee = (float)$obj->fee;
            $obj->saleFee = (float)$obj->saleFee;
            $obj->ceId = (int)$obj->ceId;
            $obj->isOpen = (int)$obj->isOpen;
            $obj->compId = (int)$obj->compId;
            $obj->productId = (int)$obj->productId;
            $obj->priceId = (int)$obj->priceId;
            if ($obj->isTeamEvent === '1') {
                $obj->isTeamEvent = TRUE;
            } else {
                $obj->isTeamEvent = FALSE;
            }
            $entries[$obj->productId] = $obj;
        }
    }
    $GLOBALS['E4S_CART_ENTRIES'] = $entries;
    return $entries;
}