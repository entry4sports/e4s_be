<?php
$e4sIgnoreErrorHandler = TRUE;
include_once E4S_FULL_PATH . 'dbInfo.php';
// return "" to continue with built in tests or return true/false
// true means Show the cheque payment option
// There MUST be a product condition rule set up for anything, just has to be one
// plugin wp-content/plugins/conditional-payments-for-woocommerce/includes/class-conditional-payments-filters.php
// this is called from plugin here
/*
public static function filter_products( $condition )
    $products = self::_get_order_products();
    if ( ! empty( $products ) ) {
        include_once "entry/v5/cart/e4sPaymentOptions.php";
        return e4s_check_payment_options($products);
    }
*/
function e4s_check_payment_options($products, $condition = '') {

    if (empty($products)) {
//        e4s_dump($products," { 1 }", true);
//        e4s_log("1", 1);
        return FALSE;
    }

    $ids = array();
    foreach ($products as $product) {
        $ids[] = $product->get_id();
    }
    if (implode(',', $ids) === '') {
//        e4s_dump($products," { 2 }", true);
//        e4s_log("2", 1);
        return FALSE;
    }

    $title = 'CHECK PAYMENT' . E4S_SQL_DELIM;
// get comps from the post records
    $sql = 'select post_content
        from ' . E4S_TABLE_POSTS . '
        where id in (' . implode(',', $ids) . ')';

    $result = e4s_queryNoLog($title . $sql);
    if ($result->num_rows < 1) {
//        e4s_dump($products," { 3 }", true);
//        e4s_log("3", 1);
        return FALSE;
    }

    $postRecords = $result->fetch_all(MYSQLI_ASSOC);
// now get unique list of comps
    $comps = array();
    foreach ($postRecords as $postRecord) {
        $jsonObj = e4s_getOptionsAsObj($postRecord['post_content']);
        if (isset($jsonObj->compid)) {
            $comps [$jsonObj->compid] = $jsonObj->compid;
        }
    }

    $sql = 'select *
        from ' . E4S_TABLE_COMPETITON . '
        where id in (' . implode(',', $comps) . ')';
    $result = e4s_queryNoLog($title . $sql);
    if ($result->num_rows < 1) {
//        e4s_dump($products," { 4 }", true);
//        e4s_log("4", 1);
        return FALSE;
    }
    $compRecords = $result->fetch_all(MYSQLI_ASSOC);

    foreach ($compRecords as $compRecord) {
        $options = $compRecord['options'];
        $coptions = e4s_addDefaultCompOptions($options);

        if (isset($coptions->cheques) === FALSE) {
//            e4s_dump($products," { 5 }", true);
//            e4s_log("5", 1);
            return FALSE;
        }
        if (isset($coptions->cheques->allow) and $coptions->cheques->allow !== TRUE) {
//            e4s_dump($products," { 6 }", true);
//            e4s_log("6", 1);
            return FALSE;
        }
        if (isset($coptions->cheques->ends) === FALSE) {
//            e4s_dump($products," { 7 }", true);
//            e4s_log("7", 1);
            return FALSE;
        }
        $endDateStr = $coptions->cheques->ends;
        $endDate = strtotime($endDateStr);

        $now = time();
        if ($endDate < $now) {
//            e4s_dump($products," { 8 }", true);
//            e4s_log("8", 1);
            return FALSE;
        }
    }
//    e4s_dump($products," { TRUE }", true);
//    logTxt($title . " return true");
    return TRUE;
}