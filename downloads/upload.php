<?php
require_once ABSPATH . 'wp-admin/includes/image.php';
require_once ABSPATH . 'wp-admin/includes/file.php';
require_once ABSPATH . 'wp-admin/includes/media.php';
require_once E4S_FULL_PATH . 'dbInfo.php';

// Let WordPress handle the upload.
// Remember, 'my_image_upload' is the name of our file input in our form above.
$attachment_id = media_handle_upload('file', 0);

if (is_wp_error($attachment_id)) {
    // There was an error uploading the image.
    Entry4UIError(9049, 'Failed to upload file. Check the file extension.', 200, '');
} else {
    $options = new stdClass();
    if ($private === null) {
        $private = FALSE;
    } else {
        $private = TRUE;
    }
    $options->private = $private;

    $options = json_encode($options);
    // The image was uploaded successfully!
    $insertSql = 'insert into ' . E4S_TABLE_ATTACHMENTS . ' (wpattachid, options)
                  values (' . $attachment_id . ",'" . $options . "')";
    e4s_queryNoLog($insertSql);

    $attachSql = 'select UNIX_TIMESTAMP(code) code
              from ' . E4S_TABLE_ATTACHMENTS . '
             where id = ' . e4s_getLastID();

    $result = e4s_queryNoLog($attachSql);
    $row = $result->fetch_assoc();

    $path = '/wp-json/view/';
    Entry4UISuccess('"path":"' . $path . $attachment_id . '/' . $row['code'] . '"');
}