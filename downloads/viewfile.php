<?php
require_once ABSPATH . 'wp-admin/includes/image.php';
require_once ABSPATH . 'wp-admin/includes/file.php';
require_once ABSPATH . 'wp-admin/includes/media.php';
require_once E4S_FULL_PATH . 'dbInfo.php';

$attachment_id = (int)$attachment_id;
if ( $attachment_id === 221649 or $attachment_id === 223174 ){
    exit;
}
$attachSql = 'select UNIX_TIMESTAMP(code) code, options
              from ' . E4S_TABLE_ATTACHMENTS . '
              where wpattachid = ' . $attachment_id;
$result = e4s_queryNoLog($attachSql);
$row = $result->fetch_assoc();

$options = $row['options'];
if ($options !== '') {
    e4s_checkAttachmentPermissions($options);
}
$fileURL = wp_get_attachment_url($attachment_id);
switch (E4S_CURRENT_DOMAIN) {
    //        $data = file_get_contents( "/var/www/vhosts/entry4sports.co.uk/httpdocs/wp-content/uploads/.......pdf" );
    case E4S_UK_DOMAIN:
//        echo $fileURL;
//        exit();
        $fileURL = explode('/wp-content/', $fileURL);
        $fileURL = '/var/www/vhosts/entry4sports.co.uk/httpdocs/wp-content/' . $fileURL[1];
        break;
}

if ($row['code'] === $code) {
    $contentType = get_post_mime_type($attachment_id);
    header('Content-Type: ' . $contentType);
// PDF Files
    if ($contentType === 'application/pdf') {
        $data = file_get_contents($fileURL);
        echo $data;
    }

// PNG files
    if ($contentType === 'image/' . E4S_PICTURE_PNG) {
        try {
            $image = imagecreatefrompng($fileURL);
//            imageinterlace($image, true);
            if ($image !== FALSE) {
                imagepng($image);
            }
        } catch (Exception $e) {
            Entry4UIError(8788, 'Image not found');
        }
    }

// JPG Files
    if ($contentType === 'image/jpeg') {
        try {
            $image = imagecreatefromjpeg($fileURL);
            if ($image !== FALSE) {
                imagejpeg($image);
            }

        } catch (Exception $e) {
            Entry4UIError(8789, 'Image not found');
        }
    }

    exit();
}
Entry4UIError(8011, 'Invalid Attachment', 404, '');

function e4s_checkAttachmentPermissions($options) {
    $authorised = TRUE;
    $options = json_decode($options);

    if (isset($options->private)) {
        if ($options->private === TRUE and e4s_getUserID() === 0) {
            // Private attachment and use not logged on
            $authorised = FALSE;
        }
    }
    if (isset($options->userids) and $authorised) {
        $authorised = es4_checkattachmentusers($options->userids);
    }
    if (isset($options->orgids) and $authorised) {
        $authorised = es4_checkattachmentorgs($options->userids);
    }
    if ($authorised === FALSE) {
        Entry4UIError(9050, 'Unauthorised access', 404, '');
    }
}

function es4_checkattachmentusers($ids) {
    $curUserId = e4s_getUserID();
    foreach ($ids as $id) {
        if ($curUserId === (int)$id) {
            return TRUE;
        }
    }
    return FALSE;
}


function es4_checkattachmentorgs($ids) {
    $curUserId = e4s_getUserID();
    foreach ($ids as $id) {
        return TRUE;
    }
    return FALSE;
}