<?php
define('E4S_IDS_NOT_ALLOWED', -1);
define('E4S_OPEN_TO_ALL_IDS', 0);
define('E4S_NO_SECURITY', '');
define('E4S_PASSED_SECURITY', 'OK');
define('E4S_AVAILABLE_SECURITY', '+');
define('E4S_NOT_AVAILABLE_SECURITY', '-');
// return true if the document gets passed security
function e4s_checkTeamEventSecurity($ceoptions, $entitySelected) {
    return e4s_checkEventSecurity($ceoptions, null, $entitySelected);
}

function e4s_checkUserEventSecurity($options, $athleteClubId) {
    return e4s_checkEventSecurity($options, $athleteClubId, null);
}

function e4s_checkEventSecurity($options, $athleteClubId, $entitySelected) {
    $userObj = e4s_getUserObj(FALSE, FALSE);

//    if( !isset($options->security) or isE4SUser() ){
    if (!isset($options->security)) {
        return E4S_NO_SECURITY;
    }
//    if no club/county/region security
    if (!isset($options->security->clubs) and !isset($options->security->counties) and !isset($options->security->regions)) {
        return E4S_NO_SECURITY;
    }

    if (!e4s_userHasEntity($userObj, $entitySelected)) {
        return 'User does not have the correct authority.';
    }

    $useUserObj = $userObj;
    if (!is_null($entitySelected)) {
        // only set if from a team
        $useUserObj = e4s_setOnlyUserEntity($userObj, $entitySelected);
    }

    // there is some security set, so need to check
    $allowRegions = _checkRegions($options, $useUserObj, $entitySelected);
    $allowCounties = _checkCounties($options, $useUserObj, $entitySelected);
    $allowClubs = _checkClubs($options, $useUserObj, $athleteClubId, $entitySelected);

    $reason = E4S_NO_SECURITY;
    if ($allowClubs === E4S_PASSED_SECURITY or $allowCounties === E4S_PASSED_SECURITY or $allowRegions === E4S_PASSED_SECURITY) {
        return $reason;
    }

    $reasonSep = '';
    $availableSep = '';
    $notAvailableSep = '';
    $notAvailable = '';
    $available = '';

    if ($allowClubs !== E4S_NO_SECURITY) {
        if (strpos($allowClubs, E4S_NOT_AVAILABLE_SECURITY) !== FALSE) {
            $notAvailable .= $notAvailableSep . $allowClubs;
            $notAvailableSep = ', ';
        } elseif (strpos($allowClubs, E4S_AVAILABLE_SECURITY) !== FALSE) {
            $available .= $availableSep . $allowClubs;
            $availableSep = ', ';
        } else {
            $reason .= $reasonSep . $allowClubs;
            $reasonSep = ', ';
        }
    }

    if ($allowCounties !== E4S_NO_SECURITY) {
        if (strpos($allowCounties, E4S_NOT_AVAILABLE_SECURITY) !== FALSE) {
            $notAvailable .= $notAvailableSep . $allowCounties;
            $notAvailableSep = ', ';
        } elseif (strpos($allowCounties, E4S_AVAILABLE_SECURITY) !== FALSE) {
            $available .= $availableSep . $allowCounties;
            $availableSep = ', ';
        } else {
            $reason .= $reasonSep . $allowCounties;
            $reasonSep = ', ';
        }
    }

    if ($allowRegions !== E4S_NO_SECURITY) {
        if (strpos($allowRegions, E4S_NOT_AVAILABLE_SECURITY) !== FALSE) {
            $notAvailable .= $notAvailableSep . $allowRegions;
            $notAvailableSep = ', ';
        } elseif (strpos($allowRegions, E4S_AVAILABLE_SECURITY) !== FALSE) {
            $available .= $availableSep . $allowRegions;
            $availableSep = ', ';
        } else {
            $reason .= $reasonSep . $allowRegions;
            $reasonSep = ', ';
        }
    }
    $reasonSep = '. ';
    if ($available !== '') {
        $available = 'Available to ' . $available . ' authorised users only' . $reasonSep;
    }
    if ($notAvailable !== '') {
        $notAvailable = 'Not available to ' . $notAvailable . ' ' . $reasonSep;
    }
    if ($reason === '') {
        $reasonSep = '';
    }
    $reason .= $reasonSep . $available . $notAvailable;
    $reason = str_replace(E4S_NOT_AVAILABLE_SECURITY, '', $reason);
    $reason = str_replace(E4S_AVAILABLE_SECURITY, '', $reason);
    e4s_addDebug($reason, 'User security return');
    return $reason;
}

function _checkClubs($ceoptions, $userObj, $athleteClubId, $entitySelected) {
    $useEntityDesc = 'club';

    if (!is_null($entitySelected)) {
        if (isset($entitySelected->school) and $entitySelected->school) {
            $useEntityDesc = 'school';
        }
    }
// If area defined ( Greater level ), show club events if open to All clubs
//    if (isset($userObj->areas) and !empty($userObj->areas) and $ceoptions->security->clubs[0] === E4S_OPEN_TO_ALL_IDS) {
// Dont think this is correct. Why should it say yes if higher auth ?
//        return E4S_PASSED_SECURITY;
//    }
// should check that the greater level has this club in it ???

//    Check User Security
    if (isset($ceoptions->security->clubs)) {

        if (sizeof($userObj->clubs) === 0 && $ceoptions->security->clubs[0] !== E4S_IDS_NOT_ALLOWED) {
//            There is user security and the current user has none
            $specific = '';
            if ($ceoptions->security->clubs[0] !== E4S_OPEN_TO_ALL_IDS) {
                $specific = 'nominated ';
            }
            return E4S_AVAILABLE_SECURITY . $specific . $useEntityDesc . 's';
        }
//        If we get here there is user security and the current user has some security to check
        foreach ($userObj->clubs as $club) {
            if ($ceoptions->security->clubs[0] === E4S_OPEN_TO_ALL_IDS) {
                // open to all clubs so return unless we are now checking an athletes club
                if (is_null($athleteClubId) or (int)$athleteClubId === 0) {
                    return E4S_PASSED_SECURITY;
                }
                break;
            }
            if ($ceoptions->security->clubs[0] === E4S_IDS_NOT_ALLOWED) {
                // Not open to clubs
                return E4S_NOT_AVAILABLE_SECURITY . $useEntityDesc . 's';
            }
            foreach ($ceoptions->security->clubs as $clubid) {
                if ($clubid === (int)$club->id) {
                    // Club allowed
                    if (is_null($entitySelected) or ($entitySelected->id === $clubid and $entitySelected->level === E4S_CLUB_ENTITY)) {
                        return E4S_PASSED_SECURITY;
                    }
                }
            }
        }
        return E4S_NOT_AVAILABLE_SECURITY . 'your ' . $useEntityDesc;
    } else {
        return E4S_NO_SECURITY;
    }

//        Now Check if event open only to certain clubs for athletes
    if ((int)$athleteClubId > 0 and isset($ceoptions->security->athleteclubs)) {
        foreach ($ceoptions->security->athleteclubs as $clubid) {
            if ($clubid === (int)$athleteClubId) {
                // Current athletes club has passed
                return E4S_PASSED_SECURITY;
            }
        }
        return E4S_NOT_AVAILABLE_SECURITY . 'your ' . $useEntityDesc;
    }

    return E4S_PASSED_SECURITY;
}

function e4s_setOnlyUserEntity($userObj, $entitySelected) {
    $newObj = array();
    $retUserObj = new stdClass();
    if ($entitySelected->level === 1) {
        // club check
        foreach ($userObj->clubs as $obj) {
            if ((int)$entitySelected->id === (int)$obj->id) {
                $newObj[] = $obj;
            }
        }
        $retUserObj->clubs = $newObj;
        $retUserObj->areas = array();
    } else {
        // area check
        foreach ($userObj->areas as $obj) {
            if ((int)$entitySelected->id === (int)$obj->areaid) {
                $newObj[] = $obj;
            }
        }
        $retUserObj->areas = $newObj;
        $retUserObj->clubs = array();
    }
    return $retUserObj;
}

function e4s_userHasEntity($userObj, $entitySelected) {
    if (is_null($entitySelected) or isE4SUser()) {
        return TRUE;
    }
    if ($entitySelected->level === 0) {
        return TRUE;
    }
    if ($entitySelected->level === 1) {
        // club check
        foreach ($userObj->clubs as $obj) {
            if ((int)$entitySelected->id === (int)$obj->id) {
                return TRUE;
            }
        }
    } else {
        // area check
        foreach ($userObj->areas as $obj) {
            if ((int)$entitySelected->id === (int)$obj->areaid) {
                return TRUE;
            }
        }
    }

    return FALSE;
}

function _checkCounties($ceoptions, $userObj, $entitySelected) {
    if (isset($ceoptions->security->counties)) {
        return _checkArea($userObj, $ceoptions->security->counties, $entitySelected, E4S_COUNTY_ENTITY);
    }
    // No County security
    return E4S_NO_SECURITY;
}

function _checkRegions($ceoptions, $userObj, $entitySelected) {

    if (isset($ceoptions->security->regions)) {
        return _checkArea($userObj, $ceoptions->security->regions, $entitySelected, E4S_REGION_ENTITY);
    }
    // No region security
    return E4S_NO_SECURITY;
}

function _checkArea($userObj, $prop, $entitySelected, $level) {
    $entityLevelUser = 'User Account';
    if (!is_null($entitySelected)) {
        switch ($entitySelected->level) {
            case E4S_CLUB_ENTITY:
                $entityLevelUser = 'Club';
                break;

            case E4S_COUNTY_ENTITY:
                $entityLevelUser = 'County';
                break;

            case E4S_REGION_ENTITY:
                $entityLevelUser = 'Region';
                break;

            case E4S_COUNTRY_ENTITY:
                $entityLevelUser = 'Country';
                break;
        }
    }
    $entityLevel = '';
    $entityLevelPlural = '';
    switch ($level) {
        case E4S_COUNTY_ENTITY:
            $entityLevel = 'County';
            $entityLevelPlural = 'Counties';
            break;

        case E4S_REGION_ENTITY:
            $entityLevel = 'Region';
            $entityLevelPlural = 'Regions';
            break;

        case E4S_COUNTRY_ENTITY:
            $entityLevel = 'Country';
            $entityLevelPlural = 'Countries';
            break;
    }

    if (!isset($prop)) {
        // property not set
        return E4S_NO_SECURITY;
    }

    $useHasThisLevel = FALSE;

    if (!is_null($entitySelected) and (int)$level === (int)$entitySelected->level) {
        foreach ($userObj->areas as $area) {
            // User has a higher entity level area than required
//        if ((int)$area->entitylevel > $level ){
//            return E4S_PASSED_SECURITY;
//        }
            if ((int)$area->entitylevel === $level || $level === 0) {
                // has an area defined
                if ($prop[0] === E4S_OPEN_TO_ALL_IDS) {
                    // open to all at this level
                    return E4S_PASSED_SECURITY;
                }
                if ($prop[0] === E4S_IDS_NOT_ALLOWED) {
                    // not available to this level
                    return E4S_NOT_AVAILABLE_SECURITY . $entityLevelPlural;
                }
                $useHasThisLevel = TRUE;
                foreach ($prop as $propid) {
                    if ($propid === (int)$area->areaid) {
                        // Area/Country allowed. is it the selected ?
                        if (is_null($entitySelected) or ($entitySelected->id === $propid and $entitySelected->level === $level)) {
                            return E4S_PASSED_SECURITY;
                        }
                    }
                }
            }
        }
    } else {
        return E4S_NOT_AVAILABLE_SECURITY . 'your ' . $entityLevelUser;
    }
    if (!$useHasThisLevel) {
        return E4S_NOT_AVAILABLE_SECURITY . 'your ' . $entityLevel;
    }

    $specific = '';
    if ($prop[0] !== E4S_OPEN_TO_ALL_IDS) {
        $specific = 'nominated ';
    }
    return E4S_AVAILABLE_SECURITY . $specific . $entityLevelPlural;
}

function e4s_getEmptySecurity() {
    $sec = new stdClass();
    $sec->clubs = array();
    $sec->counties = array();
    $sec->regions = array();
    return $sec;
}

// Output to UI
function e4s_getFullSecurityObj($security) {

    if (is_null($security) or empty($security)) {
        $security = new stdClass();
    }

    if (isset($security->clubs)) {
        $security->clubs = e4s_getFullSecurityClubObj($security->clubs);
    } else {
        $security->clubs = array();
    }

    if (isset($security->counties)) {
        $security->counties = e4s_getFullSecurityAreaObj($security->counties);
    } else {
        $security->counties = array();
    }

    if (isset($security->regions)) {
        $security->regions = e4s_getFullSecurityAreaObj($security->regions);
    } else {
        $security->regions = array();
    }
    return $security;
}

function e4s_getFullAllObj() {
    $obj = new stdClass();
    $obj->id = E4S_OPEN_TO_ALL_IDS;
    $obj->name = 'All';
    return $obj;
}

function e4s_getFullNoObj() {
    $obj = new stdClass();
    $obj->id = E4S_IDS_NOT_ALLOWED;
    $obj->name = 'None';
    return $obj;
}

function e4s_getFullAthleteSecurityClubObj($objArr) {

    if (is_null($objArr) || empty($objArr)) {
        return array();
    }

    $idArr = array();
    foreach ($objArr as $club) {
        $idArr[] = $club->id;
    }

    $newArr = e4s_getFullSecurityClubObj($idArr);

    return $newArr;
}

function e4s_getFullSecurityClubObj($objArr) {
    if (is_null($objArr) || empty($objArr)) {
        return array();
    }
    if ($objArr[0] === E4S_OPEN_TO_ALL_IDS) {
        return array(e4s_getFullAllObj());
    }
    if ($objArr[0] === E4S_IDS_NOT_ALLOWED) {
        return array(e4s_getFullNoObj());
    }

    $allClubs = e4s_getAllClubs();

    $arr = array();
    foreach ($objArr as $id) {
        $obj = new stdClass();
        $obj->id = $id;
        $obj->name = $allClubs[$id]['clubname'];
        $arr[] = $obj;
    }
    return $arr;
}

function e4s_getFullSecurityAreaObj($objArr) {
    if (is_null($objArr) || empty($objArr)) {
        return array();
    }
    if ($objArr[0] === E4S_OPEN_TO_ALL_IDS) {
        return array(e4s_getFullAllObj());
    }
    if ($objArr[0] === E4S_IDS_NOT_ALLOWED) {
        return array(e4s_getFullNoObj());
    }
    $ids = '';
    $idStrSep = '';
    foreach ($objArr as $id) {
        $ids .= $idStrSep . $id;
        $idStrSep = ',';
    }
    $sql = 'select id, name
            from ' . E4S_TABLE_AREA . '
            where id in (' . $ids . ')';
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows < 1) {
//        addDebug("Unable to retrieve areas : " . $ids);
        return array(e4s_getFullNoObj());
    }
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    $arr = array();
    foreach ($rows as $row) {
        $obj = new stdClass();
        $obj->id = $row['id'];
        $obj->name = $row['name'];
        $arr[] = $obj;
    }
    return $arr;
}

// Write to database wrapper
function e4s_setFullSecurityObj($security) {
    e4s_setFullSecurityClubObj($security);
    e4s_setFullSecurityCountyObj($security);
    e4s_setFullSecurityRegionObj($security);
    return $security;
}

function e4s_setFullSecurityClubObj(&$security) {
    if (!isset($security->clubs)) {
        return;
    }

    $security->clubs = e4s_setFullSecurityObjArr($security->clubs);

    if (empty($security->clubs)) {
        unset($security->clubs);
    } else {
//        if ($security->clubs[0] === E4S_OPEN_TO_ALL_IDS) {
//            unset($security->clubs);
//        }
    }
}

function e4s_setFullSecurityCountyObj(&$security) {
    if (!isset($security->counties)) {
        return;
    }

    $security->counties = e4s_setFullSecurityObjArr($security->counties);
    if (empty($security->counties)) {
        unset($security->counties);
    } else {
//        if ($security->areas[0] === E4S_OPEN_TO_ALL_IDS) {
//            unset($security->areas);
//        }
    }
}

function e4s_setFullSecurityRegionObj(&$security) {
    if (!isset($security->regions)) {
        return;
    }

    $security->regions = e4s_setFullSecurityObjArr($security->regions);
    if (empty($security->regions)) {
        unset($security->regions);
    } else {
//        if ($security->areas[0] === E4S_OPEN_TO_ALL_IDS) {
//            unset($security->areas);
//        }
    }
}

function e4s_setFullSecurityObjArr($objArr) {
    $retArr = array();
    foreach ($objArr as $obj) {
        if (!isset($obj->id)) {
            return $objArr;
        }
        $retArr[] = $obj->id;
    }
    return $retArr;
}