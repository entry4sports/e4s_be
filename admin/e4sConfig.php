<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_restGetConfig() {
    $config = e4s_getConfig(TRUE, TRUE);
    $meta = e4s_getConfigMeta();
	e4s_updatePof10IfRequired();
    Entry4UISuccess('
        "data":' . e4s_encode($config) . ',
        "meta":' . e4s_encode($meta));
}

function e4s_setLocalHostAdmin($obj) {
    $localAdmin = (int)checkFieldFromParamsForXSS($obj, 'localadmin:Allow local admin');
    Entry4UISuccess();
}

function e4s_getConfigMeta() {
    $locationObj = new locationClass();
    $locations = $locationObj->getHomePageLocations();
    $organisers = e4s_getOrganisersForHomePage();
    $events = e4s_getEventsForHomePage();
    $agObj = new e4sAgeClass(E4S_AGE_DEFAULT_BASE);
    $ageGroups = $agObj->getAgeGroups(null);
    $meta = new stdClass();
    $meta->locations = $locations;
    $meta->organisers = $organisers;
    $meta->events = $events;
    $meta->ageGroups = $ageGroups;
    $meta->defaults = e4s_getConfigDefaults();
    return $meta;
}

function e4s_getConfigDefaults() {
    $defaults = new stdClass();
    $searchAthlete = new stdClass();
    $searchAthlete->count = 20;
    $searchAthlete->noText = 'You have no athletes. What you playing at ?';
    $defaults->searchAthlete = $searchAthlete;
    return $defaults;
}

function e4s_getEventsForHomePage() {
    $eventObj = new e4sEvents();
    return $eventObj->listEventDefsForHomePage();
}

function e4s_getOrganisersForHomePage() {
    $sql = 'select id,
                   club name
            from ' . E4S_TABLE_COMPCLUB . '
            where options not like \'%"list":false%\'';

    $result = e4s_queryNoLog($sql);
    $orgs = array();
    while ($obj = $result->fetch_object()) {
        $obj->id = (int)$obj->id;
        $orgs[] = $obj;
    }
    return $orgs;
}