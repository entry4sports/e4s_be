<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
const E4S_OPTION_SYNC = 'aai_last_sync';

function e4s_AAISync(){
    $syncObj = new aaiSyncClass();
}

class aaiSyncClass {
    public $lastSyncTS;

    public function __construct() {
        $this->_getLastSync();
    }

    private function _getLastSync() {
        $sql = '
            select *
            from ' . E4S_TABLE_OPTIONS . "
            where option_name = '" . E4S_OPTION_SYNC . "'";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            $this->lastSyncTS = time();
            $this->_createSyncRecord();
        } else {
            $obj = $result->fetch_object(E4S_OPTIONS_OBJ);
        }
    }

    private function _createSyncRecord() {
        $sql = 'insert into ' . E4S_TABLE_OPTIONS . " (option_name,option_value, autoload)
                values (
                    '" . E4S_OPTION_SYNC . "',
                    " . time() . ",
                    'no'
                )";
        e4s_queryNoLog($sql);
    }

    public function syncArea() {

    }
}