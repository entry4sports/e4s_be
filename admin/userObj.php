<?php
/**
 * @param $update
 * @return array|mixed|object|stdClass
 */
function e4s_getUserObj($update, $cache) {

    if ($cache and !$update) {
        return $GLOBALS[E4S_USER];
    }
    $userid = e4s_getUserID();

    if ($userid === E4S_USER_ANON_ID) {
        $obj = new stdClass();
        $user = new stdClass();
        $user->id = 0;
        $user->impersonating = FALSE;
        $user->user_login = '';
        $user->user_nicename = '';
        $user->user_email = '';
        $user->google_email = '';
        $user->display_name = '';
        $user->role = E4S_ROLE_ANON;
        $user->version = e4s_getDefaultVersionObj();
        $emptyArr = new stdClass();
        $emptyArr->id = 0;
        $emptyArr->Clubname = '';
        $emptyArr->Region = '';
        $emptyArr->Country = '';
        $emptyArr->areaid = 0;
        $emptyArr->active = 0;
        $user->clubs = $emptyArr;
        $emptyArr = new stdClass();
        $emptyArr->areaid = 0;
        $emptyArr->areaname = 'No Area';
        $emptyArr->areashortname = '';
        $emptyArr->areaparentid = null;
        $emptyArr->entitylevel = 0;
        $emptyArr->entityName = '';
        $user->areas = $emptyArr;
        $obj->user = $user;
        $GLOBALS[E4S_USER] = $obj;
        return $obj;
    }

    $userAccount = e4s_getUserAccount();
    wp_set_current_user(e4s_getUserID());
//    $wpUser = wp_get_current_user();
    // Get the meta data for acting user
    $meta = e4s_getUserMeta(TRUE);
    $meta['user'] = $userAccount;
    $meta['wp_role'] = 'user';
    if (isset($GLOBALS['current_user']->roles) and !empty($GLOBALS['current_user']->roles)) {
        $meta['wp_role'] = $GLOBALS['current_user']->roles[0];
    }
    if ($update === FALSE) {
        // attempt to read cached obj
        if (array_key_exists(E4S_USER_KEY, $meta)) {
            $obj = json_decode($meta[E4S_USER_KEY]);
//            $obj->user = json_decode(json_encode($userAccount));
            $GLOBALS[E4S_USER] = $obj;
            e4s_checkVersion($GLOBALS[E4S_USER]);
            return $GLOBALS[E4S_USER];
        }
    }

    $meta = e4s_checkVersion($meta);

    // get any Club Info
    e4s_getClubInfo($meta);

    // get any Area Info
    e4s_getAreaInfo($meta);

    // get any Org Info
    e4s_getOrgInfo($meta);

    $meta['security'] = e4s_getUserSecurity();

    e4s_setUserMeta($meta);

    // remove the obj from the array
    if (array_key_exists(E4S_USER_KEY, $meta)) {
        unset($meta[E4S_USER_KEY]);
    }

    $meta = json_encode($meta);
    $GLOBALS[E4S_USER] = json_decode($meta);

    return $GLOBALS[E4S_USER];
}
function e4s_getAdminInfo() {
	$adminInfo = array();
	$userId = e4s_getUserID();
	if ( isE4SUser() or $userId <= E4S_USER_NOT_LOGGED_IN){
		return $adminInfo;
	}

	$security = $GLOBALS[E4S_USER]->security;
	$permissions = $security->permissions;
	foreach ($permissions as $permission) {
		if ($permission->role === PERM_ADMIN) {
			$obj = new stdClass();
			$obj->orgId = (int)$permission->orgid;
			$obj->compId = (int)$permission->compid;
			$adminInfo[] = $obj;
		}
	}

	return $adminInfo;
}
function e4s_getUserEmail() {
    $userEmail = '';
    if (array_key_exists(E4S_USER, $GLOBALS)) {
        if (isset($GLOBALS[E4S_USER]->user)) {
            $userEmail = $GLOBALS[E4S_USER]->user->user_email;
        }
    }
    return $userEmail;
}

function e4s_getOrgInfo(&$meta) {
    if (!empty($meta['orgs'])) {
        $orgs = implode(',', $meta['orgs']);
        $sql = 'select id, club name
                    from ' . E4S_TABLE_COMPCLUB . '
                    where id in (' . $orgs . ')';

        $orgResult = e4s_queryNoLog($sql);
        $orgRows = $orgResult->fetch_all(MYSQLI_ASSOC);
        $useRows = array();
        foreach ($orgRows as $orgRow) {
            $orgRow['locations'] = getLocsForOrg($orgRow['id']);
            $useRows[] = $orgRow;
        }
        $meta['orgs'] = $useRows;
    }
}

function e4s_getAreaInfo(&$meta) {
    if (!empty($meta['areas'])) {
        $areas = implode(',', $meta['areas']);

        $sql = 'Select a.id areaid, a.name areaname, a.shortname areashortname, a.parentid areaparentid, e.level entityLevel,  e.level entitylevel , e.name entityName
            from ' . E4S_TABLE_ENTITY . ' e,
                 ' . E4S_TABLE_AREA . ' a
            where e.id = a.entityid
            and   a.id in (' . $areas . ')';
        $results = e4s_queryNoLog($sql);
        $meta['areas'] = $results->fetch_all(MYSQLI_ASSOC);
    }
}
function e4s_getClubInfo(&$meta) {
    $clubs = '';
    $sep = '';
    if (!empty($meta['clubs'])) {
        $clubs = implode(',', $meta['clubs']);
        $sep = ',';
    }
    $compClubIds = [];
    foreach ($meta as $key=>$prop){
        if ( strpos($key,E4S_CLUBCOMP) === 0){
            $compClubIds[] = $prop;
        }
    }
    if ( sizeof($compClubIds) > 0 ){
        $sql = '
            select clubId 
            from ' . E4S_TABLE_CLUBCOMP . '
            where id in (' . implode(',',$compClubIds) . ')
        ';
        $result = e4s_queryNoLog($sql);
        while ($obj = $result->fetch_object()){
            $clubs .= $sep . $obj->clubId;
            $sep = ',';
        }
    }
    if ( $clubs !== ''){
        $sql = 'select *
                from ' . E4S_TABLE_CLUBS . '
                where id in (' . $clubs . ')';
        $results = e4s_queryNoLog($sql);
        $rows = $results->fetch_all(MYSQLI_ASSOC);
        $newRows = array();
        foreach ($rows as $row) {
            $row['entityName'] = 'Club';
            $row['entityLevel'] = 1;
            $row['options'] = e4s_getOptionsAsObj($row['options']);
            $newRows[] = $row;
        }
        $meta['clubs'] = $newRows;
    }
}

function e4s_clearUserMeta($userid) {
    $sql = 'delete from ' . E4S_TABLE_USERMETA . "
            where user_id = {$userid}
            and meta_key = '" . E4S_USER_KEY . "'";
    e4s_queryNoLog($sql);
}

function e4s_setUserMeta($meta) {
    $userid = e4s_getUserID();

    if (array_key_exists(E4S_USER_KEY, $meta)) {
        unset($meta[E4S_USER_KEY]);
        $metaJson = json_encode($meta, JSON_HEX_APOS);
        // update
        $sql = 'update ' . E4S_TABLE_USERMETA . "
                set meta_value = '" . $metaJson . "'
                where user_id = " . $userid . "
                and meta_key = '" . E4S_USER_KEY . "'";
    } else {
        // insert
        $metaJson = json_encode($meta, JSON_HEX_APOS);
        $sql = 'Insert into ' . E4S_TABLE_USERMETA . ' ( user_id, meta_key, meta_value)
                values (' . $userid . ",'" . E4S_USER_KEY . "','" . $metaJson . "')";
    }
    e4s_queryNoLog($sql);
}

function e4s_getUserMeta($allowRecurse) {
    $userid = e4s_getUserID();
	$cacheKey = E4S_CACHE_USER_OBJ . ":" . $userid;
	if ( array_key_exists($cacheKey, $GLOBALS) ){
		return $GLOBALS[$cacheKey];
	}
    $sql = 'select *
            from ' . E4S_TABLE_USERMETA . '
            where user_id = ' . $userid . "
            and meta_key like 'e4s_%'";

    $result = e4s_queryNoLog($sql);
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    $orgs = array();
    $areas = array();
    $clubs = array();
    $meta = array();
    foreach ($rows as $row) {
        if ($row['meta_key'] === E4S_AREA_ID) {
            $areas[] = (int)$row['meta_value'];
        } elseif ($row['meta_key'] === E4S_CLUB_ID) {
            $clubs[] = (int)$row['meta_value'];
        } elseif ($row['meta_key'] === E4S_ORG_ID) {
            $orgs[] = (int)$row['meta_value'];
        } elseif ($row['meta_key'] === E4S_IMPERSONATE) {
            if (!e4s_isUserBeingImpersonated() and $allowRecurse) {
                e4s_setImpersonateUserID((int)$row['meta_value']);
                // reload met with impersonating id
                return e4s_getUserMeta(FALSE);
            }
        } else {
            $meta[$row['meta_key']] = $row['meta_value'];
        }
    }
    $meta['orgs'] = $orgs;
    $meta['areas'] = $areas;
    $meta['clubs'] = $clubs;

	$GLOBALS[$cacheKey] = $meta;
    return $meta;
}

function e4s_getUserOrgs() {
    $retVal = null;
    if (isset($GLOBALS[E4S_USER])) {
        $user = $GLOBALS[E4S_USER];
        if (isset($user->orgs)) {
            return $user->orgs;
        }
        return FALSE;
    }
    return $retVal;
}

function isAdminUser() {
    if (isE4SUser()) {
        return TRUE;
    }
    if (isset($GLOBALS[E4S_WC_USER])) {
        $user = $GLOBALS[E4S_WC_USER];
        if (isset($user->caps['administrator'])) {
            return $user->caps['administrator'];
        }
        return FALSE;
    }
    return FALSE;
}

function e4s_getAccountForID($userid) {
	$cacheKey = 'e4s_accountInfo' . $userid;
	if ( array_key_exists($cacheKey,$GLOBALS) ){
		return $GLOBALS[$cacheKey];
	}
    $sql = 'select id, user_login, user_nicename, user_email, display_name
            from ' . E4S_TABLE_USERS . '
            where id = ' . $userid;
    $results = e4s_queryNoLog($sql);
    $row = $results->fetch_assoc();

    if ((int)$row['id'] === (int)$userid) {
        $row['role'] = E4S_ROLE_USER;
        if (strpos($row['user_email'], '@entry4sports.com') !== FALSE or e4s_hasFullAccess()) {
            $row['role'] = E4S_ROLE_ADMIN;
        } else {
            $adminEmail = '';
            $config = e4s_getConfig();
            $options = e4s_getOptionsAsObj($config['options']);
            if (isset($options->adminEmail)) {
                $adminEmail = $options->adminEmail;
            }
            if ($adminEmail !== '') {
                if (strpos($row['user_email'], $adminEmail) !== FALSE) {
                    $row['role'] = E4S_ROLE_APPADMIN;
                }
            }
        }
    }

    $row['google_email'] = '';

    $impersonate_property = 'impersonating';
    $row[$impersonate_property] = FALSE;
//    if (e4s_isUserBeingImpersonated()) {
//        $row[$impersonate_property] = TRUE;
//    }
    include_once E4S_FULL_PATH . 'builder/permCRUD.php';
    $perms = e4s_getPermissionsForUser($userid);
    $credit = get_user_meta($userid, E4S_CREDIT_KEY);
    if (is_null($credit)) {
        $credit = 0;
    }
    $row['e4sCredit'] = $credit;
    $row['permissions'] = $perms;
	$GLOBALS[$cacheKey] = $row;
    return $row;
}

function e4s_getUserAccount() {
    return e4s_getAccountForID(e4s_getUserID());
}

function e4s_getUserSecurity() {
    $userid = e4s_getUserID();
    $security = new stdClass();
    $sql = 'select p.*, lower(r.role) role
            from ' . E4S_TABLE_PERMISSIONS . ' p
            left join ' . E4S_TABLE_ROLES . ' r on p.roleid = r.id
            where p.userid = ' . $userid;
//            order by roleid asc, orgid asc, compid asc";


    $permResult = e4s_queryNoLog($sql);
    $permRows = $permResult->fetch_all(MYSQLI_ASSOC);
    $usePermRows = array();
    foreach ($permRows as $permRow) {
        if (is_null($permRow['role'])) {
            $permRow['role'] = 'all';
        }
        if ((int)$permRow['orgid'] === 0) {
            $permRow['orgname'] = 'All';
        } else {
            $sql = '
                select * from ' . E4S_TABLE_COMPCLUB . '
                where id = ' . $permRow['orgid'];
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows > 0) {
                $orgRow = $result->fetch_assoc();
                $permRow['orgname'] = $orgRow['club'];
            }
        }

        $usePermRows[] = $permRow;
    }
    $security->permissions = $usePermRows;

    $sql = 'select levelid, level, lower(role) role
            from    ' . E4S_TABLE_PERMISSIONLEVEL . ' pl,
                    ' . E4S_TABLE_ROLELEVEL . ' rl,
                    ' . E4S_TABLE_ROLES . ' r
            where pl.userid = ' . $userid . '
            and   pl.levelid = rl.id
            and   rl.roleid  = r.id
        ';

    $levelResult = e4s_queryNoLog($sql);
    $levelRows = $levelResult->fetch_all(MYSQLI_ASSOC);
    $lastRole = '';
    $allRoleRows = array();
    $roleRows = array();
    foreach ($levelRows as $row) {
        if ($lastRole !== $row['role'] and $lastRole !== '') {
            $allRoleRows[$lastRole] = $roleRows;
            $roleRows = array();
        }
        $useRow = new stdClass();
        $useRow->levelid = $row['levelid'];
        $useRow->level = $row['level'];
        $roleRows[] = $useRow;
        $lastRole = $row['role'];
    }
    $allRoleRows[$lastRole] = $roleRows;
    $security->permLevels = $allRoleRows;
    return $security;
}

function e4s_getUserRole() {
    return $GLOBALS[E4S_USER]->user->role;
}

function e4s_getUserName($field = 'display_name') {
    $userName = '';
    if (array_key_exists(E4S_USER, $GLOBALS)) {
        if (isset($GLOBALS[E4S_USER]->user)) {
            $userName = $GLOBALS[E4S_USER]->user->display_name;
        }
    }
    return $userName;
}

function isAppAdmin() {
    if (isE4SUser()) {
        return true;
    }
    $userRole = e4s_getUserRole();
    if ($userRole === E4S_ROLE_APPADMIN) {
        return TRUE;
    }
    return FALSE;
}

function isE4SUser() {
    $userRole = e4s_getUserRole();
    if ($userRole === E4S_ROLE_ADMIN and !e4s_isUserBeingImpersonated()) {
        return TRUE;
    }
	$userId = e4s_getUserID();
	if ( (int)$userId === 14805){
		return true;
	}
    return e4s_hasFullAccess();
}
function e4s_hasFullAccess() {
    $configObj = e4s_getConfigObj();
    return $configObj->hasFullAccess();
}

function e4s_getUserID() {
    return e4s_getImpersonateUserID();
}

function e4s_isUserBeingImpersonated() {
    return e4s_getImpersonateUserID() != e4s_getBaseUserID();
}

function e4s_setImpersonateUserID($id) {
    $GLOBALS[E4S_IMPERSONATE_USER_ID] = $id;
}

function e4s_getImpersonateUserID() {
    if ( array_key_exists(E4S_IMPERSONATE_USER_ID,$GLOBALS)) {
        return $GLOBALS[E4S_IMPERSONATE_USER_ID];
    }
    return 0;
}

function e4s_getBaseUserID() {
    return $GLOBALS[E4S_USER_ID];
}

function e4s_impersonateById($id) {
    e4s_impersonateAUser('ID', $id, FALSE);
}

function e4s_impersonate($email) {
    e4s_impersonateAUser('user_email', $email, TRUE);
}

function e4s_impersonateOutput($echoOutput, $text) {
    if ($echoOutput) {
        echo $text;
    }
}

function e4s_impersonateAUser($key, $value, $echoOutput) {
    if (!isE4SUser() and !e4s_isUserBeingImpersonated()) {
        Entry4UIError(1001, 'Not Authorised to perform this action', 404, '');
    }

// delete impersonate row if one exists
    $sql = 'delete from ' . E4S_TABLE_USERMETA . '
        where user_id = ' . e4s_getBaseUserID() . "
        and meta_key = '" . E4S_IMPERSONATE . "'";

    e4s_queryNoLog($sql);

    if (is_null($value) || $value === '' || $value === '0') {
        e4s_impersonateOutput($echoOutput, 'Resetting Impersonate settings.<br>');
        if (!$echoOutput) {
            Entry4UISuccess('');
        }
        exit();
    } else {
        $userRow = e4sGetUserRow($key, $value, FALSE);

        if ($userRow === null) {
            e4s_impersonateOutput($echoOutput, '<br>Not found');
            Entry4UIError(1002, 'User not found', 400, '');
        }
        e4s_impersonateOutput($echoOutput, 'You are impersonating user ' . $value . "<br><br>To clear impersonation. click here >> <a href=\"https://" . E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH . "/admin/impersonate?email=\">Reset</a>");

    }

    $sql = 'insert into ' . E4S_TABLE_USERMETA . ' (user_id, meta_key, meta_value)
            values (' . e4s_getBaseUserID() . ",'" . E4S_IMPERSONATE . "','" . $userRow['ID'] . "')";

    e4s_queryNoLog($sql);
    if (!$echoOutput) {
        Entry4UISuccess('');
    }
}

function e4sGetAdminUserRow($useCache) {
    $adminRowName = E4S_ADMIN_USER . 'Row';
    if ($useCache === FALSE || array_key_exists($adminRowName, $GLOBALS) === FALSE) {
        e4sGetGenericUserRow($adminRowName, 'user_nicename', E4S_ADMIN_USER, $useCache);
    }
    return $GLOBALS[$adminRowName];
}

function e4sGetUserRow($userKey, $userKeyValue, $useCache) {
    $userRowName = 'e4sUserRow';
    return e4sGetGenericUserRow($userRowName, $userKey, $userKeyValue, $useCache);
}

// Read Permissions Functions
function getPermissionObjectForComp($orgid, $compid) {
    $perms = new stdClass();
    $perms->adminMenu = FALSE;

    if (userHasPermission(PERM_BUILDER, $orgid, $compid) === TRUE) {
        $perms->builder = TRUE;
        $perms->adminMenu = TRUE;
    }
    if (userHasPermission(PERM_CHECK, $orgid, $compid) === TRUE) {
        $perms->check = TRUE;
        $perms->adminMenu = TRUE;
    }
    if (userHasPermission(PERM_REPORT, $orgid, $compid) === TRUE) {
        $perms->report = TRUE;
        $perms->adminMenu = TRUE;
    }
    return $perms;
}

function userHasPermission($role, $orgId, $compId = null) {

    if (isE4SUser()) {
        return TRUE;
    }

    $role = strtolower($role);

    if (!is_null($orgId)) {
        $orgId = (int)$orgId;
    }
    if (!is_null($compId)) {
        $compId = (int)$compId;
    }

    // get the org for a compid if not passed
    if (is_null($orgId) and $compId > 0) {
        $compObj = e4s_getCompObj($compId);
        $row = $compObj->getRow();

        if (is_null($row)) {
            Entry4UIError(9101, 'Can not read competition : ' . $compId, 400, '');
        } else {
            $orgId = (int)$row['compclubid'];
        }
    }

    // User logged in and the comp has a link check if it should be displayed
    $userPermissions = getPermissions($role, $orgId, $compId);

    if (is_null($userPermissions)) {
        // Does not have perms for this role
        return FALSE;
    }
// has the builder / admin role and creating a competition
    if ($orgId === -1) {
        if (!is_null($userPermissions)) {
            return TRUE;
        }
    }
    if (is_null($orgId) and is_null($compId)) {
        return TRUE;
    }

    foreach ($userPermissions as $permission) {
        $permOrgId = (int)$permission->orgid;
        $permCompId = (int)$permission->compid;
        if ($permOrgId === 0) {
            return TRUE;
        }
        if ($permCompId === $compId) {
            return TRUE;
        }
        if ($permOrgId === $orgId and $permCompId === 0) {
            return TRUE;
        }
    }
    return FALSE;
}

function getPermissions($role, $orgId = null, $compId = null) {
    // possible to be admin for one org and std for another !!!!
    $role = strtolower($role);

    getUserPermissions(PERM_ADMIN, $orgId, $compId);
    $rolePerms = getUserPermissions($role, $orgId, $compId);

    return $rolePerms;
}

function e4s_hasAnyPermForComp($compId) {
    if (isE4SUser()) {
        return TRUE;
    }
    $retVal = FALSE;
    $compObj = e4s_GetCompObj($compId);
    $orgId = $compObj->getOrganisationId();
    if (isset($GLOBALS[E4S_USER]->security)) {
        $rows = $GLOBALS[E4S_USER]->security->permissions;
        foreach ($rows as $row) {
            $row->compid = (int)$row->compid;
            $row->orgid = (int)$row->orgid;
            if ($row->orgid === 0 and $row->compid === 0) {
                $retVal = TRUE;
                break;
            }
            if ($row->orgid === $orgId and $row->compid === 0) {
                $retVal = TRUE;
                break;
            }
            if ($row->compid === $compId) {
                $retVal = TRUE;
                break;
            }
        }
    }
    return $retVal;
}

function getUserPermissions($role, $orgId = null, $compId = null) {
    if (array_key_exists(PERM_ADMIN . PERM_PERMS_SUFFIX, $GLOBALS)) {
        // If an admin, this overrides
        $adminPerms = $GLOBALS[PERM_ADMIN . PERM_PERMS_SUFFIX];
        if (!is_null($adminPerms)) {
            if ($orgId === 0) {
                return $adminPerms;
            }
            foreach ($adminPerms as $adminPerm) {
                if ((int)$adminPerm->compid === $compId or ((int)$adminPerm->compid === 0 and (int)$adminPerm->orgid === $orgId)) {
                    return $adminPerms;
                }
            }
        }
    }

    if (isset($GLOBALS[E4S_USER]->security)) {
        $rows = $GLOBALS[E4S_USER]->security->permissions;

        $retRows = array();

        foreach ($rows as $row) {
            if ($row->role === $role and ((int)$orgId === 0 or (int)$row->orgid === $orgId or (int)$row->compid === $compId)) {
                $retRows[] = $row;
            }
        }
        if (sizeof($retRows) === 0) {
            $retRows = null;
        }
    } else {
        $retRows = null;
    }
    $GLOBALS[$role . PERM_PERMS_SUFFIX] = $retRows;

    return $retRows;
}

function hasPermForOrgOrComp($perms, $row) {

    foreach ($perms as $perm) {
        if ($perm->orgid === 0) {
            return TRUE;
        }
        if ($perm->orgid === (int)$row['compclubid']) {
            return TRUE;
        }
        if ($perm->compid !== 0 and $perm->compid === (int)$row['compid']) {
            return TRUE;
        }
    }
    return FALSE;
}

function e4s_getAdminPermissionObject() {
    $perms = new stdClass();
    $perms->adminMenu = FALSE;
    $perms->orgs = null;
    $isE4SUser = isE4SUser();

    if (userHasPermission(PERM_BUILDER, 0, 0) === TRUE) {
        $perms->builder = TRUE;
        $perms->adminMenu = TRUE;
        if ($isE4SUser === TRUE) {
            $perms->orgs = array();
//            $perms->orgs = $GLOBALS[E4S_USER]->orgs;
        } else {
            // get list of orgs for user
            $perms->orgs = $GLOBALS[E4S_USER]->orgs;
//            $perms->orgs = e4s_getOrgsForUser();
        }
    }
    if (userHasPermission(PERM_CHECK, 0, 0) === TRUE) {
        $perms->cheques = TRUE;
        $perms->adminMenu = TRUE;
    }
    if (userHasPermission(PERM_USERSEARCH, 0, 0) === TRUE) {
        $perms->userSearch = TRUE;
        $perms->adminMenu = TRUE;
    }
    if (userHasPermission(PERM_USERPERMS, 0, 0) === TRUE) {
        $perms->userPerms = TRUE;
        $perms->userSearch = TRUE;
        $perms->adminMenu = TRUE;
    }

    return $perms;
}

function e4s_checkVersion($userObj){
    require_once E4S_FULL_PATH . 'dbInfo.php';

    $arrayPassed = true;
    if (gettype($userObj) !== 'array'){
        $arrayPassed = false;
    }else{
        $userObj = e4s_getDataAsType($userObj,E4S_OPTIONS_OBJECT);
    }
    $user = $userObj->user;

    if (isset($userObj->{E4S_VERSION})){
        $user->version = e4s_makeVersionObj($userObj->{E4S_VERSION});
        unset($userObj->{E4S_VERSION});
    }else{
        $user->version = e4s_getDefaultVersionObj();
    }

    if ( $arrayPassed ){
        $userObj = e4s_getDataAsType($userObj,E4S_OPTIONS_ARRAY);
    }
    return $userObj;
}

function e4s_getDefaultVersionNo(){
    return 1;
}
function e4s_getDefaultVersion(){
    return e4s_getVersionData(e4s_getDefaultVersionNo(), 0);
}
function e4s_getDefaultVersionObj(){
    $versionData = e4s_getDefaultVersion();
    return e4s_makeVersionObj($versionData);
}
function e4s_setVersion($version, $toggle = null, $userId = null){
    require_once E4S_FULL_PATH . 'dbInfo.php';
    $useUserId = e4s_getUserID();
    if ( $useUserId === E4S_USER_ANON_ID){
        return e4s_getDefaultVersionObj();
    }
    if ( !is_null($userId) or !is_null($toggle) ) {
        // This is only for E4S
        if (!isE4SUser()){
//            Entry4UIError(9451,"Sorry, Not authorised");
        }
        if ( !is_null($userId) ) {
            $useUserId = $userId;
        }
    }
    // update users version
    $result = e4s_getVersionResult($useUserId);

    if ($result->num_rows === 0) {
        if ( !isE4SUser() ){
            $versionData = e4s_getDefaultVersion();
        }else{
            $versionData = e4s_getVersionData($version, $toggle);
        }

        $sql = 'Insert into ' . E4S_TABLE_USERMETA . ' (user_id, meta_key, meta_value)
                    values (' . $useUserId . ",'" . E4S_VERSION . "','" . $versionData . "')";
    } else {
        $obj = $result->fetch_object();
        $versionData = e4s_makeVersionObj($obj->meta_value);
        if ( !isE4SUser() and $versionData->toggle === false){
            // not E4S and dont have toggle status so can not set version
//            Entry4UIError(9452,"Sorry, Not authorised");
            Entry4UISuccess();
        }
        $versionData->current = e4s_checkVersionPrefix($version);
        if ( isE4SUser() and !is_null($toggle) ){
            $versionData->toggle = 0;
            if ( $toggle ){
                $versionData->toggle = 1;
            }
        }
        $sql = 'update ' . E4S_TABLE_USERMETA . "
                set meta_value = '" . e4s_getVersionDataFromObj($versionData) . "'
                where umeta_id = " . $obj->umeta_id;
    }
    e4s_queryNoLog($sql);
}
function e4s_getUserVersion($userId){
    $result = e4s_getVersionResult($userId);
    if ( $result->num_rows === 0){
        return e4s_getDefaultVersionObj();
    }
    $obj = $result->fetch_object();
    return e4s_makeVersionObj($obj->meta_value);
}
function e4s_getVersionResult($userId){
    $sql = 'select umeta_id, meta_value from ' . E4S_TABLE_USERMETA . '
                where user_id = ' . $userId . "
                and meta_key = '" . E4S_VERSION . "'";
    return e4s_queryNoLog($sql);
}
function e4s_getVersionData($version, $toggle){
    $version = e4s_checkVersionPrefix($version);
    if ( $toggle ){
        return $version . ':1';
    }else{
        return $version . ':0';
    }
}
function e4s_getVersionDataFromObj($obj){
    return e4s_getVersionData($obj->current, $obj->toggle);
}
function e4s_checkVersionPrefix($version){
    if (strpos(''.$version, 'v') === false){
        $version = 'v' . $version;
    }
    return $version;
}
function e4s_makeVersionObj($versionData){
    if ( $versionData === '' ){
        return e4s_getDefaultVersionObj();
    }
    $versionObj = new stdClass();
    $verInfo = preg_split('~:~',$versionData);
    $versionObj->current = e4s_checkVersionPrefix($verInfo[0]);

    $versionObj->toggle = false;
    if ( $verInfo[1] === '1'){
        $versionObj->toggle = true;
    }
    return $versionObj;
}

function getUserPageSize() {
    // setup default paging

    $paging = array();
    $paging['page'] = 0;
    $paging['pageSize'] = 20;
    $paging['totalCount'] = 0;
    $userid = e4s_getUserID();

// update users pagesize
    $pagesizeSql = 'select * from ' . E4S_TABLE_USERMETA . '
                where user_id = ' . $userid . "
                and meta_key = '" . E4S_PAGESIZE . "'";
    $psresult = e4s_queryNoLog(basename(__FILE__) . E4S_SQL_DELIM . $pagesizeSql);
    if ($psresult->num_rows === 0) {
        $pagesizeSql = 'Insert into ' . E4S_TABLE_USERMETA . ' (user_id, meta_key, meta_value)
                    values (' . $userid . ",'" . E4S_PAGESIZE . "'," . $paging['pageSize'] . ')';
        e4s_queryNoLog(basename(__FILE__) . E4S_SQL_DELIM . $pagesizeSql);
    } else {
        $row = $psresult->fetch_assoc();
        $paging['pageSize'] = $row['meta_value'];
    }
    return $paging;
}

function updateUserPageSize($pagesize) {
    // setup default paging
    $userid = e4s_getUserID();

// update users pagesize

    $pagesizeSql = 'update ' . E4S_TABLE_USERMETA . '
                set meta_value = ' . $pagesize . '
                where user_id = ' . $userid . "
                and meta_key = '" . E4S_PAGESIZE . "'";

    e4s_queryNoLog($pagesizeSql);
}

function e4s_getCurrentUserType() {
    $areas = getUserAreas();
    if (!empty($areas)) {
        if ($areas[0]->areaid !== 0) {
            return E4S_USERTYPE_AREA;
        }
    }
    $clubs = getUserClubs();
    if (!empty($clubs)) {
        if ($clubs[0]->id !== 0) {
            return E4S_USERTYPE_CLUB;
        }
    }
    return E4S_USERTYPE_NONE;
}

function e4s_getUserType($compid, &$areaIds) {
// TODO This needs to handle multiple Clubs/Areas
    $userArea = null;
    $userType = e4s_getCurrentUserType();

    if ($userType !== E4S_USERTYPE_NONE) {
        $userClub = getUserClub();
        $userArea = getUserArea();

        $areaIds = getCompAreaIds($compid, TRUE, TRUE, 0, $userClub, $userArea);

        if ($areaIds === '') {
            $userType = E4S_USERTYPE_NONE; // Area user but not for this comp so use Assoc athletes
        }
    }
    return $userType;
}

function getUserClubAndAreaIds() {

    $userid = e4s_getUserID();
    $returnArr = array();
    if ($userid <= E4S_USER_NOT_LOGGED_IN) {
        $returnArr[] = 0;
        return $returnArr;
    }
    $userSql = 'Select u.meta_value
                from ' . E4S_TABLE_USERMETA . " u
            where u.user_id = $userid
            and   u.meta_key in ('" . E4S_CLUB_ID . "','" . E4S_AREA_ID . "')
            order by u.meta_key";

    $result = e4s_queryNoLog($userSql);

    if ($result->num_rows === 0) {
        $returnArr[] = 0;
    } else {
        $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
        foreach ($rows as $row) {
            $returnArr[] = $row['meta_value'];
        }
    }

    return $returnArr;
}

function getUserClub() {
    return getClubForUser(e4s_getUserID());
}

function getClubForUser($userid) {
    $clubs = getClubsForUser($userid);
    return $clubs[0];
}

function getUserClubs() {
//    if (!e4s_isUserBeingImpersonated()){
    return $GLOBALS[E4S_USER]->clubs;
//    }
//    $curUserId = e4s_getUserID();
//    return getClubsForUser($curUserId);
}

function getClubsForUser($userid) {
    $emptyArr = array();
    $emptyArr['id'] = 0;
    $emptyArr['Clubname'] = '';
    $emptyArr['Region'] = '';
    $emptyArr['Country'] = '';
    $emptyArr['areaid'] = 0;
    $emptyArr['active'] = 0;

    if (!is_numeric($userid)) {
        return array($emptyArr);
    }
    $userSql = 'Select c.*
                from ' . E4S_TABLE_USERMETA . ' u,
                     ' . E4S_TABLE_CLUBS . " c
            where c.id = u.meta_value
            and   u.user_id = $userid
            and   u.meta_key = '" . E4S_CLUB_ID . "'";
    $result = e4s_queryNoLog($userSql);

    if ($result->num_rows === 0) {
        return array($emptyArr);
    }

    return $result->fetch_all(MYSQLI_ASSOC);
}

function getUserArea() {
    return getUserAreaForUser(e4s_getUserID());
}

function getUserAreaForUser($userid) {
    $areas = getAreasForUser($userid);
    return $areas[0];
}

function getUserAreas() {
    e4s_getUserObj(FALSE, TRUE);
    if (isset($GLOBALS[E4S_USER]->areas)) {
        return $GLOBALS[E4S_USER]->areas;
    }
    return array();
}

function getAreasForUser($userid) {
    $userSql = 'Select a.id areaid, a.name areaname, a.shortname areashortname, a.parentid areaparentid, e.level entitylevel, e.name entityName
            from ' . E4S_TABLE_ENTITY . ' e,
                 ' . E4S_TABLE_AREA . ' a,
                 ' . E4S_TABLE_USERMETA . " u
            where e.id = a.entityid
            and   u.user_id = $userid
            and   u.meta_key = '" . E4S_AREA_ID . "'
            and   a.id = u.meta_value";
    $result = e4s_queryNoLog($userSql);

    if ($result->num_rows === 0) {
        $emptyArr = array();
        $emptyArr['areaid'] = 0;
        $emptyArr['areaname'] = 'No Area';
        $emptyArr['areashortname'] = '';
        $emptyArr['areaparentid'] = null;
        $emptyArr['entitylevel'] = 0;
        $emptyArr['entityName'] = '';
        return array($emptyArr);
    }
    return $result->fetch_all(MYSQLI_ASSOC);
}
