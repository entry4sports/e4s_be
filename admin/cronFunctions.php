<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
$GLOBALS[E4S_CRON_USER] = TRUE;
require_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_processCron($obj) {
    $action = checkFieldForXSS($obj, 'action:Action');

    switch (strtolower($action)) {
        case 'emails':
            e4s_sendOutstandingEmails();
            break;

        default:
            Entry4UIError(1000, $action . ' Not allowed');
    }
    Entry4UISuccess();
}

function e4s_test() {
    $hostname = DB_HOST;
    $database = DB_NAME;
    $username = DB_USER;
    $password = DB_PASSWORD;
    $logConn = new mysqli($hostname, $username, $password, $database);
    $logConn->query("insert into Entry4_uk_Log ( created,userid, uid, info, trace,parentid)
                            values ( now(), 1,'192.168.10.22', 'cronTest3', 'tracing2',0)");
}

function e4s_processWaitingLists() {
    e4s_writeHealth(E4S_CRON_WAITLISTCHECKER, 900);

//Process waiting lists movements and refunds
    $sql = 'select id
            from ' . E4S_TABLE_COMPETITON . '
            where waitingrefunded = false
            and active = 1';

    $result = e4s_queryNoLog($sql);
    while ($obj = $result->fetch_object()) {
        $compId = (int)$obj->id;
        $compObj = e4s_GetCompObj($compId);
        $waitingObj = new waitingClass($compId);

        if ($compObj->isWaitingListEnabled()) {
            if ($compObj->areEntriesOpen()) {
                $waitingObj->processMovements();
            }

            if ($compObj->canWaitingListRefundsBeProcessed()) {
                $compObj->performWaitingListRefunds();
            }
        } else {
            // mark done to stop being processed again
            $waitingObj->markWaitingListDone($compObj);
        }

    }
    Entry4UISuccess();
}

function checkFeesPaid() {
    $GLOBALS[E4S_CRON_USER] = TRUE;
    include_once E4S_FULL_PATH . 'reports/cron/discountchecker.php';
}

function import_irish_athletes_loadv3() {
    $GLOBALS['e4s_import_id'] = 1;
    $GLOBALS[E4S_CRON_USER] = TRUE;
    include_once E4S_FULL_PATH . 'import/loadAthletesCronV3.php';
}

function import_northern_irish_athletes() {
    $GLOBALS['e4s_import_id'] = 2;
    include_once E4S_FULL_PATH . 'import/loadAthletesCronV3.php';
}

function e4s_cronCompStatusEmail() {
    include_once E4S_FULL_PATH . 'competition/compStatus.php';
    reportCompStatus();
}

function e4s_entryCheckUpdate() {
    //defunct function
    e4s_writeHealth(E4S_CRON_ENTRYCHECKER, 120);
//    include_once E4S_FULL_PATH . 'entries/entries.php';
//    e4s_checkUnPaid();
}

function e4s_reportOnCompStatus() {
    include_once E4S_FULL_PATH . 'reports/cron/paymentStatus.php';
}

function e4s_cronHealthMonitor() {
    $configObj = e4s_getConfigObj();
    return $configObj->healthMonitor();
}
function e4s_cronDeDedup() {
    e4s_dedupAthletes();
}

function e4s_checkFailedOrders() {
    e4s_writeHealth(E4S_CRON_ORDERCHECKER, 360);
    include_once E4S_FULL_PATH . 'entries/commonEntries.php';
    e4s_processFailedOrders();
}

function e4s_CheckForEntriesClosing() {
    e4s_writeHealth(E4S_CRON_ENTRIESCLOSING, 86400);
    $compObj = new e4sCompetition();
    $compObj->informEntriesClosing();
    $compObj->informCompetitionHappening();
    Entry4UISuccess('"status":"done"');
}

function e4s_sendOutstandingEmails() {
    e4s_writeHealth(E4S_CRON_EMAILCHECKER, 360);
    $emailObj = new e4SMessageClass();
    $emailObj->sendEmail(E4S_SEND_BATCH);
}