<?php


/**
 * Random coupon.
 *
 * Get a random coupon code.
 *
 * @return string Random coupon code.
 * @since 1.0.0
 *
 */
function wccg_get_random_coupon() {

    // Generate unique coupon code
    $random_coupon = '';
    $length = 12;
    $charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    $count = strlen($charset);

    while ($length--) {
        $random_coupon .= $charset[mt_rand(0, $count - 1)];
    }

    $random_coupon = implode('-', str_split(strtoupper($random_coupon), 4));

    // Ensure coupon code is correctly formatted
    $coupon_code = apply_filters('woocommerce_coupon_code', $random_coupon);

    return $coupon_code;
}

