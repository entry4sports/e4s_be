<?php
include_once E4S_FULL_PATH . '/dbInfo.php';
function Entry4UIReturn($errno, $errstr, $retError, $additional = '',$message = '') {

    if ($retError !== 0) {
        $headerStr = 'HTTP/1.1 ' . $retError;
        if ($retError === 400) {
            $retError .= ' Bad Request';
        }
        if ($retError === 401) {
            $retError .= ' Not Authorised';
        }
        header($headerStr);
    }

    $output = '{"errNo": ' . $errno . ', "error": "' . $errstr . '"';
//    $output = '{"errNo": ' . $errno ;

    if (empty($additional) === FALSE and gettype($additional) === 'string') {
        $output .= ',' . $additional;
    } elseif (gettype($additional) === 'object') {
        $output .= ',"data":' . e4s_encode($additional);
    }
    if ( $message !== '' ){
        if ( $output !== '' ){
            $output .= ',';
        }
        $output .= '"message":' . e4s_encode($message);
    }
    $output .= e4s_getDebugData();

    $output .= '}';

    if ($errno !== 0) {
        $level = 1;
        if ( $errno < 1000 ){
            $level = -1;
        }elseif ( $errno > 9000 ){
			$level = 2;
		}
        logTxt($output, $level);
    }
    exit($output);
}

function Entry4UIMessage($msgText) {
    Entry4UIReturn(0, E4S_SUCCESS, 0, '',$msgText);
}

function Entry4UISuccess($additional = '', $message = '') {
    $type = gettype($additional);
    if ($type === 'object' or $type === 'array') {
        $additional = '"data":' . e4s_encode($additional);
    }

    Entry4UIReturn(0, E4S_SUCCESS, 0, $additional, $message);
}

function Entry4UIError($errno, $errstr, $retError = 200, $additional = '', $message = '') {
    if ($errno > E4S_MIN_ERROR_FOR_EMAIL) {
        $body = $errstr;
        $body .= '<br><br>';
        $body .= 'User : ' . e4s_getUserID() . '/' . e4s_getUserName();
        $body .= '<br><br>';
        $body .= 'Called From : ' . $_SERVER['REQUEST_URI'] . '/' . $_SERVER['QUERY_STRING'];
        $body .= '<br><br>';
        if (array_key_exists(E4S_LAST_QUERY, $GLOBALS)) {
            if ($GLOBALS[E4S_LAST_QUERY] !== '') {
                $body .= 'Last Query : ' . $GLOBALS[E4S_LAST_QUERY];
                $body .= '<br><br>';
            }
        }
        if ($errno === 9305) {
            $body .= $additional;
        }
        foreach (debug_backtrace() as $index => $tracerow) {
			if ( array_key_exists('file',$tracerow)) {
				$file = strtolower( $tracerow['file'] );
				if ( strpos( $file, E4S_CURRENT_DOMAIN ) !== false ) {
					$file = explode( E4S_CURRENT_DOMAIN, $file );
					$file = $file[1];
				}

				if ( e4s_AddLineToEmail( $file, $errno ) ) {
					$body .= ( $index + 1 ) . ') ' . $tracerow['line'] . '-' . $file . ':' . $tracerow['function'] . '<br>';
				}
			}
        }
        if (!e4s_isDevDomain()) {
            e4s_mail(E4S_ERROR_EMAIL, E4S_CURRENT_DOMAIN . ":$errno Error", $body, Entry4_mailHeader('', FALSE));
        }
    }

    Entry4UIReturn($errno, $errstr, $retError, $additional);
}

function e4s_AddLineToEmail($line ,$errNo = 0):bool{
	$return = false;
	if (strpos($line, E4S_NS) > 0) {
		$return = true;
	}
	if (strpos($line, E4S_ENTRY_NS) > 0) {
		$return = true;
	}
	if (strpos($line, 'blankslate-child/functions.php') > 0) {
		$return = true;
	}
	return $return;
}
function Entry4Error($errno, $errstr, $errfile, $errline) {
    $conn = $GLOBALS['conn'];
    $URL = $_SERVER['REQUEST_URI'];
    $trace = '';
    if (isAdminUser()) {
        $trace = e4s_getDataAsType(debug_backtrace(), E4S_OPTIONS_STRING);
        $trace = str_replace('Iloveathletics!', '!!PWD!!', $trace);
    }
    $Entry4error = '
        "errFile": "' . $errfile . '",
        "errLine": "' . $errline . '",
        "sqlErrno": ' . mysqli_errno($conn) . ',
        "sqlError": "' . mysqli_error($conn) . '",
        "sqlInfo": " ' . mysqli_info($conn) . '",
        "user":' . e4s_getUserID() . ',
        "domain": "' . E4S_CURRENT_DOMAIN . '",
        "ip": "' . e4s_getUID() . '",
        "url": "' . $URL . '",
        "trace": "' . $trace . '"';

    if (e4s_isLiveDomain()) {
        error_log($Entry4error, 1, E4S_ERROR_EMAIL, '');
    }

    Entry4UIError($errno, $errstr, 500, $Entry4error);
}

function e4s_getDebugData() {
    $output = '';
    $outputSep = '';
    $debug = $GLOBALS[E4S_DEBUG];
    if (is_null($debug) or empty($debug)) {
        return $output;
    }
    $output = ',"debug":[';
    foreach ($debug as $key => $value) {
        $output .= $outputSep . '{';
        $key = explode(E4S_DEBUG_RND, $key);
        $key = $key[0];
        if (gettype($value) === 'array' || gettype($value) === 'object') {
            $output .= '"' . $key . '":' . $GLOBALS['debug'][] = json_encode($value, JSON_NUMERIC_CHECK);
        } else {
            $output .= '"' . $key . '":"' . $value . '"';
        }
        $output .= '}';
        $outputSep = ',';
    }
    $output .= ']';
    return $output;
}