<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
function e4s_updateClubComp($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $ccId = checkFieldForXSS($obj, 'ccid:Club Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $userId = checkFieldForXSS($obj, 'userid:User ID' . E4S_CHECKTYPE_NUMERIC);
    // validate comp id;
    e4s_getCompObj($compId,false,true);

    if ( !is_null($userId) ) {
        $userId = (int)$userId;
    }
    if ( is_null($userId) or $userId === E4S_USER_NOT_LOGGED_IN ){
        $userId = e4s_getUserID();
    }
    $ccObj = new clubCompClass($compId, $ccId,false, $userId);
    $compClubs = $ccObj->update();
    Entry4UISuccess($compClubs);
}
function e4s_deleteClubComp($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $userId = checkFieldForXSS($obj, 'userid:User ID' . E4S_CHECKTYPE_NUMERIC);
    // validate comp id;
    e4s_getCompObj($compId,false,true);

    $ccObj = new clubCompClass($compId, 0,false);
    if ( !is_null($userId) ){
        $userId = (int)$userId;
        if ( $userId > E4S_USER_NOT_LOGGED_IN) {
            $ccObj->forUser($userId);
        }
    }
    $compClubs = $ccObj->delete();
    Entry4UISuccess($compClubs);
}
function getInformationForID($id) {
    $userRow = getUserForID($id);
    getInformationForUser($userRow);
}

function getInformationForEmail($email) {
    $userRow = getUserForEmail($email);
    getInformationForUser($userRow);
}

function getInformationForUser($userRow) {
    $userRow['id'] = $userRow['ID'];
    $userObj = e4sUserClass::withId($userRow['id']);
    $clubComps = $userObj->getClubComps();
    $userRow['version'] = e4s_getUserVersion($userRow['id']);
    unset($userRow['ID']);
    unset($userRow['user_pass']);
    $userRow['login'] = $userRow['user_login'];
    unset($userRow['user_login']);
    $userRow['niceName'] = $userRow['user_nicename'];
    unset($userRow['user_nicename']);
    $userRow['email'] = $userRow['user_email'];
    unset($userRow['user_email']);
    $userRow['registered'] = $userRow['user_registered'];
    unset($userRow['user_registered']);
    $userRow['displayName'] = $userRow['display_name'];
    unset($userRow['display_name']);

    unset($userRow['user_url']);
    unset($userRow['user_activation_key']);
    unset($userRow['user_status']);

    $userid = $userRow['id'];

    $useClubs = array();
    $userType = E4S_USERTYPE_NONE;

    $clubs = getClubsForUser($userid);

    if ($clubs[0]['id'] !== 0) {
        foreach ($clubs as $club) {
            $useClub = array();
            $useClub['id'] = $club['id'];
            $useClub['clubName'] = $club['Clubname'];
            $useClub['region'] = $club['Region'];
            $useClub['country'] = $club['Country'];
            $useClub['areaId'] = $club['areaid'];
            $useClubs[] = $useClub;
            $userType = E4S_USERTYPE_CLUB;
        }
    }
    $useAreas = array();

    $areas = getAreasForUser($userid);

    if ($areas[0]['areaid'] !== 0) {
        foreach ($areas as $area) {
            $useArea = array();
            $useArea['id'] = $area['areaid'];
            $useArea['name'] = $area['areaname'];
            $useArea['shortName'] = $area['areashortname'];
            $useArea['parentId'] = $area['areaparentid'];
            $useArea['entityLevel'] = $area['entitylevel'];
            $useArea['entityName'] = $area['entityName'];
            $useAreas[] = $useArea;
            $userType = E4S_USERTYPE_AREA;
        }
    }

    $useAthletes = array();
    if ($userType === E4S_USERTYPE_NONE) {
        // get user athletes
        $sql = 'select a.id, a.firstName, a.surName, a.aocode aocode, a.URN URN, a.dob, a.gender, a.classification, a.schoolid schoolid, a.clubid clubid, a.activeEndDate, c.Clubname club
                from ' . E4S_TABLE_ATHLETE . ' a left join ' . E4S_TABLE_CLUBS . ' c on a.clubid = c.id
                where a.id in ( select athleteid from ' . E4S_TABLE_USERATHLETES . ' where userid = ' . $userid . ')';
        $result = e4s_queryNoLog($sql);
        $userAthletes = $result->fetch_all(MYSQLI_ASSOC);

        foreach ($userAthletes as $userAthlete) {
            $userAthlete['classification'] = (int)$userAthlete['classification'];
            $useAthletes[] = $userAthlete;
        }
    }

    $retObj = new stdClass();
    $retObj->clubs = $useClubs;
    $retObj->clubComps = $clubComps;
    $retObj->areas = $useAreas;
    $retObj->athletes = $useAthletes;
    include_once E4S_FULL_PATH . 'builder/permCRUD.php';

    $perms = e4s_getPermissionsForUser($userid);
    $userRow['permissions'] = $perms;
    $retObj->user = $userRow;
    $allPerms = listAllDefinedPermissions();

    include E4S_FULL_PATH . 'cart/getCart.php';
    $retObj->cart = getCartForUserID($userid);
    $retObj->orders = getOrdersForUserID($userid);

    $creditObj = new e4sCredit($userid);
    $retObj->e4sCredit = $creditObj->getCredit();
    $retObj->e4sCreditAudits = $creditObj->getAudits();

    Entry4UISuccess('
        "data":' . json_encode($retObj, JSON_NUMERIC_CHECK) . ',
        "meta":' . json_encode($allPerms, JSON_NUMERIC_CHECK));
}

function e4s_getUsersInEntities($obj) {
    $model = new stdClass();
    $model->search = checkFieldForXSS($obj, 'search:Search Criteria');
    if ($model->search === '' || $model->search === null) {
        $model->search = '';
    }
    e4s_addStdPageInfo($obj, $model);
}

function e4s_getUsers($search, $extra, $page, $pagesize, $sort, $sortdesc) {
    if ($search === '') {
        Entry4UISuccess('
        "data":""');
    }
    $search = addslashes($search);
    $searchOnlyID = @FALSE;
    if (is_numeric($search)) {
        $searchOnlyID = TRUE;
    }
    $searchOnlyEmail = FALSE;
    if (strpos($search, '@')) {
        $searchOnlyEmail = TRUE;
        if (strpos($search, '<') !== FALSE and strpos($search, '>') !== FALSE) {
            $search = explode('<', $search);
            $search = explode('>', $search[1]);
            $search = $search[0];
        }
    }
    $sql = 'select ID id, user_login login, user_nicename niceName, user_email email,display_name displayName
            from ' . E4S_TABLE_USERS;
    $cont = ' where ';
    if (!$searchOnlyEmail) {
        $sql .= $cont . " id = '" . $search . "'";
        $cont = ' or ';
    }

    if (!$searchOnlyID) {
        $sql .= $cont . " user_email like '%" . $search . "%'";
        $cont = ' or ';
        if (!$searchOnlyEmail) {
//            $sql .= $cont . " user_login like '%" . $search . "%'
//            or    user_nicename like '%" . $search . "%'
//            or    display_name like '%" . $search . "%' ";
            $sql .= $cont . " concat(user_login,user_nicename,display_name) like '%" . $search . "%'";
        }
    }
    if ($extra) {
        if (!$searchOnlyID) {
            $sql .= "
            union
            select u.ID id, u.user_login login, u.user_nicename niceName, if (user_email = pm.meta_value,user_email,concat(user_email,' Order:',pm.meta_value)) email,u.display_name displayName
            FROM    " . E4S_TABLE_POSTMETA . ' pm,
                    ' . E4S_TABLE_ENTRIES . ' e ,
                    ' . E4S_TABLE_USERS . " u
            WHERE   meta_key LIKE '_billing_email'
            and     e.orderid = pm.post_id
            and     u.id = e.userid
            and     (
                        pm.meta_value like '%" . $search . "%'";
            if (!$searchOnlyEmail) {
//                $sql .= " or u.user_login like '%" . $search . "%'
//                or u.user_nicename like '%" . $search . "%'
//                or u.display_name like '%" . $search . "%' ";
                $sql .= " or concat(user_login,user_nicename,display_name) like '%" . $search . "%'";
            }

            $sql .= "
                    )
            union
            select u.ID id, u.user_login login, u.user_nicename niceName, if (user_email = pm.meta_value,user_email,concat(user_email,' Order:',pm.meta_value)) email,u.display_name displayName
            FROM    " . E4S_TABLE_POSTMETA . ' pm,
                    ' . E4S_TABLE_EVENTTEAMENTRIES . ' e ,
                    ' . E4S_TABLE_USERS . " u
            WHERE   meta_key LIKE '_billing_email'
            and     e.orderid = pm.post_id
            and     u.id = e.userid
            and     (
                        pm.meta_value like '%" . $search . "%'";

            if (!$searchOnlyEmail) {
//                $sql .= "or u.user_login like '%" . $search . "%'
//                or u.user_nicename like '%" . $search . "%'
//                or u.display_name like '%" . $search . "%' ";
                $sql .= " or concat(user_login,user_nicename,display_name) like '%" . $search . "%'";
            }
            $sql .= ' ) ';
        }
    }
    $sqlLimit = '';
    if (!isset($pagesize)) {
        $pagesize = 25;
    }
    if (is_null($pagesize)) {
        $pagesize = 25;
    }
    if (!isset($page)) {
        $pagesize = 1;
    }
    if (is_null($page)) {
        $pagesize = 1;
    }
    $pagesize = $pagesize * 2;
    if ($pagesize !== 0) {
        $sqlLimit = ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
    }
    $sql .= $sqlLimit;

    $result = e4s_queryNoLog($sql);
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    Entry4UISuccess($rows);
}

function getUserForID($key) {
    $wildcard = FALSE;
    if (strpos($key, '&lt;')) {
//        Email in format name <email.address>
        $key = explode('&lt;', $key);
        $key = explode('&gt;', $key[1]);
        $key = $key[0];
    }
    $email = TRUE;
    if (strpos($key, '@') === FALSE) {
        $email = FALSE;
    }
    if (strpos($key, '%') !== FALSE) {
        $wildcard = TRUE;
        $sql = 'select *
            from ' . E4S_TABLE_USERS . "
            where user_email like '" . $key . "'";
        if (!$email) {
            $sql .= "or user_login like '" . $key . "'";
        }
    } else {
        $sql = 'select *
            from ' . E4S_TABLE_USERS . '
            where ';

        if ($email) {
            $sql .= "user_email = '" . $key . "'";
        } else {
            if (is_numeric($key)) {
                $sql .= "id = '" . $key . "'";
            } else {
                $sql .= "user_login = '" . $key . "'";
            }
        }
    }

    $result = e4s_queryNoLog($sql);

    if ($result->num_rows !== 1) {
        if ($wildcard and $result->num_rows > 1) {
            Entry4UIError(9005, 'Multiple records found. Please enter enough to get 1 record.', 400, '');
        } else {
            e4s_addDebug('SQL : ' . $sql);
            Entry4UIError(9006, 'Invalid User Reference.(' . $result->num_rows . ')', 400, '');
        }
    }

    return $result->fetch_assoc();
}

function getUserForEmail($key) {
    return getUserForID($key);
}

function removeUserClub($userid, $clubid, $exit = TRUE) {
    removeUserFromEntity($userid, $clubid, E4S_CLUB_ID, $exit);
}

function removeUserArea($userid, $areaid) {
    removeUserFromEntity($userid, $areaid, E4S_AREA_ID);
}

function removeUserFromEntity($userid, $id, $usertype, $exit = TRUE) {
    $sql = 'select umeta_id
            from ' . E4S_TABLE_USERMETA . '
            where user_id = ' . $userid . "
            and   meta_key = '" . $usertype . "'
            and   meta_value = '" . $id . "'";
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(9007, 'Invalid Entity passed to delete.', 400, '');
    }
    $row = $result->fetch_assoc();
    $deleteSql = 'delete from ' . E4S_TABLE_USERMETA . '
                  where umeta_id = ' . $row['umeta_id'];
    e4s_queryNoLog($deleteSql);
    if ($exit) {
        Entry4UISuccess('');
    }
}

function addUserToAreaWithError($userid, $areaid) {
    return addUserToEntityWithError($userid, $areaid, E4S_AREA_ID, TRUE);
}

function addUserToClubWithError($userid, $clubid) {
    return addUserToEntityWithError($userid, $clubid, E4S_CLUB_ID, TRUE);
}

function addUserToArea($userid, $areaid) {
    return addUserToEntityWithError($userid, $areaid, E4S_AREA_ID, FALSE);
}

function addUserToClub($userid, $clubid, $failSilent = FALSE) {
    return addUserToEntityWithError($userid, $clubid, E4S_CLUB_ID, $failSilent);
}

function addUserToEntity($userid, $id, $type) {
    return addUserToEntityWithError($userid, $id, $type, FALSE);
}

function e4s_getUserEntitiy($userId, $value, $type = E4S_CLUB_ID) {
    $sql = 'select *
            from ' . E4S_TABLE_USERMETA . '
            where user_id = ' . $userId . "
            and   meta_key = '" . $type . "'
            and   meta_value = '" . $value . "'";
    return e4s_queryNoLog($sql);
}

function addUserToEntityWithError($userId, $id, $type, $failSilent) {
    // first check its not already there
    $result = e4s_getUserEntitiy($userId, $id, $type);
    if ($result->num_rows > 0) {
        if ($failSilent) {
            return FALSE;
        }
        Entry4UIError(9008, 'User authority already exists.', 400, '');
    }
    insertUserType($userId, $id, $type);
    return TRUE;
}

function insertUserType($userid, $id, $type) {
    $sql = 'insert into ' . E4S_TABLE_USERMETA . ' (user_id, meta_key, meta_value)
            values (' . $userid . ",'" . $type . "','" . $id . "')";
    $result = e4s_queryNoLog($sql);
}

function e4s_switchUser($obj, $setPassword = FALSE) {
    if (!isE4SUser()) {
        Entry4UIError(9009, 'Unauthorised access to set password', 400, '');
    }
    $email = checkFieldForXSS($obj, 'email:Email Address');

    $sql = 'select * from ' . E4S_TABLE_USERS . "
        where user_email = '" . $email . "'";
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        exit('<br>Email not found');
    }
    $userObj = $result->fetch_object(E4S_USER_OBJ);
    if (!$setPassword) {
        e4s_switchToUser($userObj);
    } else {
        e4s_setUserPassword($userObj);
    }
}

function e4s_switchToUser($obj) {
    header('Location: /wp-admin/?impersonate=' . $obj->id);
    exit();
}

function e4s_setUserPassword($obj) {
    define('DEFAULT_PASSWORD', md5(E4S_DEFAULT_PASSWORD));

    $sql = 'update ' . E4S_TABLE_USERS . '
        set user_url = user_pass
        where id = ' . $obj->id . "
        and user_pass != '" . DEFAULT_PASSWORD . "'";
    e4s_queryNoLog($sql);

    $sql = 'update ' . E4S_TABLE_USERS . "
        set user_pass = '" . DEFAULT_PASSWORD . "'
        where id = " . $obj->id;
    e4s_queryNoLog($sql);

    $sql = 'select *
           from ' . E4S_TABLE_USERS . "
           where user_pass = '" . DEFAULT_PASSWORD . "'";
    $result = e4s_queryNoLog($sql);
    $newline = '<br>';
    echo 'You are setting user ' . $obj->userEmail . ' password to ' . E4S_DEFAULT_PASSWORD;
    echo $newline . $newline . 'Users with default password' . $newline;

    $target = '';
    if ($result->num_rows > 1) {
        $target = ' target="_blank" ';
    }

    while ($row = $result->fetch_object(E4S_USER_OBJ)) {
        echo $newline . 'Reset <a ' . $target . "href=\"/wp-json/e4s/v5/admin/restorepwd?email=" . $row->userEmail . "\">{$row->userEmail}</a>";
    }
}

function e4s_restoreUserPassword($email) {
    if (!isE4SUser()) {
        Entry4UIError(9010, 'Unauthorised access to set password', 400, '');
    }
    echo 'You are resetting user ' . $email;
    $sql = 'select * from ' . E4S_TABLE_USERS . "
        where user_email = '" . $email . "'";
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        exit('<br>Email not found');
    }
    $userRow = $result->fetch_assoc();
    $sql = 'update ' . E4S_TABLE_USERS . "
        set user_pass = user_url,
            user_url = ''
        where id = " . $userRow['ID'];
    e4s_queryNoLog($sql);

    echo '<br>Password set back to users password';
}

function e4s_deleteUserAthlete($obj) {
    $athleteid = checkFieldForXSS($obj, 'id:Athletes ID');
    $userid = checkFieldForXSS($obj, 'userid:User ID');
    if (is_null($userid) or $userid === E4S_USER_NOT_LOGGED_IN or $userid === '') {
        $userid = e4s_getUserID();
    } else {
        if (!isE4SUser()) {
            Entry4UIError(9402, 'You dont have the permission to remove athlete links for other users.', 200, '');
        }
    }
    $select = 'select * ';
    $where = 'from ' . E4S_TABLE_USERATHLETES . "
            where userid = {$userid}
            and   athleteid = {$athleteid}";

    $res = e4s_queryNoLog($select . $where);
    if ($res === FALSE or $res->num_rows < 1) {
        Entry4UIError(4002, "No athlete link found to user id {$userid}", 200, '');
    }
    e4s_queryNoLog('delete ' . $where);
    Entry4UISuccess('');
}

function e4s_updateCredit($obj, $update = FALSE) {
    if (!isE4SUser()) {
        Entry4UIError(9011, 'Can not set Credit', 200, '');
    }
    $userid = checkFieldForXSS($obj, 'userid:User ID');
    $value = checkFieldForXSS($obj, 'value:Value');
    $reason = checkFieldForXSS($obj, 'reason:Reason for Credit');
    if (is_null($reason) or $reason === '') {
        $reason = 'No reason given';
    }

    $creditObj = new e4sCredit($userid);
    if ($update) {
        $creditObj->addToCredit($value, $reason);
    } else {
        $creditObj->setCredit($value, $reason);
    }

    Entry4UISuccess();
}