<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_refund($obj) {

    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (is_null($compid)) {
        $compid = 0;
    }
    $orderIds = checkFieldForXSS($obj, 'orderid:Order IDs' . E4S_CHECKTYPE_NUMERIC_ARRAY);

    $productid = checkFieldForXSS($obj, 'productid:Product ID');
    if (is_null($productid)) {
        $productid = 0;
    }
    $params = $obj->get_params('JSON');
    $eventids = array();
    if (array_key_exists('eventids', $params)) {
        $eventids = $params['eventids'];
    }
    $ignoreOrders = array();
    if (array_key_exists('ignoreorders', $params)) {
        $ignoreOrders = $params['ignoreorders'];
    }
    if ($compid === 0 and $orderIds[0] === 0 and $productid === 0) {
        Entry4UIError(8001, 'Please specify a competition, a product id or at least 1 order number', 200, '');
    }
    $productType = checkFieldForXSS($obj, 'producttype:Product Type');
    if (is_null($productType)) {
        $productType = '';
    }
    $value = checkFieldForXSS($obj, 'value:Refund Value');
    if (is_null($value)) {
        $value = 0;
    }
    $pricePaid = checkFieldForXSS($obj, 'price:Price Paid');
    if (is_null($pricePaid)) {
        $pricePaid = 0;
    }
    $warnLevel = checkFieldForXSS($obj, 'warn:Warn level');
    if (is_null($warnLevel)) {
        $warnLevel = E4S_WARN_ALL;
    }
    $reason = checkFieldForXSS($obj, 'reason:Refund Reason');
    if ($reason === '' or is_null($reason)) {
        Entry4UIError(8002, 'Please pass a reason for the refund', 200, '');
    }
    $emailTxt = checkFieldForXSS($obj, 'text:Email Text');
    if ($emailTxt === '' or is_null($emailTxt)) {
        $emailTxt = '';
    }
    $perLine = checkFieldForXSS($obj, 'perline:Refund value per line');
    if (is_null($perLine)) {
        $perLine = FALSE;
    }
    $testMode = checkFieldForXSS($obj, 'test:Test Mode');
    if (is_null($testMode)) {
        $testMode = false;
    }
    $removeEntry = checkFieldForXSS($obj, 'removeEntry:Remove Entries');
    if (is_null($removeEntry)) {
        $removeEntry = TRUE;
    }
    $refundE4SFee = checkFieldForXSS($obj, 'refundE4SFee:Refund E4S Fee');
    if (is_null($refundE4SFee)) {
        $refundE4SFee = FALSE;
    }
    $refundStripeFee = checkFieldForXSS($obj, 'refundStripeFee:Refund Stripe Fee');
    if (is_null($refundStripeFee)) {
        $refundStripeFee = FALSE;
    }
    $email = checkFieldForXSS($obj, 'email:Email Customer');
    if (is_null($email)) {
        $email = TRUE;
    }
    $multiple = checkFieldForXSS($obj, 'multi:Allow Multiple Refunds');
    if (is_null($multiple)) {
        $multiple = FALSE;
    }
    $ignorePaid = checkFieldForXSS($obj, 'ignorePaid:Refund even if marked not Paid');
    if (is_null($ignorePaid)) {
        $ignorePaid = FALSE;
    }
    $credit = checkFieldForXSS($obj, 'credit:Give a credit rather than a refund');
    if (is_null($credit)) {
        $credit = FALSE;
    }
    $e4sRefundObj = new e4sRefund($compid, $orderIds, $productid);
    $parms = new stdClass();
    $parms->reason = addslashes($reason);
    $parms->productType = $productType;
    $parms->refundAmount = $value;
    $parms->perLine = $perLine;
    $parms->pricePaid = $pricePaid;
    $parms->ignoreOrders = $ignoreOrders;
    $parms->eventids = $eventids;
    $parms->credit = $credit;
    $parms->testMode = $testMode;
    $parms->removeEntry = $removeEntry;
    $parms->ignorePaid = $ignorePaid;
    $parms->refundE4SFee = $refundE4SFee;
    $parms->refundStripeFee = $refundStripeFee;
    $parms->email = $email;
    $parms->emailTxt = $emailTxt;
    $parms->multiple = $multiple;
    $parms->warnLevel = $warnLevel;

    $e4sRefundObj->performRefund($parms);
    Entry4UISuccess('');
}