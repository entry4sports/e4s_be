<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_process($obj) {
    $action = checkFieldForXSS($obj, 'action:Action');

    if (strtolower($action) !== 'setuat' and strtolower($action) !== 'awardcheck') {
        if (e4s_getUserID() <= E4S_USER_NOT_LOGGED_IN) {
            Entry4UIError(9003, 'Please Login');
        }
    }
    switch (strtolower($action)) {
        case 'credits':
            e4s_credits($obj);
            break;
        case 'clubcompteambibs':
            e4s_clubCompTeamBibs($obj);
            break;
        case 'checkoptions':
            e4s_checkOptions($obj);
            break;
        case 'teamfix':
            e4s_teamFix($obj);
            break;
        case 'archive':
            e4s_archive($obj);
            break;
        case 'removenull':
            e4s_removeNulls($obj);
            break;
        case 'deleteunpaid':
            e4s_deleteUnpaids();
            break;
        case 'cleartrial':
            $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
            e4s_clearTrialInfo($compId);
            break;
        case 'esaa23':
            include_once E4S_FULL_PATH . 'import/importESAA2024.php';
            $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
            if ( is_null($compId)){
                Entry4UIError(3500, 'No competition passed');
            }
            e4s_importESAA23($compId);
            break;
	    case 'esaa24':
		    include_once E4S_FULL_PATH . 'import/importESAA2024.php';
		    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
		    if ( is_null($compId)){
			    Entry4UIError(3500, 'No competition passed');
		    }
		    e4s_importESAA24($compId);
		    break;
        case 'esaa24printers':
	        esaa24Printers();
            break;
        case 'dedup':
            e4s_deDup($obj);
            Entry4UISuccess();
            break;
        case 'aaisync';
            include_once E4S_FULL_PATH . 'admin/aai_sync.php';
            e4s_AAISync();
            break;
        case 'correctceid':
            e4s_correctCEidsForComp($obj);
            break;
        case 'complist':
            e4s_compList($obj);
            break;
        case 'newhp':
            e4s_newDraftHomePage($obj);
            break;
        case 'sethp':
            e4s_setHomePage($obj);
            break;
        case 'showhp':
            e4s_showHomePages($obj);
            break;
        case 'reprocesslog':
            include_once E4S_FULL_PATH . 'import/RegToAthlete.php';
            reprocessRegFromLog();
            break;
        case 'repayment':
            $processCount = repayment($obj);
            Entry4UISuccess($processCount);
            break;
        case 'aaifile':
        case 'aaidata':
        case 'aaifinance':
            Ireland_FULL_Finance_Report($obj);
            break;
        case 'aaidata2':
            e4s_FinanceReport($obj);
            break;
        case 'checkagegroups':
            header('Content-Type: text/html; charset=utf-8');
            Entry4UISuccess(checkCompEntryAgeGroups($obj));
            break;
        case 'setcontact':
            setCompContact($obj);
            break;
        case 'setuat':
            setUat($obj);
            break;
        default:
            Entry4UIError(1000, $action . ' Not allowed');
    }
    Entry4UISuccess();
}

function esaa24Printers(){
	include_once E4S_FULL_PATH . 'classes/excelClass.php';
    e4s_webPageHeader();
	$sql = 'select e.entryId, startdate, starttime, egName eventName, e.athleteId, firstName, surname, clubName, teamBibNo bibNo, heatno, laneno
from ' . E4S_TABLE_ENTRYINFO . ' e,
     ' . E4S_TABLE_SEEDING . ' se
where compid = 617
      and se.athleteid = e.athleteid
      and se.eventgroupid = e.egId
  union
  select e.id, 
      date_format(eg.startdate,\'%Y-%m-%d\') startdate,
      date_format(eg.startdate,\'%H.%i\') starttime, 
      eg.name eventName, 
      e.id, 
      \'team\' firstName,
      e.name surName, 
      c.Clubname, 
      bibno teamBibNo, 
      s.heatno, 
      s.laneno
from ' . E4S_TABLE_EVENTTEAMENTRIES . ' e left join ' . E4S_TABLE_COMPEVENTS .' ce on (e.ceid = ce.ID) left join ' . E4S_TABLE_EVENTGROUPS . ' eg  on (ce.maxGroup = eg.id) left join ' . E4S_TABLE_SEEDING . ' s on (s.eventgroupid = eg.id
    and s.athleteid = e.id),
    ' . E4S_TABLE_CLUBS . ' c
where c.id = e.entityid
  and eg.name != \'Reserves\'
  and eg.compid = 617
order by startdate, starttime, eventName, heatno, laneno';

    $result = e4s_queryNoLog($sql);

    $arr = [];
    $arr[] = ['startdate', 'starttime','eventName', 'heatno', 'laneno', 'bibNo', 'firstName', 'surname', 'county' ] ;
    while ($obj = $result->fetch_object()){
	    $arr[] = [$obj->startdate , $obj->starttime , $obj->eventName , $obj->heatno , $obj->laneno , $obj->bibNo ,$obj->firstName , $obj->surName , $obj->clubName];
    }

	$xlsx = excelClass::fromArray($arr);
	$xlsx->downloadAs("ESAA_" . date('Y-m-d H:i') . ".xlsx");
    exit();
}
function e4s_credits($obj){
	$date = checkFieldForXSS($obj, 'date:Credit To Date');
    $sql = "
        select c1.*
        from " . E4S_TABLE_CREDIT . " c1
        where c1.date = (
            select max(c2.date)
            from " . E4S_TABLE_CREDIT . " c2
            where c2.userid = c1.userid
            )
        and c1.date < '" . $date . "'
        and userid > 1
        order by userid
    ";

    $result = e4s_queryNoLog($sql);
    echo "id, currentValue,userId, reason, date<br>";
    while ( $rowObj = $result->fetch_object() ){
        echo $rowObj->id . "," . $rowObj->newValue . ","  . $rowObj->userId . "," . $rowObj->reason . ","  . $rowObj->date .  "<br>";
    }
    exit;
}
function e4s_clubCompTeamBibs($obj){

	$compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $sql = "select clubid, bibnos
            from " . E4S_TABLE_CLUBCOMP . "
            where compid = " . $compId;
    $result = e4s_queryNoLog($sql);

    $clubs = [];
    while($obj = $result->fetch_object()){
        $clubs[$obj->clubid] = explode(",", $obj->bibnos)[0];
    }
    var_dump("here");
    $sql = "
        select ce.id
        from " . E4S_TABLE_EVENTGROUPS . " eg,
             " . E4S_TABLE_COMPEVENTS . " ce,
             " . E4S_TABLE_EVENTTEAMENTRIES . " ete
        where eg.compid = 617
        and ce.maxgroup = eg.id
        and eg.options like '%isTeamEvent\":true%'
        and eg.name != 'Reserves'
    ";
    $result = e4s_queryNoLog($sql);
    var_dump($sql);
    exit;
    while($obj = $result->fetch_object()){
        $sql = "select id, entityId
                from " . E4S_TABLE_EVENTTEAMENTRIES . "
                where ceid = " . $obj->id . "
                and bibno is null";
        $teamResult = e4s_queryNoLog($sql);
var_dump($sql);
exit;
        while($teamObj = $teamResult->fetch_object()){
            $teamBib = $clubs[$teamObj->entityId];
            $sql = "update " . E4S_TABLE_EVENTTEAMENTRIES . "
                    set bibNo = " . $teamBib . "
                    where id = " . $teamObj->id;
            e4s_queryNoLog($sql);
	        var_dump($sql);
        }
        exit;
    }
}
function  e4s_checkOptions($obj){
	$compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $compObj = e4s_getCompObj($compId);
    $ceObjs = $compObj->getCeObjs();
    foreach($ceObjs as $ceObj){
        $eg = $compObj->getEventGroupByEgId($ceObj->egId);
        echo "Checking " . $eg->name . "/"  . $ceObj->id . "<br>";
        echo "CE Options : " . json_encode($ceObj->options) . "<br>";
        echo "EG Options : " . json_encode($eg->options) . "<br>";
    }
}
function e4s_getUsersClub($userId){
    $sql = "select meta_value, clubname
            from " . E4S_TABLE_USERMETA . " um,
                 " . E4S_TABLE_CLUBS . " c
            where user_id = " . $userId . "
            and meta_key = '" . E4S_CLUB_ID . "'
            and c.id = meta_value";
    $result = e4s_queryNoLog($sql);
    if ( $result->num_rows !== 1 ){
        Entry4UIError(2347, 'Incorrect number of rows returned : ' . $userId . ". " .  $sql  );
    }
    $obj = $result->fetch_object();
    return $obj->meta_value . ":" . $obj->clubname;
}
function e4s_teamFix($obj){
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $updateDB = checkFieldForXSS($obj, 'update:update Comp');
    if ( $updateDB === "1" ) {
        $updateDB = true;
        echo "Updating Database<br>";
    }else{
	    $updateDB = false;
	    echo "Not Updating Database<br>";
    }

    if ( is_null($compId) ){
        Entry4UIError(2345, 'Must pass a competition id');
    }
    // get all ceObjs
    $compObj = e4s_getCompObj($compId);
    $ceObjs = $compObj->getCeObjs();

    $userClubIds = [];
    foreach($ceObjs as $ceObj){
        $ceId = $ceObj->id;
        $eg = $compObj->getEventGroupByEgId($ceObj->egId);
        if (!isset($eg->options->isTeamEvent) or $eg->options->isTeamEvent === false){
            echo 'Skipping ' . $eg->name . "<br>";
            continue;
        }
        $eventName = $eg->name;
        echo '<br>Processing ' . $eventName . "<br>";
        $sql = "select *
                from " . E4S_TABLE_WCORDERITEMS . "
                where order_item_name like '" . $ceId . " :%'";
        $result = e4s_queryNoLog($sql);
        while ( $itemObj = $result->fetch_object()){
            $itemId = $itemObj->order_item_id;
            $orderId = $itemObj->order_id;
            echo 'Processing itemId:' . $itemId . ' Order:' . $orderId . "<br>";
            $prodSql = "select meta_value productId
                        from " . E4S_TABLE_WCORDERITEMMETA . "
                        where order_item_id = " . $itemId . "
                        and meta_key  = '_product_id'";
            $prodResult = e4s_queryNoLog($prodSql);

            $prodObj = $prodResult->fetch_object();
	        $productId = $prodObj->productId;

            $sql = "select meta_value userId
                    from " . E4S_TABLE_POSTMETA . "
                    where post_id = " . $orderId . "
                    and meta_key = '_customer_user'";
            $userResult = e4s_queryNoLog($sql);
            $userObj = $userResult->fetch_object();

            $userId = $userObj->userId;
            // we now have ceId, orderId, userId, clubId and product ID
            // check we have the team in E4S
            if ( $userId === 0 or $productId === 0 ){
                Entry4UIError(2347, 'Incorrect values returned Item :' . $itemId . ' Order:' . $orderId);
            }
            $athleteSql = "";
            $sql = "select *
                    from " . E4S_TABLE_EVENTTEAMENTRIES . "
                    where ceid = " . $ceId . "
                    and userid = " . $userId . "
                    and productid = " . $productId;
            $teamResult = e4s_queryNoLog($sql);
            if ( $teamResult->num_rows === 0 ){
	            // check or get clubId
	            if ( !array_key_exists($userId, $userClubIds) ){
		            $userClubIds[$userId] = explode(":", e4s_getUsersClub($userId));
	            }
                // add the team
                $teamName = addslashes($userClubIds[$userId][1] . " " . $eventName);
                $sql = "insert into " . E4S_TABLE_EVENTTEAMENTRIES . "
                        (ceid,name, userid, productid, orderid, paid,price, entityLevel, entityid, options)
                        values
                        (" . $ceId . ",'" . $teamName . "'," . $userId . "," . $productId . "," . $orderId . "," . E4S_ENTRY_PAID . ",0.00,1," . $userClubIds[$userId][0] . ", '{\"reason\":\"Team Fix\"}')";
	            echo $sql . "<br>";
                if ( $updateDB ) {
	                e4s_query("TeamFix" . E4S_SQL_DELIM .  $sql);
                    $teamId = e4s_getLastID();
                }else {
	                $teamId = 1;
                }
                echo "Inserted<b>";
                // try and get athletes
                $logSql = "select distinct(payload)
                           from " . E4S_TABLE_LOG . "
                           where userid = " . $userId . "
                           and info = 'URI:POST:/compeventteam/'
                           and payload like '{\"id\":0,\"ceid\":" . $ceId . ",%'";
                $logResult = e4s_queryNoLog($logSql);
                if ( $logResult->num_rows > 0 and $teamId > 0 ) {
                    $athletesArr = [];
	                while($logObj = $logResult->fetch_object()){
		                $payload = json_decode($logObj->payload);
		                $athletes = $payload->athletes;
		                $athleteEncoded = json_encode($athletes);
                        $athletesArr[$athleteEncoded] = $athleteEncoded;
	                }
                    if ( sizeof($athletesArr) === 1) {
                        $athletes   = $payload->athletes;
                        $athleteSql = "insert into " . E4S_TABLE_EVENTTEAMATHLETE . " (teamentryid, pos,athleteid)
                                                       values ";
                        $pos        = 1;
                        $sep        = "";
                        foreach ( $athletes as $athlete ) {
                            $athleteSql .= $sep . "(" . $teamId . "," . $pos ++ . "," . $athlete->id . ")";
                            $sep        = ",";
                        }
                        echo $athleteSql . "<br>";
                        if ( $updateDB ) {
                            e4s_query( "TeamFix" . E4S_SQL_DELIM . $athleteSql );
                        }
                    }else{
                        echo '!!!!! More than one set of athletes for ' . $teamName . "<br>";
                        foreach($athletesArr as $athletes){
                            echo "> Athletes : " . $athletes . "<br>";
                        }
                    }
                }else{
                    echo '!!!!! No Athletes created for ' . $teamName . "<br>";
                    if ( $logResult->num_rows > 1){
                        while($logObj = $logResult->fetch_object()){
	                        $payload = json_decode($logObj->payload);
	                        $athletes = $payload->athletes;
                            echo "> Athletes : " . json_encode($athletes) . "<br>";
                        }
                    }
                }
            }else{
                $teamObj = $teamResult->fetch_object();
                echo 'Team already exists UserId:' . $userId . ' TeamId:' . $teamObj->id . " ProductId:" . $productId . "<br>";
            }
            echo '<br>';
//            if ( $athleteSql !== "" ){
//                exit("Done");
//            }
        }
    }
}
function e4s_archive($obj){
    require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/plugins/woo-archive-e4s/woocommerce-archive-e4s.php';
	e4s_perform_archive($obj);
}
function e4s_removeNulls($obj){
    if ( !isE4SUser()){
        Entry4UIError(8738,'Unautorised request');
    }
    $table = checkFieldForXSS($obj, 'table:Table Name');
    if ( is_null($table) ){
        Entry4UIError(2345, 'Must pass a table');
    }
    $column = checkFieldForXSS($obj, 'column:Column Name');
    if ( is_null($column) ){
        Entry4UIError(2346, 'Must pass a column');
    }

    $sql = 'update ' . $table . '
            set ' . $column . " = trim('\0' from " . $column . ')';
    e4s_queryNoLog($sql);

    Entry4UISuccess();
}
function e4s_deleteUnPaid(){
    $sql = '
    delete from Entry4_postmeta
where post_id in (select e.variationID
                  from Entry4_Competition c,
                       Entry4_CompEvents ce,
                       Entry4_Entries e
                  where date < CURRENT_DATE
                    and ce.CompID = c.id
                    and e.compEventID = ce.id
                    and e.paid = 0);

delete from Entry4_posts
where id in (select e.variationID
                  from Entry4_Competition c,
                       Entry4_CompEvents ce,
                       Entry4_Entries e
                  where date < CURRENT_DATE
                    and ce.CompID = c.id
                    and e.compEventID = ce.id
                    and e.paid = 0);

delete from Entry4_Entries
where variationID not in (select id from Entry4_posts);
    ';
}
function e4s_deDup($obj){
    $firstName = checkFieldForXSS($obj, 'firstname:First Name');
    if ( is_null($firstName) ){
        $firstName = '';
    }
    $surName = checkFieldForXSS($obj, 'surname:Surname');
    if ( is_null($surName) ){
        $surName = '';
    }
    $dob = checkFieldForXSS($obj, 'dob:Date Of Birth');
    if ( is_null($surName) ){
        $surName = '';
    }
    $URN = checkFieldForXSS($obj, 'urn:URN');
    if ( is_null($URN) ){
        $URN = '';
    }
    $testMode = checkFieldForXSS($obj, 'test:Test Mode');

    if ( is_null($testMode) ){
        $testMode = true;
    }elseif ($testMode or $testMode === 'true' or $testMode === '1') {
        $testMode = true;
    }else{
        $testMode = false;
    }

    include_once E4S_FULL_PATH . 'classes/deDupClass.php';
    $obj = new deDupClass(1, $testMode, $firstName, $surName, $URN);
}
function e4s_correctCEidsForComp($obj){
    $compId = checkFieldForXSS($obj, 'compid:Competition id');
    $compObj = e4s_getCompObj($compId);
    $incorrectSQL = '
        select
            e.id entryId,
            ce.id compEventId,
            ce.ageGroupId,
            ce.maxGroup egId
        from ' . E4S_TABLE_ENTRIES . ' e,
             ' . E4S_TABLE_COMPEVENTS . ' ce,
             ' . E4S_TABLE_ATHLETE . ' a,
             ' . E4S_TABLE_EVENTS . ' ev
        where ce.compid = ' . $compId . '
        and e.compEventID = ce.ID
        and ce.EventID = ev.id
        and e.athleteid = a.id
        and ev.Gender != a.gender
    ';
    $incorrectR = e4s_queryNoLog($incorrectSQL);
    while ($incRecord = $incorrectR->fetch_object()){
        $incRecord->entryId = (int)$incRecord->entryId;
        $incRecord->ageGroupId = (int)$incRecord->ageGroupId;
        $incRecord->egId = (int)$incRecord->egId;
        $incRecord->compEventId = (int)$incRecord->compEventId;
        $newCeId = 0;
        $ceObjs = $compObj->getCeObjs();
        foreach($ceObjs as $ceObj){
            if ($ceObj->egId === $incRecord->egId){
                if ($ceObj->ageGroupId === $incRecord->ageGroupId){
                    if ($ceObj->id !== $incRecord->compEventId){
                        $newCeId = $ceObj->id;
                        break;
                    }
                }
            }
        }
        if ( $newCeId !== 0 ){
            echo 'Moved ' . $incRecord->entryId . ' from ' . $incRecord->compEventId . ' to ' . $newCeId . "<br>";
            $update = 'update ' . E4S_TABLE_ENTRIES . '
                       set compEventid = ' . $newCeId . '
                       where id = ' . $incRecord->entryId;
            e4s_queryNoLog($update);
        }
    }
    exit('done');
}
function e4s_recalculateAgeGroups($obj){
    include_once E4S_FULL_PATH . 'entries/entries.php';
    e4s_recalculateCompAgeGroups($obj);
}

function e4s_compList($obj){
    $sql = '
    select cache.cache cache
     ,c.id compId
     ,c.name
     ,c.date
     ,c.active
    ,cc.club
from ' . E4S_TABLE_CACHE . ' cache,
     ' . E4S_TABLE_COMPETITON . ' c,
     ' . E4S_TABLE_COMPCLUB . ' cc
where c.id = cache.compid
and c.compclubid = cc.id
order by c.date
    ';
    $newline = "<br>";
    $sep = ',';
    $start = '"';
    $end = '"';
    $divider = $start . $sep . $end;
    $result = e4s_queryNoLog($sql);
    echo 'comp#' . $sep . 'active' . $sep . 'name' . $sep . 'date' . $sep . 'club' . $sep . 'indiv entries' . $sep . 'team entries' . $newline;
    while ($row = $result->fetch_object() ){
        echo $row->compId;
        echo $sep;
        if ( (int)$row->active === 1 ){
            echo 'True';
        }else{
            echo 'False';
        }
        echo $sep;
        echo $start . $row->name . $end;
        echo $sep;
        echo $start . $row->date . $end;
        echo $sep;
        echo $start . $row->club . $end;
        echo $sep;
        $counts = e4s_getOptionsAsObj($row->cache);
        echo $counts->indivCnt ;
        echo $sep;
        echo $counts->teamCnt;
        echo $newline;
    }
}
function e4s_setHomePage($obj) {
    if (!isE4SUser()) {
        Entry4UIError(7455, 'Sorry. You are not authorised');
    }
    include_once E4S_FULL_PATH . 'classes/e4sHomePage.php';
    $hpId = checkFieldForXSS($obj, 'hpid:Homepage id');
    if ($hpId === '') {
        Entry4UIError(7458, 'No page id passed');
    }
    $e4sHP = new e4sHomePage();
    $e4sHP->setNewHomepage($hpId);
    ?>
    <script>
        location.href = "/";
    </script>
    <?php
}

function e4s_showHomePages($obj) {
    include_once E4S_FULL_PATH . 'classes/e4sHomePage.php';
    $e4sHP = new e4sHomePage();
    e4s_displayHomePageList($e4sHP);
}

function e4s_displayHomePageList(e4sHomePage $e4sHP) {
    $e4sHP->displayAll();
    exit();
}

function e4s_newDraftHomePage($obj) {
    if (!isE4SUser()) {
        Entry4UIError(7455, 'Sorry. You are not authorised');
    }
    include_once E4S_FULL_PATH . 'classes/e4sHomePage.php';
    $guid = checkFieldForXSS($obj, 'guid:Guid');
    if ($guid === '') {
        Entry4UIError(7458, 'No Guid passed');
    }
    $reason = checkFieldForXSS($obj, 'reason:Reason for new version');
    if ($reason === '') {
        $reason = date('Y-m-d H:i:s') . ' release';
    }
    $e4sHP = new e4sHomePage();
    $e4sHP->createNewVersion($guid, $reason);
    e4s_displayHomePageList($e4sHP);
}

function reprocessRegFromLog() {
    $sql = 'select *
            from ' . E4S_TABLE_LOG . "
            where created > '2022-10-10' and info = 'URI:/private/athlete/registration/' and payload not like '{%'
            order by created";
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows < 1) {
        Entry4UISuccess();
    }
    while ($obj = $result->fetch_object()) {
        $arr = preg_split('~&~', $obj->payload);
        $firstname = '';
        $surname = '';
        $dob = '';
        $regid = '';
        $gender = '';
        $regdate = '';
        $status = '';
        $system = '';
        $club = '';
        $class = '';
        $externId = 0;
        foreach ($arr as $keyValue) {
            $keyValueArr = preg_split('~=~', $keyValue);
            $key = $keyValueArr[0];
//            echo "K:" . $key . " ";
            $value = $keyValueArr[1];
            $value = urldecode($value);
//            echo "V:" . $value . " ";
            switch ($key) {
                case 'firstname':
                    $firstname = $value;
                    break;
                case 'surname':
                    $surname = $value;
                    break;
                case 'dob':
                    $dob = $value;
                    break;
                case 'regid':
                    $regid = $value;
                    break;
                case 'clubId':
                    $externId = (int)$value;
                    break;
                case 'club':
                    $club = $value;
                    break;
                case 'gender':
                    $gender = $value;
                    break;
                case 'regdate':
                    $regdate = $value;
                    break;
                case 'status':
                    $status = $value;
                    break;
                case 'class':
                    $class = $value;
                    break;
                case 'system':
                    $system = $value;
                    break;
            }
        }
        e4s_athleteRegistration($system, $status, $firstname, $surname, $dob, $regid, $gender, $regdate, $club, $externId, 0, $class, FALSE);
    }
    Entry4UISuccess();
}

function setUat($obj) {
    // set wp_config before running....
    $uatdate = checkFieldForXSS($obj, 'date:UAT Date');
    $uatFromDate = checkFieldForXSS($obj, 'fromdate:UAT From Date');
    $sql = 'select *
            from ' . E4S_TABLE_CONFIG;
    $result = e4s_queryNoLog($sql);
    while ($obj = $result->fetch_object(E4S_CONFIG_OBJ)) {
        $options = $obj->options;
        $options->homePage->message = 'UAT ' . $uatdate;

        $sql = 'update ' . E4S_TABLE_CONFIG . "
                set env = 'UAT',
                    options = '" . $obj->getWriteOptionsStr($options) . "'
                where id = " . $obj->id;
        e4s_queryNoLog($sql);
    }
    $sql = 'update ' . E4S_TABLE_OPTIONS . "
            set option_value = 'https://uat.entry4sports.co.uk'
            where option_name in ('siteurl','home' )";
    e4s_queryNoLog($sql);
    $sql = 'update ' . E4S_TABLE_OPTIONS . "
            set option_value = 'UAT Entry4Sports'
            where option_name in ('blogname' )";
    e4s_queryNoLog($sql);
    $sql = 'update ' . E4S_TABLE_OPTIONS . "
            set option_value = 'no'
            where option_name in ('woocommerce_archive_e4s_enabled' )";
    e4s_queryNoLog($sql);
    //update_option('woocommerce_archive_e4s_enabled','no');
    $sql = 'select option_id, option_value
            from ' . E4S_TABLE_OPTIONS . "
            where option_name = 'woocommerce_stripe_settings'";
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(9004, 'No Stripe Options Found');
    }
    $obj = $result->fetch_object();
    $obj->option_value = str_replace('"testmode";s:2:"no"', '"testmode";s:3:"yes"', $obj->option_value);
    $sql = 'update ' . E4S_TABLE_OPTIONS . "
            set option_value = '" . $obj->option_value . "'
            where option_id = " . $obj->option_id;
    e4s_queryNoLog($sql);

	// update the view
	$sql = 'show create view ' . E4S_TABLE_ENTRYINFO;
	$result = e4s_queryNoLog($sql);
	$obj = $result->fetch_assoc();
	$sql = $obj['Create View'];
	$sql = preg_split('~' . E4S_TABLE_ENTRYINFO . '`~', $sql)[1];
	$sql = 'create or replace view ' . E4S_TABLE_ENTRYINFO . ' ' . $sql;
	$sql = str_replace("`E4S`.", "", $sql);
	e4s_queryNoLog($sql);

    // remove sentry from homepage after getting hp from previous release
    e4s_removeSentry($uatFromDate);
    exit;
}

function e4s_removeSentry($uatFromDate = null){
    $hpId = (int)get_option('page_on_front');
    $db = '';
    if ( ! is_null($uatFromDate) ){
        $db .= "E4S_" . $uatFromDate . ".";
    }

    $sql = 'select post_content
            from ' . $db . E4S_TABLE_POSTS . '
            where id = ' . $hpId;

    $result = e4s_queryNoLog($sql);
    if ( $result->num_rows !== 1 ){
        Entry4UIError(9005, 'No homepage found');
    }
    $obj = $result->fetch_object();
    $content = $obj->post_content;
    $content = str_replace('<script src="https://browser.sentry-cdn.com/5.12.1/bundle.min.js" integrity="sha384-y+an4eARFKvjzOivf/Z7JtMJhaN6b+lLQ5oFbBbUwZNNVir39cYtkjW1r6Xjbxg3" crossorigin="anonymous"></script>', '', $content);
    $content = str_replace("<script>Sentry.init({ dsn: 'https://9c74b78f1c544d9389d3474ffe7c3106@sentry.io/1373748' });</script>", '', $content);
    $sql = 'update ' . E4S_TABLE_POSTS . "
            set post_content = '" . $content . "'
            where id = " . $hpId;
    e4s_queryNoLog($sql);
}

function repayment($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (is_null($compId)) {
        $compId = 0;
    }
    $orderId = checkFieldForXSS($obj, 'orderid:Order ID');
    if (is_null($orderId)) {
        $orderId = 0;
    }
    $model = new stdClass();
    $model->compId = $compId;
    $model->orderId = $orderId;
    $repaymentObj = new e4sRepayment($model);
    $repaymentObj->resetPayment();
    Entry4UISuccess();
}

function getNotNull($obj, $param) {
    $prop = checkFieldForXSS($obj, $param);
    $fieldArr = explode(':', $param);
    $fieldName = $fieldArr[0];
    $displayField = $fieldArr[1];
    if (is_null($prop)) {
        Entry4UIError(2150, $displayField . ' is null');
    }
    return $prop;
}

function setCompContact($obj) {
    $update = checkFieldForXSS($obj, 'update:Perform update');
    if (!is_null($update) and $update === '1') {
        $update = TRUE;
    } else {
        $update = FALSE;
    }

    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (is_null($compId)) {
        $compId = 0;
    }
    $orgId = checkFieldForXSS($obj, 'orgid:Organisation ID');
    if (is_null($orgId)) {
        $orgId = 0;
    }
    if ($compId === 0 and $orgId === 0) {
        Entry4UIError(2020, 'Please pass a defining key for this update');
    }
    $contactId = getNotNull($obj, 'contactid:Contact ID');
    if ($contactId < 0) {
        Entry4UIError(2025, 'Please pass a contact id for this update');
    } else {
        $sql = 'select *
                from ' . E4S_TABLE_CONTACTS . '
                where id = ' . $contactId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(2040, 'Please pass a valid contact id for this update');
        }
    }
    $sql = 'select id
                    , options
            from ' . E4S_TABLE_COMPETITON . '
            where ';
    if ($compId !== 0) {
        $sql .= ' id = ' . $compId;
    }
    if ($orgId !== 0) {
        $sql .= ' compclubid = ' . $orgId;
    }

    $result = e4s_queryNoLog($sql);
    while ($obj = $result->fetch_object()) {
        $options = e4s_removeDefaultCompOptions($obj->options);

        if ($update) {
            $contact = new stdClass();
            $contact->id = $contactId;
            $options->contact = $contact;
            $sql = 'update ' . E4S_TABLE_COMPETITON . "
                set options = '" . e4s_getOptionsAsString($options) . "'
                where id = " . $obj->id;
            $updateResult = e4s_queryNoLog($sql);
            echo 'updated ' . $obj->id . "<br>";
        } else {
            echo 'comp : ' . $obj->id . "<br>";
            if (isset($options->contact)) {
                var_dump($options->contact);
            }
        }
    }
    Entry4UISuccess();
}

function checkCompEntryAgeGroups($obj) {
    $compId = getNotNull($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $update = checkFieldForXSS($obj, 'update:Update the record');

    if (is_null($update) or $update === '0' or $update === 'false' or $update === false) {
        $update = false;
    } else {
        $update = true;
    }

    $email = checkFieldForXSS($obj, 'email:Email the athlete');

    if (is_null($email) or $email === '0' or $email === 'false' or $email === false) {
        $email = false;
    } else {
        $email = true;
    }

    $allCompDOBs = getAllCompDOBs($compId);
    $sql = 'select ce.id ceId,
                   ce.eventId,
                   ce.ageGroupId,
                   eg.name eventName,
                   eg.startDate,
                   ce.options ceOptions
            from ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTGROUPS . ' eg
            where ce.compid = ' . $compId . '
            and   ce.maxgroup = eg.id
            ';

    $result = e4s_queryNoLog($sql);
    $compEvents = array();
    while ($obj = $result->fetch_object()) {
        $obj->ceId = (int)$obj->ceId;
        $obj->eventId = (int)$obj->eventId;
        $obj->ageGroupId = (int)$obj->ageGroupId;
        $obj->ceOptions = e4s_addDefaultCompEventOptions($obj->ceOptions);
        $compEvents[$obj->eventId . '-' . $obj->ageGroupId] = $obj;
    }
    $sql = 'select e.id, 
                   e.athlete,
                   a.dob dob,
                   ce.compId,
                   ce.ageGroupId,
                   ce.eventId,
                   u.user_email userEmail,
                   c.name competition,
                   eg.name eventName,
                   eg.startDate
            from ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_ATHLETE . ' a,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_COMPETITON . ' c,
                 ' . E4S_TABLE_EVENTGROUPS . ' eg,
                 ' . E4S_TABLE_USERS . ' u
            where ce.id = e.compeventid
            and e.athleteid = a.id
            and e.userid = u.id
            and e.paid = ' . E4S_ENTRY_PAID . '
            and c.id = ce.compid
            and ce.compid = ' . $compId . '
            and ce.maxgroup = eg.id';
    $result = e4s_queryNoLog($sql);
    $results = array();
    while ($obj = $result->fetch_object()) {
        $obj->id = (int)$obj->id;
        $obj->eventId = (int)$obj->eventId;
        $obj->ageGroupId = (int)$obj->ageGroupId;
        $athleteAg = getAgeGroupInfo($allCompDOBs, $obj->dob);

        if (array_key_exists('agid', $athleteAg['ageGroup'])) {
            if ($obj->ageGroupId !== (int)$athleteAg['ageGroup']['agid']) {
//            if ($obj->ageGroupId !== (int)$athleteAg['ageGroup']['agid'] or $obj->ageGroupId === 14) {
                $updated = entryIncorrectAge($obj, $compEvents, $athleteAg, $update, $email);
                if ($updated !== '') {
                    $results[] = $updated;
                }
            }
        }
    }
    return $results;
}

function entryIncorrectAge($entryObj, $compEvents, $athleteAg, $update = FALSE, $email = FALSE) {

    $currentKey = $entryObj->eventId . '-' . $entryObj->ageGroupId;
    if (!array_key_exists($currentKey, $compEvents)) {
        return 'Current Event Not found for ' . $entryObj->athlete;
    }
    $targetAgObj = $athleteAg['ageGroup'];
    $targetKey = '';
    foreach ($athleteAg['ageGroups'] as $ag) {
//        if ((int)$ag['agid'] !== 14 ) {
        $targetKey = $entryObj->eventId . '-' . $ag['agid'];
        if (array_key_exists($targetKey, $compEvents)) {
            $targetAgObj = $ag;
            break;
        }
//        }
        $targetKey = '';
    }

    if ($targetKey === '') {
        return 'Target Event Not found for ' . $entryObj->athlete;
    }
    $currentEvent = $compEvents[$currentKey];

    $targetEvent = $compEvents[$targetKey];

    if ($targetEvent->ceId === $currentEvent->ceId) {
        return '';
    }
    if ($update) {
        $updateSql = 'update ' . E4S_TABLE_ENTRIES . '
                       set compeventid = ' . $targetEvent->ceId . ",
                           eventagegroup = '" . $targetAgObj['Name'] . "'
                       where id = " . $entryObj->id;

        e4s_queryNoLog($updateSql);
    }
    $move = 'Moved';
    if (!$update) {
        $move = 'Will move';
    }

    if ($currentEvent->eventName === $targetEvent->eventName and $currentEvent->startDate === $targetEvent->startDate) {
        // Dont send email if the same event and time
        return $move . ' ' . $entryObj->athlete . ' in the ' . $targetEvent->eventName . '(' . $currentEvent->ceId . '-' . $targetEvent->ceId . ') but no email required.';
    }

    if ($update and $email) {
        $currentDate = date_create($currentEvent->startDate);
        $currentStartDate = date_format($currentDate, E4S_FORMATTED_DATETIME);
        $targetDate = date_create($targetEvent->startDate);
        $targetStartDate = date_format($targetDate, E4S_FORMATTED_DATETIME);
        $body = 'Dear ' . $entryObj->athlete . ",<br><br>We are sorry to say there has been a mistake in the competition '" . $entryObj->competition . "'.<br>";
        $body .= 'The age groups associated to events had been configured incorrectly and a small number of athletes have been entered into the wrong event. We have corrected this as follows :<br>';
        $body .= 'You were originally in the ' . $currentEvent->eventName . ' at ' . $currentStartDate . '.<br>';
        $body .= 'You are now in the ' . $targetEvent->eventName . ' at ' . $targetStartDate . '.<br><br>';
        $body .= 'We are sorry about this issue but if you have any issues regarding this, please contact us at support@entry4sports.com <br><br>';
        $body .= 'Please do not reply to this email as it has been system generated and inbound emails are not monitored.<br><br>';
        e4s_mail($entryObj->userEmail, 'Entry correction for ' . $entryObj->athlete, $body);
    }
    return $move . ' entry for ' . $entryObj->athlete . ' from ' . $currentEvent->eventName . ' to ' . $targetEvent->eventName;
}

function e4s_getFinanceParams($obj) {
    $action = checkFieldForXSS($obj, 'action:Action');
    $action = strtolower($action);
    $orgId = checkFieldForXSS($obj, 'orgid:Organisation');
    if (is_null($orgId)) {
        $orgId = 0;
    } else {
        $orgId = (int)$orgId;
    }

    $fromOrder = checkFieldForXSS($obj, 'fromorder:From Order Number');
    if (!is_null($fromOrder) and $fromOrder !== '') {
        $fromOrder = (int)$fromOrder;
    } else {
        $fromOrder = 0;
    }
    $toOrder = checkFieldForXSS($obj, 'toorder:To Order Number');
    if (!is_null($toOrder) and $toOrder !== '') {
        $toOrder = (int)$toOrder;
    } else {
        $toOrder = 0;
    }

    $compFromDate = checkFieldForXSS($obj, 'compfromdate:From Competition Date');
    if (is_null($compFromDate) or $compFromDate === '') {
        $compFromDate = '';
    }
    $compToDate = checkFieldForXSS($obj, 'comptodate:To Competition Date');

    if (is_null($compToDate) or $compToDate === '') {
        $compToDate = '';
    }

    $fromDate = checkFieldForXSS($obj, 'fromdate:From Order Date');
    if (is_null($fromDate) or $fromDate === '') {
        $fromDate = '';
    }
    $toDate = checkFieldForXSS($obj, 'todate:To Order Date');

    if (is_null($toDate) or $toDate === '') {
        $toDate = '';
    }

    $custId = checkFieldForXSS($obj, 'custid:FromCustomer');
    if (is_null($custId) or $custId === '') {
        $custId = 0;
    } else {
        $custId = (int)$custId;
    }

    $fromCompId = checkFieldForXSS($obj, 'fromid:Competition Number');
    if (is_null($fromCompId) or $fromCompId === '') {
        $fromCompId = 0;
    } else {
        $fromCompId = (int)$fromCompId;
    }
    $toCompId = checkFieldForXSS($obj, 'toid:Competition Number');
    if (is_null($toCompId) or $toCompId === '') {
        $toCompId = 0;
    } else {
        $toCompId = (int)$toCompId;
    }

    if (is_numeric($fromCompId) and is_numeric($toCompId)) {
        $fromCompId = (int)$fromCompId;
        $toCompId = (int)$toCompId;
        if ($fromCompId < 0 or $toCompId < 0) {
            Entry4UIError(8345, 'Invalid Comp Ids have been passed');
        }
    }
    $justTickets = checkFieldForXSS($obj, 'ticketsonly:Tickets Only');
    if (is_null($justTickets) or (int)$justTickets === 0) {
        $justTickets = FALSE;
    } else {
        $justTickets = TRUE;
    }
    $details = checkFieldForXSS($obj, 'details:Include Details');
    if (is_null($details) or (int)$details === 0) {
        $details = FALSE;
    } else {
        $details = TRUE;
    }
    $retObj = new stdClass();
    $retObj->orgId = $orgId;
    $retObj->fromCompId = $fromCompId;
    $retObj->toCompId = $toCompId;
    $retObj->compFromDate = $compFromDate;
    $retObj->compToDate = $compToDate;
    $retObj->justTickets = $justTickets;
    $retObj->details = $details;
    $retObj->fromOrderNo = $fromOrder;
    $retObj->toOrderNo = $toOrder;
    $retObj->fromOrderDate = $fromDate;
    $retObj->toOrderDate = $toDate;
    $retObj->custId = $custId;
    $retObj->action = $action;
    return $retObj;

}

function e4s_FinanceReport($obj) {
    include_once E4S_FULL_PATH . 'reports/financeV2/commonFinance.php';
    $params = e4s_getFinanceParams($obj);

    $output = e4s_Finance2Report($params);
//    if ($params->action === "aaifile" ){
//        writeToFile("finance_report.csv",$output);
//    }
//    if ($params->action === "aaidata" ){
//        $output = addslashes($output);
//        Entry4UISuccess('"data":' . json_encode($output));
//    }
//    if ($params->action === "aaifinance" ){
//        exit($output);
//    }
}

function Ireland_FULL_Finance_Report($obj) {
    include_once E4S_FULL_PATH . 'reports/finance/commonFinance.php';
    $params = e4s_getFinanceParams($obj);

    $output = getFinanceData($params->orgId, $params->fromCompId, $params->toCompId, $params->compFromDate, $params->compToDate, $params->justTickets, $params->details, $params->fromOrderNo, $params->toOrderNo, $params->fromOrderDate, $params->toOrderDate, $params->custId);
    if ($params->action === 'aaifile') {
        writeToFile('finance_report.csv', $output);
    }
    if ($params->action === 'aaidata') {
        $output = addslashes($output);
        Entry4UISuccess('"data":' . json_encode($output));
    }
    if ($params->action === 'aaifinance') {
        exit($output);
    }
}

function writeToFile($fileName, $output) {
//    $txt = fopen($fileName, "w") or die("Unable to open file!");
//    fwrite($txt, $output);
//    fclose($txt);

    header('Content-Description: File Transfer');
    header('Content-Disposition: attachment; filename=' . basename($fileName));
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
//    header('Content-Length: ' . filesize($file));
    header('Content-Type: text/plain');
    ob_clean();
    flush();
//    readfile($file);
    echo $output;
}


function finance1($fromDate) {
    $sql = "
        select id id
        from " . E4S_TABLE_POSTS . "
        where post_type = '" . WC_POST_ORDER . "'
        and post_date_gmt > '" . $fromDate . "'
        order by post_date_gmt
        ";
    $result = e4s_queryNoLog($sql);
    $compInfo = array();
    while ($obj = $result->fetch_object()) {
        $order = wc_get_order($obj->id);
        $items = $order->get_items();
        $status = $order->get_status();
        if ($status !== 'pending' and $status !== 'cancelled') {
            foreach ($items as $item) {
                $product = $item->get_product();
                if ($product === FALSE) {
                    continue;
                }
                $desc = $product->get_description();
                $descObj = e4s_getDataAsType($desc, E4S_OPTIONS_OBJECT);
                if (isset($descObj->compid)) {
                    $compId = $descObj->compid;
                    if (!array_key_exists($compId, $compInfo)) {
                        $obj = new stdClass();
                        $obj->subTotal = 0;
                        $compInfo[$compId] = $obj;
                    } else {
                        $obj = $compInfo[$compId];
                    }
                    $obj->subTotal += $item->get_subtotal();
                }
            }
        }
    }
    echo 'finished';
}

?>