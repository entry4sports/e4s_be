<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
define('E4S_SELF_APPROVER', 1);
define('E4S_AUTH_APPROVER', 2);
// Main Init for Requesting SelfService from Team Entry Header
function e4s_requestSelfService($obj) {
    $userObj = e4s_getUserObj(FALSE, TRUE);
    $ssid = checkFieldForXSS($obj, 'ssid:Self Service ID' . E4S_CHECKTYPE_NUMERIC);
    if ($ssid === '' or (int)$ssid === 0 or is_null($ssid)) {
        Entry4UIError(1000, 'No Service ID', 400, '');
    }
    $entityid = checkFieldForXSS($obj, 'entityid:Entity ID' . E4S_CHECKTYPE_NUMERIC);
    if ($entityid === '' or (int)$entityid === 0 or is_null($entityid)) {
        Entry4UIError(1001, 'No Entity ID', 400, '');
    }
    $ssRow = e4s_getSelfServiceRow($ssid);
    if (is_null($ssRow)) {
        Entry4UIError(1002, 'Invalid Service ID', 400, '');
    }
    $ssRow['entityid'] = $entityid;

    e4s_requestOrAddService($ssRow);
}

// Main Init for approving a SSR from the email
function e4s_authoriseSelfService($obj) {
    $userObj = e4s_getUserObj(FALSE, TRUE);
    $ssrid = checkFieldForXSS($obj, 'id:Self Service Request ID' . E4S_CHECKTYPE_NUMERIC);
    if ($ssrid === '' or (int)$ssrid === 0 or is_null($ssrid)) {
        Entry4UIError(1000, 'No Service ID', 400, '');
    }

    $crc = checkFieldForXSS($obj, 'crc:Self Service Request CRC');
    if ($crc === '' or (int)$crc === 0 or is_null($crc)) {
        Entry4UIError(1000, 'No Service Request CRC', 400, '');
    }
    $crc = (int)$crc;
    $ssrRow = e4s_getSSRRow($ssrid, $crc);
    if (is_null($ssrRow)) {
        Entry4UISuccess('"message":"Request does not exist. This may have already been approved by another user."');
    }
    $ssrObj = _e4s_returnSRRObj($ssrRow);
    $ssRow = e4s_getSelfServiceRow($ssrObj->ssid);
    $ssRow['entityid'] = $ssrObj->entityid;
//    Approve it. Returns true if done, false if already done ( already has authority )
    $processed = _e4s_approveEntityToUser($ssRow, $ssrObj->userid, E4S_AUTH_APPROVER);
    _e4s_deleteSSRRequest($ssrObj);
    if ($processed) {
        // Action has been performed
        _e4s_notifyOfApproval($ssRow, $ssrObj);
    } else {
        // Action has NOT been performed, maybe as the use now has this access by other means
        _e4s_informCurrentUser($processed);
    }
}

function _e4s_deleteSSRRequest($ssrObj) {
    // delete all requests to all approvers for these demographics
    $sql = '
        delete
        from ' . E4S_TABLE_SELFSERVICEREQUEST . '
        where ssid = ' . $ssrObj->ssid . '
        and   entityid = ' . $ssrObj->entityid . '
        and   userid = ' . $ssrObj->userid;

    e4s_queryNoLog($sql);
}

function e4s_getSSRRow($ssrid, $crc) {
    $sql = '
        select *
        from ' . E4S_TABLE_SELFSERVICEREQUEST . '
        where id = ' . $ssrid;

    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        return null;
    }
    $ssrRow = $result->fetch_assoc();

    $rowCRC = e4s_getSSRCRC($ssrRow);

    if ($rowCRC !== $crc) {
        Entry4UIError(1003, 'Invalid Service Request CRC', 400, '');
    }
    return $ssrRow;
}

function _e4s_returnSRRObj($ssrRow) {
//    e4s_dump($ssrRow);
    $obj = new stdClass();
    $obj->id = (int)$ssrRow['id'];
    $obj->ssid = (int)$ssrRow['ssid'];
    $obj->entityid = (int)$ssrRow['entityid'];
    $obj->userid = (int)$ssrRow['userid'];
    return $obj;
}

function e4s_getSSRCRC($ssrRow) {
    $obj = _e4s_returnSRRObj($ssrRow);

    return e4s_getSSRCRCFromObj($obj);
}

function e4s_getSSRCRCFromObj($obj) {
    return $obj->ssid * $obj->entityid + $obj->userid;
}

function _e4s_approveEntityToUser($ssRow, $userid, $byWhom) {
    include_once E4S_FULL_PATH . 'admin/userMaint.php';

    $entityLevel = (int)$ssRow['entitylevel'];

    if ($entityLevel === E4S_CLUB_ENTITY) {
        $retval = addUserToClubWithError($userid, $ssRow['entityid']);
    } else {
        $retval = addUserToAreaWithError($userid, $ssRow['entityid']);
    }
    if ($retval) {
        e4s_auditSelfService($ssRow, $userid, $byWhom);
    }
    return $retval;
}

function e4s_requestOrAddService($ssRow) {

    if ($ssRow['approvers'] === '[]' or $ssRow['approvers'] === '' or is_null($ssRow['approvers'])) {
        _e4s_approveEntityToUser($ssRow, e4s_getUserID(), E4S_SELF_APPROVER);
        Entry4UISuccess('"message":"User authority updated"');
    }
    $approversObj = e4s_getSSApprovers($ssRow['approvers']);
    $emails = array();
    foreach ($approversObj as $approver) {
        $emails[] = $approver->email;
    }
    e4s_requestAccess($ssRow, $approversObj);
    Entry4UISuccess('"message":"Request for access has been made to ' . implode(', ', $emails) . '. If this is not granted or you have not heard back in a reasonable time, please contact support@entry4sports.com"');
}

function e4s_getSelfServiceRow($id) {
    $sql = '
        select *
        from ' . E4S_TABLE_SELFSERVICE . '
        where id = ' . $id;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        return null;
    }
    return $result->fetch_assoc();
}

function e4s_writeSelfService($compid, &$options) {
    //    delete any that exist
    e4s_deleteSelfService($compid);

    if (!isset($options->selfService)) {
        return;
    }

    e4s_createSelfService($compid, $options->selfService);
    unset($options->selfService);
}

function e4s_createSelfService($compid, $selfServiceObj) {
    foreach ($selfServiceObj as $selfService) {
        $sql = '
            insert into ' . E4S_TABLE_SELFSERVICE . ' (compid, entitylevel,type,approvers)
            values (
            ' . $compid . ',
            ' . $selfService->entityLevel . ",
            '" . $selfService->clubType . "',
            '" . json_encode(e4s_getApproversToCreate($selfService)) . "'
            )
        ";
        e4s_queryNoLog($sql);
    }
}

function e4s_getApproversToCreate($selfService) {

    if (!isset($selfService->approvalUsers)) {
        return [];
    }
    $approvers = array();
    foreach ($selfService->approvalUsers as $approver) {
        $appObj = new stdClass();
        $appObj->id = (int)$approver->id;
        $approvers[] = $appObj;
    }
    return $approvers;
}

function e4s_deleteSelfService($compid) {
    $sql = '
        delete from ' . E4S_TABLE_SELFSERVICE . '
        where compid = ' . $compid . ' 
    ';
    e4s_queryNoLog($sql);
}

function e4s_readSelfService($compid, &$options) {
    $options->selfService = e4s_getSelfServiceForCompid($compid);
}

function e4s_getSelfServiceForCompid($compid) {
    $sql = '
        select s.*, e.name
        from ' . E4S_TABLE_SELFSERVICE . ' s,
             ' . E4S_TABLE_ENTITY . ' e
        where compid = ' . $compid . '
        and s.entitylevel = e.level
        order by s.entitylevel
    ';
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows === 0) {
        return [];
    }
    $retArr = array();
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    foreach ($rows as $row) {
        $obj = new stdClass();
        $obj->id = (int)($row['id']);
        $obj->entityLevel = (int)$row['entitylevel'];
        $obj->clubType = $row['type'];
        $obj->entityName = $row['name'];
        $obj->approvalUsers = e4s_getSSApprovers($row['approvers']);
        $retArr[] = $obj;
    }
    return $retArr;
}

function e4s_getSSApprovers($approvers) {
    if ($approvers === '[]' or $approvers === '') {
        return [];
    }
    $approversObj = json_decode($approvers, JSON_NUMERIC_CHECK);

    $approverIDs = array();
    foreach ($approversObj as $approver) {
        $approverIDs[] = $approver['id'];
    }

    $sql = '
        select *
        from ' . E4S_TABLE_USERS . '
        where id in (' . implode(',', $approverIDs) . ')
    ';

    $result = e4s_queryNoLog($sql);
    if ($result->num_rows === 0) {
        return [];
    }
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    $approvers = array();
    foreach ($rows as $row) {
        $approver = new stdClass();
        $approver->id = (int)$row['ID'];
        $approver->login = $row['user_login'];
        $approver->niceName = $row['user_nicename'];
        $approver->email = $row['user_email'];
        $approver->displayName = $row['display_name'];
        $approvers[] = $approver;
    }
    return $approvers;
}

function _e4s_notifyOfApproval($ssRow, $ssrObj) {
    // send user an email to tell them they have been approved
    _e4s_emailSSUser($ssRow, $ssrObj);
    // Tell current user it has been done
    _e4s_informCurrentUser(TRUE);
}

function _e4s_informCurrentUser($processed) {
    if ($processed) {
        echo '
    Entry 4 Sports.
    
    This action has been successful and the user has been emailed.
    ';
    } else {
        echo '
    Entry 4 Sports.
    
    This action has not been completed. This may be because the user already has this authority by other means. You may want to check with the user to see if they can now access the system appropriately.
    ';
    }
    exit();
}

function e4s_getSSUserObj($ssrObj) {
//    debug_print_backtrace();
    $sql = '
        select * 
        from ' . E4S_TABLE_USERS . '
        where id = ' . $ssrObj->userid;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(1004, 'Invalid User in Service Request', 400, '');
    }
    $row = $result->fetch_assoc();

    $userObj = new stdClass();
    $userObj->id = $row['ID'];
    $userObj->email = $row['user_email'];
    $userObj->name = $row['display_name'];
    return $userObj;
}

function _e4s_getEntityDesc($level) {
    $sql = '
        select * 
        from ' . E4S_TABLE_ENTITY . '
        where level = ' . $level;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(1005, 'Invalid Entity Request', 400, '');
    }
    $row = $result->fetch_assoc();
    return $row['name'];
}

function _e4s_getEntityName($level, $id) {
    if ($level === E4S_CLUB_ENTITY) {
        $sql = '
            select * 
            from ' . E4S_TABLE_CLUBS . '
            where id = ' . $id;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(1006, 'Invalid Club Request', 400, '');
        }
        $row = $result->fetch_assoc();
        return $row['Clubname'];
    } else {
        $sql = '
            select * 
            from ' . E4S_TABLE_AREA . '
            where id = ' . $id;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(1007, 'Invalid Area Request', 400, '');
        }
        $row = $result->fetch_assoc();
        return $row['name'];
    }
}

function _e4s_getEntityObj($ssRow, $ssrObj) {
    $entityObj = new stdClass();
    $entityObj->level = (int)$ssRow['entitylevel'];

    if ($entityObj->level === E4S_CLUB_ENTITY) {
        $entityObj->desc = 'Club';
        if ($ssRow['type'] !== 'C') {
            $entityObj->desc = 'School';
        }
    } else {
        $entityObj->desc = _e4s_getEntityDesc($entityObj->level);
    }
    $entityObj->name = _e4s_getEntityName($entityObj->level, $ssrObj->entityid);

    return $entityObj;
}

function _e4s_emailSSUser($ssRow, $ssrObj) {
    $userObj = e4s_getSSUserObj($ssrObj);
    $entityObj = _e4s_getEntityObj($ssRow, $ssrObj);

    $body = 'Dear ' . $userObj->name . ',<br><br>';

    $body .= 'You have been granted access to the Entry4Sports system for the ' . $entityObj->desc . ' of ' . $entityObj->name;
    $body .= '<br><br>';
    $body .= "<a href='https://" . E4S_CURRENT_DOMAIN . "'>Entry4Sports</a> Website";

    $body .= Entry4_emailFooter($userObj->email);

    e4s_mail($userObj->email, 'Entry4Sports Approved Access', $body, Entry4_mailHeader(''));
}

function e4s_auditSelfService($ssRow, $userid, $byWhom) {
    $curUserObj = e4s_getUserAccount();
    $entityName = e4s_getEntityName($ssRow);
    $body = 'Nick/Paul,<br><br>';
    $body .= 'User ' . $curUserObj['display_name'] . ' has ';
    if ($byWhom === E4S_SELF_APPROVER) {
        $body .= ' self approved the adding of ' . $entityName;
    } else {
        $userObj = e4s_getAccountForID($userid);
        $body .= ' approved the request by ' . $userObj['display_name'] . ' to represent ' . $entityName;
    }
    $body .= ' on the domain of ' . E4S_CURRENT_DOMAIN;
    $body .= '<br><br>';
    $body .= Entry4_emailFooter($curUserObj['display_name']);

    $headers = Entry4_mailHeader('SelfService');
    $e4s_emails = array();
    $e4s_emails[] = 'nick@entry4sports.com';
    $e4s_emails[] = 'paul@entry4sports.com';
    e4s_mail($e4s_emails, 'Entry4Sports : Self Service Action', $body, $headers);
}

function e4s_getEntityName($ssRow) {
    $entityLevel = (int)$ssRow['entitylevel'];

    if ($entityLevel === E4S_CLUB_ENTITY) {
        $sql = 'select Clubname name
                from ' . E4S_TABLE_CLUBS . '
                where id = ' . $ssRow['entityid'];
    } else {
        $sql = 'select name
                from ' . E4S_TABLE_AREA . '
                where id = ' . $ssRow['entityid'];
    }
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(2001, 'Failed to get entity name (' . $entityLevel . '/' . $ssRow['entityid'] . ')');
    }
    $row = $result->fetch_assoc();
    return $row['name'];
}

function _e4s_sendRequestToApprover($ssRow, $ssrRow, $approverObj) {
//    e4s_dump($ssrRow);
    $ssrObj = _e4s_returnSRRObj($ssrRow);
    $userObj = e4s_getSSUserObj($ssrObj);
    $entityObj = _e4s_getEntityObj($ssRow, $ssrObj);

    $body = 'Dear ' . $approverObj->displayName . ',<br><br>';
    $body .= 'You have been requested to grant access to the Entry4Sports system for ' . $userObj->email . '. They require access to the ' . $entityObj->desc . ' of ' . $entityObj->name;
    $body .= '<br><br>';
    $body .= 'You can click the link below to approve this request if the information is correct.';
    $body .= '<br>';
//   wp-login.php?redirect_to=%2F%23%2Fentry%3Fcomporgid%3D74%26compid%3D69
//                            /#/entry?comporgid=74&compid=69
    $body .= "<a href='https://" . E4S_CURRENT_DOMAIN . '/wp-login.php?redirect_to=%2Fwp-json%2Fe4s%2Fv5%2Fselfservice%3Fid%3D' . $ssrRow['id'] . '%26crc%3D' . e4s_getSSRCRCFromObj($ssrObj) . "'>Approve Request</a>";
    $body .= '<br><br>';
    $body .= "<a href='https://" . E4S_CURRENT_DOMAIN . "'>Entry4Sports</a> Website";

    $body .= Entry4_emailFooter($userObj->email);

    $headers = Entry4_mailHeader('SelfService');
    $headers[] = 'Bcc: nick@entry4sports.com';
    $headers[] = 'Bcc: paul@entry4sports.com';
    e4s_mail($approverObj->email, 'Entry4Sports : Access Approval Request', $body, $headers);
}

function e4s_requestAccess($ssRow, $approversObj) {
    foreach ($approversObj as $approverObj) {
//        echo "\nCreate SSR Row\n";
//        e4s_dump($ssRow);
        $ssrRow = _e4s_createSSR($ssRow, $approverObj);
        _e4s_sendRequestToApprover($ssRow, $ssrRow, $approverObj);
    }
}

function _e4s_readSSRRow($ssRow, $approverObj) {
    $curUserID = e4s_getUserID();

    $sql = 'select *
            from ' . E4S_TABLE_SELFSERVICEREQUEST . '
            where ssid = ' . $ssRow['id'] . '
            and   userid = ' . $curUserID . '
            and   entityid = ' . $ssRow['entityid'] . '
            and   approveruserid = ' . $approverObj->id;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows > 0) {
        return $result->fetch_assoc();
    }
    return null;
}

function _e4s_createSSR($ssRow, $approverObj) {
    $curUserID = e4s_getUserID();
//    check it does not already exist
    $ssrRow = _e4s_readSSRRow($ssRow, $approverObj);
    if (!is_null($ssrRow)) {
//        echo "\n\nShould not be here";
        return $ssrRow;
    }
//    echo "\n\nAbout to create SSR";
    $sql = 'insert into ' . E4S_TABLE_SELFSERVICEREQUEST . ' (ssid, entityid, userid, approveruserid)
            values (
                ' . $ssRow['id'] . ',
                ' . $ssRow['entityid'] . ',
                ' . $curUserID . ',
                ' . $approverObj->id . '
            )';
    e4s_queryNoLog($sql);
    $ssrRow = _e4s_readSSRRow($ssRow, $approverObj);
//    echo "\n\nRead Check on SSR";
//    e4s_dump($ssrRow);
    if (!is_null($ssRow)) {
        return $ssrRow;
    }
    return null;
}