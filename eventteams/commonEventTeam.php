<?php
include_once E4S_FULL_PATH . 'eventteams/selfService.php';

// required for select statements
function e4s_optionIsTeamEvent() {
    return 'isTeamEvent":true';
}

function isEventATeamEvent($options) {
    $optionsObj = e4s_addDefaultEventOptions($options);
    return $optionsObj->isTeamEvent;
//    if (strpos($options,e4s_optionIsTeamEvent()) !== false){
//        return true;
//    }
//    return false;
}

function isEventTeam($ceoptions) {
    if (!is_null($ceoptions)) {
        if (isEventATeamEvent($ceoptions)) {
            return TRUE;
        }
    }

    return FALSE;
}

function e4s_getTeamType($ceoptions) {
    $teamType = E4S_TEAM_TYPE_STD;
    if (isset($ceoptions->eventTeam)) {
        if (isset($ceoptions->eventTeam->formType)) {
            if ($ceoptions->eventTeam->formType !== E4S_TEAM_TYPE_DEFAULT) {
                $teamType = $ceoptions->eventTeam->formType;
            }
        }
    }
    return $teamType;
}

function e4s_removeEventTeam($id) {
    $sql = 'insert into ' . E4S_TABLE_ATHLETE_DELETED . '
            select *, current_timestamp from ' . E4S_TABLE_ATHLETE . "
            where type = '" . E4S_TEAM_TYPE_SCHOOL . "'
            and   options like '%\"teamid\":" . $id . "%'";
    e4s_queryNoLog($sql);
    $sql = 'delete from ' . E4S_TABLE_ATHLETE . "
            where type = '" . E4S_TEAM_TYPE_SCHOOL . "'
            and   options like '%\"teamid\":" . $id . "%'";
    e4s_queryNoLog($sql);
//    $sql = "delete from " . E4S_TABLE_EVENTTEAMATHLETE . "
//            where teamentryid = " . $id;
//    e4s_query($sql);
    $sql = 'insert into ' . E4S_TABLE_EVENTTEAMENTRIES_DELETED . '
            select *, current_timestamp from ' . E4S_TABLE_EVENTTEAMENTRIES . '
            where id = ' . $id;
    e4s_queryNoLog($sql);
    $sql = 'delete from ' . E4S_TABLE_EVENTTEAMENTRIES . '
            where id = ' . $id;
    e4s_queryNoLog($sql);
//    $sql = "delete from " . E4S_TABLE_EVENTTEAMFORMS . "
//            where id = " . $id;
//    e4s_query($sql);
}

function e4s_removeEventTeamAndProduct($id, $prodid) {
    e4s_removeEventTeam($id);
    include_once E4S_FULL_PATH . 'product/commonProduct.php';
    e4s_removeProduct($prodid);
}

function e4s_getFormType($eventTeamObj) {
    return $eventTeamObj['type'];
}

function e4s_teamHasStandardAthletes($eventTeamObj, $formno) {
    if ($formno > E4S_FORMNO_INIT) {
        return FALSE;
    }
    if (e4s_getFormType($eventTeamObj) === E4S_TEAM_TYPE_SCHOOL) {
//        e4s_addDebug("e4s_teamHasStandardAthletes : " . true);
        return TRUE;
    }
//    e4s_addDebug("e4s_teamHasStandardAthletes else " . e4s_getFormType($eventTeamObj));
    return e4s_getFormType($eventTeamObj) === E4S_TEAM_TYPE_STD;
}

function e4s_isCEALeague($options) {
    if (!isset($options->eventTeam)) {
        return FALSE;
    }
    if (!isset($options->eventTeam->formType)) {
        return FALSE;
    }
    return strtolower($options->eventTeam->formType) === strtolower(E4S_TEAM_TYPE_LEAGUE);
}

function e4s_getFormDefsByField($formno, $field) {
    $rows = e4s_getFormDefs($formno);
    if (is_null($field)) {
        return $rows;
    }
    $formDefs = [];
    foreach ($rows as $row) {
        $formDefs [$row[$field]] = $row;
    }
    return $formDefs;
}

function e4s_getFormDefs($formno) {
    $sql = 'select *
            from ' . E4S_TABLE_EVENTTEAMFORMDEF . '
            where formno = ' . $formno . '
            order by pos';

    $result = e4s_queryNoLog($sql);
    if ($result->num_rows === 0) {
        Entry4UIError(1009, "Failed to find form definitions({$formno})", 404);
    }
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    return $rows;
}

//
//function e4s_getFormRows($options){
//    return e4s_getFormRowsForField(null, $options);
//}

function e4s_checkFormRows(&$options) {
    e4s_getFormRowsAndEntries(null, $options);
}

function e4s_getFormRowsAndEntries($field, &$options) {
    if (e4s_isCEALeague($options) && !isset($options->eventTeam->formRows)) {
        $options->eventTeam->formRows = e4s_getFormRowsForField($field, $options);
    }
}

function e4s_getFormRowsForField($field, $options) {
    if (!e4s_isCEALeague($options)) {
        return [];
    }

    $formRows = array();
    $formno = $options->eventTeam->showForm;

    $formDefRows = e4s_getFormDefsByField($formno, $field);
//    echo "form = " . $formno . "\nSize = " . sizeof($formDefRows) . "\n";
    foreach ($formDefRows as $formDefRow) {
        $formRow = new stdClass();
        $formRow->id = $formDefRow['id'];
        $formRow->position = $formDefRow['pos'];
        $formDefRowOptions = e4s_getOptionsAsObj($formDefRow['options']);
        $formRow->dateTime = $formDefRowOptions->datetime;
        $formRow->eventDef = new stdClass();
        $formRow->eventDef->id = $formDefRowOptions->edid;
        $formRow->eventDef->name = $formDefRow['label'];

        $formRows[] = $formRow;
    }

    return $formRows;
}
