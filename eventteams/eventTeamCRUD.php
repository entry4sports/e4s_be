<?php
define('E4S_FORMNO_NOT_SET', -1);
define('E4S_FORMNO_INIT', 0);

include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'product/WooProduct.php';
include_once E4S_FULL_PATH . 'events/commonEvent.php';
include_once E4S_FULL_PATH . 'entries/discounts.php';

function e4s_processTeam($process, $obj, $type = E4S_TEAM_TYPE_STD) {
// create obj to pass around
    $eventTeamObj = array();

    $startswith = checkFieldForXSS($obj, 'startswith:Starts With');
    $page = checkFieldForXSS($obj, 'pagenumber:Page Number' . E4S_CHECKTYPE_NUMERIC);
    $pagesize = checkFieldForXSS($obj, 'pagesize:Page Size');
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $id = checkFieldFromParamsForXSS($obj, 'id:Team id' . E4S_CHECKTYPE_NUMERIC);
    $prodId = checkFieldFromParamsForXSS($obj, 'prodId:Product Id' . E4S_CHECKTYPE_NUMERIC);
    $ceid = checkFieldFromParamsForXSS($obj, 'ceid:Competition event id' . E4S_CHECKTYPE_NUMERIC);
    $teamname = checkFieldFromParamsForXSS($obj, 'teamName:Team name');
    $entitylevel = checkFieldFromParamsForXSS($obj, 'entityLevel:Entity Level');
    $entityid = checkFieldFromParamsForXSS($obj, 'entityId:Entity ID' . E4S_CHECKTYPE_NUMERIC);
    $params = $obj->get_params('JSON');

    if (array_key_exists('athletes', $params)) {
        $athletes = $params['athletes'];
    } else {
        $athletes = null;
    }

    if (array_key_exists('formInfo', $params)) {
        $eventTeamObj['forminfo'] = $params['formInfo'];
    }

    $formrows = array();
    if (array_key_exists('formRows', $params)) {
        $formrows = $params['formRows'];
        if (!is_null($formrows) && sizeof($formrows) > 0) {
            $type = 'LEAGUE';
        }
    }

    if (!is_null($id)) {
        $eventTeamObj['id'] = (int)$id;
    }
    if (!is_null($prodId)) {
        $eventTeamObj['prodId'] = (int)$prodId;
    }else{
        $eventTeamObj['prodId'] = 0;
    }

    $isSchoolComp = FALSE;
    if (!is_null($compId)) {
        $compId = (int)$compId;
        $compObj = e4s_GetCompObj($compId);
        $isSchoolComp = $compObj->isSchool();
        $eventTeamObj['compid'] = $compId;
        $eventTeamObj['compRow'] = getCompRow($compId);
        $eventTeamObj['compRow']['options'] = e4s_getOptionsAsObj($eventTeamObj['compRow']['options']);
    }
    $GLOBALS['E4S_SCHOOL_COMP'] = $isSchoolComp;
    if (!is_null($id)) {
        $eventTeamObj['eventteamid'] = (int)$id;
    }
    $eventTeamObj['entitylevel'] = 0;
    if (!is_null($entitylevel)) {
        $eventTeamObj['entitylevel'] = (int)$entitylevel;
    }
    $eventTeamObj['entityid'] = 0;
    if (!is_null($entityid)) {
        $eventTeamObj['entityid'] = (int)$entityid;
    }
    if (!is_null($ceid)) {
        $eventTeamObj['ceid'] = (int)$ceid;
        getMetaForm($eventTeamObj);
    }

    if (!is_null($teamname)) {
//        $eventTeamObj['teamname'] = addslashes($teamname);
        $eventTeamObj['teamname'] = $teamname;
    }
    if (!is_null($athletes)) {
        $eventTeamObj['athletes'] = $athletes;
    }

    $eventTeamObj['formrows'] = $formrows;
    $eventTeamObj['type'] = $type;

    $eventTeamObj['process'] = $process;
    switch ($process) {
        case E4S_CRUD_LIST:
            listEventTeams($eventTeamObj);
            break;
        case E4S_CRUD_CREATE:
            switch ($type) {
                case E4S_TEAM_TYPE_SCHOOL:
                    createEventTeamSchool($eventTeamObj);
                    break;
                case E4S_TEAM_TYPE_LEAGUE:
                    createEventTeamLeague($eventTeamObj);
                    break;
                default:
                    createEventTeam($eventTeamObj);
                    break;
            }
            break;
        case E4S_CRUD_UPDATE:
            switch ($type) {
                case E4S_TEAM_TYPE_SCHOOL:
                    createEventTeamSchool($eventTeamObj);
                    break;
                case E4S_TEAM_TYPE_LEAGUE:
                    createEventTeamLeague($eventTeamObj);
                    break;
                default:
                    updateEventTeam($eventTeamObj);
                    break;
            }
            break;
        case E4S_CRUD_READ:
            readEventTeam($eventTeamObj);
            break;
        case E4S_CRUD_DELETE:
            deleteEventTeam($eventTeamObj);
            break;
    }
    exit();
}


# Functions here down
function createEventTeamLeague($eventTeamObj) {
    createEventTeamGeneric($eventTeamObj);
}

function createEventTeamSchool($eventTeamObj) {
    createEventTeamGeneric($eventTeamObj);
}

function createEventTeamGeneric($eventTeamObj) {
    // Create and update XXX team both call this.
    // delete anything before and simply re-create
    if ($eventTeamObj['id'] !== 0) {
        // do we have the comp before deleting
        if (!array_key_exists('ceid', $eventTeamObj)) {
            $sql = 'select ceid from ' . E4S_TABLE_EVENTTEAMENTRIES . '
            where id = ' . $eventTeamObj['id'];
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows !== 1) {
                Entry4UIError(1030, 'Failed to get Team for update : ' . $result->num_rows, 400, '');
            }
            $teamObj = $result->fetch_object();
            $teamObj->ceid = (int)$teamObj->ceid;
            $ceRow = getCompEventRow($teamObj->ceid);
            $eventTeamObj['ceid'] = $teamObj->ceid;
            $eventTeamObj['compid'] = $ceRow['CompID'];
            $ceOptions = e4s_addDefaultCompEventOptions($ceRow['options']);
            $eventTeamObj['formno'] = getMetaFormForCEOptions($ceOptions);
        }
        $eventTeamObj['existId'] = (int)$eventTeamObj['id'];
    }
    $eventTeamObj['id'] = 0;

    if (array_key_exists('existId', $eventTeamObj)) {
        $wooProductObj = createEventTeam($eventTeamObj, FALSE);
        $eventTeamObj['id'] = $eventTeamObj['existId'];
        deleteEventTeam($eventTeamObj);
        Entry4UISuccess('"data":' . json_encode($wooProductObj, JSON_NUMERIC_CHECK));
    } else {
        createEventTeam($eventTeamObj);
    }
}

function checkTeamValid($eventTeamObj) {
	$startChar = ' {';
	$endChar = '}';
	if ( $eventTeamObj['process'] === E4S_CRUD_UPDATE ){
		// force teamname not editable
		$sql = '
        select name teamName
        from ' . E4S_TABLE_EVENTTEAMENTRIES . '
        where id = ' . $eventTeamObj['id'];
		$result = e4s_queryNoLog($sql);
		if ( $result->num_rows !== 1 ){
			Entry4UIError(2002, 'Failed to get team for update', 400, '');
		}else{
			$row = $result->fetch_assoc();
			$eventTeamObj['teamname'] = $row['teamName'];
		}
		return $eventTeamObj;
	}
	$useTeamName = $eventTeamObj['teamname'];
	$hasUnique = (strpos($useTeamName,$startChar) !== false and strpos($useTeamName,$endChar) !== false );
	if ( $hasUnique and $eventTeamObj['id'] === 0 ){
		Entry4UIError(2000, 'Teamname has unique identifier (X). Please remove before creating team', 200, '');
	}
    $sql = '
        select ete.id, ete.name teamName
        from ' . E4S_TABLE_EVENTTEAMENTRIES . ' ete,
             ' . E4S_TABLE_COMPEVENTS . ' ce
        where ce.id = ete.ceid
        and ete.name like "' . addslashes($useTeamName) . $startChar . '%"
        and ce.compid = ' . $eventTeamObj['compid'] . '
        order by ete.name desc
        limit 2';
    $result = e4s_queryNoLog($sql);
    $nextChar = 'A';
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
		$rowTeamName = $row['teamName'];
		if ( strpos($rowTeamName,$startChar) !== false ){
			$arr = explode($startChar, $rowTeamName);
			$currentChar = $arr[1];
			if ( strpos($currentChar,$endChar) !== false ){
				$arr = explode($endChar, $currentChar);
				$nextChar = e4s_getNextChar($arr[0]);
			}else{
				Entry4UIError(2001, 'Teamname has unique identifier ( but no ). Please remove () before creating team', 200, '');
			}
		}
    }

	$useTeamName = str_replace('{{entity}}','',$useTeamName);
	$useTeamName = str_replace('{{eventname}}','',$useTeamName);
	$useTeamName = str_replace('{{gender}}','',$useTeamName);
	$useTeamName = str_replace('{{agegroup}}','',$useTeamName);
	$useTeamName = str_replace('{{unique}}','',$useTeamName);
	$eventTeamObj['teamname'] = $useTeamName . $startChar . $nextChar . $endChar;
    return $eventTeamObj;
}

function e4s_getNextChar($currentChar){
	$lastChar = substr($currentChar, -1);
	if (strlen($currentChar) === 1) {
		if ( $lastChar === 'Z' ) {
			return 'AA';
		}

		return chr( ord( $currentChar ) + 1 );
	}else{
		$firstChar = substr($currentChar, 0, -1);
		if ( $lastChar === 'Z' ) {
			return chr( ord( $firstChar ) + 1 ) . 'A';
		}
		return $firstChar . chr( ord( $lastChar ) + 1 );

	}
}
function e4s_checkClubCompForTeam($eventTeamObj){
    $compId = $eventTeamObj['compid'];
    $compObj = e4s_getCompObj($compId);
    if (!$compObj->isClubcomp() ){
        return $eventTeamObj;
    }
    $ccObj = new clubCompClass($compId,0,false);
    $entityId = $ccObj->getClubId();
    $eventTeamObj['entitylevel'] = 1;
    $eventTeamObj['entityid'] = $entityId;
    return $eventTeamObj;
}
function createEventTeam($eventTeamObj, $exit = TRUE) {
    if ($eventTeamObj['id'] !== 0) {
        Entry4UIError(1000, 'Invalid id passed to create a team', 400, '');
    }

    $eventTeamObj = checkTeamValid($eventTeamObj);
    $eventTeamObj = e4s_checkClubCompForTeam($eventTeamObj);
    $wooProductObj = createEventTeamProduct($eventTeamObj);
    $product_id = $wooProductObj->productId;
    // apply discounts here
    $discPriceObj = applyTeamDiscounts(array($eventTeamObj['ceid']), array($product_id), FALSE);
    $eventTeamObj = createEntry4EventTeam($eventTeamObj, $wooProductObj, $discPriceObj);
    if (!is_null($eventTeamObj)) {
        if ($exit) {
            $teamName = $eventTeamObj['teamname'];
            if ( !$eventTeamObj['overrideEntry'] ) {
                $product_cart_id = WC()->cart->generate_cart_id($product_id);
                $in_cart = WC()->cart->find_product_in_cart($product_cart_id);
                if (!$in_cart) {
                    WC()->cart->add_to_cart($product_id, 1);
                }
                Entry4UISuccess('"data":' . json_encode($wooProductObj, JSON_NUMERIC_CHECK),$teamName . ' added to your cart');
            }else{
                Entry4UISuccess('"data":' . json_encode($wooProductObj, JSON_NUMERIC_CHECK),$teamName . ' Confirmed');
            }

        } else {
            $wooProductObj->entryInfo = new stdClass();
            $wooProductObj->entryInfo->entryId = $eventTeamObj['id'];
            if ( $eventTeamObj['overrideEntry'] ) {
                $wooProductObj->entryInfo->paid = E4S_ENTRY_PAID;
            }else{
                $wooProductObj->entryInfo->paid = E4S_ENTRY_NOT_PAID;
            }
            $wooProductObj->entryInfo->orderId = 0;
            return $wooProductObj;
        }
    }
    Entry4UIError(1, 'Debug Stop', 404, '');
}

function readEventTeam($eventTeamObj) {
    Entry4UIError(1002, 'Not coded for this event', 404, '');
}

function deleteEventTeam($eventTeamObj) {
    if ($eventTeamObj['id'] === 0) {
        Entry4UIError(1002, 'No ID passed to delete a team', 400, '');
    }

    // Check if there are paid entries
    $sql = 'select et.*,
                   ce.compId
            from ' . E4S_TABLE_EVENTTEAMENTRIES . ' et,
                 ' . E4S_TABLE_COMPEVENTS . ' ce
            where et.id = ' . $eventTeamObj['id'] . '
            and   ce.id = et.ceid';
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(1003, 'Failed to get Team for deletion', 400, '');
    }

    $row = $result->fetch_assoc();
    $compObj = e4s_getCompObj($row['compId']);
    $freeEntry = $compObj->doesOrgGetFreeEntry();
    if ( (float)$row['price'] < 0.01 and !$freeEntry){
//      Check if the comp is autoPay for free entries
        $freeEntry = $compObj->markFreeEntryPaidCheck();
    }
    if ( !$freeEntry) {
        if ((int)$row['paid'] === E4S_ENTRY_PAID and $eventTeamObj['process'] !== E4S_CRUD_UPDATE) {
            Entry4UIError(2304, 'Can not delete a team that is paid for', 400, '');
        }
    }
    e4s_removeEventTeamAndProduct($eventTeamObj['id'], $row['productid']);
}

function getMetaFormForCEOptions($ceoptions) {
    $formNo = E4S_FORMNO_NOT_SET;
    if (isset($ceoptions->eventTeam)) {
        if (isset($ceoptions->eventTeam->showForm)) {
            // showForm was originally used as true/false !!!!!
            $formNo = $ceoptions->eventTeam->showForm;
        }
    }
    return $formNo;
}

function getMetaForm(&$eventTeamObj) {
    $sql = '';
    // check if athletes or form meta data
    if ($eventTeamObj['ceid'] === 0 and $eventTeamObj['eventteamid'] !== 0) {
        $sql = 'select ete.*, ce.options ceOptions, ce.compId
            from ' . E4S_TABLE_EVENTTEAMENTRIES . ' ete,
                 ' . E4S_TABLE_COMPEVENTS . ' ce
            where id = ' . $eventTeamObj['eventteamid'] . '
            and ce.id = ete.ceid';
    }
    if ($sql === '' and $eventTeamObj['ceid'] !== 0) {
        $sql = 'select id ceid, compId, options ceOptions
                    from ' . E4S_TABLE_COMPEVENTS . ' 
                    where id = ' . $eventTeamObj['ceid'];
    }
    if ($sql !== '') {
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(8900, 'Failed to find the Competition Team Event (' . $eventTeamObj['eventteamid'] . '/' . $result->num_rows . ')', 404, '');
        }
        $eventTeamRow = $result->fetch_assoc();
        $eventTeamObj['ceid'] = (int)$eventTeamRow['ceid'];
        $eventTeamObj['compid'] = (int)$eventTeamRow['compId'];
        $ceoptions = e4s_getOptionsAsObj($eventTeamRow['ceOptions']);
        $eventTeamObj['formno'] = getMetaFormForCEOptions($ceoptions);
    } else {
        $eventTeamObj['formno'] = 0;
    }
    return $eventTeamObj['formno'];
}

function e4s_buildEntityWhere($userclubs, $userareas) {

    $join = '';
    $where = ' and (';

    foreach ($userclubs as $userclub) {
        $where .= $join . '( ete.entitylevel = ' . E4S_CLUB_ENTITY . ' and ete.entityid = ' . $userclub->id . ') ';
        $join = ' or ';
    }

    foreach ($userareas as $userarea) {
        $where .= $join . '( ete.entitylevel = ' . $userarea->entitylevel . ' and ete.entityid = ' . $userarea->areaid . ') ';
        $join = ' or ';
    }

    $where .= $join;
    $where .= '(u.id = ' . e4s_getUserID() . ' and ete.entityid = 0 )';
    $where .= ') ';

    return $where;
}

function e4s_getEntity($entityId, $entityLevel, &$entityArray){
    $key = $entityLevel . '-' . $entityId;
    if ( array_key_exists($key, $entityArray) ){
        return $entityArray[$key];
    }
    $entityObj = new stdClass();
    $entityObj->id = $entityId;
    $entityObj->entityLevel = $entityLevel;
    $entityObj->name = 'Unknown';
    $entityObj->entity = 'Unknown';

    if ( (int)$entityLevel === 1 ){
        $sql = 'select t.clubname name,
                       e.name entity
                from ' . E4S_TABLE_CLUBS . ' t,
                     ' . E4S_TABLE_ENTITY . ' e';
    }else {
        $sql = 'select t.name name,
                       e.name entity
                from ' . E4S_TABLE_AREA . ' t,
                     ' . E4S_TABLE_ENTITY . ' e';
    }
    $sql .= ' where t.id = ' . $entityId . '
              and e.level = ' . $entityLevel;
    $result = e4s_queryNoLog($sql);
    if ( $result->num_rows === 1 ){
        $row = $result->fetch_object();
        $entityObj->name = $row->name;
        $entityObj->entity = $row->entity;
    }
    $entityArray[$key] = $entityObj;
    return $entityObj;
}
function listEventTeams($eventTeamObj, $exit = true) {
    $compid = $eventTeamObj['compid'];
    $allCompDOBs = getAllCompDOBs($compid);
    $entityArray = array();
    $eventSql = "SELECT ce.id
                     , ce.eventid
                     , e.Name eventName
                     , date_format(eg.startdate,'%Y-%m-%dT%H:%i:00') as startdate
                     , e.tf
                     , eg.id egId
                     , eg.name eventGroupName
                     , eg.eventno eventNo
                     , DATE_FORMAT(eg.startdate,'" . ISO_MYSQL_DATE . "') as startDateIso
                     , p.id priceid, p.price, p.saleprice, p.saleenddate
                     , ce.IsOpen
                     , ce.options ceoptions
                     , ce.maxAthletes
                     , e.options eoptions
                     , e.gender gender
                     , ce.AgeGroupID ageGroupId
                     , a.Name ageGroup
             from " . E4S_TABLE_EVENTS . '  e,
                  ' . E4S_TABLE_COMPEVENTS . ' ce,
                  ' . E4S_TABLE_EVENTGROUPS . ' eg,
                  ' . E4S_TABLE_EVENTPRICE . ' p,
                  ' . E4S_TABLE_AGEGROUPS . ' a
             where e.ID = ce.EventID
             and ce.PriceID = p.id
             and a.id = ce.agegroupid
             and ce.compid = ' . $compid . "
             and ce.maxgroup = eg.id
             and (ce.options like '%" . e4s_optionIsTeamEvent() . "%' or e.options like '%" . e4s_optionIsTeamEvent() . "%')
             order by e.Name, a.minage, e.gender
             ";

    $result = e4s_queryNoLog($eventSql);
    $rows = array();

    while ($row = $result->fetch_assoc()) {
        $ceoptions = e4s_mergeAndAddDefaultCEOptions($row['ceoptions'], $row['eoptions'], E4S_OPTIONS_OBJECT);
        // ignore if not set as a team event
        if (!$ceoptions->isTeamEvent) {
            // should only get here IF the event is a team event, but overridden in ceoptions. WHY ?
            continue;
        }
        $row['isDisabled'] = FALSE;
        /*
         , eg.id egID
                     , eg.name eventGroup
                     , eg.eventno eventNo
                     , DATE_FORMAT(eg.startdate,'" . ISO_MYSQL_DATE . "') as startDateIso
         */
        $eventGroup = new stdClass();
        $eventGroup->id = (int)$row['egId'];
        $eventGroup->name = $row['eventGroupName'];
        $eventGroup->eventNo = (int)$row['eventNo'];
        $eventGroup->startDate = $row['startDateIso'];
        $row['eventGroup'] = $eventGroup;

        unset($row['egId']);
        unset($row['eventGroupName']);
        unset($row['eventNo']);
        unset($row['startDateIso']);
        e4s_getFormRowsAndEntries('id', $ceoptions);

        // Ignore the entity passed now. Handled via UI
//        if ( !array_key_exists('entitylevel',$eventTeamObj) or !array_key_exists('entityid',$eventTeamObj)){
        // Not passed an entity id or level
        $eventTeamObj['entitylevel'] = -1;
        $eventTeamObj['entityid'] = -1;
        $userObj = e4s_getUserObj(FALSE, TRUE);


        if (isset($userObj->clubs)) {
            $userclubs = $userObj->clubs;
        } else {
            $userclubs = array();
        }

        if (sizeof($userclubs) > 0) {
            $eventTeamObj['entitylevel'] = 1;
            $eventTeamObj['entityid'] = (int)$userclubs[0]->id;
        }

        if (isset($userObj->areas)) {
            $userareas = $userObj->areas;
        } else {
            $userareas = array();
        }
        if (sizeof($userareas) > 0) {
            $eventTeamObj['entitylevel'] = (int)$userareas[0]->entitylevel;
            $eventTeamObj['entityid'] = (int)$userareas[0]->areaid;
        }
//        }
        $entityWhere = e4s_buildEntityWhere($userclubs, $userareas);

        $entity = new stdClass();
        $entity->level = (int)$eventTeamObj['entitylevel'];
        $entity->id = (int)$eventTeamObj['entityid'];
        $entity->school = e4s_isSchool($eventTeamObj['compRow']['options']);

        $securityReason = e4s_checkTeamEventSecurity($ceoptions, $entity);
        $ceoptions->warningMessage = '';
        if ($securityReason !== '') {
            if ($ceoptions->warningMessage === '') {
                $ceoptions->warningMessage = $securityReason;
            } else {
                $ceoptions->warningMessage = $securityReason;
            }
//          continue;
            $row['isDisabled'] = TRUE;
        }
        if ((int)$row['IsOpen'] === E4S_EVENT_CLOSED) {
            $row['isDisabled'] = TRUE;
            $ceoptions->warningMessage = 'Event Closed';
        }
        $row['ceoptions'] = $ceoptions;
        $row['Gender'] = $row['gender'];
        $teamType = e4s_getTeamType($ceoptions);

        $row['eoptions'] = e4s_getOptionsAsObj($row['eoptions']);

        // price object
        $priceObj = array();
        $priceObj['id'] = $row['priceid'];
        $priceObj['stdPrice'] = $row['price'];
        $priceObj['curPrice'] = $row['price'];
        $priceObj['salePrice'] = $row['saleprice'];
        $priceObj['saleDate'] = $row['saleenddate'];

        if ($priceObj['saleDate'] != '') {
            $saleDate = strtotime($priceObj['saleDate']);
            $now = time();
            if ($saleDate > $now) {
                $priceObj['curPrice'] = $priceObj['salePrice'];
            }
        }

        unset($row['priceid']);
        unset($row['price']);
        unset($row['saleprice']);
        unset($row['saleenddate']);
        $ageInfo = array();
        $ageInfo['id'] = $row['ageGroupId'];
        $ageInfo['name'] = $row['ageGroup'];
        $row['ageGroup'] = $ageInfo;
        $row['ageInfo'] = $ageInfo;
        unset($row['ageGroupId']);
        $formNo = getMetaFormForCEOptions($ceoptions);
        $gettingStandardAthletes = e4s_teamHasStandardAthletes($eventTeamObj, $formNo);
//e4s_addDebug("gettingStandardAthletes  :" . $gettingStandardAthletes);
        if (!$gettingStandardAthletes) {
            if (e4s_isCEALeague($ceoptions)) {
                $eventTeamSql = 'select ete.id
                          , ete.name teamName
                          , ete.paid
                          , ete.productid prodId
                          , ete.orderid
                          , ete.userid
                          , ete.entitylevel
                          , ete.entityid
                          , u.user_nicename username
                          , ete.price
                          , eta.id eventteamathleteid
                          , etfd.id formdefid
                          , etfd.pos
                          , etfd.label
                          , eta.athleteid
                          , etfd.required
                     from   ' . E4S_TABLE_EVENTTEAMATHLETE . ' eta,
                            ' . E4S_TABLE_EVENTTEAMFORMDEF . ' etfd,
                            ' . E4S_TABLE_EVENTTEAMENTRIES . ' ete left join
                            ' . E4S_TABLE_USERS . ' u on ete.userid = u.id
                     where  ete.ceid = ' . $row['id'] . '
                        and ete.id = eta.teamentryid
                        and etfd.formno = ' . $formNo . '
                        and eta.pos = etfd.pos
                        ' . $entityWhere . '
                     order by ete.ceid, eta.teamentryid, etfd.pos
                    ';

            } else {
                $eventTeamSql = 'select ete.id
                          , ete.name teamName
                          , ete.paid
                          , ete.productid prodId
                          , ete.orderid
                          , ete.userid
                          , ete.entitylevel
                          , ete.entityid
                          , u.user_nicename username
                          , ete.price
                          , etf.id formid
                          , etfd.id formdefid
                          , etfd.pos
                          , etfd.label
                          , etf.value
                          , etfd.required
                     from   ' . E4S_TABLE_EVENTTEAMFORMS . ' etf,
                            ' . E4S_TABLE_EVENTTEAMFORMDEF . ' etfd,
                            ' . E4S_TABLE_EVENTTEAMENTRIES . ' ete left join
                            ' . E4S_TABLE_USERS . ' u on ete.userid = u.id
                     where  ete.ceid = ' . $row['id'] . '
                        and ete.id = etf.teamentryid
                        and etfd.formno = ' . $formNo . '
                        and etf.formdefid = etfd.id
                        ' . $entityWhere . '
                     order by ete.ceid, etf.teamentryid, etfd.pos
                    ';
            }
        } else {
//            e4s_addDebug("Standard Athletes");
//    get existing event teams and athletes
            $eventTeamSql = 'select ete.id
                          , ete.name teamName
                          , ete.paid
                          , ete.productid prodId
                          , ete.orderid
                          , ete.userid
                          , ete.entitylevel
                          , ete.entityid
                          , u.user_nicename username
                          , ete.price
                          , eta.pos
                          , eta.athleteid
                          , a.URN
                          , a.aocode
                          , a.firstName
                          , a.surName
                          , a.dob
                          , a.clubid
                          , a.schoolid
                          , c.clubname clubname
                          , c2.clubname schoolname
                          , a.options aoptions
                     from   ' . E4S_TABLE_EVENTTEAMENTRIES . ' ete left join 
                            ' . E4S_TABLE_EVENTTEAMATHLETE . ' eta on ete.id = eta.teamentryid left join
                            ' . E4S_TABLE_ATHLETE . ' a on a.id   = eta.athleteid left join
                            ' . E4S_TABLE_CLUBS . ' c on a.clubid = c.id left join
                            ' . E4S_TABLE_CLUBS . ' c2 on a.schoolid = c2.id left join
                            ' . E4S_TABLE_USERS . ' u on ete.userid = u.id
                     where  ete.ceid = ' . $row['id'] . '
                     ' . $entityWhere . '
                     order by ete.ceid, eta.teamentryid, eta.pos
                    ';
        }
//        e4s_addDebug($eventTeamSql);
        $eventTeamResults = e4s_queryNoLog($eventTeamSql);
        $eventTeamRows = $eventTeamResults->fetch_all(MYSQLI_ASSOC);
        $compEventTeams = array();
        $eventRow = array();
        $athletes = array();
        $formInfo = array();
        $lastid = 0;

        $priceObj['actualPrice'] = $priceObj['curPrice'];

        if ($gettingStandardAthletes) {
            foreach ($eventTeamRows as $athleterow) {
                if ($lastid !== $athleterow['id'] and $lastid !== 0) {
                    $eventRow['athletes'] = $athletes;
                    $compEventTeams[] = $eventRow;
                    $athletes = array();
                    $eventRow = array();
                    $priceObj['actualPrice'] = $athleterow['price'];
                }
                $lastid = $athleterow['id'];
                if (count($eventRow) === 0) {
                    $eventRow['id'] = $athleterow['id'];
                    $eventRow['teamName'] = $athleterow['teamName'];
                    $eventRow['paid'] = $athleterow['paid'];
                    $eventRow['prodId'] = $athleterow['prodId'];
                    $eventRow['userId'] = $athleterow['userid'];
                    $eventRow['userName'] = $athleterow['username'];
                    $eventRow['entity'] = e4s_getEntity($athleterow['entityid'], $athleterow['entitylevel'], $entityArray);
                }
                $athlete = e4s_getAthleteForTeam($athleterow, $teamType, $allCompDOBs);
                $athletes[] = $athlete;
//                e4s_addDebug($athlete);
//                Entry4UISuccess();
            }

            if ($lastid !== 0) { // or sizeof($eventTeamRows) === 0) {
                $eventRow['athletes'] = $athletes;
                $compEventTeams[] = $eventRow;
            }
        } else {
            if (e4s_isCEALeague($ceoptions)) {
                $ce_formDefRows = $ceoptions->eventTeam->formRows;

                $lastTeamID = 0;
                $formRows = array();
                $compEventTeam = null;
                foreach ($ce_formDefRows as $ce_formDefRow) {
                    $formRows[] = (array)$ce_formDefRow;
                }

                foreach ($eventTeamRows as $teamrow) {
                    $athleteid = (int)$teamrow['athleteid'];
                    $formdefid = (int)$teamrow['formdefid'];
                    // now find the def for this team
                    $useRows = array();
                    foreach ($formRows as $formRow) {

                        $useRow = (array)$formRow;
                        if ((int)$useRow['id'] === $formdefid) {

                            $useRow['athlete'] = e4s_getAthleteForLeague($athleteid, $teamType, $allCompDOBs);
//                            $useRow['athlete'] = new stdClass();
//                            $useRow['athlete']->id = $athleteid;
                        }
                        $useRows[] = $useRow;
                    }

                    $compEventTeam = new stdClass();
                    $compEventTeam->athletes = array();
//                    $compEventTeam->formInfo = array();
                    $compEventTeam->entityLevel = (int)$eventTeamObj['entitylevel'];
                    $compEventTeam->entityId = (int)$eventTeamObj['entityid'];

                    $compEventTeam->ceid = $row['id'];
                    $compEventTeam->formRows = $useRows;
                    $compEventTeam->id = $teamrow['id'];
                    $compEventTeam->teamName = $teamrow['teamName'];
                    $compEventTeam->paid = $teamrow['paid'];
                    $compEventTeam->prodId = $teamrow['prodId'];
                    $compEventTeam->userId = $teamrow['userid'];
                    $compEventTeam->userName = $teamrow['username'];
                    $formRows = $useRows;

                    if ($lastTeamID !== 0 && $lastTeamID !== (int)$teamrow['id']) {
                        $compEventTeams[] = $compEventTeam;
                        $formRows = $ce_formDefRows;
                    }

                    $lastTeamID = (int)$teamrow['id'];
                }

                if ($lastTeamID !== 0) {
                    $compEventTeams[] = $compEventTeam;
                }
            } else {
                // school relay ?????
                foreach ($eventTeamRows as $formrow) {
                    if ($lastid !== $formrow['id'] and $lastid !== 0) {
                        $eventRow['formInfo'] = $formInfo;
                        $compEventTeams[] = $eventRow;
                        $formInfo = array();
                        $eventRow = array();
                        $priceObj['actualPrice'] = $formrow['price'];
                    }
                    $lastid = $formrow['id'];
                    if (count($eventRow) === 0) {
                        $eventRow['id'] = $formrow['id'];
                        $eventRow['teamName'] = $formrow['teamName'];
                        $eventRow['paid'] = $formrow['paid'];
                        $eventRow['prodId'] = $formrow['prodId'];
                        $eventRow['userId'] = $formrow['userid'];
                        $eventRow['userName'] = $formrow['username'];

                    }
                    $formInfoRec = array();
                    $formInfoRec['id'] = $formrow['formid'];
                    $formInfoRec['label'] = $formrow['label'];
                    $formInfoRec['pos'] = $formrow['pos'];
                    $formInfoRec['value'] = $formrow['value'];
                    if ((int)$formrow['required'] === 1) {
                        $formInfoRec['req'] = TRUE;
                    } else {
                        $formInfoRec['req'] = FALSE;
                    }
                    $formInfo[] = $formInfoRec;
                }
            }
            if ($lastid !== 0) {
                $eventRow['formInfo'] = $formInfo;
                $compEventTeams[] = $eventRow;
            }
        }

        $row['compEventTeams'] = $compEventTeams;
        $row['price'] = $priceObj;
        $rows[] = $row;
    }
    if ( $exit ) {
        Entry4UISuccess('
                 "data" : ' . json_encode($rows, JSON_NUMERIC_CHECK));
    }
    return $rows;
}

function e4s_getAthleteForLeague($athleteid, $teamType, $allCompDOBs) {
    if ($athleteid !== 0) {
        $sql = "select a.id athleteid,
                   a.firstName,
                   a.surName,
                   a.clubid,
                   a.aocode,
                   a.options aoptions,
                   a.URN URN,
                   a.dob dob,
                   c.clubname clubname,
                   '1' pos
                   from " . E4S_TABLE_ATHLETE . ' a left join
                        ' . E4S_TABLE_CLUBS . ' c on a.clubid = c.id
                   where a.id = ' . $athleteid;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(1001, 'Failed to read forms athlete', 400, '');
        }
        return e4s_getAthleteForTeam($result->fetch_assoc(), $teamType, $allCompDOBs);
    } else {
        $athlete = new stdClass();
        return $athlete;
    }

}

function e4s_getAthleteForTeam($athleterow, $teamType, $allCompDOBs) {
    if (is_null($athleterow['aoptions'])) {
        $aoptions = new stdClass();
    } else {
        $aoptions = e4s_getOptionsAsObj($athleterow['aoptions']);
    }
    $athlete = array();
    $athlete['id'] = $athleterow['athleteid'];
    $athlete['clubid'] = $athleterow['clubid'];
    $athlete['aocode'] = $athleterow['aocode'];
    $isSchoolComp = false;
    if ( array_key_exists('E4S_SCHOOL_COMP', $GLOBALS)) {
        $isSchoolComp = $GLOBALS['E4S_SCHOOL_COMP'];
    }
    if ($isSchoolComp) {
        if (array_key_exists('schoolid', $athleterow)) {
            if ((int)$athleterow['schoolid'] === E4S_UNATTACHED_ID) {
                $athlete['club'] = E4S_UNATTACHED;
            } else {
                $athlete['clubid'] = $athleterow['schoolid'];
                $athlete['club'] = $athleterow['schoolname'];
            }
        } else {
            if ((int)$athlete['clubid'] === E4S_UNATTACHED_ID) {
                $athlete['club'] = E4S_UNATTACHED;
            } else {
                $athlete['club'] = $athleterow['clubname'];
            }
        }
    } else {
        if ((int)$athlete['clubid'] === E4S_UNATTACHED_ID) {
            $athlete['club'] = E4S_UNATTACHED;
        } else {
            $athlete['club'] = $athleterow['clubname'];
        }
    }

    $athlete['pos'] = $athleterow['pos'];
    $athleterow['firstName'] = $athleterow['firstName'];
    $athleterow['surName'] = $athleterow['surName'];
    if ($teamType === E4S_TEAM_TYPE_SCHOOL) {
        $athlete['name'] = formatAthleteFirstname($athleterow['firstName']);
        if ($athleterow['surName'] !== '') {
            $athlete['name'] = formatAthlete($athleterow['firstName'] ,$athleterow['surName']);
        }
        $athlete['consent'] = FALSE;
        if (isset($aoptions->consent)) {
            $athlete['consent'] = $aoptions->consent;
        }
    } else {
        $athlete['firstName'] = formatAthleteFirstname($athleterow['firstName']);
        $athlete['surName'] = formatAthleteSurname($athleterow['surName']);
        $athlete['URN'] = $athleterow['URN'];
        $athlete['dob'] = $athleterow['dob'];
        // allow for teamname only team
        if (!is_null($athlete['dob'])) {

            $ag = getAgeGroupInfo($allCompDOBs, $athlete['dob']);

            // Dont display the VET in the get Athletes
            if (array_key_exists('vetAgeGroup', $ag)) {
                if (!is_null($ag['vetAgeGroup'])) {
                    $ag['vetAgeGroup']['Name'] = str_replace('VET ', '', $ag['vetAgeGroup']['Name']);
                }
            }
        } else {
            $ag = new stdClass();
        }
        $athlete['ageInfo'] = $ag;
    }
    return $athlete;
}

function deleteEventTeamMeta($eventTeamObj) {
    $sql = 'delete from ' . E4S_TABLE_EVENTTEAMATHLETE . '
            where teamentryid = ' . $eventTeamObj['id'];

    e4s_queryNoLog($sql);

    $sql = 'delete from ' . E4S_TABLE_EVENTTEAMFORMS . '
            where teamentryid = ' . $eventTeamObj['id'];

    e4s_queryNoLog($sql);
}

function getEventTeamRow($eventTeamObj) {
    $id = $eventTeamObj['eventteamid'];
    $sql = 'select * 
            from ' . E4S_TABLE_EVENTTEAMENTRIES . '
            where id = ' . $id;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows === 0) {
        Entry4UIError(2024, 'Failed to get the Event Team given the ID of ' . $id, 404, '');
    }
    return $result->fetch_assoc();
}

function updateEventTeam($eventTeamObj) {
    $ceRow = getCompEventRow($eventTeamObj['ceid']);
    $eventTeamObj['compid'] = $ceRow['CompID'];

    $eventTeamObj = checkTeamValid($eventTeamObj);
    $eventRow = getEventRow($ceRow['EventID']);
    $eventTeamObj['eventName'] = $eventRow['Name'];

    $compRow = getCompRow($eventTeamObj['compid']);
    getMetaForm($eventTeamObj);
    $desc = '{' . getProductDesc($compRow, $eventTeamObj) . '}';

    $postTitle = getProductTitle($compRow, $ceRow, $eventTeamObj);

    updateWooProduct($eventTeamObj['prodId'], $postTitle, $desc);

    $priceRow = getUseEventPrice($ceRow['PriceID']);
    $eventTeamRow = getEventTeamRow($eventTeamObj);

    $wooProductString = wooProductString($eventTeamRow['productid'], $priceRow);
    // Has the team Name changed
    if ($eventTeamRow['name'] !== $eventTeamObj['teamname']) {
        $sql = 'update ' . E4S_TABLE_EVENTTEAMENTRIES . "
                set name = trim('" . addslashes($eventTeamObj['teamname']) . "')
                where id = " . $eventTeamObj['id'];
        e4s_queryNoLog($sql);
    }
    updateEventTeamMeta($eventTeamObj);
    Entry4UISuccess('"data":' . $wooProductString);
}

function updateEventTeamMeta($eventTeamObj) {
    $useStdAthletes = e4s_teamHasStandardAthletes($eventTeamObj, $eventTeamObj['formno']);
    deleteEventTeamMeta($eventTeamObj);
    if ($useStdAthletes) {
        createEventTeamAthletes($eventTeamObj);
    } else {
        createEventTeamForm($eventTeamObj);
    }
}

function createEventTeamProduct($eventTeamObj) {

    $ceRow = getCompEventRow($eventTeamObj['ceid']);
    $ceoptions = e4s_getOptionsAsObj($ceRow['options']);
    $eventTeamObj['compid'] = $ceRow['CompID'];

//    $eventRow = getEventRow ( $ceRow['EventID'] );
    $egObj = eventGroup::getEgObj($ceRow['maxGroup']);
    $eventTeamObj['eventName'] = $egObj->name;

    $compRow = getCompRow($eventTeamObj['compid']);

    $desc = getProductDesc($compRow, $eventTeamObj);

    $postTitle = getProductTitle($compRow, $ceRow, $eventTeamObj);

    // get the event price taking into account any sale dates e.t.c
    $priceRow = getUseEventPrice($ceRow['PriceID']);

    $poptions = new stdClass();
    $poptions->price = '';
    $poptions->athletecnt = 0;
    if (isset($ceoptions->eventTeam->price)) {
        // Is the event price for the event or per athlete
        if (array_key_exists('athletes', $eventTeamObj) and strtolower($ceoptions->eventTeam->price) === E4S_PRICE_PER_ATHLETE) {
            // mulitple $priceRow props by number of athletes
            $athleteCnt = sizeof($eventTeamObj['athletes']);
            if ($athleteCnt === 0) {
                Entry4UIError(1020, 'Event is priced per athlete with NO athletes entered', 400, '');
            }

            $priceRow['saleprice'] = $priceRow['saleprice'] * $athleteCnt;
            $priceRow['price'] = $priceRow['price'] * $athleteCnt;
            $priceRow['usePrice'] = $priceRow['usePrice'] * $athleteCnt;
            $poptions = e4s_getOptionsAsObj($priceRow['options']);
            $poptions->price = E4S_PRICE_PER_ATHLETE;
            $poptions->athletecnt = $athleteCnt;
            $priceRow['options'] = $poptions;
        }
    }

    $wooProductObj = createWooProductWithPriceObj($eventTeamObj['compid'], $postTitle, $desc, $priceRow);
    createWooProductPurchaseNote($eventTeamObj, $wooProductObj);
    $wooProductObj->options = $poptions;
    return $wooProductObj;
}

function getProductTitle($compRow, $ceRow, $eventTeamObj) {
    $postTitle = $eventTeamObj['ceid'] . ' : ' . $compRow['name'] . ' ' . $ceRow['prettyStartDate'] . '. ' . $eventTeamObj['teamname'] . '. ' . $eventTeamObj['eventName'];
    if ($ceRow['prettyStartTime'] !== '00:00') {
        $postTitle .= ' at ' . $ceRow['prettyStartTime'];
    }
    $postTitle = ($postTitle);
    return $postTitle;
}

function getProductDesc($compRow, $eventTeamObj) {
    $desc = '';
    $desc .= '"compid":' . $eventTeamObj['compid'];
    $desc .= ',"ceid":' . $eventTeamObj['ceid'];
    $desc .= ',"compclubid":' . $compRow['compclubid'];
    $desc .= ',"locationid":' . $compRow['locationid'];
	if ( array_key_exists('autoEntry',$eventTeamObj)){
		$desc .= ',"autoEntry":1';
	}
//    if ( (int)$eventTeamObj['formno'] === -1) {
    if (e4s_teamHasStandardAthletes($eventTeamObj, $eventTeamObj['formno'])) {
        $desc .= ',"event":"' . $eventTeamObj['eventName'] . '"';
        $desc .= ',"teamName":"' . $eventTeamObj['teamname'] . '"';
        $desc .= ',"athletes":' . str_replace("'", "\'", json_encode($eventTeamObj['athletes'], JSON_NUMERIC_CHECK));
    }

    return $desc;
}

function getCompRow($compid) {
    $sql = 'SELECT compclubid, locationid, loc.location AS location , c.name competition, c.id compid, c.Date compdate, c.name, c.teamid compTeamID, c.options options
        FROM  ' . E4S_TABLE_LOCATION . ' loc
            , ' . E4S_TABLE_COMPETITON . ' c
        WHERE c.id = ' . $compid . ' 
        AND loc.id = c.locationid';

    $result = e4s_queryNoLog($sql);

    if ($result->num_rows === 0) {
        Entry4UIError(2026, 'Failed to get competition for event Team', 404, '');
    }
    return $result->fetch_assoc();
}

function getEventRow($eventID) {
    $eventRow = '';
    $sql = 'Select * from ' . E4S_TABLE_EVENTS . ' where id = ' . $eventID;

    $result = e4s_queryNoLog($sql);

    if ($result->num_rows > 0) {
        $eventRow = $result->fetch_assoc();
    } else {
        Entry4UIError(2027, 'Failed to get Event for team', 404, '');
    }
    return $eventRow;
}

function getCompEventRow($ceid) {
    $sql = "Select ce.*
            , date_format(eg.startdate,'%D %b %Y') prettyStartDate
            , date_format(eg.startdate,'%H:%i') prettyStartTime
            , ag.Name ageGroupName
        from " . E4S_TABLE_COMPEVENTS . ' ce,
             ' . E4S_TABLE_EVENTGROUPS . ' eg,
             ' . E4S_TABLE_AGEGROUPS . ' ag 
        where ce.id = ' . $ceid . '
        and   ce.maxgroup = eg.id
        and ag.id = ce.agegroupid';

    $result = e4s_queryNoLog($sql);

    if ($result->num_rows < 1) {
        Entry4UIError(1000, 'Failed to get the competition event', 400, '');
    }
    $ceRow = $result->fetch_assoc();
    $ceRow['ID'] = (int)$ceRow['ID'];
    $ceRow['CompID'] = (int)$ceRow['CompID'];
    $ceRow['EventID'] = (int)$ceRow['EventID'];
    $ceRow['AgeGroupID'] = (int)$ceRow['AgeGroupID'];
    $ceRow['PriceID'] = (int)$ceRow['PriceID'];
    $ceRow['maxGroup'] = (int)$ceRow['maxGroup'];
    $ceRow['IsOpen'] = (int)$ceRow['IsOpen'];
    $ceRow['maxathletes'] = (int)$ceRow['maxathletes'];
    $ceRow['split'] = (int)$ceRow['split'];
    $ceRow['options'] = e4s_addDefaultCompEventOptions($ceRow['options']);

    return $ceRow;
}

function createEntry4EventTeam($eventTeamObj, $wooProductObj, $discPriceObj) {
    $options = new stdClass();
    if ($wooProductObj->options) {
        $options = $wooProductObj->options;
    }
    if ( array_key_exists('resultsKey',$eventTeamObj) ){
        $options->resultsKey = $eventTeamObj['resultsKey'];
    }
	if ( array_key_exists('autoEntryautoEntry',$eventTeamObj) ){
        $options->autoEntry = $eventTeamObj['autoEntry'];
    }
    $options = e4s_removeDefaultTeamEntryOptions($options);
    $price = $wooProductObj->productPrice;

    if (isset($discPriceObj[$wooProductObj->productId])) {
        $price = $discPriceObj[$wooProductObj->productId]['usePrice'];
    }
    $useUserId = e4s_getUserID();
    if (array_key_exists('userId', $eventTeamObj)) {
        // used by auto entries to add a product to another user ( put in their cart )
        $useUserId = $eventTeamObj['userId'];
    }

    $insertPaid = E4S_ENTRY_NOT_PAID;
    $freeEntry = false;
    $compObj = e4s_getCompObj($eventTeamObj['compid']);
    if ( $compObj->markFreeEntryPaidCheck() ){
        $insertPaid = E4S_ENTRY_PAID;
        $price = 0;
        $options->fee = TRUE;
        $freeEntry = true;
    }
    $sql = 'insert into ' . E4S_TABLE_EVENTTEAMENTRIES . " (name,ceid,productid,paid,price,entitylevel, entityid, options,created,userid)
            values(
            trim('" . addslashes($eventTeamObj['teamname']) . "'),
            " . $eventTeamObj['ceid'] . ',
            ' . $wooProductObj->productId . ',
            ' . $insertPaid . ',
            ' . $price . ',
            ' . $eventTeamObj['entitylevel'] . ',
            ' . $eventTeamObj['entityid'] . ",
            '" . json_encode($options) . "',
            UTC_TIMESTAMP(),
            " . $useUserId . '
            )';

    $result = e4s_queryNoLog($sql);
    if ($result === FALSE) {
        Entry4UIError(2028, 'Failed to insert event team', 400, '');
    }
    $eventTeamObj['id'] = e4s_getLastID();

    createEventTeamMeta($eventTeamObj);
    $eventTeamObj['overrideEntry'] = $freeEntry;
    return $eventTeamObj;
}

function createEventTeamMeta($eventTeamObj) {
    $form = e4s_getFormType($eventTeamObj);
    if ($form === E4S_TEAM_TYPE_STD and sizeof($eventTeamObj['athletes']) === 0) {
        return $eventTeamObj;
    }

    if (e4s_teamHasStandardAthletes($eventTeamObj, $eventTeamObj['formno'])) {
        createEventTeamAthletes($eventTeamObj);
    } else {
        createEventTeamForm($eventTeamObj);
    }
    return TRUE;
}

function createEventTeamForm($eventTeamObj) {
    $form = e4s_getFormType($eventTeamObj);
    if ($form === E4S_TEAM_TYPE_STD) {
        Entry4UIError(1002, 'Attempt to get Form for Standard', 400, '');
    }
// get formDefs
    $formDefs = e4s_getFormDefsByField($eventTeamObj['formno'], 'label');
    if ($form !== E4S_TEAM_TYPE_LEAGUE) {
        foreach ($eventTeamObj['forminfo'] as $formInfo) {
            if (array_key_exists('label', $formInfo)) {
                $formDefRow = $formDefs[$formInfo['label']];
                $sql = 'insert into ' . E4S_TABLE_EVENTTEAMFORMS . ' (teamentryid,formdefid,value) 
                    values (
                       ' . $eventTeamObj ['id'] . ',
                       ' . $formDefRow['id'] . ",
                       '" . $formInfo['value'] . "'
                    )";

                $result = e4s_queryNoLog($sql);
                if ($result === FALSE) {
                    Entry4UIError(2001, 'Failed to insert event team form values', 404, '');
                }
            } else {
                Entry4UIError(1002, 'Form definition does not exist [' . $formInfo['label'] . ']', 404, '');
            }
        }
    } else {
        foreach ($eventTeamObj['formrows'] as $formrow) {
            $formDefRow = $formDefs[$formrow['eventDef']['name']];
            $athleteid = 0;
            if (array_key_exists('athlete', $formrow)) {
                if (array_key_exists('id', $formrow['athlete'])) {
                    $athleteid = $formrow['athlete']['id'];
                }
            }

//            $sql = "insert into " . E4S_TABLE_EVENTTEAMFORMS . " (teamentryid,formdefid,value)
//                values (
//                   " . $eventTeamObj ['id'] . ",
//                   " . $formDefRow['id'] . ",
//                   '" . $athleteid . "'
//                )";
            $sql = 'insert into ' . E4S_TABLE_EVENTTEAMATHLETE . ' (teamentryid, pos, athleteid)
            values (
            ' . $eventTeamObj ['id'] . ',
            ' . $formDefRow['pos'] . ',
            ' . $athleteid . '
            )';
            $result = e4s_queryNoLog($sql);
            if ($result === FALSE) {
                Entry4UIError(2002, 'Failed to insert event team form athletes', 404, '');
            }
        }
    }
}

function createEventTeamAthletes($eventTeamObj) {
    $athletes = $eventTeamObj['athletes'];
    foreach ($athletes as $key => $athlete) {
        $pos = (int)$key + 1;
        if (e4s_getFormType($eventTeamObj) === E4S_TEAM_TYPE_SCHOOL) {
            // Create the athlete
            $athlete['id'] = createEventTeamSchoolAthlete($athlete, $eventTeamObj ['id']);
        }
        $sql = 'insert into ' . E4S_TABLE_EVENTTEAMATHLETE . ' (teamentryid, pos, athleteid)
            values (
            ' . $eventTeamObj ['id'] . ',
            ' . $pos . ',
            ' . $athlete['id'] . '
            )';
        $result = e4s_queryNoLog($sql);
        if ($result === FALSE) {
            Entry4UIError(2001, 'Failed to insert event team athlete', 400, '"athlete":' . $athlete['id']);
        }
    }
    return TRUE;
}

function createEventTeamSchoolAthlete($athlete, $teamid) {
    $useObj = e4s_getUserObj(FALSE, TRUE);

    $clubid = 0;

    if (isset($useObj->clubs) and !empty($useObj->clubs)) {
        $clubid = (int)$useObj->clubs[0]->id;
    }
    global $conn;
    $athleteName = addslashes(trim($athlete['name']));

    $athleteNameArr = explode(' ', $athleteName);
    $firstname = $athleteNameArr[0];
    $surname = str_replace($firstname . ' ', '', $athleteName);

    $dob = '2000-01-01';
    $gender = 'F';
    $schoolid = $clubid;
    $aOptionsObj = new stdClass();
    $aOptionsObj->consent = $athlete['consent'];
    $aOptionsObj->teamId = $teamid;
    $aoptions = json_encode($aOptionsObj, JSON_NUMERIC_CHECK);

    $sql = 'Insert into ' . E4S_TABLE_ATHLETE . "(firstname, surname,dob,gender,classification,schoolid, clubid, activeenddate,type,options)
            values (
                '" . $firstname . "',
                '" . $surname . "',
                '" . $dob . "',
                '" . $gender . "',
                0,
                $schoolid,
                $clubid,
                '" . date('Y') . "-12-31',
                '" . E4S_TEAM_TYPE_SCHOOL . "',
                '" . $aoptions . "'
            )";
    e4s_queryNoLog($sql);
    return $conn->insert_id;
}
