<?php
require_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_clubCompResultReport($obj){
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $repObj = new e4s_clubCompResults($compId);
    $repObj->clubReport();
}

class e4s_clubCompResults {
    public e4sCompetition $compObj;
    public array $clubs;
    public array $eventGroups;

    public function __construct($compId) {
        $this->compObj = e4s_getCompObj($compId);
        $this->_getEventGroupInfo();
    }

    public function clubReport() {

    }

    private function _getEventGroupInfo() {
        $egIds = array();
        $egObjs = $this->compObj->getEGObjs();
        foreach ($egObjs as $key => $eventGroup) {
            $egIds[] = $eventGroup->id;
        }

        $sql = 'select rh.*
                from ' . E4S_TABLE_EVENTRESULTSHEADER . ' rh
                where egId in (' . implode(',', $egIds) . ')';
        $result = e4s_queryNoLog($sql);
        $eventGroups = array();
        while ($eg = $result->fetch_object(E4S_EVENTRESULTSHEADER_OBJ)) {
            $eg->egOptions = $egObjs[$eg->egId]->options;
            $eventGroups[$eg->egId] = $eg;
        }
    }

    private function _getClubs() {

    }
}