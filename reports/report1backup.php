<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
$expand = FALSE;
if (!isset($compId)) {
    $compid = 0;
}
if (!isset($orgid)) {
    $orgid = 0;
}
if ($orgid > 0 || $compid > 0) {
    $expand = TRUE;
}
if (userHasPermission('REPORT', $orgid, $compid) === FALSE) {
    echo 'You do not have permission to run this report';
    exit();
}
$config = e4s_getConfig();
$compRows = getReportComps($orgid, $compid);
$GLOBALS['grantCount'] = 0;
$GLOBALS['grandPaid'] = 0;
$GLOBALS['grandPaidActual'] = 0;
$GLOBALS['grandFee'] = 0;
echo '<table>';

foreach ($compRows as $compRow) {
    reportOnComp($compRow, $config, $expand);
    echo '<tr>';
    echo "<td colspan=3 style='border-bottom:2px black solid;'>";
    echo '</td>';
    echo '</tr>';
}
echo '<tr>';
echo "<td colspan='1'>";
outputDataWithLabel('Grand Entries', $GLOBALS['grantCount']);
echo '</td>';

echo "<td colspan='1'>";
outputDataWithLabel('Grand Amount', convertToCurrency($config, $GLOBALS['grandPaid']) . ' (' . convertToCurrency($config, $GLOBALS['grandPaidActual']) . ')');
echo '</td>';

echo "<td colspan='1'>";
outputDataWithLabel('Total Fees', convertToCurrency($config, $GLOBALS['grandFee']));
echo '</td>';

echo '</tr>';

echo '</table>';
exit();
function getReportComps($orgid, $compid) {
    $sql = "select *, date_format(date,'%D %b %Y') compDate, cc.club orgName
            from " . E4S_TABLE_COMPETITON . ' c,
                 ' . E4S_TABLE_COMPCLUB . ' cc 
            where cc.id = c.compclubid ';
    if ($orgid > 0) {
        $sql .= ' and c.compclubid = ' . $orgid;
    } else {
        if ($compid > 0) {
            $sql .= ' and c.id = ' . $compid;
        }
    }
    //$sql .= " and entriesopen > " . returnNow();
    $sql .= ' order by date desc';
    $result = e4s_queryNoLog($sql);
    return $result->fetch_all(MYSQLI_ASSOC);
}

function reportOnComp($compRow, $config, $expand) {
    echo "<tr style='background-color: lightgrey;'>";
    echo "<td colspan='2'>";
    $label = '';
    if (strpos($compRow['Name'], $compRow['orgName']) === FALSE) {
        $label = $compRow['orgName'] . ' ';
    }
    $label .= $compRow['Name'] . ' (' . $compRow['ID'] . '-' . $compRow['compclubid'] . ')';
    outputDataWithLabel('Competition', $label);
    echo '</td>';
    echo "<td colspan='1'>";
    outputDataWithLabel('Date', $compRow['compDate']);
    echo '</td>';
    echo '</tr>';

    echo '<tr>';
    $sql = 'select * from ' . E4S_TABLE_EVENTPRICE . '
        where compid = ' . $compRow['ID'];
    $priceResult = e4s_queryNoLog($sql);
    $priceRows = $priceResult->fetch_all(MYSQLI_ASSOC);
    echo "<td colspan='3'>";
    echo 'Defined prices :<br>';
    foreach ($priceRows as $priceRow) {
        if (is_null($priceRow['saleenddate'])) {
            echo 'Price : ' . $priceRow['description'] . ':' . $priceRow['saleprice'] . ' (' . $priceRow['salefee'] . ')<br>';
        } else {
            echo 'Std Price : ' . $priceRow['description'] . ':' . $priceRow['price'] . ' (' . $priceRow['fee'] . '). SaleEnd : ' . $priceRow['saleenddate'] . '. Sale Price : ' . $priceRow['saleprice'] . '(' . $priceRow['salefee'] . ')<br>';
        }
    }
    echo '</td>';
    echo '</tr>';

    echo '<tr>';
    echo "<td style='border-bottom: 1px black dashed;'>";
    if ($expand) {
        outputDataWithLabel('Order No', '');
    } else {
        outputDataWithLabel('Entry Count', '');
    }
    echo '</td>';
    echo "<td style='border-bottom: 1px black dashed;'>";
    outputDataWithLabel('Event Paid', '');
    echo '</td>';
    echo "<td style='border-bottom: 1px black dashed;'>";
    outputDataWithLabel('Fees', '');
    echo '</td>';

    echo '</tr>';

    $sql = 'SELECT count(e.*) count, e.price
            FROM ' . E4S_TABLE_ENTRIES . ' e,
            WHERE compEventID in ( select id from ' . E4S_TABLE_COMPEVENTS . '  where compid = ' . $compRow['ID'] . ' ) and paid = 1 group by price';

    if ($expand === TRUE) {
        $sql = 'SELECT e.orderid count, e.price pricepaid, ep.price, ep.saleprice, ep.salefee, ep.fee
            FROM ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTPRICE . ' ep
            WHERE ce.compid = ' . $compRow['ID'] . '
            and   e.compeventid = ce.id
            and   ce.priceid    = ep.id
            and   e.paid = 1 ';
    } else {
        $sql = 'SELECT count(e.id) count, e.price pricepaid, ep.price, ep.saleprice, ep.salefee, ep.fee
            FROM ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTPRICE . ' ep
            WHERE ce.compid = ' . $compRow['ID'] . '
            and   e.compeventid = ce.id
            and   ce.priceid    = ep.id
            and   e.paid = 1 
            group by e.price, ep.price, ep.saleprice, ep.salefee, ep.fee';
    }
//    echo $sql;
//    exit();
    $entryResult = e4s_queryNoLog($sql);
    $rows = $entryResult->fetch_all(MYSQLI_ASSOC);
    $totalCnt = 0;
    $totalAmount = 0;
    $totalAmountActual = 0;
    $totalFee = 0;
    foreach ($rows as $row) {
        echo '<tr>';
        echo '<td>';
        outputDataWithLabel('', $row['count']);
        echo '</td>';
        echo '<td>';
        if ($expand) {
            $subAmount = (float)$row['pricepaid'];
            $subAmountActual = (float)$row['pricepaid'];
            $totalCnt += 1;
        } else {
            $subAmount = ((double)$row['count'] * (float)$row['pricepaid']);
            $subAmountActual = ((double)$row['count'] * (float)$row['pricepaid']);
            $totalCnt += (double)$row['count'];
        }

        $totalAmount += $subAmount;
        $totalAmountActual += $subAmountActual;
        outputDataWithLabel('', convertToCurrency($config, $subAmount) . ' ( ' . $row['pricepaid'] . ' )');
        echo '</td>';
        echo '<td>';
        $subTotal = 0;
        $fee = 0;
        if ($expand) {
            $row['count'] = 1;
        }
        if ($row['pricepaid'] === $row['price']) {
            $subTotal = ((double)$row['count'] * (float)$row['fee']);
            $fee = $row['fee'];
        } else {
            if ((int)$row['pricepaid'] !== 0) {
                $subTotal = ((double)$row['count'] * (float)$row['salefee']);
                $fee = $row['salefee'];
            } else {
                $fee = '0.00';
                $subTotal = ((double)$row['count'] * (float)$row['salefee']);
                $fee = $row['salefee'];
            }
        }
        outputDataWithLabel('', convertToCurrency($config, $subTotal) . ' ( ' . $fee . ' )');
        $totalFee += $subTotal;
        echo '</td>';

        echo '</tr>';
    }

    echo '<tr>';
    echo "<td colspan='1'>";
    outputDataWithLabel('Total Entries', $totalCnt);
    $GLOBALS['grantCount'] += $totalCnt;
    echo '</td>';

    echo "<td colspan='1'>";
    outputDataWithLabel('Total Amount', convertToCurrency($config, $totalAmount));
    $GLOBALS['grandPaid'] += $totalAmount;
    $GLOBALS['grandPaidActual'] += $totalAmountActual;
    echo '</td>';

    echo "<td colspan='1'>";
    outputDataWithLabel('Total Fees', convertToCurrency($config, $totalFee));
    $GLOBALS['grandFee'] += $totalFee;
    echo '</td>';

    echo '</tr>';

}

function outputDataWithLabel($label, $data) {
    if ($label !== '') {
        echo $label;
        if ($data !== '') {
            echo ' : ';
        }
    }
    echo $data;
}