<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
$Permission = 'ukfinance';
if (userHasPermission($Permission, null, null) === FALSE) {
    echo 'You do not have permission to run this report';
    exit();
}

$config = e4s_getConfig();
$title = 'Date From ' . date('d/m/Y', strtotime('-' . $days . ' days')) . ' to Today';
$fee = 0.59;
if ($config['currency'] === '£') {
    $fee = 0.5;
}
echo '<link rel="stylesheet" href="' . E4S_PATH . '/css/pqgrid6.min.css">';
echo '<link rel="stylesheet" href="' . E4S_PATH . '/css/pqgrid6.ui.min.css">';
echo '<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.3/themes/' . E4S_JQUERY_THEME . '/jquery-ui.css">';
echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> ';
echo '<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>';
echo '<script type="text/javascript" src="' . E4S_PATH . '/js/pqgrid6.min.js"></script>';
echo '<script type="text/javascript" src="' . E4S_PATH . '/js/jszip.min.js"></script>';
echo '<script type="text/javascript" src="' . E4S_PATH . '/js/FileSaver.min.js"></script>';
echo '<div id="grid_table" style="margin:auto;"></div>';

$sql = "SELECT p.id orderno, date_format(date(post_date),'%D %b %Y') orderdate, count(e.id) linecnt
        FROM " . E4S_TABLE_POSTS . ' p,
             ' . E4S_TABLE_ENTRIES . " e
        WHERE post_type = '" . WC_POST_ORDER . "' 
        and post_status = 'wc-completed' 
        and date(post_date) >= CURRENT_DATE - INTERVAL $days  DAY
        and p.id = e.orderid
                group by p.id, date(post_date)
        order by post_date desc";
$result = e4s_queryNoLog('Finance3' . E4S_SQL_DELIM . $sql);
$rows = $result->fetch_all(MYSQLI_ASSOC);
echo "<table style='display:inline;' id='finance'>";
echo '<tr>';
echo '<td>Date</td>';
echo '<td>Order</td>';
echo '<td>Order Total</td>';
echo '<td>Order Lines</td>';
echo '<td>Strip Fee</td>';
echo '<td>Strip Net</td>';
echo '<td>Profit</td>';
echo '</tr>';
foreach ($rows as $row) {
    $orderTotal = 0;
    $stripeFee = 0;
    $stripeNet = 0;
    $costSql = 'select *
                from ' . E4S_TABLE_POSTMETA . '
                where post_id = ' . $row['orderno'] . "
                and meta_key in ('_order_total','_stripe_fee','_stripe_net')
                ";
    $costResult = mysqli_query($conn, $costSql);

    if ($costResult->num_rows > 1) {
        $costRows = $costResult->fetch_all(MYSQLI_ASSOC);
        foreach ($costResult as $costRow) {
            if ($costRow['meta_key'] === '_order_total') {
                $orderTotal = $costRow['meta_value'];
            }
            if ($costRow['meta_key'] === '_stripe_fee') {
                $stripeFee = $costRow['meta_value'];
            }
            if ($costRow['meta_key'] === '_stripe_net') {
                $stripeNet = $costRow['meta_value'];
            }
        }
    }

    echo '<tr>';
    echo '<td>' . $row['orderdate'] . '</td>';
    echo '<td>' . $row['orderno'] . '</td>';
    echo '<td>' . $orderTotal . '</td>';
    echo '<td>' . $row['linecnt'] . '</td>';
    echo '<td>' . $stripeFee . '</td>';
    echo '<td>' . $stripeNet . '</td>';
    if (E4S_CURRENT_DOMAIN === E4S_AAI_DOMAIN) {
        echo '<td>' . (((int)$row['linecnt'] * $fee)) . '</td>';
    } else {
        echo '<td>' . (((int)$row['linecnt'] * $fee) - $stripeFee) . '</td>';
    }

    echo '</tr>';
}
echo '</table>';
echo '<script>';
echo '
var tbl = $("#finance");          
var tblObj = $.paramquery.tableToArray(tbl);
var colM = [
    {
        align:"left",
        dataIndx:0,
        dataType:"string",
        title:"Date",
        width:200
    },
    {
        align:"left",
        dataIndx:1,
        dataType:"integer",
        title:"Order Number",
        width:100
    },
    {
        align:"right",
        dataIndx:2,
        dataType:"float",
        title:"Order Total",
        width:200,
        format: \'' . $config['currency'] . '##,###.00\',
        summary: {
            type: "Sum"
        }
    },
    {
        align:"right",
        dataIndx:3,
        dataType:"integer",
        title:"Line Count",
        width:150
    },
    {
        align:"right",
        dataIndx:4,
        dataType:"float",
        title:"Stripe Fee",
        width:200,
        format: \'' . $config['currency'] . '##,###.00\',
        summary: {
            type: "Sum"
        }
    },
    {
        align:"right",
        dataIndx:5,
        dataType:"float",
        title:"Stripe Net",
        width:200,
        format: \'' . $config['currency'] . '##,###.00\',
        summary: {
            type: "Sum"
        }
    },
    {
        align:"right",
        dataIndx:6,
        dataType:"float",
        title:"Profit",
        width:200,
        format: \'' . $config['currency'] . '##,###.00\',
        summary: {
            type: "Sum"
        }
    }
];
var newObj = { 
    width: "100%", 
    height: "100%", 
    title: "' . $title . '", 
    flexWidth: true ,
    numberCell: {show: false},
    dataModel:{ 
        data: tblObj.data 
    },
    toolbar: {
        items: [
            {
                type: \'select\',
                label: \'Format: \',                
                attr: \'id="export_format"\',
                options: [{ xlsx: \'Excel\', csv: \'Csv\', htm: \'Html\', json: \'Json\'}]
            },
            {
                type: \'button\',
                label: \'Export\',
                icon: \'ui-icon-arrowthickstop-1-s\',
                listener: function () {
                    var format = $("#export_format").val(),                            
                        blob = this.exportData({
                            format: format,        
                            nopqdata: true, //applicable for JSON export.                        
                            render: true
                        });
                    if(typeof blob === "string"){                            
                        blob = new Blob([blob]);
                    }
                    saveAs(blob, "pqGrid." + format );
                }
            }
        ]
    },
    colModel: colM,
//    pageModel:{ 
//        rPP: 20, 
//        type: "local" 
//    },
    groupModel: {
        on: true,
        dataIndx: [0],                       
        agg:{2:\'sum\', 3:\'sum\', 4:\'sum\',5:\'sum\',6:\'sum\'},
        grandSummary: true,           
        summaryInTitleRow: \'all\',
        titleInFirstCol: true, 
        fixCols: false, 
        indent: 20,
        collapsed: [true],
        title: [
            "{0}"
        ]
    }
};
var obj = $("#grid_table").pqGrid(newObj);
tbl.css("display", "none");
';

echo '</script>';
exit();