<?php
const E4S_COMMISSION_EMAIL = 'support@entry4sports.com';
const E4S_FIRST_COL_COUNT = 832;
function getFinanceData($orgId, $fromCompId, $toCompId, $compFromDate, $compToDate, $justTickets, $details, $fromOrderId, $toOrderId, $fromDate, $toDate, $custId) {
    $config = e4s_getConfig();

    $defaultAO = $config['defaultao']['code'];
    $useStripeConnect = FALSE;

    if (isset($config['options']->useStripeConnect)) {
        $useStripeConnect = $config['options']->useStripeConnect;
    }

    $showStripeCosts = isAdminUser();
    $sql = 'select c.id compId,
                   cc.stripeUserId,
                   cc.club organisation
            from ' . E4S_TABLE_COMPETITON . ' c,
                 ' . E4S_TABLE_COMPCLUB . ' cc
            where c.id > 0 
            and c.compclubid = cc.id ';
    if ($orgId > 0) {
        $sql .= ' and c.compclubid = ' . $orgId;
    }
    if ($compFromDate !== '') {
        $sql .= " and c.date >= '" . $compFromDate . "'";
    }
    if ($compToDate !== '') {
        $sql .= " and c.date <= '" . $compToDate . "'";
    }
    if ($fromCompId > 0 and $toCompId > 0 and $fromCompId > $toCompId) {
        // swap compIds
        $saveId = $fromCompId;
        $fromCompId = $toCompId;
        $toCompId = $saveId;
    }

    if ($fromCompId === $toCompId) {
        $sql .= ' and c.id = ' . $fromCompId;
    } elseif ($fromCompId > 0 and $toCompId > 0) {
        $sql .= ' and c.id between ' . $fromCompId . ' and ' . $toCompId;
    } elseif ($fromCompId > 0) {
        $sql .= ' and c.id >= ' . $fromCompId;
    } else {
        $sql .= ' and c.id <= ' . $toCompId;
    }
    $sql .= ' order by c.id desc';

    $result = e4s_queryNoLog($sql);
    $comps = array();
	$minCompId = 9999;
	$maxCompId = 0;
    while ($obj = $result->fetch_object()) {
		$thisCompId = (int)$obj->compId;
		if ($thisCompId < $minCompId){
			$minCompId = $thisCompId;
		}
		if ($thisCompId > $maxCompId){
			$maxCompId = $thisCompId;
		}
        $compObj = e4s_GetCompObj($thisCompId);
        if (isAdminUser()) {
            $addComp = TRUE;
        } else {
            $addComp = $compObj->isOrganiser();
        }
        if ($addComp) {
            $compObj->compId = (int)$compObj->getID();
            $compObj->stripeUserId = (int)$obj->stripeUserId;
            $compObj->value = 0;
            $compObj->refunded = 0;
            $compObj->errorValue = 0;
            $compObj->noCommissionValue = 0;
            $compObj->e4sExpectedValue = 0;
            $compObj->custExpectedValue = 0;
            $compObj->stripeValue = 0;
            $compObj->commissions = array();
            $compObj->competition = str_replace(',', ' ', $compObj->getName());
            $compObj->organisation = $obj->organisation;
            $comps [$compObj->compId] = $compObj;
        }
    }
	$fromCompId = $minCompId;
	$toCompId = $maxCompId;

    $userIds = array();
    $commissions = array();

    if ($useStripeConnect) {
        $sql = 'select id,
                    user_id userId,
                    order_item_id orderItemId,
                    order_id orderId,
                    commission,
                    commission_status status
            from ' . E4S_TABLE_YITHCOMMISSIONS;
        $where = '';
        if ($fromOrderId > 0) {
            $where = ' where order_id >= ' . $fromOrderId;
        }
        if ($toOrderId > 0) {
            if ($where === '') {
                $where = ' where ';
            } else {
                $where .= ' and ';
            }
            $where .= ' order_id <= ' . $toOrderId;

        }
        $result = e4s_queryNoLog($sql . $where);
        while ($obj = $result->fetch_object()) {
            $obj->id = (int)$obj->id;
            $obj->userId = (int)$obj->userId;
            $userIds[$obj->userId] = $obj->userId;
            $obj->orderItemId = (int)$obj->orderItemId;
            $obj->orderId = (int)$obj->orderId;
            $obj->commission = (float)$obj->commission;
            if (!array_key_exists($obj->orderItemId, $commissions)) {
                $commissions[$obj->orderItemId] = array();
            }
            $commissions[$obj->orderItemId][$obj->userId] = $obj;
        }
    }
    $users = array();
    if (!empty($userIds)) {
        $sql = 'select u.id, u.user_email
            from ' . E4S_TABLE_USERS . ' u
            where id in (' . implode(',', $userIds) . ')';
        $result = e4s_queryNoLog($sql);

        while ($obj = $result->fetch_object()) {
            $obj->id = (int)$obj->id;
            $users[$obj->id] = $obj->user_email;
        }
    }
    $sql = 'select id,
                   compId
            from ' . E4S_TABLE_COMPEVENTS . '
            where ';
    if ($fromCompId === $toCompId) {
        $sql .= ' compid = ' . $fromCompId;
    } elseif ($fromCompId > 0 and $toCompId > 0) {
        $sql .= ' compid between ' . $fromCompId . '  and ' . $toCompId . ' ';
    } elseif ($fromCompId > 0) {
        $sql .= ' compid >= ' . $fromCompId;
    } else {
        $sql .= ' compid <= ' . $toCompId;
    }
    $ceRecords = array();
    $result = e4s_queryNoLog($sql);

    while ($obj = $result->fetch_object()) {
        $ceRecords[(int)$obj->id] = (int)$obj->compId;
    }

    $tickets = array();
    $ticketSql = 'select id
                         ,orderId
                         ,itemId
                         ,compId
                         ,userId
                  from ' . E4S_TABLE_TICKET . '
                  where orderid is not null
                  and compid between ' . $fromCompId . ' and ' . $toCompId;
    $ticketResult = e4s_queryNoLog($ticketSql);
    while ($ticket = $ticketResult->fetch_object()) {
        $ticket->id = (int)$ticket->id;
        $ticket->orderId = (int)$ticket->orderId;
        $ticket->itemId = (int)$ticket->itemId;
        $ticket->compId = (int)$ticket->compId;
        $ticket->userId = (int)$ticket->userId;
        if (!array_key_exists($ticket->orderId, $tickets)) {
            $ticketItems = array();
            $tickets[$ticket->orderId] = $ticketItems;
        }
        $tickets[$ticket->orderId][$ticket->itemId] = $ticket;
    }
    $itemSql = 'select  wci.order_id orderId
                        ,wci.order_item_id orderItemId
                        ,wci.order_item_name orderItemName
                        ,wcim.meta_value liveValue
                        ,p.post_status orderStatus
                        ,p.post_date orderDate
                        ,substring(p1.post_content, 11, (locate(",", p1.post_content) - 11)) as compId
                        ,pm.meta_value custId
                        ,wcim2.meta_value productId
                        ,pm1.meta_value stock';
    if (!$useStripeConnect) {
        $itemSql .= '   ,pm2.meta_value stripeFee
                        ,pm3.meta_value stripeValue';
    }
    $itemSql .= addPreSQLFromStatement($useStripeConnect);
    $itemSql .= "where order_item_type = 'line_item'
                and p.id = wci.order_id
                and p.id = pm.post_id
                and pm1.post_id = wcim2.meta_value
                and pm1.meta_key = '_stock'
                and pm.meta_key = '_customer_user'
                and p.post_status in( '" . WC_ORDER_PAID . "', '" . WC_ORDER_REFUNDED . "')
                and wci.order_item_id = wcim.order_item_id
                and wci.order_item_id = wcim2.order_item_id
                and p1.id = wcim2.meta_value
                and substring(p1.post_content, 11, (locate(',', p1.post_content) - 11)) between " . $fromCompId . " and " . $toCompId . "
                and wcim.meta_key = '_line_total'
                and wcim2.meta_key = '_product_id'";
    if (!$useStripeConnect) {
        $itemSql .= " and p.id = pm2.post_id
                      and pm2.meta_key = '_stripe_fee'
                      and p.id = pm3.post_id
                      and pm3.meta_key = '_stripe_net'";
    }
    if ($custId > 0) {
        $itemSql .= " and pm.meta_value = '" . $custId . "'";
    }
    if ($fromOrderId > 0) {
        $itemSql .= ' and p.id >= ' . $fromOrderId;
    }
    if ($toOrderId > 0) {
        $itemSql .= ' and p.id <= ' . $toOrderId;
    }
    if ($fromDate !== '') {
        $itemSql .= " and p.post_date >= '" . $fromDate . "'";
    }
    if ($toDate !== '') {
        $itemSql .= " and p.post_date <= '" . $toDate . "'";
    }
// $itemSql is preSQL as it now needs both live and archive tables with a union
	$useSql = getItemFromSQL($itemSql,false);
	$useSql .= ' union all ';
	$useSql .= getItemFromSQL( $itemSql, true);

    $result = e4s_queryNoLog($useSql);
    $detailHeader = FALSE;
    $output = '';
    $processedComps = array();
    $compDetail = array();
    while ($obj = $result->fetch_object()) {
        $obj->orderId = (int)$obj->orderId;
        $obj->orderItemId = (int)$obj->orderItemId;
        $obj->liveValue = (float)$obj->liveValue;
        $obj->custId = (int)$obj->custId;
        if (!$useStripeConnect) {
            $obj->stripeValue = (float)$obj->stripeValue;
            $obj->stripeFee = (float)$obj->stripeFee;
        }
//        $compId = 0;
        if ((int)$obj->stock > 1) {
//            echo "Ticket : " . $obj->orderId;
            if (array_key_exists($obj->orderId, $tickets)) {
//                echo " - order";
                $ticketArr = $tickets[$obj->orderId];
                if (array_key_exists($obj->orderItemId, $ticketArr)) {
//                    echo " - item " . $obj->orderItemId;
                    $ticketObj = $tickets[$obj->orderId][$obj->orderItemId];
//                    $compId = $ticketObj->compId;
                    $obj->itemName = "Ticket:" . $obj->orderItemName;
                }
            }
//            echo " - value : " . $obj->liveValue . "\n";
        } elseif ($justTickets) {
            // do nothing
        } else {

            $nameArr = preg_split('~ :~', $obj->orderItemName);
			if(sizeof($nameArr) < 2){
				var_dump($obj);
				var_dump($useSql);
				exit;
			}
            $ceId = (int)$nameArr[0];
            $obj->itemName = $nameArr[1];
            if (!array_key_exists($ceId, $ceRecords)) {
                // event changed and not found so check entry CEID
                $sql = 'select e.compEventId
                    from ' . E4S_TABLE_WCORDERITEMMETA . ' wcim,
                         ' . E4S_TABLE_ENTRIES . ' e
                    where order_item_id = ' . $obj->orderItemId . "
                    and meta_key = '" . WC_POST_PRODUCT_ID . "'
                    and e.variationid = wcim.meta_value";
            } else {
//                $compId = (int)$ceRecords [$ceId];
            }
        }
		$compId = (int)$obj->compId;
        if ($compId !== 0) {
//            if (($compId < $fromCompId and $fromCompId > 0) or ($compId > $toCompId and $toCompId > 0)) {
//                continue;
//            }

            if (!array_key_exists($compId, $comps)) {
                // failed to find competition
                continue;
            }

            $compObj = e4s_GetCompObj($compId);
            if (!$compObj->isOrganiser()) {
                continue;
            }

            // only allow e4s and AAI to access National
            if ($compObj->getOrganisationName() === 'National') {
                if (!isE4SUser() and !isAdminUser()) {
                    continue;
                }
            }
            if ($details) {
                if (!$detailHeader) {
                    $detailHeader = TRUE;
                    $output = 'Comp No.,Competition,Date,Order Id,Order Date,Reference,Line Value,Refunded Value,';
                    if ($showStripeCosts) {
                        $output .= 'Stripe Value,';
                    }
                    $output .= "Unallocated,E4S Actual,E4S Expected,Customer,Customer Actual,Customer Expected\n";

                }
                if (!array_key_exists($compId, $compDetail)) {
                    $compDetail[$compId] = '';
                }
                $compDetail[$compId] .= aaiOutputDetail($obj, $compId, $comps[$compId], $useStripeConnect, $defaultAO, $commissions, $users);
            } else {
                aaiAddToSummary($obj, $useStripeConnect, $compId, $comps[$compId], $defaultAO, $commissions);
            }
            $processedComps[$compId] = $comps[$compId];
        }
    }

    if (!$details) {
        $output .= aaiOutputSummary($useStripeConnect, $users, $processedComps);
    } else {
        asort($compDetail);
        foreach ($compDetail as $detail) {
            $output .= $detail;
        }
    }
    return $output;
}
function addPreSQLFromStatement($useStripeConnect){
	$itemSql = ' from {E4S_TABLE_WCORDERITEMS} wci
                     ,{E4S_TABLE_WCORDERITEMMETA} wcim
                     ,{E4S_TABLE_WCORDERITEMMETA} wcim2
                     ,{E4S_TABLE_POSTS} p
                     ,{E4S_TABLE_POSTS} p1
                     ,{E4S_TABLE_POSTMETA} pm 
                     ,{E4S_TABLE_POSTMETA} pm1 ';
	if (!$useStripeConnect) {
		$itemSql .= ',{E4S_TABLE_POSTMETA} pm2
                     ,{E4S_TABLE_POSTMETA} pm3 ';
	}
	return $itemSql;
}
function getItemFromSQL($sql, $archive){
	$table_wcOrderItems = E4S_TABLE_WCORDERITEMS;
	$table_wcOrderItemMeta = E4S_TABLE_WCORDERITEMMETA;
	$table_posts = E4S_TABLE_POSTS;
	$table_postMeta = E4S_TABLE_POSTMETA;
	if ( $archive ){
		$archiveDb = e4sArchive::archiveName() . ".";
		$table_wcOrderItems = $archiveDb . E4S_TABLE_WCORDERITEMS_ARCHIVE;
		$table_wcOrderItemMeta = $archiveDb . E4S_TABLE_WCORDERITEMMETA_ARCHIVE;
		$table_posts = $archiveDb . E4S_TABLE_POSTS_ARCHIVE;
		$table_postMeta = $archiveDb . E4S_TABLE_POSTMETA_ARCHIVE;
	}
	$sql = preg_replace('~{E4S_TABLE_WCORDERITEMS}~', $table_wcOrderItems, $sql);
	$sql = preg_replace('~{E4S_TABLE_WCORDERITEMMETA}~', $table_wcOrderItemMeta, $sql);
	$sql = preg_replace('~{E4S_TABLE_POSTS}~', $table_posts, $sql);
	$sql = preg_replace('~{E4S_TABLE_POSTMETA}~', $table_postMeta, $sql);

	return $sql;
}
function formatCompName($compName) {
    $compName = preg_replace('~#~', 'No.', $compName);
    return $compName;
}

function aaiOutputDetail($obj, $compId, $comp, $useStripeConnect, $defaultAO, $commissions, $users) {
    $showStripeCosts = isAdminUser();
    $compObj = e4s_GetCompObj($compId);
    $output = '';
    $output .= $compId . ',';
    $output .= formatCompName($comp->competition) . ',';
    $output .= $comp->getDate() . ',';
    $date = date_create($comp->getDate());
    $dateStr = date_format($date, 'jS M Y');

    $output .= $obj->orderId . ',';
    $output .= $obj->orderDate . ',';
    $itemName = $obj->itemName;
    $itemNameArr = preg_split('~' . $dateStr . '.~', $itemName);
    if (sizeof($itemNameArr) > 1) {
        $itemName = trim($itemNameArr[1]);
    }
    $output .= ',' . str_replace(',', '.', $itemName);

    if ($obj->orderStatus === WC_ORDER_PAID) {
        $output .= $obj->liveValue . ',0,';
        if ($useStripeConnect) {
            $noCommissionValue = 0;
            $e4sCommissionValue = 0;
            $custCommissionValue = 0;
            $commissionObj = $compObj->getPriceSplit($defaultAO, $obj->liveValue);
            $stripeValue = $commissionObj->stripeValue;
            $e4sExpectedCommissionValue = $commissionObj->e4sValue;
            $custExpectedCommissionValue = $commissionObj->custValue;
            $cust2CommissionValue = 0;
            $customer = '';
            $customer2 = '';
            if (!array_key_exists($obj->orderItemId, $commissions)) {
                $noCommissionValue = $obj->liveValue;
            } else {
                $commission = $commissions[$obj->orderItemId];
                foreach ($commission as $commissionUser => $commissionObj) {
                    if ($commissionObj->status === 'sc_transfer_success') {
                        if ($users[$commissionUser] === E4S_COMMISSION_EMAIL) {
                            $e4sCommissionValue = $commissionObj->commission;
                        } else {
                            if ($customer === '') {
                                $customer = $users[$commissionUser];
                                $custCommissionValue = $commissionObj->commission;
                            } else {
                                $customer2 = $users[$commissionUser];
                                $cust2CommissionValue = $commissionObj->commission;
                            }
                        }
                    }
                }
            }
            if ($showStripeCosts) {
                $output .= sprintf('%.2f', $stripeValue) . ',';
            } else {
                $e4sCommissionValue += $stripeValue;
                $e4sExpectedCommissionValue += $stripeValue;
            }
            $output .= sprintf('%.2f', $noCommissionValue) . ',';
            $output .= sprintf('%.2f', $e4sCommissionValue) . ',';
            $output .= sprintf('%.2f', $e4sExpectedCommissionValue) . ',';
            if (!$compObj->isNational) {
                if ($customer === '') {
                    $customer = 'Unknown';

                    if (!array_key_exists($comp->stripeUserId, $users)) {
                        if ($comp->stripeUserId > 0) {
                            $sql = 'select u.id, u.user_email
                            from ' . E4S_TABLE_USERS . ' u
                            where id = ' . $comp->stripeUserId;
                            $result = e4s_queryNoLog($sql);
                            while ($obj = $result->fetch_object()) {
                                $obj->id = (int)$obj->id;
                                $users[$obj->id] = $obj->user_email;
                            }
                        }
                    }
                    if (array_key_exists($comp->stripeUserId, $users)) {
                        $customer = $users[$comp->stripeUserId];
                    }
                }
                $output .= $customer . ',';
                $output .= sprintf('%.2f', $custCommissionValue) . ',';
                $output .= sprintf('%.2f', $custExpectedCommissionValue);
                if ($customer2 !== '') {
                    $output .= ',' . $customer2 . ',';
                    $output .= sprintf('%.2f', $cust2CommissionValue);
                }
            }
        }
    } else {
        $output .= '0,' . $obj->liveValue;
    }
    $output .= "\n";
    return $output;
}

function aaiAddToSummary($obj, $useStripeConnect, $compId, &$comp, $defaultAO, $commissions) {
    if ($obj->orderStatus === WC_ORDER_PAID) {
        $comp->value += $obj->liveValue;

        $compObj = e4s_GetCompObj($compId, FALSE);
        $expectedObj = $compObj->getPriceSplit($defaultAO, $obj->liveValue);
        $comp->stripeValue += $expectedObj->stripeValue;
        $comp->custExpectedValue += $expectedObj->custValue;

        if ($useStripeConnect) {
            $comp->e4sExpectedValue += $expectedObj->e4sValue;
            if (!array_key_exists($obj->orderItemId, $commissions)) {
                $comp->noCommissionValue += $obj->liveValue;
            } else {
                $commission = $commissions[$obj->orderItemId];
                foreach ($commission as $commissionUser => $commissionObj) {
                    if (!array_key_exists($commissionUser, $comp->commissions)) {
                        $comp->commissions[$commissionUser] = 0;
                    }
                    if ($commissionObj->status === 'sc_transfer_success') {
                        $comp->commissions[$commissionUser] += $commissionObj->commission;
                        $comp->commissions[$commissionUser] = round($comp->commissions[$commissionUser], 2);
                    }
                }
            }
        } else {
//            $e4sValue = $obj->liveValue - $comp->custExpectedValue;
            $comp->e4sExpectedValue += $expectedObj->e4sValue;
            //$comp->e4sExpectedValue += $expectedObj->stripeValue;
        }
        $comp->e4sExpectedValue = round($comp->e4sExpectedValue, 2);
        $comp->custExpectedValue = round($comp->custExpectedValue, 2);
        $comp->stripeValue = round($comp->stripeValue, 2);
    } else {
        $comp->refunded += $obj->liveValue;
    }
}

function aaiOutputSummary($useStripeConnect, $users, $comps) {
    $showStripeCosts = isAdminUser();
    $cols = array();
    $chrCount = 1;
    define('E4S_SHEET_COMP_ID', $chrCount++);
    $cols[E4S_SHEET_COMP_ID] = (object)['name' => 'Competition ID'];
    define('E4S_SHEET_ORG_NAME', $chrCount++);
    $cols[E4S_SHEET_ORG_NAME] = (object)['name' => 'Organisation'];
    define('E4S_SHEET_COMP_NAME', $chrCount++);
    $cols[E4S_SHEET_COMP_NAME] = (object)['name' => 'Name'];
    define('E4S_SHEET_COMP_DATE', $chrCount++);
    $cols[E4S_SHEET_COMP_DATE] = (object)['name' => 'Date'];
    define('E4S_SHEET_GROSS_TAKEN', $chrCount++);
    $cols[E4S_SHEET_GROSS_TAKEN] = (object)['name' => 'Gross Taken'];
    define('E4S_SHEET_GROSS_REFUNDED', $chrCount++);
    $cols[E4S_SHEET_GROSS_REFUNDED] = (object)['name' => 'Gross Refunded'];
    if ($showStripeCosts) {
        define('E4S_SHEET_STRIPE_COSTS', $chrCount++);
        $cols[E4S_SHEET_STRIPE_COSTS] = (object)['name' => 'Stripe Costs', 'stripe' => TRUE];
    }
    define('E4S_SHEET_NO_COMMISSION', $chrCount++);
    $cols[E4S_SHEET_NO_COMMISSION] = (object)['name' => 'No Commission Value ( Gross )'];
    define('E4S_SHEET_E4S_COMMISSION', $chrCount++);
    $cols[E4S_SHEET_E4S_COMMISSION] = (object)['name' => 'E4S Commission'];
    define('E4S_SHEET_E4S_EXPECTED_COMMISSION', $chrCount++);
    $cols[E4S_SHEET_E4S_EXPECTED_COMMISSION] = (object)['name' => 'E4S Expected Commission'];
    define('E4S_SHEET_E4S_DIFF', $chrCount++);
    $cols[E4S_SHEET_E4S_DIFF] = (object)['name' => 'E4S Credit-Debit'];
    define('E4S_SHEET_CUSTOMER', $chrCount++);
    $cols[E4S_SHEET_CUSTOMER] = (object)['name' => 'Customer'];
    define('E4S_SHEET_CUSTOMER1_COMMISSION', $chrCount++);
    $cols[E4S_SHEET_CUSTOMER1_COMMISSION] = (object)['name' => 'Customer Commission'];
    define('E4S_SHEET_CUSTOMER1_EXPECTED_COMMISSION', $chrCount++);
    $cols[E4S_SHEET_CUSTOMER1_EXPECTED_COMMISSION] = (object)['name' => 'Customer Expected Commission'];
    define('E4S_SHEET_CUSTOMER1_DIFF', $chrCount++);
    $cols[E4S_SHEET_CUSTOMER1_DIFF] = (object)['name' => 'Customer Credit-Debit'];
    if ($useStripeConnect) {
        define('E4S_SHEET_CUSTOMER2', $chrCount++);
        $cols[E4S_SHEET_CUSTOMER2] = (object)['name' => 'Customer2'];
        define('E4S_SHEET_CUSTOMER2_COMMISSION', $chrCount++);
        $cols[E4S_SHEET_CUSTOMER2_COMMISSION] = (object)['name' => 'Customer2 Commission'];
        define('E4S_SHEET_CUSTOMER2_EXPECTED_COMMISSION', $chrCount++);
        $cols[E4S_SHEET_CUSTOMER2_EXPECTED_COMMISSION] = (object)['name' => 'Customer2 Expected Commission'];
        define('E4S_SHEET_CUSTOMER2_DIFF', $chrCount++);
        $cols[E4S_SHEET_CUSTOMER2_DIFF] = (object)['name' => 'Customer2 Credit-Debit'];
    }
    define('E4S_SHEET_ENTRIES_TOTAL', $chrCount++);
    $cols[E4S_SHEET_ENTRIES_TOTAL] = (object)['name' => 'Entries Gross'];
    define('E4S_SHEET_ENTRIES_DIFF', $chrCount++);
    $cols[E4S_SHEET_ENTRIES_DIFF] = (object)['name' => 'Gross Taken - Entries Gross'];
    define('E4S_SHEET_ENTRIES_START_COL', $chrCount);
    $cols[E4S_SHEET_ENTRIES_START_COL] = (object)['name' => 'Entry Counts / Price'];

    $output = '';
    $sep = '';
    foreach ($cols as $col) {
        $output .= $sep;
        $output .= $col->name;
        $sep = ',';
    }

    $output .= "\n";
    asort($comps);
    $row = 2;
    foreach ($comps as $compId => $compObj) {
        $grossTaken = $compObj->value;
        $refundedValue = $compObj->refunded;
        $stripeCosts = $compObj->stripeValue;
        $noCommissionOutput = ',' . sprintf('%.2f', $compObj->noCommissionValue);
        $e4sCommission = 0;
        $e4sExpectedCommission = $compObj->e4sExpectedValue;
        $cust1 = '';
        $cust1Commission = 0;
        $cust1ExpectedCommission = 0;
        $cust2 = '';
        $cust2Commission = 0;
        $cust2ExpectedCommission = 0;
        $customerCommissionOutput = '';
        $customer2CommissionOutput = ',,,,';
        $competitionObj = e4s_GetCompObj($compId);
        $output .= $compId . ',' . $compObj->organisation . ',' . formatCompName($compObj->competition) . ',' . $compObj->getDate() . ',' . sprintf('%.2f', $grossTaken) . ',' . $refundedValue;
        if ($useStripeConnect) {
            foreach ($compObj->commissions as $commUser => $commission) {
                if ($users[$commUser] === E4S_COMMISSION_EMAIL) {
                    $e4sCommission = $commission;
                    if (!$showStripeCosts) {
                        $e4sCommission += $stripeCosts;
                        $e4sExpectedCommission += $stripeCosts;
                    }
                } else {
                    if ($cust1 === '') {
                        $cust1 = $users[$commUser];
                        $cust1Commission = $commission;
                        $cust1ExpectedCommission = $compObj->custExpectedValue;
                    } else {
                        $cust2 = $users[$commUser];
                        $cust2Commission = $commission;
                        $cust2ExpectedCommission = $compObj->custExpectedValue;
                    }
                }
            }

            if ($cust1 !== '') {
                $customerCommissionOutput = ',' . $cust1 . ',' . sprintf('%.2f', $cust1Commission) . ',' . sprintf('%.2f', $cust1ExpectedCommission);
                $customerCommissionOutput .= ',=' . e4s_getSpreadsheetCell(E4S_SHEET_CUSTOMER1_COMMISSION, $row) . '-' . e4s_getSpreadsheetCell(E4S_SHEET_CUSTOMER1_EXPECTED_COMMISSION, $row);
                if ($cust2 !== '') {
                    $customer2CommissionOutput = ',' . $cust2 . ',' . sprintf('%.2f', $cust2Commission) . ',' . sprintf('%.2f', $cust2ExpectedCommission);
                    $customer2CommissionOutput .= ',=' . e4s_getSpreadsheetCell(E4S_SHEET_CUSTOMER2_COMMISSION, $row) . '-' . e4s_getSpreadsheetCell(E4S_SHEET_CUSTOMER2_EXPECTED_COMMISSION, $row);
                }
            } else {
                if (!$competitionObj->isNational) {
                    $customer = 'Unknown';

                    if (!array_key_exists($compObj->stripeUserId, $users)) {
                        if ($compObj->stripeUserId > 0) {
                            $sql = 'select u.id, u.user_email
                            from ' . E4S_TABLE_USERS . ' u
                            where id = ' . $compObj->stripeUserId;
                            $result = e4s_queryNoLog($sql);

                            while ($obj = $result->fetch_object()) {
                                $obj->id = (int)$obj->id;
                                $users[$obj->id] = $obj->user_email;
                            }
                        }
                    }
                    if (array_key_exists($compObj->stripeUserId, $users)) {
                        $customer = $users[$compObj->stripeUserId];
                    }

                    $customerCommissionOutput = ',' . $customer . ',0,' . sprintf('%.2f', $cust1ExpectedCommission) . ',';
                }
            }
        } else {
            $e4sCommission = 0;
            $e4sExpectedCommission = $compObj->e4sExpectedValue;
            $cust1 = $competitionObj->getOrganisationName();
            $cust1Commission = $compObj->custExpectedValue;
            $cust1ExpectedCommission = $compObj->custExpectedValue;
            $customer2CommissionOutput = '';
            $customerCommissionOutput = ',' . $cust1 . ',' . sprintf('%.2f', $cust1Commission) . ',' . sprintf('%.2f', $cust1ExpectedCommission);
            $customerCommissionOutput .= ',=' . e4s_getSpreadsheetCell(E4S_SHEET_CUSTOMER1_COMMISSION, $row) . '-' . e4s_getSpreadsheetCell(E4S_SHEET_CUSTOMER1_EXPECTED_COMMISSION, $row);
        }
        $e4sCommissionOutput = ',' . sprintf('%.2f', $e4sCommission);
        $e4sCommissionOutput .= ',' . sprintf('%.2f', $e4sExpectedCommission);
        $e4sCommissionOutput .= ',=' . e4s_getSpreadsheetCell(E4S_SHEET_E4S_EXPECTED_COMMISSION, $row) . '-' . e4s_getSpreadsheetCell(E4S_SHEET_E4S_COMMISSION, $row);
        if ($showStripeCosts) {
            $output .= ',' . $stripeCosts;
        }
        $output .= $noCommissionOutput . $e4sCommissionOutput . $customerCommissionOutput . $customer2CommissionOutput;

        $output .= ',';
        $entryCounts = e4s_getCountByPrice($compId);
        $entryOutput = '';
        $entryFactor = 0;
        $entryTotal = '';
        foreach ($entryCounts as $entryCount) {
            if ($entryOutput !== '') {
                $entryOutput .= ',';
            }
            $entryOutput .= $entryCount->eventCnt . ',' . sprintf('%.2f', $entryCount->price);
            if ($entryTotal !== '') {
                $entryTotal .= '+';
            }
            $entryTotal .= '(';
            $entryTotal .= e4s_getSpreadsheetCell(E4S_SHEET_ENTRIES_START_COL + $entryFactor++, $row);
            $entryTotal .= '*';
            $entryTotal .= e4s_getSpreadsheetCell(E4S_SHEET_ENTRIES_START_COL + $entryFactor++, $row);
            $entryTotal .= ')';
        }
        if ($entryTotal !== '') {
            $output .= '=';
        }
        $output .= $entryTotal . ',';
        $output .= '=' . e4s_getSpreadsheetCell(E4S_SHEET_GROSS_TAKEN, $row) . '-' . e4s_getSpreadsheetCell(E4S_SHEET_ENTRIES_TOTAL, $row) . ',';
        $output .= $entryOutput;
        $output .= "\n";
        $row++;
    }
    return $output;
}

function e4s_getSpreadsheetCell($colNo, $row) {
    $col = e4s_getSpreadsheetCol($colNo);
    return '$' . $col . $row;
}

function e4s_getSpreadsheetCol($colNo) {
    $maxAlphabet = 26;
    $factor = (int)($colNo / ($maxAlphabet + 1));
    $retVal = '';
    if ($factor > 0) {
        $retVal = chr(E4S_FIRST_COL_COUNT + $factor);
        $colNo = $colNo - ($factor * $maxAlphabet);
    }
    $retVal .= chr(E4S_FIRST_COL_COUNT + $colNo);
    return $retVal;
}

function e4s_getCountByPrice($compId) {
    $sql = '
        select count(e.id) eventCnt, price
        from ' . E4S_TABLE_ENTRIES . ' e,
             ' . E4S_TABLE_COMPEVENTS . ' ce
        where e.compEventID = ce.ID
        and ce.compid = ' . $compId . '
        and e.paid = ' . E4S_ENTRY_PAID . '
        group by price
        union
        select count(e.id) eventCnt, price
        from ' . E4S_TABLE_EVENTTEAMENTRIES . ' e,
             ' . E4S_TABLE_COMPEVENTS . ' ce
        where e.ceid = ce.ID
          and ce.compid = ' . $compId . '
          and e.paid = ' . E4S_ENTRY_PAID . '
        group by price';
    $result = e4s_queryNoLog($sql);

    $arr = array();
    if ($result->num_rows > 0) {
        while ($obj = $result->fetch_object()) {
            $arr[] = $obj;
        }
    }
    return $arr;
}