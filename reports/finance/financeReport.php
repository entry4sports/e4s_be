<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
const E4S_RANGE_HELP = 'Click orange number range to enter manually ( single number or \'min - max\' )';
function e4s_getMaxCompNumber() {
    $sql = 'select max(id) maxNo
            from ' . E4S_TABLE_COMPETITON;
    $result = e4s_queryNoLog($sql);
    $obj = $result->fetch_object();
    return (int)$obj->maxNo;
}

function e4s_getOrderRange() {
    $sql = "select min(id) minId,
                   max(id) maxId
            from " . E4S_TABLE_POSTS . "
            where post_type = '" . WC_POST_ORDER . "'";
    $result = e4s_queryNoLog($sql);
    $obj = $result->fetch_object();
    $retArr = array();
    $retArr[] = (int)$obj->minId;
    $retArr[] = (int)$obj->maxId;
    $archive = e4sArchive::archiveName();
	$sql = "select min(id) minId,
                   max(id) maxId
            from " . $archive . "." . E4S_TABLE_POSTS_ARCHIVE . "
            where post_type = '" . WC_POST_ORDER . "'";
	$result = e4s_queryNoLog($sql);
	$obj = $result->fetch_object();
	$obj->minId = (int)$obj->minId;
    $obj->maxId = (int)$obj->maxId;
    if ($obj->minId < $retArr[0]) {
        $retArr[0] = $obj->minId;
    }
    if ($obj->maxId > $retArr[1]) {
        $retArr[1] = $obj->maxId;
    }
    return $retArr;
}

$maxCompCount = e4s_getMaxCompNumber();
$orderRange = e4s_getOrderRange();
$orgId = 0;
?>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/<?php echo E4S_JQUERY_THEME ?>/jquery-ui.css"/>
    <link rel="stylesheet" href="<?php echo E4S_PATH ?>css/jquery.mloading.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo E4S_PATH ?>js/jquery-dateformat.min.js"></script>
    <script src="<?php echo E4S_PATH ?>js/jquery.mloading.js"></script>
    <title>
        Finance Report
    </title>
    <style>
        .e4s-header {
            text-align: center;
            font-weight: bold;
            font-size: xx-large;
        }

        .fieldset-date-fields {
            display: inline-flex;;
        }

        .date-fields {
            margin-top: 30px;
            margin-bottom: 30px;
        }

        .date-from {

        }

        .date-from-field {
            width: 90px;
        }

        .date-to {
            margin-left: 20px;
        }

        .date-to-field {
            width: 90px;
        }

        .comp-range-div {
            margin-bottom: 30px;
        }

        .e4s-comp-range {
            /*margin-top: 20px;*/
            margin: 10px 5px 10px 5px;
        }

        .order-range-div {
            margin-bottom: 30px;
        }

        .e4s-order-range {
            /*margin-top: 20px;*/
            margin: 10px 5px 10px 5px;
        }

        .fieldset-div {
            display: block;
        }

        .fieldset-title {
            font-size: x-large;
            padding: 0px 5px 0px 5px;
        }

        .range-fields {
            border: 0;
            color: #f6931f;
            font-weight: bold;
        }

        .e4s_fieldset {
            display: inline-block;
        }

        .organisation_div {
            margin: 30px 0px 30px 0px;
        }

        .customer-div {
            margin-top: 30px;
        }

        .customer-field {
            width: 400px;
        }

        .overflow {
            height: 250px;
        }
    </style>
    <script>
        function setCompSlider(min, max) {
            let setMin = 0;
            let setMax = <?php echo $maxCompCount ?>;
            if (min > setMin) {
                setMin = min;
            }
            if (max < setMax) {
                setMax = max;
            }
            if (setMax < setMin) {
                let save = setMax;
                setMax = setMin;
                setMin = save;
            }
            $("#slider-comp-range").slider("values", [setMin, setMax]);
        }

        function compCheckOther(min, max) {
            if (min === max) {
                $("#compDateFields").hide();
                $("#compDateFrom").val("");
                $("#compDateTo").val("");
                $(".organisation_div").hide();
            } else {
                $("#compDateFields").show();
                $(".organisation_div").show();
            }
        }

        function setCompetitionValues(min, max) {
            $("#competitionRange").val(min + " - " + max);
            setCompSlider(min, max);
            compCheckOther(min, max);
        }

        function getCompSliderValues() {
            return $("#slider-comp-range").slider("values");
        }

        function blurCompValues() {
            let text = $("#competitionRange").val();
            let min = 0;
            let max = 0;
            if (text.indexOf("-") > -1) {
                text = text.split("-");
                min = parseInt(text[0]);
                max = parseInt(text[1]);
            } else {
                min = parseInt(text);
                max = min;
            }
            setCompetitionValues(min, max);
        }

        function blurOrderValues() {
            let text = $("#orderRange").val();
            let min = 0;
            let max = 0;
            if (text.indexOf("-") > -1) {
                text = text.split("-");
                min = parseInt(text[0]);
                max = parseInt(text[1]);
            } else {
                min = parseInt(text);
                max = min;
            }
            setOrderValues(min, max);
        }

        function setOrderValues(min, max) {
            $("#orderRange").val(min + " - " + max);
            setOrderSlider(min, max);
        }

        function setOrderSlider(min, max) {
            let setMin = <?php echo $orderRange[0]; ?>;
            let setMax = <?php echo $orderRange[1]; ?>;
            if (min > setMin) {
                setMin = min;
            }
            if (max < setMax) {
                setMax = max;
            }
            if (setMax < setMin) {
                let save = setMax;
                setMax = setMin;
                setMin = save;
            }
            $("#slider-order-range").slider("values", [setMin, setMax]);
        }

        function getOrderSliderValues() {
            return $("#slider-order-range").slider("values");
        }

        $(function () {
            let loadOrganisations = function () {
                $.getJSON("finance/orgs", function (data, status, xhr) {
                    data.sort(function (a, b) {
                        if (a.text < b.text) return -1;
                        if (a.text > b.text) return 1;
                        return 0;
                    });
                    if (data.length > 1) {
                        $('#organisation').append($('<option>', {
                            value: 0,
                            text: 'All'
                        }))
                    }
                    $.each(data, function (i, item) {
                        $('#organisation').append($('<option>', {
                            value: item.id,
                            text: item.text
                        }));
                    });
                    $("#organisation")
                        .selectmenu()
                        .selectmenu("menuWidget")
                        .addClass("overflow");
                    $("fieldset").controlgroup();
                });
            };
            $("#compDateFrom").datepicker({
                dateFormat: "dd M yy"
            });
            $("#compDateTo").datepicker({
                dateFormat: "dd M yy"
            });
            $("#dateFrom").datepicker({
                dateFormat: "dd M yy"
            });
            $("#dateTo").datepicker({
                dateFormat: "dd M yy"
            });
            // competition number logic
            let compSlider = $("#slider-comp-range").slider({
                range: true,
                min: 0,
                max: <?php echo $maxCompCount; ?>,
                values: [0, <?php echo $maxCompCount; ?> ],
                slide: function (event, ui) {
                    setCompetitionValues(ui.values[0], ui.values[1]);
                }
            });

            setCompetitionValues($("#slider-comp-range").slider("values", 0), $("#slider-comp-range").slider("values", 1));

            // end of competition number logic

            // Order number logic
            var orderSlider = $("#slider-order-range").slider({
                range: true,
                min: <?php echo $orderRange[0]; ?>,
                max: <?php echo $orderRange[1]; ?>,
                values: [ <?php echo $orderRange[0] . ',' . $orderRange[1]; ?> ],
                slide: function (event, ui) {
                    setOrderValues(ui.values[0], ui.values[1]);
                }
            });
            setOrderValues($("#slider-order-range").slider("values", 0), $("#slider-order-range").slider("values", 1));
            // End of Order number logic

            $("input[type='radio']").checkboxradio();

            var cache = {};
            $("#customer").autocomplete({
                minLength: 3,
                source: function (request, response) {
                    var term = request.term;
                    if (term in cache) {
                        response(cache[term]);
                        return;
                    }

                    $.getJSON("finance/cust", request, function (data, status, xhr) {
                        cache[term] = data;
                        response(data);
                    });
                }
            });
            $("#resetComp").button().click(function (event) {
                event.preventDefault();
                setCompetitionValues(0, <?php echo $maxCompCount; ?>);
            });
            $("#resetOrder").button().click(function (event) {
                event.preventDefault();
                setOrderValues(<?php echo $orderRange[0]; ?>, <?php echo $orderRange[1]; ?>);
            });
            $("#competitionRange").blur(function () {
                blurCompValues();
            });
            $("#orderRange").blur(function () {
                blurOrderValues();
            });
            loadOrganisations();
            $("#runReport").button().click(function (event) {
                $("body").mLoading({
                    text: "Running Report...",
                });
                event.preventDefault();
                var url = location.href.split("reports/")[0] + "e4s?action=AAIData";
                let dateFrom = $("#dateFrom").val();
                if (dateFrom !== "") {
                    url += "&fromdate=" + new Date(dateFrom + " 12:00").toISOString().slice(0, 10);
                }
                let dateTo = $("#dateTo").val();
                if (dateTo !== "") {
                    url += "&todate=" + new Date(dateTo + " 12:00").toISOString().slice(0, 10);
                }
                let compDateFrom = $("#compDateFrom").val();
                if (compDateFrom !== "") {
                    url += "&compfromdate=" + new Date(compDateFrom + " 12:00").toISOString().slice(0, 10);
                }
                let compDateTo = $("#compDateTo").val();
                if (compDateTo !== "") {
                    url += "&comptodate=" + new Date(compDateTo + " 12:00").toISOString().slice(0, 10);
                }
                let compIds = getCompSliderValues();
                url += "&fromid=" + compIds[0];
                url += "&toid=" + compIds[1];
                let orderIds = getOrderSliderValues();
                if (orderIds[0] !== <?php echo $orderRange[0]; ?> ) {
                    url += "&fromorder=" + orderIds[0];
                }
                if (orderIds[1] !== <?php echo $orderRange[1]; ?> ) {
                    url += "&toorder=" + orderIds[1];
                }

                let ticketsOnly = $("[name=ticketsOnly]:checked").val();
                url += "&ticketsonly=" + ticketsOnly;
                let details = $("[name=details]:checked").val();
                url += "&details=" + details;
                let orgId = $("#organisation").val();
                url += "&orgid=" + orgId;
                let custId = $("#customer").val();
                if (custId.indexOf(":") > 0) {
                    custId = custId.split(":")[1];
                    url += "&custid=" + custId.trim();
                }
                $.get(
                    url,
                    "",
                    function (response) {
                        let json = JSON.parse(response);
                        let data = json.data;
                        var hiddenElement = document.createElement('a');

                        hiddenElement.href = 'data:attachment/text,' + encodeURI(data);
                        hiddenElement.target = '_blank';
                        let dt = $.format.date(Date(), "dd-MMM-yyyy HH:mm.ss");
                        hiddenElement.download = 'Finance Report ' + dt + '.csv';
                        hiddenElement.click();
                        $("body").mLoading('hide');
                    },
                    "text"
                );
            });
        });

    </script>
</head>
<body>
<div id="header" class="e4s-header">
    Entry4Sports Finance Report
</div>
<form id="financeForm">
    <div id="formFields">

        <div id="compRangeDiv" class="comp-range-div">
            <fieldset id="compRangeFieldSet" class="fieldset-div">
                <legend class="fieldset-title">Competition Information</legend>
                <label for="competitionRange">Number Range:</label>
                <input type="text" id="competitionRange" class="range-fields"><i><?php echo E4S_RANGE_HELP ?></i>
                <div id="slider-comp-range" class="e4s-comp-range"></div>
                <div>
                    <button name="resetComp" id="resetComp">Reset</button>
                </div>
                <div id="compDateFields" class="date-fields">
                        <span id="compDateFromDiv" class="date-from">
                            From Date: <input type="text" id="compDateFrom" class="date-from-field">
                        </span>
                    <span id="compDateToDiv" class="date-to">
                            To Date: <input type="text" id="compDateTo" class="date-to-field">
                        </span>
                </div>
                <div class="organisation_div">
                    <label for="organisation">Select an Organisation</label>
                    <select name="organisation" id="organisation">

                    </select>
                </div>
            </fieldset>
        </div>
        <div id="orderRangeDiv" class="order-range-div">
            <fieldset id="orderRangeFieldSet" class="fieldset-div">
                <legend class="fieldset-title">Order Information</legend>
                <label for="orderRange">Number range:</label>
                <input type="text" id="orderRange" class="range-fields"><i><?php echo E4S_RANGE_HELP ?></i>
                <div id="slider-order-range" class="e4s-order-range"></div>
                <div>
                    <button name="resetOrder" id="resetOrder">Reset</button>
                </div>
                <div id="dateFields" class="date-fields">
                        <span id="dateFromDiv" class="date-from">
                            From Date: <input type="text" id="dateFrom" class="date-from-field">
                        </span>
                    <span id="dateToDiv" class="date-to">
                            To Date: <input type="text" id="dateTo" class="date-to-field">
                        </span>
                </div>
                <span>Include Details : </span>
                <div class="order-details">
                    <label for="radio-3">Yes</label>
                    <input type="radio" name="details" id="radio-3" value="1">
                    <label for="radio-4">No</label>
                    <input type="radio" checked name="details" id="radio-4" value="0">
                </div>
                <span>Tickets Only : </span>
                <div class="order-tickets-only">
                    <label for="radio-1">Yes</label>
                    <input type="radio" name="ticketsOnly" id="radio-1" value="1">
                    <label for="radio-2">No</label>
                    <input type="radio" checked name="ticketsOnly" id="radio-2" value="0">
                </div>

                <div id="customerDiv" class="customer-div">
                    <span>For Customer : </span>
                    <span class="ui-widget">
                            <input class='customer-field' id="customer">
                        </span>
                </div>

            </fieldset>
        </div>


        <div class="widget">
            <button id="runReport">Run Report</button>
        </div>
    </div>
</form>
</body>
</html>


