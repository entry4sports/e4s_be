<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';

function ordersBeforeDate($obj){
	$date = checkFieldFromParamsForXSS($obj, 'date:Date');
	$arr = explode('-', $date);
	if ( sizeof($arr) != 3 ) {
		exit("1: Please pass a valid date ( dd-mm-yyyy )");
	}
	if ( !checkdate($arr[1],$arr[0],$arr[2])){
		exit("2: Please pass a valid date ( dd-mm-yyyy )");
	}

	e4s_ordersBeforeDate($arr[2] . "-" . $arr[1] . "-" . $arr[0]);
}
function ordersBeforeApril($obj) {

	$year = (int) checkFieldFromParamsForXSS( $obj, 'year:Competition Year' . E4S_CHECKTYPE_NUMERIC );

	if ( $year < 2020 or $year > 2030 ) {
		exit ( "Please pass a valid year" );
	}
	e4s_ordersBeforeDate($year . '-03-31');
}
function e4s_ordersBeforeDate($date){
	$sql = '
		select c.id compNo, c.name compName,c.date compDate, e.orderid, e.athlete, p.post_date, e.price
			from ' . E4S_TABLE_COMPETITION . ' c,
			     ' . E4S_TABLE_COMPEVENTS . ' ce,
			     ' . E4S_TABLE_ENTRIES . ' e,
			     ' . E4S_TABLE_POSTS . ' p
			where c.date > "' . $date . '"
			and ce.CompID = c.id
			and ce.id = e.compEventID
		    and e.orderid = p.id
			and e.paid = 1
			and orderid > 0
			and post_date <= "' . $date . '"
		union
			select c.id compNo, c.name compName,c.date compDate, e.orderid, e.athlete, p.post_date, e.price
			from ' . E4S_TABLE_COMPETITION . ' c,
			     ' . E4S_TABLE_COMPEVENTS . ' ce,
			     ' . e4sArchive::archiveName() . '.' . E4S_TABLE_ENTRIES_ARCHIVE . ' e,
			     ' . e4sArchive::archiveName() . '.' . E4S_TABLE_POSTS_ARCHIVE . ' p
			where c.date > "' . $date . '"
			and ce.CompID = c.id
			and ce.id = e.compEventID
		    and e.orderid = p.id
			and e.paid = 1
			and orderid > 0
			and post_date <= "' . $date . '"
			order by compNo, orderid
	';

	echo "CompId, compName, compDate, orderid, athlete, post_date, price\n";
	$result = e4s_queryNoLog($sql);
	if ( $result->num_rows == 0 ) {
		exit;
	}
	while ( $obj = $result->fetch_object() ){
		echo $obj->compNo . ', ' . $obj->compName . ', ' . $obj->compDate . ', ' . $obj->orderid . ', ' . $obj->athlete . ', ' . $obj->post_date . ', ' . $obj->price . "\n";
	}
	exit;
}