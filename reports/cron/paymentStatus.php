<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
define('E4S_LAST_PAYMENT_REPORT', 'e4s_last_report_run');
define('E4S_REPORT_FROM_START', '0000-00-00 00:00:00');

if (!isset($GLOBALS[E4S_CRON_USER])) {
    $Permission = E4S_ROLE;
    if (userHasPermission($Permission, null, null) === FALSE) {
//        Entry4UIError(1000, "You do not have permission to run this report.", 400, '');
    }
}

$lastRun = e4s_getLastRunReportDate();

$body = e4s_checkCompetitionsStatus($lastRun);

if ($body !== '') {
    e4s_setLastRunReportDate();
    $body = 'The following Competitions have changed status since ' . $lastRun . '<br><br>' . $body;

    $to = ['paul@entry4sports.com', 'nick@entry4sports.com', 'sharon@entry4sports.com'];
//$to = "paul@entry4sports.com";

    $subject = 'Competition Status update';
    $headers = array('Content-Type: text/html; charset=UTF-8');
    e4s_mail($to, $subject, $body, $headers);
}
e4s_writeHealth(E4S_CRON_PAYMENTSTATUSCHECKER, 86400);
exit();


function e4s_getLastRunReportDate() {
    $result = _getLastRunQuery();
    if ($result->num_rows < 1) {
        createLastRunDate();
        return E4S_REPORT_FROM_START;
    }
    $row = $result->fetch_assoc();
    return $row['option_value'];
}

function createLastRunDate() {
    $sql = 'Insert into ' . E4S_TABLE_OPTIONS . " (option_name,option_value, autoload)
                values(
                    '" . E4S_LAST_PAYMENT_REPORT . "',
                    now() - Interval 1 hour,
                    'No'
                    )";
    e4s_queryNoLog($sql);
}

function _getLastRunQuery() {
    $sql = 'Select * 
            from ' . E4S_TABLE_OPTIONS . "
            where option_name = '" . E4S_LAST_PAYMENT_REPORT . "'";
    return e4s_queryNoLog($sql);
}

function e4s_setLastRunReportDate() {
    $sql = 'update ' . E4S_TABLE_OPTIONS . "
            set option_value =  now() - Interval 1 hour
            where option_name = '" . E4S_LAST_PAYMENT_REPORT . "'";
    e4s_queryNoLog($sql);
}

function e4s_checkCompetitionsStatus($lastrun) {

    if (E4S_CURRENT_DOMAIN === E4S_UK_DOMAIN) {
        return e4s_checkStatusForEnv(E4S_UK_PREFIX, $lastrun);
    }
    if (E4S_CURRENT_DOMAIN === E4S_AAI_DOMAIN) {
        return e4s_checkStatusForEnv(E4S_NO_PREFIX, $lastrun);
    }
    return '';
}

function e4s_checkStatusForEnv($env, $lastrun) {
    $compTable = str_replace(E4S_PREFIX, $env, E4S_TABLE_COMPETITON);
    $auditTable = str_replace(E4S_PREFIX, $env, E4S_TABLE_COMPSTATUS);
    $workflowTable = str_replace(E4S_PREFIX, $env, E4S_TABLE_WORKFLOW);
    $domain = 'Unknown';
    if ($env === E4S_UK_PREFIX) {
        $domain = 'UK';
    }
    if ($env === E4S_NO_PREFIX) {
        $domain = 'AAI';
    }

    $sql = 'select c.id id, c.active, c.name, c.date date, a.reference, a.value, w.description, a.updated, c.options options
            from ' . $auditTable . ' a,
                 ' . $compTable . ' c,
                 ' . $workflowTable . " w
            where c.id = a.compid
            and   a.statusid = w.id
            and   c.date > '2020-01-01'
            order by w.description,c.name";

    $result = e4s_queryNoLog($sql);
    if ($result->num_rows === 0) {
        return '';
    }
    $modifiedBody = '';
    $outstandingBody = '';
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    foreach ($rows as $row) {
        $row['active'] = (int)$row['active'];
        if ($row['active'] === 1) {
            if ($row['updated'] > $lastrun) {
                $modifiedBody .= e4s_returnHeaderTitle($modifiedBody, $domain, $lastrun);
                $modifiedBody .= e4s_returnLineItem($row);
            } else {
                if (strtolower($row['description']) !== 'paid') {
                    $outstandingBody .= e4s_returnHeaderTitleAll($outstandingBody, $domain, $lastrun);
                    $outstandingBody .= e4s_returnLineItem($row);
                }
            }
        }
    }
    $modifiedBody .= e4s_returnTableFooter($modifiedBody);
    $outstandingBody .= e4s_returnTableFooter($outstandingBody);

    $body = $modifiedBody . $outstandingBody;
    return $body;
}

function e4s_returnHeaderTitleAll($currentBody, $domain, $lastrun) {
    if ($currentBody !== '') {
        return '';
    }
    $body = '<b>' . $domain . ' has the following outstanding Competitions.</b><br><br>';
    $body .= e4s_returnTableHeader();
    return $body;
}

function e4s_returnHeaderTitle($currentBody, $domain, $lastrun) {
    if ($currentBody !== '') {
        return '';
    }
    $body = '<b>' . $domain . ' has the following changes since : ' . $lastrun . '.</b><br><br>';
    $body .= e4s_returnTableHeader();
    return $body;
}

function e4s_returnTableFooter($currentBody) {
    if ($currentBody === '') {
        return '';
    }
    $body = '</table>';
    $body .= '<br><br>';
    return $body;
}

function e4s_returnTableHeader() {
    return '<table><tr><td>Competition</td><td>Date of Comp</td><td>Value</td><td>Status</td></tr>';
}

function e4s_returnLineItem($row) {
    $body = '<tr>';
    $body .= '<td>' . $row['id'] . ' : ' . $row['name'] . '</td>';
    $body .= '<td>' . $row['date'] . '</td>';
    $body .= '<td>' . $row['value'] . '</td>';
    $body .= '<td>' . $row['description'] . '</td>';
    $body .= '</tr>';

    return $body;
}
