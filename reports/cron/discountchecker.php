<?php
//require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'eventteams/commonEventTeam.php';

if (!isset($GLOBALS[E4S_CRON_USER])) {
    $Permission = E4S_ROLE;
    if (userHasPermission($Permission, null, null) === FALSE) {
        Entry4UIError(1000, 'You do not have permission to run this report.', 400, '');
    }
}

$unpaidArr = array();
// Get athletes that have paid nothing
$sql = '
SELECT sum(price), athleteid, athlete, compid, c.date, c.name compname,c.entriesclose, orderid 
FROM ' . E4S_TABLE_ENTRIES . ' e, 
     ' . E4S_TABLE_COMPEVENTS . ' ce,
     ' . E4S_TABLE_COMPETITON . " c 
where paid = 1 and e.compEventid = ce.ID 
  and ce.options not like '%" . e4s_optionIsTeamEvent() . "%' 
  and ce.compid = c.id
  and c.entriesclose > now()
  and e.options not like '%reason\":%'
group by athleteid, athlete, compid,c.date,c.entriesclose, orderid 
having sum(price) = 0
";
$results = e4s_queryNoLog($sql);
$rows = $results->fetch_all(MYSQLI_ASSOC);

foreach ($rows as $row) {
    // has the athlete been paid for on a different order
    $otherSql = '
        SELECT e.id
        from ' . E4S_TABLE_COMPEVENTS . ' ce,
             ' . E4S_TABLE_ENTRIES . ' e
        WHERE athleteid = ' . $row['athleteid'] . '
        and ce.compid = ' . $row['compid'] . '
        and e.compEventID = ce.ID
        and e.paid = ' . E4S_ENTRY_PAID . '
        and price > 0
    ';

    $otherResults = e4s_queryNoLog($otherSql);
    if ($otherResults->num_rows === 0) {
        // This Athlete has not been paid for
        $unpaidArr[] = $row;
    }
}
if (count($unpaidArr) === 0) {
    Entry4UISuccess('');
}
$body = '';
if (count($unpaidArr) > 0) {
    $body = 'The following have issues with payment. ( Paid Nothing ).<br><br>';

    foreach ($unpaidArr as $unpaid) {
        $body .= 'Athlete : ' . $unpaid['athlete'] . ' from order ' . $unpaid['orderid'] . '. Competition ' . $unpaid['compname'] . ' on ' . $unpaid['date'] . '. Entries close : ' . $unpaid['entriesclose'] . '<br>';
    }
    $body .= '<br><br>';
    $body .= 'Instructions on how to correct can be found here https://drive.google.com/open?id=1Ys1rtpCVlzGBdoF4ZGqoAv8Xr8XEjsvfVw_cInxc16s';
}

$to = 'nick@entry4sports.com, paul@entry4sports.com';
//$to = "paul.day@apiconsultancy.com";
$site = 'Athletics Ireland';
if (E4S_PREFIX === 'Entry4_ire_') {
    $site = 'Ireland Regional';
}
if (E4S_PREFIX === 'Entry4_uk_') {
    $site = 'UK';
}
$subject = 'Athletes from ' . $site . ' with missing payments. To be checked';
$headers = array('Content-Type: text/html; charset=UTF-8');
e4s_writeHealth(E4S_CRON_PAYMENTSCHECKER);
e4s_mail($to, $subject, $body, $headers);
//echo $body;