<?php
const E4S_COMMISSION_EMAIL = 'support@entry4sports.com';
const E4S_FIRST_COL_COUNT = 832;
const TICKET_NAME = 'Admission Ticket';

function e4s_Finance2Report($params) {
    $financeData = new e4sFinanceReportData($params);
}

class e4sFinanceConfig {
    public $settings;
    public $config;

    public function __construct($settings) {
        $this->settings = $settings;

        $this->config = e4s_getConfig();
        $this->_init();
    }

    private function _init() {
        $fromCompId = $this->getFromCompId();
        $toCompId = $this->getToCompId();
        if ($fromCompId > 0 and $toCompId > 0 and $fromCompId > $toCompId) {
            // swap compIds
            $saveId = $fromCompId;
            $this->_setFromCompId($toCompId);
            $this->_setToCompId($saveId);
        }
        if (array_key_exists('useStripeConnect', $this->config)) {
            $this->_setUseStripeConnect($this->config['useStripeConnect']);
        } else {
            $this->_setUseStripeConnect(FALSE);
        }
        $this->_setShowStripeCosts(isAdminUser());
    }

    public function getFromCompId() {
        return $this->settings->fromCompId;
    }

    public function getToCompId() {
        return $this->settings->toCompId;
    }

    private function _setFromCompId($id) {
        $this->settings->fromCompId = $id;
    }

    private function _setToCompId($id) {
        $this->settings->toCompId = $id;
    }

    private function _setUseStripeConnect($value) {
        $this->settings->useStripeConnect = $value;
    }

    private function _setShowStripeCosts($value) {
        $this->settings->showStripeCosts = $value;
    }

    public function getShowStripeCosts() {
        return $this->settings->showStripeCosts;
    }

    public function getUseStripeConnect() {
        return $this->settings->useStripeConnect;
    }

    public function getCustId() {
        return $this->settings->custId;
    }

    public function getFromOrderNo() {
        return $this->settings->fromOrderNo;
    }

    public function getToOrderNo() {
        return $this->settings->toOrderNo;
    }

    public function getCompFromDate() {
        return $this->settings->compFromDate;
    }

    public function getCompToDate() {
        return $this->settings->compToDate;
    }

    public function getOrderToDate() {
        return $this->settings->orderToDate;
    }

    public function getOrderFromDate() {
        return $this->settings->orderFromDate;
    }

    public function getOrgId() {
        return $this->settings->orgId;
    }

    public function getShowDetails() {
        return $this->settings->details;
    }

    public function getTicketsOnly() {
        return $this->settings->justTickets;
    }

    public function getDefaultAO() {
        return $this->config['defaultao']['code'];
    }
}

class e4sFinanceReportData {
    public $settingsObj;
    public $compData;
    public $commissons;
    public $userIds;
    public $ceRecords;
    public $tickets;
    public $orders;
    public $entryCounts;
    public $unpaidEntries;
    public $summaryData;

    public function __construct($params) {
        $this->settingsObj = new e4sFinanceConfig($params);
        $this->_init();
        $this->_loadData();
        $this->_processData();
    }

    private function _init() {
        $this->commissons = null;
        $this->userIds = array();
        $this->orders = array();
    }

    private function _loadData() {
        $this->_setCompData();
        $this->_setCommissions();
        $this->_setUsers();
        $this->_setCompEvents();
        $this->_setTicketInfo();
        $this->_setOrderInfo();
        $this->_setEntryCountInfo();
        $this->_setUnpaidEntriesInfo();
    }

    private function _setCompData() {
        $orgId = $this->settingsObj->getOrgId();
        $compFromDate = $this->settingsObj->getCompFromDate();
        $compToDate = $this->settingsObj->getCompToDate();
        $fromCompId = $this->settingsObj->getFromCompId();
        $toCompId = $this->settingsObj->getToCompId();

        $sql = 'select c.id compId,
                   cc.stripeUserId,
                   cc.club organisation
            from ' . E4S_TABLE_COMPETITON . ' c,
                 ' . E4S_TABLE_COMPCLUB . ' cc
            where c.id > 0 
            and c.compclubid = cc.id ';
        if ($orgId > 0) {
            $sql .= ' and c.compclubid = ' . $orgId;
        }
        if ($compFromDate !== '') {
            $sql .= " and c.date >= '" . $compFromDate . "'";
        }
        if ($compToDate !== '') {
            $sql .= " and c.date <= '" . $compToDate . "'";
        }

        if ($fromCompId === $toCompId) {
            $sql .= ' and c.id = ' . $fromCompId;
        } elseif ($fromCompId > 0 and $toCompId > 0) {
            $sql .= ' and c.id between ' . $fromCompId . ' and ' . $toCompId;
        } elseif ($fromCompId > 0) {
            $sql .= ' and c.id >= ' . $fromCompId;
        } else {
            $sql .= ' and c.id <= ' . $toCompId;
        }
        $sql .= ' order by c.id desc';
        $result = e4s_queryNoLog($sql);
        $comps = array();
        while ($obj = $result->fetch_object()) {
            $compObj = e4s_GetCompObj($obj->compId);
            if (isAdminUser()) {
                $addComp = TRUE;
            } else {
                $addComp = $compObj->isOrganiser();
            }
            if ($addComp) {
                $compDataObj = new stdClass();
                $compDataObj->compId = (int)$compObj->getID();
                $compDataObj->stripeUserId = (int)$obj->stripeUserId;
                $compDataObj->value = 0;
                $compDataObj->refunded = 0;
                $compDataObj->errorValue = 0;
                $compDataObj->noCommissionValue = 0;
                $compDataObj->e4sExpectedValue = 0;
                $compDataObj->custExpectedValue = 0;
                $compDataObj->stripeValue = 0;
                $compDataObj->commissions = array();
                $compDataObj->competition = str_replace(',', ' ', $compObj->getName());
                $compDataObj->organisation = $obj->organisation;
                $compDataObj->compObj = $compObj;
                $comps [$compDataObj->compId] = $compDataObj;
            }
        }
        $this->compData = $comps;
    }

    private function _setCommissions() {
        $commissions = array();
        if (!$this->settingsObj->getUseStripeConnect()) {
            return;
        }
        $sql = 'select  id,
                        user_id userId,
                        order_item_id orderItemId,
                        order_id orderId,
                        commission,
                        commission_status status
                from ' . E4S_TABLE_YITHCOMMISSIONS;
        $result = e4s_queryNoLog($sql);
        while ($obj = $result->fetch_object()) {
            $obj->id = (int)$obj->id;
            $obj->userId = (int)$obj->userId;
            $this->userIds[$obj->userId] = $obj->userId;
            $obj->orderItemId = (int)$obj->orderItemId;
            $obj->orderId = (int)$obj->orderId;
            $obj->commission = (float)$obj->commission;
            if (!array_key_exists($obj->orderItemId, $commissions)) {
                $commissions[$obj->orderItemId] = array();
            }
            $commissions[$obj->orderItemId][$obj->userId] = $obj;
        }
        $this->commissons = $commissions;
    }

    private function _setUsers() {
        if (!empty($this->userIds)) {
            $sql = 'select u.id, u.user_email
            from ' . E4S_TABLE_USERS . ' u
            where id in (' . implode(',', $this->userIds) . ')';
            $result = e4s_queryNoLog($sql);
            while ($obj = $result->fetch_object()) {
                $this->userIds[(int)$obj->id] = $obj->user_email;
            }
        }
    }

    private function _setCompEvents() {
        $fromCompId = $this->settingsObj->getFromCompId();
        $toCompId = $this->settingsObj->getToCompId();
        $sql = 'select id,
                   compId
            from ' . E4S_TABLE_COMPEVENTS . '
            where ';
        if ($fromCompId === $toCompId) {
            $sql .= ' compid = ' . $fromCompId;
        } elseif ($fromCompId > 0 and $toCompId > 0) {
            $sql .= ' compid between ' . $fromCompId . '  and ' . $toCompId . ' ';
        } elseif ($fromCompId > 0) {
            $sql .= ' compid >= ' . $fromCompId;
        } else {
            $sql .= ' compid <= ' . $toCompId;
        }
        $ceRecords = array();
        $result = e4s_queryNoLog($sql);
        while ($obj = $result->fetch_object()) {
            $ceRecords[(int)$obj->id] = (int)$obj->compId;
        }
        $this->ceRecords = $ceRecords;
    }

    private function _setTicketInfo() {
        $tickets = array();
        $ticketSql = 'select id
                         ,orderId
                         ,itemId
                         ,compId
                         ,userId
                  from ' . E4S_TABLE_TICKET . '
                  where orderid is not null';
        $ticketResult = e4s_queryNoLog($ticketSql);
        while ($ticket = $ticketResult->fetch_object()) {
            $ticket->id = (int)$ticket->id;
            $ticket->orderId = (int)$ticket->orderId;
            $ticket->itemId = (int)$ticket->itemId;
            $ticket->compId = (int)$ticket->compId;
            $ticket->userId = (int)$ticket->userId;
            if (!array_key_exists($ticket->orderId, $tickets)) {
                $ticketItems = array();
                $tickets[$ticket->orderId] = $ticketItems;
            }
            $tickets[$ticket->orderId][$ticket->itemId] = $ticket;
        }
        $this->tickets = $tickets;
    }

    private function _setOrderInfo() {
        $useStripeConnect = $this->settingsObj->getUseStripeConnect();
        $custId = $this->settingsObj->getCustId();
        $fromOrderNo = $this->settingsObj->getFromOrderNo();
        $toOrderNo = $this->settingsObj->getToOrderNo();
        $orderFromDate = $this->settingsObj->getOrderFromDate();
        $orderToDate = $this->settingsObj->getOrderToDate();
        $tickets = $this->tickets;

        $itemSql = 'select  wci.order_id orderId
                        ,wci.order_item_id orderItemId
                        ,wci.order_item_name orderItemName
                        ,wcim.meta_value liveValue
                        ,p.post_status orderStatus
                        ,p.post_date orderDate
                        ,pm.meta_value custId';
        if (!$useStripeConnect) {
            $itemSql .= '   ,pm2.meta_value stripeFee
                        ,pm3.meta_value stripeValue';
        }
        $itemSql .= ' from ' . E4S_TABLE_WCORDERITEMS . ' wci
                     ,' . E4S_TABLE_WCORDERITEMMETA . ' wcim
                     ,' . E4S_TABLE_POSTS . ' p
                     ,' . E4S_TABLE_POSTMETA . ' pm ';
        if (!$useStripeConnect) {
            $itemSql .= ',' . E4S_TABLE_POSTMETA . ' pm2
                     ,' . E4S_TABLE_POSTMETA . ' pm3 ';
        }
        $itemSql .= "where (order_item_name like '% : %' or order_item_name = '" . TICKET_NAME . "')
                and p.id = wci.order_id
                and p.id = pm.post_id
                and pm.meta_key = '_customer_user'
                and p.post_status in( '" . WC_ORDER_PAID . "', '" . WC_ORDER_REFUNDED . "')
                and wci.order_item_id = wcim.order_item_id
                and wcim.meta_key = '_line_total'";
        if (!$useStripeConnect) {
            $itemSql .= " and p.id = pm2.post_id
                      and pm2.meta_key = '_stripe_fee'
                      and p.id = pm3.post_id
                      and pm3.meta_key = '_stripe_net'";
        }
        if ($custId > 0) {
            $itemSql .= " and pm.meta_value = '" . $custId . "'";
        }
        if ($fromOrderNo > 0) {
            $itemSql .= ' and p.id >= ' . $fromOrderNo;
        }
        if ($toOrderNo > 0) {
            $itemSql .= ' and p.id <= ' . $toOrderNo;
        }
        if ($orderFromDate !== '') {
            $itemSql .= " and p.post_date >= '" . $orderFromDate . "'";
        }
        if ($orderToDate !== '') {
            $itemSql .= " and p.post_date <= '" . $orderToDate . "'";
        }
        $result = e4s_queryNoLog($itemSql);
        while ($obj = $result->fetch_object()) {
            $obj->orderId = (int)$obj->orderId;
            $obj->orderItemId = (int)$obj->orderItemId;
            $obj->liveValue = (float)$obj->liveValue;
            $obj->custId = (int)$obj->custId;
            if (!$useStripeConnect) {
                $obj->stripeValue = (float)$obj->stripeValue;
                $obj->stripeFee = (float)$obj->stripeFee;
            } else {
                $obj->stripeValue = 0;
                $obj->stripeFee = 0;
            }

            $this->orders[$obj->orderId] = $obj;
        }
    }

    private function _setEntryCountInfo() {
        $sql = '
            select compId
                 , count(e.id) eventCnt
                 , price
            from ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce
            where e.compEventID = ce.ID
            and e.paid = ' . E4S_ENTRY_PAID . '
            group by compId, price
            union
            select compId
            , count(e.id) eventCnt
            , price
            from ' . E4S_TABLE_EVENTTEAMENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce
            where e.ceid = ce.ID
            and e.paid = ' . E4S_ENTRY_PAID . '
            group by compId, price';
        $result = e4s_queryNoLog($sql);

        $arr = array();
        if ($result->num_rows > 0) {
            while ($obj = $result->fetch_object()) {
                $obj->compId = (int)$obj->compId;
                $obj->eventCnt = (int)$obj->eventCnt;
                $obj->price = (float)$obj->price;

                if (!array_key_exists($obj->compId, $arr)) {
                    $arr[$obj->compId] = array();
                }
                $arr[$obj->compId][] = $obj;
            }
        }
        $this->entryCounts = $arr;
    }

    private function _setUnpaidEntriesInfo() {
        $sql = 'select  e.id
                       ,e.options
                       ,p.price
                       ,p.salePrice
                       ,p.saleEndDate
                       ,ce.compId
                from ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTPRICE . ' p
                where e.orderid = 0 
                and e.paid = ' . E4S_ENTRY_PAID . '
                and e.compeventid = ce.id
                and ce.priceid = p.id';
        $result = e4s_queryNoLog($sql);
        $all = $result->fetch_all(MYSQLI_ASSOC);
        $this->unpaidEntries = array();
        foreach ($all as $entry) {
            $entryObj = new stdClass();
            $entryObj->id = (int)$entry['id'];
            $entryObj->price = (float)$entry['price'];
            $entryObj->salePrice = (float)$entry['salePrice'];
            $entryObj->options = e4s_getOptionsAsObj($entry['options']);
            $entryObj->compId = (int)$entry['compId'];
            if (!array_key_exists($entryObj->compId, $this->unpaidEntries)) {
                $this->unpaidEntries[$entryObj->compId] = array();
            }
            $this->unpaidEntries[$entryObj->compId][] = $entryObj;
        }
    }

    private function _processData() {
        $showDetails = $this->settingsObj->getShowDetails();
        $justTickets = $this->settingsObj->getTicketsOnly();
        $tickets = $this->tickets;
        $output = '';
        $details = array();

        foreach ($this->orders as $order) {
            $compId = 0;
            if ($order->orderStatus === WC_ORDER_PAID) {
                if ($order->orderItemName === TICKET_NAME) {
                    if (array_key_exists($order->orderId, $tickets)) {
                        $ticketArr = $tickets[$order->orderId];
                        if (array_key_exists($order->orderItemId, $ticketArr)) {
                            $ticketObj = $tickets[$order->orderId][$order->orderItemId];
                            $compId = $ticketObj->compId;
                            $order->itemName = TICKET_NAME . ' for ' . $compId;
                        }
                    }
                } elseif ($justTickets) {
                    // do nothing
                } else {
                    $nameArr = preg_split('~ :~', $order->orderItemName);
                    $ceId = (int)$nameArr[0];
                    $order->itemName = $nameArr[1];
                    if (array_key_exists($ceId, $this->ceRecords)) {
                        $compId = $this->ceRecords [$ceId];
                    }
                }
                if ($compId !== 0) {
                    if ($this->_isCompToBeProcessed()) {
                        if ($showDetails) {
                            if (!array_key_exists($compId, $details)) {
                                $details[$compId] = array();
                            }
                            $details[$compId][] .= $this->_getDetailRow($order, $compId);
                        } else {
                            $this->_addToSummary();
                        }
                    }
                }
            }
        }

        if ($showDetails) {
            $output .= $this->_getDetailHeader();
            // sort by compId
            asort($details);
            foreach ($details as $compDetails) {
                foreach ($compDetails as $compDetail) {
                    $output .= $compDetail . "\n";
                }
            }
        } else {
            $output .= $this->_outputSummary();
        }
        return $output;
    }

    private function _isCompToBeProcessed($compId) {
        $fromCompId = $this->settingsObj->getFromCompId();
        $toCompId = $this->settingsObj->getToCompId();

        if (($compId < $fromCompId and $fromCompId > 0) or ($compId > $toCompId and $toCompId > 0)) {
            return FALSE;
        }

        if (!array_key_exists($compId, $this->compData)) {
            // failed to find competition
            return FALSE;
        }
        $curComp = $this->compData[$compId]->compObj;
        if (!$curComp->isOrganiser()) {
            // not an organiser
            return FALSE;
        }

        // only allow e4s and AAI to access National
        if ($curComp->getOrganisationName() === 'National') {
            if (!isE4SUser() and !isAdminUser()) {
                return FALSE;
            }
        }
        return TRUE;
    }

    private function _getDetailRow($order, $compId) {
        $curComp = $this->compData[$compId]->compObj;
        $refundValue = 0;
        $output = '';

        $output .= $compId;
        $output .= ',' . formatCompName($curComp->competition);
        $output .= ',' . $curComp->getDate();
        $date = date_create($curComp->getDate());
        $dateStr = date_format($date, 'jS M Y');

        $output .= ',' . $order->orderId;
        $output .= ',' . $order->orderDate;
        $itemName = $order->itemName;
        $itemNameArr = preg_split('~' . $dateStr . '.~', $itemName);
        if (sizeof($itemNameArr) > 1) {
            $itemName = trim($itemNameArr[1]);
        }
        $output .= ',' . str_replace(',', '.', $itemName);

        $output .= ',' . sprintf('%.2f', $order->liveValue);
        $output .= ',' . sprintf('%.2f', $refundValue);
        if ($this->settingsObj->getUseStripeConnect()) {
            $noCommissionValue = 0;
            $e4sCommissionValue = 0;
            $custCommissionValue = 0;
            $priceObj = $curComp->getPriceSplit($this->settingsObj->getDefaultAO(), $order->liveValue);
            $stripeValue = $priceObj->stripeValue;
            $e4sExpectedCommissionValue = $priceObj->e4sValue;
            $custExpectedCommissionValue = $priceObj->custValue;
            $cust2CommissionValue = 0;
            $customer = '';
            $customer2 = '';
            if (!array_key_exists($order->orderItemId, $this->commissons)) {
                $noCommissionValue = $order->liveValue;
            } else {
                $commission = $this->commissons[$order->orderItemId];
                foreach ($commission as $commissionUser => $priceObj) {
                    if ($priceObj->status === 'sc_transfer_success') {
                        if ($this->userIds[$commissionUser] === E4S_COMMISSION_EMAIL) {
                            $e4sCommissionValue = $priceObj->commission;
                        } else {
                            if ($customer === '') {
                                $customer = $this->userIds[$commissionUser];
                                $custCommissionValue = $priceObj->commission;
                            } else {
                                $customer2 = $this->userIds[$commissionUser];
                                $cust2CommissionValue = $priceObj->commission;
                            }
                        }
                    }
                }
            }
            if ($this->settingsObj->getShowStripeCosts()) {
                $output .= ',' . sprintf('%.2f', $stripeValue);
            } else {
                $e4sCommissionValue += $stripeValue;
                $e4sExpectedCommissionValue += $stripeValue;
            }
            $output .= ',' . sprintf('%.2f', $noCommissionValue);
            $output .= ',' . sprintf('%.2f', $e4sCommissionValue);
            $output .= ',' . sprintf('%.2f', $e4sExpectedCommissionValue);
            if (!$curComp->isNational) {
                if ($customer === '') {
                    $customer = 'Unknown';
                    $stripeUserId = $comp->stripeUserId;
                    if (!array_key_exists($stripeUserId, $this->userIds)) {
                        if ($stripeUserId > 0) {
                            $sql = 'select u.id, u.user_email
                                    from ' . E4S_TABLE_USERS . ' u
                                    where id = ' . $stripeUserId;
                            $result = e4s_queryNoLog($sql);
                            while ($obj = $result->fetch_object()) {
                                $obj->id = (int)$obj->id;
                                $users[$obj->id] = $obj->user_email;
                            }
                        }
                    }
                    if (array_key_exists($comp->stripeUserId, $users)) {
                        $customer = $users[$comp->stripeUserId];
                    }
                }
                $output .= ',' . $customer;
                $output .= ',' . sprintf('%.2f', $custCommissionValue);
                $output .= ',' . sprintf('%.2f', $custExpectedCommissionValue);
                if ($customer2 !== '') {
                    $output .= ',' . $customer2;
                    $output .= ',' . sprintf('%.2f', $cust2CommissionValue);
                }
            }
        }
        return $output;
    }

    private function _addToSummary() {

    }

    private function _getDetailHeader() {
        $output = 'Comp No.,Competition,Date,Order Id,Order Date,Reference,Line Value,Refunded Value,';
        if ($this->settingsObj->getShowStripeCosts()) {
            $output .= 'Stripe Value,';
        }
        $output .= "Unallocated,E4S Actual,E4S Expected,Customer,Customer Actual,Customer Expected\n";
        return $output;
    }

    private function _outputSummary() {

    }

    private function addToSummary() {

    }

    private function formatCompName($compName) {
        $compName = preg_replace('~#~', 'No.', $compName);
        return $compName;
    }
}


function aaiOutputDetail($obj, $compId, $comp, $useStripeConnect, $defaultAO, $commissions, $users) {
    $showStripeCosts = isAdminUser();
    $compObj = e4s_GetCompObj($compId);
    $output = '';
    $output .= $compId . ',';
    $output .= formatCompName($comp->competition) . ',';
    $output .= $comp->getDate() . ',';
    $date = date_create($comp->getDate());
    $dateStr = date_format($date, 'jS M Y');

    $output .= $obj->orderId . ',';
    $output .= $obj->orderDate . ',';
    $itemName = $obj->itemName;
    $itemNameArr = preg_split('~' . $dateStr . '.~', $itemName);
    if (sizeof($itemNameArr) > 1) {
        $itemName = trim($itemNameArr[1]);
    }
    $output .= $itemName . ',';
//    if ( $obj->orderId === 81393 ){
//        var_dump($obj);
//        var_dump($comp);
//    }
    if ($obj->orderStatus === WC_ORDER_PAID) {
        $output .= $obj->liveValue . ',0,';
        if ($useStripeConnect) {
            $noCommissionValue = 0;
            $e4sCommissionValue = 0;
            $custCommissionValue = 0;
            $commissionObj = $compObj->getPriceSplit($defaultAO, $obj->liveValue);
            $stripeValue = $commissionObj->stripeValue;
            $e4sExpectedCommissionValue = $commissionObj->e4sValue;
            $custExpectedCommissionValue = $commissionObj->custValue;
            $cust2CommissionValue = 0;
            $customer = '';
            $customer2 = '';
            if (!array_key_exists($obj->orderItemId, $commissions)) {
                $noCommissionValue = $obj->liveValue;
            } else {
                $commission = $commissions[$obj->orderItemId];
                foreach ($commission as $commissionUser => $commissionObj) {
                    if ($commissionObj->status === 'sc_transfer_success') {
                        if ($users[$commissionUser] === E4S_COMMISSION_EMAIL) {
                            $e4sCommissionValue = $commissionObj->commission;
                        } else {
                            if ($customer === '') {
                                $customer = $users[$commissionUser];
                                $custCommissionValue = $commissionObj->commission;
                            } else {
                                $customer2 = $users[$commissionUser];
                                $cust2CommissionValue = $commissionObj->commission;
                            }
                        }
                    }
                }
            }
            if ($showStripeCosts) {
                $output .= sprintf('%.2f', $stripeValue) . ',';
            } else {
                $e4sCommissionValue += $stripeValue;
                $e4sExpectedCommissionValue += $stripeValue;
            }
            $output .= sprintf('%.2f', $noCommissionValue) . ',';
            $output .= sprintf('%.2f', $e4sCommissionValue) . ',';
            $output .= sprintf('%.2f', $e4sExpectedCommissionValue) . ',';
            if (!$compObj->isNational) {
                if ($customer === '') {
                    $customer = 'Unknown';

                    if (!array_key_exists($comp->stripeUserId, $users)) {
                        if ($comp->stripeUserId > 0) {
                            $sql = 'select u.id, u.user_email
                            from ' . E4S_TABLE_USERS . ' u
                            where id = ' . $comp->stripeUserId;
                            $result = e4s_queryNoLog($sql);
                            while ($obj = $result->fetch_object()) {
                                $obj->id = (int)$obj->id;
                                $users[$obj->id] = $obj->user_email;
                            }
                        }
                    }
                    if (array_key_exists($comp->stripeUserId, $users)) {
                        $customer = $users[$comp->stripeUserId];
                    }
                }
                $output .= $customer . ',';
                $output .= sprintf('%.2f', $custCommissionValue) . ',';
                $output .= sprintf('%.2f', $custExpectedCommissionValue);
                if ($customer2 !== '') {
                    $output .= ',' . $customer2 . ',';
                    $output .= sprintf('%.2f', $cust2CommissionValue);
                }
            }
        }
    } else {
        $output .= '0,' . $obj->liveValue;
    }
    $output .= "\n";
    return $output;
}

function aaiAddToSummary($obj, $useStripeConnect, $compId, &$comp, $defaultAO, $commissions) {
    if ($obj->orderStatus === WC_ORDER_PAID) {
        $comp->value += $obj->liveValue;

        $compObj = e4s_GetCompObj($compId, FALSE);
        $expectedObj = $compObj->getPriceSplit($defaultAO, $obj->liveValue);
        $comp->stripeValue += $expectedObj->stripeValue;
        $comp->custExpectedValue += $expectedObj->custValue;

        if ($useStripeConnect) {
            $comp->e4sExpectedValue += $expectedObj->e4sValue;
            if (!array_key_exists($obj->orderItemId, $commissions)) {
                $comp->noCommissionValue += $obj->liveValue;
            } else {
                $commission = $commissions[$obj->orderItemId];
                foreach ($commission as $commissionUser => $commissionObj) {
                    if (!array_key_exists($commissionUser, $comp->commissions)) {
                        $comp->commissions[$commissionUser] = 0;
                    }
                    if ($commissionObj->status === 'sc_transfer_success') {
                        $comp->commissions[$commissionUser] += $commissionObj->commission;
                        $comp->commissions[$commissionUser] = round($comp->commissions[$commissionUser], 2);
                    }
                }
            }
        } else {
//            $e4sValue = $obj->liveValue - $comp->custExpectedValue;
            $comp->e4sExpectedValue += $expectedObj->e4sValue;
            //$comp->e4sExpectedValue += $expectedObj->stripeValue;
        }
        $comp->e4sExpectedValue = round($comp->e4sExpectedValue, 2);
        $comp->custExpectedValue = round($comp->custExpectedValue, 2);
        $comp->stripeValue = round($comp->stripeValue, 2);
    } else {
        $comp->refunded += $obj->liveValue;
    }
}

function aaiOutputSummary($useStripeConnect, $users, $comps) {
    $showStripeCosts = isAdminUser();
    $cols = array();
    $chrCount = 1;
    define('E4S_SHEET_COMP_ID', $chrCount++);
    $cols[E4S_SHEET_COMP_ID] = (object)['name' => 'Competition ID'];
    define('E4S_SHEET_ORG_NAME', $chrCount++);
    $cols[E4S_SHEET_ORG_NAME] = (object)['name' => 'Organisation'];
    define('E4S_SHEET_COMP_NAME', $chrCount++);
    $cols[E4S_SHEET_COMP_NAME] = (object)['name' => 'Name'];
    define('E4S_SHEET_COMP_DATE', $chrCount++);
    $cols[E4S_SHEET_COMP_DATE] = (object)['name' => 'Date'];
    define('E4S_SHEET_GROSS_TAKEN', $chrCount++);
    $cols[E4S_SHEET_GROSS_TAKEN] = (object)['name' => 'Gross Taken'];
    define('E4S_SHEET_GROSS_REFUNDED', $chrCount++);
    $cols[E4S_SHEET_GROSS_REFUNDED] = (object)['name' => 'Gross Refunded'];
    if ($showStripeCosts) {
        define('E4S_SHEET_STRIPE_COSTS', $chrCount++);
        $cols[E4S_SHEET_STRIPE_COSTS] = (object)['name' => 'Stripe Costs', 'stripe' => TRUE];
    }
    define('E4S_SHEET_NO_COMMISSION', $chrCount++);
    $cols[E4S_SHEET_NO_COMMISSION] = (object)['name' => 'No Commission Value ( Gross )'];
    define('E4S_SHEET_E4S_COMMISSION', $chrCount++);
    $cols[E4S_SHEET_E4S_COMMISSION] = (object)['name' => 'E4S Commission'];
    define('E4S_SHEET_E4S_EXPECTED_COMMISSION', $chrCount++);
    $cols[E4S_SHEET_E4S_EXPECTED_COMMISSION] = (object)['name' => 'E4S Expected Commission'];
    define('E4S_SHEET_E4S_DIFF', $chrCount++);
    $cols[E4S_SHEET_E4S_DIFF] = (object)['name' => 'E4S Credit-Debit'];
    define('E4S_SHEET_CUSTOMER', $chrCount++);
    $cols[E4S_SHEET_CUSTOMER] = (object)['name' => 'Customer'];
    define('E4S_SHEET_CUSTOMER1_COMMISSION', $chrCount++);
    $cols[E4S_SHEET_CUSTOMER1_COMMISSION] = (object)['name' => 'Customer Commission'];
    define('E4S_SHEET_CUSTOMER1_EXPECTED_COMMISSION', $chrCount++);
    $cols[E4S_SHEET_CUSTOMER1_EXPECTED_COMMISSION] = (object)['name' => 'Customer Expected Commission'];
    define('E4S_SHEET_CUSTOMER1_DIFF', $chrCount++);
    $cols[E4S_SHEET_CUSTOMER1_DIFF] = (object)['name' => 'Customer Credit-Debit'];
    if ($useStripeConnect) {
        define('E4S_SHEET_CUSTOMER2', $chrCount++);
        $cols[E4S_SHEET_CUSTOMER2] = (object)['name' => 'Customer2'];
        define('E4S_SHEET_CUSTOMER2_COMMISSION', $chrCount++);
        $cols[E4S_SHEET_CUSTOMER2_COMMISSION] = (object)['name' => 'Customer2 Commission'];
        define('E4S_SHEET_CUSTOMER2_EXPECTED_COMMISSION', $chrCount++);
        $cols[E4S_SHEET_CUSTOMER2_EXPECTED_COMMISSION] = (object)['name' => 'Customer2 Expected Commission'];
        define('E4S_SHEET_CUSTOMER2_DIFF', $chrCount++);
        $cols[E4S_SHEET_CUSTOMER2_DIFF] = (object)['name' => 'Customer2 Credit-Debit'];
    }
    define('E4S_SHEET_ENTRIES_TOTAL', $chrCount++);
    $cols[E4S_SHEET_ENTRIES_TOTAL] = (object)['name' => 'Entries Gross'];
    define('E4S_SHEET_ENTRIES_DIFF', $chrCount++);
    $cols[E4S_SHEET_ENTRIES_DIFF] = (object)['name' => 'Gross Taken - Entries Gross'];
    define('E4S_SHEET_ENTRIES_START_COL', $chrCount);
    $cols[E4S_SHEET_ENTRIES_START_COL] = (object)['name' => 'Entry Counts / Price'];

    $output = '';
    $sep = '';
    foreach ($cols as $col) {
        $output .= $sep;
        $output .= $col->name;
        $sep = ',';
    }

    $output .= "\n";
    asort($comps);
    $row = 2;
    foreach ($comps as $compId => $compObj) {
        $grossTaken = $compObj->value;
        $refundedValue = $compObj->refunded;
        $stripeCosts = $compObj->stripeValue;
        $noCommissionOutput = ',' . sprintf('%.2f', $compObj->noCommissionValue);
        $e4sCommission = 0;
        $e4sExpectedCommission = $compObj->e4sExpectedValue;
        $cust1 = '';
        $cust1Commission = 0;
        $cust1ExpectedCommission = 0;
        $cust2 = '';
        $cust2Commission = 0;
        $cust2ExpectedCommission = 0;
        $customerCommissionOutput = '';
        $customer2CommissionOutput = ',,,,';
        $competitionObj = e4s_GetCompObj($compId);
        $output .= $compId . ',' . $compObj->organisation . ',' . formatCompName($compObj->competition) . ',' . $compObj->getDate() . ',' . sprintf('%.2f', $grossTaken) . ',' . $refundedValue;
        if ($useStripeConnect) {
            foreach ($compObj->commissions as $commUser => $commission) {
                if ($users[$commUser] === E4S_COMMISSION_EMAIL) {
                    $e4sCommission = $commission;
                    if (!$showStripeCosts) {
                        $e4sCommission += $stripeCosts;
                        $e4sExpectedCommission += $stripeCosts;
                    }
                } else {
                    if ($cust1 === '') {
                        $cust1 = $users[$commUser];
                        $cust1Commission = $commission;
                        $cust1ExpectedCommission = $compObj->custExpectedValue;
                    } else {
                        $cust2 = $users[$commUser];
                        $cust2Commission = $commission;
                        $cust2ExpectedCommission = $compObj->custExpectedValue;
                    }
                }
            }

            if ($cust1 !== '') {
                $customerCommissionOutput = ',' . $cust1 . ',' . sprintf('%.2f', $cust1Commission) . ',' . sprintf('%.2f', $cust1ExpectedCommission);
                $customerCommissionOutput .= ',=' . e4s_getSpreadsheetCell(E4S_SHEET_CUSTOMER1_COMMISSION, $row) . '-' . e4s_getSpreadsheetCell(E4S_SHEET_CUSTOMER1_EXPECTED_COMMISSION, $row);
                if ($cust2 !== '') {
                    $customer2CommissionOutput = ',' . $cust2 . ',' . sprintf('%.2f', $cust2Commission) . ',' . sprintf('%.2f', $cust2ExpectedCommission);
                    $customer2CommissionOutput .= ',=' . e4s_getSpreadsheetCell(E4S_SHEET_CUSTOMER2_COMMISSION, $row) . '-' . e4s_getSpreadsheetCell(E4S_SHEET_CUSTOMER2_EXPECTED_COMMISSION, $row);
                }
            } else {
                if (!$competitionObj->isNational) {
                    $customer = 'Unknown';

                    if (!array_key_exists($compObj->stripeUserId, $users)) {
                        if ($compObj->stripeUserId > 0) {
                            $sql = 'select u.id, u.user_email
                            from ' . E4S_TABLE_USERS . ' u
                            where id = ' . $compObj->stripeUserId;
                            $result = e4s_queryNoLog($sql);

                            while ($obj = $result->fetch_object()) {
                                $obj->id = (int)$obj->id;
                                $users[$obj->id] = $obj->user_email;
                            }
                        }
                    }
                    if (array_key_exists($compObj->stripeUserId, $users)) {
                        $customer = $users[$compObj->stripeUserId];
                    }

                    $customerCommissionOutput = ',' . $customer . ',0,' . sprintf('%.2f', $cust1ExpectedCommission) . ',';
                }
            }
        } else {
            $e4sCommission = 0;
            $e4sExpectedCommission = $compObj->e4sExpectedValue;
            $cust1 = $competitionObj->getOrganisationName();
            $cust1Commission = $compObj->custExpectedValue;
            $cust1ExpectedCommission = $compObj->custExpectedValue;
            $customer2CommissionOutput = '';
            $customerCommissionOutput = ',' . $cust1 . ',' . sprintf('%.2f', $cust1Commission) . ',' . sprintf('%.2f', $cust1ExpectedCommission);
            $customerCommissionOutput .= ',=' . e4s_getSpreadsheetCell(E4S_SHEET_CUSTOMER1_COMMISSION, $row) . '-' . e4s_getSpreadsheetCell(E4S_SHEET_CUSTOMER1_EXPECTED_COMMISSION, $row);
        }
        $e4sCommissionOutput = ',' . sprintf('%.2f', $e4sCommission);
        $e4sCommissionOutput .= ',' . sprintf('%.2f', $e4sExpectedCommission);
        $e4sCommissionOutput .= ',=' . e4s_getSpreadsheetCell(E4S_SHEET_E4S_EXPECTED_COMMISSION, $row) . '-' . e4s_getSpreadsheetCell(E4S_SHEET_E4S_COMMISSION, $row);
        if ($showStripeCosts) {
            $output .= ',' . $stripeCosts;
        }
        $output .= $noCommissionOutput . $e4sCommissionOutput . $customerCommissionOutput . $customer2CommissionOutput;

        $output .= ',';
        $entryCounts = e4s_getCountByPrice($compId);
        $entryOutput = '';
        $entryFactor = 0;
        $entryTotal = '';
        foreach ($entryCounts as $entryCount) {
            if ($entryOutput !== '') {
                $entryOutput .= ',';
            }
            $entryOutput .= $entryCount->eventCnt . ',' . sprintf('%.2f', $entryCount->price);
            if ($entryTotal !== '') {
                $entryTotal .= '+';
            }
            $entryTotal .= '(';
            $entryTotal .= e4s_getSpreadsheetCell(E4S_SHEET_ENTRIES_START_COL + $entryFactor++, $row);
            $entryTotal .= '*';
            $entryTotal .= e4s_getSpreadsheetCell(E4S_SHEET_ENTRIES_START_COL + $entryFactor++, $row);
            $entryTotal .= ')';
        }
        if ($entryTotal !== '') {
            $output .= '=';
        }
        $output .= $entryTotal . ',';
        $output .= '=' . e4s_getSpreadsheetCell(E4S_SHEET_GROSS_TAKEN, $row) . '-' . e4s_getSpreadsheetCell(E4S_SHEET_ENTRIES_TOTAL, $row) . ',';
        $output .= $entryOutput;
        $output .= "\n";
        $row++;
    }
    return $output;
}

function e4s_getSpreadsheetCell($colNo, $row) {
    $col = e4s_getSpreadsheetCol($colNo);
    return '$' . $col . $row;
}

function e4s_getSpreadsheetCol($colNo) {
    $maxAlphabet = 26;
    $factor = (int)($colNo / ($maxAlphabet + 1));
    $retVal = '';
    if ($factor > 0) {
        $retVal = chr(E4S_FIRST_COL_COUNT + $factor);
        $colNo = $colNo - ($factor * $maxAlphabet);
    }
    $retVal .= chr(E4S_FIRST_COL_COUNT + $colNo);
    return $retVal;
}

function e4s_getCountByPrice($compId) {
    $sql = '
        select count(e.id) eventCnt, price
        from ' . E4S_TABLE_ENTRIES . ' e,
             ' . E4S_TABLE_COMPEVENTS . ' ce
        where e.compEventID = ce.ID
        and ce.compid = ' . $compId . '
        and e.paid = ' . E4S_ENTRY_PAID . '
        group by price
        union
        select count(e.id) eventCnt, price
        from ' . E4S_TABLE_EVENTTEAMENTRIES . ' e,
             ' . E4S_TABLE_COMPEVENTS . ' ce
        where e.ceid = ce.ID
          and ce.compid = ' . $compId . '
          and e.paid = ' . E4S_ENTRY_PAID . '
        group by price';
    $result = e4s_queryNoLog($sql);

    $arr = array();
    if ($result->num_rows > 0) {
        while ($obj = $result->fetch_object()) {
            $arr[] = $obj;
        }
    }
    return $arr;
}