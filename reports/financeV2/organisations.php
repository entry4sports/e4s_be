<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
$userId = e4s_getUserID();
$allowNational = FALSE;
if (isE4SUser() or isAdminUser()) {
    $allowNational = TRUE;
}
$sql = 'select id, 
               club
        from ' . E4S_TABLE_COMPCLUB . '
        where id > 1
        order by club';
$result = e4s_queryNoLog($sql);
$records = array();
$clubs = array();

while ($obj = $result->fetch_object()) {
    $obj->id = (int)$obj->id;
    $obj->text = $obj->club;
    unset($obj->club);
    $clubs[$obj->id] = $obj;
}
$getAllClubs = FALSE;
if (isAdminUser()) {
    $getAllClubs = TRUE;
} else {
    $orgIds = e4s_getAdminOrgs();

    foreach ($orgIds as $orgId) {
        if ($orgId === 0) {
            $getAllClubs = TRUE;
        } else {
            $records[$orgId] = $clubs[$orgId];
        }
    }
}
if ($getAllClubs) {
    $records = array();
    foreach ($clubs as $id => $obj) {
        $records[$id] = $obj;
    }
}
// allow National ?
$unassoc = array();
foreach ($records as $record) {
    $addRecord = TRUE;
    if ($record->text === 'National' and !$allowNational) {
        $addRecord = FALSE;
    }
    if ($addRecord) {
        $unassoc[] = $record;
    }
}

exit(json_encode($unassoc));

function e4s_getAdminOrgs() {
    $sql = 'select orgId
            from ' . E4S_TABLE_PERMISSIONS . ' p,
                 ' . E4S_TABLE_ROLES . " r
            where p.roleid = r.id
            and   r.role in('admin','finance')
            and p.userid = " . e4s_getUserID();
    $result = e4s_queryNoLog($sql);
    $retObjs = array();
    while ($obj = $result->fetch_object()) {
        $retObjs[] = (int)$obj->orgId;
    }
    return $retObjs;
}