<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
$term = checkFieldFromParamsForXSS($obj, 'term:search term');
$sql = 'select id, 
       user_login
        from ' . E4S_TABLE_USERS . "
        where user_login like '%" . $term . "%'
        or    user_email like '%" . $term . "%'";
$result = e4s_queryNoLog($sql);
$records = array();
while ($obj = $result->fetch_object()) {
    $obj->id = (int)$obj->id;
    $value = $obj->user_login;
    if (strpos($value, '@') > 0) {
        $email = preg_split('~@~', $value);
        $value = $email[0];
    }
    $obj->label = $value;
    $obj->value = $value . ' : ' . $obj->id;
    $records[] = $obj;
}
exit(json_encode($records));
