<?php
require_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_clearSeedings($eventGroup) {
    include_once E4S_FULL_PATH . 'classes/seedingClass.php';
    $egObj = new seedingClass($eventGroup);
    $egObj->clear();
    Entry4UISuccess('');
}

function e4s_clearAllSeedings($compId) {
    $compObj = e4s_GetCompObj($compId);
    if (!$compObj->isOrganiser()) {
        Entry4UIError(5600, 'You are not authourised to use this function');
    }
    include_once E4S_FULL_PATH . 'classes/seedingClass.php';
    $egObj = new seedingClass(0);
    $egObj->clearAllForComp($compId);
    Entry4UISuccess('');
}
function e4s_setStartHeight($obj) {
	$entryId     = checkFieldForXSS( $obj, 'entryid:Entry ID' );
	$startHeight = checkFieldForXSS( $obj, 'startheight:start Height' );
	$sql = "select compId, entryid, eOptions
	        from " . E4S_TABLE_ENTRYINFO . "
	        where entryid = $entryId";
	$result = e4s_queryNoLog($sql);
	if ($result->num_rows === 0) {
		Entry4UIError(5600, 'Entry not found to set Height for.');
	}
	$row = $result->fetch_object();
	$options = json_decode($row->eOptions);
	$options->startHeight = $startHeight;
	$options = json_encode($options);
	$sql = "update " . E4S_TABLE_ENTRIES . "
	        set options = '$options'
	        where id = " . $entryId;
	e4s_queryNoLog($sql);
	$payload = new stdClass();
	$payload->entryId = $entryId;
	$payload->startHeight = $startHeight;
	e4s_sendSocketInfo($row->compId, $payload, R4S_SOCKET_FIELD_STARTHEIGHT, false);
}
function e4s_setStands($obj) {
	$entryId = checkFieldForXSS( $obj, 'entryid:Entry ID' );
	$stands = checkFieldForXSS( $obj, 'stands:Stand distance' );
	$sql = "select compId, entryid, eOptions
	        from " . E4S_TABLE_ENTRYINFO . "
	        where entryid = $entryId";
	$result = e4s_queryNoLog($sql);
	if ($result->num_rows === 0) {
		Entry4UIError(5600, 'Entry not found to set Stands for.');
	}
	$row = $result->fetch_object();
	$options = json_decode($row->eOptions);
	$options->stands = $stands;
	$options = json_encode($options);
	$sql = "update " . E4S_TABLE_ENTRIES . "
	        set options = '$options'
	        where id = " . $entryId;
	e4s_queryNoLog($sql);
	$payload = new stdClass();
	$payload->entryId = $entryId;
	$payload->stands = $stands;
	e4s_sendSocketInfo($row->compId, $payload, R4S_SOCKET_FIELD_STANDS, false);
}