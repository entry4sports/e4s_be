<?php
function e4s_secureMenuOptions() {
	?>
    <div class="e4s-flex-row e4s-flex-nowrap e4s-flex-center" style="padding-top: 10px;">
        <h5 class="e4s-header--500">Event Options</h5>
    </div>
    <hr class="e4s-hr" style="border: 2px solid ;">
    <div class="e4s-flex-column ">
        <div class="e4s-flex-column e4s-input--container">
            <a href="#"
               onclick="ClosePopup(event, this);addAthleteDialogByBib(); return false;"
               class="e4s-settings--label e4s-body--100">Add Athlete to Event
            </a>
        </div>
        <hr class="e4s-hr">
        <div class="e4s-flex-column e4s-input--container">
            <a href="#"
               onclick="ClosePopup(event, this);addNotesToEventGroup(); return false;"
               class="e4s-settings--label e4s-body--100">Add Notes to Event
            </a>
        </div>
        <hr class="e4s-hr">
        <div
                id="resultsEntryAction"
                class="e4s-flex-column e4s-input--container">
            <a href="#"
               onclick="ClosePopup(event, this);cardResults(1); return false;"
               class="e4s-settings--label e4s-body--100">Enter Results
            </a>
        </div>
        <hr id="resultsEntryActionHR" class="e4s-hr">
        <div
                id="eventGroupSettings"
                class="e4s-flex-column e4s-input--container">
            <a href="#"
               onclick="ClosePopup(event, this);updateEventGroupDialog(); return false;"
               class="e4s-settings--label e4s-body--100">Event Settings
            </a>
        </div>
        <hr id="eventGroupSettingsHR" class="e4s-hr">
        <div
                id="mergeEventAction"
                class="e4s-flex-column e4s-input--container">
            <a href="#"
               onclick="ClosePopup(event, this);mergeEvent(); return false;"
               class="e4s-settings--label e4s-body--100">Merge Event with Another
            </a>
        </div>
        <hr id="mergeEventActionHR" class="e4s-hr">
		<?php if ( e4s_GetCompObj( $GLOBALS[ E4S_OUTPUT_COMP ] )->hasIncludedEntries() ) { ?>
            <div
                    id="includeEntriesAction"
                    class="e4s-flex-column e4s-input--container">
                <a href="#"
                   onclick="ClosePopup(event, this);includeEntries(); return false;"
                   class="e4s-settings--label e4s-body--100">Process Included Entries
                </a>
            </div>
            <hr id="includeEntriesActionHR" class="e4s-hr">
		<?php } ?>
        <div
                style="display:none;"
                id="progressionsAction"
                class="e4s-flex-column e4s-input--container">
            <a href="#"
               onclick="ClosePopup(event, this);progressionObj.init(); return false;"
               class="e4s-settings--label e4s-body--100">Progressions
            </a>
            <hr id="progressionActionHR" class="e4s-hr">
        </div>
        <div id="resetEntriesActionDiv" style="display:none;">
            <div
                    id="resetEntries"
                    class="e4s-flex-column e4s-input--container">
                <a href="#"
                   onclick="ClosePopup(event, this);resetCurrentEGEntries(); return false;"
                   class="e4s-settings--label e4s-body--100">Reset Entries to Source Event
                </a>
            </div>
            <hr id="resetEntriesHR" class="e4s-hr">
        </div>
        <div
                id="seedingAction"
                class="e4s-flex-column e4s-input--container">
            <a href="#"
               onclick="ClosePopup(event, this);getAthletesInOrderAndSeed('heat'); return false;"
               class="e4s-settings--label e4s-body--100">Seeding
            </a>
        </div>

    </div>
    <!--    <hr id="mergeEventActionHR" class="e4s-hr">-->
    <div class="e4s-flex-row e4s-flex-nowrap e4s-flex-center" style="padding-top: 10px;">
        <h5 class="e4s-header--500">Global Options</h5>
    </div>
    <hr class="e4s-hr" style="border: 2px solid ;">
    <div class="e4s-flex-column ">
        <span id="dragDropActionWrapper" style="display:none;">
            <div id="dragDropAction"
                 class="e4s-flex-column e4s-input--container">
                <a href="#"
                   id="dragDropLink"
                   onclick="ClosePopup(event, this);dragDropUtil.toggleStatus(); return false;"
                   class="e4s-settings--label e4s-body--100">Enable Drag and Drop
                </a>
            </div>
             <hr id="dragDropHR" class="e4s-hr">
        </span>
        <span id="confirmActionWrapper" style="display:;">
            <div id="dragDropAction"
                 class="e4s-flex-column e4s-input--container">
                <a href="#"
                   id="confirmLink"
                   onclick="ClosePopup(event, this);quickConfirm.toggleStatus(); return false;"
                   class="e4s-settings--label e4s-body--100">Enable Confirm
                </a>
            </div>
             <hr id="dragDropHR" class="e4s-hr">
        </span>
        <span id="fieldCardSettingsAction" style="display:none;">
            <div id="fieldCardSettingsDiv"
                 class="e4s-flex-column e4s-input--container">
                <a href="#"
                   id="fieldCardSettings"
                   onclick="ClosePopup(event, this);getFieldCardSettings(); return false;"
                   class="e4s-settings--label e4s-body--100">Field Card Settings
                </a>
            </div>
            <hr id="fieldCardSettingsHR" class="e4s-hr">
        </span>
        <div
                id="seedingMonitorAction"
                class="e4s-flex-column e4s-input--container">
            <a id="seedingMonitorActionText"
               href="#"
               onclick="ClosePopup(event, this);monitorCheckInsDialog(); return false;"
               class="e4s-settings--label e4s-body--100">Monitor Seedings
            </a>
        </div>
        <hr id="seedingMonitorActionHR" class="e4s-hr">
        <div
                id="clearSeedingAction"
                class="e4s-flex-column e4s-input--container">
            <a href="#"
               onclick="ClosePopup(event, this);clearCompetitionSeedings(); return false;"
               class="e4s-settings--label e4s-body--100">Reset All Seedings
            </a>
        </div>
        <hr id="resetSeedingActionHR" class="e4s-hr">
        <!--        <div-->
        <!--            id="showRankAction"-->
        <!--            class="e4s-flex-column e4s-input--container">-->
        <!--            <a href="#"-->
        <!--               onclick="ClosePopup(event, this);rankObj.setDisplay(); return false;"-->
        <!--               class="e4s-settings--label e4s-body--100">Show UK Rankings-->
        <!--            </a>-->
        <!--        </div>-->
        <!--        <hr id="resetRankingActionHR" class="e4s-hr">-->
        <div
                id="issueRefreshAction"
                class="e4s-flex-column e4s-input--container">
            <a href="#"
               onclick="ClosePopup(event, this);e4s_refreshRequired(); return false;"
               class="e4s-settings--label e4s-body--100">Inform Clients to Refresh
            </a>
        </div>
        <hr id="issueRefreshActionHR" class="e4s-hr">
        <div
                id="stickySettingsAction"
                class="e4s-flex-column e4s-input--container">
            <a href="#"
               onclick="ClosePopup(event, this);e4s_toggleStickySettings(); return false;"
               class="e4s-settings--label e4s-body--100">Sticky Settings
            </a>
        </div>
        <hr id="resetStickyActionHR" class="e4s-hr">
        <div
                id="photoFinishAction"
                class="e4s-flex-column e4s-input--container">
            <a href="#"
               onclick="ClosePopup(event, this);photoFinishExport.exportDialog(); return false;"
               class="e4s-settings--label e4s-body--100">Update Photo Finish
            </a>
        </div>
        <!--    <hr id="photoFinishHR" class="e4s-hr">-->
    </div>

	<?php
}

?>
<script>
    // Secure Functions
    let e4sCard = {
        eventGroup: null,
        useLane: 0,
        newRaceInfo: null,

        init: function () {
            e4sCard.eventGroup = getCurrentEventGroup();
            e4sCard.egEntries = getEntriesForEg(e4sCard.eventGroup);
        },
        getSeedingFromEntries: function (raceNo) {
            if (this.eventGroup === null) {
                this.init();
            }
            let entries = this.egEntries;
            let seedingInfo = [];
            for (let e in entries) {
                let entry = entries[e];
                if (getEntryRaceNo(entry) === raceNo) {
                    let entryInfo = {};
                    entryInfo.entryId = getEntryId(entry);
                    entryInfo.athleteId = entry.athleteId;
                    let heatInfo = {};
                    heatInfo.heatNo = getRaceNo(entry);
                    heatInfo.laneNo = getLaneNo(entry);
                    heatInfo.heatNoCheckedIn = getRaceNoCheckedIn(entry);
                    heatInfo.laneNoCheckedIn = getLaneNoCheckedIn(entry);
                    entryInfo.heatInfo = heatInfo;
                    seedingInfo.push(entryInfo);
                }
            }
            return seedingInfo;
        },
        getSeedingFromCard: function (raceNo) {
            if (this.eventGroup === null) {
                this.init();
            }
            let targetRaceItems = $("#" + dragDropUtil.prefix + "race_" + raceNo + " li"); // ignore position 0 ( race Number )
            this.useLane = 0;
            this.newRaceInfo = [];
            targetRaceItems.each(function (index, item) {
                if (index > 0) {
                    if (item.checkVisibility()) {
                        e4sCard.useLane++;
                        if (item.attributes['entryid']) {
                            let newObj = {};
                            newObj.entryId = parseInt(item.attributes['entryid'].value);
                            newObj.newLaneNo = e4sCard.useLane;
                            e4sCard.newRaceInfo.push(newObj);
                        }
                    }
                }
            });

            for (let i in this.newRaceInfo) {
                let newObj = this.newRaceInfo[i];
                let entry = getEntryFromEntries(newObj.entryId, this.egEntries);
                let laneNo = newObj.newLaneNo;
                newObj.heatInfo = entry.heatInfo;
                if (isCheckInEnabled() && cardShowsCheckedIn()) {
                    setLaneNoCheckedIn(entry, laneNo);
                    setHeatNoCheckedIn(entry, raceNo);
                } else {
                    setLaneNo(entry, laneNo);
                    setHeatNo(entry, raceNo);
                }
                newObj.athleteId = entry.athleteId;
                delete (newObj.newLaneNo);
            }
            return this.newRaceInfo;
        },
        writeRaceSeedings: function (raceNo, seedings) {
            if (this.eventGroup === null) {
                this.init();
            }
            let payload = {};
            payload.compId = getCompetition().id;
            payload.egId = getEventGroupId(this.eventGroup);
            payload.raceNo = raceNo;
            if (typeof seedings === "undefined") {
                seedings = this.getSeedingFromCard(raceNo);
            }
            payload.seedingInfo = seedings;
            showPleaseWait(true, "Updating Race " + raceNo + " Seedings");
            let url = "<?php echo E4S_BASE_URL ?>seeding/update";
            $.ajax({
                type: "POST",
                url: url,
                data: payload,
                timeout: 2000,
                error: function (jqXHR, textStatus) {
                    if (textStatus === "timeout") {
                        e4sAlert("An error has occurred. Please click ok to reload.", "Error", function () {
                            location.reload();
                        });
                    }
                }
            })
        }
    };
    let quickConfirm = {
        getCookie: function (cookieName) {
            let state = readCookie(cookieName);
            return state === "true";
        },
        setCookie: function (state) {
            createCookie("<?php echo E4S_SEEDING_CONFIRM_COOKIE ?>", state);
            if (state) {
                $("#confirmLink").text("Disable Confirm");
            } else {
                $("#confirmLink").text("Enable Confirm");
            }
        },
        getStatus: function () {
            return this.getCookie("<?php echo E4S_SEEDING_CONFIRM_COOKIE ?>");
        },
        toggleStatus: function () {
            let state = !this.getStatus();
            this.setCookie(state);
            cardChange();
        },
        setCurrentStatus: function () {
            let state = this.getStatus();
            this.setCookie(state);
        },
    }
    let dragDropUtil = {
        prefix: "<?php echo E4S_CARD_FIELD_PREFIX ?>",
        race: "<?php echo E4S_CARD_RACE_PREFIX ?>",
        eventGroup: null,
        egEntries: null,
        laneCount: 0,
        firstLane: 0,
        maxAthleteCount: 0,
        initRaceNo: 0, // initial race and lane of item being dragged
        initLaneNo: 0,
        init: function () {
            dragDropUtil.eventGroup = getCurrentEventGroup();
            dragDropUtil.laneCount = getLaneCount(dragDropUtil.eventGroup);
            dragDropUtil.firstLane = getFirstLane(dragDropUtil.eventGroup);
            dragDropUtil.egEntries = getEntriesForEg(dragDropUtil.eventGroup, cardShowsCheckedIn());
            dragDropUtil.maxAthleteCount = getMaxLaneNo() - dragDropUtil.firstLane;
        },
        start: function (itemId) {
            dragDropUtil.initRaceNo = this.getRaceNoFromId(itemId);
            dragDropUtil.initLaneNo = this.getLaneNoFromId(itemId);
        },
        icon: function () {
            if (this.getStatus()) {
                return "<span class='ui-icon ui-icon-arrowthick-2-n-s'></span>";
            }
            return "";
        },
        displayMenu: function () {
            $("#dragDropActionWrapper").show();
            this.setCurrentStatus();
        },
        getCookie: function (cookieName) {
            let state = readCookie(cookieName);
            return state === "true";
        },
        setCookie: function (state) {
            createCookie("<?php echo E4S_SEEDING_DRAGDROP_COOKIE ?>", state);
            if (state) {
                $("#dragDropLink").text("Disable Drag and Drop");
            } else {
                $("#dragDropLink").text("Enable Drag and Drop");
            }
        },
        getStatus: function () {
            return this.getCookie("<?php echo E4S_SEEDING_DRAGDROP_COOKIE ?>");
        },
        toggleStatus: function () {
            let state = !this.getStatus();
            this.setCookie(state);
            cardChange();
        },
        setCurrentStatus: function () {
            let state = this.getStatus();
            this.setCookie(state);
            dragDrop.init();
        },

        setPlaceHolderStatus: function (show) {
            let display = "";
            if (!show) {
                display = "none";
            }
            $(".ui-sortable-placeholder").css("display", display);
        },
        isItemDisabled: function (ui) {
            return $(ui.item).hasClass("dragDropItemDisabled");
        },
        setItemStatus: function (ui, enabled) {
            if (enabled) {
                $(ui.item).removeClass("dragDropItemDisabled");
            } else {
                $(ui.item).addClass("dragDropItemDisabled");
            }
        },
        getRaceNoFromId: function (id) {
            let arr = id.split("_");
            if (arr[1] === "race") {
                // trackF_race_X
                return parseInt(arr[2]);
            }
            // trackF_RACE_LANE
            return parseInt(arr[1]);
        },
        getLaneNoFromId: function (id) {
            let arr = id.split("_");
            if (arr[1] === "race") {
                // trackF_race_X
                console.log("Error getting lane from " + id);
                return 0;
            }
            // trackF_RACE_LANE
            return parseInt(arr[2]);
        },
        displayRaceInfo: function (raceNo) {
            console.log("Current Items for Race " + raceNo);
            $("#" + dragDropUtil.prefix + "race_" + raceNo + " li").each(
                function (index, item) {
                    if (index > 0 && item.checkVisibility()) {
                        console.log(item.id);
                    }
                }
            )
        },
        getBlankLanes: function (raceNo) {
            let laneCount = dragDropUtil.laneCount;
            let targetRaceEntries = getEntriesForHeatNo(dragDropUtil.egEntries, raceNo);
            let blankLanes = [];
            let blankLaneNo = 1;
            let usedLanes = [];
            if (targetRaceEntries.length <= laneCount) {
                for (let e in targetRaceEntries) {
                    let entry = targetRaceEntries[e];
                    let laneNo = getEntryLaneNo(entry);
                    usedLanes.push(laneNo);
                }
            }
            usedLanes.sort();
            for (let l in usedLanes) {
                let laneNo = usedLanes[l];

                while (blankLaneNo < laneNo) {
                    blankLanes.push(blankLaneNo++);
                }
                blankLaneNo++;

            }
            while (blankLaneNo <= laneCount) {
                blankLanes.push(blankLaneNo++);
            }
            return blankLanes;
        }
    };
    let progressionObj = {
        currentEventGroup: null,
        progressions: [],
        init: function () {
            this.progressions = [];
            this.displayDialog();
        },
        displayDialog: function () {
            let html = '<div id="progressionDialogHTML" style="overflow:none; height:99%;">';
            html += '<div style="display: flex; justify-content: space-around;">';
            html += '<input id="progressionHeight" type="number" step="0.01" placeholder="Height" class="e4sInput" style="width: 75px;">';
            html += '<input id="progressionInc" type="number" step="1" placeholder="Inc" class="e4sInput" style="width: 50px;">';
            html += '<button onclick="progressionObj.add();" class="e4sButton">Add</button>';
            html += '</div>';

            html += '<fieldset style="height: 85%;overflow: scroll;">';
            html += '<legend class="e4sLegend centre"> Existing Progressions </legend>';
            html += '<table>';
            html += '<tr>';
            html += '<th style="width:20px"></th>';
            html += '<th style="width:75px;text-align: justify;">Height</th>';
            html += '<th>Increment</th>';
            html += '</tr>';

            let progressions = this.progressions;
            if (progressions.length === 0) {
                progressions = this.read();
            }
            progressions.sort(function (a, b) {
                return a.height - b.height;
            });
            for (let p in progressions) {
                let prog = progressions[p];
                html += '<tr>';
                html += '<td style="color:red"><span onclick="progressionObj.delete(' + prog.height + ');">X</span></td>';
                html += '<td>' + getDecimals(prog.height, 2) + '</td>';
                html += '<td>' + prog.inc + '</td>';
                html += '</tr>';
            }
            html += '</table>';
            html += "</fieldset>";
            html += '</div>';
            let dialog = $("#progressionDialog");
            dialog.html(html);
            dialog.dialog({
                title: 'Progressions',
                autoOpen: false,
                modal: true,
                width: 250,
                height: 325,
                buttons: {
                    "Update": function () {
                        progressionObj.write();
                        $(this).dialog("close");
                    },
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            });
            dialog.dialog("open");
            this.currentEventGroup = getCurrentEventGroup();
        },
        delete: function (height) {
            let progressions = this.progressions;
            for (let p in progressions) {
                let prog = progressions[p];
                if (prog.height === height) {
                    progressions.splice(p, 1);
                    this.displayDialog();
                    break;
                }
            }
        },
        add: function (eg = null) {
            let height = parseFloat($("#progressionHeight").val());
            let inc = parseFloat($("#progressionInc").val());
            if (isNaN(height) || isNaN(inc)) {
                e4sAlert("Please enter a valid height and increment", "Error");
                return;
            }
            if (progressionObj.getElementForHeight(height) !== null) {
                e4sAlert("Height already exists", "Error");
                return;
            }
            this.addProgression(height, inc);
            let dialog = $("#progressionDialog");
            dialog.dialog("close");
            this.displayDialog();
        },
        updateProgression: function(height, inc){
            this.addProgression(height, inc);
            this.setEventGroup(this.currentEventGroup);
        },
        addProgression: function(height, inc){
            let progressions = this.progressions;
            for(let p in progressions){
                let prog = progressions[p];
                if (height === prog.height){
                    prog.inc = inc;
                    return;
                }
            }
            let obj = {
                "height": height,
                "inc": inc
            }
            progressions.push(obj);
        },
        read: function (eg = null) {
            if (eg === null) {
                eg = getCurrentEventGroup();
                this.currentEventGroup = eg;
            }
            let progressions = eg.options.progressions;
            if (typeof progressions === "undefined") {
                return [];
            }
            // ensure height sorted
            progressions.sort(function (a, b) {
                return a.height - b.height;
            });
            this.progressions = JSON.parse(JSON.stringify(progressions));
            return progressions;
        },
        get: function (count) {
            let progressions = progressionObj.read();
            let progressionSub = 1;
            let progression = progressions[0];
            let arr = [];
            if (progression) {
                let useHeight = progression.height;
                let inc = progression.inc;
                for (let h = 1; h <= count; h++) {
                    arr.push(useHeight);
                    useHeight = getFloatDecimals(useHeight + (inc / 100), 2);
                    if (useHeight >= progression.height) {
                        if (progressions[progressionSub]) {
                            let nextProgression = progressions[progressionSub];
                            if (useHeight >= nextProgression.height) {
                                inc = nextProgression.inc;
                                progressionSub++;
                            }
                        }
                    }
                }
            }
            return arr;
        },
        write: function () {
            let eg = this.currentEventGroup;
            this.setEventGroup(eg);
            writeEGOptions(eg, eg.options);
        },
        setEventGroup: function(eg){
            eg.options.progressions = this.progressions;
        },
        getIncForheight: function (eg, height) {
            let progressions = this.read(eg);
            let inc = 0;
            for (let p in progressions) {
                let prog = progressions[p];
                if (height >= prog.height) {
                    inc = prog.inc;
                }
            }
            return inc;
        },
        getElementForHeight: function (height) {
            let progressions = this.progressions;
            for (let p in progressions) {
                let prog = progressions[p];
                if (height === prog.height) {
                    return prog;
                }
            }
            return null;
        },
        getInfo: function () {
            let progressions = this.read();
            let html = '';
            let sep = "";
            for (let p in progressions) {
                let prog = progressions[p];
                if (sep === "") {
                    sep = "Progressions : ";
                }
                html += sep + getDecimals(prog.height, 2) + "m - " + prog.inc + "cm";
                sep = ", ";
            }
            return html;
        }
    };

    function getHeaderInProgress(){
        let html = "";
        if (isEventInProgress() && !isPrinting()) {
            let colCnt = isEventAHeightEvent() ? 48 : (<?php echo E4S_CARD_DISTANCE_COLS ?> - 5 );
            if (heightCard.isPoleVaultEvent( getCurrentEventGroup())) {
                colCnt = 49;
            }
            html += '<tr class="card_table_header no-print" id="heightHeaderRow0">';
            html += '<td class="no-print" colspan=' + colCnt + '>&nbsp;</td>';
            html += '<td colspan=5 class="no-print centre eventInProgress">Event in Progress<br>Positions can change</td>';
            html += '</tr>';
        }
        return html;
    }
    let distanceCard = {
        positionArray: [], // overall Positions
        minTrialPositionArray: [], // positions at min Trial
        results: [],
        writePositions: false,
        init: function () {
            distanceCard.positionArray = [];
            distanceCard.minTrialPositionArray = [];
            distanceCard.results = [];
        },
        injectInProgressHeader: function(){
            let html = getHeaderInProgress();
            if ( html !== "" ){
                $(".card_table_headerInProgress:first").prop("outerHTML", html);
            }
        },
        isAthleteActive: function(eventNo, trial, athleteId) {
            if (anyNotActiveResult(eventNo, athleteId)) {
                return false;
            }
            return distanceCard.isAthleteTrialActive(trial, athleteId);
        },

        isAthleteTrialActive: function(trial, athleteId){
            let eventGroup = getCurrentEventGroup();
            let min = getMinTrialsFromOptions(eventGroup.options);
            if ( min === 0 || trial <= min ){
                return true;
            }
            let athleteCnt = getAthleteCntFromOptions(eventGroup.options);
            if ( athleteCnt === 0 ){
                return true;
            }
            let pos = distanceCard.getAthleteMinTrialPosition(athleteId);
            if ( pos <= athleteCnt ){
                return true;
            }
            return false;
        },
        getAthletePosition: function(athleteId){
            let posArr = distanceCard.positionArray;
            if (posArr.length === 0 ){
                return 0;
            }
            for(let a in posArr){
                let pos = posArr[a];
                if (pos['athleteId'] === athleteId){
                    return pos['position'];
                }
            }
            return 99;
        },
        getAthleteMinTrialPosition: function(athleteId){
            let posArr = distanceCard.minTrialPositionArray;
            if (posArr.length === 0 ){
                return 0;
            }
            for(let a in posArr){
                let pos = posArr[a];
                if (pos['athleteId'] === athleteId){
                    return pos['position'];
                }
            }
            return 99;
        },
        getFooterInfo: function(reportInfo){
            let eg = getCurrentEventGroup();
            let max = getMaxTrialsFromOptions(eg.options);
            let min = getMinTrialsFromOptions(eg.options);
            let athleteCnt = getAthleteCntFromOptions(eg.options);
            let progInfo = "The maximum number of trials is " + max + ". ";
            if ( min > 0 ){
                progInfo += "All athletes will attempt " + min + " trials";
                if ( athleteCnt > 0 ){
                    progInfo += " with the top " + athleteCnt + " athletes attempting a further " + (max - min) + " trials";
                }
            }
            if ( progInfo !== ""){
                if ( reportInfo !== '' ){
                    reportInfo += '<br>';
                }
                reportInfo += progInfo;
            }
            return reportInfo;
        },
        addResults: function (athleteId, athleteResults) {
            let resultLength = Object.keys(athleteResults).length;
            if ("<?php echo E4S_RESULT_PARAMS ?>" in athleteResults) {
                resultLength--;
            }
            // if ( single result, then check and add to overall results)
            if (resultLength === 1) {
                if (distanceCard.results) {
                    if (distanceCard.results[athleteId]) {
                        // put the single results in to global results and use that
                        let aResults = athleteResults;
                        for (let r in aResults) {
                            if (r.toLowerCase() !== "<?php echo E4S_RESULT_PARAMS ?>") {
                                let aResult = aResults[r];
                                aResult = distanceCard.getKeyInfo(aResult);
                                distanceCard.results[athleteId][r] = aResult.distance;
                                athleteResults = distanceCard.results[athleteId];
                                break;
                            }
                        }
                    }
                }
            }
            let sortedResults = [];
            let minTrialSortedResults = [];
            let minTrials = getMinTrialsFromOptions(getCurrentEventGroup().options);
            if ( minTrials === 0 ) {
                minTrials = 3;
            }
            let results = [];
            for (let r in athleteResults) {
                if (r.toLowerCase() !== "<?php echo E4S_RESULT_PARAMS ?>") {
                    let result = athleteResults[r];
                    if ( ("" + result).indexOf(offLine.statusSep) > -1 ){
                        let info = distanceCard.getKeyInfo(result);
                        result = info.distance;
                    }

                    results[r] = result;
                    if (typeof result === "number") {
                        sortedResults.push(result);
                        if (r <= ('t' + minTrials)) {
                            minTrialSortedResults.push(result);
                        }
                    }
                }
            }
            if (!isEmpty(sortedResults)) {
                sortedResults.sort(function (a, b) {
                    return b - a
                });
                minTrialSortedResults.sort(function (a, b) {
                    return b - a
                });
            }
            distanceCard.results[athleteId] = results;

            let addToGlobal = true;
            minTrialSortedResults['athleteId'] = athleteId;

            for(let p in distanceCard.minTrialPositionArray){
                let curResults = distanceCard.minTrialPositionArray[p];
                if (curResults['athleteId'] === athleteId){
                    addToGlobal = false;
                    distanceCard.minTrialPositionArray[p] = minTrialSortedResults;
                    break;
                }
            }
            if ( addToGlobal ) {
                distanceCard.minTrialPositionArray.push(minTrialSortedResults);
            }

            addToGlobal = true;
            sortedResults['athleteId'] = athleteId;
            for(let p in distanceCard.positionArray){
                let curResults = distanceCard.positionArray[p];
                if (curResults['athleteId'] === athleteId){
                    addToGlobal = false;
                    distanceCard.positionArray[p] = sortedResults;
                    break;
                }
            }
            if ( addToGlobal ) {
                distanceCard.positionArray.push(sortedResults);
            }
        },
        sortPositions: function () {
            distanceCard.sortPositionArray(distanceCard.minTrialPositionArray);
            distanceCard.sortPositionArray(distanceCard.positionArray);
        },
        sortPositionArray: function (positionArray) {
            positionArray.sort(function (a, b) {
                let aResult;
                let bResult;
                let maxArray = a.length > b.length ? a.length : b.length;
                for (let i = 0; i < maxArray; i++) {
                    aResult = a[i];
                    bResult = b[i];
                    if (aResult === bResult) {
                        continue;
                    }
                    if (typeof aResult === "undefined") {
                        return 1;
                    }
                    if (typeof bResult === "undefined") {
                        return -1;
                    }
                    return bResult - aResult;
                }
            });
            let lastPosition = 0;
            for (let i = 0; i < positionArray.length; i++) {
                if (lastPosition === 0) {
                    lastPosition = 1;
                } else {
                    let curResults = positionArray[i];
                    let lastResults = positionArray[i - 1];
                    let same = true;
                    for (let r in curResults) {
                        if (r !== 'athleteId' && r !== 'position') {
                            if (curResults[r] !== lastResults[r]) {
                                same = false;
                                break;
                            }
                        }
                    }
                    if (same) {
                        same = curResults.length === lastResults.length;
                    }
                    if (!same) {
                        lastPosition = i + 1;
                    }
                }
                positionArray[i]['position'] = lastPosition;
            }
        },
        displayPositions: function () {
            distanceCard.sortPositions();
            distanceCard.displayArrayPositions(distanceCard.minTrialPositionArray, "3");
            distanceCard.displayArrayPositions(distanceCard.positionArray, "");
        },
        displayArrayPositions: function (array, suffix) {
            let options = getCurrentEventGroup().options;
            let athleteCnt = getAthleteCntFromOptions(options);
            let minTrials = getMinTrialsFromOptions(options);
            let jumpOrder = getJumpOrderFromOptions(options);
            let showJumpOrder = false;
            if ( jumpOrder !== "" ){
                if ( trialCompleted(minTrials) ){
                    showJumpOrder = true;
                }
            }
            for (let hp in array) {
                let results = array[hp];
                let athleteId = results['athleteId'];
                let position = results['position'];
                if (!results[0]){
                    // No Mark, so no position
                    position = "";
                }
                let rowObj = $("[row_athleteid=" + athleteId + "]");
                let rowNo = rowObj.attr("row_no");
                $("#field_" + suffix + "pos_" + rowNo).text(position);
                if ( suffix === "3" ){
                    let pos = distanceCard.getAthleteMinTrialPosition(athleteId);
                    let joObj = $("#field_" + suffix + "jo_" + rowNo);
                    if ( showJumpOrder ) {
                        if (pos <= athleteCnt) {
                            // reverse the position for min trials
                            pos = athleteCnt - pos + 1;
                            joObj.text(pos);
                            joObj.parent().removeClass(e4sGlobal.class.notActive);
                        } else {
                            joObj.text("");
                            joObj.parent().addClass(e4sGlobal.class.notActive);
                        }
                    }else{
                        joObj.text("");
                        joObj.parent().removeClass(e4sGlobal.class.notActive);
                    }
                }
            }
        },
        getKeyInfo: function (key) {
            key = key.split(":");
            let distance = key[0];
            if (!isNaN(distance) && distance !== "") {
                distance = parseFloat(distance);
            }
            return {
                distance: distance,
                status: parseInt(key[1])
            }
        },
        displayInvalidTrials: function(){
            let eventGroup = getCurrentEventGroup();
            let entries = getEntriesForEg(eventGroup);
            let max = getMaxTrialsFromOptions(eventGroup.options);
            let min = getMinTrialsFromOptions(eventGroup.options);
            for( let e in entries ){
                let entry = entries[e];
                let rowNo = getLaneNo(entry);
                let athleteId = getAthleteId(entry);
                for (let t = 6; t > min; t--) {
                    let className = "";
                    if (t > max) {
                        className = e4sGlobal.class.notAvailable;
                    } else {
                        if (!distanceCard.isAthleteTrialActive(t, athleteId)) {
                            className = e4sGlobal.class.notActive;
                        }
                    }
                    if (className !== "") {
                        $("#field_t" + t + "_" + rowNo).parent().addClass(className);
                        if ( t === 6 ){ // just so its only done once
                            let joObj = $("#field_3jo_" + rowNo);
                            if ( joObj.text().trim() === "" ){
                                joObj.parent().addClass(e4sGlobal.class.notActive);
                            }
                        }
                    }
                }
            }
        },
        displayAthleteResults: function (athleteId, results) {
            let obj = $("[row_athleteid=" + athleteId + "]");
            if (obj.length === 0) {
                return;
            }
            let rowNo = obj.attr("row_no");
            for (let key in results) {
                if (key !== "<?php echo E4S_RESULT_PARAMS ?>") {
                    let curVal = results[key].split(offLine.statusSep);
                    let tbc = (curVal[1] === offLine.activeId ? false : true);
                    curVal = curVal[0];
                    if (isNaN(curVal)) {
                        if (!isValValidOption(curVal)) {
                            curVal = '';
                        }
                    }else if ( curVal !== "" ){
                        curVal = getDecimals(curVal,2);
                    }
                    distanceCard.setFieldResultOnCard(key, rowNo, curVal, tbc);
                }
            }
            distanceCard.addResults(athleteId, results);
            distanceCard.setBestScores("t", rowNo);
        },
        setBestScores: function (key, rowNo) {
            let b3 = 0;
            let b6 = 0;
            let b3Str = "b3";
            let b6Str = "b6";
            let noActualResults = true;
            let minTrials = getMinTrialsFromOptions(getCurrentEventGroup().options);
            if ( minTrials === 0 ){
                minTrials = 3;
            }
            for (var a = 1; a < 7; a++) {
                var m = $("#field_" + key + a + "_" + rowNo).text().trim();

                if ( "" + m !== "" ){
                    noActualResults = false;
                }
                if (!isValValidOption(m) && m !== "") {
                    var val = getFloatDecimals(m, 2);
                    if (a <= minTrials) {
                        if (val > b3) {
                            b3 = getDecimals(val,2);
                        }
                    }
                    if (val > b6) {
                        b6 = getDecimals(val,2);
                    }
                }
            }
            let extraText = getResultExtraText(b3);
            if ( noActualResults ){
                b3 = "";
                b6 = "";
            }
            distanceCard.setFieldResultOnCard(b3Str, rowNo, b3, true, extraText);
            extraText = getResultExtraText(b6);
            distanceCard.setFieldResultOnCard(b6Str, rowNo, b6, true, extraText);
        },
        setFieldResultOnCard: function (key, row, curVal, clearIfBlank, extraText = "") {
            let m = "" + curVal;

            if ( m == "0" ){
                m = "NM";
            }
            if ( extraText !== "" ){
               m += '<span class="' + e4sGlobal.class.resultExtraText + '">' + extraText + '</span>';
            }
            distanceCard.setFieldOptionOnCard(key, row, m);
        },
        setFieldOptionOnCard: function (key, row, curVal, tbc) {
            let mObj = $("#field_" + key + "_" + row);

            if (curVal === "<?php echo E4S_FIELDCARD_RETIRE ?>") {
                markObjActive($("#bib_1_" + row + "_cell"), false);
            }

            mObj.html(curVal);

			<?php if ( secureAccessToCard() ){ ?>
            offLine.resultTbc(tbc, mObj);
			<?php } ?>
        },
        getTrialResult: function (eventNo, trial, athleteId) {
            let results = getResults();
            let offlineResults = offLine.getOutstanding(eventNo, athleteId);
            let hasResults = typeof results[eventNo] !== "undefined";
            if (!hasResults && offlineResults === null) {
                return "";
            }
            let trialResult = 0;
            if (hasResults) {
                let eventResults = results[eventNo];
                if (typeof eventResults[athleteId] === "undefined") {
                    return "";
                }
                let athleteResults = eventResults[athleteId];
                if (typeof athleteResults['t' + trial] === "undefined") {
                    return "";
                }
                trialResult = athleteResults['t' + trial];
            }
            if (offlineResults !== null) {
                for (let key in offlineResults) {
                    if (key === "t" + trial) {
                        trialResult = offlineResults[key];
                        break;
                    }
                }
            }
            trialResult = ("" + trialResult).split(offLine.statusSep)[0];
            let validChar = getValidOption(trialResult);
            if (validChar !== "" || trialResult === "") {
                return validChar;
            }

            return getDecimals(trialResult, 2);
        },
        moveBestTo: function(trial){
            const table = $("#card-base table:first")[0];
            const rows = table.rows;
            const header1 = "Trial " + trial + " Position";
            const header2 = "Best of " + trial + " Trials";

            if ( trial === 5 ) {
                distanceCard.moveCopyColumn(rows, 10, 13);
                distanceCard.moveCopyColumn(rows, 9, 12);
                distanceCard.moveCopyColumn(rows, 8, 11);
                distanceCard.moveSingleRowColumn(rows,10,13,2);
                distanceCard.moveSingleRowColumn(rows,9,12,2, header1);
                distanceCard.moveSingleRowColumn(rows,8,11,2, header2);
            }
            if ( trial === 4 ) {
                distanceCard.moveCopyColumn(rows, 10, 12);
                distanceCard.moveCopyColumn(rows, 9, 11);
                distanceCard.moveCopyColumn(rows, 8, 10);
                distanceCard.moveSingleRowColumn(rows,10,12,2);
                distanceCard.moveSingleRowColumn(rows,9,11,2, header1);
                distanceCard.moveSingleRowColumn(rows,8,10,2, header2);
            }
            if ( trial === 2 ) {
                distanceCard.moveCopyColumn(rows, 8, 7);
                distanceCard.moveCopyColumn(rows, 9, 8);
                distanceCard.moveCopyColumn(rows, 10, 9);
                distanceCard.moveSingleRowColumn(rows,8,7,2, header2);
                distanceCard.moveSingleRowColumn(rows,9,8,2, header1);
                distanceCard.moveSingleRowColumn(rows,10,9,2);
            }
            if ( trial === 1 ) {
                distanceCard.moveCopyColumn(rows, 8, 6);
                distanceCard.moveCopyColumn(rows, 9, 7);
                distanceCard.moveCopyColumn(rows, 10, 8);
                distanceCard.moveSingleRowColumn(rows,8,6,2, header2);
                distanceCard.moveSingleRowColumn(rows,9,7,2, header1);
                distanceCard.moveSingleRowColumn(rows,10,8,2);
            }
        },
        moveCopyColumn: function(rows, fromIndex, toIndex, copy = false) {
            for (let i = 3; i < rows.length; i++) {
                distanceCard.moveSingleRowColumn(rows, fromIndex, toIndex, i,"", copy);
            }
        },
        moveSingleRowColumn: function(rows, fromIndex, toIndex, row, text = "", copy = false) {
            const cells = rows[row].cells;
            const cellToMove = cells[fromIndex];
            const targetCell = cells[toIndex];
            if ( text !== "" && !copy){
                $(cellToMove).find("span").text(text);
            }
            let useRow = rows[row];
            // Remove the cell from its current position if not copying
            if (!copy) {
                useRow.removeChild(cellToMove);
            }
            // Insert the cell into the target position
            if (toIndex < cells.length) {
                let newCol = useRow.insertBefore(cellToMove, targetCell);
                if ( text !== "" ) {
                    $(newCol).find("span").text(text);
                }
            } else {
                useRow.appendChild(cellToMove);
            }
        },
        displayJumpOrder: function(){
            let jumpOrder = getJumpOrderFromOptions(getCurrentEventGroup().options);
            let display = "";
            if ( jumpOrder === "" ){
                display = "none";
            }
            $(".distance_data_3jo").css("display", display);
            $(".jumpOrderHeader").css("display", display);
        }
    };
    let heightCard = {
        HEIGHT_PASS: '<?php echo E4S_FIELDCARD_PASS ?>',
        TRIAL_PASS: '<?php echo E4S_FIELDCARD_MISS ?>',
        FAIL: '<?php echo E4S_FIELDCARD_FAILURE ?>',
        RETIRE: '<?php echo E4S_FIELDCARD_RETIRE ?>',
        SUCCESS: '<?php echo E4S_FIELDCARD_SUCCESS ?>',
        CLEAR: 'C',
        MIN_HEIGHT: 1,
        valid: false,
        currentEntry: null,
        currentEntries: null,
        currentEventGroup: null,
        heightHeaders: null,
        spinnerValue: 0,
        minStartHeight: 99,
        positionArray: [],
        writePositions: false,
        manualNextObj: null,
        hasResults: null,
        getFooterInfo: function(reportInfo){
            let progInfo = progressionObj.getInfo();
            if ( progInfo !== ""){
                if ( reportInfo !== '' ){
                    reportInfo += '<br>';
                }
                reportInfo += progInfo;
            }
            return reportInfo;
        },
        reloadDialog: function () {
            let dialog = $("#heightDialog");
            if (dialog.dialog("isOpen")) {
                dialog.dialog("close");
                heightCard.displayDialog();
            }
        },
        displayRows: function () {
            let html = '';
            let eventGroup = getCurrentEventGroup();
            html += '<table class="e4sTable card_table card_header table_width_100_perc">';
            html += heightCard.getHeaderRows(eventGroup);
            for (let row = 1; row <= <?php echo E4S_FIELD_CARD_COUNT ?>; row++) {
                html += heightCard.getCardRowHTML(eventGroup, row);
            }
            html += '</table>';
            $("#heightTableRows").html(html);
            // check if we can display progressions
            heightCard.displayProgessions();
        },
        displayProgessions: function () {
            let progressions = progressionObj.get(<?php echo E4S_HEIGHT_CARD_TRIALS ?> -4);
            if (isEmpty(progressions)) {
                return;
            }

            // do we have results ?
            let resultHeight = heightCard.getLatestResultHeight();
            if (resultHeight !== 0) {
                return;
            }

            let h = 1;
            for (let p in progressions) {
                let height = progressions[p];
                let heightDisp = getDecimals(height, 2);
                $("#trial_height_0_" + h++).text(heightDisp);
            }
        },
        injectHeaderRow: function (beforeRow, headerRow) {
            if (headerRow === 99) {
                return;
            }
            let eventGroup = getCurrentEventGroup();
            let html = heightCard.getHeaderRows(eventGroup, headerRow);
            let row = $("#heightRow_" + beforeRow);
            row.before(html);
        },
        displayNextHeightForNextUp: function (entry, height){
            heightCard.setNextUpHeight(height);
            let headerSub = Object.keys(heightCard.heightHeaders).length - 1;
            let eg = getCurrentEventGroup();
            heightCard.populateHeightOnCard(eg, headerSub);
        },
        checkForClearResults: function (obj) {
			<?php if (canClearResults()) { ?>
            let height = $(obj).text();
            if (height === '') {
                return;
            }
            e4sConfirm("Clear Results from " + height + "m?", "Clear Results", function () {
                heightCard.clearResults(height);
                cardChange();
            });
        },
        clearResults: function (height) {
            let egId = getCurrentEventGroup().id;
            let url = "<?php echo E4S_BASE_URL ?>results/clearheights/" + egId + "/" + height;
            $.ajax({
                type: "POST",
                url: url
            });
			<?php } ?>
        },
// from socket
        updateClearedResults: function (data) {
            let currentEg = getCurrentEventGroup();
            let curEgId = getEventGroupId(currentEg);
            let egId = data.egId;
            let eg = getEventGroupById(egId);
            let eventNo = getEventNoFromEventGroup(eg);
            let fromHeight = data.height;
            let compResults = getResults();
            let results = compResults[eventNo];

            for (let r in results) {
                if (r === "0") {
                    continue;
                }
                let athleteResults = results[r];
                for (let ar in athleteResults) {
                    let height = heightCard.getKeyInfo(ar).height;
                    if (height >= fromHeight) {
                        delete athleteResults[ar];
                    }
                }
            }
            return curEgId === egId;
        },
        isPoleVaultEvent: function (eventGroup = null) {
            if (eventGroup === null) {
                eventGroup = getCurrentEventGroup();
            }
            if (eventGroup.eOptions.pv) {
                return true;
            }
            return false;
        },
        getHeaderRows: function (eventGroup, headerCnt = 0) {
// height Card Column Titles
            let html = getHeaderInProgress();

            html += '<tr class="card_table_header" id="heightHeaderRow1">';
            html += '<td rowspan="2" class="height_card_cell height_title height_trial">&nbsp;</td>';
            html += '<td rowspan="2" class="height_card_cell height_title height_bib centre">Bib</td>';
            html += '<td rowspan="2" class="height_card_cell height_title height_age centre">Age</td>';
            html += '<td rowspan="2" class="height_card_cell height_title height_athlete">Athlete</td>';
            html += '<td rowspan="2" class="height_card_cell height_title height_club">Organisation</td>';
            if (heightCard.isPoleVaultEvent(eventGroup)) {
                html += '<td rowspan="2" class="height_card_cell height_title height_start card_data">Stands</td>';
            }
            html += '<td rowspan="2" class="height_card_cell height_title height_start card_data">SH</td>';

            trials = <?php echo E4S_HEIGHT_CARD_TRIALS ?>;
            trialCount = 1;
            while (trialCount <= trials) {
                html += '<td colspan="3" class="height_card_cell height_result_1 height_marker centre"><span onclick="heightCard.checkForClearResults(this);" id="trial_height_' + headerCnt + '_' + trialCount + '">&nbsp;</span></td>';
                trialCount++;
            }

            html += '<td rowspan="2" class="height_card_cell height_title height_totals height_best centre cardHeaderRowSpan">Best Height</td>';
            html += '<td rowspan="2" class="height_card_cell height_title height_totals centre cardHeaderRowSpan atAttemptHeader">Achieved At Attempt</td>';
            html += '<td rowspan="2" class="height_card_cell height_title height_totals centre cardHeaderRowSpan">Total Fails</td>';
            html += '<td rowspan="2" class="height_card_cell height_title height_totals centre cardHeaderRowSpan">Pos</td>';
            html += '<td rowspan="2" colspan="1" class="height_card_cell card_spare_header_col card_spare_col">&nbsp;</td>';
            html += '</tr>';
            html += '<tr class="height_header" id="heightHeaderRow2">';

            for (let trialCount = 1; trialCount <= trials; trialCount++) {
                html += '<td class="height_card_cell height_trial height_title height_result_1 centre">1</td>';
                html += '<td class="height_card_cell height_trial height_title centre">2</td>';
                html += '<td class="height_card_cell height_trial height_title centre">3</td>';
            }

            // html += '<td class="height_card_cell height_trial height_title centre"></td>';
            html += '</tr>';
            return html;
        },
        getCardRowHTML: function (eventGroup, row) {
            let html = '<tr class="height_data_row" id="heightRow_' + row + '">';
            html += '<td class="height_card_cell height_row card_data"><span id="field_row_' + row + '">' + row + '</span></td>';
            html += '<td class="height_card_cell height_bib centre" id="bib_1_' + row + '_cell"><span id="bib_1_' + row + '"></span></td>';
            html += '<td class="height_card_cell height_age centre"><span id="field_age_' + row + '"></span></td>';
            html += '<td class="height_card_cell height_athlete"><span id="field_name_' + row + '"></span></td>';
            html += '<td class="height_card_cell height_club"><span id="field_club_' + row + '"></span></td>';
            if (heightCard.isPoleVaultEvent(eventGroup)) {
                html += '<td class="height_card_cell height_stand centre"><span id="height_stand_' + row + '"></span></td>';
            }
            html += '<td class="height_card_cell height_sh centre"><span startheight=true id="height_sh_' + row + '"></span></td>';

            trial = <?php echo E4S_HEIGHT_CARD_TRIALS ?>;
            while (trial > 0) {
                trialCol = 1;
                while (trialCol <= 3) {
                    html += '<td class="height_card_cell height_result height_result_' + trialCol + '">&nbsp;</td>';
                    trialCol++;
                }
                trial--;
            }

            html += '<td class="height_card_cell height_best centre"><span id="height_best_' + row + '"></span></td>';
            html += '<td class="height_card_cell height_trials centre"><span id="height_trials_' + row + '"></span></td>';
            html += '<td class="height_card_cell height_failures centre"><span id="height_failures_' + row + '"></span></td>';
            html += '<td class="height_card_cell height_position centre"><span id="height_position_' + row + '"></span></td>';
            html += '<td class="height_card_cell card_spare_col"></td>';
            html += '</tr>';
            return html;
        },
        makeKeyInfo: function (height, trial) {
            return height + ":" + trial;
        },
        getKeyInfo: function (key) {
            key = key.split(":");
            return {
                height: parseFloat(key[0]),
                trial: parseInt(key[1])
            }
        },
        clearPositions: function () {
            heightCard.positionArray = [];
        },
        addToPositions: function (entry, height, trial, failureCount) {
            let position = {
                entryId: getEntryId(entry),
                athleteId: entry.athleteId,
                rowNo: getLaneNo(entry),
                height: height,
                trial: trial,
                failureCount: failureCount,
                positionScore: ((trial * 100) + failureCount),
                position: 0
            };
            heightCard.positionArray.push(position);
        },
        sortPositions: function () {
            let pos = 1;
            let lastPos = 0;
            let lastScore = 0;
            let height = 0;
            heightCard.positionArray.sort(function (a, b) {
                if (a.height > b.height) {
                    return -1;
                }
                if (a.height < b.height) {
                    return 1;
                }
                if (a.positionScore < b.positionScore) {
                    return -1;
                }
                if (a.positionScore > b.positionScore) {
                    return 1;
                }
                a.joint = true;
                b.joint = true;
                return 0;
            });
            for (let p in heightCard.positionArray) {
                let obj = heightCard.positionArray[p];
                if (lastPos === 0) {
                    lastHeight = obj.height;
                    lastPos = pos;
                    lastScore = obj.positionScore;
                }

                if (lastHeight === obj.height && lastScore === obj.positionScore) {
                    obj.position = lastPos;
                } else {
                    obj.position = pos;
                    lastPos = pos;
                    lastScore = obj.positionScore;
                    lastHeight = obj.height;
                }
                pos++;
            }
        },
        displayPositions: function () {
            heightCard.sortPositions();
            for (let p in heightCard.positionArray) {
                let obj = heightCard.positionArray[p];
                let joint = obj.joint ? "=" : "";
                if (obj.height > 0) {
                    $("#height_position_" + obj.rowNo).text(obj.position + joint);
                } else {
                    $("#height_position_" + obj.rowNo).text("-");
                }
            }
            if (heightCard.writePositions) {
                heightCard.writePositionsToDB();
                heightCard.writePositions = false;
            }
        },
        writePositionsToDB: function () {
            // write positions to database
            let payload = {};
            let positions = [];
            for (let p in heightCard.positionArray) {
                let obj = heightCard.positionArray[p];
                let positionObj = {
                    athleteId: obj.athleteId,
                    position: obj.position
                };
                positions.push(positionObj);
            }
            payload.egId = getCurrentEventGroup().id;
            payload.positions = positions;
            let url = "<?php echo E4S_BASE_URL ?>height/positions";
            $.ajax({
                type: "POST",
                url: url,
                data: payload,
                timeout: 2000,
                error: function (jqXHR, textStatus) {
                    if (textStatus === "timeout") {
                        e4sAlert("An error has occurred. Please click ok to reload.", "Error", function () {
                            location.reload();
                        });
                    }
                }
            })
        },

        displayCardResults: function (entry, results, bibObj) {
            // let result1s = bibObj.closest("tr").find(".height_result_1");
            let laneNo = getLaneNo(entry);
            let result1s = $("#heightRow_" + laneNo).find(".height_result_1");
            let eventGroup = getCurrentEventGroup();
            let egHeights = heightCard.getEGHeights(eventGroup);
            let bestHeight = 0;
            let bestTrial = 0;
            let failureCount = 0;
            let failureCountPostSuccess = 0;
            let retired = false;
            let index = 0;
            // ensure sorted on height
            results = heightCard.sortResults(results);
            for (let r in results) {
                let keyInfo = heightCard.getKeyInfo(r);
                let height = keyInfo.height;
                let trial = keyInfo.trial;
                let resultInfo = results[r].split(offLine.statusSep);
                let result = resultInfo[0];
                let posted = (resultInfo[1] === offLine.activeId ? true : false);

                if (result === heightCard.RETIRE) {
                    retired = true;
                }
                if (result === heightCard.FAIL) {
                    failureCount++;
                    failureCountPostSuccess++
                }
                if (result === heightCard.SUCCESS) {
                    failureCountPostSuccess = 0;
                    if (height > bestHeight) {
                        bestHeight = height;
                        bestTrial = trial;
                    }
                }
                for (let h in egHeights) {
                    if (egHeights[h] === height) {
                        index = parseInt(h);
                        break;
                    }
                }

                let resultIndex = index;
                if (heightCard.heightHeaders[entry.heightRow]) {
                    resultIndex = heightCard.heightHeaders[entry.heightRow].sub;
                }
                let updateResult = $(result1s[index - resultIndex]);
                if (trial > 1) {
                    updateResult = updateResult.next();
                    if (trial > 2) {
                        updateResult = updateResult.next();
                    }
                }
                updateResult.text(result);
                if (!posted) {
                    updateResult.addClass("resultTBC");
                }
            }
            let bestHeightNum = getDecimals(bestHeight, 2);
            if (bestHeight > 0) {
                let extraText = getResultExtraText(bestHeightNum);
                if ( extraText !== "" ){
                    bestHeight = bestHeightNum + '<span class="' + e4sGlobal.class.resultExtraText + '">' + extraText + '</span>';
                }

                $("#height_best_" + laneNo).html(bestHeight);
                $("#height_trials_" + laneNo).text(bestTrial);
                failureCount = failureCount - failureCountPostSuccess;
                $("#height_failures_" + laneNo).text(failureCount);
            } else {
                $("#height_best_" + laneNo).text("NM");
                failureCount = 99;
                bestTrial = 4;
                $("#height_failures_" + laneNo).text("");
            }

            updateEntryActive(entry, !(failureCountPostSuccess === 3 || retired));

            heightCard.addToPositions(entry, bestHeightNum, bestTrial, failureCount);
        },
        toggleCard: function () {
            $("#card-base").is(":visible") ? heightCard.hideCard() : heightCard.showCard();
        },
        hideCard: function () {
            let isSticky = e4s_readStickySettings();
            if (isSticky) {
                e4s_toggleStickySettings();
                $("#cardActionMenu").hide();
            }
            $("#card-base").hide();
        },
        showCard: function () {
            $("#card-base").show();
        },
        displayDialog: function () {
            let curEg = getCurrentEventGroup();
            let title = curEg.typeno + ":" + curEg.event;
            let openingHeight = heightCard.getOpeningHeight(curEg);
            let currentHeight = openingHeight
            let maxHeight = heightCard.getEGMaxHeight(curEg);
            let increment = heightCard.getEventGroupInc(curEg, currentHeight);
            let step = increment / 100;

            if (curEg !== heightCard.currentEventGroup) {
                heightCard.valid = false;
            }
            heightCard.currentEventGroup = curEg;
            let maxResultHeight = heightCard.getLatestResultHeight();
            if (maxResultHeight > 0) {
                currentHeight = maxResultHeight;
            }
            heightCard.currentHeight = currentHeight;
            let dialog = $("#heightDialog");
            let html = '<div id="heightDialogHTML">';
            html += "<table>";
            html += "<tr>";
            html += '<td class="heightLeftPanel">';
            html += heightCard.getAthleteDisplayHTML(openingHeight, increment);
            html += "</td>";
            html += '<td class="heightRightPanel">';
            html += heightCard.getHeightInputHTML(curEg, currentHeight, increment);
            html += '<fieldset id="athleteFieldset" class="athleteFieldset" style="display:none">';
            html += '<legend class="e4sLegend centre athleteLegend"></legend>';
            html += '<div id="heightCardContent"></div>';
            html += '<div id="heightCardCurrentContent"></div>';
            html += '</fieldset>';
            html += "</td>";
            html += "</tr>";
            html += "<tr>";
            html += '<td colspan=2 class="heightStatusDiv">';
            html += "</td>";
            html += "</tr>";
            html += "</table>";
            html += "</div>";
            dialog.html(html);
            dialog.dialog({
                title: title,
                autoOpen: false,
                modal: true,
                width: 750,
                height: 650,
                close: function () {
                    heightCard.showCard();
                    sendCurrentAthlete();
                },
                open: function () {
                    // increment
                    $(".incrementInput").spinner({
                        min: 1,
                        max: 20,
                        step: 1,
                        stop: function (event, ui) {
                            let val = heightCard.getIncrementInMetre();
                            $(".heightInput").spinner("option", "step", val);
                            let valInCm = heightCard.getIncrement();
                            progressionObj.updateProgression(heightCard.currentHeight, valInCm);
                        }
                    });
                    if (heightCard.isPoleVaultEvent(curEg)) {
                        // Stand Distances
                        $("[pvstand=1]").spinner({
                            min: 0,
                            max:  <?php echo E4S_DEFAULT_STANDS ?>,
                            step: 5,
                            numberFormat: "n",
                            stop: function (event, ui) {
                                let curVal = $(this).val();
                                if (curVal === "") {
                                    curVal = <?php echo E4S_DEFAULT_STANDS ?>;
                                }
                            }
                        });
                    }

                    let sHeightInput = $(".heightInput");
                    sHeightInput.spinner({
                        min: openingHeight,
                        max: maxHeight,
                        numberFormat: "n",
                        step: .01,
                        create: function (event, ui) {
                            event.target.value = getDecimals(event.target.value, 2);
                        },
                        spin: function (event, ui) {
                            heightCard.spinnerValue = ui.value;
                        },
                        stop: function (event, ui) {
                            // used the spinner
                            if (heightCard.spinnerValue > 0) {
                                heightCard.heightChangedBySpinner();
                                return false;
                            }
                        }
                    });
                    $(".heightDialogContent .ui-spinner-button").click(function () {
                        $(this).siblings('input').change();
                    });
                    let val2Dec = getDecimals(sHeightInput.val(), 2);
                    sHeightInput.val(val2Dec);
                }
            });
            dialog.dialog("open");
            dialog.css("overflow", "hidden");
            heightCard.setButtons();
            heightCard.updateDisplay();
            $(".spinnerDisabled").spinner("disable");
        },
        blurSpinner: function () {
            let fieldObj = $(".heightInput");
            let uiVal = fieldObj.val();
            uiVal = getFloatDecimals(uiVal, 2);
            if (isNaN(uiVal)) {
                fieldObj.val(heightCard.currentHeight);
                return;
            }
            if (heightCard.currentHeight !== uiVal) {
                // manually changed
                heightCard.heightChangedManually(uiVal);
            }
        },
        checkStartHeightInput: function (obj) {
            let curVal = $(obj).val();
            let maxHeight = heightCard.getEGMaxHeight();
            let latestHeight = heightCard.getLatestResultHeight();
            if (curVal === "") {
                curVal = heightCard.getOpeningHeight();
            } else {
                curVal = parseFloat(curVal);
            }
            if (curVal > maxHeight) {
                curVal = maxHeight;
            }
            if (curVal < latestHeight) {
                curVal = latestHeight;
            }
            let val2Dec = getDecimals(curVal, 2);
            $(obj).val(val2Dec);
        },
        getHeightForDisplay: function (height) {
            return getDecimals(height, 2) + "m";
        },
        processNewHeightManually: function () {
            let obj = heightCard.manualNextObj;
            obj.useEntries = heightCard.getHeightEntries(obj.nextHeight);
            if (obj.useEntries.length === 0) {
                heightCard.writeMessage("There are no athletes currently competing at " + heightCard.getHeightForDisplay(obj.nextHeight));
                obj.nextHeight = obj.currentHeight;
            }
            heightCard.setCurrentHeight(obj.nextHeight, obj.useEntries);
        },
        cancelNewHeightManually: function () {
            let obj = heightCard.manualNextObj;
            obj.useEntries = heightCard.getHeightEntries(obj.currentHeight);
            heightCard.setCurrentHeight(obj.currentHeight, obj.useEntries);
        },
        heightChangedManually: function (nextHeight) {
            heightCard.clearMessage();
            let currentHeight = heightCard.getLatestResultHeight();
            heightCard.currentHeight = currentHeight;
            let eg = heightCard.currentEventGroup;
            let useEntries = null;
            if (nextHeight > currentHeight) {
                // increasing count so get correct next height
                useEntries = heightCard.getHeightEntries();
                // any left at current height
                if (useEntries.length > 0) {
                    heightCard.writeMessage("Athletes still to complete " + heightCard.getHeightForDisplay(currentHeight));
                    nextHeight = currentHeight;
                } else {
                    heightCard.manualNextObj = {
                        nextHeight: nextHeight,
                        useEntries: useEntries,
                        currentHeight: currentHeight
                    };
                    if (heightCard.getActiveEntries().length > 1) {
                        e4sConfirm("There are multiple athletes still to compete. Are you sure you want to change the height?", "Change Height", heightCard.processNewHeightManually, heightCard.cancelNewHeightManually);
                    } else {
                        heightCard.processNewHeightManually();
                    }
                    return false;
                    // nextHeight = obj.nextHeight;
                    // useEntries = obj.useEntries;
                }
            } else {
                // can not go back to invalid height
                let isHeightValid = heightCard.isValidPreviousHeight(eg, nextHeight);
                if (!isHeightValid) {
                    nextHeight = currentHeight;
                }
                useEntries = heightCard.getHeightEntries(nextHeight);
            }
            let spinner = $(".heightInput").spinner();
            if (nextHeight === 0) {
                nextHeight = heightCard.getOpeningHeight(eg);
            }
            spinner.spinner("value", nextHeight);
            $("#heightInput").val(getDecimals(nextHeight, 2))
            heightCard.setCurrentHeight(nextHeight, useEntries);
            let retVal = false;
            return retVal;
        },
        heightChangedBySpinner: function () {
            heightCard.clearMessage();
            let currentHeight = heightCard.currentHeight;
            let nextHeight = heightCard.spinnerValue;
            let eg = heightCard.currentEventGroup;
            let useEntries = null;

            // reset spinnerValue
            heightCard.spinnerValue = 0;

            if (nextHeight > currentHeight) {
                // increasing count so get correct next height
                nextHeight = heightCard.getNextResultHeight(eg, currentHeight);
                useEntries = heightCard.getHeightEntries();
                // any left at current height
                if (useEntries.length > 0) {
                    heightCard.writeMessage("Athletes still to complete " + heightCard.getHeightForDisplay(currentHeight));
                    nextHeight = currentHeight;
                } else {
                    let maxHeight = heightCard.getLatestResultHeight();
                    if (nextHeight > maxHeight) {
                        useEntries = heightCard.getHeightEntries(nextHeight);
                        if (useEntries.length === 0) {
                            let nextSH = heightCard.getNextStartHeightOverHeight(nextHeight);
                            if (nextSH === 0) {
                                heightCard.writeMessage("There are no athletes currently competing at " + heightCard.getHeightForDisplay(nextHeight));
                                nextHeight = currentHeight;
                            } else {
                                nextHeight = nextSH;
                                useEntries = heightCard.getHeightEntries(nextHeight);
                            }
                        }
                    }
                }
            } else {
                // get previous result height
                nextHeight = heightCard.getPreviousHeight(eg, currentHeight);
                // set increment in case its changed for this height from progression
                let inc = getFloatDecimals(currentHeight - nextHeight, 2);
                inc = inc * 100;
                heightCard.setIncrement(inc);
                useEntries = heightCard.getEntriesFromResultsHeight(nextHeight);
            }
            let spinner = $(".heightInput").spinner();
            if (nextHeight === 0) {
                nextHeight = heightCard.getOpeningHeight(eg);
            }
            spinner.spinner("value", nextHeight);
            heightCard.setCurrentHeight(nextHeight, useEntries);
            let retVal = false;
            return retVal;
        },
        setButtons: function () {
            let dialog = $("#heightDialog");
            let buttons = [];

            buttons.push({
                text: "Toggle Card",
                click: function () {
                    heightCard.toggleCard();
                }
            });

            if (!heightCard.isValid()) {
                buttons.push({
                    text: "Start Event",
                    click: function () {
                        heightCard.validateEvent();
                    }
                });
            } else {
                buttons.push({
                    text: "Pause Event",
                    click: function () {
                        heightCard.markInValid();
                        heightCard.reloadDialog();
                    }
                });
                buttons.push({
                    text: "Event Finished",
                    click: function () {
                        markEventComplete();
                        heightCard.markInValid();
                        $(this).dialog("close");
                    }
                });
            }
            buttons.push({
                text: "Close",
                click: function () {
                    $(this).dialog("close");
                }
            });
            dialog.dialog({
                buttons: buttons
            });
        },
        clearHeightHeaders: function () {
            heightCard.heightHeaders = {
                0: {
                    // startHeight: heightCard.getNextStartHeightOverHeight(0),
                    sub: 0
                }
            };
        },
        addHeightHeader: function (sub = 99, heightSub = 0) {
            if (!heightCard.heightHeaders[sub]) {
                heightCard.heightHeaders[sub] = {
                    sub: heightSub
                };
            }
        },
        getEntriesInLaneNoOrder: function (entries) {
            entries.sort(function (a, b) {
                if (a.heatInfo.laneNo < b.heatInfo.laneNo) {
                    return -1;
                }
                if (a.heatInfo.laneNo > b.heatInfo.laneNo) {
                    return 1;
                }
                return 0;
            });
            return entries;
        },
        sortHeightEntries: function (entries) {
            let currentResults = getResultsForEG();
            let hasResults = true;
            if (!currentResults) {
                hasResults = false;
            }

            if (!hasResults) {
                return heightCard.getEntriesInLaneNoOrder(entries);
            } else {
                entries.sort(function (a, b) {
                    let noHeight = 99;
                    let aStartHeight = heightCard.getStartHeightForEntry(a);
                    let bStartHeight = heightCard.getStartHeightForEntry(b);
                    let aHeight = aStartHeight > 0 ? aStartHeight : noHeight;
                    if (!isEntryPresent(a)) {
                        aHeight = noHeight;
                    }
                    let bHeight = bStartHeight > 0 ? bStartHeight : noHeight;
                    if (!isEntryPresent(b)) {
                        bHeight = noHeight;
                    }
                    if (aHeight < bHeight) {
                        return -1;
                    }
                    if (aHeight > bHeight) {
                        return 1;
                    }
                    return 0;
                });
            }
            heightCard.clearHeightHeaders();
            let maxResultsPerHeader = <?php echo E4S_HEIGHT_CARD_TRIALS ?>;
            let eg = getCurrentEventGroup();
            let egHeights = heightCard.getEGHeights(eg, false);
            let headerSub = 0;
            let sortedEntries = [];
            for (let e in entries) {
                let entry = entries[e];
                // get entries last result
                let lastHeight = 0;
                let athleteStartHeight = heightCard.getStartHeightForEntry(entry);
                let useHeaderSub = 99;
                if (athleteStartHeight > 0) {
                    let athleteResults = currentResults[entry.athleteId];
                    if (athleteResults) {
                        lastHeight = parseFloat(Object.keys(athleteResults).pop());
                    }
                    // get where the last Result Height is in the events heights
                    let lastResultSub = heightCard.getSubForHeight(egHeights, lastHeight);
                    let currentSub = heightCard.heightHeaders[headerSub].sub;
                    if (lastResultSub - currentSub >= maxResultsPerHeader) {
                        headerSub++;
                        let startHeightSub = heightCard.getSubForHeight(egHeights, athleteStartHeight);
                        heightCard.addHeightHeader(headerSub, startHeightSub);
                    }
                    useHeaderSub = headerSub;
                }
                if (!sortedEntries[useHeaderSub]) {
                    sortedEntries[useHeaderSub] = [];
                }
                entry.heightRow = useHeaderSub;
                sortedEntries[useHeaderSub].push(entry);
            }
// merge sortedentries arrays
            let mergedEntries = [];
            for (let s in sortedEntries) {
                let sorted = sortedEntries[s];
                mergedEntries = mergedEntries.concat(sorted);
            }
            return mergedEntries;
        },
        getSubForHeight: function (heights, height) {
            let sub = 0;
            for (let h in heights) {
                let checkHeight = heights[h];
                if (height === checkHeight) {
                    sub = parseInt(h);
                    break;
                }
            }
            return sub;
        },
        sortResults: function (results) {
            let athleteResults = Object.keys(results).sort().reduce(
                (obj, key) => {
                    obj[key] = results[key];
                    return obj;
                },
                {}
            );
            return athleteResults;
        },
        hasAnyEntryGotStartHeight: function () {
            let entries = heightCard.getEntries();
            for (let e in entries) {
                let entry = entries[e];
                if (heightCard.entryHasStartHeight(entry)) {
                    return true;
                }
            }
            return false;
        },
        getMinimumStartHeight: function () {
            let entries = heightCard.getEntries();
            let minSH = heightCard.MIN_HEIGHT;
            for (let e in entries) {
                let entry = entries[e];
                let entrySH = heightCard.getStartHeightForEntry(entry);
                if (entrySH < minSH) {
                    minSH = entrySH;
                }
            }
            return minSH;
        },
        getAthleteHeightResults: function (athleteId, height = null) {
            let results = getResultsForEG();
            if (!results) {
                return {};
            }
            if (!results[athleteId]) {
                return {};
            }

            // sort AthleteResults on property
            let athleteResults = heightCard.sortResults(results[athleteId]);

            for (let r in athleteResults) {
                let aResult = athleteResults[r];
                if (aResult === "") {
                    // clean up results
                    delete athleteResults[r];
                }
            }
            if (height === null) {
                if (isEmpty(athleteResults)) {
                    return {};
                }
                return athleteResults;
            }
            let heightResults = [];
            for (let r in athleteResults) {
                let info = heightCard.getKeyInfo(r);
                if (info.height === height) {
                    heightResults[info.trial] = athleteResults[r];
                }
            }
            return heightResults;
        },
        setResultHeightsOnEventGroup: function (eg = heightCard.currentEventGroup) {
            if (eg === null) {
                eg = getCurrentEventGroup();
            }
            let results = getResultsForEG(eg);
            let heights = [];
            for (let a in results) {
                a = parseInt(a);
                if (a > 0) {
                    let athleteResults = results[a];
                    for (let r in athleteResults) {
                        let info = heightCard.getKeyInfo(r);

                        if (!heights.includes(info.height)) {
                            heights.push(info.height);
                        }
                    }
                }
            }
            // sort heights
            heights.sort(function (a, b) {
                return a - b;
            });
            eg.options.heights = heights;
            return heights;
        },
        getEGHeights: function (eg = heightCard.currentEventGroup, force = false) {
            if (!eg.options.heights || force) {
                heightCard.setResultHeightsOnEventGroup(eg);
            }
            return eg.options.heights;
        },
        getPreviousHeight: function (eg, height) {
            let heights = heightCard.getEGHeights(eg, true);
            let prevHeight = heightCard.getOpeningHeight(eg);
            if (!isEmpty(heights)) {
                for (let h in heights) {
                    let checkHeight = heights[h];
                    if (checkHeight === height) {
                        return prevHeight;
                    }
                    prevHeight = checkHeight;
                }
            }
            return prevHeight;
        },
        isValidPreviousHeight: function (eg, height) {
            let heights = heightCard.getEGHeights(eg, true);
            if (!isEmpty(heights)) {
                for (let h in heights) {
                    if (heights[h] === height) {
                        return true;
                    }
                }
            }
            return false;
        },
        getNextResultHeight: function (eg, height) {
            let heights = heightCard.getEGHeights(eg, true);
            let nextHeight = 0;
            if (!isEmpty(heights)) {
                for (let h in heights) {
                    let checkHeight = heights[h];
                    if (nextHeight < 0) {
                        nextHeight = checkHeight;
                        break;
                    }
                    if (checkHeight === height) {
                        nextHeight = -1;
                    }
                }
            }
            if (nextHeight <= 0) {
                // need to get current height plus increment
                let uiInc = heightCard.getIncrement();
                nextHeight = getFloatDecimals(height + (uiInc / 100), 2);
                let inc = heightCard.getEventGroupInc(eg, nextHeight);
                if (uiInc !== inc) {
                    heightCard.setIncrement(inc);
                }
            }
            return nextHeight;
        },
        getLatestResultHeight: function () {
            let heights = heightCard.setResultHeightsOnEventGroup();
            if (isEmpty(heights)) {
                return 0;
            }
            return heights[heights.length - 1];
        },
        populateHeightsOnCard: function (eg) {
            for (let h in heightCard.heightHeaders) {
                heightCard.populateHeightOnCard(eg, parseInt(h));
            }
        },
        setNextUpHeight: function (height) {
            heightCard.nextUpHeight = height;
        },
        getNextUpHeight: function(){
            if ( heightCard.nextUpHeight ){
                let nextUpHeight = heightCard.nextUpHeight;
                delete heightCard.nextUpHeight;
                return nextUpHeight;
            }
            return 0;
        },
        populateHeightOnCard: function (eg, headerSub) {
            let heights = heightCard.setResultHeightsOnEventGroup();
            let nextUpHeight = heightCard.getNextUpHeight();
            if ( nextUpHeight !== 0 ){
                if ( heights[heights.length -1] < nextUpHeight ){
                    heights = JSON.parse(JSON.stringify(heights));
                    heights.push(nextUpHeight);
                }
            }

            let useSub = 0;
            if (heightCard.heightHeaders[headerSub]) {
                useSub = heightCard.heightHeaders[headerSub].sub;
            }
            for (let h = useSub; h < heights.length; h++) {
                // ensure does not go over max heights for row
                let sub = h - useSub;
                if (sub < <?php echo E4S_HEIGHT_CARD_TRIALS ?>) {
                    let height = heights[h];
                    currentHeightDisplay = getDecimals(height, 2);
                    $("#trial_height_" + headerSub + '_' + (sub + 1)).text(currentHeightDisplay);
                }
            }
        },
        entryHasStartHeight: function (entry) {
            let startHeight = heightCard.getStartHeightForEntry(entry);
            return (startHeight > 0);
        },
        entryHasStand: function (entry) {
            let options = entry.eoptions;

            if (options.stands) {
                let stands = parseFloat(options.stands);
                if (stands > 0 && stands <=  <?php echo E4S_MAX_STANDS ?>) {
                    return true;
                }
            }
            return false;
        },
        validateEvent: function (prompt = true) {
            let valid = true;
            let entries = heightCard.getEntries();
            let isPoleVault = heightCard.isPoleVaultEvent();
            if (entries.length === 0) {
                valid = false;
            }
            for (let e in entries) {
                let entry = entries[e];

                if (isEntryPresent(entry)) {
                    let startHeight = heightCard.getStartHeightForEntry(entry);
                    if (startHeight === 0) {
                        valid = false;
                        break;
                    }
                    if (startHeight < heightCard.minStartHeight) {
                        heightCard.minStartHeight = startHeight;
                    }
                    if (isPoleVault) {
                        if (!heightCard.entryHasStand(entry)) {
                            valid = false;
                            break;
                        }
                    }
                }
            }
            if (!valid) {
                if (prompt) {
                    let standText = "";
                    if (isPoleVault) {
                        standText = " and Stands";
                    }
                    e4sAlert("All athletes must have a Start height" + standText + " to be set before the event can take place", "Warning", heightCard.markInValid);
                } else {
                    return heightCard.markInValid();
                }
            } else {
                return heightCard.markValid(); // set the event as valid or not
            }
        },
        isValid: function () {
            return heightCard.valid;
        },
        markValidAndReload: function (valid) {
            heightCard.valid = valid;
            heightCard.reloadDialog();
            return valid;
        },
        markValid: function () {
            return heightCard.markValidAndReload(true);
        },
        markInValid: function () {
            return heightCard.markValidAndReload(false);
        },
        getOpeningHeight: function (eg = null) {
            if (eg === null) {
                eg = heightCard.currentEventGroup;
            }
            let egId = getEventGroupId(eg);
            let heights = heightCard.getEGHeights(eg);

            if (!isEmpty(heights)) {
                return heights[0];
            }
            // do all entries have startHeights ?
            if (heightCard.isValid()) {
                return heightCard.minStartHeight;
            }
            // does it have a split on the compevent ?
            let ceObjs = getCompEventsForEg(eg);
            let ceObj = ceObjs[0]; // has to be at least one
            if (ceObj.split > 0) {
                return ceObj.split;
            }
            let progressions = progressionObj.read(eg);
            if (!isEmpty(progressions)) {
                return progressions[0].height;
            }
            if (eg.eOptions) {
                if (eg.eOptions.min) {
                    return parseFloat(eg.eOptions.min);
                }
            }
            return heightCard.MIN_HEIGHT;
        },
        getCurrentHeight: function () {
            if (!heightCard.currentHeight) {
                heightCard.currentHeight = heightCard.getCurrentHeightFromField();
            }
            return heightCard.currentHeight;

        },
        getCurrentHeightFromField: function (eg = null) {
            if (eg === null) {
                eg = heightCard.currentEventGroup;
            }
            let height = $("#heightInput").val();
            if (isNaN(height)) {
                height = heightCard.getLatestResultHeight();
                if (isNaN(height)) {
                    height = heightCard.getOpeningHeight(eg);
                }
            } else {
                height = parseFloat(height);
            }
            return height;
        },
        setCurrentHeight: function (height, entries = null) {
            heightCard.currentHeight = height;
            $("#heightInput").val(getDecimals(height, 2));
            let inc = heightCard.getEventGroupInc(heightCard.currentEventGroup, height);
            heightCard.setIncrement(inc);
            heightCard.changeHeight(entries);
        },
        getEGMaxHeight: function (eg = null) {
            if (eg === null) {
                eg = heightCard.currentEventGroup;
            }
            let eOptions = eg.eOptions;
            let max = 5;
            if (eOptions.max) {
                max = parseFloat(eOptions.max);
            }
            return max;
        },
        getEventGroupIncrementInMetre: function (eg, height = null) {
            return heightCard.getEventGroupInc(eg, height) / 100;
        },
        getEventGroupInc: function (eg, height = heightCard.currentHeight) {
            let eOptions = eg.eOptions;
            let inc = 5;
            if (eOptions.inc) {
                inc = parseFloat(eOptions.inc);
            }
            let progInc = progressionObj.getIncForheight(eg, height);
            if (progInc > 0) {
                inc = progInc;
            }
            return inc;
        },
        getIncrementInMetre: function () {
            return heightCard.getIncrement() / 100;
        },
        getIncrement: function () {
            let inc = $(".incrementInput").spinner("value");
            return parseInt(inc);
        },
        setIncrement: function (inc) {
            // inc = getDecimals(inc, 2);
            $(".incrementInput").spinner("value", inc);
        },
        changeHeight: function (entries = null) {
            // heightCard.displayContentMessage("Select Athlete");
            heightCard.displayContent();
            heightCard.updateDisplay(entries);
        },
        getHeightInputHTML: function (eventGroup, height, increment) {
            let html = '<div id="heightInputDiv">';
            html += '<label for="heightInput">Current Height:</label>';
            html += '<input id="heightInput" class="shInputStd heightInput" value="' + height + '" onblur="heightCard.blurSpinner()">';
            html += '<label style="padding-left: 10px;" for="incrementInput">Inc:</label>';
            html += '<input id="incrementInput" class="shInputStd incrementInput" value="' + increment + '">';
            html += '</div>';
            return html;
        },
        getAthleteDisplayHTML: function (openingHeight, inc) {
            let cardIsValid = heightCard.isValid();
            let html = '';
            let eventGroup = heightCard.currentEventGroup;
            let useCheckIn = isCheckInEnabled();
            html += '<table class="fieldResultsLeftHeader">';
            html += '<tr class="fieldResultsLeftRow">';
            html += '<th style="width:15px;"></th>';
            html += '<th class="heightAthlete">Athlete</th>';
            if (heightCard.isPoleVaultEvent(eventGroup)) {
                html += '<th class="heightStand">Stands</th>';
            }
            html += '<th>Start Height</th>';
            html += '</tr>';
            html += '</table>';
            html += '<div id="heightAthletesDiv" class="heightAthletesDiv">';
            html += '<table style="width:100%">';
            let entries = heightCard.getEntries();
            // if (cardIsValid) {
            entries = heightCard.getEntriesInLaneNoOrder(entries);
            // }
            for (let e in entries) {
                let entry = entries[e];
                let entryId = getEntryId(entry);
                let athlete = getAthleteById(entry.athleteId);
                let present = isEntryPresent(entry, true);
                if ( present && useCheckIn ) {
                    present = isEntryCheckedIn(entry);
                }
                if (!present && cardIsValid) {
                    continue;
                }
                let hasResults = heightCard.getAthletesResults(entry.athleteId).length > 0;
                let disabled = "disabled";

                html += '<tr athleteeid=' + entryId + ' onclick="heightCard.heightAthleteSelected();">';
                html += '<td class="shPresent"><input ' + (cardIsValid || hasResults ? disabled : '') + ' inputeid=' + entryId + ' onchange="heightCard.markPresent(this);" type=checkbox id="entryPresent_' + entryId + '" ' + (present ? 'checked' : '') + '></td>';
                html += '<td><span athleteinfo=1 class="' + (present ? "" : e4sGlobal.class.notActive) + '">' + getBibNoToUse(athlete, entry) + "</span> : " + getAthleteNameFromAthlete(athlete) + '</td>';
                let options = entry.eoptions;
                let stands = options.stands ? options.stands : "";
                let disableSpinner = "";
                if (!present) {
                    disableSpinner += " spinnerDisabled";
                }
                if (heightCard.isPoleVaultEvent(eventGroup)) {
                    html += '<td><input isspinner=1 pvstand=1 ' + (present ? '' : disabled) + ' onblur="heightCard.setStandForEntry(' + entryId + ', this.value);" class="shInputStd ' + (present ? '' : (disabled + disableSpinner)) + '" value="' + stands + '"></td>';
                }
                let startHeight = heightCard.getStartHeightForEntry(entry);
                if (startHeight > 0) {
                    startHeight = getDecimals(startHeight, 2);
                }
                html += '<td class="athleteSHEntry"><input class="shHeightsList" onblur="heightCard.shHeightBlur(this);" onfocus="heightCard.shHeightSelected(this);" onclick="heightCard.shHeightSelected(this);" list="shHeightsList" ' + (cardIsValid || hasResults || !present ? disabled : '') + ' entryid="' + entryId + '" value="' + startHeight + '">';
                html += '</td>';
                html += '</tr>';
            }
            html += '</table>';
            html += '</div>';
            html += '<datalist id="shHeightsList">';
            let showHeights = 30;
            let heights = progressionObj.get(showHeights);
            if (isEmpty(heights)) {
                heights.push(openingHeight);
                while (showHeights > 0) {
                    showHeights--;
                    openingHeight += inc;
                    heights.push(openingHeight);
                }
            }
            for (let h in heights) {
                html += '<option value=' + getDecimals(heights[h], 2) + '>';
            }

            html += '</datalist>';
            return html;
        },
        shHeightBlur: function (field) {
            let val = field.value;
            if (val === '' && field.placeholder !== '') {
                val = field.placeholder;
            }
            field.value = val;
            let entryId = parseInt($(field).attr("entryid"));
            heightCard.checkAndSetStartHeightForEntry(entryId, field);
        },
        shHeightSelected: function (field) {
            let currentVal = field.value;
            if (currentVal !== '') {
                field.placeholder = field.value;
            }
            field.value = '';
        },
        displayContent: function (entryId = null, height = null, attempt = 1) {
            if (!heightCard.isValid()) {
                return;
            }
            if (entryId === null) {
                entryId = heightCard.getCurrentEntryId();
                if (entryId === null) {
                    return;
                }
            }
            if (height === null) {
                let eg = heightCard.currentEventGroup;
                height = heightCard.getCurrentHeightFromField(eg);
            }
            let entry = getEntryByID(entryId);
            heightCard.setCurrentEntry(entry, height);
            let results = heightCard.getAthleteHeightResults(entry.athleteId, height);
            let html = '';
            html = '<table class="attemptTable">';
            html += '<tr>';
            html += '<td class="attemptLabel trial1" onclick="heightCard.setCurrentTrial(1);">1</td>';
            html += '<td class="attemptResult" trial=1  onclick="heightCard.setCurrentTrial(1);">' + heightCard.getResult(results, 1) + '</td>';
            html += '<td class="attemptLabel trial2" onclick="heightCard.setCurrentTrial(2);">2</td>';
            html += '<td class="attemptResult" trial=2  onclick="heightCard.setCurrentTrial(2);">' + heightCard.getResult(results, 2) + '</td>';
            html += '<td class="attemptLabel trial3" onclick="heightCard.setCurrentTrial(3);">3</td>';
            html += '<td class="attemptResult" trial=3  onclick="heightCard.setCurrentTrial(3);">' + heightCard.getResult(results, 3) + '</td>';
            html += '</tr>';
            html += '</table>';
            $("#heightCardContent").html(html);
            heightCard.updateAthleteFieldSet(entry);
            heightCard.displayCurrent(entry, height, results);
        },
        updateAthleteFieldSet: function (entry) {
            let athlete = getAthleteById(entry.athleteId);
            let legendText = getBibNoToUse(athlete, entry) + " : " + getAthleteNameFromAthlete(athlete);
            $(".athleteLegend").text(" " + legendText + " ");
            $("#athleteFieldset").show();
        },
        setCurrentTrial: function (trial) {
            let entry = heightCard.getCurrentEntry();
            let height = heightCard.getCurrentHeight();
            heightCard.setCurrentAttempt(entry, height, trial);
            heightCard.reDisplayCurrent(entry, height, trial);
        },
        setCurrentEntry: function (entry, height = 0) {
            heightCard.currentEntry = entry;
            heightCard.currentHeight = height;
            heightCard.markCurrentAthlete(entry);
        },
        getCurrentEntry: function () {
            return heightCard.currentEntry;
        },
        getCurrentEntryId: function () {
            let entry = heightCard.getCurrentEntry();
            if (entry !== null) {
                return getEntryId(entry);
            }
            return null;
        },
        getCurrentEntryAthleteId: function () {
            let curEntry = heightCard.getCurrentEntry();
            if (curEntry === null) {
                return 0;
            }
            return curEntry.athleteId;
        },
        eventGroupHasResults: function () {
            let results = getResultsForEG();
            if (isEmpty(results)) {
                return false;
            }
            return true;
        },
        writeResultDirect: function (eventNo, athleteId, height, trial, result) {
            let results = getResults();
            if (!results[eventNo]) {
                results[eventNo] = {};
            }
            if (!results[eventNo][athleteId]) {
                results[eventNo][athleteId] = {};
            }
            if (result === "") {
                delete results[eventNo][athleteId][height + ":" + trial];
            } else {
                results[eventNo][athleteId][height + ":" + trial] = result;
            }
            heightCard.writePositions = true;
        },
        checkForFutureResults: function (athleteId, height) {
            let athleteResults = heightCard.getAthleteHeightResults(athleteId);
            let heights = Object.keys(athleteResults);
            for (let h in heights) {
                let info = heights[h].split(":");
                let resultHeight = parseFloat(info[0]);
                if (resultHeight > height) {
                    return true;
                }
            }
            return false;
        },
        validateResult: function (entry, trial, result) {
            heightCard.clearMessage();
            let attemptedHeight = heightCard.getCurrentHeight();
            let results = heightCard.getAthleteHeightResults(entry.athleteId, attemptedHeight);
            let trial1 = results[1] ? results[1] : "";
            let trial2 = results[2] ? results[2] : "";
            let trial3 = results[3] ? results[3] : "";
            if (trial < 3 && trial3 !== "" || trial < 2 && trial2 !== "") {
                heightCard.writeMessage("Amend the latest trial only");
                return false;
            }
            let hasResultAtThisHeight = (trial1 + trial2 + trial3) !== '';
            let latestResult = '';
            // replace trial result with the passed result
            if (trial === 1) {
                latestResult = trial1;
                trial1 = result;
            } else if (trial === 2) {
                latestResult = trial2;
                trial2 = result;
            } else if (trial === 3) {
                latestResult = trial3;
                trial3 = result;
            }
            let hasResultAtFutureHeight = heightCard.checkForFutureResults(entry.athleteId, attemptedHeight);

            if (hasResultAtFutureHeight) {
                heightCard.writeMessage("Athlete has results in at a later height. Amend the higher result(s) first");
                return false;
            }
            let newResult = latestResult === "" ? true : false;
            if (newResult) {
                if (!heightCard.isOkForheight(entry, attemptedHeight)) {
                    // heightCard.writeMessage("Athlete can not attempt this height");
                    return false;
                }
            }
            if ((trial1 === '' && trial > 1) || (trial2 === '' && trial > 2)) {
                heightCard.writeMessage("Blank attempts not allowed");
                return false;
            }
            if ((trial1 === heightCard.SUCCESS && trial > 1) || (trial2 === heightCard.SUCCESS && trial > 2)) {
                heightCard.writeMessage("Athlete has already cleared the height");
                return false;
            }
            if ((trial1 === heightCard.HEIGHT_PASS && trial > 1) || (trial2 === heightCard.HEIGHT_PASS && trial > 2)) {
                heightCard.writeMessage("Athlete has already passed this height");
                return false;
            }
            if ((trial1 === heightCard.RETIRE && trial > 1) || (trial2 === heightCard.RETIRE && trial > 2)) {
                heightCard.writeMessage("Athlete has retired at this height");
                return false;
            }
            // has future results ?
            if ((trial3 !== '' && trial < 3) || (trial2 !== '' && trial < 2)) {
                heightCard.writeMessage("To keep the event in order, you must amend the future results first");
                return false;
            }
            return true;
        },
        writeTrialResult: function (trial, result) {
            let entry = heightCard.getCurrentEntry();
            if (!heightCard.validateResult(entry, trial, result)) {
                return;
            }
            $("[trial=" + trial + "]").html(result);
            let eventNo = getSelectedEventNo();
            let currentHeight = getDecimals(heightCard.getCurrentHeight(), 2);

            let results = {
                "<?php echo E4S_RESULT_PARAMS ?>": {
                    "row": getEntryId(entry),
                    "athleteId": entry.athleteId,
                    "eventNo": eventNo
                },
                "result": {
                    "height": currentHeight,
                    "trial": trial,
                    "result": result
                }
            };
            // write to results direct in case websocket not timely.
            heightCard.writeResultDirect(eventNo, entry.athleteId, currentHeight, trial, result);
            heightCard.outputAthleteResults(entry.athleteId);
            writeAthleteResultsToDB(results, false, true);
            heightCard.outputCounts();
            heightCard.checkLastAthlete(entry, trial, result);
        },
        checkLastAthlete: function(entry, trial, result){
            console.log("Trial : " + trial);
            console.log("Result : " + result);
            let msg = "";
            let activeEntries = heightCard.getActiveEntries();
            if ( activeEntries.length === 1 ) {
                // last athlete standing
                if (activeEntries[0].athleteId === entry.athleteId){
                    // last athlete is the current athlete
                    if ( result === this.SUCCESS || !isNaN(result)) {
                        // last athlete has cleared the height
                        msg = "Last Athlete. Check Incremement before continuing.";
                    }
                }else {
                    // Current athlete out, leaving just 1 left
                    msg = "One Athlete remaining. Check Incremement before continuing.";
                }
            }
            $("#lastAthleteMsg").html(msg);
        },
        hasMessage: function () {
            return $("#heightCardContentMsg").text() !== '';
        },
        clearMessage: function () {
            $("#heightCardContentMsg").html('');
        },
        writeMessage: function (message) {
            let msgObj = $("#heightCardContentMsg");
            if (msgObj.text() === '') {
                msgObj.html(message);
            }
        },
        clearHeight: function () {
            e4sConfirm("Are you sure you want to clear the current height?", "Clear Height Results", function () {
                let entry = heightCard.getCurrentEntry();
                let currentHeight = heightCard.getCurrentHeight();

            });
        },
        displayCurrent: function (entry, height, currentHeightResults, trial = 0) {
            let trial1 = heightCard.getResult(currentHeightResults, 1);
            let trial2 = heightCard.getResult(currentHeightResults, 2);
            let trial3 = heightCard.getResult(currentHeightResults, 3);
            let html = '';
            if (trial === 0) {
                if (trial1 === '') {
                    trial = 1;
                } else if (trial2 === '') {
                    trial = 2;
                } else {
                    trial = 3;
                }
            }

            heightCard.setCurrentAttempt(entry, height, trial);

            let trialText = "First";
            if (trial === 2) {
                trialText = "Second";
            } else if (trial === 3) {
                trialText = "Third";
            }
            html += '<div class="heightCurrentAttempt">';
            html += '<div class="heightCurrentAttemptLabel">' + trialText + ' Attempt at ' + getDecimals(height, 2) + 'm</div>';
            html += '<div id="heightCardContentMsg" class="heightCardContentMsg"></div>';
            html += '<table class="currentAttemptTable">';
            html += '<tr>';
            html += '<td class="attemptLabel"><button class="ui-button ui-corner-all ui-widget" onclick="heightCard.writeTrialResult(' + trial + ',\'' + heightCard.SUCCESS + '\')";>' + heightCard.SUCCESS + '</button></td>';
            html += '<td class="attemptLabel"><button class="ui-button ui-corner-all ui-widget"  onclick="heightCard.writeTrialResult(' + trial + ',\'' + heightCard.FAIL + '\')";>' + heightCard.FAIL + '</button></td>';
            html += '<td class="attemptLabel"><button class="ui-button ui-corner-all ui-widget"  onclick="heightCard.writeTrialResult(' + trial + ',\'' + heightCard.HEIGHT_PASS + '\')";>' + heightCard.HEIGHT_PASS + '</button></td>';
            html += '<td class="attemptLabel"><button class="ui-button ui-corner-all ui-widget"  onclick="heightCard.writeTrialResult(' + trial + ',\'' + heightCard.TRIAL_PASS + '\')";>' + heightCard.TRIAL_PASS + '</button></td>';
            html += '<td class="attemptLabel"><button class="ui-button ui-corner-all ui-widget"  onclick="heightCard.writeTrialResult(' + trial + ',\'' + heightCard.RETIRE + '\')";>' + heightCard.RETIRE + '</button></td>';
            html += '<td class="attemptLabel"><button class="ui-button ui-corner-all ui-widget"  onclick="heightCard.writeTrialResult(' + trial + ',\'\')";>' + heightCard.CLEAR + '</button></td>';
            html += '</tr>';
            html += '</table>';
            html += '<div id="lastAthleteMsg"></div>';
            html += '<span id="nextAthlete"><button class="ui-button ui-corner-all ui-widget nextAthleteBtn" onclick="heightCard.nextAthlete()">Continue / Next Athlete</button></span>';
            html += '<div id="resultsHistory" class="resultsHistory">';
            html += heightCard.getAthleteResultsHTML(entry.athleteId, height);
            html += '</div>';
            $("#heightCardCurrentContent").html(html);
        },
        outputAthleteResults: function (athleteId) {
            let height = heightCard.getCurrentHeight();
            let html = heightCard.getAthleteResultsHTML(athleteId, height);
            $("#resultsHistory").html(html);
        },
        getAthleteTrialHTML: function (trials) {
            let html = '';
            if (trials[1]) {
                html += '<td class="heightResultTrial">T1 : ' + trials[1] + '</td>';
                if (trials[2]) {
                    html += '<td class="heightResultTrial">T2 : ' + trials[2] + '</td>';
                    if (trials[3]) {
                        html += '<td class="heightResultTrial">T3 : ' + trials[3] + '</td>';
                    }
                }
            }
            html += '</tr>';
            return html;
        },
        getAthleteResultsHTML: function (athleteId, height) {
            let html = '';
            let athleteResults = heightCard.getAthletesResults(athleteId, false);
            let lastHeight = 0;
            let heightResultClass = '';
            // heightCard.clearMessage();

            let failCount = 0;
            let retired = false;
            let trials;
            for (let r in athleteResults) {
                let result = athleteResults[r];
                let resultHeight = result.height;

                if (resultHeight !== lastHeight) {
                    if (html === '') {
                        html = '<div id="currentResultsHistory" class="heightResults">';
                        html += '<table>';
                    } else {
                        html += heightCard.getAthleteTrialHTML(trials);
                    }
                    // set trials for this height
                    trials = {};
                    html += '<tr class="heightResultHeight ' + heightResultClass + '"><td>Height : ' + getDecimals(resultHeight, 2) + '</td>';
                    if (heightResultClass === '') {
                        heightResultClass = 'heightResultAlt';
                    } else {
                        heightResultClass = '';
                    }
                    lastHeight = resultHeight;
                }
                if (result.result !== '') {
                    // store trial rather than output to ensure correct order on display, irrelevant of if asc or desc
                    trials[result.trial] = result.result;
                    if (height === result.height) {
                        if (result.result === heightCard.SUCCESS) {
                            heightCard.writeMessage('Athlete Cleared Height.');
                        }
                        if (result.result === heightCard.HEIGHT_PASS) {
                            heightCard.writeMessage('Athlete Passed ' + height + ' Height.');
                        }
                    }
                    if (result.result === heightCard.RETIRE) {
                        retired = true;
                        heightCard.writeMessage('Athlete Retired at ' + getDecimals(resultHeight, 2) + "m");
                    }
                    if (!retired) {
                        if (result.result === heightCard.FAIL) {
                            failCount++;
                            if (failCount === 3) {
                                heightCard.writeMessage('Athlete Failed 3 times in succession.');
                            }
                        }
                        if (result.result === heightCard.SUCCESS) {
                            failCount = 0;
                        }
                    }
                }
            }
            if (lastHeight !== 0) {
                html += heightCard.getAthleteTrialHTML(trials);
                html += '</table>';
                html += '</div>';

                let headHTML = '<fieldset class="resultFieldSet">';
                headHTML += '<legend class="e4sLegend centre"> Result History </legend>';
                html = headHTML + html + "</fieldset>";
            }
            return html;
        },
        getAthletesResults: function (athleteId, asc = true) {
            let results = getResults();
            let eventNo = heightCard.currentEventGroup.eventno;
            let athleteResults = [];
            if (results[eventNo]) {
                if (results[eventNo][athleteId]) {
                    athleteResults = results[eventNo][athleteId];
                }
            }
            let arr = [];
            for (let r in athleteResults) {
                let info = heightCard.getKeyInfo(r);
                let height = info.height;
                let trial = info.trial;
                let result = athleteResults[r];
                let resultObj = {
                    height: height,
                    trial: trial,
                    result: result
                }
                arr.push(resultObj);
            }
            // sort arr on height and trial
            arr.sort(function (a, b) {
                if (a.height < b.height) {
                    if (asc) {
                        return -1;
                    }
                    return 1;
                }
                if (a.height > b.height) {
                    if (asc) {
                        return 1;
                    }
                    return -1;
                }
                if (a.trial < b.trial) {
                    if (asc) {
                        return -1;
                    }
                    return 1;
                }
                if (a.trial > b.trial) {
                    if (asc) {
                        return 1;
                    }
                    return -1;
                }
                return 0;
            });
            return arr;
        },

        getHeightStatus: function (results, height = null) {
            let failCount = 0;
            let retObj = {};
            // let increment = heightCard.getIncrementInMetre();
            retObj.available = isEmpty(results);
            retObj.status = heightCard.CLEAR;
            retObj.trial = 0;
            retObj.height = 0;
            retObj.failCount = 0;

            if (isEmpty(results)) {
                return retObj;
            }

            if (height === null) {
                height = parseFloat(heightCard.getCurrentHeight());
            }
            // ensure we dont get 9.999999999991
            // let prevHeight = getFloatDecimals(height - increment, 2);
            let prevHeight = heightCard.getPreviousHeight(heightCard.currentEventGroup, height);
            for (let r in results) {
                let result = results[r];
                let keyInfo = heightCard.getKeyInfo(r);
                let resultHeight = keyInfo.height;
                let resultTrial = keyInfo.trial;

                if (resultHeight <= height) {
                    if (result !== '') {
                        retObj.height = resultHeight;
                        retObj.trial = resultTrial;
                        retObj.status = result;

                        switch (result) {
                            case heightCard.SUCCESS:
                                retObj.failCount = 0;
                                break;
                            case heightCard.FAIL:
                                retObj.failCount++;
                                break;
                        }
                    }
                    if (retObj.status === heightCard.RETIRE || retObj.failCount === 3) {
                        break;
                    }
                }
            }
            let lastResultHeightAndTrial = Object.keys(results).pop();
            let lastResultHeight = parseFloat(lastResultHeightAndTrial.split(":")[0]);
            if (lastResultHeight > height) {
                retObj.available = false;
            } else {
                if (retObj.status === heightCard.RETIRE || retObj.failCount > 2 || retObj.height < prevHeight) {
                    retObj.available = false;
                } else {
                    if (retObj.height === height) {
                        retObj.available = true;
                        if (retObj.status === heightCard.SUCCESS || retObj.status === heightCard.HEIGHT_PASS) {
                            retObj.available = false;
                        }
                    } else {
                        if (retObj.status === heightCard.SUCCESS || retObj.status === heightCard.HEIGHT_PASS) {
                            retObj.available = true;
                        }
                    }
                }
            }
            return retObj;
        },
        isOkForheight: function (entry, height) {
            let retVal = false;
            if (!isEntryPresent(entry)) {
                return retVal;
            }
            let startHeight = heightCard.getStartHeightForEntry(entry);
            if (startHeight > 0 && startHeight <= height) {
                // check athletes results
                let results = heightCard.getAthleteHeightResults(entry.athleteId);
                let status = heightCard.getHeightStatus(results, height);
                retVal = status.available;
            }
            return retVal;
        },
        getEntriesFromResultsHeight: function (height) {
            let entries = heightCard.getEntries();
            let currentEntries = [];
            for (let e in entries) {
                let entry = entries[e];
                let results = heightCard.getAthleteHeightResults(entry.athleteId);
                if (!isEmpty(results)) {
                    for (let r in results) {
                        let keyInfo = heightCard.getKeyInfo(r);
                        if (keyInfo.height === height) {
                            currentEntries.push(entry);
                            break;
                        }
                    }
                }
            }
            return currentEntries;
        },
        getHeightEntries: function (height = null) {
            let entries = heightCard.getEntries();
            if (height === null) {
                height = parseFloat(heightCard.getCurrentHeight());
            }
            let currentEntry = heightCard.getCurrentEntry();
            let nextEntry = null;
            let currentEntries = [];
            for (let e in entries) {
                let entry = entries[e];
                entry.next = false;
                if (heightCard.isOkForheight(entry, height)) {
                    currentEntries.push(entry);
                    if (nextEntry === null) {
                        nextEntry = entry;
                    }
                }
                if (currentEntry !== null && getEntryId(entry) === getEntryId(currentEntry)) {
                    nextEntry = null;
                }
            }
            if (nextEntry !== null) {
                nextEntry.next = true;
            }
            return currentEntries;
        },
        getNextHeight: function () {
            let entries = heightCard.getEntries();
            let currentHeight = parseFloat(heightCard.getCurrentHeight());
            let increment = heightCard.getIncrementInMetre();
            let nextHeight = currentHeight + increment;
            let nextHeightEntries = heightCard.getHeightEntries(nextHeight);
            if (nextHeightEntries.length > 0) {
                return nextHeight;
            }
            for (let e in entries) {
                let entry = entries[e];
                let startHeight = heightCard.getStartHeightForEntry(entry);
                if (startHeight > currentHeight) {
                    return startHeight;
                }
            }
            return 0;
        },
        getNextHeightEntries: function () {
            let entries = heightCard.getEntries();
            let currentHeight = parseFloat(heightCard.getCurrentHeight());
            let eg = heightCard.currentEventGroup;
            let nextHeight = heightCard.getNextResultHeight(eg, currentHeight);
            let nextHeightEntries = heightCard.getHeightEntries(nextHeight);
            if (nextHeightEntries.length > 0) {
                heightCard.setCurrentHeight(nextHeight);
                return nextHeightEntries;
            }
            // get next Start Height
            currentHeight = 0;
            for (let e in entries) {
                let entry = entries[e];
                if (isEntryPresent(entry) === false) {
                    continue;
                }
                let startHeight = heightCard.getStartHeightForEntry(entry);
                if (startHeight >= nextHeight) {
                    heightCard.setCurrentHeight(startHeight);
                    return heightCard.getHeightEntries(startHeight);
                }
            }
            return [];
        },
        nextAthlete: function () {
            if ( heightCard.getCurrentTrialResult() === '' ) {
                heightCard.writeMessage("Select a result for the current trial");
                return;
            }
            heightCard.updateDisplay();
            if (heightCard.currentEntries.length === 0) {
                // no more athletes
                heightCard.writeMessage("No more athletes to attempt height. Competition Complete.");
            }
        },
        displayNextEntry: function (entryId) {
            if (entryId === null) {
                if (heightCard.currentEntries !== null && heightCard.currentEntries.length > 0) {
                    entryId = getEntryId(heightCard.currentEntries[0]);
                }
            }
            if (entryId !== null) {
                heightCard.displayContent(entryId);
            }
        },
        clearCurrentAthlete: function (preSelect = "tr[athleteeid]") {
            let useClass = e4sGlobal.class.nextUp;
            let obj = $(preSelect + "." + useClass);
            obj.removeClass(useClass);
        },

        markCurrentAthlete: function (entry) {
            heightCard.clearCurrentAthlete();
            let obj = $("tr[athleteeid=" + getEntryId(entry) + "]");
            obj.addClass(e4sGlobal.class.nextUp);
            entry.attemptHeight = heightCard.currentHeight;
            sendCurrentAthlete(entry);
        },
        getNextStartHeightOverHeight: function (height) {
            let entries = heightCard.getEntries();
            let nextSH = 999;
            for (let e in entries) {
                let entry = entries[e];
                let checkSH = heightCard.getStartHeightForEntry(entry);
                if (checkSH > height) {
                    if (checkSH < nextSH) {
                        nextSH = checkSH;
                    }
                }
            }
            if (nextSH === 999) {
                nextSH = 0;
            }
            return nextSH;
        },
        getEntriesStartingOverHeight: function (height) {
            let entries = heightCard.getEntries();
            let arr = [];
            for (let e in entries) {
                let entry = entries[e];
                let startHeight = heightCard.getStartHeightForEntry(entry);
                if (startHeight > height) {
                    arr.push(entry);
                }
            }
            return arr;
        },
        outputCounts: function () {
            let currentHeightCount = heightCard.getEntriesFromResultsHeight(heightCard.currentHeight).length;
            let results = heightCard.setResultHeightsOnEventGroup();
            let latestHeight = 1;
            if (results.length > 0) {
                latestHeight = results[results.length - 1];
            }
            if (latestHeight < heightCard.currentHeight) {
                // new height attempted
                latestHeight = heightCard.currentHeight;
            }
            let latestHeightActiveCount = heightCard.getHeightEntries(latestHeight).length;
            let latestHeightCount = heightCard.getEntriesFromResultsHeight(latestHeight).length;
            let activeEntryCount = heightCard.getActiveEntries().length;

            let html = '<div class="heightResultStatus">';
            html += '<div>Attempted ' + getDecimals(heightCard.currentHeight, 2) + 'm : <span class="statusKPI">' + currentHeightCount + '</span></div>';
            html += '<div>To Clear ' + getDecimals(latestHeight, 2) + 'm : <span class="statusKPI">' + latestHeightActiveCount + '</span></div>';
            html += '<div>Left in Event : <span class="statusKPI">' + activeEntryCount + '</span></div>';
            html += "</div>";
            $(".heightStatusDiv").html(html);
        },
        getActiveEntries: function () {
            let entries = getEntriesForCurrentEg();
            let currentHeight = heightCard.getCurrentHeight();
            let activeEntries = [];
            for (let e in entries) {
                let entry = entries[e];
                if (isEntryPresent(entry) === false) {
                    continue;
                }
                let addEntry = false;
                let sh = heightCard.getStartHeightForEntry(entry);
                if (sh > 0) {
                    if (sh > currentHeight) {
                        addEntry = true;
                    } else {
                        let results = heightCard.getAthletesResults(entry.athleteId, false);
                        if (isEmpty(results)) {
                            addEntry = true;
                        } else {
                            let failCount = 0;
                            for (let r in results) {
                                let result = results[r];
                                if (result.result === heightCard.RETIRE) {
                                    failCount = 99;
                                    break;
                                }
                                if (result.result === heightCard.SUCCESS) {
                                    failCount = 0;
                                    break;
                                }
                                failCount++;
                                if (failCount === 3) {
                                    break;
                                }
                            }
                            if (failCount < 3) {
                                addEntry = true;
                            }
                        }
                    }
                }
                if (addEntry) {
                    activeEntries.push(entry);
                }
            }
            return activeEntries;
        },
        updateDisplay: function (entries = null) {
            heightCard.outputCounts();
            return heightCard.markCurrentAthletes(entries)
        },

        markCurrentAthletes: function (entries = null) {
            heightCard.clearMessage();
            let nextEntryId = null;
            // get next athlete based on current entry
            if (heightCard.currentEntries !== null && heightCard.getCurrentEntry() !== null) {
                let currentEntryId = heightCard.getCurrentEntryId()
                let setNext = false;
                // get next one after currentEntryId
                for (let e in heightCard.currentEntries) {
                    let entry = heightCard.currentEntries[e];
                    if (setNext) {
                        nextEntryId = getEntryId(entry);
                        break;
                    }
                    if (getEntryId(entry) === currentEntryId) {
                        setNext = true;
                    }
                }
                if ( nextEntryId === null ) {
                    // current entry not found. This means they manually clicked on an entry, so need to find next one from ui
                    let obj = $("tr[athleteeid=" + currentEntryId + "]");
                    let isNextActive = false;
                    let nextObj = obj.next();
                    while (nextObj.length > 0) {
                        // if nextObj has Class heightActiveAthlete
                        if (nextObj.hasClass("heightActiveAthlete")) {
                            nextEntryId = parseInt(nextObj.attr("athleteeid"));
                            break;
                        }else {
                            nextObj = nextObj.next();
                        }
                    }
                }
            }
            // get entries for current height
            if (entries === null) {
                entries = heightCard.getHeightEntries();
                if (isEmpty(entries)) {
                    // No one left at current height, get next height
                    entries = heightCard.getNextHeightEntries();
                }
            }
            heightCard.currentEntries = entries;
            heightCard.displayNextEntry(nextEntryId);
            $(".heightActiveAthlete").removeClass("heightActiveAthlete");
            heightCard.clearCurrentAthlete();
            for (let e in entries) {
                let entry = entries[e];
                let obj = $("tr[athleteeid=" + getEntryId(entry) + "]");

                obj.addClass("heightActiveAthlete");
                if (heightCard.getCurrentEntry() !== null && entry.athleteId === heightCard.getCurrentEntryAthleteId()) {
                    heightCard.markCurrentAthlete(entry);
                }
            }
            if (heightCard.getCurrentEntry() !== null) {
                // scroll currentEntry into view
                let obj = $("[athleteeid=" + heightCard.getCurrentEntryId() + "]");
                if (obj.length > 0) {
                    obj[0].scrollIntoView();
                }
            }
            return entries;
        },
        reDisplayCurrent: function (entry, height, trial) {
            let results = heightCard.getAthleteHeightResults(entry.athleteId, height);
            heightCard.displayCurrent(entry, height, results, trial);
        },
        setCurrentAttempt: function (entry, height, trial) {
            $(".activeAttempt").removeClass("activeAttempt");
            $(".trial" + trial).addClass("activeAttempt");
            heightCard.currentTrial = trial;
        },
        getCurrentTrialResult: function(){
            return $("[trial=" + heightCard.currentTrial + "]").text();
        },
        getResult: function (results, attempt) {
            if (results[attempt]) {
                return results[attempt];
            }
            return "";
        },
        heightAthleteSelected: function () {
            if (!heightCard.isValid()) {
                return;
            }
            if ($(event.target).attr("type") === "checkbox") {
                return;
            }
            heightCard.clearMessage();
            let targetObj = $(event.target).closest('tr');
            let entryId = parseInt(targetObj.attr("athleteeid"));
            let entry = getEntryByID(entryId);
            if (isEntryPresent(entry)) {
                heightCard.displayContent(entryId);
            } else {
                heightCard.writeMessage("Please select an athlete that is present");
            }
        },
        markPresent: function (element) {
            let obj = $(element);
            let entryId = parseInt(obj.attr("inputeid"));
            let present = obj.prop("checked");
            let entry = getEntryByID(entryId);
            setEntryPresent(entry, present);
            updateDBPresent(entry);
            heightCard.enableRow(obj.closest('tr'), present);
        },
        enableRow: function (obj, enable) {
            // enable the input fields
            let disable = enable ? "" : "disabled";
            let inputs = obj.find("input:not('[type=checkbox]')");
            let allSpinners = obj.find("[isspinner=1]");
            inputs.prop("disabled", disable);
            if (enable) {
                let curHeight = heightCard.getCurrentHeight();
                allSpinners.spinner("enable");
            } else {
                allSpinners.spinner("disable");
            }
            // add/remove strikethrough
            // markObjActive(obj.find("[athleteinfo]"),!enable);
        },
        getEntries: function () {
            return getEntriesForCurrentEg();
        },
        getStartHeightForEntry: function (entry) {
            let options = entry.eoptions;
            if (options.startHeight) {
                return parseFloat(options.startHeight);
            }
            return 0;
        },
        getStandsForEntry: function (entry) {
            let options = entry.eoptions;
            if (options.stands) {
                return options.stands;
            }
            return '';
        },
        setStandForEntry: function (entryId, distance) {
            if (distance === "") {
                return;
            }
            let entry = getEntryByID(entryId);

            if (entry === false) {
                e4sAlert("Can not get athlete", "Error");
            } else {
                let url = "https://<?php echo E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH ?>/entry/stands/" + entryId + "/" + distance;
                $.post(url);
            }
        },
        checkAndSetStartHeightForEntry: function (entryId, obj) {
            heightCard.checkStartHeightInput(obj);
            heightCard.setStartHeightForEntry(entryId, obj.value);
        },
        setStartHeightForEntry: function (entryId, height) {
            let entry = getEntryByID(entryId);

            if (entry === false) {
                e4sAlert("Can not get athlete", "Error");
            } else {
                entry.eoptions.startHeight = parseFloat(height);
                let url = "https://<?php echo E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH ?>/entry/startheight/" + entryId + "/" + height;
                $.post(url);
            }
        },
        updateEntryStartHeight: function (payload) {
            let entryId = payload.entryId;
            let entry = getEntryByID(entryId);
            entry.eoptions.startHeight = payload.startHeight;

            return isActiveEntryId(entryId);
        },
        updateEntryStands: function (payload) {
            let entryId = payload.entryId;
            let entry = getEntryByID(entryId);
            entry.eoptions.stands = payload.stands;

            return isActiveEntryId(entryId);
        }
    }
    let dragDropCard = {
        currentRaceNo: 0, // Current position of dragging item
        currentLaneNo: 0,
        newRaceInfo: null,
        initialised: false,

        init: function () {
            dragDropUtil.init();
            dragDropCard.initialised = true;

            $("." + dragDropUtil.prefix + "flex_row").sortable(
                {
                    // placeholder: "dragDropPlaceholder22",
                    connectWith: "ul",
                    tolerance: "pointer",
                    revert: true,
                    items: "li:not(." + dragDropUtil.prefix + "blank)",
                    start: function (event, ui) {
                        // Dragged Event started
                        $(ui.item).addClass("dragDropItem");
                        let itemId = ui.item[0].id;

                        dragDropUtil.start(itemId);
                        dragDropCard.setCurrentRaceInfo(dragDropUtil.initRaceNo);
                        dragDropCard.currentLaneNo = dragDropUtil.initLaneNo;
                    },

                    change: function (event, ui) {
                        // dragged event to another lane
                        let leavingRaceNo = dragDropCard.currentRaceNo;
                        let leavingLaneNo = dragDropCard.currentLaneNo;
                        let placeId = ui.placeholder.parent().attr("id");
                        let initRaceNo = dragDropUtil.initRaceNo;
                        let initLaneNo = dragDropUtil.initLaneNo;

                        if (typeof placeId === "undefined") {
                            // dragged over the header row. Not sure why sortable thinks this is ok due to selector on items.
                            dragDropCard.displayLeavingRace(leavingRaceNo);
                            dragDropCard.setCurrentItem(ui, false);
                            return;
                        }
                        dragDropCard.setCurrentRaceInfo(dragDropUtil.getRaceNoFromId(placeId));

                        if (dragDropCard.currentLaneNo === 0) {
                            // safety stop. should not get here
                            dragDropCard.currentLaneNo = leavingLaneNo
                        }

                        let currentRaceNo = dragDropCard.currentRaceNo;
                        let currentLaneNo = dragDropCard.currentLaneNo;
                        let blankLanes = dragDropUtil.getBlankLanes(currentRaceNo);
                        if (currentRaceNo === leavingRaceNo) {
                            if (blankLanes.includes(currentLaneNo)) {
                                dragDropCard.displayCurrentRace(event, ui);
                            }
                            // hide the blank element if it is showing for the current item being dragged
                            if (currentRaceNo === initRaceNo && currentLaneNo !== initLaneNo) {
                                // check at least 1 column is displayed for each lane
                                for (let l = 1; l <= dragDropUtil.laneCount; l++) {
                                    if (!dragDropCard.isItemVisible(currentRaceNo, l, false)) {
                                        if (!dragDropCard.isItemVisible(currentRaceNo, l, true)) {
                                            dragDropCard.displayCell(currentRaceNo, l, true, false);
                                        }
                                    }
                                }
                                if (blankLanes.includes(currentLaneNo)) {
                                    // moved to a "blank" lane, make sure the current lane is therefor blanked and the original is still displayed.
                                    dragDropCard.displayCell(currentRaceNo, currentLaneNo, false, true);
                                    dragDropCard.displayCell(currentRaceNo, currentLaneNo, false, false);
                                    dragDropCard.displayCell(currentRaceNo, initLaneNo, true, true);
                                } else {
                                    let hideBlank = false;
                                    let hideLane = dragDropCard.getBlankLaneToHide(blankLanes, currentLaneNo);
                                    if (hideLane === initLaneNo === currentLaneNo) {
                                        hideBlank = true;
                                    }
                                    dragDropCard.displayCell(currentRaceNo, hideLane, false, hideBlank);
                                    let x = 1;
                                }
                            } else {
                                let hideLane = dragDropUtil.initLaneNo;
                                let blankLane = true;
                                if (currentRaceNo === initRaceNo) {
                                    if (currentLaneNo !== initLaneNo) {
                                        if (blankLanes.length === 1) {
                                            hideLane = blankLanes[0];
                                        } else {
                                            if (blankLanes.includes(currentLaneNo)) {
                                                dragDropCard.displayCurrentRace(event, ui);
                                                hideLane = currentLaneNo;
                                            }
                                        }
                                    } else {
                                        if (leavingLaneNo > dragDropUtil.laneCount) {
                                            // for some reason, if moving up to init lane gives different action to moving down to initlane
                                            blankLane = false;
                                        }
                                    }
                                    if (blankLane) {
                                        dragDropCard.displayCell(currentRaceNo, leavingLaneNo, true, false);
                                    }
                                }
                                if (blankLane) {
                                    dragDropCard.displayCell(currentRaceNo, hideLane, false, true);
                                }
                            }

                        } else {
                            // make sure leaving race is displayed correctly
                            dragDropCard.displayLeavingRace(leavingRaceNo);
                            dragDropCard.displayCurrentRace(event, ui);
                        }
                        dragDropCard.setCurrentItem(ui);
                        dragDropCard.markInitLane();
                    },
                    stop: function (event, ui) {
                        // Dropped final event
                        if (dragDropUtil.isItemDisabled(ui)) {
                            cardChange();
                        } else {
                            dragDropCard.dragDropUpdateDB();
                            // cardChange();
                        }
                    }
                }
            )
        },
        getCurrentLaneNo: function (useRaceNo) {
            let raceItems = $('#' + dragDropUtil.prefix + dragDropUtil.race + useRaceNo + ' li');
            let itemLaneNo = 0;
            let initLaneNo = dragDropUtil.initLaneNo;
            let className = '<?php echo E4S_CARD_FIELD_PREFIX ?>init';
            let currentLaneNo = 0;
            raceItems.each(
                function (i, item) {
                    if (i !== 0 && currentLaneNo === 0) {
                        let id = item.id;
                        if (id !== (dragDropUtil.prefix + dragDropUtil.initRaceNo + "_" + dragDropUtil.initLaneNo)) {
                            if (id !== "") {
                                if (dragDropCard.isVisible(id)) {
                                    itemLaneNo++;
                                }
                            } else {
                                currentLaneNo = itemLaneNo + 1;
                            }
                        }
                    }
                }
            )
            return currentLaneNo;
        },
        markInitLane: function () {
            let useRaceNo = dragDropUtil.initRaceNo;
            let raceItems = $('#' + dragDropUtil.prefix + dragDropUtil.race + useRaceNo + ' li');
            let itemLaneNo = 1;
            let initLaneNo = dragDropUtil.initLaneNo;
            let className = '<?php echo E4S_CARD_FIELD_PREFIX ?>init';

            raceItems.each(
                function (i, item) {
                    if (i !== 0) {
                        let id = item.id;
                        if (id === '' || ((id !== '<?php echo E4S_CARD_FIELD_PREFIX ?>' + useRaceNo + '_' + initLaneNo) && dragDropCard.isVisible(id))) {
                            if (id !== '') { // placeholder will not have an id, so dont
                                $("#" + id).removeClass(className);
                                if (itemLaneNo === initLaneNo) {
                                    $("#" + id).addClass(className);
                                }
                            }
                            itemLaneNo++;
                        }
                    }
                }
            )
        },
        enableBlankLanes: function () {
            if (this.initialised) {
                let objs = $('.' + dragDropUtil.prefix + 'lane.ui-state-disabled:not([id*=blank])');
                objs.addClass("ui-state-enabled-temp");
                objs.removeClass("ui-state-disabled");
            }
        },
        disableBlankLanes: function () {
            if (this.initialised) {
                let objs = $('.' + dragDropUtil.prefix + 'lane.ui-state-enabled-temp');
                objs.removeClass("ui-state-enabled-temp");
                objs.addClass("ui-state-disabled");
            }
        },
        setCurrentRaceInfo: function (raceNo) {
            dragDropCard.currentRaceNo = raceNo;
            dragDropCard.currentLaneNo = dragDropCard.getCurrentLaneNo(raceNo);
        },
        disable: function () {
            $("." + dragDropUtil.prefix + "flex_row").sortable("disable");
        },
        displayCurrentRace: function (event, ui) {
            let currentRaceNo = this.currentRaceNo;
            let currentLaneNo = this.currentLaneNo;
            let initRaceNo = dragDropUtil.initRaceNo;
            let laneCount = dragDropUtil.laneCount;
            let egEntries = dragDropUtil.egEntries;

            if (currentRaceNo !== initRaceNo) {
                let currentRaceEntries = getEntriesForHeatNo(egEntries, currentRaceNo);
                if (currentRaceEntries.length < laneCount) {
                    let blankLanes = dragDropUtil.getBlankLanes(currentRaceNo);
                    if (blankLanes.length > 0) {
                        let hideLane = this.getBlankLaneToHide(blankLanes, currentLaneNo);
                        dragDropCard.hideBlankLane(currentRaceNo, hideLane);
                    }
                }
            }
        },
        displayLeavingRace: function (raceNo) {
            let laneCount = dragDropUtil.laneCount;
            let initRaceNo = dragDropUtil.initRaceNo;
            let initLaneNo = dragDropUtil.initLaneNo;

            // make sure leaving race is displayed correctly
            for (let l = 1; l <= laneCount; l++) {
                dragDropCard.displayCell(raceNo, l, true, false);
            }
            if (raceNo === initRaceNo) {
                // show the blank column for the current item being dragged away
                dragDropCard.displayCell(raceNo, initLaneNo, true, true);
            }
        },
        setCurrentItem: function (ui, force) {
            let curRaceNo = dragDropCard.currentRaceNo;
            let initRaceNo = dragDropUtil.initRaceNo;
            let egEntries = dragDropUtil.egEntries;
            let laneCount = dragDropUtil.laneCount;
            let currentEntries = getEntriesForHeatNo(egEntries, curRaceNo);
            let enabled = (curRaceNo === initRaceNo) || (currentEntries.length < laneCount);

            if (typeof force !== "undefined") {
                enabled = force;
            }
            if (!enabled) {
                let placeId = ui.placeholder.parent().attr("id");
                if (typeof placeId === "undefined") {
                    // moved to outside card, so reset
                    dragDropCard.currentRaceNo = 0;
                    dragDropCard.currentLaneNo = 0;
                }
            }
            dragDropUtil.setItemStatus(ui, enabled);
            dragDropUtil.setPlaceHolderStatus(enabled);
        },
        isItemVisible: function (raceNo, laneNo, blank) {
            let laneObjId = dragDropUtil.prefix + raceNo + "_" + laneNo;
            if (blank) {
                laneObjId += "_blank";
            }
            return dragDropCard.isVisible(laneObjId);
        },
        isVisible: function (id) {
            let obj = $("#" + id)[0];
            return obj.checkVisibility();
        },
        displayCell: function (raceNo, laneNo, display, blank) {
            let laneObjId = dragDropUtil.prefix + raceNo + "_" + laneNo;
            if (blank) {
                laneObjId += "_blank";
            }
            let obj = $("#" + laneObjId);
            if (display) {
                obj.show();
            } else {
                obj.hide();
            }
        },
        dragDropUpdateDB: function () {
            let initRaceNo = dragDropUtil.initRaceNo;
            let initLaneNo = dragDropUtil.initLaneNo;
            let currentRaceNo = this.currentRaceNo;
            let currentLaneNo = this.currentLaneNo;

            if (isNaN(currentRaceNo)) {
                return cardChange();
            }

            if (initRaceNo === currentRaceNo && initLaneNo === currentLaneNo) {
                // No change required
                return cardChange();
            }
            dragDropCard.disable(); // stop another dragDrop to refreshed
            e4sCard.writeRaceSeedings(currentRaceNo);
        },

        hideBlankLane: function (raceNo, laneNo) {
            let laneCount = dragDropUtil.laneCount;

            for (let l = 1; l <= laneCount; l++) {
                let display = l !== laneNo;
                dragDropCard.displayCell(raceNo, l, display, false);
            }
        },
        getBlankLaneToHide: function (blankLanes, dropPos) {
            if (blankLanes.includes(dropPos)) {
                return dropPos;
            }
            let blankLane = blankLanes[0];
            if (blankLanes.length > 1) {
                let leftOfDrop = 0;
                for (let b in blankLanes) {
                    let laneNo = blankLanes[b];
                    if (laneNo < dropPos) {
                        leftOfDrop = laneNo;
                    } else {
                        blankLane = laneNo;
                        if (leftOfDrop !== 0) {
                            blankLane = leftOfDrop;
                        }
                        break;
                    }
                }
            }
            return blankLane;
        }
    };
    let dragDrop = {
        item: "li_",
        headId: "overTableUL",
        process: true,
        initialised: false,
        currentRaceNo: 0, // Current Lane of dragging item
        init: function () {
            dragDropUtil.init();
            if (dragDropUtil.getStatus()) {
                dragDropCard.init();
                $("." + this.headId).sortable({
                    connectWith: "ul",
                    placeholder: "dragDropPlaceholder",
                    over: function (event, ui) {
                        dragDrop.over(event, ui);
                    },
                    start: function (event, ui) {
                        // ensure cog menu is hidden
                        $("#cardActionMenu").hide();
                        let itemId = ui.item[0].id;

                        dragDropUtil.start(itemId);
                        dragDrop.currentRaceNo = dragDropUtil.initRaceNo;
                    },
                    change: function (event, ui) {
                        let placeId = ui.placeholder.parent().attr("id");
                        dragDrop.currentRaceNo = dragDropUtil.getRaceNoFromId(placeId);
                    },
                    stop: function (event, ui) {
                        dragDrop.stop(event, ui);
                    }
                });
                this.enable();
            } else {
                if (dragDrop.initialised) {
                    setTimeout(dragDrop.disable, 500);
                }
            }
        },
        disable: function () {
            if (dragDrop.initialised) {
                $("." + this.headId).sortable("disable");
            }
            dragDrop.initialised = false;
        },
        enable: function () {
            $("." + this.headId).sortable("enable");
            dragDrop.initialised = true;
        },
        stop: function (event, ui) {
            if (dragDrop.process) {
                dragDrop.sendData(event, ui);
            } else {
                $("." + this.headId).sortable("cancel");
                dragDropUtil.setItemStatus(ui, true);
            }
            dragDrop.process = true;
        },
        over: function (event, ui) {
            let receiveEntryCount = event.target.childElementCount;
            let laneCount = dragDropUtil.laneCount;
            let status = true;
            if (receiveEntryCount > laneCount) {
                if (dragDrop.currentRaceNo !== dragDropUtil.initRaceNo) {
                    status = false;
                }
            }
            dragDropUtil.setItemStatus(ui, status);
            dragDrop.process = status;
        },
        sendData: function (event, ui) {
            let raceNo = this.currentRaceNo;

            this.reSeedData();
            this.disable(); // stop another dragDrop to refreshed
            // let seedings = this.getSeedingPayload(egEntries, raceNo);
            let seedings = e4sCard.getSeedingFromEntries(raceNo);
            e4sCard.writeRaceSeedings(raceNo, seedings);
            // cardChange();
        },
        reSeedData: function () {
            let currentRaceNo = dragDrop.currentRaceNo;
            let initRaceNo = dragDropUtil.initRaceNo;
            let initLaneNo = dragDropUtil.initLaneNo;
            let elements = $("#" + dragDropUtil.race + this.currentRaceNo).sortable("toArray");
            let useLaneNo;
            let blankLanes = dragDropUtil.getBlankLanes(currentRaceNo);
            let prevLaneNo = 0;
            let nextLaneNo = 0;
            let firstLaneNo = dragDropUtil.firstLane;
            // get prev and nextLane no
            let currentItemFound = false;
            let firstElementLaneNo = 0;
            for (let e in elements) {
                if (elements[e] === dragDropUtil.prefix + initRaceNo + "_" + initLaneNo) {
                    currentItemFound = true;
                } else {
                    if (!currentItemFound) {
                        prevLaneNo = parseInt(dragDropUtil.getLaneNoFromId(elements[e]));
                        firstElementLaneNo = firstElementLaneNo === 0 ? prevLaneNo : firstElementLaneNo;
                    } else {
                        nextLaneNo = parseInt(dragDropUtil.getLaneNoFromId(elements[e]));
                        break;
                    }
                }
            }
            let blankLaneAfter = false;
            if (prevLaneNo === 0) {
                if (nextLaneNo === 0) {
                    useLaneNo = firstLaneNo;
                } else {
                    useLaneNo = nextLaneNo - 1;
                    if (useLaneNo < 1) {
                        useLaneNo = 1;
                    }
                }
            } else {
                // is there a blank lane available
                useLaneNo = firstElementLaneNo;
                let closestBlankLane = 0;
                for (let b in blankLanes) {
                    let blankLaneNo = blankLanes[b];
                    if (blankLaneNo < prevLaneNo + 1) {
                        closestBlankLane = blankLaneNo;
                    } else {
                        blankLaneAfter = true;
                    }
                }
                if (closestBlankLane > 0 && !blankLaneAfter) {
                    let blankLaneAvailable = closestBlankLane < prevLaneNo + 1;
                    if (blankLaneAvailable) {
                        blankLaneAvailable = closestBlankLane < firstElementLaneNo;
                        if (blankLaneAvailable) {
                            useLaneNo = firstElementLaneNo - 1;
                        }
                    }
                }
            }

            for (let e in elements) {
                let elementEntryId = parseInt($("#" + elements[e]).attr("entryid"));
                let entry = getEntryFromEntries(elementEntryId, dragDropUtil.egEntries);
                let entryRaceNo = getEntryRaceNo(entry);
                let entryLaneNo = getEntryLaneNo(entry);
                if (entryLaneNo !== useLaneNo || entryRaceNo !== currentRaceNo) {
                    // useLaneNo = entryLaneNo;
                    setEntryHeatNo(entry, currentRaceNo);
                    setEntryLaneNo(entry, useLaneNo);
                }
                useLaneNo++;
            }
        }
    };
    let autoShow = {
        available: <?php if(hasSecureAccess( FALSE )){?>
            true,
        maxHeatCount: 30,
        pageInSeconds: 30,
        autoRemove: true,
        timer: null,
        list: [],
        isActive: function () {
            return autoShow.getStoredValue() !== "";
        },
        getList: function () {
            let payload = {};
            // autoShow.list = [];
            e4sSocket.send(payload, "<?php echo R4S_SOCKET_AUTOSHOW_ASK?>");
        },
        addToList: function (data) {
            if (typeof autoShow.list[data.screen] === "undefined") {
                autoShow.list[data.screen] = data;
            } else {
                for (let egId in data.events) {
                    if (typeof autoShow.list[data.screen].events[egId] === "undefined") {
                        autoShow.list[data.screen].events[egId] = data.screen[egId];
                    }
                }
            }
        },
        sendTo: function () {
            let cnt = 0;
            for (let l in autoShow.list) {
                cnt++;
            }
            if (cnt === 0) {
                return;
            }
            autoShow.sendToDialog();
        },
        setMaxTrackHeat: function (maxHeatNo) {
            if (autoShow.isActive()) {
                for (let heat = maxHeatNo + 1; heat <= autoShow.maxHeatCount; heat++) {
                    $(".track_heat_" + heat).hide();
                }
            }
        },
        checkPaging: function () {
            let events = autoShow.getEvents();
            let currentEgId = getEventFromStoredValue();
            let paramCnt = 0;
            let egArray = [];
            let currentPos = 0;
            for (let egId in events) {
                egArray.push(egId);
                paramCnt++;
                if (egId === currentEgId) {
                    currentPos = paramCnt;
                }
            }
            if (paramCnt < 2) {
                return;
            }
            let useEgId = egArray[0];
            if (currentPos < egArray.length) {
                useEgId = egArray[currentPos];
            }
            autoShow.showEvent(useEgId);
        },
        checkScroll: function () {
            let useHeight = 0;
            let header = $("#card_table_header_with_lanes");
            if (header.length === 1) {
                useHeight = header.height();
            }
            header = $("#cardHeaderDiv");
            if (header.length === 1) {
                useHeight += header.height();
            }
            let autoScrollObj = $("#autoShowScroll");
            if ((autoScrollObj.height() + useHeight + 5) > window.innerHeight) {
                $("body").css("overflow", "hidden");
                let html = autoScrollObj.html();
                autoScrollObj.append(html);
                autoScrollObj.addClass("autoScroll");
            }
            let delay = autoShow.pageInSeconds;
            let events = autoShow.getEvents();
            let currentEg = getCurrentEventGroup();
            let curEgId = getEventGroupId(currentEg);
            let pCount = 0;
            let nextEg = null;
            for (let egId in events) {
                egId = parseInt(egId);
                pCount++;
                if (egId === curEgId) {
                    let eventParams = events[egId];
                    if (eventParams.delay) {
                        delay = eventParams.delay;
                    }
                    nextEg = getCurrentEventGroup(currentEg.eventno + 1);
                }
            }

            if (pCount > 1) {
                if (nextEg !== null) {
                    let nextTime = new Date(nextEg.date).getTime();
                    let now = new Date().getTime();
                    if (nextTime < now && autoShow.autoRemove) {
                        delete (events[curEgId]);
                        autoShow.storeEvents(events);
                    }
                }
                window.setTimeout(autoShow.checkPaging, (delay * 1000));
            }
        },
        showEvent: function (egId) {
            let events = autoShow.getEvents();
            let egParam = events[egId];
            if (egParam) {
                storeEvent(egId, egParam.checkedIn);
                refreshCurrentCard();
            }
        },
        displayRequested: function (data) {
            autoShow.convertListFromJson(data);
            let storedScreen = autoShow.getStoredValue();
            if (storedScreen !== "") {
                for (let screen in autoShow.list) {
                    if (storedScreen === screen) {
                        autoShow.storeEvents(autoShow.list[screen].events);
                        for (let egId in autoShow.list[screen].events) {
                            autoShow.showEvent(egId);
                            break;
                        }
                    }
                }
            }
        },
        onCardChangePreDisplay: function (eg) {
            if (!autoShow.isActive()) {
                return;
            }
            let events = autoShow.getEvents();
            let egId = getEventGroupId(eg);
            if (events[egId]) {
                let displayType = events[egId].displayType;
                if (displayType === "<?php echo E4S_CARDMODE_CARD ?>") {
                    setViewMode("<?php echo E4S_CARDMODE_CARD ?>");
                } else {
                    setViewMode("<?php echo E4S_CARDMODE_NONECARD ?>");
                }
            }
        },
        onCardChange: function () {
            if (autoShow.isActive()) {
                $("#e4s-menu-bar").hide();
                $("body").css("margin", 0).css("padding", 0);
                $("#tabs").css("padding", 0);
                $("#card_table_header_with_lanes").on("click",
                    function () {
                        autoShow.reset()
                    }
                );
                $("#cardHeaderDiv").on("click",
                    function () {
                        autoShow.reset()
                    }
                );
                autoShow.headerDisplay();
                if (JSON.stringify(autoShow.getEvents()) === "{}") {
                    autoShow.defaultScreen();
                } else {
                    window.setTimeout(autoShow.checkScroll, 500);
                }
            }
        },
        clearHeader: function () {
            $("#header_start_time").text("");
            $("#header_end_time").text("");
            $("#header_time").text("");
        },
        headerDisplay: function () {
            $("#card_trials").text("Available here : https://<?php echo E4S_CURRENT_DOMAIN ?>/" + getCompetition().id + "/card");
            autoShow.clearHeader();
            let screenEvents = autoShow.getEvents();
            let currEgId = getCurrentEventGroup().id;
            let useEgId = 0;
            let firstEgId = 0;
            let currentMatched = false;

            for (let egId in screenEvents) {
                egId = parseInt(egId);
                if (firstEgId === 0) {
                    firstEgId = egId;
                }
                if (egId === currEgId) {
                    currentMatched = true;
                } else if (currentMatched && useEgId === 0) {
                    useEgId = egId;
                }
            }
            if (useEgId === 0) {
                useEgId = firstEgId;
            }
            if (useEgId > 0 && useEgId !== currEgId && screenEvents[currEgId]) {
                let nextEg = getEventGroupById(useEgId);

                autoShow.nextEvent = nextEg.event;
                autoShow.displayIn = screenEvents[currEgId].delay;
                autoShow.updateTime();
            }
        },
        updateTime: function () {
            let eventText = "Next Event : " + autoShow.nextEvent + " in ";
            let time = autoShow.displayIn;
            let units = "seconds";
            eventText += time + " " + units;
            autoShow.displayIn--;
            if (autoShow.displayIn >= 0) {
                $("#header_time").text(eventText);
                autoShow.timer = window.setTimeout(autoShow.updateTime, 1000);
            } else {
                autoShow.clearHeader();
            }
        },
        defaultScreen: function () {
            $("#autoShowScroll").html("Awaiting Seedings")
                .css("height", window.innerHeight)
                .css("font-size", "xxx-large")
                .css("display", "flex")
                .css("align-content", "space-around")
                .css("align-items", "center")
                .css("flex-wrap", "nowrap")
                .css("flex-direction", "column")
                .css("justify-content", "center");
            $("body").css("overflow", "hidden");
        },
        sendToDialog: function () {
            let divName = "autoShowDiv";
            let divHTML = "<div id='" + divName + "'>";
            divHTML += "Send this seeding to ?<br>";
            let checked = "";
            let listCnt = 0;
            for (let l in autoShow.list) {
                listCnt++;
            }
            if (listCnt === 1) {
                checked = "checked ";
            }
            let curEg = getCurrentEventGroup();
            let curEgId = getEventGroupId(curEg);
            let currCheckedIn = cardShowsCheckedIn();
            let displayDelay = false;
            for (let screen in autoShow.list) {
                let list = autoShow.list[screen];
                let id = autoShow.fieldScreenPrefix + screen.replace(" ", "");
                divHTML += '<br><input ' + checked + 'type="checkbox" id="' + id + '" name="' + id + '" value="' + screen + '">';
                divHTML += '<label for="' + id + '"> ' + screen + '</label><br>';
                divHTML += '<div style="margin-left: 25px;">';

                let nameEgId = autoShow.fieldEGPrefix + screen;
                let egCount = 0;
                let addCurrent = true;
                for (let egId in list.events) {
                    egCount++;
                    let fieldEgId = nameEgId + "_" + egId;
                    egId = parseInt(egId);
                    let eg = getEventGroupById(egId);
                    let checked = "";
                    if (eg !== null) {
                        if (egId === curEgId) {
                            addCurrent = false;
                            checked = "checked ";
                        }
                        let extraText = "";
                        if (checked === "") {
                            let displayType = '<?php echo E4S_CARDMODE_CARD ?>';
                            if (list.events[egId].displayType === '<?php echo E4S_CARDMODE_NONECARD ?>') {
                                displayType = "List";
                            }
                            let egDelay = list.events[egId].delay;
                            if (egDelay === null) {
                                list.events[egId].delay = autoShow.pageInSeconds;
                                egDelay = autoShow.pageInSeconds;
                            }
                            extraText = " (" + displayType + ":" + egDelay + ")";
                        }
                        divHTML += '<input ' + checked + 'e4s_Parent="' + screen + '" type="checkbox" id="' + fieldEgId + '" name="' + nameEgId + '" value="' + egId + '">';
                        divHTML += '<label for="' + fieldEgId + '"> ' + eg.event + extraText + '</label><br>';
                    }
                }
                if (addCurrent) {
                    egCount++;
                    let egId = curEgId;
                    let currObj = {};
                    currObj.screen = screen;
                    currObj.checkedIn = currCheckedIn;
                    currObj.egId = egId;
                    currObj.delay = autoShow.pageInSeconds;
                    autoShow.list[screen].events[egId] = currObj;
                    let fieldEgId = nameEgId + "_" + egId;
                    divHTML += '<input checked e4s_Parent="' + screen + '" type="checkbox" id="' + fieldEgId + '" name="' + nameEgId + '" value="' + egId + '">';
                    divHTML += '<label for="' + fieldEgId + '"> ' + curEg.event + '</label><br>';
                }
                divHTML += '</div>';
                if (egCount > 1) {
                    displayDelay = true;
                }
            }
            if (areCardsAvailable()) {
                divHTML += '<br><label for="displayType">' + curEg.event + ' display : </label>';
                checked = "";
                if (getViewMode() === "<?php echo E4S_CARDMODE_NONECARD ?>") {
                    checked = "checked ";
                }
                divHTML += '<input type="radio" id="displayType<?php echo E4S_CARDMODE_NONECARD ?>" name="displayType" ' + checked + 'value="<?php echo E4S_CARDMODE_NONECARD ?>">List ';
                checked = "";
                if (getViewMode() === "<?php echo E4S_CARDMODE_CARD ?>") {
                    checked = "checked ";
                }
                divHTML += '<input type="radio" id="displayType<?php echo E4S_CARDMODE_CARD ?>" name="displayType" ' + checked + 'value="<?php echo E4S_CARDMODE_CARD ?>"><?php echo E4S_CARDMODE_CARD ?>';
            }
            if (displayDelay) {
                divHTML += '<br>';
                divHTML += '<br>Page Displayed for : <input id="' + autoShow.fieldScreenPrefix + 'delay" size=4 value="' + autoShow.pageInSeconds + '"> seconds';
                divHTML += '<br>Auto remove events when time reached : <input type="checkbox" id="' + autoShow.fieldScreenPrefix + 'autodelete" value="1" checked> Enabled';
            }
            divHTML += "</div>";
            $("#" + divName).remove();
            $("#dialogAutoShow").html(divHTML);

            let dialog = $("#" + divName).dialog({
                resizable: false,
                title: "Show Seedings",
                height: "auto",
                width: 600,
                modal: true
            });
            let buttons = [
                {
                    text: "Cancel",
                    class: "resultButton",
                    click: function () {
                        $(this).dialog("close");
                    }
                },
                {
                    text: "OK",
                    class: "resultButton",
                    click: function () {
                        autoShow.sendToDisplay();
                        $(this).dialog("close");
                    }
                }
            ];
            dialog.dialog("option", "buttons", buttons);
            positionDialog(dialog);
        },
        fieldScreenPrefix: "as_",
        fieldEGPrefix: "as_eg_",
        sendToDisplay: function () {
            let payload = {};
            let curEg = getCurrentEventGroup();
            let curEgId = getEventGroupId(curEg);
            let displays = false;
            for (let l in autoShow.list) {
                let screen = autoShow.list[l].screen;
                let id = autoShow.fieldScreenPrefix + screen.replace(" ", "");
                if ($("#" + id).is(":checked")) {
                    let fieldEvent = autoShow.fieldEGPrefix + screen.replace(" ", "");
                    $("input:checkbox[name=" + fieldEvent + "]").each(function () {
                        let obj = $(this);
                        if (obj.is(":checked")) {
                            displays = true;
                            // event is checked, ensure in autoShowList
                            let screen = obj.attr("name").replace(autoShow.fieldEGPrefix, "");
                            let egId = obj.attr("id").replace("_" + screen, "").replace(autoShow.fieldEGPrefix, "");
                            egId = parseInt(egId);
                            let data = {
                                screen: screen,
                                events: []
                            };
                            data.events[egId] = {
                                checkedIn: false,
                                delay: 0,
                                displayType: 'Card',
                                screen: screen
                            };
                            autoShow.addToList(data);
                        } else {
                            // event is not checked
                            let listScreenName = obj.attr("e4s_Parent");
                            let listScreen = autoShow.list[listScreenName];
                            let egId = parseInt(obj.val());
                            if (typeof listScreen.events[egId]) {
                                delete listScreen.events[egId];
                            }
                        }
                    });
                }
                if (!displays) {
                    delete (autoShow.list[l]);
                }
            }
            if (displays) {
                let displayType = "<?php echo E4S_CARDMODE_NONECARD ?>";
                let cardObj = $("#displayType<?php echo E4S_CARDMODE_CARD ?>");
                if (cardObj.length > 0) {
                    if (cardObj.prop("checked")) {
                        displayType = "<?php echo E4S_CARDMODE_CARD ?>";
                    }
                }
                for (let l in autoShow.list) {
                    let events = autoShow.list[l].events;
                    if (events[curEgId]) {
                        events[curEgId].displayType = displayType;
                        let delay = $("#" + autoShow.fieldScreenPrefix + "delay").val();
                        if (typeof delay === "undefined" || delay === "") {
                            delay = autoShow.pageInSeconds;
                        } else {
                            delay = parseInt(delay);
                        }
                        events[curEgId].delay = delay;
                    }
                }

                payload = autoShow.convertListToJson();
                e4sSocket.send(payload, "<?php echo R4S_SOCKET_AUTOSHOW_DISPLAY?>");
            }
        },
        convertListToJson: function () {
            let retArr = [];
            for (let screen in autoShow.list) {
                let lObj = {};
                lObj.screen = screen;
                let paramArr = [];

                for (let egId in autoShow.list[screen].events) {
                    egId = parseInt(egId);
                    let eventParams = autoShow.list[screen].events[egId];
                    let paramObj = {};
                    paramObj.egId = egId;
                    paramObj.screen = screen;
                    paramObj.displayType = eventParams.displayType;
                    paramObj.checkedIn = eventParams.checkedIn;
                    paramObj.delay = eventParams.delay;
                    paramArr.push(paramObj);
                }
                lObj.events = paramArr;
                retArr.push(lObj);
            }
            let payload = {};
            payload.autoRemove = $("#as_autodelete").is(":checked");
            payload.screens = retArr;
            return payload;
        },
        convertListFromJson: function (data) {
            autoShow.autoRemove = data.autoRemove;
            let screens = data.screens;
            let list = {};
            for (let element in screens) {
                let screen = screens[element].screen;
                list[screen] = {};
                list[screen].screen = screen;
                let paramObj = {};
                for (let eventElement in screens[element].events) {
                    let eventParams = screens[element].events[eventElement];
                    let pObj = {};
                    pObj.displayType = eventParams.displayType;
                    pObj.checkedIn = eventParams.checkedIn;
                    pObj.delay = eventParams.delay;
                    pObj.screen = screen;
                    pObj.egId = eventParams.egId;
                    paramObj[eventParams.egId] = pObj;
                }
                list[screen].events = paramObj;
            }
            autoShow.list = list;
        },
        askRequested: function () {
            let screen = autoShow.getStoredValue();
            let events = autoShow.getEvents();
            if (screen !== "") {
                let payload = {};
                payload.screen = screen;
                payload.events = events;
                e4sSocket.send(payload, "<?php echo R4S_SOCKET_AUTOSHOW_RESPOND?>");
            }
        },
        displayDialog: function () {
            let divName = "autoShowDiv";
            let divHTML = "<div id='" + divName + "'>";
            divHTML += "Set this screen to be a seeding display ?<br>";
            divHTML += 'Please enter a unique name, so it can be referenced by Seedings.<br>';
            divHTML += '<input id="autoShowField" value="' + autoShow.getStoredValue() + '">';
            divHTML += "</div>";
            $("#" + divName).remove();
            $("#dialogAutoShow").html(divHTML);

            let dialog = $("#" + divName).dialog({
                resizable: false,
                title: "Seeding Displays",
                height: "auto",
                width: 600,
                modal: true
            });
            let buttons = [
                {
                    text: "Clear",
                    class: "resultButton",
                    click: function () {
                        autoShow.storeValue("");
                        $(this).dialog("close");
                    }
                },
                {
                    text: "Set",
                    class: "resultButton",
                    click: function () {
                        let enteredValue = $("#autoShowField").val();
                        if (enteredValue !== "") {
                            autoShow.storeValue(enteredValue);
                            e4sAlert("The screen will now refresh.<br><br>To Restore, Click the header section", "Information", autoShow.reload);
                            $(this).dialog("close");
                        }
                    }
                }
            ];
            dialog.dialog("option", "buttons", buttons);
            positionDialog(dialog);
        },
        reload: function () {
            autoShow.storeEvents("");
            refreshCurrentCard();
        },
        reset: function () {
            autoShow.storeValue("");
            autoShow.reload();
        },
        getEvents: function () {
            let val = autoShow.getStoredGeneric("autoShowParams");
            if (val === "") {
                val = "{}";
            }
            return JSON.parse(val);
        },
        storeEvents: function (val) {
            if (val !== "") {
                val = JSON.stringify(val);
            }
            autoShow.storeGeneric("autoShowParams", val);
        },
        getStoredValue: function () {
            return autoShow.getStoredGeneric("autoShow");
        },
        storeValue: function (val) {
            autoShow.storeGeneric("autoShow", val.replace(/ /g, "_"));
        },
        getStoredGeneric: function (name) {
            let val = localStorage.getItem(name);
            if (val === 'null' || val === null) {
                val = '';
            }
            return val;
        },
        storeGeneric: function (name, val) {
            localStorage.setItem(name, val);
        }
		<?php
		}else{
		?>
        false
		<?php
		}
		?>
    };
    let offLine = {
        statusSep: ":",
        activeId: "1",
        activeSuffix: ":1",
        notActiveId: "0",
        notActiveSuffix: ":0",
        storeResult: function (payload) {
// write payload to storage
            let arr = {};
            for (let r in payload.results) {
                let result = payload.results[r];
                let eventNo = result
            .<?php echo E4S_RESULT_PARAMS ?>.
                eventNo;
                let athleteId = result
            .<?php echo E4S_RESULT_PARAMS ?>.
                athleteId;
                let cookieName = '<?php echo E4S_OFFLINE_RESULTS ?>_' + eventNo;
                let cookieResults = readCookie(cookieName, true);

                if (typeof cookieResults[athleteId] === "undefined") {
                    let athlete = getAthleteById(athleteId);
                    let athleteMini = {
                        firstName: athlete.firstname,
                        surName: athlete.surname,
                        club: athlete.clubname,
                    }
                    cookieResults[athleteId] = {
                        athlete: athleteMini
                    };
                }
                let athleteResults = cookieResults[athleteId];
                for (let t in result) {
                    if (t === "<?php echo E4S_RESULT_PARAMS ?>") {
                        // only need to store the row as event and athleteId are in properties already
                        athleteResults[t] = {"row": result.<?php echo E4S_RESULT_PARAMS ?>.row};
                        // } else if ( t === 'result' ) {
                        //     let key = result[t].height + ":" + result[t].trial;
                        //     if ( typeof athleteResults['results'] === "undefined" ) {
                        //         athleteResults['results'] = {};
                        //     }
                        //     athleteResults['results'][key] = result[t];
                    } else if (t === 'result') {
                        let heightResult = result[t];
                        athleteResults[heightResult.height + ":" + heightResult.trial] = heightResult.result;
                    } else {
                        athleteResults[t] = result[t];
                    }
                }
                let cookieValue = JSON.stringify(cookieResults);
                if (cookieValue === "{}") {
                    eraseCookie(cookieName);
                } else {
                    createCookie(cookieName, cookieValue);
                }
                arr[eventNo] = cookieResults;
            }
            payload.results = arr;
            return payload;
        },
        clearResult: function (payload) {
// clear data from storage

            for (let eventNo in payload) {
                let cookieName = '<?php echo E4S_OFFLINE_RESULTS ?>_' + eventNo;
                let cookieEventResults = readCookie(cookieName, true);
                let payloadResults = payload[eventNo];
                for (let athleteId in payloadResults) {
                    if (typeof cookieEventResults[athleteId] === "object") {
                        let athleteResults = cookieEventResults[athleteId];
                        for (let t in athleteResults) {
                            if (t !== "<?php echo E4S_RESULT_PARAMS ?>") {
                                delete athleteResults[t];
                            }
                        }
                        let removeAthlete = true;
                        for (let t in athleteResults) {
                            if (t !== "<?php echo E4S_RESULT_PARAMS ?>") {
                                removeAthlete = false;
                                break;
                            }
                        }
                        if (removeAthlete) {
                            delete cookieEventResults[athleteId];
                        }
                    }
                }

                let cookieValue = JSON.stringify(cookieEventResults);
                if (cookieValue === "{}") {
                    eraseCookie(cookieName);
                } else {
                    createCookie(cookieName, cookieValue);
                }
            }
        },
        readTbc: function (eventNo) {
            let cookieName = '<?php echo E4S_OFFLINE_RESULTS ?>_' + eventNo;
            return readCookie(cookieName, true);
        },
        getOutstanding: function (eventNo, athleteId) {
            let eventResults = offLine.readTbc(eventNo);

            if (isEmpty(eventResults)) {
                return null;
            }

            if (!eventResults[athleteId]) {
                return null;
            }
            return eventResults[athleteId];
        },
        flushTbc: function () {
            let eventGroups = getEventGroups();
            let results = {};
            let flush = false;
            for (let eg in eventGroups) {
                let eventGroup = eventGroups[eg];
                let eventNo = getEventNoFromEventGroup(eventGroup);
                let egTbcResults = offLine.readTbc(eventNo);
                if (!isEmpty(egTbcResults)) {
                    results[eventNo] = egTbcResults;
                    flush = true;
                }
            }
            if (flush) {
                writeAthleteResultsToDB(results, false, false, true);
            }
        },
        hasTbc: function () {
            let eventGroups = getEventGroups();
            for (let eg in eventGroups) {
                let eventGroup = eventGroups[eg];
                let eventNo = getEventNoFromEventGroup(eventGroup);
                let egTbcResults = offLine.readTbc(eventNo);
                if (!isEmpty(egTbcResults)) {
                    return true;
                }
            }
            return false;
        },
        resultTbc: function (tbc, obj) {
            let tbcClass = "resultTBC";
            if (tbc) {
                obj.addClass(tbcClass);
            } else {
                obj.removeClass(tbcClass);
            }
        }
    };

    function e4s_refreshRequired() {
        e4sSocket.sendRefreshRequiredMsg();
    }

    function mergeEvents() {
        let fromEgId = e4sGlobal.fromEGId;
        let toEgId = e4sGlobal.toEGId;
        let url = "<?php echo E4S_BASE_URL ?>eventgroups/merge/" + fromEgId + "/" + toEgId;
        $.post(url, {});
    }

    function validateMergeEvent(eventNo) {
        let curEg = getCurrentEventGroup();
        eventNo = parseInt(eventNo);
        if (eventNo < 1 || curEg.eventno === eventNo) {
            return e4sAlert("Invalid event number", "Error");
        }
        let egArr = getEventGroups();
        for (let sub in egArr) {
            let eg = egArr[sub];
            if (eg.eventno === eventNo) {
                e4sGlobal.fromEGId = getEventGroupId(curEg);
                e4sGlobal.toEGId = getEventGroupId(eg);
                return e4sConfirm("Merge Event <br>" + curEg.typeno + "-" + curEg.event + "<br>into<br>" + eg.typeno + "-" + eg.event, "Confirm", mergeEvents);
            }
        }
        e4sAlert("Invalid event number", "Error");
    }

    function mergeEvent() {
        let eventGroups = getEventGroups();
        let curEvent = getCurrentEventGroup();
        let choices = [];
        for (let eg in eventGroups) {
            let event = eventGroups[eg];
            if (event.eventno !== curEvent.eventno) {
                if (event.eventId === curEvent.eventId) {
                    choices.push(event.eventno + ":" + event.typeno + "-" + event.event);
                }
            }
        }
        if (choices.length > 0) {
            e4sSelect("Merge with Event", "Merge Events", choices, validateMergeEvent);
        } else {
            e4sAlert("No matching events found", "Error");
        }
    }

    function processIncludedEntries() {
        let url = "<?php echo E4S_BASE_URL ?>entries/include/" + getCompetition().id;
        showPleaseWait(true, "Loading Included Entries");
        $.ajax({
            type: "POST",
            url: url,
            data: {},
            dataType: "json",
            success: function () {
                location.reload();
            }
        });
    }

    function includeEntries() {
        e4sConfirm("This competition has events that include entries from other events.<br>Do you want to include these now ?", "Include Entries", processIncludedEntries);
    }

    function clearAndReloadSeedings() {
        let url = '<?php echo E4S_BASE_URL ?>seeding/clearall/' + getCompetition().id;
        showPleaseWait(true, "Re-Seeding Competition");
        $.ajax({
            url: url,
            data: null,
            success: function () {
                location.reload();
            }
        });
    }

    function clearCompetitionSeedings() {
        e4sConfirm("Clear ALL competition seedings and re-generate.", "Are You Sure ?", clearAndReloadSeedings);
    }

    function addAthlete() {
        let athleteId = parseInt($("#athleteIdToAdd").val());
        let ageGroupId = parseInt($("#ageGroupIdToAdd").val());
        let ageGroup = $("#ageGroupToAdd").val();
        let bibNo = parseInt($("#bibNoToAdd").val());
        let eventNo = getSelectedEventNo();
        let eventGroupId = getEventGroup(eventNo).id;
        let payload = {
            "athleteId": athleteId,
            "bibNo": bibNo,
            "eventGroupId": eventGroupId,
            "ageGroupId": ageGroupId,
            "ageGroup": ageGroup,
            "force": false
        };
        // Add to database
        let url = "<?php echo E4S_BASE_URL ?>event/addAthlete";
        if ($("#addAthleteForce").is(':checked')) {
            payload.force = true;
        }
        $('#addAthleteForce').prop('checked', false);
        showPleaseWait(true, "Adding Athlete. Please wait");
        $.ajax({
            type: "POST",
            url: url,
            data: payload,
            dataType: "json",
            success: addAthleteResponse
        });
    }

    function resetCurrentEGEntries() {
        // reset just this event group
        let curEg = getCurrentEventGroup();
        let curEgId = getEventGroupId(curEg);
        let srcEg = getEntriesFromEG(curEg);
        let srcEgId = getEventGroupId(srcEg);
        addEntriesFromEgId(srcEgId, curEgId);
    }

    function addEntriesFromEgId(srcEgId, onlyForEgId) {
        let url = "<?php echo E4S_BASE_URL ?>entries/create/" + srcEgId;
        if (typeof onlyForEgId !== "undefined") {
            url += "/" + onlyForEgId;
        }
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json"
        });
    }

    function addEntriesFromThisEG() {
        let currentEg = getCurrentEventGroup();
        addEntriesFromEgId(getEventGroupId(currentEg));
    }

    function addAthleteDialogByBib() {
        let html = '';
        let athletes = getAthletes();
        if (athletes.length === 0) {
            return e4sAlert("There are no athletes currently in this competition", "Error");
        }

        html += "Enter Bib Number : <input style='width:90px' id='addBibNo'>";
        html += "<div>";
        html += "<span id='addAthleteInfo'></span>";
        html += "</div>";
        let parentAddDiv = $("#addAthleteParentDiv");
        let addDiv = $("#addAthleteDiv");
        addDiv.html(html);
        let eg = getCurrentEventGroup();

        parentAddDiv.dialog({
            title: "Add Athlete to " + eg.typeno + ":" + eg.event,
            resizable: false,
            height: 400,
            width: 450,
            modal: true
        });
        $("#addBibNo").on("keyup", function (e) {
            let addAthlete = $("#add-athlete");
            addAthlete.button("disable");
            let athletes = getAthletes();
            let bibNo = parseInt($("#addBibNo").val());
            displayAthleteToAdd();
            if (bibNo > 0) {
                for (let x in athletes) {
                    if (athletes[x].bibno === bibNo) {
                        if (displayAthleteToAdd(athletes[x])) {
                            addAthlete.button("enable");
                        } else {
                            addAthlete.button("disable");
                        }
                    }
                }
            }
        });
        parentAddDiv.show();
        parentAddDiv.dialog("option", "buttons", [{
            "id": "add-athlete",
            "text": "Add Athlete",
            "click": function () {
                addAthlete();
            }
        },
            {
                "id": "add-athlete-close-btn",
                "text": "Close",
                "click": function () {
                    $(this).dialog("close");
                }
            }]);
        $("#add-athlete").button("disable");
    }

    function seedingComplete() {
        let callRoom = getCallRoomStatus();
        confirmEventGroupSeedings(callRoom);
    }

    function updateUiSorting(entries, sortKeys) {
        let originalEntries = JSON.parse(JSON.stringify(entries));
        // delete the 0th which is generated by the above method
        delete originalEntries[0];
        let position = 1;
        for (let sk in sortKeys) {
            let key = sortKeys[sk];
            let athleteId = parseInt(key.split("-")[1]);
            // get entry for athlete
            for (let e in entries) {
                if (entries[e].athleteId === athleteId) {
                    let heatInfo = entries[e].heatInfo;
                    heatInfo.position = position;
                    heatInfo.positionCheckedIn = position;
                    // get lane/heat from original
                    for (let o in originalEntries) {
                        let originalHeatInfo = originalEntries[o].heatInfo;
                        if (originalHeatInfo.position === position) {
                            heatInfo.laneNo = originalEntries[o].heatInfo.laneNo;
                            heatInfo.laneNoCheckedIn = originalEntries[o].heatInfo.laneNoCheckedIn;
                            heatInfo.heatNo = originalEntries[o].heatInfo.heatNo;
                            heatInfo.heatNoCheckedIn = originalEntries[o].heatInfo.heatNoCheckedIn;
                        }
                    }
                    break;
                }
            }
            position++;
        }
        cardChange();
    }

    function togglePulse(id, state) {
        let obj;
        if (id === "") {
            obj = $(".pulse");
        } else {
            obj = $("#" + id);
        }

        if (typeof state === "undefined") {
            obj.toggleClass("pulse");
        } else {
            if (state) {
                obj.addClass("pulse");
            } else {
                obj.removeClass("pulse");
            }
        }
    }

    function populateNewHeatInfo(newHeatNo, newLaneNo) {
        getSwitchedInAthlete(0);
        $("#newHeatNo").val(newHeatNo);
        $("#newLaneNo").val(newLaneNo);
    }

    function getEntryForHeatLane(heatNo, laneNo) {
        let bibObj = $("#bib_" + heatNo + "_" + laneNo);
        let bibNo = bibObj.text();
        let entryId = parseInt(bibObj.attr("eId"));
        let isTeam = bibObj.attr("team") === "1";
        let entry = getEntryByID(entryId, isTeam);
        if (entry !== null) {
            entry.bibNo = getBibNo(bibNo, entry);
        }
        return entry;
    }

    function isSwitchDialogDisplayed() {
        return $("#trackChange").length === 1;
    }

    function switchTrackPresent(heatNo, laneNo) {

        let entry = getEntryForHeatLane(heatNo, laneNo);
        let bibNo = "<?php echo E4S_NO_BIBNO?>";
        if (entry === null) {
            if (!isSwitchDialogDisplayed()) {
                return;
            }
        } else {
            if (typeof entry.bibNo !== "undefined") {
                bibNo = entry.bibNo;
            }
        }
        dragDrop.disable();
        if (isSwitchDialogDisplayed()) {
            showPulse(heatNo, laneNo, true);
            let fromHeatNo = parseInt($("#fromHeatNo").text());
            let fromLaneNo = parseInt($("#fromLaneNo").text());
            showPulse(fromHeatNo, fromLaneNo, false);
        } else {
            if (bibNo !== "<?php echo E4S_NO_BIBNO?>") {
                showPulse(heatNo, laneNo, true);
            }
        }

        if (bibNo !== "<?php echo E4S_NO_BIBNO?>") {
            if (isSwitchDialogDisplayed()) {
                getSwitchedInAthleteByEntryId(entry, bibNo);
            } else {
                if (entry !== null) {
                    processTrackChange(entry);
                }
            }
        } else {
            // check if move dialog open and populate iith heat/lane
            if (isSwitchDialogDisplayed()) {
                populateNewHeatInfo(heatNo, laneNo);
            }
        }
    }

    function showPulse(heatNo, laneNo, clear) {
        let id = "bib_" + heatNo + "_" + laneNo + "_cell";
        if (clear) {
            togglePulse("", false);
        }
        togglePulse(id, true);
    }

    function switchAthleteBibNo() {
        let bibNo = $("#switchedInBibNo").val();
        getSwitchedInAthlete(bibNo);
    }

    function getSwitchedInAthlete(bibNo) {
        $("#switchedInAthlete").text("");
        setSwitchedEntityId(0);
        $("#switchedInBibNo").val("");
        if (bibNo + "" !== "0") {
            $("#newHeatNo").val("");
            $("#newLaneNo").val("");
            // let bibNo = $("#switchedInBibNo").val().trim();
            $("#switchedInBibNo").val(bibNo);
            let eg = getCurrentEventGroup();
            let entries = getEntriesForEg(eg);
            for (let e in entries) {
                let entry = entries[e];
                let athlete = getAthleteById(entry.athleteId);
                let checkBib = getBibNoToUse(athlete, entry);

                if (checkBib + "" === bibNo + "") {
                    $("#switchedInAthlete").text(getAthleteNameFromAthlete(athlete));
                    let heatNo = getEntryRaceNo(entry);
                    let laneNo = getEntryLaneNo(entry);
                    $("#newHeatNo").val(heatNo);
                    $("#newLaneNo").val(laneNo);
                    setSwitchedEntityId(entry.athleteId);
                    break;
                }
            }
        }
    }

    function getSwitchedInAthleteByEntryId(entry, bibNo) {
        $("#switchedInAthlete").text("");
        setSwitchedEntityId(0);
        $("#switchedInBibNo").val("");
        if (entry) {
            $("#newHeatNo").val("");
            $("#newLaneNo").val("");
            // let bibNo = $("#switchedInBibNo").val().trim();
            $("#switchedInBibNo").val(bibNo);
            let entityName = "";
            let entityId = 0;
            if (entry.teamid) {
                entityName = entry.teamname;
                entityId = entry.teamid;
            } else {
                entityId = entry.athleteId;
                let athlete = getAthleteById(entityId);
                entityName = getAthleteNameFromAthlete(athlete);
            }

            $("#switchedInAthlete").text(entityName);
            let heatNo = getEntryRaceNo(entry);
            let laneNo = getEntryLaneNo(entry);
            $("#newHeatNo").val(heatNo);
            $("#newLaneNo").val(laneNo);
            setSwitchedEntityId(entityId);
        }
    }

    function getEntriesForEgHeatNo(eventGroup, heatNo) {
        return getEntriesForEgIdHeatNo(getEventGroupId(eventGroup), heatNo);
    }

    function getEntriesForEgIdHeatNo(egId, heatNo) {
        let entries = getEntriesForEgId(egId);
        return getEntriesForHeatNo(entries, heatNo);
    }

    function getEntriesForHeatNo(entries, heatNo) {
        let retEntries = [];
        for (let x = 0; x < entries.length; x++) {
            let entry = entries[x];
            if (getEntryRaceNo(entry) === heatNo) {
                retEntries.push(entry);
            }
        }
        return retEntries;
    }

    function scoreboardQueueConfig(toScoreboard = null) {
        if (toScoreboard === null) {
            toScoreboard = $("#includeDisplay").prop("checked");
        } else {
            $("#includeDisplay").prop("checked", toScoreboard);
        }
        if (toScoreboard) {
            $("#scoreboardConfig").show();
        } else {
            $("#scoreboardConfig").hide();
        }
        setUseOfScoreboards(toScoreboard);
    }

    function starterOptionsDialog(eventGroup, heatNo) {
        if (heatNo < getMaxHeatNo()) {
            let divName = "e4sStarterOptions";
            let divHTML = "<div id='" + divName + "'>";
            let entity = "Athletes";
            if (isEventATeamEvent(eventGroup)) {
                entity = "Teams";
            }
            divHTML += entity + " for Race " + heatNo + "<br><br>";
            divHTML += '<span class="confirm_title_style">Clear Confirmation:</span><span class="confirm_desc_style">Un-Confirms the race starting list.</span><br>';
            divHTML += '<span class="confirm_title_style">Confirm:</span><span class="confirm_desc_style">This will confirm the ' + entity.toLowerCase() + ' for race ' + heatNo + '.</span><br>';
            divHTML += "<div>";
            divHTML += "<br><input type='checkbox' id='includeDisplay' onchange='scoreboardQueueConfig();'> Send to Scoreboard ?</input><br>";
            divHTML += "<span id='scoreboardConfig' style='display:none;'>Queue Position: <input type='radio' id='scorePositionNext' name='scorePosition' checked value='next'>Next</input>";
            divHTML += " <input type='radio' id='scorePositionForce' name='scorePosition' value='force'>force</input>";
            divHTML += " <span style='float:right;'> Display Time : <input type='number' min='5' max='60' style='width:50px;' id='scoreDisplayTime' value='15'> seconds</span>";
            divHTML += "</span>";
            divHTML += "</div>";
            divHTML += "</div>";
            $("#" + divName).remove();
            $("#dialogStarterOptions").html(divHTML);
            let useScoreboard = getUseOfScoreboards();

            scoreboardQueueConfig(useScoreboard);

            let dialog = $("#" + divName).dialog({
                resizable: false,
                title: "Event Participants",
                height: "auto",
                width: 600,
                modal: true
            });
            let buttons = [
                {
                    text: "Clear Confirmation",
                    class: "resultButton",
                    click: function () {
                        writeStarterOptions(eventGroup, heatNo, false);
                        $(this).dialog("close");
                    }
                }, {
                    text: "Confirm",
                    class: "resultButton",
                    click: function () {
                        writeStarterOptions(eventGroup, heatNo, true);
                        $(this).dialog("close");
                    }
                },
                {
                    text: "Close",
                    class: "resultButton",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ];
            dialog.dialog("option", "buttons", buttons);
            positionDialog(dialog);
        }
    }

    function writeStarterOptions(eventGroup, heatNo, confirmed) {
        let toScoreboard = $("#includeDisplay").prop("checked");
        let inQueueDisplay = $("#scorePositionForce").is(":checked");
        let entries = getEntriesForEgHeatNo(eventGroup, heatNo);
        let displayTime = parseInt($("#scoreDisplayTime").val());
        if (displayTime < 5) {
            displayTime = 5;
        }
        if (displayTime > 60) {
            displayTime = 60;
        }
        let useCheckedIn = cardShowsCheckedIn();
        let checkedIn = 0;
        if (useCheckedIn) {
            checkedIn = 1;
        }
        let url = "<?php echo E4S_BASE_URL ?>seeding/" + getEventGroupId(eventGroup) + "/" + checkedIn + "/" + heatNo;
        let payload = {};
        let compObj = getCompetition();
        payload.competition = {};
        payload.competition.id = compObj.id;
        payload.competition.name = compObj.name;
        payload.toScoreboard = !!toScoreboard,
            payload.scoreboardConfig = {
                "force": !!inQueueDisplay,
                "displayTime": displayTime
            };
        payload.confirmed = confirmed ? 1 : 0;
        let sendEntries = [];
        for (let e in entries) {
            let entry = entries[e];
            let newObj = {};
            let athlete = null;
            let laneNo = getEntryLaneNo(entry);
            if (entry.teamid) {
                let team = getEntityFromHeatAndLane(heatNo, laneNo);
                team.clubname = '';
                newObj.athlete = team;
                newObj.entryId = entry.teamid;
            } else {
                let athlete = JSON.parse(JSON.stringify(getAthleteById(entry.athleteId)));
                athlete.firstname = athlete.firstname;
                athlete.surname = athlete.surname;
                athlete.bibno = getBibNoToUse(athlete, entry);
                newObj.athlete = athlete;
                newObj.entryId = getEntryId(entry);
            }

            delete (newObj.athlete.events);
            newObj.heatNo = heatNo;
            // let laneNo = entry.heatInfo.laneNo;
            // if (useCheckedIn) {
            //     laneNo = entry.heatInfo.laneNoCheckedIn;
            // }
            newObj.laneNo = laneNo;

            newObj.present = 1;
            if (!isEntryPresent(entry)) {
                newObj.present = 0;
                newObj.athlete.bibno = "DNS";
            }
            sendEntries.push(newObj);
        }
        payload.participants = sendEntries;
        payload.eventGroup = eventGroup;
        let sendPayload = {};
        sendPayload.data = payload;
        $.post(url, sendPayload);
    }

    function moveHeatVertical(heatNo, factor) {
        let heatInfo = getHeatInfo(heatNo);
        let compId = getCompetition().id;

        let payload = {
            compId: compId,
            egId: heatInfo.egId,
            heatNo: heatNo,
            checkIn: isCheckInInUse(),
            factor: factor
        };

        $.post("<?php echo E4S_BASE_URL ?>entries/moveVert", payload);
    }

    function moveHeatHorizontal(heatNo, factor, fromLaneNo) {
        let heatInfo = getHeatInfo(heatNo);
        let compId = getCompetition().id;
        let entries = [];
        let entriesObj = [];
        if (typeof fromLaneNo === "undefined") {
            fromLaneNo = 0;
        }
        for (let e in heatInfo.entries) {
            let entry = heatInfo.entries[e];
            let entryLaneNo = getEntryLaneNo(entry);
            let entryInfo = {
                athleteId: entry.athleteId,
                entryId: getEntryId(entry),
                teamId: 0
            };
            if (entry.teamid && entry.teamid > 0) {
                entryInfo.teamId = entry.teamid;
                entryInfo.entryId = entry.teamid;
                entryInfo.athleteId = 0;
            }
            if (fromLaneNo === 0) {
                entries.push(entryInfo);
            } else {
                entriesObj[entryLaneNo] = entryInfo;
            }
        }
        if (fromLaneNo > 0) {
            // process upto the next blank lane
            for (let l in entriesObj) {
                let lane = parseInt(l);
                if ((lane <= fromLaneNo && factor > 0) || (lane >= fromLaneNo && factor < 0)) {
                    entries.push(entriesObj[l]);
                }
            }
        }
        let payload = {
            compId: compId,
            egId: heatInfo.egId,
            entryInfo: entries,
            checkIn: isCheckInInUse(),
            factor: factor
        };

        $.post("<?php echo E4S_BASE_URL ?>entries/moveHoriz", payload);
    }

    function bindStarterOptions(eventGroup) {
        let heatCount = getHeatCountForEG(eventGroup);
        let laneCount = getLaneCount(eventGroup);
        let maxInHeat = getMaxInHeatFromEg(eventGroup);

        // add click handle to all bib cells  (including banks! )
        $("[bibselect]").on("click", function () {
            let bibInfo = this.id.split("_");
            let heatNo = parseInt(bibInfo[1]);
            let laneNo = parseInt(bibInfo[2]);
            if (isSwitchDialogDisplayed()) {
                showPulse(heatNo, laneNo, true);
                switchTrackPresent(heatNo, laneNo);
            }
        });
        for (let h = 1; h <= heatCount; h++) {
            $("[e4sheat=" + h + "]").on("click", function (e) {
                starterOptionsDialog(eventGroup, h);
            });

        }
    }

    function updatePb(obj) {
        let key = obj.id;
        let value = $("#" + key).val();
        if (key.indexOf("pb_") > -1) {
            let arr = key.split("pb_");
            key = arr[1];
        }
        $.post("<?php echo E4S_BASE_URL ?>competition/pbupdate?entryid=" + key + "&value=" + value);
    }

    function writeFieldSeedings(entries) {
        let sortKeys = $("#sortableList").sortable("toArray", {attribute: 'key'});
        let url = "https://<?php echo E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH . '/' ?>seeding/write/";
        // get the eventgroupid from the first element
        let egId = seedingsGetEventGroupId(sortKeys[0]);
        url += egId;
        let poolCnt = getPoolCount();
        let entryCnt = sortKeys.length;
        let maxInHeat = Math.ceil(entryCnt / poolCnt);

        // if checkedIn seedings, we only send them so update rather than insert
        let payload = {
            checkedIn: cardShowsCheckedIn(),
            maxInHeat: maxInHeat
        };

        payload.seedings = $("#sortableList").sortable("toArray", {attribute: 'key'});
        $.post(url, payload);

        updateUiSorting(entries, sortKeys);
    }

    function seedingsGetEventGroupId(key) {
        return key.split("-")[0];
    }

    function updateEventGroupMaxInHeat() {
        let eventGroup = getCurrentEventGroup();
        let maxInHeat = event.target.value;
        let url = '<?php echo E4S_BASE_URL ?>eventgroup/maxinheat/' + getEventGroupId(eventGroup) + '/' + maxInHeat;
        $.post(url);
    }

    function writeEGOptions(eventGroup, options, callback) {
        let url = '<?php echo E4S_BASE_URL ?>eventgroup/cardupdate/' + getEventGroupId(eventGroup);
        let payload = {
            options: options
        };
        $.post(url, payload, callback);
    }

    function allowDoubleUp(eventGroup) {
        if (eventGroup.eOptions) {
            if (eventGroup.eOptions.allowDoubleUp) {
                return eventGroup.eOptions.allowDoubleUp;
            }
        }
        return false;
    }

    function getDoubleUpLanes(eventGroup) {
        if (eventGroup.options) {
            if (eventGroup.options.seed) {
                if (eventGroup.options.seed.doubleup) {
                    return eventGroup.options.seed.doubleup.toString();
                }
            }
        }
        return "";
    }

    function getEventGroupAutoQualify(eventGroup) {
        if (eventGroup.options) {
            if (eventGroup.options.seed) {
                if (eventGroup.options.seed.qualifyToEg) {
                    // if ( eventGroup.options.seed.qualifyToEg.rules ){
                    return eventGroup.options.seed.qualifyToEg;
                    // }
                }
            }
        }
        return {
            auto: 0,
            nonAuto: 0
        };
    }

    function updateEventGroupDialog() {
        if ( isEventGroupTrack() ) {
            return updateEventGroupTrackDialog();
        }
        if ( isEventAHeightEvent() ){
            return false;
        }
        updateEventGroupFieldDialog();
    }

    function addTrialsToCompOptions(tOptions){
        let comp = getCompetition();
        let options = comp.options;
        options.trials = tOptions;
    }

    function minTrialsChanged(){
        let visibility = $("#egOptionsMinTrials").val() === "0" ? "none" : "";
        let athleteCntObj = $("#egOptionsAthleteCnt");
        let athleteCnt = getAthleteCntFromOptions(getCurrentEventGroup().options);
        if (athleteCnt === 0){
            athleteCnt = 8;
        }
        $("[mintrialsrequired=true]").css("display", visibility);
        if ( visibility !== "" ){
            athleteCntObj.val(0);
            $("#egOptionsJumpOrder0").prop("checked", true);
        }else{
            athleteCntObj.val(athleteCnt);
        }
    }
    function updateEventGroupFieldDialog(callback = null) {
        let eventGroup = getCurrentEventGroup();
        let divName = "e4sEventGroupOptions";
        let divHTML = "<div id='" + divName + "'>";
        let minTrials = getMinTrialsFromOptions(eventGroup.options);
        divHTML += '<table>';
        divHTML += '<tr>';
        divHTML += '<td class="confirm_title_style">Max Trials : </td><td class="confirm_desc_style"><input type="number" min=1 max=6 style="width:50px;" id="egOptionsMaxTrials" value="' + getMaxTrialsFromOptions(eventGroup.options) + '"></td>';
        divHTML += '</tr>';
        divHTML += '<tr>';
        divHTML += '<td class="confirm_title_style">Min Trials : </td><td class="confirm_desc_style"><input type="number" min=0 max=6 style="width:50px;" id="egOptionsMinTrials" value="' + minTrials + '" onchange="minTrialsChanged();"></td>';
        divHTML += '</tr>';
        divHTML += '<tr id="athleteCntRow" mintrialsrequired=true>';
        divHTML += '<td class="confirm_title_style">Athletes to Progress : </td><td class="confirm_desc_style"><input type="number" min=1 style="width:50px;" id="egOptionsAthleteCnt" value="' + getAthleteCntFromOptions(eventGroup.options) + '"></td>';
        divHTML += '</tr>';
        divHTML += '<tr mintrialsrequired=true>';
        divHTML += '<td class="confirm_title_style">Jump Order : </td>';

        let jumpOrder = getJumpOrderFromOptions(eventGroup.options);
        let checked = "";
        if ( jumpOrder === "" ){
            checked = " checked ";
        }
        divHTML += '<td class="confirm_desc_style"><input type="radio" id="egOptionsJumpOrder0" name="egOptionsJumpOrder" value="" ' + checked + '>No Change</td>';
        divHTML += '</tr>';
        divHTML += '<tr mintrialsrequired=true>';
        checked = "";
        if ( jumpOrder === "-" ){
            checked = " checked ";
        }
        divHTML += '<td>&nbsp;</td><td class="confirm_desc_style"><input type="radio" id="egOptionsJumpOrder-1" name="egOptionsJumpOrder" value="-" ' + checked + '>Reverse</td>';
        divHTML += '</tr>';

        divHTML += '<tr>';
        divHTML += '<td class="confirm_title_style">Set as Default : </td><td class="confirm_desc_style"><input type="checkbox" id="egOptionsDefault" value=""></td>';
        divHTML += '</tr>';
        divHTML += '</table>';

        $("#" + divName).remove();
        $("#dialogEGOptions").html(divHTML);

        let dialog = $("#" + divName).dialog({
            resizable: false,
            title: getEventGroupTitle(eventGroup),
            height: "auto",
            width: 350,
            modal: true,
            open: minTrialsChanged()
        });
        let buttons = [
            {
                text: "Confirm",
                class: "resultButton",
                click: function () {
                    let options = {};
                    let max = $("#egOptionsMaxTrials").val();
                    let min = $("#egOptionsMinTrials").val();
                    let athleteCnt = $("#egOptionsAthleteCnt").val();
                    let setDefault = $("#egOptionsDefault").is(":checked");
                    let jumpOrder = $('input[name="egOptionsJumpOrder"]:checked').val();

                    if (setDefault) {
                        options.trialsDefault = true;
                        // add to competition options
                        addTrialsToCompOptions(options.trials);
                        // remove from event group
                        clearTrialsFromOptions(options);
                    }else{
                        setMaxTrialsFromOptions(options, max);
                        setMinTrialsFromOptions(options, min);
                        setAthleteCntFromOptions(options, athleteCnt);
                        setJumpOrderFromOptions(options, jumpOrder);
                    }
                    options.reSeed = false;

                    writeEGOptions(eventGroup, options, reloadCard);
                    $(this).dialog("close");
                    if (callback) {
                        writeLaunchOption(callback.name);
                    }
                }
            },
            {
                text: "Close",
                class: "resultButton",
                click: function () {
                    $(this).dialog("close");
                    if (callback) {
                        callback();
                    }
                }
            }
        ];
        dialog.dialog("option", "buttons", buttons);
        positionDialog(dialog);
        $("label").css("padding-left", "3px");
    }

    function updateEventGroupTrackDialog() {
        let eventGroup = getCurrentEventGroup();
        let egQualify = getEventGroupAutoQualify(eventGroup);
        let divName = "e4sEventGroupOptions";
        let divHTML = "<div id='" + divName + "'>";
        let laneCount = getLaneCount(eventGroup);
        let firstLane = getFirstLane(eventGroup);
        divHTML += '<table><tr>';
        divHTML += '<td class="confirm_title_style">Max per Heat : </td><td class="confirm_desc_style"><input type="number" style="width:50px;" id="egOptionsMaxInHeat" value="' + getMaxInHeatFromOptions(eventGroup.options) + '"></td>';
        divHTML += '</tr><tr>';
        divHTML += '<td class="confirm_title_style">Lane Count : </td><td class="confirm_desc_style"><input type="number" style="width:50px;" id="egOptionslaneCount" value="' + laneCount + '"></td>';
        divHTML += '</tr><tr>';
        divHTML += '<td class="confirm_title_style">First Lane : </td><td class="confirm_desc_style"><input type="number" style="width:50px;" id="egOptionsFirstLane" value="' + firstLane + '"></td>';
        if (allowDoubleUp(eventGroup)) {
            let doubleUpLanes = getDoubleUpLanes(eventGroup);
            divHTML += '</tr><tr>';
            divHTML += '<td class="confirm_title_style">Double Up Lanes : </td><td class="confirm_desc_style"><input  style="width:70px;" id="egOptionsDoubleUp" value="' + doubleUpLanes + '"></td>';
        }
        divHTML += "</div>";
        divHTML += '</tr><tr>';
        divHTML += '<td class="confirm_title_style">Seeding : </td>';
        divHTML += '<td class="confirm_desc_style">';
        let checked = "";
        if (getSeedingAge(eventGroup)) {
            checked = "checked";
        }
        divHTML += '<input type="checkbox" id="optionsSeedingAge" name="optionsSeeding" ' + checked + ' value="A">';
        divHTML += '<label for="currentCard">Age</label>';
        divHTML += '&nbsp;&nbsp;';
        checked = "";
        if (getSeedingGender(eventGroup)) {
            checked = "checked";
        }
        divHTML += '<input type="checkbox" id="optionsSeedingGender" name="optionsSeeding" ' + checked + ' value="G">';
        divHTML += '<label for="currentCard">Gender</label>';
        divHTML += '</td>';
        divHTML += '</tr>';
        if (egQualify.id > 0) {
            // if ( egQualify.rules.auto > 0 ) {
            let autoQualify = 2;
            let nonAutoQualify = 3;
            if (egQualify.rules) {
                autoQualify = egQualify.rules.auto;
                nonAutoQualify = egQualify.rules.nonAuto;
            }
            divHTML += '<tr>';
            divHTML += '<td class="confirm_title_style">Qualifying : </td>';
            divHTML += '<td class="">Auto : <input id="autoQualifyCount" type="number" class="autoQualifyRulesField" value="' + autoQualify + '"></td>';
            divHTML += '<td class="">Non-Auto : <input id="nonAutoQualifyCount" type="number" class="autoQualifyRulesField" value="' + nonAutoQualify + '"></td>';
            divHTML += '</tr>';
            // }
        }
        divHTML += '<tr>';
        divHTML += '<td class="confirm_title_style">Re-Seed Event : </td>';
        divHTML += '<td class="confirm_desc_style">';
        divHTML += '<input type="checkbox" id="optionsReSeed" name="optionsReSeed" checked="checked" value="Y">';
        divHTML += '<label for="optionsReSeed">Yes</label>';
        divHTML += '</td>';
        divHTML += '</tr>';
        divHTML += '</table>';
        $("#" + divName).remove();
        $("#dialogEGOptions").html(divHTML);

        let dialog = $("#" + divName).dialog({
            resizable: false,
            title: "Options for  " + getEventGroupTitle(eventGroup),
            height: "auto",
            width: 500,
            modal: true
        });
        let buttons = [
            {
                text: "Confirm",
                class: "resultButton",
                click: function () {
                    let options = {};
                    let maxInHeat = $("#egOptionsMaxInHeat").val();
                    let laneCount = $("#egOptionslaneCount").val();
                    let firstLane = $("#egOptionsFirstLane").val();
                    let doubleUp = $("#egOptionsDoubleUp").val();
                    let autoQualify = $("#autoQualifyCount").val();
                    let nonAutoQualify = $("#nonAutoQualifyCount").val();

                    setMaxInHeat(options, maxInHeat);
                    setLaneCountInOptions(options, laneCount);
                    setFirstLaneInOptions(options, firstLane);
                    setDoubleUpInOptions(options, doubleUp);
                    setAutoQualifyInOptions(options, autoQualify);
                    setNonAutoQualifyInOptions(options, nonAutoQualify);
                    setSeedingAge(options, $("#optionsSeedingAge").prop("checked"));
                    setSeedingGender(options, $("#optionsSeedingGender").prop("checked"));
                    options.reSeed = $("#optionsReSeed").prop("checked");
                    let callBack = reloadCard;
                    if (options.reSeed) {
                        callBack = reSeedCard;
                    }
                    writeEGOptions(eventGroup, options, callBack);
                    $(this).dialog("close");
                }
            },
            {
                text: "Close",
                class: "resultButton",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ];
        dialog.dialog("option", "buttons", buttons);
        positionDialog(dialog);
        $("label").css("padding-left", "3px");
    }

    function setSeedingAge(options, value) {
        options.seed.age = value;
    }

    function getSeedingAge(eventGroup) {
        return eventGroup.options.seed.age;
    }

    function setSeedingGender(options, value) {
        options.seed.gender = value;
    }

    function getSeedingGender(eventGroup) {
        return eventGroup.options.seed.gender;
    }

    function confirmEventGroupSeedings(callRoom) {
        let eventGroup = getCurrentEventGroup();
        let url = "<?php echo E4S_BASE_URL ?>seeding/confirm/" + getEventGroupId(eventGroup);
        let entries = getEntriesForEg(eventGroup);
        let seededEntries = [];
        for (let e in entries) {
            let entry = entries[e];
            let entryObj = {
                "athleteId": entry.athleteId ? entry.athleteId : null,
                "teamId": entry.teamid ? entry.teamid : null,
                "entryid": entry.teamid ? entry.teamid : getEntryId(entry),
                "heatInfo": {
                    "heatNo": getRaceNo(entry),
                    "heatNoCheckedIn": getRaceNoCheckedIn(entry),
                    "laneNo": getLaneNo(entry),
                    "laneNoCheckedIn": getLaneNoCheckedIn(entry)
                }
            };
            seededEntries.push(entryObj);
        }
        let payload = {
            "present": callRoom ? "0" : "1",
            "compid": getCompetition().id,
            "entries": seededEntries
        };
        $.post(url, payload, function () {
            if (autoShow.available) {
                autoShow.sendTo();
            }
        });
    }

    function reSeedCard() {
        showPleaseWait(true, "Re-Seeding Card");
        let url = "https://<?php echo E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH . '/' ?>seeding/clear/";
        url += getCurrentEventGroup().id;
        $.post(url).then(function () {
            refreshCurrentCard();
        });
    }

    function reloadCard() {
        showPleaseWait(true, "Updating Card");
        refreshCurrentCard();
    }

    function getCallRoomStatus() {
        return getCompetition().options.cardInfo.callRoom;
    }

    function setCallRoomStatusInOptions(status) {
        getCompetition().options.cardInfo.callRoom = status;
    }

    function updatePhotofinish(pf) {
        let url = "https://<?php echo E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH ?>/competition/update?compid=" + getCompetition().id + "&action=<?php echo E4S_COMP_SETTING_PF ?>&val=" + pf;
        $.post(url);
        photoFinishExport.autoExportBtn(pf);
    }

    function getPFSystem() {
        return getCompetition().options.pf.system;
    }

    function updatePhotofinishSystem() {
        let system = photoFinishExport.getPFSystemFromUI();
        let url = "https://<?php echo E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH ?>/competition/update?compid=" + getCompetition().id + "&action=<?php echo E4S_COMP_SETTING_SYSTEM ?>&val=" + system;
        $.post(url);
    }

    function setCallRoomStatus() {
        let status = $("#callRoom").is(":checked");
        setCallRoomStatusInOptions(status);
        let statusStr = "false";
        if (status) {
            statusStr = "true";
        }
        let url = "https://<?php echo E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH ?>/competition/update?compid=" + getCompetition().id + "&action=<?php echo E4S_COMP_SETTING_CALLROOM ?>&val=" + statusStr;
        $.post(url);
    }

    function poolCountChanged() {
        redrawSeedingsOnPoolCount();
    }

    function getPoolCount() {
        return parseInt($("#egPoolCount").val());
    }

    function redrawSeedingsOnPoolCount() {
        let pool1 = "#dfeffc";
        let pool2 = "#f0f0f0";
        let pool3 = "#e9f6ff";
        let entryCnt = getEntriesForCurrentEg(false).length;
        let poolCnt = getPoolCount();
        let maxInHeat = Math.ceil(entryCnt / poolCnt);
        $("ul li").css("background", pool1);
        if (poolCnt > 1) {
            $("ul li:gt(" + maxInHeat + ")").css("background", pool2);
            if (poolCnt > 2) {
                $("ul li:gt(" + (maxInHeat * 2) + ")").css("background", pool3);
            }
        }

    }

    function clearAndPopulateSeeding(entries) {
        $("#manualSeedDialog").remove();
        let sortOrder = getSeedingSort();
        let eventGroup = getCurrentEventGroup();
        let html = '<div id="manualSeedDialog">';
        html += '<style>';
        html += '#sortableList { list-style-type: none; margin: 0; padding: 0; width: 60%; box-sizing: unset !important }';
        html += '#sortableList li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; width:500; height: 18px; box-sizing: unset !important}';
        html += '#sortableList li span { margin-left: -1.3em; }';
        html += '</style>';
        if (!isEventATeamEvent(eventGroup)) {
            html += '<table style="width:90%;">';
            html += "<tr>";
            html += "<td>";
            html += 'Sort By : ';
            html += '<input type="radio" id="seedSort<?php echo E4S_SEED_HEAT ?>" name="seedSort" onclick="seedSortBy(this);"';
            if (sortOrder === "<?php echo E4S_SEED_HEAT ?>") {
                html += " checked ";
            }
            html += '>';
            if (isEventGroupField()) {
                html += 'Position';
            } else {
                html += 'Race/Lane';
            }

            html += '<input type="radio" id="seedSort<?php echo E4S_SEED_BIB ?>" name="seedSort" onclick="seedSortBy(this);" ';
            if (sortOrder === "<?php echo E4S_SEED_BIB ?>") {
                html += " checked ";
            }
            html += '>Bib No ';
            html += '<input type="radio" id="seedSort<?php echo E4S_SEED_FORENAME ?>" name="seedSort" onclick="seedSortBy(this);"';
            if (sortOrder === "<?php echo E4S_SEED_FORENAME ?>") {
                html += " checked ";
            }
            html += '>Forename ';
            html += '<input type="radio" id="seedSort<?php echo E4S_SEED_SURNAME ?>" name="seedSort" onclick="seedSortBy(this);"';
            if (sortOrder === "<?php echo E4S_SEED_SURNAME ?>") {
                html += " checked ";
            }
            html += '>Surname ';
            if (isEventGroupField()) {
                html += '<input type="radio" id="seedSort<?php echo E4S_SEED_CHECKEDIN ?>" name="seedSort" onclick="seedSortBy(this);"';
                if (sortOrder === "<?php echo E4S_SEED_CHECKEDIN ?>") {
                    html += " checked ";
                }
                html += '>CheckIn ';
            }
            html += "</td>";
            html += "</tr>";
            html += "</table>";
        }
        html += '<div style="overflow: scroll; height: 400px;">';
        html += "<ul id='sortableList'>";

        // let isFinal = isSelectedAFinal(eventGroup);
        let eventName = eventGroup.event;
        let firstEntry = -1;

        for (let e in entries) {
            let entry = entries[e];
            if (firstEntry === -1) {
                firstEntry = e;
            }
            html += addEntryToSeedingList(entry, eventGroup);
        }
        html += "</ul>";
        html += "</div>";
        let callRoomEnabled = "";
        if (getCallRoomStatus()) {
            callRoomEnabled = " checked ";
        }
        html += '<div><input id="callRoom" type="checkbox" value="1" onchange="setCallRoomStatus();"' + callRoomEnabled + '> Check to get Call Room/Starter Assistant to Mark Present<div>';

        if (isEventGroupField()) {
            let maxInHeat = getMaxInHeatFromEg(eventGroup);
            let entryCnt = getEntriesForCurrentEg(false).length;
            let poolCnt = 1;
            if (maxInHeat > 0) {
                poolCnt = Math.ceil(entryCnt / maxInHeat);
            }

            html += '<div class="maxInHeat">';
            html += 'Number of Pools : <input type="number" min="1" max="3" style="width:50px;" onchange="poolCountChanged(); return false;" id="egPoolCount" value="' + poolCnt + '"</input>';
            html += '</div>';
        }

        let parentSeedingsDiv = $("#seedingsDiv");
        parentSeedingsDiv.css("overflow", "hidden");
        let seedingsDiv = $("#seedings");
        seedingsDiv.html(html);
        if (isEventGroupField()) {
            let sortable = $("#sortableList");
            sortable.sortable({
                create: function (event, ui) {
                    redrawSeedingsOnPoolCount();
                },
                stop: function (event, ui) {
                    redrawSeedingsOnPoolCount();
                }
            });
            sortable.disableSelection();
        }
        if (autoShow.available) {
            autoShow.getList();
        }
        parentSeedingsDiv.show();

        let dialogHeight = 600;
        if (isEventGroupField()) {
            dialogHeight = 650;
        }
        parentSeedingsDiv.dialog({
            title: "Seeding for " + eventName,
            resizable: false,
            height: dialogHeight,
            width: 650,
            modal: true
        });
        let buttons = [];
        if (!isEventATeamEvent(eventGroup)) {
            buttons.push({
                text: "Help",
                class: "",
                click: function () {
                    displayForceHelp(null);
                }
            });
        }
        buttons.push({
            text: "Set Display",
            class: "",
            click: function () {
                autoShow.displayDialog();
                $(this).dialog("close");
            }
        });
        if ( !getResultsForEG(eventGroup) ) {
            if (!isEventATeamEvent(eventGroup)) {
                buttons.push({
                    text: "Generate",
                    class: "e4sRedText",
                    click: function () {
                        // e4sConfirm("Are you sure ?", "Reset to System Generated Seeding.", reSeedCard);
                        updateEventGroupDialog();
                    }
                })
            }

            buttons.push({
                text: "Mark Seeded",
                click: function () {
                    if (autoShow.available) {
                        autoShow.getList();
                    }
                    seedingComplete();
                    $(this).dialog("close");
                }
            });
        }
        if (isEventGroupField()) {
            buttons.push({
                text: "Update",
                click: function () {
                    displayForceHelp(false);
                    writeFieldSeedings(entries);
                    setPoolCounter(1);
                    $(this).dialog("close");
                }
            });
        }

        parentSeedingsDiv.dialog("option", "buttons", buttons);
    }

    function getAthletesInOrderAndSeed(sortOrder) {
        setRefreshSeeding(false);
        let entryArr = getEntriesInOrderForEvent(sortOrder);
        clearAndPopulateSeeding(entryArr);
    }

    function seedSortBy(obj) {
        let sortOrder = $(obj)[0].id;
        sortOrder = sortOrder.replace('seedSort', '');
        getAthletesInOrderAndSeed(sortOrder);
    }

    function displayForceHelp(show) {
        let content;

        if (isEventGroupField()) {
            content = 'Below are the athletes in this event. They are ( default ) shown in order of PB. This will be slowest/fastest dependant on the settings for the competition.' +
                'Please drag the athletes rows to the choosen position. On refreshing, they will then be re-seeded.';
        } else {
            content = 'The athletes for this event are shown below. You can view these in various sort orders simply by clicking the option at the top ( Sort By ).' +
                'To modify an athletes information, click on the pen icon to the left of their bib number.'
        }

        let helpDiv = $(".forceHelp");
        helpDiv.text(content);
        if (show === null) {
            helpDiv.toggle();
        } else {
            if (show) {
                helpDiv.show();
            } else {
                helpDiv.hide();
            }
        }
    }

    function showEntryOptions(heatNo, laneNo) {
        let entry = getEntryForHeatLane(heatNo, laneNo);
        location.href = '/#/actions/' + getCompetition().id + '/' + entry.athleteId + '/' + entry.clubId + '/' + getEntryId(entry);
    }

    function removeEntryFromRace(heatNo, laneNo) {
        let entry = getEntryForHeatLane(heatNo, laneNo);
        let url = "<?php echo E4S_BASE_URL ?>entry/delete/" + getEntryId(entry) + "/Removed from Card";
        $.post(url);
    }

    function addCompNotesToAthlete(heatNo, laneNo) {
        let entry = getEntryForHeatLane(heatNo, laneNo);
        let athlete = getAthleteById(entry.athleteId);
        let bibNo = "<?php echo E4S_NO_BIBNO?>";
        let notes = getBibNotesForAthlete(athlete);
        if (entry === null) {
            return;
        } else {
            if (typeof entry.bibNo !== "undefined") {
                bibNo = entry.bibNo;
            }
        }
        e4sGlobal.selectedBibNo = bibNo;
        e4sGlobal.selectedAthlete = athlete;
        let btnLables = ['Update', 'Close'];
        e4sRequest(' ', "Notes for Athlete : " + getNameFromEntity(athlete), notes, updateCompNotesForAthlete, btnLables);
    }

    function addNotesToEventGroup() {
        let currentEg = getCurrentEventGroup();
        e4sRequest(currentEg.event + " notes", "Please enter notes for this event", currentEg.notes, updateEgNotes);
    }

    function updateCompNotesForAthlete(origNotes, notes) {
        let bibNo = e4sGlobal.selectedBibNo;
        let athlete = e4sGlobal.selectedAthlete;
        // let origNotes = getBibNotesForAthlete(athlete);
        if (notes === origNotes) {
            // not changed
            return;
        }
        let payload = {
            "compId": getCompetition().id,
            "bibNo": bibNo,
            "athleteId": athlete.id,
            "notes": notes
        };
        let url = "<?php echo E4S_BASE_URL ?>bib/notes";
        $.ajax({
            type: "POST",
            url: url,
            data: payload,
            dataType: "json"
        });
    }

    function updateEgNotes(origNotes, notes) {
        let currentEg = getCurrentEventGroup();
        let payload = {
            "id": getEventGroupId(currentEg),
            "notes": notes
        };
        let url = "<?php echo E4S_BASE_URL ?>eventgroup/notes";
        $.ajax({
            type: "POST",
            url: url,
            data: payload,
            dataType: "json"
        });
    }

    function addAthleteResponse(response) {
        showPleaseWait(false);
        if (response.errNo === 0) {
            $("#addAthleteForceDiv").hide();
            $("#addAthleteErrors").text("");
        } else {
            $("#addAthleteErrors").text(response.error);
            if (response.errNo === 8500) {
                $("#addAthleteForceDiv").show();
            }
        }
    }

    function displayAthleteToAdd(athlete) {
        let html = "";
        if (typeof athlete !== "undefined") {
            html += "<br>";
            let style = "border: 1px solid lightgray;border-collapse: collapse;";
            html += "<table style='width:100%; " + style + "'>";
            html += "<tr>";
            html += "<td style='width:100px;" + style + "'>Athlete</td><td style='" + style + "'>" + getAthleteNameFromAthlete(athlete) + "</>";
            html += "</tr>";
            html += "<tr>";
            html += "<td style='" + style + "'>Gender</td><td style='" + style + "'>" + getFullGender(athlete.gender) + "</>";
            html += "</tr>";
            html += "<tr>";
            html += "<td style='" + style + "'>Age Group</><td style='" + style + "'>" + athlete.agegroup + "</>";
            html += "</tr>";
            html += "<tr>";
            html += "<td style='" + style + "'>Club</><td style='" + style + "'>" + athlete.clubname + "</>";
            html += "</tr>";
            html += "</table>";
            html += "<br><div id='addAthleteErrors' class='addAthleteWarning'></div>";
            html += "<div id='addAthleteForceDiv' style='display:none;'>";
            html += "<input id='addAthleteForce' type='checkbox' value='Yes'>Confirm Entry";
            html += "</div>";
            html += "<input readonly style='display:none;' id='athleteIdToAdd' value='" + athlete.id + "'>";
            html += "<input readonly style='display:none;' id='ageGroupIdToAdd' value='" + athlete.ageGroupId + "'>";
            html += "<input readonly style='display:none;' id='ageGroupToAdd' value='" + athlete.agegroup + "'>";
            html += "<input readonly style='display:none;' id='bibNoToAdd' value='" + athlete.bibno + "'>";
            $("#addAthleteInfo").html(html);
            let entries = getEntriesForCurrentEg();
            for (let e in entries) {
                let entry = entries[e];
                if (entry.athleteId === athlete.id) {
                    $("#addAthleteErrors").text("Athlete already entered");
                    return false;
                }
            }
        } else {
            $("#addAthleteInfo").html('');
        }
        $("#addAthleteErrors").text("");
        return true;
    }

    function clearScoreboard() {
        let url = "https://<?php echo E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH . '/' ?>scoreboard/clear/";
        // get the eventgroupid from the first element
        let compid = getCompetition().id;
        let eventno = getSelectedEventNo();
        url += compid + "/" + eventno;
        $.post(url);
        showScoreboardIcon(0);
    }

    function isEmpty(obj) {
        return Object.keys(obj).length === 0;
    }

    function getRetireTrial(athleteId) {
        let currentEg = getCurrentEventGroup();
        let egResults = getResultsForEG(currentEg);
        let athleteResults = egResults[athleteId];
        let trial = 1;
        if (athleteResults) {
            for (let t = 5; t > 0; t--) {
                let result = athleteResults['t' + t];
                if (result) {
                    if (result === '<?php echo E4S_FIELDCARD_RETIRE ?>') {
                        trial = -t;
                    } else {
                        trial = t + 1;
                    }
                    break;
                }
            }
        }
        return trial;
    }

    function writeResultFromOptions(field, value, byAthlete) {
        field = field.replace(/Options/g, "");
        let fieldObj = $("#" + field);
        // Does the field have attribs that means move to next
        let trial = parseInt(fieldObj.attr("e4strial"));
        if (typeof trial === "undefined") {
            return;
        }
        let useTrial = trial;
        if (value === "<?php echo E4S_FIELDCARD_RETIRE ?>" && trial < 6) {
            // returns positive if retire is not set
            // returns negative if retire is already set
            useTrial = getRetireTrial(fieldObj.attr("athleteid"));
            if (useTrial < 0) {
                return;
            }
        }
        if (useTrial === trial) {
            fieldObj.val(value);
        }
        let sub = fieldObj.attr("e4ssub");
        let last = fieldObj.attr("lastathlete");
        last = last === "true";
        let prefix;
        let useElement = sub;
        if (byAthlete) {
            prefix = field.replace(trial, "");
            useElement = trial;
        } else {
            prefix = field.replace(sub, "");
            markUIActive(prefix, sub, value);
        }
        // Code the move and blur is not triggered
        if (trial === useTrial) {
            processNewResultEntry(prefix, useTrial, useElement);
        } else {
            writeResultsDirect(fieldObj, useTrial, sub, value);
        }
        moveToNextResult(prefix, trial, useElement, last);
    }

    function addPoint() {
        let keyValue = parseInt(event.key);
        if (isNaN(keyValue)) {
            return;
        }
        let targetValue = event.srcElement.value;
        if (targetValue.indexOf(".") > -1) {
            return;
        }
        if (targetValue.length === 2) {
            event.srcElement.value += ".";
        }
    }

    function resultKeyPress(prefix, trial, element, isLastElement) {
        // Move to next. Enter does not auto move
        let move = false;
        let validCode = false;
        let validNumber = false;
        let validChar = false;
        let returnVal = true;
        let currentValue = event.srcElement.value;
        let selectedText = window.getSelection().toString();
        // console.log(event.keyCode);
        // 8 backspace, 46 = delete, 37 = left, 39 = right
        if (event.keyCode === 13 || event.keyCode === 9 || event.keyCode === 8 || event.keyCode === 37 || event.keyCode === 39 || event.keyCode === 46) {
            validCode = true;
            if (event.keyCode === 13 || event.keyCode === 9) {
                if (!event.shiftKey) {
                    // tab jumps 2, so only move if the last element in trial
                    if ( event.keyCode === 13 || event.keyCode === 9) {
                    // if ( event.keyCode === 13 || getNextInTrial(prefix, element) === null ) {
                        move = true;
                        event.preventDefault(); // we handle the move
                    }
                    returnVal = false;
                }
            }
        } else {
            // valid keys 0-9 ., newline, tab
            let validNumbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "."];

            for (let i = 0; i < validNumbers.length; i++) {
                let maxLen = 5;
                if (currentValue.indexOf(".") > -1) {
                    let vals = currentValue.split(".");
                    if (vals[1].length > 1) {
                        maxLen = currentValue.length;
                    }
                }
                if (currentValue.length < maxLen) {
                    if (event.key === validNumbers[i]) {
                        validNumber = true;
                        addPoint();
                        break;
                    }
                }
            }
            if (!validNumber) {
                if (currentValue === '') {
                    let option = getValidOption(event.key);
                    if (option !== "") {
                        validChar = true;
                        move = true;
                        event.srcElement.value = option;
                        event.preventDefault();
                        returnVal = false;
                    }
                }
            }
        }
        if (validCode || validNumber || validChar) {
            if (move) {
                distanceCard.processBlur = false;
                processNewResultEntry(prefix, trial, element);
                let eventGroup = getCurrentEventGroup();
                if (isLastElement && trial === getMaxTrialsFromOptions(eventGroup.options)) {
                    completeEvent(card_results_distance_dialog);
                } else {
                    if ( !trialCompleted(trial) ) {
                        trial--;
                    }
                    moveToNextResult(prefix, trial, element, isLastElement);
                }
            }
            return returnVal;
        } else {
            return false;
        }
    }

    function trialCompleted(trial){
        let results = distanceCard.results;
        let completed = true;
        for(let r in results){
            let result = results[r];
            if ( typeof result["t" + trial] === "undefined" || result["t" + trial] === "" ){
                completed = false;
                break;
            }
        }
        return completed;
    }
    function resultBlur(prefix, trial, element) {
        console.log("Blur " + element);
        if ( distanceCard.processBlur ) {
            let fieldValue = event.srcElement.value;
            if ( fieldValue !== distanceCard.focusValue ) {
                processNewResultEntry(prefix, trial, element);
            }
        }
    }
    function resultFocus(prefix, trial, element) {
        let fieldObj = $("#" + prefix + element);
        let fieldVal = fieldObj.val();
        if (fieldVal === "0.00") {
            fieldVal = '';
        }
        fieldVal = fieldVal.trim();
        console.log("Focus " + element + " : " + fieldVal);
        fieldObj.val(fieldVal);
        fieldObj.attr("e4sVal", fieldVal);
        if ( fieldVal === "" ) {
            sendNextUp(fieldObj);
        }
        distanceCard.processBlur = true;
        distanceCard.focusValue = fieldVal;
    }

    function clearNextUpTimeout() {
        if (typeof e4sGlobal.nextUpTimeout !== "undefined") {
            clearTimeout(e4sGlobal.nextUpTimeout);
        }
        sendCurrentAthlete();
    }

    function sendCurrentAthlete(entry) {
        let payload = {};
        payload.athlete = {};
        if (entry) {
            payload.athlete.id = entry.athleteId;
            let athlete = getAthleteById(entry.athleteId);
            payload.athlete.name = getAthleteNameFromAthlete(athlete);
            if ( entry.attemptHeight ){
                payload.athlete.attempt = entry.attemptHeight;
            }
        }
        payload.comp = {};
        payload.comp.id = getCompetition().id;
        payload.comp.name = getCompetition().name;
        payload.event = {}
        let eg = getCurrentEventGroup();
        payload.event.id = getEventGroupId(eg);
        payload.event.name = eg.event;
        payload.event.heatNo = 1;
        // send websocket payload
        e4sSocket.send(payload, "<?php echo R4S_SOCKET_EVENT_NEXT_UP?>");
    }

    // timeout may be 0 as in, clicked show from card results, so show immediately
    function sendNextUp(fieldObj, timeout) {
        // process if required
        let sendMsg = true;
        if ( e4sGlobal.nextUpFieldObj ){
            sendMsg = !(e4sGlobal.nextUpFieldObj.attr("id") === fieldObj.attr("id"));
        }
        if (sendMsg) {
            e4sGlobal.nextUpFieldObj = fieldObj;
            clearNextUpTimeout();
            if (typeof timeout === "undefined") {
                timeout = 3000;
            }
        // wait 3 seconds then send if it is ok. also makes sure the last result has had time to send and display
            e4sGlobal.nextUpTimeout = window.setTimeout(sendNextUpToDB, timeout);
        }
    }

    function sendNextUpToDB() {
        let fieldObj = e4sGlobal.nextUpFieldObj;
        let eg = getCurrentEventGroup();
        let egId = getEventGroupId(eg);
        // only set next up if there is not a value in the field
        // if (fieldObj.val() === "") {
            e4sGlobal.nextUpSentToDB = true;
            showScoreboardIcon(fieldObj.attr("e4ssub"));
            let athleteId = fieldObj.attr("athleteId");
            let trial = fieldObj.attr("e4strial");

            let payload = {
                "egId": parseInt(egId),
                "athleteId": parseInt(athleteId),
                "heatNo": parseInt(trial)
            };

            let url = "<?php echo E4S_BASE_URL ?>event/nextup";
            $.ajax({
                type: "POST",
                url: url,
                data: payload,
                dataType: "json"
            });
        //} else if (typeof e4sGlobal.nextUpSentToDB !== "undefined" && e4sGlobal.nextUpSentToDB) {
        //    let url = "<?php //echo E4S_BASE_URL ?>//event/nextup/" + egId;
        //    e4sGlobal.nextUpSentToDB = false;
        //    $.ajax({
        //        type: "DELETE",
        //        url: url,
        //        dataType: "json"
        //    });
        // }
    }

    function processNewResultEntry(prefix, trial, element) {
        let fieldObj = $("#" + prefix + element);
        fieldObj.val(fieldObj.val().trim());
        let value = fieldObj.val();

        markUIActive(prefix, element, value);
        if (fieldObj.attr("e4sVal") !== value) {
            writeResult(prefix, trial, element);
        }
    }

    function athleteResultsForEventNo(athleteId, eventNo = null) {
        if (eventNo === null) {
            let eg = getCurrentEventGroup();
            eventNo = eg.eventno;
        }
        let results = getResults();
        if (!results[eventNo]) {
            return null;
        }
        let eventResults = results[eventNo];
        if (!eventResults[athleteId]) {
            return null;
        }
        return eventResults[athleteId];
    }

    function athleteResultsForEG(athleteId, eg = null) {
        if (eg === null) {
            eg = getCurrentEventGroup();
        }
        let eventNo = eg.eventno;
        return athleteResultsForEventNo(athleteId, eventNo);
    }

    function anyNotActiveResult(eventNo, athleteId) {
        let athleteResults = athleteResultsForEventNo(athleteId, eventNo);
        for (let r in athleteResults) {
            let athleteResult = athleteResults[r];
            if (valueMarkNotActive(athleteResult)) {
                return true;
            }
        }
        return false;
    }

    // ==========================
    // Distance Functions
    // ==========================
    function checkTrialDistanceResults(trial){
        let eventGroup = getCurrentEventGroup();
        let trials = getMaxTrialsFromOptions(eventGroup.options);
        if ( trial > trials ) {
            return;
        }
        trialDistanceResults(trial);
    }
    function trialDistanceResults(trial = 1) {
        let eventGroup = getCurrentEventGroup();
        let trials = getMaxTrialsFromOptions(eventGroup.options);
        let entries = getEntriesInOrderForEvent('<?php echo E4S_SEED_HEAT ?>');
        let lastEntry = null;

        let prefix = "<?php echo E4S_RESULT_PREFIX ?>";
        let divName = card_results_distance_dialog;
        let divHTML = "<div id='" + divName + "'>";
        divHTML += '    <div class="e4s-flex-row e4s-flex-nowrap" style="gap: 12px; padding-bottom: 5px; justify-content: space-evenly; border-bottom: 2px solid; font-size: 16px">';
        divHTML += '        <button id="trial1Btn" onclick="trialDistanceResults(1)" class="e4s-button e4s-button--';
        divHTML += trial === 1 ? "primary" : "tertiary";
        divHTML += ' ui-button">';
        divHTML += '            <span class="e4s-body--100">Trial 1</span>';
        divHTML += '        </button>';
        if ( trials > 1) {
            divHTML += '        <button id="trial1Btn" onclick="trialDistanceResults(2)" class="e4s-button e4s-button--';
            divHTML += trial === 2 ? "primary" : "tertiary";
            divHTML += ' ui-button">';
            divHTML += '            <span class="e4s-body--100">Trial 2</span>';
            divHTML += '        </button>';
        }
        if ( trials > 2 ) {
            divHTML += '        <button id="trial1Btn" onclick="trialDistanceResults(3)" class="e4s-button e4s-button--';
            divHTML += trial === 3 ? "primary" : "tertiary";
            divHTML += ' ui-button">';
            divHTML += '            <span class="e4s-body--100">Trial 3</span>';
            divHTML += '        </button>';
        }
        if ( trials > 3 ) {
            divHTML += '        <button id="trial1Btn" onclick="trialDistanceResults(4)" class="e4s-button e4s-button--';
            divHTML += trial === 4 ? "primary" : "tertiary";
            divHTML += ' ui-button">';
            divHTML += '            <span class="e4s-body--100">Trial 4</span>';
            divHTML += '        </button>';
        }
        if ( trials > 4 ) {
        divHTML += '        <button id="trial1Btn" onclick="trialDistanceResults(5)" class="e4s-button e4s-button--';
        divHTML += trial === 5 ? "primary" : "tertiary";
        divHTML += ' ui-button">';
        divHTML += '            <span class="e4s-body--100">Trial 5</span>';
        divHTML += '        </button>';
        }
        if ( trials > 5 ) {
            divHTML += '        <button id="trial1Btn" onclick="trialDistanceResults(6)" class="e4s-button e4s-button--';
            divHTML += trial === 6 ? "primary" : "tertiary";
            divHTML += ' ui-button">';
            divHTML += '            <span class="e4s-body--100">Trial 6</span>';
            divHTML += '        </button>';
        }
        divHTML += '    </div>';
        divHTML += '<table style="display:block;max-height:400px; min-height:200px; overflow:auto;">';
        let sub = 0;
        let elementStatus = [];
        let pageNo = getFieldCardPool();
        let jumpOrder = getJumpOrderFromOptions(eventGroup.options);
        let minTrials = getMinTrialsFromOptions(eventGroup.options);
        let topAthleteCnt = getAthleteCntFromOptions(eventGroup.options);
        let sortOrder = (trial <= minTrials|| minTrials === 0) ? "" : jumpOrder;
        let sortedEntries = [];
        for(let e in entries){
            let entry = entries[e];
            if (getEntryRaceNo(entry) !== pageNo) {
                continue;
            }
            let sortEntry = {};
            sortEntry.key = e;
            sortEntry.athleteId = getAthleteId(entries[e]);
            sortEntry.rowNo = getEntryLaneNo(entry);
            sortEntry.sortPos = distanceCard.getAthleteMinTrialPosition(sortEntry.athleteId);
            sortEntry.active = distanceCard.isAthleteActive(entry.eventno, trial, sortEntry.athleteId);
            sortedEntries.push(sortEntry);
        }

        switch ( sortOrder ) {
            case "":
                // standard sort by active then lane order
                sortedEntries.sort(function (a, b) {
                    if (a.active === b.active) {
                        if (a.rowNo < b.rowNo) {
                            return -1;
                        }
                        return 1
                    }
                    if (a.active) {
                        return -1;
                    }
                    return 1;
                });
                break;
            case "-":
                // reverse order of positions at trial ( minTrial )
                sortedEntries.sort(function (a, b) {
                    if (a.active && b.active) {
                        if (a.sortPos <= topAthleteCnt && b.sortPos <= topAthleteCnt) {
                            if (a.sortPos > b.sortPos) {
                                return -1;
                            }
                            return 1;
                        }
                        if (a.rowNo < b.rowNo) {
                            return -1;
                        }
                        return 1;
                    }
                    if (a.active) {
                        return -1;
                    }
                    return 1;
                });
                break;
        }

        for (let s in sortedEntries) {
            let sortEntry = sortedEntries[s];
            let entry = entries[sortEntry.key];
            if (getEntryRaceNo(entry) !== pageNo) {
                continue;
            }
            sub++;
            let entryId = getEntryId(entry);
            let eventNo = entry.eventno;
            let athleteId = entry.athleteId;
            let athlete = getAthleteById(athleteId);
            let hasResults = athleteResultsForEventNo(athleteId, eventNo) === null ? false : true;
            let fieldid = prefix + sub;
            let value = distanceCard.getTrialResult(eventNo, trial, athleteId);
            let last = parseInt(s) === sortedEntries.length - 1;
            let rowClass = "";
            if (sub % 2 === 0) {
                rowClass = "trailAltRow";
            }
            divHTML += "<tr class='" + rowClass + "'>";
            divHTML += "<td class='trialResultsTd' >";
            let present = isEntryPresent(entry);
            let active = distanceCard.isAthleteActive(eventNo, trial, athleteId);
            let displayPresent = "";
            let displayAbsent = "none";
            let disabled = "";

            if (!present) {
                displayPresent = "none";
                displayAbsent = "";
            }
            if (!present || !active) {
                disabled = " disabled ";
            }
            elementStatus[sub] = present;
            let clickHTML = ''
            if (active && !hasResults) {
                clickHTML = 'markPresent("' + prefix + '","' + sub + '",false,' + entryId + ');';
            }
            divHTML += "<div onclick='" + clickHTML + "'><span id='athletepresent_" + sub + "' style='display:" + displayPresent + ";' athletepresent=true class=\'ui-icon ui-icon-check'></span></div>";
            if (active || (!present && hasResults)) {
                clickHTML = 'markPresent("' + prefix + '","' + sub + '",true,' + entryId + ');';
            }
            divHTML += "<div onclick='" + clickHTML + "'><span id='athleteabsent_" + sub + "' style='display:" + displayAbsent + ";' athletepresent=false class='ui-icon ui-state-disabled ui-icon-close'></span></div>";
            divHTML += "</td>";
            divHTML += "<td class='trialBibCol trialResultsTd centre " + (active ? "" : e4sGlobal.class.notActive) + "' rowbibno=" + prefix + sub + ">";
            divHTML += getBibNoToUse(athlete, entry);
            divHTML += "</td>";
            divHTML += "<td class='trialAthleteCol trialResultsTd'>";
            divHTML += getAthleteNameFromAthlete(athlete);
            divHTML += "</td>";
            divHTML += "<td>";
            divHTML += "<span id='sb_" + sub + "' scoreboard=true class=\"ui-icon ui-icon-contact " + e4sGlobal.class.notVisible + "\"></span>";
            divHTML += "</td>";
            divHTML += "<td class='trialFieldCol trialResultsTd'>";
            divHTML += "<input athleteId='" + athleteId + "' " +
                " lastathlete=" + last +
                " autocomplete='off'" +
                " e4ssub=" + sub +
                " e4strial=" + trial +
                " e4sEntryId=" + entryId +
                disabled +
                " value='" + value + "'" +
                " class='distanceEntry' tabindex=" + sub +
                " id='" + fieldid +
                "' onkeydown='resultKeyPress(\"" + prefix + "\"," + trial + "," + sub + "," + last + ");' " +
                "' onfocus='resultFocus(\"" + prefix + "\"," + trial + "," + sub + ")' " +
                "' onblur='resultBlur(\"" + prefix + "\"," + trial + "," + sub + ")' " +
                ">";
            let visible = "";
            if ( !valueMarkNotActive(value)) {
                if (!present || !active) {
                    visible = e4sGlobal.class.notVisible;
                }
            }
            divHTML += "<span id='" + fieldid + "OptionsParent' class='" + visible + "'>";

            divHTML += " <button id='" + fieldid + "BTN' class='ui-button ui-corner-all ui-widget resultShow' onclick='sendInfoToScoreboard(\"" + prefix + "\"," + trial + "," + sub + "); return false;'>Show</button>";
            divHTML += "<select e4sOptions=true class='resultClear' name='" + fieldid + "Options' id='" + fieldid + "Options'></select>";
            divHTML += "</span>";
            divHTML += "</td>";
            divHTML += "</tr>";
        }
        divHTML += "</table>";
        divHTML += "</div>";
        // remove if already been shown before
        $("#" + divName).remove();
        $("#dialogTrialResults").html(divHTML);
        let dialog = $("#" + divName).dialog({
            resizable: false,
            title: "Results for Trial " + trial,
            height: "auto",
            width: 620,
            modal: true,
            close: clearNextUpTimeout
        });
        let buttons = [
            {
                text: "Clear scoreboard",
                class: "resultButton clearSB",
                click: function () {
                    clearScoreboard();
                }
            },{
                text: "Event Settings",
                class: "resultButton eventSettingBTN",
                click: function () {
                    updateEventGroupFieldDialog(trialDistanceResults);
                    $(this).dialog("close");
                }
            }, {
                text: "Complete",
                class: "resultButton",
                click: function () {
                    completeEvent(divName);
                }
            }, {
                text: "Ok",
                class: "resultButton",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ];

        dialog.dialog("option", "buttons", buttons);
        positionDialog(dialog);
        setValidOptions(false);
        moveToNextResult(prefix, trial, 0, false);

        setTimeout(function () {
            displayEGNotes();
        }, 2000);
    }

    function completeEvent(divName = '') {
        let eg = getCurrentEventGroup();
        closeDialog = divName;
        e4sConfirm("This will mark results complete. Are you sure ?", "Complete " + eg.event + " ?", markEventComplete);
    }

    function markEventComplete() {
        if (closeDialog !== '') {
            $('#' + closeDialog).dialog("close");
            closeDialog = '';
        }

        let compId = getCompetition().id;
        let eg = getCurrentEventGroup();
        let egId = getEventGroupId(eg);
        let payload = {};
        let url = "https://<?php echo E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH ?>/closeresult/" + compId + "/" + egId;
        $.post(url, payload);
    }

    function sendInfoToScoreboard(prefix, trial, row) {
        let fieldObj = $("#" + prefix + row);
        sendNextUp(fieldObj, 0);
        if (fieldObj.val() !== "") {
            writeResult(prefix, trial, row);
        }
    }

    function writeResult(prefix, trial, row) {
        let fieldObj = $("#" + prefix + row);
        let val = fieldObj.val().trim();
        markUIActive(prefix, row, val);

        if (!isValValidOption(val) && val !== "") {
            val = getFloatDecimals(fieldObj.val(), 2);
            if (isNaN(val)) {
                val = 0;
            }
        }
        fieldObj.val(val);
        writeResultsDirect(fieldObj, trial, row, val);
    }

    function writeResultsDirect(fieldObj, trial, row, val) {
        let eventNo = getSelectedEventNo();
        let athleteId = parseInt(fieldObj.attr("athleteId"));
        let results = {
            "<?php echo E4S_RESULT_PARAMS ?>": {
                "row": row,
                "athleteId": athleteId,
                "eventNo": eventNo
            }
        };
        results["t" + trial] = val + offLine.activeSuffix;
        distanceCard.displayAthleteResults(athleteId, results);
        distanceCard.displayPositions();
        results["t" + trial] = val;
        writeAthleteResultsToDB(results, false, true);
        showScoreboardIcon(row);
    }

    function callResultDialog(row, athleteId, athletename, eventNo) {
        getResultsForAthleteForEdit(row);
        let eventGroup = getEventGroup(eventNo);
        if (isEventGroupField(eventGroup)) {
            if (eventGroup.uomtype === "D") {
                distanceResultsEntry(row, athleteId, athletename, eventGroup);
            }
            if (eventGroup.uomtype === "H") {
                heightResultsEntry();
            }
        }
    }

    function heightResultsEntry(row, athleteId, athletename, eventGroup) {
        let dialogObj = $("#dialogHeightResult");
        dialogObj.dialog({
            resizable: false,
            title: "Results for " + athletename,
            height: "auto",
            width: 500,
            modal: true
        });
        dialogObj.dialog("option", "buttons", {
            Cancel: function () {
                $(this).dialog("close");
            },
            Update: function () {
                writeResultsForAthlete(athleteId, getEventNoFromEventGroup(eventGroup), row);
                $(this).dialog("close");
            }
        });
    }

    function distanceResultsEntry(row, athleteId, athletename, eventGroup) {
        $("[e4sAthleteResult=true]").attr("e4ssub", row);

        let dialogObj = $("#dialogDistanceResult");

        dialogObj.dialog({
            resizable: true,
            title: "Results for " + athletename,
            height: "auto",
            width: 350,
            modal: true
        });
        dialogObj.dialog("option", "buttons", {
            Cancel: function () {
                $(this).dialog("close");
            },
            Update: function () {
                writeResultsForAthlete(athleteId, getEventNoFromEventGroup(eventGroup), row);
                $(this).dialog("close");
            }
        });
        setValidOptions(true);
    }

    function writeResultsForAthlete(athleteId, eventNo, row) {
        let t1 = $("#t1").val();
        let t2 = $("#t2").val();
        let t3 = $("#t3").val();
        let t4 = $("#t4").val();
        let t5 = $("#t5").val();
        let t6 = $("#t6").val();

        let results = {
            "t1": t1 + offLine.activeSuffix,
            "t2": t2 + offLine.activeSuffix,
            "t3": t3 + offLine.activeSuffix,
            "t4": t4 + offLine.activeSuffix,
            "t5": t5 + offLine.activeSuffix,
            "t6": t6 + offLine.activeSuffix,
            "<?php echo E4S_RESULT_PARAMS ?>": {
                "row": row,
                "athleteId": athleteId,
                "eventNo": eventNo
            }
        };
        distanceCard.displayAthleteResults(athleteId, results);
        distanceCard.displayPositions();
        writeAthleteResultsToDB(results, true, false);
    }

    function cardResults(trialOrHeight) {
        let eventGroup = getCurrentEventGroup();
        switch (eventGroup.type) {
            case '<?php echo E4S_EVENT_FIELD ?>':
                switch (eventGroup.uomtype) {
                    case '<?php echo E4S_UOM_DISTANCE ?>':
                        trialDistanceResults(trialOrHeight);
                        break;
                    case '<?php echo E4S_UOM_HEIGHT ?>':
                        heightCard.displayDialog();
                        break;
                }
                break;
            case '<?php echo E4S_EVENT_TRACK  ?>':
                break;
        }
    }

    function writeAthleteResultsToDB(results, clean, forScoreboard, flushing) {
        let eventGroup = getCurrentEventGroup();
        let payload = {
            "compid": getCompetition().id,
            "clean": clean,
            "scoreboard": forScoreboard,
            "egId": getEventGroupId(eventGroup)
        };
        if (!flushing) {
            payload.results = [results];
            payload = offLine.storeResult(payload);
        } else {
            payload.results = results;
        }
        let simplePositions = {};
        if (distanceCard.positionArray.length > 0) {
            for (let p in distanceCard.positionArray) {
                let pos = distanceCard.positionArray[p];
                simplePositions[pos.athleteId] = pos.position;
            }
        }

        payload.positions = simplePositions;
        let url = "<?php echo E4S_BASE_URL ?>postresult";
        $.ajax({
            type: "POST",
            url: url,
            data: payload,
            dataType: "json"
        });
    }

    function processTrackChange(entry) {
        let html = '<div id="trackChange">';
        let entityObj = null;
        let isTeam = false;
        if (entry.athleteId) {
            entityObj = getAthletes()[entry.athleteId];
        } else {
            isTeam = true;
            entityObj = getTeamEntryById(entry.teamid);
        }
        let heatNo = getEntryRaceNo(entry);
        let laneNo = getEntryLaneNo(entry);
        let maxHeatNo = getMaxHeatNo();
        if (cardShowsCheckedIn()) {
            maxHeatNo = getMaxHeatNoCheckedIn();
        }

        // save to global
        outputReportData.curEntry = entry;

        html += '<table>';
        html += '<tr>';
        html += '<td><span class="trackChangeCurrentLabel">Bib# :</span> ';
        let bibNo = getBibNoToUse(entityObj, entry);

        html += '<span class="trackChangeData">' + bibNo + '</span>';
        html += '</td>';
        html += '<td><span class="trackChangeCurrentLabel">Race# :</span> ';
        html += '<span id="fromHeatNo" class="trackChangeData">' + heatNo + '</span>';
        html += '</td>';
        html += '<td colspan=2><span class="trackChangeCurrentLabel">Lane# :</span> ';
        html += '<span id="fromLaneNo" class="trackChangeData">' + laneNo + '</span>';
        html += '</td>';
        html += "</tr>";
        html += '<tr>';
        html += '<td colspan="4">';
        if (isTeam) {
            html += '<span class="trackChangeAthleteName">' + entityObj.teamname + '</span>';
        } else {
            html += '<span class="trackChangeAthleteName">' + getAthleteNameFromAthlete(entityObj) + '</span>';
        }
        html += '<p>Present: <input type="checkbox" id="lanePresent"';
        if (isEntryPresent(entry)) {
            html += ' checked ';
        }
        html += '/>';
        html += '</p>';
        html += '</td>';
        html += '</tr>';

        html += '<tr>';
        html += '<td colspan=4>';
        html += '<div class="trackChangeSwitchLabel trackChangeCurrentLabel">Move/Switch with</div>';
        html += '<div class="trackChangeSwitchHelp">Enter below or click data on card</div>';
        html += '</td>';
        html += "</tr>";

        html += '<tr>';
        html += '<td><span class="trackChangeCurrentLabel">Bib# :</span><input style="width:50px;" id="switchedInBibNo" onkeyup="switchAthleteBibNo();" value=""></td>';
        html += '<td><span class="trackChangeCurrentLabel">Race# :</span><input type="number" style="width:50px;" id="newHeatNo" onchange="validateHeatCountEntry(this,' + maxHeatNo + ');" value="' + heatNo + '"></td>';
        html += '<td colspan=2><span class="trackChangeCurrentLabel">Lane# :</span><input type="number" style="width:50px;" id="newLaneNo" onchange="validateLaneCountEntry(this);" value="' + laneNo + '"></td>';
        html += '</tr>';

        html += '<tr>';
        html += '<td colspan=4><span class="trackChangeSwitchedAthleteName" id="switchedInAthlete">&nbsp;</span></td>';
        html += '</tr>';

        html += '</table>';
        html += '</div>';
        let dialog = $(html).dialog({
            title: 'Track Update',
            open: function (event, ui) {
                dragDropCard.enableBlankLanes();
            },
            close: function (event, ui) {
                togglePulse("", false);
                $("#trackChange ").remove();
                dragDropCard.disableBlankLanes();
                dragDrop.init();
            }
        });


        dialog.dialog("option", "buttons",
            {
                'Save Change': function () {
                    let presence = $("#lanePresent ").is(':checked');
                    let newHeatNo = parseInt($("#newHeatNo").val());
                    let newLaneNo = parseInt($("#newLaneNo").val());
                    if (newHeatNo < 1 || newLaneNo < 1) {
                        return;
                    }
                    let process;
                    // Have Heat or Lane Changed
                    if (newHeatNo !== heatNo || newLaneNo !== laneNo) {
                        process = true;
                    } else {
                        // If unseed, allow confirmation
                        process = isHeatLaneUnSeeded(heatNo, laneNo);
                    }
                    if (process) {
                        // Moved lanes
                        processMoveLanes(entry, newHeatNo, newLaneNo, presence);
                    } else {
                        if (isEntryPresent(entry) !== presence) {
                            // presence changed Only
                            processSwitchPresent(outputReportData.curEntry);
                        }
                    }

                    $(this).dialog('close');
                },
                'Cancel': function () {
                    $(this).dialog('close');
                }
            }
        );
        positionDialog(dialog);
    }

    function processTrackChangeFromList(athleteId) {
        let entries = getEntriesInOrderForEvent(null);
        for (let e in entries) {
            let entry = entries[e];
            if (entry.athleteId === athleteId) {
                setRefreshSeeding(true);
                return processTrackChange(entry);
            }
        }
        e4sAlert('Athlete not found');
    }

    function addEntryToSeedingList(entry, eventGroup) {
        let entity;
        let entityId;
        let entityName;
        let isTeam = false;
        let isTrack = isEventGroupTrack(eventGroup);
        if (entry.teamid) {
            entityId = entry.teamid;
            entity = getTeamEntryById(entityId);
            entityName = entity.teamname;
            isTeam = true;
        } else {
            entityId = entry.athleteId;
            entity = getAthleteById(entityId);
            entityName = getAthleteNameFromAthlete(entity);
        }
        let html = "";
        html += '<li class="ui-state-default" key=' + entry.eventGroupId + '-' + entityId + '>';
        html += '<table><tr>';

        let bibNo = getBibNoToUse(entity, entry);

        let clickEvent = "";
        if ( isTrack ){
            clickEvent = "processTrackChangeFromList(" + entityId + ");";
        }
        html += '<td style="width:50px;" onclick="' + clickEvent + '">';
        html += '<span class="seedBibValue">' + bibNo + '</span>';

        if (!isEntryCheckedIn(entry)) {
            html += '<span class="' + e4sGlobal.class.bibNotCheckedIn + '"></span>';
        }

        html += '</td><td style="width:400px;"><span class="seedAthleteName">';
        html += entityName;
        html += '<span class="fieldSeedClubname"> ' + entity.clubname + '</span>';
        if (!isTeam) {
            html += ' (<input onblur="updatePb(this);" class="editPb" id="pb_' + getEntryId(entry) + '" value="' + getDecimals(entry.pb, 2) + '">)';
        }
        html += '</span></td>';
        if (isEventGroupTrack()) {
            if (isEntryPresent(entry)) {
                html += '<td><span class="ui-icon ui-icon-check seedPresentIcon"></span></td>';
            } else {
                html += '<td><span class="ui-icon ui-icon-close ui-icon-redIcon seedPresentIcon"></span></td>';
            }
            html += '<td style="width:70px;"><span class="seedHeatLabel">race:</span><span class="seedHeatValue">' + getEntryRaceNo(entry) + '</span></td>';
            html += '<td style="width:65px;"><span class="seedLaneLabel">lane:</span><span class="seedLaneValue">' + getEntryLaneNo(entry) + '</span></td>';
        }
        html += '</tr></table>';
        html += '</li>';
        return html;
    }

    function getRefreshSeeding() {
        return e4sGlobal.refreshSeeding;
    }

    function setRefreshSeeding(value) {
        e4sGlobal.refreshSeeding = value;
    }

    function processMoveLanes(entry, newHeatNo, newLaneNo, presence) {
        if (isEntryPresent(entry) !== presence) {
            setEntryPresent(entry, presence);
        }
        updateDbWithLaneInfo(entry, newHeatNo, newLaneNo);
    }

    function updateDbWithLaneInfo(entry, newHeatNo, newLaneNo) {
        let useHeatNo = getEntryRaceNo(entry);
        let useLaneNo = getEntryLaneNo(entry);
        let eg = getCurrentEventGroup();
        let entries = getEntriesForEg(eg);
        let toEntryId = 0;
        let fromEntryId = getEntryId(entry);
        if (entry.teamid) {
            fromEntryId = entry.teamid;
        }
        for (let e in entries) {
            let toEntry = entries[e];

            if (getEntryLaneNo(toEntry) === newLaneNo && getEntryRaceNo(toEntry) === newHeatNo) {
                toEntryId = getEntryId(toEntry);
                if (toEntry.teamid) {
                    toEntryId = toEntry.teamid;
                }
            }

        }
        let athleteId = entry.athleteId ? entry.athleteId : 0;
        let teamId = entry.teamid ? entry.teamid : 0;
        let unseeded = !isEntrySeeded(entry);
        let payload = {
            fromentryid: fromEntryId,
            toentryid: toEntryId,
            egid: entry.eventGroupId,
            athleteid: athleteId,
            teamid: teamId,
            switchedathleteid: getSwitchedEntityId(),
            compid: getCompetition().id,
            present: isEntryPresent(entry),
            checkedin: cardShowsCheckedIn(),
            fromheatno: useHeatNo,
            fromlaneno: useLaneNo,
            toheatno: newHeatNo,
            tolaneno: newLaneNo,
            unseeded: unseeded
        };
        let url = "https://<?php echo E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH ?>/track/move";
        $.post(url, payload);
    }

    function markPresent(prefix, row, present, entryId) {
        entryId = parseInt(entryId);
        let entry = getEntryByID(entryId);
        if (entry === null) {
            e4sAlert("Failed to find entry!!!");
            return;
        }
        setEntryPresent(entry, present);
        // update database
        updateDBPresent(entry);
        markUIPresent(prefix, row, present);
        entry.prefix = prefix;
    }

    function updateDBPresent(entry) {
        // use entry.present in case not checked in but somehow got to field
        let entryId = getEntryId(entry);
        let team = "";
        if (entry.teamid) {
            entryId = entry.teamid;
            team = "team/";
        }
        let url = "https://<?php echo E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH ?>/entry/" + team + "present/" + entry.present + "/" + entryId + "?compid=" + getCompetition().id;
        $.post(url);
    }

    function displayEntriesResetMenuItem() {
        let curEg = getCurrentEventGroup();
        let srcEg = getEntriesFromEG(curEg);
        let hide = true;
        if (srcEg !== null) {
            if (typeof srcEg.entries === "undefined") {
                return;
            }
            if (srcEg.entries.length === 0) {
                curEg.entries = [];
                return;
            }

            // Use less than so its possible to add to this child indiv event of a multi event
            if (typeof curEg.entries === "undefined" || curEg.entries.length !== srcEg.entries.length) {
                hide = false;
            }
        }
        if (hide) {
            $("#resetEntriesActionDiv").hide();
        } else {
            $("#resetEntriesActionDiv").show();
        }
    }

    function displaySeedingMenuItem() {
        let curEg = getCurrentEventGroup();
        if (curEg.type === "<?php echo E4S_EVENT_MULTI ?>") {
            $("#seedingActionDiv").hide();
        } else {
            $("#seedingActionDiv").show();
        }
    }

    function showResultsEntryMenuItem() {
        displayMenuAction("resultsEntryAction", isEventGroupField());
    }

    function setSeedingMonitorMenuItem() {
        if (getSeedingMonitor()) {
            $("#seedingMonitorActionText").text("Monitor Status is On");
            setSeedingCompleteButton(true);
        } else {
            $("#seedingMonitorActionText").text("Monitor Status is Off");
            setSeedingCompleteButton(false);
        }
    }

    function setSeedingCompleteButton(status) {
        if (status && !isEventGroupSeeded(getCurrentEventGroup())) {
            $("#markSeedingComplete").show();
        } else {
            $("#markSeedingComplete").hide();
        }

    }

    function checkSeedingNotes() {
        if (e4sGlobal.seedingInterval) {
            clearInterval(e4sGlobal.seedingInterval);
        }
        if (getSeedingMonitor() && isCheckInInUse()) {
            e4sGlobal.seedingInterval = setInterval(function () {
                displaySeedingNotes(monitorCheckIns());
            }, 5000);
        } else {
            delete e4sGlobal.seedingInterval;
            displaySeedingNotes("");
        }
    }

    function monitorCheckIns() {
        let nowMilliSeconds = new Date().valueOf();
        let retval = "";
        let egArr = getEventGroups();
        for (let eg in egArr) {
            let eventGroup = egArr[eg];
            if (getEventGroupType(eventGroup) !== "<?php echo E4S_EVENT_FIELD ?>") {
                let entries = getEntriesForEg(eventGroup);
                if (!isEventGroupSeeded(eventGroup) && eventGroup.checkinClosesDT && entries.length > 0) {
                    let checkinStatus = getCheckInStatus(eventGroup);
                    let secondsToClose = parseInt((eventGroup.checkinClosesDT.valueOf() - nowMilliSeconds) / 1000);
                    switch (checkinStatus) {
                        case <?php echo E4S_CHECKIN_OPEN ?>:
                            if (secondsToClose < 360) {
                                let timeToClose = "";
                                if (secondsToClose > 60) {
                                    let minsToClose = Math.floor(secondsToClose / 60);
                                    timeToClose = minsToClose + " mins ";
                                    secondsToClose = secondsToClose - (minsToClose * 60);
                                }
                                timeToClose += secondsToClose + " secs";
                                retval += getEventGroupTitle(eventGroup) + " closes in " + timeToClose + ". ";
                            }
                            break;
                        case <?php echo E4S_CHECKIN_CLOSED ?>:
                            if (secondsToClose > -360) {
                                retval += getEventGroupTitle(eventGroup) + " awaiting seeding. ";
                            }
                            break;
                    }
                }
                // if event group is a final, check results for the heat
                if (getHeatEg(eventGroup)) {

                }
            }
        }
        return retval;
    }

    function getHeatEg(checkEg) {
        let checkEgId = getEventGroupId(checkEg);
        let eventGroups = getEventGroups();
        for (let e in eventGroups) {
            let eg = eventGroups[e];
            let finalId = getEgQualifyToId(eg);
            if (finalId === checkEgId) {
                return eg;
            }
        }
        return 0;
    }

    function getEGFinalId(checkEg) {
        return getEgQualifyToId(checkEg);
    }

    function getEgQualifyToId(eg) {
        let seed = getEGSeed(eg);
        if (seed === null) {
            return 0;
        }
        return seed.qualifyToEg.id;
    }

    function getEGSeed(eg) {
        if (eg.options.seed) {
            return eg.options.seed;
        }
        return null;
    }

    function getFieldCardSettings() {
        debugger;
    }

    function monitorCheckInsDialog() {
        let divName = "seedingMonitorDiv";
        let divHTML = "<div id='" + divName + "'>";
        divHTML += "Do you want to monitor when event check-in closes ?<br><br>";
        divHTML += 'If you do, messages will appear here 3 minutes before an event is due to close.<br><br>';
        divHTML += 'The message will remain until the event has seedings confirmed.';
        divHTML += "</div>";
        $("#" + divName).remove();
        $("#dialogSeedingMonitor").html(divHTML);

        let dialog = $("#" + divName).dialog({
            resizable: false,
            title: "Monitor check-in closures",
            height: "auto",
            width: 600,
            modal: true
        });
        let buttons = [
            {
                text: "Yes",
                class: "resultButton",
                click: function () {
                    initiateSeedingMonitor();
                    $(this).dialog("close");
                }
            },
            {
                text: "No",
                class: "resultButton",
                click: function () {
                    stopSeedingMonitor();
                    $(this).dialog("close");
                }
            }
        ];
        dialog.dialog("option", "buttons", buttons);
        positionDialog(dialog);
    }

    function initiateSeedingMonitor() {
        setSeedingMonitor(true);
        cardChange();
    }

    function stopSeedingMonitor() {
        setSeedingMonitor(false);
        cardChange();
    }

    function setSeedingMonitor(state) {
        createCookie("<?php echo E4S_SEEDING_MONITOR_COOKIE ?>", state);
    }

    function getSeedingMonitor() {
        let state = readCookie("<?php echo E4S_SEEDING_MONITOR_COOKIE ?>");
        return state === "true";
    }

    function setUseOfScoreboards(state) {
        createCookie("<?php echo E4S_SCOREBOARDS_COOKIE ?>", state);
    }

    function getUseOfScoreboards() {
        let state = readCookie("<?php echo E4S_SCOREBOARDS_COOKIE ?>");
        return state === "true";
    }

    function lynxAthlete(addCLub) {
        let output = "";
        let sep = ",";
        let newline = "\r\n";
        let athletes = getAthletes();
        let sortedAthletes = [];
        for (let a in athletes) {
            sortedAthletes.push(athletes[a]);
        }
        sortedAthletes.sort(function (a, b) {
            if (a.bibno < b.bibno) {
                return -1;
            }
            return 1;
        });
        for (let x in sortedAthletes) {
            let athlete = sortedAthletes[x];
            output += athlete.bibno + sep;
            let surname = athlete.surname;

            if (addCLub) {
                surname += " (" + athlete.clubname.substring(0, 4) + ")";
            }
            output += surname + sep;
            output += athlete.firstname + sep;
            output += athlete.clubname;
            output += newline;
        }
        return output;
    }

    function timeTronicsAthlete(addClub) {
        let output = "";
        let tab = "\t";
        let newline = "\r\n";
        let athletes = getAthletes();
        for (let x in athletes) {
            let athlete = athletes[x];
            let athleteName = getAthleteNameFromAthlete(athlete);
            if (addClub) {
                athleteName += " (" + athlete.clubname.substring(0, 4) + ")";
            }
            output += athlete.bibno + tab;
            output += " " + tab;
            output += athleteName + tab;
            output += athlete.ageshortgroup + tab;
            output += athlete.gender + tab;
            output += "" + tab; // Area
            output += formatDateForTimetronics(athlete.dob) + tab;
            output += "0" + tab;
            output += athlete.clubname;
            output += newline;
        }
        return output;
    }

    function lynxEventsSch() {
        let sep = ",";
        let newline = "\r\n";
        let entries = getEntries();
        let output = "";
        let sortArr = {};
        let lasteventno = -1;
        let lastheatNo = -1;
        for (let x in entries) {
            let entry = entries[x];
            let heatNo = entry.heatInfo.heatNo;
            let eventno = getEventNoFromEntry(entry);
            if (entry.tf === '<?php  echo E4S_EVENT_TRACK ?>') {
                if (lasteventno + heatNo !== 0) {
                    if (lasteventno !== eventno || lastheatNo !== heatNo) {
                        sortArr[(eventno * 1000) + heatNo] = entry;
                    }
                }
            }
            lasteventno = eventno;
            lastheatNo = heatNo;
        }
        for (let x in sortArr) {
            let e = sortArr[x];
            output += getEventNoFromEntry(e) + sep;
            output += "1" + sep;
            output += e.heatInfo.heatNo + newline;
        }

        return output
    }

    function lynxEvents(options) {
        let choices = options.choices;
        let useCheckedIn = options.checkedIn;
        let sep = ",";
        let newline = "\r\n";
        let entries = getEntries();
        let teams = getTeams();
        let output = "";
        let warnMsg = "";
        let lastEventNo = 0;
        let lastHeatNo = 0;
        let trackEntries = [];
        for (let x in entries) {
            let entry = entries[x];
            if (isEventNoInSelected(choices, getEventNoFromEntry(entry))) {
                if (entry.tf === '<?php  echo E4S_EVENT_TRACK ?>') {
                    if (entry.present) {
                        entry.heatInfo.useHeatNo = entry.heatInfo.heatNo;
                        entry.heatInfo.useLaneNo = entry.heatInfo.laneNo;
                        if (useCheckedIn) {
                            entry.heatInfo.useHeatNo = entry.heatInfo.heatNoCheckedIn;
                            entry.heatInfo.useLaneNo = entry.heatInfo.laneNoCheckedIn;
                        }
                        entry.isTeam = false;
                        trackEntries.push(entry);
                    }
                }
            }
        }

        let clubComps = getClubComps();
        for (let x in teams) {
            let entry = teams[x];
            if (entry.event.toLowerCase().indexOf("reserve") === -1) {
                if (isEventNoInSelected(choices, getEventNoFromEntry(entry))) {
                    if (entry.present) {
                        entry.position = (Math.random() * entry.teamid) / Math.random();
                        entry.isTeam = true;
                        let teamName = entry.teamname;
                        teamName = teamName.replace(/team /ig, "");
                        for (let c in clubComps) {

                            if (c.toLowerCase() === teamName.toLowerCase()) {
                                teamName = c;
                                break;
                            }
                        }
                        if (typeof clubComps[teamName] !== "undefined") {
                            entry.teamBibNo = clubComps[teamName].bibNos[0];
                        } else {
                            entry.teamBibNo = entry.bibNo;
                        }
                        entry.heatInfo.useHeatNo = entry.heatInfo.heatNo;
                        entry.heatInfo.useLaneNo = entry.heatInfo.laneNo;
                        if (useCheckedIn) {
                            entry.heatInfo.useHeatNo = entry.heatInfo.heatNoCheckedIn;
                            entry.heatInfo.useLaneNo = entry.heatInfo.laneNoCheckedIn;
                        }
                        trackEntries.push(entry);
                    }
                }
            }
        }

        trackEntries.sort(function (a, b) {
            let aEventNo = getEventNoFromEntry(a);
            let bEventNo = getEventNoFromEntry(b);
            if (aEventNo < bEventNo) {
                return -1;
            }
            if (aEventNo > bEventNo) {
                return 1;
            }
            if (a.heatInfo.useHeatNo < b.heatInfo.useHeatNo) {
                return -1;
            }
            if (a.heatInfo.useHeatNo > b.heatInfo.useHeatNo) {
                return 1;
            }
            if (a.heatInfo.useLaneNo < b.heatInfo.useLaneNo) {
                return -1;
            }
            return 1;
        });
        let teamLaneNo = 1;
        for (let x in trackEntries) {
            let entry = trackEntries[x];
            let heatNo = 1;
            let laneNo = 0;
            let eventNo = getEventNoFromEntry(entry);

            heatNo = entry.heatInfo.useHeatNo;
            laneNo = entry.heatInfo.useLaneNo;

            if (heatNo !== 0 && laneNo !== 0) {
                if (lastEventNo !== eventNo || lastHeatNo !== heatNo) {
                    output += eventNo + sep;
                    output += "1" + sep;
                    output += heatNo + sep;
                    let eg = getEventGroup(eventNo);
                    output += getEventGroupTitle(eg) + " Race " + heatNo + sep;
                    output += sep + sep + sep + sep + sep + 0;
                    output += newline;
                }
                let bibNo = "";
                if (entry.isTeam) {
                    bibNo = entry.teamBibNo;
                    if (("" + bibNo).length > 0) {
                        output += sep;
                        output += bibNo + sep;
                        output += laneNo + sep;
                        let teamName = entry.teamname;
                        teamName = teamName.replace(/team /ig, "");
                        output += teamName.toUpperCase() + sep;
                        output += 'Team' + sep;
                        output += teamName + sep;
                        output += newline;
                    }
                } else {
                    let athlete = getAthleteById(entry.athleteId);
                    bibNo = getBibNoToUse(athlete, entry);
                    if (("" + bibNo).length > 0) {
                        output += sep;
                        output += bibNo + sep;
                        output += laneNo + sep;
                        output += athlete.surname.trim().toUpperCase() + sep;
                        output += toTitleCase(athlete.firstname.trim()) + sep;
                        output += athlete.clubname + sep;
                        output += newline;
                    }
                }

                if (("" + bibNo).length === 0) {
                    warnMsg = "Please generate Bib numbers.";
                    break;
                }
            }

            lastEventNo = eventNo;
            lastHeatNo = heatNo;
        }
        if (warnMsg) {
            e4sAlert(warnMsg);
            return "";
        } else {
            return output;
        }
    }

    function includeEntryOnDate(entry, checkDate) {
        if (checkDate === "") {
            return true;
        }
        return entry.startdate.split(" ")[0] === checkDate;
    }

    function timeTronicsEvents(options) {
        // choices, useCheckedIn, selectedDate
        let choices = options.choices;
        let selectedDate = options.date;
        let useCheckedIn = options.checkedIn;
        let sep = "\t";
        let newline = "\r\n";
        let entries = getEntries();
        let output = "";
        let warnMsg = "";

        for (let x in entries) {
            let entry = entries[x];
            let eventNo = getEventNoFromEntry(entry);
            if (entry.tf === '<?php  echo E4S_EVENT_TRACK ?>') {
                let heatNo = entry.heatInfo.heatNo;
                let laneNo = entry.heatInfo.laneNo;
                if (useCheckedIn) {
                    heatNo = entry.heatInfo.heatNoCheckedIn;
                    laneNo = entry.heatInfo.laneNoCheckedIn;
                }
                if (heatNo !== 0 && laneNo !== 0) {
                    if (includeEntryOnDate(entry, selectedDate) && isEventNoInSelected(choices, eventNo)) {
                        let athlete = getAthleteById(entry.athleteId);
                        if (athlete.bibno !== 0) {
                            output += athlete.bibno + sep;
                            output += eventNo + sep;
                            output += heatNo + sep;
                            output += laneNo + newline;
                        } else {
                            warnMsg = "Please generate Bib numbers.";
                        }
                    }
                }
            }
        }
        if (warnMsg) {
            e4sAlert(warnMsg);
            return "";
        }
        return output;
    }

    function downloadIndividual(pfFormat) {
        updatePhotofinish(pfFormat);
        let data = photoFinishExport.getData(pfFormat);

        if (data === "") {
            e4sAlert("No data found to export", "Warning");
        } else {
            let fileExt = "<?php echo E4S_PF_TT_IND_EXT ?>";
            if (pfFormat === "<?php echo E4S_PF_LYNX ?>") {
                fileExt = "<?php echo E4S_PF_LYNX_IND_EXT ?>";
            }
            processBlob(data, fileExt, "<?php echo E4S_PF_EVENTS ?>");
        }
    }

    function sendIndividualDirectToPF() {
        photoFinishExport.disablePFDirectButton();
        let pfFormat = $("#selectPF").val();
        if (pfFormat === '<?php echo E4S_PF_TT ?>') {
            exportTTIndividualEvents();
        } else {
            photoFinishExport.sendToSocket();
        }
    }

    function sendPFFileToSocket(fileName, body, system = null) {
        let url = "<?php echo E4S_BASE_URL ?>photofinish";
        let sendPayload = {};
        if (system === null) {
            if (photoFinishExport.uiDisplayed()) {
                system = photoFinishExport.getPFSystemFromUI();
            } else {
                system = photoFinishExport.readOptions().system;
            }
        }
        sendPayload.compid = getCompetition().id;
        sendPayload.filename = fileName;
        sendPayload.system = system;
        sendPayload.body = body;
        $.post(url, sendPayload);
        photoFinishExport.lastExportTime = new Date();
        displayEGNotes();
    }

    function exportTTIndividualEvents() {
        e4sGlobal.selectedEvents = [];
        $("[name=eventToPrint]").each(function (index) {
            let element = $(this);
            if (element[0].checked) {
                e4sGlobal.selectedEvents.push(parseInt(element.val()));
            }
        });
        let entries = getEntries();
        let entriesByHeat = [];
        for (let x in entries) {
            let entry = entries[x];
            if (e4sGlobal.selectedEvents.includes(entry.eventno)) {
                if (typeof entriesByHeat[entry.eventno] === "undefined") {
                    entriesByHeat[entry.eventno] = [];
                }
                let heatNo = getEntryRaceNo(entry);
                if (typeof entriesByHeat[entry.eventno][heatNo] === "undefined") {
                    entriesByHeat[entry.eventno][heatNo] = [];
                }
                entriesByHeat[entry.eventno][heatNo].push(entry);
            }
        }
        let tab = '\t';
        let nl = '\r\n';
        for (let e in entriesByHeat) {
            let eventStr = "000" + e;
            eventStr = "E" + eventStr.substr(-3, 3);
            for (let h in entriesByHeat[e]) {
                let heatStr = "0" + h;
                let fileName = eventStr + "H" + heatStr.substr(-2, 2);
                let body = '';
                for (let l in entriesByHeat[e][h]) {
                    let entry = entriesByHeat[e][h][l];
                    if (body === '') {
                        fileName += ',' + entry.eventGroup.replace(/ /g, '') + '.' + '<?php echo E4S_PF_TT_IND_DIRECT_EXT ?>';
                        body = '# # # # # # # # ' + nl;
                        body += '# Competition ' + getCompetition().id + ':' + getCompetition().name + nl;
                        body += '# T' + e + ' ' + entry.eventGroup + nl;
                        body += '# # # # # # # # ' + nl;
                    }
                    let athlete = getAthleteById(entry.athleteId);
                    body += getBibNoToUse(athlete, entry) + tab;
                    body += (parseInt(l) + 1) + tab;
                    body += getAthleteNameFromAthlete(athlete) + tab;
                    body += athlete.clubname + nl;
                }
                if (body !== '') {
                    // console.log(fileName);
                    sendPFFileToSocket(fileName, body);
                }
            }
        }
    }

    let photoFinishExport = {
        lastExport: null,
        lastExportTime: null,
        autoEnabled: null,
        pfDirectBtn: null,
        storageKey: "pfAutoExport",
        autoTimer: null,
        dialogName: "e4sExportDialog",
        compId: 0,

        init: function () {
            photoFinishExport.compId = getCompetition().id;
            photoFinishExport.autoEnabled = photoFinishExport.readEnable();
            photoFinishExport.startExit();
        },
        getUIOptions: function () {
            let autoOptions = {};
            autoOptions.checkedIn = false;
            autoOptions.choices = [];
            autoOptions.date = "";
            autoOptions.system = parseInt(photoFinishExport.getPFSystemFromUI());
            let selection = $("#checkInExport").val();
            if (selection === "1") {
                autoOptions.checkedIn = true;
            }
            $("[name=eventToPrint]").each(function (index) {
                let element = $(this);
                if (element[0].checked) {
                    autoOptions.choices.push(parseInt(element.val()));
                }
            });
            autoOptions.date = $("#exportDateEvent option:selected").val();
            return autoOptions;
        },
        readEnable: function () {
            let enableCookie = readCookie(photoFinishExport.storageKey);
            return enableCookie === "" + photoFinishExport.compId ? true : false;
        },
        writeEnable: function () {
            createCookie(photoFinishExport.storageKey, this.autoEnabled ? photoFinishExport.compId : '');
        },
        uiDisplayed: function () {
            if ($("#" + photoFinishExport.dialogName).length > 0) {
                return true;
            }
            return false;
        },
        startAutoExport: function () {
            if (photoFinishExport.uiDisplayed()) {
                // dialog is shown
                this.writeOptions();
            }
            this.autoTimer = setInterval(photoFinishExport.checkLatest, 5000);
        },
        exitAutoExport: function () {
            // clear the autoTimer
            clearInterval(this.autoTimer);
        },
        retrieveOptions: function () {
            if (this.autoEnabled) {
                return this.readOptions();
            } else {
                return this.getUIOptions();
            }
        },
        readOptions: function () {
            let options = JSON.parse(localStorage.getItem(this.storageKey));
            return options;
        },
        writeOptions: function () {
            // write the options in the UI to storage
            let uiOptions = this.getUIOptions();
            localStorage.setItem(this.storageKey, JSON.stringify(uiOptions));
        },
        getLatestExport: function () {
            let options = photoFinishExport.retrieveOptions();
            return lynxEvents(options);
        },
        checkLatest: function () {
            let latestData = photoFinishExport.getLatestExport();
            if (photoFinishExport.lastExport !== latestData) {
                // send latest
                photoFinishExport.sendToSocket(latestData);
            }
            photoFinishExport.lastExport = latestData;
        },
        getPFSystemFromUI: function () {
            return $("#selectSystem").val();
        },
        sendToSocket: function (body = null) {
            if (body === null) {
                body = photoFinishExport.getLatestExport();
            }

            if (body !== "") {
                sendPFFileToSocket('lynx.evt', body);
            }
        },
        getData: function (pfFormat) {
            let options = this.retrieveOptions();
            if (pfFormat === "<?php echo E4S_PF_LYNX ?>") {
                blob = lynxEvents(options);
            } else {
                blob = timeTronicsEvents(options);
            }
            // if (isCheckInEnabled() && !sendCheckedInInfo) {
            if (isCheckInEnabled()) {
                e4sAlert("File generated for entries but Check-In is enabled. Please check", "Check-In Warning");
            }
            return blob;
        },
        disablePFDirectButton: function () {
            let btn = $("#pfDirect");
            btn.prop("disabled", true);
            btn.css("background", "var(--e4s-button--<?php echo $GLOBALS[ E4S_ENVIRONMENT ] ?>--primary__disabled-background)");
            this.pfDirectBtn = setTimeout(this.enablePFDirectButton, 10000);
        },
        enablePFDirectButton: function (data = null) {
            let btn = $("#pfDirect");
            if (btn.length < 1) {
                return;
            }

            btn.prop("disabled", false);
            btn.css("background", "");
            clearTimeout(this.pfDirectBtn);
            if (data !== null) {
                if (typeof data.file !== "undefined") {
                    this.lastExportTime = new Date();
                    $("#exportMessage").text("Processed : " + data.file + "  (" + this.getLastExportTime() + ")");
                }
            }
        },
        getLastExportTime: function () {
            if (this.lastExportTime === null) {
                return "";
            }
            return formatDate(this.lastExportTime, 'HH:mm:ss');
        },
        toggleEnabled: function () {
            this.autoEnabled = !this.autoEnabled;
            this.writeEnable();
            this.autoExportBtn();
            this.init();
            if (this.autoEnabled) {
                e4sAlert("Auto Export enabled. The EVT file will be written automatically if changes are made to events that are selected. You may close this dialog.");
            }
        },
        startExit: function () {
            if (this.autoEnabled) {
                this.startAutoExport();
            } else {
                this.exitAutoExport();
            }
            displayEGNotes();
        },
        exportDialog: function () {
            let egs = getEventGroups();
            let optionsHTML = "";
            for (let eg in egs) {
                if (egs[eg].type === "<?php echo E4S_EVENT_TRACK ?>") {
                    if (egs[eg].options.cardDisplay) {
                        optionsHTML += '<option value="' + getEventNoFromEventGroup(egs[eg]) + '">' + getEventGroupTitle(egs[eg]) + '</option>';
                    }
                }
            }

            let html = "";
            html += '<div id="' + photoFinishExport.dialogName + '">';
            html += 'Please select the events you would like to export.';

            html += '<div id="e4sPrintSelection" class="e4sPrintSelectionDiv">';
            html += '<button type="button" class="ui-button ui-corner-all ui-widget e4sButtonSelectAll" onclick="e4s_printSelect(true);">';
            html += '<span class="ui-button-icon ui-icon ui-icon-circle-plus"></span>';
            html += '<span class="ui-button-icon-space"> </span>Select All</button>';

            html += '<button type="button" class="ui-button ui-corner-all ui-widget" onclick="e4s_printSelect(false);">';
            html += '<span class="ui-button-icon ui-icon ui-icon-circle-minus"></span>';
            html += '<span class="ui-button-icon-space"> </span>Clear Selection</button>';

            html += '<div id="exportOptionsSpan" class="exportOptions">';
            html += '<div id="exportFromEventSpan" class="exportFromEvent"><span id="exportFromLabel" class="exportFromLabel">From:</span><select id="exportFromEvent" onchange="photoFinishExport.exportSelector()">';
            html += optionsHTML;
            html += '</select></div>';
            html += '<div id="exportToEventSpan" class="exportToEvent"><span id="exportToLabel" class="exportToLabel">To:</span><select id="exportToEvent" onchange="photoFinishExport.exportSelector()">';
            html += optionsHTML;
            html += '</select></div>';

            let useCheckIn = isCheckInEnabled();
            html += '<div id="checkInExportSpan" ';
            if (!useCheckIn) {
                html += ' style="display:none" ';
            }
            html += ' class="checkInExport"><span id="exportToLabel" class="exportStatusLabel">Status: </span><select id="checkInExport">';
            html += '<option value="0"';
            if (!useCheckIn) {
                html += ' selected ';
            }
            html += '>Entries</option>';
            html += '<option value="1" ';
            if (useCheckIn) {
                html += ' selected ';
            }
            html += '>Checked In</option>';
            html += '</select></div>';

            html += '<div id="seededExportSpan" ';
            if (!useCheckIn) {
                html += ' style="display:none" ';
            }
            html += ' class="checkInExport"><span id="exportSeededLabel" class="exportStatusLabel">Seeded: </span><select onchange="photoFinishExport.exportSelector()" id="exportSeeded">';
            html += '<option value="0"';
            html += ' selected ';
            html += '>All</option>';
            html += '<option value="1" ';
            html += '>Only</option>';
            html += '</select></div>';

            let eventDatesArrayHTML = this.getUniqueDates(egs);
            let dateDisplay = ' style="display:none"; ';
            if (eventDatesArrayHTML.length > 2) {
                dateDisplay = '';
            }

            html += '<div id="exportDateSpan" class="exportDate" ' + dateDisplay + '>';
            html += '<span id="exportDateLabel" class="exportDateLabel">Date:</span>';
            html += '<select id="exportDateEvent" onchange="photoFinishExport.exportSelector()">';
            html += eventDatesArrayHTML.join();
            html += '</select></div>';

            html += '</div>';

            html += '<div id="e4sExportEvents" class="e4sPrintEventsDiv">';
            html += '</div>';
            html += '<div class="getPFClass" id="updatePFType">';
            let pfType = getPFType();
            let selected = '';
            html += '<label for="selectPF">Photo Finish System : </label>';
            html += '<select id="selectPF" onchange="updatePhotofinish( $(this).val() );">';
            if (pfType === '<?php echo E4S_PF_TT ?>') {
                selected = 'selected';
            }
            html += '<option ' + selected + ' value="<?php echo E4S_PF_TT ?>"><?php echo E4S_PF_TT_FULL ?>';
            selected = '';
            if (pfType === '<?php echo E4S_PF_LYNX ?>') {
                selected = 'selected';
            }
            html += '<option ' + selected + ' value="<?php echo E4S_PF_LYNX ?>"><?php echo E4S_PF_LYNX_FULL ?>';
            html += '</select>';
            html += '&nbsp;';
            let pfSystem = getPFSystem();
            html += '<label for="selectSystem">System # : </label>';
            html += '<select id="selectSystem" onchange="updatePhotofinishSystem();">';
            html += '<option value="1" ' + (pfSystem === 1 ? " selected" : "") + '>Main</option>';
            html += '<option value="2" ' + (pfSystem === 2 ? " selected" : "") + '>Secondary</option>';
            html += '<option value="3" ' + (pfSystem === 3 ? " selected" : "") + '>Third</option>';
            html += '<option value="4" ' + (pfSystem === 4 ? " selected" : "") + '>Fourth</option>';
            html += '<option value="5" ' + (pfSystem === 5 ? " selected" : "") + '>Fifth</option>';
            html += '</select>';
            html += '</div>';
            html += '<span id="exportMessage"></span>';
            html += '</div>';
            $(html).dialog({
                width: 650,
                height: 760,
                modal: true,
                title: "Entry4Sports Photo Finish Export",
                open: function (event, ui) {
                    e4s_print_getEventsForFilter('<?php echo E4S_EVENT_TRACK ?>', 'e4sExportEvents');
                    e4s_setExportEventSelector();
                    e4s_printSelect(true);
                    $("#" + photoFinishExport.dialogName).addClass("e4sPrintDialogDiv");
                },
                close: function () {
                    $("#" + photoFinishExport.dialogName).remove()
                },
                buttons: [
                    {
                        text: "Export",
                        icon: "ui-icon-arrowthickstop-1-s",
                        click: function () {
                            let pfFormat = $("#selectPF").val();
                            downloadIndividual(pfFormat);
                        }
                    }, {
                        text: "Auto Export",
                        icon: "ui-icon-arrowthickstop-1-s",
                        id: "pfAutoDirect",
                        style: pfType === '<?php echo E4S_PF_LYNX ?>' ? "" : "display:none",
                        click: function () {
                            photoFinishExport.toggleEnabled();
                        }
                    }, {
                        text: "Send Direct to PF",
                        icon: "ui-icon-arrowthickstop-1-s",
                        id: "pfDirect",
                        click: function () {
                            sendIndividualDirectToPF();
                        }
                    },
                    {
                        text: "Close",
                        icon: "ui-icon-closethick",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ]
            });
            this.autoExportBtn();
        },
        autoExportBtn: function (pf = null) {
            if (pf === null) {
                pf = $("#selectPF").val();
            }
            let btnObj = $("#pfAutoDirect");
            if (pf === '<?php echo E4S_PF_LYNX ?>') {
                if (this.autoEnabled) {
                    btnObj.text("Disable Auto Export");
                } else {
                    btnObj.text("Enable Auto Export");
                }
                btnObj.show();
            } else {
                btnObj.hide();
            }
        },
        getUniqueDates: function (egs) {
            let optionsHTMLArray = ["<option selected value=''>All Dates</option>"];
            let dates = [];
            let dateSort = [];
            for (let eg in egs) {
                let date = egs[eg].date.split("T")[0];
                if (!dates[date]) {
                    dateSort.push(date);
                }
                dates[date] = new Date(date);
            }

            dateSort.sort();
            for (let d = 0; d < dateSort.length; d++) {
                let useDate = dateSort[d];
                optionsHTMLArray.push("<option value='" + useDate + "'>" + formatDate(dates[useDate], "D MMM yyyy") + "</option>");
            }
            return optionsHTMLArray;
        },
        exportSelector: function () {
            let from = parseInt($("#exportFromEvent :selected").val());
            let to = parseInt($("#exportToEvent :selected").val());
            let seeded = parseInt($("#exportSeeded :selected").val());

            $("[name=eventToPrint]").each(
                function (e) {
                    let element = $(this);
                    let val = parseInt(element.val());
                    let selectedDate = $("#exportDateEvent option:selected").val();
                    let process = true;

                    let egs = getEventGroups();
                    for (let eg in egs) {
                        let eventGroup = egs[eg];
                        if (eventGroup.eventno === val) {
                            if (selectedDate !== "") {
                                let egDate = eventGroup.date.split("T")[0];
                                if (egDate !== selectedDate) {
                                    process = false;
                                }
                            }
                            if (seeded) {
                                if (!isEventGroupSeeded(eventGroup)) {
                                    process = false;
                                }
                            }
                        }
                    }
                    if (val >= from && val <= to && process) {
                        element.attr("checked", "checked");
                    } else {
                        element.removeAttr("checked");
                    }
                }
            )
        }
    }

    function calculateSummary(data) {
        let orderTotal = 0,
            feeTotal = 0,
            summary = [];
        for (let i = 0; i < data.length; i++) {
            let row = data[i],
                price = removeCurrency(row[1]),
                fee = removeCurrency(row[2]);
            orderTotal += price;
            if (!isNaN(fee)) {
                feeTotal += fee;
            }

            let key = row[1];
            let useKey = "";
            for (let sum in summary) {
                if (sum === key) {
                    useKey = sum;
                }
            }
            if (useKey !== "") {
                summary[key] = summary[key] + 1;
            } else {
                summary[key] = 1;
            }
        }
        orderTotal = "<?php echo $config['currency'] ?>" + $.paramquery.formatCurrency(orderTotal);
        feeTotal = "<?php echo $config['currency'] ?>" + $.paramquery.formatCurrency(feeTotal);
        let summaryArr = [["==========", "==========", "=========="]];
        let sumText = "Counts";
        for (let sum in summary) {
            summaryArr.push([sumText, sum, summary[sum]]);
            sumText = "";
        }
        summaryArr.push(["Totals", orderTotal, feeTotal]);
        return summaryArr;
    }

    function moveGrid(fromGrid, toGrid) {
        let newDiv = $("#" + fromGrid).detach();
        newDiv.prop("id", toGrid);
        newDiv.appendTo("#" + toGrid);
    }

    function changeToGrid(table, grid, tablename) {
        let tbl = $("#" + table);
        let obj = $.paramquery.tableToArray(tbl);
        let newObj = {
            width: 1000,
            height: 'flex',
            maxHeight: 800,
            resizable: true,
            title: tablename,
            flex: {
                on: true,
                one: true
            }
        };

        newObj.dataModel = {data: obj.data};
        newObj.colModel = obj.colModel;
        newObj.toolbar = getToolbar(table);
        if (table === "finance_table") {
            newObj.summaryData = calculateSummary(obj.data);
        }

        let $grid = $("#general_grid").pqGrid(newObj);
        $grid.pqGrid("option", "width", "auto")
            .pqGrid("refresh");

        moveGrid("general_grid", grid);
        $("#general_grid_parent").append("<div id='general_grid' style='margin:auto;'></div>");
    }

    function populateTree(data, table, gridFrom, gridTo) {
        let obj = {
            title: "Teams",
            height: "flex",
            maxHeight: 800,
            resizable: true,
            filterModel: {on: true, mode: "AND", header: true},
            swipeModel: {on: "touch"},
            stripeRows: false,
            rowInit: function (ui) {
                let rd = ui.rowData, color = ["lightyellow", "lightgreen", "pink", "lightblue"];
                if (rd.pq_gsummary) {
                    return {style: "background:" + color[rd.pq_level] + ";"};
                }
            },
            toolbar: getToolbar(table),
            treeModel: {
                dataIndx: "teamname",
                checkbox: false,
                checkboxHead: true,
                cascade: true,
                icons: false,
                render: function (ui) {
                    //custom icon.
                    if ((ui.cellData || "").indexOf("license") >= 0) {
                        return {iconCls: "ui-icon-pencil"};
                    }
                },
                summary: true
                //checkbox: true
            },
            sortModel: {ignoreCase: true},
            scrollModel: {autoFit: true},
            columnTemplate: {minWidth: '8%', maxWidth: '80%', width: 100},
            menuIcon: true,
            colModel: [
                //optional column required only to implement disabled checkboxes.
                {
                    dataIndx: 'pq_tree_cb',
                    hidden: true,
                    editable: function (ui) {
                        //disable the checkboxes based on a condition.
                        return ui.rowData.teamname.indexOf('license') === -1;
                    }
                },
                {
                    dataIndx: 'teamname', title: 'Team name',
                    exportRender: true,
                    filter: {
                        crules: [{condition: "contain"}]
                    }
                },
                {dataIndx: 'agegroup', title: 'Age Group', dataType: 'text'},
                {dataIndx: 'event', title: 'Event', dataType: 'text'},
                {dataIndx: 'gender', title: 'Gender', dataType: 'text'},
                {dataIndx: 'price', title: 'Price', dataType: 'text'},
                {dataIndx: 'user', title: 'User', dataType: 'text'},
                {dataIndx: 'email', title: 'Email', dataType: 'text'},
                {dataIndx: 'created', title: 'Created', dataType: 'text'},
                {dataIndx: 'athleteId', title: 'Athlete ID', dataType: 'text'},
                {dataIndx: 'clubname', title: 'Club', dataType: 'text'},
                {dataIndx: 'firstname', title: 'Firstname', dataType: 'text'},
                {dataIndx: 'surname', title: 'Surname', dataType: 'text'},
                {dataIndx: 'dob', title: 'D.O.B', dataType: 'text'},
                {dataIndx: 'urn', title: 'URN', dataType: 'text'}
            ],
            dataModel: {data: data}
        };
        if (typeof data !== "undefined") {
            pq.grid("#" + gridFrom, obj);
            moveGrid(gridFrom, gridTo);
        }
    }

    function easySwitchPresent(heatNo, laneNo) {
        let entry = getEntryForHeatLane(heatNo, laneNo);
        if (entry !== null) {
            processSwitchPresent(entry);
        }
    }

    function processSwitchPresent(entry) {
        setEntryPresent(entry, !isEntryPresent(entry));
        updateDBPresent(entry);
    }

    function validateLaneCountEntry(obj) {
        let currentEventGroup = getCurrentEventGroup();
        let laneCnt = getLaneCount(currentEventGroup);
        let maxInHeat = getMaxInHeatFromEg(currentEventGroup);

        let useCount = laneCnt;

        if (maxInHeat > laneCnt) {
            // if not in lanes allow 1 more so the athlete can be added rather than switched
            useCount++;
        }
        if (parseInt(obj.value) < 1) {
            obj.value = 1;
        }
        if (parseInt(obj.value) > useCount) {
            obj.value = useCount;
        }

        let heatNo = $("#newHeatNo").val();
        let bibNo = $("#bib_" + heatNo + "_" + obj.value).text();
        bibNo = bibNo.trim();
        if (bibNo === "") {
            bibNo = 0;
        }
        getSwitchedInAthlete(bibNo);
    }

    function validateHeatCountEntry(obj, max) {
        if (parseInt(obj.value) < 1) {
            obj.value = 1;
        }
        if (parseInt(obj.value) > max) {
            obj.value = max;
        }
        // $("#switchedInBibNo").val("");
        // $("#switchedInAthlete").val("");
        let laneNo = $("#newLaneNo").val();
        let bibNo = $("#bib_" + obj.value + "_" + laneNo).text();
        bibNo = bibNo.trim();
        if (bibNo === "") {
            bibNo = 0;
        }
        getSwitchedInAthlete(bibNo);
    }

    function setSwitchedEntityId(entityId) {
        e4sGlobal.switchedEntityId = entityId;
    }

    function getSwitchedEntityId() {
        return e4sGlobal.switchedEntityId;
    }

    function getPFType() {
        return getCompetition().options.pf.type;
    }

    function e4s_exportAthleteDialog(e4sName) {
        let html = "";
        html += '<div id="e4sExportDialog">';
        html += '<div id="e4sExportAthleteSystem">';
        html += 'Please select the Photo Finish System : ';
        let pfType = getPFType();
        html += '<select id="selectPF"';
        html += ' onchange="updatePhotofinish(this.value);"';
        html += '>';
        let selected = '';
        if (pfType === '<?php echo E4S_PF_TT ?>') {
            selected = ' selected ';
        }
        html += '<option value="<?php echo E4S_PF_TT ?>" ' + selected + ' ><?php echo E4S_PF_TT_FULL ?></option>';
        selected = '';
        if (pfType === '<?php echo E4S_PF_LYNX ?>') {
            selected = ' selected ';
        }
        html += '<option value="<?php echo E4S_PF_LYNX ?>" ' + selected + ' ><?php echo E4S_PF_LYNX_FULL ?></option>';
        html += '</select>';
        html += '</div>';
        html += '<div id="e4sExportAthleteName" style="display:';
        //if ( pfType === '<?php //echo E4S_PF_LYNX ?>//' ){
        //    html += "none";
        //}
        html += '">';
        html += 'Add Club Name : ';
        html += '<select id="selectPFClubName">';
        html += '<option value="Y">Yes</option>';
        html += '<option value="N" selected>No</option>';
        html += '</select>';
        html += '</div>';
        html += '</div>';

        $("#e4sExportDialog").remove();
        $(html).dialog({
            width: 500,
            height: 200,
            modal: true,
            title: "Entry4Sports Photo Finish Athlete Export",

            buttons: [
                {
                    text: "Export",
                    icon: "ui-icon-arrowthickstop-1-s",
                    click: function () {
                        let blob = "";
                        let pfFormat = $("#selectPF").val();
                        let fileExt = "";
                        let addClub = $("#selectPFClubName").val() === "Y";
                        switch (pfFormat) {
                            case "<?php echo E4S_PF_TT ?>":
                                fileExt = "<?php echo E4S_PF_TT_ATH_EXT ?>";
                                blob = timeTronicsAthlete(addClub);
                                break;
                            case "<?php echo E4S_PF_LYNX ?>":
                                fileExt = "<?php echo E4S_PF_LYNX_PPL_EXT ?>";
                                blob = lynxAthlete(addClub);
                                break;
                        }
                        processBlob(blob, fileExt, e4sName);
                        $(this).dialog("close");
                    }
                },
                {
                    text: "Cancel",
                    icon: "ui-icon-closethick",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ]
        });
    }

    function e4s_readStickySettings() {
        let sticky = readCookie("<?php echo E4S_STICKY_COOKIE ?>");
        if (sticky === "true") {
            sticky = true;
        } else {
            sticky = false;
        }
        return sticky;
    }

    function e4s_writeStickySettings(sticky) {
        createCookie("<?php echo E4S_STICKY_COOKIE ?>", sticky);
    }

    function e4s_toggleStickySettings() {
        let sticky = e4s_readStickySettings();
        e4s_writeStickySettings(!sticky);
        e4s_displayStickySettings();
    }

    function e4s_displayStickySettings() {
        let sticky = e4s_readStickySettings();
        let cardBase = $("#card-base");
        let cardSettingsBtn = $("#Settings");
        let cardSettingsMenu = $("#cardActionMenu");
        if (!sticky || isPrinting()) {
            cardBase.removeClass("card_base_sticky");
            cardSettingsMenu.removeClass("cardActionMenu_sticky");
            cardSettingsBtn.show();
        } else {
            cardBase.addClass("card_base_sticky");
            cardSettingsMenu.addClass("cardActionMenu_sticky");
            cardSettingsBtn.hide();
        }
    }

    function e4s_setExportEventSelector() {
        $("#exportToEvent option").last().attr("selected", true);
    }

    function getToolbar(name) {
        let options;
        let toolbarName = name.replace("_table", "");
        if (toolbarName === "<?php echo E4S_PF_ATHLETES ?>") {
            options = [{
                xlsx: "Excel",
                csv: "Csv",
			<?php echo E4S_COMP_SETTING_PFATHLETES ?>:
            "Photo Finish",
                htm
        :
            "Html"
        }]
        } else {
            options = [{
                xlsx: "Excel",
                csv: "Csv",
			<?php echo E4S_COMP_SETTING_PF ?>:
            "Photo Finish",
                htm
        :
            "Html"
        }]
        }
        let toolbar = {
            items: [
                {
                    type: "select",
                    label: "Format: ",
                    attr: "id=" + name + "_export_format",
                    options: options,
                    listener: function (e) {
                    }
                },
                {
                    type: "button",
                    label: "Export",
                    cls: "e4s-button e4s-button--tertiary e4s_card_button",
                    listener: function (e) {
                        let fileExt = $("#" + name + "_export_format").val();
                        let blob = "";
                        let e4sName = name.replace("_table", "");
                        let process = true;
                        switch (fileExt) {
                            case "<?php echo E4S_COMP_SETTING_PF ?>":
                                process = false;
                                photoFinishExport.exportDialog();
                                break;
                            case "<?php echo E4S_COMP_SETTING_PFATHLETES ?>":
                                process = false;
                                e4s_exportAthleteDialog(e4sName);
                                break;
                            default:
                                blob = this.exportData({
                                    //url: "/pro/demos/exportData",
                                    format: fileExt,
                                    nopqdata: true, //applicable for JSON export.
                                    render: true
                                });
                                break;
                        }
                        if (process) {
                            processBlob(blob, fileExt, e4sName);
                        }
                    }
                },
                {
                    type: "button",
                    cls: "e4s-button e4s-button--tertiary e4s_card_button",
                    label: "Print",
                    listener: function () {
                        let exportHtml = this.exportData({
                            title: "Entry4Sports Entries",
                            format: "htm",
                            render: true
                        });
                        e4s_print(exportHtml);
                    }
                }
            ]
        };

        if (name === "team_table" || name === "tree_table") {
            toolbar.items.push({type: "separator"});
            if (name === "tree_table") {
                toolbar.items.push({
                    type: "button",
                    cls: "e4s-button e4s-button--tertiary e4s_card_button",
                    label: "Switch Format",
                    listener: function () {
                        $("#team_tree_grid").hide();
                        $("#team_grid").show();
                    }
                });
            }
            if (name === "team_table") {
                toolbar.items.push({
                    type: "button",
                    cls: "e4s-button e4s-button--tertiary e4s_card_button",
                    label: "Switch Format",
                    listener: function () {
                        $("#team_tree_grid").show();
                        $("#team_grid").hide();
                    }
                });
            }
        }
        return toolbar;
    }

    function getLynxScheduleName() {
        return "<?php echo E4S_PF_LYNX_FULL ?>.<?php echo E4S_PF_LYNX_SCH_EXT ?>";
    }

    function getLynxIndivName(fileExt) {
        return "<?php echo E4S_PF_LYNX_FULL ?>." + fileExt;
    }

    function processBlob(blob, fileExt, e4sName) {
        if (typeof blob === "string") {
            blob = new Blob([blob]);
        }
        let filename;
        if (fileExt === "<?php echo E4S_PF_LYNX_PPL_EXT ?>" || fileExt === "<?php echo E4S_PF_LYNX_IND_EXT ?>") {
            filename = getLynxIndivName(fileExt);
        } else {
            filename = "E4S_" + getCompetition().name + "-" + e4sName + "." + fileExt;
        }
        saveAs(blob, filename);
        if (fileExt === "<?php echo E4S_PF_LYNX_IND_EXT ?>") {
            blob = lynxEventsSch();
            blob = new Blob([blob]);
            saveAs(blob, getLynxScheduleName());
        }
    }
</script>