<?php
/*
 search for
// PHP Data Function  - to get to where the data is read in php
// PHP Functions where php code starts
// e4s_getHeatLaneInfoWithCheckin to get backend seeding
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
require_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'reports/compreport1.php';
include_once E4S_FULL_PATH . 'results/results.php';
const E4S_RESULT_PREFIX = 'resultByt';
const E4S_CARD_FIELD_PREFIX = 'trackF_';
const E4S_CARD_RACE_PREFIX = 'race_';
const E4S_CARD_DISTANCE_COLS = 16;
const E4S_HEAT_CACHE = 'E4S_HeatCache_';
const E4S_ENTRY_COUNT_OBJ = 'e4sEntryCountObj';
const E4S_OUTPUT_COMP = 'E4S_OUTPUT_COMP';
const E4S_OUTPUT_DATA = 'E4S_OUTPUT_DATA';
const E4S_OUTPUT_OBJ = 'E4S_OUTPUT_OBJ';
const E4S_BIB_NOS = 'E4S_BIB_NOS';
const E4S_DATE_ONLY = 'D';
const E4S_TIME_ONLY = 'T';
const E4S_HEATCACHE_AGE = 'A';
const E4S_HEATCACHE_GENDER = 'G';
const E4S_HEATCACHE_BOTH = 'B';
const E4S_SEED_FORENAME = 'athletef';
const E4S_SEED_SURNAME = 'athletes';
const E4S_SEED_CHECKEDIN = 'status';
const E4S_SEED_BIB = 'bib';
const E4S_DEFAULT_LANE_COUNT = 5;
const E4S_DEFAULT_STANDS = 80;
const E4S_MAX_STANDS = 80;
const E4S_CHECKIN_NOT_OPEN = -1;
const E4S_CHECKIN_OPEN = 0;
const E4S_CHECKIN_CLOSED = 1;
const E4S_CLASS_TO_BE_SEEDED = 'seed_UnSeeded';
const E4S_CLASS_SEEDED = 'seed_Seeded';
const E4S_CLASS_VOIDLANE = 'void_lane';
const E4S_INCLUDE_ENTITY = TRUE;
const E4S_REGION = 'REGION';
const E4S_COUNTY = 'COUNTY';
const E4S_CLUB = 'CLUB';
const E4S_FIELD_CARD_COUNT = 25;
const E4S_HEIGHT_CARD_TRIALS = 14;
const E4S_SHOW_ONLY_CARDS = 'e4s_show_only_cards';
const E4S_CURRENT_CARD_STORE_NAME = 'e4sCurrentCard';
const E4S_CARDMODE_COOKIE = 'e4sCardMode';
const E4S_STICKY_COOKIE = 'e4sStickyMode';
const E4S_OFFLINE_RESULTS = 'e4sOfflineResults';
const E4S_SCOREBOARDS_COOKIE = 'e4sUseScoreboards';
const E4S_SEEDING_MONITOR_COOKIE = 'e4sSeedingMonitor';
const E4S_SEEDING_DRAGDROP_COOKIE = 'e4sDragDrop';
const E4S_SEEDING_CONFIRM_COOKIE = 'e4sQuickConfirm';
const E4S_SHOW_RANKING_COOKIE = 'e4sMaxRanking';
const E4S_DISPLAY_MODE_COOKIE = 'e4sDisplayMode';
const E4S_DISPLAY_LIGHT_MODE = 'light';
const E4S_DISPLAY_DARK_MODE = 'dark';
const E4S_CARDMODE_CARD = 'Card';
const E4S_CARDMODE_NONECARD = 'NoneCard';
const E4S_RESULT_PARAMS = 'params';
const E4S_CARD_RESULTS_DIV = 'cardResults';
const E4S_CARD_ATTEMPT_DIV = 'cardAttempt';
const E4S_ENVIRONMENT = 'e4s_env';
// menu items when clicking a bib number
const E4S_MENU_MOVE_ENTRY = 'moveEntry';
const E4S_MENU_REMOVE_ENTRY = 'removeEntry';
const E4S_MENU_TOGGLE_PRESENCE = 'togglePresence';

$GLOBALS[E4S_ENVIRONMENT] = '';
$GLOBALS[E4S_OUTPUT_DATA] = '';
$GLOBALS[E4S_SHOW_ONLY_CARDS] = TRUE;
// search for PHP Functions to get to server code
// Search for PHP Data Function for main data function
function showOutputReport($compId, $onlyCards = TRUE) {
//    if ( $compId === 400 ){
//        exit;
//    }
    $GLOBALS[E4S_SHOW_ONLY_CARDS] = $onlyCards;
    $GLOBALS[E4S_OUTPUT_COMP] = $compId;
    $compObj = e4s_GetCompObj($compId);
    $coptions = $compObj->getFullOptions();
//    if ( !$compObj->isOrganiser() ){
//        if ( !$compObj->areCardsAvailable() ){
//            echo "The organiser has disabled E4S Cards and will be providing a seeding list separately.";
//            return;
//        }
//    }
    $school = FALSE;
    if (isset($coptions->school)) {
        $school = $coptions->school;
    }

    $GLOBALS[E4S_BIB_NOS] = e4s_getCurrentBibNos($compId);
    $reportData = getReportData($compId);

    $GLOBALS[E4S_OUTPUT_OBJ] = e4s_getOptionsAsObj($reportData);
    $config = e4s_getConfig();
    $env = strtoupper($config['env']);
    if ($env === 'PROD') {
        $env = '';
    } else {
        $env .= ' ';
    }
    $cfgoptions = e4s_getOptionsAsObj($config['options']);
    $uiTheme = E4S_JQUERY_THEME;
    if ( $config['theme'] === 'IRL' ) {
        $GLOBALS[E4S_ENVIRONMENT] = 'ia-';
        $uiTheme = 'humanity';
    }
    if (isset($cfgoptions->uiTheme)) {
        $uiTheme = $cfgoptions->uiTheme;
    }

    header('Content-Type: text/html; charset=utf-8');
    ?>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?php echo $env . $compObj->getID() . ' : Information' ?>
        </title>
        <link rel="stylesheet"
              href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/<?php echo $uiTheme ?>/jquery-ui.css"/>
        <link rel="stylesheet" href="<?php echo E4S_PATH ?>/paramquery/pqgrid.min.css">
        <link rel="stylesheet" href="<?php echo E4S_PATH ?>/paramquery/pqgrid.ui.min.css">
        <link rel="stylesheet" href="<?php echo E4S_PATH ?>/css/toastr.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@6.2.1/css/fontawesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.ui.position.js"></script>
        <script type="text/javascript" src="<?php echo E4S_PATH ?>/reports/commonReports.js"></script>
        <script type="text/javascript" src="<?php echo E4S_PATH ?>/js/jquery-dateformat.min.js"></script>
        <script type="text/javascript" src="<?php echo E4S_PATH ?>/js/toastr.min.js"></script>
        <script type="text/javascript" src="<?php echo E4S_PATH ?>/paramquery/pqgrid.min.js"></script>
        <script type="text/javascript" src="<?php echo E4S_PATH ?>/paramquery/pqtouch.min.js"></script>
        <script type="text/javascript" src="<?php echo E4S_PATH ?>/paramquery/jszip.min.js"></script>
        <script type="text/javascript" src="<?php echo E4S_PATH ?>/paramquery/FileSaver.js"></script>
        <link rel="stylesheet" href="<?php echo E4S_PATH ?>/css/entry4sports.css">
        <link rel="stylesheet" href="<?php echo E4S_PATH ?>/reports/e4sReports.css">
        <script type="text/javascript" src="<?php echo E4S_PATH ?>/js/menu-bar.js"></script>
        <?php
        include_once E4S_FULL_PATH . 'css/menu-bar_css.php';
        ?>
        <style>
            @media print
            {
                .no-print, .no-print *
                {
                    display: none !important;
                }
            }

            @keyframes line-anim {
                from {
                    stroke: white;
                    fill: transparent;
                }

                to {
                    stroke: black;
                    stroke-dashoffset: 0;
                    stroke-width: 0;
                }
            }

            @keyframes pulsing {
                0% {
                    transform: scale(1);
                }

                50% {
                    transform: scale(1.1);
                }

                100% {
                    transform: scale(1);
                }
            }
        </style>
        <?php
        getCss();
        ?>
        <style>
            .ui-widget.ui-widget-content {
                border: 1px solid var(--e4s-card--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__border-color);
            }

            .ui-corner-all, .ui-corner-bottom, .ui-corner-right, .ui-corner-br {
                border-bottom-right-radius: var(--e4s-card--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__border-radius);
            }

            .ui-corner-all, .ui-corner-bottom, .ui-corner-left, .ui-corner-bl {
                border-bottom-left-radius: var(--e4s-card--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__border-radius);
            }

            .ui-corner-all, .ui-corner-top, .ui-corner-right, .ui-corner-tr {
                border-top-right-radius: var(--e4s-card--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__border-radius);
            }

            .ui-corner-all, .ui-corner-top, .ui-corner-left, .ui-corner-tl {
                border-top-left-radius: var(--e4s-card--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__border-radius);
            }

            .ui-widget-content {
                border: var(--e4s-card--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__border) var(--e4s-card--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__border-color);
                background: var(--e4s-card--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__background);
                color: var(--e4s-input-field__text-color);
            }

            .ui-widget {
                font-family: 'Cabin', Arial, sans-serif;
                font-size: 1.1em;
            }

            .ui-widget-header {
                border: none;
                background: var(--e4s-navigation-bar--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__background);
                color: var(--e4s-navigation-link--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__text-color);
                font-weight: bold;
            }

            .ui-dialog-buttonset button {
                height: var(--e4s-button__height);
                padding: var(--e4s-button__padding);
                border: none;
                border-radius: var(--e4s-button__border-radius);
                cursor: pointer;
                transition: all 0.36s;
                color: var(--e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__text-color) !important;
                background: var(--e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__background);
                font: var(--e4s-body--100);
            }

            .ui-dialog-buttonset button:hover,
            .ui-dialog-buttonset button:focus {
                background: var(--e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__hover-background);
                border: none;
                font: inherit;
                font-weight: inherit;
                color: var(--e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__text-color);
            }

            .ui-dialog-buttonset button:active {
                background: var(--e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__active-background);
                border: none;
                font: inherit;
                font-weight: inherit;
                color: var(--e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__text-color);
            }

            .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
                font: var(--e4s-body--100);
                font-size: 1em;
            }

            .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active {
                border-radius: var(--e4s-button__border-radius) !important;
                font: var(--e4s-body--100);
                color: var(--e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>tertiary__text-color);
            }

            #sortableList li {
                margin: 4px;
                padding: 8px;
                padding-left: 1.5em;
                width: 560px;
                height: 18px;
                box-sizing: unset !important;
            }

            .ui-icon-pencil {
                background-position: -64px -116px;
            }

            .ui-icon-check {
                background-position: -64px -142px;
            }

            div#seedingsDiv input[type="radio"] {
                accent-color: var(--e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__background);
            }

            #manualSeedDialog > table {
                width: 100%;
                padding: 0 0 8px 0;
            }
            .card_spare_header_col {
                border-bottom: 2px solid black;
            }
            .card_spare_col {
                width: 3vw;
                border-right: 1px solid black;
            }
            .atAttemptHeader {
                font-size: 0.6em;
            }
            .cardHeaderRowSpan {
                line-height: 1vw;
            }
            .heightStatusDiv{
                padding-top: 10px;
                font-weight: bold;
            }
            .shHeightsList {
                width: 65px;
                font-family: sans-serif !important;
            }
            .shPresent {
                width: 10px;
            }
            .shInputStd{
                width: 50px;
                margin-right: 1em !important;
                font-family: sans-serif !important;
            }
            .shInputdisabled{
            }
            .statusKPI {
                font-size: 1.5vh;
                font-weight: 800;
            }
            .heightResultStatus {
                display: flex;
                width: 100%;
                justify-content: space-around;
                align-content: center;
                flex-direction: row;
                font-weight: 500;
                border-top: 1px dashed;
            }
            .athleteAttemptHeader {
                text-align:center;
                font-weight: bold;
            }
            .heightActiveAthlete {
                background-color: #bae6fd;
            }
            .heightLeftPanel {
                align-content: start;
            }
            .heightRightPanel {
                vertical-align: top;
                padding-left: 15px;
            }
            .athleteSHEntry {
                text-align: center;
                width: 25px;
            }
            .incrementInput {
                width: 40px !important;
            }
            .heightInput {

            }
            .athleteFieldset {
                padding: 10px;
            }
            .resultsHistory {
                padding-top: 5px;
            }
            .resultFieldSet {
                border-left: 0px;
                border-right: 0px;
                border-bottom: 0px;
            }
            .heightDialogAthletes{
                width:40%;
            }
            .heightDialogContent{
                width:60%;
            }
            .attemptTable {
                width: 100%;
                margin-top: 10px;
            }
            .currentAttemptTable {
                width: 100%;
                margin-top: 10px;
            }
            .attemptLabel {
                text-align: center;
                width: 35px;
            }
            .attemptResult {
                text-align: center;
                font-weight: bold;
                width: 35px;
                border: 1px solid;
            }
            .athleteAttemptHeaderAthlete {
                font-size: 1.0rem;
            }
            .athleteAttemptHeaderHeight {
                font-size: .75rem;
            }
            .activeAttempt {
                /*border: 1px dashed red;*/
                background-color: var(--e4s-button--tertiary__active-border-color) !important;
            }
            .fieldSeedClubname {
                margin-left: .5em !important;
            }
            .jumpOrderHeader {
                /*background-color: red;*/
            }
            .e4sNotVisible {
                visibility: hidden;
            }
            .e4sLegend {
                padding: 0px 10px 0px 10px;
            }
            .athleteLegend{
                font-weight:600;
            }
            .nextAthleteBtn {
                width: 100%;
                margin-top: 5px;
            }
            .heightContentDiv{
                align-content: start;
                padding: 5px;
            }
            .heightAthletesDiv {
                overflow-y: scroll;
                max-height: 95%;
                height: 390px;
                width: 375px;
                max-width: 100%;
            }
            .heightAthlete {
                text-align: justify;
                width:200px;
            }
            .heightResults {
                overflow-y: scroll;
                height : 175px;
            }
            .strikeThroughOrig {
                text-decoration: line-through;
            }
            .strikethrough {
                position: relative;
            }
            .notAvailableTrial {
                background-color: coral;
            }
            .notActiveTrial {
                background-color: bisque;
            }
            .strikethroughForce {
                position: relative;
            }
            .strikethroughForce:before {
                border-top: 2px solid red;
                position: absolute;
                content: "";
                left: 0;
                top: 50%;
                right: 0;

                -webkit-transform:rotate(-30deg);
                -moz-transform:rotate(-30deg);
                -ms-transform:rotate(-30deg);
                -o-transform:rotate(-30deg);
                transform:rotate(-30deg);
            }
            .strikethrough:before {
                position: absolute;
                content: "";
                left: 0;
                top: 50%;
                right: 0;
                border-top: 2px solid orange;

                -webkit-transform:rotate(-30deg);
                -moz-transform:rotate(-30deg);
                -ms-transform:rotate(-30deg);
                -o-transform:rotate(-30deg);
                transform:rotate(-30deg);
            }
        </style>
        <script>
            const card_results_distance_dialog = 'resultsByTrial';
            let closeDialog = '';
            function initToaster() {
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            }
            function isTouchDevice() {
                return (('ontouchstart' in window) ||
                    (navigator.maxTouchPoints > 0) ||
                    (navigator.msMaxTouchPoints > 0));
            }
            function getCurrentEntries() {
                let eventGroup = getCurrentEventGroup();
                return getEntriesForEg(eventGroup);
            }

            function getCurrentEventGroup() {
                let curEg = e4sGlobal.currentEventGroup;
                return curEg;
            }

            function getEventGroupType(eventGroup) {
                return eventGroup.type;
            }

            function getEventGroupId(eventGroup) {
                return eventGroup.id;
            }

            function isEventGroupField(eventGroup) {
                if (!eventGroup) {
                    eventGroup = getCurrentEventGroup();
                }
                return getEventGroupType(eventGroup) === "<?php echo E4S_EVENT_FIELD ?>";
            }

            function isEventGroupTrack(eventGroup) {
                if (!eventGroup) {
                    eventGroup = getCurrentEventGroup();
                }
                return getEventGroupType(eventGroup) === "<?php echo E4S_EVENT_TRACK ?>";
            }

            function isEventGroupMulti(eventGroup) {
                if (!eventGroup) {
                    eventGroup = getCurrentEventGroup();
                }
                return getEventGroupType(eventGroup) === "<?php echo E4S_EVENT_MULTI ?>";
            }

            function setCurrentEventGroup() {
                // return getEventGroup(getSelectedEventNo());
                let eventNo = getSelectedEventNo();
                let eg = getEventGroup(eventNo);
                if (eg === null) {
                    eg = getEventGroups()[0];
                }
                if (isEventGroupField(eg)) {
                    // set default athletes per heat ( lane Count ) for field events
                    if (!hasEventGroupGotLaneCount(eg)) {
                        setLaneCount(eg, 20);
                    }
                }
                e4sGlobal.currentEventGroup = eg;
                return eg;
            }

            function getSelectedEventNo() {
                let eventNo = $("#event_selector :selected").val();
                if (eventNo === "") {
                    eventNo = 0;
                }

                return parseInt(eventNo);
            }

            function getEventSelectedSeed() {
                let retObj = {};
                retObj.gender = "";
                retObj.ageGroupId = "";

                let eventGroup = getCurrentEventGroup();
                return eventGroup.seed;
            }
            function getCompEventsForEg(eg = null){
                if ( eg === null){
                    eg = getCurrentEventGroup();
                }
                let egId = getEventGroupId(eg);
                let ceObjs = getCompEvents();
                let ceArr = [];
                for( let c in ceObjs){
                    let ce = ceObjs[c];
                    if ( ce.egId === egId ){
                        ceArr.push(ce);
                    }
                }
                return ceArr;
            }
            function first_selector() {
                manual_eventElement_select(2);
            }

            function prev_selector() {
                let curElement = getCurrentOptionElement();
                if (curElement > 1) {
                    manual_eventElement_select(curElement);
                }
            }

            function next_selector() {
                let curElement = getCurrentOptionElement();
                manual_eventElement_select(curElement + 2);
            }

            function last_selector() {
                let last = $("#event_selector > option").length;
                manual_eventElement_select(last);
            }

            function manual_event_select(eventGroup) {
                let eventNo = eventGroup.eventno;
                let inList = false;
                $("#event_selector option").each(function (val, name) {
                    let selectVal = $('#event_selector :nth-child(' + (val + 1) + ')').val();
                    if (parseInt(selectVal) === eventNo) {
                        inList = true;
                    }
                });

                e4sGlobal.currentEventGroup = eventGroup;
                storeCurrentEventShown();
                if (!inList) {
                    filterObj.clear();
                } else {
                    $("#event_selector").val(eventNo);
                    cardChange();
                }
            }

            function getEventNoFromEventGroup(eventGroup) {
                return eventGroup.eventno;
            }

            function getEventNoFromEntry(entry) {
                if ( typeof entry.eventno !== 'undefined') {
                    return entry.eventno;
                }
                let eg = getEventGroupById(entry.eventGroupId);
                return eg.eventno;
            }

            function getEventNoFromCE(ce) {
                return ce.eventNo;
            }

            function jumpToFeeder() {
                let curEg = getCurrentEventGroup();
                let heatEg = getHeatEg(curEg);
                if ( heatEg !== 0 ){
                    manual_event_select(heatEg);
                }
            }
            function jumpToFeeder2() {
                let curEg = getCurrentEventGroup();
                let eventGroups = getEventGroups();

                for (let e in eventGroups) {
                    let eg = eventGroups[e];
                    if (getEgQualifyToId(eg) === curEg.id) {
                        manual_event_select(eg);
                    }
                }
            }

            function jumpToFinal() {
                let curEg = getCurrentEventGroup();
                let targetId = getEgQualifyToId(curEg);
                let targetEg = getEventGroupById(targetId);
                manual_event_select(targetEg);
            }

            function checkSourceTarget() {
                let curEg = getCurrentEventGroup();
                if (curEg === null) {
                    return;
                }

                let heatEg = getHeatEg(curEg);
                let btnObj = $("#qualifySource");
                if (heatEg !== 0) {
                    btnObj.show();
                    btnObj.text("< " + heatEg.typeno);
                } else {
                    btnObj.hide();
                }

                let finalId = getEGFinalId(curEg);
                btnObj = $("#qualifyTarget");
                if ( finalId !== 0 ){
                    let finalEg = getEventGroupById(finalId);
                    btnObj.show();
                    btnObj.text("> " + finalEg.typeno);
                }else{
                    btnObj.hide();
                }
            }

            function getMaxOptions() {
                return $("#event_selector > option").length - 1;
            }

            function getCurrentOptionElement() {
                let options = $("#event_selector > option");

                for (let o in options) {
                    let option = options[o];
                    if (option.selected) {
                        return parseInt(o);
                    }
                }
                return 0;
            }

            function getOptionValue(option) {
                return option.value;
            }

            function getOptionText(option) {
                return option.text;
            }

            function getElementOptionsValue(element) {
                let options = $("#event_selector > option");
                let option = options[element];
                return getOptionValue(option);
            }

            function manual_eventElement_select(element) {
                $('#event_selector :nth-child(' + element + ')').prop('selected', true);
                cardChange();
            }

            function checkSeedings(){
                let seedings = outputReportData.seedings;
                let entries = getEntries();
                for(let e in entries ){
                    let entry = entries[e];
                    let egId = entry.eventGroupId;
                    let athleteId = entry.athleteId;
                    if (typeof seedings[egId] !== "undefined"){
                        if ( typeof seedings[egId][athleteId] !== "undefined") {
                            let seeding = seedings[egId][athleteId];
                            setHeatNo(entry, seeding.heatNo);
                            setHeatNoCheckedIn(entry, seeding.heatNoCheckedIn);
                        }
                    }
                }
            }
            function getResultExtraText(val, athleteId = 0){
                if ( isNaN(val) ){
                    return '';
                }
                val = getFloatDecimals(val, 2);
                let standards = getCurrentEventGroup().standards;
                let lastStd = 0;
                let extraText = '';
                for(let s in standards){
                    let std = standards[s];
                    if ( val >= std && std > lastStd){
                        lastStd = std;
                        extraText = "(" + s + ")";
                    }
                }
                return extraText;
            }
            function refreshCurrentCard() {
                let urlParams = new URLSearchParams(window.location.search);
                urlParams.delete('egid');
                urlParams.delete('eventno');
                location.search = urlParams.toString();
            }
            function getEGIdFromQS(){
                return getParamFromQS('egid');
            }
            function getParamFromQS(param){
                let urlParams = new URLSearchParams(window.location.search);
                return urlParams.get(param);
            }
            function getEventNoFromQS(){
                return getParamFromQS('eventno');
            }
            function checkLoadEvent(){
                let qsParam = getEGIdFromQS();
                if ( qsParam !== null ){
                    qsParam = parseInt(qsParam);
                    let eg = getEventGroupById(qsParam);
                    if ( eg !== null ){
                        storeEvent(qsParam, isCheckInEnabled());
                        return;
                    }
                }
                qsParam = getEventNoFromQS();
                if ( qsParam !== null ){
                    qsParam = parseInt(qsParam);
                    let eg = getEventGroup(qsParam);
                    if ( eg !== null ){
                        storeEvent(eg.id, isCheckInEnabled());

                    }
                }
            }
            function onLoadCardChange() {
                initEventGroups();
                checkLoadEvent();
                let storedValue = getCurrentEventStoredValue();

                filterObj.init();

                setEventChoices();
                if (storedValue !== null) {
                    let checkedIn = getCheckedInFromStoredValue(storedValue);
                    let egId = getEventFromStoredValue(storedValue);
                    let eg = getEventGroupById(parseInt(egId));
                    if (eg === null) {
                        // Incorrect EventGroup found. Reset
                        resetCurrentEventShown();
                        if ( autoShow.available ){
                            autoShow.reset();
                        }
                    }else{
                        let eventNo = getEventNoFromEventGroup(eg);

                        $("#event_selector").val(eventNo);
                        setCheckedInStatus(checkedIn);
                    }
                }
                checkSeedings();
                cardChange();
                photoFinishExport.init(); // after events are loaded
                <?php if ( secureAccessToCard() ){ ?>
                offLine.flushTbc();
                dragDropUtil.displayMenu();
                <?php } ?>
            }

            function setEventChoices() {
                let filterDate = filterObj.currentDate;
                let filterType = filterObj.currentType;
                let eventGroups = getEventGroups();
                let selectField = $("#event_selector");
                selectField.empty();
                let option = "<option value=''>Select an Event</option>";
                selectField.append(option);
                let selected = "selected";
                for (let eg in eventGroups) {
                    let eventGroup = eventGroups[eg];
                    if ( eventGroup.options.cardDisplay ){
                        let egDate = eventGroup.date.split("T")[0];
                        if (egDate === filterDate || filterDate === "") {
                            if (eventGroup.type === filterType || filterType === "") {
                                let description = getEventGroupTitle(eventGroup);
                                <?php if ( secureAccessToCard() ){ ?>
                                description += ' :(' + getEventGroupId(eventGroup) + ')';
                                <?php } ?>
                                option = "<option " + selected + " value='" + getEventNoFromEventGroup(eventGroup) + "'>" + description + "</option>";
                                selected = "";
                                selectField.append(option);
                            }
                        }
                    }
                }
            }

            function toggleCheckedIn() {
                let obj = $("#entries_confirmed");
                toggleMenuItem(obj);
                cardChange();
            }

            function setCheckedInStatus(show, eg) {
                if (typeof eg !== "undefined") {
                    if (typeof eg.allowForce !== "undefined") {
                        if (!eg.allowForce) {
                            return;
                        }
                    }
                    eg.allowForce = false;
                }
                if (show === true || show === '1') {
                    show = true;
                } else {
                    show = false;
                }
                let obj = $("#entries_confirmed");
                toggleMenuItem(obj, show);
            }

            function showEntriesCheckedInSelection(display) {
                if (display) {
                    $("#entries_confirmed").show();
                } else {
                    setCheckedInStatus(false);
                    $("#entries_confirmed").hide();
                }
            }

            function cardShowsCheckedIn() {
                let obj = $("#entries_confirmed");
                if ( isEventGroupField() ){
                    return false;
                }
                return obj.hasClass("active");
            }

            function isCheckInEnabled() {
                let checkinOptions = getCompetition().options.checkIn;
                return checkinOptions.enabled; // && !entriesOnly;
            }

            function switchDisplayMode(mode) {
                let darkClass = "<?php echo E4S_DISPLAY_DARK_MODE ?>";
                if (mode === darkClass) {
                    $("#card-base").addClass(darkClass);
                    $(".card_table").addClass(darkClass);
                    $(".bibTable").addClass(darkClass);
                    $(".overOddRow > .overBib > .bibTable").addClass(darkClass + "odd");
                    $(".fieldBibNo").addClass(darkClass);
                    $(".card_table_header").addClass(darkClass);
                    $(".track_data_grid").addClass(darkClass);
                    $(".track_heat_divider").addClass(darkClass);
                    $(".<?php echo E4S_CLASS_SEEDED ?>").addClass(darkClass);
                    $(".card_link_text_span").addClass(darkClass);
                    $(".track_info_row").addClass(darkClass);
                    $(".distance_data_col").addClass(darkClass);
                    $(".overLabelHead").addClass(darkClass);
                    $(".overTable").addClass(darkClass);
                    $(".overOddRow").addClass(darkClass + "odd");
                    $("." + e4sGlobal.class.bibPresent).css("color", "black");
                    $("." + e4sGlobal.class.bibCheckedIn).css("color", "black");
                } else {
                    $("#card-base").removeClass(darkClass);
                    $(".card_table").removeClass(darkClass);
                    $(".bibTable").removeClass(darkClass);
                    $(".overOddRow > .overBib > .bibTable").removeClass(darkClass + "odd");
                    $(".fieldBibNo").removeClass(darkClass);
                    $(".card_table_header").removeClass(darkClass);
                    $(".track_data_grid").removeClass(darkClass);
                    $(".track_heat_divider").removeClass(darkClass);
                    $(".<?php echo E4S_CLASS_SEEDED ?>").removeClass(darkClass);
                    $(".card_link_text_span").removeClass(darkClass);
                    $(".track_info_row").removeClass(darkClass);
                    $(".distance_data_col").removeClass(darkClass);
                    $(".overLabelHead").removeClass(darkClass);
                    $(".overTable").removeClass(darkClass);
                    $(".overOddRow").removeClass(darkClass + "odd");
                    $("." + e4sGlobal.class.bibPresent).css("color", "white");
                    $("." + e4sGlobal.class.bibCheckedIn).css("color", "white");
                }
            }

            function switchDisplayModes() {
                if (getDisplayMode() === '<?php echo E4S_DISPLAY_DARK_MODE ?>') {
                    setDisplayMode('<?php echo E4S_DISPLAY_LIGHT_MODE ?>');
                } else {
                    setDisplayMode('<?php echo E4S_DISPLAY_DARK_MODE ?>');
                }
                // refresh window
                // location.reload();
                cardChange();
            }

            function setDisplayMode(mode) {
                createCookie("<?php echo E4S_DISPLAY_MODE_COOKIE ?>", mode);
            }

            function getDisplayMode() {
                return readCookie("<?php echo E4S_DISPLAY_MODE_COOKIE ?>");
            }

            function goToResults() {
                let competition = getCompetition();
                let url = '/#/results/' + competition.id;

                let eg = getCurrentEventGroup();
                if (eg !== null) {
                    url = '/#/results-read/' + competition.id + "/" + eg.id;
                }
                window.open(url, "E4S_" + competition.id + "_results");
            }

            function getCurrentEventStoredValue() {
                let storageName = getCurrentCardStorageName();
                let val = readCookie(storageName);
                if (val === "undefined") {
                    return null;
                }
                return val;
                // return localStorage.getItem(storageName);
            }
            function resetCurrentEventShown() {
                let storageName = getCurrentCardStorageName();
                createCookie(storageName, "undefined");
            }
            function storeCurrentEventShown() {
                let storageName = getCurrentCardStorageName();
                let value = getCurrentCardInfo();
                createCookie(storageName, value);
            }
            function storeEvent(egId, checkedIn) {
                let storageName = getCurrentCardStorageName();
                let value = getCardInfo(egId, checkedIn);
                createCookie(storageName, value);
            }
            function getEventFromStoredValue(value) {
                if (!value){
                    value = getCurrentEventStoredValue();
                }
                let arr = value.split("_");
                return arr[0];
            }

            function getCheckedInFromStoredValue(value) {
                if (!value){
                    value = getCurrentEventStoredValue();
                }
                let arr = value.split("_");
                return parseInt(arr[1]);
            }

            function getCurrentCardInfo() {
                let currentEg = getCurrentEventGroup();
                if (currentEg === null) {
                    return null;
                }
                let egId = currentEg.id;
                let checkedIn = cardShowsCheckedIn();
                return getCardInfo(egId, checkedIn);
            }
            function getCardInfo(egId, checkedIn){
                let retVal = egId + "_";
                if (checkedIn) {
                    retVal += "1";
                } else {
                    retVal += "0";
                }
                return retVal;
            }

            function getCurrentCardStorageName() {
                let competition = getCompetition();
                let compId = competition.id;
                let storageName = '<?php echo E4S_CURRENT_CARD_STORE_NAME ?>' + "_" + compId;
                return storageName;
            }

            function clearActiveEntries() {
                e4sGlobal.activeEntries = [];
            }

            function getActiveEntries() {
                return e4sGlobal.activeEntries;
            }
            function getActiveEntriesType() {
                return e4sGlobal.activeEntriesType;
            }
            function setActiveEntriesType(type) {
                // false = indiv. true = team
                e4sGlobal.activeEntriesType = type;
            }
            function isActiveEntry(entry) {
                let entityId = getEntryId(entry);
                if (entry.teamid && entry.teamid > 0 ){
                    entityId = entry.teamid;
                }
                return isActiveEntryId(entityId);
            }

            function isActiveEntryId(id) {
                let entries = getActiveEntries();
                return typeof entries[id] === "boolean";
            }

            function isActiveEntityId(entityId) {
                let entries = getActiveEntries();
                let activeType = getActiveEntriesType();
                for (let e in entries) {
                    let entry = getEntryByID(parseInt(e), activeType);
                    let entityId = entry.athleteId;

                    if (entry.athleteId === entityId) {
                        return true;
                    }
                }
                return false;
            }

            function addActiveEntry(id) {
                let entries = getActiveEntries();
                entries[id] = true;
            }

            function displaySeedingNotes(notes) {
                let obj = $("#seedingNotes");
                obj.text(notes);
                if (notes === "") {
                    obj.hide();
                } else {
                    obj.show();
                }
            }

            function displayEGNotes(notes = null) {
                if (notes === null) {
                    notes = getCurrentEventGroup().notes;
                }
                <?php if ( secureAccessToCard() ){ ?>
                if ( photoFinishExport.autoEnabled ){
                    notes = "PF Auto Export " + photoFinishExport.getLastExportTime() + "." + notes;
                }
                if (offLine.hasTbc()) {
                    if (notes !== "") {
                        notes += ". ";
                    }
                    notes += "Results outstanding. Continue to enter results and refresh the page when internet restored.";
                }
                <?php } ?>

                if ( isEventInProgress() ){
                    if (notes !== "") {
                        notes += ". ";
                    }
                    notes += "Event is currently in progress";
                }

                let obj = $("#eventGroupNotes");
                obj.text(notes);
                if (notes === "") {
                    obj.hide();
                } else {
                    obj.show();
                }
            }
            <?php if (secureAccessToCard()) {
                echo '</script>';
                include_once E4S_FULL_PATH . 'reports/output/secureFunctions.php';
                echo '<script>';
            }else{
                ?>
                    let autoShow = false;
                <?php
            }
            ?>
            function isEventInProgress(eg = null){
                if ( eg === null){
                    eg = getCurrentEventGroup();
                }
                if ( eg.options.resultsOpen && eg.options.resultsOpen === true ){
                    // has results ?
                    return getResultsForEG(eg) ? true : false;
                }
                return false;
            }
            function help(){
                $("#cardhelp").toggle();
                $("#card-base").toggle();
                $("#seedingNotes").toggle();
                $("#eventGroupNotes").toggle();
                let sticky = e4s_readStickySettings();
                if ( sticky ){
                    e4s_toggleStickySettings();
                }
                if ( $("#cardhelp").is(":visible") ){
                    window.setTimeout(function(){
                        $("body").on("click",help);
                    },500);
                }else{
                    $("body").off("click");
                }
            }
            function getCardPage() {
                let pageNo = outputReportData.currentPage;
                if (typeof pageNo === "undefined") {
                    pageNo = 1;
                }
                return pageNo;
            }

            function setCardPage(pageNo) {
                outputReportData.currentPage = pageNo;
            }
            function hidePoolCounter(){
                $("#pool_selectorDiv").hide();
            }

            function getFieldCardPool() {
                if ( isPrinting() ){
                    return getCardPage();
                }
                let pageNo = $("#pool_selector :selected").val();
                if (typeof pageNo === "undefined") {
                    return 1;
                }
                return parseInt(pageNo);
            }

            function setPoolCounter(pageCnt) {
                let pageCounter = $("#pool_selector");
                let pageCounterDiv = $("#pool_selectorDiv");
                let currentSelected = parseInt(pageCounter.children("option:selected").val());
                pageCounter.empty();
                let list = '';
                let selected = "";
                for (let pageNumber = 1; pageNumber <= pageCnt; pageNumber++) {
                    selected = "";
                    if (pageNumber === currentSelected) {
                        selected = " selected ";
                    }
                    list += "<option " + selected + " value='" + pageNumber + "'>" + pageNumber + "</option>";
                }
                pageCounter.append(list);
                if (pageCnt < 2) {
                    pageCounterDiv.hide()
                } else {
                    pageCounterDiv.show();
                }
            }

            // Launch Card
            function cardChange() {
                let eg = setCurrentEventGroup();
                if (eg.options.maxathletes === -1) {
                    // its a final, so show entries not checked
                    setCheckedInStatus(false, eg);
                }
                setActiveEntriesForEventGroup(eg);
                showEntryInfoInHeader();
                resetTrackColours();
                if ( autoShow.available ){
                    autoShow.onCardChangePreDisplay(eg);
                }
                hidePoolCounter();
                populateCard(eg);
                if ( autoShow.available ){
                    autoShow.onCardChange();
                }
                e4s_displayStickySettings();
                showActions();
                if (eg !== null) {
                    displayEGNotes(eg.notes);
                }
                switchDisplayMode(getDisplayMode());
                storeCurrentEventShown();

            <?php if ( secureAccessToCard() ){ ?>
                checkSeedingNotes();
                if ( bibNosHaveBeenGenerated() ) {
                    $.contextMenu({
                        selector: '.bibNoMenu',
                        className: 'e4s_context_menu',
                        trigger: 'left',
                        build: function ($trigger, e) {
                            items = setContextMenus($trigger);
                            if ( items === null || isSwitchDialogDisplayed()){
                                return false;
                            }
                            return {
                                callback: function (key, options) {
                                    var target = options.$trigger[0].id;
                                    var targetArr = target.split('_');
                                    var heatNo = parseInt(targetArr[1]);
                                    var laneNo = parseInt(targetArr[2]);
                                    switch (key) {
                                        case "<?php echo E4S_MENU_TOGGLE_PRESENCE ?>":
                                            easySwitchPresent(heatNo, laneNo);
                                            break;
                                        case "<?php echo E4S_MENU_MOVE_ENTRY ?>":
                                            switchTrackPresent(heatNo, laneNo);
                                            break;
                                        case "<?php echo E4S_MENU_REMOVE_ENTRY ?>":
                                            removeEntryFromRace(heatNo, laneNo);
                                            break;
                                        case "Notes":
                                            addCompNotesToAthlete(heatNo, laneNo);
                                            break;
                                        case "MoveRight":
                                            moveHeatHorizontal(heatNo, 1);
                                            break;
                                        case "MoveRightFrom":
                                            moveHeatHorizontal(heatNo, 1, options.items['MoveRightFrom'].laneNo);
                                            break;
                                        case "MoveLeft":
                                            moveHeatHorizontal(heatNo, -1);
                                            break;
                                        case "MoveLeftFrom":
                                            moveHeatHorizontal(heatNo, -1, options.items['MoveLeftFrom'].laneNo);
                                            break;
                                        case "MoveUp":
                                            moveHeatVertical(heatNo, -1);
                                            break;
                                        case "MoveDown":
                                            moveHeatVertical(heatNo, 1);
                                            break;
                                        case "Options":
                                            showEntryOptions(heatNo, laneNo);
                                            break;
                                        case "changeBibNo":
                                            changeBibNo(options.items[key]);
                                            break;
                                    }
                                },
                                items: items
                            }
                        }
                    });
                }
                dragDrop.init();
                <?php } else {?>
                $(".athletepb").hide();
                $(".overPB").hide();
                <?php } ?>
            }
            function changeBibNo(itemOptions){
                e4sRequest("Enter the new Bib Number","Change Bib Number " + itemOptions.fromBibNo, itemOptions.fromBibNo, checkAndUpdateBibNo);
            }
            function checkAndUpdateBibNo(origBibNo, newBibNo){
                if ( origBibNo === newBibNo ){
                    return;
                }
                if (isNaN (Number(newBibNo) ) ) {
                    e4sAlert("Bib Number must be a number", "Error");
                    return;
                }
                let bibNoInt = parseInt(newBibNo);
                let origBibNoInt = parseInt(origBibNo);
                // is number in use
                let entries = getEntries();
                let origEntry = null;
                for (let e in entries) {
                    let entry = entries[e];
                    if (!entry.athlete){
                        entry.athlete = getAthleteById(entry.athleteId);
                    }
                    if (entry.athlete.bibno === bibNoInt) {
                        e4sAlert("Bib Number " + newBibNo + " is already in use.", "Error");
                        return;
                    }
                    if (entry.athlete.bibno === origBibNoInt) {
                        origEntry = entry;
                    }
                }
                if (origEntry === null) {
                    e4sAlert("Original Bib Number " + origBibNo + " not found.", "Error");
                    return;
                }
                let compId = getCompetition().id;
                let athleteId = origEntry.athleteId;
                let url = "<?php echo E4S_BASE_URL ?>bib/change/" + compId + "/" + athleteId + "/" + bibNoInt;
                $.ajax({
                    type: "POST",
                    url: url
                });
            }
            function bibNosHaveBeenGenerated(){
                let bibNos = getBibNos();

                for(let b in bibNos){
                    return true;
                }
                return false;
            }
            function getEntityFromHeatAndLane(heatNo, laneNo){
                let entry = getEntryForHeatLane(heatNo, laneNo);
                if ( entry === null ){
                    return null;
                }
                if ( entry.teamid )  {
                    return getTeamObject(entry);
                }
                return getAthleteById(entry.athleteId);
            }
            function setContextMenus($trigger){
                let triggerId = $trigger.attr("id");
                let arr = triggerId.split("_");
                let raceNo = parseInt(arr[1]);
                let laneNo = parseInt(arr[2]);
                let heatInfo = getHeatInfo(raceNo);
                let teamEntry = false;
                let entity = getEntityFromHeatAndLane(raceNo, laneNo);
                if ( entity === null ){
                    return null;
                }
                let entry = null;
                for(let h in heatInfo.entries){
                    let hEntry = heatInfo.entries[h];
                    let entityId = hEntry.athleteId;
                    if ( hEntry.teamid ){
                        entityId = hEntry.teamid;
                        teamEntry = true;
                    }
                    if ( entityId === entity.id) {
                        entry = hEntry;
                        break;
                    }
                }
                let present = isEntryPresent(entry);
                let presentText = "Mark DNS";
                if (!present){
                    presentText = "Mark Present";
                }
                let bibNo = getBibNo(entry.bibNo, entry);
                let items = {
                    "Title": {
                        name: bibNo + " : " + getNameFromEntity(entity) + " Actions",
                        disabled: true,
                        className: "contextMenuHeader"
                    }
                }
                if (! teamEntry ){
                    items["Notes"] =  {
                        name: "Competition Notes", icon: function () {
                            return 'e4sItem';
                        }
                    }
                }
                let showPresent = false;
                if (isEventGroupTrack()){
                    showPresent = true;
                }else{
                    if ( !teamEntry ){
                        // only show if no results
                        let egResults = getResultsForEG();
                        if ( typeof egResults === "undefined" ){
                            showPresent = true;
                        }else {
                            let athleteId = entity.id;
                            if (typeof egResults[athleteId] === "undefined") {
                                showPresent = true;
                            }else if (!present){
                                showPresent = true;
                            }
                        }
                    }
                }
                if ( showPresent ) {
                    items["<?php echo E4S_MENU_TOGGLE_PRESENCE ?>"] = {
                        name: presentText, icon: function () {
                            return 'e4sItem';
                        }
                    }
                }
                if (isEventGroupTrack()) {
                    items["<?php echo E4S_MENU_MOVE_ENTRY ?>"] = {
                        name: "Move Race/Lane", icon: function () {
                            return 'e4sItem';
                        }
                    }
                }
                if ( showPresent ) {
                    // if we can mark present/unpresent, we can remove the entry
                    items["<?php echo E4S_MENU_REMOVE_ENTRY ?>"] = {
                        name: "Remove Entry", icon: function () {
                            return 'e4sItem';
                        }
                    }
                }
                let viewMode = getViewMode();
                let MoveLeftTxt = "Left";
                let MoveRightTxt = "Right";
                if ( viewMode === "<?php echo E4S_CARDMODE_NONECARD ?>"){
                    MoveLeftTxt = "Up";
                    MoveRightTxt = "Down";
                }
                let titleDisplayed = false;
                let moveHorizDisplayed = false;

                if (isEventGroupTrack()) {
                    if (heatInfo.minLaneNo > 1) {
                        items = clickRaceMenuTitle(items, raceNo);
                        titleDisplayed = true;
                        items.MoveLeft = {
                            name: "Move All " + MoveLeftTxt + " One Lane", icon: function () {
                                return 'e4sItem';
                            }
                        }
                    }
                    if (laneNo > 2) {
                        let bibCell = $("#bib_" + raceNo + "_" + (laneNo - 1) + "_cell");
                        if (bibCell.text().trim() === "") {
                            if (!titleDisplayed) {
                                items = clickRaceMenuTitle(items, raceNo);
                                titleDisplayed = true;
                            }
                            items.MoveLeftFrom = {
                                name: "Move Lanes " + laneNo + " - " + heatInfo.maxLaneNo + " " + MoveLeftTxt + " One Lane",
                                icon: function () {
                                    return 'e4sItem';
                                },
                                laneNo: laneNo
                            };
                            moveHorizDisplayed = true;
                        }
                    }

                    if (heatInfo.maxLaneNo < heatInfo.maxLanes) {
                        if (!titleDisplayed) {
                            items = clickRaceMenuTitle(items, raceNo);
                            titleDisplayed = true;
                        }
                        items.MoveRight = {
                            name: "Move All " + MoveRightTxt + " One Lane", icon: function () {
                                return 'e4sItem';
                            }
                        }
                    }

                    if (laneNo < (heatInfo.maxLanes - 1)) {
                        let bibCell = $("#bib_" + raceNo + "_" + (laneNo + 1) + "_cell");
                        if (bibCell.text().trim() === "") {
                            if (!titleDisplayed) {
                                items = clickRaceMenuTitle(items, raceNo);
                                titleDisplayed = true;
                            }

                            items.MoveRightFrom = {
                                name: "Move Lanes " + heatInfo.minLaneNo + " - " + laneNo + " " + MoveRightTxt + " One Lane",
                                icon: function () {
                                    return 'e4sItem';
                                },
                                laneNo: laneNo
                            };
                            moveHorizDisplayed = true;
                        }
                    }
                }
                // there has to be at least 1 entry as clicked bibNo
                titleDisplayed = false;
                if (isEventGroupTrack()) {
                    if (raceNo > 1) {
                        titleDisplayed = true;
                        items = clickEventMenuTitle(items, raceNo);
                        items.MoveUp = {
                            name: "Switch with Race " + (raceNo - 1),
                            icon: function () {
                                return 'e4sItem';
                            }
                        };
                    }
                    if (!titleDisplayed) {
                        items = clickEventMenuTitle(items, raceNo);
                    }
                    items.MoveDown = {
                        name: "Switch with Race " + (raceNo + 1),
                        icon: function () {
                            return 'e4sItem';
                        }
                    };
                }
                <?php if ( hasSecureAccess(false) ){ ?>
                if (!teamEntry) {
                    items = clickEntryMenuTitle(items, raceNo);
                    items.Options = {
                        name: "Entry Options", icon: function () {
                            return 'e4sItem';
                        }
                    }
                    items.changeBibNo = {
                        name: "Change Bib No",
                        fromBibNo: bibNo,
                        icon: function () {
                            return 'e4sItem';
                        }
                    }
                }
                <?php } ?>
                return items;
            }
            function clickEntryMenuTitle(items, heatNo) {
                items.sepd100 = "---------";
                items.Title102 = {name: "Entry Actions", disabled:true, className:"contextMenuHeader"};
                return items;
            }
            function clickRaceMenuTitle(items, heatNo) {
                items.sepd10 = "---------";
                items.Title2 = {name: "Race " + heatNo + " Actions", disabled:true, className:"contextMenuHeader"};
                return items;
            }
            function clickEventMenuTitle(items, heatNo) {
                items.sepd20 = "---------";
                items.Title3 = {name: "Event Actions", disabled:true, className:"contextMenuHeader"};
                return items;
            }
            function getHeatInfo(heatNo){
                let eg = getCurrentEventGroup();
                let entries = getEntriesForEg(eg);
                let maxLanes = getLaneCount(eg);
                let checkInInUse = isCheckInInUse() && isEventGroupTrack(eg);
                let heatInfo = {
                    egId: eg.id,
                    minLaneNo: maxLanes,
                    maxLaneNo: 0,
                    maxLanes: maxLanes,
                    entries: []
                };
                for (let e in entries){
                    let entry = entries[e];
                    let process = true;
                    if ( checkInInUse && !isEntryCheckedIn(entry)){
                        process = false;
                    }
                    if ( process) {
                        if (getEntryRaceNo(entry) === heatNo) {
                            heatInfo.entries.push(entry);
                            let laneNo = getEntryLaneNo(entry);
                            if (laneNo < heatInfo.minLaneNo) {
                                heatInfo.minLaneNo = laneNo;
                            }
                            if (laneNo > heatInfo.maxLaneNo) {
                                heatInfo.maxLaneNo = laneNo;
                            }
                        }
                    }
                }
                return heatInfo;
            }
            function showEntryInfoInHeader() {
                setEntryInfoOnCard(false);
            }
            function keepCheckingEntryInfo() {
                setEntryInfoOnCard(true);
            }
            function isEventAHeightEvent(eventGroup = null){
                if ( eventGroup === null ){
                    eventGroup = getCurrentEventGroup();
                }
                return eventGroup.uomtype === "<?php echo E4S_UOM_HEIGHT ?>";
            }
            function isEventATeamEvent(eventGroup){
                return eventGroup.isTeamEvent;
            }
            function setEntryInfoOnCard(cardChangeAllowed) {
                let comp = getCompetition();
                let cardsAvailableFrom = dateCardsAreAvailable();
                let cardStatus = "";
                let cardStatusTime = "";
                let milliSecondsForRefresh = 0;
                let nowMilliSeconds = new Date().valueOf();
                let eventGroup = getCurrentEventGroup();

                if (eventGroup !== null && !scheduleOnlyEvent(eventGroup)) {
                    if (isCheckInEnabled() && !isEventATeamEvent(eventGroup)) {
                        // setCheckInDatesForEventGroup(eventGroup);
                        let checkinOpensDT = eventGroup.checkinOpensDT;
                        let checkinClosesDT = eventGroup.checkinClosesDT;
                        let checkinStatus = getCheckInStatus(eventGroup);
                        switch (checkinStatus) {
                            case <?php echo E4S_CHECKIN_NOT_OPEN ?>:
                                cardStatus = "Check-in opens ";
                                milliSecondsForRefresh = nowMilliSeconds - checkinOpensDT.valueOf();
                                let openArr = checkinOpensDT.toLocaleTimeString().split(":");
                                cardStatusTime = checkinOpensDT.toDateString() + " " + openArr[0] + ":" + openArr[1];
                                setCheckedInStatus(false, eventGroup);
                                break;
                            case <?php echo E4S_CHECKIN_OPEN ?>:
                                milliSecondsForRefresh = checkinClosesDT.valueOf() - nowMilliSeconds;
                                cardStatusTime = checkinClosesDT.getHours() + ":";
                                let mins = checkinClosesDT.getMinutes();
                                if (mins < 10) {
                                    cardStatusTime = cardStatusTime + "0";
                                }
                                cardStatusTime = cardStatusTime + mins;
                                cardStatus = "Check-in closes at ";
                                break;
                            case <?php echo E4S_CHECKIN_CLOSED ?>:
                                cardStatus = "Check-in closed ";

                                let closeArr = checkinClosesDT.toLocaleTimeString().split(":");
                                cardStatusTime = checkinClosesDT.toDateString() + " " + closeArr[0] + ":" + closeArr[1];
                                setCheckedInStatus(true, eventGroup);
                                if (cardChangeAllowed) {
                                    cardChange();
                                }
                                break;
                        }
                    } else {
                        // check if Entries have closed
                        let entriesOpensDT = parseISOString(comp.entriesOpen);
                        let entriesClosesDT = parseISOString(cardsAvailableFrom);
                        if (nowMilliSeconds < entriesOpensDT.valueOf()) {
                            cardStatus = "Entries opens ";
                            let openArr = entriesOpensDT.toLocaleTimeString().split(":");
                            cardStatusTime = entriesOpensDT.toDateString() + " " + openArr[0] + ":" + openArr[1];
                        } else {
                            if (nowMilliSeconds > entriesClosesDT.valueOf()) {
                                cardStatus = "Entries Closed ";
                            } else {
                                milliSecondsForRefresh = entriesClosesDT.valueOf() - nowMilliSeconds;
                                cardStatus = "Entries Close ";
                            }
                            let openArr = entriesClosesDT.toLocaleTimeString().split(":");
                            cardStatusTime = entriesClosesDT.toDateString() + " " + openArr[0] + ":" + openArr[1];
                        }
                    }
                }
                let cardStatusObj = $("#cardEntryInfoStatus");
                cardStatusObj.text(cardStatus);
                let cardStatusTimeObj = $("#cardEntryInfoTime");
                cardStatusTimeObj.text(cardStatusTime);

                if (e4sGlobal.checkEntryInfoDate) {
                    clearTimeout(e4sGlobal.checkEntryInfoDate);
                }
                if (milliSecondsForRefresh > 0) {
                    // refresh at the appropriate time
                    e4sGlobal.checkEntryInfoDate = setTimeout(keepCheckingEntryInfo, milliSecondsForRefresh);
                }
                displayEventGroupSeedingInfo(eventGroup);
            }

            function displayEventGroupSeedingInfo(eventGroup) {
                let obj = $("#cardEntryInfoSeedingIndicator");
                let text = "";
                if (isEventGroupSeeded(eventGroup)) {
                    text = "- Seeded";
                } else {
                    text = "- Awaiting Seeding";
                }
                if ( isEventGroupTrack(eventGroup) ) {
                    text += " (" + getEventGroupSeedType(eventGroup) + ")";
                }
                obj.text(text);
                $("#markSeedingComplete").hide();
            }

            function getEventGroupSeedType(eventGroup){
                let options = eventGroup.options;
                let seed = options.seed;
                if (seed.type === '<?php echo E4S_SEED_HEAT ?>'){
                    return 'Heats';
                }
                if (seed.type === '<?php echo E4S_SEED_OPEN ?>'){
                    return 'Open/Arrow';
                }
            }
            function isEventGroupSeeded(eventGroup) {
                if (eventGroup === null) {
                    return false;
                }
                return eventGroup.options.seed.seeded;
            }

            function setEventGroupSeeded(eventGroup, seeded) {
                eventGroup.options.seed.seeded = seeded;
                let currentEg = getCurrentEventGroup();
                if (getEventGroupId(currentEg) === getEventGroupId(eventGroup)) {
                    displayEventGroupSeedingInfo(eventGroup);
                }
            }

            function resetTrackColours() {
                $("[e4s_ToBeReset]").css("background-color", "white").css("color", "black");
            }

            function showTitle(title) {
                let comp = getCompetition();
                if (title === "Track/Field Cards") {
                    let eg = getCurrentEventGroup();
                    if (eg !== null) {
                        title = getEventGroupTitle(eg);
                    }
                }
                if (title) {
                    document.title = comp.id + " : " + title;
                }
            }
            function displayMenuItems(currentEg) {
                let curEventNo = getEventNoFromEventGroup(currentEg);
                if (curEventNo < 0) {
                    $("#cardActions").hide();
                    return false;
                }
                $("#cardActions").show();
                return true;
            }
            function showActions() {
                let currentEg = getCurrentEventGroup();
                if (currentEg === null) {
                    $("#cardActions").hide();
                    return;
                }
                if (displayMenuItems(currentEg)) {
                    showTitle("Track/Field Cards");
                    displayToggleViewModeMenuItem();
                    <?php if ( secureAccessToCard() ){ ?>
                    displaySeedingMenuItem();
                    displayEntriesResetMenuItem();
                    showResultsEntryMenuItem();
                    setSeedingMonitorMenuItem();
                    displayFieldCardSettingsMenuItem(currentEg);
                    displayEventGroupSettingsMenuItem(currentEg);
                    <?php } ?>
                }
                checkSourceTarget();
            }

            function displayMenuAction(name, show) {
                if (show) {
                    $("#" + name).show();
                    $("#" + name + "HR").show();
                } else {
                    $("#" + name).hide();
                    $("#" + name + "HR").hide();
                }
            }
            function displayFieldCardSettingsMenuItem(eventGroup) {
                displayMenuAction("fieldCardSettingsAction", false);
                let display = false;
                if ( isEventAHeightEvent(eventGroup) ) {
                    display = !heightCard.hasAnyEntryGotStartHeight();
                }
                displayMenuAction("progressionsAction", display);
             }
            function displayEventGroupSettingsMenuItem(eventGroup) {
                displayMenuAction("eventGroupSettings", showCardSettings(eventGroup));
            }
            function showCardSettings(eventGroup){
                if (isEventGroupTrack(eventGroup)) {
                    return true;
                }
                if ( isEventAHeightEvent(eventGroup) ){
                    return false;
                }
                return true;
            }
            function setOutputData(section, data) {
                outputReportData[section] = data;
            }
            function getOutputData(section) {
                return outputReportData[section];
            }
            function getClubComps() {
                return getOutputData("clubCompClubs");
            }
            function getCompetition() {
                return getOutputData("competition");
            }
            function getCompEvents() {
                return getOutputData("compEvents");
            }
            function getEvents() {
                return getOutputData("events");
            }
            function getEntries() {
                return getOutputData("entries");
            }
            function setEntries(entries) {
                setOutputData("entries", entries);
            }
            function getTeams() {
                return getOutputData("teams");
            }
            function getBibNos() {
                return getOutputData("bibNos");
            }
            function getAthletes() {
                return getOutputData("athletes");
            }
            function getAgeGroups() {
                return getOutputData("agegroups");
            }
            function getEventGroups() {
                return getOutputData("eventgroups");
            }
            function getResults() {
                return getOutputData("results");
            }
            function getResultsForAthlete(eventNo, athleteId){
                let results = getResults()[eventNo];
                if ( results ){
                    return results[athleteId];
                }
                return null;
            }
            function getResultsForEG(eg = null){
                if ( eg === null ){
                    eg = getCurrentEventGroup();
                }
                let results = getResults()[eg.eventno];
                return results;
            }
            function getRankings() {
                return getOutputData("rankings");
            }
            function getTreeData() {
                return getOutputData("treedata");
            }

            function getEventGroupById(egId) {
                let eventGroups = getEventGroups();
                let eventGroup = null;
                for (let eg in eventGroups) {
                    if (eventGroups[eg].id === egId) {
                        eventGroup = eventGroups[eg];
                    }
                }
                return eventGroup;
            }

            const rankObj = {
                getEventForAthlete: function(athleteId, eventId){
                    let athleteRanks = getRankings();
                    let ranking = athleteRanks[athleteId];
                    for(let r in ranking){
                        if ( ranking[r].eventId === eventId ){
                            return ranking[r].rank;
                        }
                    }
                    return 0;
                },
                updateDisplay: function(rankValue) {
                    rankObj.setValue(rankValue);

                    $(".pbrank").each(
                        function(index,item){
                            var node = $(this);
                            if (parseInt(node.attr("pbrank")) < rankValue){
                                node.show()
                            }else{
                                node.hide();
                            }
                        }
                    )
                },
                getDisplay: function(rank){
                    return "<span class='pbrank' pbrank=" + rank + " style='display:none;'> UK:" + rank + "</span>";
                },

                setValue: function (value) {
                    createCookie("<?php echo E4S_SHOW_RANKING_COOKIE ?>", value);
                },
                getValue: function () {
                    let value = readCookie("<?php echo E4S_SHOW_RANKING_COOKIE ?>");
                    if (value === null || isNaN(value)) {
                        value = 0;
                    }
                    return parseInt(value);
                },
                setDisplay: function(){
                    let divName = "e4sRanking";
                    let divHTML = "<div id='" + divName + "'>";

                    divHTML += '<label for="maxRank">Max Ranking to Show :</label>';
                    divHTML += '<input type="text" id="maxRank" readonly style="border:0; color:#f6931f; font-weight:bold;">';
                    divHTML += '<div id="slider-rank" style="height:16px;margin-top: 10px;"></div>';

                    $("#" + divName).remove();
                    $("#dialogEGOptions").html(divHTML);

                    let dialog = $("#" + divName).dialog({
                        resizable: false,
                        title: "Show UK Rankings",
                        height: "auto",
                        width: 400,
                        modal: true,
                        open: function ( event, ui){
                            rankObj.rankSliderInit();
                        },
                    });
                    let buttons = [
                        {
                            text: "Set",
                            class: "resultButton",
                            click: function () {
                                rankObj.updateDisplay(parseInt($( "#maxRank" ).val()));
                                $(this).dialog("close");
                            }
                        },
                        {
                            text: "Clear",
                            class: "resultButton",
                            click: function () {
                                rankObj.setValue(0);
                                $(this).dialog("close");
                            }
                        }
                    ];
                    dialog.dialog("option", "buttons", buttons);
                    positionDialog(dialog);
                },
                rankSliderInit: function(){
                    $( "#slider-rank" ).slider({
                        range: "min",
                        min: 0,
                        max: 200,
                        step: 10,
                        value: rankObj.getValue(),
                        slide: function( event, ui ) {
                            $( "#maxRank" ).val( ui.value );
                        }
                    });
                    $( "#maxRank" ).val( $( "#slider-rank" ).slider( "value" ) );
                }
            }

            function getEventGroup(eventNo) {
                let eventGroups = getEventGroups();
                let eventGroup = null;
                for (let eg in eventGroups) {
                    if (getEventNoFromEventGroup(eventGroups[eg]) === eventNo) {
                        eventGroup = eventGroups[eg];
                        break;
                    }
                }
                return eventGroup;
            }

            function getEventGroupTitle(eventGroup) {
                return eventGroup.typeno + "-" + eventGroup.event;
            }
            function getEntryId(entry){
                return entry.entryId;
            }
            function getAthleteId(entry){
                return entry.athleteId;
            }
            function getEntriesFromEG(eventGroup) {
                let eg = null;
                if (typeof eventGroup === "undefined") {
                    eventGroup = getCurrentEventGroup();
                }
                let egId = getEntriesFromId(eventGroup);
                if (egId > 0){
                    eg = getEventGroupById(egId);
                }
                return eg;
            }
            function populateEntriesFrom(eventGroup){
                let srcEg = getEntriesFromEG(eventGroup);
                if ( srcEg === null ){
                    return;
                }
                let getEntries = false;

                // eventGroup.entries = getEntriesForEgId(srcEgId);
                if (typeof srcEg.entries === "undefined"){
                    srcEg.entries = getEntriesForEg(srcEg);
                }

                if (typeof srcEg.entries === "undefined") {
                    return;
                }
                if (srcEg.entries.length === 0 ){
                    eventGroup.entries = [];
                    return;
                }else{
                    eventGroup.entries = srcEg.entries;
                }

                // Use less than so its possible to add to this child indiv event of a multi event
                if ( typeof eventGroup.entries === "undefined" || eventGroup.entries.length < srcEg.entries.length ) {
                    <?php if ( secureAccessToCard() ){ ?>
                        showPleaseWait(true,"Loading Combined Event Entries");
                        addEntriesFromEgId(srcEg.id);
                        location.reload();
                    <?php } ?>
                }
            }

            function getEntriesFromId(eventGroup){
                if (eventGroup.options.entriesFrom) {
                    return eventGroup.options.entriesFrom.id;
                }
                return 0;
            }
            function populateCard(eventGroup) {
                if (eventGroup === null) {
                    return;
                }
                if (getEntriesFromId(eventGroup) > 0){
                    populateEntriesFrom(eventGroup);
                }
                let athletesAdded = false;
                switch (getEventGroupType(eventGroup)) {
                    case '<?php echo E4S_EVENT_TRACK ?>':
                    case '<?php echo E4S_EVENT_ROAD ?>':
                    case '<?php echo E4S_EVENT_MULTI ?>':
                    case '<?php echo E4S_EVENT_XCOUNTRY ?>':
                        athletesAdded = populateTrackCard(eventGroup);
                        break;
                    case '<?php echo E4S_EVENT_FIELD ?>':
                        athletesAdded = displayFieldCard(eventGroup);
                        break;
                }
                return athletesAdded;
            }

            function formatDate(date, format) {
                /*
    yy = short year
    yyyy = long year
    M = month (1-12)
    MM = month (01-12)
    MMM = month abbreviation (Jan, Feb ... Dec)
    MMMM = long month (January, February ... December)
    d = day (1 - 31)
    dd = day (01 - 31)
    ddd = day of the week in words (Monday, Tuesday ... Sunday)
    E = short day of the week in words (Mon, Tue ... Sun)
    D - Ordinal day (1st, 2nd, 3rd, 21st, 22nd, 23rd, 31st, 4th...)
    h = hour in am/pm (0-12)
    hh = hour in am/pm (00-12)
    H = hour in day (0-23)
    HH = hour in day (00-23)
    mm = minute
    ss = second
    SSS = milliseconds
    a = AM/PM marker
    p = a.m./p.m. marker
                 */
                return $.format.date(date, format);
            }


            // ==============================================================================================================================
            // HEIGHT CARD Code
            // ==============================================================================================================================
            function showFieldHeightCard() {
                let baseCardHTML = $("#heightTableDiv").html();
                $("#card-base").html(baseCardHTML);
                heightCard.displayRows();
            }

            // ==============================================================================================================================
            // DISTANCE CARD CODE
            // ==============================================================================================================================
            function showFieldDistanceCard() {
                let baseCardHTML = $("#distanceTableDiv").html();
                $("#card-base").html(baseCardHTML);
            }

            function getResultsForTrialForEdit(trial) {
                for (let row = 1; row <= 3; row++) {
                    getFieldResult("<?php echo E4S_RESULT_PREFIX ?>" + trial, row);
                }
            }

            function getResultsForAthleteForEdit(row) {
                getFieldResult("t1", row);
                getFieldResult("t2", row);
                getFieldResult("t3", row);
                getFieldResult("3pos", row);
                getFieldResult("t4", row);
                getFieldResult("t5", row);
                getFieldResult("t6", row);
                getFieldResult("pos", row);
            }

            function returnFieldResult(key, row) {
                let addM = "m";
                let m = $("#field_" + key + addM + "_" + row).text().trim();
                let dot = "";
                let c = "";
                if (!isValValidOption(m)) {
                    // must be a number
                    if (isNaN(parseInt(m))) {
                        m = "";
                    } else {
                        if (m !== "") {
                            dot = ".";
                        }
                        c = $("#field_" + key + "c_" + row).text().trim();
                    }
                }
                return m + dot + c;
            }

            function getFieldResult(key, row) {
                $("#" + key).val(returnFieldResult(key, row));
            }

            function processSocketEvent(data) {
                if (data.domain !== '<?php echo E4S_CURRENT_DOMAIN ?>') {
                    // ignore. not for this domain
                    return;
                }
                delete data.domain; // dont need it now its checked ok
                if (data.comp.id !== getCompetition().id) {
                    return; // not for this competition
                }
                let refresh = false;
                // updatedata store
                switch (data.action) {
                    case "<?php echo R4S_SOCKET_FIELD_RESULT ?>":
                        refresh = updateFieldResult(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_ATHLETE_BIBNO ?>":
                        refresh = updateAthleteBibNo(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_ENTRIES_CHECKIN ?>":
                        refresh = updateCheckinOnDataStore(data);
                        break;
                    case "<?php echo R4S_SOCKET_TRACK_MOVE ?>":
                        refresh = updateTrackMoveOnDataStore(data);
                        break;
                    case "<?php echo R4S_SOCKET_ENTRIES_PRESENT ?>":
                        refresh = updateEntriesOnDataStore(data);
                        break;
                    case "<?php echo R4S_SOCKET_SEED_ENTRIES ?>":
                        refresh = updateSeedingOnDataStore(data);
                        break;
                    case "<?php echo R4S_SOCKET_ENTRIES_ADD_ATHLETE ?>":
                        addAthleteForEventFromCard(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_ENTRIES_ADD_ENTRY ?>":
                        refresh = addEntryFromEntries(data.payload);
                        break;
                    case "<?php echo R4S_ADD_NOTES ?>":
                        refresh = addNotesToAthlete(data.payload.data);
                        break;
                    case "<?php echo R4S_CONFIRM_HEAT ?>":
                    case "<?php echo R4S_CONFIRM_HEAT_NO_SCOREBOARD ?>":
                        refresh = updateConfirmedHeat(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_ENTRIES_EG_MESSAGE ?>":
                        refresh = toastMsg(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_EVENT_QUALIFICATIONS ?>":
                        refresh = addQualifyEntries(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_SEED_CONFIRMED ?>":
                        refresh = confirmSeedings(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_ENTRIES_COPIED ?>":
                        refresh = addCopiedEntries(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_EVENT_GROUP_OPTIONS ?>":
                        refresh = updateEventGroupOptions(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_ENTRY_PB ?>":
                        refresh = updateEntryPerformance(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_AUTOSHOW_ASK ?>":
                         if ( autoShow.available ){
                             autoShow.askRequested();
                         }
                        break;
                    case "<?php echo R4S_SOCKET_AUTOSHOW_RESPOND ?>":
                        if ( autoShow.available ){
                            autoShow.addToList(data.payload);
                        }
                        break;
                    case "<?php echo R4S_SOCKET_AUTOSHOW_DISPLAY ?>":
                        if ( autoShow.available ){
                            autoShow.displayRequested(data.payload);
                        }
                        break;
                    case "<?php echo R4S_SOCKET_CALLROOM ?>":
                        updateCallRoom(data.payload.data);
                        break;
                    case "<?php echo R4S_SOCKET_EVENT_GROUP_MERGE ?>":
                        refresh = e4sMergeEvents(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_UPDATE_BIBINFO ?>":
                        refresh = updateBibInfo(data.payload.bibInfo);
                        break;
                    case "<?php echo R4S_SOCKET_TRACK_MOVEHORIZ ?>":
                        refresh = moveEntriesToAnotherLane(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_SETPF ?>":
                        setPFType(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_PF_RESPONSE ?>":
                        photoFinishExport.enablePFDirectButton(data.payload.data);
                        break;
                    case "<?php echo R4S_SOCKET_TRACK_MOVEVERT ?>":
                        refresh = moveEntriesVertically(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_EVENT_RESULTS_STATE ?>":
                        refresh = updateEventResultsState(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_SEED_DROPPED ?>":
                        updateDrapDropSeedings(data.payload);
                        refresh = true;
                        break;
                    case "<?php echo R4S_SOCKET_FIELD_STANDS ?>":
                        refresh = heightCard.updateEntryStands(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_FIELD_STARTHEIGHT ?>":
                        refresh = heightCard.updateEntryStartHeight(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_REFRESH_REQUIRED ?>":
                        refreshRequested(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_ENTRY_CANCEL ?>":
                        refresh = cancelEntry(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_EVENT_NEXT_UP ?>":
                        refresh = fieldNextUpReceived(data.payload);
                        break;
                    case "<?php echo R4S_SOCKET_CLEAR_HEIGHT_RESULTS ?>":
                        refresh = heightCard.updateClearedResults(data.payload);
                        break;
                }
                let entryId = 0;
                let entry = 0;
                // check if ui changes required
                switch (data.action) {
                    case "<?php echo R4S_SOCKET_ENTRIES_PRESENT ?>":
                        entryId = data.payload.entryId;
                        if (isActiveEntryId(entryId)) {
                            refresh = true;
                        }
                        break;
                    case "<?php echo R4S_SOCKET_ENTRIES_ADD_ATHLETE ?>":
                        entryId = data.payload.entryId;
                        entry = getEntryByID(entryId);
                        let currentEventNo = getSelectedEventNo();
                        let eventNo = getEventNoFromEventGroup(data.payload.eventGroup);
                        if (currentEventNo === eventNo) {
                            addNewAthleteToCard(entry);
                            refresh = true;
                        }
                        break;
                <?php if ( secureAccessToCard() ){ ?>
                    case "<?php echo R4S_SOCKET_TRACK_MOVE ?>":
                        if (getRefreshSeeding()) {
                            getAthletesInOrderAndSeed(getSeedingSort());
                        }
                        break;
                <?php } ?>
                }

                // post CardRefresh
                <?php if ( secureAccessToCard() ){ ?>
                switch (data.action) {
                    case "<?php echo R4S_SOCKET_EVENT_GROUP_OPTIONS ?>":
                        if (getRefreshSeeding()) {
                            getAthletesInOrderAndSeed(getSeedingSort());
                        }
                        break;
                }
                <?php } ?>
                if (refresh) {
                    cardChange();
                    <?php if ( secureAccessToCard() ){ ?>
                    switch (data.action) {
                        case "<?php echo R4S_SOCKET_SEED_DROPPED ?>":
                            dragDrop.init();
                            break;
                    }
                    <?php } ?>
                }
            }
            function getNextUpForEgId(egId){
                let nextUp = getNextUp();
                if ( ! nextUp[egId] ){
                    nextUp[egId] = {
                        "athleteId": 0,
                        "attempt": 0
                    };
                }else{
                    if ( typeof nextUp[egId] === "number" ){
                        nextUp[egId] = {
                            "athleteId": nextUp[egId],
                            "attempt": 0
                        };
                    }
                }
                return nextUp[egId];
            }
            function getNextUp(){
                let compId = getCompetition().id;
                // get local storage
                let nextUp = localStorage.getItem("nextUp_" + compId);
                if ( nextUp === null ) {
                    nextUp = {};
                }else{
                    nextUp = JSON.parse(nextUp);
                }
                return nextUp;
            }
            function saveNextUp(egId, athleteId, attempt = 0){
                let curPayload = getNextUp();
                if ( athleteId === 0 ){
                    athleteId = null;
                    // remove from list
                    delete curPayload[egId];
                }else{
                    curPayload[egId] = {
                        "athleteId": athleteId,
                        "attempt": attempt
                    };
                }
                let compId = getCompetition().id;
                localStorage.setItem("nextUp_" + compId, JSON.stringify(curPayload));
            }
            function fieldNextUpReceived(payload){
                let egId = payload.event.id;
                let curEg = getCurrentEventGroup();
                let athleteId = payload.athlete.id ? payload.athlete.id : 0;
                let attempt = payload.athlete.attempt ? payload.athlete.attempt : 0;
                saveNextUp(egId, athleteId, attempt);
                if ( curEg.id === egId ){
                    displayCurrentAthlete(curEg, athleteId, attempt);
                }
                return false;
            }

            function cancelEntry(payload){
                let entryId = payload.entryId;
                let refresh = false;

                cancelEntryById(entryId);
                if (isActiveEntryId(entryId)) {
                    refresh = true;
                }
                return refresh;
            }
            function refreshRequested(payload){
                if (payload.device !== socket_deviceKey ){
                    markRefreshRequired();
                }
            }
            function updateEventResultsState(data){
                let eventGroup = getEventGroupById(data.egId);
                eventGroup.options.resultsOpen = data.open;
                displayEGNotes();
                if ( getEventGroupId(getCurrentEventGroup()) === data.egId ){
                    return true;
                }
                return false;
            }
            function updateDrapDropSeedings(seedingInfo){
                showPleaseWait(false);
                for(let s in seedingInfo){
                    let seedingEntry = seedingInfo[s];
                    let isTeam = seedingEntry.teamId ? true : false;
                    let entry = getEntryByID(getEntryId(seedingEntry), isTeam);
                    setHeatNo(entry, getRaceNo(seedingEntry));
                    setLaneNo(entry, getLaneNo(seedingEntry));
                    setHeatNoCheckedIn(entry, getRaceNoCheckedIn(seedingEntry));
                    setLaneNoCheckedIn(entry, getLaneNoCheckedIn(seedingEntry));
                    setSeeded(entry, true);
                }
            }
            function moveEntriesVertically(payload){
                let heatNo = payload.heatNo;
                let egId = payload.egId;
                let factor = payload.factor;
                let targetHeatNo = heatNo + factor;
                let checkedIn = payload.checkedIn;
                let currentEgId = getCurrentEventGroup().id;

                let entries = getEntriesForEgId(egId);
                for(let e in entries) {
                    let entry = entries[e];
                    let entryHeatInfo = entry.heatInfo;
                    let entryHeatNo = getRaceNo(entry);
                    if (checkedIn){
                        entryHeatNo = getRaceNoCheckedIn(entry);
                    }
                    if (entryHeatNo === heatNo) {
                       entryHeatNo = targetHeatNo;
                    } else if(entryHeatNo === targetHeatNo) {
                        entryHeatNo = heatNo;
                    }
                    if (checkedIn){
                        setHeatNoCheckedIn(entry, entryHeatNo);
                    }else{
                        setHeatNo(entry, entryHeatNo);
                    }
                }

                return currentEgId === egId;
            }
            function setPFType(payload){
                let cOptions = getCompetition().options;
                cOptions.pf.type = payload.data;
            }
            function e4sMergeEvents(data){
                let curEg = getCurrentEventGroup();
                let toEg = getEventGroupById(data.toEgId);
                let fromEg = getEventGroupById(data.fromEgId);
                let entries = getEntriesForEgId(data.fromEgId);
                let egArr = getEventGroups();
                for(let e in entries){
                    let entry = entries[e];
                    entry.event = toEg.event;
                    entry.eventGroup = toEg.eventGroup;
                    entry.eventGroupId = toEg.eventGroupId;
                    entry.eventno = toEg.eventno;
                    clearHeatInfo(entry);
                    entry.heatInfo.position = 0;
                }
                if( curEg.id === data.fromEgId){
                    next_selector();
                }

                for(let sub in egArr){
                    let eg = egArr[sub];
                    if (eg.id === data.fromEgId){
                        delete egArr[sub];
                        break;
                    }
                }

                setEventChoices();
                if( curEg.id === data.fromEgId || curEg.id === data.toEGId){
                    return true;
                }
                return false;
            }
            function moveEntriesToAnotherLane(payload){
                let factor = payload.factor;
                let checkedIn = payload.checkedIn;
                let refresh = null;
                let isTeam = false;
                for(let e in payload.entryInfo){
                    let entryInfo = payload.entryInfo[e];
                    let entityId = entryInfo.entryId;
                    isTeam = false;
                    if (entityId === 0 ){
                        entityId = entryInfo.teamId;
                        isTeam = true;
                    }
                    if ( refresh === null){
                        refresh = isActiveEntityId(entityId);
                    }

                    let entry = getEntryByID(entityId, isTeam);
                    if ( checkedIn ){
                        setLaneNoCheckedIn(entry,getLaneNoCheckedIn(entry) + factor);
                    }else{
                        setLaneNo(entry,getLaneNo(entry) + factor);
                    }
                }
                return refresh;
            }
            function updateBibInfo(payload){
                let athleteId = payload.athleteId;
                let notes = payload.notes;

                setCompNotesForAthleteId(athleteId, notes);

                return isActiveEntityId(athleteId);
            }
            function updateCallRoom(payload){
                setCallRoomStatusInOptions(payload.callRoom);
            }
            function getFloatDecimals(val, numOfDecimals) {
                val = getDecimals(val, numOfDecimals);
                return parseFloat(val);
            }
            function getDecimals(val, numOfDecimals) {
                if (typeof val === "undefined") {
                    return "";
                }
                if (typeof val === "string") {
                    val = val.replace("#", "");
                    if ( val.indexOf(":") !== -1 ){
                        let valArr = val.split(":");
                        let minSec = parseFloat(valArr[1]).toFixed(numOfDecimals);
                        if ( minSec < 10){
                            minSec = "0" + minSec;
                        }
                        return valArr[0] + ":" + minSec;
                    }
                }
                return parseFloat(val).toFixed(numOfDecimals);
            }

            function updateEntryPerformance(data) {
                let entry = getEntryByID(data.entryId);
                entry.pb = data.value;
                return true;
            }

            function updateFieldResult(data) {
                <?php if ( secureAccessToCard() ){ ?>
                offLine.clearResult(data);
                <?php } ?>
                let currentEg = getCurrentEventGroup();
                let currentEventNo = getEventNoFromEventGroup(currentEg);
                let refresh = false;
                let results = getResults();
                for (let eventNo in data) {
                    if ( isNaN(eventNo) ){
                        continue;
                    }
                    eventNo = parseInt(eventNo);
                    let eventResults = results[eventNo];
                    let eg = getEventGroup(eventNo);
                    if (!eventResults) {
                        results[eventNo] = {};
                        eventResults = results[eventNo];
                    }
                    let newEventResults = data[eventNo];
                    for (let athleteId in newEventResults) {
                        if (currentEventNo === eventNo) {
                            if (isActiveEntityId(parseInt(athleteId))) {
                                refresh = true;
                            }
                        }
                        let newAthleteResults = newEventResults[athleteId];
                        let athleteResults = eventResults[athleteId];
                        if (!athleteResults) {
                            eventResults[athleteId] = {};
                            athleteResults = eventResults[athleteId];
                        }
                        // if height result
                        if ( isEventAHeightEvent(eg) ){
                            if ( typeof eventResults[athleteId] === "undefined" ){
                                eventResults[athleteId] = {};
                            }
                            for(let key in newAthleteResults){
                                if ( key === "athlete" || key === "<?php echo E4S_RESULT_PARAMS ?>" ){
                                    continue;
                                }
                                athleteResults[key] = newAthleteResults[key];
                            }
                            //
                            // let resultHeight = getDecimals(newAthleteResults.height, 2);
                            // if ( newAthleteResults.result === '' ) {
                            //     delete eventResults[athleteId][resultHeight + ":" + newAthleteResults.trial];
                            //     let deleteAthlete = JSON.stringify(eventResults[athleteId]) === '{}';
                            //     if ( deleteAthlete ){
                            //         delete eventResults[athleteId];
                            //     }
                            // }else{
                            //     eventResults[athleteId][resultHeight + ":" + newAthleteResults.trial] = newAthleteResults.result;
                            // }
                        }else {
                            for (let key in newAthleteResults) {
                                if (key.toLowerCase() !== "<?php echo E4S_RESULT_PARAMS ?>") {
                                    athleteResults[key] = newAthleteResults[key];
                                }
                            }
                        }
                    }
                    populateHeaderStart(eg);
                }
                return refresh;
            }

            function updateAthleteBibNo(data) {
                let athleteId = data.athleteId;
                let bibNo = data.bibNo;
                return updateBibNo(athleteId, bibNo);

            }

            function updateBibNo(athleteId, newBibNo){
                let athlete = getAthleteById(athleteId);
                let refresh = false;
                if (athlete) {
                    let bibNos = getBibNos();
                    let oldBib = bibNos[athlete.bibno];
                    let newBib;
                    if ( oldBib){
                        newBib = JSON.parse(JSON.stringify(oldBib));
                        delete bibNos[athlete.bibno];
                    }else{
                        newBib = {
                            athleteId: athleteId,
                            collected: true,
                            compNotes: '',
                            notes: ''
                        };
                    }

                    athlete.bibno = newBibNo;
                    bibNos[newBibNo] = newBib;
                    refresh = isActiveEntityId(athleteId);
                }
                return refresh;
            }
            function updateEventGroupOptions(data) {
                let egId = data.egId;
                let eg = getEventGroupById(egId);
                let options = data.options;
                setEventGroupSeeded(eg, false);
                setMaxInHeatForEventGroup(eg, getMaxInHeatFromOptions(options));
                setLaneCount(eg, getLaneCountFromOptions(options));
                setFirstLane(eg, getFirstLaneFromOptions(options));
                setSeedAge(eg, getSeedAgeFromOptions(options));
                setSeedGender(eg, getSeedGenderFromOptions(options));
                setDoubleUp(eg, getDoubleUpFromOptions(options));
                setAutoQualify(eg, getAutoQualifyFromOptions(options));
                setNonAutoQualify(eg, getNonAutoQualifyFromOptions(options));
                // reset Entries
                // let entries = data.entries.entries;
                let entries = getEntriesForEg(eg);
                for (let e in entries) {
                    let entry = entries[e];
                    clearHeatInfo(entry);
                }
                let currentEg = getCurrentEventGroup();
                return egId === currentEg.id;
            }

            function confirmSeedings(data) {
                let egEntries = getEntriesForEgId(data.egId);
                let eg = getEventGroupById(data.egId);
                let confirmedEntries = data.entries;
                let associatedEgEntries = [];
                let refresh = false;
                let present = data.present === 1 ? true : false;
                let entityId = 0;
                for (let e in egEntries) {
                    let entry = egEntries[e];
                    entityId = entry.athleteId;
                    if ( typeof entry.teamid !== "undefined" ){
                        entityId = entry.teamid;
                    }
                    associatedEgEntries[entityId] = entry;
                }
                for (let e in confirmedEntries) {
                    let confirmedEntry = confirmedEntries[e];
                    entityId = confirmedEntry.athleteId;
                    if ( typeof confirmedEntry.teamId !== "undefined" ){
                        entityId = confirmedEntry.teamId;
                    }
                    let entry = associatedEgEntries[entityId];
                    if (entry) {
                        if (isActiveEntry(entry)) {
                            refresh = true;
                        }
                        setEntryPresent(entry, present);
                        setSeeded(entry, true);
                        setHeatNo(entry, getRaceNo(confirmedEntry));
                        setHeatNoCheckedIn(entry, getRaceNoCheckedIn(confirmedEntry));
                        setLaneNo(entry, getLaneNo(confirmedEntry));
                        setLaneNoCheckedIn(entry, getLaneNoCheckedIn(confirmedEntry));
                    }
                }
                setEventGroupSeeded(eg, true);
                return refresh;
            }

            function addCopiedEntries(data) {
                let processedEgIds = [];
                let newEventGroup = null;
                let allEntries = getEntries();
                let refresh = true;

                for (let e in data) {
                    let dataObj = data[e];

                    if (!processedEgIds[dataObj.newEgId]) {
                        // remove any old / existing entries if egId not processed as yet

                        newEventGroup = getEventGroupById(dataObj.newEgId);
                        delete newEventGroup.entries;
                        for (let e in allEntries) {
                            if (allEntries[e].eventGroupId === dataObj.newEgId) {
                                delete allEntries[e];
                            }
                        }
                        processedEgIds[dataObj.newEgId] = newEventGroup;
                    } else {
                        newEventGroup = processedEgIds[dataObj.newEgId];
                    }

                    let egEntries = getEntriesForEgId(dataObj.oldEgId);
                    for (let e in egEntries) {
                        let sourceEntry = egEntries[e];
                        if (getEntryId(sourceEntry) === dataObj.oldEntryId) {
                            // clone to new entry
                            let newEntry = JSON.stringify(sourceEntry);
                            newEntry = JSON.parse(newEntry);
                            newEntry.entryId = dataObj.newEntryId;
                            newEntry.compeventid = dataObj.newCeId;
                            newEntry.eventGroupId = dataObj.newEgId;
                            newEntry.eventGroup = newEventGroup.event;
                            newEntry.eventno = getEventNoFromEventGroup(newEventGroup);
                            newEntry.tf = newEventGroup.uomType;
                            clearHeatInfo(newEntry);
                            allEntries.push(newEntry);
                        }
                    }
                }
                return refresh;
            }

            function addQualifyEntries(data) {
                deleteExistingEntriesForEgId(data.targetEgId);
                let qualEntryCount = data.entries.length;
                let localQualEntryCount = 0;
                let entries = getEntries();
                let sourceEntries = getEntriesForEgId(data.sourceEgId);
                let targetEg = getEventGroupById(data.targetEgId);
                clearEgEntries(targetEg);
                for (let se in sourceEntries) {
                    let srcEntry = sourceEntries[se];
                    for (let qi in data.entries) {
                        let qualifyInfo = data.entries[qi];
                        if (qualifyInfo.athleteId === srcEntry.athleteId) {
                            // clone srcEntry
                            let newEntry = JSON.parse(JSON.stringify(srcEntry));
                            newEntry.entryId = qualifyInfo.entryId;
                            newEntry.eventGroupId = data.targetEgId;
                            newEntry.compeventid = qualifyInfo.compEventId;
                            newEntry.eoptions = qualifyInfo.options;
                            newEntry.pb = qualifyInfo.pb;
                            clearHeatInfo(newEntry);
                            entries.push(newEntry);
                            localQualEntryCount++;
                        }
                    }
                    setEntries(entries);
                }
                setEventGroupSeeded(targetEg, false);
                cardChange();
                if ( localQualEntryCount !== qualEntryCount ){
                    markRefreshRequired();
                }
            }

            function clearHeatInfo(entry) {
                setHeatNo(entry, 0);
                setLaneNo(entry, 0);
                setHeatNoCheckedIn(entry, 0);
                setLaneNoCheckedIn(entry, 0);
                setSeeded(entry, false);
            }

            function deleteExistingEntriesForEgId(egId) {
                let entries = getEntries();
                let newEntries = entries.filter(function (entry) {
                    return entry.eventGroupId !== egId;
                });
                setEntries(newEntries);
            }

            function toastMsg(data) {
                let eg = getEventGroupById(data.egId);
                let currentEG = getCurrentEventGroup();
                let msg = data.message;
                if (msg !== "") {
                    toastr["error"](msg, eg.event);
                }
                eg.notes = msg;
                if (currentEG.id === data.egId) {
                    displayEGNotes(msg);
                }
            }

            function addNotesToAthlete(data) {
                let refresh = false;
                let athleteId = data.athleteId;
                let note = data.note;
                let bibNos = getBibNos();
                for (let b in bibNos) {
                    let bibObj = bibNos[b];
                    if (bibObj.athleteId === athleteId) {
                        bibObj.notes = note;
                        let entries = getEntriesForCurrentEg();
                        for (let e in entries) {
                            let entry = entries[e];
                            if (entry.athleteId === athleteId) {
                                refresh = true;
                            }
                        }
                        break;
                    }
                }
                return refresh;
            }

            function updateConfirmedHeat(data) {
                let curEg = getCurrentEventGroup();

                for (let p in data.participants) {
                    let participant = data.participants[p];
                    let entry = getEntryByID(participant.entryId);
                    if ( entry === null ){
                        entry = getTeamEntryById(participant.entryId);
                    }
                    entry.heatInfo.confirmed = data.confirmed;
                    if (getEventGroupId(curEg) === getEventGroupId(data.eventGroup)) {
                        setHeatConfirmed(participant.heatNo, data.confirmed);
                    }
                }
                return false;
            }

            function updateCheckinOnDataStore(data) {
                let payload = data.payload;
                let checkins = payload.checkins;
                let refresh = false;
                for (let c in checkins) {
                    let entry = getEntryByID(parseInt(c));
                    setEntryCollected(entry, payload.collected);
                    setHeatNoCheckedIn(entry, 0)
                    setLaneNoCheckedIn(entry, 0)

                    // update bibNo if required;
                    updateBibNo(entry.athleteId, checkins[c].bibNo);
                    let checkedIn = payload.collected;
                    if ( checkedIn ){
                        checkedIn = checkins[c].checkedIn;
                    }
                    setEntryCheckedIn(entry, checkedIn);
                    if ( checkedIn ) {
                        if (getRaceNoCheckedIn(entry) === 0) {
                            let entries = getEntriesForEvent(getEventNoFromEntry(entry));
                            let eventGroup = getEventGroupById(entry.eventGroupId);
                            let checkinStatus = getCheckInStatus(eventGroup);
                            if (checkinStatus !== <?php echo E4S_CHECKIN_CLOSED ?> ) {
                                let laneCount = getLaneCount(eventGroup);
                                let heatCount = getTrackHeatCount();
                                let taken = false;

                                for (let h = 1; h <= heatCount; h++) {
                                    for (let l = 1; l <= laneCount; l++) {
                                        for (let e in entries) {
                                            let checkEntry = entries[e];
                                            if (getRaceNoCheckedIn(checkEntry) === h && getLaneNoCheckedIn(checkEntry) === l) {
                                                taken = true;
                                                break;
                                            }
                                        }
                                        if (!taken) {
                                            setHeatNoCheckedIn(entry, h);
                                            setLaneNoCheckedIn(entry, l);
                                            h = heatCount + 1;
                                            l = laneCount + 1;
                                            break;
                                        }
                                        taken = false;
                                    }
                                }
                                if (!taken) {
                                    for (let e in entries) {
                                        let checkEntry = entries[e];
                                        setSeeded(checkEntry, false);
                                    }
                                }
                            }
                            // } else {
                            //     seedUnSeeded(entry);
                        }
                    }
                    if (!refresh) {
                        refresh = isActiveEntry(entry);
                    }
                }
                return refresh;
            }

            function updateEntriesOnDataStore(data) {
                let payload = data.payload;
                let egId = payload.egId;
                let present = payload.present;
                let refresh = false;
                if (present || present === 1) {
                    present = true;
                } else {
                    present = false;
                }
                if (egId !== 0) {
                    let eg = getEventGroupById(egId);
                    setEventGroupSeeded(eg, true);
                    let entries = getEntriesForEg(eg);
                    for (let e in entries) {
                        let entry = entries[e];
                        setEntryPresent(entry, present);
                        if ( present ){
                            setEntryCheckedIn(entry, true);
                        }
                        if (!refresh) {
                            refresh = isActiveEntry(entry);
                        }
                    }
                } else {
                    for (let e in payload.entryIds) {
                        let isTeam = false;
                        if (typeof payload.isTeam !== "undefined") {
                            isTeam = payload.isTeam;
                        }
                        let entry = getEntryByID(payload.entryIds[e], isTeam);
                        setEntryPresent(entry,present);
                        if ( present && !isTeam){
                            setEntryCheckedIn(entry, true);
                        }
                        if (!refresh) {
                            refresh = isActiveEntry(entry);
                        }
                    }
                }
                return refresh;
            }

            function updateTrackMoveOnDataStore(data) {
                let refresh = false;
                let checkedIn = data.payload.checkedIn;
                let isTeam = data.payload.isTeam;
                if ( !isTeam ) {
                    let entries = getEntries();
                    let entryA = data.payload.entryA;
                    let entryB = data.payload.entryB;
                    for (let e in entries) {
                        let entry = entries[e];
                        let heatInfo = entry.heatInfo;
                        let useEntry = null;
                        if (getEntryId(entry) === getEntryId(entryA)) {
                            useEntry = entryA;
                        } else if (getEntryId(entry) === getEntryId(entryB)) {
                            useEntry = entryB;
                        }
                        if (useEntry !== null) {
                            if (!refresh) {
                                refresh = isActiveEntry(useEntry);
                            }
                            if (!checkedIn) {
                                heatInfo.laneNo = useEntry.laneNo;
                                heatInfo.heatNo = useEntry.heatNo;
                            } else {
                                heatInfo.laneNoCheckedIn = useEntry.laneNo;
                                heatInfo.heatNoCheckedIn = useEntry.heatNo;
                            }
                            heatInfo.present = isEntryPresent(useEntry);
                            // mark seeded
                            setSeeded(entry, true);
                        }
                    }
                }else{
                    // moving teams
                    let useEntry = data.payload.entryA;
                    if (!refresh) {
                        refresh = isActiveEntry(useEntry);
                    }
                    let updateTeamEntry = null;
                    let useEntryId = getEntryId(useEntry);
                    if (useEntryId !== 0 ) {
                        updateTeamEntry = getTeamEntryById(useEntryId);

                        if (checkedIn) {
                            updateTeamEntry.heatInfo.laneNoCheckedIn = useEntry.laneNo;
                            updateTeamEntry.heatInfo.heatNoCheckedIn = useEntry.heatNo;
                        } else {
                            updateTeamEntry.heatInfo.laneNo = useEntry.laneNo;
                            updateTeamEntry.heatInfo.heatNo = useEntry.heatNo;
                        }
                        // updateTeamEntry.present = isEntryPresent(useEntry);
                        // mark seeded
                        setSeeded(updateTeamEntry, true);
                    }
                    useEntry = data.payload.entryB;
                    useEntryId = getEntryId(useEntry);
                    if (useEntryId !== 0 ) {
                        updateTeamEntry = getTeamEntryById(useEntryId);

                        if (checkedIn) {
                            updateTeamEntry.heatInfo.laneNoCheckedIn = useEntry.laneNo;
                            updateTeamEntry.heatInfo.heatNoCheckedIn = useEntry.heatNo;
                        } else {
                            updateTeamEntry.heatInfo.laneNo = useEntry.laneNo;
                            updateTeamEntry.heatInfo.heatNo = useEntry.heatNo;
                        }
                        // updateTeamEntry.present = isEntryPresent(useEntry);
                        // mark seeded
                        setSeeded(updateTeamEntry, true);
                    }
                }
                return refresh;
            }

            function updateSeedingOnDataStore(data) {
                let payload = data.payload;
                let seedings = payload;
                if ( typeof seedings.seedings !== "undefined" ){
                    seedings = seedings.seedings;
                }
                let currentEg = getCurrentEventGroup();
                if (currentEg === null || typeof currentEg === "undefined") {
                    return false;
                }
                setMaxInHeatForEventGroup(currentEg, payload.maxInHeat);
                let allEntries = getEntries();
                let refresh = false;
                // What if entry does not exist/ final
                for (let e in allEntries) {
                    let entry = allEntries[e];
                    let payloadEntry = seedings[entry.eventGroupId + "-" + entry.athleteId];
                    if (typeof payloadEntry !== "undefined") {
                        updateInboundEntry(payloadEntry, entry);
                        if (!refresh) {
                            if (currentEg.id === entry.eventGroupId) {
                                refresh = true;
                            }
                        }
                    }
                }
                return refresh;
            }

            function updateInboundEntry(inboundEntry, entry) {
                if (typeof inboundEntry.heatNoCheckedIn !== "undefined") {
                    setHeatNoCheckedIn(entry, inboundEntry.heatNoCheckedIn);
                    setLaneNoCheckedIn(entry, inboundEntry.laneNoCheckedIn);
                    entry.heatInfo.heatPositionCheckedIn = inboundEntry.heatPositionCheckedIn;
                }
                if (typeof inboundEntry.heatNo !== "undefined") {
                    setHeatNo(entry, inboundEntry.heatNo);
                    setLaneNo(entry, inboundEntry.laneNo);

                    entry.heatInfo.heatPosition = inboundEntry.heatPosition;
                }

                setSeeded(entry, true);
            }

            function updateCurEntryPresent(curEntry, newEntry) {
                if (typeof newEntry.entry.present === "undefined") {
                    newEntry.entry.present = false;
                }

                // updateCurrentEntry(newEntry);
                return newEntry;
            }

            // point of this function ?
            function updateCurrentEntry(entry) {
                let entryObj = getEntryByID(entry.entryId);
                entryObj = entry;
                return entry;
            }

            function setEntry(element, entry) {
                let entries = getEntries();
                entries[element] = entry;
                return entry;
            }

            function setResultLink(athlete, entry) {
                <?php
                if (hasResultsAccess()) {
                    echo "
                let usePos = entry.heatInfo.laneNo;
                if ( cardShowsCheckedIn() ){
                    usePos = entry.heatInfo.laneNoCheckedIn;
                }
                let targetDom = $(\"#field_name_\" + usePos);
                let parms = usePos + ',' + entry.athleteId + ',\'' + getAthleteNameFromAthlete(athlete) + '\',' + getEventNoFromEntry(entry);
                targetDom.wrapInner('<span onclick=\"callResultDialog(' + parms + ');return false;\"></span>');
                ";
                }
                ?>
            }

            function getTeamEntryById(teamId) {
                let teams = getTeams();
                for (let t in teams) {
                    let team = teams[t];
                    if (team.teamid === teamId) {
                        return team;
                    }
                }
                return null;
            }
            function cancelEntryById(entryId){
                let entries = getEntries();
                let entry = getEntryByID(entryId);
                let eg = getEventGroupById(entry.eventGroupId);
                removeEntryFromList(entries, entryId);
                if ( eg.entries ){
                    removeEntryFromList(eg.entries, entryId);
                }
            }
            function removeEntryFromList(entries, entryId){
                for (let e in entries) {
                    let entry = entries[e];
                    if (entry.entryId === entryId) {
                        entries.splice(e,1);
                        return true;
                    }
                }
                return false;
            }
            function getEntryByID(entryId, isTeam = false) {
                if (isTeam) {
                    return getTeamEntryById(entryId);
                }
                let entries = getEntries();
                for(let e in entries){
                    let entry = entries[e];
                    if (getEntryId(entry) === entryId) {
                        return entry;
                    }
                }
                return null;
            }

            function getEntryByElement(element) {
                let entries = getEntries();
                return entries[element];
            }

            function clearEgEntries(eventGroup) {
                delete eventGroup.entries;
            }

            function clearEgEntriesForEgId(egId) {
                let eventGroup = getEventGroupById(egId);
                clearEgEntries(eventGroup);
            }

            function getEntriesForEvent(eventNo) {
                let eventGroup = getEventGroup(eventNo);
                if (eventGroup === null) {
                    return [];
                }
                return getEntriesForEg(eventGroup);
            }

            function getEntriesForEgId(egId) {
                let eg = getEventGroupById(egId);
                return getEntriesForEg(eg);
            }
            function getEntriesForCurrentEg(onlyCheckedIn = false) {
                let currentEg = getCurrentEventGroup();
                return getEntriesForEg(currentEg, onlyCheckedIn);
            }
            function getEntriesForEg(eventGroup, onlyCheckedIn = false) {
                if (eventGroup === null) {
                    return [];
                }
                if (typeof eventGroup.entries !== "undefined") {
                    if (onlyCheckedIn) {
                        return getCheckedInEntries(eventGroup.entries);
                    } else {
                        return eventGroup.entries
                    }
                }
                let entries;
                if (isEventATeamEvent(eventGroup)) {
                    entries = getTeams();
                } else {
                    entries = getEntries();
                }
                let retEntries = [];
                let egId = getEventGroupId(eventGroup);

                for (let x in entries) {
                    let entry = entries[x];
                    if (entry.eventGroupId === egId) {
                        retEntries.push(entry);
                    }
                }
                eventGroup.entries = retEntries;
                if (onlyCheckedIn) {
                    return getCheckedInEntries(retEntries);
                } else {
                    return retEntries;
                }
            }

            function getCheckedInEntries(entries){
                let retEntries = [];
                for (let x in entries) {
                    let entry = entries[x];
                    if (isEntryCheckedIn(entry)) {
                        retEntries.push(entry);
                    }
                }
                return retEntries;
            }
            function setActiveEntriesForEventGroup(eventGroup) {
                let entries = getEntriesForEg(eventGroup);
                setActiveEntriesForEntries(entries);
                setActiveEntriesType(isEventATeamEvent(eventGroup));
            }

            function setActiveEntriesForEntries(entries) {
                clearActiveEntries();
                for (let x = 0; x < entries.length; x++) {
                    if ( entries[x] ) {
                        let entityId = getEntryId(entries[x]);
                        if (typeof entityId === 'undefined') {
                            entityId = entries[x].teamid;
                        }
                        addActiveEntry(entityId);
                    }
                }
            }

            function getEventGroupPages(eventGroup) {
                let maxInHeat = getMaxInHeatFromEg(eventGroup);
                let entries = getEntriesForEg(eventGroup);
                if (entries.length === 0 || maxInHeat === 0) {
                    return 1;
                }
                let entryCnt = entries.length;

                if ( cardShowsCheckedIn() ){
                    entryCnt = getCheckedInEntries(entries).length;
                }
                return Math.ceil(entryCnt / maxInHeat);
            }
            function displayFieldCard(eventGroup) {
                let eventno = getEventNoFromEventGroup(eventGroup);
                let fieldType = eventGroup.uomtype;
                let isHeightEvent = isEventAHeightEvent(eventGroup);
                let pageSize = getFieldRowCount();
                let maxInHeat = getMaxInHeatFromEg(eventGroup);
                let showCheckedIn = cardShowsCheckedIn() && isCheckInInUse();
                let pageNo = getFieldCardPool();
                let entries = getEntriesForEg(eventGroup);
                if (isHeightEvent) {
                    showFieldHeightCard();
                    heightCard.getEGHeights(eventGroup, true);
                    entries = heightCard.sortHeightEntries(entries);
                } else {
                    showFieldDistanceCard();
                    let minTrials = getMinTrialsFromOptions(eventGroup.options);
                    if ( minTrials !== 3 ){
                        distanceCard.moveBestTo(minTrials);
                    }
                    distanceCard.displayJumpOrder();
                    let obj = null;
                    for (let i = 1; i <= pageSize; i++) {
                        $("#field_age_" + i).text("");
                        obj = $("#bib_1_" + i + "_status_cell");
                        obj.removeClass(e4sGlobal.class.bibPresent);
                        obj.removeClass(e4sGlobal.class.bibNotPresent);
                        obj.removeClass(e4sGlobal.class.bibNotCheckedIn);
                    }
                }

                populateCardHeader(eventGroup);
                displayCheckInOption(eventGroup);

                if (entries.length === 0) {
                    return setPoolCounter(1);
                }

                let lastPos = 0;
                let hasCheckedIn = false;
                let cardHeatLanesUsed = [];
                for (let x = 0; x < entries.length; x++) {
                    let entry = entries[x];
                    // stamp the fieldType on the entry
                    entry.fieldType = fieldType;
                    checkIfEntryAlreadyOnCard(entry, eventGroup, cardHeatLanesUsed);
                    let useHeatNo = getEntryRaceNo(entry)

                    if (useHeatNo === 0) {
                        setHeatLaneForUnSeeded(entry);
                        useHeatNo = getEntryRaceNo(entry);
                    }
                    let usePos = x + 1;
                    if (!isHeightEvent) {
                        usePos = getEntryLaneNo(entry);
                    }
                    setLaneNo(entry, usePos);
                    cardHeatLanesUsed[useHeatNo + "-" + usePos] = true;

                    if (usePos > lastPos) {
                        lastPos = usePos;
                    }
                    if (isEntryCheckedIn(entry)) {
                        hasCheckedIn = true;
                    }
                }
                if ( maxInHeat === 0 ){
                    maxInHeat = lastPos;
                }

                setPoolCounter(getEventGroupPages(eventGroup));

                heightCard.clearPositions();
                distanceCard.init();

                let lastHeightRow = 0;
                for (let x = 0; x < entries.length; x++) {
                    let entry = entries[x];
                    if ( isHeightEvent ){
                        if ( entry.heightRow > lastHeightRow ){
                            // insert new header row html
                            heightCard.injectHeaderRow(x + 1, entry.heightRow);
                        }
                        lastHeightRow = entry.heightRow;
                    }
                    let heatNo = getEntryRaceNo(entry);
                    if ( heatNo !== pageNo ){
                        continue;
                    }
                    if (!showCheckedIn || isEntryCheckedIn(entry)) {
                        let athlete = getAthleteById(entry.athleteId);
                        let usePos = x + 1;
                        if (!isHeightEvent) {
                            usePos = getEntryLaneNo(entry);
                        }
                        if (usePos === 0) {
                            lastPos++;
                            if (showCheckedIn) {
                                setHeatNoCheckedIn(entry, 1);
                                setLaneNoCheckedIn(entry, lastPos);
                            } else {
                                setHeatNo(entry, 1);
                                setLaneNo(entry, lastPos);
                            }
                        }
                        setFieldInfo(entry, athlete, pageNo, pageSize, hasCheckedIn, x === (entries.length - 1));
                    }
                }

                if( isHeightEvent ){
                    heightCard.populateHeightsOnCard(eventGroup);
                    heightCard.displayPositions();
                }else{
                    distanceCard.displayPositions();
                    distanceCard.displayInvalidTrials();
                    if ( isEventInProgress()) {
                        distanceCard.injectInProgressHeader();
                    }
                }
                let hasResults = !getResultsForEG() ? false : true;
                if (hasResults && !isEventGroupSeeded(eventGroup)) {
                    setEventGroupSeeded(eventGroup, true);
                }
                populateFieldFooter(eventno);
                displayCurrentAthlete(eventGroup);
                return entries.length > 0 ? true : false;
            }

            function displayCurrentAthlete(eventGroup, athleteId = null, attempt = 0){
                $(".bibNoMenu." + e4sGlobal.class.nextUp).removeClass(e4sGlobal.class.nextUp);
                let egId = getEventGroupId(eventGroup);
                if ( athleteId === null) {
                    athleteId = getNextUpForEgId(egId).athleteId;
                }
                if ( athleteId !== 0 ){
                    let entries = getEntriesForEg(eventGroup);
                    for( let e in entries ){
                        let entry = entries[e];
                        if ( entry.athleteId === athleteId ){
                            let rowNo = getLaneNo(entry);
                            // $("[eid=" + getEntryId(entry) + "]").parent().addClass(e4sGlobal.class.nextUp);
                            $("#bib_1_" + rowNo + "_cell").addClass(e4sGlobal.class.nextUp);
                            if ( isEventAHeightEvent() ) {
                                if ( attempt !== 0 ){
                                    heightCard.displayNextHeightForNextUp(entry, attempt);
                                }
                            }else{
                                let trial = 1;
                                // get next trial for athlete
                                let athleteResults = athleteResultsForEG(athleteId);
                                if ( athleteResults !== null ){
                                    let maxTrials = getMaxTrialsFromOptions(eventGroup.options);
                                    for ( let t = 1; t <= maxTrials; t++ ){
                                        if ( athleteResults['t' + t] ){
                                            if ( trial <= t ){
                                                trial = t + 1;
                                            }
                                        }
                                    }
                                }
                                $("#field_t" + trial + "_" + rowNo).parent().addClass(e4sGlobal.class.nextUp);
                            }
                        }
                    }
                }
            }

            function getFullGender(gender) {
                if (gender.toUpperCase() === "<?php echo E4S_GENDER_FEMALE ?>") {
                    gender = "Female";
                }
                if (gender.toUpperCase() === "<?php echo E4S_GENDER_MALE ?>") {
                    gender = "Male";
                }
                return gender;
            }

            function setFieldInfo(entry, athlete, page, pageSize, hasCheckedIn, lastRow) {
                // let pageSize = getFieldRowCount();
                let pageFactor = (page * pageSize) - pageSize;
                let gender = getFullGender(athlete.gender);
                let results = getResults();
                let athleteResults = null;
                let useLaneNo = getEntryLaneNo(entry);

                if (useLaneNo === 0) {
                    return false;
                }
                // useLaneNo -= pageFactor;
                let row = "#row_" + useLaneNo;
                let bibSelect = "#bib_1_" + useLaneNo;
                let bibCellSelect = bibSelect + "_cell";
                let ageSelect = "#field_age_" + useLaneNo;
                let genderSelect = "#field_gender_" + useLaneNo;
                let athleteSelect = "#field_name_" + useLaneNo;
                let clubSelect = "#field_club_" + useLaneNo;
                let startHeightSelect = "#height_sh_" + useLaneNo;
                let standsSelect = "#height_stand_" + useLaneNo;
                let isPresent = isEntryPresent(entry);
                let isCheckedIn = isEntryCheckedIn(entry);
                let eventNo = getEventNoFromEntry(entry);
                let isHeight = isEventAHeightEvent();
                if (!hasCheckedIn) {
                    isCheckedIn = true;
                }

                if (typeof results[eventNo] !== "undefined") {
                    results = results[eventNo];
                    if (typeof results[athlete.id] !== "undefined") {
                        athleteResults = results[athlete.id];
                    }
                }

                if ( athleteResults !== null ){
                    // isPresent = true;
                }
                markAthleteOnFieldCardStatus(isCheckedIn, isPresent, useLaneNo);
                markNotes(athleteSelect,athlete);
                let bibNoCell = $(bibCellSelect);
                if ( isHeight ){
                    // inject the bibno table
                    let bibNo = getBibNoToUse(athlete, entry);
                    let html = getBibHTML(1, useLaneNo, bibNo, '',entry, '');
                    bibNoCell.html(html);
                }else{
                    $(row).attr("row_athleteid", athlete.id);
                    $(row).attr("row_no", useLaneNo);
                }
                let bibObj = null;
                if (entry.athleteId !== 0) {
                    if ( !isHeight) {
                        bibObj = $(bibSelect);
                        bibObj.attr("eid", getEntryId(entry));

                        bibObj.text(getBibNoToUse(athlete, entry));
                        addQualifyClass(bibCellSelect, entry);
                    }
	                <?php if ( secureAccessToCard() ){ ?>
                        bibNoCell.addClass("bibNoMenu");
	                <?php } ?>
                    $(genderSelect).text(gender);
                    let ageGroupValue = athlete.ageshortgroup;
                    if (!isPresent) {
                        ageGroupValue = "DNS";
                    }
                    $(ageSelect).text(ageGroupValue);
                    $(athleteSelect).html(getAthleteNameForCard(athlete, entry));
                    $(clubSelect).text(athlete.clubname);
                    if ( isHeight && isEntryPresent(entry)){
                        let sh = heightCard.getStartHeightForEntry(entry);
                        if ( sh > 0) {
                            $(startHeightSelect).text(getDecimals(sh, 2));
                        }
                        if ( heightCard.isPoleVaultEvent() ) {
                            let stands = heightCard.getStandsForEntry(entry);
                            $(standsSelect).text(stands);
                        }
                    }
                }

                let outstandingResults = null;
                <?php if ( secureAccessToCard() ){ ?>
                // add any currently unwritten results
                    outstandingResults = offLine.getOutstanding(getEventNoFromEntry(entry), entry.athleteId);
                <?php } ?>
                let mergedResults = {};
                // merge athleteResults and outstandingResults
                if ( athleteResults !== null ){
                    for ( let key in athleteResults ){
                        if ( key !== "<?php echo E4S_RESULT_PARAMS ?>" && key !== "athlete") {
                            let result = "" + athleteResults[key];
                            if ( result.indexOf(offLine.statusSep) !== false ){
                                result += offLine.activeSuffix;
                            }
                            mergedResults[key] = result;
                        }
                    }
                }
                if ( outstandingResults !== null ){
                    for ( let key in outstandingResults ){
                        if ( key !== "<?php echo E4S_RESULT_PARAMS ?>" && key !== "athlete") {
                            let result = "" + outstandingResults[key];
                            if ( result.indexOf(offLine.statusSep) !== false ){
                                result += offLine.notActiveSuffix;
                            }
                            mergedResults[key] = result;
                        }
                    }
                    displayEGNotes();
                }
                if ( !isEmpty(mergedResults) ) {
                    if ( isHeight ) {
                        heightCard.displayCardResults(entry, mergedResults, bibObj);
                    } else {
                        distanceCard.displayAthleteResults(entry.athleteId,mergedResults);
                    }
                }
            }
            function setValidOptions(byAthlete) {
                let optionsSelector = $("[e4sOptions=true]");
                optionsSelector.children("option").remove();
                optionsSelector.append("<option disabled selected value=''>Select option</option>");

                for (let v = 0; v < validOptions.length; v++) {
                    let options = validOptions[v].split(":");
                    optionsSelector.append("<option value='" + options[0] + "'>" + options[1] + "</option>");
                }
                try {
                    optionsSelector.selectmenu("destroy");
                } catch (e) {

                }

                optionsSelector.selectmenu({
                    classes: {
                        "ui-selectmenu-button": "resultClear"
                    },
                    change: function (event, data) {
                        let val = data.item.value;
                        let field = event.target.id;
                        writeResultFromOptions(field, val, byAthlete);
                    }
                });
            }
            function getValidOptionChars() {
                let arr = [];
                for (let v = 0; v < validOptions.length; v++) {
                    let option = validOptions[v].split(":");
                    arr.push(option[0]);
                }
                return arr;
            }
            function valueMarkNotActive(value) {
                for (let v = 0; v < validOptions.length; v++) {
                    let option = validOptions[v].split(offLine.statusSep);
                    if (value === option[0] && option[2] === offLine.activeId) {
                        return true;
                    }
                }
                return false;
            }

            function getValidOption(val){
                for (let v = 0; v < validOptions.length; v++) {
                    let validOption = validOptions[v].split(offLine.statusSep)[0];
                    if (validOption.toUpperCase() === ("" + val).toUpperCase()) {
                        return validOption;
                    }
                }
                return "";
            }
            function isValValidOption(val) {
                return getValidOption(val) !== "";
            }

            function updateEntryActive(entry, isActive) {
                let rowNo = getLaneNo(entry);
                let obj = $("#bib_1_" + rowNo + "_cell");
                markObjActive(obj, isActive);
            }

            function markObjActive(obj, isActive) {
                let className = e4sGlobal.class.notActive;
                if (isActive) {
                    obj.removeClass(className);
                } else {
                    obj.addClass(className);
                }
            }
            function markObjVisible(obj, visible) {
                if ( visible ){
                    obj.removeClass(e4sGlobal.class.notVisible);
                }else{
                    obj.addClass(e4sGlobal.class.notVisible);
                }
            }
            function markUIActive(prefix, row, value) {
                let active = !valueMarkNotActive(value);
                let obj = $("[rowbibno=" + prefix + row + "]");
                markObjActive(obj, active);
                obj = $("#" + prefix + row);
                obj.prop("disabled", !active);
                // obj = $("#" + prefix + row + "OptionsParent");
                // markObjVisible(obj, active)
            }


            // ==============================================================================================================================
            // TRACK CARD CODE
            // ==============================================================================================================================
            function showTrackCard(eventGroup, heatCount) {
                let baseTrackCardHTML = "";
                if (heatCount > 0) {
                    let laneCount = getLaneCount(eventGroup);
                    baseTrackCardHTML = generateTrackCardHTML(heatCount, laneCount);
                } else {
                    baseTrackCardHTML = $("#trackOverTableDiv").html();
                }
                $("#card-base").html(baseTrackCardHTML);
                populateCardHeader(eventGroup);

            }

            function generateCardBaseHead(eventType, colCount) {
                // html = "<div id=\"" + name + "\" style=\"display:none\">";
                let html = "<table id=\"card_table_header_with_lanes\" class=\"e4sTable card_table table_width_100_perc\">";
                html += "<tr>";
                html += "<td colspan=\"" + colCount + "\" >";
                html += generateCardHeader(eventType);
                html += "</td>";
                html += "</tr>";
                return html;
            }

            function generateCardHeader(eventType) {
                let html = "<table class=\"e4sTable card_table card_header table_width_100_perc\">";
                html += "<tr>";
                html += "  <td>";
                html += "    <span class=\"card_header_label card_header_e4s\" id=\"card_title\"></span>";
                html += "  </td>";
                html += "  <td>";
                html += "    <span class=\"card_header_label\">Date : </span>";
                html += "    <span id=\"card_date\" class=\"card_header_data\"></span>";
                html += "  </td>";
                html += "  <td>";
                html += "    <span class=\"card_header_label\">Venue : </span>";
                html += "    <span id=\"card_venue\" class=\"card_header_data\"></span>";
                html += "  </td>";
                html += "  <td>";
                html += "    <span class=\"card_header_label\">Meeting : </span>";
                html += "    <span  id=\"card_competition\" class=\"card_header_data\"></span>";
                html += "  </td>";
                html += "</tr>";
                html += "<tr>";
                html += "  <td>";
                html += "    <span class=\"card_header_label\">Event : </span>";
                html += "    <span id=\"card_eventno\" class=\"card_header_data\"></span> :";
                html += "    <span id=\"card_event\" class=\"card_header_data\"></span>";
                html += "  </td>";

                html += "  <td colspan=2>";
                let trialText = "";
                if (eventType !== "<?php echo E4S_EVENT_TRACK ?>") {
                    trialText = "Trials : ";
                }
                html += "    <span class=\"card_header_label\">" + trialText + "</span>";
                html += "    <span id=\"card_trials\" class=\"card_header_data\"></span>";
                html += "  </td>";
                html += "  <td>";
                html += '    <div class="card_header_times">';
                html += '        <span id="header_start_time">Start Time : </span>';
                html += '        <span id="header_end_time">End Time :</span>';
                html += '        <span id="header_time" class="header_time">Card # :</span>';
                html += "    </div>";
                html += "  </td>";
                html += "</tr>";
                html += "<tr>";
                html += "</table>";

                return html;
            }

            function getBibHTML(heatNo, laneNo, bibNo, className, entry, wrapper) {
                let blank = '&nbsp;';
                let addTable = false;
                let alignment = 'center';
                if ( wrapper !== '' ){
                    alignment = "left";
                }
                if (typeof className === 'undefined') {
                    className = 'track_data_grid track_bibno_data';
                }
                if ( typeof bibNo === 'undefined' ){
                    bibNo = blank;
                }else{
                    className += ' bibNoMenu';
                }
                let entryIdAttr = "";
                let statusClass = '';
                let status2Class = 'bibStatus2';
                let teamAttr = "";
                if (typeof entry !== 'undefined') {
                    let entryId = getEntryId(entry);
                    if ( typeof entry.teamid !== 'undefined'){
                        entryId = entry.teamid;
                        teamAttr = " team=1 ";
                    }
                    entryIdAttr = "eid='" + entryId + "' ";
                    if ( isCheckInInUse() && !isEntryCheckedIn(entry) ){
                        statusClass = e4sGlobal.class.bibNotCheckedIn;
                    }else if (!isEntryPresent(entry) ){
                        statusClass = e4sGlobal.class.bibNotPresent;
                    } else {
                        statusClass = e4sGlobal.class.bibPresent;
                    }
                }

                let wrapperTitle = ' id="bib_' + heatNo + '_' + laneNo + '_cell"' + ' class="' + className + '">';
                let returnStr =
                    '<table class="e4sTable bibTable" style="width: 100%;">' +
                    '  <tbody>' +
                    '    <tr style="">' +
                    '       <td style="text-align: ' + alignment + ';">' +
                    '           <span class="e4sBibNo" ' + entryIdAttr + teamAttr + ' id=\"bib_' + heatNo + '_' + laneNo + '\">' + bibNo + '</span>' +
                    '       </td>' +
                    '       <td  class="statusTD ' + statusClass + '" id="bib_' + heatNo + '_' + laneNo + '_status_cell">' +
                    '       </td>' +
                    '   </tr>' +
                    '</tbody>' +
                    '</table>';

                if (wrapper !== ''){
                    returnStr =  '<' + wrapper + wrapperTitle + returnStr + '</' + wrapper + '>';
                }

                return returnStr;
            }

            function generateTrackCardHTML(heatCnt, laneCnt) {
                let labelTDCount = 2;
                let dataTDCount = 2;
                let dataRowCount = 3;
                let firstLane = getFirstLane();
                let maxInHeat = getMaxInHeatFromEg(getCurrentEventGroup());
                let lastValidLane = maxInHeat + firstLane - 1;
                let totalColCnt = (laneCnt * dataTDCount) + labelTDCount;
                let html = "";

                html += generateCardBaseHead("T", totalColCnt);
                html += '</table>';

                html += '<div id="track_card" class="<?php echo E4S_CARD_FIELD_PREFIX ?>flex_col <?php echo E4S_CARD_FIELD_PREFIX ?>border">';
                // Lane Headers Row
                html += '<ul class="<?php echo E4S_CARD_FIELD_PREFIX ?>flex_row <?php echo E4S_CARD_FIELD_PREFIX ?>header_row <?php echo E4S_CARD_FIELD_PREFIX ?>header">';
                html += '<li class="<?php echo E4S_CARD_FIELD_PREFIX ?>border <?php echo E4S_CARD_FIELD_PREFIX ?>header_race ui-state-disabled <?php echo E4S_CARD_FIELD_PREFIX ?>blank">';
                html += '<span>&nbsp;</span>';
                html += '</li>';

                for (let x = 1; x <= laneCnt; x++) {
                    let doubleUpArr = getDoubledUpLaneNoAndClass(x);
                    let useLaneCnt = doubleUpArr[0];
                    let doubleUpClass = doubleUpArr[1];

                    html += '<li class="<?php echo E4S_CARD_FIELD_PREFIX ?>border <?php echo E4S_CARD_FIELD_PREFIX ?>header_lane ui-state-disabled <?php echo E4S_CARD_FIELD_PREFIX ?>blank ' + doubleUpClass +'">';
                    html += '<span>' + useLaneCnt + '</span>';
                    html += '</li>';
                }
                html += "</ul>";

//      Heat Row
                for (let raceNo = 1; raceNo <= heatCnt; raceNo++) {
                    html += '<ul id="<?php echo E4S_CARD_FIELD_PREFIX ?><?php echo E4S_CARD_RACE_PREFIX ?>' + raceNo + '" class="<?php echo E4S_CARD_FIELD_PREFIX ?>flex_row" >';
                    for (let laneNo = 1; laneNo <= laneCnt; laneNo++) {
                        let laneClass = "";
                        if ( laneNo < firstLane || laneNo > lastValidLane ){
                            laneClass = "<?php echo E4S_CLASS_VOIDLANE ?>";
                        }
                        if ( laneNo === 1 ) {
                            html += '<li e4sheat="' + raceNo + '" class="<?php echo E4S_CARD_FIELD_PREFIX ?>border <?php echo E4S_CARD_FIELD_PREFIX ?>race <?php echo E4S_CARD_FIELD_PREFIX ?>blank">';
                            html += '<div>';
                            html += '<div class="<?php echo E4S_CARD_FIELD_PREFIX ?>raceNo" trackHeat="' + raceNo + '">' + raceNo + '</div>';
                            html += '<div class="<?php echo E4S_CARD_FIELD_PREFIX ?>time" trackTime="' + raceNo + '"></div>';
                            html += '</div>';
                            html += '</li>';
                        }
                        html += getRaceLaneHTML(raceNo, laneNo, laneClass, false);
                        html += getRaceLaneHTML(raceNo, laneNo, laneClass, true);
                    }
                    html += '</ul>';
                }

                html += '</div>';
                <?php if (hasSecureAccess(FALSE)) { ?>
                if ( !autoShow.available ) {
                    html += generateTrackFooter(totalColCnt);
                }
                <?php } ?>

                return html;
            }

            function getDoubledUpLaneNoAndClass(laneNo, eventGroup = null){
                if ( eventGroup === null ){
                    eventGroup = getCurrentEventGroup();
                }
                let doubleUpArr = [];
                if ( eventGroup.options.seed ){
                    if ( eventGroup.options.seed.doubleup ){
                        doubleUpArr = eventGroup.options.seed.doubleup;
                        if ( doubleUpArr[0] === '' ) {
                            return [laneNo, ""];
                        }
                    }
                }
                let sub = 0;
                if ( doubleUpArr.length > 0 ) {
                    if ( laneNo < doubleUpArr[0]) {
                        return [laneNo, ""];
                    }
                    for (let du = 0; du < doubleUpArr.length; du++) {
                        let laneNoToCheck = laneNo - (du*1);
                        if ( laneNoToCheck === doubleUpArr[du] ) {
                            return [doubleUpArr[du], "doubleUp"];
                        }
                        if (laneNoToCheck === doubleUpArr[du] + 1 ) {
                            return [doubleUpArr[du], "doubleUp"];
                        }
                        if ( laneNoToCheck > doubleUpArr[du] ){
                            sub++;
                        }
                    }
                }
                return [laneNo - sub,""];
            }
            function getRaceLaneHTML(raceNo, laneNo, laneClass, blank){
                let html = '';
                let blankStr = blank ? '_blank':'';
                let style  = blank ? ' style="display:none;" ':'';
                let otherClass = blank ? ' <?php echo E4S_CARD_FIELD_PREFIX ?>blank ': ' ui-state-disabled ';

                html += '<li id="<?php echo E4S_CARD_FIELD_PREFIX ?>' + raceNo + '_' + laneNo + blankStr + '"' + style + 'class="<?php echo E4S_CARD_FIELD_PREFIX ?>border <?php echo E4S_CARD_FIELD_PREFIX ?>lane' + otherClass + laneClass + '">';
                html += '<table class="<?php echo E4S_CARD_FIELD_PREFIX ?>entry_table">';
                html += '<tr class="<?php echo E4S_CARD_FIELD_PREFIX ?>entry_table_row">';
                html += '<td class="<?php echo E4S_CARD_FIELD_PREFIX ?>entry_table_row_bib" bibselect=true id="bib_' + raceNo + '_' + laneNo + '_cell' + blankStr + '">&nbsp;</td>';
                html += '<td class="<?php echo E4S_CARD_FIELD_PREFIX ?>entry_table_row_age" id="age_' + raceNo + '_' + laneNo + blankStr + '">&nbsp;</td>';
                html += '</tr>';
                html += '<tr class="<?php echo E4S_CARD_FIELD_PREFIX ?>center">';
                html += '<td colspan=2 class="<?php echo E4S_CARD_FIELD_PREFIX ?>athlete"  id="athlete_' + raceNo + '_' + laneNo + blankStr + '">&nbsp;</td>';
                html += '</tr>';
                html += '<tr class="<?php echo E4S_CARD_FIELD_PREFIX ?>center">';
                html += '<td colspan=2 class="<?php echo E4S_CARD_FIELD_PREFIX ?>club" id="club_' + raceNo + '_' + laneNo + blankStr + '">&nbsp;</td>';
                html += '</tr>';
                html += '</table>';
                html += '</li>';
                return html;
            }
            function generateTrackFooter(totalColCnt) {
                let html = "";

                html += "<tr>";
                html += "<td class=\"track_info_row\" colspan=\"" + totalColCnt + "\">";
                html += "<div id=\"track_info\" style=\"margin-left: 85px\">Information</div>";
                html += "</td>";
                html += "</tr>";

                html += "<tr>";
                html += "<td class=\"card_link_text\" colspan=\"" + totalColCnt + "\">";
                html += "<span class=\"card_link_text_span\">Entry4Sports Track Card. For more information https://entry4sports.com</span>";
                html += "</td>";
                html += "</tr>";

                return html;
            }


            function getFieldRowCount() {
                return <?php echo E4S_FIELD_CARD_COUNT ?>;
            }

            // defunct
            function getTrackHeatCount() {
                <?php if (secureAccessToCard()) { ?>
                return 10;
                <?php } else { ?>
                return 20;
                <?php } ?>
            }

            function getNextInTrial(prefix, element){
                let curObj = $("#" + prefix + element);
                let newObj = null;
                let lastAthleteInTrial = true;
                while (element <= getFieldRowCount()) {
                    element++;
                    newObj = $("#" + prefix + element);
                    if (newObj.prop("disabled") === false) {
                        lastAthleteInTrial = false;
                        break;
                    } else {
                        lastAthleteInTrial = newObj.attr("lastathlete") === "true";
                        if (element === getFieldRowCount()) {
                            //safety
                            lastAthleteInTrial = true;
                        }
                        if (lastAthleteInTrial) {
                            break;
                        }
                    }
                }
                if ( lastAthleteInTrial ){
                    return null;
                }
                return newObj;
            }
            function moveToNextResult(prefix, trial, element, isLastElement) {
                let curObj = $("#" + prefix + element);
                let nextObj = null;
                if (!isLastElement) {
                    nextObj = getNextInTrial(prefix, element);

                    if (nextObj !== null) {
                        nextObj.focus();
                        nextObj.select();
                    }
                }
                if (nextObj === null) {
                    let eventGroup = getCurrentEventGroup();
                    if (trial < getMaxTrialsFromOptions(eventGroup.options)) {
                        trialDistanceResults(trial + 1);
                    } else {
                        // Do What ???
                        curObj.blur();
                    }
                }
            }

            function markUIPresent(prefix, row, present) {
                let optionsObj = $("#" + prefix + row + "OptionsParent");
                if (present) {
                    $("#athletepresent_" + row).show();
                    $("#athleteabsent_" + row).hide();
                    $("#" + prefix + row + "BTN").show();
                    $("#" + prefix + row + "Options-button").show();
                } else {
                    $("#athletepresent_" + row).hide();
                    $("#athleteabsent_" + row).show();
                    $("#" + prefix + row + "BTN").hide();
                    $("#" + prefix + row + "Options-button").hide();
                }
                markObjVisible(optionsObj, present);
                $("#" + prefix + row).prop("disabled", !present);
            }

            function markAthleteOnFieldCardStatus(isCheckedIn, status, position) {
                let addStatusClass = e4sGlobal.class.bibPresent;
                if (!isCheckedIn) {
                    addStatusClass = e4sGlobal.class.bibNotCheckedIn;
                } else if ( !status ){
                    addStatusClass = e4sGlobal.class.bibNotPresent;
                }

                $("#bib_1_" + position + "_status_cell")
                    .removeClass(e4sGlobal.class.bibPresent)
                    .removeClass(e4sGlobal.class.bibNotPresent)
                    .addClass(addStatusClass);
            }

            function showScoreboardIcon(row) {
                $("[scoreboard=true]").addClass(e4sGlobal.class.notVisible);
                if (row !== 0) {
                    $("#sb_" + row).removeClass(e4sGlobal.class.notVisible);
                }
            }

            function clearResult(field, voidTrial) {
                let newVal = "";
                if (voidTrial) {
                    newVal = "<?php echo E4S_FIELDCARD_FAILURE ?>";
                }
                $("#" + field).val(newVal);
            }
            function getNameFromEntity(entity, inClass = ''){
                if ( entity.teamname ){
                    return entity.teamname;
                }
                let athleteName = getAthleteName(entity.firstname, entity.surname);
                if ( inClass !== '' && parseInt(entity.classification) !== 0){
                    athleteName += ' <span class="athleteClassification"><img class="athleteClassificationIcon" src="/resources/para_icon_small.gif">' + inClass + entity.classification + '</img></span>';
                }
                return athleteName;
            }
            function getAthleteNameFromAthlete(athlete){
                return getAthleteName(athlete.firstname, athlete.surname);
            }
            function getAthleteName(firstName, surName ){
                let name = "Unknown";
                if ( firstName !== null && surName !== null ) {
                    name = firstName + ' ' + surName;
                }
                return name;
            }
            function setValidOption(field) {
                let inputFieldObj = $("#" + field);
                let optionsHTML = "<ul style='width:200px; position: absolute; z-index: 999;' id=\"menu\">" +
                    "  <li><div>Foul</div></li>" +
                    "  <li><div>Retired</div></li>" +
                    "  <li><div>Passed</div>" +
                    "</ul>";
                inputFieldObj.after(optionsHTML);

                $("#menu").menu();
            }

            function getFirstEntry(prefix) {
                $("#" + prefix + 1).select();
            }

            function setSeedingSort(sortOrder) {
                e4sGlobal.seedingSortOrder = sortOrder;
            }

            function getSeedingSort() {
                if (e4sGlobal.seedingSortOrder) {
                    return e4sGlobal.seedingSortOrder.toLowerCase();
                }
                return '<?php echo E4S_SEED_HEAT?>';
            }

            function getEntriesInOrderForEvent(sortOrder) {
                let entryArr = [];
                // let eventno = getSelectedEventNo();
                // let entries = getEntriesForEvent(eventno);
                let eg = getCurrentEventGroup();
                let entries = getEntriesForEg(eg);

                if (sortOrder !== null) {
                    setSeedingSort(sortOrder);
                } else {
                    setSeedingSort('<?php echo E4S_SEED_HEAT?>');
                }
                entries.sort(function (a, b) {
                    let athletes = getAthletes();
                    let seedingSort = getSeedingSort();
                    switch (seedingSort) {
                        case "<?php echo E4S_SEED_BIB ?>":
                            if (athletes[a.athleteId].bibno < athletes[b.athleteId].bibno) {
                                return -1;
                            }
                            break;
                        case "<?php echo E4S_SEED_FORENAME ?>":
                            let aFullNameF = athletes[a.athleteId].firstname + athletes[a.athleteId].surname;
                            let bFullNameF = athletes[b.athleteId].firstname + athletes[b.athleteId].surname;
                            if (aFullNameF < bFullNameF) {
                                return -1;
                            }
                            break;
                        case "<?php echo E4S_SEED_SURNAME ?>":
                            let aFullNameS = athletes[a.athleteId].surname + athletes[a.athleteId].firstname;
                            let bFullNameS = athletes[b.athleteId].surname + athletes[b.athleteId].firstname;

                            if (aFullNameS < bFullNameS) {
                                return -1;
                            }
                            break;
                        default:
                            let aHeatNo = getEntryRaceNo(a);
                            let bHeatNo = getEntryRaceNo(b);
                            let aLaneNo = getEntryLaneNo(a);
                            let bLaneNo = getEntryLaneNo(b);

                            if ( seedingSort === '<?php echo E4S_SEED_CHECKEDIN ?>'){
                                if ( isEntryCheckedIn(a) !== isEntryCheckedIn(b) ){
                                    if ( isEntryCheckedIn(a) ){
                                        return -1;
                                    }
                                    return 1;
                                }
                            }

                            if (aHeatNo < bHeatNo) {
                                return -1;
                            }
                            if (aHeatNo > bHeatNo) {
                                return 1;
                            }
                            if (aLaneNo < bLaneNo) {
                                return -1;
                            }
                            if (aLaneNo > bLaneNo) {
                                return 1;
                            }
                            break;
                    }

                    return 1;
                });
                for (let x = 0; x < entries.length; x++) {
                    let entry = entries[x];
                    let heatNo = getEntryRaceNo(entry);
                    let laneNo = getEntryLaneNo(entry);
                    if (!cardShowsCheckedIn() || isEntryCheckedIn(entry)) {
                        let position = heatNo + "-" + laneNo;
                        entryArr[position] = entry;
                    }
                }
                return entryArr;
            }

            function setHeatCountForEG(eventGroup, heatCnt) {
                eventGroup.heatCount = heatCnt;
            }

            function getHeatCountForEG(eventGroup) {
                if (typeof eventGroup.heatCount === "undefined") {
                    let entries = getEntriesForEg(eventGroup);
                    return entries.length;
                }
                return eventGroup.heatCount;
            }

            function getPageSizeForEntry(entry) {
                let egId = entry.eventGroupId;
                let eventGroup = getEventGroupById(egId);
                return getHeatCountForEG(eventGroup);
            }

            function setPrinting(printing) {
                e4sGlobal.isPrinting = printing;
            }

            function getPrinting() {
                return e4sGlobal.isPrinting;
            }

            function isPrinting() {
                if (typeof getPrinting() === "undefined") {
                    setPrinting(false);
                }
                return getPrinting();
            }

            function getTrackPrintHeatCount() {
                return 10;
            }
            function getLaneCount(eventGroup) {
                return getEventGroupLaneCount(eventGroup);
            }

            function getEventGroupLaneCount(eventGroup) {
                if (typeof eventGroup === "undefined" ) {
                    eventGroup = getCurrentEventGroup();
                }

                return getLaneCountFromOptions(eventGroup.options);
            }

            function hasEventGroupGotLaneCount(eventGroup) {
                return hasOptionsGotLaneCount(eventGroup.options);
            }

            function hasOptionsGotLaneCount(options) {
                if (options) {
                    if (options.seed) {
                        if (options.seed.laneCount) {
                            return true;
                        }
                    }
                }
                return false;
            }

            function getLaneCountFromOptions(options) {
                if (hasOptionsGotLaneCount(options)) {
                    return options.seed.laneCount;
                }
                return getCompetitionLaneCount();
            }

            function getCompetitionLaneCount() {
                return getCompetition().options.laneCount;
            }

            function getMaxInHeatFromEg(eventGroup) {
                let maxInHeat = getMaxInHeatFromOptions(eventGroup.options);
                if ( isEventGroupField(eventGroup) ){
                    return maxInHeat;
                }
                let laneCount = getLaneCount(eventGroup);
                if ( maxInHeat !== 0 ){
                    return maxInHeat;
                }
                if (eventGroup.options.maxathletes === -1) {
                    // check if it has a qualifer
                    return e4s_getMaxInHeatForQualifierEG(maxInHeat, getEventNoFromEventGroup(eventGroup));
                }
                if ( maxInHeat === 0 ){
                    maxInHeat = getLaneCount();
                }
                return maxInHeat;
            }
            function clearTrialsFromOptions(options){
                if ( options.trials ){
                    delete(options.trials);
                }
                return options;
            }
            function setMaxTrialsFromOptions(options, value) {
                if ( !options.trials ){
                    options.trials = {};
                }
                options.trials.max = value;
            }
            function setMinTrialsFromOptions(options, value) {
                if ( !options.trials ){
                    options.trials = {};
                }
                options.trials.min = value;
            }
            function setAthleteCntFromOptions(options, value) {
                if ( !options.trials ){
                    options.trials = {};
                }
                options.trials.athleteCnt = value;
            }
            function getOptionsForTrials(options){
                if ( !options.trials ){
                    options = getCompetition().options;
                }
                if ( !options.trials ){
                    options.trials = {};
                }
                return options;
            }
            function getMaxTrialsFromOptions(options) {
                options = getOptionsForTrials(options);
                let maxTrials = options.trials.max;
                if ( typeof maxTrials === 'undefined' ){
                    maxTrials = 6;
                }
                if ( maxTrials > 6 ){
                    maxTrials = 6;
                }
                return maxTrials;
            }
            function getMinTrialsFromOptions(options) {
                options = getOptionsForTrials(options);
                let minTrials = options.trials.min;
                let maxTrials = getMaxTrialsFromOptions(options);
                if ( typeof minTrials === 'undefined' ){
                    minTrials = 0;
                }
                if ( minTrials < 0 || minTrials >= maxTrials ){
                    minTrials = 0;
                }
                return minTrials;
            }
            function setJumpOrderFromOptions(options, value) {
                if ( !options.trials ){
                    options.trials = {};
                }
                options.trials.jumpOrder = value;
            }
            function getJumpOrderFromOptions(options) {
                options = getOptionsForTrials(options);
                let jumpOrder = options.trials.jumpOrder;

                if ( typeof jumpOrder === 'undefined' ){
                    jumpOrder = "";
                }

                return jumpOrder;
            }
            function getAthleteCntFromOptions(options) {
                options = getOptionsForTrials(options);
                let athleteCnt = options.trials.athleteCnt;

                if ( typeof athleteCnt === 'undefined' ){
                    minTrials = 0;
                }
                if ( athleteCnt < 0 ){
                    athleteCnt = 0;
                }
                return athleteCnt;
            }
            function getMaxInHeatFromOptions(options) {
                let maxInHeat = options.maxInHeat;
                if ( typeof maxInHeat === 'undefined' ){
                    maxInHeat = 0;
                }
                return maxInHeat;
            }
            function e4s_getMaxInHeatForQualifierEG(maxInHeat, eventNo) {
                let eventGroups = getEventGroups();
                for (let e in eventGroups) {
                    let eg = eventGroups[e];
                    let seed = getEGSeed(eg);
                    if (seed !== null) {
                        let qualifyEventNo = seed.qualifyToEg.eventNo;
                        if (qualifyEventNo === eventNo) {
                            maxInHeat = getMaxInHeatFromEg(eg);
                            break;
                        }
                    }
                }
                return maxInHeat;
            }
            function canCardBeUsedForEvent(eventGroup) {
                if ( isEventGroupField(eventGroup) ){
                    return true;
                }
                let laneCount = getLaneCount(eventGroup);
                let maxInHeat = getMaxInHeatFromEg(eventGroup);
                return maxInHeat <= laneCount;
            }

            function e4s_getCardForEvent(eventGroup) {
                setTrackMaximums(eventGroup);
                let printHeatCount = getTrackPrintHeatCount();
                let heatCnt = printHeatCount;
                let print = isPrinting();
                let cardPossible = canCardBeUsedForEvent(eventGroup);
                if (!print) {
                    if (cardShowsCheckedIn()) {
                        heatCnt = getMaxHeatNoCheckedIn();
                    } else {
                        heatCnt = getMaxHeatNo();
                    }
                    if (heatCnt < printHeatCount) {
                        heatCnt = printHeatCount;
                    } else {
                        heatCnt += 1;
                    }
                    if ( autoShow.available ){
                        if ( autoShow.isActive() ) {
                            heatCnt = autoShow.maxHeatCount;
                        }
                    }
                }
                let entriesAdded = false;
                setHeatCountForEG(eventGroup, heatCnt);

                if (cardPossible && getViewMode() === "<?php echo E4S_CARDMODE_CARD ?>") {
                    showTrackCard(eventGroup, heatCnt);
                    entriesAdded = populateTrackCardLanes(eventGroup, heatCnt);
                } else {
                    showTrackCard(eventGroup, 0);
                    entriesAdded = populateCardListV2(eventGroup);
                }
                populateTrackFooter(getEventNoFromEventGroup(eventGroup));
                return entriesAdded;
            }

            function isToggleModeAvailable(eventGroup) {
                let retval = false;
                if ( isEventGroupField(eventGroup) ){
                    // only card is available for field events
                    return retval;
                }
                if (canCardBeUsedForEvent(eventGroup)) {
                    retval = areCardsAvailable(eventGroup);
                }
                return retval;
            }

            function displayToggleViewModeMenuItem() {
                let toggleObj = $("#toggleViewMode");
                toggleObj.hide();
                let eg = getCurrentEventGroup();
                if (eg !== null) {
                    if (isToggleModeAvailable(eg)) {
                        toggleMenuItem(toggleObj, getViewMode() === "<?php echo E4S_CARDMODE_CARD ?>");
                        toggleObj.show();
                    }
                }
            }

            function toggleMenuItem(obj, on) {
                obj.toggleClass("active", on);
            }

            function toggleViewMode() {
                let curMode = getViewMode();
                if (curMode === "<?php echo E4S_CARDMODE_CARD ?>") {
                    setViewMode("<?php echo E4S_CARDMODE_NONECARD ?>");
                } else {
                    setViewMode("<?php echo E4S_CARDMODE_CARD ?>");
                }
                cardChange();
            }

            function setViewMode(mode) {
                createCookie("<?php echo E4S_CARDMODE_COOKIE ?>", mode);
            }

            function getViewMode() {
                return readCookie("<?php echo E4S_CARDMODE_COOKIE ?>");
            }

            function scheduleOnlyEvent(eventGroup) {
                if (eventGroup && eventGroup.options.maxathletes === -1) {
                    // is it a combined event ?
                    if (eventGroup.options.entriesFrom.id === 0) {
                        // schedule only event ( final )
                        return true;
                    }
                }
                return false;
            }

            function displayCheckInOption(eventGroup){
                let showCheckIn = false;
                if ( isEventGroupTrack(eventGroup) ) {
                    if (!isEventATeamEvent(eventGroup) && !scheduleOnlyEvent(eventGroup) && isCheckInInUse()) {
                        let checkinStatus = getCheckInStatus(eventGroup);
                        if (checkinStatus !== <?php echo E4S_CHECKIN_NOT_OPEN ?>) {
                            showCheckIn = !seedOnEntriesOnly();
                        }
                    }
                }
                showEntriesCheckedInSelection(showCheckIn);
            }
            function populateTrackCard(eventGroup) {
                displayCheckInOption(eventGroup);
                let entriesAdded = false;
                let showCard = areCardsAvailable(eventGroup);
                if (!isPrinting() && !scheduleOnlyEvent(eventGroup)) {
                    if (!cardShowsCheckedIn() && isCheckInInUse()) {
                        showCard = false;
                    }
                }
                if (showCard) {
                    entriesAdded = e4s_getCardForEvent(eventGroup);
                } else {
                    showTrackCard(eventGroup, 0);
                    entriesAdded = populateNoSeedingCard(eventGroup);
                }
                return entriesAdded;
            }

            function areCardsAvailable(eventGroup) {
                if (!eventGroup){
                    eventGroup = getCurrentEventGroup();
                }
                if ( isEventGroupField(eventGroup) ){
                    return true;
                }
                if (scheduleOnlyEvent(eventGroup)) {
                    return true;
                }

                let cOptions = getCompetition().options;
                let cardEnabled = cOptions.cardInfo.enabled;
                if (cardEnabled) {
                    cardEnabled = areCardsNowAvailable();
                    if (cardEnabled) {
                        let checkinEnabled = isCheckInEnabled();
                        let cardShowingCheckedIn = cardShowsCheckedIn();
                        //if (checkinEnabled && (!cardShowingCheckedIn || getCheckInStatus(eventGroup) !== <?php //echo E4S_CHECKIN_CLOSED ?>//)) {
                        if (checkinEnabled && getCheckInStatus(eventGroup) !== <?php echo E4S_CHECKIN_CLOSED ?>) {
                            cardEnabled = false;
                        }
                        let msg = "Warning! Checked In seedings will be used. ";
                        if (checkinEnabled && !cardShowingCheckedIn) {
                            if (eventGroup.notes.indexOf(msg) < 0) {
                                // data = {
                                //     egId: eventGroup.id,
                                //     message: msg + eventGroup.notes
                                // }
                                // toastMsg(data);
                                eventGroup.notes = msg + eventGroup.notes;
                            }
                        } else {
                            if (eventGroup.notes.indexOf(msg) > -1) {
                                eventGroup.notes = eventGroup.notes.replace(msg, "");
                            }
                        }
                    }
                }
                return cardEnabled;
            }

            function dateCardsAreAvailable() {
                let comp = getCompetition();
                let cardsAvailableFrom = comp.entriesclose;
                if (comp.options.cardInfo && comp.options.cardInfo.availableFrom) {
                    if (comp.options.cardInfo.availableFrom !== "") {
                        cardsAvailableFrom = comp.options.cardInfo.availableFrom + " 00:00:00";
                    }
                }
                return cardsAvailableFrom;
            }

            function areCardsNowAvailable() {
                let cardsAvailableFrom = dateCardsAreAvailable();
                let cardsAvailableFromDate = parseISOString(cardsAvailableFrom);

                let nowDt = new Date();
                let entriesClosed = false;
                if (nowDt > cardsAvailableFromDate) {
                    entriesClosed = true;
                }
                // return entriesClosed;
                // always make them available for now
                return true;
            }

            function areEntriesClosed() {
                let comp = getCompetition();
                let entriesCloseDate = parseISOString(comp.entriesclose);
                let nowDt = new Date();
                let entriesClosed = false;
                if (nowDt > entriesCloseDate) {
                    entriesClosed = true;
                }
                return entriesClosed;
            }

            function parseISOString(s) {
                let b = s.split(/\D+/);
                let retDate = new Date(b[0], b[1] - 1, b[2], b[3], b[4]);
                return retDate;
            }

            function setCheckInDatesForEventGroup(eventGroup) {
                if (typeof eventGroup.checkinOpensDT !== "undefined") {
                    // already set
                    return;
                }
                let cOptions = getCompetition().options;
                let toMins = cOptions.checkIn.defaultTo;
                let fromMins = cOptions.checkIn.defaultFrom;
                let egOptions = eventGroup.options;
                if (typeof egOptions.checkIn !== "undefined") {
                    if ( egOptions.checkIn.to > 0 ) {
                        toMins = egOptions.checkIn.to;
                    }
                    if ( egOptions.checkIn.from > 0 ) {
                        fromMins = egOptions.checkIn.from;
                    }
                }
                if (fromMins === "") {
                    fromMins = 0;
                }
                let checkinOpensDT;
                if (fromMins === 0) {
                    checkinOpensDT = parseISOString(cOptions.checkIn.checkInDateTimeOpens);
                } else {
                    checkinOpensDT = new Date(eventGroup.date);
                }

                let checkinClosesDT = new Date(eventGroup.date);

                checkinOpensDT.setMinutes(checkinOpensDT.getMinutes() - fromMins);
                eventGroup.checkinOpensDT = checkinOpensDT;
                checkinClosesDT.setMinutes(checkinClosesDT.getMinutes() - toMins);
                eventGroup.checkinClosesDT = checkinClosesDT;
            }

            function getCheckInStatus(eventGroup) {
                let status = <?php echo E4S_CHECKIN_OPEN ?>; // -1 = not open yet, 0 = open, 1 = closed
                if (isCheckInEnabled() && eventGroup !== null) {
                    let nowDt = new Date();
                    // setCheckInDatesForEventGroup(eventGroup);
                    let checkinOpensDT = eventGroup.checkinOpensDT;
                    let checkinClosesDT = eventGroup.checkinClosesDT;

                    if (nowDt < checkinOpensDT) {
                        status = <?php echo E4S_CHECKIN_NOT_OPEN ?>;
                    } else if (nowDt > checkinClosesDT) {
                        status = <?php echo E4S_CHECKIN_CLOSED ?>;
                    }
                }

                return status;
            }

            function populateNoSeedingCard(eventGroup) {
                let athletesAdded = false;
                if (isEventATeamEvent(eventGroup)) {
                    athletesAdded = populateTeamNoSeedingCard(eventGroup);
                } else {
                    athletesAdded = populateIndivNoSeedingCard(eventGroup);
                }
                return athletesAdded;
            }

            function showTeamAthletes() {
                return getCompetition().options.showTeamAthletes;
            }

            function populateTeamNoSeedingCard(eventGroup) {
                let entries = getEntriesForEg(eventGroup);
                let overHTML = "";
                if (showTeamAthletes()) {
                    overHTML += "<div style='padding: 10px 0 10px 0;'>Hover the cursor over each row to see athlete list</div>";
                }

                overHTML += "<table class='overTable'>";
                let pos = 0;
                let eventType = eventGroup.type;
                for (let e in entries) {
                    let entry = entries[e];

                    let rowClass = "";
                    if (pos % 2) {
                        rowClass = "overOddRow";
                    }
                    pos++;
                    let athleteList = "";
                    if (showTeamAthletes()) {
                        athleteList = "Athlete List\n\n";
                        for (let a in entry.athletes) {
                            let athlete = getAthleteById(entry.athletes[a]['athleteId']);
                            athleteList += getNameFromEntity(athlete, eventType) + "\n";
                        }
                    }
                    overHTML += "<tr class='" + rowClass + "' title='" + athleteList + "'>";
                    overHTML += "<td class='overTeamName'>" + entry.teamname + "</td>";
                    overHTML += "<td class='overAge'>" + getFullGender(entry.gender) + "</td>";
                    overHTML += "<td class='overAge'>" + entry.agegroup + "</td>";
                    overHTML += "<td class='overAge'>" + entry.athletes.length + " athletes</td>";
                    overHTML += "</tr>";

                }
                $("#trackOverTableInfo").html(overHTML);
                // $( document ).tooltip();
            }

            function populateIndivNoSeedingCard(eventGroup) {
                let athletesAdded = false;
                let entries = getEntriesForEg(eventGroup);
                let cardShowingCheckedIn = cardShowsCheckedIn();
                let overHTML = "";
                let showMsg = true;
                if ( autoShow.available ){
                    showMsg = !autoShow.isActive();
                }
                if (showMsg) {
                    overHTML += "<div style='padding: 10px 0 10px 0;'>";
                    if (!isEventGroupMulti(eventGroup)) {
                        if (getViewMode() === '<?php echo E4S_CARDMODE_CARD ?>') {
                            if (cardShowingCheckedIn) {
                                overHTML += "Card will be shown when Check-in closes";
                            } else if (isCheckInEnabled()) {
                                overHTML += "Card will be shown on the checked in page when Check-in has closed";
                            } else {
                                overHTML += "Card will be shown when entries have closed";
                            }
                        } else {
                            overHTML += "List mode enabled";
                        }
                    }
                    overHTML += "</div>";
                }
                overHTML += "<table class='overTable'>";

                overHTML += "<tr class='overRow'>";
                overHTML += "<td class='overLabel overBibLabel'>Bib#</td>";
                overHTML += "<td class='overLabel overAgeLabel'>Age Group</td>";
                overHTML += "<td class='overLabel overNameLabel'>Athlete</td>";
                overHTML += "<td class='overLabel overClubLabel'>Organisation</td>";
                overHTML += "<td class='overLabel overGenderLabel'>Gender</td>";
                overHTML += "<td class='overLabel overPBLabel'>Seeding Performance</td>";
                overHTML += "</tr>";

                for (let e in entries) {
                    let entry = entries[e];
                    let useRaceNo = getEntryRaceNo(entry);
                    if ( cardShowingCheckedIn ){
                        useRaceNo = getRaceNoCheckedIn(entry);
                    }
                    if (useRaceNo === 0) {
                        setHeatLaneForUnSeeded(entry);
                        let x = 1;
                    }
                }
                entries = getEntriesInOrderForEvent("<?php echo E4S_SEED_FORENAME ?>");
                let showCheckedIn = isCheckInInUse();
                let eventType = eventGroup.type;
                let pos = 0;
                for (let e in entries) {
                    let entry = entries[e];
                    let showAthlete = true;
                    if (cardShowingCheckedIn && !isEntryCheckedIn(entry)) {
                        showAthlete = false;
                    }
                    if (showAthlete) {
                        athletesAdded = true;
                        let entryHeatNo = getEntryRaceNo(entry);
                        let entryLaneNo = getEntryLaneNo(entry);
                        let athlete = getAthleteById(entry.athleteId);
                        entry.athlete = athlete;
                        let rowClass = "";
                        let bibClass = "overBib";
                        if (pos % 2) {
                            rowClass = "overOddRow";
                        }
                        pos++;
                        overHTML += "<tr class='" + rowClass + "'>";
                        overHTML += getBibHTML(entryHeatNo, entryLaneNo, getBibNoToUse(entry.athlete, entry), bibClass, entry,'td');
                        overHTML += "<td class='overName'>" + getNameFromEntity(entry.athlete, eventType) + "</td>";
                        overHTML += "<td class='overClub'>" + entry.clubname + "</td>";
                        overHTML += "<td class='overGender'>" + getFullGender(entry.gender) + "</td>";
                        overHTML += "<td class='overAge'>" + entry.athlete.ageshortgroup + "</td>";
                        overHTML += "<td class='overPB'>" + formatPbForScreen(entry.pb, eventGroup) + "</td>";
                        overHTML += "</tr>";
                    }
                }
                $("#trackOverTableInfo").html(overHTML);
                return athletesAdded;
            }

            function populateCardListV2(eventGroup) {
                let cardShowsCheckin = cardShowsCheckedIn();
                let entries = getEntriesForEg(eventGroup);
                let orderArr = [];
                let isField = isEventGroupField(eventGroup);
                let eventNo = getEventNoFromEventGroup(eventGroup);
                let eventResults = undefined;
                let hasResults = false;
                let cardHeatLanesUsed = [];
                let isTeamEvent = isEventATeamEvent(eventGroup);
                if (isField) {
                    eventResults = getResultsForEG(eventGroup);
                    hasResults = eventResults !== undefined;
                }
                displayCheckInOption(eventGroup);
                let showCheckedIn = false;
                for (let x = 0; x < entries.length; x++) {
                    let entry = entries[x];
                    checkIfEntryAlreadyOnCard(entry, eventGroup, cardHeatLanesUsed);
                    let useHeatNo = getEntryRaceNo(entry);
                    let useLaneNo = getEntryLaneNo(entry);

                    if (useHeatNo === 0) {
                        setHeatLaneForUnSeeded(entry);
                        useHeatNo = getEntryRaceNo(entry);
                        useLaneNo = getEntryLaneNo(entry);
                    }
                    cardHeatLanesUsed[useHeatNo + "-" + useLaneNo] = true;
                    let athlete = getAthleteById(entry.athleteId);
                    let process = true;
                    entry.athlete = athlete;
                    if (cardShowsCheckin) {
                        process = isEntryCheckedIn(entry);
                    }
                    if (process) {
                        let position = (useHeatNo * 30) + useLaneNo;
                        orderArr[position] = entry;
                        if ( !showCheckedIn ) {
                            if (isEntryCheckedIn(entry)) {
                                showCheckedIn = true;
                            }
                        }
                    }
                }

                let overHTML = "";
                let lastHeatNo = -1;
                let heatPos = 1;
                let heats = [];
                let rowNo = 1;

                for (let pos in orderArr) {
                    let rowClass = "";
                    let bibClass = "overBib";
                    if (rowNo % 2) {
                        rowClass = "overOddRow";
                    }
                    rowNo++;
                    let entry = orderArr[pos];
                    let useBibNo ;
                    let entryHeatNo = getEntryRaceNo(entry);
                    let entryLaneNo = getEntryLaneNo(entry);
                    let doubleUpArr = getDoubledUpLaneNoAndClass(entryLaneNo);
                    let useLaneCnt = doubleUpArr[0];
                    let doubleUpClass = doubleUpArr[1];
                    heats[entryHeatNo] = entry.heatInfo.confirmed;
                    let useName;
                    let agegroup;
                    if ( isTeamEvent ){
                        useBibNo = entry.bibNo;
                        agegroup = entry.agegroup;
                        useName = entry.teamname;
                        entry.clubname = getTeamObject(entry,eventGroup).clubname;
                    }else{
                        useBibNo = getBibNoToUse(entry.athlete, entry);
                        agegroup =entry.athlete.agegroup;
                        useName = getAthleteNameForCard(entry.athlete, entry);
                    }
                    if (entryHeatNo !== lastHeatNo) {
                        if ( overHTML !== "" ){
                            overHTML += "</ul>";
                        }
                        overHTML += getHeatChangeV2(eventGroup, entryHeatNo, isField, hasResults);
                        overHTML += "<ul id=\"" + dragDropUtil.race + entryHeatNo + "\" class='overTableUL overTable_" + entryHeatNo + "'>";
                        lastHeatNo = entryHeatNo;
                        heatPos = 1;
                    }
                    overHTML += "<li id=\"<?php echo E4S_CARD_FIELD_PREFIX ?>" + entryHeatNo + "_" + entryLaneNo + "\" entryid=" + getEntryId(entry) + " class='dragDropMain " + rowClass + "'>";
                    overHTML += "<span class='overPos " + doubleUpClass + "'>" + dragDropUtil.icon() + useLaneCnt + "</span>";
                    overHTML += getBibHTML(entryHeatNo, entryLaneNo, useBibNo, 'overBib', entry, 'span');

                    if ( !isEntryPresent(entry, true) && quickConfirm.getStatus() ) {
                        agegroup = '<input type="checkbox" onchange="easySwitchPresent(' + entryHeatNo + ',' + entryLaneNo + ');"><span style="font-size:0.7vw;max-font-size: 16px;"> Confirm</span>';
                    }
                    overHTML += "<span class='overAge'>" + agegroup + "</span>";
                    overHTML += '<span class="overName"><span id="athlete_' + entryHeatNo + '_' + entryLaneNo + '">' + useName + '</span></span>';
                    overHTML += '<span class="overClub"><span id="club_' + entryHeatNo + '_' + entryLaneNo + '">' + entry.clubname + '</span></span>';
                    overHTML += "<span class='overGender'>" + getFullGender(entry.gender) + "</span>";

                    if (hasResults) {
                        overHTML += "<span class='overResults'>" + getLastResult(eventNo, entry.athleteId) + "</span>";
                        overHTML += "<span class='overResults'>" + getBestResult(eventNo, entry.athleteId) + "</span>";
                    }
                    overHTML += "</li>";

                    heatPos++;
                }
                overHTML += "</ul>";
                $("#trackOverTableInfo").html(overHTML);
                for (let pos in orderArr) {
                    let entry = orderArr[pos];
                    let useHeatNo = getEntryRaceNo(entry);
                    let useLaneNo = getEntryLaneNo(entry);
                    let athleteSelect = "#athlete_" + useHeatNo + "_" + useLaneNo;
                    if ( !isTeamEvent ) {
                        let athlete = entry.athlete;
                        markNotes(athleteSelect, athlete);
                    }
                    if (!isField) {
                        if (!isEntrySeeded(entry)) {
                            setHeatLaneUnSeeded(useHeatNo, useLaneNo, eventGroup);
                        }
                    }
                    addQualifyClass("#bib_" + useHeatNo + "_" + useLaneNo, entry);
                    //if ( entry.paid === <?php //echo E4S_ENTRY_QUALIFY ?>// ){
                    //    $("#bib_" + useHeatNo + "_" + useLaneNo).addClass("e4s_qualify_entry");
                    //}
                }
                for (let h in heats) {
                    setHeatConfirmed(h, heats[h]);
                }
                <?php if ( hasSeedingAccess() ) { ?>
                if (!isPrinting()) {
                    bindStarterOptions(eventGroup);
                }
                <?php } ?>
                return heats.length > 1 ? true : false;
            }

            function getOverItemRowHTML(rowObj){
                
            }
            function getLastResult(eventNo, athleteId) {
                let results = getResultsForAthlete(eventNo, athleteId);
                let lastTrial = 0;
                let result = 0;
                for (let t in results) {
                    let trial = parseInt(t.replace('t', ''));
                    if (trial > lastTrial) {
                        lastTrial = trial;
                        result = results[t];
                    }
                }
                return result;
            }

            function getBestResult(eventNo, athleteId) {
                let results = getResultsForAthlete(eventNo, athleteId);;
                let bestResult = 0;
                for (let t in results) {
                    let result = parseFloat(results[t]);
                    if (result > bestResult) {
                        bestResult = result;
                    }
                }
                return bestResult;
            }
            function getHeatChangeV2(eventGroup, heatNo, isField, hasResults) {
                let output = "";

                if (!isField) {
                    let heatTimeString = getHeatTime(eventGroup, heatNo);
                    if (heatTimeString !== "") {
                        heatTimeString = " (" + heatTimeString + ")";
                    }
                    // output = "<li class='dragDropMain'><span>&nbsp;</span></li>";
                    output += "<li class='dragDropMain overHeat Heat" + heatNo + "' id='track_heat_no" + heatNo + "'>";
                    output += "<span class=\"trackCardHeatNameLabel trackCardHeatName\">" + getEventGroupTitle(eventGroup) + "</span>";
                    output += "<span class=\"trackCardHeatNoLabel trackCardHeatNo\"><span e4sheat=" + heatNo + ">Race " + heatNo + heatTimeString + "</span></span>";
                    output += "</li>";
                }
                let overHeaderHTML = "";
                if (hasResults) {
                    overHeaderHTML += "<li class='dragDropMain overLabelHead'>";
                    overHeaderHTML += "<span>";
                    overHeaderHTML += "&nbsp;";
                    overHeaderHTML += "</span>";
                    overHeaderHTML += "<span class='overLabel overResultsLabel'>";
                    overHeaderHTML += "Results";
                    overHeaderHTML += "</span>";
                    overHeaderHTML += "</li>";
                }
                overHeaderHTML += "<li class='dragDropMain overLabelHead'>";
                overHeaderHTML += "<span class='overLabel overPosLabel'>";
                if ( isEventGroupField(eventGroup)){
                    overHeaderHTML += "Pos";
                }else{
                    overHeaderHTML += "Lane";
                }
                overHeaderHTML += " #</span>";
                overHeaderHTML += "<span class='overLabel overBibLabel'>Bib #</span>";
                overHeaderHTML += "<span class='overLabel overAgeLabel'>Age Group</span>";
                overHeaderHTML += "<span class='overLabel overNameLabel'>Athlete</span>";
                overHeaderHTML += "<span class='overLabel overClubLabel'>Organisation</span>";
                overHeaderHTML += "<span class='overLabel overGenderLabel'>Gender</span>";

                if (hasResults) {
                    overHeaderHTML += "<span class='overLabel overResultsLabel'>Last</span>";
                    overHeaderHTML += "<span class='overLabel overResultsLabel'>Best</span>";
                }
                overHeaderHTML += "</li>";

                return "<ul>" + output + overHeaderHTML + "</ul>";
            }
            function getHeatChange(eventGroup, heatNo, isField, hasResults) {
                let output = "";

                if (!isField) {
                    let heatTimeString = getHeatTime(eventGroup, heatNo);
                    if (heatTimeString !== "") {
                        heatTimeString = " (" + heatTimeString + ")";
                    }
                    output = "<tr><td colspan=6>&nbsp;</td></tr>";
                    output += "<tr class='overHeat Heat" + heatNo + "' id='track_heat_no" + heatNo + "'>";
                    output += "<td colspan='3'>";
                    output += "<span class=\"trackCardHeatName\">" + getEventGroupTitle(eventGroup) + "</span>";
                    output += "</td>";
                    output += "<td colspan='3'>";
                    output += "<span class=\"trackCardHeatNo\">Race " + heatNo + heatTimeString + "</span>";
                    output += "</td>";
                    output += "</tr>";
                }
                let overHeaderHTML = "";
                if (hasResults) {
                    overHeaderHTML += "<tr class='overLabelHead'>";
                    overHeaderHTML += "<td colspan=6>";
                    overHeaderHTML += "&nbsp;";
                    overHeaderHTML += "</td>";
                    overHeaderHTML += "<td colspan=2 class='overLabel overResultsLabel'>";
                    overHeaderHTML += "Results";
                    overHeaderHTML += "</td>";
                    overHeaderHTML += "</tr>";
                }
                overHeaderHTML += "<tr class='overLabelHead'>";
                overHeaderHTML += "<td class='overLabel overPosLabel'>";
                if ( isEventGroupField(eventGroup)){
                    overHeaderHTML += "Pos";
                }else{
                    overHeaderHTML += "Lane";
                }
                overHeaderHTML += " #</td>";
                overHeaderHTML += "<td class='overLabel overBibLabel'>Bib #</td>";
                overHeaderHTML += "<td class='overLabel overNameLabel'>Athlete</td>";
                overHeaderHTML += "<td class='overLabel overClubLabel'>Organisation</td>";
                overHeaderHTML += "<td class='overLabel overGenderLabel'>Gender</td>";
                overHeaderHTML += "<td class='overLabel overAgeLabel'>Age Group</td>";
                if (hasResults) {
                    overHeaderHTML += "<td class='overLabel overResultsLabel'>Last</td>";
                    overHeaderHTML += "<td class='overLabel overResultsLabel'>Best</td>";
                }
                overHeaderHTML += "</tr>";

                return output + overHeaderHTML;
            }

            function getAgeGroupsKeyed() {
                let ageGroups = getAgeGroups();
                let returnAGs = {};
                for (let a = 0; a < ageGroups.length; a++) {
                    returnAGs['a' + ageGroups[a].id] = ageGroups[a].name;
                }
                return returnAGs;
            }

            function addMinutes(date, minutes) {
                return new Date(date.getTime() + minutes * 60000);
            }

            function showHeatTimeOnCard(eventGroup, entry) {
                let useHeatNo = getEntryRaceNo(entry);

                let heatTimeString = getHeatTime(eventGroup, useHeatNo);
                $("[tracktime='" + useHeatNo + "']").text(heatTimeString);
            }

            function getHeatTime(eventGroup, heatNo) {
                let heatPageCount = getTrackHeatCount();
                let page = getCardPage();
                let factor = (page - 1) * heatPageCount;
                let mins = eventGroup.options.heatDurationMins;
                if (mins === 0) {
                    return "";
                }
                let startDateTime = new Date(eventGroup.date);

                mins = mins * ((heatNo + factor) - 1);
                let heatTime = addMinutes(startDateTime, mins);

                return formatDate(heatTime, "HH:mm");
            }

            function getEmptyPosition(eventGroup, checkedIn) {
                let maxAthletesPerHeat = getLaneCount(eventGroup);
                let maxHeatNo = getMaxHeatNo() + 1;
                maxHeatNo = <?php echo E4S_FIELD_CARD_COUNT ?>;

                let arrayObj = {};
                for (let h = 1; h <= maxHeatNo; h++) {
                    for (let l = 1; l <= maxAthletesPerHeat; l++) {
                        arrayObj[h + "-" + l] = false;
                    }
                }
                let entries = getEntriesForEg(eventGroup);
                for (let e in entries) {
                    let entry = entries[e];
                    let useHeatNo = getEntryRaceNo(entry);
                    let useLaneNo = getEntryLaneNo(entry);

                    delete arrayObj[useHeatNo + "-" + useLaneNo];
                }
                for (let a in arrayObj) {
                    return a.split("-");
                }
            }

            function setHeatLaneForUnSeeded(entry) {
                let eventGroup = getEventGroupById(entry.eventGroupId);
                let maxAthletesPerHeat = getLaneCount(eventGroup);
                let firstPosition = getFirstLane();

                let nextPosition = getEmptyPosition(eventGroup, cardShowsCheckedIn());
                let heatNo = parseInt(nextPosition[0]);
                let laneNo = parseInt(nextPosition[1]);
                if (cardShowsCheckedIn()) {
                    if ( isEntryCheckedIn(entry, eventGroup) ) {
                        setHeatNoCheckedIn(entry, heatNo);
                        setLaneNoCheckedIn(entry, laneNo);
                    }
                } else {
                    setHeatNo(entry, heatNo);
                    setLaneNo(entry, laneNo);
                }
                setSeeded(entry, false);
            }

            function setUnseededLaneNo(laneNo) {
                let firstLane = getFirstLane();
                if (laneNo < firstLane) {
                    laneNo = firstLane;
                }
                e4sGlobal.unSeededLaneNo = laneNo;
                return laneNo;
            }

            function setUnseededLaneNoCheckedIn(laneNo) {
                let firstLane = getFirstLane();
                if (laneNo < firstLane) {
                    laneNo = firstLane;
                }
                e4sGlobal.unSeededLaneNoCheckedIn = laneNo;
                return laneNo;
            }

            function getMaxHeatNo() {
                return e4sGlobal.maxHeatNo;
            }

            function getMaxHeatNoCheckedIn() {
                return e4sGlobal.maxHeatNoCheckedIn;
            }

            function setMaxHeatNo(heatNo) {
                e4sGlobal.maxHeatNo = heatNo;
            }

            function setMaxHeatNoCheckedIn(heatNo) {
                e4sGlobal.maxHeatNoCheckedIn = heatNo;
            }

            function setMaxLaneNo(laneNo) {
                e4sGlobal.maxLaneNo = laneNo;
            }

            function getMaxLaneNo() {
                return e4sGlobal.maxLaneNo;
            }

            function updateHeatMaximums(heatNo, heatNoCheckedIn, maxLaneNo, unseededlaneNo, unseededlaneNoCheckedIn) {
                setMaxLaneNo(maxLaneNo);
                setMaxHeatNo(heatNo);
                setMaxHeatNoCheckedIn(heatNoCheckedIn);
                setUnseededLaneNo(unseededlaneNo);
                setUnseededLaneNoCheckedIn(unseededlaneNoCheckedIn);
            }

            function setTrackMaximums(eventGroup) {
                let maxHeatNo = 0;
                let maxLaneNo = 0;
                let maxHeatNoCheckedIn = 0;
                let entries = getEntriesForEg(eventGroup);
                let laneCnt = getLaneCount(eventGroup);
                maxHeatNo = Math.ceil(entries.length / laneCnt);
                // get maxInfo for entered athletes
                for (let x = 0; x < entries.length; x++) {
                    let entry = entries[x];
                    let heatNo = getRaceNo(entry);
                    let laneNo = getLaneNo(entry);
                    let heatNoCheckedIn = getRaceNoCheckedIn(entry);
                    let laneNoCheckedIn = getLaneNoCheckedIn(entry);
                    if (heatNo > maxHeatNo) {
                        maxHeatNo = heatNo;
                    }
                    if (laneNo > maxLaneNo) {
                        maxLaneNo = laneNo;
                    }
                    if (heatNoCheckedIn > maxHeatNoCheckedIn) {
                        maxHeatNoCheckedIn = heatNoCheckedIn;
                    }
                    if (laneNoCheckedIn > maxLaneNo) {
                        maxLaneNo = laneNoCheckedIn;
                    }
                }
                // add 1 to each so we can start adding unseeded athletes
                maxHeatNo++;
                maxHeatNoCheckedIn++;
                maxLaneNo++;
                updateHeatMaximums(maxHeatNo, maxHeatNoCheckedIn, maxLaneNo, 1, 1)
            }

            function getAthleteById(id) {
                let athletes = getAthletes();
                return athletes[id];
            }

            function addEntryFromEntries(payload) {
                let athlete = checkAndCreateAthlete(payload);
                let entryObj = {};
                entryObj.athlete = athlete;
                entryObj.entryId = payload.entryInfo.id;
                entryObj.clubName = payload.clubRecord.Clubname;
                entryObj.eventNo = getEventNoFromCE(payload.ceRecord);
                entryObj.ceId = payload.ceRecord.ID;
                entryObj.startDate = payload.ceRecord.startdate;
                entryObj.eventName = payload.ceRecord.eventName;
                entryObj.egId = payload.ceRecord.maxGroup;

                _addEntry(entryObj);
                return getSelectedEventNo() === getEventNoFromCE(payload.ceRecord);
            }

            function checkAndCreateAthlete(payload) {
                let athletes = getAthletes();
                let athlete = athletes[payload.athleteRecord.id];
                if (typeof athlete === "undefined") {
                    // need to create the athlete record
                    athlete = {};
                    athlete.ageGroupId = payload.ageGroup.ageGroup.id;
                    athlete.agegroup = payload.ageGroup.ageGroup.Name;
                    athlete.bibno = payload.bibInfo.bibNo;
                    athlete.classification = payload.athleteRecord.classification;
                    athlete.clubid = payload.clubRecord.id;
                    athlete.clubname = payload.clubRecord.Clubname;
                    athlete.county = payload.clubRecord.Country;
                    athlete.events = [];
                    athlete.firstname = payload.athleteRecord.firstName;
                    athlete.gender = payload.athleteRecord.gender;
                    athlete.ageshortgroup = payload.ageGroup.ageGroup.shortName + athlete.gender;
                    athlete.id = payload.athleteRecord.id;
                    athlete.maxgroup = payload.ceRecord.maxGroup;
                    athlete.region = payload.clubRecord.Region;
                    athlete.surname = payload.athleteRecord.surName;
                    athlete.urn = payload.athleteRecord.URN;
                    athletes[payload.athleteRecord.id] = athlete;
                }
                return athlete
            }

            function addAthleteForEventFromCard(payload) {
                let ageGroup = "";
                let ageGroups = getAgeGroups();
                for (let a in ageGroups) {
                    if (ageGroups[a].id === payload.ageGroupId) {
                        ageGroup = ageGroups[a].name;
                        break;
                    }
                }
                //
                let entryObj = {};
                entryObj.athlete = getAthletes()[payload.athleteId];
                entryObj.entryId = payload.entryId;
                entryObj.clubName = payload.clubName;
                entryObj.eventNo = getEventNoFromEventGroup(payload.eventGroup);
                entryObj.ceId = payload.ceObj.id;
                entryObj.startDate = payload.ceObj.startdate;
                entryObj.eventName = payload.eventGroup.name;
                entryObj.egId = payload.eventGroup.id;

                return _addEntry(entryObj);
            }

            function _addEntry(entryObj) {
                let athleteObj = entryObj.athlete;
                let athleteId = athleteObj.id;
                let dob = athleteObj.dob;
                let gender = athleteObj.gender;
                let ageGroup = athleteObj.agegroup;
                let entryId = entryObj.entryId;
                let clubName = entryObj.clubName;
                let eventNo = entryObj.eventNo;
                let ceId = entryObj.ceId;
                let startDate = entryObj.startDate;
                let eventName = entryObj.eventName;
                let egId = entryObj.egId;
                let entry = {
                    agegroup: ageGroup,
                    athleteId: athleteId,
                    dob: dob,
                    gender: gender,
                    clubname: clubName,
                    entryId: entryId,
                    compeventid: ceId,
                    startdate: startDate,
                    county: "",
                    coupon: "",
                    email: "",
                    entryPosition: 0,
                    event: eventName,
                    eventGroup: eventName,
                    eventGroupId: egId,
                    created: startDate,
                    orderid: 0,
                    phone: "",
                    price: 0,
                    region: "",
                    checkedIn: true,
                    present: true,
                    teambibno: 0,
                    pb: 0.00,
                    eoptions: {},
                    heatInfo: {
                        heatNo: 0,
                        laneNo: 0,
                        heatNoCheckedIn: 0,
                        laneNoCheckedIn: 0,
                        useLanes: 'A'
                    },
                    eventno: eventNo
                };
                getEntries().push(entry);
                clearEgEntriesForEgId(egId);
                return entry;
            }

            function addNewAthleteToCard(entry) {
                let eventNo = getSelectedEventNo();
                let eventGroup = getEventGroup(eventNo);
                setTrackMaximums(eventGroup);
                addEntryToCard(entry, eventGroup);
            }

            function getTeamObject(entry, eventGroup){
                let teamObj = {
                    agegroup: entry.agegroup,
                    ageshortgroup: 'Team',
                    bibno: entry.bibNo,
                    classification: 0,
                    clubname: entry.teamname,
                    firstname: '',
                    gender: entry.gender,
                    id: entry.teamid,
                    maxgroup: entry.eventGroupId,
                    surname: entry.teamname,
                    useLanes: 'A'
                };
                return teamObj;
            }
            function addEntryToCard(entry, eventGroup) {
                let useCheckIn = cardShowsCheckedIn();
                let page = getCardPage();
                let raceNo = getEntryRaceNo(entry);
                let laneNo = getEntryLaneNo(entry);
                let entity = null;
                if ( isEventATeamEvent(eventGroup) ){
                    entity = getTeamObject(entry, eventGroup);
                }else{
                    entity = getAthleteById(entry.athleteId);
                }

                if (raceNo === 0 || $("#bib_" + raceNo + "_" + laneNo + "_cell").text().trim() !== '') {
                    // unseeded athlete
                    setHeatLaneForUnSeeded(entry);
                }
                return setTrackInfo(entry, eventGroup, entity, useCheckIn, page)
            }

            function checkIfEntryAlreadyOnCard(entry, eventGroup, cardHeatLanesUsed){
                let maxInHeat = getMaxInHeatFromEg(eventGroup);
                let laneCount = getLaneCount(eventGroup);
                let heatNo = getEntryRaceNo(entry);
                let laneNo = getEntryLaneNo(entry);
                let useCount = maxInHeat;
                if ( laneCount > maxInHeat ){
                    useCount = laneCount;
                }

                //TODO allow manual override ( laneCount rather than maxInHeat )
                if ( eventGroup.type === '<?php echo E4S_EVENT_TRACK ?>' && (laneNo < 1 || laneNo > useCount)){
                    clearHeatInfo(entry)
                }else{
                    let heatLaneKey = heatNo + "-" + laneNo;
                    if ( typeof cardHeatLanesUsed[heatLaneKey] !== "undefined" ){
                        clearHeatInfo(entry);
                    }
                }
            }
            function checkEntryVisibility(entry) {
                if (entry.heatInfo.heatNo < 0) {
                    return false;
                }
                return true;
            }
            function populateTrackCardLanes(eventGroup, heatCnt) {
                let entries = getEntriesForEg(eventGroup);
                let firstEntry = null;
                let page = getCardPage();

                // set Heat Numbers
                $("[trackHeat]").each(function () {
                    let page = getCardPage();
                    let factor = (page - 1) * heatCnt;
                    let heatNo = parseInt($(this).attr("trackHeat"));
                    $("[tracktime='" + heatNo + "']").text("");
                    $(this).text(factor + heatNo);
                });
                let maxHeatNo = 0;
                // show those with defined heat/lane before any unseeded
                let unseededEntries = [];
                let cardHeatLanesUsed = [];
                for (let x = 0; x < entries.length; x++) {
                    let entry = entries[x];
                    if ( !checkEntryVisibility(entry) ){
                        continue;
                    }
                    let useHeatNo = getEntryRaceNo(entry);
                    let useLaneNo = getEntryLaneNo(entry);

                    checkIfEntryAlreadyOnCard(entry, eventGroup, cardHeatLanesUsed);
                    if (isEntrySeeded(entry)) {
                        cardHeatLanesUsed[useHeatNo + "-" + useLaneNo] = true;
                        if (addEntryToCard(entry, eventGroup)) {
                            if (firstEntry === null) {
                                firstEntry = entry;
                            }
                        }
                        showHeatTimeOnCard(eventGroup, entry);
                    } else {
                        unseededEntries.push(entry);
                    }

                    if ( useHeatNo > maxHeatNo ){
                        maxHeatNo = useHeatNo;
                    }
                }
                for (let x = 0; x < unseededEntries.length; x++) {
                    let entry = unseededEntries[x];

                    if (addEntryToCard(entry, eventGroup)) {
                        if (firstEntry === null) {
                            firstEntry = entry;
                        }
                    }
                    setEntryUnSeeded(entry, eventGroup);
                    showHeatTimeOnCard(eventGroup, entry);
                }
                if ( autoShow.available ){
                    autoShow.setMaxTrackHeat(maxHeatNo);
                }
                <?php if ( hasSeedingAccess() ) { ?>
                if (!isPrinting() && autoShow.available) {
                    bindStarterOptions(eventGroup);
                }
                <?php } ?>
                if (firstEntry !== null) {
                    let maxInHeat = getMaxInHeatFromEg(eventGroup);

                    let useLanes = firstEntry.heatInfo.useLanes.toUpperCase();
                    if (useLanes !== "A") {
                        for (let lane = 1; lane <= maxInHeat; lane++) {
                            let emptyLane = false;
                            if (useLanes === "E" && lane % 2 === 1) {
                                emptyLane = true;
                            }
                            if (useLanes === "O" && lane % 2 === 0) {
                                emptyLane = true;
                            }
                            if (emptyLane) {
                                let entry = {
                                    athleteId: 0,
                                    heatInfo: {
                                        heatNo: ((heatCnt * page) - heatCnt) + 1,
                                        laneNo: lane
                                    },
                                    emptyLane: "Lane Empty"
                                };
                                setTrackInfo(entry, eventGroup, null, false, page);
                            }
                        }
                    }
                }
                return firstEntry !== null ? true : false;
            }

            function isEntrySeeded(entry) {
                if (typeof entry.heatInfo.seeded === "undefined") {
                    return true;
                }
                return entry.heatInfo.seeded;
            }

            function setSeeded(entry, seeded) {
                entry.heatInfo.seeded = seeded;
            }

            function populateCardHeader(eventGroup) {
                let competition = getCompetition();
                let cardType = "Track";
                let cardTitle = "E4S " + cardType + " Card";
                let pageNo = 1;
                let eventNo = getEventNoFromEventGroup(eventGroup);
                if (isEventGroupField(eventGroup)) {
                    cardType = "Field";
                    cardTitle = "E4S " + cardType + " Card";
                    pageNo = getFieldCardPool();
                } else {
                    if (isEventATeamEvent(eventGroup)) {
                        cardTitle = "E4S Team Entry List";
                    } else if (!areCardsAvailable(eventGroup)) {
                        cardTitle = "E4S Entry List";
                    }
                    if (eventGroup.options){
                        if (eventGroup.options.seed){
                            if (eventGroup.options.seed.qualifyToEg) {
                                if (eventGroup.options.seed.qualifyToEg.id > 0) {
                                    if (eventGroup.options.seed.qualifyToEg.rules) {
                                        cardTitle += " - To Qualify : first " + eventGroup.options.seed.qualifyToEg.rules.auto + " and fastest " + eventGroup.options.seed.qualifyToEg.rules.nonAuto;
                                    }
                                }
                            }
                        }
                    }
                }
                let entries = getEntriesForEg(eventGroup);
                let entryCount = 0;
                let entryCountCheckedIn = 0;

                for (let e in entries) {
                    let entry = entries[e];
                    let useTotalCount = true;
                    if ( isEventGroupField(eventGroup) ){
                        if ( getViewMode() === '<?php echo E4S_CARDMODE_CARD ?>') {
                            useTotalCount = getEntryRaceNo(entry) === pageNo;
                        }
                    }
                    if (useTotalCount) {
                        if (cardShowsCheckedIn()) {
                            if (isEntryCheckedIn(entry)) {
                                entryCountCheckedIn++;
                            }
                        } else {
                            entryCount++;
                        }
                    }
                }

                $("#card_title").text(cardTitle);
                $("#card_competition").text(competition.name);
                $("#card_date").text(formatDate(eventGroup.date, 'D MMM yy HH:mm'));
                let venue = competition.location;
                if (eventGroup.options.venue) {
                    venue = eventGroup.options.venue;
                }
                $("#card_venue").text(venue);
                let useEntryCount = entryCount;
                if (cardShowsCheckedIn()) {
                    useEntryCount = entryCountCheckedIn;
                }
                $("#card_event").text(eventGroup.event + " (" + useEntryCount + " entries)");
                if (eventGroup.typeno !== "") {
                    $("#card_eventno").text(eventGroup.typeno);
                } else {
                    $("#card_eventno").text(eventNo);
                }
                populateHeaderStart(eventGroup);

                let trialInfo = eventGroup.options.trialInfo;
                let standardText = checkStandards(eventGroup);
                let compInfo = competition.options.cardInfo;
                if ( trialInfo === '') {
                    if (cardType === 'Field') {
                        trialInfo = compInfo.field;
                    }
                    if (cardType === 'Track') {
                        trialInfo = compInfo.track;
                    }
                }
                if (trialInfo !== "" && standardText !== "" ){
                    trialInfo += ". ";
                }
                trialInfo += standardText;
                $("#card_trials").text(trialInfo);
            }

            function populateHeaderStart(eventGroup){
                let eventNo = eventGroup.eventno;
                let useTime = "";
                if ( isEventGroupField(eventGroup) ) {
                    let results = getResults();

                    if (typeof results[eventNo] !== "undefined") {
                        // we have results
                        if (typeof results[eventNo][0] !== "undefined") {
                            useTime = results[eventNo][0];
                            // if only 0 exists, first result as been deleted
                            let hasResults = false;
                            for(let r in results[eventNo]){
                                if (r !== "0"){
                                    hasResults = true;
                                    break;
                                }
                            }
                            if ( !hasResults){
                                useTime = "";
                            }
                        }else{
                            // first result and [0] not set
                            let uiDate = new Date();
                            useTime = formatDate(uiDate, "hh:mm");
                            results[eventNo][0] = useTime;
                        }
                    }
                }
                $("#header_start_time").text(useTime);
            }

            function checkStandards(eventGroup){
                getEventGroupStandards(eventGroup);
                let standardText = "";
                for ( let s in eventGroup.standards ){
                    let val = getDecimals(eventGroup.standards[s],2);
                    standardText += " " + s + ": " + val + getEventUOM(eventGroup, val);
                }
                return standardText;
            }
            function getEventGroupStandards(eventGroup = null){
                if ( eventGroup === null ){
                    eventGroup = getCurrentEventGroup();
                }
                let egId = getEventGroupId(eventGroup);
                let arr = Array();
                let ceArr = getCompEvents();
                let standards = getCompetition().standards;
                for(let ceId in ceArr){
                    let ce = ceArr[ceId];
                    if ( ce.egId === egId) {
                        for(let s in standards) {
                            let standard = standards[s];
                            let key = ce.ageGroupId + "-" + ce.eventId;
                            if (typeof standard[key] !== "undefined") {
                                let val = "" + standard[key][0].valueText;
                                val = val.replace('<?php echo E4S_ENSURE_STRING ?>','');
                                arr[s] = getFloatDecimals(val,2);
                            }
                        }
                    }
                }

                eventGroup.standards = arr;

                return arr;
            }
            function populateTrackFooter(eventNo) {
                let eg = getEventGroup(eventNo);
                $("#track_info").text(eg.options.reportInfo);
            }

            function populateFieldFooter(eventNo) {
                let eg = getEventGroup(eventNo);
                let reportInfo = eg.options.reportInfo;
                let competition = getCompetition();
                if ( reportInfo === ''){
                    let compCard = competition.options.card.footer;
                    reportInfo = compCard.field;
                }
                let notCheckedInBibs = [];
                if ( isPrinting() && !cardShowsCheckedIn()){
                    let entries = getEntriesForEg(eg);
                    let sep = '';
                    let plural = '';
                    let plural2 = 'is';

                    if ( reportInfo !== '' ){
                        sep = '. ';
                    }
                    for (let e in entries){
                        let entry = entries[e];
                        if ( !isEntryCheckedIn(entry) && isCheckInInUse() ) {
                            let bibNo = getAthleteById(entry.athleteId).bibno;
                            notCheckedInBibs.push(getBibNo(bibNo, entry));
                        }
                    }
                    let totalNotCheckedIn = notCheckedInBibs.length;
                    let bibNos = '';
                    if (totalNotCheckedIn > 0 ) {
                        if (totalNotCheckedIn === 1) {
                            bibNos = notCheckedInBibs[0];
                        }
                        if (totalNotCheckedIn > 1) {
                            plural = 's';
                            plural2 = 'are';
                            if (totalNotCheckedIn === 2) {
                                bibNos = notCheckedInBibs[0] + ' and ' + notCheckedInBibs[0];
                            }else{
                                let grammer = '';
                                for(let c in notCheckedInBibs) {
                                    c = parseInt(c);
                                    if (c > 0) {
                                        grammer = ',';
                                    }
                                    if ( c === (totalNotCheckedIn - 1) ){
                                        grammer = ' and ';
                                    }
                                    bibNos += (grammer + notCheckedInBibs[c]);
                                }
                            }
                        }

                        reportInfo += (sep + 'At time of printing, Bib number' + plural + ' ' + bibNos + ' ' + plural2 + ' not checked in and may not be present.');
                    }
                }
                if ( isEventAHeightEvent(eg)) {
                    reportInfo = heightCard.getFooterInfo(reportInfo);
                }else{
                    reportInfo = distanceCard.getFooterInfo(reportInfo);
                }
                $("#field_info").html(reportInfo);
            }

            function getFirstLane(eventGroup) {
                if (!eventGroup) {
                    eventGroup = getCurrentEventGroup();
                }
                if (!isEventGroupTrack()) {
                    return 1;
                }
                let egOptions = eventGroup.options;

                return getFirstLaneFromOptions(egOptions);
            }

            function getFirstLaneFromOptions(options) {
                if (options) {
                    if (options.seed) {
                        if (options.seed.firstLane) {
                            return options.seed.firstLane;
                        }
                    }
                }
                return 1;
            }

            function setFirstLane(eventGroup, value) {
                if (value === 0) {
                    value = 1;
                }
                setFirstLaneInOptions(eventGroup.options, value);
            }

            function setFirstLaneInOptions(options, value) {
                if (!options.seed) {
                    options.seed = {};
                }
                options.seed.firstLane = value;
            }

            function setDoubleUpInOptions(options, value) {
                if (!options.seed) {
                    options.seed = {};
                }
                if (typeof value === "string") {
                    options.seed.doubleup = value.split(",");
                }else{
                    options.seed.doubleup = value;
                }
            }
            function getAutoQualifyFromOptions(options){
                if ( options.seed.qualifyToEg ) {
                    if ( options.seed.qualifyToEg.rules ) {
                        return options.seed.qualifyToEg.rules.auto;
                    }
                }
                return 2;
            }
            function getNonAutoQualifyFromOptions(options){
                if ( options.seed.qualifyToEg ) {
                    if ( options.seed.qualifyToEg.rules ) {
                        return options.seed.qualifyToEg.rules.nonAuto;
                    }
                }
                return 2;
            }
            function getSeedAgeFromOptions(options){
                return options.seed.age;
            }
            function getDoubleUpFromOptions(options){
                if ( options.seed.doubleup ) {
                    return options.seed.doubleup;
                }
                return "";
            }
            function setDoubleUp(eventGroup, value){
                setDoubleUpInOptions(eventGroup.options, value);
            }
            function getSeedGenderFromOptions(options){
                return options.seed.gender;
            }

            function setSeedGender(eventGroup, value){
                setSeedGenderInOptions(eventGroup.options, value);
            }
            function setSeedGenderInOptions(options, value) {
                if (!options.seed) {
                    options.seed = {};
                }
                options.seed.gender = value;
            }
            function setSeedAge(eventGroup, value){
                setSeedAgeInOptions(eventGroup.options, value);
            }
            function setSeedAgeInOptions(options, value) {
                if (!options.seed) {
                    options.seed = {};
                }
                options.seed.age = value;
            }

            function setLaneCount(eventGroup, value) {
                if (value === 0) {
                    value = getCompetitionLaneCount();
                }
                setLaneCountInOptions(eventGroup.options, value);
            }

            function setLaneCountInOptions(options, value) {
                if (!options.seed) {
                    options.seed = {};
                }
                options.seed.laneCount = value;
            }

            function setMaxInHeatForEventGroup(eg, value) {
                let options = eg.options;
                setMaxInHeat(options, value);
            }

            function setMaxInHeat(obj, value) {
                obj.maxInHeat = value;
            }

            function setAutoQualify(eg, value) {
                let options = eg.options;
                setAutoQualifyInOptions(options, value);
            }
            function setAutoQualifyInOptions(options, value){
                if ( !options.seed ){
                    options.seed = {};
                }
                if ( !options.seed.qualifyToEg ){
                    options.seed.qualifyToEg = {};
                }
                if ( !options.seed.qualifyToEg.rules ){
                    options.seed.qualifyToEg.rules = {};
                }
                options.seed.qualifyToEg.rules.auto = value;
            }
            function setNonAutoQualify(eg, value) {
                let options = eg.options;
                setNonAutoQualifyInOptions(options, value);
            }
            function setNonAutoQualifyInOptions(options, value){
                if ( !options.seed ){
                    options.seed = {};
                }
                if ( !options.seed.qualifyToEg ){
                    options.seed.qualifyToEg = {};
                }
                if ( !options.seed.qualifyToEg.rules ){
                    options.seed.qualifyToEg.rules = {};
                }
                options.seed.qualifyToEg.rules.nonAuto = value;
            }
            function isCheckInInUse() {
                // InUse is set once at least 1 athlete has checked in
                return outputReportData.checkInInUse && isCheckInEnabled();
            }

            function setEntryCheckedIn(entry, state) {
                entry.checkedIn = state;
                if ( state ){
                    setEntryCollected(entry, true);
                }
            }

            function isEntryCheckedIn(entry, eventGroup) {
                if (!eventGroup) {
                    eventGroup = getCurrentEventGroup();
                }
                if (eventGroup.options.maxathletes < 0 && eventGroup.options.entriesFrom.id === 0) {
                    return true;
                }

                return entry.checkedIn && isBibCollected(entry) && isCheckInEnabled();
            }
            function isBibCollected(entry){
                let bibObj = getBibObjForEntry(entry);
                if ( bibObj ) {
                    return bibObj.collected;
                }
                return true;
            }
            function getBibObjForEntry(entry){
                if ( typeof entry.athleteId !== "undefined" ) {
                    let athlete = getAthleteById(entry.athleteId);
                    let bibNos = getBibNos();
                    return bibNos[athlete.bibno];
                }
                return 0;
            }
            function setEntryCollected(entry, collected){
                let bibObj = getBibObjForEntry(entry);
                if ( typeof bibObj !== "undefined" ) {
                    bibObj.collected = collected;
                }
            }
            function setEntryPresent(entry, state){
                entry.present = state;
            }
            function isEntryPresent(entry, ignoreCheckIn = false) {
                if ( isCheckInInUse() && !ignoreCheckIn ){
                    return isEntryCheckedIn(entry) && entry.present;
                }
                return entry.present;
            }

            function getAthleteEntryFromEntries(athleteId, entries){
                for(let e in entries){
                    let entry = entries[e];
                    if ( entry.athleteId === athleteId ){
                        return entry;
                    }
                }
                return null;
            }
            function getEntryFromEntries(entryId, entries){
                for(let e in entries){
                    let entry = entries[e];
                    if ( entry.teamid && entry.teamid === entryId ){
                        return entry;
                    }
                    if ( getEntryId(entry) === entryId ){
                        return entry;
                    }
                }
                return null;
            }

            function setEntryHeatNo(entry, heatNo){
                if (isCheckInEnabled() && cardShowsCheckedIn()) {
                    setHeatNoCheckedIn(entry, heatNo);
                }else{
                    setHeatNo(entry, heatNo);
                }
            }
            function setHeatNo(entry, heatNo){
                entry.heatInfo.heatNo = heatNo;
            }
            function setHeatNoCheckedIn(entry, heatNo){
                entry.heatInfo.heatNoCheckedIn = heatNo;
            }
            function getEntryRaceNo(entry) {
                let heatNo = getRaceNo(entry);

                if (isCheckInEnabled() && cardShowsCheckedIn()) {
                    heatNo = getRaceNoCheckedIn(entry);
                }
                return heatNo;
            }
            function getRaceNo(entry) {
                return entry.heatInfo.heatNo;
            }
            function getRaceNoCheckedIn(entry) {
                return entry.heatInfo.heatNoCheckedIn;
            }

            function setEntryLaneNo(entry, laneNo){
                if (isCheckInEnabled() && cardShowsCheckedIn()) {
                    setLaneNoCheckedIn(entry, laneNo);
                }else{
                    setLaneNo(entry, laneNo);
                }
            }

            function setLaneNo(entry, laneNo){
                entry.heatInfo.laneNo = laneNo;
            }
            function setLaneNoCheckedIn(entry, laneNo){
                entry.heatInfo.laneNoCheckedIn = laneNo;
            }
            function getEntryLaneNo(entry) {
                let laneNo = getLaneNo(entry);
                if (isCheckInEnabled() && cardShowsCheckedIn()) {
                    laneNo = getLaneNoCheckedIn(entry);
                }
                return laneNo;
            }
            function getLaneNo(entry){
                return entry.heatInfo.laneNo;
            }
            function getLaneNoCheckedIn(entry){
                return entry.heatInfo.laneNoCheckedIn;
            }

            function displayTrackEntityPresence(entry, eventGroup) {
                let entity = null;
                let bibNo = 0;
                if ( isEventATeamEvent(eventGroup) ) {
                    entity = getTeamObject(entry, eventGroup);
                    bibNo = entity.bibno;
                }else {
                    entity = getAthleteById(entry.athleteId);
                    bibNo = getBibNoToUse(entity, entry);
                }
                let heatPageCount = getTrackHeatCount();
                let page = getCardPage();
                let factor = (page - 1) * heatPageCount;
                let statusClass = '';
                let status2Class = 'bibStatus2';
                let presentClass = e4sGlobal.class.bibPresent;
                if ('' + bibNo !== '') {
                    if ( isCheckInInUse() && !isEntryCheckedIn(entry) ){
                        statusClass = e4sGlobal.class.bibNotCheckedIn;
                    }else if (!isEntryPresent(entry) ){
                        statusClass = e4sGlobal.class.bibNotPresent;
                    }else{
                        statusClass = presentClass;
                    }
                }
                let dns = statusClass !== presentClass;
                let heatNo = getEntryRaceNo(entry);
                let laneNo = getEntryLaneNo(entry);
                let bibFieldKey = "#bib_" + (heatNo - factor) + "_" + laneNo;
                let bibStatusField = $(bibFieldKey + "_status_cell");
                if (typeof bibStatusField.attr('class') === "undefined") {
                    // e4sAlert("There is an issue with the configuration of this event. If you have changed parameters, try re-seeding","Configuration Error");
                    return;
                }
                let bibField = $(bibFieldKey);
                bibField.attr("eId", getEntryId(entry));
                bibStatusField.addClass(statusClass).attr("e4s_ToBeReset", true);

                let age = entity.ageshortgroup;
                if (dns && bibField.text() !== "<?php echo E4S_NO_BIBNO ?>") {
                    if ( quickConfirm.getStatus() ) {
                        age = '<input type="checkbox" onchange="easySwitchPresent(' + heatNo + ',' + laneNo + ');"><span style="font-size:0.7vw;max-font-size: 16px;"> Confirm</span>';
                    } else{
                        age = 'DNS';
                    }
                }
                $("#age_" + (heatNo - factor) + "_" + laneNo).html(age);
            }

            function formatPbForScreen(pb, eventGroup) {
                if (typeof pb === "number") {
                    if (pb === 0) {
                        return "0.00";
                    }
                    return pb;
                }
                pb = pb.replace("#0:", "#");
                if ( typeof eventGroup === 'undefined'){
                    eventGroup = getCurrentEventGroup();
                }
                let uom = getEventUOM(eventGroup,pb);
                if (uom === "pts" && pb.indexOf(".") > 0) {
                    pb = pb.split(".")[0];
                }
                pb += " " + uom;
                return pb.replace("#", "");
            }

            function getEventUOM(eventGroup, score){
                if ( score.indexOf(":") > 0){
                    return "m";
                }
                let uomOptions = eventGroup.uomOptions[0];
                return uomOptions.short;
            }
            function getAthleteNameForCard(athlete, entry) {
                let pb = formatPbForScreen(entry.pb);
                let suffix = '';

                if ( !(entry.pb === "#0.00" || entry.pb === "#0:00.00") ){
                    suffix = " ( " + pb;
                    let eOptions = entry.eoptions;
                    if (typeof eOptions.qualify !== "undefined") {
                        suffix += " " + eOptions.qualify.type + eOptions.qualify.heatNo ;
                    }
                    // showRanking in securefunctions
                    var rank = rankObj.getEventForAthlete(athlete.id, getCurrentEventGroup().eventId);
                    rank = 44;
                    if (rank > 0) {
                        suffix += rankObj.getDisplay(rank);
                    }
                    suffix += " )";
                }
                return "<span class='athleteName'>" + getNameFromEntity(athlete, entry.tf) + "</span><span class='athletepb'>" + suffix + "</span>";
            }

            function getBibNoToUse(entity, entry){
                let bibNo = entity.bibno;
                if ( entity.teamid ){
                    bibNo = entity.bibNo;
                }
                return getBibNo(bibNo, entry);
            }
            function getBibNo(bibNo, entry) {
                if (entry !== null) {
                    if (typeof entry.teambibno !== "undefined") {
                        if (entry.teambibno !== "" && entry.teambibno !== 0) {
                            return entry.teambibno;
                        }
                    }
                }

                if (typeof bibNo === "string") {
                    bibNo = bibNo.trim();
                    if (bibNo === "") {
                        bibNo = 0;
                    }
                }
                if (bibNo === 0) {
                    return '<?php echo E4S_NO_BIBNO ?>'
                }

                return bibNo;
            }

            function setHeatConfirmed(heatNo, confirmed) {
                let heatDiv = "#track_heat_no" + heatNo;
                let heatDivObj = $(heatDiv);
                if (confirmed === 1) {
                    heatDivObj.addClass("heatConfirmed");
                } else {
                    heatDivObj.removeClass("heatConfirmed");
                }
            }

            function setCompNotesForAthleteId(athleteId, notes){
                let athlete = getAthleteById(athleteId);
                let bibs = getBibNos();
                let bibObj = bibs[athlete.bibno];
                if ( bibObj ) {
                    bibObj.compNotes = notes;
                }
            }
            function getBibNotesForAthlete(athlete) {
                let bibNo = athlete.bibno;
                return getBibNotesForBibNo(bibNo);
            }

            function getBibNotesForBibNo(bibNo) {
                let notes = "";
                if (bibNo !== 0) {
                    let bibs = getBibNos();
                    let bibObj = bibs[bibNo];
                    notes = bibObj.compNotes;
                }
                return notes;
            }

            function markNotes(athleteSelect, athlete) {
                let notes = "";
                if (athlete !== null) {
                    notes = getBibNotesForAthlete(athlete);
                }

                if (notes !== "") {
                    let bibSelect = athleteSelect.replace("athlete", "bib");
                    $(bibSelect).addClass("e4sCardNotes");
                }
            }

            function setTrackInfo(entry, eventGroup, entity, cardShowsCheckedIn, page) {
                let useHeatNo = getEntryRaceNo(entry);
                let useLaneNo = getEntryLaneNo(entry);
                let pageSize = getPageSizeForEntry(entry);
                let pageFactor = (page * pageSize) - pageSize;
                let eventIsSchedOnly = eventGroup.options.maxathletes === -1;
                if (cardShowsCheckedIn) {
                    if (!isEntryCheckedIn(entry)) {
                        return false;
                    }
                }

                let minHeatNo = pageFactor + 1;
                let maxHeatNo = (page * pageSize);
                if (useHeatNo < minHeatNo || useHeatNo > maxHeatNo) {
                    return false;
                }
                useHeatNo -= pageFactor;

                let heat_lane = useHeatNo + "_" + useLaneNo;
                let entryCell = "#<?php echo E4S_CARD_FIELD_PREFIX ?>" + heat_lane;
                let bibSelectCell = "#bib_" + heat_lane + "_cell";
                let bibSelect = "#bib_" + heat_lane;
                let ageSelect = "#age_" + heat_lane;
                let athleteSelect = "#athlete_" + heat_lane;
                let clubSelect = "#club_" + heat_lane;

                setHeatConfirmed(useHeatNo, entry.heatInfo.confirmed);

                if ( isEventATeamEvent(eventGroup)){
                    $("#<?php echo E4S_CARD_FIELD_PREFIX ?>" + heat_lane).removeClass("ui-state-disabled");
                    $(bibSelectCell).addClass('bibNoMenu');
                    $(bibSelectCell).html( getBibHTML(useHeatNo, useLaneNo, entity.bibno, 'overBib', entry,''));

                    addQualifyClass(bibSelectCell, entry);
                    $(ageSelect).text(entity.ageshortgroup);
                    let teamName = entity.clubname.replace("Team ","");
                    let eOptions = entry.options;

                    if (typeof eOptions.qualify !== "undefined") {
                        let score = eOptions.qualify.score;
                        score = score.replace('<?php echo E4S_ENSURE_STRING ?>','');
                        teamName += " ( " + score + " " + eOptions.qualify.type + eOptions.qualify.heatNo + " )";
                    }
                    $(athleteSelect).html(teamName);
                    $(clubSelect).text('');
                    $(entryCell).attr("entryid", entry.teamid);
                }else if (entry.athleteId !== 0) {
                    $("#<?php echo E4S_CARD_FIELD_PREFIX ?>" + heat_lane).removeClass("ui-state-disabled");
                    $(bibSelectCell).addClass('bibNoMenu');
                    $(bibSelectCell).html( getBibHTML(useHeatNo, useLaneNo, getBibNoToUse(entity, entry), 'overBib', entry,''));
                    addQualifyClass(bibSelectCell, entry);
                    //if ( entry.paid === <?php //echo E4S_ENTRY_QUALIFY ?>// ) {
                    //    $(bibSelectCell).addClass("e4s_qualify_entry");
                    //}
                    $(ageSelect).text(entity.ageshortgroup);
                    $(athleteSelect).html(getAthleteNameForCard(entity, entry));

                    $(clubSelect).text(entity.clubname);
                    $(entryCell).attr("entryid", getEntryId(entry));
                    markNotes(athleteSelect, entity);
                } else {
                    $(bibSelectCell).text("");
                    removeQualifyClass($(ageSelect).text(""));
                    // $(ageSelect).text("").removeClass("e4s_qualify_entry");
                    if (typeof entry.emptyLane !== "undefined") {
                        $(athleteSelect).text(entry.emptyLane);
                    } else {
                        $(athleteSelect).text("");
                    }

                    $(clubSelect).text("");
                    markNotes(athleteSelect, null)
                }
                $(clubSelect).addClass("<?php echo E4S_CLASS_SEEDED ?>");
                $(clubSelect).removeClass("<?php echo E4S_CLASS_TO_BE_SEEDED ?>");
                if (cardShowsCheckedIn || eventIsSchedOnly) {
                    if (!isEntrySeeded(entry)) {
                        setHeatLaneUnSeeded(useHeatNo, useLaneNo, eventGroup);
                    }
                }
                displayTrackEntityPresence(entry, eventGroup);
                return true;
            }

            function addQualifyClass(selector, entry){
                let eg = getCurrentEventGroup();
                if ( eg.options.maxathletes > -1 ) {
                    if (entry.paid === <?php echo E4S_ENTRY_QUALIFY ?>) {
                        $(selector).addClass("e4s_qualify_entry");
                    }
                }
            }
            function removeQualifyClass(obj){
                obj.removeClass("e4s_qualify_entry");
            }
            function setEntryUnSeeded(entry, eventGroup){
                let useHeatNo = getEntryRaceNo(entry);
                let useLaneno = getEntryLaneNo(entry);
                setHeatLaneUnSeeded(useHeatNo, useLaneno, eventGroup);
            }
            function setHeatLaneUnSeeded(heatNo, laneNo, eventGroup) {
                let clubSelect = $("#club_" + heatNo + "_" + laneNo);
                clubSelect.text("TO BE SEEDED")
                    .removeClass("<?php echo E4S_CLASS_SEEDED ?>")
                    .addClass("<?php echo E4S_CLASS_TO_BE_SEEDED ?>");
                setEventGroupSeeded(eventGroup, false);
            }

            function isHeatLaneUnSeeded(heatNo, laneNo) {
                let clubSelect = $("#club_" + heatNo + "_" + laneNo);
                return clubSelect.hasClass("<?php echo E4S_CLASS_TO_BE_SEEDED ?>");
            }

            function formatChanged() {
                let formatSet = e4s_getPrintMode();
                if (formatSet === '<?php echo E4S_CARDMODE_CARD ?>') {
                    // force page break and hide
                    $("#cardPageBreak").attr("checked", "checked");
                    $("#e4sPrintPageBreakDiv").hide();
                } else {
                    // show pagebreak
                    $("#e4sPrintPageBreakDiv").show();
                }
            }

            function e4s_print_getEventsForFilter(filter, element) {
                let egs = getEventGroups();
                let currentEg = getCurrentEventGroup();
                let eventSelection = "";

                if (filter === "") {
                    eventSelection += e4s_print_getHTMLForEventGroup(currentEg, true);
                } else {
                    for (let eg in egs) {
                        let eventGroup = egs[eg];
                        if ( eventGroup.options.cardDisplay ) {
                            if (getEventGroupType(eventGroup) === filter) {
                                eventSelection += e4s_print_getHTMLForEventGroup(eventGroup, false);
                            }
                        }
                    }
                }
                $("#" + element).html(eventSelection);
                e4s_printInitSections();
            }

            function e4s_print_getHTMLForEventGroup(eventGroup, checked) {
                let checkedClass = '';
                 // check if checked in is in usse and they have not been seeded
                if ( eventGroup.type === '<?php echo E4S_EVENT_TRACK ?>') {
                    if (isCheckInInUse()){
                        if (!isEventGroupSeeded(eventGroup)){
                            checkedClass = 'print_warn_checkin';
                        }
                    }
                }
                let html = '<span class="e4s_print_eventSelection ' + checkedClass + '"><input type="checkbox" id="print' + eventGroup.typeno + '" name="eventToPrint" ';
                if (checked) {
                    html += ' checked="checked" ';
                }
                html += 'value="' + getEventNoFromEventGroup(eventGroup) + '">';
                html += '<label for="print' + eventGroup.typeno + '">' + getEventGroupTitle(eventGroup) + '</label></span>';
                return html;
            }

            function getPrintTypeSelected() {
                return $("input[name=eventType]:checked").val();
            }

            function e4s_print_dialog() {
                let egs = getEventGroups();
                let currentEg = getCurrentEventGroup();
                let hasTrack = false;
                let hasField = false;
                let eventElement = 'e4sPrintEvents';

                for (let eg in egs) {
                    if (egs[eg].type === "<?php echo E4S_EVENT_FIELD ?>") {
                        hasField = true;
                    }
                    if (egs[eg].type === "<?php echo E4S_EVENT_TRACK ?>") {
                        hasTrack = true;
                    }
                }
                let html = "";
                html += '<div id="e4sPrintDialog">';
                html += 'Please select the events you would like to print.';
                html += '<div id="e4sPrintFilters" style="display:inline-flex;padding: 10 0 0 0;">';
                html += '<span id="e4sPrintFilterLabel">Event Type : </span>';
                let checked = 'checked="checked" ';
                let currentType = "-";

                if (currentEg !== null) {
                    html += '<input type="radio" id="currentCard" name="eventType" ' + checked + ' value="" onchange="e4s_print_getEventsForFilter(\'\',\'' + eventElement + '\');">';
                    html += '<label for="currentCard">Current Card</label><br>';
                    checked = "";
                    currentType = "";
                }
                if (hasTrack) {
                    if (currentType === "-") {
                        currentType = "<?php echo E4S_EVENT_TRACK ?>";
                    }
                    html += '<input type="radio" id="html" name="eventType" value="<?php echo E4S_EVENT_TRACK ?>" ' + checked + ' onchange="e4s_print_getEventsForFilter(\'<?php echo E4S_EVENT_TRACK ?>\',\'' + eventElement + '\');">';
                    html += '<label for="Track">Track</label>';
                    checked = "";
                }
                if (hasField) {
                    if (currentType === "-") {
                        currentType = "<?php echo E4S_EVENT_FIELD ?>";
                    }
                    html += '<input type="radio" id="css" name="eventType" value="<?php echo E4S_EVENT_FIELD ?>" ' + checked + ' onchange="e4s_print_getEventsForFilter(\'<?php echo E4S_EVENT_FIELD ?>\',\'' + eventElement + '\');">';
                    html += '<label for="Field">Field</label><br>';
                    checked = "";
                }

                html += '</div>';

                html += '<div id="e4sPrintTrackFormatDiv" class="e4sPrintTrackFormatDiv">';
                html += '<span id="e4sPrintFormatLabel">Format : </span>';
                html += '<input type="radio" id="cardFormat" name="printFormat" checked="checked" value="<?php echo E4S_CARDMODE_CARD ?>" onchange="formatChanged();">';
                html += '<label for="cardFormat">Card (if set)</label>';
                html += '<input type="radio" id="listFormat" name="printFormat" value="<?php echo E4S_CARDMODE_NONECARD ?>" onchange="formatChanged();">';
                html += '<label for="listFormat">List</label>';

                html += '<span id="e4sPrintPageBreakDiv">';
                // html += '<span id="e4sPrintPageBreakLabel">Page Break : </span>';
                html += '<input type="checkbox" id="cardPageBreak" name="cardPageBreak" value="Yes">';
                html += '<label for="cardPageBreak">Page Break between Events</label>';
                html += '</span>';
                html += '</div>';

                html += '<div id="e4sPrintSelection" class="e4sPrintSelectionDiv">';
                html += '<button type="button" class="ui-button ui-corner-all ui-widget e4sButtonSelectAll" onclick="e4s_printSelect(true);">';
                html += '<span class="ui-button-icon ui-icon ui-icon-circle-plus"></span>';
                html += '<span class="ui-button-icon-space"> </span>Select All</button>';

                html += '<button type="button" class="ui-button ui-corner-all ui-widget" onclick="e4s_printSelect(false);">';
                html += '<span class="ui-button-icon ui-icon ui-icon-circle-minus"></span>';
                html += '<span class="ui-button-icon-space"> </span>Clear Selection</button>';
                html += '</div>';

                html += '<div id="e4sPrintEvents" class="e4sPrintEventsDiv">';
                // if ( currentEg !== null ) {
                //     html += e4s_print_getHTMLForEventGroup(currentEg, true);
                // }

                html += '</div>';
                if ( isCheckInInUse() ) {
                    html += '<div class="print_warn_checkin" style="padding-top: 20px;">Any highlighted events are awaiting seeding</div>';
                }
                html += '</div>';
                $("#e4sPrintDialog").remove();
                $(html).dialog({
                    width: 600,
                    height: 700,
                    modal: true,
                    title: "Entry4Sports Card Print",
                    open: function (event, ui) {
                        e4s_print_getEventsForFilter(currentType, eventElement);
                        $("#e4sPrintDialog").addClass("e4sPrintDialogDiv");
                    },
                    buttons: [
                        {
                            text: "Print",
                            icon: "ui-icon-print",
                            click: function () {
                                e4s_printSelected();
                            }
                        },
                        {
                            text: "Cancel",
                            icon: "ui-icon-closethick",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ]
                });
            }

            function e4s_printSelect(selectAll) {
                if (selectAll) {
                    $("[name=eventToPrint]").attr("checked", "checked");
                } else {
                    $("[name=eventToPrint]").removeAttr("checked");
                }
            }

            function e4s_printInitSections() {
                let selectedType = getPrintTypeSelected();
                let selectedTypeOrig = selectedType;
                if (selectedType === "") {
                    let currentEg = getCurrentEventGroup();
                    selectedType = currentEg.type;
                }

                // select buttons
                if (selectedTypeOrig !== "") {
                    $("#e4sPrintSelection").show();
                } else {
                    $("#e4sPrintSelection").hide();
                }
                formatChanged();
            }

            function e4s_getPrintMode() {
                return $("input[name=printFormat]:checked").val();
            }

            function e4s_printSelected() {
                let egs = getEventGroups();
                let selectedEGs = [];
                let userMode = getViewMode();
                $("[name=eventToPrint]:checked").each(
                    function (e) {
                        let selected = $(this);
                        let eventNo = parseInt(selected.val());
                        selectedEGs.push(getEventGroup(eventNo));
                    });
                if (selectedEGs.length > 0) {
                    // set selected Print Format
                    setViewMode(e4s_getPrintMode());

                    e4s_print_eventGroups(selectedEGs);

                    // restore users Mode
                    setViewMode(userMode);
                    cardChange();
                }
            }

            function e4s_print_eventGroups(eventGroups) {
                showPleaseWait(true, "Printing");
                let cssHtml = '<?php getCss() ?>';
                let cardTableHTMLForPrint = "";
                setPrinting(true);
                for (let eg in eventGroups) {
                    let eventGroup = eventGroups[eg];
                    let egPrint = returnEventGroupPrint(eventGroup);

                    if ( egPrint !== "") {
                        cardTableHTMLForPrint += egPrint;
                        if (pageBreakSelected()) {
                            cardTableHTMLForPrint += addPageBreak();
                        }
                    }
                }

                setCardPage(1);
                setPrinting(false);
                cardChange();
                showPleaseWait(false);
                e4s_print("" + cssHtml + cardTableHTMLForPrint);
            }

            function pageBreakSelected() {
                return $("#cardPageBreak").is(':checked');
            }

            function e4s_print_card() {
                let eventGroup = getCurrentEventGroup();
                if (eventGroup === null) {
                    e4sAlert("Please select an event");
                    return;
                }
                e4s_print_eventGroups([eventGroup]);
            }

            function returnEventGroupPrint(eventGroup) {
                let cardTableHTMLForPrint = "";
                let lastPageHTML = "";
                let pageCount = getEventGroupPages(eventGroup);
                for (let page = 1; page <= pageCount; page++) {
                    setCardPage(page);
                    let athletesAdded = populateCard(eventGroup);
                    let cardTableHTML = $("#card-base").html();
                    if (lastPageHTML !== cardTableHTML && athletesAdded) {
                        if (page > 1) {
                            cardTableHTMLForPrint += addPageBreak();
                        }
                        cardTableHTMLForPrint += cardTableHTML;
                    }
                    lastPageHTML = cardTableHTML;
                }
                return cardTableHTMLForPrint;
            }

            function addPageBreak() {
                return '<div class="pageBreak"></div>';
            }

            function e4s_print(sourceHTML) {
                let newWin = window.open("", "", "width=1200, height=700"),
                    doc = newWin.document.open();
                doc.write(sourceHTML);
                doc.close();
                newWin.print();
            }

            function formatDateForTimetronics(date) {
                if ( date === null ){
                    return '';
                }
                let useDate = date.split("-");
                return useDate[2] + "/" + useDate[1] + "/" + useDate[0];
            }

            function seedOnEntriesOnly() {
                let checkinOptions = getCompetition().options.checkIn;
                let entriesOnly = checkinOptions.seedOnEntries;
                if (typeof entriesOnly === "undefined") {
                    entriesOnly = false;
                }
                return entriesOnly;
            }

            function isEventNoInSelected(choices, eventNo) {
                return choices.includes(eventNo);
            }

            function toTitleCase(str) {
                return str.replace(
                    /\b\w+/g,
                    function (txt) {
                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    }
                );
            }
            function markRefreshRequired(){
                $("#Refresh").addClass("refreshRequired");
            }
            $(function () {
                <?php if (showIndivTab()) { ?>
                changeToGrid("individual_table", "individual_grid", "Individual Entries");
                <?php } if (showAthleteTab()) { ?>
                changeToGrid("athlete_table", "athlete_grid", "Athletes");
                <?php } if (hasFinancialAccess()) { ?>
                changeToGrid("finance_table", "finance_grid", "Finances");
                <?php } if (showTeamTab()) { ?>
                changeToGrid("team_table", "team_grid", "Teams");
                <?php } ?>
                window.onbeforeunload = e4s_onUnload;

                onLoadCardChange();
                <?php if (showIndivTab() or showAthleteTab() or hasFinancialAccess() or showTeamTab() ) { ?>
                populateTree(getTreeData(), "tree_table", "tree_grid", "team_tree_grid");
                <?php } ?>
                $("#tabs").tabs({
                    beforeActivate: function (event, ui) {
                        showTitle(ui.newTab[0].innerText);
                    }
                });
                $("#tabs").show();
                initToaster();

                showPleaseWait(false);
                let launchFunction = getLaunchOption();
                if (launchFunction !== "") {
                    writeLaunchOption("");
                    window[launchFunction]();
                }
            });

            function writeLaunchOption(functionName){
                // save to local storage
                localStorage.setItem("e4sLaunchOption", functionName);
            }
            function getLaunchOption(){
                let functionName = localStorage.getItem("e4sLaunchOption");
                if ( functionName === null ){
                    functionName = "";
                }
                return functionName;
            }
            function initEventGroups() {
                let egs = getEventGroups();
                for (let eg in egs) {
                    let eventGroup = egs[eg];
                    setCheckInDatesForEventGroup(eventGroup);
                }
            }

            function showPleaseWait(status, text, logoColour) {
                let obj = $(".loading-wrapper");
                if (status) {
                    if (!text) {
                        text = "Loading, please wait";
                    }
                    text += "...";
                    $("#e4sLoadingText").html(text);
                    if (logoColour) {
                        $("#E4S_Logo").removeClass("colour")
                            .removeClass("all-black")
                            .removeClass("all-white")
                            .addClass(logoColour)
                    }
                    obj.show();
                } else {
                    obj.hide();
                }
            }

            function removeCurrency(val) {
                val = val.split("<?php echo $config['currency'] ?>");
                return parseFloat(val[1]);
            }

            function createCookie(name, value, days) {
                let expires;

                if (days) {
                    let date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    expires = "; expires=" + date.toGMTString();
                } else {
                    expires = "";
                }
                if (value !== "") {
                    value = encodeURIComponent(value);
                }
                document.cookie = encodeURIComponent(name) + "=" + value + expires + "; path=/";
            }

            function readCookie(name, json) {
                let nameEQ = encodeURIComponent(name) + "=";
                let ca = document.cookie.split(';');
                let retval = "undefined";
                if (json) {
                    retval = {};
                }
                for (let i = 0; i < ca.length; i++) {
                    let c = ca[i];
                    while (c.charAt(0) === ' ')
                        c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) === 0) {
                        let v = decodeURIComponent(c.substring(nameEQ.length, c.length));
                        if (v === '""' || v === "") {
                            return retval;
                        }
                        if (json) {
                            v = JSON.parse(v);
                        }
                        return v;
                    }
                }
                return retval;
            }

            function eraseCookie(name) {
                createCookie(name, "", -1);
            }

            function e4s_onUnload() {
                e4sSocket.close();
            }

            // Global Vars
            let outputReportData = <?php echo e4s_getOptionsAsString($GLOBALS[E4S_OUTPUT_OBJ]) ?>;
            let validOptions = ["<?php echo E4S_FIELDCARD_CLEAR ?>:Clear:0", "<?php echo E4S_FIELDCARD_PASS ?>:Pass:0", "<?php echo E4S_FIELDCARD_RETIRE ?>:Retire:1", "<?php echo E4S_FIELDCARD_FAILURE ?>:Foul:0"];
            let e4sGlobal = {
                activeEntries: [],
                activeEntriesType: false, // Indiv
                isPrinting: false,
                maxLaneNo: 0,
                maxHeatNo: 0,
                maxHeatNoCheckedIn: 0,
                unSeededLaneNo: 0,
                unSeededLaneNoCheckedIn: 0
            };
            e4sGlobal.class = {};
            e4sGlobal.class.bibPresent = "e4sBibPresent";
            e4sGlobal.class.bibNotPresent = "e4sBibNotPresent";
            e4sGlobal.class.bibCheckedIn = "e4sBibCheckedIn";
            e4sGlobal.class.bibNotCheckedIn = "e4sBibNotCheckedIn";
            e4sGlobal.class.notActive = "notActiveTrial";
            e4sGlobal.class.notAvailable = "notAvailableTrial";
            e4sGlobal.class.notVisible = "e4sNotVisible";
            e4sGlobal.class.nextUp = "e4s_current_athlete";
            e4sGlobal.class.resultExtraText = "resultExtraText";

            let filterObj = {
                data: {},
                currentDate: "",
                currentType: "",
                init: function () {
                    this.data = this.getDateAndTypes();
                    let comp = getCompetition();
                    let currVal = readCookie("e4s_filter_date_" + comp.id);
                    if (currVal === "undefined") {
                        currVal = "";
                    }
                    this.currentDate = currVal;

                    currVal = readCookie("e4s_filter_type_" + comp.id);
                    if (currVal === "undefined") {
                        currVal = "";
                    }
                    this.currentType = currVal;
                    this.setActive();
                    this.setDates();
                    this.setTypes();
                    this.showClear();
                },
                showClear: function () {
                    if (this.currentDate === "" && this.currentType === "") {
                        $("#filterClear").hide();
                    } else {
                        $("#filterClear").show();
                    }
                },
                setActive: function () {
                    if (this.currentType === "" && this.currentDate === "") {
                        $("#Filter").removeClass("active");
                    } else {
                        $("#Filter").addClass("active");
                    }
                },
                filtersChanged: function () {
                    setEventChoices();
                    first_selector();
                    this.setActive();
                    this.showClear();
                },
                clear: function () {
                    // showPleaseWait(true,"Clearing Filters");
                    this.setCurrentType("");
                    this.setCurrentDate("");
                    this.setDates();
                    this.setTypes();
                    this.filtersChanged();
                },
                setTypes: function () {
                    this.populateTypes();
                },
                populateTypes: function () {
                    let selectField = $("#filterTypes");
                    selectField.empty();
                    let selected = "";
                    let option = "<option value=''>Select Event Type</option>";
                    selectField.append(option);
                    let obj = this.data.types;
                    if (this.currentDate !== "") {
                        obj = this.data.data[this.currentDate];
                    }
                    for (let t in obj) {
                        let eventType = "Track";
                        switch (t) {
                            case '<?php echo E4S_EVENT_FIELD ?>':
                                eventType = 'Field';
                                break;
                            case '<?php echo E4S_EVENT_ROAD ?>':
                                eventType = 'Road';
                                break;
                            case '<?php echo E4S_EVENT_XCOUNTRY ?>':
                                eventType = 'X-Country';
                                break;
                            case '<?php echo E4S_EVENT_MULTI ?>':
                                eventType = 'Multi-Event';
                                break;
                        }
                        selected = "";
                        if (t === this.currentType) {
                            selected = "selected";
                        }
                        option = "<option " + selected + " value='" + t + "'>" + eventType + " Only</option>";
                        selectField.append(option);
                    }
                },
                onChangeType: function () {
                    let selectField = $("#filterTypes");
                    let selectedVal = selectField.val();
                    this.setCurrentType(selectedVal);
                    this.filtersChanged();
                },
                getCurrentType: function () {
                    return this.currentType;
                },
                setCurrentType: function (type) {
                    let comp = getCompetition();
                    createCookie("e4s_filter_type_" + comp.id, type, 1);
                    this.currentType = type;
                },
                setDates: function () {
                    let obj = $("#dateFilter");
                    if (this.data.counts.dateCount > 1) {
                        this.populateDates();
                        obj.show();
                    } else {
                        this.setCurrentDate("");
                        obj.hide();
                    }
                },
                populateDates: function () {
                    let selectField = $("#filterDates");
                    selectField.empty();
                    let selected = "";
                    let option = "<option " + selected + " value=''>Select Event Date</option>";
                    let currentType = this.currentType;
                    selectField.append(option);
                    for (let d in this.data.data) {
                        selected = "";
                        if (d === this.currentDate) {
                            selected = "selected";
                        }
                        if (currentType === "" || this.data.data[d][currentType]) {
                            let uiDate = new Date(d);
                            uiDate = formatDate(uiDate, "D MMM yyyy");
                            option = "<option " + selected + " value='" + d + "'>" + uiDate + "</option>";
                            selectField.append(option);
                        }
                    }
                },
                onChangeDate: function () {
                    let selectField = $("#filterDates");
                    let selectedVal = selectField.val();
                    this.setCurrentDate(selectedVal);
                    this.filtersChanged();
                },
                getCurrentDate: function () {
                    return this.currentDate;
                },
                setCurrentDate: function (dt) {
                    let comp = getCompetition();
                    createCookie("e4s_filter_date_" + comp.id, dt, 1);
                    this.currentDate = dt;
                },
                getDateAndTypes: function () {
                    let eventGroups = getEventGroups();
                    let dateAndTypes = [];
                    let types = [];
                    let dateCount = 0;
                    let typeCount = 0;
                    for (let eg in eventGroups) {
                        let event = eventGroups[eg];
                        let type = event.type;
                        let date = event.date; // ISO Date
                        let dateOnly = date;
                        if (dateOnly.indexOf("T") > 0) {
                            dateOnly = date.split("T")[0];
                        } else if (dateOnly.indexOf(" ") > 0) {
                            dateOnly = date.split(" ")[0];
                        }

                        if (!dateAndTypes[dateOnly]) {
                            dateAndTypes[dateOnly] = [];
                            dateCount++;
                        }
                        dateAndTypes[dateOnly][type] = true;
                        if (!types[type]) {
                            types[type] = true;
                            typeCount++;
                        }
                    }

                    return {
                        counts: {
                            dateCount: dateCount,
                            typeCount: typeCount
                        },
                        data: dateAndTypes,
                        types: types
                    };
                }
            };
            <?php socketClass::outputJavascript() ?>
        </script>
    </head>
    <body>

    <div id="outputReportDiv">
        <?php displayHeader() ?>

        <div id="tabs" style="display:none;">
            <ul class="no-print" style="<?php if ($GLOBALS[E4S_SHOW_ONLY_CARDS]) {
                echo 'display:none;';
            } ?>">
                <?php if (showSummaryTab()) { ?>
                    <li><a href="#Summary-tab">Summary</a></li>
                <?php }
                if (showAthleteTab()) { ?>
                    <li><a href="#Athletes-tab"> Athletes</a></li>
                <?php }
                if (showIndivTab()) { ?>
                    <li><a href="#Individual-tab">Individual Entries</a></li>
                <?php }
                if (showTeamTab()) { ?>
                    <li><a href="#Team-tab">Team Entries</a></li>
                <?php } ?>
                <li><a href="#Card-tab">Track/Field Cards</a></li>
                <?php if (hasFinancialAccess()) { ?>
                    <li><a href="#Finance-tab">Financial</a></li>
                <?php } ?>
            </ul>
            <?php if (showSummaryTab()) { ?>
                <div id="Summary-tab">
                    <?php echo summaryTable() ?>
                </div>
            <?php }
            if (showIndivTab()) { ?>
                <div id="Individual-tab">
                    <div id="individual_grid" style="margin:auto;"></div>
                    <?php echo individualTable() ?>
                </div>
            <?php }
            if (showTeamTab()) { ?>
                <div id="Team-tab">
                    <div id="team_tree_grid" style="display:none; margin: auto;"></div>
                    <div id="team_grid" style="margin:auto;"></div>
                    <?php echo teamTable() ?>
                </div>
            <?php }
            if (showAthleteTab()) { ?>
                <div id="Athletes-tab">
                    <div id="athlete_grid" style="margin:auto;"></div>
                    <?php echo athletesTable() ?>
                </div>
            <?php } ?>
            <div id="Card-tab" style="<?php if ($GLOBALS[E4S_SHOW_ONLY_CARDS]) {
                echo 'padding:0;';
            } ?>">
                <!-- Start of new menu -->
                <div id="e4s-menu-bar" class="e4s-flex-row e4s-full-width e4s-flex-wrap e4s-menu-bar">
                    <!-- Container storing input and event filtering -->
                    <div class="e4s-flex-row e4s-flex-end">
                        <div class="e4s-flex-column e4s-full-width">
                            <div class="e4s-flex-row e4s-flex-center e4s-menu-bar--input-container">
                                <select id="event_selector"
                                        onchange="cardChange();"
                                        class="e4s-input-field e4s-input-field--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary">
                                </select>
                                <div class="e4s-flex-row e4s-flex-center event-selectors-container">
                                    <!-- First item selector -->
                                    <button id="FirstSelector"
                                            title="First Event"
                                            class="e4s-button e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>tertiary"
                                            onclick="first_selector();">
                                        <div class="e4s-flex-column e4s-flex-center e4s-justify-flex-center icon-container">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="12.002" height="12"
                                                 viewBox="0 0 12.002 12">
                                                <g transform="translate(17579.651 15020.5)">
                                                    <path
                                                            d="M12,16a1,1,0,0,1-.707-.293l-5-5a1,1,0,0,1,0-1.414l5-5a1,1,0,1,1,1.414,1.414L8.414,10l4.293,4.293A1,1,0,0,1,12,16Z"
                                                            transform="translate(-17585.652 -15024.5)"/>
                                                    <path
                                                            d="M12,16a1,1,0,0,1-.707-.293l-5-5a1,1,0,0,1,0-1.414l5-5a1,1,0,1,1,1.414,1.414L8.414,10l4.293,4.293A1,1,0,0,1,12,16Z"
                                                            transform="translate(-17580.652 -15024.5)"/>
                                                </g>
                                            </svg>
                                        </div>
                                    </button>

                                    <!-- Previous item selector -->
                                    <button id="PreviousSelector"
                                            title="Previous Event"
                                            class="e4s-button e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>tertiary"
                                            onclick="prev_selector();">
                                        <div class="e4s-flex-column e4s-flex-center e4s-justify-flex-center icon-container">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="7.002" height="12"
                                                 viewBox="0 0 7.002 12">
                                                <path
                                                        d="M12,16a1,1,0,0,1-.707-.293l-5-5a1,1,0,0,1,0-1.414l5-5a1,1,0,1,1,1.414,1.414L8.414,10l4.293,4.293A1,1,0,0,1,12,16Z"
                                                        transform="translate(-6 -4)"/>
                                            </svg>
                                        </div>
                                    </button>

                                    <!-- Next item selector -->
                                    <button id="NextSelector"
                                            title="Next Event"
                                            class="e4s-button e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>tertiary"
                                            onclick="next_selector();">
                                        <div class="e4s-flex-column e4s-flex-center e4s-justify-flex-center icon-container">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="7.002" height="12"
                                                 viewBox="0 0 7.002 12">
                                                <path
                                                        d="M8,16a1,1,0,0,1-.707-1.707L11.586,10,7.293,5.707A1,1,0,1,1,8.707,4.293l5,5a1,1,0,0,1,0,1.414l-5,5A1,1,0,0,1,8,16Z"
                                                        transform="translate(-6.998 -4)"/>
                                            </svg>
                                        </div>
                                    </button>

                                    <!-- Last item selector -->
                                    <button id="LastSelector"
                                            title="Last Event"
                                            class="e4s-button e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>tertiary"
                                            onclick="last_selector();">
                                        <div class="e4s-flex-column e4s-flex-center e4s-justify-flex-center icon-container">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="12.002" height="12"
                                                 viewBox="0 0 12.002 12">
                                                <g transform="translate(17447.503 15020.5)">
                                                    <path
                                                            d="M8,16a1,1,0,0,1-.707-1.707L11.586,10,7.293,5.707A1,1,0,1,1,8.707,4.293l5,5a1,1,0,0,1,0,1.414l-5,5A1,1,0,0,1,8,16Z"
                                                            transform="translate(-17454.5 -15024.5)"/>
                                                    <path
                                                            d="M8,16a1,1,0,0,1-.707-1.707L11.586,10,7.293,5.707A1,1,0,1,1,8.707,4.293l5,5a1,1,0,0,1,0,1.414l-5,5A1,1,0,0,1,8,16Z"
                                                            transform="translate(-17449.5 -15024.5)"/>
                                                </g>
                                            </svg>
                                        </div>
                                    </button>

                                    <!-- Show heat events button -->
                                    <span class="qualify_buttons">
                                        <button id="qualifySource"
                                                title="Qualify From Event"
                                                class="e4s-button e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>tertiary e4s-flex-center e4s-justify-flex-center hidden"
                                                onclick="jumpToFeeder();">
                                            <span class="e4s-body--100">QF</span>
                                        </button>

                                        <!-- Show semi final events button -->
                                        <button id="qualifyTarget"
                                                title="Qualify To Event"
                                                class="e4s-button e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>tertiary e4s-flex-center e4s-justify-flex-center hidden"
                                                onclick="jumpToFinal();">
                                            <span class="e4s-body--100">QT</span>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div id="pool_selectorDiv" style="display:none;">
                                Pool : <select onchange="cardChange();" class="pool_selector" id="pool_selector">
                                    <option value="1" checked>1</option>
                                </select>
                            </div>
                            <!-- Event check-in info. -->
                            <div id="cardEntryInfoDiv"
                                 class="e4s-flex-row e4s-flex-nowrap event-information-container">
                                <div class="icon-container">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                              d="M18,10a8,8,0,1,0-8,8A8,8,0,0,0,18,10ZM9,13a1,1,0,0,0,2,0V11a1,1,0,0,0-2,0ZM9,7a1,1,0,1,0,1-1A1,1,0,0,0,9,7Z"
                                              transform="translate(-2 -2)"/>
                                    </svg>
                                </div>
                                <span
                                        id="cardEntryInfoStatus"
                                        class="e4s-body--200">
                                </span>
                                <span
                                        id="cardEntryInfoTime"
                                        class="e4s-body--200">
                                </span>
                                <span
                                        id="cardEntryInfoSeedingIndicator"
                                        class="e4s-body--200">
                                </span>
                                <span
                                        id="markSeedingComplete"
                                        class="markSeedingComplete"
                                        style="display:none">
                                    <button class="e4s-button e4s-button--tertiary active"
                                            onclick="seedingComplete();">Seeding Complete</button>
                                </span>
                            </div>
                        </div>
                    </div>

                    <!-- Container storing global filtering and quick actions, e.g. refresh, settings, etc. -->
                    <div class="e4s-flex-row e4s-flex-center e4s-justify-flex-end">
                        <div class="e4s-flex-row e4s-flex-center e4s-menu-bar-quick-actions--container">
                            <button id="helpPage"
                                    title="Help"
                                    class="e4s-button e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>tertiary"
                                    onclick="help();return false;">
                                <div class="e4s-flex-column e4s-flex-center e4s-justify-flex-center icon-container">
                                    <span style="font-weight:bolder; color:var(--e4s-navigation-bar--primary__background);">?</span>
                                </div>
                            </button>
                            <!-- View type button -->
                            <button id="toggleViewMode"
                                    title="Type of View"
                                    class="e4s-button e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>tertiary"
                                    onclick="toggleViewMode()">
                                <div class="e4s-flex-column e4s-flex-center e4s-justify-flex-center icon-container">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                        <path fill-rule="evenodd"
                                              d="M1,2.5A1.5,1.5,0,0,1,2.5,1h15A1.5,1.5,0,0,1,19,2.5v15A1.5,1.5,0,0,1,17.5,19H2.5A1.5,1.5,0,0,1,1,17.5ZM8,5h8V7H8Zm8,4H8v2h8ZM8,13h8v2H8ZM5,7A1,1,0,1,0,4,6,1,1,0,0,0,5,7Zm1,3A1,1,0,1,1,5,9,1,1,0,0,1,6,10ZM5,15a1,1,0,1,0-1-1A1,1,0,0,0,5,15Z"
                                              transform="translate(-1 -1)"/>
                                    </svg>
                                </div>
                            </button>
                            <!-- Entries/checked-in button -->
                            <button id="entries_confirmed"
                                    class="e4s-button e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>tertiary"
                                    title="Checked-in Entries Toggle"
                                    onclick="toggleCheckedIn();">
                                <div class="e4s-flex-column e4s-flex-center e4s-justify-flex-center icon-container">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20.002" height="20.002"
                                         viewBox="0 0 20.002 20.002">
                                        <g transform="translate(0 0.001)">
                                            <path
                                                    d="M7.519,2.395a8,8,0,0,1,5.12.053A1,1,0,1,0,13.3.56a10,10,0,1,0,6.568,7.812,1,1,0,0,0-1.974.326A8,8,0,1,1,7.52,2.395Z"/>
                                            <path
                                                    d="M17.847,4.74a1.125,1.125,0,1,0-1.694-1.48L9.945,10.354,7.795,8.2A1.125,1.125,0,0,0,6.2,9.8l3,3a1.125,1.125,0,0,0,1.642-.054l7-8Z"/>
                                        </g>
                                    </svg>
                                </div>
                            </button>
                            <!-- Filter button -->
                            <button id="Filter"
                                    title="Filters"
                                    class="e4s-button e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>tertiary"
                                    onclick="ClosePopup(event);ShowPopup(event, 'FilterPopup');"
                                    data-button-type="TriggerPopup">
                                <div class="e4s-flex-column e4s-flex-center e4s-justify-flex-center icon-container">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                        <g transform="translate(-2 -2)">
                                            <path
                                                    d="M2,5A1,1,0,0,1,3,4H5A1,1,0,0,1,5,6H3A1,1,0,0,1,2,5ZM8,5A1,1,0,0,1,9,4h8a1,1,0,0,1,0,2H9A1,1,0,0,1,8,5ZM8,15a1,1,0,0,1,1-1h8a1,1,0,0,1,0,2H9A1,1,0,0,1,8,15ZM2,15a1,1,0,0,1,1-1H5a1,1,0,0,1,0,2H3A1,1,0,0,1,2,15Zm12-5a1,1,0,0,1,1-1h2a1,1,0,0,1,0,2H15A1,1,0,0,1,14,10ZM2,10A1,1,0,0,1,3,9h8a1,1,0,0,1,0,2H3A1,1,0,0,1,2,10Z"/>
                                            <path fill-rule="evenodd"
                                                  d="M7,8A3,3,0,1,0,4,5,3,3,0,0,0,7,8ZM7,6A1,1,0,1,0,6,5,1,1,0,0,0,7,6Zm6,7a3,3,0,1,0-3-3A3,3,0,0,0,13,13Zm0-2a1,1,0,1,0-1-1A1,1,0,0,0,13,11Z"/>
                                            <path fill-rule="evenodd"
                                                  d="M13,13a3,3,0,1,0-3-3A3,3,0,0,0,13,13Zm0-2a1,1,0,1,0-1-1A1,1,0,0,0,13,11ZM7,18a3,3,0,1,0-3-3,3,3,0,0,0,3,3Zm0-2a1,1,0,1,0-1-1A1,1,0,0,0,7,16Z"/>
                                        </g>
                                    </svg>
                                </div>
                            </button>

                            <!-- Refresh -->
                            <button id="Refresh"
                                    title="Refresh"
                                    class="e4s-button e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>tertiary"
                                    onclick="refreshCurrentCard(); return false;">
                                <div class="e4s-flex-column e4s-flex-center e4s-justify-flex-center icon-container">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                        <path
                                                d="M17,11a1,1,0,0,1,1,1,3,3,0,0,1-3,3H5.414l1.293,1.293a1,1,0,0,1-1.414,1.414l-3-3a1,1,0,0,1,0-1.414l3-3a1,1,0,1,1,1.414,1.414L5.414,13H15a1,1,0,0,0,1-1A1,1,0,0,1,17,11ZM3,9A1,1,0,0,1,2,8,3,3,0,0,1,5,5h9.586L13.293,3.707a1,1,0,0,1,1.414-1.414l3,3a1,1,0,0,1,0,1.414l-3,3a1,1,0,1,1-1.414-1.414L14.586,7H5A1,1,0,0,0,4,8,1,1,0,0,1,3,9Z"
                                                transform="translate(-2 -2)"/>
                                    </svg>
                                </div>
                            </button>

                            <!-- Settings -->
                            <button id="Settings"
                                    title="Options"
                                    class="e4s-button e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>tertiary"
                                    onclick="ClosePopup(event);ShowPopup(event, 'cardActionMenu');"
                                    data-button-type="TriggerPopup">
                                <div class="e4s-flex-column e4s-flex-center e4s-justify-flex-center icon-container">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                        <path
                                                d="M10,13a3,3,0,1,1,3-3A3,3,0,0,1,10,13Zm7.476-1.246a1.994,1.994,0,0,1,0-3.508,1,1,0,0,0,.376-1.4L16.785,5.109a1,1,0,0,0-1.327-.355L15.011,5a2,2,0,0,1-2.945-1.755V3a1,1,0,0,0-1-1H8.934a1,1,0,0,0-1,1v.242A2,2,0,0,1,4.989,5l-.447-.243a1,1,0,0,0-1.327.355L2.148,6.842a1,1,0,0,0,.376,1.4,1.994,1.994,0,0,1,0,3.508,1,1,0,0,0-.376,1.4l1.067,1.733a1,1,0,0,0,1.327.355L4.989,15a2,2,0,0,1,2.945,1.755V17a1,1,0,0,0,1,1h2.132a1,1,0,0,0,1-1v-.242A2,2,0,0,1,15.011,15l.447.243a1,1,0,0,0,1.327-.355l1.067-1.733a1,1,0,0,0-.376-1.4Z"
                                                transform="translate(-2 -2)"/>
                                    </svg>
                                </div>
                            </button>
                        </div>
                    </div>
                </div>
                <!-- End of new menu -->
                <div id="eventGroupNotes" class="dat-info--info" style="padding: 10 5 10 5; display: none"></div>
                <div id="seedingNotes" class="dat-info--warn" style="padding: 10 5 10 5; display : none"></div>
                <div id="heightDialog" class="heightDialog" style="display:none;"></div>
                <div id="progressionDialog" class="progressionDialog" style="display:none;"></div>
                <div id="card-base" class="card_base">
                    Please select an event.
                </div>
                <div id="cardActionMenu"
                     class="e4s-flex-column e4s-menu-popup--container hidden">

                    <div class="e4s-flex-row e4s-flex-nowrap e4s-flex-center">
                        <h5 class="e4s-header--500">Options</h5>
                    </div>
                    <hr class="e4s-hr" style="border: 2px solid ;">
                    <div class="e4s-flex-column ">
                        <div class="e4s-flex-column e4s-input--container">
                            <a href="#"
                               onclick="ClosePopup(event);goToResults(); return false;"
                               class="e4s-settings--label e4s-body--100">Competition Results
                            </a>
                        </div>
                        <hr class="e4s-hr">
                        <div class="e4s-flex-column e4s-input--container">
                            <a href="#"
                               onclick="ClosePopup(event);switchDisplayModes(); return false;"
                               class="e4s-settings--label e4s-body--100">Display Mode
                            </a>
                        </div>
                        <hr class="e4s-hr">
                        <div class="e4s-flex-column e4s-input--container">
                            <a href="#"
                               onclick="ClosePopup(event);e4s_print_dialog(); return false;"
                               class="e4s-settings--label e4s-body--100">Print Card(s)
                            </a>
                        </div>
                    </div>
		            <?php if (secureAccessToCard()) {
			            e4s_secureMenuOptions();
		            } ?>
                </div>
            </div>
            <div id="cardhelp" style="display: none">
                <iframe src="/resources/cardhelp.pdf" style="width:100%; height:100%;">
                </iframe>

            </div>
            <?php if (hasFinancialAccess()) { ?>
                <div id="Finance-tab">
                    <div id="finance_prices" style="margin:auto;">
                        <?php echo generatePricesTable($compId) ?>
                    </div>
                    <div id="finance_grid" style="margin:auto;"></div>
                    <?php echo generateFinanceSheet() ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <div id="general_grid_parent" style="visibility: hidden">
        <div id="general_grid" style="margin:auto;"></div>
    </div>
    <div id="addAthleteParentDiv" style="display:none;">
        <span class="forceHelp">
            Enter here the Bib number of the athlete you are adding to this event.
        </span>
        <div id="addAthleteDiv"></div>
    </div>
    <div id="seedingsDiv" style="display:none;">
        <span class="forceHelp">
        </span>
        <div id="seedings"></div>
    </div>
    <div id="dialogStarterOptions" style="display:none;"></div>
    <div id="dialogEGOptions" style="display:none;"></div>
    <div id="dialogSeedingMonitor" style="display:none;"></div>
    <div id="dialogAutoShow" style="display:none;"></div>
    <div id="dialogTrialResults" style="display:none;"></div>
    <div id="e4sPromptHolder" style="display:none;"></div>
    <!-- Settings popup -->

    <!-- Filter popup -->
    <div id="FilterPopup" class="e4s-flex-column e4s-menu-popup--container hidden">
        <div class="e4s-flex-row e4s-flex-nowrap e4s-flex-center e4s-justify-flex-space-between">
            <h5 class="e4s-header--500">Filters</h5>
            <a id="filterClear" href="#" class="e4s-hyperlink--100 e4s-hyperlink--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary hidden"
               onclick="filterObj.clear();">Clear</a>
        </div>
        <div class="e4s-flex-column e4s-menu-popup--items-container">
            <!-- Event type filter -->
            <div id="typeFilter">
                <summary class="e4s-body--100">Event Type</summary>
                <div class="e4s-flex-column e4s-input--container">
                    <select id="filterTypes"
                            onchange="filterObj.onChangeType();"
                            class="e4s-input-field e4s-input-field--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary">
                    </select>
                </div>
                <hr class="e4s-hr">
            </div>
            <!-- Date filter -->
            <div id="dateFilter" class="hidden">
                <summary class="e4s-body--100">Date</summary>
                <div class="e4s-flex-column e4s-input--container">
                    <select id="filterDates"
                            onchange="filterObj.onChangeDate();"
                            class="e4s-input-field e4s-input-field--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary">
                    </select>
                </div>
                <hr class="e4s-hr">
            </div>

        </div>
    </div>
    <div id="dialogHeightResult" style="display:none;" class="dialogDistanceResult" title="Distance Results">
        <table>
            <tr>
                <td>Height 1 :</td>
                <td>
                    <input class="HeightEntry" tabindex=1 id="h1">
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <input onchange="heightAttempt(1,1);" type="checkbox" id="ht1_1" name="ht1" value="1">
                                <label for="ht1_1">A 1</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input onchange="heightAttempt(1,2);" type="checkbox" id="ht1_2" name="ht1" value="2">
                                <label for="ht1_2">A 2</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input onchange="heightAttempt(1,3);" type="checkbox" id="ht1_3" name="ht1" value="3">
                                <label for="ht1_3">A 3</label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <button class="ui-button ui-corner-all ui-widget resultClear"
                            onclick="clearResult('h1', false); return false;">Clear
                    </button>
                </td>
            </tr>
        </table>
    </div>
    <div id="dialogDistanceResult" style="display:none;" class="dialogDistanceResult" title="Distance Results">
        <table>
            <tr>
                <td>Trial 1 :</td>
                <td>
                    <input type="number" class="distanceEntry" e4sAthleteResult=true e4strial=1 tabindex=1 id="t1">
                    <select e4sOptions=true class="resultClear" name="t1Options" id="t1Options">
                    </select>
                </td>
            </tr>
            <tr>
                <td>Trial 2 :</td>
                <td>
                    <input class="distanceEntry" e4sAthleteResult=true e4strial=2 tabindex=2 id="t2">
                    <select e4sOptions=true class="resultClear" name="t2Options" id="t2Options">
                    </select>
                </td>
            </tr>
            <tr>
                <td>Trial 3 :</td>
                <td>
                    <input class="distanceEntry" e4sAthleteResult=true e4strial=3 tabindex=3 id="t3">
                    <select e4sOptions=true class="resultClear" name="t3Options" id="t3Options">
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr>
                </td>
            </tr>

            <tr>
                <td>Trial 4 :</td>
                <td>
                    <input class="distanceEntry" e4sAthleteResult=true e4strial=4 tabindex=4 id="t4">
                    <select e4sOptions=true class="resultClear" name="t4Options" id="t4Options">
                    </select>
                </td>
            </tr>
            <tr>
                <td>Trial 5 :</td>
                <td>
                    <input class="distanceEntry" e4sAthleteResult=true e4strial=5 tabindex=5 id="t5">
                    <select e4sOptions=true class="resultClear" name="t5Options" id="t5Options">
                    </select>
                </td>
            </tr>
            <tr>
                <td>Trial 6 :</td>
                <td>
                    <input class="distanceEntry" e4sAthleteResult=true e4strial=6 lastathlete=true tabindex=6 id="t6">
                    <select e4sOptions=true class="resultClear" name="t6Options" id="t6Options">
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr>
                </td>
            </tr>
        </table>
    </div>
    <div id="entryOptionsMenu" class="e4s-flex-column e4s-menu-popup--container hidden">
        <div class="e4s-flex-row e4s-flex-nowrap e4s-flex-center e4s-justify-flex-space-between">
            <h5 class="e4s-header--500">Entry Options</h5>
        </div>
        <div class="e4s-flex-column e4s-menu-popup--items-container">
            <div class="e4s-flex-column e4s-input--container">
                <a href="#"
                   onclick="ClosePopup(event);goToResults(); return false;"
                   class="e4s-settings--label e4s-body--100"><span class="ui-icon ui-icon-person"></span>Mark Present
                </a>
            </div>
            <hr class="e4s-hr">

            <div class="e4s-flex-column e4s-input--container">
                <a href="#"
                   onclick="ClosePopup(event);goToResults(); return false;"
                   class="e4s-settings--label e4s-body--100"><span class="ui-icon ui-icon-person"></span>Mark Absent
                </a>
            </div>
            <hr class="e4s-hr">

            <div class="e4s-flex-column e4s-input--container">
                <a href="#"
                   onclick="ClosePopup(event);goToResults(); return false;"
                   class="e4s-settings--label e4s-body--100"><span class="ui-icon ui-icon-person"></span>Move Lane/Heat
                </a>
            </div>
            <hr class="e4s-hr">

            <div class="e4s-flex-column e4s-input--container" id="compNotes">
                <a href="#"
                   onclick="ClosePopup(event);goToResults(); return false;"
                   class="e4s-settings--label e4s-body--100"><span class="ui-icon ui-icon-person"></span>Add Competition Notes
                </a>
            </div>
        </div>
    </div>
    <?php
    echo generateTrackCardHTML(E4S_DEFAULT_LANE_COUNT);
    echo generateTrackCardOverHTML();
    echo generateDistanceCardHTML($school);
    echo generateHeightCardHTML($school);
    ?>
    <div id="tree_grid" style="display:; margin: auto;"></div>
    <div class="loading-wrapper" style="display:none;">
        <div class="logo-container">
            <svg id="E4S_Logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 180 80" class="all-white">
                <g id="LogoGroup">
                    <g id="EntryGroup">
                        <path id="E"
                              d="M36.52,37.4h-14.69c-2.27,0-4.25-.52-5.95-1.55-1.84-1.12-2.88-2.9-3.14-5.34-.25-2.44,.19-5.13,1.31-8.06,1.12-2.9,2.7-5.53,4.76-7.88,2.25-2.56,4.72-4.42,7.41-5.6,1.48-.63,2.83-1.08,4.07-1.34,1.23-.26,2.58-.39,4.05-.39h13.62l-2.06,5.34h-12.58c-1.12,0-2.12,.15-3,.45-.88,.3-1.84,.84-2.88,1.62-1.05,.78-1.85,1.53-2.42,2.26-.57,.73-1.06,1.64-1.48,2.74h19.82l-2.06,5.34H21.47c-.38,.98-.58,1.83-.6,2.56-.02,.73,.16,1.49,.54,2.26,.38,.78,.86,1.36,1.44,1.77,.59,.4,1.38,.6,2.39,.6h13.28l-2.01,5.21Z"/>
                        <path id="n"
                              d="M59.7,37.4h-5.99l5.66-14.69c.14-.37,.19-.71,.13-1.01-.06-.3-.19-.6-.39-.88-.39-.49-.95-.73-1.7-.73h-5.86l-6.67,17.32h-5.99l8.59-22.32h11.46c2.04,0,3.47,.12,4.3,.35,1.3,.34,2.16,1.11,2.57,2.28,.45,1.38,.35,2.89-.28,4.52l-5.84,15.17Z"/>
                        <path id="t"
                              d="M84.16,19.74h-5.82l-3.9,10.12c-.35,.92-.33,1.68,.07,2.28,.49,.6,1.32,.9,2.5,.9h1.77l-1.68,4.35h-5.56c-1.67,0-2.86-.6-3.57-1.81-.41-.72-.55-1.51-.42-2.37,.13-.86,.42-1.88,.88-3.06l4.01-10.43h-3.79l1.79-4.65h3.79l3.02-7.84h5.9l-3.02,7.84h5.82l-1.79,4.65Z"/>
                        <path id="r"
                              d="M99.36,19.91h-6.68l-6.74,17.49h-5.95l8.59-22.32h5.95l-1.19,3.1c.61-.78,1.25-1.41,1.9-1.9,.65-.49,1.4-.86,2.26-1.12,.39-.12,.79-.22,1.22-.3,.42-.09,.83-.13,1.24-.13,.49,0,.93,.06,1.32,.17l-1.92,5Z"/>
                        <path id="y"
                              d="M124.95,15.09l-9,23.27c-2.74,7.08-6.37,6.98-11.82,6.98H40.48l2.12-5.15h59.81c.98-.04,6.55,.45,7.44-1.87l.33-.86h-6.89c-1.32,0-2.38-.15-3.19-.45-.8-.3-1.47-.88-2-1.74-.95-1.49-1-3.35-.14-5.56l5.62-14.61h6.29l-5.55,14.3c-.22,.57-.35,1.01-.39,1.29-.04,.29,.04,.6,.24,.95,.2,.34,.49,.58,.87,.71,.39,.13,.89,.19,1.53,.19h5.5l6.72-17.45h6.16Z"/>
                    </g>
                    <g id="_4Group">
                        <g id="Calendar">
                            <path fill-rule="evenodd"
                                  d="M153.92,2.45h11.13l-.92,2.38h-11.13l.92-2.38Zm-13.88,0h8.24l-.92,2.38h-5.86l-14.77,38.36h34.12l14.77-38.36h-5.86l.92-2.38h8.24l-16.6,43.12h-38.88L140.04,2.45Z"/>
                            <path
                                    d="M154,29.04l5.16-13.4-18.09,13.4h12.93Zm9.17,5.17h-4.27l-2.02,5.26h-6.89l2.02-5.26h-20.68l2.37-6.16,25.76-18.74h9.03l-7.53,19.56h4.27l-2.06,5.34Z"/>
                        </g>
                        <path id="Holder" fill-rule="evenodd"
                              d="M168.8,0h0c.86,0,1.3,.71,.97,1.57l-1.59,4.13c-.33,.87-1.31,1.57-2.18,1.57h0c-.86,0-1.3-.71-.97-1.57l1.59-4.13c.33-.87,1.31-1.57,2.18-1.57m-16.76,0h0c.87,0,1.3,.71,.97,1.57l-1.59,4.13c-.33,.87-1.31,1.57-2.18,1.57h0c-.86,0-1.3-.71-.97-1.57l1.59-4.13c.33-.87,1.31-1.57,2.18-1.57Z"/>
                    </g>
                    <g id="SportsGroup">
                        <path id="S"
                              d="M34.25,60.99c-1.02,2.64-2.75,4.87-5.2,6.68-2.45,1.81-5.02,2.71-7.72,2.71H0l2.09-5.43H19.67c1.18,0,2.27-.27,3.29-.83,1.16-.63,1.96-1.5,2.38-2.6,.99-2.57-.32-3.86-3.94-3.86H12.52c-2.56,0-4.49-.79-5.81-2.37-1.32-1.58-1.5-3.59-.56-6.03,1.04-2.7,2.82-4.89,5.35-6.57,2.53-1.68,5.33-2.52,8.4-2.52h20.38l-1.99,5.17H20.32c-.8,0-1.75,.34-2.82,1.01-1.08,.67-1.79,1.47-2.15,2.39-.4,1.03-.34,1.9,.18,2.61,.52,.7,1.34,1.06,2.46,1.06h9.78c3.04,0,5.13,.78,6.27,2.35,1.13,1.57,1.2,3.64,.21,6.23h0Z"/>
                        <path id="p"
                              d="M54.92,59.01c.72-1.87,.71-3.4-.02-4.59-.73-1.19-1.99-1.79-3.77-1.79h-4.22l-5.01,13.01h4.65c1.72,0,3.36-.61,4.91-1.83,1.55-1.22,2.7-2.82,3.47-4.8m6.25-.35c-1.37,3.56-3.39,6.4-6.06,8.51-2.67,2.11-5.51,3.17-8.52,3.17h-6.51l-3.14,8.14h-6.03l11.73-30.46h11.98c3.39,0,5.64,1.01,6.77,3.02,1.07,1.92,.99,4.47-.22,7.63Z"/>
                        <path id="o"
                              d="M82.73,59.05c.7-1.81,.71-3.32,.05-4.55-.66-1.22-1.97-1.83-3.93-1.83-1.78,0-3.49,.62-5.14,1.87-1.64,1.25-2.83,2.82-3.56,4.72-.84,2.18-.92,3.92-.24,5.21,.68,1.29,1.95,1.94,3.82,1.94,2.13,0,3.96-.67,5.52-2.02,1.42-1.24,2.58-3.02,3.48-5.34m6.66,.04c-1.49,3.88-3.87,6.85-7.14,8.92-3,1.9-6.45,2.84-10.36,2.84s-6.61-.86-8.1-2.58c-1.74-1.98-1.8-5.07-.18-9.26,1.35-3.5,3.76-6.26,7.24-8.27,3.11-1.81,6.43-2.71,9.96-2.71,3.88,0,6.61,.97,8.21,2.91,1.59,1.94,1.72,4.66,.37,8.16Z"/>
                        <path id="r-2"
                              d="M107.87,52.85h-6.68l-6.74,17.49h-5.95l8.59-22.32h5.95l-1.19,3.1c.61-.78,1.25-1.41,1.9-1.9,.65-.49,1.4-.86,2.26-1.12,.39-.12,.79-.22,1.22-.3,.42-.09,.83-.13,1.24-.13,.49,0,.93,.06,1.32,.17l-1.92,5Z"/>
                        <path id="t-2"
                              d="M124.35,52.68h-5.82l-3.9,10.12c-.35,.92-.33,1.68,.07,2.28,.49,.6,1.32,.9,2.5,.9h1.77l-1.68,4.35h-5.56c-1.67,0-2.86-.6-3.57-1.81-.41-.72-.55-1.51-.42-2.37,.13-.86,.42-1.88,.88-3.06l4.01-10.43h-3.79l1.79-4.65h3.79l3.02-7.84h5.9l-3.02,7.84h5.82l-1.79,4.65Z"/>
                        <path id="s"
                              d="M144.09,63.53c-.73,1.9-1.97,3.52-3.71,4.87-1.74,1.35-3.54,2.02-5.41,2.02h-14.78l1.56-4.05h12.15c.86,0,1.67-.22,2.42-.65,.75-.43,1.29-1.05,1.6-1.85,.72-1.87-.19-2.8-2.71-2.8h-6.2c-1.32,0-2.36-.63-3.11-1.9-.75-1.26-.84-2.63-.28-4.09,.8-2.07,1.9-3.73,3.3-5,1.62-1.44,3.46-2.15,5.53-2.15h14.22l-1.63,4.22h-12.67c-.49,0-1,.24-1.54,.71-.54,.47-.93,1.01-1.16,1.62-.25,.66-.24,1.23,.06,1.7,.29,.47,.77,.71,1.43,.71h6.68c2.1,0,3.52,.62,4.25,1.87,.74,1.25,.74,2.84,0,4.76"/>

                    </g>
                </g>
            </svg>
        </div>

        <p id="e4sLoadingText"></p>

    </div>
    </body>
    </html>
    <?php
}

//PHP Functions

function showTeamTab(): bool {
    if (!hasSecureAccess()) {
        return FALSE;
    }
    $teams = $GLOBALS[E4S_OUTPUT_OBJ]->teams;
    if (empty($teams)) {
        return FALSE;
    }
    return TRUE;
}

function showIndivTab(): bool {
    if (!hasSecureAccess()) {
        return FALSE;
    }
    $obj = $GLOBALS[E4S_OUTPUT_OBJ]->entries;
    if (empty($obj)) {
        return FALSE;
    }
    return TRUE;
}

function showSummaryTab(): bool {
    if (!hasSecureAccess()) {
        return FALSE;
    }
    $obj = $GLOBALS[E4S_OUTPUT_OBJ]->athletes;
    if (empty($obj)) {
        return FALSE;
    }
    return TRUE;
}

function showAthleteTab(): bool {
    if (!hasSecureAccess()) {
        return FALSE;
    }
    $obj = $GLOBALS[E4S_OUTPUT_OBJ]->athletes;
    if (empty($obj)) {
        return FALSE;
    }
    return TRUE;
}

function isCheckInEnabled() {
    $obj = $GLOBALS[E4S_OUTPUT_OBJ]->competition;
    $options = e4s_addDefaultCompOptions($obj->options);

    return isCheckinEnabledFromOptions($options);
}

function isSeedOnCheckInEnabled() {
    $obj = $GLOBALS[E4S_OUTPUT_OBJ]->competition;
    $options = e4s_addDefaultCompOptions($obj->options);

    return !isSeedOnEntriesEnabledFromOptions($options);
}

function isCheckinEnabledFromOptions($options) {
    if ($options->checkIn->enabled) {
        return TRUE;
    }
    return FALSE;
}

function isSeedOnEntriesEnabledFromOptions($options) {
    if (isset($options->checkIn->seedOnEntries)) {
        return $options->checkIn->seedOnEntries;
    }
    return FALSE;
}

function hasSecureAccess($checkCardOnly = TRUE) {
    if (!isUserLoggedIn() or ($GLOBALS[E4S_SHOW_ONLY_CARDS] and $checkCardOnly)) {
        return false;
    }

    return userHasPermission(PERM_ADMIN, null, $GLOBALS[E4S_OUTPUT_COMP]);
}

function hasFinancialAccess() {
    if (!isUserLoggedIn() or $GLOBALS[E4S_SHOW_ONLY_CARDS]) {
        return FALSE;
    }
    return userHasPermission(PERM_FINANCE, null, $GLOBALS[E4S_OUTPUT_COMP]);
}

function hasSeedingAccess() {
    if (!isUserLoggedIn()) {
        return FALSE;
    }

    return userHasPermission(PERM_SEEDING, null, $GLOBALS[E4S_OUTPUT_COMP]);
}

function hasResultsAccess() {
    if (!isUserLoggedIn()) {
        return FALSE;
    }
    return userHasPermission(PERM_RESULTS, null, $GLOBALS[E4S_OUTPUT_COMP]);
}

function isProductionEnv(){
	$config = e4s_getConfig();
	return strtoupper($config['env']) === E4S_ENV_PROD;
}
function e4s_getSqlForRefunds($compid) {
    return 'SELECT r.created created,
                    r.orderid orderid, 
                    r.value refundvalue,
                    r.reason reason,
                    r.options options
            FROM ' . E4S_TABLE_REFUNDS . ' r
            WHERE r.compid = ' . $compid;
}

function outputCompRefundValues($sql, $config) {
    if (!isE4SUser()) {
        return;
    }

    $entryResult = e4s_queryNoLog($sql);
    $rows = $entryResult->fetch_all(MYSQLI_ASSOC);

    foreach ($rows as $row) {
        $refundOptions = e4s_getOptionsAsObj($row['options']);
        $refundAmount = (float)$row['refundvalue'];
        $lineAmount = (float)$refundOptions->lines[0]->linevalue;
        if ($refundAmount < $lineAmount) {
            continue;
        }
        echo '<tr>';
        echo '<td>';
        $fieldValue = $row['orderid'];
        outputDataWithLabel('', $fieldValue);
        echo '</td>';
        echo '<td>';

        $fieldValue = $row['refundvalue'];
        outputDataWithLabel('', convertToCurrency($config, -$fieldValue));
        echo '</td>';
        echo '<td>';

        $fieldValue = ' - ';
        outputDataWithLabel('', $fieldValue);
        echo '</td>';
        echo '<td>';

        $fieldValue = $row['created'];
        outputDataWithLabel('', $fieldValue);
        echo '</td>';
        echo '<td>';

        $fieldValue = $row['reason'];
        $fieldValue .= ' ' . $refundOptions->order->name;

        outputDataWithLabel('', $fieldValue);
        echo '</td>';
        echo '</tr>';
    }
}

function generateFinanceSheet() {
    $compid = $GLOBALS[E4S_OUTPUT_COMP];
    $config = e4s_getConfig();
    $entryCountObj = e4s_getEntryCountObj($compid);
    $retObj = new stdClass();
    $retObj->totalCnt = 0;
    $retObj->totalAmount = 0;
    $retObj->totalAmountActual = 0;
    $retObj->totalFee = 0;
    $retObj->orderOnly = TRUE;
    $retObj->summary = array();
    echo '<table id="finance_table" style="display:none;">';
    echo '<tr>';
    echo '<th>Order</th>';
    echo '<th>Price</th>';
    echo '<th>Fee</th>';
    echo '<th>Entry Date</th>';
    echo '<th>Information</th>';
    echo '</tr>';
    $sql = e4s_getSqlForIndivOrders($compid);
    outputCompValues($sql, $config, TRUE, $retObj, $entryCountObj);
    $sql = e4s_getSqlForTeamOrders($compid);
    if (!is_null($sql)) {
        outputCompValues($sql, $config, TRUE, $retObj, $entryCountObj);
    }
    $sql = e4s_getSqlForRefunds($compid);
    if (!is_null($sql)) {
        outputCompRefundValues($sql, $config);
    }
    echo '</table>';
}

function e4s_formatDateForReport($date) {
    $useDate = explode('-', $date);
    return $useDate[2] . '/' . $useDate[1] . '/' . $useDate[0];
}

function e4s_formatTimeForReport($time) {
    $useTime = explode(':', $time);
    if ((int)$useTime[0] === 0 and (int)$useTime[1] === 0) {
        return E4S_NO_TIME;
    }
    return $useTime[0] . ':' . $useTime[1];
}

function e4s_formatDateAndTimeForReport($date, $dateOrTime = '') {
    $useDateAndTime = explode(' ', $date);
    $useDate = e4s_formatDateForReport($useDateAndTime[0]);
    if ($dateOrTime === E4S_DATE_ONLY) {
        return $useDate;
    }
    $useTime = '00:00';
    if (sizeof($useDateAndTime) > 1) {
        $useTime = e4s_formatTimeForReport($useDateAndTime[1]);
    }

    if ($dateOrTime === E4S_TIME_ONLY) {
        return $useTime;
    }
    return $useDate . ' ' . $useTime;
}

function generatePricesTable($compid) {
    $priceRows = e4s_getCompPrices($compid);
    $config = e4s_getConfig();
    $currency = $config['currency'];
    $html = '<table>';
    $html .= '<tr>';
    $html .= '<th>Description</th>';
    $html .= '<th>Standard Price</th>';
    $html .= '<th>E4S Fee</th>';
    $html .= '<th>Late Price From</th>';
    $html .= '<th>Late Price</th>';
    $html .= '<th>E4S Fee</th>';
    $html .= '</tr>';
    foreach ($priceRows as $priceRow) {
        $html .= '<tr>';
        $desc = $priceRow['description'];
        if ($desc === '') {
            $desc = 'Standard Price';
        }
        $html .= '<td>' . $desc . '</td>';
        $html .= '<td>' . $currency . $priceRow['saleprice'] . '</td>';
        $html .= '<td>' . $currency . $priceRow['salefee'] . '</td>';
        if (is_null($priceRow['saleenddate'])) {
            $html .= '<td>&nbsp;</td>';
            $html .= '<td>&nbsp;</td>';
            $html .= '<td>&nbsp;</td>';
        } else {
            $html .= '<td>' . e4s_formatDateAndTimeForReport($priceRow['saleenddate'], E4S_DATE_ONLY) . '</td>';
            $html .= '<td>' . $currency . $priceRow['price'] . '</td>';
            $html .= '<td>' . $currency . $priceRow['fee'] . '</td>';
        }
        $html .= '</tr>';
    }
    $html .= '</table>';
    return $html;
}

function getCss() {
    $css = '
        <style type = "text/css">
        .e4s_current_athlete {
            background-color: var(--e4s-button--tertiary__active-border-color) !important;
        }
       .heightResultTrial{
            padding-left: 1.5rem;   
       }
       .heightResultHeight {
            font-size: 0.75em;
       }
       .heightResultAlt {
            background-color: var(--e4s-button--tertiary__background);
       }
        .autoQualifyRulesField {
            width: 50px;
        }
        .maxInHeat {
            padding-top: 15px;
        }
        .doubleUp {
            background-color: var(--e4s-navigation-bar--primary__background);
            color: var(--e4s-navigation-link--primary__text-color);
        }
        .refreshRequired {
            cursor: pointer;
            box-shadow: 0 0 0 rgba(22,22,22, 0.6);
            animation: pulse 2s infinite;
            background-color: yellow ;
        }
        .pulse {
          cursor: pointer;
          box-shadow: 0 0 0 rgba(22,22,22, 0.6);
          animation: pulse 2s infinite;
        }
        @-webkit-keyframes pulse {
          0% {
            -webkit-box-shadow: 0 0 0 0 rgba(22,22,22, 0.6);
          }
          70% {
              -webkit-box-shadow: 0 0 0 10px rgba(22,22,22, 0);
          }
          100% {
              -webkit-box-shadow: 0 0 0 0 rgba(22,22,22, 0);
          }
        }
        @keyframes pulse {
          0% {
            -moz-box-shadow: 0 0 0 0 rgba(22,22,22, 0.6);
            box-shadow: 0 0 0 0 rgba(22,22,22, 0.6);
          }
          70% {
              -moz-box-shadow: 0 0 0 10px rgba(22,22,22, 0);
              box-shadow: 0 0 0 10px rgba(22,22,22, 0);
          }
          100% {
              -moz-box-shadow: 0 0 0 0 rgba(22,22,22, 0);
              box-shadow: 0 0 0 0 rgba(22,22,22, 0);
          }
        }
        .athleteClassificationIcon {
            width: 0.7vw;
        }
        .athleteClassification {
            font-size: 0.7vw;
            font-weight: bold;
            color: var(--e4s-input-field__text-color);
        }
        .getPFClass {
            padding-top: 10px;
        }
        .print_warn_checkin {
            color: red;
            font-weight: bold;
        }
        .resultTBC {
            color: white !important;
            background-color: red !important;
        }
        .card_header_times {
            display: flex;
            justify-content: space-between;
        }
        .table_width_100_perc{
            width: 100%;
            border-spacing: 0;
        }
        .seedingNotes {
            display: inline-block;
            color: black;
            background-color: orange;
            width: 100%;
            line-height: 25px; 
            text-indent: 5px;
        }
        .eventGroupNotes {
            display: inline-block;
            color: white;
            background-color: darkred;
            width: 100%;
            line-height: 25px; 
            text-indent: 5px;
        }
        .exportOptions {
            display: flex;
            gap: 15px;
            justify-content: space-between;
            max-width: 560px;
            flex-direction: row;
            flex-wrap: wrap;
            padding-top: 15px;
        }
        .exportDateLabel {
            width: 60;
        }
        .exportStatusLabel {
            width: 60;
            display: inline-block;

        }          
        .exportFromLabel {
            width: 60;
            display: inline-block;
        }        
        .exportToLabel {
            width: 60;
            display: inline-block;
        }
        .e4sHidden {
            display: none;
        }
        .' . E4S_CARD_FIELD_PREFIX . 'entry_table_row_age {
            font-size: 0.85vw;
        }
        .' . E4S_CARD_FIELD_PREFIX . 'entry_table_row_bib {
            font-size: 1vw;
        }
        .trackCardHeatNameLabel {
            flex-grow: 2;
        }   
        .trackCardHeatName {
            font-size: large;
        }       
        .trackCardHeatNoLabel {
            flex-grow: 3;
        }    
        .trackCardHeatNo {
            font-size: x-large;
        }
        .contextMenuHeader {
            color: var(--e4s-input-field--' . $GLOBALS[E4S_ENVIRONMENT] . 'primary__caret-color) !important;
            font-weight: 700;
        }
       
        .overResults {
            text-align: center;
            width: 80px;
        }
        input[type="radio"] {
            margin-right: 5;
        }
        #e4sPrintPageBreakDiv label {
            margin-left: 5;
        }
        #e4sPrintPageBreakDiv #cardPageBreak {
            margin-left: 40px;
        }
        #e4sPrintTrackFormatDiv label {
            margin-right: 5;
        }  
        #e4sPrintPageBreakLabel {
            margin-left: 40px;
        }
        
        .e4sPrintSelectionDiv {
            margin-top: 15px;
        }        
        .e4sPrintTrackPageBreakDiv {
            margin-top: 15px;
        }
        .e4sButtonSelectAll {
            margin-right: 180px;
            margin-left: 20px;
        }  
        #e4sPrintFilters label {
            margin-right: 30;
        }
        .e4s_print_eventSelection {
            min-width: 250px;
        }
        .e4sPrintTrackFormatDiv {
            margin-top: 15px;
        }
        .e4sPrintDialogDiv {
            overflow:hidden !important;
        }
        .e4sPrintEventsDiv {
            flex-wrap: wrap;
            display: inline-flex;
            flex-direction: row;
            align-content: flex-start;
            justify-content: space-between;
            gap: 8px;
            width: 555px;
            margin-top: 20px;
            height: 350px;
            overflow-y: scroll;
            border: solid darkgray;
        }
        .qualify_buttons {
            padding-left: 20px;
        }
        .selector_dates {
            font-size: 1rem;
            padding-left: 10px;
        }
        .confirm_title_style {
            font-weight: 800;
            line-height: 35px;
        }
        .confirm_desc_style {
            font-size: medium;
            margin-left: 15px;
        }
        .editPb {
            width: 60px;
        }
        
        .e4sRedText {
            color: red;
        }
        .dropdown {
          float: left;
          overflow: hidden;
        }

        .dropdown .dropbtn {
          cursor: pointer;
          font-size: 16px;  
          border: none;
          outline: none;
          color: white;
          padding: 14px 16px;
          background-color: inherit;
          font-family: inherit;
          margin: 0;
        }
        
        .navbar a:hover, .dropdown:hover .dropbtn, .dropbtn:focus {
          background-color: red;
        }
        
        .dropdown-content {
            display: none;
            position: absolute;
            color: #2e6e9e;
            background-color: #dfeffc;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
            right: 12px;
        }
        
        .dropdown-content a {
          float: none;
          color: black;
          padding: 12px 16px;
          text-decoration: none;
          display: block;
          text-align: left;
        }
        
        .dropdown-content a:hover {
          background-color: #ddd;
        }
    
        .fieldBibNo {
            padding: 0 5 0 5;
            font-weight: 700;
            font-size: 1.5vh;
        }
        .darkodd {
            background-color: #444 !important;
        }
        .e4sCardDefault::before {
         content: " ";
        }
        .bibStatus2 {
            line-height: 4px;
        }
        div ul {
            margin: 0px;
            padding-left: 0px;
        }
        @media screen {

            .card_base {
                
            }
            .card_base_sticky {
                width: 84%;
                max-width: 84%;
                display: inline-block;
            }
            .cardActionMenu_sticky {               
                display: inline-block !important;
                position: static !important;
                vertical-align: top;
                width: 15% !important;
                max-width: 15% !important;
                min-width: 15% !important;
            }
            .card_base_light {
                color: black;
                background-color: white;
            } 
            .dark {
                color: white !important;
                background-color: black !important;
                border-color: white !important;
//                border-bottom-color: white !important;
            }
                       
            .heatConfirmed {
                background: lightskyblue;
            }

           
            .e4sCardDefault {
                color: black;
//                background-color: white;
            }
            
            .statusTD {
                float: right;
                padding-right: 5px;
                padding-left: 5px;
            }
            .e4sBibNotPresent::before {
                content: url("/resources/unchecked.gif");
            }
            .e4sBibPresent::before {
                content: url("/resources/checked.gif");
            }
            .e4sBibCheckedIn::before {
                content: "_";
            }
            .e4sBibCheckedIn {
//                color: white;
//                height: 5px;
//                font-size: 0.75vw;
//                text-align: center;
            }

            .e4sBibNotCheckedIn::before {
               content: url("/resources/xchecked.gif");
            }
            .e4sBibNotCheckedIn {
                    float: right;
            }
        }
//        For Printing
        @media print {
            .card_base {
                color: black;
                background-color: white;
            }
            .card_base_light {
            } 
            .card_base_dark {
            }
            .heatConfirmed {
                background: white;
            }
            
            .e4sCardDefault {
                background-color: white;
                color: black;
            }
            .e4sBibNotPresent::before {
                content: " ";
            }
            .e4sBibNotPresent {
                background-color: white;
                color: black;
                height: 5px;
                font-size: x-small;
                text-align: center;
            }
            
            .e4sBibPresent::before {
                content: " ";
            }
            .e4sBibPresent {
                background-color: white;
                color: black;
                height: 5px;
                font-size: x-small;
                text-align: center;
            }
            .e4sBibCheckedIn::before {
                content: " ";
            }
            .e4sBibCheckedIn {
                background-color: white;
                color: black;
                height: 5px;
                font-size: x-small;
                text-align: center;
            }
            .e4sBibNotCheckedIn::before {
                content: " ";
            }
            .e4sBibNotCheckedIn {
                background-color: white;
                color: black;
                height: 5px;
                font-size: x-small;
                text-align: center;
            }
        }
        // End of Prining
        .e4sCardNotes {
        }
        .e4sCardNotes::before {
            content:"\2691 ";
            color: #c3c30f;
            font-size: 0.8vw;
        }

        .ui-icon-redIcon { 
            background-image: url(http://download.jqueryui.com/themeroller/images/ui-icons_ff0000_256x240.png) !important; 
        }
        .selector_navigation {
            font-size: 0.35em;
            position: relative;
            left: 55px;
        }
        .seedPresentIcon {
           position:relative !important;
        }
        .seedAthleteName {
            position:relative !important;
            margin-left:5px !important;
            font-size:0.8em;
        }
        .seedBibValue {
            position: relative !important;
            margin-left: 0px !important;
            font-weight: 700;
            float: left;
        }
        .seedLaneLabel {
            position: relative !important;
            margin-left: 0px !important;
        }
        .seedLaneValue {
            position: relative !important;
            margin-left: 5px !important;
            font-weight: 700;
        }
        .seedHeatLabel {
            position: relative !important;
            margin-left: 0px !important;
        }
        .seedHeatValue {
            position: relative !important;
            margin-left: 5px !important;
            font-weight: 700;
        }
        .track_CheckedIn {
            background-color: black;
            color: white
        }
        .' . E4S_CLASS_VOIDLANE . ' {
            background-color: linen;
        }
        .' . E4S_CLASS_SEEDED . ' {
            color: black
        }
        .' . E4S_CLASS_TO_BE_SEEDED . ' {
            background-color: red;
            color: white;
            padding-right: 5px;
            padding-left: 5px;
            height: 0.75vh;
        }
        .track_seed_input_heat {
            position: relative !important;
//            margin-left: 12px !important;
         margin-right: 30px !important;
         float: right;
        }
        .track_seed_input_lane {
            position: relative !important;
//            margin-left: 12px !important;
            float: right;
        }
        .track_seed_input {
            width:50px;
        }
        .trackChangePresent {
            float: right;
        }
        .trackChangeAthleteLabel {
            font-weight: 800;
            font-size: small;
        }
        .trackChangeSwitchHelp{
            font-size: smaller;
            font-style: italic;
        }
        .trackChangeSwitchLabel{
            display:block;
            padding: 20 0 20 0;
            text-align: center;
        }
        .trackChangeData {
            font-size: larger;
            font-weight: 800;
        }
        .trackChangeCurrentLabel{
            font-weight: 800;
            font-size: small;
        }
        .trackChangeAthleteName {
            padding-left: 5;
            font-weight: 800;
            font-size: medium;
        }
        .trackChangeSwitchedAthleteName {
            display: block;
            padding-top: 5;
            font-weight: 800;
            font-size: medium;
            min-height: 20px;
        }
        .loader {
            position: relative;
            width: 80px;
            margin: 100px auto;
        }
        .e4s_header_label {
            width:10%;
            font-size: 20px;
        
        }
        .e4s_header_data {
            width:40%;
            font-size: 20px;
            font-weight: 700;
        }
        .e4s_summary_table {
            text-align: center;
        }
        .e4s_summary_gender {
            font-size: 16px;
        } 
        .e4s_summary_label {
            background-color: #dfeffc;
            color: #2e6e9e;
        }
        .e4s_summary_table {
            border-collapse: collapse;
        }
        .e4s_summary_table td {
            border: 1px solid black;
        }
        .e4s_total_data {
            font-weight: bold;
            font-style: oblique; 
            background-color: lightcyan;
        }
        .e4s_total_label {
            font-weight: bold;
            background-color: #dfeffc;
            color: #2e6e9e;
        }
        .e4s_grand_totals {
            border-top-style: double !important;
            border-top-width: 4px !important;
        }
        .pool_selector {
            width: 45px;
        }
        .event_selector {
            padding-bottom: 10px;
        }
        .event_selector {
            float: left;
        }
        .forceHelp {
            font-size: 1vw;
            display: none;
        }
        .entries_page {
            float: left;
        }
        .pageBreak {
            page-break-after: always;
        }
        .height_row {
            font-size: 1vw;
            padding: 0 10 0 10;
        }
        .distance_row {
            font-size: 1vh;
        }
        .height_data_row {
            background-color: white;
            line-height: 2.2vh;
            font-size: 0.8em;
        }
        .distance_data_row {
            background-color: white;
            line-height: 2.4vh;
            font-size: 1.5vh;
        }
        .card_menu_bar {
            font-size:0.8em;
            height: 50px;
        }        
        
        .card_menu_right {
            float: right;
            font-size:0.8em;
        }
        .card_menu_refresh { 
            float: right;        
        }
        .height_column_header_label {
            font-size: 10px;
            text-align: center;
            background-color: lightgrey;
            border: 1px solid;
        }
        .card_BibNo {
            width: 100%;
            text-align: center;
        }
        .card_data {
            text-align: center;
        } 
        .card_data_left {
            text-align: left;
            padding-left: 5px;
        }
        .eventInProgress {
            line-height: 0.75vw;
            font-size: 0.75vw;
            color: red;
        }
        .fieldResultsLeftHeader {
            width:100%;
            border-spacing: 0;
            border-collapse: collapse;
        }
        .fieldResultsLeftRow {
            border-bottom: 1px solid black;
        }
        .heightCardContentMsg {
            font-size: 0.5vw;
        }
        .height_card_col_6 {
        }
        .height_card_col_8 {
        }
        .height_card_col_10 {
        }
        .height_card_col_12 {
        }
        .height_card_col_14 {
        }
        .height_card_col_16 {
        }
        .height_card_col_18 {
        }
        .height_card_col_20 {
        }
        .height_data_best  {
        }
        .height_card_col_22 {
            width:40px;
        }
        .height_data_postition {
        }
        .height_card_col_23 {
            width:40px;
        }
        .height_data_height {
            width: 50px;
        } 
        .height_data_t_height {
            width: 40px;
        } 
        .height_tickbox {
            width:10px;
        }
        .height_data_trial {
            height:10px;
            font-size: 8px;
        }
        .centre {
            text-align: center;
        }
        .height_header {
            background-color: lightgrey;
        }
        .height_trial{
            width:1vw;
        }
        .height_best {
            width: 3.5vw;
            font-size: 1em;
            font-weight: 500;
            border-left: 3px solid !important;
        }
        .height_totals {
            width: 1vw;
        }
        .height_start {
            width: 3vw;
        }
        .height_club{
            font-size: 1vw;
            font-weight: 500;
            padding-left: 5;
        }
        .height_athlete{
            font-size: 1vw;
            font-weight: 600;
            padding-left: 5;
            width: 15vw;
        }
        .height_bib {
            width: 3vw;
            font-size: 1vw;
            font-weight: 700;
            text-align: center;
        }
        .card_bib {
            width: 3vw;
            font-size: 1vw;
            font-weight: 700;
            text-align: center;
        }
        .card_age {
            width: 2vw;
            font-size: 0.8vw;
            font-weight: 400;
        }
        .height_age {
            width: 2vw;
            font-size: 0.65vw;
            font-weight: 400;
        }
        .card_gender {
            width: 2vw;
        }
        .card_name {
            width: 20vw;
        }
        .card_club {
            font-size: 0.9vw;
        }
        .trialHeader {
            padding-left: 0px !important;
            text-align: center;
        }
        .field_data {
            font-size: 0.8vw;
            font-weight: 400;
        }
        .field_data_large {
            font-size: 1vw;
            font-weight: 500;
        }
        .height_card_cell {
            border-left: 1px solid;
            border-bottom: 1px solid;
            border-right: 0px solid;
            border-top: 0px solid;
            border-color: black;
        }
        .height_column_header_label {
            font-size: 10px;
            text-align: center;
            background-color: lightgrey;
            border: 1px solid;
        }
        .height_result {
            text-align: center;
        }
        .height_result_1 {
            border-left: 2px solid;
        }
        .height_result_2 {

        }
        .height_result_3 {

        }
        .height_title {
            border-bottom: 2px solid;
            font-size: 0.8em;
            font-weight: 500;
        }
        .trialResultsTd{
            
        }
        .trailAltRow {
            background-color: lightyellow;
        }
        .trialFieldCol{
            border-bottom: 1px solid black;
            min-width: 205px;
        }
        .trialAthleteCol {
            border-bottom: 1px solid black;
            font-size: 0.8rem;
            width: 195px;
        }
        .trialBibCol{
            border-bottom: 1px solid black;
            font-weight: 600;
            width: 40px;
            font-size: 1em;
        }
         .overLabelHead {
            background: lightgrey;
            font-weight: 700;
        }
        .overLabel {
            border-bottom: 2px;
            border-bottom-color: black;
            border-bottom-style: double;
        }
        .overPBLabel{
        }
        .overResultsLabel {
            text-align: center;
//            width: 80px;
        }        
        .overTable {
            border-spacing: 0;
            width:  100%;
        }
        .overOddRow {
            background-color: #eee;
        }
        .overHeat {
            font-size: large;
            font-weight: 800;
            background-color: var(--e4s-navigation-bar--' . $GLOBALS[E4S_ENVIRONMENT] . 'primary__background);
            color: var(--e4s-navigation-link--' . $GLOBALS[E4S_ENVIRONMENT] . 'primary__text-color);
        }
        .overPos{
            flex-grow: 1;
            flex-basis: 30px;
        }
        .overPosLabel{
            flex-grow: 1;
            flex-basis: 30px;
        }
        .overName{
            flex-grow: 3;
            flex-basis: 250px;
        }
        .overNameLabel{
            flex-grow: 3;
            flex-basis: 250px;
        }
        .overTeamName{
            flex-grow: 3;
            flex-basis: 300px;
        }
        .overGenderLabel{
            flex-grow: 1;
            flex-basis: 80px;
        }
        .overGender{
            flex-grow: 1;
            flex-basis: 80px;
        }
        .overBibLabel{
            flex-grow: 1;
            flex-basis: 20px;
            padding-right: 10px;
        }
        .overBib{
            flex-grow: 1;
            flex-basis: 20px;
            padding-right: 10px;
        }
        .overClubLabel {
            flex-grow: 4;
            flex-basis: 350px;
        }
        .overClub{
            flex-grow: 4;
            flex-basis: 350px;
        }
        .overAgeLabel {
            flex-grow: 2;
            flex-basis: 180px;
        }
        .overAge{
            flex-grow: 2;
            flex-basis: 180px;
        }
        .overPB{
            flex-grow: 1;
            flex-basis: 50px;
        }
        
        .distance_column_header_label{
            border: 1px solid;
            border-bottom: 2px solid;
            font-size: 0.8vw;
            padding-left: 5;
            font-weight: 500;
            vertical-align: bottom;
            word-spacing: 1px;
        }
        .distance_card_col_1 {
            width: 2vw;
        }  
        .distance_card_col_2 {
            width: 3vw;
        }
        .distance_card_col_4 {
            width: 25vw;
        }
        .distance_card_col_6 {
            width: 4vw;
        }
        .distance_card_col_7 {
            width: 4vw;
        }
        .distance_card_col_8 {
            width: 4vw;
        }
        .distance_card_col_9 {
            width: 4vw;
        }
        .distance_card_col_10 {
            width: 3vw;
        }
        .distance_card_col_11 {
            width: 3vw;
        } 
        .distance_card_col_12 {
            width:4vw;
        } 
        .distance_card_col_13 {
            width:4vw;
        } 
        .distance_card_col_14 {
            width:4vw;
        }
        .distance_card_col_15 {
            width:4vw;
        }
        .distance_card_col_16 {
            width: 3vw;
        } 

        .distance_data_best {
            font-weight: bolder !important;
            border-left: solid 2px black;
            border-bottom: solid 1px black;
            text-align: center;
            width: 2vw;
            font-size: 1vw;
            font-weight: normal;
        }
               
        .resultExtraText {
            margin-right: 0.25vw;
            font-size: 0.5vw;
        }

        .distance_data_3jo {
            color: var(--e4s-navigation-bar--primary__background);
            width: 2vw;
            max-width: 2vw;
            border-right: solid 2px black;
        }
        .distance_data_3pos {
            width: 2vw;
            max-width: 2vw;
            border-left: solid 2px black;
            border-right: solid 2px black;
        }
        .distance_data_col{
            border-left: 1px solid;
            border-bottom: 1px solid;
            border-color: black;
        }
        .distance_data_result {
            border-bottom: 1px solid !important;
            border-left: 1px solid !important;
            text-align: center;
            width: 2vw;
            font-size: 1vw;
            font-weight: normal;
        }
        
        .distance_data_pos {
            font-size: 1vw;
            text-align: center;
            font-weight: bolder;
            border-left: solid 2px black;
            border-right: solid 2px black;
        }
        .distanceEntry {
            width: 70px;
        }
        .floatLeft {
            float: left;
        }
        .floatRight {
            float: right;
        }
        .clearSB {
            position: absolute;
            left: 20px;
        }
        .eventSettingBTN {
            position: absolute;
            left: 120px;
        }
        .resultButton {
            font-size: 0.6em !important;
        }
        .resultShow {
            font-size: 0.6em !important;
            padding: 7px;
        }
        .resultClear {
            font-size: 0.6em !important;
            width: 150px;
        }
        .field_info_row {
            border:1px solid;
        }
        .border_top_strong {
            border-top: 2px solid !important;
        }
        .border_right_strong {
            border-right: 2px solid !important;
        }
        .border_left_strong {
            border-left: 2px solid !important;
        }
        .border_bottom_strong {
            border-bottom: 2px solid !important;
        }
        .card_field_judge_row{
            height: 25px;
        }
        
        .card_judges_label {
            text-align: center;
            vertical-align: middle;
            font-size: 0.6vw;
            border: 1px solid;
            width: 30px;
        }       
        .card_judges_count_label {
            vertical-align: top;
            text-align: left;
            font-size: 0.6vw;
            border: 1px solid;
        }   
        .card_judges_area {
            border: 1px solid;
            height: 30px;
        }
        .height_data_col {
            border:1px solid;
        }
        .field_info_row{
            height: 60px;
            vertical-align: top;
            text-align: left;
            border: 2px solid black;
            font-style: italic;
            font-size: small;
            padding: 2px;
            background-image: url(/resources/entry4sports-small.jpg);
            background-size: 80px 40px;
            background-position: bottom left;
            background-repeat: no-repeat;
        }

        .card_table_header {
            background-color: lightgrey;
            border:1px solid;
        }
        
        .track_lane_header{
        
        }
        .track_data_grid {
            border: 1px solid black;
        }
        .track_lane_header_label {
            text-align: center;
            border: 1px solid;
        }
        .track_heatNo_value {
            font-weight:bolder;
            text-align: center;
        }
        .track_heatNo_time {
            font-size:0.7em;
            margin-top:0.7em;
        }
        .track_heatNo_label {
        }
        .track_heat_labels {
        }
        .track_heat_divider {
            border-bottom: 5px double black;
        }
        .track_bibno_label {
            font-size: small;
        } 
        .track_bibno_data {
            width: 40px;
            text-align: center;
            font-weight: 600;
        }
        .track_age_label {
        }
        .track_age_data {
            width: 60px;
            font-size: .8vw;
            text-align: center;
        }
        .track_athlete_label {
            font-size: small;
        }
        .track_athlete_data_text{
            max-width: 120px;
        }
        .track_athlete_data {
            font-size: 0.8vw;
            font-family: inherit;
            font-weight: 600;
            text-align: center;
            background-color: white;
            border-bottom: 0px;
        }
        .athletepb {
            font-size:0.5vw;
        }
        .athleteName {
            font-size: 0.9vw;
            font-weight: 600;
        }
        .track_club_label {
            font-size: small;
        }            
        .track_club_data {
            text-align: center;
            font-size: .90vw;
            border-top: 0px;
        }
        .track_info_row {
            height: 80px;
            vertical-align: top;
            text-align: left;
            border: 2px solid black;
            font-style: italic;
            font-size: small;
            padding: 2px;
            background-image: url(/resources/entry4sports-small.jpg);
            background-size: 80px 40px;
            background-position: bottom left;
            background-repeat: no-repeat;
        }
        .card_link_text {
            text-align: center;
            font-weight: 500;
            font-size: 0.6rem;
        }
        
        .card_link_field_text_span {
            position: relative;
            top: 40px;
            font-size: smaller;
            float: right;
            background-color: white;
        }        
        .card_link_text_span {
            position: relative;
            top: -15px;
            left: -5px;
            font-size: smaller;
            float: right;
            background-color: white;
        }
         /* track card flex*/
            ul li::marker {
                content: "" !important;
            }
            .e4s_qualify_entry {
                background-color: gold;
            }
            .e4sItem {
                padding-left: 2.5em;
            }

            .header_time {
                padding-right: 2.5vw;
                font-size: 0.8vw;
            }

            .' . E4S_CARD_FIELD_PREFIX. 'border {
                border: 1px black solid;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'border_thick {
                border-bottom: 2px black double;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'flex_col {
                display:flex;
                flex-direction: column;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'center {
                text-align: center;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'flex_row {
                display:flex;
                flex-direction: row;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'header {
                flex-grow: 1;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'header_lane {
                display: grid;
                justify-items: center;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'header_row {
                flex-grow: 1;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'header_info {
                flex-grow: 1;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'header_race {
                flex-grow: 1;
                flex-basis: 32px;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'race {
                display:flex;
                flex-grow: 1;
                flex-basis: 32px;
                flex-direction: column;
                justify-content: center;
                align-items: center;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'raceNo {
                font-size: 1.5vw;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'time {
                font-size: 0.8vw;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'header_lane {
                flex-grow: 2;
                flex-basis: 5vw;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'lane {
                display:flex;
                flex-grow: 2;
                flex-basis: 5vw;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'entry_table {
                width: 100%;
                text-align: center;
                border-spacing: 0;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'entry_table_row_age {
                border-bottom: 1px solid black;
                border-left: 1px solid black;
                height: 22px;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'entry_table_row_bib{
                border-bottom: 1px solid black;
                border-right: 1px solid black;
                height: 22px;
                width: 50%;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'athlete {
                font-size: 0.6vw;
                font-weight: 600;
            }
            .' . E4S_CARD_FIELD_PREFIX. 'club {
                font-size: 0.7vw;
                padding-bottom: 5px;
            }
            /* track card flex*/
            .e4sBibNo {
                font-weight: 800;
                font-size: 0.75vw;
                padding-left: 5px;
            }
            .dragDropMain {
                display: flex;
                line-height: 2;
            }

            .' . E4S_CARD_FIELD_PREFIX. 'blank {

            }
            .' . E4S_CARD_FIELD_PREFIX. 'init {
                background-color: var(--e4s-button--' . $GLOBALS[E4S_ENVIRONMENT] . 'primary__disabled-background) !important;
            }
            .addAthleteWarning {
                color: var(--e4s-input-field--error__text-color) !important;
            }
            .ui-sortable-placeholder {
                visibility: visible !important;
                background-color: var(--e4s-button--' . $GLOBALS[E4S_ENVIRONMENT] . 'primary__hover-background) !important;
            }
            .dragDropItem {
                background-color: var(--e4s-button--' . $GLOBALS[E4S_ENVIRONMENT] . 'tertiary__background);
            }
            .dragDropItemDisabled{
                background-color: red !important;
            }
            .dragDropPlaceholder {
                background-color: var(--e4s-navigation-bar--' . $GLOBALS[E4S_ENVIRONMENT] . 'primary__background);
            }
            .ui-state-disabled {
                opacity: 100 !important;
            }
            .ui-button-disabled {
                background-color: var(--e4s-button--' . $GLOBALS[E4S_ENVIRONMENT] . 'primary__disabled-background) !important;
            }
            .e4s_card_button {
                color: black !important;
                padding: 0px 5px 0px 5px;
            }
            #cardHeaderDiv {
                position: sticky;
                z-index: 1;
            }
            #card_table_header_with_lanes {
                position: sticky;
                z-index: 1;
            }
            .autoScroll {
                -webkit-animation-name: autoScroll;
                -webkit-animation-duration: 30s;
                -webkit-animation-iteration-count: infinite;
                -webkit-animation-direction: up;
                -webkit-animation-timing-function:linear;
                margin: 0px 10px 0px 10px;
            }

            @-webkit-keyframes autoScroll {
                0% {
                    transform:translateY(0);
                }
                100% {
                    transform:translateY(-50%);
                }
            }
            select.e4s-input-field {
                padding: 4px 8px;
            }

            .loading-wrapper {
                position: fixed;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                background: rgba(0, 0, 0, .8);
                z-index: 1000;
                overflow: hidden;
            }

            .loading-wrapper p {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 16px;
                color: #ffffff;
            }

            .logo-container {
                width: 200px;
                height: 100px;
            }

            .logo-container svg {
                animation-duration: 3s;
                animation-delay: 1.2s;
                animation-timing-function: ease-in-out;
                animation-iteration-count: infinite;
                animation-direction: normal;
                animation-name: pulsing;
            }

            #E4S_Logo.all-white g,
            #E4S_Logo.all-white path {
                fill: #ffffff;
            }

            #E4S_Logo.all-black g,
            #E4S_Logo.all-black path {
                fill: #000000;
            }

            #E4S_Logo.colour #EntryGroup {
                fill: #1f427d;
            }

            #E4S_Logo.colour #_4Group #Calendar {
                fill: #f7b008;
            }

            #E4S_Logo.colour #_4Group #Holder {
                fill: #1f427d !important;
            }

            #E4S_Logo.colour #SportsGroup {
                fill: #ba0000;
            }

            .logo-container svg path {
                stroke: white;
                stroke-width: 1;
                fill: transparent;
            }

            .logo-container svg path:nth-child(1) {
                /* E */
                /* 181 */
                stroke-dasharray: 400;
                stroke-dashoffset: 400;
                animation: line-anim 1s ease forwards;
            }

            .logo-container svg path:nth-child(2) {
                /* n */
                /* 120 */
                stroke-dasharray: 120;
                stroke-dashoffset: 120;
                animation: line-anim 1s ease forwards 0.1s;
            }

            .logo-container svg path:nth-child(3) {
                /* t */
                /* 101 */
                stroke-dasharray: 101;
                stroke-dashoffset: 101;
                animation: line-anim 1s ease forwards 0.2s;
            }

            .logo-container svg path:nth-child(4) {
                /* r */
                /* 79 */
                stroke-dasharray: 79;
                stroke-dashoffset: 79;
                animation: line-anim 1s ease forwards 0.3s;
            }

            .logo-container svg path:nth-child(5) {
                /* y */
                /* 269 */
                stroke-dasharray: 269;
                stroke-dashoffset: 269;
                animation: line-anim 1s ease forwards 0.4s;
            }

            .logo-container svg path:nth-child(6) {
                /* calendar border */
                /* 308 */
                stroke-dasharray: 308;
                stroke-dashoffset: 308;
                animation: line-anim 1s ease forwards 0.5s;
            }

            .logo-container svg path:nth-child(7) {
                /* 4 */
                /* 171 */
                stroke-dasharray: 171;
                stroke-dashoffset: 171;
                animation: line-anim 1s ease forwards 0.6s;
            }

            .logo-container svg path:nth-child(8) {
                /* holder */
                /* 38 */
                stroke-dasharray: 38;
                stroke-dashoffset: 38;
                animation: line-anim 1s ease forwards 0.7s;
            }

            .logo-container svg path:nth-child(9) {
                /* S */
                /* 194 */
                stroke-dasharray: 194;
                stroke-dashoffset: 194;
                animation: line-anim 1s ease forwards 0.8s;
            }

            .logo-container svg path:nth-child(10) {
                /* p */
                /* 145 */
                stroke-dasharray: 145;
                stroke-dashoffset: 145;
                animation: line-anim 1s ease forwards 0.9s;
            }

            .logo-container svg path:nth-child(11) {
                /* o */
                /* 123 */
                stroke-dasharray: 123;
                stroke-dashoffset: 123;
                animation: line-anim 1s ease forwards 0.10s;
            }

            .logo-container svg path:nth-child(12) {
                /* r */
                /* 79 */
                stroke-dasharray: 79;
                stroke-dashoffset: 79;
                animation: line-anim 1s ease forwards 0.11s;

            }

            .logo-container svg path:nth-child(13) {
                /* t */
                /* 101 */
                stroke-dasharray: 101;
                stroke-dashoffset: 101;
                animation: line-anim 1s ease forwards 0.12s;
            }

            .logo-container svg path:nth-child(14) {
                /* s */
                /* 135 */
                stroke-dasharray: 135;
                stroke-dashoffset: 135;
                animation: line-anim 1s ease forwards 0.13s;
            }
        </style>
    ';

    $split = explode("\r\n", $css);
    foreach ($split as $line) {
        echo $line;
    }
}

function init() {
//    make events an associate array
    $events = $GLOBALS[E4S_OUTPUT_OBJ]->events;
    $eventsNameArr = array();
    $eventsIdArr = array();
    foreach ($events as $event) {
        $eventsNameArr[trim($event->gender . '-' . $event->name)] = $event;
        $eventsIdArr[$event->id] = $event;
    }

    $GLOBALS[E4S_OUTPUT_OBJ]->eventsByName = $eventsNameArr;
    $GLOBALS[E4S_OUTPUT_OBJ]->eventsById = $eventsIdArr;
}

function displayHeader() {
    init();
    $obj = $GLOBALS[E4S_OUTPUT_OBJ]->competition;
    if ($GLOBALS[E4S_SHOW_ONLY_CARDS]) {
        return;
    }
    echo '
    <table>
        <tr>
            <td class="e4s_header_label">Competition</td><td class="e4s_header_data"> ' . $obj->id . ': ' . $obj->name . '</td>
            <td class="e4s_header_label">Date</td><td class="e4s_header_data">' . e4s_formatDateForReport($obj->date) . '</td>
        </tr>
        <tr>
            <td class="e4s_header_label">Organiser</td><td class="e4s_header_data"> ' . $obj->club . '</td>
            <td class="e4s_header_label">Location</td><td class="e4s_header_data">' . $obj->location . '</td>
        </tr>
    </table>
    ';
}

function getBibNo($bibno) {
    if ($bibno === 0) {
        return E4S_NO_BIBNO;
    }
    return substr('000' . $bibno, -4);
}

function e4s_showAgColumn($eventGroup):bool {

    if (!hasSecureAccess()){
        return false;
    }

    if (!$eventGroup->isTeamEvent) {
        return true;
    }
    $compId = $GLOBALS[E4S_OUTPUT_COMP];
    $compObj = e4s_GetCompObj($compId);
    $ceObjs = $compObj->getCeObjs();
    foreach ($ceObjs as $ceObj) {
        if ($ceObj->egId === $eventGroup->id) {
            if ( isset($ceObj->options) ){
                if ( isset($ceObj->options->eventTeam) ){
                    if ( isset($ceObj->options->eventTeam) ){
                        if (isset($ceObj->options->eventTeam->disableTeamNameEdit)){
                            if ($ceObj->options->eventTeam->disableTeamNameEdit) {
                                return false;
                            }
                        }
                    }
                }
            }
        }
    }

    return true;
}
function e4s_showPbColumn($eventGroup):bool{
    if ($eventGroup->type === E4S_EVENT_XCOUNTRY) {
        return false;
    }
    return true;
}
function e4s_showTeamNameColumn($eventGroup):bool{
    return $eventGroup->isTeamEvent;
}
function e4s_showClassificationColumn($athletes):bool{
    if ( !array_key_exists('showClassification',$GLOBALS)) {
        $GLOBALS['showClassification'] = FALSE;
        foreach ($athletes as $athlete) {
            if ((int)$athlete->classification !== 0) {
                $GLOBALS['showClassification'] = TRUE;
                break;
            }
        }
    }
    return $GLOBALS['showClassification'];
}
function getEntryCheckedInStatus($entry){
    $status = false;
    if ( isset($entry->checkedIn) ) {
        $status = $entry->checkedIn;
    }
	if ($status){
		return 'Yes';
	}
	return 'No';
}
function getBibCollectedStatus($bibNo){
    $bibNos = $GLOBALS[E4S_OUTPUT_OBJ]->bibNos;

    if ( isset($bibNos->$bibNo) ){
        return $bibNos->$bibNo->collected;
    }
    return false;
}
function getBibCollectionText($bibNo){
    $collected = getBibCollectedStatus($bibNo);
    if ($collected){
        return 'Yes';
    }
    return 'No';
}
function athletesTable(): string {

    $html = '<table id="athlete_table" style="display:none;">';
    $html .= '<tr>';
    $html .= '<td>Bib No</td>';
    $html .= '<td>Collected</td>';
    if (hasSecureAccess()) {
        $html .= '<td>Athlete ID</td>';
        $html .= '<td>Firstname</td>';
        $html .= '<td>Surname</td>';
    }
    $html .= '<td>Athlete</td>';
    $html .= '<td>Gender</td>';
    $html .= '<td>Club</td>';
    $html .= '<td>County</td>';
    $html .= '<td>Region</td>';
    $html .= '<td>Age Group</td>';
    if (hasSecureAccess()) {
//        $html .= '<td>Short Age</td>';
        $html .= '<td>Short Age</td>';
        $html .= '<td>Date of Birth</td>';
        $html .= '<td>URN</td>';
    }

    $eventGroups = $GLOBALS[E4S_OUTPUT_OBJ]->eventgroups;
    $athletes = $GLOBALS[E4S_OUTPUT_OBJ]->athletes;
    if ( e4s_showClassificationColumn($athletes) ) {
        $html .= '<td>Classification</td>';
    }
    foreach ($eventGroups as $eventGroup) {
//        if (e4s_showAgColumn($eventGroup)) {
            $html .= '<td>' . $eventGroup->event . ' AG</td>';
//        }
        if (e4s_showTeamNameColumn($eventGroup)) {
            $entity = ucwords(strtolower(getEntityFromEventName($eventGroup->event)));
            $html .= '<td>' . $eventGroup->event . ' ' . $entity . ' Team Name</td>';
        }else{
	        $html .= '<td>' . $eventGroup->event . '</td>';
        }

        if ( e4s_showPbColumn($eventGroup) ) {
            $html .= '<td>' . $eventGroup->event . '</td>';
        }
    }
    if (E4S_INCLUDE_ENTITY) {
        $html .= '<td>' . ucwords(strtolower(E4S_CLUB)) . '</td>';
        $html .= '<td>' . ucwords(strtolower(E4S_COUNTY)) . '</td>';
        $html .= '<td>' . ucwords(strtolower(E4S_REGION)) . '</td>';
        $html .= '<td>' . 'Age Category' . '</td>';
    }
    $html .= '</tr>';
    $extraCols = hasSecureAccess();
    foreach ($athletes as $athlete) {
        $html .= '<tr>';
        $html .= '<td>' . getBibNo($athlete->bibno) . '</td>';
        $html .= '<td>' . getBibCollectionText($athlete->bibno) . '</td>';
        if ($extraCols) {
            $html .= '<td>' . $athlete->id . '</td>';
            $html .= '<td>' . formatAthleteFirstname($athlete->firstname) . '</td>';
            $html .= '<td>' . formatAthleteSurname($athlete->surname) . '</td>';
        }

        $html .= '<td>' . formatAthlete($athlete->firstname ,$athlete->surname) . '</td>';
        $html .= '<td>' . $athlete->gender . '</td>';
        $html .= '<td>' . $athlete->clubname . '</td>';
        $html .= '<td>' . $athlete->county . '</td>';
        $html .= '<td>' . $athlete->region . '</td>';
        $html .= '<td>' . $athlete->agegroup . '</td>';
        if ($extraCols) {
//            $html .= '<td>' . shortAgeGroup($athlete->agegroup) . '</td>';
            $html .= '<td>' . shortAgeGroup($athlete->agegroup, $athlete->gender) . '</td>';
//            $html .= '<td>' . $athlete->dob . '</td>';
            // convert yyy-mm-dd to dd/mm/yyyy
            $html .= '<td>' . date('d/m/Y', strtotime($athlete->dob)) . '</td>';
            $html .= '<td>' . $athlete->urn . '</td>';
        }
        if ( e4s_showClassificationColumn($athletes) ) {
            $html .= '<td>' . $athlete->classification . '</td>';
        }

        $representClub = '';
        $representCounty = '';
        $representRegion = '';
        $representAgeGroup = ' ';
        foreach ($eventGroups as $eventGroup) {
            $pb = '';
            $teamName = '';
            $useAthleteEvent = null;
            foreach ($athlete->events as $athleteEvent) {
                if ($eventGroup->id === $athleteEvent->eventid) {
                    $useAthleteEvent = $athleteEvent;
                    $pb = $athleteEvent->pb;
                    if ($pb === '0.00') {
                        $pb = '0';
                    }
                    if (isset($eventGroup->isTeamEvent) and isset($athleteEvent->teamName)) {
                        $teamName = $athleteEvent->teamName;
                        $representAgeGroup = $useAthleteEvent->eventAgeGroup;
                    }
                    break;
                }
            }

            if ($pb !== '') {
//                if (e4s_showAgColumn($eventGroup)) {
                    $html .= '<td>';
                    if (is_null($useAthleteEvent)) {
                        $html .= ' ';
                    } else {
                        $html .= shortAgeGroup($useAthleteEvent->eventAgeGroup, $athlete->gender);
                    }
                    $html .= '</td>';
//                }
                if (e4s_showTeamNameColumn($eventGroup)) {
                    $html .= '<td>';
                    $html .= $teamName;
                    $html .= '</td>';
                    if (E4S_INCLUDE_ENTITY) {
                        $eventEntity = getEntityFromEventName($eventGroup->event);
                        if ($eventEntity === E4S_CLUB) {
                            $representClub = $athlete->clubname;
                        }
                        if ($eventEntity === E4S_COUNTY) {
                            $representCounty = $athlete->county;
                        }
                        if ($eventEntity === E4S_REGION) {
                            $representRegion = $athlete->region;
                        }
                    }
                }else{
	                $html .= '<td>';
	                $html .= $eventGroup->event . " EventName";
	                $html .= '</td>';
                }
                if ( e4s_showPbColumn($eventGroup) ) {
                    $html .= '<td>';
                    $html .= $pb;
                    $html .= '</td>';
                }
            } else {
//                if (e4s_showAgColumn($eventGroup)) {
                    $html .= '<td>';
                    $html .= ' ';
                    $html .= '</td>';
//                }
//                if (e4s_showTeamNameColumn($eventGroup)) {
                    $html .= '<td>';
                    $html .= ' ';
                    $html .= '</td>';
//                }
                if ( e4s_showPbColumn($eventGroup) ) {
                    $html .= '<td>';
                    $html .= ' ';
                    $html .= '</td>';
                }
            }
        }
        if (E4S_INCLUDE_ENTITY) {
            $html .= '<td>' . $representClub . '</td>';
            $html .= '<td>' . $representCounty . '</td>';
            $html .= '<td>' . $representRegion . '</td>';
            if (trim($representClub . $representCounty . $representRegion) === '') {
                $representAgeGroup = '';
            }
            $html .= '<td>' . $representAgeGroup . '</td>';
        }
        $html .= '</tr>';
    }

    $html .= '</table>';

    return $html;
}

function getEntityFromEventName($eventName): string {
    $eventName = strtoupper($eventName);
    if (strpos($eventName, E4S_REGION) !== FALSE) {
        return E4S_REGION;
    }
    if (strpos($eventName, E4S_COUNTY) !== FALSE) {
        return E4S_COUNTY;
    }
    if (strpos($eventName, E4S_CLUB) !== FALSE) {
        return E4S_CLUB;
    }
    return '';
}

function shortAgeGroup($agegroup, $gender = '') {
//e4s_dump($agegroup,"Initial",false,true);
    if ($agegroup === '') {
        return '';
    }
    if ($gender === E4S_GENDER_FEMALE or $gender === E4S_GENDER_GIRL) {
        $gender = E4S_GENDER_WOMAN;
    }
    if ($gender === E4S_GENDER_BOY or $gender === E4S_GENDER_MALE) {
        $gender = E4S_GENDER_MAN;
    }
//    else {
//        $gender = E4S_GENDER_MALE;
//    }
    if (strpos($agegroup, 'Under') !== FALSE) {
        $ageCheck = (int)str_replace('Under', '', $agegroup);
        if ($ageCheck < 17) {
            if ($gender === E4S_GENDER_WOMAN) {
                $gender = E4S_GENDER_GIRL;
            } elseif ($gender === E4S_GENDER_MALE) {
                $gender = E4S_GENDER_BOY;
            }
        }
    }

    $masters = FALSE;
    $agegroupCase = strtolower($agegroup);
    if (strpos($agegroupCase, 'mas') !== FALSE or strpos($agegroupCase, 'vet') !== FALSE) {
        $masters = TRUE;
    }
    $agegroup = str_replace('Under', 'U', $agegroup);
    $agegroup = str_replace('Seniors', 'Sen', $agegroup);
    $agegroup = str_replace('Senior', 'Sen', $agegroup);
    $agegroup = str_replace('Masters', 'Mas', $agegroup);
    $agegroup = str_replace('Master', 'Mas', $agegroup);
    $agegroup = str_replace('Juniors', 'Jun', $agegroup);
    if ($gender !== '') {
        $agegroup = str_replace('VET ', '', $agegroup);
    }
    $agegroup = str_replace(' ', '', $agegroup);

    if ($gender !== '') {
//        e4s_dump($agegroup,"Check",false,true);

        if ($agegroup === $gender or $agegroup[0] !== $gender) {
            if ($masters) {
                $agegroup = $gender . $agegroup;
            } else {
                $agegroup = $agegroup . $gender;
            }
        }
    }

    return $agegroup;
}

function pbSort($pb) {
    if ($pb === '' or $pb === '0') {
        return 9999;
    }
    return (float)$pb;

}

function teamRow($team) {
    $html = '<tr>';
    $html .= '<td>' . $team->teamid . '</td>';
    $html .= '<td>' . $team->teamname . '</td>';
    $html .= '<td>' . $team->agegroup . '</td>';
    $html .= '<td>' . $team->athleteEvent . '</td>';
    $html .= '<td>' . fullGender($team->gender) . '</td>';
    if (hasSecureAccess()) {
        $html .= '<td>' . $team->price . '</td>';
        $html .= '<td>' . $team->user . '</td>';
        $html .= '<td>' . $team->email . '</td>';
        $html .= '<td>' . $team->created . '</td>';
    }
    return $html;
}

function teamTable() {
    $incCounty = e4s_isAAIDomain();
    $html = '<table id="team_table" style="display:none;">';
    $html .= '<tr>';
    $html .= '<td>Team ID</td>';
    $html .= '<td>Team Name</td>';
    $html .= '<td>Age Group</td>';
    $html .= '<td>Event</td>';
    $html .= '<td>Gender</td>';
    if (hasSecureAccess()) {
        $html .= '<td>Price</td>';
        $html .= '<td>User</td>';
        $html .= '<td>Email</td>';
        $html .= '<td>Created</td>';
    }
    $teams = $GLOBALS[E4S_OUTPUT_OBJ]->teams;
    $hasAthletes = FALSE;
    foreach ($teams as $team) {
        if (sizeof($team->athletes) > 0) {
            $hasAthletes = TRUE;
            break;
        }
    }
    if ($hasAthletes) {
        $html .= '<td>Forename</td>';
        $html .= '<td>Surname</td>';
        $html .= '<td>Club</td>';
        if ( $incCounty ){
            $html .= '<td>County</td>';
            $html .= '<td>Region</td>';
        }
        $html .= '<td>Notes</td>';
        if (hasSecureAccess()) {
            $html .= '<td>DOB</td>';
            $html .= '<td>Reg</td>';
            $html .= '<td>ID</td>';
        }
    }
    $html .= "</tr>";

    $treeData = array();
    $athletes = (array)$GLOBALS[E4S_OUTPUT_OBJ]->athletes;

    foreach ($teams as $team) {
        $teamDataObj = new stdClass();
        $teamDataObj->id = $team->teamid;
        $teamDataObj->teamname = $team->teamname;
        $teamDataObj->agegroup = $team->agegroup;
        $teamDataObj->event = $team->athleteEvent;
        $teamDataObj->gender = fullGender($team->gender);
        $teamDataObj->price = $team->price;
        $teamDataObj->user = $team->user;
        $teamDataObj->email = $team->email;
        $teamDataObj->created = $team->created;
        $teamDataObj->athleteId = "";
        $teamDataObj->clubname = "";
        $teamDataObj->firstname = "";
        $teamDataObj->surname = "";
        $teamDataObj->county = "";
        $teamDataObj->region = "";
        $teamDataObj->dob = "";
        $teamDataObj->urn = "";
        $teamDataObj->children = array();

        if (!$hasAthletes) {
            $html .= teamRow($team);
            $html .= "</tr>";
        } else {
            if (sizeof($team->athletes) === 0) {
                $html .= teamRow($team);

                $html .= '<td>No Athletes</td>';
                $html .= '<td> </td>';
                $html .= '<td> </td>';
                if ( $incCounty ){
                    $html .= '<td> </td>'; // county
                    $html .= '<td> </td>'; // Region
                }
                $html .= '<td> </td>';

                if (hasSecureAccess()) {
                    $html .= '<td> </td>';
                    $html .= '<td> </td>';
                    $html .= '<td> </td>';
                }
                $html .= "</tr>";
            } else {
                foreach ($team->athletes as $teamAthlete) {
                    $html .= teamRow($team);

                    $athleteId = (int)$teamAthlete->athleteId;
                    $athlete = $athletes[$athleteId];
                    $html .= '<td>' . $athlete->firstname . '</td>';
                    $html .= '<td>' . $athlete->surname . '</td>';
                    $html .= '<td>' . $athlete->clubname . '</td>';
                    if ( $incCounty ){
                        $html .= '<td>'  . $athlete->county . '</td>';
                        $html .= '<td>'  . $athlete->region . '</td>';
                    }
                    $html .= '<td>' . $teamAthlete->notes . '</td>';
                    if (hasSecureAccess()) {
                        $html .= '<td>' . date("d/m/Y", strtotime($athlete->dob)) . '</td>';
                        $html .= '<td>' . $athlete->urn . '</td>';
                        $html .= '<td>' . $athleteId . '</td>';
                    }
                    $html .= "</tr>";

                    $teamAthleteDataObj = new stdClass();
                    $teamAthleteDataObj->id = "";
                    $teamAthleteDataObj->teamname = "";
                    $teamAthleteDataObj->agegroup = "";
                    $teamAthleteDataObj->event = "";
                    $teamAthleteDataObj->gender = "";
                    $teamAthleteDataObj->price = "";
                    $teamAthleteDataObj->user = "";
                    $teamAthleteDataObj->email = "";
                    $teamAthleteDataObj->created = "";
                    $teamAthleteDataObj->athleteId = $athleteId;
                    $teamAthleteDataObj->clubname = $athlete->clubname;
                    $teamAthleteDataObj->county = $athlete->county;
                    $teamAthleteDataObj->region = $athlete->region;
                    $teamAthleteDataObj->firstname = $athlete->firstname;
                    $teamAthleteDataObj->surname = $athlete->surname;
                    $teamAthleteDataObj->dob = $athlete->dob;
                    $teamAthleteDataObj->urn = $athlete->urn;

                    $teamDataObj->children[] = $teamAthleteDataObj;
                }
            }
        }
        $treeData[] = $teamDataObj;
    }
    $html .= "</table>";

    $GLOBALS[E4S_OUTPUT_OBJ]->treedata = $treeData;
    return $html;
}

function individualTable(): string {

    $html = '<table id="individual_table" style="display:none;">';
    $html .= '<tbody>';
    $html .= '<tr>';
    $html .= '<th>Bib No</th>';
    $html .= '<th>Checked In</th>';
    if (hasSecureAccess()) {
        $html .= '<th>Entry ID</th>';
        $html .= '<th>Event ID</th>';
    }
    $html .= '<th>Event</th>';
    $html .= '<th>Discipline</th>';
    $html .= '<td>Event Age Group</td>';
    $html .= '<td>Event No</td>';
    $html .= '<td>Split</td>';
    $html .= '<td>Event Date</td>';
    $html .= '<td>Event Time</td>';

    if (isCheckInEnabled()) {
        $html .= '<td>Heat No<br>Entered</td>';
        $html .= '<th>Lane No<br>Entered</td>';
        $html .= '<td>Heat No<br>Checked-In</td>';
        $html .= '<th>Lane No<br>Checked-In</th>';
    } else {
        $html .= '<td>Heat No</td>';
        $html .= '<th>Lane No</th>';
    }
    $html .= '<th>T-F</th>';
    if (hasSecureAccess()) {
        $html .= '<th>Order No</th>';
        $html .= '<th>Line No</th>';
        $html .= '<th>Athlete ID</th>';
        $html .= '<td>Firstname</td>';
        $html .= '<td>Surname</td>';
    }

    $html .= '<td>Athlete</td>';
    $html .= '<td>Gender</td>';
    $html .= '<td>Club</td>';
    $html .= '<td>County</td>';
    $html .= '<td>Region</td>';
    $html .= '<td>Age Group</td>';
    if (hasSecureAccess()) {
        $html .= '<td>Short Age</td>';
        $html .= '<td>Date of Birth</td>';
        $html .= '<td>URN</td>';
    }
    $athletes = (array)$GLOBALS[E4S_OUTPUT_OBJ]->athletes;
    if ( e4s_showClassificationColumn($athletes) ) {
        $html .= '<td>Classification</td>';
    }
    $html .= '<td>PB</td>';
    if (hasSecureAccess()) {
        $html .= '<td>PB Sort</td>';
        $html .= '<td>Price</td>';
        $html .= '<td>Fee</td>';
        $html .= '<td>Coupon</td>';
        $html .= '<td>Value</td>';
        $html .= '<td>Email</td>';
        $html .= '<td>Contact</td>';
        $html .= '<td>Created</td>';
    }
    $html .= '</tr>';

    $obj = $GLOBALS[E4S_OUTPUT_OBJ]->entries;
    $eventsByName = $GLOBALS[E4S_OUTPUT_OBJ]->eventsByName;

    $text = '';
    foreach ($obj as $entry) {
        if (!isset($athletes['' . $entry->athleteId])) {
            e4s_dump($athletes, false,true);
        }
        $athlete = $athletes['' . $entry->athleteId];
        if (is_null($athlete->firstname)) {
            if ($text === '') {
                $text = 'Please inform E4S : ';
                echo $text;
            }
            echo $entry->athleteId . ', ';
            continue;
        }
        $html .= '<tr>';
        $html .= '<td>' . getBibNo($athlete->bibno) . '</td>';
        $html .= '<td>' . getEntryCheckedInStatus($entry) . '</td>';

        if (hasSecureAccess()) {
            $html .= '<td>' . $entry->entryId . '</td>';
            if (!array_key_exists($athlete->gender . '-' . $entry->event, $eventsByName)) {
                $html .= '<td>UNKNOWN</td>';
            }else {
                $html .= '<td>' . $eventsByName[$athlete->gender . '-' . $entry->event]->id . '</td>';
            }
        }
        $html .= '<td>' . $entry->eventGroup . '</td>';
        $html .= '<td>' . $entry->event . '</td>';
        $html .= '<td>' . $entry->agegroup . '</td>';
        $html .= '<td>' . $entry->eventno . '</td>';
        $html .= '<td>' . $entry->split . '</td>';
        $html .= '<td>' . e4s_formatDateAndTimeForReport($entry->startdate, E4S_DATE_ONLY) . '</td>';
        $html .= '<td>' . e4s_formatDateAndTimeForReport($entry->startdate, E4S_TIME_ONLY) . '</td>';
        $html .= '<td>' . $entry->heatInfo->heatNo . '</td>';
        $html .= '<td>' . $entry->heatInfo->laneNo . '</td>';

        if (isCheckInEnabled()) {
            $html .= '<td>' . $entry->heatInfo->heatNoCheckedIn . '</td>';
            $html .= '<td>' . $entry->heatInfo->laneNoCheckedIn . '</td>';
        }
        $html .= '<td>' . e4s_expandEventType($entry->tf) . '</td>';
        if (hasSecureAccess()) {
            $html .= '<td>' . $entry->orderid . '</td>';
            $html .= '<td>' . $entry->variationid . '</td>';
            $html .= '<td>' . $athlete->id . '</td>';
            $html .= '<td>' . formatAthleteFirstname($athlete->firstname) . '</td>';
            $html .= '<td>' . formatAthleteSurname($athlete->surname) . '</td>';
        }

        $html .= '<td>' . formatAthlete($athlete->firstname, $athlete->surname) . '</td>';
        $html .= '<td>' . $athlete->gender . '</td>';
        $html .= '<td>' . $athlete->clubname . '</td>';
        $html .= '<td>' . $athlete->county . '</td>';
        $html .= '<td>' . $athlete->region . '</td>';
        $html .= '<td>' . $athlete->agegroup . '</td>';
        if (hasSecureAccess()) {
            $html .= '<td>' . shortAgeGroup($athlete->agegroup, $athlete->gender) . '</td>';
//            $html .= '<td>' . $athlete->dob . '</td>';
                        $html .= '<td>' . date('d/m/Y', strtotime($athlete->dob)) . '</td>';
            $html .= '<td>' . $athlete->urn . '</td>';
        }
        if ( e4s_showClassificationColumn($athletes) ) {
            $html .= '<td>' . $athlete->classification . '</td>';
        }

        $entryPB = str_replace('#', '',$entry->pb);
        $html .= '<td>' . $entryPB . '</td>';
        if (hasSecureAccess()) {
            $html .= '<td>' . pbSort($entryPB) . '</td>';
            $html .= '<td>' . $entry->price . '</td>';
            $html .= '<td>' . $entry->fee . '</td>';
            $html .= '<td>' . $entry->coupon . '</td>';
            $html .= '<td>' . $entry->value . '</td>';
            $html .= '<td>' . $entry->email . '</td>';
            $html .= '<td>' . $entry->phone . '</td>';
            $html .= '<td>' . $entry->created . '</td>';
        }

        $html .= '</tr>';
    }
    $html .= '</tbody>';
    $html .= '</table>';

    return $html;
}

function summaryTable(): string {
    $entries = $GLOBALS[E4S_OUTPUT_OBJ]->entries;
    $teams = $GLOBALS[E4S_OUTPUT_OBJ]->teams;
    // Genders => Events => AgeGroups
    $genders = array();
    $events = array();
    $ageGroups = array();
    foreach ($entries as $entry) {
        if (!array_key_exists($entry->gender, $genders)) {
            $genders [$entry->gender] = $entry->gender;
        }
        if (!array_key_exists($entry->event, $events)) {
            $events [$entry->event] = $entry->event;
        }
        if (!array_key_exists($entry->agegroup, $ageGroups)) {
            $obj = new stdClass();
            $obj->cnt = 0;
            $obj->teams = -1;
            $ageGroups [$entry->agegroup] = $obj;
        }
    }
    foreach ($teams as $team) {
        if (!array_key_exists($team->gender, $genders)) {
            $genders [$team->gender] = $team->gender;
        }
        if (!array_key_exists($team->event, $events)) {
            $events [$team->event] = $team->event;
        }
        if (!array_key_exists($team->agegroup, $ageGroups)) {
            $obj = new stdClass();
            $obj->cnt = 0;
            $obj->teams = -1;
            $ageGroups [$team->agegroup] = $obj;
        }
    }
    $summaryObj = $genders;
    foreach ($summaryObj as $gender => $summaryGender) {
        $summaryObj[$gender] = $events;
        foreach ($events as $event) {
            $summaryObj[$gender][$event] = e4s_deepCloneTo($ageGroups, E4S_OPTIONS_ARRAY);
        }
    }
    foreach ($entries as $entry) {
	    if ( (int)$entry->paid === E4S_ENTRY_QUALIFY){
		    continue;
	    }
        $summaryObj[$entry->gender][$entry->event][$entry->agegroup]->cnt += 1;
    }

    foreach ($teams as $team) {
        if ( $summaryObj[$team->gender][$team->event][$team->agegroup]->teams === -1 ){
	        $summaryObj[$team->gender][$team->event][$team->agegroup]->teams = 1;
        }else {
	        $summaryObj[ $team->gender ][ $team->event ][ $team->agegroup ]->teams += 1;
        }
        $summaryObj[$team->gender][$team->event][$team->agegroup]->cnt += sizeof($team->athletes);
    }
	$html = '<div><button type="button" class="e4s-button e4s-button--primary e4s_card_button ui-button ui-corner-all ui-widget no-print" style="color:white!important;" onclick="window.print();">Print</button></div>';
    $html .= "<table id='e4s_summary_table' class='e4s_summary_table'>";
    $genderCnt = sizeof($genders);
    $cols = sizeof($ageGroups) * $genderCnt;
    $cols += $genderCnt; // Totals = No Of Genders, Label and GrandTotal not included
    $html .= '<tr>';
    $html .= '<td>&nbsp;</td>';
    foreach ($genders as $gender) {
        $html .= "<td class='e4s_summary_gender' colspan='" . $cols / $genderCnt . "'>" . fullGender($gender) . '</td>';
    }
    $html .= '<td>&nbsp;</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= "<td class='e4s_summary_label'>Event</td>";
    $ageGroupTotals = array();
    foreach ($genders as $gender) {
        $ageGroupTotals[$gender] = array();
        foreach ($ageGroups as $ageGroup => $count) {
            $obj = new stdClass();
            $obj->cnt = 0;
            $obj->teams = 0;
            $ageGroupTotals[$gender][$ageGroup] = $obj;
            $html .= "<td class='e4s_summary_label'>" . $ageGroup . '</td>';
        }
        $html .= "<td class='e4s_total_label'>Total</td>";
    }
    $html .= "<td class='e4s_total_label'>Grand Total</td>";
    $html .= '</tr>';

    foreach ($events as $event) {
        $eventCnt = 0;
        $eventTeamCnt = 0;
        $teamEvent = FALSE;
        $html .= '<tr>';
        $html .= "<td class='e4s_summary_label'>" . $event . '</td>';
        foreach ($genders as $gender) {
            $eventGenderCnt = 0;
            $eventTeamGenderCnt = 0;
            foreach ($ageGroups as $ageGroup => $count) {
                $eventGenderAgCnt = $summaryObj[$gender][$event][$ageGroup]->cnt;
                $eventGenderCnt += $eventGenderAgCnt;
                $ageGroupTotals[$gender][$ageGroup]->cnt += $eventGenderAgCnt;
                $output = '' . $eventGenderAgCnt;
                $eventTeamGenderAgCnt = $summaryObj[$gender][$event][$ageGroup]->teams;
                if ($eventTeamGenderAgCnt > -1) {
                    $teamEvent = TRUE;
                    $ageGroupTotals[$gender][$ageGroup]->teams += $eventTeamGenderAgCnt;
                    $eventTeamGenderCnt += $eventTeamGenderAgCnt;
//                    $output = $eventTeamGenderAgCnt . ' / ' . $output;
                    $output = ' ' . $eventTeamGenderAgCnt;
                }

                $html .= "<td class=''>" . $output . '</td>';
            }
            $output = '' . $eventGenderCnt;
            $eventCnt += $eventGenderCnt;

            if ($teamEvent) {
//                $output = $eventTeamGenderCnt . ' / ' . $output;
                $output = '' . $eventTeamGenderCnt ;
                $eventTeamCnt += $eventTeamGenderCnt;
            }
            $html .= "<td class='e4s_total_data'>" . $output . '</td>';
        }
        $output = '' . $eventCnt;
        if ($teamEvent) {
//            $output = $eventTeamCnt . ' / ' . $output;
            $output = '' . $eventTeamCnt ;
        }
        $html .= "<td class='e4s_total_data'>" . $output . '</td>';
        $html .= '</tr>';
    }

    $html .= '<tr>';
    $html .= "<td class='e4s_total_label e4s_grand_totals'>Totals</td>";
    $genderTotals = array();
    $grandTotal = 0;
    foreach ($genders as $gender) {
        $genderTotals[$gender] = 0;
        foreach ($ageGroups as $ageGroup => $count) {
            $html .= "<td class='e4s_total_data e4s_grand_totals'>" . $ageGroupTotals[$gender][$ageGroup]->cnt . '</td>';
            $genderTotals[$gender] += $ageGroupTotals[$gender][$ageGroup]->cnt;
        }
        $html .= "<td class='e4s_total_data e4s_grand_totals'>" . $genderTotals[$gender] . '</td>';
        $grandTotal += $genderTotals[$gender];
    }
    $html .= "<td class='e4s_total_data e4s_grand_totals'>" . $grandTotal . '</td>';
    $html .= '</tr>';
    $html .= '</table>';
    return $html;
}

function generateCardBaseHead($eventType, $name, $colCount) {
    $html = "<div id='{$name}' style='display:none'>";
    $html .= "<table class='e4sTable card_table table_width_100_perc'>";
    $html .= '<tr>';
    $html .= "<td colspan='" . $colCount . "' class=''>";
    $html .= generateCardHeader();
    $html .= '</td>';
    $html .= '</tr>';
    return $html;
}

function generateFieldDistanceCardColHTML($text, $colNumber, $colSpan = 1, $rowspan = 1, $extraClass = '', $trial = 0) {
    $html = "<td colspan='{$colSpan}' rowspan='{$rowspan}' class='distance_column_header_label distance_card_col_{$colNumber} {$extraClass}'>";
//     Needs EDM Access not full Access
    if (hasResultsAccess()) {
        $html .= "<span onclick='checkTrialDistanceResults({$trial})'>{$text}</span>";
    } else {
        $html .= "<span>{$text}</span>";
    }

    $html .= '</td>';

    return $html;
}

function generateHeightCardHTML($school): string {
    $maxCol = 23;
    $html = generateCardBaseHead('<?php echo E4S_UOM_HEIGHT ?>', 'heightTableDiv', $maxCol);
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '<span id="heightTableRows"></span>';

    if (hasSecureAccess(FALSE)) {
        $html .= generateFieldFooter();
    }

    $html .= '</div>';
    return $html;
}

function generateDistanceCardHTML($school) {
    $maxColNo = E4S_CARD_DISTANCE_COLS;
    $html = generateCardBaseHead('D', 'distanceTableDiv', $maxColNo);
    $colNo = 1;
    // distance Card Column Titles
    $html .= "<tr class='card_table_headerInProgress'>";
    $html .= "</tr>";
    $html .= "<tr class='card_table_header'>";
    $html .= generateFieldDistanceCardColHTML('', $colNo++, 1, 1, '');
    $html .= generateFieldDistanceCardColHTML('Bib', $colNo++, 1, 1, 'card_bib');
    $html .= generateFieldDistanceCardColHTML('Age', $colNo++, 1, 1, 'card_age');
    $html .= generateFieldDistanceCardColHTML('Athlete', $colNo++, 1, 1, 'card_name');
    if ($school) {
        $html .= generateFieldDistanceCardColHTML('School', $colNo++, 1, 1, 'card_club');
    } else {
        $html .= generateFieldDistanceCardColHTML('Organisation', $colNo++, 1, 1, 'card_club');
    }
    $html .= generateFieldDistanceCardColHTML('1st Trial', $colNo++, 1, 1, 'trialHeader', 1);
    $html .= generateFieldDistanceCardColHTML('2nd Trial', $colNo++, 1, 1, 'trialHeader', 2);
    $html .= generateFieldDistanceCardColHTML('3rd Trial', $colNo++, 1, 1, 'trialHeader', 3);
    $html .= generateFieldDistanceCardColHTML('Best of 3 Trials', $colNo++, 1, 1,'trialHeader');
    $html .= generateFieldDistanceCardColHTML('Trial 3 Position', $colNo++, 1, 1, 'trialHeader');
    $html .= generateFieldDistanceCardColHTML('Jump Order', $colNo++, 1, 1, 'jumpOrderHeader');
    $html .= generateFieldDistanceCardColHTML('4th Trial', $colNo++, 1, 1, 'trialHeader', 4);
    $html .= generateFieldDistanceCardColHTML('5th Trial', $colNo++, 1, 1, 'trialHeader', 5);
    $html .= generateFieldDistanceCardColHTML('6th Trial', $colNo++, 1, 1, 'trialHeader', 6);
    $html .= generateFieldDistanceCardColHTML('Best Trial', $colNo++, 1, 1,'trialHeader');
    $html .= generateFieldDistanceCardColHTML('Final Position', $colNo, 1, 1, 'trialHeader');
    $html .= '</tr>';

    if ($maxColNo !== $colNo) {
        Entry4UIError(9122, 'Field Card Column Error');
    }
    
    $resultClass = 'distance_data_result';
    $best = ' distance_data_best ';
    $pos = 'distance_data_pos';
    $finalPos = 'distance_data_3pos';
    for ($x = 1; $x <= E4S_FIELD_CARD_COUNT; $x++) {
        $html .= "<tr class='distance_data_row' id='row_" . $x . "'>";
        $html .= '<td id="row_1_' . $x . '_cell" class="distance_data_col distance_row card_data">' . $x . '</td>';
        $html .= '<td id="bib_1_' . $x . '_cell" class="distance_data_col distance_data_bib field_data card_bib card_data">';
        $html .= '<table class="e4sTable bibTable" style="width:100%">';
        $html .= '  <tbody>';
        $html .= '  <tr>';
        $html .= '       <td class="centre">';
        $html .= '           <span class="e4sBibNo" id="bib_1_' . $x . '">&nbsp;</span>';
        $html .= '       </td>';
        $html .= '       <td class="statusTD" id="bib_1_' . $x . '_status_cell">';
        $html .= '       </td>';
        $html .= '   </tr></tbody>';
        $html .= '</table>';
        $html .= '</td>';

        $html .= generateDistanceCardDataHTML('age', $x, 'card_age field_data card_data');
        $html .= generateDistanceCardDataHTML('name', $x, 'card_name field_data_large card_data_left');
        $html .= generateDistanceCardDataHTML('club', $x, 'card_club field_data_large card_data_left');
        $html .= generateDistanceCardDataHTML('t1', $x, $resultClass, false);
        $html .= generateDistanceCardDataHTML('t2', $x, $resultClass, false);
        $html .= generateDistanceCardDataHTML('t3', $x, $resultClass, false);
        $html .= generateDistanceCardDataHTML('b3', $x,  $best  , false);
        $html .= generateDistanceCardDataHTML('3pos', $x, $pos);
        $html .= generateDistanceCardDataHTML('3jo', $x, $pos);
        $html .= generateDistanceCardDataHTML('t4', $x, $resultClass, false);
        $html .= generateDistanceCardDataHTML('t5', $x, $resultClass, false);
        $html .= generateDistanceCardDataHTML('t6', $x, $resultClass, false);
        $html .= generateDistanceCardDataHTML('b6', $x, $best , false);
        $html .= generateDistanceCardDataHTML('pos', $x, $finalPos, true);
        $html .= '</tr>';
    }
	$html .= '</table>';

    if (hasSecureAccess(FALSE)) {
        $html .= generateFieldFooter();
    }

    $html .= '</div>';
    return $html;
}

function generateDistanceCardDataHTML($text, $rowNumber, $extraClass = '', $incDataCol = true) {
    $dataColClass = "distance_data_col ";
    if ( !$incDataCol ) {
        $dataColClass = "";
    }
    $html = "<td class='" . $dataColClass . "distance_data_{$text} {$extraClass}'>";
    $html .= "<span id='field_{$text}_{$rowNumber}'>&nbsp;</span>";
    $html .= '</td>';

    return $html;
}

function generateFieldFooter() {
    $html = "<table class='e4sTable card_table table_width_100_perc'>";
    $html .= '<tr>';
    $html .= "<td class='field_info_row' colspan='5'>";
    $html .= "<div id='field_info' style='margin-left: 85px'>Information</div>";
    $html .= "<div class='card_link_field_text_span'>Entry4Sports Field Card. For more information https://entry4sports.com</div>";
    $html .= '</td>';
    $html .= '</tr>';

    $html .= "<tr class='card_field_judge_row'>";
    $html .= "<td class='card_judges_label border_top_strong' >";
    $html .= '<span>Officials</span>';
    $html .= '</td>';
    $html .= "<td class='card_judges_count_label border_top_strong'>";
    $html .= '<span>1 : </span>';
    $html .= '</td>';
    $html .= "<td class='card_judges_count_label border_top_strong'>";
    $html .= '<span>2 : </span>';
    $html .= '</td>';
    $html .= "<td class='card_judges_count_label border_top_strong'>";
    $html .= '<span>3 : </span>';
    $html .= '</td>';
    $html .= "<td class='card_judges_count_label border_top_strong'>";
    $html .= '<span>Ref : </span>';
    $html .= '</td>';
    $html .= '</tr>';
	$html .= '</table>';
    return $html;
}

function generateTrackCardOverHTML() {
    $useColCount = 18;
    $html = generateCardBaseHead('T', 'trackOverTableDiv', $useColCount);

    $html .= '<tr>';
    $html .= "<td colspan='{$useColCount}'>";
    $html .= "<div id='autoShowScroll'>";
    $html .= "<div id='trackOverTableInfo'></div>";
    $html .= '</div>';
    $html .= '</td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '</div>';
    return $html;
}

function generateTrackCardHTML($laneCnt) {
    $labelTDCount = 2;
    $dataTDCount = 2;
    $dataRowCount = 3;
    $heatCnt = 20;
    if (hasSecureAccess()) {
        $heatCnt = 10;
    }

    $totalColCnt = ($laneCnt * $dataTDCount) + $labelTDCount;
    $html = generateCardBaseHead('T', 'trackTableDiv', $totalColCnt);
    // Lane Headers Row
    $html .= "<tr class='card_table_header'>";
    $html .= '<td style="width:5%;" colspan="' . $labelTDCount . '">';
    $html .= '<span>&nbsp;</span>';
    $html .= '</td>';

    $laneWidth = 95 / $laneCnt;
    for ($x = 1; $x <= $laneCnt; $x++) {
        $html .= '<td colspan="' . $dataTDCount . '" style="width:' . $laneWidth . '%;" class="track_lane_header_label">';
        $html .= "<span>{$x}</span>";
        $html .= '</td>';
    }
    $html .= '</tr>';
//      Heat Row
    for ($heat = 1; $heat <= $heatCnt; $heat++) {
        $html .= "<tr class='track_heat_" . $heat . "'>";
        $html .= "<td rowspan='{$dataRowCount}' class='x track_data_grid track_heatNo_label track_heat_divider'>";
        $html .= "<div class='track_heatNo_value' trackHeat='{$heat}'>{$heat}</div>";
        $html .= "<div class='track_heatNo_time' trackTime='{$heat}'></div>";
        $html .= '</td>';
        $html .= "<td class='track_data_grid track_heat_labels track_bibno_label'>";
        $html .= '<span>Bib #</span>';
        $html .= '</td>';
        for ($laneRow = 1; $laneRow <= $laneCnt; $laneRow++) {
            $html .= "<td class='track_data_grid track_bibno_data'>";
            $html .= "<span id='bib_{$heat}_{$laneRow}'>&nbsp;</span>";
            $html .= '</td>';
            $html .= "<td class='track_data_grid track_age_data'>";
            $html .= "<span id='age_{$heat}_{$laneRow}'>&nbsp;</span>";
            $html .= '</td>';
        }
        $html .= '</tr>';

        $html .= "<tr class='track_heat_" . $heat . "'>";
        $html .= "<td class='track_data_grid track_heat_labels track_athlete_label'>";
        $html .= '<span>Athlete</span>';
        $html .= '</td>';
        for ($laneRow = 1; $laneRow <= $laneCnt; $laneRow++) {
            $html .= "<td colspan='2' class='track_data_grid track_data track_athlete_data'>";
            $html .= "<span class='track_athlete_data_text' id='athlete_{$heat}_{$laneRow}'>&nbsp;</span>";
            $html .= '</td>';
        }
        $html .= '</tr>';

        $html .= "<tr class='track_heat_" . $heat . " track_heat_divider'>";
        $html .= "<td class='track_data_grid track_heat_labels track_club_label'>";
        $html .= '<span>Club</span>';
        $html .= '</td>';
        for ($laneRow = 1; $laneRow <= $laneCnt; $laneRow++) {
            $html .= "<td colspan='2' class='track_data_grid track_data track_club_data'>";
            $html .= "<span id='club_{$heat}_{$laneRow}'>&nbsp;</span>";
            $html .= '</td>';
        }
        $html .= '</tr>';
    }

    if (hasSecureAccess(FALSE)) {
        $html .= generateTrackFooter($totalColCnt);
    }
    $html .= '</table>';
    $html .= '</div>';
    $html .= '<table>';
    $html .= '<tr>';
    $html .= '</tr>';
    $html .= '</table>';

    return $html;
}

function generateCardHeader() {
    $html = "<div id='cardHeaderDiv'>";
    $html .= "<table class='e4sTable card_table card_header table_width_100_perc' >";
    $html .= '<tr>';
    $html .= '<td>';
    $html .= "<span class='card_header_label card_header_e4s' id='card_title'></span>";
    $html .= '</td>';
    $html .= '<td>';
    $html .= "<span class='card_header_label'>Date : </span>";
    $html .= "<span id='card_date' class='card_header_data'></span>";
    $html .= '</td>';
    $html .= '<td>';
    $html .= "<span class='card_header_label'>Venue : </span>";
    $html .= "<span id='card_venue' class='card_header_data'></span>";
    $html .= '</td>';
    $html .= '<td>';
    $html .= "<span class='card_header_label'>Meeting : </span>";
    $html .= "<span  id='card_competition' class='card_header_data'></span>";
    $html .= '</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td>';
    $html .= "<span class='card_header_label'>Event : </span>";
    $html .= "<span  id='card_eventno' class='card_header_data'></span> : ";
    $html .= "<span  id='card_event' class='card_header_data'></span>";
    $html .= '</td>';
    $html .= '<td colspan=2>';
    $trialText = '';
//    if ( $eventType !== "T" ){
//        $trialText = "Trials : ";
//    }
    $html .= "<span class='card_header_label'>" . $trialText . '</span>';
    $html .= "<span id='card_trials' class='card_header_data'></span>";

    $html .= '</td>';
    $html .= '<td>';
	$html .= '<div class="card_header_times">';
    $html .= '<span class="card_header_label">Start Time : ';
    $html .= '<span id="header_start_time" class="card_header_data"></span>';
	$html .= '</span>';
    $html .= '<span id="header_end_time" class="card_header_label">End Time : </span>';
    $html .= '<span id="header_time" class="header_time">Card # :</span>';
	$html .= '</div>';
    $html .= '</td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '</div>';

    return $html;
}

function generateTrackFooter($totalColCnt) {
    $html = '';
//    $html =  "<table class='e4sTable card_table table_width_100_perc'>";
    $html .= '<tr>';
    $html .= "<td class='track_info_row' colspan='{$totalColCnt}'>";
    $html .= "<span id='track_info'>Information</span>";
    $html .= '</td>';
    $html .= '</tr>';

    $html .= '<tr>';
    $html .= "<td class='card_link_text' colspan='{$totalColCnt}'>";
    $html .= "<span class='card_link_text_span'>Entry4Sports Track Card. For more information https://entry4sports.com</span>";
    $html .= '</td>';
    $html .= '</tr>';

    return $html;
}

/*    Report output functions  */
// PHP Data Function
function getReportData($compid) {
    $baseAGRows = e4s_getBaseAgeGroups();
    $GLOBALS['baseAGRows'] = $baseAGRows;

    $config = e4s_getConfig();

// Get Info about Comp
    $allCompDOBs = getAllCompDOBs($compid);
    $compObj = e4s_GetCompObj($compid);
    $compRow = $compObj->getFullRow();
    $coptions = $compObj->getFullOptions();
    $compRow['options'] = $coptions;
    $nonGuests = array();
    if (isset($coptions->nonGuest)) {
        $nonGuests = $coptions->nonGuest;
    }
// add certain config properties to the comp options
    $coptions->vetDisplay = $config['vetDisplay'];
    $coptions->currency = $config['currency'];

    $heatOrder = E4S_SLOWEST_FIRST;
    if (isset($coptions->heatOrder)) {
        $heatOrder = strtolower($coptions->heatOrder);
    }

//Get Club and Area Information
    $sql = 'select * from ' . E4S_TABLE_CLUBS;
    $result = e4s_queryNoLog($sql);
    $clubsFromDB = $result->fetch_all(MYSQLI_ASSOC);
    $clubsArr = array();
    $blankClub = array();
    $blankClub['id'] = '0';
    $blankClub['Clubname'] = E4S_UNATTACHED;
    $blankClub['Region'] = 'Unknown';
    $blankClub['Country'] = 'Unknown';
    $blankClub['areaid'] = '0';
    $blankClub['clubtype'] = 'c';
    $blankClub['active'] = 0;
    $blankClub['options'] = '{}';

    $clubsArr['0'] = $blankClub;

    foreach ($clubsFromDB as $clubRow) {
        $clubsArr[$clubRow['id']] = $clubRow;
    }

    $sql = 'select * from ' . E4S_TABLE_AREA;
    $result = e4s_queryNoLog($sql);
    $areasFromDB = $result->fetch_all(MYSQLI_ASSOC);
    $areaArr = array();
    foreach ($areasFromDB as $areaRow) {
        $areaArr[$areaRow['id']] = $areaRow;
    }

    $ageGroupIds = array();
    $eventIds = array();
    $ceObjs = $compObj->getCeObjs();
    foreach ($ceObjs as $ceObj){
        $ageGroupIds[$ceObj->ageGroupId] = $ceObj->ageGroupId;
        $eventIds[] = $ceObj->eventId;
        if (isset($ceObj->options->ageGroups)) {
            foreach ($ceObj->options->ageGroups as $ageGroup) {
                if (isset($ageGroup->ageGroup)) {
                    $ageGroupIds[$ageGroup->ageGroup] = $ageGroup->ageGroup;
                }
            }
        }
    }


    $sql = 'SELECT id, Name 
        FROM ' . E4S_TABLE_AGEGROUPS . '
        where id in (' . implode(',', $ageGroupIds) . ') order by minage';
    $result = e4s_queryNoLog($sql);
    $sep = '';
    $globalAgeGroups = array();
    outputTxt('{');
    outputTxt('"agegroups" :');
    outputTxt('[');
    while ($row = mysqli_fetch_array($result, TRUE)) {
        outputTxt($sep);
        outputTxt('{');
        outputTxt('"id": ' . $row['id'] . ',');
        outputTxt('"name": "' . $row['Name'] . '"');
        $globalAgeGroups[$row['id']] = $row['Name'];
        outputTxt('}');
        $sep = ',';
    }
    $GLOBALS['E4S_AGE_GROUPS'] = $globalAgeGroups;
    outputTxt(']');
    outputTxt(',');

    // EventObj
    $sql = 'SELECT * 
        FROM ' . E4S_TABLE_EVENTS . '
        where id in (' . implode(',',$eventIds) . ')
        order by name';

    $result = e4s_queryNoLog($sql);

    outputTxt('"events" :');
    outputTxt('[');
    $sep = '';
    while ($eventRow = mysqli_fetch_array($result, TRUE)) {
        outputTxt($sep);
        outputTxt('{');
        outputTxt('"id": ' . $eventRow['ID'] . ',');
        outputTxt('"name": "' . $eventRow['Name'] . '",');
        outputTxt('"gender": "' . $eventRow['Gender'] . '"');
        outputTxt('}');
        $sep = ',';
    }

    outputTxt(']');
    outputTxt(',');

// EventGroupObj
    $sqlSep = '~';
    $EG_ID = 0;
    $EG_EVENTNO = 1;
    $EG_TYPENO = 2;
    $EG_NAME = 3;
    $EG_TF = 4;
    $EG_DATE = 5;
    $EG_UOMTYPE = 6;
    $EG_EVENTID = 7;
//    Not sure why I concat values to split out ????
    $sql = "
        SELECT distinct (concat(eg.id,'" . $sqlSep . "',eg.eventNo,'" . $sqlSep . "',ifnull(eg.typeNo,'') ,'" . $sqlSep . "',eg.name,'" . $sqlSep . "',e.tf,'" . $sqlSep . "',eg.startdate,'" . $sqlSep . "',u.uomtype,'" . $sqlSep . "',e.id)) vals,
                        eg.notes egNotes,
                        eg.options egOptions,
                        ce.options ceOptions,
                        e.options eOptions,
                        u.uomoptions uomOptions
        FROM " . E4S_TABLE_EVENTS . ' e, 
             ' . E4S_TABLE_COMPEVENTS . ' ce, 
             ' . E4S_TABLE_EVENTGROUPS . ' eg, 
             ' . E4S_TABLE_UOM . " u 
        WHERE e.id = ce.EventID
        AND   ce.maxgroup = eg.id
        AND   e.uomid = u.id 
        AND   ce.compid = $compid
        ORDER by eg.eventno
    ";

    $result = e4s_queryNoLog($sql);

    $eventGroupRows = $result->fetch_all(MYSQLI_ASSOC);
    $eventGroups = array();

    foreach ($eventGroupRows as $eventGroupRow) {
        $vals = explode($sqlSep, $eventGroupRow['vals']);
        $arr = array();
        $arr['id'] = (int)$vals[$EG_ID];
        $arr['eventNo'] = (int)$vals[$EG_EVENTNO];
        $arr['typeNo'] = $vals[$EG_TYPENO];
//        $arr['manualSeeding'] = e4s_isManualSeedings ( (int)$vals[$EG_ID] );
        $arr['event'] = $vals[$EG_NAME];
        $arr['eventId'] = $vals[$EG_EVENTID];
        $arr['notes'] = $eventGroupRow['egNotes'];
        $arr['type'] = $vals[$EG_TF];
        $arr['date'] = e4s_sql_to_iso($vals[$EG_DATE]);
        $arr['uomType'] = $vals[$EG_UOMTYPE];
        $arr['uomOptions'] = e4s_getOptionsAsObj($eventGroupRow['uomOptions']);
        $arr['eOptions'] = e4s_getOptionsAsObj($eventGroupRow['eOptions']);
        $egOptions = e4s_addDefaultEventGroupOptions($eventGroupRow['egOptions']);

        $ceOptions = e4s_addDefaultCompEventOptions($eventGroupRow['ceOptions']);
        $arr['isTeamEvent'] = $ceOptions->isTeamEvent;
        if ($ceOptions->heatInfo->heatDurationMins > 0 and $egOptions->heatDurationMins === 0) {
            $egOptions->heatDurationMins = $ceOptions->heatInfo->heatDurationMins;
        }
        $arr['options'] = $egOptions;
        $eventGroups[$arr['id']] = $arr;
    }

    $checkInInUse = FALSE;
    outputTxt('"entries" :');
    outputTxt('[');
    $sep = '';
    $athleteArr = array();
    $bibArr = array();
    $multiPBs = array();
    $entryCountObj = e4s_getEntryCountObj($compid);
    $GLOBALS[E4S_ENTRY_COUNT_OBJ] = $entryCountObj;

    $bibsGenerated = FALSE;
    $seedingRows = array();
    $preloadedSeedingRows = array();

    if (sizeof($GLOBALS[E4S_BIB_NOS]) > 0) { // and $compObj->isDayAfterClose()){
        $bibsGenerated = TRUE;
        $seedingRows = getSeedingRows($compid);
        $preloadedSeedingRows = $seedingRows;
    }
    $egAgeInfo = array();

    $entryRows = getIndivEntryRows($compid, E4S_TABLE_ENTRIES);
    if ( $compObj->useArchiveData() ){
        $archiveEntryRows = getIndivEntryRows($compid, E4S_TABLE_ENTRIES_ARCHIVE, true);
        $entryRows = array_merge($entryRows, $archiveEntryRows);
    }
    for($i = 0; $i < sizeof($entryRows); $i++) {
        $row = $entryRows[$i];
	    $athleteId = (int)$row['athleteId'];
	    $egId = (int)$row['maxgroup'];

	    $onWaitingList = $compObj->isAthleteOnWaitingListForId($athleteId, $egId) ;
	    if ($onWaitingList) {
		    unset( $entryRows[ $i ] );
	    }else{
		    $egAgeInfo = setEventGroupAgeInfo($row, $egAgeInfo);
	    }
    }

    foreach ($entryRows as $entryRow) {
	    $athleteId = (int) $entryRow['athleteId'];
	    $egid      = (int) $entryRow['maxgroup'];

	    $hasSeedings = FALSE;
	    if ( sizeof( $preloadedSeedingRows ) > 0 ) {
		    if ( array_key_exists( $egid, $preloadedSeedingRows ) ) {
			    $hasSeedings = TRUE;
		    }
	    }

	    if ( ! array_key_exists( 'ageGroups', $eventGroups[ $egid ] ) ) {
		    $eventGroups[ $egid ]['ageGroups'] = array();
	    }

	    $eventGroups[ $egid ]['ageGroups'][ $entryRow['ageGroupId'] ] = TRUE;
	    outputTxt( $sep );
	    $entryRow['athleteId'] = $athleteId;
	    $checkedIn             = 'false';
	    if ( $entryRow['checkedIn'] === '1' ) {
		    $checkedIn = 'true';
	    }
	    outputTxt( '{' );
	    outputTxt( '"entryId": "' . $entryRow['entryId'] . '",' );
	    outputTxt( '"checkedIn": ' . $checkedIn . ',' );
	    outputTxt( '"compeventid": "' . $entryRow['compeventid'] . '",' );
	    outputTxt( '"teambibno": "' . $entryRow['teamBibNo'] . '",' );
	    outputTxt( '"split": ' . $entryRow['split'] . ',' );
	    outputTxt( '"event": "' . trim( $entryRow['event'] ) . '",' );
	    outputTxt( '"eventGroup": "' . trim( $entryRow['eventGroup'] ) . '",' );
	    outputTxt( '"eventGroupId": "' . trim( $entryRow['eventGroupId'] ) . '",' );
	    $eOptions = e4s_getOptionsAsObj( $entryRow['eOptions'] );
	    outputTxt( '"eoptions": ' . e4s_getOptionsAsString( $eOptions ) . ',' );
	    outputTxt( '"startdate": "' . $entryRow['startdate'] . '",' );
        outputTxt('"paid": ' . $entryRow['paid'] . ',');
        outputTxt('"tf": "' . $entryRow['tf'] . '",');
        outputTxt('"athleteId": "' . $entryRow['athleteId'] . '",');
        if (hasSecureAccess()) {
            outputTxt('"orderid": "' . $entryRow['orderid'] . '",');
            outputTxt('"variationid": "' . $entryRow['variationid'] . '",');
        }
        $club = $entryRow['clubname'];
        $county = $entryRow['clubid'];
        $region = '-';
        if (array_key_exists($entryRow['clubid'], $clubsArr)) {
            $clubRow = $clubsArr[$entryRow['clubid']];
            $club = $clubRow['Clubname'];
            if (array_key_exists($clubRow['areaid'], $areaArr)) {
                $areaRow = $areaArr[$clubRow['areaid']];
                $county = $areaRow['name'];

                if (array_key_exists($areaRow['parentid'], $areaArr)) {
                    $areaRow = $areaArr[$areaRow['parentid']];
                    $region = $areaRow['name'];
                }
            }
        }

        if (sizeof($nonGuests) > 0) {
            $guest = TRUE;
            foreach ($nonGuests as $nonGuestAoCode) {
                if ($entryRow['aoCode'] === $nonGuestAoCode) {
                    $guest = FALSE;
                    break;
                }
            }
            if ($guest) {
                $county = 'Guest';
                $region = '-';
            }
        }
        outputTxt('"clubId": "' . $entryRow['clubid'] . '",');
        outputTxt('"clubname": "' . $club . '",');
        outputTxt('"county": "' . $county . '",');
        outputTxt('"region": "' . $region . '",');

        if (!array_key_exists('eventagegroup', $entryRow)) {
            $athlete['eventagegroup'] = $entryRow['agegroup'];
        }
        $athlete['eventagegroup'] = e4s_getVetDisplay($athlete['eventagegroup'], $entryRow['gender']);

        outputTxt('"agegroup": "' . $entryRow['agegroup'] . '",');
        $present = 'true';
        if ($entryRow['present'] === '0') {
            $present = 'false';
        }
        outputTxt('"present": ' . $present . ',');

        $entryRow['county'] = $county;
        $entryRow['region'] = $region;

        $evoptions = e4s_addDefaultEventOptions($entryRow['evoptions']);
        $egOptions = e4s_addDefaultEventGroupOptions($entryRow['egOptions']);

        // add/update Athlete to array
        e4s_addAthlete($athleteArr, $bibArr, $entryRow, $clubsArr);
        $heatInfo = $evoptions->heatInfo;
        $heatInfo->heatNo = 0;
        $heatInfo->heatNoCheckedIn = 0;
        $heatInfo->laneNo = 0;
        $heatInfo->laneNoCheckedIn = 0;
        $heatInfo->maxInHeat = 0;
        $heatInfo->confirmed = null;

        $pb = formatEntryPB($entryRow);
// Seeding Data Generation
        if ($entryRow['uomtype'] === E4S_UOM_TIME) {
            if (!$bibsGenerated or !$hasSeedings) {
	            $ceOptions = e4s_addDefaultCompEventOptions($entryRow['ceoptions']);
                $seedingRows[$egid][$athleteId] = e4s_getHeatLaneInfo($heatInfo, $entryRow, $compid, $heatOrder, $coptions, $egOptions, $entryRows, $egAgeInfo, $ceOptions);
            } else {
                if (!array_key_exists($egid, $seedingRows)) {
                    $seedingRows[$egid] = array();
                }
                if (array_key_exists($athleteId, $seedingRows[$egid])) {
                    $heatInfo->laneNo = $seedingRows[$egid][$athleteId]->laneNo;
                    $heatInfo->heatNo = $seedingRows[$egid][$athleteId]->heatNo;
                    $heatInfo->laneNoCheckedIn = $seedingRows[$egid][$athleteId]->laneNoCheckedIn;
                    $heatInfo->heatNoCheckedIn = $seedingRows[$egid][$athleteId]->heatNoCheckedIn;
                    $heatInfo->confirmed = FALSE;
                    if (isset($seedingRows[$egid][$athleteId]->confirmed)) {
                        $heatInfo->confirmed = $seedingRows[$egid][$athleteId]->confirmed;
                    }
                } else {
                    $seedRow = new stdClass();
                    $seedRow->eventGroupId = $egid;
                    $seedRow->athleteId = $athleteId;
                    $seedRow->laneNo = 0;
                    $seedRow->heatNo = 0;
                    $seedRow->laneNoCheckedIn = 0;
                    $seedRow->heatNoCheckedIn = 0;
                    $seedRow->confirmed = null;
                    $seedingRows[$egid][$athleteId] = $seedRow;
                    // create a blank seeding row for this athlete
                    $seedv2Obj = new seedingV2Class($compid, $egid);
                    $seedv2Obj->insertNonSeededAthletes([$athleteId]);
                }
            }
        } else if ($entryRow['tf'] === E4S_EVENT_FIELD or $entryRow['tf'] === E4S_EVENT_MULTI) {
            e4s_getFieldSortInfo($heatInfo, $entryRow, $compid, $heatOrder, $coptions, $egOptions, $egAgeInfo);
        }

        outputTxt('"heatInfo": ' . e4s_getOptionsAsString($heatInfo) . ',');
//        if ( $heatInfo->heatNoCheckedIn > 0 ){
        if ((int)$entryRow['checkedIn'] === 1) {
            $checkInInUse = TRUE;
        }

        outputTxt('"pb": "' . (string)$pb . '",');
        if (hasSecureAccess()) {
            outputTxt('"dob": "' . $entryRow['dob'] . '",');
        }
        outputTxt('"gender": "' . $entryRow['gender'] . '",');

        $eventName = $entryRow['event'];
        $eventno = 0;

        if (array_key_exists($egid, $eventGroups)) {
            $eventName = $eventGroups[$egid]['event'];
            $eventno = $eventGroups[$egid]['eventNo'];
        }

        $useFee = 0;
        if ($entryRow['baseprice'] === $entryRow['price']) {
            if ($entryRow['fee'] !== null and (float)$entryRow['price'] > (float)$entryRow['fee']) {
                $useFee = $entryRow['fee'];
            }
        } else {
            if ($entryRow['salefee'] !== null and (float)$entryRow['price'] > (float)$entryRow['salefee']) {
                $useFee = $entryRow['salefee'];
            }
        }
        outputTxt('"entryPosition": "' . $entryRow['entryPosition'] . '",');
        if (hasSecureAccess()) {
            outputTxt('"fee": "' . $useFee . '",');
            outputTxt('"price": "' . $entryRow['price'] . '",');
            outputTxt('"coupon": "' . $entryRow['coupon'] . '",');
            outputTxt('"value": "' . $entryRow['value'] . '",');
            outputTxt('"email": "' . $entryRow['email'] . '",');
            outputTxt('"phone": "' . $entryRow['phone'] . '",');
            outputTxt('"created": "' . $entryRow['created'] . '",');
        }
// maxgroup holds the eventgroup id but put out the event name
        outputTxt('"eventno": "' . $eventno . '",');
        outputTxt('"eventGroup": "' . trim($eventName) . '"');
        outputTxt('}');
        $sep = ',';

    }

    outputTxt('],');
    if (sizeof($preloadedSeedingRows) !== sizeof($seedingRows)) {
        e4s_writeAllSeedings($compid, $seedingRows);
    }
    outputTxt('"seedings": ' . e4s_getOptionsAsString($seedingRows) . ',');
    if ($checkInInUse) {
        $checkInInUse = 'true';
    } else {
        $checkInInUse = 'false';
    }

    outputTxt('"checkInInUse" :' . $checkInInUse . ',');
    // compEvents
    outputTxt('"compEvents" :');
    outputTxt('[');
    $sep = '';
    $compEvents = $compObj->getCeObjs();
    foreach ($compEvents as $ceObj) {
        outputTxt($sep);
        outputTxt('{');
        outputTxt('"id": ' . $ceObj->id . ',');
        outputTxt('"eventId": ' . $ceObj->eventId . ',');
        outputTxt('"ageGroupId": ' . $ceObj->ageGroupId . ',');
        outputTxt('"egId": ' . $ceObj->egId . ',');
        outputTxt('"split": ' . $ceObj->split );
        outputTxt('}');
        $sep = ',';
    }

    outputTxt(']');
    outputTxt(',');
    // eventGroups
    outputTxt('"eventgroups" :');
    outputTxt('[');
    $sep = '';

    foreach ($eventGroups as $eventGroupArr) {
	    if (isReserveEvent($eventGroupArr['event'] ) ){
		    continue;
	    }
        $egOptions = $eventGroupArr['options'];
        $ageGroups = array();
        if (array_key_exists('ageGroups', $eventGroupArr)) {
            $ageGroups = $eventGroupArr['ageGroups'];
        }
        outputTxt($sep);
        outputTxt('{');
        outputTxt('"id": ' . $eventGroupArr['id'] . ',');
        outputTxt('"eventno": ' . $eventGroupArr['eventNo'] . ',');
        if ($eventGroupArr['isTeamEvent']) {
            outputTxt('"isTeamEvent": true,');
        } else {
            outputTxt('"isTeamEvent": false,');
        }
        outputTxt('"typeno": "' . $eventGroupArr['typeNo'] . '",');
//        outputTxt('"manualSeeding": ' . ($eventGroupArr['manualSeeding'] ? "true" : "false") . ',');
        outputTxt('"type": "' . $eventGroupArr['type'] . '",');
        outputTxt('"date": "' . $eventGroupArr['date'] . '",');
        outputTxt('"event": "' . $eventGroupArr['event'] . '",');
        outputTxt('"eventId": "' . $eventGroupArr['eventId'] . '",');
        outputTxt('"notes": "' . $eventGroupArr['notes'] . '",');
        outputTxt('"options": ' . e4s_getOptionsAsString($egOptions) . ',');
        outputTxt('"uomtype": "' . $eventGroupArr['uomType'] . '",');
        outputTxt('"uomOptions": ' . e4s_getOptionsAsString($eventGroupArr['uomOptions']) . ',');
        outputTxt('"eOptions": ' . e4s_getOptionsAsString($eventGroupArr['eOptions']) . ',');
        outputTxt('"agegroups":' . e4s_getOptionsAsString($ageGroups));
        outputTxt('}');
        $sep = ',';
    }
    outputTxt(']');
    outputTxt(',');

// League Teams
    $sql = 'SELECT ete.*, e.name event, ce.maxGroup maxgroup, ce.options ceoptions, ag.name agegroup
        FROM ' . E4S_TABLE_EVENTTEAMENTRIES . ' ete,
             ' . E4S_TABLE_COMPEVENTS . ' ce,
             ' . E4S_TABLE_EVENTS . ' e,
             ' . E4S_TABLE_AGEGROUPS . " ag
        where ce.options like '%\"formType\":\"LEAGUE\"%'
        and ce.compid = " . $compid . '
        and ce.id = ete.ceid
        and ce.agegroupid = ag.id
        and ce.eventid = e.id
        and paid = ' . E4S_ENTRY_PAID . '
        order by ceid, name';
    $result = e4s_queryNoLog('LeagueTEAMS' . E4S_SQL_DELIM . $sql);
    $leagues = $result->fetch_all(MYSQLI_ASSOC);
    outputTxt('"leagueTeams" :');
    outputTxt('[');
    $sep = '';
    $compDate = $compRow['compDate'];
    $leagueBibNo = 1;
    $formDefRows = array();
    foreach ($leagues as $league) {
        outputTxt($sep);
        $sep = ',';
        outputTxt('{');
        outputTxt('"teamid": ' . $league['id'] . ',');
        outputTxt('"teambibno": "' . $leagueBibNo . '",');
        $leagueBibNo = $leagueBibNo + 1;
        outputTxt('"teamname": "' . $league['name'] . '",');
        outputTxt('"group": "' . $league['maxgroup'] . '",');
        outputTxt('"created": "' . $league['created'] . '",');
        $entityName = '';

        if ((int)$league['entitylevel'] === E4S_CLUB_ENTITY) {
            if (array_key_exists($league['entityid'], $clubsArr)) {
                $clubRow = $clubsArr[$league['entityid']];
                $entityName = $clubRow['Clubname'];
            }
        }
        if ((int)$league['entitylevel'] === E4S_COUNTY_ENTITY or (int)$league['entitylevel'] === E4S_REGION_ENTITY) {
            if (array_key_exists($league['entityid'], $areaArr)) {
                $areaRow = $areaArr[$league['entityid']];
                $entityName = $areaRow['name'];
            }
        }

        $ceoptions = e4s_getOptionsAsObj($league['ceoptions']);
        $useFormNo = 0;
        if (isset($ceoptions->eventTeam)) {
            if (isset($ceoptions->eventTeam->showForm)) {
                $useFormNo = (int)$ceoptions->eventTeam->showForm;
            }
        }
        if ($useFormNo === 0) {
            Entry4UIError(9123, 'Failed to get League show form', 404);
        }

        if (!array_key_exists($useFormNo, $formDefRows)) {
            $defSql = 'select *
                   from ' . E4S_TABLE_EVENTTEAMFORMDEF . '
                   where formno = ' . $useFormNo . '
                   order by pos
                   ';
            $defResult = e4s_queryNoLog($defSql);
            if ($defResult->num_rows === 0) {
                Entry4UIError(9124, 'Failed to get League form definitions (' . $useFormNo . ')', 404);
            }
            $entryRows = $defResult->fetch_all(MYSQLI_ASSOC);
            $useRows = array();
            // currently, just show those for the compdate
            foreach ($entryRows as $row) {
                $addRow = TRUE;
                $defOptions = e4s_getOptionsAsObj($row['options']);
                if (isset($defOptions->datetime)) {
                    $defDate = e4s_dateOnly($defOptions->datetime);
                    if ($defDate !== $compDate) {
                        $addRow = FALSE;
                    }
                }
                if ($addRow) {
                    $useRows[] = $row;
                }
            }
            $formDefRows[$useFormNo] = $useRows;
        }
        $useDefRows = $formDefRows[$useFormNo];
        $athletesql = 'select a.*,a.id athleteId, eta.pos pos
                   from ' . E4S_TABLE_EVENTTEAMATHLETE . ' eta,
                        ' . E4S_TABLE_ATHLETE . ' a
                   where eta.athleteId = a.id
                   and   eta.athleteId != 0
                   and   eta.teamentryid = ' . $league['id'] . '
                   order by eta.pos
                   ';
        $athleteresult = e4s_queryNoLog($athletesql);
        $athleteRows = array();
        $athletes = $athleteresult->fetch_all(MYSQLI_ASSOC);
        foreach ($athletes as $athlete) {
            $county = $athlete['clubid'];
            $region = '-';
            if (array_key_exists($athlete['clubid'], $clubsArr)) {
                $clubRow = $clubsArr[$athlete['clubid']];

                if (array_key_exists($clubRow['areaid'], $areaArr)) {
                    $areaRow = $areaArr[$clubRow['areaid']];
                    $county = $areaRow['name'];

                    if (array_key_exists($areaRow['parentid'], $areaArr)) {
                        $areaRow = $areaArr[$areaRow['parentid']];
                        $region = $areaRow['name'];
                    }
                }
            }

            // add the team created date for potential sorting
            $athlete['created'] = $league['created'];
            // add other data required for the athletes tab
            // This should be the athletes age group NOT the event I think
            $athlete['agegroup'] = $league['agegroup'];
            $athlete['maxgroup'] = $league['maxgroup'];
            $athlete['county'] = $county;
            $athlete['region'] = $region;
            $athlete['event'] = $league['event'];
            $athlete['pb'] = 0;
            // add/update Athlete to array
            e4s_addAthlete($athleteArr, $bibArr, $athlete, $clubsArr);

            $athleteRows[$athlete['pos']] = $athlete;
        }
        outputTxt('"events": [');
        $eventSep = '';
        foreach ($useDefRows as $useDefRow) {
            $useAthleteRow = array();
            if (array_key_exists($useDefRow['pos'], $athleteRows)) {
                $useAthleteRow = $athleteRows[$useDefRow['pos']];
            } else {
                $useAthleteRow['athleteId'] = 0;
            }
            outputTxt($eventSep);
            outputTxt('{');
            outputTxt('"eventName": "' . $useDefRow['label'] . '",');
            outputTxt('"athleteId": "' . $useAthleteRow['athleteId'] . '"');
            outputTxt('}');
            $eventSep = ',';
        }
        outputTxt(']}');
    }
    outputTxt('],');

//  Teams
    $sql = 'select  ete.*
                    ,u.display_name username
                    ,u.user_email useremail
                    ,e.Name event
                    ,e.gender gender
                    ,ag.name agegroup
                    ,ce.maxgroup maxgroup
                    ,ce.options ceoptions
                    ,heatNo
                    ,laneNo
                    ,heatNoCheckedin
                    ,laneNoCheckedin
        from ' . E4S_TABLE_EVENTTEAMENTRIES . ' ete join ' . E4S_TABLE_COMPEVENTS . ' ce on (ete.ceid = ce.id and ce.compid = ' . $compid . ') left outer join ' . E4S_TABLE_SEEDING . ' s on (ete.id = s.athleteid  and ce.maxGroup = s.eventgroupid),             
             ' . E4S_TABLE_AGEGROUPS . ' ag,
             ' . E4S_TABLE_EVENTS . ' e,
             ' . E4S_TABLE_USERS . ' u
        where ce.eventid = e.id
        and ce.agegroupid = ag.id
        and ete.paid in (' . E4S_ENTRY_PAID . ',' . E4S_ENTRY_QUALIFY . ')
        and u.id = ete.userid
        order by ete.name
       ';

    $result = e4s_queryNoLog('TEAMS' . E4S_SQL_DELIM . $sql);
    $teams = $result->fetch_all(MYSQLI_ASSOC);
    outputTxt('"teams" :');
    outputTxt('[');
    $sep = '';
    $forms = array();
    foreach ($teams as $team) {
        $teamCEOptions = e4s_getOptionsAsObj($team['ceoptions']);
        $showForm = 0;
        if (isset($teamCEOptions->eventTeam)) {
            $eventTeam = $teamCEOptions->eventTeam;
            if (isset($eventTeam->showForm)) {
                $showForm = $eventTeam->showForm;
                if ($showForm > 0) {
                    if (!array_key_exists($showForm, $forms)) {
                        $sql = 'select pos,
                                       label
                                from ' . E4S_TABLE_EVENTTEAMFORMDEF . '
                                where formno = ' . $showForm;
                        $defResult = e4s_queryNoLog($sql);
                        if ($defResult->num_rows > 0) {
                            $formDefArr = array();
                            while ($defObj = $defResult->fetch_object()) {
                                $formDefArr[$defObj->pos] = $defObj->label;
                            }
                            $forms[$showForm] = $formDefArr;
                        }
                    }
                }
            }
        }
        $athleteEventCol = $team['event'];
        if (isset($teamCEOptions->athleteEventCol)) {
            $athleteEventCol = $teamCEOptions->athleteEventCol;
        }
        outputTxt($sep);
        outputTxt('{');
        outputTxt('"teamid": ' . $team['id'] . ',');
        outputTxt('"teamname": "' . $team['name'] . '",');
        $teamBibNo = e4s_getTeamBib($compObj, $team);
        outputTxt('"bibNo": "' . $teamBibNo . '",');
        if (hasSecureAccess()) {
            outputTxt('"order": ' . $team['orderid'] . ',');
            outputTxt('"price": ' . $team['price'] . ',');
            outputTxt('"user": "' . $team['username'] . '",');
            outputTxt('"email": "' . $team['useremail'] . '",');
        }
        outputTxt('"event": "' . $team['event'] . '",');
        outputTxt('"athleteEvent": "' . $athleteEventCol . '",');
        outputTxt('"agegroup": "' . $team['agegroup'] . '",');
        outputTxt('"present": "' . $team['present'] . '",');
        outputTxt('"gender": "' . $team['gender'] . '",');
//        outputTxt('"groupx": "' . $team['maxgroup'] . '",');
        outputTxt('"eventGroupId": "' . $team['maxgroup'] . '",');
        outputTxt('"created": "' . $team['created'] . '",');

        outputTxt('"options": ' . $team['options'] . ',');

        // Team heatInfo
        $heatInfo = new stdClass();
        $heatInfo->heatNo = 0;
        $heatInfo->heatNoCheckedIn = 0;
        $heatInfo->laneNo = 0;
        $heatInfo->laneNoCheckedIn = 0;
        $heatInfo->maxInHeat = 8;
        $heatInfo->confirmed = null;
        $heatInfo->useLanes = 'A';

        if ( !is_null($team['heatNo']) ){
            $heatInfo->heatNo = (int)$team['heatNo'];
        }
        if ( !is_null($team['heatNoCheckedin']) ){
            $heatInfo->heatNoCheckedIn = (int)$team['heatNoCheckedin'];
        }
        if ( !is_null($team['laneNo']) ){
            $heatInfo->laneNo = (int)$team['laneNo'];
        }
        if ( !is_null($team['laneNoCheckedin']) ){
            $heatInfo->laneNoCheckedIn = (int)$team['laneNoCheckedin'];
        }
	    outputTxt('"heatInfo": ' . e4s_getOptionsAsString($heatInfo) . ',');
        outputTxt('"athletes": [');
        $athletesql = 'select a.*, a.id athleteId, eta.pos
                   from ' . E4S_TABLE_EVENTTEAMATHLETE . ' eta,
                        ' . E4S_TABLE_ATHLETE . ' a
                   where eta.athleteId = a.id
                   and   eta.athleteId != 0
                   and eta.teamentryid = ' . $team['id'] . '
                   order by eta.pos
                   ';
//    echo $athletesql . "<br>";
        $athleteresults = e4s_queryNoLog('TeamAthletes' . E4S_SQL_DELIM . $athletesql);
        $athletes = $athleteresults->fetch_all(MYSQLI_ASSOC);
        $athletesep = '';
        $sep = '';
        $processedAthletes = array();
        foreach ($athletes as $athlete) {
            $athlete['athleteId'] = (int)$athlete['athleteId'];
            $athlete['id'] = (int)$athlete['id'];
            $athleteNotes = '';
            if ($showForm > 0) {
                $useForm = $forms[$showForm];
                if (array_key_exists((int)$athlete['pos'], $useForm)) {
                    $athleteNotes = $useForm[(int)$athlete['pos']];
                }
            }

            if (!array_key_exists($athlete['athleteId'], $processedAthletes)) {
                $processedAthletes[$athlete['athleteId']] = true;

                outputTxt($athletesep);
                outputTxt('{');

                $county = $athlete['clubid'];
                $region = '-';
                if (array_key_exists($athlete['clubid'], $clubsArr)) {
                    $clubRow = $clubsArr[$athlete['clubid']];

                    if (array_key_exists($clubRow['areaid'], $areaArr)) {
                        $areaRow = $areaArr[$clubRow['areaid']];
                        $county = $areaRow['name'];

                        if (array_key_exists($areaRow['parentid'], $areaArr)) {
                            $areaRow = $areaArr[$areaRow['parentid']];
                            $region = $areaRow['name'];
                        }
                    }
                }

                // add the team created date for potential sorting
                $athlete['created'] = $team['created'];
                // add other data required for the athletes tab
                // this should be the athletes age group NOT the event

                $ag = getAgeGroupInfo($allCompDOBs, $athlete['dob']);
                if (isset($ag['vetAgeGroup'])) {
                    $useAgeGroup = e4s_getVetDisplay($ag['vetAgeGroup']['Name'], $athlete['gender']);
                } else {
                    $useAgeGroup = $ag['ageGroup']['Name'];
                }
                $athlete['agegroup'] = $useAgeGroup;

                $athlete['maxgroup'] = $team['maxgroup'];
                $athlete['county'] = $county;
                $athlete['region'] = $region;
                $athlete['eventagegroup'] = $team['agegroup'];
                $athlete['event'] = $team['event'];
                $athlete['teamName'] = $team['name'];
                $athlete['pb'] = 0;
                $athlete['eventagegroup'] = e4s_getVetDisplay($athlete['eventagegroup'], $athlete['gender']);

                e4s_addAthlete($athleteArr, $bibArr, $athlete, $clubsArr);

                outputTxt('"athleteId": "' . $athlete['athleteId'] . '",');
                outputTxt('"pos": "' . $athlete['pos'] . '",');
                outputTxt('"notes": "' . $athleteNotes . '"');
                outputTxt('}');
                $athletesep = ',';
            } else {
                // Already added so check the athlete event group
                $id = (int)$athlete['athleteId'];
                $athlete = $athleteArr[$id];
                if (array_key_exists('eventagegroup', $athlete)) {
                    if (strpos($athlete['eventagegroup'], $team['agegroup']) === FALSE) {
                        if ($athlete['eventagegroup'] === '') {
                            $athlete['eventagegroup'] = $team['agegroup'];
                        } else {
                            $athlete['eventagegroup'] .= '/' . $team['agegroup'];
                        }
                    }
                } else {
                    $athlete['eventagegroup'] = $team['agegroup'];
                }
                $athlete['eventagegroup'] = e4s_getVetDisplay($athlete['eventagegroup'], $athlete['gender']);
            }
        }
        outputTxt(']');
        outputTxt('}');
        $sep = ',';
    }
    outputTxt('],');

    outputTxt('"clubCompClubs":{');
    $sep = '';
    if (!is_null($compObj->clubComp)){
        $clubs = $compObj->clubComp->clubs;
        foreach($clubs as $club){

            outputTxt($sep);
            outputTxt(' "' . $club->clubName . '":{');
            outputTxt('  "bibNos":["' .  implode('","', $club->bibNos) . '"]');
            outputTxt('}');

            $sep = ',';
        }
    }

    outputTxt('},');

// Add Bib Arr
    outputTxt('"bibNos":{');
    $sep = '';
    $bibArr = $GLOBALS[E4S_BIB_NOS];

    foreach ($bibArr as $bibObj) {
        outputTxt($sep);
        outputTxt('  "' . $bibObj->bibNo . '":{');
        outputTxt('  "athleteId":' . $bibObj->athleteId . ',');
        $collected = 'false';
        if ( $bibObj->collected ){
            $collected = 'true';
        }
        outputTxt('  "collected":' . $collected . ',');
        $notes = '';
        if (!is_null($bibObj->notes)) {
            $notes = $bibObj->notes;
        }
        outputTxt('  "notes":"' . $notes . '",');
        $notes = '';
        if (!is_null($bibObj->compNotes)) {
            $notes = $bibObj->compNotes;
        }
        outputTxt('  "compNotes":"' . $notes . '"');
        outputTxt('}');
        $sep = ',';
    }
    outputTxt('},');

    outputTxt('"competition":{');
    outputTxt('  "id":"' . $compRow['compId'] . '",');
    outputTxt('  "name":"' . str_replace('"', '\"', $compRow['compName']) . '",');
    outputTxt('  "club":"' . $compRow['club'] . '",');
    outputTxt('  "location":"' . $compRow['location']->name . '",');
    outputTxt('  "date":"' . $compRow['compDate'] . '",');
    outputTxt('  "daysToComp":"' . $compRow['daysToComp'] . '",');
    outputTxt('  "entriesOpen":"' . $compRow['opendate'] . '",');
    outputTxt('  "entriesclose":"' . $compRow['closedate'] . '",');
    outputTxt('  "daysToClose":"' . $compRow['daysToClose'] . '",');

    $coptions = json_encode($coptions, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
    outputTxt('  "options":' . $coptions . ',');
    $standards = json_encode($compObj->getStandards(E4S_STANDARD_ORDER_COMBINED),JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
    outputTxt('  "standards":' . $standards );
    outputTxt('},');

// Results
    outputTxt('"results":' . json_encode(e4s_getResultsForComp($compRow['compId'])));
    outputTxt(',');

// Rankings
    outputTxt('"rankings":' . json_encode(getRankings($athleteArr)));
    outputTxt(',');

// Athletes
    outputTxt('"athletes":' . json_encode(getAthleteObjForOutput($athleteArr, $compObj)));
    outputTxt('}');

    return $GLOBALS[E4S_OUTPUT_DATA];
}
function getIndivEntryRows($compId, $tableName, $archive = false){
	if ( $archive ) {
		$tableName = e4sArchive::archiveName() . "." . $tableName;
	}
	$sql = '
        select ce.CompID AS compid,
            eg.startdate AS startdate,
            ce.split split,
            ce.options ceoptions,
            ce.maxGroup AS maxgroup,
            ce.ageGroupId ageGroupId,  
            ag.minage minAge,   
            e.eventAgeGroup AS agegroup,
            e.id AS entryId,
            e.teambibno teamBibNo,
            e.created AS created,
            e.compEventID AS compeventid,
            e.orderid AS orderid,
            e.variationID AS variationid,
            e.clubid AS clubid,
            cl.Clubname AS clubname,
            e.price AS price,
            e.present as present,
            e.paid,
            e.checkedin checkedIn,
            e.entrypos entryPosition,
            ev.Name AS event,
            eg.Name AS eventGroup,
            eg.id as eventGroupId,
            eg.options egOptions,   
            ev.tf AS tf,
            ev.options as evoptions,
            e.athleteId AS athleteId,
            a.gender AS gender,
            a.firstName AS firstName,
            a.surName AS surName,
            a.dob AS dob,
            a.URN AS URN,
            a.aoCode,
            a.classification AS classification,
            e.pb AS pb,
            e.options eOptions,
            ep.price as baseprice,
            ep.fee AS fee ,
            ep.salefee AS salefee ,
            c.coupon, 
            c.value, 
            p.post_excerpt as notes, 
            pm.meta_value as email,
            pm2.meta_value as phone,
            u.uomtype,
            u.uomOptions
        from ' . $tableName . ' e 
        left join ' . E4S_TABLE_ATHLETE . ' a on a.id = e.athleteId 
        left join ' . E4S_TABLE_COMPEVENTS . ' ce on ce.ID = e.compEventID
        left join ' . E4S_TABLE_EVENTGROUPS . ' eg on ce.maxgroup = eg.id
        left join ' . E4S_TABLE_EVENTS . ' ev on ce.EventID = ev.ID
        left join ' . E4S_TABLE_ATHLETEPB . ' pb on ev.id = pb.eventid and pb.athleteId = a.id
        left join ' . E4S_TABLE_UOM . ' u on u.id = ev.uomid
        left join ' . E4S_TABLE_EVENTPRICE . ' ep on ep.ID = ce.PriceID
        left join ' . E4S_TABLE_POSTMETA . " as pm on e.orderid = pm.post_id and pm.meta_key = '_billing_email'
        left join " . E4S_TABLE_POSTMETA . " as pm2 on e.orderid = pm2.post_id and pm2.meta_key = '_billing_phone'
        left join " . E4S_TABLE_POSTS . ' as p on p.id = e.orderid
        left join ' . E4S_TABLE_COUPONS . ' as c on e.orderid = c.orderid
        left join ' . E4S_TABLE_CLUBS . ' as cl on e.clubid = cl.id
        left join ' . E4S_TABLE_AGEGROUPS . ' as ag on ag.id = ce.agegroupid
        where e.paid in (' . E4S_ENTRY_PAID . ',' . E4S_ENTRY_QUALIFY . ')
        and e.athleteId > 0
        and ce.compid = ' . $compId;
//        and ce.maxathletes > -1";

	$sql .= ' order by cast(ce.maxgroup as signed), cl.clubname desc, e.athleteId asc, e.created desc';

	$result = e4s_queryNoLog($sql);
	return $result->fetch_all(MYSQLI_ASSOC);
}
function e4s_getTeamBib($compObj, $team){

    $teamBibNo = '';
    if (array_key_exists('bibNo', $team)){
        $teamBibNo = $team['bibNo'];
    }
    if ( $teamBibNo === '' or is_null($teamBibNo) ){
        $teamBibNo = $team['id'];
	    if (!is_null($compObj->clubComp)){
		    $teamName = $team['name'];
		    $clubs = $compObj->clubComp->clubs;
		    foreach($clubs as $club){
			    if ( $club->clubName === $teamName ) {
				    $teamBibNo = $club->bibNos[0];
				    updateTeamBibNo($team['id'], $teamBibNo );
                    break;
			    }
		    }
	    }
    }
    return $teamBibNo;
}
function updateTeamBibNo($id, $bibNo ){
    $sql = 'update ' . E4S_TABLE_EVENTTEAMENTRIES . ' set bibNo = "' . $bibNo . '" where id = ' . $id;
    e4s_queryNoLog($sql);
}
function getRankings($athleteArr):array{
    $rankings = array();

    $sql="
        select *
        from " . E4S_TABLE_ATHLETERANK . "
        where athleteId in (" . implode(',', [1,2,166969]) . ")
        and year = " . date('Y');
    $result = e4s_queryNoLog($sql);
    while($obj = $result->fetch_object(E4S_ATHLETERANK_OBJ)){
        if ( !array_key_exists($obj->athleteId, $rankings) ) {
            $useObj = new stdClass();
            $useObj->eventId = $obj->eventId;
            $useObj->rank = $obj->rank;
            $useObj->info = $obj->info;
	        $rankings[ $obj->athleteId ] = [];
        }
        $rankings[$obj->athleteId][] = $useObj;
    }
    return $rankings;
}
function formatEntryPB($entryRow): string {
    $pb = '' . $entryRow['pb'];
    if ($pb === '' or $pb === '0' or is_null($entryRow['pb'])) {
        $pb = '0.00';
    }
    $uomOptions = e4s_getOptionsAsObj($entryRow['uomOptions']);
    $uomPattern = $uomOptions[0]->pattern;
    $pbs = explode('.', $pb);
    // $pb will be xxx.xx ( height or in seconds )
    if ($entryRow['uomtype'] === E4S_UOM_TIME) {
        $pb_100secs = '0';
        if (sizeof($pbs) > 1) {
            $pb_100secs = $pbs[1];
            if (strlen($pb_100secs) === 1) {
                $pb_100secs .= '0';
            }
        }
        $pb_100secs = (int)$pb_100secs;
        $pb_mins = 0;
        $pb_secs = (int)$pbs[0];
        if ((float)$pb > 60) {
            $pbTotalSecs = $pb_secs;

            $pb1 = (string)($pbTotalSecs / 60);
            $pb2 = explode('.', $pb1);
            $pb_mins = (int)$pb2[0];
            $pb_secs = $pbTotalSecs - ($pb_mins * 60);
        }
        $pb = formatPBOnUOM($uomPattern, $pb_mins, $pb_secs, $pb_100secs);
    } else {
        $pb_cm = '00';
        if (sizeof($pbs) > 1) {
            $pb_cm = $pbs[1];
            if (strlen($pb_cm) === 1) {
                $pb_cm .= '0';
            }
        }
        $pb = $pbs[0] . '.' . $pb_cm;
    }
    return '#' . $pb;
}

function formatPBOnUOM($pattern, $pb_mins, $pb_secs, $pb_100secs) {
    $pb = '';
//    $patternArr = explode(".", $pattern);
    if (strpos($pattern, 'm') === FALSE) {
        // minutes are Not required, so if there are mins, put back into seconds
        if ($pb_mins > 0) {
            $pb_secs += ($pb_mins * 60);
        }
    } else {
        if (strpos($pattern, 'mm') !== FALSE) {
            if ($pb_mins < 10) {
                $pb = '0';
            }
        }
        $pb .= $pb_mins . ':';
    }
    if (strpos($pattern, 's') !== FALSE) {
        if (strpos($pattern, 'ss') !== FALSE) {
            if ($pb_secs < 10) {
                $pb .= '0';
            }
        }
        $pb .= $pb_secs;
    }
    if (strpos($pattern, 'S') !== FALSE) {
        $pb .= '.';
        if (strpos($pattern, 'SS') !== FALSE) {
            if ($pb_100secs < 10) {
                $pb .= '0';
            }
            $pb .= $pb_100secs;
        }
    }
    return $pb;
}

function getSeedingRows($compid) {
    $seedingObj = new seedingV2Class($compid, 0);
    return $seedingObj->getAllSeedRecords();
}

// only really for track
function e4s_writeAllSeedings($compid, $seedingRows) {
    if (sizeof($seedingRows) === 0) {
        return;
    }
    $seedingObj = new seedingV2Class($compid, 0);
    $seedingObj->clearAndWriteSeedings($seedingRows);
}

/*
   Holds the min age for each agegroup in the eventGroup
*/
function setEventGroupAgeInfo($row, $egAgeInfo) {
    $egId = (int)$row['eventGroupId'];
    $agId = (int)$row['ageGroupId'];
    $minAge = (int)$row['minAge'];

    if (!array_key_exists($egId, $egAgeInfo)) {
        $egAgeInfo[$egId] = new stdClass();
        $egAgeInfo[$egId]->ageOnly = array();
    }
    $egAgeInfo[$egId]->ageOnly[$agId] = $minAge;

    return $egAgeInfo;
}

function getAthleteObjForOutput($athleteArr, e4sCompetition $compObj) {
    $newArr = array();
    $isSecure = hasSecureAccess();
    if ($isSecure) {
        return $athleteArr;
    }

    foreach ($athleteArr as $athlete) {
        $newAthlete = $athlete;
        if ( $compObj->anonymizeAthletes() ) {
            $newAthlete['firstname'] = 'Athlete';
            $newAthlete['surname'] = $athlete['id'];
        }
        unset($newAthlete['dob']);
        unset($newAthlete['sortkey']);
        unset($newAthlete['urn']);
        unset($newAthlete['clubid']);
        unset($newAthlete['created']);
        $newArr[$athlete['id']] = $newAthlete;
    }

    return $newArr;
}

function outputTxt($txt) {
    $GLOBALS[E4S_OUTPUT_DATA] .= $txt;
}

function e4s_getSortingSQL($egId, $heatOrder, $useCheckedIn, $isTime = TRUE, $seedGender = '', $seedAgeGroupIds = 0, $orderByGender = FALSE, $orderByAgeId = FALSE) {
    $sql = 'Select      
                s.laneNo laneNo,
                s.heatNo heatNo,
                s.laneNocheckedin laneNoCheckedIn,
                s.heatNocheckedin heatNoCheckedIn,
                s.confirmed confirmed,
                e.athleteId,
                a.dob,
                e.checkedIn checkedIn,
                ag.minage minAge,
                e.gender gender,
                if(e.entryPb=0,9999,e.entryPb) as eventpb,
                IFNULL(if(pb.pb=0,9999,pb.pb),9999) as pb,
                e.eventid eventId,
                e.ceOptions ceoptions,
                e.eOptions entryOptions,
                c.options coptions,
                e.egoptions egoptions
                from ' . E4S_TABLE_ENTRYINFO . ' e 
                    left join ' . E4S_TABLE_SEEDING . ' s on e.egId = s.eventgroupid and s.athleteId = e.athleteId 
                    left join ' . E4S_TABLE_ATHLETEPB . ' pb on e.EventID = pb.eventid and pb.athleteId = e.athleteId 
                    left join ' . E4S_TABLE_AGEGROUPS . ' ag on e.ageGroupId = ag.id
                    left join ' . E4S_TABLE_BIBNO . ' b on b.compid = e.compId and b.athleteId = e.athleteId,
                     ' . E4S_TABLE_COMPETITON . ' c,
                     ' . E4S_TABLE_ATHLETE . ' a
                where paid in (' . E4S_ENTRY_PAID . ',' . E4S_ENTRY_QUALIFY . ")
                and e.athleteId = a.id
                and c.id = e.compid
                and e.egId = {$egId}";
    if ($seedGender !== '') {
        $sql .= "       and e.gender = '" . $seedGender . "'";
    }
    if ( $seedAgeGroupIds !== 0) {
        $sql .= '       and e.agegroupid in( ' . $seedAgeGroupIds . ')';
    }

    if ($useCheckedIn) {
        $sql .= ' and e.checkedin = 1 and b.collected = 1 ';
    }
    $sql .= ' order by e.egid, ';
    if ($orderByGender) {
        $sql .= ' e.gender,a.dob desc,';
    }
    if ($orderByAgeId) {
        $sql .= ' a.dob desc,e.gender,';
    }
    $sql .= ' if(e.entrypb = 0, 9999, e.entrypb)';

    if ($heatOrder === E4S_SLOWEST_FIRST and $isTime) {
        $sql .= ' desc';
    }
    if ($heatOrder === E4S_FASTEST_FIRST and !$isTime) {
        $sql .= ' desc';
    }
    $sql .= ', athleteId, e.gender';
    return $sql;
}

function e4s_getFieldSortInfoWithCheckin(&$heatInfo, $row, $compId, $useCheckin, $heatOrder, $egOptions) {
    $athleteId = (int)$row['athleteId'];
    $egId = (int)$row['maxgroup'];

    $cacheGender = '';
    $cacheAgeGroup = '';
    // useLanes
    $obj = new stdClass();
    $obj->heatNo = 1;
    $obj->laneNo = 0;
    $obj->maxInHeat = 99; // TODO This will need to be changed when heats in field dev'd
    $obj->laneCount = E4S_FIELD_CARD_COUNT;
    $obj->useLanes = E4S_ALL_LANES;

    $egId = (int)$egId;
    $cacheKey = 'EventCount_' . $egId . $cacheGender . $cacheAgeGroup . '_' . $useCheckin;
    $orderByGender = FALSE;
    $orderByAge = FALSE;

    if (isset($egOptions->seed)) {
        if (isset($egOptions->seed->gender)) {
            if ($egOptions->seed->gender) {
                $orderByGender = TRUE;
            }
        }
        if (isset($egOptions->seed->age)) {
            if ($egOptions->seed->age) {
                $orderByAge = TRUE;
            }
        }
    }

    // has the group been read before, if so simply return save doing sql statement
    if (!array_key_exists($cacheKey, $GLOBALS)) {
        $compObj = e4s_getCompObj($compId);
        if ( !$compObj->areCardsAvailable() ){
            return false;
        }
        $sql = e4s_getSortingSQL( $egId, $heatOrder, $useCheckin, FALSE, '', 0, $orderByGender, $orderByAge);
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return FALSE;
        }

        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $writeSeedings = false;
        foreach ($rows as $row) {
            // if any row does not have seeding
            if (is_null($row['laneNo'])) {
                $writeSeedings = TRUE;
            }
        }
        $processedRows = array();
        $processedObjs = array();
        $position = 1;
        $checkedInPosition = 1;
        $lastGender = '';
        $lastMinAge = 0;

        foreach ($rows as $row) {
            $row['checkedIn'] = (int)$row['checkedIn'];
            $addARow = FALSE;
            $thisMinAge = (int)$row['minAge'];
            if ($lastGender === '') {
                $lastGender = $row['gender'];
            }
            if ($lastMinAge === 0) {
                $lastMinAge = $thisMinAge;
            }
            if ($orderByAge and $lastMinAge !== $thisMinAge) {
                $addARow = TRUE;
            }
            if ($orderByGender and $lastGender !== $row['gender']) {
                $addARow = TRUE;
            }

            $lastMinAge = $thisMinAge;
            $lastGender = $row['gender'];
            $row['athleteId'] = (int)$row['athleteId'];

            if (!e4s_isOTDEntry($row['entryOptions'])) {
//                $waitingPos = $entryCountObj->isAthleteOnWaitingListForEG($row['athleteId'], $egId);
                $onWaitingList = $compObj->isAthleteOnWaitingListForId($row['athleteId'], $egId) ;
                if ($onWaitingList) {
                    continue;
                }
            }
            if ($writeSeedings) {
                if ($addARow) {
                    $position++;
                    if ($row['checkedIn'] === 1) {
                        $checkedInPosition++;
                    }
                }
                $row['laneNo'] = $position++;
                $row['heatNo'] = 1;
                if ($row['checkedIn'] === 1) {
                    $row['laneNoCheckedIn'] = $checkedInPosition++;
                    $row['heatNoCheckedIn'] = 1;
                } else {
                    $row['laneNoCheckedIn'] = 0;
                    $row['heatNoCheckedIn'] = 0;
                }
            }
            $row['eventId'] = (int)$row['eventId'];

            unset($row['ceoptions']);
            unset($row['egoptions']);
            $processedRows[] = $row;
            $obj = new stdClass();
            $obj->athleteId = $row['athleteId'];
            $obj->heatNo = (int)$row['heatNo'];
            $obj->laneNo = (int)$row['laneNo'];
            $obj->heatNoCheckedIn = (int)$row['heatNoCheckedIn'];
            $obj->laneNoCheckedIn = (int)$row['laneNoCheckedIn'];
            $obj->confirmed = $row['confirmed'];
            if (!array_key_exists($egId, $processedObjs)) {
                $processedObjs[$egId] = array();
            }
            $processedObjs[$egId][$obj->athleteId] = $obj;
        }

        if ($writeSeedings) {
            $seedingObj = new seedingV2Class($compId, $egId);
            error_log('Write Seedings for EG ID: ' . $egId . ' $cacheKey: ' . $cacheKey);
            $seedingObj->clearAndWriteSeedings($processedObjs);
        }
        $GLOBALS[$cacheKey] = $processedRows;
    }

    $entries = $GLOBALS[$cacheKey];

    foreach ($entries as $entry) {
        if ($entry['athleteId'] === $athleteId) {
            $heatInfo->heatNoCheckedIn = $entry['heatNoCheckedIn'];
            $heatInfo->laneNoCheckedIn = $entry['laneNoCheckedIn'];
            $heatInfo->positionCheckedIn = $entry['laneNoCheckedIn'];
            $heatInfo->heatNo = $entry['heatNo'];
            $heatInfo->laneNo = $entry['laneNo'];
            $heatInfo->position = $entry['laneNo'];
            $heatInfo->confirmed = $entry['confirmed'];
            break;
        }
    }
    return TRUE;
}

// Generate Seedings
function e4s_getHeatLaneInfo(&$heatInfo, $row, $compid, $heatOrder, $coptions, $egOptions, $rows, $egAgeInfo, $ceOptions) {
    e4s_getHeatLaneInfoWithCheckin($heatInfo, $row, $compid, $heatOrder, FALSE, $egOptions, $rows, $egAgeInfo,$ceOptions);
    if (isCheckinEnabledFromOptions($coptions)) {
        e4s_getHeatLaneInfoWithCheckin($heatInfo, $row, $compid, $heatOrder, TRUE, $egOptions, $rows, $egAgeInfo, $ceOptions);
    }
    $seedRow = new stdClass();
    $seedRow->eventGroupId = (int)$row['eventGroupId'];

    $seedRow->athleteId = $row['athleteId'];
    $seedRow->laneNo = $heatInfo->laneNo;
    $seedRow->heatNo = $heatInfo->heatNo;
    $seedRow->laneNoCheckedIn = $heatInfo->laneNoCheckedIn;
    $seedRow->heatNoCheckedIn = $heatInfo->heatNoCheckedIn;
    $seedRow->confirmed = $heatInfo->confirmed;
    return $seedRow;
}

function e4s_getFieldSortInfo(&$heatInfo, $row, $compid, $heatOrder, $coptions, $egOptions, $egAgeInfo) {
    e4s_getFieldSortInfoWithCheckin($heatInfo, $row, $compid, FALSE, $heatOrder, $egOptions);
    if (isCheckinEnabledFromOptions($coptions)) {
        e4s_getFieldSortInfoWithCheckin($heatInfo, $row, $compid, TRUE, $heatOrder, $egOptions);
    }
}

function e4s_getTrackLaneCount($coptions, $egOptions): int {
    if (isset($egOptions->seed->laneCount)) {
        if ($egOptions->seed->laneCount > 0) {
            return $egOptions->seed->laneCount;
        }
    }
    if (isset($coptions->laneCount)) {
        if ($coptions->laneCount !== '') {
            return $coptions->laneCount;
        }
    }
    return E4S_DEFAULT_LANE_COUNT;
}

function e4s_defaultHeatCacheObj() {
    $obj = new stdClass();
    $obj->count = 0;
    $obj->rollingCount = 0;
    $obj->enabled = FALSE;
    return $obj;
}

function e4s_addHeatKeyCache($keyType, $egId, $key, $checkedIn) {
    $cacheNameAndEG = E4S_HEAT_CACHE . $egId . $keyType;
    if (!array_key_exists($cacheNameAndEG, $GLOBALS)) {
        $GLOBALS[$cacheNameAndEG] = array();
    }
    $egCache = $GLOBALS[$cacheNameAndEG];
    if (!array_key_exists($key, $egCache)) {
        $egCache[$key] = e4s_defaultHeatCacheObj();
        $egCache[$key . '_checkedin'] = e4s_defaultHeatCacheObj();
    }
    $egCache[$key]->count += 1;

    if ($checkedIn === 1) {
        $egCache[$key . '_checkedin']->count += 1;
    }

    $GLOBALS[$cacheNameAndEG] = $egCache;

}

function e4s_setHeatCache($rows, $egAgeInfo) {
    // read through $rows for the egId
    $cachedKey = 'e4s_cached_entries';
    if (!array_key_exists($cachedKey, $GLOBALS)) {
        $GLOBALS[$cachedKey] = array();
    }
    foreach ($rows as $row) {
        $entryId = (int)$row['entryId'];
        if (array_key_exists($entryId, $GLOBALS[$cachedKey])) {
            // entry has already been processed. How did that get in here though ?
            continue;
        }
        $GLOBALS[$cachedKey][$entryId] = TRUE;
        $checkedIn = (int)$row['checkedIn'];
        $egId = (int)$row['maxgroup'];

        $key = strtoupper($row['gender']);
        e4s_addHeatKeyCache(E4S_HEATCACHE_GENDER, $egId, $key, $checkedIn);

        $key = $row['ageGroupId'];
        e4s_addHeatKeyCache(E4S_HEATCACHE_AGE, $egId, $key, $checkedIn);

        $key = strtoupper($row['gender'] . $row['ageGroupId']);
        e4s_addHeatKeyCache(E4S_HEATCACHE_BOTH, $egId, $key, $checkedIn);
    }

    foreach ($egAgeInfo as $egId => $useAgeAndGender) {

        $femaleCheckedInRolling = 0;
        $femaleRolling = 0;
        $maleCheckedInRolling = 0;
        $maleRolling = 0;
        $ages = $useAgeAndGender->ageOnly;
        $ageIds = array();
        foreach ($ages as $ageId => $minAge) {
            $ageIds[$minAge] = $ageId;
        }

        usort($ages, 'sortAges');

        $heatCache = $GLOBALS[E4S_HEAT_CACHE . $egId . E4S_HEATCACHE_BOTH];

        foreach ($ages as $minAge) {
            $sortedAge = $ageIds[$minAge];
            $useKey = E4S_GENDER_FEMALE . $sortedAge;
            if (array_key_exists($useKey, $heatCache)) {
                $heatCache[$useKey]->rollingCount = $femaleRolling;
                $femaleRolling += $heatCache[$useKey]->count;
            }
            $useKey .= '_checkedin';
            if (array_key_exists($useKey, $heatCache)) {
                $heatCache[$useKey]->rollingCount = $femaleCheckedInRolling;
                $femaleCheckedInRolling += $heatCache[$useKey]->count;
            }
            $useKey = E4S_GENDER_MALE . $sortedAge;
            if (array_key_exists($useKey, $heatCache)) {
                $heatCache[$useKey]->rollingCount = $maleRolling;
                $maleRolling += $heatCache[$useKey]->count;
            }
            $useKey .= '_checkedin';
            if (array_key_exists($useKey, $heatCache)) {
                $heatCache[$useKey]->rollingCount = $maleCheckedInRolling;
                $maleCheckedInRolling += $heatCache[$useKey]->count;
            }
        }
    }
}

function sortAges($a, $b) {
    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}

/*
 * Only called when no seedings in db
 $heatCacheType = Seed by PB ( blank) Age (A), Gender (G) or both (B)
 */
function e4s_getIncHeatCnt($heatCacheType, $egId, $row, $gender, $egAgeInfo) {
    $cacheNameAndEG = E4S_HEAT_CACHE . $egId . $heatCacheType;

    $heatInc = new stdClass();
    $heatInc->entriesHeats = 0;
    $heatInc->checkedInHeats = 0;

    $minAge = $row['minAge'];

    if (!array_key_exists($cacheNameAndEG, $GLOBALS)) {
        return $heatInc;
    }

    $heatCache = $GLOBALS[$cacheNameAndEG];

    $addHeats = 0;
    $addCheckedInHeats = 0;
    $useAgeAndGender = $egAgeInfo[$egId];

    foreach ($heatCache as $cacheKey => $cacheObj) {
        $addCount = FALSE;
        $processCheckedIn = FALSE;
        if (stripos($cacheKey, '_checkedin') !== FALSE) {
            $processCheckedIn = TRUE;
        }
        $cacheKeyAgeId = str_replace('_checkedin', '', '' . $cacheKey);
        $cacheKeyAgeId = str_replace(E4S_GENDER_FEMALE, '', '' . $cacheKeyAgeId);
        $cacheKeyAgeId = str_replace(E4S_GENDER_MALE, '', '' . $cacheKeyAgeId);
        $cacheKeyAgeId = (int)$cacheKeyAgeId;
        $genderCacheKey = '';

        if ($heatCacheType === E4S_HEATCACHE_AGE) {
            if ($useAgeAndGender->ageOnly[$cacheKeyAgeId] < $minAge) {
                $addCount = TRUE;
            }
        } else {
            $genderCacheKey = $cacheKey[0];
        }
        if ($heatCacheType === E4S_HEATCACHE_GENDER) {
            // this is a male row
            if ($gender === E4S_GENDER_MALE) {
//                and there are females
                if ($genderCacheKey === E4S_GENDER_FEMALE) {
                    $addCount = TRUE;
                }
            }
        }
        if ($heatCacheType === E4S_HEATCACHE_BOTH) {
            // this is a male row and there are females
            if ($gender === E4S_GENDER_MALE) {
                // are there any females
                if ($genderCacheKey === E4S_GENDER_FEMALE) {
                    $addCount = TRUE;
                } else {
                    if ($useAgeAndGender->ageOnly[$cacheKeyAgeId] < $minAge) {
                        $addCount = TRUE;
                    }
                }
            } else {
                if ($useAgeAndGender->ageOnly[$cacheKeyAgeId] < $minAge and $genderCacheKey === $gender) {
                    if ($cacheObj->count > 0) {
                        $addCount = TRUE;
                    }
                }
            }
        }
//        $addCount = false;
        if ($addCount and $cacheObj->count > 0) {
            $heatInc->count = $cacheObj->count;
            if ($processCheckedIn) {
                $addCheckedInHeats += ceil($cacheObj->count / $row['maxInHeat']);
            } else {
                $addHeats += ceil($cacheObj->count / $row['maxInHeat']);
            }
        }
    }
    $heatInc->checkedInHeats = $addCheckedInHeats;
    $heatInc->entriesHeats = $addHeats;

    return $heatInc;
}

function e4s_seedHeats($heats, $isBaseHeat):array{
    if ( $isBaseHeat ){
        $heats = e4s_randomiseHeats($heats);
    }else{
        $heats = e4s_seed422($heats);
    }

    return $heats;
}
const E4S_SECTION_TOP4 = 'Top4';
const E4S_SECTION_MID2 = 'Mid2';
const E4S_SECTION_BOT2 = 'Bot2';

function e4s_seed422($heats){
    // randomise fastest 4 in lanes 3-6 then next fastest 2 in lanes 1 and 2, then next fastest 2 in lanes 7 and 8
    $heatInfoArr = array();
    // create arrays of athletes for each heat

    foreach ($heats as $athleteId=>$heat) {
        $heatNo = $heat['heatNo'];
        if ( !array_key_exists($heatNo, $heatInfoArr) ){
            $heatInfoArr[$heatNo] = array();
        }
        $section = E4S_SECTION_TOP4;
        if ( $heat['heatPosition'] > 4 ){
            $section = E4S_SECTION_MID2;
            if ( $heat['heatPosition'] > 6 ) {
                $section = E4S_SECTION_BOT2;
            }
        }
        if ( !array_key_exists($section, $heatInfoArr[$heatNo]) ){
            $heatInfoArr[$heatNo][$section] = array();
        }
        $heatInfoArr[$heatNo][$section][] = $athleteId;
    }

    // shuffle each heat
    foreach ($heatInfoArr as $heatNo=>$heatInfo) {
        foreach ($heatInfo as $section=>$sectionHeatInfo) {
            shuffle($sectionHeatInfo);
            $sectionCnt = 0;
            foreach ($sectionHeatInfo as $heatPosition=>$athleteId) {
                if ( $section === E4S_SECTION_TOP4 ){
                    $factor = 1;
                }
                if ( $section === E4S_SECTION_MID2 ){
                    // 8 lane track mid get pos 5 and 7 to ensure they get lane 1 and 2
                    $factor = 5 + ($sectionCnt * 1);
                }
                if ( $section === E4S_SECTION_BOT2 ){
                    // 8 lane track mid get pos 6 and 8 to ensure they get lane 7 and 8
                    $factor = 6 + ($sectionCnt * 1);
                }
                $heats[$athleteId]['heatPosition'] = $heatPosition + $factor;
                $sectionCnt++;
//                var_dump($heatNo . ':' . $athleteId . " - " . $heats[$athleteId]['heatPosition'] . '=(' . $heatPosition . " + " . $factor . ") - " . $heats[$athleteId]['eventpb']);
            }
        }
    }
//    foreach($heatInfoArr[1] as $section=>$sectionHeatInfo){
//        var_dump($section);
//        foreach($sectionHeatInfo as $heatPosition=>$athleteId){
//            var_dump($heats[$athleteId]['heatPosition'] . ':' . $athleteId . '/' . $heats[$athleteId]['eventpb']);
//        }
//    }
//    exit;
    return $heats;
}
function e4s_randomiseHeats($heats):array {
    $heatInfoArr = array();
    // creat arrays of athletes for each heat
    foreach ($heats as $athleteId=>$heat) {
        if ( !array_key_exists($heat['heatNo'], $heatInfoArr) ){
            $heatInfoArr[$heat['heatNo']] = array();
        }
        $heatInfoArr[$heat['heatNo']][] = $athleteId;
    }
    // shuffle each heat
    foreach ($heatInfoArr as $heatNo=>$heat) {
        shuffle($heat);
        foreach ($heat as $heatPosition=>$athleteId) {
            $heats[$athleteId]['heatPosition'] = $heatPosition + 1;
        }
    }

    return $heats;
}
// Calculate Seedings
function e4s_getHeatLaneInfoWithCheckin(&$heatInfo, $entryRow, $compId, $heatOrder, $useCheckedIn, $egOptions, $rows, $egAgeInfo, $ceOptions) {
//    $entryCountObj = $GLOBALS[E4S_ENTRY_COUNT_OBJ];
    $athleteId = (int)$entryRow['athleteId'];
    $egId = (int)$entryRow['maxgroup'];
    $ageGroupId = (int)$entryRow['ageGroupId'];

    $obj = new stdClass();
    $obj->heatNo = 0;
    $obj->laneNo = 0;
    $obj->position = 0;
    $obj->confirmed = null;
    $obj->maxInHeat = 0;
    $obj->laneCount = 0;
    $obj->useLanes = $heatInfo->useLanes;
    $obj->firstLane = $egOptions->seed->firstLane;
    if ($obj->firstLane < 1 or is_null($obj->firstLane)) {
        $obj->firstLane = 1;
    }
    $seedGender = '';
    $cacheGender = '';
    $seedAgeGroupIds = 0;
    $cacheAgeGroup = '';
    $seedType = E4S_SEED_OPEN;
    $heatCacheType = '';

    if (isset($egOptions->seed)) {
        if (isset($egOptions->seed->type)) {
            $seedType = $egOptions->seed->type;
        }
        if (isset($egOptions->seed->gender)) {
            if ($egOptions->seed->gender) {
                $seedGender = $entryRow['gender'];
                $cacheGender = '_' . E4S_HEATCACHE_GENDER . $seedGender;
                $heatCacheType = E4S_HEATCACHE_GENDER;
            }
        }
        if (isset($egOptions->seed->age)) {
            if ($egOptions->seed->age) {
                $seedAgeGroupIds = $ageGroupId;
                $cacheAgeGroup = '_' . E4S_HEATCACHE_AGE . $seedAgeGroupIds;
                if ($heatCacheType === '') {
                    $heatCacheType = E4S_HEATCACHE_AGE;
                } else {
                    $heatCacheType = E4S_HEATCACHE_BOTH;
                }
            }
        }
    }
    $cacheKey = 'EventCount_' . $egId . $cacheGender . $cacheAgeGroup . '_' . $useCheckedIn;
    if ( $seedType === E4S_SEED_HEAT ){
        $heatOrder = '';
    }
    // has the group been read before, if so simply return save doing sql statement
    if (!array_key_exists($cacheKey, $GLOBALS)) {
        $qualifyObj = new e4sQualifyClass();
        // is this a base heat with no eg qualify to this one
        $isBaseHeat = $qualifyObj->isBaseHeat($compId, $egId);

        $compObj = e4s_getCompObj($compId);
        if (! $compObj->areCardsAvailable() ){
            return false;
        }
        if ($cacheAgeGroup . $cacheGender !== '') {
            // at least one set
            e4s_setHeatCache($rows, $egAgeInfo);
        }
        // add to the cache a default
        $GLOBALS[$cacheKey] = array();

        if ( $seedAgeGroupIds !== 0 ) {
	        if ( isset( $ceOptions->ageGroups ) ) {
		        // has upscaling
		        foreach ( $ceOptions->ageGroups as $ageGroupObj ) {
			        $seedAgeGroupIds .= ',' . $ageGroupObj->ageGroup;
		        }
	        }
        }
        $sql = e4s_getSortingSQL( $egId, $heatOrder, $useCheckedIn, TRUE, $seedGender, $seedAgeGroupIds);

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return FALSE;
        }
        $rows = $result->fetch_all(MYSQLI_ASSOC);

        $processedRows = array();
        $maxInHeatCnt = 0;
        $position = 1;
        $numEntries = sizeof($rows);
        foreach ($rows as $row) {
            $row['athleteId'] = (int)$row['athleteId'];

            if (!e4s_isOTDEntry($row['entryOptions'])) {
                //                $waitingPos = $entryCountObj->isAthleteOnWaitingListForEG($row['athleteId'], $egId);
                $onWaitingList = $compObj->isAthleteOnWaitingListForId($row['athleteId'], $egId) ;
                if ($onWaitingList > 0) {
                    continue;
                }
            }
            $row['eventid'] = (int)$row['eventId'];
            $row['rowPosition'] = $position++;
            $row['coptions'] = e4s_addDefaultCompOptions($row['coptions']);
            $row['ceoptions'] = e4s_addDefaultCompEventOptions($row['ceoptions']);
            $row['useLanes'] = E4S_ALL_LANES;
            $row['maxInHeat'] = 0;
            if ($maxInHeatCnt === 0) {
                // first time in for this event
                $coptions = $row['coptions'];

                $maxInHeatCnt = e4s_getHeatCount($row);

                if ($maxInHeatCnt < 0) {
//                  e4s_addDebug($obj,"No Heat defined");
//                  No Heat Cnt specified so no calcs can be done
                    return FALSE;
                }

                // validate if All/Odd/Even lanes is ok
                $useLanes = $row['ceoptions']->heatInfo->useLanes;

                if ($useLanes !== E4S_ALL_LANES) {
                    $maxLaneCnt = e4s_getTrackLaneCount($coptions, $egOptions);
                    if ($maxLaneCnt & 1) {
                        // Odd lanes on track defined
                        if ($useLanes === E4S_EVEN_LANES) {
                            $availableLanes = floor($maxLaneCnt / 2);
                        } else {
                            $availableLanes = ceil($maxLaneCnt / 2);
                        }
                    } else {
                        $availableLanes = $maxLaneCnt / 2;
                    }
                    if ($availableLanes < $maxInHeatCnt) {
                        Entry4UIError(2020, "Event Group ID {$egId} has an issue. The track has {$availableLanes} available lanes, but the max athletes per heat is set to {$maxInHeatCnt}.");
                    }
                }
            }

            $row['useLanes'] = $useLanes;
            $row['maxInHeat'] = $maxInHeatCnt;
            unset($row['eoptions']);
            unset($row['egoptions']);

            $row['heatNoIncrement'] = e4s_getIncHeatCnt($heatCacheType, $egId, $row, $seedGender, $egAgeInfo);
            $heatInfoObj = e4s_getHeatInfo($row, $numEntries, $egOptions);
            $row['heatPosition'] = $heatInfoObj->heatPosition;
            $row['heatNo'] = $heatInfoObj->heatNo;
            $processedRows[$row['athleteId']] = $row;
        }

        if ( $seedType === E4S_SEED_HEAT ){
            $processedRows = e4s_seedHeats($processedRows, $isBaseHeat);
        }
        $GLOBALS[$cacheKey] = $processedRows;
    }

    $entries = $GLOBALS[$cacheKey];

    if (!isset($entries[$athleteId])) {
        $obj->laneCount = 0; // Lanes count for the track
        $obj->maxInHeat = 0; // Maximum athletes per heat. laneCount if not set in CE
        $obj->useLanes = E4S_ALL_LANES;
        $obj->heatNo = 0; // Heat the athlete is in
        $obj->heatNoIncrement = null; // Heat increment
        $obj->laneNo = 0; // Lane the athlete is in
        $obj->position = 1; // Athletes overall position
        $obj->heatPosition = 1; // Athlete position in heat
        $obj->confirmed = null;
    } else {
        $entry = $entries[$athleteId];
        $numEntries = sizeof($entries);
        $obj->heatNoIncrement = $entry['heatNoIncrement']; // Heat increment object
        $obj->laneCount = e4s_getTrackLaneCount($entry['coptions'], $egOptions);
        $obj->maxInHeat = $entry['maxInHeat'];
        $obj->useLanes = $entry['useLanes'];
        $obj->confirmed = $entry['confirmed'];
        $obj->position = $entry['rowPosition'];

        if ($seedType === E4S_SEED_OPEN) {
            $obj = e4s_openSeed($entry, $numEntries, $obj, $heatOrder);
        } else {
            if ( array_key_exists('heatPosition', $entry)) {
                $obj->heatPosition = $entry['heatPosition'];
                $obj->heatNo = $entry['heatNo'];
            }
//            $obj = e4s_heatSeed($entry, $numEntries, $obj, $heatOrder, $egId);
            $obj = e4s_heatSeedV2($numEntries, $obj, $heatOrder);
        }
    }

    if ($obj->laneCount > 0 and $obj->laneNo > 0 and $obj->firstLane > 1) {
        $firstLane = $obj->firstLane;
        $factor = 0;
        if ($obj->laneCount > $obj->maxInHeat) {
            $factor = $obj->maxInHeat - $obj->laneCount;
        }
        if ($obj->maxInHeat + $firstLane <= $obj->laneCount) {
            $obj->laneNo += $factor + ($firstLane - 1);
        }
    }

    $useHeatPosition = 0;
    $heatInfo->heatPositionCheckedIn = $useHeatPosition;
    $heatInfo->heatPosition = $useHeatPosition;
    if (isset($obj->heatPosition)) {
        $useHeatPosition = $obj->heatPosition;
    }

    $entriesInc = 0;
    $checkedInInc = 0;
    if (isset($obj->heatNoIncrement)) {
        $entriesInc = $obj->heatNoIncrement->entriesHeats;
        $checkedInInc = $obj->heatNoIncrement->checkedInHeats;
    }
    if ($useCheckedIn) {
//        $heatInfo->heatNoCheckedIn = $obj->heatNo ;
        $heatInfo->heatNoCheckedIn = $obj->heatNo + $checkedInInc;
        $heatInfo->laneNoCheckedIn = $obj->laneNo;
        $heatInfo->positionCheckedIn = $obj->position;
        $heatInfo->heatPositionCheckedIn = $useHeatPosition;
        if ($obj->laneCount < $obj->maxInHeat) {
            $heatInfo->arrowLaneno = $obj->laneNo;
            $heatInfo->laneNoCheckedIn = $useHeatPosition;
        }
    } else {
//        $heatInfo->heatNo = $obj->heatNo;
        $heatInfo->heatNo = $obj->heatNo + $entriesInc;
        $heatInfo->laneNo = $obj->laneNo;
        $heatInfo->position = $obj->position;
        $heatInfo->heatPosition = $useHeatPosition;
        if ($obj->laneCount < $obj->maxInHeat) {
            $heatInfo->arrowLaneno = $obj->laneNo;
            $heatInfo->laneNo = $useHeatPosition;
        }
    }
    $heatInfo->confirmed = $obj->confirmed;
    $heatInfo->useLanes = $obj->useLanes;
    $heatInfo->maxInHeat = $obj->maxInHeat;

    return TRUE;
}

function getMaxAthletesInHeat($maxInHeat, $laneCount) {
    $maxInHeatCnt = $maxInHeat;
    if ($maxInHeatCnt === 0) {
        $maxInHeatCnt = $laneCount;
    }
    return $maxInHeatCnt;
}

function getNumberOfHeats($maxInHeat, $laneCount, $numEntries) {
    $maxInHeatCnt = getMaxAthletesInHeat($maxInHeat, $laneCount);
    return (int)ceil($numEntries / $maxInHeatCnt);
}

function e4s_getHeatInfo($entry, $numEntries, $egOptions):stdClass{
    $position = $entry['rowPosition'];
    $maxInHeat = $entry['maxInHeat'];
    $laneCount = e4s_getTrackLaneCount($entry['coptions'], $egOptions);

    if ($maxInHeat === '') {
        $maxInHeat = $laneCount;
    }

    $numOfHeats = getNumberOfHeats($maxInHeat, $laneCount, $numEntries);

    if ($numOfHeats === 1) {
        $heatNo = 1;
        $heatPosition = $position;
    } else {
        $heatNo = $position % $numOfHeats;
        if ($heatNo === 0) {
            $heatNo = $numOfHeats;
        }
        $heatPosition = (int)ceil($position / $numOfHeats);
    }

    $retObj = new stdClass();
    $retObj->heatNo = $heatNo;
    $retObj->heatPosition = $heatPosition;
    return $retObj;
}
function e4s_heatSeedV2($numEntries, $obj, $heatOrder) {
    $position = $obj->position;
    if ($obj->maxInHeat === '') {
        $obj->maxInHeat = $obj->laneCount;
    }

    $numOfHeats = getNumberOfHeats($obj->maxInHeat, $obj->laneCount, $numEntries);
    $minPerHeat = (int)floor($numEntries / $numOfHeats);
    $heatsWithMax = $numEntries - ($numOfHeats * $minPerHeat);
    if ($heatsWithMax === 0) {
        $maxPerHeat = $minPerHeat;
    } else {
        $maxPerHeat = $minPerHeat + 1;
    }

    $heatsWithMaxAthletes = $numEntries % $numOfHeats;
    if ($heatsWithMaxAthletes === 0) {
        $heatsWithMaxAthletes = $numOfHeats;
    }

    if ( ! isset($obj->heatPosition)) {
        if ($numOfHeats === 1) {
            $heatNo = 1;
            $heatPosition = $position;
        } else {
            $heatNo = $position % $numOfHeats;
            if ($heatNo === 0) {
                $heatNo = $numOfHeats;
            }
            $heatPosition = (int)ceil($position / $numOfHeats);
        }
        $obj->heatNo = $heatNo;
        $obj->heatPosition = $heatPosition;
    }

    if ($obj->heatNo > $heatsWithMaxAthletes) {
        $obj->laneNo = e4s_getLaneDraw($obj, $minPerHeat, $heatOrder);
    } else {
        $obj->laneNo = e4s_getLaneDraw($obj, $maxPerHeat, $heatOrder);
    }

    return $obj;
}
function e4s_heatSeed($entry, $numEntries, $obj, $heatOrder, $egId) {
    $position = $entry['rowPosition'];
    // workout how many heats there are
    $debugEgId = -1;

    if ($obj->maxInHeat === '') {
        $obj->maxInHeat = $obj->laneCount;
    }
    $numOfHeats = getNumberOfHeats($obj->maxInHeat, $obj->laneCount, $numEntries);
//    $maxInHeatCnt = getMaxAthletesInHeat($obj->maxInHeat, $obj->laneCount);
    $minPerHeat = (int)floor($numEntries / $numOfHeats);
//    $maxPerHeat = (int)ceil($numEntries / $numOfHeats);
    $heatsWithMax = $numEntries - ($numOfHeats * $minPerHeat);

    if ($heatsWithMax === 0) {
        $maxPerHeat = $minPerHeat;
    } else {
        $maxPerHeat = $minPerHeat + 1;
    }

    $heatsWithMaxAthletes = $numEntries % $numOfHeats;
    if ($heatsWithMaxAthletes === 0) {
        $heatsWithMaxAthletes = $numOfHeats;
    }
    if ($numOfHeats === 1) {
        $obj->heatNo = 1;
        $heatPosition = $position;
    } else {
        $obj->heatNo = $position % $numOfHeats;
        if ($obj->heatNo === 0) {
            $obj->heatNo = $numOfHeats;
        }

        $heatPosition = (int)ceil($position / $numOfHeats);
    }
    if ( array_key_exists('heatPosition', $entry)) {
        $heatPosition = $entry['heatPosition'];
    }
    $obj->heatPosition = $heatPosition;
    $obj->position = $position;

    if ($obj->heatNo > $heatsWithMaxAthletes) {
//        e4s_addDebug($minPerHeat,"Min");
        $obj->laneNo = e4s_getLaneDraw($obj, $minPerHeat, $heatOrder);
    } else {
//        e4s_addDebug($maxPerHeat,"Max");
        $obj->laneNo = e4s_getLaneDraw($obj, $maxPerHeat, $heatOrder);
    }

    return $obj;
}

function e4s_openSeed($entry, $numEntries, $obj, $heatOrder) {
    $position = $entry['rowPosition'];
    if ($obj->maxInHeat === '') {
        $obj->maxInHeat = 0;
    }

    $maxInHeatCnt = getMaxAthletesInHeat($obj->maxInHeat, $obj->laneCount);
    $numOfHeats = (int)ceil($numEntries / $maxInHeatCnt);
    $minPerHeat = (int)floor($numEntries / $numOfHeats);
    $maxPerHeat = (int)ceil($numEntries / $numOfHeats);

    $heatsWithMax = $numEntries - ($numOfHeats * $minPerHeat);
    if ($heatsWithMax === 0) {
        // Comp Full
        $heatsWithMax = $numOfHeats;
        $heatsWithMin = 0;
        $athletesInMinCountHeats = 0;
        $athletesInMaxCountHeats = $numEntries;
    } else {
        $heatsWithMin = $numOfHeats - $heatsWithMax;
        $athletesInMinCountHeats = ($heatsWithMin * $minPerHeat);
        $athletesInMaxCountHeats = ($heatsWithMax * $maxPerHeat);
    }

    $fastFirst = FALSE;
    if (isset($entry['coptions']->heatOrder)) {
        $fastFirst = $entry['coptions']->heatOrder === E4S_FASTEST_FIRST;
    }

    if ($fastFirst) {
        if ($position <= $athletesInMaxCountHeats) {
            // Athlete in a heat with maximum number of athletes
            $obj->heatNo = (int)ceil($position / $maxPerHeat);

            $positionInHeat = $position % $maxPerHeat;
            if ($positionInHeat === 0) {
                $positionInHeat = $maxPerHeat;
            }
            $obj->heatPosition = $positionInHeat;
            $obj->laneNo = e4s_getLaneDraw($obj, $maxPerHeat, $heatOrder);
        } else {
            // athlete in a heat with minimum entries in heat
            $usePosition = $position - $athletesInMaxCountHeats;
            $obj->heatNo = (int)ceil($usePosition / $minPerHeat);
            $positionInHeat = $usePosition % $minPerHeat;
            if ($positionInHeat === 0) {
                $positionInHeat = $minPerHeat;
            }
            $obj->heatPosition = $positionInHeat;
            $obj->heatNo += $heatsWithMax;
            $obj->laneNo = e4s_getLaneDraw($obj, $minPerHeat, $heatOrder);
        }
    } else {
        if ($position <= $athletesInMinCountHeats) {
            // Athlete in a heat with minimum number of athletes
            $obj->heatNo = (int)ceil($position / $minPerHeat);

            $positionInHeat = $position % $minPerHeat;
            if ($positionInHeat === 0) {
                $positionInHeat = $minPerHeat;
            }
            $obj->heatPosition = $positionInHeat;
            $obj->laneNo = e4s_getLaneDraw($obj, $minPerHeat, $heatOrder);
        } else {
            // athlete in a heat with 1 more than minimum
            $usePosition = $position - $athletesInMinCountHeats;
            $obj->heatNo = (int)ceil($usePosition / $maxPerHeat);
            $positionInHeat = $usePosition % $maxPerHeat;
            if ($positionInHeat === 0) {
                $positionInHeat = $maxPerHeat;
            }
            $obj->heatPosition = $positionInHeat;
            $obj->heatNo += $heatsWithMin;

            $obj->laneNo = e4s_getLaneDraw($obj, $maxPerHeat, $heatOrder);
        }
    }

    $obj->maxInHeat = $maxInHeatCnt;
    $obj->position = $position;

    return $obj;
}

/*
 * AthletePos if slowest first is reversed
 * so pos 8 with maxlanes 8 means fastest
 */
function e4s_getLaneDraw($obj, $maxLanesInHeat, $heatOrder) {
    $lanes = array();
    $laneFactor = 1;
    $firstLane = 1;
    if (isset($obj->firstLane)) {
        $firstLane = $obj->firstLane;
    }
    $useFirstLane = $firstLane - 1;
    $noOfLanesOnTrack = $obj->laneCount - $useFirstLane;

    if ($maxLanesInHeat > $noOfLanesOnTrack) {
        $maxLanesInHeat = $noOfLanesOnTrack;
    }
    $counter = 1;
    $fastestLane = ceil($noOfLanesOnTrack / 2);

    if ($obj->useLanes !== E4S_ALL_LANES) {
        $laneFactor = 2;
    }

    if ($obj->useLanes === E4S_ODD_LANES and !($fastestLane & 1)) {
        $fastestLane--;
    }

    if ($obj->useLanes === E4S_EVEN_LANES and ($fastestLane & 1)) {
        $fastestLane++;
    }
    $lanes[$counter] = $fastestLane;

    if ($noOfLanesOnTrack & 1) {
        // odd no of lanes on track
        $next = TRUE;
        $maxFactor = -$laneFactor;
        $minFactor = $laneFactor;
    } else {
        // even no of lanes on track
        $next = FALSE;
        $maxFactor = $laneFactor;
        $minFactor = -$laneFactor;
    }

    $maxLane = $fastestLane + $maxFactor;
    $minLane = $fastestLane + $minFactor;
    while ($counter < $maxLanesInHeat) {
        $counter += 1;
        $next = !$next;
        if ($next) {
            $lanes[$counter] = $maxLane;
            $maxLane += $maxFactor;
        } else {
            $lanes[$counter] = $minLane;
            $minLane += $minFactor;
        }
    }

    $lanePosition = $obj->heatPosition;

    // get the position in this heat
    if ($heatOrder === E4S_SLOWEST_FIRST) {
        $lanePosition = ($maxLanesInHeat + 1) - $obj->heatPosition;
        if ($lanePosition < 1) {
            $lanePosition = $maxLanesInHeat;
        }
    } elseif ( $heatOrder !== '' ) {
        $lanePosition = $obj->heatPosition % $maxLanesInHeat;
        if ($lanePosition < 1) {
            $lanePosition = 1;
        }
    }

	if ( array_key_exists($lanePosition, $lanes) ) {
		return (int) $lanes[ $lanePosition ] + ( $firstLane - 1 );
	}
	return 0;
}

function e4s_getHeatCount($row) {
    $egOptions = $row['egoptions'];

    // Updating in the build SHOULD update all CEs, but not so read the egoptions as main priority
    // check event Group first, then check ce first , then the comp for lane count, the the event itself
    $heatCount = E4S_DEFAULT_LANE_COUNT;

    $egOptions = e4s_getOptionsAsObj($egOptions);
    if (isset($egOptions->maxInHeat)) {
        if ($egOptions->maxInHeat !== 0) {
            $heatCount = $egOptions->maxInHeat;
        }
    }
    return $heatCount;
}

function ceiling($number, $significance = 1) {
    return (is_numeric($number) and is_numeric($significance)) ? (ceil($number / $significance) * $significance) : FALSE;
}

function e4s_getCurrentBibNos($compid) {
    $bibObj = new bibClass($compid);
    return $bibObj->getBibNumbers();
}

function e4s_addEventToAthlete(&$athleteArr, $row) {
    $id = (int)$row['athleteId'];
    $athlete = $athleteArr [$id];

    // not sure why I do this ???
    if ((int)$row['maxgroup'] < (int)$athlete['maxgroup']) {
        $athlete['maxgroup'] = $row['maxgroup'];
    }
    e4s_addEventToAthleteRecord($athlete, $athleteArr, $row);
}

function e4s_addEventToAthleteRecord(&$athlete, &$athleteArr, $row) {
    // ----------
    if (array_key_exists('eventagegroup', $row)) {
        $eventagegroup = $row['eventagegroup'];
    } else {
        $eventagegroup = $row['agegroup'];
    }
    $eventagegroup = e4s_getVetDisplay($eventagegroup, $athlete['gender']);
    if (array_key_exists('events', $athlete)) {
        $athleteEvents = $athlete['events'];
    } else {
        $athleteEvents = array();
    }

    $athleteEvent = new stdClass();
    $athleteEvent->event = trim($row['event']);
    $athleteEvent->eventAgeGroup = $eventagegroup;
    $athleteEvent->eventid = (int)$row['maxgroup'];
    $athleteEvent->teamName = '';
    if (array_key_exists('teamName', $row)) {
        $athleteEvent->teamName = $row['teamName'];
    }
    $pb = $row['pb'];
    if (is_null($pb)) {
        $pb = 0;
    }
    $athleteEvent->pb = $pb;

    $athleteEvents[] = $athleteEvent;
    $athlete['events'] = $athleteEvents;
    $athleteArr[(int)$athlete['id']] = $athlete;
}

// Add Athlete to the array for BibNo allocation
function e4s_addAthlete(&$athleteArr, &$bibArr, $row, $clubArr) {
    $athlete = array();
    if (array_key_exists((int)$row['athleteId'], $athleteArr)) {
        return e4s_addEventToAthlete($athleteArr, $row);
    }

    $athlete['id'] = (int)$row['athleteId'];
    $athlete['firstname'] = formatAthleteFirstname($row['firstName']);
    $athlete['surname'] = formatAthleteSurname($row['surName']);
    $athlete['gender'] = $row['gender'];
    $athlete['dob'] = $row['dob'];
    $athlete['urn'] = $row['URN'];
    $athlete['county'] = $row['county'];
    $athlete['region'] = $row['region'];
    $athlete['agegroup'] = $row['agegroup'];

    $baseGroup = e4s_getBaseAGForDOBForComp($GLOBALS[E4S_OUTPUT_COMP], $row['dob'], $GLOBALS['baseAGRows']);
    $ageGroupId = 0;
    if (array_key_exists('ageGroupId', $row)) {
        $ageGroupId = $row['ageGroupId'];
    }
    $athlete['ageGroupId'] = $ageGroupId;

    if (!is_null($baseGroup)) {
        $athlete['agegroup'] = $baseGroup['Name'];
        $athlete['ageGroupId'] = (int)$baseGroup['id'];
    }

    $athlete['agegroup'] = e4s_getVetDisplay($athlete['agegroup'], $row['gender']);

    $athlete['ageshortgroup'] = shortAgeGroup($athlete['agegroup'], $row['gender']);

//    e4s_addEventToAthleteRecord($athlete,$athleteArr,$row);
    if (array_key_exists('eventagegroup', $row)) {
        $eventagegroup = $row['eventagegroup'];
    } else {
        $eventagegroup = $row['agegroup'];
    }
    $eventagegroup = e4s_getVetDisplay($eventagegroup, $athlete['gender']);
    $athleteEvents = array();
    $athleteEvent = new stdClass();
    $athleteEvent->event = trim($row['event']);
    $athleteEvent->eventAgeGroup = $eventagegroup;
    $athleteEvent->eventid = (int)$row['maxgroup'];
    $athleteEvent->teamName = '';
    if (array_key_exists('teamName', $row)) {
        $athleteEvent->teamName = $row['teamName'];
    }
    $pb = $row['pb'];
    if (is_null($pb)) {
        $pb = 0;
    }
    $athleteEvent->pb = $pb;
    $athleteEvents[] = $athleteEvent;
    $athlete['events'] = $athleteEvents;

    $athlete['classification'] = $row['classification'];
    $athlete['clubid'] = $row['clubid'];
    if (array_key_exists($row['clubid'], $clubArr)) {
        $clubRow = $clubArr[$row['clubid']];
        $club = $clubRow['Clubname'];
        $athlete['clubname'] = $club;
    } else {
        $athlete['clubname'] = 'Unknown';
    }
    $athlete['created'] = $row['created'];
    $athlete['maxgroup'] = $row['maxgroup'];
    if (array_key_exists($athlete['id'], $GLOBALS[E4S_BIB_NOS])) {
        $athlete['bibno'] = $GLOBALS[E4S_BIB_NOS][$athlete['id']]->bibNo;
        $bibArr[(int)$athlete['bibno']] = (int)$athlete['id'];
    } else {
        $athlete['bibno'] = 0;
    }

    $athleteArr[(int)$athlete['id']] = $athlete;
    return $athlete;
}

function secureAccessToCard(): bool {
    return hasSecureAccess() or hasSeedingAccess() or hasResultsAccess();
}
function canClearResults(): bool {
    return !isProductionEnv() or isE4SUser();
}