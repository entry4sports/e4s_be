<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
e4s_checkFinanceReportAccess();

$config = e4s_getConfig();
$title = 'Date From ' . date('d/m/Y', strtotime('-' . $days . ' days')) . ' to Today';
echo '<link rel="stylesheet" href="' . E4S_PATH . '/css/pqgrid6.min.css">';
echo '<link rel="stylesheet" href="' . E4S_PATH . '/css/pqgrid6.ui.min.css">';
echo '<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.3/themes/' . E4S_JQUERY_THEME . '/jquery-ui.css">';
echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> ';
echo '<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>';
echo '<script type="text/javascript" src="' . E4S_PATH . '/js/pqgrid6.min.js"></script>';
echo '<script type="text/javascript" src="' . E4S_PATH . '/js/jszip.min.js"></script>';
echo '<script type="text/javascript" src="' . E4S_PATH . '/js/FileSaver.min.js"></script>';
echo "<button type=\"button\" id=\"pq-grid-table-btn\" style=\"margin:0px 20px 20px;\">Change Table to Grid</button>";
echo '<div id="grid_table" style="margin:auto;"></div>';

$sql = "SELECT date_format(date(post_date),'%D %b %Y') orderdate, e.orderid, sum(e.price) price, c.id as compid, c.name competition
        FROM " . E4S_TABLE_POSTS . ' p,
             ' . E4S_TABLE_ENTRIES . ' e,
             ' . E4S_TABLE_EVENTS . ' ev,
             ' . E4S_TABLE_COMPEVENTS . ' ce,
             ' . E4S_TABLE_COMPETITON . " c
        WHERE post_type = '" . WC_POST_ORDER . "' 
        and post_status = 'wc-completed' 
        and date(post_date) >= CURRENT_DATE - INTERVAL $days  DAY
        and p.id = e.orderid
        and ev.id = ce.eventid
        and ce.id = e.compeventid
        and ce.compid = c.id
        group by date(post_date),c.id 
        order by post_date desc";

$result = e4s_queryNoLog('Report4' . E4S_SQL_DELIM . $sql);
$rows = $result->fetch_all(MYSQLI_ASSOC);

echo "<table id='finance'>";
echo '<tr>';
echo '<td>Date</td>';
echo '<td>Competition Ref</td>';
echo '<td>Competition Name</td>';
echo '<td>Order</td>';
echo '<td>Line Cost</td>';

echo '</tr>';
foreach ($rows as $row) {
    echo '<tr>';
    echo '<td>' . $row['orderdate'] . '</td>';
    echo '<td>' . $row['compid'] . '</td>';
    echo '<td>' . str_replace('&', ' and ', urldecode($row['competition'])) . '</td>';
    echo '<td>' . $row['orderid'] . '</td>';
    echo '<td>' . $row['price'] . '</td>';
    echo '</tr>';
}
echo '</table>';
echo '<script>';
echo '
var tbl = $("#finance");          
var tblObj = $.paramquery.tableToArray(tbl);
var colM = [
    {
        align:"left",
        dataIndx:0,
        dataType:"string",
        title:"Date",
        width:200
    },
    {
        align:"right",
        dataIndx:1,
        dataType:"integer",
        title:"#",
        width:50
    },
        {
        align:"left",
        dataIndx:2,
        dataType:"string",
        title:"Competition",
        width:450
    },
    {
        align:"right",
        dataIndx:3,
        dataType:"integer",
        title:"Order#",
        width:75
    },
    {
        align:"right",
        dataIndx:4,
        dataType:"float",
        title:"Line Cost",
        width:200,
        format: \'' . $config['currency'] . '##,###.00\',
        summary: {
          type: "sum"
        }
    },
];
var newObj = { 
    width: "100%", 
    height: "100%", 
    title: "' . $title . '", 
    flexWidth: true ,
    numberCell: {show: false},
    dataModel:{ 
        data: tblObj.data 
    },
    toolbar: {
        items: [
            {
                type: \'select\',
                label: \'Format: \',                
                attr: \'id="export_format"\',
                options: [{ xlsx: \'Excel\', csv: \'Csv\', htm: \'Html\', json: \'Json\'}]
            },
            {
                type: \'button\',
                label: \'Export\',
                icon: \'ui-icon-arrowthickstop-1-s\',
                listener: function () {
                    var format = $("#export_format").val(),                            
                        blob = this.exportData({
                            format: format,        
                            nopqdata: true, //applicable for JSON export.                        
                            render: true
                        });
                    if(typeof blob === "string"){                            
                        blob = new Blob([blob]);
                    }
                    saveAs(blob, "pqGrid." + format );
                }
            }
        ]
    },
    colModel: colM,

    groupModel: {
        on: true,
        dataIndx: [0],                       
        grandSummary: true,           
        summaryInTitleRow: \'all\',
        titleInFirstCol: true, 
        fixCols: false, 
        indent: 20,
        collapsed: [true],
        title: [
            "{0}"
        ]
    }
};
//var obj = $("#grid_table").pqGrid(newObj);
//tbl.css("display", "none");
   $(function () {
        function changeToTable(that) {
        debugger;
            var tbl = $("#finance");
            tbl.show();
            $("#grid_table").pqGrid("destroy");
            $(that).button("option", "label", "Change Table To Grid");
        }
        function changeToGrid(that) {

            var tbl = $("#finance");
            var obj = $.paramquery.tableToArray(tbl);
            //var newObj = {title: "Grid From Table", height:"100%", flexWidth: true };
           // newObj.dataModel = { data: obj.data };
            //newObj.colModel = obj.colModel;
//            newObj.pageModel = { rPP: 20, type: "local" };
            //$("#grid_table").pqGrid(newObj);
            var obj = $("#grid_table").pqGrid(newObj)
            $(that).button("option", "label", "Change Grid back to Table");
            tbl.hide();
        }
        //toggle removed from $ 1.9
        $("#pq-grid-table-btn").button().click(function () {
            if ($("#grid_table").hasClass(\'pq-grid\')) {
                changeToTable(this);
            }
            else {
                changeToGrid(this);
            }
        });
    });
';

echo '</script>';
exit();