<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';

e4s_checkFinanceReportAccess();

$config = e4s_getConfig();
echo '<link rel="stylesheet" href="' . E4S_PATH . '/css/pqgrid6.min.css">';
echo '<link rel="stylesheet" href="' . E4S_PATH . '/css/pqgrid6.ui.min.css">';
echo '<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.3/themes/' . E4S_JQUERY_THEME . '/jquery-ui.css">';
echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> ';
echo '<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>';
echo '<script type="text/javascript" src="' . E4S_PATH . '/js/pqgrid6.min.js"></script>';
echo '<script type="text/javascript" src="' . E4S_PATH . '/js/jszip.min.js"></script>';
echo '<script type="text/javascript" src="' . E4S_PATH . '/js/FileSaver.min.js"></script>';
echo '<div id="grid_table" style="margin:auto;"></div>';
echo '<style>
                table {
                  border-collapse: collapse;
                }
                table, th, td {
                      border: 1px solid #bfbebf;
                }
            </style>
';
$sql = 'SELECT * from ' . E4S_TABLE_COMPETITON . '
        order by date desc';
$result = e4s_queryNoLog($sql);
$rows = $result->fetch_all(MYSQLI_ASSOC);
echo '<div id="grid_table" style="margin:auto;"></div>';
echo "<table id='finance'>";
echo '<tr>';
echo '<td>#</td>';
echo '<td>Name</td>';
echo "<td style='width:100px;'>Date</td>";
echo "<td style='width:100px;'>Report</td>";
echo "<td style='width:100px;'>New Report</td>";
echo '</tr>';
foreach ($rows as $row) {
    echo '<tr>';
    echo '<td>' . $row['ID'] . '</td>';
    echo '<td>' . $row['Name'] . '</td>';
    echo '<td>' . $row['Date'] . '</td>';
    echo "<td><a href='/wp-json/e4s/v5/public/reports/output/" . $row['ID'] . "'>Report</a></td>";
    echo "<td><a href='/wp-json/e4s/v5/reports/finance/" . $row['ID'] . "'>New Report</a></td>";
    echo '</tr>';
}
echo '</table>';
?>
<!--<script>-->
<!--var tbl = $("#finance");          -->
<!--var tblObj = $.paramquery.tableToArray(tbl);-->
<!--var colM = [-->
<!--    {-->
<!--        align:"left",-->
<!--        dataIndx:0,-->
<!--        dataType:"integer",-->
<!--        title:"CompID",-->
<!--        width:20-->
<!--    },-->
<!--    {-->
<!--        align:"left",-->
<!--        dataIndx:1,-->
<!--        dataType:"string",-->
<!--        title:"Competition",-->
<!--        width:350-->
<!--    },-->
<!--    {-->
<!--        align:"left",-->
<!--        dataIndx:2,-->
<!--        dataType:"date",-->
<!--        title:"Date",-->
<!--        width:50-->
<!--    },-->
<!--         {-->
<!--        align:"center",-->
<!--        dataIndx:3,-->
<!--        dataType:"string",-->
<!--        title:"Report",-->
<!--        width:50-->
<!--    }-->
<!--];-->
<!--var newObj = { -->
<!--    width: "100%", -->
<!--    height: "100%", -->
<!--    title: "' . $title .'", -->
<!--    flexWidth: true ,-->
<!--    numberCell: {show: false},-->
<!--    dataModel:{ -->
<!--        data: tblObj.data -->
<!--    },-->
<!--    toolbar: {-->
<!--        items: [-->
<!--            {-->
<!--                type: "select",-->
<!--                label: "Format: ",                -->
<!--                attr: "id='export_format'",-->
<!--                options: [{ xlsx: "Excel", csv: "Csv", htm: "Html", json: "Json"}]-->
<!--            },-->
<!--            {-->
<!--                type: "button",-->
<!--                label: "Export",-->
<!--                icon: "ui-icon-arrowthickstop-1-s",-->
<!--                listener: function () {-->
<!--                    var format = $("#export_format").val(),                            -->
<!--                        blob = this.exportData({-->
<!--                            format: format,        -->
<!--                            nopqdata: true, //applicable for JSON export.                        -->
<!--                            render: true-->
<!--                        });-->
<!--                    if(typeof blob === "string"){                            -->
<!--                        blob = new Blob([blob]);-->
<!--                    }-->
<!--                    saveAs(blob, "pqGrid." + format );-->
<!--                }-->
<!--            }-->
<!--        ]-->
<!--    },-->
<!--    colModel: colM-->
<!--};-->
<!---->
<!--var tbl = $("#finance");-->
<!--var obj = $.paramquery.tableToArray(tbl);-->
<!--obj = $("#grid_table").pqGrid(newObj)-->
<!--tbl.hide();-->
<!---->
<!--</script>;-->