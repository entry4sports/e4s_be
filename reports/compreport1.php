<?php
include_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_runReport1($compid, $orgid) {
    $expand = FALSE;
    if (!isset($compid)) {
        $compid = 0;
    }
    if (!isset($orgid)) {
        $orgid = 0;
    }
    if ($orgid > 0 || $compid > 0) {
        $expand = TRUE;
    }
    e4s_checkFinanceReportAccess();

    $config = e4s_getConfig();
    $compRows = getReportComps($orgid, $compid);
    $GLOBALS['grantCount'] = 0;
    $GLOBALS['grandPaid'] = E4S_ENTRY_NOT_PAID;
    $GLOBALS['grandPaidActual'] = 0;
    $GLOBALS['grandFee'] = 0;
    echo '<table>';
    foreach ($compRows as $compRow) {

        reportOnComp($compRow, $config, $expand);

        echo '<tr>';
        echo "<td colspan=3 style='border-bottom:2px black solid;'>";
        echo '</td>';
        echo '</tr>';
//    echo "row : " . $compRow['id'] . ". ";
    }
    echo '<tr>';
    echo "<td colspan='1'>";
    outputDataWithLabel('Grand Entries', $GLOBALS['grantCount']);
    echo '</td>';

    echo "<td colspan='1'>";
    outputDataWithLabel('Grand Amount', convertToCurrency($config, $GLOBALS['grandPaid']) . ' (' . convertToCurrency($config, $GLOBALS['grandPaidActual']) . ')');
    echo '</td>';

    echo "<td colspan='1'>";
    outputDataWithLabel('Total Fees', convertToCurrency($config, $GLOBALS['grandFee']));
    echo '</td>';
    echo '</tr>';
    echo '</table>';
    exit();
}

function getReportComps($orgid, $compid) {
    $sql = "select *, date_format(date,'%D %b %Y') compDate, cc.club orgName
            from " . E4S_TABLE_COMPETITON . ' c,
                 ' . E4S_TABLE_COMPCLUB . ' cc 
            where cc.id = c.compclubid ';
    if ($orgid > 0) {
        $sql .= ' and c.compclubid = ' . $orgid;
    } else {
        if ($compid > 0) {
            $sql .= ' and c.id = ' . $compid;
        }
    }
    //$sql .= " and entriesopen > " . returnNow();
    $sql .= ' order by date desc';
    $result = e4s_queryNoLog($sql);
    return $result->fetch_all(MYSQLI_ASSOC);
}

function e4s_getCompPrices($compid) {
    $sql = 'select * from ' . E4S_TABLE_EVENTPRICE . '
        where compid = ' . $compid;
    $priceResult = e4s_queryNoLog($sql);
    return $priceResult->fetch_all(MYSQLI_ASSOC);
}

function reportOnComp($compRow, $config, $expand) {
    echo "<tr style='background-color: lightgrey;'>";
    echo "<td colspan='2'>";
    $label = '';
    if (strpos($compRow['Name'], $compRow['orgName']) === FALSE) {
        $label = $compRow['orgName'] . ' ';
    }
    $label .= $compRow['Name'] . ' (' . $compRow['ID'] . '-' . $compRow['compclubid'] . ')';
    outputDataWithLabel('Competition', $label);
    echo '</td>';
    echo "<td colspan='1'>";
    outputDataWithLabel('Date', $compRow['compDate']);
    echo '</td>';
    echo '</tr>';

    echo '<tr>';
    $priceRows = e4s_getCompPrices($compRow['ID']);
    echo "<td colspan='3'>";
    echo 'Defined prices :<br>';

    foreach ($priceRows as $priceRow) {
        if (is_null($priceRow['saleenddate'])) {
            echo 'Price ' . $priceRow['description'] . ' : ' . $priceRow['saleprice'] . ' (' . $priceRow['salefee'] . ')<br>';
        } else {
            echo 'Std Price ' . $priceRow['description'] . ' : ' . $priceRow['price'] . ' (' . $priceRow['fee'] . '). SaleEnd : ' . $priceRow['saleenddate'] . '. Sale Price : ' . $priceRow['saleprice'] . '(' . $priceRow['salefee'] . ')<br>';
        }
    }
    echo '</td>';
    echo '</tr>';

    echo '<tr>';
    echo "<td style='border-bottom: 1px black dashed;'>";
    if ($expand) {
        outputDataWithLabel('Order No', '');
    } else {
        outputDataWithLabel('Entry Count', '');
    }

    echo '</td>';
    echo "<td style='border-bottom: 1px black dashed;'>";
    outputDataWithLabel('Event Paid', '');

    echo '</td>';
    echo "<td style='border-bottom: 1px black dashed;'>";
    outputDataWithLabel('Fees', '');

    echo '</td>';
    echo '</tr>';

//    $sql = "SELECT count(e.*) count, e.price
//            FROM " . E4S_TABLE_ENTRIES . " e,
//            WHERE compEventID in ( select id from " . E4S_TABLE_COMPEVENTS . "  where compid = " . $compRow['ID'] . " ) and paid = 1 group by price";

    if ($expand === TRUE) {
        $sql = e4s_getSqlForIndivOrders($compRow['ID']);
    } else {
        $sql = 'SELECT count(e.id) count, e.price pricepaid, ep.name pricename, ep.price, ep.saleprice, ep.salefee, ep.fee, e.options
            FROM ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTPRICE . ' ep
            WHERE ce.compid = ' . $compRow['ID'] . '
            and   e.compeventid = ce.id
            and   ce.priceid    = ep.id
            and   e.paid = ' . E4S_ENTRY_PAID . '
            group by e.price, ep.price, ep.saleprice, ep.salefee, ep.fee, e.options';
    }

    $retObj = new stdClass();
    $retObj->totalCnt = 0;
    $retObj->totalAmount = 0;
    $retObj->totalAmountActual = 0;
    $retObj->totalFee = 0;
    $retObj->summary = array();
//    e4s_dump($retObj);
//    e4s_echo(">>>>>>>>>>>>>>> 8 <<<<<<<<<<<<<<<<");
//    $debug = true;
    outputCompValues($sql, $config, $expand, $retObj);
//    e4s_echo(">>>>>>>>>>>>>>> 9 <<<<<<<<<<<<<<<<");

    if ($expand === TRUE) {
        $sql = e4s_getSqlForTeamOrders($compRow['ID']);
    } else {
        $sql = 'SELECT count(e.id) count, e.price pricepaid, ep.name pricename, ep.price, ep.saleprice, ep.salefee, ep.fee, e.options
            FROM ' . E4S_TABLE_EVENTTEAMENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTPRICE . ' ep
            WHERE ce.compid = ' . $compRow['ID'] . '
            and   e.ceid = ce.id
            and   ce.priceid    = ep.id
            and   e.paid = ' . E4S_ENTRY_PAID . '
            group by e.price, ep.price, ep.saleprice, ep.salefee, ep.fee';
    }

    outputCompValues($sql, $config, $expand, $retObj);
    echo '<tr>';
    echo "<td colspan='1'>";
    outputDataWithLabel('Total Entries', $retObj->totalCnt);

    $GLOBALS['grantCount'] += $retObj->totalCnt;
    echo '</td>';

    echo "<td colspan='1'>";
    outputDataWithLabel('Total Amount', convertToCurrency($config, $retObj->totalAmount));

    $GLOBALS['grandPaid'] += $retObj->totalAmount;
    $GLOBALS['grandPaidActual'] += $retObj->totalAmountActual;
    echo '</td>';

    echo "<td colspan='1'>";
    outputDataWithLabel('Total Fees', convertToCurrency($config, $retObj->totalFee));

    $GLOBALS['grandFee'] += $retObj->totalFee;
    echo '</td>';
    echo '</tr>';

}

function e4s_getSqlForTeamOrders($compid) {
    return 'SELECT e.periodStart created, e.orderid count, e.ceid ceId, e.price pricepaid, e.name teamName, ep.name pricename, ep.price, ep.saleprice, ep.salefee, ep.fee, e.options
            FROM ' . E4S_TABLE_EVENTTEAMENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTPRICE . ' ep
            WHERE ce.compid = ' . $compid . '
            and   e.ceid = ce.id
            and   ce.priceid    = ep.id
            and   e.paid = ' . E4S_ENTRY_PAID . ' ';
}

function e4s_getSqlForIndivOrders($compid) {
    return "SELECT e.periodStart created, e.orderid count, ce.maxGroup egId, e.compeventid ceId, e.price pricepaid, ep.name pricename, ep.price, ep.saleprice, ep.salefee, ep.fee, e.options, a.id aId, concat(a.firstname, ' ',a.surname) athlete
            FROM " . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_ATHLETE . ' a,
                 ' . E4S_TABLE_EVENTPRICE . ' ep
            WHERE ce.compid = ' . $compid . '
            and   e.compeventid = ce.id
            and   ce.priceid    = ep.id
            and   e.athleteid   = a.id
            and   e.paid = ' . E4S_ENTRY_PAID;
}

function outputCompValues($sql, $config, $expand, &$retObj, $entryCountObj = null) {
//e4s_dump($sql,"SQL",true);
    $entryResult = e4s_queryNoLog($sql);
    $rows = $entryResult->fetch_all(MYSQLI_ASSOC);
    $totalCnt = 0;
    $totalAmount = 0;
    $totalAmountActual = 0;
    $totalFee = 0;
    $compObj = e4s_getCompObj($entryCountObj->compId);
    foreach ($rows as $row) {
        if (array_key_exists('aId', $row)) {
            // Indiv Event, checking waiting list
            $athleteId = (int)$row['aId'];
            $onWaitingList = $compObj->isAthleteOnWaitingListForId($athleteId, (int)$row['egId']) ;
            if ($onWaitingList) {
                // Not interested if on waiting list
                continue;
            }
        }

        $options = e4s_getOptionsAsObj($row['options']);
        echo '<tr>';
        echo '<td>';
        $name = $row['pricename'];
        if (isset($options->price) and isset($options->athletecnt)) {
            // team event price per indiv, so multiple by cnt
            if ($options->price === E4S_PRICE_PER_ATHLETE and $options->athletecnt > 0) {
                $name .= ' ( ' . $options->athletecnt . ' athletes )';
            }
        }

        if (isset($retObj->orderOnly)) {
            outputDataWithLabel('', $row['count']);
        } else {
            outputDataWithLabel('', $row['count'] . ' : ' . $name);
        }

        echo '</td>';
        echo '<td>';
        if ($expand) {
            $subAmount = (float)$row['pricepaid'];
            $subAmountActual = (float)$row['pricepaid'];
            $totalCnt += 1;
        } else {
            $subAmount = ((double)$row['count'] * (float)$row['pricepaid']);
            $subAmountActual = ((double)$row['count'] * (float)$row['pricepaid']);
            $totalCnt += (double)$row['count'];
        }

        $totalAmount += $subAmount;
        $totalAmountActual += $subAmountActual;
        if ($expand) {
            outputDataWithLabel('', convertToCurrency($config, $subAmount));
        } else {
            outputDataWithLabel('', convertToCurrency($config, $subAmount) . ' ( ' . $row['pricepaid'] . ' )');
        }

        echo '</td>';
        echo '<td>';

        if ($expand) {
            $row['count'] = 1;
        }
        $reason = 'N/A';
        if (array_key_exists('athlete', $row)) {
            $reason = $row['athlete'];
        } elseif (array_key_exists('teamName', $row)) {
            $reason = $row['teamName'];
        }
//        e4s_dump($row,"Row",true);
        if ($row['pricepaid'] === $row['price']) {
            $fee = $row['fee'];
        } else {
            if ((int)$row['pricepaid'] !== 0) {
                $fee = $row['salefee'];
            } else {
                $fee = $row['salefee'];
                if (isset($options->fee) and $options->fee === FALSE) {
                    $fee = 0.00;
                }
                if (isset($options->reason) and $options->reason !== '') {
                    $reason .= ' : ' . $options->reason;
                }
            }
        }
        if (isset($options->price) and isset($options->athletecnt)) {
            // team event price per indiv, so multiple by cnt
            if ($options->price === E4S_PRICE_PER_ATHLETE and $options->athletecnt > 0) {
                $fee = $fee * $options->athletecnt;
            }
        }
        $subTotal = ((double)$row['count'] * (float)$fee);
//        if ( $expand ){
        outputDataWithLabel('', convertToCurrency($config, $subTotal));
//        }else{
//            outputDataWithLabel("" ,convertToCurrency($config, $subTotal)  . " ( " . $fee . $reason . " )") ;
        //        }
        echo '</td>';
        echo '<td>';
        outputDataWithLabel('', $row['created']);
        echo '</td>';
        echo '<td>';
        echo $reason;
        echo '</td>';
        $totalFee += $subTotal;
        echo '</tr>';
        if (array_key_exists('p-' . $row['pricepaid'], $retObj->summary)) {
            $cnt = $retObj->summary['p-' . $row['pricepaid']];
            $retObj->summary['p-' . $row['pricepaid']] = $cnt + 1;
        } else {
            $retObj->summary['p-' . $row['pricepaid']] = 1;
        }

    }
    $retObj->totalCnt = $retObj->totalCnt + $totalCnt;
    $retObj->totalAmount = $retObj->totalAmount + $totalAmount;
    $retObj->totalAmountActual = $retObj->totalAmountActual + $totalAmountActual;
    $retObj->totalFee = $retObj->totalFee + $totalFee;

    return $retObj;
}

function outputDataWithLabel($label, $data) {
    if ($label !== '') {
        echo $label;
        if ($data !== '') {
            echo ' : ';
        }
    }
    echo $data;
}