<?php
require_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_showEntriesV2($obj) {
	$compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
	if ( is_null($compId) ) {
        Entry4UIError(9701, 'This report is unavailable');
    }
    $reportObj = new entryV2Report($compId);
    $reportObj->outputReport();
	exit();
}
class entryV2Report {
    var e4sCompetition $compObj;
    var $eventGroups;
    var $entries;
    var $athleteIds;
    var $athleteObjs;
    var $rankings;
    var perfAthlete $perfObj;
    var $performances;
    var $standards;
    var $egEventIds;
    var $ceObjs;
    function __construct($compId){
	    $this->compObj = e4s_getCompObj($compId);
    }

    function outputReport() {
        e4s_webPageHeader();
        $this->_getData();
        $this->_outputHeader();
        $this->_outputBody();
        $this->_outputFooter();
    }
    private function _outputHeader(){
	    $uiTheme = E4S_JQUERY_THEME;
        ?>
            <html>
                <head>
                    <link rel="stylesheet" href="<?php echo E4S_PATH ?>/reports/e4sReports.css">
                    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/<?php echo $uiTheme ?>/jquery-ui.css"/>
                    <link rel="stylesheet" id="mat_icons-css" href="https://fonts.googleapis.com/icon?family=Material+Icons&amp;ver=6.4.2" type="text/css" media="all">
                    <script type="text/javascript" src="<?php echo E4S_PATH ?>/reports/commonReports.js"></script>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
                    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>

                    <style>
                        .e4sHeader {
                            height: 45px;
                            background-color: #1e40af;
                        }
                        .e4sLogo {
                            width: 85px;
                        }
                        .PromptHolder {
                            position: absolute;
                            top: 35%;
                            left: 35%;
                            margin-top: -50px;
                            margin-left: -50px;
                            width: 30%;
                            height: 30%;
                        }
                        .e4s_qualify_entry {
                            background-color: gold;
                        }
                        .datePicker {
                            width: 100px;
                        }
                        .red-text {
                            color: #F44336;
                        }
                        .inherit {
                            font-size: inherit;
                        }
                        td {
                            border-bottom: 1px solid lightgrey;
                        }
                        .filter_type {
                            width: 7%;
                        }
                        .filter_gender {
                            width: 7%;
                        }
                        .filter_athlete {
                            width: 18%;
                        }
                        .filter_eventgroup {
                            width: 18%;
                        }
                        .table_header {
                            background-color: lightgrey;
                            color: black;
                            font-weight: bold;
                        }
                        .table_header_date {
                            width: 30%;
                        }
                        .table_header_event {
                            width: 30%;
                        }
                        .table_header_standard {
                            width: 40%;
                        }
                        .align_left{
                            text-align: left;
                        }
                        .align_justify {
                            text-align: justify;
                        }
                        .options_th {
                            width: 5%;
                        }
                        .athlete_th {
                            width: 18%;
                        }
                        .gender_th {
                            width: 5%;
                        }
                        .club_th {
                            width: 18%;
                        }
                        .urn_th {
                            width: 7%;
                        }
                        .age_th {
                            width: 5%;
                        }
                        .pb_th {
                            width: 5%;
                        }
                        .pbAchieved_th{
                            width: 7%;
                        }
                        .sb_th{
                            width: 5%;
                        }
                        .sbAchieved_th {
                            width: 7%;
                        }
                        .rank_th {
                            width: 5%;
                        }
                        .qp_th {
                            width: 5%;
                        }
                        .date_th {
                            width: 8%;
                        }
                        .pinkClass {
                            background-color: pink;
                        }
                        .goldClass {
                            background-color: gold;
                        }
                        .datePinkClass {
                            background-color: pink;
                        }
                        .dateGoldClass {
                            background-color: gold;
                        }
                    </style>
                    <script>
                        let e4sGlobal = {};
                        window.addEventListener(
                            "message",
                            (event) => {
                                if (event.origin !== "https://<?php echo E4S_CURRENT_DOMAIN ?>") return;
                                $("#e4sPromptHolder").html("");
                                $("#e4sPromptHolder").removeClass("PromptHolder");
                                if (event.data.type === "pbSaved") {
                                    // debugger;
                                    let entryId = event.data.payload.entryId;
                                    let perf = event.data.payload.perfInfo;
                                    updateSeedPerfDisplay(entryId, perf);
                                }
                            },
                            false,
                        );
                        let filterTimeout = null;
<?php
if ( $this->_isOrganiser() ) {
    ?>
                        function getFormattedPerf(type, perf){
                            perf = parseFloat(perf).toFixed(2);
                            if (type !== '<?php echo E4S_EVENT_FIELD ?>' && perf > 59.59) {
                                let mins = Math.floor(perf / 60);
                                let secs = perf - (mins * 60);
                                let prefix = '';
                                if (secs < 10) {
                                    prefix = '0';
                                }
                                perf = mins + ':' + prefix + secs.toFixed(2);
                            }
                            return perf;
                        }
                        function editSeedPerf(entryId){
                            let divName = "e4sUpdatePerf";
                            $("#" + divName).remove();

                            let divHTML = "<div id='" + divName + "'>";
                            divHTML = '<div class="e4sHeader">';
                            divHTML += '<img src="/resources/e4s_logo.png" class="e4sLogo">';
                            divHTML += '</div>';
                            divHTML += '<iframe id="' + divName + '" src="https://<?php echo E4S_CURRENT_DOMAIN ?>/#/athlete-pb/' + entryId + '" style="background:white; width:95.6%; height:96%; border:1px solid; padding: 2%;"></iframe>';
                            divHTML += '</div>';
                            $("#e4sPromptHolder").html(divHTML);
                            $("#e4sPromptHolder").addClass("PromptHolder");
                        }
                        function updateSeedPerfDisplay(entryId, perf){
                            // value needs to be 2 decimals OR shown in minutes if track
                            let entryObj = $("[entryid=" + entryId + "]");
                            let qpObj = entryObj.find("[id*=qp]");
                            qpObj.attr("e4svalue", perf.perf);
                            let html = '<a href="#" onclick="editSeedPerf(' + entryId + '); return false;"><i class="material-icons red-text inherit">edit</i>';
                            if ( perf.perfText[0] === '0'){
                                perf.perfText = perf.perfText.slice(1);
                            }
                            if ( perf.perfText === ''){
                                // should be formatted
                                perf.perfText = perf.perf;
                            }
                            html += perf.perfText;
                            html += '</a>';

                            qpObj.html(html);
                            updatePerfCSS();
                        }

                        function launchOptionsForEntry(entryId, athleteId, clubId){
                            var url = "/#/actions/<?php echo $this->compObj->getID() ?>/" + athleteId + "/" + clubId + "/" + entryId;
                            window.open(url, '_self');
                        }
                        function toggleAllRank(){
                            $(".e4sRankAll").toggle();
                        }
                        function updateDateChecked(){
                            const d = new Date();
                            let today = e4sShortDate(d);
                            let dateId = "#" + event.currentTarget.parentElement.id;
                            $( dateId ).html(today);
                        }
                        function updateAthletePof10(urn){
                            updateDateChecked();
                            let url = '<?php echo E4S_BASE_URL ?>athlete/updatepof10/' + urn + '?force=true';
                            // showPleaseWait(true, "Updating Athlete Pof10");
                            $.ajax({
                                url: url,
                                data: null,
                                timeout: 3000, // 3 seconds
                                error: function (jqXHR, textStatus, errorThrown) {
                                    // showPleaseWait(false);
                                    e4sAlert('Failed to update Athlete Pof10 information', 'Error');
                                },
                                success: function (resp) {
                                    updateAthleteInfo(resp);
                                    updatePerfCSS();
                                }
                            });
                        }
                        function updateAthleteInfo(resp){
                            let data = resp.data;
                            let athleteId = data.athleteId;
                            let athletePerfs = data.perfs;
                            let athleteRanking;

                            // format rankings as per the loading method
                            let rankings = [];
                            for (let athleteInfo in data.rankings ){
                                let indivRankingId = data.rankings[athleteInfo];
                                for ( let index in indivRankingId ){
                                    let rankObj = indivRankingId[index];
                                    let key = athleteId + '_' + rankObj.eventId;
                                    if ( !rankings.hasOwnProperty(key)){
                                        athleteRanking = {};
                                        athleteRanking.athleteId = athleteId;
                                        athleteRanking.eventId = rankObj.eventId;
                                        rankings[key] = athleteRanking;
                                    }else{
                                        athleteRanking = rankings[key];
                                    }
                                    if ( rankObj.ageGroup === 'ALL' ) {
                                        athleteRanking.rankAll = rankObj.rank;
                                    }else {
                                        athleteRanking.rank = rankObj.rank;
                                    }
                                }

                            }
                            for ( rankKey in rankings ){
                                let rankObj = rankings[rankKey];
                                let rankId = ".rank_" + athleteId + "_" + rankObj.eventId;

                                let rankNode = $( rankId );

                                let rankText = '';
                                let space = '';
                                if ( rankObj.rank ){
                                    rankText = rankObj.rank;
                                    if ( rankText !== '' ){
                                        space = ' ';
                                    }
                                }
                                if ( rankObj.rankAll && rankObj.rankAll !== '' ){
                                    rankText += space + '<span class="e4sRankAll" style="display:none;">(' + rankObj.rankAll + ')</span>';
                                }
                                rankNode.html(rankText);
                            }

                            for ( eventId in athletePerfs ){
                                let perf = athletePerfs[eventId];
                                let age = perf.curAgeGroup;
                                let sbText = perf.sbText;
                                let pbText = perf.pbText;
                                let sbAchieved = perf.sbAchieved ;
                                if ( sbAchieved !== null ){
                                    sbAchieved = e4sShortDateFromStr(sbAchieved);
                                }else{
                                    sbAchieved = '';
                                }
                                let pbAchieved = perf.pbAchieved;
                                if ( pbAchieved !== null ){
                                    pbAchieved = e4sShortDateFromStr(pbAchieved);
                                }else{
                                    pbAchieved = '';
                                }

                                let ageId = "#age_" + athleteId + "_" + eventId;
                                let sbId = "#SB_" + athleteId + "_" + eventId;
                                let pbId = "#PB_" + athleteId + "_" + eventId;
                                let sbAchievedId = "#sbAchieved_" + athleteId + "_" + eventId;
                                let pbAchievedId = "#pbAchieved_" + athleteId + "_" + eventId;

                                let ageNode = $( ageId );
                                let sbNode = $( sbId );
                                let pbNode = $( pbId );
                                let sbAchievedNode = $( sbAchievedId );
                                let pbAchievedNode = $( pbAchievedId );

                                ageNode.html(age);
                                sbNode.html(sbText);
                                updateE4SValue(sbNode, perf.sb);
                                pbNode.html(pbText);
                                updateE4SValue(pbNode, perf.pb);
                                sbAchievedNode.html(sbAchieved);
                                pbAchievedNode.html(pbAchieved);
                            }
                        }
                        function updateE4SValue(node, value){
                            if ( node.length > 0 ){
                                node.attr("e4svalue", value);
                            }else{
                                node.attr("e4svalue", 0);
                            }
                        }
                        function e4sShortDateFromStr(dateStr){
                            return e4sShortDate(new Date(dateStr));
                        }
                        function e4sShortDate(date){
                            let dateStr = date.toLocaleDateString('en-GB', {  year:"numeric", month:"short", day:"2-digit"});
                            return dateStr.replace(/\//g, ' ');
                        }
                        function initBasedOn(){
                            let items = [
                                {value: 'SB', text: 'Seasons Best'},
                                {value: 'PB', text: 'Personal Best'}
<?php
   if (sizeof($this->standards) > 0 ) {
       foreach($this->standards as $standard=>$standards){
           ?>
                                ,{value: '<?php echo $standard; ?>', text: '<?php echo $standard; ?>'}
           <?php
       }
   }
?>
                            ];
                            $.each(items, function (i, item) {
                                $('#basedOn').append($('<option>', {
                                    value: item.value,
                                    text : item.text
                                }));
                                if ( item.value === 'SB' || item.value === 'PB' ){
                                    $('#datebasedOn').append($('<option>', {
                                        value: item.value,
                                        text : item.text
                                    }));
                                }
                            });
                        }
                        function initDatePicker(){
                            $( "#datepickerFrom" ).datepicker(
                                {
                                    showOn: "both",
                                    defaultDate: -365,
                                    buttonImage: "/resources/calendar.gif",
                                    buttonImageOnly: false,
                                    buttonText: "Select From Date",
                                    minDate: -2000,
                                    maxDate: "+0D",
                                    changeMonth: true,
                                    changeYear: true,
                                    dateFormat: "d M yy"
                                }
                            );
                            $( "#datepickerTo" ).datepicker(
                                {
                                    showOn: "both",
                                    defaultDate: +0,
                                    buttonImage: "/resources/calendar.gif",
                                    buttonImageOnly: false,
                                    buttonText: "Select To Date",
                                    minDate: -2000,
                                    maxDate: "+0D",
                                    changeMonth: true,
                                    changeYear: true,
                                    dateFormat: "d M yy"
                                }
                            );
                        }
                        function initSlider() {
                            $("#slider-variance").slider({
                                range: "min",
                                min: 0,
                                max: 50,
                                step: 0.5,
                                value: 5,
                                slide: function (event, ui) {
                                    $("#maxRank").val(ui.value);
                                    updatePerfCSS(ui.value);
                                }
                            });
                            let defValue = $( "#slider-variance" ).slider( "value" );
                            $( "#maxRank" ).val( defValue );
                            updatePerfCSS(defValue);
                        }
                        function updatePerfDateCSS(){
                            let basedOn = $("#datebasedOn").val().toLowerCase();
                            let basedOnFromDate = $( "#datepickerFrom" ).datepicker( "getDate" );
                            let fromDate = null;
                            if ( basedOnFromDate !== null) {
                                fromDate = basedOnFromDate.getFullYear();

                                if (basedOnFromDate.getMonth() < 9) {
                                    fromDate += '0';
                                }
                                fromDate += (basedOnFromDate.getMonth() + 1);
                                if (basedOnFromDate.getDate() < 10) {
                                    fromDate += '0';
                                }
                                fromDate += basedOnFromDate.getDate();
                            }
                            let basedOnToDate = $( "#datepickerTo" ).datepicker( "getDate" );
                            let toDate = null;
                            if ( basedOnToDate !== null) {
                                toDate = basedOnToDate.getFullYear();

                                if (basedOnToDate.getMonth() < 9) {
                                    toDate += '0';
                                }
                                toDate += (basedOnToDate.getMonth() + 1);
                                if (basedOnToDate.getDate() < 10) {
                                    toDate += '0';
                                }
                                toDate += basedOnToDate.getDate();
                            }
                            let className = basedOn + 'Achieved';
                            $("." + className).each(
                                function(index,item){
                                    let node = $(this);
                                    let nodeValue = node.attr("e4sachieved");
                                    let colour = '';

                                    if (nodeValue !== '' ) {
                                        if ( fromDate !== null) {
                                            if (nodeValue >= fromDate) {
                                                colour = "gold";
                                            } else {
                                                colour = "pink";
                                            }
                                        }
                                        if ( toDate !== null) {
                                            if (nodeValue > toDate) {
                                                colour = "pink";
                                            }else if ( colour !== 'pink' ){
                                                colour = "gold";
                                            }
                                        }
                                    }
                                    applyVarianceClass(node, colour);
                                }
                            )
                        }
                        function displayStatus(){
                            let gold = $("#showGold").prop("checked");
                            let pink = $("#showPink").prop("checked");
                            let clear = $("#showClear").prop("checked");

                            if ( !gold ) {
                                $(".goldClass").closest("tr").hide();
                            }
                            if ( !pink ) {
                                $(".pinkClass").closest("tr").hide();
                            }
                            if ( !clear ) {
                                $(".clearClass").closest("tr").hide();
                            }
                        }
                        function applyVarianceClass(node, colour){
                            node.removeClass("pinkClass");
                            node.removeClass("goldClass");
                            node.removeClass("clearClass");
                            if ( colour === '' ) {
                                colour = 'clear';
                            }
                            node.addClass(colour + "Class");
                        }
                        function updateCounters(){
                            let filteredCount = $("[entryid]:visible").length;
                            let pinkCount = $(".pinkClass:not(.sbAchieved)").length;
                            let goldCount = $(".goldClass:not(.sbAchieved)").length;
                            let totalCount = $("[entryid]").length;
                            e4sGlobal.filtered = filteredCount !== totalCount;
                            let inVarCount = totalCount - pinkCount - goldCount;

                            let pinkPercent = Math.round((pinkCount/totalCount) * 100);
                            let goldPercent = Math.round((goldCount/totalCount) * 100);
                            let inVarPercent = Math.round((inVarCount/totalCount) * 100);
                            let html = 'Total: ' + totalCount + ' => Red: ' + pinkCount + ' (' + pinkPercent + '%) Clear: ' + inVarCount + ' (' + inVarPercent + '%) Gold: ' + goldCount + ' (' + goldPercent + '%)';
                            if ( e4sGlobal.filtered ){
                                pinkCount = $(".pinkClass:not(.sbAchieved):visible").length;
                                pinkPercent = Math.round((pinkCount/filteredCount) * 100);
                                goldCount = $(".goldClass:not(.sbAchieved):visible").length;
                                goldPercent = Math.round((goldCount/filteredCount) * 100);
                                inVarCount = filteredCount - pinkCount - goldCount;
                                inVarPercent = Math.round((inVarCount/filteredCount) * 100);
                                html += '<br>Filtered Total: ' + filteredCount + ' => Red: ' + pinkCount + ' (' + pinkPercent + '%) Clear: ' + inVarCount + ' (' + inVarPercent + '%) Gold: ' + goldCount + ' (' + goldPercent + '%)';
                            }
                            // Global Counts
                            $("#varianceCounts").html(html);
                            // Each Event Counts.
                            $(".e4sEventGroup").each(
                                function(index, item){
                                    let egObj = $(this);
                                    let egId = egObj.attr("id").replace("eg_","");

                                    let pinkCount = egObj.find(".pinkClass:not(.sbAchieved)").length;
                                    let goldCount = egObj.find(".goldClass:not(.sbAchieved)").length;
                                    let totalCount = egObj.find("[entryid]").length;
                                    let inVarCount = totalCount - pinkCount - goldCount;

                                    let pinkPercent = Math.round((pinkCount/totalCount) * 100);
                                    let goldPercent = Math.round((goldCount/totalCount) * 100);
                                    let inVarPercent = Math.round((inVarCount/totalCount) * 100);
                                    let html = 'Red: ' + pinkCount + ' (' + pinkPercent + '%) Clear: ' + inVarCount + ' (' + inVarPercent + '%) Gold: ' + goldCount + ' (' + goldPercent + '%)';
                                    if ( e4sGlobal.filtered ){
                                        pinkCount = egObj.find(".pinkClass:not(.sbAchieved):visible").length;
                                        goldCount = egObj.find(".goldClass:not(.sbAchieved):visible").length;
                                        totalCount = egObj.find("[entryid]:visible").length;
                                        inVarCount = totalCount - pinkCount - goldCount;

                                        pinkPercent = Math.round((pinkCount/totalCount) * 100);
                                        goldPercent = Math.round((goldCount/totalCount) * 100);
                                        inVarPercent = Math.round((inVarCount/totalCount) * 100);
                                        html += '<br>(' + totalCount + ' filtered entries) Red: ' + pinkCount + ' (' + pinkPercent + '%) Clear: ' + inVarCount + ' (' + inVarPercent + '%) Gold: ' + goldCount + ' (' + goldPercent + '%)';
                                    }
                                    $("#egCount_" + egId).html(html);
                                }
                            )
                        }
                        function updatePerfCSS(variance){
// return;
                            if ( typeof variance === 'undefined' ){
                                variance = parseFloat($( "#maxRank" ).val());
                            }
                            let basedOn = $("#basedOn").val();
                            let egId = 0;
                            let lastEgId = 0;
                            let egType = '';
                            let checkAgainstValue = 0;

                            $(".e4sDiff").each(
                                function(index,item){
                                    let diff = 100;
                                    let node = $(this);
                                    let nodeValue = parseFloat(node.attr("e4svalue"));

                                    if ( basedOn === 'SB' || basedOn === 'PB' ) {
                                        let checkAgainstNodeId = node[0].id.replace("qp",basedOn);
                                        let checkAgainstNode = $("#" + checkAgainstNodeId );
                                        checkAgainstValue = parseFloat(checkAgainstNode.attr("e4svalue"));
                                    }else{
                                        let egId = parseInt(node.attr("egid"));
                                        if ( egId !== lastEgId ){
                                            // set eventType
                                            let egNode = $("#egname_" + egId);
                                            egType = egNode.attr("e4stype").toUpperCase();
                                            let checkAgainstNodeId = basedOn + "-" + egId;
                                            let checkAgainstNode = $("#" + checkAgainstNodeId );
                                            checkAgainstValue = parseFloat(checkAgainstNode.attr("stdvalue"));
                                            lastEgId = egId;
                                        }
                                    }

                                    if ( nodeValue !== 0 || checkAgainstValue !== 0 ){
                                        diff = (nodeValue * 100)/(checkAgainstValue * 100);
                                        diff = Math.round(diff) / 100;
                                        diff = Math.abs(diff);
                                    }
                                    let colour = '';

                                    if (nodeValue === 0 || checkAgainstValue > 0 ) {
                                        if (basedOn === 'SB' || basedOn === 'PB') {
                                            if (diff > variance) {
                                                colour = "pink";
                                            }
                                        } else {
                                            // standard value
                                            if (nodeValue > 0 && nodeValue <= checkAgainstValue && egType === '<?php echo E4S_EVENT_TRACK ?>') {
                                                colour = "gold";
                                            } else if (nodeValue >= checkAgainstValue && egType === '<?php echo E4S_EVENT_FIELD ?>') {
                                                colour = "gold";
                                            } else if (diff > variance || nodeValue === 0) {
                                                colour = "pink";
                                            }
                                        }
                                    }
                                    applyVarianceClass(node, colour);
                                }
                            )
                            updateCounters();
                        }
                        function updateAllAthletes(){
                            e4sConfirm("This will update all athletes in this competition that have not been updated in past 24 hours.<br>Are you sure?", "Update ALL Athletes performance", function(){
                                let url = '<?php echo E4S_BASE_URL ?>checkCompPof10/<?php echo $this->compObj->getID() ?>';
                                // showPleaseWait(true, "Updating Athlete Pof10");
                                $.ajax({
                                    url: url,
                                    data: null,
                                    timeout: 3000, // 3 seconds
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        e4sAlert('The updates have been requested. Please re-visit this page once complete.', 'Information');
                                    },
                                    success: function (resp) {
                                        e4sAlert('Complete. The page will now reload', 'Information', function(){
                                            location.reload();
                                        });
                                    }
                                });
                            });
                        }
                        function updateAllAthletesForEgId(egId, eventName){
                            e4sConfirm("This will update all athletes in " + eventName + " that have not been updated in past 24 hours.<br>Are you sure?", "Update " + eventName + " performances", function(){
                                let url = '<?php echo E4S_BASE_URL ?>checkCompPof10/<?php echo $this->compObj->getID() ?>/' + egId;
                                // showPleaseWait(true, "Updating Athlete Pof10");
                                $.ajax({
                                    url: url,
                                    data: null,
                                    timeout: 3000, // 3 seconds
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        e4sAlert('The updates have been requested. Please re-visit this page once complete.', 'Information');
                                    },
                                    success: function (resp) {
                                        e4sAlert('Complete. The page will now reload', 'Information', function(){
                                            location.reload();
                                        });
                                    }
                                });
                            });
                        }
                        $().ready( function(){
                            initBasedOn();
                            initSlider();
                            initDatePicker();
                        })
     <?php
    }
    ?>

                    function clearFilters(){
                        $("#filter_eventgroup").val('');
                        $("#filter_athlete").val('');
                        $("#filter_gender").val('');
                        $("#filter_club").val('');
                        $("#filter_type").val('');
                        processFilter();
                    }
                    function getFilterValue(filterName, attributeName, criteria){
                        let filterValue = $("#" + filterName).val();
                        if ( filterValue !== '' ){
                            return '[' + attributeName + ']:not([' + attributeName + criteria + '"' + filterValue + '" i])';
                        }
                        return '';
                    }
                    function autoProcessFilter(){
                        if ( filterTimeout !== null ){
                            clearTimeout(filterTimeout);
                        }
                        filterTimeout = setTimeout(processFilter, 750);
                    }
                    function initEventHeaders(){
                        $("[e4sathlete]").show(); // reset display
                        $("[id*=eg]").show();
                        $(".goldClass").closest("tr").show();
                        $(".pinkClass").closest("tr").show();
                        $(".clearClass").closest("tr").show();
                    }
                    function hideEmptyEvents(){
                        $("[id*=eg_]").each(
                            function(index, item){
                                let egId = $(this).attr("id");
                                let visibleRows = $("#" + egId).find("tr:visible").length;
                                if ( visibleRows < 3 ){
                                    $("#" + egId).hide();
                                }
                            }
                        )
                    }
                    function processFilter(){
                        if ( filterTimeout !== null ){
                            clearTimeout(filterTimeout);
                        }
                        initEventHeaders();
                        displayStatus();
                        let filter_Selectors = [];
                        filter_Selectors.push(getFilterValue('filter_eventgroup','e4seventgroup', '*='));
                        filter_Selectors.push(getFilterValue('filter_athlete','e4sathlete', '*='));
                        filter_Selectors.push(getFilterValue('filter_club','e4sclub', '*='));
                        filter_Selectors.push(getFilterValue('filter_gender','e4sgender', '='));
                        filter_Selectors.push(getFilterValue('filter_type','e4seventtype', '='));

                        filter_Selectors = filter_Selectors.filter(n => n).join(",");
                        if ( filter_Selectors !== '' ) {
                            $(filter_Selectors).hide();
                        }

                        hideEmptyEvents();
                        updateCounters();
                    }
                    </script>
                </head>
                <body>
        <div id="e4sPromptHolder"></div>
        <div style="">
            <table class="e4sTable card_table card_header">
                <tr>
                    <td>
                        <span class="card_header_label card_header_e4s" id="card_title">Performance / Seeding Report</span>
                    </td>
                    <td>
                        <span class="card_header_label">Date : </span>
                        <span id="card_date" class="card_header_data"><?php echo $this->_getFormattedDate($this->compObj->getDate(), 0); ?></span>
                    </td>
                    <td>
                        <span class="card_header_label">Venue : </span>
                        <span id="card_venue" class="card_header_data"><?php echo $this->compObj->getLocationName() ?></span>
                    </td>
                    <td>
                        <span class="card_header_label">Meeting : </span>
                        <span id="card_competition" class="card_header_data"><?php echo $this->compObj->getName() ?></span>
                    </td>
                </tr>
<?php
    if ( $this->_isOrganiser() ) {
?>
                <tr>
                    <td colspan="4">
                        <table style="width: 100%">
                            <tr>
                                <td>
                                    Based On : <select id="basedOn" onchange="updatePerfCSS();"></select>
                                    Variance : <input type="text" id="maxRank" readonly style="background: lightgrey; width: 30px; border:0; font-weight:bold;">%
                                    <span id="slider-variance" style="display: inline-block; width:200px; height:16px; margin: 0px 10px;"></span>
                                </td>
                                <td>
                                    <span>&nbsp;</span>
                                </td>
                                <td>
                                    Date Based On : <select id="datebasedOn" onchange="updatePerfDateCSS();"></select> From:<input class="datePicker" type="text" id="datepickerFrom" onchange="updatePerfDateCSS();"> To:<input class="datePicker" type="text" id="datepickerTo" onchange="updatePerfDateCSS();">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <span id="varianceCounts"></span>
                                </td>
                                <td>
                                    Show :
                                    <input type="checkbox" id="showPink" checked onchange="processFilter();">Red
                                    <input type="checkbox" id="showClear" checked onchange="processFilter();">Clear
                                    <input type="checkbox" id="showGold" checked onchange="processFilter();">Gold
                                </td>
                            </tr>
                        </table>
                    </td>

                    <td>
                        <span class="updateLink"><a href="#" onclick="updateAllAthletes(); return false;">Update All Athletes</a></span>
                    </td>

                    <td>
                        <span class="updateRankLink"><a href="#" onclick="toggleAllRank(); return false;">Toggle All Ranking Display</a></span>
                    </td>
                </tr>
<?php
    }
?>
                <tr>
                    <td colspan="6">
                    <table class="e4sTable" style="width:100%;">
                        <tr class="table_header">
                            <td class="options_th">Filter<br><a href="#" onclick="clearFilters();return false;">Clear</a></td>
                            <td class="filter_athlete"><label for="filter_athlete">Athlete</label><br>
                                <select id="filter_gender" onchange="processFilter();">
                                    <option value="">All</option>
                                    <option value="F">Female</option>
                                    <option value="M">Male</option>
                                </select>
                                <input id="filter_athlete" value="" onkeyup="autoProcessFilter();" onchange="processFilter();">
                            </td>
                            <td class="filter_eventgroup"><label for="filter_eventgroup">Event</label><br>
                                <select id="filter_type" onchange="processFilter();">
                                    <option value="">All</option>
                                    <option value="<?php echo E4S_EVENT_FIELD ?>">Field</option>
                                    <option value="<?php echo E4S_EVENT_TRACK ?>">Track</option>
                                </select>
                                <input id="filter_eventgroup" value="" onkeyup="autoProcessFilter();" onchange="processFilter();">
                            </td>

                            <td class="filter_club"><label for="filter_club">Organisation</label><br><input id="filter_club" value="" onkeyup="autoProcessFilter();" onchange="processFilter();"></td></td>
                        </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td class="card_header_gap" colspan="6">
                        <span>&nbsp;</span>
                    </td>
                </tr>
            </table>
        </div>
    <?php

    }
    private function _outputBody(){
        ?>
            <div style="height:90%; overflow: scroll;">
        <?php
        foreach($this->eventGroups as $eventGroup){
            ?>
                <div id="<?php echo 'eg_' . $eventGroup->id ?>" class="e4sEventGroup">
            <?php
            $this->_outputEventHeader($eventGroup);
            $this->_outputEntries($eventGroup);
            ?>
                </div>
            <?php
        }
        ?>
    </div>
	<?php
    }
    private function _isOrganiser(){
        return $this->compObj->isOrganiser();
    }
    private function _outputEventHeader($eventGroup){
	    $eventTime= $this->_getFormattedDateTime($eventGroup->startDate);
        $maxAthletes = $eventGroup->maxAthletes;
        if ( $maxAthletes > 0 ) {
            $maxAthletes = '/' . $maxAthletes;
        }else{
            $maxAthletes = '';
        }
        $eventStandards = $this->_getEventStandards($eventGroup);
        if ( !isset($eventGroup->entries)) {
	        $eventGroup->entries = [];
        }
        ?>
        <br>
        <table class="e4sTable card_table card_header">
            <tr>
                <td class="table_header_date">
                    <span class="card_header_label card_header_e4s" id="event_time"><?php echo $eventTime ?></span>
                </td class="table_header_event">
                <td>
                    <span class="table_header_event card_header_data card_header_e4s"
                          id="<?php echo 'egname_' . $eventGroup->id ?>"
                          e4stype="<?php echo $eventGroup->type ?>"
                          egname="<?php echo $eventGroup->name ?>">
                        <?php echo $eventGroup->name .
                               '<span class="entriesCount">(' . sizeof($eventGroup->entries) . $maxAthletes . ' entries) 
                                   <span id="egCount_' . $eventGroup->id . '"></span>
                               </span>'
                        ?>
                    </span>
                </td>
                <td class="table_header_info">
                    <?php
                    if (sizeof($eventStandards) > 0 ) {
                        ?>
                        <span class="table_header_standard card_header_label card_header_e4s"><?php echo $this->_getEventStandardText($eventStandards, $eventGroup) ?></span>
                        <?php
                    }
                    ?>
                </td>
                <?php if ( $this->_isOrganiser() ) { ?>
                <td>
                    <span class="updateLink"><a href="#" onclick="updateAllAthletesForEgId(<?php echo $eventGroup->id ?>,'<?php echo $eventGroup->name ?>'); return false;">Update Event Athletes</a></span>
                </td>
                <?php } ?>
            </tr>
        </table>
        <?php
    }
    private function _getEventStandardText($eventStandards, $event){
        $text = '';
        $sep = '';
        foreach($eventStandards as $standard){
            foreach($standard as $stdValue) {
                $text .= $sep;
                $id = $stdValue->description . '-' . $event->id;
                $text .= "<span id='" . $id . "' stdvalue='" . $stdValue->value . "'>";
	            $text .= $stdValue->description . ': ' . e4s_removeEnsureString($stdValue->valueText);
	            $text .= "</span>";
                $sep = ', ';
            }
        }
        return $text;
    }
    private function _getEventStandards($event) {
        if ( $this->standards === null or sizeof($this->standards) === 0 ) {
            return [];
        }
        $ceObjsForEvent = $this->ceObjs[$event->id];
        $eventStandards = [];
        foreach($ceObjsForEvent as $ceObj){
            $key = $ceObj->ageGroupId . '-' . $ceObj->eventId;
            foreach($this->standards as $standard){
                if ( array_key_exists($key, $standard) ) {
                    $eventStandards[] = $standard[$key];
                }
            }
        }

        return $eventStandards;
    }
    private function _outputEntries($eventGroup){
        $entries = $eventGroup->entries;

        if ( sizeof($entries) === 0 ) {
            ?>
            <div>No Entries</div>
            <?php
            return;
        }
	    $athleteIds = array();
	    foreach($entries as $entry){
		    $athleteIds[] = $entry->athleteId;
	    }
	    $this->_getPerfData($athleteIds);
        $eventType = $eventGroup->entries[0]->tf;
        $showPof10 = false;
        if ( $eventType === E4S_EVENT_TRACK or $eventType === E4S_EVENT_FIELD) {
            $showPof10 = true;
        }
	    ?>
            <div>
        <table class="e4sTable card_table">
            <tr class="table_header">
<?php
    if ( $this->_showOptions() ) {
?>
                <th class="options_th align_justify"></th>
<?php
    }
?>
                <th class="athlete_th align_justify">Athlete</th>
                <th class="gender_th align_justify">Gender</th>
                <th class="club_th align_justify">Organisation</th>
<?php
    if ( $this->_isOrganiser() ) {
?>
                <th class="urn_th align_justify">URN</th>
<?php
    }
?>
                <th class="age_th align_justify">Age</th>
                <th class="qp_th align_justify">Seed<br>Perf</th>
<?php
    if ( $showPof10 ) {
?>
                <th class="sb_th align_justify">SB</th>
                <th class="sbAchieved_th align_justify">Achieved</th>
                <th class="pb_th align_justify">PB</th>
                <th class="pbAchieved_th align_justify">Achieved</th>
        <th class="rank_th">UK Rankings<br><span class="e4sRankAll" style="display:none;">Year (All)</span></th>
<?php
        if ( $this->_isOrganiser() ) {
?>
                <th class="date_th align_justify">Date Checked</th>
<?php
        }
    }
?>
            </tr>
	    <?php
        foreach($entries as $entry){
            $perf = $this->_getEntryPerf($entry);
//var_dump($entry);
//exit;
            if  ( is_null($perf) ) {
                $perf = new stdClass();
                $perf->curAgeGroup = '';
                $perf->pbText = '';
	            $perf->pbAchieved = '';
	            $perf->sbText = '';
                $perf->sbAchieved = '';
            }
	        $athlete = $this->_getAthlete($entry);
            if ( is_null($athlete) ) {
                // Athlete not found
                continue;
                $athlete = new stdClass();
                $athlete->lastCheckedPB = '';
            }

            ?>
            <tr <?php echo $this->_getEntryTags($entry, $eventGroup, $athlete) ?>>
            <?php
            if ( $this->_showOptions() ) {
            ?>
            <td class="">
	            <?php
	            if ( $this->_showOptionsForEntry($entry) ) {
		            ?>
                <button type='button' class='ui-button ui-corner-all ui-widget' style='font-size:0.6em !important;' onclick='launchOptionsForEntry(<?php echo $entry->entryId ?>,<?php echo $entry->athleteId ?>,<?php echo $entry->clubId ?>); return false;' >Options</button>
		            <?php
	            }
	            ?>
            </td>
	        <?php
        }
	    ?>
                <td class="<?php if ($entry->paid === E4S_ENTRY_QUALIFY){echo "e4s_qualify_entry";}?> "><?php echo $athlete->firstName . ' ' . strtoupper($athlete->surName)?></td>
                <td class=""><?php echo $this->_getGender($athlete->gender) ?></td>
                <td class=""><?php echo $entry->clubName?></td>
	            <?php
                $urnDisplayed = null;
	            if ( $this->_isOrganiser() ) {
		            $urnDisplayed = false;
                    if ( $athlete->URN !== '' and !is_null($athlete->URN)) {
                        if ( E4S_CURRENT_DOMAIN !== E4S_AAI_DOMAIN ){
                            $urnDisplayed = true;
                        }
                    }
	            }
                if ( !is_null($urnDisplayed) ) {
                    if ( $urnDisplayed ){
                    ?>
                        <td class=""><a target="e4spof10" href="https://www.thepowerof10.info/athletes/profile.aspx?ukaurn=<?php echo $athlete->URN ?>"><img src="/app/images/po10transparent.png" style="height: inherit;"> <?php echo $athlete->URN ?></a></td>
                    <?php
                    }else{
?>
                        <td class=""></td>
<?php
                    }
                }
                ?>

                <td class=""><span <?php echo $this->_getDomId($entry,'age') ?> ><?php echo $perf->curAgeGroup ?></span></td>
                <td class="">
                    <span <?php echo $this->_getDomId($entry,'qp') ?> class="e4sDiff" e4svalue="<?php echo $entry->entryPb ?>">
                        <?php
                        if ( $this->_showEditSeedPerf($entry) ) {
/*                            <a href="#" onclick="editSeedPerf(<?php echo $entry->entryId ?>); return false;"><i class="material-icons red-text inherit">edit</i>*/
                            echo $this->_getEditPerfHTML($entry);
                        }else {
	                        echo $this->_getQPText( $entry->entryPb );
                        }

                        ?>
                    </span></td>
<?php
    if ( $showPof10 ) {
?>
                <td class=""><span <?php echo $this->_getDomId($entry,'SB') ?>  e4svalue="<?php echo $this->_formatE4SValue($perf, 'sb') ?>" ><?php echo $perf->sbText ?></span></td>
                <td class=""><span class="sbAchieved" e4sachieved="<?php echo $this->_getFormattedDate($perf->sbAchieved, -1) ?>" <?php echo $this->_getDomId($entry,'sbAchieved') ?> ><?php echo $this->_getFormattedDate($perf->sbAchieved, 1) ?></span></td>
                <td class=""><span <?php echo $this->_getDomId($entry,'PB') ?>  e4svalue="<?php echo $this->_formatE4SValue($perf, 'pb') ?>" ><?php echo $perf->pbText ?></span></td>
                <td class=""><span class="pbAchieved" e4sachieved="<?php echo $this->_getFormattedDate($perf->pbAchieved, -1) ?>" <?php echo $this->_getDomId($entry,'pbAchieved') ?> ><?php echo $this->_getFormattedDate($perf->pbAchieved, 1) ?></span></td>
                <td class=""><span <?php echo $this->_getDomId($entry,'rank') ?> ><?php echo $this->_getEventRank($entry, $perf->curAgeGroup) ?></span></td>
<?php
        if ( $this->_isOrganiser() ) {
?>
                <td class=""><?php echo $this->_getCheckedDateHTML($entry, $athlete) ?></td>
<?php
        }
    }
?>
            </tr>
            <?php
        }

	    ?>
        </table>
            </div>
	    <?php
    }
    private function _getEditPerfHTML($entry) {
	    $html = '<a href="#" onclick="editSeedPerf(' . $entry->entryId . '); return false;"><i class="material-icons red-text inherit">edit</i>';
        $html .= $this->_getQPText( $entry->entryPb );
        $html .= '</a>';
        return $html;
    }
    private function _formatE4SValue($perf, $field){
        if ( !isset($perf->$field) ){
            return 0;
        }
        $val = $perf->$field;
        if ( is_null($val) or $val === '' ) {
            return 0;
        }
        return $val;
    }
	private function _outputFooter(){
		?>
        </body>
        </html>
		<?php
	}
    private function _getDomId($entry, $fieldName){
        $text = " id=" . $fieldName . "_" . $entry->athleteId . "_" . $entry->eventId;

        if ( $fieldName === 'qp' ) {
	        $text .= " egid=" . $entry->egId;
        }
        return $text;
    }
    private function _getCheckedDateHTML($entry, $athlete){
	    $urn = $this->_getAthleteURN($athlete);
        $formattedDate = $this->_getFormattedDate($athlete->lastCheckedPB, 1);
        if ( $formattedDate === '' and $urn !== '') {
	        $formattedDate = 'Update';
        }
        if ( is_null($athlete->lastCheckedPB) ) {
	        $athlete->lastCheckedPB = '';
        }
	    $endDate = explode(' ',$athlete->lastCheckedPB)[0];
	    $today = date('Y-m-d');

	    if ($endDate === $today or $urn === '') {
            // been checked in past day
            $html = $formattedDate;
	    }else{
            $html = "<a href='#' onclick='updateAthletePof10(\"" . $urn ."\"); return false;'><i class=\"material-icons red-text inherit\">update</i>" . $formattedDate . "</a>";
        }

        return "<span " . $this->_getDomId( $entry, 'date' ) . ">" . $html . "</span>";
    }
    private function _getEntryTags($entry, $eventGroup, $athlete){
//        $urn = $this->_getAthleteURN($athlete);
//        $tags .= 'athleteid="' . $event->athleteId . '" ';
//        $tags .= 'urn="' . $urn . '" ';

        $tags = ' ';
        $tags .= 'e4seventtype="' . $entry->tf . '" ';
        $tags .= 'entryid="' . $entry->entryId . '" ';
        $tags .= 'e4seventgroup="' . $eventGroup->name. '"';
        $tags .= 'e4sathlete="' . $athlete->firstName . ' ' . $athlete->surName . '"';
        $tags .= 'e4sgender="' . $athlete->gender .'"';
        $tags .= 'e4sclub="' . $entry->clubName . '"';
        return $tags;
    }
    private function _getAthleteURN($athlete){
        if ( isset($athlete->URN) ) {
            return $athlete->URN;
        }
        return '';
    }
    private function _getGender($gender){
	    $gender = strtoupper($gender);
        if ( $gender === E4S_GENDER_MALE ) {
            return 'Male';
        }
        return "Female";
    }
    private function _showOptions(){
        $userId = e4s_getUserId();
        if ( $userId <= E4S_USER_NOT_LOGGED_IN ) {
            return false;
        }
        return true;
    }
    private function _showEditSeedPerf($entry){
        return $this->_showOptionsForEntry($entry);
    }
    private function _showOptionsForEntry($entry){
        if ( $entry->paid === E4S_ENTRY_QUALIFY){
            return false;
        }
        $userId = e4s_getUserId();
        if ( $userId <= E4S_USER_NOT_LOGGED_IN ) {
            return false;
        }
        if ( $this->compObj->isOrganiser() ) {
            return true;
        }
        if ( $entry->userId === $userId ) {
            return true;
        }
        return false;
    }
    private function _getQPText($qp){
        if ( $qp < 100 ) {
            return $qp;
        }
        return resultsClass::getResultFromSeconds($qp);
    }
	private function _getDiff($entry, $perf, $perfType = 'pb'){
        if ( !isset($entry->entryPb)){
	        return "";
        }
        if ( (float)$entry->entryPb === 0.00 ){
            return 100;
        }
		if ( !isset($perf->$perfType)){
			return "";
		}

        $calc = ($perf->$perfType/$entry->entryPb) * 100;
        $diff = 100 - $calc;
        return round($diff, 2);
	}
	private function _getFormattedDate($date, $short = 0){
        if ( $date === '' or is_null($date)) {
            return '';
        }

		if (gettype($date) === 'string') {
			$date = date_create( $date );
		}
        switch ($short){
            case 1:
                $format = "j M Y";
                break;
            case -1:
                $format = "Ymd";
                break;
            default:
                $format = "jS F Y";
                break;
        }

		return date_format($date,$format);
	}
	private function _getFormattedDateTime($date){
		if (gettype($date) == 'string')
			$date = date_create($date) ;
		return date_format($date,"jS F Y H:i");
	}
    private function _getAthlete($entry){
        $athleteId = $entry->athleteId;
        if ( array_key_exists($athleteId, $this->athleteObjs) ) {
	        if (!$this->compObj->isOrganiser() and $this->compObj->anonymizeAthletes()) {
		        $this->athleteObjs[$athleteId]->firstName = 'Athlete';
		        $this->athleteObjs[$athleteId]->surName = $athleteId;
	        }
            return $this->athleteObjs[$athleteId];
        }
        return null;
    }
    private function _getEntryPerf($entry){
        $athleteId = (int)$entry->athleteId;
        $eventId = (int)$entry->eventId;

        if ( array_key_exists($athleteId, $this->performances) ) {
            foreach($this->performances[$athleteId] as $perfObj){
                if ( $perfObj->eventId == $eventId ) {
                    $perfObj->pbText = e4s_removeEnsureString($perfObj->pbText);
                    $perfObj->sbText = e4s_removeEnsureString($perfObj->sbText);
                    return $perfObj;
                    break;
                }
            }
        }
        return null;
    }
    private function _getEventRank($entry, $curAgeGroup):string{
        $athleteId = $entry->athleteId;
        $eventId = $entry->eventId;
        $rankThisYear = '';
        $rankLastYear = '';
	    $rankAll = '';

	    $year = (int)date('Y');

        $displayYear = "'" . ($year - 2000);
        $lastYear = $year - 1;
	    $displayLastYear = "'" . ($year - 2001);

        if ( array_key_exists($athleteId, $this->rankings) ) {
            foreach($this->rankings[$athleteId] as $rankObj){
                if ( $rankObj->eventId == $eventId ) {
                    if ( strtoupper($rankObj->ageGroup) === 'ALL' ) {
	                    if ( $rankObj->year === $year ) {
		                    $rankAll = $rankObj->rank;
	                    }
                    }elseif ($rankObj->ageGroup === $curAgeGroup){
                        if ( $rankObj->year === $year ) {
                            $rankThisYear = $rankObj->rank;
                        }else if ( $rankObj->year === $lastYear ) {
                            $rankLastYear = $rankObj->rank;
                        }
                    }
                }
            }
        }
        if ( $rankThisYear . $rankLastYear . $rankAll === ''){
            return '';
        }

	    if ( $rankLastYear !== '' ) {
		    $rankLastYear = '<span class="e4sRank'. $lastYear .'" style="display:none;">' . $displayLastYear . ':' . $rankLastYear . '</span>';
	    }
        if ( $rankThisYear !== '' ) {
		    $rankThisYear = '<span class="e4sRank'. $year .'">' . $displayYear . ":" . $rankThisYear . '</span>';
	    }
	    if ( trim($rankAll) !== '' ) {
		    $rankAll = '<span class="e4sRankAll" style="display:none;">(' . $rankAll . ')</span>';
	    }
        return $rankLastYear . $rankThisYear . $rankAll; ;

    }
    // Data methods
    private function _getData(){
        $this->perfObj     = new perfAthlete();
        $this->eventGroups = $this->_getEventGroups();
	    $this->_getStandards();
        if ( sizeof($this->standards) > 0 ){
            // ceObjs are only required if standards are being used
	        $this->ceObjs = $this->_getCEObjs();
        }

        $this->_getEntries();
	    $this->_getAthletes();
        unset($this->athleteIds);
    }
	private function _getStandards(){
		$standards = $this->compObj->getStandards(E4S_STANDARD_ORDER_COMBINED);
        $simpleStandards = array();
        foreach($standards as $standard=>$standardObjs){
            if ( !array_key_exists($standard, $simpleStandards) ) {
	            $simpleStandards[ $standard ] = [];
            }
            foreach($standardObjs as $key=>$standardObjArr) {
	            if ( !array_key_exists($key, $simpleStandards[$standard]) ) {
		            $simpleStandards[ $standard ][$key] = [];
	            }
	            foreach ( $standardObjArr as $standardObj ) {
		            $simpleStandard              = new stdClass();
		            $simpleStandard->ageGroupId  = $standardObj->ageGroupId;
		            $simpleStandard->eventId     = $standardObj->eventId;
		            $simpleStandard->value       = $standardObj->value;
		            $simpleStandard->valueText   = $standardObj->valueText;
		            $simpleStandard->description = $standardObj->description;

		            $simpleStandards[ $standard ][ $key ][] = $simpleStandard;
	            }
            }
        }
		$this->standards = $simpleStandards;
	}
	private function _getCEObjs(){
		$compCeObjs = $this->compObj->getCEObjs();
		$ceObjs = array();
		foreach($compCeObjs as $ceObj){
			if ( !array_key_exists($ceObj->egId, $ceObjs)){
				$ceObjs[$ceObj->egId] = array();
			}
            $ceSimpleObj = new stdClass();
            $ceSimpleObj->ageGroupId = $ceObj->ageGroupId;
            $ceSimpleObj->eventId = $ceObj->eventId;
            $ceSimpleObj->id = $ceObj->id;
            $ceSimpleObj->egId = $ceObj->egId;

			$ceObjs[$ceObj->egId][] = $ceSimpleObj;
		}
		return $ceObjs;
	}
    private function _getEventGroups():array{
        $evtObjs = $this->compObj->getEGObjs();
        $eventGroups = array();
        foreach ($evtObjs as $evtObj){
	        // not Interested in schedule only events.
            // If we change this then we need to change the _getEntries method
            if ( $evtObj->options->maxathletes > -1 ) {
                $simpleEGObj = new stdClass();
	            $simpleEGObj->entries = array();
                $simpleEGObj->id = (int)$evtObj->id;
                $simpleEGObj->eventNo = (int)$evtObj->eventNo;
                $simpleEGObj->maxAthletes = (int)$evtObj->options->maxathletes;
                $simpleEGObj->typeNo = $evtObj->typeNo;
                $simpleEGObj->name = $evtObj->name;
                $simpleEGObj->type = $evtObj->type;
                $simpleEGObj->startDate = $evtObj->startDate;

                $eventGroups[$evtObj->eventNo] = $simpleEGObj;
            }
        }
        ksort($eventGroups);
        return $eventGroups;
    }
    private function _getEntries(){
        // _getEventGroups does not return schedule only events so
        // but get qualiy too in case IncLudeEntriesFrom
	    $this->athleteIds = array();
        if ( $this->compObj->hasIndivEvents()) {
	        $sql = "select *
                from " . E4S_TABLE_ENTRYINFO . "
                where compid = " . $this->compObj->getId() . "
                and paid in ( " . E4S_ENTRY_PAID . "," . E4S_ENTRY_QUALIFY . ")
                order by eventNo,surname, firstname
                ";
	        $result = e4s_queryNoLog( $sql );
	        while ( $row = $result->fetch_object() ) {
		        $this->athleteIds[] = $row->athleteId;
		        if ( ! array_key_exists( $row->eventNo, $this->eventGroups ) ) {
                    continue;
                    // could be a semi final plus or a multi event
//			        Entry4UIError( 9072, "Event {$row->eventNo} not found" );
		        }
		        $simpleRow            = new stdClass();
                $simpleRow->isTeam    = E4S_INDIV_ENTRY;
		        $simpleRow->athleteId = (int) $row->athleteId;
		        $simpleRow->entryId   = (int) $row->entryId;
		        $simpleRow->eventId   = (int) $row->eventid;
		        $simpleRow->egId      = (int) $row->egId;
		        $simpleRow->paid      = (int) $row->paid;
		        $simpleRow->userId    = (int) $row->userId;
		        $simpleRow->entity    = $row->entity;
		        $simpleRow->entryPb   = $row->entryPb;
		        $simpleRow->tf        = $row->tf;
		        $simpleRow->clubId    = $row->clubId;
		        $simpleRow->clubName  = $row->clubName;

		        $this->eventGroups[ $row->eventNo ]->entries[] = $simpleRow;
	        }
        }
        if ( $this->compObj->hasTeamEvents() and false ) {
	        $sql = "select te.id entryId,
                           te.name teamName,
                           te.ceid ceId,
                           te.paid paid,
                           te.entityid,
                           te.entitylevel,
                           ce.EventID eventId,
                           ce.maxGroup egId,
                           te.userid userId,
                           ea.athleteid athleteId,
                           eg.eventNo eventNo,
                           c.clubName clubName,
                           a.name areaName
                    from " . E4S_TABLE_EVENTTEAMENTRIES . " te left join " . E4S_TABLE_EVENTTEAMATHLETE . " ea on te.id = ea.teamentryid left join on " . E4S_TABLE_CLUBS . " c on te.entityid = c.id left join on " . E4S_TABLE_AREA . " a on te.entityid = a.id,
                         " . E4S_TABLE_COMPEVENTS . " ce,
                         " . E4S_TABLE_EVENTGROUPS . " eg
                    where te.ceid = ce.ID
                      and te.paid = " . E4S_ENTRY_PAID . "
                      and ce.maxgroup = eg.id
                      and ce.CompID = " . $this->compObj->getID() . "
                    order by te.id";

	        $result = e4s_queryNoLog( $sql );

	        while ( $row = $result->fetch_object() ) {
                if ( !is_null($row->athleteId)) {
	                $this->athleteIds[] = (int)$row->athleteId;
                }
		        if ( ! array_key_exists( $row->eventNo, $this->eventGroups ) ) {
			        Entry4UIError( 9073, 'Event not found' );
		        }
		        $simpleRow            = new stdClass();
		        $simpleRow->isTeam    = E4S_TEAM_ENTRY;
		        $simpleRow->entryId   = (int) $row->entryId;
		        $simpleRow->eventId   = (int) $row->eventid;
		        $simpleRow->egId      = (int) $row->egId;
		        $simpleRow->paid      = (int) $row->paid;
		        $simpleRow->userId    = (int) $row->userId;
		        $simpleRow->entity    = $row->entityLevel . ':' . $row->entityId;
		        $simpleRow->tf        = E4S_EVENT_TRACK;
		        $simpleRow->clubId    = $row->entityId;
                if ( (int)$row->entityLevel === E4S_CLUB_ENTITY ) {
	                $simpleRow->clubName = $row->clubName;
                }else {
	                $simpleRow->clubName = $row->areaName;
                }

		        $simpleRow->athleteId = (int) $row->athleteId;
		        $this->eventGroups[ $row->eventNo ]->entries[] = $simpleRow;
	        }
        }
    }
    private function _getPerfData($athleteIds):void {
	    $year = date('Y');
        $year = $year - 1;
        $this->rankings = $this->perfObj->getRankings($athleteIds, $year);
	    $this->_getPerformances($athleteIds);
    }

    private function _getPerformances($athleteIds){
	    $pbs = $this->perfObj->getPerformances($athleteIds);
        $simplePBs = [];
        if ( sizeof($pbs) > 0 ){
            foreach($pbs as $athleteId=>$athlete){
                foreach($athlete as $eventId=>$perf){
                    $simplePB = new stdClass();
                    $simplePB->ageGroup = $perf->ageGroup;
	                $simplePB->curAgeGroup = $perf->curAgeGroup;
                    $simplePB->eventId = $eventId;
                    $simplePB->pb = $perf->pb;
                    $simplePB->pbText = $perf->pbText;
                    $simplePB->pbAchieved = $perf->pbAchieved;
                    $simplePB->sb = $perf->sb;
                    $simplePB->sbText = $perf->sbText;
                    $simplePB->sbAchieved = $perf->sbAchieved;

                    if ( !array_key_exists($athleteId, $simplePBs) ){
                        $simplePBs[$athleteId] = [];
                    }
                    if ( !array_key_exists($eventId, $simplePBs[$athleteId])) {
                        $simplePBs[$athleteId][$eventId] = [];
                    }
                    $simplePBs[$athleteId][$eventId] = $simplePB;
                }
            }
        }
	    $this->performances = $simplePBs;
//        var_dump($this->performances);
//        exit;
    }
    private function _getAthletes(){
        if(sizeof($this->athleteIds) > 0 ) {
	        $sql      = '
            select id id, gender, firstName firstName, surName surName, urn URN, lastCheckedPB lastCheckedPB
            from ' . E4S_TABLE_ATHLETE . ' 
            where id in (' . implode( ',', $this->athleteIds ) . ')  
        ';
	        $result   = e4s_queryNoLog( $sql );
	        $athletes = array();
	        while ( $obj = $result->fetch_object( E4S_ATHLETE_OBJ ) ) {
		        $simpleAthleteObj                = new stdClass();
		        $simpleAthleteObj->id            = $obj->id;
		        $simpleAthleteObj->gender        = $obj->gender;
		        $simpleAthleteObj->firstName     = $obj->firstName;
		        $simpleAthleteObj->surName       = $obj->surName;
		        $simpleAthleteObj->URN           = $obj->URN;
		        $simpleAthleteObj->lastCheckedPB = $obj->lastCheckedPB;

		        $athletes[ $obj->id ] = $simpleAthleteObj;
	        }
	        $this->athleteObjs = $athletes;
        }else{
            $this->athleteObjs = [];
        }
    }
}