function e4sAlert(text, title, callback, addButtons) {
    let buttons = [];
    if (addButtons) {
        buttons = addButtons;
    }
    buttons.push({
        text: "OK",
        class: "resultButton",
        click: function () {
            if (callback && callback !== null) {
                callback();
            }
            $(this).dialog("close");
        }
    });
    let options = {
        title: title,
        text: text
    };
    e4sPrompt(options, buttons);
}
function e4sSelect(text, title, values, callback, useBtnLabels) {
    let btnLabels = ['OK', 'Cancel'];
    if (useBtnLabels) {
        btnLabels = useBtnLabels;
    }
    let buttons = [{
        text: btnLabels[0],
        class: "resultButton",
        click: function () {
            let value = $("#requestInput").val();
            $(this).dialog("close");
            if (callback) {
                callback(value);
            }
        }
    }, {
        text: btnLabels[1],
        class: "resultButton",
        click: function () {
            $(this).dialog("close");
        }
    }];
    let options = {
        title: title,
        text: text,
        value: values,
        select: true
    };
    e4sPrompt(options, buttons);
}
function e4sRequest(text, title, value, callback, useBtnLabels) {
    let btnLabels = ['OK', 'Cancel'];
    if (useBtnLabels) {
        btnLabels = useBtnLabels;
    }
    e4sGlobal.originalValue = value;
    let buttons = [{
        text: btnLabels[0],
        class: "resultButton",
        click: function () {
            let value = $("#requestInput").val();
            $(this).dialog("close");
            if (callback) {
                callback(e4sGlobal.originalValue, value);
            }
        }
    }, {
        text: btnLabels[1],
        class: "resultButton",
        click: function () {
            $(this).dialog("close");
        }
    }];
    let options = {
        title: title,
        text: text,
        value: value,
        request: true
    };
    e4sPrompt(options, buttons);
}

function e4sConfirm(text, title, yesCallback, noCallback) {
    let buttons = [{
        text: "Yes",
        class: "resultButton",
        click: function () {
            if (yesCallback) {
                yesCallback();
            }
            $(this).dialog("close");
        }
    }, {
        text: "No",
        class: "resultButton",
        click: function () {
            if (noCallback) {
                noCallback();
            }
            $(this).dialog("close");
        }
    }];
    let options = {
        title: title,
        text: text
    };
    e4sPrompt(options, buttons);
}

function e4sPrompt(options, buttons) {
    let divName = "e4sPrompt";
    let divHTML = "<div id='" + divName + "'>";
    let value = options.value;
    divHTML += options.text ? options.text : "Warning !";
    if (options.request) {
        divHTML += "<br><input style='width:99%' id='requestInput' value='" + value + "' />";
    }
    if (options.select) {
        divHTML += "<br><select style='width:99%' id='requestInput'>";
        for (let i = 0; i < value.length; i++) {
            let val = "" + value[i];
            let display = value[i];
            if ( val.indexOf(":") > -1 ) {
                let parts = val.split(":");
                val = parts[0];
                display = parts[1];
            }
            divHTML += "<option value='" + val + "'>" + display + "</option>";
        }
        divHTML += "</select>";
    }
    divHTML += "</div>";
    $("#" + divName).remove();
    $("#e4sPromptHolder").html(divHTML);

    options = options ? options : {};
    let dialog = $("#" + divName).dialog({
        resizable: options.resizable ? options.resizable : false,
        title: options.title ? options.title : "Entry4Sports",
        height: options.height ? options.height : "auto",
        width: options.width ? options.width : 500,
        modal: true
    });
    let useButtons = buttons ? buttons : [
        {
            text: "Close",
            class: "resultButton",
            click: function () {
                $(this).dialog("close");
            }
        }
    ];
    dialog.dialog('option', 'buttons', useButtons);
    positionDialog(dialog);
}

function positionDialog(dialog) {
    dialog.dialog({
        position: {my: "center", at: "top+300", of: window}
    });
}