<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
e4s_checkFinanceReportAccess();
$perms = getPermissions(PERM_FINANCE);
$config = e4s_getConfig();
$title = 'Date From ' . date('d/m/Y', strtotime('-' . $days . ' days')) . ' to Today';

echo '<link rel="stylesheet" href="' . E4S_PATH . '/css/pqgrid6.min.css">';
echo '<link rel="stylesheet" href="' . E4S_PATH . '/css/pqgrid6.ui.min.css">';
echo '<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.3/themes/' . E4S_JQUERY_THEME . '/jquery-ui.css">';
echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> ';
echo '<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>';
echo '<script type="text/javascript" src="' . E4S_PATH . '/js/pqgrid6.min.js"></script>';
echo '<script type="text/javascript" src="' . E4S_PATH . '/js/jszip.min.js"></script>';
echo '<script type="text/javascript" src="' . E4S_PATH . '/js/FileSaver.min.js"></script>';
echo '<div id="grid_table" style="margin:auto;"></div>';
$sql = 'SELECT c.compclubid, c.id compid, date_format(date(e.periodStart),\'%D %b %Y\') Date, c.Name, count(e.id) Entries, sum(e.price) Total
FROM ' . E4S_TABLE_ENTRIES . ' e,
	' . E4S_TABLE_COMPEVENTS . ' ce,
    ' . E4S_TABLE_COMPETITON . ' c
WHERE date(e.periodStart) >= CURRENT_DATE - INTERVAL ' . $days . ' DAY
and ce.id = e.compEventID
and e.paid = ' . E4S_ENTRY_PAID . '
and ce.compid = c.ID
group by c.Name, date(e.periodStart), c.compclubid, c.id
order by e.periodStart desc,c.id';
$result = e4s_queryNoLog($sql);
$rows = $result->fetch_all(MYSQLI_ASSOC);
echo "<table style='display:inline;' id='finance'>";
echo '<tr>';
echo '<td>Date</td>';
echo '<td>Competition</td>';
echo '<td>Entries</td>';
echo '<td>Total</td>';
echo '</tr>';
foreach ($rows as $row) {
    if (hasPermForOrgOrComp($perms, $row)) {
        echo '<tr>';
        echo '<td>' . $row['Date'] . '</td>';
        echo '<td>' . $row['Name'] . '</td>';
        echo '<td>' . $row['Entries'] . '</td>';
        echo '<td>' . $row['Total'] . '</td>';
        echo '</tr>';
    }
}
echo '</table>';
echo '<script>';
echo '
var tbl = $("#finance");          
var tblObj = $.paramquery.tableToArray(tbl);
var colM = [
    {
        align:"left",
        dataIndx:0,
        dataType:"string",
        title:"Date",
        width:200
    },
    {
        align:"left",
        dataIndx:1,
        dataType:"string",
        title:"Competition",
        width:500
    },
    {
        align:"right",
        dataIndx:2,
        dataType:"integer",
        title:"Entries",
        width:125
    },
    {
        align:"right",
        dataIndx:3,
        dataType:"float",
        title:"Total",
        width:150,
        format: \'' . $config['currency'] . '##,###.00\',
        summary: {
            type: "Sum"
        }
    }
];
var newObj = { 
    width: "100%", 
    height: "100%", 
    title: "' . $title . '", 
    flexWidth: true ,
    numberCell: {show: false},
    dataModel:{ 
        data: tblObj.data 
    },
    toolbar: {
        items: [
            {
                type: \'select\',
                label: \'Format: \',                
                attr: \'id="export_format"\',
                options: [{ xlsx: \'Excel\', csv: \'Csv\', htm: \'Html\', json: \'Json\'}]
            },
            {
                type: \'button\',
                label: \'Export\',
                icon: \'ui-icon-arrowthickstop-1-s\',
                listener: function () {
                    var format = $("#export_format").val(),                            
                        blob = this.exportData({
                            format: format,        
                            nopqdata: true, //applicable for JSON export.                        
                            render: true
                        });
                    if(typeof blob === "string"){                            
                        blob = new Blob([blob]);
                    }
                    saveAs(blob, "pqGrid." + format );
                }
            }
        ]
    },
    colModel: colM,
//    pageModel:{ 
//        rPP: 20, 
//        type: "local" 
//    },
    groupModel: {
        on: true,
        dataIndx: [0],                       
        agg:{2:\'sum\', 3:\'sum\'},
        grandSummary: true,           
        summaryInTitleRow: \'all\',
        titleInFirstCol: true, 
        fixCols: false, 
        indent: 20,
        collapsed: [true],
        title: [
            "{0}"
        ]
    }
};
var obj = $("#grid_table").pqGrid(newObj);
tbl.css("display", "none");
';

echo '</script>';
exit();

