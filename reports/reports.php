<?php
require_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_getWaitingAthletes($obj) {
    header('Content-Type: text/html; charset=utf-8');

    $compId = checkFieldForXSS($obj, 'compid:Competition ID');
    $compObj = e4s_getCompObj($compId);
    if (!$compObj->isOrganiser()) {
        Entry4UIError(9701, 'This report is unavailable');
    }
    $sql = '
        select   e.athlete
                ,c.clubname
                ,a.urn
                ,ev.name eventName
                ,pb.sb pb
                ,e.waitingPos
                ,u.user_email email
        from ' . E4S_TABLE_ENTRIES . ' e,
             ' . E4S_TABLE_COMPEVENTS . ' ce,
             ' . E4S_TABLE_ATHLETE . ' a,
             ' . E4S_TABLE_EVENTS . ' ev,
             ' . E4S_TABLE_USERS . ' u,
             ' . E4S_TABLE_ATHLETEPB . ' pb,
             ' . E4S_TABLE_CLUBS . ' c
        where e.compEventID = ce.ID
        and ce.EventID = ev.id
        and ce.CompID = ' . $compId . '
        and e.userid = u.ID
        and c.id = e.clubid
        and e.athleteid = a.id
        and pb.athleteid = a.id
        and pb.eventid = ev.id
        and waitingPos > 0
        order by ev.Name, e.waitingPos
    ';
    $result = e4s_queryNoLog($sql);
    ?>
    <html>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        .athlete_th {
            width: 220px;
        }

        .club_th {
            width: 250px;
        }

        .urn_th {
            width: 80px;
        }

        .event_th {
            width: 200px;
        }

        .pb_th {
            width: 80px;
        }

        .email_th {

        }

        .headerInfo {
            font-size: xx-large;
        }
    </style>
    <body>
    <p class="headerInfo">Waiting List information for Competition
        : <?php echo $compObj->getDisplayName() ?></p>
    <table>
        <tr>
            <th class="athlete_th">Athlete</th>
            <th class="club_th">Club</th>
            <th class="urn_th">URN</th>
            <th class="event_th">Event</th>
            <th class="pb_th">PB</th>
            <th class="pos_th">Waiting Position</th>
            <th class="email_th">Email</th>
        </tr>
        <?php
        while ($obj = $result->fetch_object()) {
            ?>
            <tr>
                <td>
                    <?php
                    echo $obj->athlete;
                    ?>
                </td>
                <td>
                    <?php
                    echo $obj->clubname;
                    ?>
                </td>
                <td>
                    <?php
                    echo $obj->urn;
                    ?>
                </td>
                <td>
                    <?php
                    echo $obj->eventName;
                    ?>
                </td>
                <td>
                    <?php
                    echo $obj->pb;
                    ?>
                </td>
                <td>
                    <?php
                    echo $obj->waitingPos;
                    ?>
                </td>
                <td>
                    <?php
                    echo $obj->email;
                    ?>
                </td>
            </tr>
            <?php
        }
        ?>

    </table>
    </body>
    </html>
    <?php
}