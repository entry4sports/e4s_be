<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'eventteams/commonEventTeam.php';
$Permission = $GLOBALS['E4S_ROLE'];
if (userHasPermission($Permission, null, null) === FALSE) {
    echo 'You do not have permission to run this report.';
    exit();
}
//$perms = getPermissions($Permission);
$config = e4s_getConfig();
// Get athletes that have paid nothing
$sql = '
SELECT sum(price), athlete, orderid 
FROM ' . E4S_TABLE_ENTRIES . ' e, 
     ' . E4S_TABLE_COMPEVENTS . " ce 
where paid = 1 and e.compEventid = ce.ID 
and ce.options not like '%" . e4s_optionIsTeamEvent() . "%' 
group by athlete, orderid having sum(price) = 0
";
$results = e4s_queryNoLog($sql);
$rows = $results->fetch_all(MYSQLI_ASSOC);

foreach ($rows as $row) {
    // has the athlete been paid for on a different order
    $athleteSql = '
    select a.*
    from ' . E4S_TABLE_ATHLETE . ' a,
         ' . E4S_TABLE_ENTRIES . ' e
    where e.orderid = ' . $row['orderid'] . "
    and e.athlete = '" . $row['athlete'] . "'
    amd e.athleteid = a.id
    ";

    $athleteResults = e4s_queryNoLog($athleteSql);
    if ($athleteResults->num_rows !== 1) {
        Entry4UIError(1000, 'Failed to find athlete record ' . $row['athlete']);
    }

}