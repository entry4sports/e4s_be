<?php
include_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_runStripeReport() {
    $reportObj = array();
    $result = e4s_getStripeDBResult();
    while ($obj = $result->fetch_object()) {
//        $reportObj[] = $obj;
        e4s_addToStripeTotals($obj, $reportObj);
    }
    e4s_stripeOutputData($reportObj);
}

function e4s_stripeOutputData($reportObj) {
    $lastCompId = 0;

    foreach ($reportObj as $compId => $compData) {
        if ($compId !== $lastCompId or $lastCompId === 0) {
            e4s_stripeOutputHeader($compId, $compData);
        }

        foreach ($compData->users as $email => $user) {
            e4s_stripeOutputTotal($email, $user->totals);
        }
    }
}

function e4s_stripeOutputTotal($email, $totals) {
    echo $email . ' => ' . number_format($totals->highPrice / 100, 2) . "\n";
    if ($totals->lowPrice > 0) {
        echo $email . ' => ' . number_format($totals->lowPrice / 100, 2) . "\n";
    }
}

function e4s_stripeOutputHeader($id, $compData) {
    $name = $compData->name;
    $date = $compData->date;
    $gross = number_format($compData->gross / 100, 2);
    echo $id . ':' . $name . ' (' . $date . ') => ' . $gross . "\n";
}

function e4s_addToStripeTotals($obj, &$reportObj) {
    if (!array_key_exists($obj->compId, $reportObj)) {
        $reportObj[$obj->compId] = e4s_getDefaultReportObj($obj);
    }
    $compObj = &$reportObj[$obj->compId];
    $users = &$compObj->users;
    if (!array_key_exists($obj->user_email, $users)) {
        $users[$obj->user_email] = e4s_getDefaultUserObj();
    }
    $products = &$users[$obj->user_email]->products;
    if (!array_key_exists($obj->product, $products)) {
        $products[$obj->product] = e4s_getDefaultProductObj();
    }
    $totals = $users[$obj->user_email]->totals;
    $productObj = &$products[$obj->product];
    $productObj->price = $obj->price;
    $compObj->gross += $obj->price;
    if ($obj->net > $productObj->highPrice) {
        if ($productObj->highPrice > 0) {
            $compObj->gross -= $obj->price; // only add price once
        }
        $totals->highPrice -= $productObj->highPrice;
        $productObj->lowPrice = $productObj->highPrice;
        $totals->lowPrice += $productObj->lowPrice;
    }
    $productObj->highPrice = $obj->net;
    $totals->highPrice += $productObj->highPrice;
}

function e4s_getDefaultReportObj($dbObj) {
    $obj = new stdClass();
    $obj->name = $dbObj->competitionName;
    $obj->date = $dbObj->competitionDate;
    $obj->gross = 0;
    $obj->users = array();
    return $obj;
}

function e4s_getDefaultUserObj() {
    $obj = new stdClass();
    $obj->totals = e4s_getDefaultProductObj();
    $obj->products = array();
    return $obj;
}

function e4s_getDefaultProductObj() {
    $obj = new stdClass();
    $obj->price = 0;
    $obj->highPrice = 0;
    $obj->lowPrice = 0;
    return $obj;
}

function e4s_getStripeDBResult() {
    $sql = '
        select c.date competitionDate,
           ce.CompID compId,
           c.Name competitionName,
           yc.product_id product,
           yc.commission*100 net,
           e.price*100 price,
           u.user_email user_email
        from Entry4_yith_wcsc_commissions yc,
             Entry4_users u,
             Entry4_Entries e,
             Entry4_CompEvents ce,
             Entry4_Competition c
        where yc.user_id = u.id
        and e.variationID = yc.product_id
        and ce.id = e.compEventID
        and c.id = ce.CompID
        and e.paid = ' . E4S_ENTRY_PAID . '
        order by c.Date desc,ce.CompID desc, yc.product_id, yc.commission asc
    ';
    return e4s_queryNoLog($sql);
}
