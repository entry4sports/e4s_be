<?php
define('E4S_COMP_ALLNOTES', 'e4sAllNotes');
define('E4S_COMP_NOTES', 'e4sNotes');
define('E4S_COMP_EMAILTXT', 'emailText');
define('E4S_COMP_INFO', 'information');
define('E4S_COMP_NEWS', 'newsFlash');
define('E4S_COMP_TANDC', 'termsConditions');
define('E4S_COMP_TICKET', 'ticket');
define('E4S_COMP_TICKETFORM', 'ticketForm');
define('E4S_COMP_HOMEINFO', 'homeInfo');
define('E4S_NEW_LINE_FROM', '~');
define('E4S_NEW_LINE_TO', '\n');

class notesClass {
    public $newlineFrom = '~';
    public $newlineTo = "\n";

    public function __construct() {
    }

	public static function getNotes($guid, $htmlToUI = true) {
		$notes = notesClass::get($guid);
		if ( !$htmlToUI ) {
			return $notes;
		}
		$retNotes = [];

		foreach ($notes as $key => $value) {
			$retNotes[$key] = e4s_htmlToUI($value);
		}
		return $retNotes;
	}
    public static function get($guid, $key = '') {
	    if ( array_key_exists( E4S_COMP_ALLNOTES, $GLOBALS ) ) {
		    if ( array_key_exists( $guid, $GLOBALS[ E4S_COMP_ALLNOTES ] ) ) {
				if ( $key === '' ){
					return $GLOBALS[ E4S_COMP_ALLNOTES ][ $guid ];
				}
			    if ( array_key_exists( $key, $GLOBALS[ E4S_COMP_ALLNOTES ][ $guid ] ) ) {
				    return $GLOBALS[ E4S_COMP_ALLNOTES ][ $guid ][ $key ];
			    }
		    }
	    }else{
		    $GLOBALS[ E4S_COMP_ALLNOTES ] = [];
	    }
	    $globalIds = [$guid];

	    if ( array_key_exists(E4S_PROCESSING_HOMEPAGE_COMPS, $GLOBALS)) {
		    if ( array_key_exists(E4S_PROCESSING_COMPS_IDS, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS])) {
			    $globalIds = $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_IDS];
				$globalIds[$guid] = $guid;
		    }
	    }
	    $sql = 'select guid, e4sKey, e4sText
                    from ' . E4S_TABLE_TEXT . "
                    where guid in ('" . implode("','",$globalIds) . "')";

	    $results   = e4s_queryNoLog( $sql );
	    $notes = $GLOBALS[ E4S_COMP_ALLNOTES ];
	    while ( $obj = $results->fetch_object() ) {
		    $retText = str_replace( E4S_NEW_LINE_FROM, E4S_NEW_LINE_TO, $obj->e4sText );
		    if ( ! array_key_exists( $obj->guid, $notes ) ) {
			    $notes[ $obj->guid ] = [];
		    }
		    $notes[ $obj->guid ][ $obj->e4sKey ] = $retText;
	    }
	    $GLOBALS[ E4S_COMP_ALLNOTES ] = $notes;

	    if ( array_key_exists( $guid, $notes ) ) {
			if ( $key === '' ){
				return $notes[$guid];
			}
		    if ( array_key_exists( $key, $notes[$guid] ) ) {
			    return $notes[$guid][ $key ];
		    }
	    }
		if ( $key === '' ) {
			return [];
		}
	    return '';
    }

    public static function getValues($obj, &$model) {
        $model->e4sNotes = checkFieldForXSS($obj, 'e4sNotes:Competition Notes');
        $model->information = checkFieldForXSS($obj, 'information:Competition information');
        $model->newsFlash = checkFieldForXSS($obj, 'newsFlash:News Flash');
        $model->termsConditions = checkFieldForXSS($obj, 'termsConditions:Terms and Conditions');
        $model->checkinText = checkFieldForXSS($obj, 'checkinText:Competition Checkin Text');
        $model->emailText = checkFieldForXSS($obj, 'emailText:Competition Email Text');
    }

    public static function setText(&$model) {
        notesClass::set($model->id, E4S_COMP_NOTES, $model->e4sNotes);
        notesClass::set($model->id, E4S_COMP_INFO, $model->information);
        notesClass::set($model->id, E4S_COMP_TANDC, $model->termsConditions);
        notesClass::set($model->id, E4S_COMP_NEWS, $model->newsFlash);
        notesClass::set($model->id, E4S_COMP_EMAILTXT, $model->emailText);
        $infoText = '';
        if (isset($model->options->homeInfo)) {
            $infoText = $model->options->homeInfo;
        }
        notesClass::set($model->id, E4S_COMP_HOMEINFO, $infoText);
    }

    public static function set($guid, $key, $value) {
        $value = e4s_htmlFromUI($value);
        $value = str_replace(E4S_NEW_LINE_TO, E4S_NEW_LINE_FROM, htmlentities($value, ENT_QUOTES, 'UTF-8'));
        $sql = 'Insert into ' . E4S_TABLE_TEXT . " (guid, e4skey, e4sText)
            values('" . $guid . "','" . $key . "','" . $value . "')
            on duplicate key update
            guid = '" . $guid . "',
            e4skey = '" . $key . "',
            e4sText = '" . $value . "'";

        e4s_queryNoLog($sql);
    }

    public static function delete($guid) {
        $sql = 'delete from ' . E4S_TABLE_TEXT . "
                where guid = '" . $guid . "'";
        e4s_queryNoLog($sql);
    }
}