<?php

class e4sArchive {
	var $max_age;
	var $floor;
	var $processLimit = 125;
	var $counters;
	var $dbName;
	var $archiveDbName;
	var $suffix;
	var $e4s_key = "e4s_key";
	var $runId;
	function __construct($daysOld, $useDB = ''){
		$this->max_age = $daysOld;
		$this->floor = date('Y-m-d H:i:s', strtotime("-$daysOld days"));
		$this->runId = time();
		$this->dbName = DB_NAME;
		if ( $useDB !== '' ) {
			$this->dbName = $useDB;
		}
		$this->archiveDbName = e4sArchive::archiveName();
		$this->suffix = str_replace('E4S','',DB_NAME);
		$this->_checkTables();
		$this->counters = new stdClass();
	}
	static function archiveName(){
		return 'E4S_Archive';
	}
	function restore(){
		$this->_progress("Restoring Tables");
		$this->_restoreTable(E4S_TABLE_ENTRIES_ARCHIVE, E4S_TABLE_ENTRIES);
		$this->_restoreTable(E4S_TABLE_EVENTTEAMENTRIES_ARCHIVE, E4S_TABLE_EVENTTEAMENTRIES);
		$this->_restoreTable(E4S_TABLE_EVENTTEAMATHLETE_ARCHIVE, E4S_TABLE_EVENTTEAMATHLETE);
		$incCounter = 50000;
		for($counter = 1; $counter < 7000000; $counter += $incCounter) {
			$this->_restoreTable(E4S_TABLE_POSTMETA_ARCHIVE, E4S_TABLE_POSTMETA,$counter, $counter + $incCounter);
		}
		$incCounter = 25000;
		$this->_restoreTable(E4S_TABLE_POSTS_ARCHIVE, E4S_TABLE_POSTS);

		for($counter = 1; $counter < 2000000; $counter += $incCounter) {
			$this->_restoreTable(E4S_TABLE_WCORDERITEMMETA_ARCHIVE, E4S_TABLE_WCORDERITEMMETA,$counter, $counter + $incCounter);
		}

		$this->_restoreTable(E4S_TABLE_WCORDERITEMS_ARCHIVE, E4S_TABLE_WCORDERITEMS);
		$this->_progress("Restoring Tables Complete");
		Entry4UISuccess();
	}
	function clearRestore(){
		$this->_progress("Clearing Tables");
		$this->_clearRestoreTable(E4S_TABLE_ENTRIES_ARCHIVE, E4S_TABLE_ENTRIES);
		$this->_clearRestoreTable(E4S_TABLE_EVENTTEAMENTRIES_ARCHIVE, E4S_TABLE_EVENTTEAMENTRIES);
		$this->_clearRestoreTable(E4S_TABLE_EVENTTEAMATHLETE_ARCHIVE, E4S_TABLE_EVENTTEAMATHLETE);
		$this->_clearRestoreTable(E4S_TABLE_POSTMETA_ARCHIVE, E4S_TABLE_POSTMETA, 'meta_id');
		$this->_clearRestoreTable(E4S_TABLE_POSTS_ARCHIVE, E4S_TABLE_POSTS, 'ID');
		$this->_clearRestoreTable(E4S_TABLE_WCORDERITEMMETA_ARCHIVE, E4S_TABLE_WCORDERITEMMETA, 'meta_id');
		$this->_clearRestoreTable(E4S_TABLE_WCORDERITEMS_ARCHIVE, E4S_TABLE_WCORDERITEMS, 'order_item_id');
		$this->_progress("Clearing Tables Complete");
		Entry4UISuccess();
	}
	private function _progress($msg){
		echo $msg . "\n";
		logTxt("E4S Archive::" . $msg);
	}
	private function _clearRestoreTable($archiveTable, $table, $keyName = 'id'){
		$archiveTable = $this->archiveDbName . "." . $archiveTable;
		$table = $this->dbName . "." . $table;
		$sql = "select " . $keyName . " from " . $archiveTable . " limit 1";
		$this->_progress($sql);
		$result = e4s_queryNoLog($sql);
		if ( $result->num_rows === 0 ){
			$this->_progress("Clearing $table is not required" );
		}
		$row = $result->fetch_object();

		$sql = "select * from " . $table . " where " . $keyName . " = '" . $row->$keyName . "'";
		$this->_progress($sql);

		$result = e4s_queryNoLog($sql);
		if ( $result->num_rows === 0 ){
			$this->_progress("Clearing $table is not required" );
		} else {
			$sql = "delete from " . $table . " where " . $keyName . " in (select " . $keyName . " from " . $archiveTable . ")";
			$this->_progress("Clearing $table : $sql");
			e4s_queryNoLog( $sql );
		}
	}
	private function _restoreTable($archiveTable, $table, $from = 0, $to = 0){
//		$this->_clearRestoreTable($archiveTable, $table, $keyName);
		$table = $this->dbName . "." . $table;
		$archiveTable = $this->archiveDbName . "." . $archiveTable;
		$cols = $this->getTableCols( $archiveTable );
		$e4s_key = $this->e4s_key;

		$sql = "insert ignore into " . $table . " (";
		$sep = "";
		foreach ( $cols as $col ) {
			if ( $col === $e4s_key ){
				continue;
			}
			$sql .= $sep . $col;
			$sep = ",";
		}
		$sql .= ") select ";
		$sep = "";
		foreach ( $cols as $col ) {
			$useCol = $col;
			if ( $col === $e4s_key ){
				continue;
			}

			$sql .= $sep . $useCol;
			$sep = ",";
		}
		$sql .= " from " . $archiveTable;
		if ( $from !== 0 and $to !== 0 ){
			$sql .= " where " . $e4s_key . " between " . $from . " and " . $to;
		}else{
			if ( $from !== 0 ){
				$sql .= " where " . $e4s_key . " >= " . $from;
			} elseif ( $to !== 0 ){
				$sql .= " where " . $e4s_key . " <= " . $to;
			}
		}
		$this->_progress( "Restoring $archiveTable to $table : $sql");
		e4s_queryNoLog($sql);
	}
	// defunct as too slow and memory intensive
	private function _restoreTableByRow($archiveTable, $table, $keyName = 'id'){
		$archiveTable = $this->archiveDbName . "." . $archiveTable;
		$cols = $this->getTableCols( $archiveTable );
		$sql = "select * from " . $archiveTable;
		$result = e4s_queryNoLog($sql);
		while ( $row = $result->fetch_object() ) {
			$e4s_key = $this->e4s_key;
			$sql = "select " . $keyName . " from " . $table . " where " . $keyName . " = " . $row->$e4s_key;
			$exists = e4s_queryNoLog($sql);
			if ( $exists->num_rows > 0 ){
				// already exists;
//				var_dump($row->$key . " already exists");
				continue;
			}
			$sql = "insert into " . $table . " (";
			$sep = "";
			foreach ( $cols as $col ) {
				if ( $col === $e4s_key ){
					continue;
				}
				$sql .= $sep . $col;
				$sep = ",";
			}
			$sql .= ") values (";
			$sep = "";
			foreach ( $cols as $col ) {
				if ( $col === $e4s_key ){
					continue;
				}
				$value = $row->$col;
				if ( strtolower($col) === strtolower($keyName) ){
					$value = $row->$e4s_key;
				}
				$sql .= $sep . "'" . $value . "'";
				$sep = ",";
			}
			$sql .= ")";
			e4s_queryNoLog($sql);
			break;
		}
	}
	function getTableCols($table){
		$sql = "desc " . $table;
		$result = e4s_queryNoLog($sql);
		$cols = array();
		while ($col = $result->fetch_object()){
			$cols[] = $col->Field;
		}
		return $cols;
	}
	function process(){
		$hour = date('H');
		if ( (int)$hour > 7 ){
			// only archive between midnight and 6am
//			return;
		}
		$txt = "Archive Process Started ... " . $this->runId;
		logTxt($txt);
		$this->_archiveOrders();
		$txt = "Archive Process Orders ... " . $this->runId;
		logTxt($txt);
		$this->_checkUnconnectedProducts();
		$txt = "Archive Process Products ..." . $this->runId;
		logTxt($txt);
		$this->_archiveLogs();
		$txt = "Archive Process Complete ... "  . $this->runId . ' - ' . e4s_getDataAsType($this->counters, E4S_OPTIONS_STRING);
		logTxt($txt);
		Entry4UISuccess('',$txt);
	}
	private function _archiveLogs(){
		$daysToKeep = 7;
		$sql = "insert into " . $this->_getArchiveTable( E4S_TABLE_LOG_ARCHIVE ) . " ( id, created, userid, uid,info,trace,payload,parentid)
			select * from " . E4S_TABLE_LOG . "
			where created < (CURRENT_DATE - interval " . $daysToKeep ." day)
			";
		e4s_queryNoLog($sql);
		$sql = "delete from " . E4S_TABLE_LOG . "
			where created < (CURRENT_DATE - interval " . $daysToKeep ." day)
			";
		e4s_queryNoLog($sql);
	}
	private function _archiveOrders(){
		$sql = "select p.*
			from " . E4S_TABLE_POSTS . " p
			where post_type = '" . WC_POST_ORDER . "'
			 AND p.post_date < '$this->floor'
			 order by post_date
			 limit 0," . $this->processLimit ;

		$result = e4s_queryNoLog($sql);
		while ( $order = $result->fetch_object() ) {
			if ( !$this->_archiveOrder($order)){
				Entry4UIError(9103,"Failed to archive Order " . $order->ID);
			}
		}
	}
	private function _archiveOrder($order): bool{
		$sql = "select oi.*
			from " . E4S_TABLE_WCORDERITEMS . " oi
			where order_id = " . $order->ID;
		$result = e4s_queryNoLog($sql);
		while ( $orderItem = $result->fetch_object() ) {
			if (!$this->_archiveOrderItem($orderItem)){
				return false;
			}
		}
		if (!$this->_archiveEntry($order->ID)){
			return false;
		}
		if (!$this->_archivePostMeta($order->ID)){
			return false;
		}
		if (!$this->_archiveObject($order, E4S_TABLE_POSTS, E4S_TABLE_POSTS_ARCHIVE)){
			return false;
		}
		if ( !isset($this->counters->orders) ){
			$this->counters->orders = 0;
		}
		$this->counters->orders++;
		return true;
	}
	private function _archiveEntry($orderId): bool{
		$sql = "select e.*
			from " . E4S_TABLE_ENTRIES . " e
			where orderid = " . $orderId;

		$result = e4s_queryNoLog($sql);
		if ( !isset($this->counters->entries) ){
			$this->counters->entries = 0;
		}
		while ( $entry = $result->fetch_object() ) {
			if (!$this->_archiveObject($entry, E4S_TABLE_ENTRIES, E4S_TABLE_ENTRIES_ARCHIVE)){
				return false;
			}
			$this->counters->entries++;
		}
		if (!$this->_archiveTeamEntry($orderId)){
			return false;
		}
		return true;
	}
	private function _archiveTeamEntry($orderId): bool{
		$sql = "select e.*
			from " . E4S_TABLE_EVENTTEAMENTRIES . " e
			where orderid = " . $orderId;

		$result = e4s_queryNoLog($sql);
		if ( !isset($this->counters->teamEntries) ){
			$this->counters->teamEntries = 0;
		}
		while ( $entry = $result->fetch_object() ) {
			if (!$this->_archiveTeamAthletes($entry)){
				return false;
			}
			if (!$this->_archiveObject($entry, E4S_TABLE_EVENTTEAMENTRIES, E4S_TABLE_EVENTTEAMENTRIES_ARCHIVE)){
				return false;
			}
			$this->counters->teamEntries++;
		}

		return true;
	}
	private function _archiveTeamAthletes($teamEntry): bool{
		$sql = "select *
			from " . E4S_TABLE_EVENTTEAMATHLETE . "
			where teamentryid = " . $teamEntry->id;

		$result = e4s_queryNoLog($sql);
		if ( !isset($this->counters->teamAthletes) ){
			$this->counters->teamAthletes = 0;
		}
		while ( $teamAthleteEntry = $result->fetch_object() ) {
			if (!$this->_archiveObject($teamAthleteEntry, E4S_TABLE_EVENTTEAMATHLETE, E4S_TABLE_EVENTTEAMATHLETE_ARCHIVE)){
				return false;
			}
			$this->counters->teamAthletes++;
		}

		return true;
	}
	private function _archiveOrderItem($orderItem): bool{
		$sql = "select oim.*
			from " . E4S_TABLE_WCORDERITEMMETA . " oim
			where order_item_id = " . $orderItem->order_item_id;

		$result = e4s_queryNoLog($sql);
		$productId = 0;
		if ( !isset($this->counters->orderItems) ){
			$this->counters->orderItems = 0;
		}
		if ( !isset($this->counters->orderItemMeta) ){
			$this->counters->orderItemMeta = 0;
		}
		while ( $orderMetaItem = $result->fetch_object() ) {
			if ( $orderMetaItem->meta_key === WC_POST_PRODUCT_ID ){
				$productId = (integer)$orderMetaItem->meta_value;
			}
			if (!$this->_archiveObject($orderMetaItem, E4S_TABLE_WCORDERITEMMETA, E4S_TABLE_WCORDERITEMMETA_ARCHIVE)){
				return false;
			}
			$this->counters->orderItemMeta++;
		}
		if ( $productId !== 0 ){
			if (!$this->_archiveProductId($productId)){
				return false;
			}
		}
		if (!$this->_archiveObject($orderItem, E4S_TABLE_WCORDERITEMS, E4S_TABLE_WCORDERITEMS_ARCHIVE)){
			return false;
		}
		$this->counters->orderItems++;
		return true;
	}
	private function _archiveProduct($product): bool{
		if (!$this->_archivePostMeta($product->ID)){
			return false;
		}
		if ( !isset($this->counters->products) ){
			$this->counters->products = 0;
		}
		if ( ! $this->_archiveObject( $product, E4S_TABLE_POSTS, E4S_TABLE_POSTS_ARCHIVE ) ) {
			return FALSE;
		}
		$this->counters->products++;
		return true;
	}
	private function _archiveProductId($id): bool{
		$sql = "select p.*
			from " . E4S_TABLE_POSTS . " p
			where id = " . $id;

		$result = e4s_queryNoLog($sql);
		if ( $result->num_rows === 1 ) {
			$product = $result->fetch_object();
			if (!$this->_archiveProduct($product)){
				return false;
			}
		}
		return true;
	}
	private function _archivePostMeta($id): bool{
		$sql = "select pm.*
			from " . E4S_TABLE_POSTMETA . " pm
			where post_id = " . $id;
		if ( !isset($this->counters->postMeta) ){
			$this->counters->postMeta = 0;
		}
		$result = e4s_queryNoLog($sql);
		while ( $postMeta = $result->fetch_object() ) {
//			logtxt("Archive PostMeta: " . $postMeta->meta_key . " " . $postMeta->meta_value);
			if (!$this->_archiveObject($postMeta, E4S_TABLE_POSTMETA, E4S_TABLE_POSTMETA_ARCHIVE)){
				return false;
			}
			$this->counters->postMeta++;
		}
		return true;
	}
	private function _archiveObject($object, $table, $archiveTable): bool{
		$archiveTable = $this->_getArchiveTable($archiveTable);
		$properties = get_object_vars($object);
		$sql = "insert into $archiveTable (";
		$first = true;
		$values = '';
		$keyField = '';
		$keyValue = '';
		foreach ($properties as $key => $value) {
			if ($first){
				$keyField = $key;
				$keyValue = $value;
				$first = false;
			}else{
				$sql .= ",";
				$values .= ",";
			}
			$sql .= $key;
			$values .= "'" . addslashes($value) . "'";
		}
		$sql .= ") values (";
		$sql .= $values;
		$sql .= ")";
		$sql .= " on duplicate key update " . $keyField . " = " . $keyField;
		$result = e4s_queryNoLog($sql);

		if (!$result){
			return false;
		}
		$sql = "delete from $table where $keyField = $keyValue";
		e4s_queryNoLog($sql);

		return true;
	}
	private function _checkUnconnectedProducts(){
		// archive off products that are not used
		$sql = "
			select p.*
			from " . E4S_TABLE_POSTS . " p
			where post_type = '" . WC_POST_PRODUCT . "'
			  and post_title != '" . E4S_BASE_PRODUCT . "'
			  and p.post_date < '$this->floor'
			  and p.id not in (select cast(meta_value as unsigned) as product_id
			                   from " . E4S_TABLE_WCORDERITEMMETA ."
			                   where meta_key = '" . WC_POST_PRODUCT_ID . "')
			limit 0," . $this->processLimit ;
		if ( !isset($this->counters->unconnectedProducts) ){
			$this->counters->unconnectedProducts = 0;
		}
		$result = e4s_queryNoLog($sql);

		while ($product = $result->fetch_object()) {
			$sql = "
				select count(e.id) as count
				from " . E4S_TABLE_ENTRIES . " e
				where variationID = $product->ID
				union
				select count(t.id) as count
				from " . E4S_TABLE_EVENTTEAMENTRIES . " t
				where productid = $product->ID";
			$entryResult = e4s_queryNoLog($sql);
			$hasEntry = false;
			while($entry = $entryResult->fetch_object()) {
				if ( (int)$entry->count !== 0 ) {
					$hasEntry = true;
					break;
				}
			}
			if ( !$hasEntry ) {
				if (!$this->_archiveProduct($product) ){
					Entry4UIError(9103,"Failed to archive Product " . $product->ID);
				}
				$this->counters->unconnectedProducts++;
			}
		}
	}
	static function checkArchiveDB(){
		$database = e4sArchive::archiveName();

		$sql = 'CREATE DATABASE if not exists ' . $database ;
		e4s_queryNoLog($sql);
		return true;
	}
	private function _checkTables() {
		if ( !self::checkArchiveDB() ){
			Entry4UIError(9101, "Error: Can not use Archive Database");
		}
		if (!$this->_checkTable(E4S_TABLE_POSTS, E4S_TABLE_POSTS_ARCHIVE, 'ID')) {
			Entry4UIError(9101, "Error: Posts table errored");
		}
		if (!$this->_checkTable(E4S_TABLE_POSTMETA, E4S_TABLE_POSTMETA_ARCHIVE, 'meta_id')) {
			Entry4UIError(9101, "Error: PostMeta table errored");
		}
		if (!$this->_checkTable(E4S_TABLE_WCORDERITEMS, E4S_TABLE_WCORDERITEMS_ARCHIVE, 'order_item_id')) {
			Entry4UIError(9101, "Error: WCOrderItem table errored");
		}
		if (!$this->_checkTable(E4S_TABLE_WCORDERITEMMETA, E4S_TABLE_WCORDERITEMMETA_ARCHIVE, 'meta_id')) {
			Entry4UIError(9101, "Error: WCOrderItem table errored");
		}
		if (!$this->_checkTable(E4S_TABLE_ENTRIES, E4S_TABLE_ENTRIES_ARCHIVE, 'id')) {
			Entry4UIError(9101, "Error: Entries table errored");
		}
		if (!$this->_checkTable(E4S_TABLE_EVENTTEAMENTRIES, E4S_TABLE_EVENTTEAMENTRIES_ARCHIVE, 'id')) {
			Entry4UIError(9101, "Error: TeamEntries table errored");
		}
		if (!$this->_checkTable(E4S_TABLE_EVENTTEAMATHLETE, E4S_TABLE_EVENTTEAMATHLETE_ARCHIVE, 'id')) {
			Entry4UIError(9101, "Error: TeamAthlete table errored");
		}
		if (!$this->_checkTable(E4S_TABLE_LOG, E4S_TABLE_LOG_ARCHIVE, 'id')) {
			Entry4UIError(9101, "Error: Log table errored");
		}
	}

	private function _getArchiveTable($tablename){
		return $this->archiveDbName . '.' . $tablename . $this->suffix;
	}
	private function _checkTable($origTable, $archiveTable, $keyField):bool{
		// create a copy of the Entry4_posts table named Entry4_posts_archive
		$archiveTable = $this->_getArchiveTable($archiveTable);
		$origTable = DB_NAME . '.' . $origTable;
		try {
			e4s_queryNoLog( "CREATE TABLE IF NOT EXISTS " . $archiveTable  . " LIKE " . $origTable);
			$origResult    = e4s_queryNoLog( "desc " . $origTable  );
			$origColumns = array();
			$orderedColumns = array();
			while ($origDesc = $origResult->fetch_object()){
				$orderedColumns[] = $origDesc;
				$origColumns[$origDesc->Field] = $origDesc;
			}
			$archiveResult = e4s_queryNoLog( "desc " . $archiveTable);
			$archiveColumns = array();
			while ($archiveDesc = $archiveResult->fetch_object()){
				$archiveColumns[$archiveDesc->Field] = $archiveDesc;
			}
			$lastField = '';
			foreach ($orderedColumns as $orderedDesc){
				$origDesc = null;
				if ( array_key_exists($orderedDesc->Field, $origColumns) )  {
					$origDesc = $origColumns[ $orderedDesc->Field ];
				}
				$archiveDesc = null;
				if ( array_key_exists($orderedDesc->Field, $archiveColumns) )  {
					$archiveDesc = $archiveColumns[ $orderedDesc->Field ];
				}

				if ( !is_null($origDesc) and !is_null($archiveDesc) ) {
					// check field type is ok
					if ( $origDesc->Type !== $archiveDesc->Type and strtoupper($keyField) !== strtoupper($origDesc->Field) ) {
						$sql = 'alter table ' . $archiveTable . ' modify ' . $origDesc->Field . ' ' . $origDesc->Type . ' after ' . $lastField . ';';
						e4s_queryNoLog( $sql );
					}
				}
				if ( is_null($archiveDesc) ) {
					// add field to archive table
					$sql = 'alter table ' . $archiveTable . ' add column ' . $origDesc->Field . ' ' . $origDesc->Type . ' after ' . $lastField . ';';
					e4s_queryNoLog( $sql );
				}

				$lastField = $origDesc->Field;
			}
			$hasE4SKey = false;
			foreach($archiveColumns as $archiveDesc){
				if ( !array_key_exists($archiveDesc->Field, $origColumns) ) {
					if ( $archiveDesc->Field === $this->e4s_key){
						$hasE4SKey = true;
					}else {
						// remove field from archive table
						$sql = 'alter table ' . $archiveTable . ' drop column ' . $archiveDesc->Field . ';';
						e4s_queryNoLog( $sql );
					}
				}
			}

			if ( !$hasE4SKey ) {
				// tables exist but needs key field update
				e4s_queryNoLog( "alter table " . $archiveTable . " modify " . $keyField . " bigint unsigned not null;" );
				e4s_queryNoLog( "alter table " . $archiveTable . " drop primary key;" );
				// remove any existing key/index
				$sql = '
					select *
					FROM information_schema.TABLE_CONSTRAINTS WHERE
		            CONSTRAINT_SCHEMA = DATABASE() AND
		            TABLE_NAME        = "' . $archiveTable . '"
				';
				$result = e4s_queryNoLog( $sql );
				while ( $keyObj = $result->fetch_object() ) {
					$sql = 'alter table ' . $archiveTable . ' drop key ' . $keyObj->CONSTRAINT_NAME . ';';
					e4s_queryNoLog( $sql );
				}
				e4s_queryNoLog( "alter table " . $archiveTable . " add column `" . $this->e4s_key . "` bigint unsigned NOT NULL AUTO_INCREMENT, ADD PRIMARY KEY (`" . $this->e4s_key . "`);" );
			}
			return true;
		}catch (Exception $e){
			return false;
		}
	}
}