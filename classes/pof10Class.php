<?php

class pof10Class {
    public $urn;
    public $gender;
    public $events;
    public $athlete;
    public $pof10Record;

    public function __construct() {
    }

    public static function getWithURN($aocode, $urn) {
        $athlete = athleteClass::withURN($aocode, $urn);
        return pof10Class::getWithAthleteRow($athlete->athleteRow);
    }

    public static function getWithAthleteRow($athleteRow) {
        $instance = new self();

        if (is_null($athleteRow)) {
            return null;
        }

        if ($athleteRow['aocode'] !== E4S_AOCODE_EA) {
            return null;
        }
        $instance->athlete = $athleteRow;
        $instance->events = null;
        $instance->urn = $athleteRow['URN'];
        $instance->gender = $athleteRow['gender'];
        $instance->_setPof10Record();
        return $instance;
    }

    private function _setPof10Record() {
        $this->pof10Record = null;
        $content = $this->_getPof10Page();
        $this->_checkPof10Id($content);
        $this->_getPof10RecordFromContent($content);
    }

    private function _normaliseObj($obj) {
        $obj->id = (int)$obj->id;
        $obj->athleteId = (int)$obj->athleteId;
        $obj->info = base64_decode($obj->info);
        return $obj;
    }

    public function checkPof10RecordIsUptoDate($obj) {
        $lastChecked = strtotime($obj->checked . ' + 7 days');
        $now = time();
        if ($lastChecked < $now) {
            $obj = null;
        }
        return $obj;
    }

    private function _getPof10Page() {
        if ($this->urn === '') {
            return '';
        }
        $content = file_get_contents('http://www.thepowerof10.info/athletes/profile.aspx?ukaurn=' . $this->urn);
        $pof10Error = explode('cphBody_lblErrorMessage"><font color="Red"></font></span>', $content);

        if (sizeof($pof10Error) === 0) {
            return '';
        }


        return $content;
    }

    private function _getPof10RecordFromContent($pof10Content) {
        if (is_null($pof10Content)) {
            return null;
        }
        if (strpos($pof10Content, 'cphBody_divBestPerformances') === FALSE) {
            return null;
        }
        if (strpos($pof10Content, 'cphBody_pnlPerformances') === FALSE) {
            return null;
        }
        if (strpos($pof10Content, 'alternatingrowspanel') === FALSE) {
            return null;
        }
        // Get Performance Table
        $arr = explode('cphBody_divBestPerformances', $pof10Content);
        $arr = $arr[1];
        $arr = explode('cphBody_pnlPerformances', $arr);
        $arr = $arr[0];
        $arr = explode('alternatingrowspanel">', $arr);
        if (sizeof($arr) < 2) {
            return null;
        }
        $arr = $arr[1];
        $arr = explode('</table>', $arr);
        $arr = $arr[0];
        return $arr;
    }

    private function _checkPof10Id($content) {
        // if they have an athlete obj with a URN but not a pof10id, get it
        $athlete = $this->athlete;
        if (is_null($athlete)) {
            return;
        }
        $process = FALSE;
        if ($athlete['aocode'] === E4S_AOCODE_EA) {
            if ($athlete['URN'] !== '') {
                $pof10Id = $athlete['pof10id'];
                if (is_null($pof10Id) or $pof10Id === '') {
                    $process = TRUE;
                }
            }
        }
        if (!$process) {
            return;
        }
        $pof10Id = $this->_getPof10Id($content);
        if (is_numeric($pof10Id) and $pof10Id > 0) {
            $sql = 'update ' . E4S_TABLE_ATHLETE . '
                    set pof10id = ' . $pof10Id . '
                    where id = ' . $athlete['id'];
            e4s_queryNoLog($sql);
        }
    }

    private function _getPof10Id($content) {
        if (is_null($content)) {
            return null;
        }
        $pof10info = explode('aspx?athleteid=', $content);
        if (sizeof($pof10info) < 2) {
            return 0;
        }
        $pof10info = $pof10info[1];
        $pof10info = explode("\"><img id", $pof10info);
        $pof10info = $pof10info[0];
        $pof10info = explode('&amp;', $pof10info);
        return $pof10info[0];
    }

    public function updateE4SPBs() {
        // allows for events with different params e.g sp weight
        // pof10 puts the biggest first generally
        $processed = array();
        if (!isset($this->pof10Record)) {
            return null;
        }
        if (!isset($this->pof10Record->info)) {
            return null;
        }
        $content = $this->pof10Record->info;
        $pbs = $this->_getPerformanceRowsFromContent($content, TRUE);
        if (is_null($pbs)) {
            return null;
        }
        foreach ($pbs as $pb) {
            if (!array_key_exists($pb->eventid, $processed)) {
                $readSql = 'select * from ' . E4S_TABLE_ATHLETEPB . '
                        where athleteid = ' . $this->athlete['id'] . '
                        and eventid = ' . $pb->eventid;

                $readResult = e4s_queryNoLog($readSql);

                if ($readResult->num_rows < 1) {
                    $this->_createPB($pb);
                } else {
                    $this->_updatePB($pb);
                }
            }
            $processed [$pb->eventid] = TRUE;
        }
        return TRUE;
    }

    private function _getPerformanceRowsFromContent($content, $validOnly = FALSE) {
        $rows = explode('</tr>', $content);

        $pbArr = array();
        foreach ($rows as $row) {
            if ($row !== '') {
                if (strpos($row, '<tr class="bestperformancesheader">') === FALSE) {
                    $obj = $this->_getEventAndPB($row);
                    if (!$validOnly or $obj->eventid !== 0) {
                        $pbArr[] = $obj;
                    }
                }
            }
        }
        return $pbArr;
    }

    private function _getEventAndPB($row) {
        $obj = new stdClass();
        $content = explode('bestperformancesheader"><b>', $row);

        $content = $content[1];
        $event = explode('</b>', $content);
        $obj->pof10event = $event[0];
        $e4sEvent = $this->_getE4SEvent($event[0]);
        if (is_null($e4sEvent)) {
            $obj->eventid = 0;
            $obj->event = '';
        } else {
            $obj->eventid = (int)$e4sEvent['id'];
            $obj->event = $e4sEvent['Name'];
        }
        $pb = explode('LightPink;">', $content);
        $pb = $pb[1];
        $pb = explode('</td>', $pb);
        $obj->pb = $this->_getBestPB($pb[0]);
        $sb = str_replace('<td>', '', $pb[1]);
        $obj->sb = $this->_getBestPB($sb);
        return $obj;
    }

    private function _getE4SEvent($event) {
        $maxlen = 5;
        $len = $maxlen;
        $minlen = 2;
        if (is_null($this->events)) {
            $this->_getEvents();
        }
        $event = strtoupper($event);
        while ($len >= $minlen) {
            $pof10 = substr($event, 0, $len);

            if (array_key_exists($pof10, $this->events)) {
                return $this->events[$pof10];
            }
            $len--;
        }

        // Event not defined !
        $this->_emailPBIssue($event . ' not coded for . URN : ' . $this->urn);
        return null;
    }

    private function _emailPBIssue($msg) {
        logTxt($msg);
//        $subjectTest = "PB Issue";
//        $body = "Issue with PB<br>" . $msg;
//        $useSendTo = E4S_ERROR_EMAIL;
//        $body .= Entry4_emailFooter($useSendTo);
        //e4s_mail( $useSendTo, $subjectTest, $body);

    }

    private function _getEvents() {
        $sql = '
        select eg.*, ed.pof10, ed.Name
        from ' . E4S_TABLE_EVENTGENDER . ' eg,
             ' . E4S_TABLE_EVENTDEFS . " ed
             where eg.gender = '" . $this->gender . "'
             and eg.eventid = ed.id
             and ed.pof10 is not null";
        $result = e4s_queryNoLog($sql);

        $events = array();
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        foreach ($rows as $row) {
            $pof10 = strtoupper($row['pof10']);
            if (strpos($pof10, ',') !== FALSE) {
                $pof10 = explode(',', $pof10);
            } else {
                $pof10 = array($pof10);
            }
            foreach ($pof10 as $value) {
                $events[$value] = $row;
            }
        }
        $this->events = $events;

        return $events;
    }

    private function _getBestPB($passedPB) {
        $bestpb = 0;
        // includes a slash
        if (strpos($passedPB, '/') === FALSE) {
            $pbArr = array($passedPB);
        } else {
            $pbArr = explode('/', $passedPB);
        }
        foreach ($pbArr as $pb) {
            if ($pb === '') {
                $pb = '0';
            } else {
                if (strpos($pb, 'i') !== FALSE) {
                    $pb = str_replace('i', '', $pb);
                }
                if (strpos($pb, ':') !== FALSE) {
                    $pb = resultsClass::getResultInSeconds($pb);
                }
            }
            $pb = (float)$pb;
            if ($pb > $bestpb) {
                $bestpb = $pb;
            }
        }
        return $bestpb;
    }

    private function _createPB($pb) {
        $insert = 'insert into ' . E4S_TABLE_ATHLETEPB . ' (athleteid,eventid, pb, pbtext, sb, pof10pb)
                values (
                    ' . $this->athlete['id'] . ',
                    ' . $pb->eventid . ',
                    ' . $pb->pb . ",
                    '" . $pb->pb . "',
                    " . $pb->sb . ',
                    ' . $pb->pb . '
                )';
        e4s_queryNoLog($insert);
    }

    private function _updatePB($pb) {
        $update = 'update ' . E4S_TABLE_ATHLETEPB . ' 
                    set pb = ' . $pb->pb . ',
                        sb = ' . $pb->sb . ',
                        pof10pb = ' . $pb->pb . '
                   where athleteid = ' . $this->athlete['id'] . '
                   and   eventid = ' . $pb->eventid;

        e4s_queryNoLog($update);
    }
}