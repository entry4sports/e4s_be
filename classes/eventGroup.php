<?php

function e4s_getAthletesForEGId($obj) {
    $egId = checkFieldForXSS($obj, 'egid:Event Group ID');
    $egObj = eventGroup::getObj($egId, TRUE);
    $entries = $egObj->getAthletesForEG();
    Entry4UISuccess($entries);
}
function e4s_clearTrialInfo($compId){
    $egObj = new eventGroup($compId, TRUE);
    $egObj->clearTrialInfo();
}

function e4s_closeResults($obj){
	$compId = checkFieldForXSS($obj, 'compid:Competition ID');
	$egId = checkFieldForXSS($obj, 'egid:Event Group ID');
	$eventGroupObj = new eventGroup($compId, false);
	$egObj = $eventGroupObj->compObj->getEventGroupByEgId($egId);
	$eventGroupObj->markResultsState($egObj, false);
	Entry4UISuccess('');
}
class eventGroup {
    public $compId;
    public $compObj;
    public $readOnly;
    public $nameField = 'name';
    public $idField = 'id';
    public $egObj;

    public function __construct($compId, $readonly = FALSE) {
        $this->compId = $compId;
        $this->compObj = e4s_getCompObj($compId);
        $this->readOnly = $readonly;
        $this->egObj = null;
    }

	public function markResultsState($egObj, $open = true) {
		$this->egObj = $egObj;
		if ( $this->compObj->isOrganiser()) {
			$this->updateResultsPossible( true, $open );
		}else{
			Entry4UIError( 9205, 'You are not authorised to perform this function' );
		}
	}
    public static function getEntriesFromEgIds($egId) {
        $sql = 'select id
                from ' . E4S_TABLE_EVENTGROUPS . "
                where options like '%entriesFrom\":{\"id\":" . $egId . "%'";
        $result = e4s_queryNoLog($sql);
        $egIds = array();
        if ($result->num_rows > 1) {
            while ($obj = $result->fetch_object(E4S_EVENTGROUP_OBJ)) {
                $egIds[] = $obj->id;
            }
        }
        return $egIds;
    }

	public function getIncludedEntries($egObj){
		$options = $egObj->options;
		if ( isset($options->includeEntriesFromEgId) and isset($options->includeEntriesFromEgId->id) and $options->includeEntriesFromEgId->id > 0 ){
			return $options->includeEntriesFromEgId->id;
		}
		return 0;
	}
    public function clearTrialInfo() {
        $egs = $this->compObj->getEGObjs();
        foreach ($egs as $eg) {
            $eg->options->trialInfo = '';
            $eg->options = e4s_removeDefaultEventGroupOptions($eg->options);
            $sql = 'update ' . E4S_TABLE_EVENTGROUPS . " set options = '" . e4s_getOptionsAsString($eg->options) . "'
                  where id = " . $eg->id;
            e4s_queryNoLog($sql);
        }

    }

    public function cleanUpUnUsed() {
        $sql = 'delete
                FROM ' . E4S_TABLE_EVENTGROUPS . " 
                WHERE compid = {$this->compId} 
                and id not in ( select distinct(maxgroup) from " . E4S_TABLE_COMPEVENTS . " ce where ce.compid = {$this->compId} )";

        e4s_queryNoLog($sql);
    }

    public function delete() {
        $entries = $this->getEntriesForEg();
        $deleteSeedings = FALSE;

        if (sizeof($entries) !== 0) {
            foreach ($entries as $entry) {
                if ($entry->paid === E4S_ENTRY_QUALIFY) {
                    $deleteSeedings = TRUE;
                } else {
                    Entry4UIError(2305, 'Entries exist. Unable to delete.');
                }
            }
        }
        $egIds = eventGroup::getEntriesFromEgIds($this->egObj->id);
        $egIds[] = $this->egObj->id;
        $egIdsStr = implode(',', $egIds);

        $sql = 'delete from ' . E4S_TABLE_COMPEVENTS . '
                where maxgroup in (' . $egIdsStr . ')';
        e4s_queryNoLog($sql);

        $sql = 'delete from ' . E4S_TABLE_EVENTGROUPS . '
                where id in (' . $egIdsStr . ')';
        e4s_queryNoLog($sql);

        if ($deleteSeedings) {
            $sql = 'delete from ' . E4S_TABLE_SEEDING . '
                where eventgroupid in (' . $egIdsStr . ')';
            e4s_queryNoLog($sql);
        }
    }

    public function getEntriesForEg() {
        $entries = [];
        $sql = 'select entryId, paid
                from ' . E4S_TABLE_ENTRYINFO . '
                where egid = ' . $this->egObj->id;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows > 0) {
            while ($obj = $result->fetch_object()) {
                $entry = new stdClass();
                $entry->id = (int)$obj->entryId;
                $entry->paid = (int)$obj->paid;
                $entries[] = $entry;
            }
        }
        return $entries;
    }

    public function getEntriesFromEG($egId) {
        $egObj = $this->compObj->getEventGroupByEgId($egId);
        return $this->getEntriesFromEGObj($egObj);
    }

    public function getEntriesFromEGObj($egObj) {
        return $egObj->options->entriesFrom->id;
    }

    public function combinedIdFromObj($egObj) {
		if ( isset($egObj->eventDef) ){
			if ( isset($egObj->eventDef->options) ){
				if ( isset($egObj->eventDef->options->scoringSystem) ){
					return $egObj->eventDef->options->scoringSystem;
				}
			}
		}
		if ( isset($egObj->options->combinedId) ) {
			return $egObj->options->combinedId;
		}
		return 0;
    }

    public static function getObj($egId, $readOnly = FALSE) {
        $egObj = eventGroup::getEgObj($egId);
        if (is_null($egObj)) {
            Entry4UIError(8567, 'Unable to find event group for ' . $egId);
        }
        $instance = new self($egObj->compId, $readOnly);
        $instance->egObj = $egObj;
        return $instance;
    }

    public static function getEgObj($egId) {
        // used only to get compid, then get egObj from competition to ensure constant object is returned.
        $sql = 'select compId
                from ' . E4S_TABLE_EVENTGROUPS . '
                where id = ' . $egId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            return null;
        }
        $egObj = $result->fetch_object();
        $compObj = e4s_getCompObj((int)$egObj->compId);
        $egObj = $compObj->getEventGroupByEgId($egId);
        return $egObj;
    }

    public function __destruct() {
    }

//	$open  = 0 / closed. 1 / open
    public function updateResultsPossible($possible, $open = false) {
		$egId = $this->egObj->id;
        $egOptions = $this->getOptions();
        $egOptions->resultsPossible = $possible;
        $egOptions->resultsOpen = $open;
        $egOptions = e4s_removeDefaultEventGroupOptions($egOptions);
        $sql = 'update ' . E4S_TABLE_EVENTGROUPS . "
                set options = '" . e4s_getOptionsAsString($egOptions) . "'
                where id = " . $egId;
        e4s_queryNoLog($sql);
	    $this->sendEventResultsState($egId, $open);
    }

    public function getOptions() {
        return $this->egObj->options;
    }

    public function getEventGroups() {
        return $this->compObj->getEGObjs();
    }

    public function updateEG($obj, $sendEgSocketMsg = FALSE) {
        // $existObj = eventGroup::getEgObj($obj->egId);
        $existObj = $this->compObj->getEventGroupByEgId($obj->egId);
        $options = $this->_v1Tov2options($existObj->options, $obj->options);
        $qualifyObj = new e4sQualifyClass($options);
        $options = $qualifyObj->egBeingSaved();
        $options = e4s_removeDefaultEventGroupOptions($options);
        $startDate = e4s_iso_to_sql($obj->eventDateTimeISO);
        if ($existObj->eventNo !== $obj->eventNo or $existObj->typeNo !== $obj->typeNo) {
            $this->compObj->setNoEventNoGeneration(FALSE);
        }
        $typeNo = $obj->typeNo;
        if (isset($obj->type)) {
            $typeNo = $obj->type . $obj->typeNo;
        }
        $bibSortNo = $obj->bibSortNo;
        if (is_null($bibSortNo)) {
            $bibSortNo = 'null';
        }
        $notes = '';
        if (isset($obj->notes) and !is_null($obj->notes)) {
            $notes = $obj->notes;
        }
        $sql = 'update ' . E4S_TABLE_EVENTGROUPS . "
                set name = '" . $obj->name . "',
                    eventno = " . $obj->eventNo . ',
                    bibsortno = ' . $bibSortNo . ",
                    startdate = '" . $startDate . "',
                    notes = '" . $notes . "',
                    typeno = '" . $typeNo . "',
                    options = '" . e4s_getOptionsAsString($options) . "'
                where id = " . $obj->egId;
        e4s_queryNoLog($sql);
        $this->_updateCERecords($obj);
        if ($sendEgSocketMsg) {
            $this->sendNotesSocket($obj->egId, $notes);
        }
        return $this->compId;
    }

    private function _v1Tov2options($v1Options, $v2Options) {
        $v1Options = $this->_setMaxAthletes($v1Options, $v2Options->limits['maxAthletes']);
        $v1Options = $this->_setMaxInHeat($v1Options, $v2Options->limits['maxInHeat']);

        $v1Options->seed = $v2Options->seed;

        return $v1Options;
    }

    private function _setMaxAthletes($options, $value) {
        return eventGroup::setMaxAthletes($options, $value);
    }

    public static function setMaxAthletes($options, $value) {
        $options->maxathletes = $value;
        return $options;
    }

    private function _setMaxInHeat($options, $value) {
        return eventGroup::setMaxInHeat($options, $value);
    }

    public static function setMaxInHeat($options, $value) {
        $options->maxInHeat = $value;
        return $options;
    }

    public static function setSeedAge($options, $value) {
        $options->seed->age = $value;
        return $options;
    }

	public static function setAutoQualify($options, $value) {
		if ( !isset($options->seed) ){
			$options->seed = new stdClass();
		}
		if ( !isset($options->seed->qualifyToEg) ){
			$options->seed->qualifyToEg = new stdClass();
		}
		if ( !isset($options->seed->qualifyToEg->rules) ){
			$options->seed->qualifyToEg->rules = new stdClass();
		}
		$options->seed->qualifyToEg->rules->auto = $value;
		return $options;
	}
	public static function setNonAutoQualify($options, $value) {
		if ( !isset($options->seed) ){
			$options->seed = new stdClass();
		}
		if ( !isset($options->seed->qualifyToEg) ){
			$options->seed->qualifyToEg = new stdClass();
		}
		if ( !isset($options->seed->qualifyToEg->rules) ){
			$options->seed->qualifyToEg->rules = new stdClass();
		}
		$options->seed->qualifyToEg->rules->nonAuto = $value;
		return $options;
	}
	 public static function setSeedDoubleUp($options, $value) {
        $options->seed->doubleup = $value;
        return $options;
    }

    public static function setSeedGender($options, $value) {
        $options->seed->gender = $value;
        return $options;
    }

    private function _updateCERecords($obj) {
        $sql = 'select *
                from ' . E4S_TABLE_COMPEVENTS . '
                where maxgroup = ' . $obj->egId;
        $ceResult = e4s_queryNoLog($sql);
        $startDate = e4s_iso_to_sql($obj->eventDateTimeISO);
        while ($ceObj = $ceResult->fetch_object()) {
            $ceOptions = e4s_addDefaultCompEventOptions($ceObj->options);
            $ceOptions = $this->_setMaxInHeat($ceOptions, $obj->options->limits['maxInHeat']);
            $ceOptions = e4s_removeDefaultCompEventOptions($ceOptions);
            $sql = 'update ' . E4S_TABLE_COMPEVENTS . '
                set maxathletes = ' . $obj->options->limits['maxAthletes'] . ', ';
            if (isset($obj->isOpen)) {
                $sql .= ' isopen = ' . $obj->isOpen . ', ';
            }
            $sql .= " startdate = '" . $startDate . "',
                    sortdate = '" . $startDate . "',
                    options = '" . e4s_getOptionsAsString($ceOptions) . "'
                where id = " . $ceObj->ID;
            e4s_queryNoLog($sql);
        }
    }

    public function sendNotesSocket($egId, $notes) {
        $data = new stdClass();
        $data->egId = $egId;
        $data->message = $notes;
        e4s_sendSocketInfo($this->compId, $data, R4S_SOCKET_ENTRIES_EG_MESSAGE);
    }
	public function sendEventResultsState($egId, $open) {
		$data = new stdClass();
		$data->egId = $egId;
		$data->open = $open;
		e4s_sendSocketInfo($this->compId, $data, R4S_SOCKET_EVENT_RESULTS_STATE);
	}
    public function getAgeGroups() {
        $sql = 'select distinct a.id id, 
                        a.name name
                from ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_AGEGROUPS . ' a
                where ce.agegroupid = a.id
                and ce.maxgroup = ' . $this->egObj->id;
        $result = e4s_queryNoLog($sql);
        $arr = array();
        while ($obj = $result->fetch_object()) {
            $arr[] = $obj;
        }
        return $arr;
    }

    public function getAthletesForEG() {
        if (!$this->compObj->isOrganiser()) {
            Entry4UIError(9205, 'You are not authorised to perform this function');
        }
        if (is_null($this->egObj)) {
            Entry4UIError(9206, 'EventGroup Has not been set');
        }

        $sql = 'select a.id athleteId,
                       a.firstname firstName,
                       a.surname surName, 
                       a.dob, 
                       a.gender,
                       e.id entryId, 
                       e.paid,      
                       e.pb pb,
                       e.price price,
                       e.clubid clubId,
                       e.eventAgeGroup eventAgeGroup,
                       c.clubname clubName,
                       u.id userId,
                       u.user_nicename userName,
                       u.user_email userEmail,
                       u.display_name userDisplayName
                from ' . E4S_TABLE_ENTRIES . ' e left join ' . E4S_TABLE_ATHLETE . ' a on (e.athleteid = a.id) left join ' . E4S_TABLE_CLUBS . ' c on (e.clubid = c.id),
                     ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_USERS . ' u
                where e.compeventid = ce.id
                and   e.userid = u.id
                and   ce.maxgroup = ' . $this->egObj->id;
        $result = e4s_queryNoLog($sql);
        $retVal = new stdClass();

        $comp = new stdClass();
        $comp->id = $this->compObj->getID();
        $comp->name = $this->compObj->getName();
        $retVal->comp = $comp;

        $retVal->eventGroup = null;
        $entries = array();
        while ($entryObj = $result->fetch_object()) {
            $retObj = new stdClass();

            if (is_null($retVal->eventGroup)) {
                $egObj = new stdClass();
                $egObj->id = $this->egObj->id;
                $egObj->date = e4s_sql_to_iso($this->egObj->startDate);
                $egObj->name = $this->egObj->name;
                $egObj->eventNo = $this->egObj->eventNo;
                $retVal->eventGroup = $egObj;
            }

            $athleteObj = new stdClass();
            $athleteObj->id = $entryObj->athleteId;
            $athleteObj->firstName = $entryObj->firstName;
            $athleteObj->surName = $entryObj->surName;
            $athleteObj->dob = $entryObj->dob;
            $athleteObj->gender = $entryObj->gender;
            $athleteObj->ageGroup = $entryObj->eventAgeGroup;
            $retObj->athlete = $athleteObj;

            $eObj = new stdClass();
            $eObj->id = $entryObj->entryId;
            $eObj->paid = $entryObj->paid;
            $eObj->price = $entryObj->price;
            $eObj->pb = $entryObj->pb;
            $retObj->entry = $eObj;

            $clubObj = new stdClass();
            $clubObj->id = $entryObj->clubId;
            $clubObj->clubName = $entryObj->clubName;
            $retObj->club = $clubObj;

            $userObj = new stdClass();
            $userObj->id = $entryObj->userId;
            $userObj->userName = $entryObj->userName;
            $userObj->userDisplayName = $entryObj->userDisplayName;
            $retObj->user = $userObj;

            $entries[] = $retObj;
        }
        $retVal->eventGroup->entries = $entries;
        return $retVal;
    }

    public function listEventGroups($model, $exit) {
        $sql = "select *, DATE_FORMAT(startdate,'" . ISO_MYSQL_DATE . "') as startdateiso
        from " . E4S_TABLE_EVENTGROUPS . '
        where compid = ' . $this->compId;

        $startsWith = e4s_getStartsWith($model, 'name');
        if ($startsWith !== '') {
            $sql .= ' and ' . $startsWith;
        }

        $sql .= ' order by name ';
        $sql .= e4s_getPageLimit($model);

        $result = e4s_queryNoLog($sql);
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $newRows = array();
        foreach ($rows as $row) {
            $row['compId'] = (int)$row['compId'];
            $row['eventNo'] = (int)$row['eventNo'];
            if (is_null($row['bibSortNo'])) {
                $row['bibSortNo'] = '';
            } else {
                $row['bibSortNo'] = (int)$row['bibSortNo'];
            }
            $row['startdate'] .= e4s_getOffset($row['startdateiso']);
//            unset($row['startdate']);
            if (!$this->readOnly) {
                $row['options'] = e4s_getOptionsAsObj($row['options']);
            } else {
                unset($row['options']);
            }
            $newRows[] = $row;
        }
        if ($exit) {
            Entry4UISuccess('
        "data":' . json_encode($newRows, JSON_NUMERIC_CHECK));
        }
        return $newRows;
    }

    public function updateMultiEvent($model) {
        $egId = $model->id;
        $multiEgObj = new multiEventGroup($egId, $this->compObj);
        $model = $multiEgObj->updateEventGroup($model);
        return $model;
    }

    public function saveMultiEvent($model) {
        $egId = $model->id;
        $multiEgObj = new multiEventGroup($egId, $this->compObj);
        $model = $multiEgObj->updateEventGroup($model);
        return $model;
    }

    public function updateEGFromCEModel($cemodel, $partialUpdate = false) {
        // get or create the Event Group record from the ceModel sent in from builder
        $ceOptions = e4s_addDefaultCompEventOptions($cemodel->options);
        $model = new stdClass();
        $model->compId = $cemodel->compid;
        $model->startdate = $cemodel->startdate;
        // Need to generate egOptions here
        $model->options = e4s_addDefaultEventGroupOptions($cemodel->egoptions);
        $model->id = (int)$cemodel->maxgroup;
        $model->ceOptions = $ceOptions;

		if ( isset($ceOptions->includeEntriesFromEgId) ){
			$model->options->includeEntriesFromEgId->id = $ceOptions->includeEntriesFromEgId->id;
		}
        // SET HERE QUALIFY
        if (isset($ceOptions->seed)) {
            $model->options->seed = $ceOptions->seed;
        }
        $model->options->checkIn = $ceOptions->checkIn;
        $model->options->isTeamEvent = $ceOptions->isTeamEvent;
        $model->options->athleteSecurity = $ceOptions->athleteSecurity;
        $modelQualify = new e4sQualifyClass($model->options);
        // only interested in the id
        $modelQualify->removeName();
        $model->options = $this->_setMaxAthletes($model->options, $cemodel->maxathletes);

        $model->options = $this->_setMaxInHeat($model->options, $ceOptions->maxInHeat);

        // Needs setting
        $model->eventno = 0;
        $model->type = $cemodel->event['tf'];
        $model->name = $cemodel->groupname;

        return $this->updateEGFromCE($model, $partialUpdate);
    }

    public function updateEGFromCE($model, $partialUpdate = false) {
		$keyField = $this->nameField;
        if (isset($model->name)) {
            $keyValue = $model->name;
        }
        if (isset($model->groupname)) {
            $keyValue = $model->groupname;
        }
		//does the new group exist ( Merge )
	    $nameRes = $this->getEGByKey($keyField, $keyValue);
	    $res = $nameRes;
		if ( !$partialUpdate) {
			// updating all ce's for eg
			if ($nameRes->num_rows === 0 ) {
				// renaming event group
				if ( $model->id !== 0 ) {
					$keyField = $this->idField;
					$keyValue = $model->id;
				}
				$idRes = $this->getEGByKey( $keyField, $keyValue );
				$res   = $idRes;
			}
	    }
		// check if the eventGroup exists, use their seedings info
		if ( $res->num_rows > 0 ) {
			// split to an existing group, get target
			$row              = $res->fetch_object();
			if ( $model->id !== (int)$row->id) {
				$model->id            = (int) $row->id;
				$model->eventno       = (int) $row->eventNo;
				$nameOptions          = e4s_getOptionsAsObj( $row->options );
				$model->options->seed = $nameOptions->seed;
			}
		}

        $clearSeedings = FALSE;
	    $qualifyObj = new e4sQualifyClass($model->options);
	    $model->options = $qualifyObj->egBeingSaved($model->id);
        if ($res->num_rows === 0) {
            $egID = $this->createEventGroup($model);
        } else {
//            $row = $res->fetch_object();
            $options = e4s_addDefaultEventGroupOptions($row->options);
            $odSeed = $options->seed;
            $modelSeed = $model->options->seed;
            if ($odSeed->gender !== $modelSeed->gender) {
                $clearSeedings = TRUE;
            }
            if ($odSeed->age !== $modelSeed->age) {
                $clearSeedings = TRUE;
            }
            if ($odSeed->type !== $modelSeed->type) {
                $clearSeedings = TRUE;
            }
            $egID = $this->updateEventGroupByName($model);
        }

        if ($clearSeedings) {
            $seedingObj = new seedingV2Class($model->compId, $egID);
            $seedingObj->clear();
        }
        // EGs updated so ensure comp
        return $egID;
    }

    public function getEGByKey($key, $value) {
        if ($value === '') {
            $ret = new stdClass();
            $ret->num_rows = 0;
            return ($ret);
        }

        $exist = 'select *
          from ' . E4S_TABLE_EVENTGROUPS . "
          where compid = {$this->compId}
          and  {$key} = '{$value}'";

        return e4s_queryNoLog($exist);
    }

    public function createEventGroup($model) {
        $eventNo = $this->getNextEventNumber();
        $options = new stdClass();
        if (!is_null($model->options)) {
            $options = $model->options;
        }
	    $options->entriesFrom = new stdClass();
		if ( isset($model->ceOptions->entriesFrom) ) {
			if ( $model->ceOptions->entriesFrom->id > 0 ) {
				$options->entriesFrom = $model->ceOptions->entriesFrom;
			}
		}
        $options = e4s_removeDefaultEventGroupOptions($options);
        $options = e4s_getOptionsAsString($options);
        $sql = 'insert into ' . E4S_TABLE_EVENTGROUPS . '(compid,typeno, eventno, name, startdate, options)
            values (
            ' . $this->compId . ",
            '" . $model->type . $eventNo . "',
            " . $eventNo . ",
            '" . $model->name . "',
            '" . e4s_iso_to_sql($model->startdate) . "',
            '" . $options . "'
            )";

        e4s_queryNoLog($sql);
        return e4s_getLastID();
    }

    public function getNextEventNumber() {
        $sql = 'select max(eventNo) eventno
            from ' . E4S_TABLE_EVENTGROUPS . '
            where compid = ' . $this->compId;

        $results = e4s_queryNoLog($sql);
        if ($results->num_rows < 1) {
            return 1;
        }
        $row = $results->fetch_assoc();

        return (int)$row['eventno'] + 1;
    }

    public function updateEventGroupByName($model) {
        $this->updateEGRow($model);
        $egRow = $this->getEGRowByKey($this->nameField, $model->name);
        $model->id = $egRow['id'];
        // Update all CE Records for this egRow
        $this->updateCERecordsFromEGRow($model);
        // return the egRow id
        return (int)$egRow[$this->idField];
    }

    public function updateEGRow($model) {
        // check MultEventInfo
        $sql = 'update ' . E4S_TABLE_EVENTGROUPS . "
            set startdate = '" . $model->startdate . "',
                name = '" . $model->name . "'";

        if (!is_null($model->options)) {
            $this->updateMultiEvent($model);
            $model->options = e4s_removeDefaultEventGroupOptions($model->options);
            $sql .= ", options = '" . e4s_getOptionsAsString($model->options) . "'";
        }
        $sql .= ' where compid = ' . $this->compId . '
            and ';
		if ( $model->id > 0 ){
			$sql .= ' id = ' . $model->id;
        }  else{
			$sql .= ' name = "' . $model->name . '"';
		}

        e4s_queryNoLog($sql);
    }

    public function getEGRowByKey($key, $value) {
        $res = $this->getEGByKey($key, $value);
        if ($res->num_rows === 1) {
            return $res->fetch_assoc();
        }
        return null;
    }

    public function updateCERecordsFromEGRow($model) {
        $sql = 'select * 
                from ' . E4S_TABLE_COMPEVENTS . '
                where (maxgroup = ' . $model->id . ' and compid = ' . $this->compId . ')';
        if (isset($model->ceids)) {
            $sql .= ' or (id in (' . implode(',', $model->ceids) . ') )';
        }

        $res = e4s_queryNoLog($sql);
        if ($res->num_rows === 0) {
            return;
        }
        $ceRows = $res->fetch_all(MYSQLI_ASSOC);
        foreach ($ceRows as $ceRow) {
            $this->updateCERowFromEGRow($ceRow, $model);
        }
    }

    public function updateCERowFromEGRow($ceRow, $model) {
        $ceOptions = e4s_addDefaultCompEventOptions($ceRow['options']);
        if (!is_null($model->options) and isset($model->options->maxInHeat)) {
            $egOptions = $model->options;
            $ceOptions = $this->_setMaxInHeat($ceOptions, $this->_getMaxInHeat($egOptions));
        } else {
            $egOptions = new stdClass();
            $egOptions = $this->_setMaxInHeat($egOptions, $this->_getMaxInHeat($ceOptions));
            $egOptions = $this->_setMaxAthletes($egOptions, $ceRow['maxathletes']);
            $egOptions->trialInfo = '';
            $egOptions->reportInfo = '';
            $model->options = $egOptions;
            $this->updateEGRow($model);
        }
        $ceOptions = e4s_removeDefaultCompEventOptions($ceOptions);
        unset($ceOptions->eventGroupInfo);
        $update = 'update ' . E4S_TABLE_COMPEVENTS . " set maxgroup = '" . $model->id . "'," . "   startdate = '" . e4s_iso_to_sql($model->startdate) . "'," . "   sortdate = '" . e4s_iso_to_sql($model->startdate) . "'," . '   maxathletes = ' . $this->_getMaxAthletes($egOptions) . ',' . "   options = '" . e4s_getOptionsAsString($ceOptions) . "'" . ' where id  = ' . $ceRow['ID'];

        e4s_queryNoLog($update);
    }

	// trials
	public static function setMaxTrials($options, $value) {
		if ( !isset($options->trials) ){
			$options->trials = new stdClass();
		}
		$options->trials->max = $value;
		return $options;
	}
	public static function getMaxTrials($options, $defaultValue = null) {
		if (isset($options->trials)) {
			if ( isset($options->trials->max) ){
				return $options->trials->max;
			}
		}
		return $defaultValue;
	}
    public static function setMinTrials($options, $value) {
	    if ( !isset($options->trials) ){
		    $options->trials = new stdClass();
	    }
		$options->trials->min = $value;
		return $options;
	}
	public static function getMinTrials($options, $defaultValue = null) {
		if (isset($options->trials->min)) {
			return $options->trials->min;
		}
		return $defaultValue;
	}
	public static function clearTrials($options) {
		if ( isset($options->trials) ){
			unset($options->trials);
		}
		return $options;
	}
	public static function setAthleteCnt($options, $value) {
		if ( !isset($options->trials) ){
			$options->trials = new stdClass();
		}
		$options->trials->athleteCnt = $value;
		return $options;
	}
	public static function getAthleteCnt($options, $defaultValue = 0) {
		if (isset($options->trials->athleteCnt)) {
			return $options->trials->athleteCnt;
		}
		return $defaultValue;
	}
	public static function getJumpOrder($options, $defaultValue = "") {
		if (isset($options->trials->jumpOrder)) {
			return $options->trials->jumpOrder;
		}
		return $defaultValue;
	}
	public static function setJumpOrder($options, $value) {
		if ( !isset($options->trials) ){
			$options->trials = new stdClass();
		}
		$options->trials->jumpOrder = $value;
		return $options;
	}

	public static function getSeedGender($options, $defaultValue = FALSE) {
        if (isset($options->seed)) {
            if (isset($options->seed->gender)) {
                if ($options->seed->gender === TRUE or $options->seed->gender === 'true') {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }
        return $defaultValue;
    }
	public static function setProgressions($options, $value) {
		$options->progressions = $value;
		return $options;
	}
	public static function getProgressions($options, $defaultValue = null) {
		if (isset($options->progressions)) {
				return $options->progressions;
		}
		return $defaultValue;
	}
	public static function getSeedDoubleUp($options, $defaultValue = '') {
		if (isset($options->seed)) {
			if (isset($options->seed->doubleup)) {
				return $options->seed->doubleup;
			}
		}
		return $defaultValue;
	}
    public static function getSeedAge($options, $defaultValue = FALSE) {
        if (isset($options->seed)) {
            if (isset($options->seed->age)) {
                if ($options->seed->age === TRUE or $options->seed->age === 'true') {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }
        return $defaultValue;
    }

	public static function getAutoQualify($options, $defaultValue = 0) {
		if (isset($options->seed)) {
			if (isset($options->seed->qualifyToEg)) {
				if (isset($options->seed->qualifyToEg->rules)) {
					if (isset($options->seed->qualifyToEg->rules->auto)) {
						return $options->seed->qualifyToEg->rules->auto;
					}
				}
			}
		}
		return 0;
	}
	public static function getNonAutoQualify($options, $defaultValue = 0) {
		if (isset($options->seed)) {
			if (isset($options->seed->qualifyToEg)) {
				if (isset($options->seed->qualifyToEg->rules)) {
					if (isset($options->seed->qualifyToEg->rules->nonAuto)) {
						return $options->seed->qualifyToEg->rules->nonAuto;
					}
				}
			}
		}
		return 0;
	}
    public static function getResultsPossible($options, $defaultValue = TRUE) {
        if (isset($options->resultsPossible)) {
            return (int)$options->resultsPossible;
        }
        return $defaultValue;
    }

    private function _getResultsPossible($options, $defaultValue = TRUE) {
        return eventGroup::getResultsPossible($options, $defaultValue);
    }

    private function _getMaxInHeat($options, $defaultValue = 0) {
        return eventGroup::getMaxInHeat($options, $defaultValue);
    }

    public static function getMaxInHeat($options, $defaultValue = 0) {
        if (isset($options->maxInHeat)) {
            return (int)$options->maxInHeat;
        }
        return $defaultValue;
    }

    private function _getMaxAthletes($options, $defaultValue = 0) {
        return eventGroup::getMaxAthletes($options, $defaultValue);
    }

    public static function getMaxAthletes($options, $defaultValue = 0) {
        if (isset($options->maxathletes)) {
            return $options->maxathletes;
        }
        if (isset($options->maxAthletes)) {
            return $options->maxAthletes;
        }
        return $defaultValue;
    }

    public function moveEventGroup($model) {
        // get or create the group
        // have to create a model it requires
        $useModel = new stdClass();
        $useModel->compid = $this->compId;
        $useModel->startdate = $model->group->dateTime;
        $useModel->eventno = $model->group->eventNo;
        $useModel->name = $model->group->name;
        $useModel->ceids = $model->ceids;
        $useModel->options = null;
        $egID = $this->updateEGFromCE($useModel);
        $egRow = $this->getEGRowByKey('id', $egID);
        $useModel->id = $egID;

        $useModel->options = e4s_getOptionsAsObj($egRow['options']);
        $this->updateCERecordsFromEGRow($useModel);
        Entry4UISuccess('');
    }

    public function cloneEventGroups($toCompId) {
//     Get source maxGroups
        $egObjs = $this->compObj->getEGObjs();
        if (sizeof($egObjs) < 1) {
            // Non defined ???? or ticket only
            return;
        }
        $egMap = array();

        foreach ($egObjs as $sourceEGRow) {
            $sourceEgId = $sourceEGRow->id;
            $newEGID = $this->cloneEGRow($sourceEGRow, $toCompId);
            $egMap[$sourceEgId] = $newEGID;
            $this->updateClonedCERow($toCompId, $sourceEgId, $newEGID);
        }

        // now go through new ones updating any linkages
        $targetCompObj = e4s_getCompObj($toCompId);
        $targetEgObjs = $targetCompObj->getEGObjs();
        foreach($targetEgObjs as $obj) {
            // check options for linked EG's
            $update = FALSE;
            $options = $obj->options;
            $seed = $options->seed;
            $qualifyToEg = $seed->qualifyToEg;
            $qualifyToEgId = $qualifyToEg->id;
            if ($options->entriesFrom->id !== 0) {
                $options->entriesFrom->id = $egMap[$options->entriesFrom->id];
                $update = TRUE;
            }
            if (array_key_exists($qualifyToEgId, $egMap)) {
                $qualify = new stdClass();
                $qualify->id = $egMap[$qualifyToEgId];
                $options->seed->qualifyToEg = $qualify;
                $update = TRUE;
            }
            if ($update) {
                $options = e4s_removeDefaultEventGroupOptions($options);
                $sql = 'update ' . E4S_TABLE_EVENTGROUPS . "
                        set options = '" . e4s_getOptionsAsString($options) . "'
                        where id = " . $obj->id;
                e4s_queryNoLog($sql);
            }
        }
    }

    public function cloneEGRow($egrow, $tocompid) {
        $egOptions = e4s_removeDefaultEventGroupOptions($egrow->options);

		$startDate = e4s_adjustClonedDate($egrow->startDate);
        $insertSQL = 'Insert into ' . E4S_TABLE_EVENTGROUPS . ' (compid, eventno,typeno, name, startdate, options)
                      values (' . $tocompid . ',' . $egrow->eventNo . ',' . "'" . $egrow->typeNo . "'," . "'" . $egrow->name . "'," . "'" . $startDate . "'," . "'" . e4s_getOptionsAsString($egOptions) . "'" . ')';
        e4s_queryNoLog($insertSQL);
        return e4s_getLastID();
    }

    public function updateClonedCERow($tocompid, $oldegid, $newegid) {
        $updateSQL = 'update ' . E4S_TABLE_COMPEVENTS . ' ' . "set maxgroup = '" . $newegid . "'
                     where compid = " . $tocompid . "
                     and maxgroup = '" . $oldegid . "'";
        e4s_queryNoLog($updateSQL);
    }

    public function generateEventGroupOptions(&$cemodel) {
        $ceoptions = e4s_getOptionsAsObj($cemodel->options);
        $egoptions = new stdClass();
        $egoptions->trialInfo = '';
        $egoptions->reportInfo = '';
        $egoptions = $this->_setMaxAthletes($egoptions, $cemodel->maxathletes);
        $egoptions = $this->_setMaxInHeat($egoptions, $this->_getMaxInHeat($ceoptions));
        if (isset($ceoptions->eventGroupInfo->trialInfo)) {
            $egoptions->trialInfo = $ceoptions->eventGroupInfo->trialInfo;
            unset($ceoptions->eventGroupInfo->trialInfo);
        }
        if (isset($ceoptions->eventGroupInfo->reportInfo)) {
            $egoptions->reportInfo = $ceoptions->eventGroupInfo->reportInfo;
            unset($ceoptions->eventGroupInfo->reportInfo);
        }
        unset($ceoptions->eventGroupInfo);

        $cemodel->egoptions = $egoptions;
        $cemodel->options = $ceoptions;
    }

    public function checkAndInjectEventGroupInfo($egid, &$row, &$options) {
        if (!isset ($options->eventGroupInfo)) {
            $options->eventGroupInfo = new stdClass();
            $options->eventGroupInfo->trialInfo = '';
            $options->eventGroupInfo->reportInfo = '';
        }
        $egRow = null;
        if ($egid !== '') {
            $egRow = $this->getEGRowByKey('id', $egid);
        }

        if (!is_null($egRow)) {
            $egOptions = e4s_addDefaultEventGroupOptions($egRow['options']);
	        $options->includeEntriesFromEgId = $egOptions->includeEntriesFromEgId;
			if ( $options->includeEntriesFromEgId->id === 0){
				$options->includeEntriesFromEgId->eventNo = 0;
				$options->includeEntriesFromEgId->name = '';
				$options->includeEntriesFromEgId->isMultiEvent = false;
			}else{
				$includeFromEgObj = $this->compObj->getEventGroupByEgId($options->includeEntriesFromEgId->id);
				$options->includeEntriesFromEgId->eventNo = $includeFromEgObj->eventNo;
				$options->includeEntriesFromEgId->name = $includeFromEgObj->name;
				$options->includeEntriesFromEgId->isMultiEvent = false; // not used
			}
            $multiEGObj = new multiEventGroup(0, $this->compObj);
            $options = $multiEGObj->updateCEOptions((int)$egRow['id'], (int)$row['ID'], $options, $egOptions);
            $options->athleteSecurity = $egOptions->athleteSecurity;
            $options->eventGroupInfo->trialInfo = $egOptions->trialInfo;
            $options->eventGroupInfo->reportInfo = $egOptions->reportInfo;
            $qualifyObj = new e4sQualifyClass($egOptions);
            $qualifyId = $qualifyObj->getId();
            if ($qualifyId > 0) {
                // get latest eg name for qualifier
                $sql = 'select compid compId,
                               name,
                               eventNo
                        from ' . E4S_TABLE_EVENTGROUPS . '
                        where id = ' . $qualifyId;
                $result = e4s_queryNoLog($sql);
                if ($result->num_rows === 1) {
                    $egObj = $result->fetch_object();
                    $qualifyObj->setName($this->_checkEgName($egObj->name));
                    $qualifyObj->setCompId((int)$egObj->compId);
                    $qualifyObj->setEventNo((int)$egObj->eventNo);
                }
            }
            $options->seed = $qualifyObj->getSeed();
        } else {
            // event DOES NOT have an egGroup. need to create one
            $egRow = $this->e4s_checkAndCreate($row);
        }
        $egObj = new stdClass();
        $egObj->type = substr($egRow['typeNo'], 0, 1);
        $egObj->typeNo = (int)substr($egRow['typeNo'], 1);
        $egObj->id = (int)$egRow['id'];
        $egObj->eventNo = (int)$egRow['eventNo'];
        if (is_null($egRow['bibSortNo'])) {
            $egObj->bibSortNo = '';
        } else {
            $egObj->bibSortNo = (int)$egRow['bibSortNo'];
        }
        $egRow['name'] = $this->_checkEgName($egRow['name']);
        $egObj->name = $egRow['name'];
        $egObj->notes = $egRow['notes'];
        $row['eventGroupSummary'] = $egObj;
        // old properties
        $row['maxGroup'] = $egObj->id;
        $row['eventGroup'] = $egObj->name;
        $row['eventNo'] = $egObj->eventNo;
    }

    private function _checkEgName($name) {
        $name = str_replace('&lt;', '<', $name);
        $name = str_replace('&gt;', '>', $name);
        return $name;
    }

    public function e4s_checkAndCreate($row) {
        $egRow = $this->getEGRowByKey('name', $row['eventname']);

        if (is_null($egRow)) {
            $model = new stdClass();
            $model->options = '{}';
            $model->name = $row['eventname'];
            $model->startdate = $row['startdate'];
            $egid = $this->createEventGroup($model);
            $egRow = $this->getEGRowByKey('id', $egid);
        }
        e4s_updateCERecordsEventGroup($row['ID'], $egRow['id']);
        return $egRow;
    }

    public function makeEventsSequential(): bool {
        if ((int)$this->compId === 261 or (int)$this->compId === 265 or (int)$this->compId === 258 or (int)$this->compId === 269) {
            return FALSE;
        }

        $compObj = e4s_getCompObj($this->compId);
        if (!$compObj->allowEventNoGeneration()) {
            return FALSE;
        }

        $this->cleanUpUnUsed();

        $sql = '
            select distinct(e.tf) eventType, eg.*
            from ' . E4S_TABLE_EVENTGROUPS . ' eg,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTS . ' e
            where eg.compid = ' . $this->compId . '
            and eg.compId = ce.CompID
            and eg.id = ce.maxGroup
            and ce.EventID = e.id
            order by e.tf desc, eg.startdate
        ';

        $results = e4s_queryNoLog($sql);
        if ($results->num_rows < 1) {
            return FALSE;
        }
        $rows = $results->fetch_all(MYSQLI_ASSOC);
        $eventno = 1;
        $typeCounts = array();
        foreach ($rows as $row) {
            $eventType = $row['eventType'];
            if (!array_key_exists($eventType, $typeCounts)) {
                $typeCounts[$eventType] = 0;
            }
            $typeCounts[$eventType] += 1;

            $this->updateEventGroupNumber($row['id'], $eventno, $eventType, $typeCounts);

            $eventno++;
        }
        return TRUE;
    }

    public function updateEventGroupNumber($id, $eventno, $eventType, $typeCounts) {
        $sql = 'update ' . E4S_TABLE_EVENTGROUPS . '
            set eventno = ' . $eventno . ",
                typeNo = '" . $eventType . $typeCounts[$eventType] . "'
            where id = " . $id;

        e4s_queryNoLog($sql);
    }

    private function _setSetSeeded($options, $value) {
        return eventGroup::setSeeded($options, $value);
    }

    public static function setSeeded($options, $value) {
        if (!isset($options->seed)) {
            $options->seed = new stdClass();
        }
        $options->seed->seeded = $value;
        return $options;
    }

    private function _setFirstLane($options, $value) {
        return eventGroup::setFirstLane($options, $value);
    }

    public static function setFirstLane($options, $value) {
        if (!isset($options->seed)) {
            $options->seed = new stdClass();
        }
        $options->seed->firstLane = $value;
        return $options;
    }

    private function _getFirstLane($options, $defaultValue = 1) {
        return eventGroup::getFirstLane($options, $defaultValue);
    }

    public static function getFirstLane($options, $defaultValue = 0) {
        if (isset($options->seed)) {
            if (isset($options->seed->firstLane)) {
                return (int)$options->seed->firstLane;
            }
        }
        return 1;
    }

    private function _setLaneCount($options, $value) {
        return eventGroup::setLaneCount($options, $value);
    }

    public static function setLaneCount($options, $value) {
        if (!isset($options->seed)) {
            $options->seed = new stdClass();
        }
        $options->seed->laneCount = $value;
        return $options;
    }

    private function _getLaneCount($options, $defaultValue = 0) {
        return eventGroup::getLaneCount($options, $defaultValue);
    }

    public static function getLaneCount($options, $defaultValue = 0) {
        if (isset($options->seed)) {
            if (isset($options->seed->laneCount)) {
                return (int)$options->seed->laneCount;
            }
        }
        return $defaultValue;
    }

}