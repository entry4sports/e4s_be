<?php
const E4S_AGE_DEFAULT_COMP = 'C';
const E4S_AGE_DEFAULT_BASE = 'B';
class e4sAgeClass {
    public $compId;
    public $ageGroups;
    public $compAGs;
    public $baseAGs;
    public $allReceived;
    public $aoCode;
    public $useDate;

    public function __construct($defaultRetrieval, $key = '') {
        $this->ageGroups = array();
        $this->compAGs = array();
        $this->baseAGs = array();
        $this->allReceived = FALSE;
        $this->aoCode = '';
        $this->compId = 0;
        if ($defaultRetrieval === E4S_AGE_DEFAULT_COMP) {
            if ($key === '' or $key === 0) {
                Entry4UIError(9300, 'Invalid parameters');
            }
            $this->compId = (int)$key;
        }
        if ($defaultRetrieval === E4S_AGE_DEFAULT_BASE) {
            $this->aoCode = $key;
        }

        if ($this->aoCode === '') {
            $config = e4s_getConfig();
            $this->aoCode = $config['defaultao']['code'];
        }
        if ($defaultRetrieval === E4S_AGE_DEFAULT_BASE) {
            $this->_setBaseAgeGroups();
        }
    }

    public function getBaseAGForAthleteDOB($dob, $useDate) {
        if (sizeof($this->baseAGs) === 0) {
            $this->_setBaseAgeGroups();
        }
        if ($useDate !== $this->useDate) {
            $this->getAgeGroupsWithDOBs($this->baseAGs, $useDate);
        }
        $useBaseAG = null;
        foreach ($this->baseAGs as $baseAG) {
            if ($dob >= $baseAG->fromDate and $dob <= $baseAG->toDate) {
                $useBaseAG = $baseAG;
                break;
            }
        }
        return $useBaseAG;
    }

    public function getAgeGroupsWithDOBs($ageGroups, $forDate) {
        $this->useDate = $forDate;
        global $tz;
        $forDate = DateTime::createFromFormat(E4S_SHORT_DATE, $forDate, $tz);
        $forYear = (int)$forDate->format('Y');
        $forMonth = (int)$forDate->format('m');
        $forDay = (int)$forDate->format('d');
        foreach ($ageGroups as $ageGroup) {
            // get Min Age
            $useMinDay = $ageGroup->minAtDay;
            if (is_null($useMinDay)) {
                $useMinDay = $ageGroup->atDay;
            }
            if ($useMinDay === 0) {
                $useMinDay = $forDay;
            }

            $useMinMonth = $ageGroup->minAtMonth;
            if (is_null($useMinMonth)) {
                $useMinMonth = $ageGroup->atMonth;
            }
            if ($useMinMonth === 0) {
                $useMinMonth = $forMonth;
            }

            $useMinYear = $ageGroup->minYear;
            if (is_null($useMinYear)) {
                $useMinYear = $ageGroup->year;
            }

            $useMinYear = $forYear + $useMinYear;


            $minDate = ($useMinYear - $ageGroup->minAge) . '-' . $useMinMonth . '-' . $useMinDay;

            $date = new DateTime($minDate);

            $ageGroup->toDate = $date->format(E4S_SHORT_DATE);
            // get Max Age
            $useMaxDay = $ageGroup->atDay;

            if ($useMaxDay === 0) {
                $useMaxDay = $forDay;
            }

            $useMaxDay = (int)$useMaxDay;

            if ($useMaxDay < 10) {
                $useMaxDay = '0' . $useMaxDay;
            }

            $useMaxMonth = $ageGroup->atMonth;

            if ($useMaxMonth === 0) {
                $useMaxMonth = $forMonth;
            }
            $useMaxYear = $ageGroup->year;
            $useMaxYear = $forYear + $useMaxYear;

            $maxDate = ($useMaxYear - $ageGroup->maxAge - 1) . '-' . $useMaxMonth . '-' . $useMaxDay;
            $date = new DateTime($maxDate);
            $date->add(new DateInterval('P1D'));
            $ageGroup->fromDate = $date->format(E4S_SHORT_DATE);
        }
        return $ageGroups;
    }

    public function getAgeGroups($useId) {
        $sendAGs = array();
        if (is_null($useId)) {
            $useDate = Date('Y-m-d');
            $baseAGs = $this->getBaseAgeGroups();
            $baseAGs = $this->getAgeGroupsWithDOBs($baseAGs, $useDate);
            foreach ($baseAGs as $baseAG) {
                $sendAG = new stdClass();
                $sendAG->id = $baseAG->id;
                $sendAG->name = $baseAG->name;
                $sendAG->fromDate = $baseAG->fromDate;
                $sendAG->toDate = $baseAG->toDate;
                $sendAGs[] = $sendAG;
            }
        } else {
            $compObj = e4s_getCompObj($useId);
            $baseAGs = $compObj->getAgeGroups();
            foreach ($baseAGs as $baseAG) {
                $sendAG = new stdClass();
                $sendAG->id = $baseAG['id'];
                $sendAG->name = $baseAG['Name'];
                $sendAG->fromDate = $baseAG['fromDate'];
                $sendAG->toDate = $baseAG['toDate'];
                $sendAGs[] = $sendAG;
            }
        }
        return $sendAGs;
    }

    public function getBaseAgeGroups($aoCode = '') {
        if ($this->aoCode === '') {
            $this->aoCode = $aoCode;
        }
        if ($this->aoCode === '') {
            Entry4UIError(9310, 'No Organisation passed');
        }
        $this->_setBaseAgeGroups();
        return $this->baseAGs;
    }

    private function _setBaseAgeGroups() {
        $globalKey = "e4s_base_agegroups";
        $aoCode = $this->aoCode;
        if (sizeof($this->baseAGs) > 0) {
            return;
        }
        if ( array_key_exists($globalKey, $GLOBALS)){
            $this->baseAGs = $GLOBALS[$globalKey];
            return;
        }
        $sql = 'select *
            from ' . E4S_TABLE_AGEGROUPS . "
            where options like '%" . $aoCode . "%'
            order by minAge";

        $res = e4s_queryNoLog($sql);
        $baseAGRows = array();
        while ($row = $res->fetch_object(E4S_AGEGROUP_OBJ)) {
            $process = FALSE;
            foreach ($row->options as $option) {
                if ($option->aocode === $aoCode) {
                    if (isset($option->base) and $option->base === 1) {
                        $process = TRUE;
                    }
                }
            }
            if ($process) {
                if (!array_key_exists($row->id, $this->ageGroups)) {
                    $this->ageGroups[$row->id] = $row;
                }
                $baseAGRows[] = $row;
            }
        }
        $GLOBALS[$globalKey] = $baseAGRows;
        $this->baseAGs = $baseAGRows;
    }

    public function getAgeGroup($agId) {
        if (array_key_exists($agId, $this->ageGroups)) {
            return $this->ageGroups[$agId];
        }
        return $this->_readGroup($agId);
    }

    private function _readGroup($agId) {
        $sql = 'select *
                from ' . E4S_TABLE_AGEGROUPS . '
                where id = ' . $agId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4Error(5602, 'Failed to get Age Group(' . $agId . ')');
        }
        $row = $result->fetch_object(E4S_AGEGROUP_OBJ);
        $this->ageGroups[$agId] = $row;
        return $row;
    }
}