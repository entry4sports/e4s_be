<?php

class clubCompClass {
    public $ccId;
    public $compObj;
    public $data;
    public $clubs;
    public $userId;
    public $meta;

    public function __construct($compId, $ccId, $includeEntryData, $userId = 0) {
        $this->meta = null;
        $this->ccId = $ccId;
        $this->clubs = array();
        $this->userId = $userId;
        if ($userId === 0) {
            $this->userId = e4s_getUserID();
        }
        if ($compId !== 0) {
            $this->compObj = e4s_getCompObj($compId);
            $this->_getData($includeEntryData);
        }
    }

    public function forUser($userId) {
        $this->userId = $userId;
    }

    public function getCcId() {
        return $this->ccId;
    }

    public function hasEntryData() {
        return isset($this->clubComp->data->entryData);
    }

    private function _getData($includeEntryData = TRUE) {
        $data = new stdClass();

        $data->configData = $this->_getConfigData();
        if (is_null($data->configData)) {
            Entry4UIError(999, 'Unable to get Club Comp Data');
        }
        $data->clubs = $this->clubs;
        $this->data = $data;
        if ($includeEntryData) {
            $this->setEntryData();
        }

        return $this->data;
    }

    public function renumberEntries($egId, $entity) {
        $bibNos = $this->getBibNos();
        if (sizeof($bibNos) === 1) {
            return;
        }
        $subscript = 0;
        $sql = '
            select e.id,
                   e.teamBibNo
            from ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . " ce
            where ce.maxGroup = '" . $egId . "'
            and   ce.id = e.compeventid
            and   e.entity = '" . $entity . "'
            order by teamBibNo
        ";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return;
        }
        while ($obj = $result->fetch_object()) {
            $id = (int)$obj->id;
            if ($obj->teamBibNo !== $bibNos[$subscript]) {
                $uSql = '
                    update ' . E4S_TABLE_ENTRIES . "
                    set teamBibNo = '" . $bibNos[$subscript] . "'
                    where id = " . $id;
                e4s_queryNoLog($uSql);
            }
            $subscript++;
        }
    }

    public function getNextBibForEG($entity, $egId, $ceRow) {
        $sql = '
            select e.teamBibNo
            from ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . " ce
            where e.compeventid = ce.id
            and ce.maxgroup = '" . $egId . "'
            and e.entity = '" . $entity . "'            
        ";
        $result = e4s_queryNoLog($sql);
        $bibNos = $this->getBibNos();

//        if ( isset($this->compObj->getOptions()->teamBibOnAge) ){
        if (sizeof($bibNos) === 1) {
            $ageChar = $ceRow['minage'][0];
            return $bibNos[0][0] . $ageChar;
        } else {
            if ($result->num_rows > 0) {
                while ($obj = $result->fetch_object()) {
                    foreach ($bibNos as $element => $bibNo) {
                        if ($bibNo === $obj->teamBibNo) {
                            array_splice($bibNos, $element, 1);
                            break;
                        }
                    }
                }
            }
            return $bibNos[0];
        }
    }

    public function getBibNos() {
        return $this->data->configData->bibNos;
    }

    private function _getCurrentCompClubId() {
        $user = $GLOBALS[E4S_USER];
        $key = E4S_CLUBCOMP . $this->compObj->getID();
        $ccId = 0;
        if (isset($user->$key)) {
            $ccId = (int)$user->$key;
        }
        return $ccId;
    }

    private function _updateCurrentUser() {
        if ($this->userId === e4s_getUserID()) {
            e4s_getUserObj(TRUE, FALSE);
        }
    }

    public function delete() {
        $compClubs = $this->getClubComps();
        $compId = $this->compObj->getID();
        if (array_key_exists($compId, $compClubs)) {
            // does not already exists
            $sql = 'delete from ' . E4S_TABLE_USERMETA . '
                    where user_id = ' . $this->userId . "
                    and meta_key = '" . E4S_CLUBCOMP . $compId . "'";
            e4s_queryNoLog($sql);
        } else {
            Entry4UIError(7050, 'Club comp record does not exist : (' . $compId . '/' . $this->userId . ')');
        }
        $this->_updateCurrentUser();
        $this->_deleteUserAthletes();
        return $this->getClubComps();
    }

    private function _deleteUserAthletes() {
        $sql = 'delete from ' . E4S_TABLE_USERATHLETES . '
                where userid = ' . $this->userId;
        e4s_queryNoLog($sql);
    }

    public function update() {
        $key = E4S_CLUBCOMP . $this->compObj->getID();
        $compClubs = $this->getClubComps();
        $update = FALSE;
        if (array_key_exists($this->compObj->getID(), $compClubs)) {
            $update = TRUE;
        }
        if ($update) {
            $sql = 'update ' . E4S_TABLE_USERMETA . "
                    set meta_value = '" . $this->getCcId() . "'
                    where user_id = " . $this->userId . "
                    and meta_key = '" . $key . "'";
        } else {
            $sql = 'insert into ' . E4S_TABLE_USERMETA . ' (user_id,meta_key, meta_value)
                    values (
                        ' . $this->userId . ",
                        '" . $key . "',
                        '" . $this->getCcId() . "'
                    )";
        }
        e4s_queryNoLog($sql);
        $this->_deleteUserAthletes();
        $this->_createUserAthletes();
        $this->_updateCurrentUser();
        return $this->getClubComps();
    }

    private function _createUserAthletes() {
        $key = E4S_CLUBCOMP . $this->compObj->getID();
        $ccId = $this->getCcId();

        $sql = 'select user_id
                from ' . E4S_TABLE_USERMETA . "
                where meta_key = '" . $key . "'
                and   meta_value = '" . $ccId . "'";
        $result = e4s_queryNoLog($sql);
        $userIds = array();
        while ($obj = $result->fetch_object()) {
            $userIds[] = (int)$obj->user_id;
        }
        $sql = 'select distinct athleteId
                from ' . E4S_TABLE_USERATHLETES . '
                where userid in (' . implode(',', $userIds) . ')';
        $result = e4s_queryNoLog($sql);
        $insertSql = '';
        $sep = '';
        while ($obj = $result->fetch_object()) {
            if ($insertSql === '') {
                $insertSql = 'insert into ' . E4S_TABLE_USERATHLETES . ' (athleteid, userid)
                      values ';
            }
            $insertSql .= $sep . '(' . $obj->athleteId . ',' . $this->userId . ')';
            $sep = ',';
        }
        if ($insertSql !== '') {
            e4s_queryNoLog($insertSql);
        }
    }

    public function getClubComps() {
        $compClubs = $this->_getCoreClubCompInfo();
        if (empty($compClubs)) {
            return array();
        }
        $compIds = array();
        $clubCompIds = array();
        foreach ($compClubs as $compId => $compClub) {
            $compIds[] = $compId;
            $clubCompIds[] = $compClub->clubCompId;
        }

        $compSql = 'select id, name
                    from ' . E4S_TABLE_COMPETITON . '
                    where id in ( ' . implode(',', $compIds) . ')';
        $result = e4s_queryNoLog($compSql);
        while ($compRow = $result->fetch_object()) {
            $compObj = new stdClass();
            $compObj->id = $compRow->id;
            $compObj->name = $compRow->name;
            $compClubs[(int)$compRow->id]->comp = $compObj;
        }

        $clubs = array();
        $clubSql = 'select cc.id clubCompId, c.id clubId, c.clubname name
                   from ' . E4S_TABLE_CLUBS . ' c,
                        ' . E4S_TABLE_CLUBCOMP . ' cc
                   where cc.id in ( ' . implode(',', $clubCompIds) . ')
                    and  cc.clubid = c.id';
        $result = e4s_queryNoLog($clubSql);
        while ($clubRow = $result->fetch_object()) {
            $club = new stdClass();
            $club->id = (int)$clubRow->clubId;
            $club->name = $clubRow->name;
            $clubs[$clubRow->clubCompId] = $club;
        }

        foreach ($compClubs as $compClub) {
            $clubCompId = $compClub->clubCompId;
            $compClub->club = $clubs[$clubCompId];
        }
        return $compClubs;
    }

    private function _getCoreClubCompInfo() {
        $meta = $this->_getMeta();
        $compClubs = array();

        foreach ($meta as $key => $value) {
            if (strpos($key, E4S_CLUBCOMP) !== FALSE) {
                $compId = str_replace(E4S_CLUBCOMP, '', $key);
                $obj = new stdClass();
                $obj->clubCompId = (int)$value;
                $compClubs[(int)$compId] = $obj;
            }
        }
        return $compClubs;
    }

    private function _getMeta() {
        if (!is_null($this->meta)) {
            return $this->meta;
        }
        $sql = 'select *
            from ' . E4S_TABLE_USERMETA . '
            where user_id = ' . $this->userId;
        $result = e4s_queryNoLog($sql);
        $meta = array();
        while ($obj = $result->fetch_object(E4S_USERMETA_OBJ)) {
            $meta[$obj->key] = $obj->value;
        }
        $this->meta = $meta;
        return $meta;
    }

    private function _getConfigData() {
        $compId = $this->compObj->getID();
        $sql = '
            select   ccomp.id clubCompId, c.id clubId, c.clubName
                    ,ccat.id categoryId, ccat.categoryName, ccat.maxEntries, ccat.maxRelays
                    ,ccomp.bibNos
            from ' . E4S_TABLE_CLUBS . ' c,
                 ' . E4S_TABLE_CLUBCOMP . ' ccomp,
                 ' . E4S_TABLE_CLUBCATEGORY . " ccat
            where ccomp.compid = {$compId}
            and ccomp.clubId = c.id
            and   ccomp.categoryId = ccat.id
            order by clubname";

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            Entry4UIError(7500, 'Error retrieving Area Competition Configuration (' . $this->ccId . ')');
        }
        $retObj = null;
        $currentCCId = $this->_getCurrentCompClubId();

        while ($obj = $result->fetch_object()) {
            $obj = $this->_normaliseConfig($obj);
            if (is_null($retObj)) {
                $set = FALSE;
                if ($this->compObj->isOrganiser() and $currentCCId === 0) {
                    $set = TRUE;
                }
                if ($currentCCId === $obj->clubCompId) {
                    $set = TRUE;
                }
                if ($set) {
                    $retObj = $obj;
                }
            }
//            $obj->maxRelays += $this->compObj->getTeamExcludeCount();
            $this->clubs[] = $obj;
        }

        return $retObj;
    }

    private function _normaliseConfig($obj) {
        $obj->clubCompId = (int)$obj->clubCompId;
        $obj->clubId = (int)$obj->clubId;
        $obj->categoryId = (int)$obj->categoryId;
        $obj->maxEntries = (int)$obj->maxEntries;
        $obj->maxRelays = (int)$obj->maxRelays;
        $obj->bibNos = preg_split('~,~', $obj->bibNos);
        $obj->maxPerEvent = sizeof($obj->bibNos);
        return $obj;
    }

    public function getClubId() {
        return $this->data->configData->clubId;
    }

    public function setEntryData() {
        $this->data->entryData = $this->_getEntryData();
    }

	private function isExcludedFromCount($options) {
		$options = e4s_getOptionsAsObj($options);

		if ( isset($options->excludeFromCntRule) ) {
			if ( $options->excludeFromCntRule === TRUE ) {
				return 0;
			}
		}
		return 1;
	}
    private function _getEntryData() {

        $compId = $this->compObj->getID();
        $sql = "
            select e.AgeGroup eventAgeGroup
                 ,concat(e.firstname,' ', e.surname) entryName
                 ,e.teamBibNo teamBibNo
                 ,e.entryId
                 ,e.egId
                 ,e.egname eventGroup
                 ,e.ceOptions
                 ,0 isTeam
            from " . E4S_TABLE_ENTRYINFO . " e
            where e.compid = {$compId}
            and   e.entity = '1:" . $this->getClubId() . "'
            union
            select a.name eventAgeGroup
                 ,e.name entryName
                 ,e.id entryId
                 ,'' teamBibNo
                 ,ce.maxGroup egId
                 ,eg.name eventGroup
                 ,ce.options ceOptions
                 ,1 isTeam
            from " . E4S_TABLE_EVENTTEAMENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTGROUPS . ' eg,
                 ' . E4S_TABLE_AGEGROUPS . " a
            where e.ceid = ce.id
            and   ce.compid = {$compId}
            and   e.entityid = {$this->getClubId()}
            and   ce.maxGroup = eg.id
            and   a.id = ce.AgeGroupID
            order by teambibno
        ";

        $result = e4s_queryNoLog($sql);
        $entryData = new stdClass();
        $entryData->entries = [];
        $entryData->entryCnt = 0;
        $entryData->teams = [];
        $entryData->teamCnt = 0;

        while ($obj = $result->fetch_object(E4S_ENTRY_OBJ)) {
            $obj->egId = (int)$obj->egId;
            $obj->isTeam = (int)$obj->isTeam;
            $useObj = null;
            if (!isset($entryData->entries[$obj->egId])) {
                $useObj = new stdClass();
                $useObj->athletes = [];
                $useObj->teams = [];
                $useObj->eventGroup = $obj->eventGroup;
                $useObj->eventAgeGroup = $obj->eventAgeGroup;
                $useObj->egCount = 0;
            } else {
                $useObj = $entryData->entries[$obj->egId];
            }

            $useObj->egCount++;
            if ($obj->isTeam === 0) {
                $athlete = new stdClass();
                $athlete->entryId = $obj->entryId;
                $athlete->entryName = $obj->entryName;
                $athlete->teamBibNo = E4S_NO_BIBNO;
                if (!is_null($obj->teamBibNo)) {
                    $athlete->teamBibNo = $obj->teamBibNo;
                }
                $useObj->athletes[] = $athlete;
                $entryData->entryCnt += $this->isExcludedFromCount($obj->ceOptions);
                $entryData->entries[$obj->egId] = $useObj;
            } else {
                $useObj->teams[] = $obj->entryName;
                $entryData->teamCnt += $this->isExcludedFromCount($obj->ceOptions);
                $entryData->teams[$obj->egId] = $useObj;
            }
        }

//        $entryData->fullTeaminfo = $this->_getTeamData();

        return $entryData;
    }

    /**
     * @return bool
     */
    public function userEntryLimitReached(): bool {
        return $this->data->entryData->entryCnt >= $this->data->configData->maxEntries;
    }

    public function userEGLimitReached($egId): bool {
        if (!isset($this->data->entryData->entries[$egId]->egCount)) {
            return FALSE;
        }
        return $this->data->entryData->entries[$egId]->egCount >= $this->data->configData->maxPerEvent;
    }
}