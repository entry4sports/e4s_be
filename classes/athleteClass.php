<?php
const E4S_URN_NOT_FOUND_ERROR = 204;
const E4S_URN_AND_DATE_NOT_FOUND_ERROR = 206;
class athleteClass {
    public $athleteRow;
    public $athleteId;
    public $keySep;
    public $pof10Obj;
    public $pbObj;
    public $userAthletes;
    public $events;
    public $validateDOB;
	private $performances;
	public $cacheKey = 'e4sAthletes';

    public function __construct($checkDob = TRUE) {
        $this->keySep = ':';
        $this->validateDOB = $checkDob;
		$this->performances = null;
    }

	public function getAthleteFromCache($athleteId, $force = false){
		if ( !$force ) {
			if ( array_key_exists( $this->cacheKey, $GLOBALS ) ) {
				if ( array_key_exists( $athleteId, $GLOBALS[$this->cacheKey] ) ) {
					return $GLOBALS[$this->cacheKey][$athleteId];
				}
			}
		}
		return null;
	}
	public function addAthleteToCache($athlete):void{
		if ( !array_key_exists( $this->cacheKey, $GLOBALS ) ) {
			$GLOBALS[$this->cacheKey] = [];
		}
		$GLOBALS[$this->cacheKey][$athlete->id] = $athlete;

	}
    public static function withID($athleteId) {
        $instance = new self();

        $instance->athleteId = $athleteId;
        $instance->setAthlete('a.id', $athleteId);
        return $instance;
    }

    public function moveTo($newId, $options = null) {
        $origId = $this->athleteId;
        $curUserId = e4s_getUserID();
        // Move userAthlete Record
        // check new one exists
        $newRow = e4s_getAthleteRow('id', $newId, TRUE);

        $sql = 'select userid
                from ' . E4S_TABLE_USERATHLETES . '
                where athleteid in (' . $origId . ',' . $newId . ')';
        $result = e4s_queryNoLog($sql);
        $curUserFound = FALSE;
        $userIds = array();
        while ($obj = $result->fetch_object()) {
            $userIds[] = $obj->userid;
            $obj->userid = (int)$obj->userid;
            if ($obj->userid === $curUserId) {
                $curUserFound = TRUE;
            }
        }
        $sql = 'delete from ' . E4S_TABLE_USERATHLETES . '
                where athleteid in (' . $origId . ',' . $newId . ')';
        e4s_queryNoLog($sql);

        $sql = 'insert into ' . E4S_TABLE_USERATHLETES . ' ( athleteid, userid )
                values ';
        $sep = '';
        foreach ($userIds as $userId) {
            $sql .= $sep . '(' . $newId . ',' . $userId . ')';
            $sep = ',';
        }
        if (!$curUserFound) {
            $sql .= $sep . '(' . $newId . ',' . $curUserId . ')';
        }
        e4s_queryNoLog($sql);
        if ($origId === $newId) {
            return;
        }

        $sql = 'update ' . E4S_TABLE_BIBNO . '
                set athleteid = ' . $newId . '
                where athleteid = ' . $origId;
        e4s_queryNoLog($sql);

        $sql = 'update ' . E4S_TABLE_ENTRIES . '
                set athleteid = ' . $newId . '
                where athleteid = ' . $origId;
        e4s_queryNoLog($sql);

        $sql = 'update ' . E4S_TABLE_EVENTTEAMATHLETE . '
                set athleteid = ' . $newId . '
                where athleteid = ' . $origId;
        e4s_queryNoLog($sql);

        $sql = 'update ' . E4S_TABLE_POSTS . "
                set post_content = replace(post_content,'athleteid\":" . $origId . ",','athleteid\":" . $newId . ",')
                where post_content like '%athleteid\":" . $origId . ",%'";
        e4s_queryNoLog($sql);

        if (!is_null($options)) {
            $sql = 'update ' . E4S_TABLE_ATHLETE . "
            set options ='" . $options . "'
            where id = " . $newId;
            e4s_queryNoLog($sql);
        }
        $this->delete($origId);
    }

    public function setAthlete($initkey, $initvalue) {
        $keys = explode($this->keySep, $initkey);
        $values = explode($this->keySep, $initvalue);
        $cont = '';

        $sql = 'select a.*, c.clubname club
               from ' . E4S_TABLE_ATHLETE . ' a 
                        left join ' . E4S_TABLE_CLUBS . ' c on a.clubid = c.id 
               where ';

        foreach ($keys as $item => $key) {
            $sql .= $cont . $key . " = '" . $values[$item] . "'";
            $cont = ' and ';
        }
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            $this->athleteRow = null;
            return FALSE;
        }
        $athleteRow = $result->fetch_assoc();
//        var_dump($athleteRow);
        $athleteRow = athleteClass::normaliseArray($athleteRow);
        $clubId = (int)$athleteRow['clubId'];
        if ($clubId === 0) {
            $athleteRow['clubId'] = E4S_UNATTACHED_ID;
            $athleteRow['club'] = E4S_UNATTACHED;
        }

        $club2Id = $athleteRow['club2Id'];

        $athleteRow['club2'] = '';
        // if 2nd claim club
        if ($club2Id !== 0) {
            $claimSQL = 'select *
                 from ' . E4S_TABLE_CLUBS . '
                 where id = ' . $club2Id;
            $claimresult = e4s_queryNoLog($claimSQL);
            if ($claimresult->num_rows === 1) {
                $claimClub = $claimresult->fetch_assoc();
                $athleteRow['club2'] = $claimClub['Clubname'];
            }
        }

        // if school id exists
        $schoolId = $athleteRow['schoolId'];

        $athleteRow['school'] = '';
        // if school specified
        if ($schoolId !== 0) {
            $claimSQL = 'select *
                 from ' . E4S_TABLE_CLUBS . '
                 where id = ' . $schoolId;
            $claimresult = e4s_queryNoLog($claimSQL);
            if ($claimresult->num_rows === 1) {
                $claimClub = $claimresult->fetch_assoc();
                $athleteRow['school'] = $claimClub['Clubname'];
            }
        }

        $athleteRow['options'] = e4s_addDefaultAthleteOptions($athleteRow['options']);
        $this->athleteRow = $athleteRow;
        return $athleteRow;
    }

    public static function normaliseArray($row) {
        $row['id'] = (int)$row['id'];
        $row['clubId'] = (int)$row['clubid'];
        $row['club2Id'] = (int)$row['club2id'];
        $row['schoolId'] = (int)$row['schoolid'];
        $row['classification'] = (int)$row['classification'];
        $row['firstName'] = formatAthleteFirstname($row['firstName']);
        $row['surName'] = formatAthleteSurname($row['surName']);
//        $row['image'] = $this->_setImage($row);
        unset($row['firstname']);
        unset($row['surname']);
//        unset($row['clubid']);
//        unset($row['club2id']);
        unset($row['schoolid']);
        unset($row['club_donotuse']);
        if (array_key_exists('image', $row)) {
            $row['image'] = athleteClass::getImage($row['image'], $row['pof10id']);
        } else {
            Entry4UIError(9034, 'No Image Set for Athlete');
            $row['image'] = '';
        }
        return $row;
    }

    public static function getImage($image, $pof10Id) {
        $pof10Image = 'https://www.thepowerof10.info/athletes/profilepic.aspx?athleteid=';
        if (is_null($image) or $image === '') {
            $image = $pof10Image;
            if (!is_null($pof10Id)) {
                $image .= $pof10Id;
            } else {
                $image .= 'unknown';
            }
        }
        return $image;
    }

    public static function withURN($aoCode, $urn) {
        $instance = new self();

        $instance->setAthlete('aoCode' . $instance->keySep . 'urn', $aoCode . $instance->keySep . $urn);
        return $instance;
    }

    private function _getURNFromEA($firstname, $surname, $dob) {
        $date = date_create($dob);
        $dob = date_format($date, 'd+F+Y');
        $eaObj = new eaRegistrationClass(E4S_EA_LIVE);
        $urn = $eaObj->getURN($firstname, $surname, $dob);
        return $urn;
    }

    private function _getURNFromAAI($firstname, $surname, $dob) {
        $date = date_create($dob);
        $dob = date_format($date, 'd+F+Y');
        $eaObj = new eaRegistrationClass(E4S_EA_LIVE);
        $urn = $eaObj->getURN($firstname, $surname, $dob);
        return $urn;
    }

    private function _tryExternalForURN($model) {
        $urn = $model->urn;
        if (!is_null($urn) and ('' . $urn) !== '') {
            return $urn;
        }

        $dob = date_create($model->dob);
        $thisYear = date('Y');
        $dobY = date_format($dob, 'Y');
        if ((int)$thisYear - (int)$dobY < 10) {
            return null;
        }
        if (isset($model->options)) {
            $options = e4s_addDefaultAthleteOptions($model->options);
            $check = str_replace("'", '', $model->firstName . $model->surName . $model->dob);
            if (!is_null($options->urnChecked) and $options->urnChecked === $check) {
                // checked and nothing has changed
                return null;
            }
            $options->urnChecked = $check;
            $options = e4s_removeDefaultAthleteOptions($options);
            $sql = 'update ' . E4S_TABLE_ATHLETE . "
                set options = '" . e4s_getOptionsAsString($options) . "'
                where id = " . $model->id;
            e4s_queryNoLog($sql);
        }
        $newURN = null;
        if ($model->aoCode === E4S_AOCODE_EA) {
            $newURN = $this->_getURNFromEA($model->firstName, $model->surName, $model->dob);
        }
        if ($model->aoCode === E4S_AOCODE_AAI) {
//            $newURN = $this->_getURNFromAAI($model->firstName, $model->surName, $model->dob);
        }
        if (is_null($newURN)) {
            return null;
        }
        if ($urn !== $newURN) {
            // check if URN does not already exist
            $sql = 'select id
                    from ' . E4S_TABLE_ATHLETE . "
                    where aocode = '" . $model->aoCode . "'
                    and urn = " . $newURN;
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows !== 0) {
                return null;
            }
            $sql = 'update ' . E4S_TABLE_ATHLETE . "
            set aocode = '" . $model->aoCode . "',
                urn = " . $newURN . '
            where id = ' . $model->id;
            e4s_queryNoLog($sql);

        }
        return $newURN;
    }

    public function checkModelAgainstExtSystem($model, $date = null) {
        $urn = $model->urn;
        if (is_null($urn) or $urn === 0) {
            $urn = $this->_tryExternalForURN($model);
        } else {
            $urn = $this->getValidURN($urn);
        }
        if (is_null($urn)) {
            return $model;
        }
        $model->urn = $urn;
        // if ANI, don't allow any change of std fields unless E4S
        if ($model->aoCode === E4S_AOCODE_ANI) {
            return $this->searchE4SDataForRegisteredAthlete($model);
        }

        $extAthlete = $this->getExternalAthleteForURN($model->aoCode, $urn, $date);

        if (is_null($extAthlete)) {
            $this->_athleteNotFound($urn, $model->aoCode);
        }
        if ($model->aoCode === E4S_AOCODE_EA) {
            $extAthlete = $this->_getE4SClubInfoFromEA($extAthlete);
        }
        if (is_null($extAthlete)) {
            $this->_athleteNotFound($urn, $model->aoCode);
        }
        return $this->_syncAthleteWithExt(null, $extAthlete, $model->dob);
    }

    public function getValidURN($urn) {
        if (intval($urn) === (int)$urn) {
            $urn = $urn;
        }
        $urn = trim($urn);
        if (strpos($urn, ' ')) {
            $urn = explode(' ', $urn);
            $urn = $urn[1];
        }

        if (strpos(strtolower($urn), 'ANI')) {
            $urn = explode('ani', strtolower($urn));
            $urn = $urn[1];
        }
        return $urn;
    }

    public function searchE4SDataForRegisteredAthlete($model) {
        $urn = $model->urn;
        if (is_null($urn) or $urn === 0) {
            $urn = '';
        } else {
            $urn = $this->getValidURN($urn);
        }
        $aoCode = $model->aoCode;
        if (is_null($aoCode)) {
            $aoCode = '';
        }
        if ($urn === '' or $aoCode === '') {
            Entry4Error(2817, 'No URN Information passed');
        }
        $sql = 'select * ,DATEDIFF(activeEndDate, curDate()) paidup
                from ' . E4S_TABLE_ATHLETE . "
                where urn = '" . $urn . "'
                and   aocode = '" . $aoCode . "'";

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            if (!e4s_isAAIDomain() and ($aoCode === E4S_AOCODE_AAI or $aoCode === E4S_AOCODE_ANI)) {
                // get from AAI
                $this->_getAthleteFromAAI($aoCode, $urn);
                $result = e4s_queryNoLog($sql);
            }
        }

        if ($result->num_rows === 0) {
            $this->_athleteNotFound($urn, $model->aoCode);
        }
        $obj = $result->fetch_object();
        if ($obj->dob !== $model->dob) {
            $this->_athleteDOBMismatch($urn, $model->aoCode);
        }
        $this->setAthlete('a.id', $obj->id);
        return e4s_getDataAsType($this->getRow(), E4S_OPTIONS_OBJECT);
    }

    public function returnAthleteToEA($obj) {
        $aoCode = checkFieldForXSS($obj, 'aocode:Athlete AO Code');
        $urn = checkFieldForXSS($obj, 'urn:Athlete URN');
        $code = checkFieldForXSS($obj, 'code:Security Code');
        $validate = checkFieldForXSS($obj, 'valid:Validate Code');

        if ($this->_getSecurityCodeInfo($aoCode, $urn, $validate) !== $code) {
            Entry4UIError(5890, 'Invalid parameters passed');
        }
        $this->setAthlete('aoCode' . $this->keySep . 'urn', $aoCode . $this->keySep . $urn);
        Entry4UISuccess('"data":' . json_encode($this->getRow()));
    }

    public function _getAthleteFromAAI($aoCode, $urn) {

        $validate = rand(1, 99);
        $code = $this->_getSecurityCodeInfo($aoCode, $urn, $validate);

        $curl = curl_init();
        $url = 'https://' . E4S_AAI_DOMAIN . '/wp-json/e4s/v5/athlete/getaai?aocode=' . $aoCode . '&urn=' . $urn . '&code=' . $code . '&validate=' . $validate;

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        $result = e4s_getDataAsType($result, E4S_OPTIONS_OBJECT);
        $this->_checkAAIDataExists($result->data);
        return TRUE;
    }

    private function _checkAAIDataExists($aaiData) {
        // first check club
        $sql = 'select id
                from ' . E4S_TABLE_CLUBS . "
                where clubname = '" . addslashes($aaiData->club) . "'";
        $result = e4s_queryNoLog($sql);
        $clubId = 0;
        if ($result->num_rows === 1) {
            $obj = $result->fetch_object();
            $clubId = (int)$obj->id;
        }
        if ($clubId === 0) {
            // need to create club for Ireland
            $clubId = $this->_createClubInEAForAAI($aaiData);
        }
        $sql = 'insert into ' . E4S_TABLE_ATHLETE . " (firstname, surname, aocode, urn,dob,gender,clubid,type,activeEndDate)
                values(
                    '" . addslashes($aaiData->firstName) . "',
                    '" . addslashes($aaiData->surName) . "',
                    '" . $aaiData->aocode . "',
                    '" . $aaiData->URN . "',
                    '" . $aaiData->dob . "',
                    '" . $aaiData->gender . "',
                    " . $clubId . ",
                    'A',
                    '" . $aaiData->activeEndDate . "'
                )";
        e4s_queryNoLog($sql);
    }

    private function _createClubInEAForAAI($aaiData) {
        $sql = 'select id
                from ' . E4S_TABLE_AREA . "
                where name = '";
        $country = 'Ireland';
        if ($aaiData->aoCode === E4S_AOCODE_AAI) {
            $sql .= 'Ireland';
        } else {
            $country = 'Northern Ireland';
            $sql .= 'Ireland';
        }
        $sql .= "'";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(7340, 'Ireland not found');
        }
        $obj = $result->fetch_object();
        $areaId = (int)$obj->id;
        $sql = 'insert into ' . E4S_TABLE_CLUBS . " (clubname, region, country, areaid, clubtype, active)
                values(
                    '" . addslashes($aaiData->club) . "',
                    'Ireland',
                    '" . $country . "',
                    " . $areaId . ",
                    'C',
                    true
                )";
        e4s_queryNoLog($sql);
        return e4s_getLastID();
    }

    private function _getSecurityCodeInfo($aoCode, $urn, $validate) {
        if ($validate < 10) {
            return md5($urn . $validate . $aoCode);
        }
        if ($validate > 75) {
            return md5($urn . $aoCode . $validate);
        }
        return md5($validate . $urn . $aoCode);
    }

    private function _athleteNotFound($urn, $system) {
		$msg = 'Unknown System : [' . $system . ']';
        switch ($system) {
            case E4S_AOCODE_AAI:
                $msg = $urn . ' is not a valid URN. If you have entered this correctly, please check on AAI Registrations portal for your registration.';
                break;
            case E4S_AOCODE_ANI:
                $msg = $urn . ' is not a valid URN. If you have entered this correctly, please check on ANI Registrations portal for your registration.';
                break;
            case E4S_AOCODE_EA:
                $msg = $urn . ' is not a valid URN. If you have entered this correctly, please check on England Athletics Registration portal for your registration.';
                break;
        }

        Entry4UIError(8550, $msg, E4S_URN_NOT_FOUND_ERROR);
    }

    // Check the EA system for the athlete.

    private function _athleteDOBMismatch($urn, $system) {
	    $msg = 'Unknown System : [' . $system . ']';
        switch ($system) {
            case E4S_AOCODE_AAI:
                $msg = 'Athlete with URN of ' . $urn . ' does not match. If you have entered this correctly, please check on AAI Registrations portal for your registration.';
                break;
            case E4S_AOCODE_ANI:
                $msg = 'Athlete with URN of ' . $urn . ' does not match. If you have entered this correctly, please check on ANI Registrations portal for your registration.';
                break;
            case E4S_AOCODE_EA:
                $msg = 'Athlete with URN of ' . $urn . ' does not match. If you have entered this correctly, please check on England Athletics Registration portal for your registration.';
                break;
        }

        Entry4UIError(8550, $msg, E4S_URN_AND_DATE_NOT_FOUND_ERROR);
    }

    // Check the EA system for the athlete.

    public function getRow($withPBs = FALSE) {
        if (is_null($this->athleteRow)) {
            return null;
        }
        if ($withPBs and !array_key_exists('pbInfo', $this->athleteRow)) {
            $this->athleteRow['pbInfo'] = $this->getPBs();
        }
        return $this->athleteRow;
    }

	// Get the athlete's PBs
	// $compObj is the competition object to get the PBs for
	// This is passed from getCompEvents and used to check if the comp requires PBs within a certain period

	public function getPBsV2():array{
		$perfObj = new perfAthlete();
		$athleteId = $this->getID();
		$pbs = $perfObj->getPerformances(array($athleteId));
		if ( !array_key_exists($athleteId, $pbs) ){
			return array();
		}
		return $pbs[$athleteId];
	}
	public function getPBs():array{
        $pbSQL = '
            select ev.ID eventid,
               Name eventname,
               ev.min,
               ev.max,
               ev.options evoptions,
               u.id uomid,
               u.uomtype,
               u.uomoptions,
               pb.id pbid,
               pb.pb,
               pb.pbtext pbText,
               pof10pb, 
               sb
        from ' . E4S_TABLE_UOM . ' u,
             ' . E4S_TABLE_EVENTS . ' ev left join ' . E4S_TABLE_ATHLETEPB . ' pb on (pb.athleteid = ' . $this->getID() . '
                 and pb.eventid = ev.id)
        where ev.uomid = u.id
        and ev.id in (
            select distinct e.EventID
            from ' . E4S_TABLE_ENTRYINFO . ' e
            where athleteid = ' . $this->getID() . '
            union
            select pb2.eventid
            from ' . E4S_TABLE_ATHLETEPB . ' pb2
            where pb2.athleteid = ' . $this->getID() . '
            )
        ';

        $result = e4s_queryNoLog($pbSQL);
        $pbArr = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $pbArrFixed = array();
        foreach ($pbArr as $pb) {
            $pbArrFixedArr = array();

            $pbArrFixedArr['eventid'] = (int)$pb['eventid'];
            if (is_null($pb['pb'])) {
                $pbArrFixedArr['pb'] = 0.0;
                $pbArrFixedArr['pbid'] = 0;
            } else {
                $pbArrFixedArr['pb'] = $pb['pb'];
                $pbArrFixedArr['pbid'] = $pb['pbid'];
            }

            $pbArrFixedArr['pbText'] = $pb['pbText'];
            if (is_null($pbArrFixedArr['pbText'])) {
                $pbArrFixedArr['pbText'] = '';
            }

            if (is_null($pb['pof10pb'])) {
                $pbArrFixedArr['pof10pb'] = 0.0;
            } else {
                $pbArrFixedArr['pof10pb'] = $pb['pof10pb'];
            }
            if (is_null($pb['sb'])) {
                $pbArrFixedArr['sb'] = 0.0;
            } else {
                $pbArrFixedArr['sb'] = $pb['sb'];
            }

            $pbArrFixedArr['eventName'] = $pb['eventname'];
            $pbArrFixedArr['options'] = e4s_getOptionsAsObj($pb['evoptions']);
            $uomOptions = e4s_getOptionsAsObj($pb['uomoptions']);
            unset($uomOptions['min']);
            unset($uomOptions['max']);

            $pbArrFixedArr['min'] = (float)$pb['min'];
            $pbArrFixedArr['max'] = (float)$pb['max'];
            $uomObj = new stdClass();
            $uomObj->id = $pb['uomid'];
            $uomObj->type = $pb['uomtype'];
            $uomObj->options = $uomOptions;
            $pbArrFixedArr['uomInfo'] = $uomObj;
            $pbArrFixed[] = $pbArrFixedArr;
        }
        return $pbArrFixed;
    }

    public function getID() {
        return $this->athleteRow['id'];
    }

    public function getExternalAthleteForURN($aoCode, $urn, $date = null) {
        if ($aoCode === E4S_AOCODE_EA) {
            $eaReg = new eaRegistrationClass(E4S_EA_LIVE);
            return $eaReg->validateURNForEventDate($urn, $date);
        }
        if ($aoCode === E4S_AOCODE_AAI) {
            $aaiReg = new aaiRegistrationClass();
            return $aaiReg->validateURN($urn);
        }
        if ($aoCode === E4S_AOCODE_ANI) {
            $aniReg = new aniRegistrationClass();
            return $aniReg->validateURN($urn);
        }
        return null;
    }

    private function _getE4SClubInfoFromEA($eaAthlete) {
        if (isset($eaAthlete->status)) {
            $eaAthlete->CompetitiveRegStatus = $eaAthlete->status;
        }
        if (!isset($eaAthlete->CompetitiveRegStatus) or is_null($eaAthlete->CompetitiveRegStatus)) {
            Entry4UIError(8450, 'Athlete not found');
        }

        $eaAthlete->e4sClubId = $this->_checkEAClub($eaAthlete->clubName, $eaAthlete->clubId);
        $eaAthlete->e4sClub2Id = 0;
        if ($eaAthlete->club2Id > 0) {
            $eaAthlete->e4sClub2Id = $this->_checkEAClub($eaAthlete->club2Name, $eaAthlete->club2Id);
        }
        return $eaAthlete;
    }

    private function _checkEAClub($clubName, $externId) {
        $aoExternId = E4S_AOCODE_EA . '-' . $externId;
        $clubName = addslashes(clubTranslate($clubName));
	    if ( strpos(strtolower($clubName),"unattached" ) !== false ){
		    return E4S_UNATTACHED_ID;
	    }
        $sql = 'select id
                      ,externId
                from ' . E4S_TABLE_CLUBS . ' 
                where externId in ("' . $aoExternId . '","' . $externId . '")
                or    (clubName = "' . $clubName . '" and country in ("England","United Kingdom"))';
        $result = e4s_queryNoLog($sql);
        $returnId = 0;
        switch ($result->num_rows) {
            case 1:
                $obj = $result->fetch_object();
                $returnId = (int)$obj->id;
                if ($obj->externId !== $aoExternId) {
                    $sql = 'update ' . E4S_TABLE_CLUBS . "
                            set externid = '" . $aoExternId . "'
                            where id = " . $obj->id;
                    e4s_queryNoLog($sql);
                }
                break;
            case 0:
                $sql = 'Insert into ' . E4S_TABLE_CLUBS . " ( clubname,externid,country,areaid,clubtype,active)
                        values (
                            '" . addslashes($clubName) . "',
                            '" . $aoExternId . "',
                            'England',
                            4,
                            'C',
                            true
                        )";
                e4s_queryNoLog($sql);
                $returnId = e4s_getLastID();
                break;
            default:
                Entry4UIError(9403, 'Another club has the same external id as EA Club ' . $clubName);
                break;
        }
        return $returnId;
    }

    private function _getE4SClubInfoFromExt($extAthlete, $aoCode) {
        if (!isset($extAthlete->status) or is_null($extAthlete->status)) {
            Entry4UIError(8451, 'Athlete not found');
        }
        if ($aoCode === E4S_AOCODE_AAI) {
            $extAthlete->e4sClubId = $this->_checkAAIClub($extAthlete);
        }
        if ($aoCode === E4S_AOCODE_ANI) {
            $extAthlete->e4sClubId = $this->_checkANIClub($extAthlete);
        }
        $extAthlete->e4sClub2Id = 0;

        return $extAthlete;
    }

    private function _checkAAIClub($extAthlete) {
        $clubName = addslashes(clubTranslate($extAthlete->clubName));
	    if ( strpos(strtolower($clubName),"unattached" ) !== false ){
		    return E4S_UNATTACHED_ID;
	    }
        $externId = E4S_AOCODE_AAI . '-' . $extAthlete->clubId;
        $sql = 'select id
                      ,externId
                from ' . E4S_TABLE_CLUBS . " 
                where externId = '" . $externId . "' 
                or    (clubName = '" . $clubName . "' and country = 'Eire')";
        $result = e4s_queryNoLog($sql);
        $returnId = 0;
        switch ($result->num_rows) {
            case 1:
                $obj = $result->fetch_object();
                $returnId = (int)$obj->id;
                if ($obj->externId !== $externId) {
                    $sql = 'update ' . E4S_TABLE_CLUBS . "
                            set externid = '" . $externId . "'
                            where id = " . $obj->id;
                    e4s_queryNoLog($sql);
                }
                break;
            case 0:
                $sql = 'select id
                        from ' . E4S_TABLE_AREA . "
                        where entityid = 2
                        and name = '" . $extAthlete->county . "'";
                $result = e4s_queryNoLog($sql);
                if ($result->num_rows !== 1) {
                    Entry4UIError(8709, 'Unable to get area for AAI club ' . $clubName);
                }
                $areaObj = $result->fetch_object();
                $sql = 'Insert into ' . E4S_TABLE_CLUBS . " (clubname, externid,country,areaid,clubtype,active)
                        values (
                            '" . addslashes($clubName) . "',
                            '" . $externId . "',
                            'Eire',
                            '" . $areaObj->id . "',
                            'C',
                            true
                        )";
                e4s_queryNoLog($sql);
                $returnId = e4s_getLastID();
                break;
            default:
                e4s_addDebugForce($extAthlete);
                Entry4UIError(9404, 'Another club has the same external id as IRL Club ' . $clubName . '. Please contact AAI or email ' . E4S_SUPPORT_EMAIL);
                break;
        }
        return $returnId;
    }

    private function _checkANIClub($extAthlete) {
        $clubName = addslashes(clubTranslate($extAthlete->clubName));
		if ( strpos(strtolower($clubName),"unattached" ) !== false ){
			return E4S_UNATTACHED_ID;
		}
        $externId = E4S_AOCODE_ANI . '-' . $extAthlete->clubId;
        $sql = 'select id
                      ,externId
                from ' . E4S_TABLE_CLUBS . " 
                where externId = '" . $externId . "'
                or    (clubName = '" . $clubName . "' 
                       and country = 'Northern Ireland'
                       )";
        $result = e4s_queryNoLog($sql);
        $returnId = 0;
        switch ($result->num_rows) {
            case 1:
                $obj = $result->fetch_object();
                $returnId = (int)$obj->id;
                if ($obj->externId !== $externId) {
                    $sql = 'update ' . E4S_TABLE_CLUBS . "
                            set externid = '" . $externId . "'
                            where id = " . $obj->id;
                    e4s_queryNoLog($sql);
                }
                break;
            case 0:
                $sql = 'select id
                        from ' . E4S_TABLE_AREA . "
                        where entityid = 3
                        and name = 'Ulster'";
                $result = e4s_queryNoLog($sql);
                if ($result->num_rows !== 1) {
                    Entry4UIError(8710, 'Unable to get area for ANI club ' . $clubName);
                }
                $areaObj = $result->fetch_object();
                $sql = 'Insert into ' . E4S_TABLE_CLUBS . " (clubname,externid, country,areaid,clubtype,active)
                        values (
                            '" . addslashes($clubName) . "',
                            '" . $externId . "',
                            'Northern Ireland',
                            '" . $areaObj->id . "',
                            'C',
                            true
                        )";
                e4s_queryNoLog($sql);
                $returnId = e4s_getLastID();
                break;
            default:
                Entry4UIError(9405, 'Another club has the same external id as ANI Club ' . $clubName . '. Please contact ANI or email ' . E4S_SUPPORT_EMAIL);
                break;
        }
        return $returnId;
    }

    private function _syncAthleteWithExt($e4sAthlete, $extAthlete, $dobEntered = '') {
        if (is_null($e4sAthlete)) {
            $sql = 'select *
                    from ' . E4S_TABLE_ATHLETE . '
                    where urn = ' . $extAthlete->urn . '
                    and   aocode = "' . $extAthlete->system . '"';
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows === 0) {
                $sql = 'select *
                    from ' . E4S_TABLE_ATHLETE . "
                    where firstname = '" . addslashes($extAthlete->firstName) . "'
                    and   surname = '" . addslashes($extAthlete->surName) . "'
                    and   clubid = (select id from " . E4S_TABLE_CLUBS . " where externid = '" . $extAthlete->system . '-' . $extAthlete->clubId . "')
                    and   dob = '" . $extAthlete->dob . "'";
                $result = e4s_queryNoLog($sql);
            }
            if ($result->num_rows === 1) {
                $e4sAthlete = $this->_normaliseAthlete($result->fetch_object());
            } elseif ($result->num_rows > 1) {
                Entry4UIError(4510, 'Failed to get Athlete');
            }
        }

        if (is_null($e4sAthlete)) {
			if ( $extAthlete->system === E4S_AOCODE_AAI){
				$extAthlete->e4sClub2Id = 0;
				$sql = "select id
					from " . E4S_TABLE_CLUBS . "
					where clubname = '" . addslashes($extAthlete->clubName) . "'
					or    externid = '" . E4S_AOCODE_AAI . "-" . $extAthlete->clubId . "'";
				$result = e4s_queryNoLog($sql);
				if ( $result->num_rows === 1 ){
					$extAthlete->e4sClubId = (int)$result->fetch_object()->id;
				}
			}
            $e4sAthlete = array();
            $e4sAthlete['id'] = 0;

            $thisYear = (int)Date('Y');
            $activeEndDate = $thisYear . '-12-31';
        } else {
            $e4sAthlete = e4s_getDataAsType($e4sAthlete, E4S_OPTIONS_ARRAY);
            $checkDOB = $dobEntered;
            if ($checkDOB === '') {
                $checkDOB = $e4sAthlete['dob'];
            }
            if ($extAthlete->dob !== $checkDOB and $this->validateDOB) {
//                ignore, just set to that from EA
                $this->_athleteDOBMismatch($extAthlete->urn, $extAthlete->system);
            }
            $activeEndDate = $e4sAthlete['activeEndDate'];
            if (isset($extAthlete->activeEndDate)) {
                $activeEndDate = $extAthlete->activeEndDate;
            }
        }

        if ((int)$e4sAthlete['id'] === 0) {
            $sql = 'insert into ' . E4S_TABLE_ATHLETE . " (firstname, surname, aocode, urn, dob, gender, clubid, club2id, type, activeenddate, lastChecked)
                    values(
                    '" . addslashes($extAthlete->firstName) . "',
                    '" . addslashes($extAthlete->surName) . "',
                    '" . $extAthlete->system . "',
                    " . $extAthlete->urn . ",
                    '" . $extAthlete->dob . "',
                    '" . $extAthlete->gender . "',
                    " . $extAthlete->e4sClubId . ',
                    ' . $extAthlete->e4sClub2Id . ",
                    'A',
                    '" . $activeEndDate . "',
                    CURRENT_TIMESTAMP
                    )";
        } else {
            // tmp fix to get over aai athletes on uk system
	        if (!isset($extAthlete->e4sClubId)) {
		        $sql = "select id
                        from " . E4S_TABLE_CLUBS . "
                        where clubname = '" . addslashes($extAthlete->clubName) . "'";
		        $result = e4s_queryNoLog($sql);
		        if ($result->num_rows !== 1) {
			        if ( $extAthlete->system !== '' and $extAthlete->clubId !== '' ) {
				        $sql = 'select id
	                        from ' . E4S_TABLE_CLUBS . "
	                        where externid = '" . $extAthlete->system . '-' . $extAthlete->clubId . "'";
				        $result = e4s_queryNoLog( $sql );
			        }
		        }
		        if ($result->num_rows === 1) {
			        $clubObj = $result->fetch_object();
			        $extAthlete->e4sClubId = $clubObj->id;
		        } else {
			        e4s_addDebugForce($extAthlete);
			        Entry4UIError(4511, "Failed to get Club for Athlete (" . $extAthlete->clubName . "/" . $e4sAthlete['id'] . ")");
		        }
//                    $extAthlete->e4sClubId = $e4sAthlete['clubId'];
		        $extAthlete->e4sClub2Id = 0;
	        }
            $options = e4s_removeDefaultAthleteOptions($e4sAthlete['options']);
            $sql = 'update ' . E4S_TABLE_ATHLETE . "
                    set firstname = '" . addslashes($extAthlete->firstName) . "',
                        surname = '" . addslashes($extAthlete->surName) . "',
                        dob = '" . $extAthlete->dob . "',
                        aocode = '" . $extAthlete->system . "',
                        urn = " . $extAthlete->urn . ",
                        gender = '" . $extAthlete->gender . "',
                        clubid = " . $extAthlete->e4sClubId . ',
                        club2id = ' . $extAthlete->e4sClub2Id . ",
                        type = 'A',
                        activeenddate = '" . $activeEndDate . "',
                        lastChecked = CURRENT_TIMESTAMP,
                        options = '" . addslashes(e4s_getOptionsAsString($options)) . "'
                    where id = " . $e4sAthlete['id'];
        }
        e4s_queryNoLog($sql);
        if ((int)$e4sAthlete['id'] === 0) {
            $e4sAthlete['id'] = e4s_getLastID();
        }
        $e4sAthlete['firstName'] = $extAthlete->firstName;
        $e4sAthlete['surName'] = $extAthlete->surName;
        $e4sAthlete['dob'] = $extAthlete->dob;
        $e4sAthlete['gender'] = $extAthlete->gender;
        $e4sAthlete['activeEndDate'] = $activeEndDate;
        $e4sAthlete['clubid'] = $extAthlete->e4sClubId;
        $e4sAthlete['club2id'] = $extAthlete->e4sClub2Id;
        $e4sAthlete['clubname'] = $extAthlete->clubName;
        $e4sAthlete['club'] = $extAthlete->clubName;
        $e4sAthlete['club2'] = $extAthlete->club2Name;

        $e4sAthlete['status'] = $extAthlete->status;
        return $e4sAthlete;
    }

    private function _normaliseAthlete($athlete) {
        $athlete->id = (int)$athlete->id;
        $athlete->clubId = (int)$athlete->clubid;
        unset($athlete->clubid);
        $athlete->club2Id = (int)$athlete->club2id;
        unset($athlete->club2id);
        $athlete->pof10Id = (int)$athlete->pof10id;
        unset($athlete->pof10Id);
        $athlete->classification = (int)$athlete->classification;
        $athlete->schoolId = (int)$athlete->schoolid;
        unset($athlete->schoolid);
        $athlete->options = e4s_addDefaultAthleteOptions($athlete->options);
        return $athlete;
    }

    public function checkAndUpdateExtAthlete($e4sAthlete, $date = null) {
        $model = new stdClass();
        $model->firstName = $e4sAthlete['firstName'];
        $model->surName = $e4sAthlete['surName'];
        $model->dob = $e4sAthlete['dob'];
        $model->urn = $e4sAthlete['URN'];
        $model->aoCode = $e4sAthlete['aocode'];
        $model->id = (int)$e4sAthlete['id'];
        $model->options = $e4sAthlete['options'];
        $urn = $this->_tryExternalForURN($model);
        if (!is_null($urn)) {
            $e4sAthlete['URN'] = $urn;
        }
//        if ($e4sAthlete['aocode'] !== E4S_AOCODE_ANI) {
        $urn = $e4sAthlete['URN'];
        if (!is_null($urn) and $urn !== '') {
            $checkAthlete = TRUE;
//            $lastExtChecked = $e4sAthlete['lastChecked'];
            if (array_key_exists('serverNow', $e4sAthlete)) {
                $checkAthlete = FALSE;
                $serverNow = $e4sAthlete['serverNow'];
                $nextExtCheck = $e4sAthlete['nextExtCheck'];
                if ($nextExtCheck <= $serverNow) {
                    if (!is_null($date)) {
                        $checkAthlete = TRUE;
                    } else {
                        $activeEndDateTS = date_create($e4sAthlete['activeEndDate'])->getTimestamp();
                        $nowTS = strtotime('now');
                        if ($activeEndDateTS < $nowTS) {
                            $checkAthlete = TRUE;
                        }
                    }
                }
            }
//                $checkAthlete = FALSE;
            if ($checkAthlete) {
                $thisYear = (int)Date('Y');
                if (is_null($date)) {
                    $date = $thisYear . '-12-31';
                }
                $extAthlete = $this->getExternalAthleteForURN($e4sAthlete['aocode'], $urn, $date);
                if (!is_null($extAthlete)) {
                    if (!isset($extAthlete->system)) {
                        // URN is invalid
                        $sql = 'update ' . E4S_TABLE_ATHLETE . '
                               set urn = null,
                               lastChecked = null
                               where id = ' . $e4sAthlete['id'];
                        e4s_queryNoLog($sql);
                        $e4sAthlete['urn'] = '';
                        return $e4sAthlete;
                    } else {
						e4s_addDebugForce("checkAndUpdateExtAthlete");
						e4s_addDebugForce($extAthlete);
                        if ($extAthlete->system === E4S_AOCODE_EA) {
                            $extAthlete = $this->_getE4SClubInfoFromEA($extAthlete);
                            if ($extAthlete->status !== E4S_EA_REGISTERED) {
                                $thisYear -= 1;
                                $date = $thisYear . '-12-31';
                            }
                        }
                        if ($extAthlete->system === E4S_AOCODE_AAI) {
                            $extAthlete = $this->_getE4SClubInfoFromExt($extAthlete, $extAthlete->system);
	                        $date = $extAthlete->activeEndDate;
                        }
                        if ($extAthlete->system === E4S_AOCODE_ANI) {
                            $extAthlete = $this->_getE4SClubInfoFromExt($extAthlete, $extAthlete->system);
                            $date = $extAthlete->activeEndDate;
                        }
                    }


                    $extAthlete->activeEndDate = $date;
                    return $this->_syncAthleteWithExt($e4sAthlete, $extAthlete);
                }
            }
        }
        return $e4sAthlete;
    }

    public function searchForAndSetRegisteredAthlete($model) {
        return $this->setAthlete('a.id', $model->id);
    }

    public function getAthleteFromPof10($model) {
        $urn = $model->urn;
        $pof10 = file_get_contents('http://www.thepowerof10.info/athletes/profile.aspx?ukaurn=' . $urn);
        $pof10Error = explode('cphBody_lblErrorMessage"><font color="Red"></font></span>', $pof10);

        if (sizeof($pof10Error) === 0) {
            Entry4UIError(1050, 'EA record for athlete ' . $urn . ' not found. Please check data and try again or continue entering athlete manually');
        }
// Athlete Info
        $pof10info = explode('cphBody_pnlAthleteDetails">', $pof10);

        if (count($pof10info) < 2) {
            Entry4UIError(1050, 'EA record for ' . $urn . ' not found. Please check data and try again or continue entering athlete manually');
        }

        $pof10info = explode('<div id="cphBody_pnlAbout', $pof10info[1]);
        $pof10info = $pof10info[0];
        $gender = explode('Gender:</b></td><td>', $pof10info);
        $gender = explode('</td>', $gender[1]);
        $gender = substr($gender[0], 0, 1);

        $club = explode('Club:</b></td><td>', $pof10info);
        $club = explode('</td>', $club[1]);
        $club = $club[0];

        $model->clubId = $this->getClubID($club);

        $pof10athlete = explode('<h2>', $pof10);
        $pof10athlete = explode('</h2>', $pof10athlete[1]);
        $pof10athlete = $pof10athlete[0];
        $pof10athleteArr = explode(' ', trim($pof10athlete));

        $model->firstName = trim($pof10athleteArr[0]);
        $model->surName = trim(str_replace($model->firstName . ' ', '', $pof10athlete));
        $model->gender = $gender;
        $model->email = '';
        $model->id = 0;
        $model->club2Id = 0;
        $model->schoolId = 0;
        $model->classification = 0;
        $model->activeEndDate = date('Y') . '-12-31';
        $this->create($model);
    }

    public function getClubID($club) {
        // try to get club
        $clubid = 0;
        $club = addslashes($club);
        $club = clubTranslate($club);
        $sql = 'select id from ' . E4S_TABLE_CLUBS . "
            where clubname like '" . $club . "%'";
        $return = e4s_queryNoLog($sql);
        if ($return->num_rows === 1) {
            $row = $return->fetch_assoc();
            $clubid = $row['id'];
        }
        return $clubid;
    }

    public function create($model) {
        if ((int)$model->id !== 0) {
            Entry4UIError(9203, 'Can pass a valid id to create', 200, '');
        }
        if ((int)$model->schoolId === 0 and (int)$model->clubId === 0) {
            Entry4UIError(8045, 'Please enter either a school or club for this athlete.');
        }
        if ($model->aoCode === '') {
            $model->urn = null;
        }

        if ($this->checkForDuplicate($model)) {
            return $this->getExtendedRow();
        }
        $athleteType = 'A';

        if ((int)$model->schoolId !== 0 and (int)$model->clubId === 0) {
//            $athleteType = "SCH";
        }
        // create athlete and return instance
        $sql = 'Insert into ' . E4S_TABLE_ATHLETE . " ( firstname, surname, gender, dob, aocode, email, urn, clubid,club2id, schoolid, classification, activeenddate, type )
            values(
                '" . addslashes($model->firstName) . "',
                '" . addslashes($model->surName) . "',
                '" . $model->gender . "',
                '" . $model->dob . "',
                '" . $model->aoCode . "',
                '" . $model->email . "',";
        if (is_null($model->urn) or $model->urn === '') {
            $sql .= 'null,';
        } else {
            $sql .= "'" . $model->urn . "',";
        }

        $sql .= "'" . $model->clubId . "',
                '" . $model->club2Id . "',
                '" . $model->schoolId . "',
                '" . $model->classification . "',
                '" . $model->activeEndDate . "',
                '" . $athleteType . "'
            )";

        e4s_queryNoLog('Insert Athlete' . E4S_SQL_DELIM . $sql);
        $this->setAthlete('a.id', e4s_getLastID());
        $this->addUserAthletes();
        $this->_checkSchoolEntity($model);
        return TRUE;
    }

    private function checkForDuplicate($model) {
        $urn = $model->urn;
        if (is_null($urn) or $urn === 0) {
            $urn = '';
        }
        $sql = 'select * from ' . E4S_TABLE_ATHLETE . "
        where type = 'A' and ";

        if ($urn !== '') {
            $sql .= "
                urn = '" . $urn . "'
                and   aocode = '" . $model->aoCode . "'";
        } else {
            $sql .= " 
                firstname = '" . addslashes($model->firstName) . "'
                and surname = '" . addslashes($model->surName) . "'
                and dob = '" . $model->dob . "'
                and clubid = " . $model->clubId . '
            ';
        }
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return FALSE;
        }

        $useId = 0;
        $updateUrn = FALSE;
        while ($obj = $result->fetch_object()) {
            $obj = athleteClass::normaliseObj($obj);
            $existingUrn = $obj->URN;
            if (is_null($existingUrn)) {
                $existingUrn = '';
            }
            if ($existingUrn !== $urn) {
                if ($urn !== '') {
                    // URN was passed in
                    Entry4UIError(2525, 'Details do not match those found with the details you have provided. Please check, try again or contact E4S on support@entry4sports.com');
                }
                $useId = $obj->id;
                $updateUrn = TRUE;
            } else {
                // urn given
                if ($obj->dob !== $model->dob) {
                    Entry4UIError(2015, 'Details found but with a different date of birth. Please check, try again or contact E4S on support@entry4sports.com');
                } else {
                    $useId = $obj->id;
                    if ($existingUrn === '' and $urn !== '') {
                        $updateUrn = TRUE;
                    }
                }
            }

            if ($obj->clubId === $model->clubId and $obj->dob === $model->dob) {
                if ($existingUrn === '') {
                    if ($useId !== 0) {
                        Entry4UIError(2006, 'Duplicate athletes found. Please check, try again or contact E4S on support@entry4sports.com');
                    }
                }
                $useId = $obj->id;
                if ($existingUrn === '' and $urn !== '') {
                    $updateUrn = TRUE;
                }
            }
        }

        if ($useId !== 0) {
            if ($updateUrn) {
                // found a duplicate but without the URN, so add it
                $this->addURNToExistingAthlete($useId, $model->aoCode, $urn);
            }
            $this->setAthlete('a.id', $useId);
            $this->addUserAthletes();

            return TRUE;
        }
        return FALSE;
    }

    public static function normaliseObj($obj) {
        $arr = e4s_getDataAsType($obj, E4S_OPTIONS_ARRAY);
        $arr = athleteClass::normaliseArray($arr);
        return e4s_getDataAsType($arr, E4S_OPTIONS_OBJECT);
    }

    public function addURNToExistingAthlete($id, $aoCode, $urn) {
        if ($urn === '') {
            return;
        }
        $sql = 'update ' . E4S_TABLE_ATHLETE . '
                set urn = ' . $urn . ",
                    aocode = '" . $aoCode . "'
                where id = " . $id;
        e4s_queryNoLog($sql);
    }

    public function addUserAthletes() {
        $athleteId = $this->getID();
        $userid = e4s_getUserID();
        $sql = 'select *
            from ' . E4S_TABLE_USERATHLETES . '
            where userid = ' . $userid . '
            and   athleteid = ' . $athleteId;
        $results = e4s_queryNoLog($sql);

        if ($results->num_rows === 0) {
            $uSql = 'insert into ' . E4S_TABLE_USERATHLETES . '(userid, athleteid)
                 values (' . $userid . ',' . $athleteId . ' )';
            e4s_queryNoLog($uSql);
			// force pof10 update for this users athletes
	        e4s_updatePof10IfRequired(true);
        }
        $this->_addToOtherClubReps();
    }

    private function _addToOtherClubReps() {
        $userId = e4s_getUserID();
        $athleteId = $this->getID();
        $userObj = $GLOBALS[E4S_USER];
        $compClubIds = [];
        $props = get_object_vars($userObj);
        foreach ($props as $key => $prop) {
            if (strpos($key, E4S_CLUBCOMP) === 0) {
                $compClubIds[] = $prop;
            }
        }
        if (empty($compClubIds)) {
            return;
        }
        $sql = '
            select user_id
            from ' . E4S_TABLE_USERMETA . "
            where meta_key like '" . E4S_CLUBCOMP . "%'
            and meta_value in (" . implode(',', $compClubIds) . ')
            and user_id != ' . $userId . '
        ';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return;
        }
        while ($obj = $result->fetch_object()) {
            $sql = 'select id
                    from ' . E4S_TABLE_USERATHLETES . '
                    where athleteid = ' . $athleteId . '
                    and userid = ' . $obj->user_id;
            $readResult = e4s_queryNoLog($sql);
            if ($readResult->num_rows === 0) {
                $uSql = 'insert into ' . E4S_TABLE_USERATHLETES . '(userid, athleteid)
                 values (' . $obj->user_id . ',' . $athleteId . ' )';
                e4s_queryNoLog($uSql);
            }
        }
    }

    public function getExtendedRow($incCancelled = TRUE) {
        if (is_null($this->athleteRow)) {
            return null;
        }
        $row = new ArrayObject($this->athleteRow);
        $clonedRow = $row->getArrayCopy();
        $clonedRow['events'] = $this->getAllEvents($incCancelled);
        $clonedRow['userAthletes'] = $this->getUserAthletes();
        $clonedRow['pbInfo'] = $this->getPBs();
        $clonedRow['clubInfo'] = $this->_getClubInfo($clonedRow);
        unset($clonedRow['clubid']);
//        unset($clonedRow['schoolid']);
        unset($clonedRow['club2id']);
        return $clonedRow;
    }

    public function getAllEvents($incCancelled = TRUE) {
        $abs = '';
        if ($incCancelled) {
            $abs = 'abs';
        }
        $sql = 'select  c.id compid,
                    c.name compname,
                    DATE_FORMAT(c.date,"' . ISO_MYSQL_DATE . '") compdate,
                    evt.id eventid,
                    evt.tf,
                    evt.name eventname,
                    DATE_FORMAT(eg.startdate,"' . ISO_MYSQL_DATE . '") eventtime,
                    ent.orderid orderno,
                    ent.price price,
                    ent.paid paid,
                    ent.id entryid,
                    ent.pb perf,
                    abs(ent.userid) userid,
                    ent.teamBibNo,
                    ent.options,
                    u.user_nicename username,
                    u.user_email email,
                    ce.id ceid,
                    evt.min,
                    evt.max,
                    ce.split,
                    uom.uomoptions uomOptions

            from ' . E4S_TABLE_COMPETITON . ' c,
                 ' . E4S_TABLE_ATHLETE . ' a,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTGROUPS . ' eg,
                 ' . E4S_TABLE_EVENTS . ' evt,
                 ' . E4S_TABLE_ENTRIES . ' ent,
                 ' . E4S_TABLE_USERS . ' u,
                 ' . E4S_TABLE_UOM . ' uom
            where ';

        $sql .= $abs . '(ent.athleteid) = ' . $this->getID() . '
            and   ' . $abs . '(ent.athleteid) = a.id
            and   ' . $abs . '(ent.userid) = u.id
            and   ' . $abs . '(ent.compeventid) = ce.id
            and   ce.eventid = evt.id
            and   ce.maxgroup = eg.id
            and   ce.compid = c.id
            and   uom.id = evt.uomid
            and   ent.options not like "%cartemptied%"
            ';

        $result = e4s_queryNoLog($sql);
        $entryRows = $result->fetch_all(MYSQLI_ASSOC);
        $processedEventRows = array();

        foreach ($entryRows as $entryRow) {
	        $entryRow['ceid'] = (int)$entryRow['ceid'];
	        $entryRow['entryid'] = (int)$entryRow['entryid'];
	        $entryRow['compid'] = (int)$entryRow['compid'];

	        $entryRow = $this->_getPerformanceInfo($entryRow);

			$entryRow['options'] = e4s_addDefaultEntryOptions($entryRow['options']);
            $entryRow['compdate'] .= e4s_getOffset($entryRow['compdate']);
            $entryRow['eventtime'] .= e4s_getOffset($entryRow['eventtime']);
            if ($entryRow['teamBibNo'] === '0' or is_null($entryRow['teamBibNo'])) {
                $entryRow['teamBibNo'] = '';
            }
            $processedEventRows[] = $entryRow;
        }
        $this->events = $processedEventRows;
        return $this->events;
    }

	private function _getPerformanceInfo($entryRow){
		if ( is_null($this->performances) ){
			$athleteId = $this->getID();
			$perfAthleteObj = new perfAthlete();
			$performances = $perfAthleteObj->getPerformances($athleteId);
			if ( sizeof($performances) > 0 ){
				$this->performances = $performances[$athleteId];
			}else{
				$this->performances = array();
			}
		}

		$entryRow['perfText'] = resultsClass::ensureDecimals($entryRow['perf']);
		if ($entryRow['tf'] === E4S_EVENT_TRACK) {
			$entryRow['perfText'] = resultsClass::getResultFromSeconds($entryRow['perf']);
		}

		$entryRow['perfInfo'] = new stdClass();
		$entryRow['perfInfo']->perf = $entryRow['perf'];
		$entryRow['perfInfo']->perfText = E4S_ENSURE_STRING . $entryRow['perfText'];

		// init perf so always there
		$entryRow['perfInfo']->pb = 0;
		$entryRow['perfInfo']->pbText = '';
		$entryRow['perfInfo']->pbAchieved = '';
		$entryRow['perfInfo']->sb = 0;
		$entryRow['perfInfo']->sbText = '';
		$entryRow['perfInfo']->sbAchieved = '';

		$entryRow['perfInfo']->ageGroup = '';
		$entryRow['perfInfo']->curAgeGroup = '';
		$entryRow['perfInfo']->eventName = '';
		$entryRow['perfInfo']->uom = e4s_getOptionsAsObj($entryRow['uomOptions']);

		$entryRow['perfInfo']->limits = new stdClass();
		$entryRow['perfInfo']->limits->min = (int)$entryRow['min'];
		$entryRow['perfInfo']->limits->max = (int)$entryRow['max'];

		foreach ($this->performances as $perf){
			if ( (int)$entryRow['eventid'] === $perf->eventId ){
				$entryRow['perfInfo']->pb = $perf->pb;
				$entryRow['perfInfo']->pbText = $perf->pbText;
				$entryRow['perfInfo']->pbAchieved = $perf->pbAchieved;
				$entryRow['perfInfo']->sb = $perf->sb;
				$entryRow['perfInfo']->sbText = $perf->sbText;
				$entryRow['perfInfo']->sbAchieved = $perf->sbAchieved;

				$entryRow['perfInfo']->ageGroup = $perf->ageGroup;
				$entryRow['perfInfo']->curAgeGroup = $perf->curAgeGroup;
				$entryRow['perfInfo']->eventName = $perf->eventName;
				break;
			}
		}
		unset($entryRow['perf']);
		unset($entryRow['perfText']);
		unset($entryRow['min']);
		unset($entryRow['max']);
		unset($entryRow['uomOptions']);

		return $entryRow;
	}
    public function getUserAthletes() {
        // add UserAthlete info
        $sql = 'select u.id id, u.user_email email, u.user_nicename userName
            from ' . E4S_TABLE_USERATHLETES . ' ua,
                 ' . E4S_TABLE_USERS . ' u 
            where u.id = ua.userid
            and ua.athleteid = ' . $this->getID();
        $result = e4s_queryNoLog($sql);
        $this->userAthletes = $result->fetch_all(MYSQLI_ASSOC);
        return $this->userAthletes;
    }

    private function _getClubInfo($row) {
        $ids = array();
        $row['clubid'] = (int)$row['clubid'];
        if ($row['clubid'] > 0 and $row['clubid'] !== E4S_UNATTACHED_ID) {
            $ids[] = $row['clubid'];
        }
        $row['club2id'] = (int)$row['club2id'];
        if ($row['club2id'] > 0 and $row['club2id'] !== E4S_UNATTACHED_ID) {
            $ids[] = $row['club2id'];
        }
        $clubs = array();
        if (sizeof($ids) > 0) {
            $sql = 'select u.*, meta_value clubId
                from ' . E4S_TABLE_USERS . ' u,
                     ' . E4S_TABLE_USERMETA . " um
                where u.id = um.user_id
                and meta_key = '" . E4S_CLUB_ID . "'
                and um.meta_value in ( " . implode(',', $ids) . ')
                ';
            $result = e4s_queryNoLog($sql);

            while ($obj = $result->fetch_object()) {
                $clubId = (int)$obj->clubId;
                if (!array_key_exists($clubId, $clubs)) {
                    $clubs[$clubId] = array();
                }
                $userObj = new stdClass();
                $userObj->id = (int)$obj->ID;
                $userObj->userName = $obj->user_nicename;
                $userObj->displayName = $obj->display_name;
                $userObj->email = $obj->user_email;
                $clubs[$clubId][] = $userObj;
            }
        }
        return $clubs;
    }

    private function _checkSchoolEntity($model) {
        if ($model->schoolId > 0) {
            $userId = e4s_getUserID();
            // get users email
            $email = e4s_getUserEmail();
            // get first domain
            $arr = preg_split('~@~', $email);
            $domain = $arr[1];
            $arr = preg_split('~\.~', $domain);
            $school = $arr[0];
            $clubSql = 'select id
                        from ' . E4S_TABLE_CLUBS . "
                        where clubname = '" . addslashes($school) . "'
                        and clubtype = 'S'";
            $clubResult = e4s_queryNoLog($clubSql);
            if ($clubResult->num_rows !== 1) {
                return;
            }
            $clubObj = $clubResult->fetch_object();
            $schoolId = (int)$clubObj->id;
            include_once E4S_FULL_PATH . 'admin/userMaint.php';
            $entityResult = e4s_getUserEntitiy($userId, $schoolId);
            if ($entityResult->num_rows !== 1) {
                return;
            }
            // user has an entity matching the first domain of the email, so update to the schoolid passed
            if (addUserToClub($userId, $model->schoolId, TRUE)) {
                // added schoolid so now remove incorrect one
                removeUserClub($userId, $schoolId, FALSE);
            }
        }
    }

    public function getDOB() {
        return $this->athleteRow['dob'];
    }

    public function updatePBsFromPof10Info() {
        if (is_null($this->pof10Obj)) {
            $this->pof10Obj = pof10Class::getWithAthleteRow($this->athleteRow);
        }
        if (is_null($this->pof10Obj)) {
            return null;
        }
        $this->pof10Obj->updateE4SPBs();
    }

    // updating PB from athlete Maintenance
    public function updatePBFromPayload($obj) {
        $this->pbObj = null;
        if (!is_null($this->athleteRow)) {
            $this->pbObj = new pbClass($this->getID());
            if (!is_null($this->pbObj)) {
                $this->pbObj->updatePBFromPayload($obj);
            }
        }
        return $this->pbObj;
    }

    public function delete($useId = null) {
        if (is_null($useId)) {
            $useId = $this->getID();
        }
        if (is_null($this->athleteRow)) {
            Entry4UIError(2110, 'Unable to delete not found', 200, '');
        }
        if (!is_null($this->getEntries($useId))) {
            Entry4UIError(2015, 'Unable to delete as in use', 200, '');
        }

        self::deleteAthletes([$useId]);

        return TRUE;
    }

    public static function deleteAthletes($ids) {
        $sql = 'insert into ' . E4S_TABLE_ATHLETE_DELETED . '
            select *, current_timestamp from ' . E4S_TABLE_ATHLETE . '
            where id in (' . implode(',', $ids) . ')';
        e4s_queryNoLog($sql);

        $sql = 'delete from ' . E4S_TABLE_ATHLETE . '
            where id in (' . implode(',', $ids) . ')';
        e4s_queryNoLog($sql);
    }

    public function getEntries($useId = null) {
        if (is_null($useId)) {
            $useId = $this->getID();
        }
        $sql = 'select *
            from ' . E4S_TABLE_ENTRIES . '
            where  athleteid = ' . $useId;
        $result = e4s_queryNoLog($sql);
        $rows = null;
        if ($result->num_rows > 0) {
            $rows = $result->fetch_all(MYSQLI_ASSOC);
        }
        return $rows;
    }
}