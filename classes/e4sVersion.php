<?php

class e4sVersion {
	var $config;
	var $dbVersionInfo;
	var $latestVersionInfo;
	var $latestVersionStr;
	var $updates;
	function __construct(){

		$this->config = e4s_getConfig();
		$this->updates = [
			'23.11.01',
			'23.11.28',
			'23.12.20',
			'24.02.01',
			'24.03.27',
			'24.05.08',
			'24.05.13',
			'24.05.30',
			'24.06.05',
			'24.07.17',
			'24.08.02',
			'24.10.05',
			'24.10.10',
			'25.01.07'
		];
		$this->latestVersionStr = end($this->updates);

		if ( !isE4SUser() ) {
			return;
		}

		$this->checkVersion();
	}
	private function _upgradeDB(){
		$updates = $this->updates;
		foreach ($updates as $update){
			if (!$this->_upgradeDBFrom($update)){
				return;
			};
		}
	}
	private function checkVersion(){
		$this->latestVersionInfo = $this->_getVersionInfo($this->latestVersionStr);
		$configObj = e4s_getConfigObj();
		$version = $configObj->getVersion();
		if ( is_null($version)){
			$version = '0.0.0';
		}
		$this->dbVersionInfo = $this->_getVersionInfo($version);
		$currentVersionInfo = $this->_getVersionInfo($version);

		if($this->dbVersionInfo->releaseNo === $this->latestVersionInfo->releaseNo){
			// get out, already at latest version
			return;
		}

		$this->_upgradeDB();

		if($this->dbVersionInfo->releaseNo !== $currentVersionInfo->releaseNo){
			// update config with Latest Version as its changed
			$configObj->setVersion($this->dbVersionInfo->releaseStr);
		}
	}

	private function _getReleaseNo($releaseStr){
		$releaseNoArr = $this->_explodeRelease($releaseStr);
		$releaseNo = (int)$releaseNoArr[0] * 1000000 + (int)$releaseNoArr[1] * 1000 + (int)$releaseNoArr[2];
		return $releaseNo;
	}
	private function _explodeRelease($releaseStr){
		$releaseNoArr = explode('.', $releaseStr);
		return array(($releaseNoArr[0]), ($releaseNoArr[1]), ($releaseNoArr[2]));
	}
	private function _getVersionInfo($str){
		$releaseNoArr = $this->_explodeRelease($str);
		$dbVersionInfo = new stdClass();
		$dbVersionInfo->major = $releaseNoArr[0];
		$dbVersionInfo->minor = $releaseNoArr[1];
		$dbVersionInfo->point = $releaseNoArr[2];
		$dbVersionInfo->releaseStr = $str;
		$dbVersionInfo->releaseNo = $this->_getReleaseNo($str);
		$dbVersionInfo->updatefunction = '_updateTo' . $dbVersionInfo->major . $dbVersionInfo->minor . $dbVersionInfo->point;
		return $dbVersionInfo;
	}

	/*
	 * @param $fromVersionNo string
	 * @param $functionName function
	 * @return bool true means keep processing, false means stop processing
	 */
	public function _upgradeDBFrom($fromVersionNo):bool{
		$version = $this->_getVersionInfo($fromVersionNo);
		if ($version->releaseNo <= $this->dbVersionInfo->releaseNo ) {
			// already at or past this version
			return true;
		}
		if ($version->releaseNo > $this->latestVersionInfo->releaseNo ) {
			// this version is past the current version. This would only occur if we restore old code
			return false;
		}

		$this->{$version->updatefunction}();
		$this->dbVersionInfo = $version;

		return true;
	}

	private function _updateTo231101(){
		$sql = "
		create or replace view " . E4S_TABLE_EVENTS ." as
			select eg.id      AS ID,
			       ed.Name    AS Name,
			       ed.tf      AS tf,
			       eg.gender  AS Gender,
			       eg.options AS options,
			       ed.uomid   AS uomid,
			       eg.min     AS min,
			       eg.max     AS max,
			       u.uomtype  AS eventType,
			       u.uomOptions
			from " . E4S_TABLE_EVENTDEFS ." ed,
			     " . E4S_TABLE_EVENTGENDER  ." eg,
			     " . E4S_TABLE_UOM . " u
			where ed.ID = eg.eventid
			  and uomid = u.id;

		";
		e4s_queryNoLog($sql);

		$sql = "
			select *
			from " . E4S_TABLE_COMPETITON . "
			limit 1;
		";
		$result = e4s_queryNoLog($sql);
		$compObj = $result->fetch_object();
		if ( !isset($compObj->resultsAvailable)){
			$sql = "
				alter table " . E4S_TABLE_COMPETITON . "
				add resultsAvailable TINYINT not null default 0";
			e4s_queryNoLog($sql);
			$sql = "
				update " . E4S_TABLE_COMPETITON . " c,
				    " . E4S_TABLE_EVENTRESULTSHEADER . " rh,
				    " . E4S_TABLE_EVENTGROUPS . " eg
				set resultsAvailable = 1
				where eg.id = rh.egid
				  and eg.compid = c.id
			";
			e4s_queryNoLog($sql);
		}
	}

	private function _updateTo231128(){
		$sql = "
			select *
			from " . E4S_TABLE_EVENTTEAMENTRIES . "
			limit 1;
		";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();

		// if $obj has bibNo property

		if ( !property_exists($obj,'bibNo')) {
			$sql = "
				alter table " . E4S_TABLE_EVENTTEAMENTRIES . "
				add bibNo VARCHAR(10) null after orderid;
			";
			e4s_queryNoLog($sql);
		}
		$sql = "
			select *
			from " . E4S_TABLE_CONFIG . "
			limit 1";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();
		if ( !property_exists($obj, 'version')) {
			$sql = "
				alter table " . E4S_TABLE_CONFIG . "
				add version varchar(10) null after systemName;
			";
			e4s_queryNoLog($sql);
		}
		$sql = "DROP VIEW if exists Entry4_uk_Timetable;";
		e4s_queryNoLog($sql);
	}
	private function _updateTo231220(){
		$sql = "
			select *
			from " . E4S_TABLE_ATHLETE . "
			limit 1;
		";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();

		// if $obj has lastCheckedPB property

		if ( !property_exists($obj,'lastCheckedPB')) {
			$sql = "
				alter table " . E4S_TABLE_ATHLETE . "
				add lastCheckedPB timestamp null after lastChecked;
			";
			e4s_queryNoLog($sql);
		}
		if ( !property_exists($obj,'curAgeGroup')) {
			$sql = "
				alter table " . E4S_TABLE_ATHLETE . "
				add curAgeGroup varchar(10) after dob;
			";
			e4s_queryNoLog($sql);
		}

		$sql = " drop table if exists " . E4S_TABLE_ATHLETEPB . "2";
		e4s_queryNoLog($sql);

		$sql = " drop table if exists " . E4S_TABLE_ATHLETEPERF;
		e4s_queryNoLog($sql);

		$sql = "
			create table if not exists " . E4S_TABLE_ATHLETEPERF . "
			(
			    id          int auto_increment,
			    athleteid   int                 not null,
			    eventid     int                 not null,
			    perf        varchar(10)         not null,
			    wind        varchar(5)         not null,
			    info		varchar(100)        null,
			    ageGroup	varchar(10)         not null,
			    achieved timestamp  default CURRENT_TIMESTAMP null,
			    primary key (id),
			    constraint id
			        unique (id)
			)
		";
		e4s_queryNoLog($sql);

		$sql = "SELECT COUNT(1) IndexIsThere FROM INFORMATION_SCHEMA.STATISTICS
				WHERE table_schema=DATABASE() AND table_name='" . E4S_TABLE_ATHLETEPERF . "' AND index_name='athletePerf'";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();
		if ( $obj->IndexIsThere == 0 ){
			$sql = "
				CREATE INDEX athletePerf ON " . E4S_TABLE_ATHLETEPERF . " (athleteid, eventid);
			";
			e4s_queryNoLog($sql);
		}

		$sql = "
			create table if not exists " . E4S_TABLE_ATHLETERANK . "
			(
			    id          int auto_increment,
			    athleteid   int not null,
			    eventid     int not null,
			    rank        int not null,
                ageGroup	varchar(10) not null,
			    year        int not null,
			    info		varchar(100) null,
			    primary key (id),
			    constraint id
			        unique (id)
			)
		";
		e4s_queryNoLog($sql);
		$sql = "SELECT COUNT(1) IndexIsThere FROM INFORMATION_SCHEMA.STATISTICS
				WHERE table_schema=DATABASE() AND table_name='" . E4S_TABLE_ATHLETERANK . "' AND index_name='athleterank'";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();
		if ( $obj->IndexIsThere == 0 ){
			$sql = "
				CREATE INDEX athleterank ON " . E4S_TABLE_ATHLETERANK . " (athleteid, eventid, year)
			";
			e4s_queryNoLog($sql);
		}

		$result = e4s_queryNoLog( "desc " . E4S_TABLE_ATHLETERANK  );
		$foundColumn = false;
		while ($descRow = $result->fetch_object()){
			if ($descRow->Field === 'ageGroup'){
				$foundColumn = true;
				break;
			}
		}
		if ( !$foundColumn ){
			$sql = "
				alter table " . E4S_TABLE_ATHLETERANK . "
				add ageGroup varchar(10) not null after rank;
			";
			e4s_queryNoLog($sql);
		}

		$sql = "
			select *
			from " . E4S_TABLE_ATHLETEPB . "
			limit 1;
		";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();

		if ( property_exists($obj,'tracksb')) {
			$sql = 'alter table ' . E4S_TABLE_ATHLETEPB . '
				    drop column tracksb';
			e4s_queryNoLog($sql);
		}

		//       if(isnull(t.name), '', t.name)        AS teamName,
		$sql = " drop view if exists " . E4S_TABLE_ENTRYINFO;
		e4s_queryNoLog($sql);

		$sql = "
			select *
			from " . E4S_TABLE_ENTRIES . "
			limit 1;
		";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();

		if ( !property_exists($obj,'trackedPB')) {
			$sql = 'alter table ' . E4S_TABLE_ENTRIES . '
                add trackedPB tinyint default 1 null after pb';
			e4s_queryNoLog($sql);
		}
		$sql = " drop view if exists " . E4S_TABLE_ENTRYINFO;
		e4s_queryNoLog($sql);
		$sql = "
			create view " . E4S_TABLE_ENTRYINFO . " as
				select e.id                                  AS entryId,
				       e.pb                                  AS entryPb,
				       e.trackedPB                           AS trackedPb,
				       e.variationID                         AS productId,
				       e.orderid                             AS orderId,
				       e.price                               AS price,
				       e.paid                                AS paid,
				       e.entity                              AS entity,
				       e.periodStart                         AS periodStart,
				       e.created                             AS created,
				       e.options                             AS eOptions,
				       e.userid                              AS userId,
				       e.ageGroupID                          AS ageGroupId,
				       e.eventAgeGroup                       AS eventAgeGroup,
				       e.checkedin                           AS checkedin,
				       b.bibno                               AS bibNo,
				       e.teambibno                           AS teamBibNo,
				       e.discountid                          AS discountid,
				       e.confirmed                           AS confirmed,
				       a.classification                      AS classification,
				       a.id                                  AS athleteId,
				       a.aocode                              AS aoCode,
				       a.URN                                 AS urn,
				       a.firstName                           AS firstName,
				       a.surName                             AS surName,
				       a.dob                                 AS dob,
				       ev.uomid                              AS uomId,
				
				       c.id                                  AS clubId,
				       c.areaid                              AS areaId,
				       c.Clubname                            AS clubName,
				       s.Clubname                            AS schoolName,
				       s.id                                  AS schoolId,
				       ce.CompID                             AS compId,
				       ce.ID                                 AS ceId,
				       ce.IsOpen                             AS isOpen,
				       ce.startdate                          AS eventStartDate,
				       ce.options                            AS ceOptions,
				       ce.split                              AS split,
				       ce.EventID                            AS eventid,
				       ce.PriceID                            AS priceid,
				       eg.id                                 AS egId,
				       date_format(eg.startdate, '%Y-%m-%d') AS startdate,
				       date_format(eg.startdate, '%H.%i')    AS starttime,
				       eg.name                               AS egName,
				       eg.options                            AS egOptions,
				       eg.forceClose                         AS forceClose,
				       eg.eventNo                            AS eventNo,
				       eg.bibSortNo                          AS bibSortNo,
				       eg.typeNo                             AS typeNo,
				       ev.Gender                             AS gender,
				       ev.Name                               AS eventName,
				       ev.tf                                 AS tf,
				       ag.MaxAge                             AS maxAge,
				       ag.Name                               AS ageGroup,
				       uom.uomoptions                        AS uomOptions
				from ((((((((((" . E4S_TABLE_ENTRIES . " e left join " . E4S_TABLE_ATHLETE . " a
				                on ((a.id = e.athleteid))) left join " . E4S_TABLE_COMPEVENTS . " ce
				               on ((ce.ID = e.compEventID))) left join " . E4S_TABLE_EVENTS . " ev
				              on ((ce.EventID = ev.ID))) left join " . E4S_TABLE_BIBNO . " b
				             on (((b.compid = ce.CompID) and
				                  (b.athleteid = e.athleteid)))) left join " . E4S_TABLE_ATHLETEPB . " p
				            on ((ce.EventID = p.eventid and a.id = p.athleteid))) left join " . E4S_TABLE_CLUBS . " c
				           on ((c.id = e.clubid))) left join " . E4S_TABLE_CLUBS . " s
				         on ((s.id = a.schoolid))) left join " . E4S_TABLE_AGEGROUPS . " ag
				        on ((ce.AgeGroupID = ag.id))) join " . E4S_TABLE_EVENTGROUPS . " eg on ce.maxGroup = eg.id) join " . E4S_TABLE_UOM . " uom on uom.id = ev.uomid)
		";
		e4s_queryNoLog($sql);

		$sql = "
			select *
			from " . E4S_TABLE_COMPEVENTS . "
			limit 1;
		";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();

		if ( !property_exists($obj,'within')) {
			$sql = 'alter table ' . E4S_TABLE_COMPEVENTS . '
                add within int null after split';
			e4s_queryNoLog($sql);
		}

		$sql = " drop view if exists " . E4S_TABLE_EVENTS ;
		e4s_queryNoLog($sql);
		$sql = "
			create  view " . E4S_TABLE_EVENTS . " as
				select eg.id        AS ID,
					   ed.id		AS eventDefId,
				       ed.Name      AS Name,
				       ed.pof10     AS pof10,
				       ed.tf        AS tf,
				       eg.gender    AS Gender,
				       eg.options   AS options,
				       ed.uomid     AS uomid,
				       eg.min       AS min,
				       eg.max       AS max,
				       u.uomtype    AS eventType,
				       u.uomoptions AS uomOptions
				from " . E4S_TABLE_EVENTDEFS . " ed
				         join " . E4S_TABLE_EVENTGENDER . " eg
				         join " . E4S_TABLE_UOM . " u
				where ((ed.ID = eg.eventid) and (ed.uomid = u.id));
		";
		e4s_queryNoLog($sql);
	}

	private function _updateTo240201(){
		// AAI has diffrent db to UK/UAT so need to check that too
//		$this->_renameDeleted('_240201');
	}
	private function _updateTo240327(){
		$sql = "
			select *
			from " . E4S_TABLE_COMPETITON . "
			limit 1;
		";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();

		if ( !property_exists($obj,'type')) {
			$sql = 'alter table ' . E4S_TABLE_COMPETITON . '
                add type varchar(1) null after resultsAvailable';
			e4s_queryNoLog($sql);
		}
	}
	private function _updateTo240508(){
		$sql = "update " . E4S_TABLE_CLUBS . "
			    set clubtype = 'I',
			    country = 'International'
			    where country = 'Overseas'";
		e4s_queryNoLog($sql);
		$sql = "select id
		 		from " . E4S_TABLE_CLUBS . "
			    where clubname = 'Scotland'";
		$result = e4s_queryNoLog($sql);
		if ( $result->num_rows == 0 ) {
			$sql = "insert into " . E4S_TABLE_CLUBS . " (clubname, country, clubtype, areaid, active, region)
			    values ('Scotland', 'International', 'I',44,true,'Europe')";
			e4s_queryNoLog($sql);
		}
		$sql = "select *
		        from " . E4S_TABLE_AO . "
		        where code = 'INT'";
		$result = e4s_queryNoLog($sql);
		if ( $result->num_rows === 0 ) {
			$sql = "insert into " . E4S_TABLE_AO . " (code, description, urnformat)
			    values ('INT', 'International', '(^$)')";
			e4s_queryNoLog($sql);
		}
	}
	private function _updateTo240513(){
		$sql = "SELECT COUNT(1) IndexIsThere FROM INFORMATION_SCHEMA.STATISTICS
				WHERE table_schema=DATABASE() AND table_name='Entry4_uk_table'";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();
		if ( $obj->IndexIsThere > 0 ){
			$sql = "
				rename table Entry4_uk_table to Entry4_uk_AuditTable;
			";
			e4s_queryNoLog($sql);
		}
	}
	private function _updateTo240530(){
		// add team bib no
		$sql = "
			select *
			from " . E4S_TABLE_EVENTTEAMENTRIES . "
			limit 1;
		";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();

		if ( !property_exists($obj,'bibNo')) {
			$sql = 'alter table ' . E4S_TABLE_EVENTTEAMENTRIES . '
                add bibNo varchar(10) null after price';
			e4s_queryNoLog($sql);
		}
		if ( !property_exists($obj,'present')) {
			$sql = 'alter table ' . E4S_TABLE_EVENTTEAMENTRIES . '
			    add present tinyint default 0 null after options';
			e4s_queryNoLog($sql);
		}
		$sql = "
			select *
			from " . E4S_TABLE_EVENTTEAMENTRIES . "_deleted
			limit 1;
		";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();

		if ( !property_exists($obj,'bibNo')) {
			$sql = 'alter table ' . E4S_TABLE_EVENTTEAMENTRIES . '_deleted
                add bibNo varchar(10) null after price';
			e4s_queryNoLog($sql);
		}
		if ( !property_exists($obj,'present')) {
			$sql = 'alter table ' . E4S_TABLE_EVENTTEAMENTRIES . '_deleted
			    add present tinyint default 0 null after options';
			e4s_queryNoLog($sql);
		}
	}
	private function _updateTo240605(){
		$sql = "
			select *
			from " . E4S_TABLE_COMBINEDCALC . "
			limit 1;
		";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();

		if ( !property_exists($obj,'minVal')) {
			$sql = 'alter table ' . E4S_TABLE_COMBINEDCALC . '
                add minVal float(5,2) null after c,
                add maxVal float(5,2) null after minVal,
                add calcNo int null after maxVal';
			e4s_queryNoLog($sql);
		}
	}
	private function _updateTo240717(){
		$sql = "
			select *
			from " . E4S_TABLE_PHOTOFINISH . "
			limit 1;
		";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();

		if ( !property_exists($obj,'system')) {
			$sql = 'alter table ' . E4S_TABLE_PHOTOFINISH . '
                 add system int default 1 not null after compId';
			e4s_queryNoLog($sql);
		}
	}
	private function _updateTo240802(){
		$sql = "
			select *
			from " . E4S_TABLE_AO . "
			limit 1;
		";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();

		if ( !property_exists($obj,'liveLink')) {
			$sql = 'alter table ' . E4S_TABLE_AO . '
                 add liveLink tinyint default 1 not null';
			e4s_queryNoLog($sql);
		}
	}

	private function _updateTo241005(){
		$sql = "
			select *
			from " . E4S_TABLE_CARDRESULTS . "
			limit 1;
		";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();

		if ( !property_exists($obj,'created')) {
			$sql = 'alter table ' . E4S_TABLE_CARDRESULTS . '
                 add created timestamp default CURRENT_TIMESTAMP not null';
			e4s_queryNoLog($sql);
		}

		$sql = "
			select *
			from " . E4S_TABLE_SCOREBOARD . "
			limit 1;
		";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();

		if ( !property_exists($obj,'resultKey')) {
			$sql = 'alter table ' . E4S_TABLE_SCOREBOARD . '
                 add resultKey varchar(10) null after score';
			e4s_queryNoLog($sql);
		}
	}
	private function _updateTo241010(){
		$sql = "
			select *
			from " . E4S_TABLE_COMPCLUB . "
			limit 1;
		";
		$result = e4s_queryNoLog($sql);
		$obj = $result->fetch_object();

		if ( !property_exists($obj,'status')) {
			$sql = 'alter table ' . E4S_TABLE_COMPCLUB . '
                 add status varchar(10) default "New" not null';
			e4s_queryNoLog($sql);
		}
	}
	private function _updateTo250107() {
		$sql = "update " . E4S_TABLE_COMPCLUB . "
				set status = 'Approved'";
		e4s_queryNoLog($sql);
	}
	private function _renameDeleted($ext){
		e4sArchive::checkArchiveDB();
		$archiveDB = e4sArchive::archiveName() ;
		$archiveTable = E4S_TABLE_ATHLETE_DELETED . $ext;
		$sql = "SELECT *
			FROM information_schema.tables
			WHERE table_schema = '" . $archiveDB ."'
			  AND table_name = '" . $archiveTable . "'
			LIMIT 1";

		$result = e4s_queryNoLog($sql);
		if ( $result->num_rows == 0 ) {
			$archiveTable = $archiveDB . "." . $archiveTable;
			$sql = "rename table  " . E4S_TABLE_ATHLETE_DELETED . " to " . $archiveTable;
			e4s_queryNoLog( $sql );

			$sql = "create table  " . E4S_TABLE_ATHLETE_DELETED . " like " . E4S_TABLE_ATHLETE;
			e4s_queryNoLog( $sql );

			$sql = 'alter table ' . E4S_TABLE_ATHLETE_DELETED . '
                add deleted timestamp default CURRENT_TIMESTAMP';
			e4s_queryNoLog( $sql );
		}

		$archiveTable =  E4S_TABLE_EVENTTEAMENTRIES_DELETED . $ext;
		$sql = "SELECT *
			FROM information_schema.tables
			WHERE table_schema = '" . $archiveDB ."'
			  AND table_name = '" . $archiveTable . "'
			LIMIT 1";

		$result = e4s_queryNoLog($sql);
		if ( $result->num_rows == 0 ) {
			$archiveTable = $archiveDB . "." . $archiveTable;
			$sql = "rename table " . E4S_TABLE_EVENTTEAMENTRIES_DELETED . " to " . $archiveTable;
			e4s_queryNoLog( $sql );
			$sql = "create table  " . E4S_TABLE_EVENTTEAMENTRIES_DELETED . " like " . E4S_TABLE_EVENTTEAMENTRIES;
			e4s_queryNoLog( $sql );

			$sql = 'alter table ' . E4S_TABLE_EVENTTEAMENTRIES_DELETED . '
                add deleted timestamp default CURRENT_TIMESTAMP';
			e4s_queryNoLog( $sql );
		}
	}
	private function _updateToNextRelease(){
		$E4S_TABLE_ATHLETEPOF10 = E4S_COMMON_PREFIX . 'Athlete_Pof10';
		$sql = " drop table if exists " . $E4S_TABLE_ATHLETEPOF10;
		e4s_queryNoLog($sql);
	}
}