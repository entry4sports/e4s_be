<?php

class e4sRepayment {
    public $compId;
    public $orderId;
    public $users;
    public $entries;
    public $prices;
    public $config;

    public function __construct($model) {
        $this->config = e4s_getConfig();
        $this->compId = 0;
        if (isset($model->compId)) {
            $this->compId = $model->compId;
        }
        if ($this->compId > 0) {
            $this->_getPrices();
        } else {
            $this->prices = null;
        }
        $this->orderId = 0;
        if (isset($model->orderId)) {
            $this->orderId = $model->orderId;
        }
        $this->users = null;
        $this->entries = null;
    }

    private function _getPrices() {
        $sql = 'select *
                from ' . E4S_TABLE_EVENTPRICE . '
                where compid = ' . $this->compId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            return;
        }
        $this->prices = array();
        while ($obj = $result->fetch_object()) {
            $newObj = new stdClass();
            $newObj->id = (int)$obj->ID;
            $newObj->compId = $this->compId;
            $newObj->name = $obj->name;
            $newObj->price = (float)$obj->price;
            $newObj->salePrice = (float)$obj->saleprice;
            $newObj->fee = (float)$obj->fee;
            $newObj->saleFee = (float)$obj->salefee;
            $newObj->saleEndDate = $obj->saleenddate;
            $this->prices[$newObj->id] = $newObj;
        }
    }

    public function resetPayment() {
        $userCount = 0;
        $this->_getEntries();
        if (!empty($this->entries)) {
            $this->_getUsers();
            $userCount = $this->_process();
        }
        return $userCount;
    }

    private function _getEntries() {
        $sql = 'select e.*, ce.priceId
                from ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce
                where e.paid = ' . E4S_ENTRY_PAID . '
                and   e.compeventid = ce.id
                and   ce.compid = ' . $this->compId . '
                and   e.price   = 0
                ';
        $result = e4s_queryNoLog($sql);
        $entries = array();
        if ($result->num_rows > 0) {
            while ($entry = $result->fetch_object()) {
                $newEntry = $this->_normaliseEntry($entry);
                $entries[$newEntry->id] = $newEntry;
            }
        }
        $this->entries = $entries;
        return $entries;
    }

    private function _normaliseEntry($entry) {
        $newEntry = new stdClass();
        $newEntry->id = (int)$entry->id;
        $newEntry->userId = (int)$entry->userid;
        $newEntry->paid = (int)$entry->paid;
        $newEntry->priceId = (int)$entry->priceId;
        $newEntry->price = (float)$entry->price;
        $newEntry->orderId = (int)$entry->orderid;
        if (isset($entry->variationID)) {
            $newEntry->productId = (int)$entry->variationID;
        } else {
            $newEntry->productId = (int)$entry->productid;
        }

        return $newEntry;
    }

    private function _getUsers() {
        $userIds = array();
        foreach ($this->entries as $entry) {
            $userIds[] = $entry->userId;
        }
        $sql = 'select user_login loginName
                        ,user_nicename niceName
                        ,display_name displayName
                        , user_email email
                        , id
                from ' . E4S_TABLE_USERS . '
                where id in ( ' . implode(',', $userIds) . ')';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            return;
        }
        while ($userObj = $result->fetch_object()) {
            $userObj->id = (int)$userObj->id;
            $this->users[$userObj->id] = $userObj;
        }
    }

    private function _process() {
        $processedUsers = array();
        foreach ($this->entries as $entry) {
//            echo $entry->id . ",";
            if (!array_key_exists($entry->priceId, $this->prices)) {
                Entry4UIError(8450, 'Price Record not found for Entry ' . $entry->id);
            }
            $priceObj = $this->prices[$entry->priceId];
            if (!array_key_exists($entry->userId, $this->users)) {
                Entry4UIError(8451, 'User Record not found for Entry ' . $entry->id);
            }
            $this->_updatePostMeta($entry->productId, $priceObj);
            $this->_updateEntry($entry, $priceObj);
            if (!array_key_exists($entry->userId, $processedUsers)) {
                $processedUsers[$entry->userId] = array();
            }
            $processedUsers[$entry->userId][] = $entry;
        }
        foreach ($processedUsers as $userId => $processedUser) {
            $this->_sendEmail($userId, $processedUser);
        }
//        var_dump($processedUsers);
        return sizeof($processedUsers);
    }

    private function _updatePostMeta($productId, $priceObj) {
        $sql = 'update ' . E4S_TABLE_POSTMETA . '
                set meta_value = ' . $priceObj->price . '
                where post_id = ' . $productId . "
                and meta_key in ('_price', '_regular_price', '_sale_price')";
        e4s_queryNoLog($sql);

        $sql = 'update ' . E4S_TABLE_POSTMETA . '
                set meta_value = 1
                where post_id = ' . $productId . "
                and meta_key = '_stock'";
        e4s_queryNoLog($sql);

        $sql = 'update ' . E4S_TABLE_POSTMETA . "
                set meta_value = 'instock'
                where post_id = " . $productId . "
                and meta_key = '_stock_status'";
        e4s_queryNoLog($sql);
    }

    private function _updateEntry($entry, $priceObj) {
        $sql = 'update ' . E4S_TABLE_ENTRIES . '
                set paid = ' . E4S_ENTRY_NOT_PAID . ',
                    price = ' . $priceObj->price . '
                where id = ' . $entry->id;
        e4s_queryNoLog($sql);
    }

    private function _sendEmail($userId, $userEntries) {
        $userObj = $this->users[$userId];
        $newline = '<br>';
        $body = 'Dear ' . $userObj->displayName . ',' . $newline;
        $body .= 'We are sorry to inform you that a competition you recently entered ( MCAA U20 and Seniors June 2022 )  did not have their prices setup correctly and as such, you were not charged the correct fees.' . $newline;
        $body .= 'The prices have been corrected and put back into your Entry4Sports basket and are awaiting payment.' . $newline;
        $body .= 'The order(s) affected are :' . $newline;
        $orders = array();
        foreach ($userEntries as $userEntry) {
            if (!array_key_exists($userEntry->orderId, $orders)) {
                $body .= $userEntry->orderId . $newline;
                $orders[$userEntry->orderId] = TRUE;
            }
        }
        $body .= $newline;
        $body .= 'If you could please re-visit the Entry4Sports site and go to your basket, you will be able to complete the payment process and your entries will then be confirmed.' . $newline;
        $body .= "<a href=\"https://entry4sports.co.uk\">Entry4Sports Web Site</a>" . $newline;
        $body .= 'We apologise for any inconvenience caused.' . $newline . $newline;

        e4s_mail($userObj->email, $this->config['systemName'] . ':' . 'Issue with fees for MCAA Entries.', $body, Entry4_mailHeader('entries', TRUE));

    }

    private function _output($msg) {
        echo 'Repayment:' . $msg . "\n";
    }
}