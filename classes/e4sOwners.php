<?php
const E4S_OWNER_KEY_URN = 'u';
const E4S_OWNER_KEY_ID = 'i';
const E4S_TYPE_OWNER = 'Owner';
const E4S_TYPE_CLUB = 'Club';
const E4S_TYPE_COUNTY = 'County';
const E4S_TYPE_REGION = 'Region';
class e4sOwners {
    public $compObj;
    public $aoCode;
    public $sourceEgIds;
    public $clubObjs;
    public $athleteObjs;
    public $teamObjs;
    public $urns;
    public $athleteIds;
    public $keyType;

    public function __construct($keys, $keyType, $egIds = null) {
        $this->sourceEgIds = [];
        if (!is_null($egIds)) {
            if (gettype($egIds) !== E4S_OPTIONS_ARRAY) {
//            ensure array
                $egIds = [$egIds];
            }
            $this->sourceEgIds = $egIds;
            $this->_getCompObj();
        }
        $this->clubObjs = [];
        $config = e4s_getConfig();
        $this->aoCode = $config['defaultao'];
        $this->athleteObjs = [];
        $this->teamObjs = [];
        $this->keyType = $keyType;
        if ($this->_useURN()) {
            $this->urns = $keys;
        }
        if ($this->_useAthleteId()) {
            $this->athleteIds = $keys;
        }
    }

    public static function withURNs($urns, $egIds = null) {
        $instance = new self($urns, E4S_OWNER_KEY_URN, $egIds);

        return $instance;
    }

    public static function withAthleteIds($athleteIds, $egIds = null) {
        $instance = new self($athleteIds, E4S_OWNER_KEY_ID, $egIds);

        return $instance;
    }

    private function _useAthleteId() {
        if ($this->keyType === E4S_OWNER_KEY_ID) {
            return TRUE;
        }
        return FALSE;
    }

    private function _useURN() {
        if ($this->keyType === E4S_OWNER_KEY_URN) {
            return TRUE;
        }
        return FALSE;
    }

    private function _getDefaultAthleteObj() {
        $obj = new stdClass();
        $obj->urn = 0;
        $obj->id = 0;
        $obj->club = new stdClass();
        $obj->club->id = 0;
        $obj->club->name = '';
        $obj->county = new stdClass();
        $obj->county->id = 0;
        $obj->county->name = '';
        $obj->region = new stdClass();
        $obj->region->id = 0;
        $obj->region->name = '';
        $obj->entries = [];
        $obj->owners = [];
        return $obj;
    }

    public function getReturnObject($includeTeams) {
        $retObj = new stdClass();
        $retObj->athletes = $this->athleteObjs;
        if ($includeTeams) {
            $retObj->teams = $this->teamObjs;
        }
        return $retObj;
    }

    public function getOwners($includeEventInfo = TRUE) {
        $this->_getAthletes();
        $this->_getOwners();
        $this->_getSrcOwners();
        $this->_getClubInfo();
    }

    private function _getOwners() {
        $idUrns = [];
        $athleteIds = [];
        foreach ($this->athleteObjs as $Urn => $athleteObj) {
            $athleteIds[] = $athleteObj->id;
            $idUrns[$athleteObj->id] = $Urn;
        }
        $sql = '
            select u.id userId
                    ,u.user_email userEmail
                    ,ua.athleteId
            from ' . E4S_TABLE_USERS . ' u,
                 ' . E4S_TABLE_USERATHLETES . ' ua
            where u.id = ua.userid
            and   ua.athleteid in (' . implode(',', $athleteIds) . ')
        ';
        $result = e4s_queryNoLog($sql);
        while ($obj = $result->fetch_object()) {
            $obj->userId = (int)$obj->userId;
            $obj->athleteId = (int)$obj->athleteId;
            $urn = $idUrns[$obj->athleteId];
            $athleteObj = $this->athleteObjs[$urn];
            $owners = $athleteObj->owners;
            if (!array_key_exists($obj->userId, $owners)) {
                $userObj = $this->_getDefaultUserObj();
                $owners[$obj->userId] = $userObj;
            }
            $userObj = $owners[$obj->userId];
            $userObj->id = $obj->userId;
            $userObj->name = $obj->userEmail;
            if (!array_key_exists(E4S_TYPE_OWNER, $userObj->types)) {
                $userObj->types[] = E4S_TYPE_OWNER;
            }
            $athleteObj->owners = $owners;
        }
    }

    private function _getClubInfo() {
        $clubIds = [];
        $areaIds = [];
        foreach ($this->clubObjs as $clubId => $clubObj) {
            $clubIds[] = $clubId;
            if (!in_array($clubObj->countyId, $areaIds)) {
                if (!is_null($clubObj->countyId)) {
                    $areaIds[] = $clubObj->countyId;
                }
            }
            if (!in_array($clubObj->regionId, $areaIds)) {
                if (!is_null($clubObj->regionId)) {
                    $areaIds[] = $clubObj->regionId;
                }
            }
        }
        $sql = '
            select u.id userId
                  ,u.user_email userEmail
                  ,um.meta_key type
                  ,um.meta_value areaId
            from ' . E4S_TABLE_USERS . ' u,
                 ' . E4S_TABLE_USERMETA . " um
            where u.id = um.user_id
            and (
                (um.meta_key = '" . E4S_CLUB_ID . "' and um.meta_value in (" . implode(',', $clubIds) . ") )
                or
                (um.meta_key = '" . E4S_AREA_ID . "' and um.meta_value in (" . implode(',', $areaIds) . ') )
            )
        ';
        $result = e4s_queryNoLog($sql);
        $clubObjs = [];
        $areaObjs = [];
        while ($obj = $result->fetch_object()) {
            $obj->userId = (int)$obj->userId;
            $obj->areaId = (int)$obj->areaId;
            if ($obj->type === E4S_CLUB_ID) {
                if (!array_key_exists($obj->areaId, $clubObjs)) {
                    $clubObj = new stdClass();
                    $clubObj->id = $obj->areaId;
                    $clubObj->users = [];
                    $clubObjs[$obj->areaId] = $clubObj;
                }
                $users = $clubObjs[$obj->areaId]->users;
                $users[$obj->userId] = $obj->userEmail;
                $clubObjs[$obj->areaId]->users = $users;
            }
            if ($obj->type === E4S_AREA_ID) {
                if (!array_key_exists($obj->areaId, $areaObjs)) {
                    $areaObj = new stdClass();
                    $areaObj->id = $obj->areaId;
                    $areaObj->users = [];
                    $areaObjs[$obj->areaId] = $areaObj;
                }
                $users = $areaObjs[$obj->areaId]->users;
                $users[$obj->userId] = $obj->userEmail;
                $areaObjs[$obj->areaId]->users = $users;
            }
        }
        foreach ($this->athleteObjs as $athleteObj) {
            $athleteClubId = $athleteObj->club->id;
            if (array_key_exists($athleteClubId, $this->clubObjs)) {
                $clubObj = $this->clubObjs[$athleteClubId];
                $athleteObj->county->id = $clubObj->countyId;
                $athleteObj->county->name = $clubObj->county;
                $athleteObj->region->id = $clubObj->regionId;
                $athleteObj->region->name = $clubObj->region;
            }
            if (array_key_exists($athleteClubId, $clubObjs)) {
                $users = $clubObjs[$athleteClubId]->users;
                $owners = $athleteObj->owners;
                foreach ($users as $userId => $userEmail) {
                    if (array_key_exists($userId, $owners)) {
                        if (!in_array(E4S_TYPE_CLUB, $owners[$userId]->types)) {
                            $owners[$userId]->types[] = E4S_TYPE_CLUB;
                        }
                    } else {
                        $owners[$userId] = $this->_getDefaultUserObj();
                        $owners[$userId]->id = $userId;
                        $owners[$userId]->name = $userEmail;
                        $owners[$userId]->types[] = E4S_TYPE_CLUB;
                    }
                }
                if (array_key_exists($athleteClubId, $this->clubObjs)) {
                    $clubObj = $this->clubObjs[$athleteClubId];
                    if (array_key_exists($clubObj->countyId, $areaObjs)) {
                        $areaObj = $areaObjs[$clubObj->countyId];
                        foreach ($areaObj->users as $userId => $userEmail) {
                            if (array_key_exists($userId, $owners)) {
                                if (!in_array(E4S_TYPE_COUNTY, $owners[$userId]->types)) {
                                    $owners[$userId]->types[] = E4S_TYPE_COUNTY;
                                }
                            } else {
                                $owners[$userId] = $this->_getDefaultUserObj();
                                $owners[$userId]->id = $userId;
                                $owners[$userId]->name = $userEmail;
                                $owners[$userId]->types[] = E4S_TYPE_COUNTY;
                            }
                        }
                    }
                    if (array_key_exists($clubObj->regionId, $areaObjs)) {
                        $areaObj = $areaObjs[$clubObj->regionId];
                        foreach ($areaObj->users as $userId => $userEmail) {
                            if (array_key_exists($userId, $owners)) {
                                if (!in_array(E4S_TYPE_REGION, $owners[$userId]->types)) {
                                    $owners[$userId]->types[] = E4S_TYPE_REGION;
                                }
                            } else {
                                $owners[$userId] = $this->_getDefaultUserObj();
                                $owners[$userId]->id = $userId;
                                $owners[$userId]->name = $userEmail;
                                $owners[$userId]->types[] = E4S_TYPE_REGION;
                            }
                        }
                    }
                }
                $athleteObj->owners = $owners;
            }
        }
    }

    private function _getURNs() {
        return $this->urns;
    }

    private function _getAthleteIds() {
        return $this->athleteIds;
    }

    private function _getAthletes() {
        $sql = 'select a.id athleteId
                       ,a.URN
                       ,c.id clubId
                       ,c.clubname
                       ,county.id countyId
                       ,county.name county
                       ,region.id regionId
                       ,region.name region
                from ' . E4S_TABLE_ATHLETE . ' a,
                     ' . E4S_TABLE_CLUBS . ' c
                         left join ' . E4S_TABLE_AREA . ' county on (county.id = c.areaid)
                         left join ' . E4S_TABLE_AREA . ' region on (region.id = county.parentid)
                where a.clubid = c.id';

        if ($this->_useURN()) {
            $urns = $this->_getURNs();
            if (sizeof($urns) === 0) {
                Entry4UIError(5900, 'Failed to find any athletes for the data passed');
            }
            $sql .= '  and   a.urn in (' . implode(',', $urns) . ")
                and   a.aocode = '" . $this->aoCode['code'] . "'";
        } else {
            $athleteIds = $this->_getAthleteIds();
            $sql .= ' and a.id in (' . implode(',', $this->athleteIds) . ')';
        }
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows > 0) {
            while ($obj = $result->fetch_object()) {
                $obj->athleteId = (int)$obj->athleteId;
                if (!is_null($obj->URN)) {
                    $obj->URN = (int)$obj->URN;
                }
                $obj->clubId = (int)$obj->clubId;
                if (!is_null($obj->countyId)) {
                    $obj->countyId = (int)$obj->countyId;
                }
                if (!is_null($obj->regionId)) {
                    $obj->regionId = (int)$obj->regionId;
                }
                if (!array_key_exists($obj->URN, $this->athleteObjs)) {
                    $this->_setAthleteObj($obj);
                }
            }
        } else {
            Entry4UIError(6950, 'No athletes found');
        }
    }

    private function _setAthleteObj($obj) {
        $athleteObj = $this->_getDefaultAthleteObj();
        $athleteObj->id = (int)$obj->athleteId;
        $athleteObj->urn = $obj->URN;
        $athleteObj->club->id = $obj->clubId;

        if (!array_key_exists($obj->clubId, $this->clubObjs)) {
            $areaObj = new stdClass();
            $areaObj->countyId = $obj->countyId;
            $areaObj->county = $obj->county;
            $areaObj->regionId = $obj->regionId;
            $areaObj->region = $obj->region;
            $this->clubObjs[$obj->clubId] = $areaObj;
        }
        $athleteObj->club->name = $obj->clubname;
        $this->athleteObjs[$obj->URN] = $athleteObj;
        return $athleteObj;
    }

    private function _getDefaultUserObj() {
        $userObj = new stdClass();
        $userObj->id = 0;
        $userObj->name = '';
        $userObj->types = [];
        return $userObj;
    }

    private function _addOwner($obj) {
        if (!array_key_exists($obj->URN, $this->athleteObjs)) {
            $this->_setAthleteObj($obj);
        }
        $athleteObj = $this->athleteObjs[$obj->URN];

        if (!array_key_exists($obj->egId, $athleteObj->entries)) {
            $entryObj = new stdClass();
            $entryObj->entryId = $obj->entryId;
            $entryObj->ownerId = $obj->userId;
            $athleteObj->entries[$obj->egId] = $entryObj;
        }
        if (!array_key_exists($obj->userId, $athleteObj->owners)) {
            $userObj = $this->_getDefaultUserObj();
            $userObj->id = $obj->userId;
            $userObj->name = $obj->userEmail;
//            if ( strpos($userObj->name,"entry4sports") === false ) {
            $athleteObj->owners[$obj->userId] = $userObj;
//            }
        }
        $userObj = $athleteObj->owners[$obj->userId];
//        $typeObj = $this->_getDefaultTypeObj();
//        $typeObj->type = E4S_TYPE_OWNER;
        $userObj->types[] = E4S_TYPE_OWNER;

        $this->athleteObjs[$obj->URN] = $athleteObj;
    }

    private function _getDefaultTypeObj() {
        $typeObj = new stdClass();
        $typeObj->type = '';
        $typeObj->entityId = 0;
        $typeObj->entityName = '';
        return $typeObj;
    }

    private function _getSrcOwners() {
        if (!empty($this->sourceEgIds)) {
            if ($this->sourceEgIds[0] === 0) {
                return;
            }
        } else {
            return;
        }
        $unUsedEgIds = $this->sourceEgIds;
        $urns = $this->_getURNs();

        $sql = 'select a.id athleteId
                       ,a.URN
                       ,e.id entryId
                       ,e.userId
                       ,ce.maxgroup egId
                       ,u.user_email userEmail
                       ,c.id clubId
                       ,c.clubname
                from ' . E4S_TABLE_ATHLETE . ' a,
                     ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_USERS . ' u,
                     ' . E4S_TABLE_CLUBS . ' c,
                     ' . E4S_TABLE_COMPEVENTS . ' ce
                where ce.maxGroup in (' . implode(',', $this->sourceEgIds) . ')
                and   e.compeventid = ce.id
                and   e.userid = u.id
                and   e.athleteid = a.id
                and   e.clubid = c.id
                and   a.urn in (' . implode(',', $urns) . ")
                and   a.aocode = '" . $this->aoCode['code'] . "'";
        $result = e4s_queryNoLog($sql);

        while ($obj = $result->fetch_object()) {
            $obj->athleteId = (int)$obj->athleteId;
            $obj->URN = (int)$obj->URN;
            $obj->entryId = (int)$obj->entryId;
            $obj->userId = (int)$obj->userId;
            $obj->egId = (int)$obj->egId;
            foreach ($unUsedEgIds as $element => $unUsedEgId) {
                if ($unUsedEgId === $obj->egId) {
                    unset($unUsedEgIds[$element]);
                }
            }
            unset($unUsedEgIds[$obj->egId]);
            $obj->clubId = (int)$obj->clubId;
            $this->_addOwner($obj);
        }

        $unUsedEgIds = array_values($unUsedEgIds);
        if (!empty($unUsedEgIds)) {
            // We have teams to get
            $this->_getTeamInfo($unUsedEgIds);
        }
    }

    private function _getTeamInfo($teamEgIds) {
        $sql = '
            select e.id entryId,
                   e.ceId,
                   e.userId,
                   e.name teamName,
                   e.entityLevel,
                   e.entityId,
                   ea.pos,
                   ea.athleteId,
                   a.firstName,
                   a.surName,
                   ce.ageGroupId,
                   ce.eventId,
                   ce.maxGroup egId,
                   u.user_email userEmail
            from ' . E4S_TABLE_EVENTTEAMENTRIES . ' e,
                 ' . E4S_TABLE_EVENTTEAMATHLETE . ' ea,
                 ' . E4S_TABLE_ATHLETE . ' a,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_USERS . ' u
            where ce.maxGroup in (' . implode(',', $teamEgIds) . ')
            and e.ceid = ce.id
            and e.id = ea.teamentryid
            and u.id = e.userid
            and a.id = ea.athleteid
            order by e.id, ea.pos
        ';
        $result = e4s_queryNoLog($sql);
        while ($obj = $result->fetch_object()) {
            $obj->entryId = (int)$obj->entryId;
            $obj->egId = (int)$obj->egId;
            $egId = $obj->egId;
            $obj->ceId = (int)$obj->ceId;
            $user = new stdClass();
            $user->id = (int)$obj->userId;
            $user->userEmail = $obj->userEmail;
            $obj->user = $user;
            unset($obj->userId);
            unset($obj->userEmail);
            $entity = new stdClass();
            $entity->level = (int)$obj->entityLevel;
            $entity->id = (int)$obj->entityId;
            $obj->entity = $entity;
            unset($obj->entityLevel);
            unset($obj->entityId);

            $obj->ageGroupId = (int)$obj->ageGroupId;
            $obj->eventId = (int)$obj->eventId;
            $athlete = new stdClass();
            $athlete->id = (int)$obj->athleteId;
            $athlete->firstName = formatAthleteFirstname($obj->firstName);
            $athlete->surName = formatAthleteSurname($obj->surName);
            $athlete->post = (int)($obj->pos);

            unset($obj->athleteId);
            unset($obj->pos);
            unset($obj->firstName);
            unset($obj->surName);

            if (!array_key_exists($egId, $this->teamObjs)) {
                $this->teamObjs[$egId] = array();
            }
            $teamEg = $this->teamObjs[$egId];
            if (!array_key_exists($obj->teamName, $teamEg)) {
                $obj->athletes = array();
                $teamEg[$obj->teamName] = $obj;
            }
            $this->teamObjs[$egId] = $teamEg;
            $teamObj = $this->teamObjs[$egId][$obj->teamName];
            $athletes = $teamObj->athletes;
            if (!array_key_exists($athlete->id, $athletes)) {
                $athletes[$athlete->id] = $athlete;
            }
            $this->teamObjs[$egId][$obj->teamName]->athletes = $athletes;
        }
    }

    private function _getCompObj() {
        $this->compObj = null;
//        assume all egs are from the same comp
        if (!empty($this->sourceEgIds)) {
            $egId = $this->sourceEgIds[0];
            if ($egId > 0) {
                $sql = 'select compId
                    from ' . E4S_TABLE_EVENTGROUPS . '
                    where id = ' . $egId;
                $result = e4s_queryNoLog($sql);
                if ($result->num_rows === 1) {
                    $egObj = $result->fetch_object();
                    $this->compObj = e4s_getCompObj((int)$egObj->compId);
                }
            }
        }
    }
}
