<?php

class e4sUserClass {
    public $userId;
    public $userIds;
    public $email;
    public $row;

    public static function withId($id) {
        $instance = new self();
        $instance->userId = $id;
        return $instance;
    }

    public static function withEmail($email) {
        $instance = new self();
        $instance->email = $email;
        return $instance;
    }

    public function _construct() {
        $this->email = '';
        $this->userId = 0;
        $this->userIds = array();
        $this->row = null;
    }

    public function getClubComps() {
        $compClubObj = new clubCompClass(0, 0, FALSE);
        $compClubObj->forUser($this->userId);
        $compClubs = $compClubObj->getClubComps();
        return $compClubs;
    }

    public function setUserIds($userIds) {
        $this->userIds = $userIds;
    }

    public function getRows($userIds = null) {
        if (is_null($userIds)) {
            $userIds = $this->userIds;
        }
        $rows = array();
        if (is_null($userIds) or empty($userIds)) {
            return $rows;
        }
        $sql = $this->_getSQL();
        $sql .= ' where id in (' . implode(',', $userIds) . ')';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return $rows;
        }

        while ($row = $result->fetch_object()) {
            $row = $this->_normalise($row);
            $rows[$row->id] = $row;
        }
        return $rows;
    }

    private function _getSQL() {
        $sql = 'select id
                        ,user_login login
                        ,user_nicename niceName
                        ,user_email email
                        ,display_name displayName
            from ' . E4S_TABLE_USERS;
        return $sql;
    }

    private function _normalise($row) {
        $row->id = (int)$row->id;
        return $row;
    }

    public function getRow() {
        $sql = $this->_getSQL();
        if ($this->userId !== 0) {
            $sql .= ' where id = ' . $this->userId;
        } elseif ($this->email !== '') {
            $sql .= ' where user_email = ' . $this->email;
        }
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            return null;
        }
        $this->row = $this->_normalise($result->fetch_object());
        return $this->row;
    }

    public function getUIName() {
        $name = $this->row->displayName;
        if (strpos($name, '@')) {
            $arr = preg_split('~@~', $name);
            $name = $arr[0];
        }
        $name = ucwords(strtolower($name));
        return $name;
    }

    public function getAdminPerms(){
        if ( $this->userId === e4s_getUserID() ){
            $userData = $GLOBALS[E4S_USER];
        }else {
            $sql = 'select meta_value
                    from ' . E4S_TABLE_USERMETA . '
                    where user_id = ' . $this->userId . "
                    and meta_key = '" . E4S_USER_KEY . "'";
            $result = e4s_queryNoLog($sql);
            if ( $result->num_rows !== 1 ){
                return [];
            }
            $obj = $result->fetch_object();
            $userData = json_decode($obj->meta_value);
        }
        $perms = $this->_getPermissions($userData);
        $adminPerms = [];
        foreach($perms as $perm){
            if ($perm->role->name === 'admin'){
                $perm->id = (int)$perm->id;
                $perm->userid = (int)$perm->userid;
                $perm->role->id = (int)$perm->role->id;
                $perm->comp->id = (int)$perm->comp->id;
                $perm->org->id = (int)$perm->org->id;
                $adminPerms[] = $perm;
            }
        }
        return $adminPerms;
    }
    private function _getPermissions($userData):array{
        $perms = [];
        if (isset($userData->user)) {
            if (isset($userData->user->permissions)) {
                $perms = $userData->user->permissions;
            }
        }
        return $perms;
    }
}

class e4s_userAthletes {
	var $cacheKey = 'e4s_user_athlete_cache';
	var $userType = 'u';
	var $athleteType = 'a';
	public function __construct() {
	}
	public function getData($type, $id, $force = false) {
		$typeId = $type . '_' . $id;
		if ( $type === $this->userType ) {
			$dbField = 'userid';
		} else {
			$dbField = 'athleteid';
		}
		if ( ! $force ) {
			$data = $this->_getDataFromCache($typeId);
			if ( ! empty($data) ) {
				return $data;
			}
		}
		$sql = 'select *
			from ' . E4S_TABLE_USERATHLETES . '
			where ' . $dbField . ' = ' . $id ;
		$result = e4s_queryNoLog($sql);
		$data = [];
		if ($result->num_rows !== 0) {
			while ($row = $result->fetch_object(E4S_USERATHLETES_OBJ)) {
				$data[] = $row;
			}
		}
		$this->_setDataInCache($typeId, $data);
		return $data;
	}
	private function _getDataFromCache($id) {
		$data = [];
		if ( array_key_exists($this->cacheKey, $GLOBALS) ){
			if ( array_key_exists($id, $GLOBALS[$this->cacheKey]) ){
				$data = $GLOBALS[$this->cacheKey][$id];
			}
		}
		return $data;
	}
	private function _setDataInCache($id, $data) {
		if ( ! array_key_exists($this->cacheKey, $GLOBALS) ){
			$GLOBALS[$this->cacheKey] = [];
		}
		$GLOBALS[$this->cacheKey][$id] = $data;
	}
}