<?php

class photofinish {
    public e4sCompetition $compObj;

    public function __construct($compId) {
        $this->compObj = e4s_getCompObj($compId);
    }

    public function validateAuth() {
        if (!$this->compObj->isOrganiser()) {
            Entry4UIError(4099, 'Unathourised');
        }
    }

    public function response($fileName) {
        $this->sendSocket($fileName, 1,R4S_SOCKET_PF_RESPONSE);
    }

    public function readFile($fileName) {
        $this->validateAuth();
        $sql = 'select *
              from ' . E4S_TABLE_PHOTOFINISH . '
              where compId = ' . $this->compObj->getID() . "
              and   filename = '" . $fileName . "'";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(7905, 'No File Found');
        }
        $obj = $result->fetch_object(E4S_PHOTOFINISH_OBJ);
        Entry4UISuccess($obj);
    }

    public function create($fileName, $system, $payload) {
        $this->validateAuth();
        $sql = 'select *
              from ' . E4S_TABLE_PHOTOFINISH . '
              where compId = ' . $this->compObj->getID() . '
              and   filename = "' . $fileName . '"
              and system = ' . $system ;
		$result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            $sql = 'insert into ' . E4S_TABLE_PHOTOFINISH . ' (compid, filename, system, body)
                    values(
                        ' . $this->compObj->getID() . ",
                        '" . $fileName . "',
                        " . $system . ",
                        '" . addslashes($payload) . "'
                    )";
        } else {
            $obj = $result->fetch_object(E4S_PHOTOFINISH_OBJ);

            $sql = 'update ' . E4S_TABLE_PHOTOFINISH . "
                    set body = '" . addslashes($payload) . "'
                    where id = " . $obj->id;
        }
        e4s_queryNoLog($sql);
        $this->sendSocket($fileName, $system, R4S_SOCKET_PF_FILE);
    }

    private function sendSocket($filename, $system, $type) {
        $payload = new stdClass();
        $payload->file = $filename;
        $payload->system = $system;
        $this->compObj->sendSocketInfo($payload, $type);
    }
}