<?php
// v1
include_once E4S_FULL_PATH . 'classes/pbClass.php';
include_once E4S_FULL_PATH . 'classes/pof10Class.php';
// v2
include_once E4S_FULL_PATH . 'classes/pof10V2Class.php';

class perfAthlete {
	public function __construct(){

	}
	function updateTrackedEntries($athleteId){
		$sql = "select e.entryId, e.ceId, e.compID, entryPb
				from " . E4S_TABLE_ENTRYINFO . " e
				where athleteid = " . $athleteId . "
				  and   e.paid in ( " . E4S_ENTRY_PAID. ", " . E4S_ENTRY_QUALIFY . " )
				  and   e.eventStartDate > CURRENT_DATE()
				  and trackedPb = 1
				order by compId";
		$results = e4s_queryNoLog( $sql );
		if ( $results->num_rows === 0 ) {
			return;
		}
		$performances = $this->getPerformances( $athleteId );
		$performances = $performances[$athleteId];
		while( $entryObj = $results->fetch_object( E4S_ENTRYINFO_OBJ ) ) {
			$entryPb = $entryObj->entryPb;
			$entryPb = resultsClass::getResultInSeconds( $entryPb );
			$entryPb = (float)$entryPb;
			$compObj = e4s_getCompObj( $entryObj->compId );
			$ceObj = $compObj->getCEByCeID($entryObj->ceId);
			if ( !array_key_exists( $ceObj->eventId, $performances ) ) {
				continue;
			}
			if ( $entryPb !== $performances[$ceObj->eventId]->sb ) {
				$sql = "update " . E4S_TABLE_ENTRIES . "
				        set pb = " . $performances[$ceObj->eventId]->sb . "
				        where id = " . $entryObj->entryId;
				e4s_queryNoLog( $sql );
			}
		}
	}
	function getPerformances($athleteIdArr):array{
		if ( gettype($athleteIdArr) === 'integer' ){
			$athleteIdArr = array($athleteIdArr);
		}
		$sql = '
			select p.*,
			       e.name eventName,
	               e.options options,
	               e.eventType,
	               e.tf,
	               e.uomOptions uomOptions,
	               e.min,
	               e.max,
	               a.curAgeGroup
			from ' . E4S_TABLE_ATHLETEPERF . ' p,
			     ' . E4S_TABLE_EVENTS . ' e,
			     ' . E4S_TABLE_ATHLETE . ' a
			where athleteid in (' . implode(',', $athleteIdArr) . ')
			and e.id = p.eventid
			and a.id = p.athleteid
			and p.perf > 0
			order by eventid, achieved desc
			';
		$result = e4s_queryNoLog($sql);

		$pbs = array();
		$currentYear = date('Y');
		while ( $obj = $result->fetch_object(E4S_ATHLETEPERFOBJ) ) {
			$obj->options  = e4s_getOptionsAsObj( $obj->options );
			$obj->uomOptions = e4s_getOptionsAsObj( $obj->uomOptions );
			$check = true;
			if ( $obj->info !== '' and $obj->ageGroup !== $obj->curAgeGroup) {
				$obj->pb = 0;
				$obj->pbAchieved = '';
			}

			if ( $check ){
				$obj->achieved = explode(' ', $obj->achieved)[0];
				if ( ! array_key_exists( $obj->athleteId, $pbs ) ) {
					$pbs[ $obj->athleteId ] = array();
				}
				if ( ! array_key_exists( $obj->eventId, $pbs[ $obj->athleteId ] ) ) {
					$obj->sb = 0;
					$obj->pb = $obj->perf;
					$obj->sbAchieved = null;
					$obj->pbAchieved = $obj->achieved;
					$pbs[ $obj->athleteId ][ $obj->eventId ] = $obj;
				}
				$pbObj = $pbs[ $obj->athleteId ][ $obj->eventId ];

				if ($obj->tf === E4S_EVENT_TRACK){
					$process = true;
					if ( $obj->info !== ''){
						if ( $obj->info[0] === 'U'){
							if ( stripos($obj->info,$obj->ageGroup) === false ){
								$process = false;
							}
						}
					}
					if ($process) {
						if ( $obj->perf > 0 and $obj->perf < $pbObj->pb ) {
							$pbObj->pb         = $obj->perf;
							$pbObj->pbAchieved = $obj->achieved;
						}
					}
				}
				if ($obj->tf === E4S_EVENT_FIELD){
					if($obj->perf > 0 and $obj->perf > $pbObj->pb) {
						$pbObj->pb = $obj->perf;
						$pbObj->pbAchieved = $obj->achieved;
					}
				}
				// Get Seasons Best Performance
				if ( e4s_startsWith($obj->achieved, $currentYear) ) {
					if ($obj->tf === E4S_EVENT_TRACK){
						$process = true;
						if ( $obj->info !== ''){
							if ( $obj->info[0] === 'U'){
								if ( stripos($obj->info,$obj->ageGroup) === false ){
									$process = false;
								}
							}
						}
						if ($process) {
							if ( $pbObj->pb < $pbObj->sb or $pbObj->sb === 0 ) {
								$pbObj->sb         = $pbObj->pb;
								$pbObj->sbAchieved = $pbObj->pbAchieved;
							}
						}
					}
					if ($obj->tf === E4S_EVENT_FIELD){
						if($pbObj->pb > $pbObj->sb or $pbObj->sb === 0) {
							$pbObj->sb = $pbObj->pb;
							$pbObj->sbAchieved = $pbObj->pbAchieved;
						}
					}
				}
				if ( isset($pbObj->perf) ){
					unset($pbObj->perf);
				}
				if ( isset($pbObj->achieved) ){
					unset($pbObj->achieved);
				}
				if ($obj->tf === E4S_EVENT_TRACK) {
					$pbObj->pbText = E4S_ENSURE_STRING . resultsClass::getResultFromSeconds($pbObj->pb);
					$pbObj->sbText = E4S_ENSURE_STRING . resultsClass::getResultFromSeconds($pbObj->sb);
				}else{
					// ensure 2 decimal places
					$pbObj->pb = resultsClass::ensureDecimals($pbObj->pb);
					$pbObj->pbText = E4S_ENSURE_STRING . $pbObj->pb;
					$pbObj->sb = resultsClass::ensureDecimals($pbObj->sb);
					$pbObj->sbText = E4S_ENSURE_STRING . $pbObj->sb;
				}
			}
		}
		return $pbs;
	}
	function getRankings($athleteIdArr, $year = 0):array{
		if ( gettype($athleteIdArr) === 'integer' ){
			$athleteIdArr = array($athleteIdArr);
		}
		$rankings = array();

		if ( $year === 0 ){
			$year = date('Y');
		}
		$sql="
	        select *
	        from " . E4S_TABLE_ATHLETERANK . "
	        where athleteId in (" . implode(',', $athleteIdArr) . ")
	        and year >= " . $year;
		$result = e4s_queryNoLog($sql);
		while($obj = $result->fetch_object(E4S_ATHLETERANK_OBJ)){
			if ( !array_key_exists($obj->athleteId, $rankings) ) {
				$rankings[ $obj->athleteId ] = [];
			}
			unset($obj->optionsSet);
			$rankings[$obj->athleteId][] = $obj;
		}

		return $rankings;
	}
	public function getBlankAthlete(){
		$athlete = new stdClass();
		$athlete->id = 0;
		$athlete->pof10Id = 0;
		$athlete->urn = '';
		$athlete->name = '';
		$athlete->club = '';
		$athlete->county = '';
		$athlete->region = '';
		$athlete->country = '';
		$athlete->gender = '';
		$athlete->ageGroup = '';
		$athlete->coach = '';
		$athlete->imageURL = '';
		$athlete->lastCheckedPB = '';
		return $athlete;
	}
	public function getBlankRank(){
		$rank = new stdClass();
		$rank->event = '';
		$rank->ageGroup = '';
		$rank->year = '';
		$rank->rank = 0;
		return $rank;
	}
	public function getBlankPerf(){
		$perf = new stdClass();
		$perf->event = '';
		$perf->date = '';
		$perf->perf = 0;
		$perf->info = '';
		$perf->ageGroup = '';
		$perf->wind = '';
		return $perf;
	}
}