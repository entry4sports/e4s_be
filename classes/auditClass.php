<?php

class e4sAudit {
    public $conn;
    public $tableAudit;
    public $tableTable;
    public $compid;
    public $userid;
    public $auditid;
    public $active;
    public $compObj;

    public function __construct() {
        $auditDB = DB_NAME;
        $auditHost = DB_HOST;
        $auditUser = DB_USER;
        $auditPwd = DB_PASSWORD;
        switch (E4S_CURRENT_DOMAIN) {
            case E4S_DEMO_DOMAIN:
                $auditDB = 'dbs279890';
                $auditHost = 'db5000286677.hosting-data.io';
                $auditUser = 'dbu88397';
                $auditPwd = 'Iloveathletics!1';
                break;
            case '10.0.0.24':
	        case '10.0.0.34':
            case '10.0.0.43':
            case E4S_AAI_DOMAIN:
            case E4S_AAI_UAT_DOMAIN:
                $auditDB = 'e4sAudit';
                $auditHost = 'localhost';
                $auditUser = 'entrysystem';
                $auditPwd = 'entry4sport@Aainet';
                break;
        }

        $this->conn = new mysqli($auditHost, $auditUser, $auditPwd, $auditDB);
        $this->tableAudit = E4S_LOG_PREFIX . 'Audit';
        $this->tableTable = E4S_LOG_PREFIX . 'AuditTable';
        $this->userid = (int)e4s_getUserID();
        $this->compid = 0;
        $this->active = TRUE;
    }

    public static function withID($compid) {
        $instance = new self();
        $instance->compid = $compid;
        $instance->compObj = e4s_GetCompObj($compid);
        $instance->active = $instance->compObj->isActive();
        return $instance;
    }

    public static function withRow(array $row) {
        $instance = new self();
        $instance->compObj = e4sCompetition::withRow($row);
        $instance->compid = $instance->compObj->compId;
        $instance->active = $instance->compObj->isActive();
        return $instance;
    }

    public function setActive($toActive) {
        $this->active = $toActive;
    }

    public function writeAudit($reason, $snapshot = FALSE, $table = null, $id = null, $rows = null) {
        // only audit if comp is active
        if (!$this->active) {
            return;
        }
        if (is_null($id)) {
            $field = 'compid';
            $id = $this->compid;
        } else {
            $field = 'id';
        }
        if (!is_null($reason) and strlen($reason) > 200) {
            // reason can not be more than 200 chars
            $reason = substr($reason, 0, 195) . '...';
        }
        $sql = 'insert into ' . $this->tableAudit . '(compid, userid, reason, created) ' . 'values ';

        $sqlInsert = '({COMPID},' . $this->userid . ',' . "'" . addslashes($reason) . " {AUDITID}'," . 'now()' . ')';

        $thisSql = $sql . str_replace('{COMPID}', $this->compid, $sqlInsert);
        $thisSql = str_replace('{AUDITID}', '', $thisSql);
        mysqli_query($this->conn, $thisSql);

        $error = mysqli_error($this->conn);
        $this->auditid = $this->conn->insert_id;
        if ($error !== '') {
            Entry4UIError(7010, 'Failed to write Audit : ' . $error, 400, '');
        }

        if ($this->compid === 0) {
            $reasonArr = explode(': ', $reason);
            if (sizeof($reasonArr) === 2) {
                $locId = $reasonArr[1];
                // get all comps with current location id and create an audit for them
                $compSql = 'select id from ' . E4S_TABLE_COMPETITON . '
                        where locationid = ' . $locId;
                $compResult = e4s_queryNoLog($compSql);
                if ($compResult->num_rows > 0) {
                    $existSql = $sql;
                    $sqlSub = '';
                    while ($compObj = $compResult->fetch_object()) {
                        $lineSql = str_replace('{COMPID}', $compObj->id, $sqlInsert);
                        $lineSql = str_replace('{AUDITID}', '/ ' . $this->auditid, $lineSql);
                        $existSql .= $sqlSub . $lineSql;
                        $sqlSub = ',';
                    }
                    mysqli_query($this->conn, $existSql);
                }
            }
        }


        if ($snapshot) {
            if (is_null($table) or $table === E4S_TABLE_COMPETITON) {
                $this->_writeTableAudit(E4S_TABLE_COMPETITON, 'id', $this->compid, $rows);
            }
            if (is_null($table) or $table === E4S_TABLE_COMPEVENTS) {
                $this->_writeTableAudit(E4S_TABLE_COMPEVENTS, $field, $id, $rows);
            }
            if (is_null($table) or $table === E4S_TABLE_COMPDISCOUNTS) {
                $this->_writeTableAudit(E4S_TABLE_COMPDISCOUNTS, $field, $id, $rows);
            }
            if (is_null($table) or $table === E4S_TABLE_COMPRULES) {
                $this->_writeTableAudit(E4S_TABLE_COMPRULES, $field, $id, $rows);
            }
            if (is_null($table) or $table === E4S_TABLE_EVENTGROUPS) {
                $this->_writeTableAudit(E4S_TABLE_EVENTGROUPS, $field, $id, $rows);
            }
            if (is_null($table) or $table === E4S_TABLE_EVENTPRICE) {
                $this->_writeTableAudit(E4S_TABLE_EVENTPRICE, $field, $id, $rows);
            }
            if ($table === E4S_TABLE_LOCATION) {
                $this->_writeTableAudit(E4S_TABLE_LOCATION, $field, $id, $rows);
            }
        }
    }

    public function _writeTableAudit($tablename, $fieldname = 'compid', $value = null, $rows = null) {
        if (is_null($value)) {
            $value = $this->compid;
        }

        if (gettype($value) === 'array') {
            $inOrEqualsAndValue = ' in (' . implode(',', $value) . ')';
        } else {
            $inOrEqualsAndValue = ' = ' . $value;
        }
        if (is_null($rows)) {
            $sql = 'select *
                from ' . $tablename . '
                where ' . $fieldname . $inOrEqualsAndValue;
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows === 0) {
                return;
            }
            $rows = $result->fetch_all(MYSQLI_ASSOC);
        }
        $insert = 'insert into ' . $this->tableTable . ' (auditid,tablename,tabledata)
                   values ( ' . $this->auditid . ',' . "'" . $tablename . "'," . "'" . addslashes(json_encode($rows, JSON_NUMERIC_CHECK, JSON_HEX_APOS)) . "'" . ')';
//var_dump($insert);
        mysqli_query($this->conn, $insert);
//        echo "here!!!!!!!";
        $error = mysqli_error($this->conn);
        if ($error !== '') {
            Entry4UIError(7015, 'Failed to write Table Audit : ' . $error, 400, '');
        }
    }

    public function addToRow(&$row) {
        $audits = $this->getAudits();
        if (sizeof($audits) === 0) {
            $row['audits'] = array();
            return;
        }

        $userids = array();
        foreach ($audits as $audit) {
            $userids [$audit['userid']] = $audit['userid'];
        }

        $userSql = 'Select id, display_name
                    from ' . E4S_TABLE_USERS . ' where id in (' . implode(',', $userids) . ')';
//        echo "\n" . $userSql . "\n";
        $res = e4s_queryNoLog($userSql);
        $userRows = $res->fetch_all(MYSQLI_ASSOC);
        $usersArr = array();
        foreach ($userRows as $userRow) {
            $usersArr[$userRow['id']] = $userRow['display_name'];
        }
        $returnAudits = array();
        foreach ($audits as $audit) {
            $audit['created'] = e4s_sql_to_iso($audit['created']);
            $audit['userName'] = $usersArr[$audit['userid']];
            $returnAudits[] = $audit;
        }
        $row['audits'] = $returnAudits;
    }

    public function getAudits() {
        $sql = 'select *
                from ' . $this->tableAudit . '
                where compid = ' . $this->compid . '
                order by created desc
                limit 10';

        $results = mysqli_query($this->conn, $sql);
        if ($results->num_rows === 0) {
            return array();
        }
        $error = mysqli_error($this->conn);

        if ($error !== '') {
            Entry4UIError(7010, 'Failed to read Audits for ' . $this->compid . ' : ' . $error, 400, '');
        }
        return $results->fetch_all(MYSQLI_ASSOC);
    }
}