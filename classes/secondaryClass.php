<?php
include_once E4S_FULL_PATH . 'classes/secondaryRefClass.php';

class secondaryDefClass {
    public $model = null;

    public function __construct($model) {
        $this->model = $model;
        return $this->process();
    }

    public function process() {
        $exit = TRUE;
        if (isset($this->model->exit)) {
            $exit = $this->model->exit;
        }
        $process = $this->model->process;
        if ($process === '') {
            Entry4UIError(9305, 'No action passed to Secondary CRUD', 200, '');
        }
        switch ($process) {
            case E4S_CRUD_LIST:
            case E4S_CRUD_LIST . E4S_SECUST_LIST:
                return $this->listDefs($exit);
                break;
            case E4S_CRUD_CREATE:
                $this->create($exit);
                break;
            case E4S_CRUD_READ:
                $this->read($exit);
                break;
            case E4S_CRUD_UPDATE:
                $this->update($exit);
                break;
            case E4S_CRUD_DELETE:
                $this->delete($exit);
                break;
        }
    }

    public function listDefs($exit = TRUE) {
        $model = $this->model;
        $objtype = $model->objType;
        $objid = $model->objId;
        $pagesize = $model->pageInfo->pagesize;
        $page = $model->pageInfo->page;

        $usePaging = FALSE;
        if (isset($pagesize) and isset($page)) {
            $usePaging = TRUE;
        }

        $sql = 'select *
                from ' . E4S_TABLE_SECONDARYREFS . ' sr
                where compid = ' . $model->compId . '
                and   objid = ' . $objid . "
                and   objtype = '" . $objtype . "'";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            $retArr = array();
            if ($exit) {
                Entry4UISuccess('
                   "data":' . json_encode($retArr, JSON_NUMERIC_CHECK));
            }
            return $retArr;
        }

        $row = $result->fetch_assoc();
        $sql = 'select sd.*
                from ' . E4S_TABLE_SECONDARYDEFS . ' sd,
                     ' . E4S_TABLE_SECONDARYREFS . " sr
                where sd.refid = sr.id
                and   sr.objkey like '" . $row['objkey'] . "%'";

        if ($model->compId === 0) {
            $sql .= " and sd.peracc = '' ";
        }

        if ($usePaging and $pagesize !== 0) {
            $sql .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
        }

        $result = e4s_queryNoLog($sql);
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $fullObjs = array();
        foreach ($rows as $row) {
            $obj = $this->getObjFromRow($row);
            if (isset($obj->prod)) {
                if (isset($obj->prod->name)) {
                    $fullObjs[] = $obj;
                }
            }
        }

        if ($exit) {
            Entry4UISuccess('
                "data":' . json_encode($fullObjs, JSON_NUMERIC_CHECK));
        }
        return $fullObjs;
    }

    public function getObjFromRow($row) {

        $obj = new stdClass();
        $obj->id = (int)$row['id'];
        if ($obj->id === $this->model->refObj->id) {
            $obj->refObj = $this->model->refObj;
        } else {
            $obj->refObj = $this->getObjRef((int)$row['refid']);
        }

        $obj->prod = $this->model->prod;
        $prodid = (int)$row['prodid'];
        $dataReq = FALSE;
        if ($row['dataReq'] === '1') {
            $dataReq = TRUE;
        }

        if ($prodid !== $obj->id and $prodid !== 0) {
            $this->model->prod->id = $prodid;
            $prodObj = new e4sProduct($obj->prod, $this->model->compId);
            $obj->prod = $prodObj->read();

            if (!is_null($obj->prod)) {
                if (isset($obj->prod->ticket)) {
                    $obj->prod->ticket->dataReq = $dataReq;
                }
            }
//$this->_debug("obj->prod", $obj->prod, true);
        }

        $obj->perAcc = $row['peracc'];
        $obj->maxAllowed = (int)$row['maxAllowed'];

        return $obj;
    }

    public function getObjRef($id = 0) {
        $model = $this->model->refObj;

        if ($id !== 0) {
            $model->id = $id;
        }
        $refObj = new secondaryRefClass($model);
        return $refObj->read(TRUE);
    }

    public function read($exit = FALSE) {
        $obj = $this->getSecondaryObj('id', $this->model->id, TRUE);
        if ($exit) {
            Entry4UISuccess('
                "data":' . json_encode($obj, JSON_NUMERIC_CHECK));
        }
        return $obj;
    }

    public function getSecondaryObj($key, $value, $mustExist, $delimeter = '') {
        $row = $this->getRowFromDB($key, $value, $mustExist, E4S_TABLE_SECONDARYDEFS, $delimeter);
        if (is_null($row)) {
            return null;
        }

        return $this->getObjFromRow($row);
    }

    public function getRowFromDB($key, $value, $mustExist, $tableName, $delimeter = '') {
        return e4s_getCRUDRow($tableName, $key, $value, $mustExist, $delimeter);
    }

    public function create($exit) {

        $model = $this->model;

        if ($model->id !== 0) {
            Entry4UIError(9306, 'Create a secondary object with a non zero id', 200, '');
        }
        if ($model->prod->id !== 0) {
            $this->_error(9315, 'Create a secondary object with a non zero product id', TRUE);
        }

        $model->refObj = $this->getObjRef();
        $model->prod = $this->createProduct($model);
        $dataReq = 'false';
        if ($model->prod->ticket->dataReq) {
            $dataReq = 'true';
        }
        $sql = 'Insert into ' . E4S_TABLE_SECONDARYDEFS . ' ( prodid, maxallowed, refid, peracc, datareq )
        values(
            ' . $model->prod->id . ',
            ' . $model->maxAllowed . ',
            ' . $model->refObj->id . ",
            '" . $model->perAcc . "',
            " . $dataReq . ')';

        e4s_queryNoLog($sql);

        $obj = $this->getSecondaryObj('id', e4s_getLastID(), TRUE, $delimeter = '');

        if ($exit) {
            Entry4UISuccess('
                "data":' . json_encode($obj, JSON_NUMERIC_CHECK));
        }
    }

    private function _error($err, $str, $exit = FALSE) {
        if ($exit) {
            Entry4UIError($err, $str, 200, '');
        }
        return FALSE;
    }

    public function createProduct($givenModel = null) {
        $model = $this->model;
//        $this->_debug("Create prod 2Class", $model,true);
        if (!is_null($givenModel)) {
            $model = $givenModel;
        }
        if ($model->prod->id !== 0) {
            $this->_error(7010, 'Can not create product : ' . $model->prod->id, TRUE);
        }

        $prodModel = $this->getProductModel($model);

        $prodObj = new e4sProduct($prodModel, $model->compId);
        $model->prod = $prodObj->createProduct();
        return $model->prod;
    }

    public function getProductModel($passedDefModel = null) {
        $defModel = $this->model;

        if (!is_null($passedDefModel)) {
            $defModel = $passedDefModel;
        }

        $model = new stdClass();
        $model->perAcc = $defModel->perAcc;
        $model->id = $defModel->prod->id;
        $model->name = $defModel->prod->name;
        $model->description = $defModel->prod->description;
        $model->image = $defModel->prod->image;
        $model->stockQty = $defModel->prod->stockQty;
        if (isset($defModel->prod->attributes)) {
            $model->attributes = $defModel->prod->attributes;
        }

        if (isset($defModel->prod->attributeValues)) {
            $model->attributeValues = $defModel->prod->attributeValues;
        }
        $model->virtual = FALSE;
        if (isset($defModel->prod->virtual)) {
            $model->virtual = $defModel->prod->virtual;
        }
        $model->download = FALSE;
        if (isset($defModel->prod->download)) {
            $model->download = $defModel->prod->download;
            if ($model->download) {
                $compObj = e4s_GetCompObj($defModel->compId);
                $contentObj = new stdClass();
                $contentObj->compid = $compObj->getID();
                $contentObj->compname = $compObj->getName();
                $contentObj->ticket = $model->name;
                $model->description = e4s_getDataAsType($contentObj, E4S_OPTIONS_STRING);
            }
        }
        if (isset($defModel->prod->ticket)) {
            $model->ticket = $defModel->prod->ticket;
        }
        $price = new stdClass();
        $price->price = $defModel->prod->price->price;
        $price->salePrice = $defModel->prod->price->salePrice;
        $price->saleEndDate = $defModel->prod->price->saleEndDate;

        $model->price = $price;
        return $model;
    }

    public function update($exit) {
        $model = $this->model;
//echo "update 1\n";
//var_dump($model);
//exit();
        if ($model->id === 0) {
            $this->_error(7102, 'Update a secondary object with a zero id', TRUE);
        }
        if ($model->prod->id === 0) {
            $this->_error(7105, 'Update a secondary object with a zero product id', TRUE);
        }
        $row = $this->getRowFromDB('id', $model->id, TRUE, E4S_TABLE_SECONDARYDEFS);
//        var_dump($row);
//        echo "\n";
//        var_dump($model);
//        exit();
        if (is_null($row)) {
            $this->_error(7110, 'Failed to read the Secondary row', TRUE);
        }
        if ((int)$row['prodid'] === 0) {
            $this->_error(7115, 'Product not retrieved from DB', TRUE);
        }

        $dataReq = 'false';

        if (isset($model->prod->ticket)) {
            if (isset($model->prod->ticket->dataReq)) {
                if ($model->prod->ticket->dataReq) {
                    $dataReq = 'true';
                }
            }
        }
        $sql = 'update ' . E4S_TABLE_SECONDARYDEFS . ' 
                set dataReq = ' . $dataReq . ",
                    maxAllowed = $model->maxAllowed,
                    peracc = '" . $model->perAcc . "'
                where id = " . $row['id'];
        e4s_queryNoLog($sql);
//        $obj = $this->getObjFromRow($row);

//$this->_debug("Update 3",$model, true);

        $prodModel = $this->getProductModel($model);
        $prodObj = new e4sProduct($prodModel, $model->compId);
        $prodObj->update(TRUE);

// update any def specific fields
//$this->_debug("Update 99",$prodObj);
    }

    public function delete($exit = FALSE) {
        $this->model = $this->read(FALSE);

        if ($this->model->id === 0 or $this->model->prod->id === 0) {
            if ($exit) {
                $this->_error(7000, 'Unable to get product/def to delete', TRUE);
            }
        }
        $prodObj = new e4sProduct($this->model->prod);
        $result = $prodObj->delete(FALSE);
        if (!$result) {
            if ($exit) {
                $this->_error(7010, 'Unable to delete product', TRUE);
            }
            return FALSE;
        } else {
//            ok to delete 2nd Def
            $result = $this->_deleteDef();
            if (!$result) {
                $this->_error(7020, 'Failed to delete the Definition', TRUE);
            }
        }

        if ($exit) {
            Entry4UISuccess('');
        }
        return TRUE;
    }

    private function _deleteDef() {
        $sql = 'delete from ' . E4S_TABLE_SECONDARYDEFS . ' 
                where id = ' . $this->model->id;
        return e4s_queryNoLog($sql);
    }

    public static function getProductMaxQty($id, &$perAcc) {
        $sql = 'select maxAllowed, peracc
                from ' . E4S_TABLE_SECONDARYDEFS . '
                where prodid = ' . $id;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            return null;
        }
        $row = $result->fetch_assoc();
        $perAcc = $row['peracc'];
        return (int)$row['maxAllowed'];
    }
//  parameter = Secondary model
//  returns prod model

    public function getForProd() {
        return $this->getRowFromDB('prodid', $this->model->prodid, TRUE, E4S_TABLE_SECONDARYDEFS, '');
    }

    private function _debug($msg, $obj, $exit = TRUE) {

        echo $msg . "\n";
        var_dump($obj);
        if ($exit) {
            exit();
        }
    }

}