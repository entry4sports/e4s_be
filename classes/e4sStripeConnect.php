<?php
include_once E4S_FULL_PATH . 'e4s_constants.php';

class e4sStripeConnect {
    public $userId;
    public $stati;

    public function __construct($userId) {
        $this->userId = $userId;
        $this->stati = null;
    }

    public function approveUser() {
        if ($this->isConnected() === FALSE) {
            Entry4UIError(8451, 'User is not marked as Connected.');
        }
        if ($this->isApproved() !== FALSE) {
            Entry4UIError(8452, 'User is already marked as Approved.');
        }
        $this->_setStatus(E4S_USER_META_APPROVED_STRIPE);
    }

    public function isConnected() {
        return $this->_getCurrentStatus(E4S_USER_META_STRIPE);
    }

    private function _getCurrentStatus($status) {
        $statusArr = $this->_getStatus();
        if (!empty($statusArr)) {
            foreach ($statusArr as $statusObj) {
                if ($statusObj->meta_key === $status) {
                    return $statusObj->meta_value;
                }
            }
        }
        return FALSE;
    }

    private function _getStatus() {
        if (!is_null($this->stati)) {
            return $this->stati;
        }
        $sql = 'select meta_key
                      ,meta_value
            from ' . E4S_TABLE_USERMETA . '
            where user_id = ' . $this->userId . "
            and   meta_key in ('" . E4S_USER_META_STRIPE . "','" . E4S_USER_META_APPROVED_STRIPE . "')";

        $result = e4s_queryNoLog($sql);
        $returnArr = array();
        while ($obj = $result->fetch_object()) {
            $returnArr[] = $obj;
        }
        $this->stati = $returnArr;
        return $returnArr;
    }

    public function isApproved() {
        return $this->_getCurrentStatus(E4S_USER_META_APPROVED_STRIPE);
    }

    private function _setStatus($status) {
        $sql = 'insert into ' . E4S_TABLE_USERMETA . ' ( user_id, meta_key, meta_value )
            values (
                ' . $this->userId . ",
                '" . $status . "',
                CURRENT_TIMESTAMP()
            )";
        e4s_queryNoLog($sql);
    }

    /**
     * @param $status
     * @return void
     */
    public function checkCurrentStatus($status) {
        // status = connect or discconnect
        if ($status !== E4S_YITH_CONNECTED) {
            $this->clearStatus();
        } elseif ($this->isConnected() === FALSE) {
            // Newly connected. Update and send email
            $this->_setStatus(E4S_USER_META_STRIPE);
            $this->_informNewConnection();
        }
        $this->outputStatus();
    }

    public function testEmail() {
        $this->_informNewConnection();
    }

    public function clearStatus() {
        $sql = 'delete from ' . E4S_TABLE_USERMETA . '
                where user_id = ' . $this->userId . "
                and   meta_key in ('" . E4S_USER_META_STRIPE . "','" . E4S_USER_META_APPROVED_STRIPE . "')";
        e4s_queryNoLog($sql);
        $this->stati = null;
    }

    private function _informNewConnection() {
        $body = 'There is a new Stripe Connection that needs checking on the E4S System.<br><br>';
        $body .= 'Please click the option below once you have manually confirmed everything is ok. Clicking here will allow them to create competitions.<br>';
        $body .= 'User Id : ' . $this->userId . '<br>';
        $body .= 'Name : ' . e4s_getUserName() . '<br>';
        $body .= 'Email : ' . e4s_getUserEmail() . '<br>';
        $body .= 'Organisation(s) : ';
        foreach (e4s_getUserOrgs() as $org) {
            $body .= $org->id . ' / ' . $org->name . '<br>';
        }
        $body .= '<br><br>';
        $body .= 'Please ensure you are logged in by using this link<br>';
        $body .= '<a href="https://"' . E4S_CURRENT_DOMAIN . '"/#/login">Login to Online Entries System</a><br><br>';
        $body .= 'Once logged in.<br>';
        $body .= '<a href="' . $this->_getApproveLink() . '">Confirm Stripe Details</a><br>';
        $body .= '<br></br>Thanks<br>';
        $body .= 'E4S Support Team';
        $config = e4s_getConfigObj();
        $sendTo = $config->getStripeApprovers();
        e4s_mail($sendTo, 'New Stripe Account for Approval', $body);
    }

    private function _getApproveLink() {
        $link = 'https://' . E4S_CURRENT_DOMAIN . '/wp-json/e4s/v5/stripe/approve/' . $this->userId . '/' . $this->getCode();
        return $link;
    }

    public function getCode($userId = null) {
        if (is_null($userId)) {
            $userId = $this->userId;
        }
        return md5(E4S_CURRENT_DOMAIN . $userId);
    }

    public function outputStatus() {
        echo 'E4S Status : ' . $this->getStatus();
    }

    public function getStatus() {
        $txt = '';
        $this->_getStatus();
        if (is_null($this->stati)) {
            $txt .= 'Not Connected';
        } else {
            $approved = $this->isApproved();
            $connected = $this->isConnected();
            if ($approved !== FALSE) {
                $approved = date_create($approved);
                $txt .= 'Approved ' . date_format($approved, 'd/m/Y H:i');
            } elseif ($connected !== FALSE) {
                $connected = date_create($connected);
                $txt .= 'Connected ' . date_format($connected, 'd/m/Y H:i') . ', Awaiting Approval';
            } else {
                $txt .= 'Awaiting Approval';
            }
        }

        return $txt;
    }

    public function testInform() {
        $this->_informNewConnection();
    }

    public function updateExisting() {
        if (!isE4SUser()) {
            Entry4UIError(7899, 'Unable to initialise Stripe');
        }
        $sql = 'select user_id
                 from ' . E4S_TABLE_USERMETA . "
                 where meta_key = 'stripe_user_id'";
        $result = e4s_queryNoLog($sql);
        while ($obj = $result->fetch_object()) {
            $this->userId = (int)$obj->user_id;
            $this->stati = null;
            $this->_getStatus();
            $connected = FALSE;
            $approved = FALSE;
            $sql = 'insert into ' . E4S_TABLE_USERMETA . '(user_id, meta_key, meta_value) values ';
            foreach ($this->stati as $statusObj) {
                if ($statusObj->meta_key === E4S_USER_META_STRIPE) {
                    $connected = TRUE;
                }
                if ($statusObj->meta_key === E4S_USER_META_APPROVED_STRIPE) {
                    $approved = TRUE;
                }
            }
            if (!$connected or !$approved) {
                $sep = '';
//                 at least one missing and needs creating
                if (!$connected) {
                    $sql .= '(' . $this->userId . ",'" . E4S_USER_META_STRIPE . "','2020-01-01 12:00:00')";
                    $sep = ',';
                }
                if (!$approved) {
                    $sql .= $sep . '(' . $this->userId . ",'" . E4S_USER_META_APPROVED_STRIPE . "','2020-01-01 12:00:00')";
                }
                e4s_queryNoLog($sql);
            }
        }

    }
}