<?php
define('E4S_SEAT_STATUS_AVAILABLE', -1);
define('E4S_SEAT_STATUS_ORDERED', E4S_ENTRY_NOT_PAID);
define('E4S_SEAT_STATUS_PAID', E4S_ENTRY_PAID);
define('E4S_SEAT_MAX_ALLOWED', 1);

class e4sSeatingClass {
    public $obj; // e4s Object from TicketClass
    public $compObj;
    public $attribs;

    public function __construct($obj) {
        $this->obj = $obj;
        if (isset($obj->compId)) {
            $this->compObj = e4s_GetCompObj($obj->compId);
        }
    }

    public static function getAllocationForUser() {
        $retVal = array();
        $sql = 'select *
                from ' . E4S_TABLE_STADIUMSEATING . '
                where userid = ' . e4s_getUserID() . '
                and   status = ' . E4S_SEAT_STATUS_ORDERED;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return $retVal;
        }

        while ($obj = $result->fetch_object()) {
            $retVal[] = $obj;
        }
        return $retVal;
    }

    public static function resetSeatsForUser() {
        $sql = 'update ' . E4S_TABLE_STADIUMSEATING . '
                set status = ' . E4S_SEAT_STATUS_AVAILABLE . ',
                    userid = 0
                where userid = ' . e4s_getUserID() . ' 
                and status = ' . E4S_SEAT_STATUS_ORDERED;
        e4s_queryNoLog($sql);
    }

    public static function getSeatingInfoForTicket($ticketId) {
        $retVal = new stdClass();
        $retVal->id = 0;
        $retVal->stadium = '';
        $retVal->stand = '';
        $retVal->block = '';
        $retVal->row = '';
        $retVal->date = '';
        $retVal->seatNo = 0;
        $sql = 'select t.seatNo seatNo,
                       ss.stadium stadium,
                       ss.stand stand,
                       ss.block block,
                       ss.row row,
                       ss.date date      
                from ' . E4S_TABLE_TICKET . ' t,
                     ' . E4S_TABLE_STADIUMSEATING . ' ss
                where t.id = ' . $ticketId . '
                  and t.stadiumSeatId = ss.id';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            return $retVal;
        }
        $obj = $result->fetch_object();
        $retVal->id = $ticketId;
        $retVal->stadium = $obj->stadium;
        $retVal->stand = $obj->stand;
        $retVal->block = $obj->block;
        $retVal->row = $obj->row;
        $retVal->date = $obj->date;
        $retVal->seatNo = $obj->seatNo;
        return $retVal;
    }

    public function markSeatsPaidForId($id) {
        $sql = 'update ' . E4S_TABLE_STADIUMSEATING . '
                set status = ' . E4S_SEAT_STATUS_PAID . '
                where id = ' . $id;
        e4s_queryNoLog($sql);
    }

    public function checkSeatAvailablity() {
        $this->_getAttribsAsAssocArray();
        $result = $this->_getSeatsAvailable();
        $availableBlocks = 'No other blocks available.';
        if ($result->num_rows < $this->obj->qtyRequired) {
            $date = $this->attribs[E4S_DATE_KEY];
            if (isE4SUser()) {
                $useDate = $date->format('Y-m-d');
                $sql = 'select distinct(block) block
                        from ' . E4S_TABLE_STADIUMSEATING . '
                        where status = ' . E4S_SEAT_STATUS_AVAILABLE . "
                        and date = '" . $useDate . "'
                        order by block";

                $dateResult = e4s_queryNoLog($sql);
                if ($dateResult->num_rows > 0) {
                    $blocks = array();
                    while ($obj = $dateResult->fetch_object()) {
                        $blocks[] = $obj->block;
                    }
                    $availableBlocks = 'Seats available in blocks : ' . implode(',', $blocks);
                }
            }
            $date = $date->format('l, jS F Y');
            Entry4UIError(1000, 'Sorry. There are no seats in ' . $this->attribs[E4S_BLOCK_KEY] . ' on ' . $date . ' available. Sold out. ' . $availableBlocks);
        }

        $this->_markRequiredSeatsOnOrder($result);
    }

    private function _getAttribsAsAssocArray() {

        $arr = array();
        foreach ($this->obj->attributeValues as $key => $attrib) {
            if ($key !== E4S_ATTRIB_ATHLETE) {
                $name = strtoupper($attrib['name']);
                $name = str_replace(' ', '-', $name);
                $value = $attrib['value'];
                if ($name === E4S_DATE_KEY) {
                    $value = $this->_getDateObjFromAttrib($value);
                }
                if ($name === E4S_SEAT_KEY) {
                    $value = (int)$value;
                }
                $arr[$name] = $value;
            }
        }
        $this->attribs = $arr;
    }

    private function _getDateObjFromAttrib($attribDate) {
        if (strpos($attribDate, '2021') === FALSE) {
            $attribDate .= ' 2021';
        }

        $dateObj = DateTime::createFromFormat('jS M Y', $attribDate);
//        $date = $dateObj->format('Y-m-d');
        return $dateObj;
    }

    private function _getSeatsAvailable() {
        // check they have not ordered/ing more than alowed
        $sql = '
            select count(*) onOrder
                from ' . E4S_TABLE_STADIUMSEATING . "
                where date = '" . $this->_getSqlDateFromDateObj($this->attribs[E4S_DATE_KEY]) . "'
                and userid = " . e4s_getUserID() . '
                and status != ' . E4S_SEAT_STATUS_AVAILABLE;
        $result = e4s_queryNoLog($sql);
        $obj = $result->fetch_object();
//        if ((int)$obj->onOrder + $this->obj->qtyRequired > E4S_SEAT_MAX_ALLOWED ){
        if ((int)$obj->onOrder + $this->obj->qtyRequired > E4S_SEAT_MAX_ALLOWED) {
            $date = $this->attribs[E4S_DATE_KEY];
            $date = $date->format('l, jS F Y');
            Entry4UIError(6000, 'Sorry you are only allowed a maximum of ' . E4S_SEAT_MAX_ALLOWED . ' seat bookings for ' . $date);
        }
        $sql = '
            select *
                from ' . E4S_TABLE_STADIUMSEATING . "
                where date = '" . $this->_getSqlDateFromDateObj($this->attribs[E4S_DATE_KEY]) . "'
                and block = '" . $this->attribs[E4S_BLOCK_KEY] . "'
                and status = " . E4S_SEAT_STATUS_AVAILABLE . '
                and max >= ' . $this->attribs[E4S_SEAT_KEY] . '
                order by row, seat
                limit ' . $this->obj->qtyRequired;

        $result = e4s_queryNoLog($sql);
        return $result;
    }

    private function _getSqlDateFromDateObj($dateObj) {
        return $dateObj->format('Y-m-d');
    }

    private function _markRequiredSeatsOnOrder($result) {
        $ids = array();
        while ($obj = $result->fetch_object()) {
            $ids[] = $obj->id;
        }
        if (sizeof($ids) === 0) {
            Entry4UIError(8050, 'Incorrect Seats read');
        }
        $sql = 'update ' . E4S_TABLE_STADIUMSEATING . ' 
                set userid = ' . e4s_getUserID() . ',
                    status = ' . E4S_SEAT_STATUS_ORDERED . ' 
                where id in (' . implode(',', $ids) . ')';
        e4s_queryNoLog($sql);
    }

    public function deleteSeatAllocation($ticketGuids) {

    }

    public function getSeatAllocation($wcItem, $exist = FALSE) {
        $retVal = new stdClass();
        $retVal->stadiumSeatId = 0;
        $retVal->seatNo = 0;

        // check but should be pointless
        if (!$this->_useSeatAllocation($wcItem)) {
            return $retVal;
        }

        $stadiumSeats = $this->getAllocatedStadiumTickets($wcItem, $exist);

        if (sizeof($stadiumSeats) < 1) {
            Entry4UIError(8023, 'Unable to find allocated Seats');
        }
        foreach ($stadiumSeats as $stadiumId => $stadiumSeat) {
            // get the first available
            $retVal->stadiumSeatId = $stadiumId;
            $retVal->seatNo = $stadiumSeat;
            break;
        }

        return $retVal;
    }

    private function _useSeatAllocation($wcItem) {

        // is a stadium defined in competition options
        $stadium = $this->compObj->getStadium();
        if ($stadium === '') {
            return FALSE;
        }
        // are any of the attributes a stand
        $attribs = e4sTicket::getAttribsFromWcItem($wcItem);
        foreach ($attribs as $key => $value) {
            $uKey = str_replace(' ', '-', strtoupper($key));
            if ($uKey === E4S_SEAT_KEY) {
                return TRUE;
            }
        }
//        if ( isE4SUser() ){
//            var_dump("_useSeatAllocation d");
//            var_dump($attribs);
//        }
        // seating not used
        return FALSE;
    }

    public function getAllocatedStadiumTickets($wcItem, $exist = FALSE) {
        $userId = e4s_getUserID();
        $attribs = e4sTicket::getAttribsFromWcItem($wcItem);

        $date = '2021-07-09';
        $dateAttrib = 'date';
        if (array_key_exists('Date', $attribs)) {
            $dateAttrib = 'Date';
        }
        if (array_key_exists('DATE', $attribs)) {
            $dateAttrib = 'DATE';
        }
        if ($attribs[$dateAttrib] === '10th July 2021') {
            $date = '2021-07-10';
        }
        if ($attribs[$dateAttrib] === '11th July 2021') {
            $date = '2021-07-11';
        }

        $sql = 'select id id,
                       seat seat
                from ' . E4S_TABLE_STADIUMSEATING . '
                where userid = ' . $userId . "
                and   date = '" . $date . "'
                and status != " . E4S_SEAT_STATUS_AVAILABLE;

        $result = e4s_queryNoLog($sql);
        $allocatedSeats = array();
        while ($obj = $result->fetch_object()) {
            $allocatedSeats[$obj->id] = (int)$obj->seat;
        }
        return $allocatedSeats;
    }

    public function getSeatingForTicket($ticketId) {
        $sql = 'select id id,
                       stadiumSeatId stadiumSeatId,
                       seatNo seatNo
                from ' . E4S_TABLE_TICKET . '
                where id = ' . $ticketId;

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return null;
        }
        return $result->fetch_object();
    }

    public function getNextStadiumObj($wcItem) {

        $stadium = $this->compObj->getStadium();
        $stand = '';
        $block = '';
        $row = '';
        $date = '2021-07-09';
        $seatsRequired = 2;
        $attribs = e4sTicket::getAttribsFromWcItem($wcItem);

        foreach ($attribs as $key => $value) {
            $uKey = strtoupper($key);
            if ($uKey === E4S_STADIUM_KEY or $uKey === E4S_STAND_KEY) {
                $stand = $value;
            }
            if ($uKey === E4S_BLOCK_KEY) {
                $block = $value;
            }
            if ($uKey === E4S_ROW_KEY) {
                $row = $value;
            }
            if ($uKey === E4S_DATE_KEY) {
                $date = $this->_getSqlDateFromAttrib($value);

            }
            if ($uKey === E4S_SEAT_KEY) {
                $seatsRequired = (int)$value;
            }
        }
        $sql = 'select id,
                       seat startSeat
                 from ' . E4S_TABLE_STADIUMSEATING . "
                 where stadium = '" . $stadium . "'";
        if ($stand !== '') {
            $sql .= " and stand = '" . $stand . "'";
        }
        if ($block !== '') {
            $sql .= " and block = '" . $block . "'";
        }
        if ($row !== '') {
            $sql .= " and row = '" . $row . "'";
        }
        if ($date !== '') {
            $sql .= " and date = '" . $date . "'";
        }
        $sql .= ' and max >= ' . $seatsRequired;
        $sql .= ' and status = ' . E4S_SEAT_STATUS_AVAILABLE;
        $sql .= ' order by stand, date, block, row, seat ';

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            return null;
        }
        return $result->fetch_object();
    }

    // defunct function

    private function _getSqlDateFromAttrib($attribDate) {
        $obj = $this->_getDateObjFromAttrib($attribDate);
        return $this->_getSqlDateFromDateObj($obj);
    }
}