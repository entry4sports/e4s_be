<?php
const E4S_KEY_SEP = '.';
class pof10V2Class {
	var $perfAthlete;
	var $athlete;
	var $rankings;
	var $performances;
	var $events;
	public function __construct($urn, $athleteId = 0) {
		$this->perfAthlete = new perfAthlete();
		$this->athlete = $this->perfAthlete->getBlankAthlete();
		$this->athlete->urn = $urn;
		$this->athlete->id = $athleteId;
		$this->_getE4SAthlete();
		$this->rankings = array();
		$this->performances = array();
	}
	public function getAthleteId() {
		return $this->athlete->id;
	}
	private function _shouldAthleteBeUpdated() {
		return self::shouldAthleteBeUpdated($this->athlete->lastCheckedPB);
	}
	static function shouldAthleteBeUpdated($lastCheckedPB) {
		$update = false;

		if ( is_null($lastCheckedPB)){
			$update = true;
		}else {
			$lastCheckedPB = strtotime( $lastCheckedPB );
			$today         = strtotime( date( 'Y-m-d' ) );
			if ( $lastCheckedPB < $today ) {
				$update = true;
			}
		}
		return $update;
	}
	public function updateDb($force = false) {
		if ($force or $this->_shouldAthleteBeUpdated() ) {
			$pof10Content = $this->_getPof10Page();
			$this->_getE4SDataFromContent( $pof10Content );
			$this->_writeRankings();
			$this->_writePerformances();
			$this->_updateAthleteLastCheckedPB();
			$this->_updateTrackedEntries();
		}
	}
	private function _updateTrackedEntries(){
		$perfAthleteObj = new perfAthlete();
		$perfAthleteObj->updateTrackedEntries($this->athlete->id);
	}
	private function _updateAthleteLastCheckedPB(){
		$sql = "update " . E4S_TABLE_ATHLETE . "
		        set lastCheckedPB = '" . date(E4S_MYSQL_DATE) . "',
		            curAgeGroup = '" . $this->athlete->ageGroup . "'
		        where id = " . $this->athlete->id;
		e4s_queryNoLog( $sql );
	}

	private function _writePerformances(){
		$sql = "select p.*, e.tf, e.eventDefId
		        from " . E4S_TABLE_ATHLETEPERF . " p,
		             " . E4S_TABLE_EVENTS . " e
		        where p.athleteid = " . $this->athlete->id . "
		        and   p.eventid = e.id";
		$result = e4s_queryNoLog($sql);

		$performances = array();
		while ($obj = $result->fetch_object(E4S_ATHLETEPERFOBJ)) {
			// format the datetime to get just the date
			$achieved = date('Y-m-d', strtotime($obj->achieved));

			$key = $obj->eventId . E4S_KEY_SEP . $achieved . E4S_KEY_SEP . $obj->perf;
			$add = true;
			if ( array_key_exists($key, $performances) ) {
				$perf = $performances[$key];
				if ( $perf->perf === $obj->perf ) {
					$add = false;
				}
				if ( $perf->perf < $obj->perf and $obj->tf === E4S_EVENT_FIELD) {
					$add = false;
				}
				if ( $perf->perf > $obj->perf and $obj->tf === E4S_EVENT_TRACK) {
					$add = false;
				}
			}
			if ($add) {
				$performances[ $key ] = $obj;
			}
		}
		$sql = '';
		$sep = '';
		foreach($this->performances as $performance){
			$event = $this->_getE4SEvent( $performance->event );
			if ( is_null($event) ) {
				continue;
			}
			$performance->eventId = $event['id'];
//			if ( $event['tf'] === E4S_EVENT_FIELD ) {
//				get implement info e.g SP4K = 4K
				$performance->info = str_replace(strtoupper($event['pof10']),'',strtoupper($performance->event));
//			}
			$date = date('Y-m-d', strtotime($performance->date));
			$ageGroup = $performance->ageGroup;
			$eventId = $event['id'];
			$key = $eventId . E4S_KEY_SEP . $date . E4S_KEY_SEP . $performance->perf;
			if (!array_key_exists($key, $performances) ) {
				if ( $sql === '' ) {
					$sql = "insert into " . E4S_TABLE_ATHLETEPERF . "
				        (athleteid, eventid, perf, ageGroup, achieved, wind, info)
				        values ";
				}
				$sql .= $sep . "(" . $this->athlete->id . ", " . $eventId . ", '" . $performance->perf . "', '" . $ageGroup . "','" . $date . "','" . $performance->wind . "','" . $performance->info . "')";
				$sep = ',';
			}
		}
		if ( $sql !== '' ) {
			e4s_queryNoLog( $sql );
		}
	}
	private function _writeRankings(){
		$rankYear = (int)date('Y') - 1;
		$currentRankings = array();
		$sql = "select *
		        from " . E4S_TABLE_ATHLETERANK . "
		        where athleteid = " . $this->athlete->id . "
		        and  year >= " . $rankYear;
		$result = e4s_queryNoLog($sql);
		while ($obj = $result->fetch_object(E4S_ATHLETERANK_OBJ)) {
			$key = $obj->eventId . $obj->ageGroup . $obj->year;
			$currentRankings[$key] = $obj;
		}
		$rankings = $this->rankings;

		foreach ($rankings as $ranking) {
			if ( $ranking->year < $rankYear ) {
				continue;
			}

			$event = $this->_getE4SEvent( $ranking->event );
			if ( ! is_null( $event ) ) {
				$eventId = $event['id'];
				$info    = str_replace( $ranking->ageGroup, '', $ranking->event );
				$info    = str_replace( strtoupper( $event['pof10'] ), '', $info );
				$info    = str_replace( 'W', '', $info );
				$info    = str_replace( 'M', '', $info );
				$key     = $eventId . $ranking->ageGroup . $ranking->year;
				if ( array_key_exists( $key, $currentRankings ) ) {
					$currentRanking = $currentRankings[ $key ];
					if ( $currentRanking->rank !== $ranking->rank ) {
						$sql = "update " . E4S_TABLE_ATHLETERANK . "
					        set rank = " . $ranking->rank . ",
					            info = '" . $info . "'
					        where id = " . $currentRanking->id;
						e4s_queryNoLog( $sql );
					}
				} else {
					$sql = "insert into " . E4S_TABLE_ATHLETERANK . "
				        (athleteid, eventid, rank, ageGroup, year, info)
				        values
				        (" . $this->athlete->id . ", " . $eventId . ", " . $ranking->rank . ", '" . $ranking->ageGroup . "', " . $ranking->year . ",'" . $info . "')";
					e4s_queryNoLog( $sql );
				}
			}
		}
	}
	private function _getE4SAthlete(){
		$athlete = null;
		if ( $this->athlete->id !== 0 ) {
			$athleteObj = new athleteClass();
			$athlete = $athleteObj->getAthleteFromCache($this->athlete->id);
		}

		if ( is_null($athlete) ) {
			$sql    = "select *
		        from " . E4S_TABLE_ATHLETE . "
		        where aocode = '" . E4S_AOCODE_EA . "'
		        and   urn = '" . $this->athlete->urn . "'";
			$result = e4s_queryNoLog( $sql );
			if ( $result->num_rows !== 1 ) {
				Entry4UIError( 9817, 'Athlete not found in E4S database. URN : ' . $this->athlete->urn );
			}
			$athlete = $result->fetch_object( E4S_ATHLETE_OBJ );
		}
		$this->athlete->id = $athlete->id;
		$this->athlete->name = $athlete->firstName . ' ' . $athlete->surName;
		$this->athlete->gender = $athlete->gender;
		$this->athlete->lastCheckedPB = $athlete->lastCheckedPB;
	}
	private function _getPof10Page() {
		$urn = $this->athlete->urn;
		if ($urn === '') {
			return '';
		}
		$content = file_get_contents('http://www.thepowerof10.info/athletes/profile.aspx?ukaurn=' . $urn);
		$pof10Error = explode('cphBody_lblErrorMessage"><font color="Red"></font></span>', $content);

		if (sizeof($pof10Error) === 0) {
			return '';
		}

		return $content;
	}
	private function _getE4SDataFromContent($pof10Content) : void {
		if (is_null($pof10Content)) {
			return;
		}

		$mainContent = explode('"cphBody_pnlMain"', $pof10Content);
		if (sizeof($mainContent) < 2) {
			return;
		}
		$mainContent = $mainContent[1];
		$mainContent = explode('"footer_pnlPOT"', $mainContent);
		if (sizeof($mainContent) < 2) {
			return;
		}
		$mainContent = $mainContent[0];
		$allContent = explode('<td width="220" valign="top">', $mainContent);
		if (sizeof($allContent) < 2) {
			return;
		}

		$mainContent = explode('cphBody_pnlAbout', $allContent[0]);
		if (sizeof($mainContent) < 2) {
			return;
		}

		$this->_getAthleteInfo($mainContent[0]);

		$mainContent = explode('cphBody_pnlPerformances', $mainContent[1]);
		if (sizeof($mainContent) < 2) {
			return;
		}
		$bestContent = $mainContent[0];
		$this->_getBestPerformances($bestContent);
		$perfContent = explode('cphBody_pnlBlog', $mainContent[1]);
		if (sizeof($mainContent) < 2) {
			return;
		}
		$perfContent = $perfContent[0];
		$this->_getPerformances($perfContent);

		$rightContent = $allContent[1];
		$this->_getRankings($rightContent);
		return;
	}
	private function _getBestPerformances($content) :void{

	}
	private function _getPerformances($content) :void{
		$content = explode('<table', $content);
		if (sizeof($content) < 2) {
			return;
		}
		$content = $content[2];
		$rows = explode('</tr>', $content);
		if (sizeof($rows) < 2) {
			return;
		}
		$perfAgeGroup = '';
		foreach ($rows as $rowNum=>$row){
			$cols = explode('</td>', $row);
			if ( sizeof($cols) < 10 ) {
				if ( strpos($row,'<b>') !== FALSE) {
					$perfTitle = explode( '<b>', $row )[1];
					$perfTitle = explode( '</b>', $perfTitle )[0];
					$arr       = explode( ' ', $perfTitle );
					if ( sizeof( $arr ) > 2 ) {
						$perfAgeGroup = $arr[1];
					}
				}
				continue;
			}
			if ( strpos($cols[11],'Date') !== FALSE ) {
				continue;
			}
			$perf = $this->perfAthlete->getBlankPerf();
			$perf->ageGroup = $perfAgeGroup;
			foreach($cols as $col=>$colData) {
				$col = (int) $col;
				switch ( $col ) {
					case 0:
						$colData = explode( '<td>', $colData );
						if ( sizeof( $colData ) < 2 ) {
							break;
						}
						$perf->event = $colData[1];
						break;
					case 1:
						$colData = str_replace( '<td>', '', $colData );
						$perf->perf = (float)resultsClass::getResultInSeconds( $colData );
						break;
					case 2:
						$colData = str_replace( '<td>', '', $colData );
						$perf->info = $colData;
						break;
					case 3:
						$colData = explode( '">', $colData );
						if ( sizeof( $colData ) < 2 ) {
							break;
						}
						$perf->wind = (float)$colData[1];
						break;
					case 11:
						$colData = explode( '">', $colData )[1];
						$perf->date = $colData;
						break;
				}
			}
			$this->performances[] = $perf;
		}
	}
	private function _getRankings($content) :void{
		$arr = explode('<b>Rank</b>', $content);
		if (sizeof($arr) < 2) {
			return;
		}
		$arr = explode('<tr', $arr[1]);
		if (sizeof($arr) < 2) {
			return;
		}
		foreach ($arr as $rankRow){
			$arr = explode('"_blank">', $rankRow);
			if (sizeof($arr) < 2) {
				continue;
			}
			$rowData = explode('</td>', $arr[1]);
			$ranking = $this->perfAthlete->getBlankRank();
			foreach($rowData as $col=>$colData){

				switch ($col){
					case 0:
						$colData = str_replace('</a>', '', $colData);
						$ranking->event = $colData;
						break;
					case 2:
						$colData = str_replace('<td>', '', $colData);
						$ranking->ageGroup = $colData;
						break;
					case 3:
						$colData = str_replace('<td align="center">', '', $colData);
						$ranking->year = (int)$colData;
						break;
					case 4:
						$colData = str_replace('<td align="right">', '', $colData);
						$ranking->rank = (int)$colData;
						break;
				}
			}
			$this->rankings[] = $ranking;
		}
	}
	private function _getAthleteInfo($pof10Content) {
		if (strpos($pof10Content, 'athleteprofilesubheader') !== FALSE) {
			$this->_getAthleteIdAndName($pof10Content);
		}
		if (strpos($pof10Content, 'cphBody_pnlAthleteDetails') !== FALSE) {
			$this->_getAthleteOtherInfo($pof10Content);
		}
	}
	private function _getAthleteIdAndName($pof10Content):void {
		$arr = explode('athleteprofilesubheader">', $pof10Content);
		if (sizeof($arr) < 2) {
			return;
		}
		$athleteInfo = $arr[1];
		$arr = explode('<h2>', $athleteInfo);
		if (sizeof($arr) < 2) {
			return;
		}
		$arr = $arr[1];
		$arr = explode('</h2>', $arr);
		if (sizeof($arr) > 1) {
			$this->athlete->name = trim($arr[0]);
		}

		$arr = explode('athleteid=', $athleteInfo);
		if (sizeof($arr) < 2) {
			return;
		}
		$arr = $arr[1];
		$arr = explode('&', $arr);
		if (sizeof($arr) > 1) {
			$this->athlete->pof10Id = (int)trim($arr[0]);
			$this->athlete->imageURL = 'http://www.thepowerof10.info/athletes/profilepic.aspx?athleteid=' . $this->athlete->pof10Id;
		}
	}
	private function _getAthleteOtherInfo($pof10Content) : void{
		$arr = explode('cphBody_pnlAthleteDetails">', $pof10Content);
		if (sizeof($arr) < 2) {
			return;
		}
		$rows = explode('/table>', $arr[1]);
		if (sizeof($arr) < 2) {
			return;
		}
		$arr = explode('<table cellspacing="0" cellpadding="2">', $rows[1]);
		if (sizeof($arr) < 2) {
			return;
		}
		$arr = $arr[1];
		$arr = explode('<tr>', $arr);
		if (sizeof($arr) < 2) {
			return;
		}

		foreach ($arr as $row){
			if ( $row !== "" ) {
				$rowArr = explode( '/b>', $row );
				if ( sizeof( $rowArr ) > 1 ) {
					$element = explode( '<b>', $rowArr[0] )[1];
					$element = explode( ':', $element )[0];
				}
				$part2 = explode( '<td>', $rowArr[1] );
				if ( sizeof( $part2 ) > 1 ) {
					$value = explode( '</td>', $part2[1] )[0];
				}
				switch ($element){
					case 'Club':
						$this->athlete->club = $value;
						break;
					case 'County':
						$this->athlete->county = $value;
						break;
					case 'Region':
						$this->athlete->region = $value;
						break;
					case 'Nation':
						$this->athlete->country = $value;
						break;
					case 'Age Group':
						$this->athlete->ageGroup = $value;
						break;
					case 'Gender':
						$this->athlete->gender = $value;
						break;
				}
			}
		}
		$arr = $rows[2];
		$rowArr = explode( 'Coach:', $arr );
		if ( sizeof( $rowArr ) < 2 ) {
			return;
		}
		$rowArr = explode( '">', $rowArr[1] );
		if ( sizeof( $rowArr ) < 2 ) {
			return;
		}
		$this->athlete->coach = explode( '</a>', $rowArr[1] )[0];
	}
	private function _getE4SEvent($event) {
		$maxlen = 5;
		$len = $maxlen;
		$minlen = 2;
		if (is_null($this->events)) {
			$this->_getEvents();
		}
		$event = strtoupper($event);
		$indoor = e4s_endsWith($event, 'I');
		while ($len >= $minlen) {
			$pof10 = substr($event, 0, $len);

			if (array_key_exists($pof10, $this->events)) {
				$this->events[$pof10]['indoor'] = $indoor;
				return $this->events[$pof10];
			}
			$len--;
		}

		// Event not defined !
		$this->_emailPBIssue($event . ' not coded for . URN : ' . $this->athlete->urn);
		return null;
	}
	private function _getEvents() {
		$sql = '
	        select e.*
	        from ' . E4S_TABLE_EVENTS . ' e
            where e.gender = "' . $this->athlete->gender[0] . '"
            and e.pof10 is not null';
		$result = e4s_queryNoLog($sql);

		$events = array();
		$rows = $result->fetch_all(MYSQLI_ASSOC);
		foreach ($rows as $row) {
			$pof10 = strtoupper($row['pof10']);
			$row['eventDefId'] = (int)$row['eventDefId']; // Discipline ID
			$row['id'] = (int)$row['ID']; // Event and Gender ID
			unset($row['ID']);
			$row['name'] = $row['Name'];
			unset($row['Name']);

			if (strpos($pof10, ',') !== FALSE) {
				$pof10 = explode(',', $pof10);
			} else {
				$pof10 = array($pof10);
			}
			foreach ($pof10 as $value) {
				$events[$value] = $row;
			}
		}
		$this->events = $events;

		return $events;
	}
	private function _emailPBIssue($msg) {
//		logTxt( $msg );
	}
}