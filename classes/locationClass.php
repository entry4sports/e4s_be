<?php


class locationClass {
    public array $locations;

    public function __construct() {
        $this->locations = array();
    }

    public function getLocation($id) {
        foreach ($this->locations as $location) {
            if ($location['id'] === $id) {
                return $location[$id];
            }
        }
        $this->getLocations([$id]);
        return $this->locations[$id];
    }

    public function getLocations($locationIds): array {
        $sql = '
            select *
            from ' . E4S_TABLE_LOCATION . '
            where id in ( ' . implode(',', $locationIds) . ')
           ';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return array();
        }
        $rows = $result->fetch_all(MYSQLI_ASSOC);

        foreach ($rows as $row) {
            $row = $this->getStandardRow($row);
            $row['id'] = (int)$row['id'];
            $this->locations[$row['id']] = $row;
        }

        return $this->locations;
    }

    public function getStandardRow($row) {
        if (!is_null($row)) {
            $row['address1'] = addslashes($row['Address1']);
            $row['address2'] = addslashes($row['Address2']);
            $row['town'] = addslashes($row['Town']);
            $row['county'] = addslashes($row['County']);
            $row['name'] = addslashes($row['location']);
            unset($row['Address1']);
            unset($row['Address2']);
            unset($row['Town']);
            unset($row['County']);
            unset($row['location']);
            foreach ($row as $key => $value) {
                if (is_null($value)) {
                    $row[$key] = '';
                }
            }
        }
        return $row;
    }

    public function getHomePageLocations() {
        $sql = '
            select id,
                   location name,
                   postCode
            from ' . E4S_TABLE_LOCATION;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return array();
        }
        $rows = $result->fetch_all(MYSQLI_ASSOC);

        foreach ($rows as $row) {
            $row['id'] = (int)$row['id'];
            $this->locations[$row['id']] = $row;
        }

        return $this->locations;
    }
}