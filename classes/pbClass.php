<?php
/*
 Update PB from Athlete maint
    if not a URN athlete
        update our pb
    end if
    track = true
    if pb supplied different from our pb then
        track = false
    end if
    update open comps setting pb and track

Creating an Entry
    Select User
        Check Pof10
            update PBs
            update open comps that are tracked
    create Entry
        track = true
        if pb supplied different from our pb then
            track = false
        end if


OTD Update
    update pb on entry with track = false
 */

class pbClass {
    public $athleteid;
    public $pbs;

    public function __construct($athleteid) {
        $this->athleteid = $athleteid;
        $this->_getAllCurrentPBs();
    }

    public function _getAllCurrentPBs() {
        $this->pbs = array();
        $sql = 'select *
                from ' . E4S_TABLE_ATHLETEPB . '
                where athleteid = ' . $this->athleteid;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return;
        }
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        foreach ($rows as $row) {
            $this->pbs[$row['eventid']] = $this->_formatRow($row);
        }
    }

    public function _formatRow($row) {
        return $row;
    }

    private function _updatePB($eId, $pb, $pbText) {
        $event = 'select * 
            from ' . E4S_TABLE_EVENTS . "
            where id = $eId";

        $result = e4s_queryNoLog($event);
        if ($result->num_rows == 0) {
            Entry4UIError(3030, 'Invalid Event', 400, '');
        }

        if (!array_key_exists($eId, $this->pbs)) {
            $sql = 'insert into ' . E4S_TABLE_ATHLETEPB . ' ( athleteid,eventid, pb, pbtext)
                    values ($this->athleteid,$eId, $pb,"' . $pbText . '")';
        } else {
            $sql = 'update ' . E4S_TABLE_ATHLETEPB . '
                    set pb = ' . $pb . ", 
                        pbtext = '" . $pbText . "'
                    where athleteid = $this->athleteid
                    and eventid = $eId
                   ";
        }
        e4s_queryNoLog($sql);
        $this->_updateOpenEntries($eId, $pb);
    }

    private function _updateOpenEntries($eId, $pb) {
        // Get open Comp/EventInfo
        $compSql = 'select ce.compId compId,
                            b.bibno bibNo,
                            e.id entryId,
                            e.options eOptions
                    from ' . E4S_TABLE_COMPETITON . ' c,
                         ' . E4S_TABLE_COMPEVENTS . ' ce 
                    left join ' . E4S_TABLE_ENTRIES . ' e on e.compeventid = ce.id
                    left join ' . E4S_TABLE_BIBNO . ' b on b.compid = ce.compid and b.athleteid = e.athleteid
                    where e.athleteid = ' . $this->athleteid . '
                    and EventID = ' . $eId . '
                    and ce.compid = c.id
                    and startdate > now()';
        $compResult = e4s_queryNoLog($compSql);

        while ($obj = $compResult->fetch_object()) {
            $process = FALSE;
            if (is_null($obj->bibNo) or isE4SUser()) {
                $process = TRUE;
            } else {
                $compObj = e4s_GetCompObj($obj->compId);
                if ($compObj->isOrganiser()) {
                    $process = TRUE;
                }
            }
            if ($process) {
                $entryObj = new entryClass($obj->entryId);
                $entryObj->updatePB($pb);
            }
        }
    }

    // Given an eventId from athlete maintenance, update open entries that have not already had bibs generated ( seeded ) or if the user is e4s or organiser

    public function updatePBFromPayload($obj) {
        $eId = $obj->get_param('eid');
        $pb = $obj->get_param('pb');

        if (is_null($pb)) {
            $pb = checkFieldFromParamsForXSS($obj, 'pb:PB');
        }
        if (!is_numeric($pb)) {
            Entry4UIError(3013, 'Invalid PB value', 200, '');
            exit();
        }
        $pbText = $obj->get_param('pbText');
        if (is_null($pbText)) {
            $pbText = checkFieldFromParamsForXSS($obj, 'pbText:PB Text');
        }

        $this->_updatePB($eId, $pb, $pbText);
    }
}