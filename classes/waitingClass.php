<?php

class waitingClass {
    public $compObj;
    public $entries;

    public function __construct($compId) {
        $this->compObj = e4s_getCompObj($compId);
        $this->entries = array();
    }

    public function getCountsForComp() {
        return $this->_getCounts(0);
    }

    public function getCountsForEgId($egId) {
        return $this->_getCounts($egId);
    }

    private function _getCounts($id) {
        $entries = $this->_getEntries($id);
        return $this->_processEgEntries($entries);
    }

    private function _getMaxAthletesForEg($egId) {
        $egObj = $this->compObj->getEventGroupByEgId($egId);
        return eventGroup::getMaxAthletes($egObj->options);
    }

    public function getLatestNonWaitingTime($egId) {
        $entries = $this->_getEntries($egId);
        if ( array_key_exists($egId, $entries)) {
	        $entries = $entries[$egId];
		}else{
			return date('Y-m-d H:i:s');
        }
        $maxAthletes = $this->_getMaxAthletesForEg($egId);

        $entry = null;
        if (sizeof($entries) < $maxAthletes) {
            $entry = end($entries);
        } else {
            $entry = $entries[$maxAthletes - 1];
        }
        return $entry->periodStart;
    }

    private function _processEgEntries($entries) {
        $counters = [];
        foreach ($entries as $egId => $egEntry) {
            $countObj = $this->_getDefaultCountObj();
            $maxAthletes = $this->_getMaxAthletesForEg($egId);
            $this->_setMaxAthletes($countObj, $maxAthletes);
            $cnt = 1;
            foreach ($egEntry as $entry) {
                $this->_addTotalCount($countObj);
                if ($entry->paid === E4S_ENTRY_NOT_PAID) {
                    $this->_addUnPaidCount($countObj);
                } else {
                    $this->_addPaidCount($countObj);
                }
                if ($cnt > $maxAthletes and $maxAthletes > 0) {
                    $this->_addWaitingCount($countObj);
                }
                $cnt++;
            }
            $counters[$egId] = $countObj;
        }
        return $counters;
    }

    private function _getDefaultCountObj() {
        $countObj = new stdClass();
        $countObj->totalCount = 0;
        $countObj->maxAthletes = 0;
        $countObj->paidCount = 0;
        $countObj->unPaidCount = 0;
        $countObj->waitingCount = 0;
        return $countObj;
    }

    private function _setMaxAthletes(&$countObj, $cnt) {
        $countObj->maxAthletes = $cnt;
    }

    private function _addTotalCount(&$countObj, $cnt = 1) {
        $countObj->totalCount += $cnt;
    }

    private function _addPaidCount(&$countObj, $cnt = 1) {
        $countObj->paidCount += $cnt;
    }

    private function _addUnPaidCount(&$countObj, $cnt = 1) {
        $countObj->unPaidCount += $cnt;
    }

    private function _addWaitingCount(&$countObj, $cnt = 1) {
        $countObj->waitingCount += $cnt;
    }
    // $egId = 0 means get all for comp
    // $paid -1 dont care, 0 = no, 1 = yes
    public function getWaitingListEntries($egId = 0, $paid = -1) {
        $allEntries = $this->_getEntries($egId);
        $waitingEntries = array();
        foreach ($allEntries as $egId => $entries) {
            $maxAthletes = $this->_getMaxAthletesForEg($egId);
            if ($maxAthletes > 0) {
                $cnt = 1;
                foreach ($entries as $entry) {
                    $process = $paid === -1;
                    if ($paid === E4S_ENTRY_NOT_PAID and $entry->paid === E4S_ENTRY_NOT_PAID) {
                        $process = TRUE;
                    }
                    // allow for paid and on hold ( cheque )
                    if ($paid !== E4S_ENTRY_NOT_PAID and $entry->paid !== E4S_ENTRY_NOT_PAID) {
                        $process = TRUE;
                    }
                    if ($process and $cnt > $maxAthletes) {
                        $waitingEntries[] = $entry;
                    }
                    $cnt++;
                }
            }
        }
        return $waitingEntries;
    }

    public function getAthleteWaitingPos($athleteId, $egId) {

        $entries = $this->_getEntries($egId);

        if (sizeof($entries) === 0) {
            // No Entries
            return 0;
        }

        $maxAthletes = $this->_getMaxAthletesForEg($egId);
        $cnt = -($maxAthletes - 1);

        foreach ($entries as $entry) {
            if ($athleteId === $entry->athleteId) {
                break;
            }
            $cnt++;
        }
        return $cnt;
    }

    public function isAthleteOnWaitingListforEg($athleteId, $egId) {
        $waitingPos = $this->getAthleteWaitingPos($athleteId, $egId);
        return $waitingPos > 0;
    }

    private function _getEntries($egId = 0): array {
        $entries = $this->compObj->getEntriesV2($egId);

        if ($egId === 0) {
            return $entries;
        }

        if (!array_key_exists($egId, $entries)) {
            return array();
        }
        return $entries[$egId];
    }

    public function performWaitingListRefunds() {
        $compObj = $this->compObj;
        $entries = $this->getWaitingListEntries(0, E4S_ENTRY_PAID);
        foreach ($entries as $entry) {
            $e4sRefundObj = new e4sRefund($compObj->getID(), [$entry->orderId], $entry->productId);

            if ($entry->price < 0.01) {
                // a free entry so just remove
                $e4sRefundObj->removeEntryOnly($entry);
            } else {
                $athlete = $entry->firstName . ' ' . $entry->surName;
                $parms = new stdClass();
                $parms->reason = 'Waiting list Refund for ' . $athlete;
                $parms->refundAmount = 0;
                $parms->pricePaid = $entry->price;
                $parms->eventids = [];
                $parms->credit = FALSE;
                $parms->testMode = !e4s_isLiveDomain();
                $parms->removeEntry = TRUE;
                $parms->ignorePaid = FALSE;
                $parms->refundE4SFee = FALSE;
                $parms->refundStripeFee = FALSE;
                $parms->email = TRUE;
                $newline = '<br>';
                $parms->emailTxt = 'Information about your Refund for Waiting list entry : ' . $newline;
                $parms->emailTxt = 'Competition ' . $compObj->getFormattedName() . $newline;
                $parms->emailTxt = 'Athlete ' . $athlete . $newline;
                $parms->emailTxt = 'Event ' . $entry->egName . $newline;
                $parms->multiple = TRUE;

                $e4sRefundObj->performRefund($parms, FALSE);
            }
        }
        $this->markWaitingListDone();
    }

    public function markWaitingListDone() {
        $compObj = $this->compObj;
        $options = $compObj->getOptions();
        $options->subscription->refunded = date('c');
        $options = $compObj->getOptionsForWriting($options);
        $sql = 'update ' . E4S_TABLE_COMPETITON . "
                set waitingrefunded = true,
                    options = '" . e4s_getOptionsAsString($options) . "'
                where id = " . $this->compObj->getID();
        e4s_queryNoLog($sql);
        $cacheObj = new cacheClass($this->compObj->getID());
        $cacheObj->clearCache();
    }

    public function getFullEvents() {
        $allEntries = $this->_getEntries(0);
        $fullEvents = array();

        foreach ($allEntries as $egId => $egEntries) {
            $maxAthletes = $this->_getMaxAthletesForEg($egId);
            if ($maxAthletes < 1) {
                // None set or a scheduled only event
                continue;
            }
            $cnt = 0;

            foreach ($egEntries as $entry) {
                if ($entry->paid !== E4S_ENTRY_NOT_PAID) {
                    // Paid or awaiting Payment or qualifying(free) entry
                    $cnt++;
                }
                if ($cnt === $maxAthletes) {
                    $fullEvents[] = $egId;
                    break;
                }
            }
        }
        return $fullEvents;
    }

    // process movements from the Waiting List
    public function processMovements() {
        $allEntries = $this->_getEntries(0);
        foreach ($allEntries as $egId => $egEntries) {
            $maxAthletes = $this->_getMaxAthletesForEg($egId);
            if ($maxAthletes > 0) {
                $cnt = -($maxAthletes - 1);
                foreach ($egEntries as $entry) {
                    if ($entry->paid === E4S_ENTRY_NOT_PAID) {
                        // not interested in Not Paid entries
                        continue;
                    }
                    $eOptions = $entry->eOptions;
                    $waitingInfo = $eOptions->waitingInfo;
                    $update = FALSE;
                    if ($cnt > 0) {
                        // On the waiting list but were not originally
                        if ($waitingInfo->originalPos === 0 and $waitingInfo->wentOn === '') {
                            $entry->waitingPos = $cnt;
                            $waitingInfo->wentOn = date('Y-m-d H:i:s');
                            $update = TRUE;
                        }
                    } else {
                        if ($waitingInfo->originalPos > 0 and $waitingInfo->cameOff === '') {
                            // They were on waiting list and are now not and have not been emailed
                            $entry->waitingPos = 0;
                            $waitingInfo->cameOff = date('Y-m-d H:i:s');
                            $update = TRUE;
                        }
                    }
                    if ($update) {
                        $this->_updateWaitingInfo($entry);
                        $this->emailMovement($entry);
                    }
                    $cnt++;
                }
            }
        }
    }

    private function _updateWaitingInfo(e4sEntryInfo $entry) {
        $eOptions = e4s_removeDefaultEntryOptions($entry->eOptions);
        $sql = 'update ' . E4S_TABLE_ENTRIES . "
                set options = '" . e4s_encode($eOptions) . "'
                where id = " . $entry->entryId;
        e4s_queryNoLog($sql);
    }

    public function emailMovement(e4sEntryInfo $entry) {
        $emailTo = E4S_SUPPORT_EMAIL;
        $onWaitingList = $this->compObj->isAthleteOnWaitingListForId($entry->athleteId, $entry->egId);
        if ($onWaitingList) {
            // Athlete moved onto waiting list !!!!!
            $body = 'Entry4Sports<br><br>';
            $body .= 'Competition : ' . $this->compObj->getID() . '<br>';
            $body .= 'Entry id : ' . $entry->entryId . '<br>';
            $subject = 'Athlete MOVED ONTO WAITING LIST !!!!!';
            $headers = Entry4_mailHeader('Entries', FALSE);
        } else {
            $uSql = 'select user_email email,
                            user_nicename userName
                     from ' . E4S_TABLE_USERS . '
                     where id = ' . $entry->userId;
            $result = e4s_queryNoLog($uSql);
            if ($result->num_rows !== 1) {
                Entry4UIError(9350, 'Invalid user for entry : ' . $entry->entryId);
            }
            $userObj = $result->fetch_object();
            $subject = 'IMPORTANT Information about your entry to the ' . $this->compObj->getName() . ' competition. Please read.';
            $emailTo = $userObj->email;

            $body = 'Dear ' . $userObj->userName . ',<br><br>';
            $body .= formatAthleteFirstname($entry->firstName) . ' ' . formatAthleteFirstname($entry->surName);
            $body .= ' (Order #:' . $entry->orderId . ") '" . $entry->egName . "' event at the " . $this->compObj->getName() . ' competition (#' . $this->compObj->getID() . ') held on ';
            $date = strtotime($entry->eventStartDate);
//            $body .= date_format($date,"d/m/Y H:i:s");
            $body .= date('l jS \of F Y g:i a', $date);
            $body .= ' at ' . $this->compObj->getLocationName() . '.<br><br>';
            $body .= 'THIS ENTRY IS NOW CONFIRMED and has been moved off the waiting list.<br><br>';
            $body .= 'You can check this by clicking the following link to the entry list : ';
            $body .= "<a href=\"https://" . E4S_CURRENT_DOMAIN . '/' . $this->compObj->getID() . "/entries\">" . $this->compObj->getName() . ' Entries</a>';
            if (!e4s_isLiveDomain()) {
                $body .= '<br><br>This was generated from the domain : ' . E4S_CURRENT_DOMAIN;
            }
            $headers = Entry4_mailHeader('Entries', TRUE);
        }
        $body .= Entry4_emailFooter(null);
        if (!e4s_isLiveDomain()) {
            $emailTo = E4S_SUPPORT_EMAIL;
        }
        if (e4s_isDevDomain()) {
            $emailTo = 'paul@entry4sports.com';
        }

        e4s_mail($emailTo, $subject, $body, $headers);
    }
}