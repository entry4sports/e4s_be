<?php


class e4sOrders {
    public $compid;
    public $athleteid;

    public function __construct($compid) {
        $this->compid = $compid;
        $this->athleteid = 0;
    }

    public static function athleteID($compid, $athleteid) {
        $instance = new self((int)$compid);
        $instance->athleteid = (int)$athleteid;
        return $instance;
    }

    public static function getDownloadForItem($item, $downloads) {
        $returnDownload = null;
//echo "<br>getDownloadForItem<br>";
//var_dump($item);

        $item_id = $item->get_id();
        $product = $item->get_product();
        if ($product === FALSE) {
            // product has been deleted
            echo 'WARNING: Product has been deleted for item ' . $item_id;
            return null;
        }

        $descObj = e4s_getWCProductDescObj($product);

        $itemProdId = $product->get_id();
        $itemParentProdId = $product->get_parent_id();

        // Tickets
        foreach ($downloads as $download) {
            $productId = $download['product_id'];

            if ($productId === $itemProdId || $productId === $itemParentProdId) {

                $download['item_id'] = $item_id;
                $download['comp_id'] = $descObj->compid;
                $returnDownload = $download;
                break;
            }
        }

        if (is_null($returnDownload)) {
            // Not a 2ndry Ticket, so check if Athlete
            if (isset($descObj->athlete)) {
                $returnDownload = array();
                $returnDownload['download_url'] = E4S_TICKET_URL;
                $returnDownload['item_id'] = $item_id;
                $returnDownload['order_id'] = $item->get_order_id();
                $returnDownload['product_id'] = $itemProdId;
                $returnDownload['athlete_id'] = $descObj->athleteid;
                $returnDownload['download_name'] = $descObj->compid . ' : ' . $descObj->athlete;
                $returnDownload['comp_id'] = $descObj->compid;
            }
        }

        return $returnDownload;
    }

    public static function getProductPurchasedForAthlete($productId, $variationId, $athleteInfo) {
        $sql = '
            SELECT sum(m3.meta_value) orderedQty 
            FROM ' . E4S_TABLE_WCORDERITEMMETA . ' m1, 
                 ' . E4S_TABLE_WCORDERITEMMETA . ' m2, 
                 ' . E4S_TABLE_WCORDERITEMMETA . " m3 
            where m1.order_item_id = m2.order_item_id 
              and m1.order_item_id = m3.order_item_id 
              and m1.meta_key = '_athlete' 
              and m1.meta_value = '" . $athleteInfo . "'";
        if ($variationId !== 0) {
            $sql .= " and m2 . meta_key = '_variation_id'
            and m2 . meta_value = '" . $variationId . "'";
        } else {
            $sql .= " and m2 . meta_key = '". WC_POST_PRODUCT_ID. "'
            and m2 . meta_value = '" . $productId . "'";
        }
        $sql .= " and m3.meta_key = '_qty'";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            return 0;
        }
        $row = $result->fetch_assoc();
        return (int)$row['orderedQty'];
    }

    public function eventOrderInfo($compEntry = null) {

        $orderInfo = new stdClass();
        $orderInfo->orderId = 0;
        $orderInfo->productId = 0;
        $orderInfo->e4sLineValue = 0;
        $orderInfo->wcLineValue = 0;
        $orderInfo->isRefund = FALSE;
        $orderInfo->refundValue = 0;
        $orderInfo->isCredit = FALSE;
        $orderInfo->creditValue = 0;
        $orderInfo->discountId = 0;
        $orderInfo->dateOrdered = '';
        if (is_null($compEntry)) {
            return $orderInfo;
        }

        $orderInfo->discountId = (int)$compEntry['discountid'];
        $orderInfo->orderId = (int)$compEntry['orderid'];
        $orderInfo->productId = (int)$compEntry['variationID'];
        $orderInfo->e4sLineValue = (float)$compEntry['price'];
        if ($orderInfo->orderId !== 0) {
            $wcOrder = wc_get_order($orderInfo->orderId);
            if ($wcOrder !== FALSE) {
                $wcDateCreated = $wcOrder->get_date_created();
                $e4sDate = $wcDateCreated->date('Y-m-d');

                $orderInfo->dateOrdered = $e4sDate;
                foreach ($wcOrder->get_items() as $orderItem) {
                    $itemProductId = $orderItem->get_product_id();

                    if ($itemProductId === $orderInfo->productId) {
                        $orderInfo->wcLineValue = floatval($orderItem->get_total());
                        // Perhaps have these values in config or the priceClass ?
                        $orderInfo->creditValue = $orderInfo->wcLineValue * 0.90;
                        $orderInfo->refundValue = $orderInfo->wcLineValue * 0.85;
                        if ($compEntry['isOrganiser']) {
                            $orderInfo->creditValue = $orderInfo->wcLineValue;
                            $orderInfo->refundValue = $orderInfo->wcLineValue * 0.90;
                        }
                        if ($compEntry['isE4S']) {
                            $orderInfo->creditValue = $orderInfo->wcLineValue;
                            $orderInfo->refundValue = $orderInfo->wcLineValue;
                        }
                        break;
                    }
                }
            }
        }

        if ($orderInfo->wcLineValue !== $orderInfo->e4sLineValue and $orderInfo->orderId !== 0) {
            $this->addAnyRefundInfo($orderInfo);
        }

        return $orderInfo;
    }

    private function addAnyRefundInfo(&$orderInfo) {
        $sql = 'select *
                from ' . E4S_TABLE_REFUNDS . '
                where compid = ' . $this->compid . '
                and orderid = ' . $orderInfo->orderId;

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            return;
        }
        $found = FALSE;
        while ($obj = $result->fetch_object()) {
            $options = e4s_getDataAsType($obj->options, E4S_OPTIONS_OBJECT);
            foreach ($options->lines as $line) {

                if ($line->productid === $orderInfo->productId) {
                    if ($obj->credit === '0') {
                        $orderInfo->isRefund = TRUE;
                    } else {
                        $orderInfo->isCredit = TRUE;
                    }
                    $found = TRUE;
                }
                if ($found) {
                    break;
                }
            }
        }
    }

    public function getOrdersPurchasedForAthlete() {
        return $this->_getItemsPurchasedForAthlete('ORDER');
    }

    private function _getItemsPurchasedForAthlete($retType) {
        $retObj = array();
        // Get All products the athlete
        $orderResult = $this->_getProductsPurchasedForAthlete();
        if ($orderResult->num_rows === 0) {
//            e4s_addDebug("1", "No Orders");
            return $retObj;
        }
        $orderRows = $orderResult->fetch_all(MYSQLI_ASSOC);
        $e4sOrders = array();
        $prodIds = array();
        foreach ($orderRows as $orderRow) {
            $userObj = new stdClass();
            $userObj->id = $orderRow['userid'];
            $userObj->name = $orderRow['username'];

            $orderId = (int)$orderRow['orderId'];
            $orderItemId = (int)$orderRow['orderItemId'];
            $metaKey = $orderRow['metaKey'];
            $metaValue = $orderRow['metaValue'];

            if (!array_key_exists($orderItemId, $e4sOrders)) {
                $e4sOrders [$orderItemId] = array();
            }

            $e4sOrders [$orderItemId][$metaKey] = $metaValue;
            if ($metaKey === WC_POST_PRODUCT_ID) {
                $prodIds[$orderItemId] = $metaValue;

                // add the order info to the array once
                $e4sOrders [$orderItemId]['_order_id'] = $orderId;
                $e4sOrders [$orderItemId]['_user'] = $userObj;
            }
        }
        // Remove those not for the competition passed

        // Get list of prods for this comp
        $defSql = 'select sd.prodid prodid
                   from ' . E4S_TABLE_SECONDARYDEFS . ' sd,
                        ' . E4S_TABLE_SECONDARYREFS . ' sr
                   where sd.prodid in (' . implode(',', $prodIds) . ')
                   and   sd.refid = sr.id
                   and   sr.compid = ' . $this->compid . "
                   and   sd.peracc = '" . E4S_PERACC_ATHLETE . "'";

        $defResult = e4s_queryNoLog($defSql);
        if ($defResult->num_rows === 0) {
            // None for this comp
            e4s_addDebug('2', 'No Orders');
            return $retObj;
        }
        $compProdRows = $defResult->fetch_all(MYSQLI_ASSOC);
        $compProds = array();
        foreach ($compProdRows as $compProdRow) {
            $id = (int)$compProdRow['prodid'];
            $compProds[$id] = $id;
        }
        // Get list of orders only for the products of this comp
        $e4sOrdersForComp = array();
        foreach ($e4sOrders as $itemId => $e4sOrder) {
            $orderProdId = (int)$e4sOrder[WC_POST_PRODUCT_ID];

            if (in_array($orderProdId, $compProds)) {
                $item = new stdClass();
                $item->order = $e4sOrder;
                $item->orderId = $e4sOrder['_order_id'];
                $item->qtyPurchased = $e4sOrder['_qty'];
                $item->id = (int)$e4sOrder['_variation_id'];
                $item->parentId = (int)$e4sOrder[WC_POST_PRODUCT_ID];
                if ($item->id === 0) {
                    $item->id = $item->parentId;
                    $item->parentId = 0;
                }
                $e4sOrdersForComp[$itemId] = $item;
            }
        }

        if (empty($e4sOrdersForComp)) {
            // No orders for this comp
            e4s_addDebug('3', 'No Orders');
            return $retObj;
        }
        if ($retType === 'ORDER') {
            return $e4sOrdersForComp;
        }
//        e4s_dump($e4sOrders,"Got Some Products",true);
        if ($retType === 'PRODUCT') {
            return $this->_getProducts($e4sOrdersForComp);
        }

        // should not get here
        e4s_addDebug($retType, 'retType');
        e4s_addDebug($retObj, 'retObj');
        return array();
    }

    public function _getProductsPurchasedForAthlete() {
        $orderSql = 'select u.ID userid, u.user_nicename username, wcOrder.order_id orderId, wcOrderMeta2.order_item_id orderItemId,wcOrderMeta2.meta_key metaKey, wcOrderMeta2.meta_value metaValue
                     from ' . E4S_TABLE_WCORDERITEMMETA . ' wcOrderMeta1,
                          ' . E4S_TABLE_WCORDERITEMMETA . ' wcOrderMeta2,
                          ' . E4S_TABLE_WCORDERITEMS . ' wcOrder,
                          ' . E4S_TABLE_POSTMETA . ' pm,
                          ' . E4S_TABLE_ATHLETE . ' a,
                          ' . E4S_TABLE_USERS . ' u
                     where a.id = ' . $this->athleteid . "
                     and   wcOrderMeta1.meta_key = '" . strtolower(E4S_ATTRIB_ATHLETE) . "'
                     and   wcOrderMeta1.meta_value = " . e4sProduct::getAthleteAttributeKey(TRUE) . "
                     and   wcOrderMeta1.order_item_id = wcOrderMeta2.order_item_id
                     and   wcOrderMeta2.order_item_id = wcOrder.order_item_id
                     and   pm.post_id = wcOrder.order_id
                     and   pm.meta_value = u.id
                     and   pm.meta_key = '_customer_user'
                     order by wcOrderMeta2.order_item_id, wcOrderMeta2.meta_key";
//                     and   wcOrderMeta2.meta_key = '_variation_id'
//                    ";
        return e4s_queryNoLog($orderSql);
    }

    private function _getProducts($productArr) {

        $e4sProdArr = array();
        foreach ($productArr as $prod) {
            $e4sProd = new e4sProduct(null, $this->compid);
            $useKey = null;
            if (isset($prod->product)) {
                $useKey = $prod->product;
            } else {
                $useKey = $prod->id;
            }
            $prodObj = $e4sProd->read($useKey, FALSE);

            $prodObj->orderId = $prod->orderId;
            if (isset($prodObj->parentId)) {
                if ($prodObj->parentId !== 0) {
                    $prodObj->prodId = $prodObj->parentId;
                    $prodObj->variantId = $prodObj->id;
                } else {
                    $prodObj->prodId = $prodObj->id;
                    $prodObj->variantId = 0;
                }
            }
            unset($prodObj->parentId);
            unset($prodObj->id);

            if (isset($prod->user)) {
                $prodObj->user = $prod->user;
            }
            if (isset($prod->order)) {
                if (array_key_exists('_user', $prod->order)) {
                    $prodObj->user = $prod->order['_user'];
                    unset($prod->order['_user']);
                }
                $prodObj->orderLine = $prod->order;
                unset($prodObj->orderLine['_variation_id']);
                unset($prodObj->orderLine[WC_POST_PRODUCT_ID]);
                unset($prodObj->orderLine['_qty']);
                unset($prodObj->orderLine['_order_id']);
                unset($prodObj->orderLine['_line_tax_data']);
            }
            $prodObj->qtyPurchased = $prod->qtyPurchased;
            unset($prodObj->soldQty);
//            $retObj = new stdClass();
//            $retObj->prod = $prodObj;
            $e4sProdArr[] = $prodObj;
        }
        return $e4sProdArr;
    }

    public function getProductsPurchasedForAthlete() {
        return $this->_getItemsPurchasedForAthlete('PRODUCT');
    }

    public function getProductsPurchased() {

        $compObj = e4s_GetCompObj($this->compid, FALSE);
        $fromDate = $compObj->getRow()['EntriesOpen'];
        $toDate = $compObj->getRow()['Date'];

        $searchArr = array('_customer_user' => e4s_getUserID(), 'status' => array('wc-completed'));
        if (!is_null($fromDate)) {
            $fromArr = explode(' ', $fromDate);
            $fromDate = $fromArr[0];
        } else {
            $fromDate = '2020-12-01';
        }
        if (!is_null($toDate)) {
            $toArr = explode(' ', $toDate);
            $toDate = $toArr[0];
        } else {
            $toDate = date('Y-m-d');
        }
        $searchArr['date_paid'] = $fromDate . '...' . $toDate;

        $customer_orders = wc_get_orders($searchArr);

        $productArr = []; //
        foreach ($customer_orders as $customer_order) {
            $orderId = $customer_order->get_id();

            $orderq = wc_get_order($customer_order);
            $items = $orderq->get_items();
            foreach ($items as $item) {
                $itemQty = $item->get_quantity();
                $product = $item->get_product();
                if ($product !== FALSE) { // product not deleted
                    $useId = $product->get_id();

                    $parentId = $product->get_parent_id();
                    if ($parentId !== 0) {
                        $useId = $parentId;
                    }
                    $terms = get_the_terms($useId, 'product_cat');
                    if ($terms !== FALSE) {
                        foreach ($terms as $term) {
                            if (strpos($term->name, '-' . $this->compid) > 0) {
                                if (array_key_exists($useId, $productArr)) {
                                    $obj = $productArr[$useId];
                                    $obj->qtyPurchased += $itemQty;
                                } else {
                                    $obj = new stdClass();
                                    $obj->product = $product;
                                    $obj->qtyPurchased = $itemQty;
                                }
                                $obj->orderId = $orderId;
                                $userObj = new stdClass();
                                $userObj->id = e4s_getUserID();
                                $userObj->name = $GLOBALS[E4S_USER]->user->user_nicename;
                                $obj->user = $userObj;
                                $productArr[$useId] = $obj;
                                break;
                            }
                        }
                    }
                }
            }
        }

        return $this->_getProducts($productArr);
    }

}
