<?php
define('E4S_ENTRY_UPDATE', 'update');
define('E4S_ENTRY_DELETE', 'delete');
define('E4S_ENTRY_PERIOD', 1800);
define('E4S_ENTRY_EXPIRED_MINS', 40);


class entryClass {
    public $searchParams;
    public $entries;
    public $events;
    public $compId;
    public $entryId;

    public function __construct($entryId = 0) {
        $this->entryId = $entryId;
    }

    public static function addWaitingPos($options, $waitingPos) {
        $options->waitingInfo->originalPos = $waitingPos;
        return $options;
    }

	public function getPerf(){
		$obj = new stdClass();
		$obj->entryId = $this->entryId;

		$sql = 'select a.id,
		               a.firstName,
		               a.surName,
		               a.URN URN,
		               a.aoCode,
		               e.pb,
		               ce.id ceId,
		               ce.split,
		               ce.eventId,
		               ev.Name eventName,
		               ev.tf,
		               ev.min,
		               ev.max,
		               ev.uomOptions uom
		        from ' . E4S_TABLE_ENTRIES . ' e,
		             ' . E4S_TABLE_ATHLETE . ' a,
		             ' . E4S_TABLE_EVENTS . ' ev,
		             ' . E4S_TABLE_COMPEVENTS . ' ce
		        where e.id = ' . $this->entryId . '
		              and abs(e.athleteid) = a.id
		              and abs(e.compeventid) = ce.id
		              and abs(ce.eventid) = ev.id';
		$result = e4s_queryNoLog($sql);
		if ($result->num_rows === 0) {
			Entry4UIError(4141, 'Unable to find the Entry');
		}
		$obj->athleteMini = $result->fetch_object();
		$obj->athleteMini->id = (int)$obj->athleteMini->id;
		$obj->athleteMini->firstName = formatAthleteFirstname($obj->athleteMini->firstName);
		$obj->athleteMini->surName = formatAthleteSurname($obj->athleteMini->surName);
		$obj->compEventMini = new stdClass();
		$obj->compEventMini->split = (float)$obj->athleteMini->split;
		$obj->compEventMini->ceId = (int)$obj->athleteMini->ceId;

		$perfObj = new perfAthlete();
		$perfs = $perfObj->getPerformances($obj->athleteMini->id);
		if ( !array_key_exists($obj->athleteMini->id, $perfs) ){
//			Entry4UIError(4142, 'Unable to find the Performances for athlete (' . $obj->athleteMini->id . ')');
		}

		foreach ($perfs as $athleteId=>$athletePerf){
			// it always should but check
			if ( (int)$athleteId === (int) $obj->athleteMini->id ) {
				foreach ( $athletePerf as $eventId=>$perf ) {
					if ( $eventId === (int) $obj->athleteMini->eventId ) {
						$perf->perf = (float)$obj->athleteMini->pb;
						$obj->performance = $perf;
						break;
					}
				}
			}
		}
		if ( !isset($obj->performance)){
			$perf = new stdClass();
			$perf->perf = (float)$obj->athleteMini->pb;

			$perf->pb = 0;
			$perf->pbText = '';
			$perf->pbAchieved = '';
			$perf->sb = 0;
			$perf->sbText = '';
			$perf->sbAchieved = '';
			$perf->ageGroup = '';
			$perf->curAgeGroup = '';

			$perf->eventName = $obj->athleteMini->eventName;

			$perf->uom = e4s_getOptionsAsObj($obj->athleteMini->uom);

			$perf->limits = new stdClass();
			$perf->limits->min = (float)$obj->athleteMini->min;
			$perf->limits->max = (float)$obj->athleteMini->max;

			$perf->uom = e4s_getOptionsAsObj($obj->athleteMini->uom);
			$obj->performance = $perf;
		}else{
			$limits = new stdClass();
			$limits->min = (float)$obj->performance->min;
			$limits->max = (float)$obj->performance->max;
			$obj->performance->limits = $limits;
		}
		if ( isset($obj->performance->uomOptions) ){
			$obj->performance->uom = $obj->performance->uomOptions;
			unset($obj->performance->uomOptions);
		}
		// ensure 2 decimal places
		$perf->perf = resultsClass::ensureDecimals($perf->perf);
		$perf->perfText = E4S_ENSURE_STRING . $perf->perf;

		if ($obj->athleteMini->tf === E4S_EVENT_TRACK) {
			$perf->perfText = E4S_ENSURE_STRING . resultsClass::getResultFromSeconds($perf->perf);
		}

		unset($obj->athleteMini->tf);
		unset($obj->athleteMini->uom);
		unset($obj->athleteMini->eventName);
		unset($obj->athleteMini->pb);
		unset($obj->athleteMini->min);
		unset($obj->athleteMini->max);
		unset($obj->athleteMini->split);
		unset($obj->athleteMini->ceId);
		unset($obj->athleteMini->eventId);
		unset($obj->performance->min);
		unset($obj->performance->max);
		return $obj;
	}
    public function updatePB($pb, $track = false) {
		if ( $track ){
			$track = 1;
		}else{
			$track = 0;
		}

        $obj = null;
		$sql = '
				select e.id entryId
                   , e.options eOptions
                   , ce.compId
				   , ce.startDate
            from ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce
            where e.id = ' . $this->entryId . '
            and abs(e.compeventid) = ce.id
		';

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            $obj = $result->fetch_object(E4S_ENTRYINFO_OBJ);
	        $today = date('Y/m/d');
	        $ceDate = date('Y/m/d', strtotime($obj->startDate));
	        if ($today > $ceDate) {
		        $config = e4s_getConfig();
				if (  strtoupper($config['env']) === E4S_ENV_PROD ) {
					Entry4UIError( 4143, 'You cannot update the Seeding Performance data after the event date' );
				}
	        }
            $eOptions = e4s_removeDefaultEntryOptions( e4s_addDefaultEntryOptions($obj->eOptions) );
            $updateSql = 'update ' . E4S_TABLE_ENTRIES . "
                          set pb = '" . $pb . "',
                          	  trackedPB = " . $track . ", 
                              options = '" . e4s_getDataAsType($eOptions, E4S_OPTIONS_STRING) . "'
                          where id = " . $this->entryId;
            e4s_queryNoLog($updateSql);
        }
        return $obj;
    }

	public function addIncludedEntries($compId){
		$compObj = e4s_getCompObj($compId);
		if ( !$compObj->isOrganiser()){
			Entry4UIError(9781,'You are not authorised to call this process.' );
		}

		$compObj->getEGObjs();
		$entryGroupObj = new eventGroup($compId);
		foreach($compObj->eventGroups as $egId=>$egObj){
			$includedEgId = $entryGroupObj->getIncludedEntries($egObj);
			if ( $includedEgId > 0 ){
				$compObj->addIncludedEntries($egId, $includedEgId);
			}
		}
	}
	function copyEntry($sourceEntry, $useCeId, $paid = E4S_ENTRY_QUALIFY):int{
		$checkedIn = $sourceEntry->checkedIn ? 1 : 0;
		$confirmed = $sourceEntry->confirmed;
		if (is_null($confirmed)) {
			$confirmed = 'null';
		} else {
			$confirmed = "'" . $confirmed . "'";
		}
		if ( !isset($sourceEntry->athlete)) {
			// an EntryInfo object
			$sourceEntry->athlete = formatAthlete($sourceEntry->firstName, $sourceEntry->surName);
			$sourceEntry->vetAgeGroupId = 0;
			$sourceEntry->teamId = 0;
			$sourceEntry->waitingPos = 0;
			$sourceEntry->entryPos = 0;
		}
		$sourceEntry->athlete = addslashes( $sourceEntry->athlete );
		$sql = "insert into " . E4S_TABLE_ENTRIES . "(compEventId, athleteid,teambibno,athlete,pb,discountid,variationid,clubid,orderid,paid,price,eventAgeGroup,ageGroupId,vetAgeGroupID,teamid,userid,options,checkedin,waitingpos,entrypos,confirmed,present)
                         values(
                            {$useCeId},
                            {$sourceEntry->athleteId},
                            '{$sourceEntry->teamBibNo}',
                            '{$sourceEntry->athlete}',
                            0,
                            0,
                            {$sourceEntry->productId},
                            {$sourceEntry->clubId},
                            {$sourceEntry->orderId},
                            {$paid},
                            0.00,
                            '{$sourceEntry->eventAgeGroup}',
                            {$sourceEntry->ageGroupId},
                            {$sourceEntry->vetAgeGroupId},
                            {$sourceEntry->teamId},
                            " . e4s_getUserID() . ",
                            '" . $sourceEntry->getWriteOptionsStr() . "',
                            " . $checkedIn . ",
                            {$sourceEntry->waitingPos},
                            {$sourceEntry->entryPos},
                            " . $confirmed . ",
                            1
                         );";
		e4s_queryNoLog($sql);
		return e4s_getLastID();
	}
    public function createEntriesForEg($sourceEgId, $targetEgId = null) {
        if (!is_null($targetEgId)) {
            $targetEgIds = [$targetEgId];
        }else {
            $targetEgIds = eventGroup::getEntriesFromEgIds($sourceEgId);
        }
        $targetCEs = $this->_getTargetCEs($targetEgIds);
        $this->_deleteAutoEntries($targetEgIds);
        $sourceEntries = $this->_getSourceEntries($sourceEgId);

        $newEntries = array();

        foreach ($targetCEs as $newEgId => $targetCEByAgIds) {
            $athleteIds = array();
            foreach ($sourceEntries as $sourceEntry) {
                $targetCE = $targetCEByAgIds[$sourceEntry->ageGroupId];
	            $newEntryId = $this->copyEntry($sourceEntry, $targetCE->id);
                $newObj = new stdClass();
                $newObj->newEntryId = $newEntryId;
                $newObj->oldEntryId = $sourceEntry->id;
                $newObj->newCeId = $targetCE->id;
                $newObj->newEgId = $newEgId;
                $newObj->oldEgId = $sourceEgId;
                $newEntries[] = $newObj;
                $athleteIds[] = $sourceEntry->athleteId;
            }
            $seedv2Obj = new seedingV2Class($this->compId, $newEgId);
            if (is_null($this->compId)) {
                $this->compId = $seedv2Obj->compObj->getID();
            }
            $seedv2Obj->insertNonSeededAthletes($athleteIds);
        }
        e4s_sendSocketInfo($this->compId, $newEntries, R4S_SOCKET_ENTRIES_COPIED);
    }

    private function _getTargetCEs($targetEgIds) {
        $sql = 'select *
                from ' . E4S_TABLE_COMPEVENTS . '
                where maxgroup in (' . implode(',', $targetEgIds) . ')';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            Entry4UIError(9703, 'No CompEvents found');
        }
        $ceObjs = array();
        while ($obj = $result->fetch_object(E4S_COMPEVENT_OBJ)) {
            if (!array_key_exists($obj->egId, $ceObjs)) {
                $ceObjs[$obj->egId] = array();
            }

            $ceObjs[$obj->egId][$obj->ageGroupId] = $obj;
        }
        return $ceObjs;
    }

    private function _deleteAutoEntries($egIds) {
        $sql = 'delete from ' . E4S_TABLE_ENTRIES . '
                where compeventid in (
                    select id 
                    from ' . E4S_TABLE_COMPEVENTS . ' 
                    where maxGroup in (' . implode(',', $egIds) . ')
                )';
        e4s_queryNoLog($sql);
        $sql = 'delete from ' . E4S_TABLE_SEEDING . '
                where eventgroupid in (' . implode(',', $egIds) . ')';
        e4s_queryNoLog($sql);
    }

    private function _getSourceEntries($egId) {
        $sql = 'select e.*, ce.compId
                from ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce
                where maxgroup = ' . $egId . '
                and   ce.id = e.compeventid
                and paid in (' . E4S_ENTRY_PAID . ',' . E4S_ENTRY_QUALIFY . ')
                order by agegroupid';
        $result = e4s_queryNoLog($sql);
        $entries = array();
        $entryCountObj = null;
        while ($obj = $result->fetch_object(E4S_ENTRY_OBJ)) {
            if (is_null($entryCountObj)) {
                $entryCountObj = e4s_getEntryCountObj($obj->compId);
            }
            $entryObj = $entryCountObj->getAthleteEntryForEgId($obj->athleteId, $egId);
            if ($entryObj->waitingPos === 0) {
                unset($obj->compId);
                $entries[$obj->id] = $obj;
            }
        }
        return $entries;
    }

    // entry made on event with a priceDiscount

    public function checkAndSetPriceDiscount($priceRow) {
        $priceRow['applyDiscounts'] = TRUE;
        $pOptions = e4s_addDefaultPriceOptions($priceRow['options']);
        if ($pOptions->discount->validToDate !== '') {
//            $athleteDob = date_create($athlete['dob']);
            $toDate = e4s_isoToDate($pOptions->discount->validToDate);
            $timeStamp = $toDate->getTimestamp();
            $now = strtotime('now');
            if ($now > $timeStamp) {
                // time has passed
                return $priceRow;
            }
        }
//        first check if in date
        $priceId = $priceRow['ID'];
        $sql = 'select count(e.id) entryCount
                from ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce
                where ce.id = e.compeventid
                and   ce.priceid = ' . $priceId;
        $result = e4s_queryNoLog($sql);
        $obj = $result->fetch_object();
        $obj->entryCount = (int)$obj->entryCount;

        if ($obj->entryCount < $pOptions->discount->count) {
            // discount should be applied
            $priceRow['saleenddate'] = null;
            $priceRow['price'] = $pOptions->discount->price;
            $priceRow['saleprice'] = $pOptions->discount->price;
            $priceRow['usePrice'] = $pOptions->discount->price;
            $priceObj = new priceClass($priceRow['compid'], FALSE);
            $priceRow['fee'] = $priceObj->getFeeForPrice($priceRow['usePrice']);
            $priceRow['salefee'] = $priceRow['fee'];
            $priceRow['applyDiscounts'] = FALSE;
        }
        return $priceRow;
    }

    // move single Entry ID
    public function moveOffWaitingList($entryId) {
        return $this->moveEntriesOffWaitingList([$entryId]);
    }

    // move array of Entry IDs
    public function moveEntriesOffWaitingList($entryIds) {
        $this->entries = entryClass::getEntries($entryIds, FALSE);
        // should be all from same comp but just in case
        $compIds = array();
        $errors = array();
        $compId = 0;
        foreach ($this->entries as $entry) {
            $compId = $entry->compId;
            $compObj = e4s_getCompObj($compId);
            if (!$compObj->isOrganiser()) {
                Entry4UIError(9311, 'You are not authorised to perform this function');
            }
            if ($compObj->isWaitingListEnabled()) {
                $compIds[$compId] = $compId;
                $compObj->disableWaitingListProcessing();
            } elseif (!(array_key_exists($compId, $compIds))) {
                $errors[] = 'Waiting list not enabled for competition ' . $compId;
            }
        }
        if (is_null($this->compId)) {
            $this->compId = $compId;
        }
        foreach ($compIds as $compId) {
            foreach ($this->entries as $entry) {
                if ($compId === $entry->compId) {
                    $egId = $entry->egId;
                    // check is full before we move the entry in
                    if ($this->_isEventFull($egId)) {
                        $egObj = new eventGroup($compId, FALSE);
                        $egInfo = $this->_getEgInfo($egId);
                        $egOptions = $egInfo->options;
                        $v2Options = new stdClass();
                        $v2Options->limits = array();
                        $maxAthletes = eventGroup::getMaxAthletes($egOptions);
                        $maxInHeat = eventGroup::getMaxInHeat($egOptions);
                        $v2Options->limits['maxAthletes'] = $maxAthletes + 1;
                        $v2Options->limits['maxInHeat'] = $maxInHeat;
                        $v2Options->seed = $egOptions->seed;
                        $egInfo->options = $v2Options;
                        $egObj->updateEG($egInfo);
                    }

                    $latestTime = $this->_getLatestNonWaitingTime($egId);
                    $this->_moveEntryOffWaitingList($entry, $latestTime);
                }
            }
        }
        return $errors;
    }

    private function _getLatestNonWaitingTime($egId) {
        $waitingObj = new waitingClass($this->compId);
        return $waitingObj->getLatestNonWaitingTime($egId);
    }

    private function _getEgInfo($egId): stdClass {
        $compObj = e4s_getCompObj($this->compId);
        $stdEgObj = $compObj->getEventGroupByEgId($egId);
        $obj = new stdClass();
        $obj->egId = $egId;
        $obj->options = $stdEgObj->options;
        $obj->eventDateTimeISO = $stdEgObj->startDate;
        $obj->typeNo = $stdEgObj->typeNo;
        $obj->eventNo = $stdEgObj->eventNo;
        $obj->bibSortNo = $stdEgObj->bibSortNo;
        $obj->name = $stdEgObj->name;
        return $obj;
    }

    /**
     * @param int $egId
     * @return bool
     */
    private function _isEventFull(int $egId): bool {
        $compObj = e4s_getCompObj($this->compId);
        $egObj = $compObj->getEventGroupByEgId($egId);
        $maxAthletes = eventGroup::getMaxAthletes($egObj->options);
        $entries = $compObj->getEntriesForEgId($egId);
        $paidCnt = 0;
        foreach ($entries as $entry) {
            if ($entry->paid !== E4S_ENTRY_NOT_PAID) {
                $paidCnt++;
            }
            if ($paidCnt > $maxAthletes) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public static function getEntries($ids, bool $isTeam): array {
        $sql = 'select  e.*, 
                        ce.maxgroup egId,
                        ce.compId
                from ' . E4S_TABLE_COMPEVENTS . ' ce,';
        if ($isTeam) {
            $sql .= E4S_TABLE_EVENTTEAMENTRIES;
        } else {
            $sql .= E4S_TABLE_ENTRIES;
        }

        $sql .= ' e
            where e.id in (' . implode(',', $ids) . ')';

        if ($isTeam) {
            $sql .= ' and e.ceid = ce.id';
        } else {
            $sql .= ' and e.compeventid = ce.id';
        }

        $result = e4s_queryNoLog($sql);
        $entries = array();
        while ($obj = $result->fetch_object()) {
            $stdObj = entryClass::getStdEntryObj($obj, $isTeam);
            $entries[$stdObj->id] = $stdObj;
        }
        return $entries;
    }

    public static function getStdEntryObj($obj, $isTeam) {
        $obj->id = (int)$obj->id;
        $obj->paid = (int)$obj->paid;
        $obj->egId = (int)$obj->egId;
        $obj->compId = (int)$obj->compId;

        if (isset($obj->present)) {
            $obj->present = (int)$obj->present;
        } else {
            $obj->present = TRUE;
        }
        $obj->orderId = (int)$obj->orderid;
        unset($obj->orderid);
        $obj->price = (float)$obj->price;
        $obj->userId = (int)$obj->userid;
        unset($obj->userid);
        if ($isTeam) {
            $obj->ceId = (int)$obj->ceid;
            unset ($obj->ceid);
            $obj->entityLevel = (int)$obj->entitylevel;
            unset ($obj->entitylevel);
            $obj->entityId = (int)$obj->entityid;
            unset ($obj->entityid);
            $obj->options = e4s_getOptionsAsObj($obj->options);
        } else {
            $obj->compEventId = (int)$obj->compEventID;
            unset ($obj->compEventID);
            $obj->athleteId = (int)$obj->athleteid;
            unset ($obj->athleteid);
            $obj->bibNo = (int)$obj->bibno;
            unset($obj->bibno);
            $obj->variationId = (int)$obj->variationID;
            unset($obj->variationID);
            $obj->clubId = (int)$obj->clubid;
            unset($obj->clubid);
            $obj->discountId = (int)$obj->discountid;
            unset($obj->discountid);
            $obj->ageGroupId = (int)$obj->ageGroupID;
            unset($obj->ageGroupID);
            $obj->vetAgeGroupId = (int)$obj->vetAgeGroupID;
            unset($obj->vetAgeGroupID);
            $obj->waitingPos = (int)$obj->waitingPos;

            if (isset($obj->entryPos)) {
                $obj->entryPos = (int)$obj->entryPos;
            } else {
                $obj->entryPos = 0;
            }
            $obj->options = e4s_addDefaultEntryOptions($obj->options);
        }
        return $obj;
    }

    private function _moveEntryOffWaitingList($entry, $time) {
        $options = e4s_addDefaultEntryOptions($entry->options);
        $options->waitingInfo->cameOff = date('Y-m-d H:i:s');
        $options = e4s_removeDefaultEntryOptions($options);

        $sql = 'update ' . E4S_TABLE_ENTRIES . "
               set waitingPos = 0,
                   options = '" . e4s_getOptionsAsString($options) . "',
                   periodStart = '" . $time . "'
               where id = " . $entry->id;
        e4s_queryNoLog($sql);
        $sql = 'select *
                from ' . E4S_TABLE_ENTRYINFO . '
                where entryid = ' . $entry->id;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            $entryInfo = $result->fetch_object(E4S_ENTRYINFO_OBJ);
            $waitObj = new waitingClass($entry->compId);
            // ensure waiting list is read after this update
            if (array_key_exists(E4S_EG_COUNTS, $GLOBALS)) {
                unset($GLOBALS[E4S_EG_COUNTS][$entry->egId]);
            }
            $compObj = e4s_getCompObj($this->compId);
            if ($compObj->isBeforeCompDate()) {
                $waitObj->emailMovement($entryInfo);
                logTxt('Entry ' . $entry->id . ' has been moved and emailed');
            }
        }
    }

    public function list($obj) {
        $entityId = checkFieldForXSS($obj, 'entityid:Entity ID' . E4S_CHECKTYPE_NUMERIC);
        $this->searchParams = new entrySearchClass($obj);
        $this->entries = array();
        $this->events = array();

        // Indiv Entries
        $this->_getIndivEntries($entityId);
        // Team Entries
        $this->_getTeamEntries();

        $data = new stdClass();
        $data->entries = $this->entries;
        $data->events = $this->events;
        $compObj = e4s_GetCompObj($this->searchParams->compId);
        $data->competition = new stdClass();
        $data->competition->id = $compObj->getID();
        $data->competition->name = $compObj->getName();
        $data->competition->date = $compObj->getDate();
        $data->competition->location = $compObj->getLocationName();
        Entry4UISuccess($data);
    }

    private function _getIndivEntries($entityId = null) {
        $sql = $this->_buildSql($entityId);
//        e4s_addDebugForce($sql);
        $results = e4s_queryNoLog($sql);
        while ($obj = $results->fetch_object()) {
            $obj->team = FALSE;
            $obj = $this->_normaliseObj($obj);
            $this->events[$obj->event->id] = $obj->event;
            unset($obj->event);
            $this->entries[] = $obj;
        }
    }

    private function _buildSql($entityId = null): string {
        $sql = '
            select   e.id entryId
                    ,e.pb
                    ,e.eventAgeGroup
                    ,e.waitingPos
                    ,e.checkedin
                    ,e.userId
                    ,u.user_email userEmail
                    ,ce.id ceId
                    ,ce.isOpen  
                    ,ce.maxAthletes
                    ,ce.split
                    ,eg.id egId
                    ,eg.eventNo
                    ,eg.typeNo
                    ,eg.name eventName
                    ,eg.startDate
                    ,c.id clubId
                    ,c.clubName
                    ,a.firstName
                    ,a.surName
                    ,a.id athleteId
            from ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTGROUPS . ' eg,
                 ' . E4S_TABLE_CLUBS . ' c,
                 ' . E4S_TABLE_USERS . ' u,
                 ' . E4S_TABLE_ATHLETE . ' a
            where e.compeventid = ce.id
            and   ce.maxgroup = eg.id
            and   e.clubid = c.id
            and   e.athleteid = a.id
            and   e.userid = u.id
            and   e.paid = ' . E4S_ENTRY_PAID . '
        ';
        if (!is_null($entityId)) {
            $sql .= ' and e.entityid = ' . $entityId . ' ';
        }
        $sql .= $this->searchParams->addWhereStatement();

        return $sql;
    }

    private function _normaliseObj($obj) {
        $newObj = new stdClass();
        $entry = new stdClass();
        $entry->entryId = (int)$obj->entryId;
        $entry->waiting = (int)$obj->waitingPos;
        $entry->ceId = (int)$obj->ceId;
        $entry->egId = (int)$obj->egId;
        $entry->team = $obj->team;
        $userObj = new stdClass();
        $userObj->id = (int)$obj->userId;
        $userObj->email = $obj->userEmail;
        $entry->user = $userObj;
        $newObj->entry = $entry;

        $newObj->athletes = [$this->_normaliseAthlete($obj)];

        $event = new stdClass();
        $event->name = $obj->eventName;
        $event->startDate = e4s_sql_to_iso($obj->startDate);
        $event->isOpen = (int)$obj->isOpen === 0 ? FALSE : TRUE;
        $event->id = (int)$obj->egId;
        $event->eventNo = (int)$obj->eventNo;
        $event->typeNo = $obj->typeNo;
        $event->maxAthletes = (int)$obj->maxAthletes;
        $event->split = $obj->split;
        $newObj->event = $event;

        return $newObj;
    }

    private function _normaliseAthlete($obj) {
        $athlete = new stdClass();
        $athlete->id = (int)$obj->athleteId;
        $athlete->firstName = $obj->firstName;
        $athlete->surName = $obj->surName;
        $athlete->ageGroup = $obj->eventAgeGroup;

        $club = new stdClass();
        $club->id = (int)$obj->clubId;
        $club->name = $obj->clubName;
        $athlete->club = $club;
        return $athlete;
    }

    private function _getTeamEntries() {
        $sql = $this->_buildTeamSql();
        $results = e4s_queryNoLog($sql);
        $lastId = 0;
        while ($obj = $results->fetch_object()) {
            $obj->entryId = (int)$obj->entryId;
            $obj->team = TRUE;
            $obj->waitingPos = 0;
            if (!is_null($obj->areaName)) {
                $obj->clubName = $obj->areaName;
            }
            if ($lastId === $obj->entryId) {
                $athlete = $this->_normaliseAthlete($obj);
                $obj = $this->entries[$lastId];
                $obj->athletes[] = $athlete;
            } else {
                $lastId = $obj->entryId;
                $obj = $this->_normaliseObj($obj);
                $this->events[$obj->event->id] = $obj->event;
                unset($obj->event);
                $this->entries[$lastId] = $obj;
            }
        }
    }

    /*
      $obj takes format
      $obj->competitions[
        compName [
            {
                athlete, event, status
            }
        ]
      ]
      $obj->user {
        id,login,niceName,email,displayName
      }
     */

    private function _buildTeamSql(): string {
        $sql = '
            select   e.id entryId
                     ,club.clubName
                     ,area.name areaName
                     ,ag.name eventAgeGroup
                     ,e.entitylevel
                     ,e.entityid clubId
                     ,ce.id ceId
                     ,ce.isOpen
                     ,ce.maxAthletes
                     ,ce.split
                     ,eg.id egId
                     ,eg.eventNo
                     ,eg.typeNo
                     ,eg.name eventName
                     ,eg.startDate
                     ,a.firstName
                     ,a.surName
                     ,a.id athleteId
            from ' . E4S_TABLE_EVENTTEAMATHLETE . ' ea,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTGROUPS . ' eg,
                 ' . E4S_TABLE_AGEGROUPS . ' ag,
                 ' . E4S_TABLE_ATHLETE . ' a ,
                 (' . E4S_TABLE_EVENTTEAMENTRIES . ' e left join ' . E4S_TABLE_CLUBS . ' club on (e.entityid = club.id and e.entitylevel = 1)) left join ' . E4S_TABLE_AREA . ' area on (e.entityid = area.id and e.entitylevel > 1)   

            where e.ceid = ce.id
            and   ce.AgeGroupID = ag.id
            and   ce.maxgroup = eg.id
            and   e.id = ea.teamentryid
            and   ea.athleteid = a.id
            and   e.paid = ' . E4S_ENTRY_PAID . '
        ';

        $sql .= $this->searchParams->addTeamWhereStatement();
        return $sql;
    }

    public function putEntryInUsersAccount($productId, $userId, $paid, $isTeamEvent = FALSE, $reason = '', $force = TRUE) {
        $emailUserObj = null;
        $entryObj = $this->_getEntry($productId, $isTeamEvent);
        if (is_null($entryObj)) {
            Entry4UIError(8345, 'Invalid Entry parameters');
        }
        $compObj = e4s_getCompObj($entryObj->compId);
        if (!isE4SUser() and !$compObj->isOrganiser()) {
            Entry4UIError(9304, 'You are not authorised to use this function');
        }

        $options = e4s_addDefaultEntryOptions($entryObj->options);
        if ($reason === '') {
            $reason = 'Moved to users basket by user ' . e4s_getUserID();
        }
        $options->reason = $reason;
        if ($paid === E4S_ENTRY_PAID) {
            $force = FALSE;
            $options->fee = TRUE;

        } else {
            unset($options->fee);
            if ($userId !== 0) {
                $emailUserObj = e4sUserClass::withId($userId);
            }
        }
        $options = $this->_setForceBasket($options, $force);
        if ($userId === 0) {
            $userId = e4s_getUserID();
        }

        $options = e4s_removeDefaultEntryOptions($options);
        $sql = 'update ';
        if ($entryObj->isTeamEvent) {
            $sql .= E4S_TABLE_EVENTTEAMENTRIES;
        } else {
            $sql .= E4S_TABLE_ENTRIES;
        }
        $sql .= ' set userid = ' . $userId . ',
                      paid = ' . $paid . ',';
        if ($paid === E4S_ENTRY_PAID) {
            $sql .= ' price = 0,';
        }
        $sql .= "     options = '" . addslashes(e4s_getOptionsAsString($options)) . "'
                  where id = " . $entryObj->entryId;
        e4s_queryNoLog($sql);
//        Are we putting in another cart or marking as paid
        if ($userId !== e4s_getUserID() or $paid === E4S_ENTRY_PAID) {
            e4sProduct::removeFromWCCart($productId);
        }
        if (!is_null($emailUserObj)) {
//            $this->emailUserAbountEntries();
        }
    }

    private function _getEntry($productId, $isTeamEvent) {
        $entrySql = 'select e.id entryId
                            ,e.options';
        if (!$isTeamEvent) {
            $entrySql .= '  ,e.athlete entryName';
        } else {
            $entrySql .= '  ,e.name entryName';
        }
        $entrySql .= '      ,ce.compId compId
                            ,eg.name eventName
                     from ' . E4S_TABLE_COMPEVENTS . ' ce,
                          ' . E4S_TABLE_EVENTGROUPS . ' eg,';
        if (!$isTeamEvent) {
            $entrySql .= E4S_TABLE_ENTRIES . ' e
                    where ce.id = e.compeventid 
                    and e.variationid = ' . $productId;
        } else {
            $entrySql .= E4S_TABLE_EVENTTEAMENTRIES . ' e
                    where ce.id = e.ceid 
                    and e.productid = ' . $productId;
        }
        $entrySql .= ' and ce.maxgroup = eg.id';
        $result = e4s_queryNoLog($entrySql);
        if ($result->num_rows !== 1) {
            if ($isTeamEvent) {
                return null;
            } else {
                // isteamevent not passed so tried entries and failed, so call again with true for teamEvent
                return $this->_getEntry($productId, TRUE);
            }
        }
        $obj = $result->fetch_object();
        $obj->entryId = (int)$obj->entryId;
        $obj->compId = (int)$obj->compId;
        $obj->isTeamEvent = $isTeamEvent;
        return $obj;
    }

    private function _setForceBasket($options, $force = FALSE) {
        if ($force) {
            $options->forceBasket = $force;
        } else {
            unset($options->forceBasket);
        }
        return $options;
    }

    public function emailUserAbountEntries($obj) {
        $newline = '<br>';
        $body = 'Dear ' . $obj->user->displayName . ',' . $newline . $newline;
        $entryCount = 0;
        foreach ($obj->competitions as $entries) {
            foreach ($entries as $entry) {
                $entryCount++;
            }
        }
        $entryTxt = 'Entry';
        if ($entryCount > 1) {
            $entryTxt = 'Entries';
        }
        $body .= 'Your Entry4Sports cart has been updated with the following ' . strtolower($entryTxt) . '.' . $newline . $newline;
        $added = FALSE;
        $removed = FALSE;
        foreach ($obj->competitions as $compName => $entries) {
            $body .= 'Competition : ' . $compName . $newline;
            foreach ($entries as $entry) {
                $recipient = $entry->athlete;
                if ($recipient === '') {
                    $recipient = $entry->teamName;
                }
                $body .= 'Event ' . $entry->status . ': ' . $entry->event . '/' . $recipient . $newline;
                switch ($entry->status) {
                    case 'Added':
                        $added = TRUE;
                        break;
                    case 'Removed':
                        $removed = TRUE;
                        break;
                }
            }
        }

        if (!$added) {
            // Only removed
            $subject = $entryTxt . ' have been removed from your Entry4Sports cart.';
        } else {
            $subject = $entryTxt . ' added to your Entry4Sports cart and are awaiting payment.';
            $body .= $newline . 'To finalise your entry, please use the following link ( ';
            $body .= '<a href="https://' . E4S_CURRENT_DOMAIN . '/#/cart-redirect">Your E4S Cart</a>';
            $body .= ' ) or logon to the Entry4Sports system and complete the payment process by clicking on your cart ( top right ).' . $newline;
        }

        $body .= Entry4_emailFooter();
        e4s_mail($obj->user->email, $subject, $body, Entry4_mailHeader('', FALSE));
    }

    public function getForceBasket($options) {
        $force = FALSE;
        if (isset($options->forceBasket)) {
            $force = $options->forceBasket;
        }
        return $force;
    }

    public function checkExpiredEntries() {
        // remove those where the comp has past
        $sql = '
            select entryId, 
                   productId,
                   userId userId
            from ' . E4S_TABLE_ENTRYINFO . '
            where paid = ' . E4S_ENTRY_NOT_PAID . '
            and userid > ' . E4S_USER_NOT_LOGGED_IN . '
            and startdate < current_date
            and created < current_date';
        $result = e4s_queryNoLog($sql);
        while ($obj = $result->fetch_object()) {
            self::removeProductAndEntry((int)$obj->productId, FALSE, (int)$obj->userId, FALSE, TRUE, TRUE);
        }
        // update those that are still 'live'
        $sql = '
            update ' . E4S_TABLE_ENTRIES . '
                set periodStart = DATE_ADD(periodStart, INTERVAL ' . E4S_ENTRY_EXPIRED_MINS . ' minute)
            where paid = ' . E4S_ENTRY_NOT_PAID . '
              and userid > ' . E4S_USER_NOT_LOGGED_IN . '
              and periodStart < DATE_SUB(now(), INTERVAL ' . E4S_ENTRY_EXPIRED_MINS . ' minute);';
        e4s_queryNoLog($sql);
    }

    // force is only currently set  if checking expired entries ( Dont check entity, just remove )
    public static function removeProductAndEntry($prodId, $removeFromCart, $userId = null, $exit = TRUE, $checkDiscounts = TRUE, $force = FALSE) {
        $productModel = new stdClass();
        $productModel->exit = $exit;
        $productModel->force = $force;
        $productModel->process = E4S_PROCESS_DELETE_PRODUCT;
        $productModel->prod = new stdClass();
        $productModel->prod->id = $prodId;
        $productModel->removeFromCart = $removeFromCart;
        if (!is_null($userId)) {
            // called from cron agent to check unpaid entries,
            $productModel->userId = $userId;
        }
        $productModel->checkDiscounts = $checkDiscounts;
        new e4sProduct($productModel);
    }

    public function WCPaymentMadeForOrder($orderId) {
        $order = wc_get_order($orderId);
        $orderStatus = $order->get_status();
        $wcOrderProducts = $order->get_items();

        foreach ($wcOrderProducts as $wcOrderProduct) {
            $this->_markEntryPaid($orderStatus, $wcOrderProduct);
        }
    }

    private function _markEntryPaid($orderStatus, $wcOrderProd) {
        $orderId = $wcOrderProd->get_order_id();
        $prodId = (int)$wcOrderProd->get_product_id();
        $price = (float)$wcOrderProd->get_subtotal();
        $paid = E4S_ENTRY_NOT_PAID;
        $teamEntry = FALSE;
        $secondaryItem = FALSE;
        // select everything so we can pass to the message rather than get again
        $sql = 'select e.*,
                       ce.compid compId
                from ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce
                where variationID = ' . $prodId . '
                and   e.compeventid = ce.id';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            $sql = 'select e.*,
                       ce.compid compId
                from ' . E4S_TABLE_EVENTTEAMENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce
                where productid = ' . $prodId . '
                and   e.ceid = ce.id';
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows > 0) {
                $teamEntry = TRUE;
            } else {
                // Secondary item ????
                $secondaryItem = TRUE;
                $paid = E4S_ENTRY_PAID;
                $wcProd = wc_get_product($prodId);
                $desc = $wcProd->get_description();
                $options = '';
                if (strpos($desc, 'compid') === FALSE) {
                    Entry4UIError(9035, 'Unable to get compId from Product : ' . $prodId);
                } else {
                    $desc = e4s_getDataAsType($desc, E4S_OPTIONS_OBJECT);
                }
                $obj = new stdClass();
                $obj->compId = $desc->compid;
//                Entry4UIError(9801,"Failed to find entry for product " . $prodId);
            }
        }
        if (!$secondaryItem) {
            $obj = $result->fetch_object();
            $options = e4s_getOptionsAsObj($obj->options);
            $options->lastStatus = $orderStatus;
            if ($orderStatus === E4S_ORDER_ONHOLD) {
                $paid = E4S_ENTRY_AWAITING_PAYMENT;
            }
            if ($orderStatus === E4S_ORDER_PAID) {
                $paid = E4S_ENTRY_PAID;
            }
            if (!$teamEntry) {
                $sql = 'update ' . E4S_TABLE_ENTRIES;
            } else {
                // order line is for a team entry
                $sql = 'update ' . E4S_TABLE_EVENTTEAMENTRIES;
            }
            $sql .= '
                set paid    = ' . $paid . ',
                    orderid = ' . $orderId . ',
                    price   = ' . $price . ",
                    options = '" . e4s_getOptionsAsString($options) . "'
                where id = " . $obj->id;
            e4s_queryNoLog($sql);
        }
//        $msgObj = new e4SMessageClass();
        // add in the new values save getting again. What are these use for ?????
        $obj->paid = $paid;
        $obj->orderid = $orderId;
        $obj->price = $price;
        $obj->options = $options;
//        $msgObj->checkNewEntry($obj);
    }
}

class entrySearchClass {
    public $compId;
    public $name;
    public $teamName;
    public $eventName;
    public $clubName;
    public $date;
    public $gender;
    public $waiting;

    public function __construct($obj) {
        $this->compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
        if (!is_null($this->compId)) {
            $this->compId = (int)$this->compId;
            if ($this->compId < 1 or $this->compId > 999999) {
                $this->compId = null;
            }
        }
        $this->name = checkFieldForXSS($obj, 'name:Athlete Name');
        if (is_null($this->name)) {
            $this->name = '';
        }
        $this->teamName = checkFieldForXSS($obj, 'team:Team Name');
        if (is_null($this->teamName)) {
            $this->teamName = '';
        }
        $this->eventName = checkFieldForXSS($obj, 'event:Event Name');
        if (is_null($this->eventName)) {
            $this->eventName = '';
        }
        $this->clubName = checkFieldForXSS($obj, 'club:Club Name');
        if (is_null($this->clubName)) {
            $this->clubName = '';
        }
        $this->date = checkFieldForXSS($obj, 'date:Event Date and Time');
        if (is_null($this->date)) {
            $this->date = '';
        }
        $this->gender = checkFieldForXSS($obj, 'gender:Athlete Gender');
        if (is_null($this->gender)) {
            $this->gender = '';
        } else {
            $this->gender = strtoupper($this->gender);
        }
        if ($this->gender !== 'F' and $this->gender !== 'M') {
            $this->gender = '';
        }
        $this->waiting = checkFieldForXSS($obj, 'waiting:On Waiting List');
        if (!is_null($this->waiting)) {
            if ($this->waiting === '0') {
                $this->waiting = FALSE;
            } elseif ($this->waiting === '1') {
                $this->waiting = TRUE;
            } else {
                $this->waiting = null;
            }
        }
    }

    public function addWhereStatement() {
        $sql = '';
        if (!is_null($this->compId)) {
            $sql .= ' and ce.compid = ' . $this->compId;
        }
        if ($this->name !== '') {
            $sql .= " and e.athlete like '%" . $this->name . "%'";
        }
        if ($this->gender !== '') {
            $sql .= " and a.gender = '" . $this->gender . "'";
        }
        if ($this->eventName !== '') {
            $sql .= " and eg.name like '%" . $this->eventName . "%'";
        }
        if ($this->clubName !== '') {
            $sql .= " and c.clubName like '%" . $this->clubName . "%'";
        }
        if ($this->waiting === FALSE) {
            $sql .= ' and e.waitingpos = 0';
        }
        if ($this->date !== '') {
            $sql .= " and date(eg.startdate) = '" . $this->date . "'";
        }
        return $sql;
    }

    public function addTeamWhereStatement() {
        $sql = '';
        if (!is_null($this->compId)) {
            $sql .= ' and ce.compid = ' . $this->compId;
        }
        if ($this->name !== '') {
            $sql .= " and concat(a.firstname,a.surname) like '%" . $this->name . "%'";
        }
        if ($this->clubName !== '') {
            $sql .= " and (c.clubName like '" . $this->clubName . "%' or area.name like '" . $this->clubName . "%')";
        }
        if ($this->gender !== '') {
            $sql .= " and a.gender = '" . $this->gender . "'";
        }
        if ($this->teamName !== '') {
            $sql .= " and e.name like '%" . $this->teamName . "%'";
        }
        if ($this->eventName !== '') {
            $sql .= " and eg.name like '%" . $this->eventName . "%'";
        }
        if ($this->date !== '') {
            $sql .= " and date(eg.startdate) = '" . $this->date . "'";
        }
        $sql .= ' order by e.id,ea.pos';
        return $sql;
    }
}