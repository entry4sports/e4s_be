<?php

define('E4S_CAT_TICKET', 'ticket');
define('E4S_CAT_SHOP', 'shop');
define('E4S_ATTRIB_PREFIX', 'attribute_');
define('E4S_PERACC_STD', '');
define('E4S_PERACC_ATHLETE', 'A');
define('E4S_PERACC_USER', 'U');
define('E4S_DEFAULT_IMAGE', '/resources/default.gif');
define('E4S_PROCESS_DELETE_PRODUCT', '_delete_prod');
define('E4S_TICKET_NAME', 'E4S Ticket');
define('E4S_TICKET_URL', 'https://e4sticket');

class e4sProduct {
    public $model;
    public $compId;
    public $compObj;
    public $catArray = array();
    public $userId;
    public $checkDiscounts;
    public $exit;

    public function __construct($model, $compId = 0) {
        $suffix = 'Variation';
        $this->model = $model;
        $this->compId = $compId;
        $this->compObj = null;
        $this->exit = TRUE;
        if (isset($model->exit)) {
            $this->exit = $model->exit;
        }
        if (isset($model->process)) {
            switch ($model->process) {
                case E4S_PROCESS_DELETE_PRODUCT:
                    if (isset($model->userId)) {
                        $this->userId = $model->userId;
                    } else {
                        $this->userId = e4s_getUserID();
                    }
                    if (isset($model->checkDiscounts)) {
                        $this->checkDiscounts = $model->checkDiscounts;
                    } else {
                        $this->checkDiscounts = TRUE;
                    }
                    $force = FALSE;
                    if (isset($model->force)) {
                        $force = $model->force;
                    }
                    $this->removeProduct($force);
                    if ($this->exit) {
                        $this->exitThisObj();
                    }
                    break;
                case E4S_CRUD_DELETE:
                    break;

                case E4S_CRUD_CREATE:
                case E4S_CRUD_READ:
                case E4S_CRUD_UPDATE:
                case E4S_CRUD_READ . $suffix:
                    // Not currently Used
                    $this->model->attributes = $this->_checkAthleteInAttributes();
                    break;
                case E4S_CRUD_CREATE . $suffix:
                    $this->model->attributes = $this->_checkAthleteInAttributes();
                    $this->createVariation(TRUE);
                    break;
                case E4S_CRUD_UPDATE . $suffix:
                    $this->model->attributes = $this->_checkAthleteInAttributes();
                    $this->updateVariation(TRUE);
                    break;
                case E4S_CRUD_DELETE . $suffix:
                    $this->delete(TRUE);
                    break;
            }
        } else {
            if (!is_null($this->model)) {
                $this->model->attributes = $this->_checkAthleteInAttributes();
            }
        }
//        Entry4UIError(5000,"No Process passed",200,"");
    }

    // $force: Called from expired to force he removal without checks
    public function removeProduct($force = FALSE) {
        $prodId = $this->_getProdId();
        $entryRow = null;
        $teamEntry = FALSE;
        $athleteID = 0;

        // add order by paid incase 2 are returned as in they switched event and the old one still exists.
        // Think this is only dev but
        $sql = 'select e.*, ce.options ceoptions, ev.options eoptions , ce.compid, ce.id ceid, ce.maxGroup
                from ' . E4S_TABLE_ENTRIES . ' e left join ' . E4S_TABLE_COMPEVENTS . ' ce on e.compEventID = ce.id left join ' . E4S_TABLE_EVENTS . ' ev on  ce.EventID = ev.id
                where e.variationid = ' . $prodId . '
                order by paid';

        $result = e4s_queryNoLog($sql);

        if ($result->num_rows === 0) {
            $sql = 'select ete.*, ce.options ceoptions, ev.options eoptions , ce.compid, ce.id ceid, ce.maxGroup
                    from ' . E4S_TABLE_EVENTTEAMENTRIES . ' ete,
                         ' . E4S_TABLE_COMPEVENTS . ' ce,
                         ' . E4S_TABLE_EVENTS . ' ev 
                    where ete.productid = ' . $prodId . '
                    and   ete.ceid = ce.id
                    and   ce.EventID = ev.id
                    order by paid';
            $result = e4s_queryNoLog($sql);
        }
        if ($result->num_rows === 0) {
            return $this->exitThisObj();
        }

        $entryRow = $result->fetch_assoc();

        $process = $force;
        if (!$force) {
            // team has them separate
            if (array_key_exists('entityid', $entryRow)) {
                $entityId = $entryRow['entityid'];
                $entityLevel = $entryRow['entitylevel'];
                $entity = $entityLevel . '-' . $entityId;
            } else {
                $entity = $entryRow['entity'];
            }

            if ((int)$entryRow['userid'] === (int)$this->userId) {
                $process = TRUE;
            } elseif ($entity !== '') {
                // check if user has the entity
//                $entity = e4s_getEntityKey($entityLevel, $entityId);
                $process = e4s_doesUserHaveEntity($entity);
            }
            if (!$process) {
                return $this->exitThisObj();
            }
        }
        if ($this->compId === 0) {
            $this->compId = (int)$entryRow['compid'];
        }
        $this->compObj = e4s_getCompObj($this->compId, FALSE, FALSE);
        if (is_null($this->compObj)) {
            return $this->exitThisObj();
        }

        if ((int)$entryRow['paid'] === E4S_ENTRY_PAID and !((int)$entryRow['orderid'] === 0 and $entryRow['price'] === '0.00')) {
            return $this->exitThisObj();
        }
        if (array_key_exists('athleteid', $entryRow)) {
            $athleteID = (int)$entryRow['athleteid'];
        }

        $ceoptions = e4s_mergeAndAddDefaultCEOptions($entryRow['ceoptions'], $entryRow['eoptions'], E4S_OPTIONS_OBJECT);
        $teamEntry = $ceoptions->isTeamEvent;
        $freeEntry = FALSE;

        if ($athleteID !== 0) {
            $this->removeSecondary($athleteID, (int)$entryRow['compid']);
        }
        if (!isset($this->model->removeFromCart)) {
            e4sProduct::removeFromWCCart($prodId);
        }
        $this->removeEntry($teamEntry, (int)$entryRow['id']);
        $this->removeWCProduct($prodId);
        notesClass::delete($prodId);

        if ($this->compObj->checkClubComp() > -1 and $entity !== '') {
            $this->compObj->setClubComp(FALSE);
            $this->compObj->clubComp->renumberEntries((int)$entryRow['maxGroup'], $entity);
        }
        if ($this->checkDiscounts) {
            if (!is_null($entryRow['compid'])) { // Entry not found or issue with CE record so no compid found ( auto entries )
                if (($athleteID !== 0 and !$freeEntry) or $teamEntry) {
                    include_once E4S_FULL_PATH . 'entries/discounts.php';
                    if ($teamEntry) {
                        reApplyTeamDiscounts($entryRow['compid']);
                    } else {
                        reApplyDiscounts($athleteID, $entryRow['compid']);
                    }
                }
            }
        }

        if (!isset($this->model->removeFromCart)) {
            return $this->exitThisObj();
        }
    }

    public function _getProdId() {
        return $this->model->prod->id;
    }

    private function exitThisObj() {
        if ($this->exit) {
            if (!is_null($this->compObj)) {
                $this->compObj->setClubComp(TRUE);
                if ($this->compObj->getClubId() !== 0) {
                    $meta = new stdClass();
                    $meta->compClubInfo = $this->compObj->getClubCompData();
                    Entry4UISuccess('"meta":' . json_encode($meta, JSON_NUMERIC_CHECK), 'Removed');
                }
            }
            Entry4UIMessage('Removed');
        }
        return TRUE;
    }

    public function removeSecondary($athleteID, $compId = 0) {
        if ($compId === 0) {
            $compId = $this->compId;
        }

        $prodId = $this->_getProdId();
        // check there are no other entries in this comp for this athlete
        $sql = 'select e.id
                from ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce
                where e.compeventid = ce.id
                and   ce.compid = ' . $compId . '
                and   e.athleteid = ' . $athleteID . '
                and   e.variationid != ' . $prodId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows > 0) {
            // The athlete is entered into other events, so dont remove 2ndary products
            return FALSE;
        }

        // get list of 2ndry products
        $orderObj = e4sOrders::athleteID($compId, $athleteID);
        $orders = $orderObj->getOrdersPurchasedForAthlete();
        if (!empty($orders)) {
            Entry4UIError(4500, 'Secondary Products already purchased for this athlete.');
        }

        // Check the cart and remove items that are 2ndry for this athlete
        $cartObj = new e4sCart();
        return $cartObj->removeSecondaryFromCartForAthlete($compId, $athleteID);
    }

    public static function removeFromWCCart($prodId, $variationid = 0) {
        foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
            if ((int)$cart_item['product_id'] === $prodId) {
                if ($variationid === 0 or $variationid === (int)$cart_item['variation_id']) {
                    $cart_key = $cart_item['key'];
                    try {
                        WC()->cart->remove_cart_item($cart_key);
                        unset(WC()->cart->cart_contents[$cart_key]);
                    } catch (Exception $err) {

                    }
                }
            }
        }
    }

    public function removeEntry($teamEntry, $entryId) {
        $prodId = $this->_getProdId();
        if (!$teamEntry) {
            $deleteEntrySQL = 'delete from ' . E4S_TABLE_ENTRIES . '
               where variationid = ' . $prodId . "
               and(
                  options like '%\"otd\":true%'
                  or 
                  paid = " . E4S_ENTRY_NOT_PAID;

            if ($this->compObj->markFreeEntryPaidCheck()) {
                $deleteEntrySQL .= ' or price = 0';
            }
            $deleteEntrySQL .= ' )';

            e4s_queryNoLog($deleteEntrySQL);
        } else {
            include_once E4S_FULL_PATH . 'eventteams/commonEventTeam.php';
            e4s_removeEventTeam($entryId);
        }
    }

    public function removeWCProduct() {
        $prodId = $this->_getProdId();

        include_once E4S_FULL_PATH . 'product/commonProduct.php';
        e4s_removeProduct($prodId);
    }

    public function delete($exit = FALSE) {
        $model = $this->model;

        $product = wc_get_product($model->id);
        if (empty($product)) {
            if ($exit) {
                $this->_error(4000, 'Unable to get product', TRUE);
            }
        }
        if ($product->get_total_sales() > 0) {
            if ($exit) {
                $this->_error(4010, 'Unable to delete product. Has sales', TRUE);
            }
        }
        $result = $this->_deleteProduct($product);
        if ($result) {
            $result = 'true';
        } else {
            $result = 'false';
        }
        if ($exit) {
            Entry4UISuccess('
                "data":{"deleted":' . $result . '}');
        }
        return $result;
    }

    // Create a Variation product for a parent

    private function _error($err, $str, $exit = FALSE) {
        if ($exit) {
            Entry4UIError($err, $str, 200, '');
        }
        return FALSE;
    }

    private function _deleteProduct($product, $force = FALSE) {
//        $this->_debug("delete prod", $product);
        // If we're forcing, then delete permanently.
        if ($force) {
            if ($product->is_type('variable')) {
                foreach ($product->get_children() as $child_id) {
                    $child = wc_get_product($child_id);
                    $child->delete(TRUE);
                }
            } elseif ($product->is_type('grouped')) {
                foreach ($product->get_children() as $child_id) {
                    $child = wc_get_product($child_id);
                    $child->set_parent_id(0);
                    $child->save();
                }
            }

            $product->delete(TRUE);
            $result = $product->get_id() > 0 ? FALSE : TRUE;
        } else {
            $product->delete();
            $result = WC_POST_DELETED === $product->get_status();
        }

        if (!$result) {
            return FALSE;
        }

        // Delete parent product transients.
        if ($parent_id = wp_get_post_parent_id($product->get_id())) {
            wc_delete_product_transients($parent_id);
        }
        return TRUE;
    }

//    return id of product

    private function _checkAthleteInAttributes() {
        $athleteAttributeKey = null;
        $nameKey = 'name';
        $valueKey = 'values';
        $value = 'Please select athlete';

        if (isset($this->model->attributes)) {
            $attributeArray = $this->model->attributes;
        } else {
            $attributeArray = array();
        }

        foreach ($attributeArray as $key => $attribute) {
            if (strcasecmp($attribute[$nameKey], E4S_ATTRIB_ATHLETE) === 0) {
                $athleteAttributeKey = $key;
                break;
            }
        }
        if (isset($this->model->perAcc)) {
            if (e4s_isPerAcc(E4S_PERACC_ATHLETE, $this->model->perAcc)) {
                if (!is_null($athleteAttributeKey)) {
                    $attributeArray[$athleteAttributeKey][$valueKey] = $value;
                } else {
                    $attributeArray[] = array($nameKey => E4S_ATTRIB_ATHLETE, $valueKey => $value);
                }
            } else {
                unset($attributeArray[$athleteAttributeKey]);
            }
        }
        return $attributeArray;
    }

    public function createVariation($exit = TRUE) {
        $model = $this->model;

        if ($model->prod->parentId === 0) {
            $this->_error(6000, 'Can not create a product without a parent');
        }
        $parentId = $model->prod->parentId;
        if ($model->prod->id === 0) {
            $this->_error(6010, 'Can not create a product with an id');
        }
//$this->_debug("createVariation", $this->model, true);

        // Get the Variable product object (parent)
        $parentProduct = wc_get_product($parentId);

        if ($parentProduct === FALSE) {
            $this->_error(6015, 'Invalid parent passed', TRUE);
        }

        $model->prod->id = $this->_createVariationForParent($parentProduct, $model->prod);

        $this->_updateImage($parentProduct);
        $parentProduct->save();
//        $this->_debug("attributes", $attributes);

        // sync all menu order fields accordingly
        $this->_setMenuOrder($parentId, $model->prod->id, $model->prod->menuOrder);
        $retProd = new stdClass();
        $retProd->prod = $model->prod;
        if ($exit) {
            Entry4UISuccess('
                "data":' . json_encode($retProd, JSON_NUMERIC_CHECK));
        }
        return $model;
    }

    public function _createVariationForParent($parentProduct, $prod) {
        $parentId = $parentProduct->get_id();
        $variation_post = array('post_title' => $parentProduct->get_name(), 'post_name' => 'product-' . $parentId . '-variation', 'post_status' => WC_POST_PUBLISH, 'post_parent' => $parentId, 'post_type' => 'product_variation', 'menu_order' => $prod->menuOrder, 'guid' => $parentProduct->get_permalink());

        // Creating the product variation
        $variation_id = wp_insert_post($variation_post);
        update_post_meta($variation_id, '_variation_description', $prod->description);
        $prod->id = $variation_id;
//$this->_debug("createVariation 2", $model, true);
        $this->_addQtyInfo($prod);

        foreach ($prod->attributeValues as $attribute) {
            $wcAttrName = sanitize_title($this->_addAttributePrefix($attribute['name']));
            $value = $attribute['value'];
            if (strtolower($attribute['name']) === E4S_ATTRIB_ATHLETE) {
                $value = '';
            }
            update_post_meta($variation_id, $wcAttrName, $value);
        }
        $varProd = wc_get_product($variation_id);
        $this->_setDownloadFile($varProd);
        $varProd->save();
        return $variation_id;
    }

    private function _addQtyInfo($model) {
        $post_id = $model->id;
        $price = $model->price->price;
        $salePrice = $model->price->salePrice;
        $saleEndDate = $model->price->saleEndDate;

        $downloadable = 'no';
        if (isset($model->download)) {
            if ($model->download) {
                $downloadable = 'yes';
            }
        }

        $virtual = 'no';
        if (isset($model->virtual)) {
            if ($model->virtual) {
                $virtual = 'yes';
            }
        }
        update_post_meta($post_id, '_manage_stock', 'yes');
        update_post_meta($post_id, '_backorders', 'no');
        update_post_meta($post_id, '_downloadable', $downloadable);
        update_post_meta($post_id, '_virtual', $virtual);
        update_post_meta($post_id, '_product_version', '4.2.0');
        update_post_meta($post_id, '_stock', $model->stockQty);
        update_post_meta($post_id, '_regular_price', $price);
        if ($saleEndDate === '') {
            update_post_meta($post_id, '_price', $price);
        } else {
            update_post_meta($post_id, '_price', $salePrice);
        }

        if ($salePrice < $price and $saleEndDate !== '') {
            update_post_meta($post_id, '_sale_price', $salePrice);
            if ($saleEndDate !== '') {
                $date = $this->getTimeStamp($saleEndDate);
                update_post_meta($post_id, '_sale_price_dates_to', $date);
            }
        }
    }

    private function getTimeStamp($dateStr) {
        $date = new DateTime($dateStr);
        return $date->getTimestamp();
    }

    private function _addAttributePrefix($name) {
        return e4sProduct::addAttributePrefix($name);
    }

    public static function addAttributePrefix($name) {
        if (strncmp($name, E4S_ATTRIB_PREFIX, strlen(E4S_ATTRIB_PREFIX)) === 0) {
//            prefix already included
            return $name;
        }
        return E4S_ATTRIB_PREFIX . $name;
    }

    public function _setDownloadFile($prod) {
        if (isset($this->model->download)) {
            if ($this->model->download) {
                $download_id = md5(E4S_TICKET_URL);
// Creating an empty instance of a WC_Product_Download object
                $pd_object = new WC_Product_Download();

// Set the data in the WC_Product_Download object
                $pd_object->set_id($download_id);
                $pd_object->set_name(E4S_TICKET_NAME);
                $pd_object->set_file(E4S_TICKET_URL);

                $downloads = array();
// Add the new WC_Product_Download object to the array
                $downloads[] = $pd_object;
                $prod->set_downloads($downloads);
            }
        }
    }

    private function _updateImage($product) {
        $model = $this->model;
        if (isset($model->prod)) {
            $model = $model->prod;
        }
        $image_id = (int)$product->get_image_id();
        if (!isset($model->image) or $model->image === '' or $model->image === E4S_DEFAULT_IMAGE) {
            // remove image from product
            $product->set_image_id('');
            return;
        }

        if (strpos($model->image, '/' . $image_id . '/') > 0) {
            // image not changed
            return;
        }
        $arr = explode('/', $model->image);

        // update products image
        $product->set_image_id((int)$arr[3]);
    }

    private function _setMenuOrder($parentId, $productId, $setMenuOrder) {
        if ($parentId < 1) {
            $this->_error(7010, 'Invalid Parent for Menu Order Sequence.', TRUE);
        }
        if ($productId < 1) {
            $this->_error(7020, 'Invalid Product for Menu Order Sequence.', TRUE);
        }
//        Get All child Products
        $sql = 'select ID id, menu_Order menuOrder
                from ' . E4S_TABLE_POSTS . '
                where post_parent = ' . $parentId . '
                order by menu_order';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            $this->_error(7030, 'No variations found in Menu Order Sequence.', TRUE);
        }
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $useMenuOrder = 1;
        if ($setMenuOrder === 1) {
            $useMenuOrder = 2;
        }
        foreach ($rows as $row) {
//            echo "\n" . $useMenuOrder;
            if ((int)$row['id'] !== $productId) {
                if ((int)$row['menuOrder'] !== $useMenuOrder) {
                    $updateSql = 'update ' . E4S_TABLE_POSTS . '
                                  set menu_order = ' . $useMenuOrder . '
                                  where id = ' . $row['id'];
                    e4s_queryNoLog($updateSql);
                }
                $useMenuOrder++;
            }

            if ($useMenuOrder === $setMenuOrder) {
                $useMenuOrder++;
            }
        }
//        echo "\nUse = " . $useMenuOrder;
//        echo "\nSet = " . $setMenuOrder;
//        echo "\nProd = " . $productId;

        if ($useMenuOrder > $setMenuOrder) {
            $useMenuOrder = $setMenuOrder;
        }
//        echo "\nUse = " . $useMenuOrder;
        $updateSql = 'update ' . E4S_TABLE_POSTS . '
                      set menu_order = ' . $useMenuOrder . '
                      where id = ' . $productId;
        e4s_queryNoLog($updateSql);
    }

    public function updateVariation($exit = FALSE) {
        $model = $this->model->prod;
        $product = wc_get_product($model->id);

        $model->soldQty = (int)$product->get_total_sales();
//        $variation = new WC_Product_Variation( $model->id );
//       var_dump($variation->get_attribute_summary());
//       exit();
        $this->_updateImage($product);

        $price = $model->price->price;
        $salePrice = $model->price->salePrice;
        $saleEndDate = $model->price->saleEndDate;
        if ($product === FALSE) {
            $this->_error(6500, 'Failed to get product to update');
        }
        $attribs = array();

        foreach ($model->attributeValues as $attribute) {
            $attribs [$attribute['name']] = $attribute['value'];
        }

        if ($model->soldQty === 0) {
            $product->set_attributes($attribs);
        } else {
//            If Attribs have changed, error
            $existingAttribs = $product->get_attributes();

            foreach ($existingAttribs as $name => $value) {
                if ($attribs[$name] !== $value) {
                    $this->_error(6510, 'Unable to change attributes once selling has begun.', TRUE);
                }
            }
        }

        $product->set_description($model->description);
        $product->set_stock($model->stockQty);

//        allow price changes only if not sold any
//        if ( $model->soldQty === 0 ) {
        $product->set_price($price);
        $product->set_regular_price($price);
        $product->set_sale_price(null);
        $product->set_date_on_sale_to(null);

        if ($salePrice < $price) {
            if ($saleEndDate !== '') {
                $date = $this->getTimeStamp($saleEndDate);
                $now = $this->getTimeStamp('');
                if ($date > $now) {
                    $product->set_price($salePrice);
                    $product->set_sale_price($salePrice);
                    $product->set_date_on_sale_to($date);
                }
            }
        }
//        }

        $product->save();
        $this->_setMenuOrder($model->parentId, $model->id, $model->menuOrder);

        $retProduct = new stdClass();
        $retProduct->prod = $model;
        if ($exit) {
            Entry4UISuccess('
                "data":' . json_encode($retProduct, JSON_NUMERIC_CHECK));
        }
        return $model;
    }

    public static function getAthleteInfoFromVariations($variations, $justId = TRUE) {
        $athleteInfo = null;
        foreach ($variations as $key => $value) {
            if ($key === E4S_ATTRIB_PREFIX . E4S_ATTRIB_ATHLETE) {
                $athleteInfo = $value;
                if ($justId) {
                    $athleteInfo = e4sProduct::getAthleteAttributeId($value);
                }
            }
        }
        return $athleteInfo;
    }

    public static function getAthleteAttributeId($value) {
        $divider = ': ';
        $values = explode($divider, $value);
        $retval = 0;
        if (sizeof($values) === 2) {
            $retval = (int)$values[0];
        }
        return $retval;
    }

    public static function getAthleteAttributeKey($sql = FALSE, $id = 0, $firstname = '', $surname = '') {
        $divider = ': ';
        if ($sql) {
            $retval = "concat(a.id,'" . $divider . "',a.firstname,' ',a.surname)";
        } else {
            $retval = $id . $divider . $firstname;
            if ($surname !== '') {
                $retval .= ' ' . $surname;
            }
        }
        return $retval;
    }

    public function createProduct() {
        $model = $this->model;
//$this->_debug("createProduct 1", $model, true);
        $productType = 'simple';
        if (isset($model->attributes) and !empty($model->attributes)) { //and !$this->isDownloadable() ??
            $productType = 'variable';
        }

        $model->id = $this->baseProduct($productType, $model->name, $model->description);
        $this->_addQtyInfo($model);
        $this->_addAttributes($model);
        $this->_setOther();
        $this->_updateText($model);
        $product = wc_get_product($model->id);
        $this->setProductCategory($product);
        $this->_updateImage($product);
        $this->_createAthleteVariation($product);
        $product->save();
        $this->_setDownloadFile($product);
        $product->save();

        return $model;
    }

    private function baseProduct($type, $name, $description) {
        $post_id = wp_insert_post(array('post_author' => 1, 'post_title' => $name, 'post_content' => $description, 'post_status' => WC_POST_PUBLISH, 'post_type' => 'product',));
        wp_set_object_terms($post_id, $type, 'product_type');
        return $post_id;
    }

    private function _addAttributes($passedModel = null) {
        $model = $this->_useModel($passedModel);

//$this->_debug("_addAttributes 1", $model);
        if (isset($model->attributes)) {
            $i = 0;
            // Loop through the attributes array

            foreach ($model->attributes as $attribute) {
                $attribName = htmlspecialchars(stripslashes($attribute['name']));
                wp_set_object_terms($model->id, $attribute['values'], $attribName, TRUE);
                $product_attributes[$attribName] = array('name' => $attribName, // set attribute name
                    'value' => $attribute['values'], // set attribute value
                    'position' => $i, 'is_visible' => '1', 'is_variation' => '1', 'is_taxonomy' => '0');

                $i++;
            }
//$this->_debug("_addAttributes 2", $product_attributes);
            if ($i > 0) {
                update_post_meta($model->id, '_product_attributes', $product_attributes);
            }
        }
    }

    private function _useModel($passedModel = null) {
        if (is_null($passedModel)) {
            return $this->model;
        }
        return $passedModel;
    }

    private function _setOther() {

    }

    private function _updateText($model) {
        notesClass::delete(E4S_TICKET_PREFIX . $model->id);

        if (!isset($model->ticket)) {
            return null;
        }
        $ticket = $model->ticket;

        if (isset($ticket->ticketText)) {
            notesClass::set(E4S_TICKET_PREFIX . $model->id, E4S_COMP_TICKET, $ticket->ticketText);
        }

        if (isset($ticket->ticketFormText)) {
            notesClass::set(E4S_TICKET_PREFIX . $model->id, E4S_COMP_TICKETFORM, $ticket->ticketFormText);
        }
        return $ticket;
    }

    public function setProductCategory($product) {
        $catName = 'comp' . $this->compId;
        $suffix = '-' . $this->compId;
        $compCatId = $this->getCategory($catName, null);
//        $this->_debug("compCatId",$compCatId,false);
//        $this->_debug("catName",$catName,false);
        $shopCatId = $this->getCategory(E4S_CAT_SHOP . $suffix, $compCatId);
//        $this->_debug("shopCatId",$shopCatId,false);
        $catArray = array($shopCatId);

        if ($this->isDownloadable()) {
            $catName = E4S_CAT_TICKET . $suffix;
            $ticketCatId = $this->getCategory($catName, $shopCatId);
//            $this->_debug("ticketCatId",$ticketCatId,false);
//            $this->_debug("catName",$catName,false);
            $catArray[] = $ticketCatId;
        }
//        $this->_debug("catArray",$catArray,true);
        $product->set_category_ids($catArray);
    }

    public function getCategory($cat_name, $parent = null) {
        $key = $cat_name . $parent;
        // check array for performance
        if (array_key_exists($key, $this->catArray)) {
            return $this->catArray[$key];
        }
        $id = term_exists($cat_name, 'product_cat', $parent);
        if (is_null($id)) {
            $id = $this->createCategory($cat_name, $parent);
        }
        if (is_array($id)) {
            $id = $id['term_id'];
        }
        $this->catArray[$key] = $id;
        return $id;
    }

    public function createCategory($catName, $parent = 0) {
        $data = array('name' => $catName, 'slug' => $catName, //            'description' => $this->row['Name'],
            'parent' => $parent, 'display' => 'default', 'image' => '',);

        try {
            $data = apply_filters('woocommerce_api_create_product_category_data', $data, $this);
            $insert = wp_insert_term($catName, 'product_cat', $data);
            if (is_wp_error($insert)) {
                // $insert->get_error_message()
                Entry4UIError(9108, 'Product category already exists/not created.', 200, '');
            }

            $id = $insert['term_id'];

            update_term_meta($id, 'display_type', 'default' === $data['display'] ? '' : sanitize_text_field($data['display']));

            do_action('woocommerce_api_create_product_category', $id, $data);

        } catch (Exception $err) {
            Entry4UIError(9109, $err->getMessage(), 200, '');
        }
        return $id;
    }

    public function isDownloadable() {
        return $this->model->download;
    }

    public function _createAthleteVariation($parentProduct) {
        if (!isset($this->model->attributes)) {
            return;
        }

        $varProdModel = new stdClass();
        $varProdModel->is = 0;
        $varProdModel->download = $this->model->download;
        $varProdModel->virtual = $this->model->virtual;
        $varProdModel->menuOrder = 1;
        $varProdModel->stockQty = $this->model->stockQty;
        $varProdModel->description = $this->model->description;
        $varProdModel->attributeValues = $this->_convertAttributeValues($this->model->attributes);
        $varProdModel->price = $this->model->price;
//        echo "Check and create Athlete ?\n";
//        var_dump($varProdModel);
//        exit();
        $varProdModel->id = $this->_createVariationForParent($parentProduct, $varProdModel);
    }

    public function _convertAttributeValues($attributes) {
        $attribValues = array();
        foreach ($attributes as $attribute) {
//            $attribute['value'] = $attribute['values'];
            $attribute['value'] = '';
            unset ($attribute['values']);
            $attribValues[] = $attribute;
        }
        return $attribValues;
    }

    public function update($exit = FALSE) {
        $model = $this->model;

        $product = wc_get_product($model->id);
        $this->_forceProductType($model, $product);
//$this->_debug("ProdUpdate 1",$product->get_type());
        $this->_updateImage($product);
        $product->set_name($model->name);
        $product->set_description($model->description);
        $product->set_short_description($model->description);
        $retval = $product->save();
        if ($retval !== $model->id) {
            $this->_error(7100, 'Failed to update product');
        }
        $this->_addQtyInfo($model);
        $this->_addAttributes($model);
        $this->_updateText($model);
//$this->_debug("ProdUpdate 2",$postArray);

        if ($exit) {
            Entry4UISuccess($model);
        }
        return TRUE;
    }

    private function _forceProductType($passedModel = null, $product = null) {
        $model = $this->_useModel($passedModel);
        if (is_null($product)) {
            $product = wc_get_product($model->id);
        }
        $prodType = $product->get_type();

        if (!empty($model->attributes) and $prodType !== 'variable') {
            // Ensure product is a variable
            wp_set_object_terms($model->id, 'variable', 'product_type');
        }
        if (empty($model->attributes) and $prodType !== 'simple') {
            // Ensure product is a variable
            wp_set_object_terms($model->id, 'simple', 'product_type');
        }
    }

    public function read($obj = null, $exit = FALSE) {
        $model = new stdClass();

        if (is_null($obj)) {
            $obj = $this->model->id;
        }

        if (gettype($obj) === 'object') {
            $product = $obj;
        } else {
            $product = wc_get_product($obj);
            if ($product === FALSE) {
                if ($exit) {
                    Entry4UISuccess('
                        "data":' . json_encode(null, JSON_NUMERIC_CHECK));
                }
                return null;
            }
        }

        $id = (int)$product->get_id();
        $prodType = $product->get_type();
        $image_id = (int)$product->get_image_id();
        $image_url = E4S_DEFAULT_IMAGE;
        if ($image_id !== 0) {
            $image_url = $this->_getImageURL($image_id);
        }
        $model->image = $image_url;

        if ($product->get_status() !== 'trash') {
            $model->id = (int)$id;
            $model->download = $product->is_downloadable();
            $model->virtual = $model->download;
            if ($model->download) {
                $model->image = e4sTicket::getTicketIcon();
            }
            $model->ticket = $this->_getTicketText($model->id);
            $model->parentId = $product->get_parent_id();
	        $model->soldQty = 0;
//	        $model->stockQty = get_post_meta($id, '_stock', TRUE);
	        $model->stockQty = 0;
            $model->name = $product->get_title();
            $description = $product->get_description();

            if ($prodType === 'variation') {
                if ($description !== '') {
                    $model->name = $description;
                    if (strpos($description, '{') !== FALSE) {
                        $descObj = e4s_getDataAsType($description, E4S_OPTIONS_OBJECT);
                        if (isset($descObj->ticket) and $prodType === 'variation') {
                            $model->name = $descObj->ticket;
                        }
	                    if (isset($descObj->soldQty)) {
		                    $model->soldQty = $descObj->soldQty;
	                    }
                    }
                }
            }
	        if ($prodType === 'simple') {
		        if ($description !== '') {
			        if (strpos($description, '{') !== FALSE) {
				        $descObj = e4s_getDataAsType($description, E4S_OPTIONS_OBJECT);
				        if (isset($descObj->soldQty)) {
					        $model->soldQty = $descObj->soldQty;
				        }
			        }
		        }
	        }
            $model->description = '';
            if (strpos($description, '}') === FALSE) {
                $model->description = $description;
            }

//            $model->soldQty = $product->get_total_sales();

            $price = new stdClass();
            $price->price = $product->get_regular_price();
            $price->salePrice = $product->get_sale_price();
            $date = get_post_meta($id, '_sale_price_dates_to', TRUE);
            $price->saleEndDate = '';
            if ($date !== '') {
                $price->saleEndDate = date('c', $date);
            }

            $model->price = $price;

            if ($prodType === 'variable') {
                $prod_attributes = get_post_meta($id, '_product_attributes', TRUE);
                if ($prod_attributes !== '') {
//$this->_debug("read 2",$prod_attributes, false);
                    $attributes = array();
                    foreach ($prod_attributes as $prod_attribute) {
                        $attribute = new stdClass();
                        $attribute->name = $prod_attribute['name'];
                        $attribute->values = $prod_attribute['value'];
                        $attributes[] = $attribute;
                    }
                    $model->attributes = $attributes;
                }
            }
            if ($prodType === 'variation') {
                $variationArr = array();
                $attributes = $product->get_variation_attributes();
                $parent_attributes = get_post_meta($model->parentId, '_product_attributes', TRUE);

                foreach ($parent_attributes as $parent_attribute) {
                    foreach ($attributes as $name => $value) {
//$this->_debug("sanitized",sanitize_title("attribute" . $parent_attribute['name']), false);

                        if (sanitize_title(E4S_ATTRIB_PREFIX . $parent_attribute['name']) === $name) {
                            $attrArr = new stdClass();
                            $attrArr->name = $parent_attribute['name'];
                            $attrArr->value = $value;
                            $variationArr[] = $attrArr;
                            continue;
                        }
                    }
                }
                $model->attributeValues = $variationArr;
            }
        }

        if ($prodType !== 'variation') {
            if ($prodType === 'variable') {
                $variationsArr = array();
                foreach ($product->get_children() as $varid) {
//$this->_debug("CHILD->", $varid, true);
                    $childObj = $this->read($varid, FALSE);
                    unset($childObj->ticket);
                    $model->download = $childObj->download;
                    $model->virtual = $childObj->download;
                    if (!is_null($childObj)) {
                        $variationsArr[] = $childObj;
                    }
//$this->_debug("read var:" . $varid,$this->read($varid, false), true);
                }
                if (!empty($variationsArr)) {
                    $model->variations = $variationsArr;
                }
            }
        }
        if (empty((array)$model)) {
            $model = null;
        }
        if ($exit) {
            Entry4UISuccess('
                "data":' . json_encode($model, JSON_NUMERIC_CHECK));
        }
        return $model;
    }

    private function _getImageURL($id) {
        $row = $this->_getAttachment($id);

        $path = '/wp-json/view/';
        return $path . $id . '/' . $row['code'];
    }

    private function _getAttachment($id) {
        $attachSql = 'select UNIX_TIMESTAMP(code) code
              from ' . E4S_TABLE_ATTACHMENTS . '
             where wpattachid = ' . $id;

        $result = e4s_queryNoLog($attachSql);
        if ($result->num_rows === 0) {
            $this->_setAttachment($id);
            $result = e4s_queryNoLog($attachSql);
        }
        return $result->fetch_assoc();
    }

    private function _setAttachment($id) {
        $options = '';
        $insertSql = 'insert into ' . E4S_TABLE_ATTACHMENTS . ' (wpattachid, options)
                  values (' . $id . ",'" . $options . "')";
        e4s_queryNoLog($insertSql);
    }

    private function _getTicketText($productId) {
        $ticket = new stdClass();
        $info = notesClass::get(E4S_TICKET_PREFIX . $productId, E4S_COMP_TICKET);
        $ticket->ticketText = e4s_htmlToUI($info);
        $info = notesClass::get(E4S_TICKET_PREFIX . $productId, E4S_COMP_TICKETFORM);
        $ticket->ticketFormText = e4s_htmlToUI($info);
        return $ticket;
    }

    private function _removeAttributePrefix($name) {
        return e4sProduct::removeAttributePrefix($name);
    }

    public static function removeAttributePrefix($name) {
        return str_replace(E4S_ATTRIB_PREFIX, '', $name);
    }

    private function _debug($msg, $obj, $exit = TRUE) {

        echo $msg . "\n";
        var_dump($obj);
        if ($exit) {
            exit();
        }
    }
}

function e4s_isPerAcc($perAcc, $value) {
//    Both are blank
//    echo "perAcc = [{$perAcc}]\n";
//    echo "value = [{$value}]\n";
    if ($value === '' and $perAcc === '') {
        return TRUE;
    }
//    One is blank
    if ($value === '' or $perAcc === '') {
        return FALSE;
    }
    $len = strlen($value);
    if (strlen($perAcc) < $len) {
        $len = strlen($perAcc);
    }
    if (strncasecmp($perAcc, $value, $len) === 0) {
        return TRUE;
    }
    return FALSE;
}