<?php
define('E4S_FINANCIAL_STATUS_REFUNDED', 'Refunded');
define('E4S_FINANCIAL_STATUS_NOATHLETE', 'No Athlete');
define('E4S_FINANCIAL_STATUS_NOENTRY', 'No Entry');
define('E4S_FINANCIAL_STATUS_CEEVENT', 'No C-Event');
define('E4S_FINANCIAL_STATUS_MANUAL', 'Manual Entry');

class financialClass {
    public $compid;
    public $products;
    public $productIds;
    public $productCnt;
    public $paidEntryCnt;
    public $athletes;
    public $athleteIds;
    public $compEvents;
    public $ceIds;
    public $orderIdsByProduct;
    public $orders;
    public $refunds;
    public $exceptions;
    public $compObj;
    public $priceObj;
    public $summaryObj;
    public $lines;
    public $e4sUser;
    public $couponsUsed;

    public function __construct($compid) {
        $this->e4sUser = isE4SUser();
        $this->compid = $compid;
        $this->compObj = e4s_GetCompObj($compid, TRUE);
        if (!is_null($this->compObj->row)) {
            $this->priceObj = $this->compObj->priceObj;
            $this->init();
        } else {
            Entry4UIError(8010, 'No competition found');
        }
    }

    private function init() {
        $this->athletes = array();
        $this->ceIds = array();
        $this->productIds = array();
        $this->exceptions = array();
        $this->summaryObj = null;
        $this->refunds = FALSE;
        $this->couponsUsed = FALSE;

        $this->_getProducts();
        $this->_getAthletes();
        $this->_getCompEvents();
        if (is_null($this->compEvents)) {
            Entry4UIError(8000, 'Competition has no Events setup.');
        }

        $this->_getEntries();
        $this->_getOrders();
        $this->_getRefunds();
        $this->_generateLines();
    }

    private function _getProducts() {
        $sql = '
            SELECT ID id, post_content
            FROM ' . E4S_TABLE_POSTS . " 
            WHERE post_excerpt = 'compid:" . $this->compid . "'
        ";
        $result = e4s_queryNoLog($sql);
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $products = array();
        foreach ($rows as $row) {
            $product = new stdClass();
            $product->config = e4s_getDataAsType($row['post_content'], E4S_OPTIONS_OBJECT);
            if (is_null($product->config)) {
                // Issue with Content ( Invalid Json String or something )
                $this->_addException('No Config', $row);
            } else {
                $productId = (int)$row['id'];
                $product->productId = $productId;
                $product->wcProduct = wc_get_product($productId);
                $products[$productId] = $product;
            }
        }
        $this->products = $products;
        $this->_getDataFromProducts();
    }

    private function _addException($name, $obj, $key = '') {
        if (!array_key_exists($name, $this->exceptions)) {
            $this->exceptions[$name] = array();
        }
        $saveObj = new stdClass();
        $saveObj->obj = $obj;
        $saveObj->key = $key;
        $this->exceptions[$name][] = $saveObj;
    }

    private function _getDataFromProducts() {
        $blankArray = array();
        if (empty($this->products)) {
            $this->athleteIds = $blankArray;
            $this->ceIds = $blankArray;
            return;
        }
        $athleteIds = $blankArray;
        $ceIds = $blankArray;
        foreach ($this->products as $productId => $product) {
            $this->productIds[] = $productId;
            // get a unique list of athleteids
            if (!is_null($product->config)) {
                $athleteid = $product->config->athleteid;
                $athleteIds[$athleteid] = $athleteid;

                $ceid = $product->config->ceid;
                $ceIds[$ceid] = $ceid;
            }
        }
        $this->athleteIds = $athleteIds;
        $this->ceIds = $ceIds;
    }

    private function _getAthletes($athleteids = null) {
        if (is_null($athleteids)) {
            $athleteids = $this->athleteIds;
        }
        $sql = "select a.id athleteid, clubid clubid, clubname clubname, firstname, surname, concat(firstname, ' ', surname) fullname
                from " . E4S_TABLE_ATHLETE . ' a left join ' . E4S_TABLE_CLUBS . ' c on (a.clubid = c.id )
                where a.id in (' . implode(',', $athleteids) . ')';
        $result = e4s_queryNoLog($sql);

        while ($row = $result->fetch_assoc()) {
            $athleteObj = new stdClass();
            $athleteObj->athleteId = (int)$row['athleteid'];
            $athleteObj->firstName = $row['firstname'];
            $athleteObj->surName = $row['surname'];
            $athleteObj->fullName = $row['fullname'];

            $club = new stdClass();
            $club->clubId = (int)$row['clubid'];
            $club->clubName = $row['clubname'];
            $athleteObj->club = $club;
            $this->athletes[$athleteObj->athleteId] = $athleteObj;
        }
    }

    private function _getCompEvents($ceIds = null) {
        if (is_null($ceIds)) {
            $ceIds = $this->ceIds;
        }
        $sql = 'select e.name name, e.id eventid, ce.id ceid
                from ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTS . ' e
                where ce.eventid = e.id
                and   ce.id in (' . implode(',', $ceIds) . ')';
//        echo $sql;
        $result = e4s_queryNoLog($sql);
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        foreach ($rows as $row) {
            $eventObj = new stdClass();
            $eventObj->eventid = (int)$row['eventid'];
            $eventObj->eventName = $row['name'];
            $this->compEvents [(int)$row['ceid']] = $eventObj;
        }
    }

    private function _getEntries() {
        $entryOnlyAthletes = array();
        $entryOnlyCEIDs = array();
        $sql = 'select e.*
                from ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce
                where e.compeventid = ce.id
                and   e.paid = ' . E4S_ENTRY_PAID . '
                and   ce.compid = ' . $this->compid;
        $results = e4s_queryNoLog($sql);
        $rows = $results->fetch_all(MYSQLI_ASSOC);
        $this->paidEntryCnt = sizeof($rows);
        $this->productCnt = sizeof($this->products);

        foreach ($rows as $row) {
            $productId = (int)$row['variationID'];
            $athleteId = (int)$row['athleteid'];
            $ceId = (int)$row['compEventID'];
            if (!isset($this->products[$productId])) {
                $this->products[$productId] = new stdClass();
                $this->products[$productId]->wcProduct = null;
                $config = new stdClass();
                $config->athleteid = $athleteId;
                $config->ceid = $ceId;
                $config->compid = $this->compid;
                $this->products[$productId]->config = $config;
            }

            $entryObj = new stdClass();
            $entryObj->entryId = (int)$row['id'];
            $entryObj->ceId = $ceId;
            $entryObj->clubId = (int)$row['clubid'];
            $entryObj->orderId = (int)$row['orderid'];
            $entryObj->price = (float)$row['price'];
            $entryObj->discountId = (int)$row['discountid'];
            $entryObj->ageGroupID = (int)$row['ageGroupID'];
            $entryObj->vetAgeGroupID = (int)$row['vetAgeGroupID'];
            $entryObj->options = e4s_getDataAsType($row['options'], E4S_OPTIONS_OBJECT);
            $entryObj->userId = (int)$row['userid'];
            $entryObj->created = $row['periodStart'];
            $this->products[$productId]->entry = $entryObj;

// check we have the athlete

            if (!isset($this->athleteIds[$athleteId])) {
                $entryOnlyAthletes[] = $athleteId;
            }
            if (!empty($entryOnlyAthletes)) {
//                echo "Entry Only Athletes\n";
//                var_dump($entryOnlyAthletes);
                $this->_getAthletes($entryOnlyAthletes);
            }

            // check we have the ceid/event
            if (!isset($this->ceIds[$ceId])) {
                $entryOnlyCEIDs[] = $ceId;
            }
            if (!empty($entryOnlyCEIDs)) {
//                echo "Entry Only CompEvents\n";
                $this->_getCompEvents($entryOnlyCEIDs);
            }
        }
    }

    private function _getOrders() {
        $orderResults = $this->_getOrdersIdsForProduct($this->productIds);
        $rows = $orderResults->fetch_all(MYSQLI_ASSOC);
        $this->orders = array();
        $this->orderIdsByProduct = array();

        foreach ($rows as $row) {
            $orderId = (int)$row['orderId'];
            if (!array_key_exists($orderId, $this->orders)) {
                $orderObj = new stdClass();
                $orderObj->productId = (int)$row['productId'];
                $orderObj->wcOrder = wc_get_order($orderId);
                $orderObj->refunds = array();
                $this->orders[(int)$row['orderId']] = $orderObj;
            }
            $this->orderIdsByProduct[(int)$row['productId']] = $orderId;
        }
    }

    private function _getOrdersIdsForProduct($productIds, $order_status = array('wc-completed', 'wc-processing', 'wc-refunded')) {
        $sql = "
            SELECT order_items.order_id orderId, order_item_meta.meta_value productId
            FROM " . E4S_TABLE_WCORDERITEMS . " as order_items
            LEFT JOIN " . E4S_TABLE_WCORDERITEMMETA . " as order_item_meta ON order_items.order_item_id = order_item_meta.order_item_id
            LEFT JOIN " . E4S_TABLE_POSTS . " as posts ON order_items.order_id = posts.ID
            WHERE posts.post_type = '" . WC_POST_ORDER . "'
            AND posts.post_status IN ( '" . implode("','", $order_status) . "' )
            AND order_items.order_item_type = 'line_item'
            AND order_item_meta.meta_key = '" . WC_POST_PRODUCT_ID . "'
            AND order_item_meta.meta_value in (" . implode(',', $productIds) . ")
        ";

        $results = e4s_queryNoLog($sql);
        return $results;
    }

    private function _getRefunds() {
        $sql = 'select id, orderid, value, reason, options
                from ' . E4S_TABLE_REFUNDS . '
                where compid = ' . $this->compid . '
                and orderid in (' . implode(',', $this->orderIdsByProduct) . ')';
        $result = e4s_queryNoLog($sql);
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        foreach ($rows as $row) {
            $obj = e4s_getDataAsType($row, E4S_OPTIONS_OBJECT);
            $obj->options = e4s_getDataAsType($obj->options, E4S_OPTIONS_OBJECT);
            $this->orders[(int)$row['orderid']]->refunds[] = $obj;
            $this->refunds = TRUE;
        }
    }

    private function _generateLines() {
        $this->lines = array();
        foreach ($this->products as $productLine) {
            $lineObj = $this->_generateLine($productLine);
            if (!is_null($lineObj)) {
                $this->lines[] = $lineObj;
                $this->_addToSummary($lineObj);
            }
        }
    }

    private function _generateLine($productLine) {
        if (!$this->_validateLine($productLine)) {
            return null;
        }

        $lineObj = new stdClass();
        $config = $productLine->config;
        $athleteId = $config->athleteid;
        $productId = $productLine->productId;
        $lineObj->athleteId = $athleteId;
        $lineObj->productId = $productId;
        $lineObj->productName = $productLine->wcProduct->get_name();

        $orderId = 0;
        if (isset($productLine->entry)) {
            $orderId = $productLine->entry->orderId;
        }
        if ($orderId === 0) {
            if (array_key_exists($productId, $this->orderIdsByProduct)) {
                $orderId = $this->orderIdsByProduct[$productId];
            }
        }
        $lineObj->orderId = $orderId;

        $order = null;
        $athleteExists = array_key_exists($athleteId, $this->athletes);
        $ceIdExists = array_key_exists($config->ceid, $this->compEvents);
        $order = null;
        $orderItem = null;
        $lineFee = 0;
        $orderFee = 0;
        $lineCnt = 0;
        $lineCost = 0;
        $orderNet = 0;

        $valueToReduceOutCost = 0;
        $lineObj->couponsUsed = array();
        $lineObj->status = array();
        if (array_key_exists($orderId, $this->orders)) {
            $orderObj = $this->orders[$orderId];
            $order = $orderObj->wcOrder;

            $wcCoupons = $order->get_coupons();
            $orderCoupons = array();
            if (!empty($wcCoupons)) {
                $this->couponsUsed = TRUE;
                foreach ($wcCoupons as $wcCoupon) {
                    $coupon = new stdClass();
                    $coupon->id = $wcCoupon->get_id();

                    $coupon->name = $wcCoupon->get_name();
                    $coupon->amount = $wcCoupon->get_discount();
                    // Must be a more structured way to get this ????
                    $coupon->postId = $wcCoupon->get_meta_data()[0]->get_data()['value']['id'];
                    $coupon->type = $wcCoupon->get_meta_data()[0]->get_data()['value']['discount_type'];
                    if ($coupon->type === 'percent') {
                        $valueToReduceOutCost += $coupon->amount;
                    }
                    $orderCoupons[] = $coupon;
                }
            }
            $lineObj->couponsUsed = $orderCoupons;
            $lineCnt = $order->get_item_count();

            $productRefunds = array();

            if (!empty($orderObj->refunds)) {
                $refundArr = $orderObj->refunds;

                foreach ($refundArr as $refund) {
                    $reason = $refund->reason;
                    if (isset($refund->options)) {
                        $refundStripeFee = $refund->options->stripefee;
                        $refundE4SFee = $refund->options->e4sfee;
                        foreach ($refund->options->lines as $line) {
                            if ($line->productid === $productId) {
                                if (!isset($line->linevalue)) {
                                    $line->linevalue = $line->value;
                                }
                                $obj = new stdClass();
                                $obj->reason = $reason;
                                $obj->refundValue = 0;
                                if (!$refundE4SFee) {
                                    $obj->refundValue = $line->linevalue - $line->e4slinefee;
                                } elseif ($refundStripeFee) {
                                    $lineFee = $line->linevalue * $this->priceObj->getStripePerc();
                                    $lineFee += ($this->priceObj->getStripeAddCost() / $lineCnt);
                                    $obj->refundValue = $line->linevalue - $lineFee;
                                } else {
                                    $obj->refundValue = $line->linevalue;
                                }
                                $productRefunds[] = $obj;
                            }
                        }
                    }
                }
            }
            $lineObj->refunds = $productRefunds;
            foreach ($order->get_items() as $item) {
                if ($item->get_product_id() === $productId) {
                    $orderItem = $item;
                    $lineCost = $orderItem->get_subtotal();
                    $lineFee = $lineCost * $this->priceObj->getStripePerc();
                    $lineFee += ($this->priceObj->getStripeAddCost() / $lineCnt);
                    $lineFee = number_format($lineFee, 2);
                    $orderFee = $order->get_meta('_stripe_fee');
                    $orderNet = $order->get_meta('_stripe_net');
                    break;
                }
            }
            if ($order->get_status() === 'refunded') {
                $lineObj->status[] = E4S_FINANCIAL_STATUS_REFUNDED;
            }
        }

        if (!isset($productLine->entry)) {
            $lineObj->status[] = E4S_FINANCIAL_STATUS_NOENTRY;
            $entry = null;
            $status = 0;
        } else {
            if (!$athleteExists) {
                $lineObj->status[] = E4S_FINANCIAL_STATUS_NOATHLETE;
            }
            if (!$ceIdExists) {
                $lineObj->status[] = E4S_FINANCIAL_STATUS_CEEVENT;
            }
            if (is_null($productLine->wcProduct)) {
                $lineObj->status[] = E4S_FINANCIAL_STATUS_MANUAL;
            }
            $entry = $productLine->entry;
            $status = 1;
        }
        $lineObj->entryCnt = $status;

        if ($athleteExists) {
            $athleteName = $this->athletes[$athleteId]->fullName;
//            if ( $athleteName === "Tom Pearson" ){
//                var_dump($productLine);
//                exit();
//            }
        } else {
            $lineObj->status[] = E4S_FINANCIAL_STATUS_NOATHLETE;
            $athleteName = $lineObj->productName;
        }
        $lineObj->athleteName = $athleteName;

        if ($ceIdExists) {
            $event = $this->compEvents[$config->ceid];
            $eventName = $event->eventName;
        } else {
            $eventName = $lineObj->productName;
        }
        $lineObj->eventName = $eventName;

        if ($status === 0) {
            $orderId = $this->orderIdsByProduct[$productId];
        } else {
            $orderId = $entry->orderId;
        }
        $lineObj->orderId = $orderId;

        $lineObj->lineCost = $lineCost;

        $lineCostOut = 0;
        $lineOldCostOut = 0;
        if ($status === 1) {
            if ($orderId > 0 and $entry->price > 0) {
                $lineCostOut = $this->priceObj->getOrgPrice($entry->price);
                $lineOldCostOut = $this->priceObj->getOrgPrice($entry->price, TRUE);
            } else {
                $addFee = TRUE;
                if (isset($entry->options->fee)) {
                    $addFee = $entry->options->fee;
                }
                if ($addFee) {
                    $lineCostOut = -$this->priceObj->getFeeForPrice(0);
                    $lineOldCostOut = -$this->priceObj->getOldFeeForPrice(0);
                }
            }
        }
        // $valueToReduceOutCost : value form coupon reductions
        $lineObj->lineCostOut = $lineCostOut - $valueToReduceOutCost;
        $lineObj->lineOldCostOut = $lineOldCostOut - $valueToReduceOutCost;
        $lineObj->lineFee = $lineFee;
        $refundReason = array();
        if (!empty($productRefunds)) {
            foreach ($productRefunds as $productRefund) {
                $refundReason[] = $productRefund->reason;
            }
        }
        if (empty($refundReason)) {
            $refundReason = ['Unknown. Check Values'];
        }
        $lineObj->refundReason = $refundReason;

        $refundValue = 0;
        if (!empty($productRefunds)) {
            foreach ($productRefunds as $productRefund) {
                $refundValue += $productRefund->refundValue;
            }
        }
        $lineObj->refundValue = $refundValue;
        if ($refundValue > 0) {
            $lineObj->status[] = E4S_FINANCIAL_STATUS_REFUNDED;
        }
        $lineObj->lineCnt = $lineCnt;
        $lineObj->orderNet = (float)$orderNet;
        $lineObj->orderFee = (float)$orderFee;
        $lineObj->reason = '';
        if ($status === 1) {
            if (isset($entry->options->reason)) {
                $lineObj->reason = $entry->options->reason;
            }
        }
        $lineObj->options = '';
        if ($status === 1) {
            $optionsStr = e4s_getDataAsType($entry->options, E4S_OPTIONS_STRING);
            if ($optionsStr !== '{}') {
                $lineObj->options = $optionsStr;
            }
        }
        return $lineObj;
    }

    private function _validateLine($productLine) {
        $return = TRUE;

        $config = $productLine->config;

        if (!isset($config->athleteid)) {
            $this->_addException('No Athlete in Config', $productLine);
            $return = FALSE;
        }

//        if ( !isset($this->athletes[$config->athleteid]) ){
//            $this->_addException("No Athlete", $productLine,$config->athleteid );
//            $return = false;
//        }

//        if ( !array_key_exists($config->ceid , $this->compEvents) ){
//            $this->_addException("No CompEvent Key", $productLine, $config->ceid);
//            $return = false;
//        }
        if (!isset($productLine->entry)) {
            if (!array_key_exists($productLine->productId, $this->orderIdsByProduct)) {
                $return = FALSE;
            }
        }
        return $return;
    }

    private function _addToSummary($lineObj) {
        if (is_null($this->summaryObj)) {
            $this->summaryObj = new stdClass();
            $this->summaryObj->entryCnt = 0;
            $this->summaryObj->lineCost = 0;
            $this->summaryObj->lineCostArray = array();
            $this->summaryObj->lineCostOut = 0;
            $this->summaryObj->lineOldCostOut = 0;
            $this->summaryObj->lineFee = 0;
            $this->summaryObj->refundValue = 0;
        }
        $this->summaryObj->entryCnt += $lineObj->entryCnt;
        $this->summaryObj->lineCost += $lineObj->lineCost;
        $this->summaryObj->refundValue += $lineObj->refundValue;
        $this->summaryObj->lineCostOut += $lineObj->lineCostOut;
        $this->summaryObj->lineOldCostOut += $lineObj->lineOldCostOut;
        $this->summaryObj->lineFee += $lineObj->lineFee;
        if (!array_key_exists($lineObj->lineCost, $this->summaryObj->lineCostArray)) {
            $this->summaryObj->lineCostArray [$lineObj->lineCost] = 0;
        }
        $this->summaryObj->lineCostArray [$lineObj->lineCost] += 1;
    }

    public function outputReport() {
        echo '
            <style>
                table {
                  border-collapse: collapse;
                }
                table, th, td {
                      border: 1px solid #bfbebf;
                }
            </style>';
        echo '<table>';
        $this->_outputHeader();
        $lineNo = 1;
        foreach ($this->lines as $lineObj) {
            $this->_outputLine($lineObj, $lineNo++);
        }
        echo '</table>';
        $this->_outputSummary();
    }

    private function _outputHeader() {

        echo '<tr>';
        echo '<th>Row</th>';
        echo '<th>Status</th>';
        echo '<th>Athlete</th>';
        echo '<th>Event</th>';
        echo '<th>Order #</th>';
        echo '<th>In</th>';
        echo '<th>Out</th>';
        echo '<th>Out ( Old Pricing )</th>';
        echo '<th>Item Stripe Fee</th>';
        if ($this->refunds) {
            echo '<th>Refund Reason</th>';
            echo '<th>Refund Value</th>';
        }
        echo '<th>Order Item Count</th>';
        echo '<th>Order Net</th>';
        echo '<th>Order Stripe Fee</th>';
        if ($this->couponsUsed) {
            echo '<th>Coupon Used</th>';
            echo '<th>Coupon Type</th>';
        }
        echo '<th>Options</th>';
        echo '<th>Info</th>';
        echo '</tr>';
    }

    private function _outputLine($line, $lineNo) {
        echo '<tr>';

        $this->_outputColumn($lineNo);
        $this->_outputColumn(implode('/', $line->status));
        $this->_outputColumn($line->athleteName);
        $this->_outputColumn($line->eventName);
        $outputValue = $line->orderId;
        if ($this->e4sUser and $outputValue !== 0) {
            $outputValue = "<a href='/wp-admin/post.php?post=" . $outputValue . "&action=edit' target='E4SOrders'>" . $outputValue . '</a>';
        }
        $this->_outputColumn($outputValue);

        $this->_outputColumn(number_format($line->lineCost, 2));
        $this->_outputColumn(number_format($line->lineCostOut, 2));
        $this->_outputColumn(number_format($line->lineOldCostOut, 2));
        $this->_outputColumn(number_format($line->lineFee, 2));
        if ($this->refunds) {
            if (empty($line->refundReason)) {
                $this->_outputColumn('');
            } else {
                $this->_outputColumn(implode(',', $line->refundReason));
            }
            $this->_outputColumn(number_format($line->refundValue, 2));
        }
        $this->_outputColumn($line->lineCnt);
        $this->_outputColumn(number_format($line->orderNet, 2));
        $this->_outputColumn(number_format($line->orderFee, 2));
        if ($this->couponsUsed) {
            $couponNames = array();
            $couponValues = array();
            foreach ($line->couponsUsed as $couponUsed) {
                $couponName = $couponUsed->name;
                $couponId = $couponUsed->postId;
                if ($this->e4sUser and $outputValue !== 0) {
                    $couponName = "<a href='/wp-admin/post.php?post=" . $couponId . "&action=edit' target='E4SCoupons'>" . $couponName . '</a>';
                }
                $couponNames[] = $couponName;
                $couponValues[] = $couponUsed->amount;
            }
            $this->_outputColumn(implode(',', $couponNames));
            $this->_outputColumn(implode(',', $couponValues));
        }
        $this->_outputColumn($line->reason);
        $this->_outputColumn($line->options);

        echo '<tr>';
    }

    private function _outputColumn($value, $public = TRUE) {
        if ($this->e4sUser or $public) {
            echo '<td>' . $value . '</td>';
        }
    }

    private function _outputSummary() {
        echo '<table>';
        echo '<tr>';
        echo '<td>WC Entries</td>';
        echo '<td>' . $this->productCnt . '</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td>E4S Paid Entry Count</td>';
        echo '<td>' . $this->paidEntryCnt . '</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td>Entry Count</td>';
        echo '<td>' . $this->summaryObj->entryCnt . '</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td>Total Income</td>';
        echo '<td>' . number_format($this->summaryObj->lineCost, 2) . '</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td>Total Refunded</td>';
        echo '<td>' . number_format($this->summaryObj->refundValue, 2) . '</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td>Total Stripe Fees</td>';
        echo '<td>' . number_format($this->summaryObj->lineFee, 2) . '</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td>Total Payout</td>';
        echo '<td>' . number_format($this->summaryObj->lineCostOut, 2) . '</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td>E4S Profit</td>';
        echo '<td>' . number_format($this->summaryObj->lineCost - $this->summaryObj->lineCostOut - $this->summaryObj->refundValue, 2) . '</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td>Total Payout ( Old Pricing )</td>';
        echo '<td>' . number_format($this->summaryObj->lineOldCostOut, 2) . '</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td>E4S Profit ( Old Pricing )</td>';
        echo '<td>' . number_format($this->summaryObj->lineCost - $this->summaryObj->lineOldCostOut - $this->summaryObj->refundValue, 2) . '</td>';
        echo '</tr>';
        echo '</table>';

        echo '<table>';
        echo '<tr>';
        echo "<td colspan='3'>Breakdown</td>";
        echo '</tr>';
        foreach ($this->summaryObj->lineCostArray as $price => $count) {
            echo '<tr>';
            echo "<td colspan='1'>" . $count . '</td>';
            echo "<td colspan='1'> at </td>";
            echo "<td colspan='1'>" . $price . '</td>';
            echo '</tr>';
        }
        echo '</table>';

        if (sizeof($this->exceptions) > 0) {
            echo '<table>';
            echo '<tr>';
            echo "<td colspan='3'>Exceptions</td>";
            echo '</tr>';
            foreach ($this->exceptions as $key => $exception) {
                echo '<tr>';
                echo "<td colspan='1'>" . $key . '</td>';
                echo "<td colspan='1'>" . sizeof($exception) . '</td>';
                echo "<td colspan='1'>";
                $sep = '';
                foreach ($exception as $obj) {
                    if ($obj->key !== '') {
                        echo $sep . $obj->key;
                        $sep = ', ';
                    }
                }
                echo '</td>';
                echo '</tr>';
            }
            echo '</table>';
        }
    }
}