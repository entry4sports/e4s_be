<?php
define('E4S_CONTACT_ORGANISER', 'Organiser');
define('E4S_CATEGORY_FOR_ORGANISER', 'Competition Query');
define('E4S_CATEGORY_FOR_REGISTRATION', 'Registration Query');

class contactRequestClass {
    public $compObj;
    public $compid;
    public $userid;
    public $category;
    public $body;
	public $foaE4S;
    public $emailFrom;

    public function __construct($compObj, $userid) {
        $this->compObj = $compObj;
        $this->compid = $this->compObj->getID();
        $this->userid = $userid;
		$this->foaE4S = false;
    }

    public function sendOrganiserRequest($category, $body, $email, $foaE4S) {
        $this->category = $category;
        $this->body = $body;
        $this->emailFrom = $email;
		$this->foaE4S = $foaE4S;
        $this->insertRequest(E4S_CONTACT_ORGANISER);
        $this->sendEmail($category);
    }

    public function insertRequest($type) {
        $sql = 'insert into ' . E4S_TABLE_REQUESTINFO . ' (compid,category,body,contacttype, userid) 
            values (' . $this->compid . ",'" . $this->category . "','" . $this->body . "','" . $type . "'," . $this->userid . ')';
        e4s_queryNoLog($sql);
    }

    public function sendEmail($category) {
        $bccUs = FALSE;
        $cOptions = $this->compObj->setContactInfoInCOptions();
        $contactObj = $this->compObj->getContactObjFromCOptions($cOptions);
        $email = E4S_SUPPORT_EMAIL;
        $userName = E4S_SUPPORT_NAME;
        $regEmail = '';
        if (strcasecmp($category, E4S_CATEGORY_FOR_REGISTRATION) === 0) {
            $config = e4s_getConfig();
            $cfgOptions = $config['options'];
            if (isset($cfgOptions->registrationsEmail)) {
                $regEmail = $cfgOptions->registrationsEmail;
                // Send to Organiser ?
                $category = E4S_CATEGORY_FOR_ORGANISER;
            }
        }
		if ( !$this->foaE4S ) {
			if ( isset( $contactObj->email ) and $contactObj->email !== '' ) {
				if ( strcasecmp( $category, E4S_CATEGORY_FOR_ORGANISER ) === 0 ) {
					$email    = $contactObj->email;
					$userName = $contactObj->userName;
				}
			}
		}else{
			$email = ["nick@entry4sports.com", "paul@entry4sports,com"];
		}
        $user = $GLOBALS[E4S_USER]->user;

        if ((int)$user->id === 0) {
            $fromEmail = $this->emailFrom;
        } else {
            $fromEmail = $user->user_login . ' < ' . $user->user_email . ' >';
        }
        $userInfo = 'From user : ' . $fromEmail;
        if ((int)$user->id !== 0) {
            $userInfo .= ' (' . $user->id . ')';
        }
        $body = str_replace("\n\r", '<br>', $this->body);
        $body = str_replace("\n", '<br>', $body);
        $this->body = 'Dear ' . $userName . ',<br>';
	    if ($regEmail !== '' and E4S_CURRENT_DOMAIN === E4S_AAI_DOMAIN) {
		    $this->body .= '<br><br>This as a registration query. If you cannot answer the query and require AAI assistance, please email ' . $regEmail . "<br><br>";
	    }
        if ($userName === E4S_SUPPORT_NAME) {
            $this->body .= $userInfo;
        } else {
            $this->body .= 'You have received this message from the Entry4Sports Website. To reply, click the email address below or copy to a new email in your chosen client.<br>';
            $this->body .= 'DO NOT reply to this email directly.<br><br>';
            $this->body .= 'From ' . $fromEmail . '<br>';
        }

        $this->body .= '<br><br>' . $body;
        $this->body .= Entry4_emailFooter($email);

        $response = e4s_mail($email, E4S_CURRENT_DOMAIN . '/' . $this->compid . ' (' . $this->compObj->row['Date'] . '): ' . $this->category, $this->body, Entry4_mailOrganiserHeader($fromEmail, $bccUs));
        if (!$response) {
            Entry4UIError(9303, 'Failed to send email with Request Info', 200, '"body":"' . addslashes($this->body) . '"');
        }
    }
}

function Entry4_mailOrganiserHeader($email, $sendBcc = FALSE) {

    $headers = array('Content-Type: text/html; charset=UTF-8');

    $headers[] = 'Reply-to: ' . $email;
//    $headers[] = 'Reply-to: paul.day@apiconsultancy.com' ;

    if ($sendBcc) {
        $headers[] = 'Bcc:paul@entry4sports.com';
    }

    return $headers;
}