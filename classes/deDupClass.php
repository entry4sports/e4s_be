<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
function e4s_dedupAthletes() {
    $obj = new deDupClass(200, FALSE, '', '');
    Entry4UISuccess();
}

class deDupClass {
    public $firstName;
    public $surName;
    public $dob;
    public $clubId;
    public $urn;
    public $dupAthletes;
    public $processCnt;
    public $testMode;
    public $athleteObj;
    public $athleteObjs;
    public $ids;
    public $keepId;

    public function __construct($processCnt = 1, $testMode = FALSE, $firstName = '', $surName = '', $urn = '', $keepId = 0) {
        $this->processCnt = $processCnt;
        $this->testMode = $testMode;
        $this->firstName = $firstName;
        $this->surName = $surName;
        $this->urn = $urn;
        $this->keepId = $keepId;
        $this->_process();
    }

    private function _process() {
        $this->_getAthletes();

        foreach ($this->dupAthletes as $key => $obj) {
            if ($this->testMode) {
                echo 'Processing : ' . $obj->firstName . ' ' . $obj->surName . "\n";
            }
            $this->_processAthlete($key);
            $this->processCnt--;
            if ($this->processCnt === 0) {
                break;
            }
        }
    }

    private function _getAthletes() {
        $sql = '
            select a.firstname firstName,
                   a.surname surName,
                   a.dob dob,
                   a.clubid clubId
            from ' . E4S_TABLE_ATHLETE . ' a
            where ' . $this->_getWhere() . '
            group by a.firstname, a.surname, a.dob, a.clubid
            having count(*) > 1
        ';

        $result = e4s_queryNoLog($sql);
        $this->dupAthletes = array();
        while ($obj = $result->fetch_object()) {
            $obj->dupKey = $this->_getDupKey($obj);
            $this->dupAthletes[$obj->dupKey] = $obj;
        }
    }

    private function checkURNData() {
        if ($this->urn === '') {
            return;
        }
        $sql = 'select firstName, surName, dob, clubId
                from ' . E4S_TABLE_ATHLETE . "
                where aocode = '" . E4S_AOCODE_EA . "'
                and urn = " . $this->urn;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            Entry4UIError(6036, 'No athlete found for URN ' . $this->urn);
        }
        $obj = $result->fetch_object();
        $this->firstName = $obj->firstName;
        $this->surName = $obj->surName;
        $this->clubId = (int)$obj->clubId;
        $this->dob = $obj->dob;
    }

    private function _getWhere() {
        $this->checkURNData();
        $where = " aocode not in ('IRL','ANI') ";
        if ($this->firstName !== '') {
            $where .= " and firstname = '" . addslashes($this->firstName) . "' ";
        }
        if ($this->surName !== '') {
            $where .= " and surname = '" . addslashes($this->surName) . "' ";
        }
        if ($this->dob !== '') {
            $where .= " and dob = '" . $this->dob . "' ";
        }
        if ($this->clubId !== '') {
            $where .= ' and clubId = ' . $this->clubId . ' ';
        }
        return $where;
    }

    private function _getDupKey($obj) {
        return addslashes($obj->firstName) . '~' . addslashes($obj->surName) . '~' . $obj->dob . '~' . $obj->clubId;
    }

    private function _processAthlete($key) {
        $objs = $this->_getFullAthletes($key);
        if (sizeof($objs) === 0) {
//            No athletes found ???
            Entry4UIError(6010, 'No athletes found');
        }
        if (sizeof($objs) === 1) {
//            Nothing to do, why did it get here
            Entry4UIError(6020, 'Only 1 record found');
        }

        if ($this->_hasErrors()) {
            $this->_showErrors();
            echo "==========================\n";
        } else {
//            Entry4_StartTransaction();

            $this->_updateBibs();
            $this->_updateUserAthletes();
            $this->_updatePBs();
            $this->_updateEntries();
			$this->_updateResults();
            $this->_updateOrders();
            $this->_updateAthlete();
        }
    }

    private function _getFullAthletes($key) {
        $arr = array();
        $this->ids = array();
        $sql = 'select  id id,
                        firstname firstName,
                        surname surName,
                        gender gender,
                        dob dob,    
                        urn urn,
                        aocode aoCode,
                        clubid clubId,
                        club2id club2Id,
                        schoolid schoolId,
                        email email,
                        classification classification
                from ' . E4S_TABLE_ATHLETE . "
                where '" . $key . "' = " . $this->_getSqlDupKey();
        $sql .= ' and ' . $this->_getWhere();
        $sql .= ' order by URN desc';

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            return $arr;
        }

        $this->_initAthleteObj();
        while ($obj = $result->fetch_object()) {
            $obj->id = (int)$obj->id;
            $this->_setAthleteObj($obj);
            $arr [$obj->id] = $obj;
            $this->ids[] = $obj->id;

        }
        $this->athleteObjs = $arr;
        return $arr;
    }

    private function _getSqlDupKey() {
        return "concat(firstname,'~',surName,'~',dob,'~',clubId)";
    }

    private function _initAthleteObj() {
        $this->athleteObjs = array();
        $this->athleteObj = new stdClass();
    }

    private function _setAthleteObj($obj): bool {
        if (!isset($this->athleteObj->id)) {
            $this->athleteObj = $obj;
            return TRUE;
        }
        if ($obj->id === $this->keepId) {
            $this->athleteObj = $obj;
        }
        if (!is_null($obj->urn) and !is_null($this->athleteObj->urn) and $obj->urn !== '') {
            if ($this->athleteObj->urn !== '' and $this->athleteObj->urn !== $obj->urn) {
                $this->audit(1, 'URN not able to be set', $obj, FALSE);
                return FALSE;
            } else {
                $this->athleteObj->aoCode = $obj->aoCode;
                $this->athleteObj->urn = $obj->urn;
            }
        }
        if (!is_null($obj->schoolId) and !is_null($this->athleteObj->schoolId) and $obj->schoolId !== '0') {
            if ($this->athleteObj->schoolId !== '0' and $this->athleteObj->schoolId !== $obj->schoolId) {
                $this->audit(2, 'School not able to be set', $obj, TRUE);
            } else {
                $this->athleteObj->schoolId = $obj->schoolId;
            }
        }
        if (!is_null($obj->club2Id) and !is_null($this->athleteObj->club2Id) and $obj->club2Id !== '0') {
            if ($this->athleteObj->club2Id !== '0' and $this->athleteObj->club2Id !== $obj->club2Id) {
                $this->audit(3, 'club2 not able to be set', $obj, TRUE);
            } else {
                $this->athleteObj->club2Id = $obj->club2Id;
            }
        }
        if (!is_null($obj->gender) and !is_null($this->athleteObj->gender) and $obj->gender !== '') {
            if ($this->athleteObj->gender !== '' and $this->athleteObj->gender !== $obj->gender) {
                $this->audit(4, 'Gender not able to be set', $obj, TRUE);
            } else {
                $this->athleteObj->gender = $obj->gender;
            }
        }
        if (!is_null($obj->classification) and !is_null($this->athleteObj->classification) and $obj->classification !== '0') {
            if ($this->athleteObj->classification !== '0' and $this->athleteObj->classification !== $obj->classification) {
                $this->audit(5, 'Classification not able to be set', $obj, TRUE);
            } else {
                $this->athleteObj->classification = $obj->classification;
            }
        }
        if (!is_null($obj->email) and !is_null($this->athleteObj->email) and $obj->email !== '') {
            if ($this->athleteObj->email !== '' and $this->athleteObj->email !== $obj->email) {
                $this->audit(6, 'Email not able to be set', $obj, FALSE);
                return FALSE;
            } else {
                $this->athleteObj->email = $obj->email;
            }
        }
        return TRUE;
    }

    private function audit($errNo, $err, $obj, $exit) {
        $this->_setError($err);

        if ($exit) {
//            exit();
        }
    }

    private function _setError($err) {
        if (!isset($this->athleteObj->errors)) {
            $this->athleteObj->errors = array();
        }
        $this->athleteObj->errors[] = $err;
    }

    private function _hasErrors() {
        if (!isset($this->athleteObj->errors)) {
            return FALSE;
        }
        return TRUE;
    }

    private function _showErrors() {
        var_dump($this->athleteObj);
    }

    private function _updateBibs() {
        $sql = 'select compid compId,
                       athleteid athleteId
                from ' . E4S_TABLE_BIBNO . '
                where athleteid in ( ' . implode(',', $this->ids) . ' )';
        $result = e4s_queryNoLog($sql);
        $arr = array();
        while ($obj = $result->fetch_object()) {
            if (array_key_exists($obj->compId, $arr)) {
                $this->audit(9, 'duplicate Bib', $obj, FALSE);
                return;
            }
            $arr[$obj->compId] = $obj->athleteId;
        }
        $sql = 'update ' . E4S_TABLE_BIBNO . '
                set athleteid = ' . $this->athleteObj->id . '
                where athleteid in ( ' . implode(',', $this->ids) . ' )';
        $this->_processSql($sql);
    }

    private function _processSql($sql) {
        if ($this->testMode) {
            var_dump($sql);
        } else {
            e4s_queryNoLog('DeDup' . E4S_SQL_DELIM . $sql);
        }
    }

    private function _updateUserAthletes() {
        $sql = 'select id id,
                       userid userId
                from ' . E4S_TABLE_USERATHLETES . '
                where athleteid in (' . implode(',', $this->ids) . ')';
        $result = e4s_queryNoLog($sql);
        $deleteIds = array();
        $keepIds = array();
        while ($obj = $result->fetch_object()) {
            if (!array_key_exists($obj->userId, $keepIds)) {
                $keepIds[$obj->userId] = $obj->id;
            } else {
                $deleteIds[] = $obj->id;
            }
        }
        if (sizeof($deleteIds) > 0) {
            $deleteSql = 'delete from ' . E4S_TABLE_USERATHLETES . '
                          where id in (' . implode(',', $deleteIds) . ')';
            $this->_processSql($deleteSql);
        }
        if (sizeof($keepIds) > 0) {
            $updateSql = 'update ' . E4S_TABLE_USERATHLETES . '
                            set athleteid = ' . $this->athleteObj->id . '
                            where id in (' . implode(',', $keepIds) . ')';

            $this->_processSql($updateSql);
        }
    }

    private function _updatePBs() {
        $sql = 'select * 
                from ' . E4S_TABLE_ATHLETEPB . '
                where athleteid in ( ' . implode(',', $this->ids) . ')';
        $result = e4s_queryNoLog($sql);
        $keepObjs = array();
        $deleteIds = array();
        while ($obj = $result->fetch_object()) {
            if (array_key_exists($obj->eventid, $keepObjs)) {
                if ((float)$obj->pb < (float)$keepObjs[$obj->eventid]->pb) {
                    $deleteIds[] = $keepObjs[$obj->eventid]->id;
                    $keepObjs[$obj->eventid] = $obj;
                } else {
                    $deleteIds[] = $obj->id;
                }
            } else {
                $keepObjs [$obj->eventid] = $obj;
            }
        }
        if (sizeof($deleteIds) > 0) {
            $sql = 'delete from ' . E4S_TABLE_ATHLETEPB . '
                    where id in (' . implode(',', $deleteIds) . ')';
            $this->_processSql($sql);
        }
        $sql = 'update ' . E4S_TABLE_ATHLETEPB . '
                set athleteid = ' . $this->athleteObj->id . '
                where athleteid in ( ' . implode(',', $this->ids) . ')';
        $this->_processSql($sql);
    }

    private function _updateEntries() {
        $sql = 'select compeventid compEventId, 
                       athleteid athleteId
                from ' . E4S_TABLE_ENTRIES . '
                where athleteid in (' . implode(',', $this->ids) . ')';
        $result = e4s_queryNoLog($sql);
        $ceIds = array();
        while ($obj = $result->fetch_object()) {
            if (array_key_exists($obj->compEventId, $ceIds)) {
                // "same" athlete entered into the same event
                $this->audit(8, 'Duplicate Entry', $obj, TRUE);
                return;
            }
            $ceIds[$obj->compEventId] = $obj->athleteId;
        }

        $updateSql = 'update ' . E4S_TABLE_ENTRIES . '
        set athleteid = ' . $this->athleteObj->id . '
        where athleteid in (' . implode(',', $this->ids) . ')';

        $this->_processSql($updateSql);
    }
	private function _updateResults() {
		$updateSql = 'update ' . E4S_TABLE_EVENTRESULTS . '
        set athleteid = ' . $this->athleteObj->id . '
        where athleteid in (' . implode(',', $this->ids) . ')';

		$this->_processSql($updateSql);
	}
    private function _updateOrders() {
        /*
         * "athleteid":99999 in post_content
         */
        foreach ($this->ids as $id) {
            if ($id !== $this->athleteObj->id) {
                $search = "\"athleteid\":" . $id . ',';
                $replace = "\"athleteid\":" . $this->athleteObj->id . ',';
                $updateSql = '
                    update ' . E4S_TABLE_POSTS . " 
                    set post_content = replace(post_content,'" . $search . "','" . $replace . "')
                    where post_content like '%" . $search . "%'
                ";
                $this->_processSql($updateSql);
            }
        }
    }

    private function _updateAthlete() {

        $useIds = array_slice($this->ids, 1);

        $urn = "'" . $this->athleteObj->urn . "'";
        if (is_null($this->athleteObj->urn)) {
            $urn = 'null';
        }
        $email = "'" . $this->athleteObj->email . "'";
        if (is_null($this->athleteObj->email)) {
            $email = 'null';
        }
        $schoolId = "'" . $this->athleteObj->schoolId . "'";
        if (is_null($this->athleteObj->schoolId)) {
            $schoolId = 'null';
        }
        $deleteIds = array();
        foreach ($this->ids as $id) {
            if ($id !== $this->athleteObj->id) {
                $deleteIds[] = $id;
            }
        }

        if (sizeof($deleteIds) > 0) {
            if (!$this->testMode) {
                athleteClass::deleteAthletes($deleteIds);
            } else {
                echo 'Deleting athlete ids ' . implode(',', $deleteIds) . "\n";
            }
        }
        $sql = 'update ' . E4S_TABLE_ATHLETE . " 
            set firstname = '" . $this->athleteObj->firstName . "',
                surname = '" . $this->athleteObj->surName . "',
                aocode = '" . $this->athleteObj->aoCode . "',
                urn = " . $urn . ",
                dob = '" . $this->athleteObj->dob . "',
                gender = '" . $this->athleteObj->gender . "',
                classification = " . $this->athleteObj->classification . ',
                email = ' . $email . ',
                clubid = ' . $this->athleteObj->clubId . ',
                club2id = ' . $this->athleteObj->club2Id . ',
                schoolid = ' . $schoolId . '
            where id = ' . $this->athleteObj->id;

        $this->_processSql($sql);
    }
}

