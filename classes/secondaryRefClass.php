<?php

define('E4S_SECONDREF_SYSTEMNAME', 'E4S');
define('E4S_SECONDREF_SYSTEM', 'SYSTEM');
define('E4S_SECONDREF_AO', 'AO');
define('E4S_SECONDREF_ORG', 'ORG');
define('E4S_SECONDREF_COMP', 'COMP');
define('E4S_SECONDREF_EVENT', 'EVENT');
define('E4S_SECONDREF_EGROUP', 'GROUP');
define('E4S_SECONDREF_CE', 'CE');

class secondaryRefClass {
    public $model;
    public $divider;

    public function __construct($model) {
        if (isset($this)) {
            $this->divider = '-';
        }
        if (!isset($model->id)) {
            $model->id = 0;
        }
        if (!isset($model->objName)) {
            $model->objName = '';
        }
        if (!isset($model->objKey)) {
            $model->objKey = '';
        }

        if (!isset($model->objId)) {
            $model->objId = 0;
        }
        if (!isset($model->objType)) {
            $model->objType = '';
        } else {
            $model->objType = $this->validateObjType($model->objType);
        }
//echo "7\n";
//var_dump($model);
//exit();
        if ($model->id === 0) {
            switch ($model->objKey) {
                case E4S_SECONDREF_SYSTEM:
                case E4S_SECONDREF_AO:
                case E4S_SECONDREF_ORG:
                    $model->compId = 0;
                    break;
                default:
                    if (!isset($model->compId)) {
                        $model->compId = 0;
                    }
                    if ($model->compId === 0) {
                        $this->_error(9050, 'No Competition ID passed', TRUE);
                        break;
                    }
            }
        }
        $this->model = $model;
    }

    private function validateObjType($type = null) {
        if (is_null($type)) {
            $type = $this->model->objType;
        }
        $type = strtoupper($type);
        switch ($type) {
            case E4S_SECONDREF_SYSTEM:
            case E4S_SECONDREF_AO:
            case E4S_SECONDREF_ORG:
            case E4S_SECONDREF_COMP:
            case E4S_SECONDREF_EVENT:
            case E4S_SECONDREF_EGROUP:
            case E4S_SECONDREF_CE:
                break;
//            case "EVENT_GROU":
//            case "EVENT_GROUP":
//                $type = E4S_SECONDREF_EGROUP;
//                break;
            default:
                $type = '';
        }
        return $type;
    }

    private function _error($err, $str, $exit = FALSE) {
        if ($exit) {
            Entry4UIError($err, $str, 200, '');
        }
        return FALSE;
    }

    public function read($create = FALSE) {
        $model = $this->model;

        if ($model->id === 0) {

            if ($create) {
//                echo "refObj Create";
//                var_dump($model);
//                exit();
                return $this->create();
            } else {
                $this->_error(1000, 'Can not read blank ID');
                return null;
            }
        }
        $sql = 'select *
                from ' . E4S_TABLE_SECONDARYREFS . '
                where id = ' . $model->id;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            if ($create) {
                return $this->create();
            }
            return null;
        }
        $row = $result->fetch_assoc();

        return $this->convertRow($row);
    }

    public function create($obj = null) {
        if (is_null($obj)) {
            $obj = $this->model;
        }

        if ($obj->id !== 0) {
            $this->_error(1000, 'Can not create non-blank ID');
            return null;
        }
        if ($obj->objId === 0) {
            $this->_error(1010, 'Can not create blank Obj ID');
            return null;
        }
        if ($obj->objType === '') {
            $this->_error(1020, 'Can not create Obj Type');
            return null;
        }

        return $this->readFromObj($obj, TRUE);
    }

    private function processCE($obj, $create = FALSE) {

        $ceid = $obj->objId;
        // Get info for the passed CE
        $sql = 'select evg.id id, evg.name egname, ce.compid compid, ag.name agname, e.gender gender
                from ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTGROUPS . ' evg,
                     ' . E4S_TABLE_EVENTS . ' e,
                     ' . E4S_TABLE_AGEGROUPS . ' ag
                where evg.id = ce.maxGroup
                and   ag.id = ce.agegroupid
                and   e.id  = ce.eventid
                and   ce.id = ' . $ceid;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            $this->_error(9080, 'Failed to get CE Info', TRUE);
        }
        $row = $result->fetch_assoc();

        $name = $row['egname'] . ' - ' . $row['agname'] . ' - ' . $row['gender'];
        // Now check for the parents reference
        $orgObj = new stdClass();
        $orgObj->objId = (int)$row['id'];
        $orgObj->objType = E4S_SECONDREF_EGROUP;
        $orgObj->objName = $name;
        $orgObj->compId = (int)$row['compid'];
        $orgObj = $this->readFromObj($orgObj, $create);

        if (!is_null($orgObj) and $create) {
            //  Got parent, so create CE
            $obj->objType = E4S_SECONDREF_CE;
            $obj->objId = $ceid;
            $obj->objKey = $orgObj->objKey . $this->divider . $ceid;
            $obj->objName = $name;
            $obj->compId = $orgObj->compId;
            return $this->insert($obj);
        }
        return null;
    }

    private function readFromObj($obj, $create = FALSE) {
        $sql = 'select *
                from ' . E4S_TABLE_SECONDARYREFS . '
                where objid = ' . $obj->objId . "
                and   objtype = '" . $obj->objType . "'";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            $row = $result->fetch_assoc();
            return $this->convertRow($row);
        }
        if ($result->num_rows === 0 and $create) {

            switch ($obj->objType) {
                case E4S_SECONDREF_SYSTEM:
                    return $this->processSystem($obj, TRUE);
                case E4S_SECONDREF_AO:
                    return $this->processAO($obj, $create);
                case E4S_SECONDREF_ORG:
                    return $this->processOrganisation($obj, $create);
                case E4S_SECONDREF_COMP:
                    return $this->processCompetition($obj, $create);
                case E4S_SECONDREF_EGROUP:
                    return $this->processEventGroup($obj, $create);
                case E4S_SECONDREF_EVENT:
                    return $this->processEvent($obj, $create);
                case E4S_SECONDREF_CE:
                    return $this->processCE($obj, $create);
                default:
                    $this->_error(9010, "Invalid Obj type [$obj->objtype]", TRUE);
            }
        }
        return null;
    }

    private function convertRow($row) {
        $model = new stdClass();
        $model->id = (int)$row['id'];
        $model->objKey = $row['objkey'];
        $model->objId = (int)$row['objid'];
        $model->objType = $row['objtype'];
        $model->objName = $row['objname'];
        $model->compId = (int)$row['compid'];
        $this->model = $model;
        return $this->model;
    }

    private function processSystem($obj, $create = FALSE) {
        if ($create) {
            $obj->objType = E4S_SECONDREF_SYSTEM;
            $obj->compId = 0;
            $obj->objId = 1;
            $obj->objName = 'System';
            $obj->objKey = E4S_SECONDREF_SYSTEMNAME;
            return $this->insert($obj);
        }
        return null;
    }

    private function insert($obj) {
        if (!isset($obj->objName)) {
            $obj->objName = 'To Be Set';
        }
        $sql = 'Insert into ' . E4S_TABLE_SECONDARYREFS . '(compid, objkey, objid, objtype, objname )
                values (
                    ' . $obj->compId . ",
                    '" . $obj->objKey . "',
                    " . $obj->objId . ",
                    '" . $obj->objType . "',
                    '" . $obj->objName . "'
                )";
        e4s_queryNoLog($sql);
        $obj->id = e4s_getLastID();
        return $obj;
    }

    private function processAO($obj, $create = FALSE) {
        $aoSql = 'select *
                  from ' . E4S_TABLE_AO . "
                  where id = '" . $obj->objId . "'";
        $aoResult = e4s_queryNoLog($aoSql);
        if ($aoResult->num_rows !== 1) {
            $this->_error(9035, 'Unable to find ' . $obj->objId, TRUE);
        }

        $aoRow = $aoResult->fetch_assoc();

        $aoId = $obj->objId;
        $aoName = $this->getKey($aoId, $aoRow['code']);
        $desc = $aoRow['description'];
        $sysObj = new stdClass();
        $sysObj->objId = 1;
        $sysObj->compId = 0;
        $sysObj->objName = $desc;
        $sysObj->objType = E4S_SECONDREF_SYSTEM;

        $sysObj = $this->readFromObj($sysObj, $create);
        if (!is_null($sysObj) and $create) {
            $obj->objType = E4S_SECONDREF_AO;
            $obj->compId = 0;
            $obj->objId = $aoId;
            $obj->objName = $desc;
            $obj->objKey = E4S_SECONDREF_SYSTEMNAME . $this->divider . $aoName;
            return $this->insert($obj);
        }
        return null;
    }

    private function getKey($id, $name) {
        return substr(strtoupper($name), 0, 4) . '_' . $id;
    }

    private function processOrganisation($obj, $create = FALSE) {
        $sql = 'select cc.id, cc.club club
                from ' . E4S_TABLE_COMPCLUB . ' cc
                where cc.id = ' . $obj->objId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            $this->_error(9030, 'Failed to get organisation', TRUE);
        }
        $row = $result->fetch_assoc();

        $orgId = $obj->objId;
        $club = $row['club'];
        $orgName = $this->getKey($orgId, $club);

        $config = e4s_getConfig();
        $aoSql = 'select *
                  from ' . E4S_TABLE_AO . "
                  where code = '" . $config['defaultao']['code'] . "'";
        $aoResult = e4s_queryNoLog($aoSql);
        if ($aoResult->num_rows !== 1) {
            $this->_error(9030, 'Unable to find ' . $config['defaultao']['code'], TRUE);
        }
        $aoRow = $aoResult->fetch_assoc();
        $aoObj = new stdClass();
        $aoObj->objId = $aoRow['id'];
        $aoObj->objName = $club;
        $aoObj->objType = E4S_SECONDREF_AO;
        $aoObj->compId = 0;

        $aoObj = $this->readFromObj($aoObj, $create);
        if (!is_null($aoObj) and $create) {
            $obj->objType = E4S_SECONDREF_ORG;
            $obj->compId = 0;
            $obj->objId = $orgId;
            $obj->objName = $club;
            $obj->objKey = $aoObj->objKey . $this->divider . $orgName;
            return $this->insert($obj);
        }
        return null;
    }

    private function processCompetition($obj, $create = FALSE) {
//         Get parent info for the passed competition
        $sql = 'select compclubid, name name from ' . E4S_TABLE_COMPETITON . '
                where id = ' . $obj->objId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            $this->_error(9020, 'Failed to get competition', TRUE);
        }
        $row = $result->fetch_assoc();
        $compid = $obj->objId;
        $name = $row['name'];
//        Now check for the parents reference
        $orgObj = new stdClass();
        $orgObj->objId = (int)$row['compclubid'];
        $orgObj->objType = E4S_SECONDREF_ORG;
        $orgObj->objName = $name;
        $orgObj->compId = 0;
        $orgObj = $this->readFromObj($orgObj, $create);
        if (!is_null($orgObj) and $create) {
//            Got parent, so create competition
            $obj->objType = E4S_SECONDREF_COMP;
            $obj->compId = $compid;
            $obj->objId = $compid;
            $obj->objName = $name;
            $obj->objKey = $orgObj->objKey . $this->divider . $compid;
            return $this->insert($obj);
        }
        return null;
    }

    private function processEventGroup($obj, $create = FALSE) {
        $egid = $obj->objId;
        // Get info for the passed eventgroup
        $sql = 'select DISTINCT ed.id edid,ed.name, evg.name evgname, ce.compid compid
                from ' . E4S_TABLE_EVENTGENDER . ' eg, 
                     ' . E4S_TABLE_EVENTDEFS . ' ed,
                     ' . E4S_TABLE_EVENTGROUPS . ' evg,
                     ' . E4S_TABLE_COMPEVENTS . ' ce
                where ed.id = eg.eventid
                and eg.id = ce.EventID
                and maxGroup = evg.id
                and evg.id = ' . $egid;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            $this->_error(9080, 'Failed to get Event Group Info', TRUE);
        }
        $row = $result->fetch_assoc();
        $egname = $row['evgname'];
        $eventGroupName = $this->getKey($egid, $egname);

        // Now check for the parents reference
        $orgObj = new stdClass();
        $orgObj->objId = (int)$row['edid'];
        $orgObj->objType = E4S_SECONDREF_EVENT;
        $orgObj->objName = $egname;
        $orgObj->compId = (int)$row['compid'];
        $orgObj = $this->readFromObj($orgObj, $create);
        if (!is_null($orgObj) and $create) {
            //  Got parent, so create eventGroup
            $obj->objType = E4S_SECONDREF_EGROUP;
            $obj->objId = $egid;
            $obj->objName = $egname;
            $obj->objKey = $orgObj->objKey . $this->divider . $eventGroupName;
            $obj->compId = $orgObj->compId;
            return $this->insert($obj);
        }
        return null;
    }

    private function processEvent($obj, $create = FALSE) {
        $compid = $obj->compId;
        //  Get parent info for the passed event
        $sql = 'select name from ' . E4S_TABLE_COMPETITON . '
                where id = ' . $compid;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            $this->_error(9060, 'Failed to get Competition', TRUE);
        }

        $eventid = $obj->objId;
        $eventsql = 'select name name from ' . E4S_TABLE_EVENTDEFS . '
                where id = ' . $eventid;
        $result = e4s_queryNoLog($eventsql);
        if ($result->num_rows !== 1) {
            $this->_error(9070, 'Failed to get Event Definition', TRUE);
        }
        $row = $result->fetch_assoc();
        $ename = $row['name'];
        $eventName = $this->getKey($eventid, $ename);

        // Now check for the parents reference
        $orgObj = new stdClass();
        $orgObj->objId = $compid;
        $orgObj->objType = E4S_SECONDREF_COMP;
        $orgObj->objName = $ename;
        $obj->compId = $compid;
        $orgObj = $this->readFromObj($orgObj, $create);
        if (!is_null($orgObj) and $create) {
            //  Got parent, so create event
            $obj->objType = E4S_SECONDREF_EVENT;
            $obj->objId = $eventid;
            $obj->objName = $ename;
            $obj->objKey = $orgObj->objKey . $this->divider . $eventName;
            $obj->compId = $compid;
            return $this->insert($obj);
        }
        return null;
    }

    private function _debug($msg, $obj, $exit = TRUE) {
        echo $msg . "\n";
        var_dump($obj);
        if ($exit) {
            exit();
        }
    }
}