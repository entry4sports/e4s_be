<?php

const E4S_PRICE_MIN_FEE = 0.50;
const E4S_PRICE_NATIONAL_MIN_FEE = 0.59;
const E4S_PRICE_NATIONAL_ADD_COST = 0;
const E4S_PRICE_ADD_COST = 0.50;
const E4S_PRICE_MIN_COST = 5;
const E4S_PRICE_OLD_MIN_COST = 10;
const E4S_PRICE_NATIONAL_MIN_COST = 12;
const E4S_PRICE_PERC_CHARGED = 5;
const E4S_STRIPE_ADD_COST = 0.20;
const E4S_STRIPE_NATIONAL_ADD_COST = 0.25;

class priceClass {
    public $compid;
    public $minFee;
    public $minCost;
    public $addCost;
    public $perc;
    public $message;
    public $nationMinCost;
    public $nationalAddCost;
    public $isNational = FALSE;
    public $defaultAO;

    public function __construct($compid = 0, $isNational = FALSE, $compPricing = null) {
	    $pricing = self::getDefaultPricing();
		if ( !is_null($compPricing) ){
			$pricing = $compPricing;
		}
        $this->isNational = $isNational;
        $this->compid = $compid;
        $config = e4s_getConfig();
        $this->defaultAO = $config['defaultao']['code'];

        $this->minFee = E4S_PRICE_MIN_FEE;
        if (isset($pricing->minFee)) {
            $this->minFee = $pricing->minFee;
        }

        $this->minCost = E4S_PRICE_MIN_COST;
        if (isset($pricing->minCost)) {
            $this->minCost = $pricing->minCost;
        }

        $this->addCost = E4S_PRICE_ADD_COST;
        if (isset($pricing->addCost)) {
            $this->addCost = $pricing->addCost;
        }

        $this->perc = E4S_PRICE_PERC_CHARGED;
        if (isset($pricing->percCharged)) {
            $this->perc = $pricing->percCharged;
        }

        $this->nationMinCost = E4S_PRICE_NATIONAL_MIN_COST;
        if (isset($pricing->nationMinCost)) {
            $this->nationMinCost = $pricing->nationMinCost;
        }

        $this->nationalAddCost = E4S_PRICE_NATIONAL_ADD_COST;
        if (isset($pricing->nationalAddCost)) {
            $this->nationalAddCost = $pricing->nationalAddCost;
        }

	    $options = $config['options'];
        $this->message = '';
        if (isset($options->message)) {
            $this->message = $options->message;
        }
    }

	static public function getDefaultPricing(){
		$config = e4s_getConfig();
		$options = $config['options'];
		if ( !isset($options->pricing) ){
			$pricing = new stdClass();

			$pricing->minFee = E4S_PRICE_MIN_FEE;
			$pricing->minCost = $options->minCost;
			$pricing->addCost = $options->addCost;
			$pricing->percCharged = $options->percCharged;
			$pricing->nationalminCost = $options->nationalminCost;
			$pricing->nationaladdCost = $options->nationaladdCost;
			$options->pricing = $pricing;
		}
		$pricing = e4s_getDataAsType($options->pricing, E4S_OPTIONS_STRING);
		return e4s_getDataAsType($pricing, E4S_OPTIONS_OBJECT);
	}
	static public function validatePriceDate($saleEndDate, $entriesOpenDate, $entriesCloseDate):string {
		if ( $saleEndDate === "0000-00-00") {
			return '';
		}
		$saleEndDateTs = strtotime($saleEndDate);
		$entriesOpenTs = strtotime($entriesOpenDate);
		$entriesCloseTs = strtotime($entriesCloseDate);
		if ( $saleEndDateTs < $entriesOpenTs ){
			return 'Prices MUST have a date after the entries open.';
		}
		if ( $saleEndDateTs > $entriesCloseTs ){
			return 'Prices MUST have a date before entries close.';
		}
		return '';
	}
    public function getMinCost() {
        if ($this->isNational) {
            return $this->nationMinCost;
        }
        return $this->minCost;
    }

    public function getMessage() {
        return $this->message;
    }

    public function getStripeFeeForPrice($ao, $price) {
        if (is_null($ao)) {
            $ao = $this->defaultAO;
        }
        $stripeFee = ($price * $this->getStripePerc()) + $this->getStripeAddCost($ao);

        return round($stripeFee, 2);
    }

    public function getStripePerc() {
        return 0.014;
    }

    public function getStripeAddCost($ao = null) {
        if (is_null($ao)) {
            $ao = $this->defaultAO;
        }
        if ($ao === E4S_AOCODE_EA) {
            return E4S_STRIPE_ADD_COST;
        }
        if ($ao === E4S_AOCODE_AAI) {
            return E4S_STRIPE_NATIONAL_ADD_COST;
        }
        return 0;
    }

    public function getOrgPrice($price, $oldPricing = FALSE) {
//        $orgPerc = $price * $this->getPerc()/100;
//        echo "<br>perc:" . $this->getPerc();
//        echo "<br>orgPerc:" . $orgPerc;
//        echo "<br>addCost:" . $this->getAddCost();
//        $orgPrice = $price - $orgPerc - $this->getAddCost();
        if ($oldPricing) {
            return $price - $this->getOldFeeForPrice($price);
        }
        return $price - $this->getFeeForPrice($price);
    }

    public function getOldFeeForPrice($price) {
        $fee = E4S_PRICE_MIN_FEE;
        $minPrice = E4S_PRICE_OLD_MIN_COST;
        if ($this->defaultAO === E4S_AOCODE_AAI) {
            $fee = E4S_PRICE_NATIONAL_MIN_FEE;
            $minPrice = E4S_PRICE_NATIONAL_MIN_COST;
        }
        if ($price <= $minPrice) {
            return $fee;
        }
        return $price * (E4S_PRICE_PERC_CHARGED / 100);
    }

    public function getFeeForPrice($price) {
        $fee = $this->percentageOf($price, 2);
        $fee += $this->getAddCost();
        $minFee = $this->getMinFee();

        if ($fee < $minFee) {
            $fee = $minFee;
        }

        return $fee;
    }

    public function percentageOf($value, $decimals = 2) {
        return round($value * ($this->getPerc() / 100), $decimals);
    }

    public function getPerc() {
        return $this->perc;
    }

    public function getAddCost() {
        if ($this->isNational) {
            return $this->nationalAddCost;
        }
        return $this->addCost;
    }

    public function getMinFee() {
        return $this->minFee;
    }

    public function getCostWithoutE4SFeeFromAthletePrice($price) {

        $priceWithoutAddCost = $price - $this->getAddCost();
        $dec = ($this->perc / 100) + 1;
        $fee = $priceWithoutAddCost / $dec;
        $minFee = $this->getMinFee();

        if ($fee < $minFee) {
            $fee = $minFee;
        }

        return $fee;
    }

    public function calculateFees($direction, $options, $price, $fee) {
        $feeIncluded = $options->feeIncluded;
        $priceModel = new stdClass();
        $priceModel->price = $price;
        if ($direction === E4S_PRICE_IN) {
            $fee = $this->getFeeForPrice($price);
        }

        $priceModel->fee = $fee;

        // Check or calculate the fee
        // if Direction is Inbound, the price written to db is price to athlete
        if ($direction === E4S_PRICE_IN) {
            if (!$feeIncluded) {
                $priceModel->price = $price + $fee;
            }
        } else {
            if (!$feeIncluded) {
                $priceModel->price = $price - $fee;
            }
        }
        return $priceModel;
    }

    public function recalculatePrices($allowNone = FALSE) {
        $sql = 'select *
            from ' . E4S_TABLE_EVENTPRICE . '
            where compid = ' . $this->compid;
        $results = e4s_queryNoLog($sql);
        if ($results->num_rows < 1) {
            if ($allowNone) {
                // ticket only comp
                return;
            }
            Entry4UIError(9036, 'Failed to read prices for cloned competition ' . $this->compid, 200, '');
        }
        $rows = $results->fetch_all(MYSQLI_ASSOC);
        foreach ($rows as $row) {
            $options = e4s_addDefaultPriceOptions($row['options']);
            $salePrice = (float)$row['saleprice'];
            $salefee = (float)$row['salefee'];
            $price = (float)$row['price'];
            $fee = (float)$row['fee'];

            if ($options->feeIncluded === FALSE) {
                $salePrice = $salePrice - $salefee;
                $price = $price - $fee;
            }

            $salefee = $this->getFeeForPrice($salePrice);
            $fee = $this->getFeeForPrice($price);
            if ($options->feeIncluded === FALSE) {
                $salePrice = $salePrice + $salefee;
                $price = $price + $fee;
            }

            $update = 'update ' . E4S_TABLE_EVENTPRICE . ' set saleprice = ' . $salePrice . ',
                            salefee   = ' . $salefee . ',
                            price = ' . $price . ',
                            fee   = ' . $fee . '
                        where id = ' . $row['ID'];
            e4s_queryNoLog($update);
        }
    }

    public function getEventFreePrice() {
        // does a final free price exist ?
        $priceRec = $this->_readEventFeePrice();
        if (is_null($priceRec)) {
            $priceRec = $this->_createEventFeePrice();
        }
        return $priceRec;
    }

    private function _readEventFeePrice() {
        $sql = 'select *
              from ' . E4S_TABLE_EVENTPRICE . '
              where compid = ' . $this->compid . "
              and   name = '" . E4S_QUALIFY_PRICE . "'";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            return $this->_normaliseObj($result->fetch_object());
        }
        return null;
    }

    private function _normaliseObj($obj) {
        $obj->compId = $obj->compid;
        unset($obj->compid);
        $obj->id = $obj->ID;
        unset($obj->ID);
        $obj->salePrice = $obj->saleprice;
        unset($obj->saleprice);
        $obj->saleEndDate = $obj->saleenddate;
        unset($obj->saleenddate);
        $obj->saleFee = $obj->salefee;
        unset($obj->salefee);
        return $obj;
    }

    private function _createEventFeePrice() {
        $sql = 'insert into ' . E4S_TABLE_EVENTPRICE . ' (compid, name,description,price, saleprice,saleenddate,salefee,fee, options) 
                values (
                    ' . $this->compid . ",
                    '" . E4S_QUALIFY_PRICE . "',
                    'Qualifying price record',
                    0,
                    0,
                    '',
                    0,
                    0,
                    ''
                )";
        e4s_queryNoLog($sql);
        return $this->_readEventFeePrice();
    }
}

