<?php

include_once E4S_OTD_PATH . '/tfConstants.php';

class resultsClass {
    public $compId;
    public $rootDir;
    public $compDir;
    public $pfDir;

    public function __construct($compId) {
        $this->compId = $compId;
        $GLOBALS['R4S_COMP_ID'] = $compId;
        $this->rootDir = $_SERVER['DOCUMENT_ROOT'];
        $this->compDir = R4S_RESULT_DIR . '/' . $this->compId;
    }

	public static function isValidResult($result){
		$valid = true;
		if ( !is_numeric($result)){
			$result = str_replace(':', '', $result);
			$result = str_replace('.', '', $result);
		}
		if ( !is_numeric($result)){
			$valid = false;
		}
		return $valid;
	}
    public static function getResultFromSeconds($origScore) {
        $score = resultsClass::getNonNumeric($origScore);
        if ($score !== '') {
            return $score;
        }
        $origScore = (string)$origScore;

        $checkScore = str_replace(':', '.', $origScore);
        $scoreArr = preg_split('~\.~', $checkScore);
        if (sizeof($scoreArr) > 2) {
            $origScore = resultsClass::getResultInSeconds($origScore);
        }
        if (strpos($origScore, '.') === FALSE) {
            $origScore = $origScore . '.00';
        }
        $scoreArr = preg_split('~\.~', $origScore);
        $secs = (int)$scoreArr[0];
		$mins = 0;
        if (strlen($scoreArr[1]) === 1) {
            $mills = $scoreArr[1] . '0';
        } else {
            $mills = substr($scoreArr[1] . ' ', 0, 2);
        }
        if ($secs > 59) {
            $mins = floor($secs / 60);
            $secs = floor((float)$origScore - ($mins * 60));

            if ($secs < 10) {
                $secs = '0' . $secs;
            }
            $origScore = $mins . ':' . $secs . '.' . $mills;
        } else {
            $origScore = $secs . '.' . $mills;
        }

        return (string)$origScore;
    }

    public static function getNonNumeric($score) {
        $scoreTxt = strtoupper('' . $score);
        if ($scoreTxt === 'DQ' or $scoreTxt === 'DNS' or $scoreTxt === 'DNF' or $scoreTxt === 'DIS') {
            return $scoreTxt;
        }
        return '';
    }

    public static function getResultInSeconds($origScore) {
        $score = resultsClass::getNonNumeric($origScore);
        if ($score !== '') {
            return $score;
        }

        if (strpos($origScore, ':') !== FALSE and strpos($origScore, '.') === FALSE) {
            $origScore .= '.00';
        }
        $score = str_replace(':', '.', $origScore);
        $scoreArr = preg_split('~\.~', $score);

        if (sizeof($scoreArr) > 2) {
            $mins = (int)$scoreArr[0];
            $secs = (int)$scoreArr[1];
            $mills = substr($scoreArr[2] . ' ', 0, 2);
            $score = (($mins * 60) + $secs) . '.' . $mills;
        } else {
            $score = $origScore;
        }
		return self::ensureDecimals($score);
}

	public static function ensureDecimals($perf){
		return number_format((float)$perf, 2, '.', '');
	}
    public function getResultsAndMetaForEvent($eventNo, $heatNo) {
        $response = new stdClass();
        $response->results = $this->getResultsForEvent($eventNo, $heatNo);
        $response->meta = $this->getMetaForEvent($eventNo);
        return $response;
    }

    public function getResultsForEvent($eventNo, $heatNo) {
        $sql = '
            select * 
            from ' . E4S_TABLE_CARDRESULTS . "
            where compid = {$this->compId}
            and eventNo = " . $eventNo . "
            and resultkey = 'h" . $heatNo . "'
            order by athleteid
        ";

        $results = e4s_queryNoLog($sql);
        $returnResults = array();
        while ($obj = $results->fetch_object()) {
            $options = e4s_getOptionsAsObj($obj->options);
            if (isset($options->timeInSeconds)) {
                $obj->timeInSeconds = (float)$options->timeInSeconds;
            } else {
                if (strpos($obj->resultvalue, ':') !== FALSE) {
                    $arr = preg_split('~:~', $obj->resultvalue);
                    $mins = (int)$arr[0];
                    $secs = (float)($arr[1]);
                    $obj->timeInSeconds = ($mins * 60) + $secs;
                } else {
                    $obj->timeInSeconds = (float)$obj->resultvalue;
                }
            }
            $obj->eaAward = 0;
            if (isset($options->eaAward)) {
                $obj->eaAward = $options->eaAward;
            }
            $obj->ageGroup = '';
            if (isset($options->ageGroup)) {
                $obj->ageGroup = $options->ageGroup;
            }
//            $obj->timeInSeconds = e4s_ensureString($obj->timeInSeconds);
            $returnResults[] = $obj;
        }
        return $returnResults;
    }

    public function getMetaForEvent($eventNo) {
        $obj = null;
        $sql = "select name eventName,
                       DATE_FORMAT(startdate,'" . ISO_MYSQL_DATE . "') as scheduleTime
                from " . E4S_TABLE_EVENTGROUPS . ' 
                where compid = ' . $this->compId . '
                and eventno = ' . $eventNo;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            $obj = $result->fetch_object();
        }
        return $obj;
    }

    public function getResultsForComp() {
        $sql = '
            select * 
            from ' . E4S_TABLE_CARDRESULTS . "
            where compid = {$this->compId}
            order by eventno, athleteid
        ";
        return $this->_getResults($sql);
    }

    private function _getResults($sql) {
		$dtFormat = 'H:i';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return array();
        }
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $retArr = array();
        $athleteArr = array();
        $lasteventno = -1;
		$firstResultTS = null;
        foreach ($rows as $row) {
            $eventno = (int)$row['eventno'];
            if ($lasteventno !== $eventno and $lasteventno !== -1) {
	            $athleteArr[0] = '';
				if ( $firstResultTS !== null) {
					$athleteArr[0] = date( $dtFormat, $firstResultTS  );
				}
                $retArr[$lasteventno] = $athleteArr;
                $athleteArr = array();
	            $firstResultTS = null;
            }
			if ($firstResultTS === null){
	            $firstResultTS = strtotime($row['created']);
			}
            $useAthleteId = (int)$row['athleteid'];
            if (isset($athleteArr[$useAthleteId])) {
                $trialArr = $athleteArr[$useAthleteId];
            } else {
                $trialArr = array();
            }

            $trialArr[$row['resultkey']] = $row['resultvalue'];

            $lasteventno = $eventno;
            $athleteArr[$useAthleteId] = $trialArr;
        }
		// save firstResultTS to array 0 in format 'h:i'
	    $athleteArr[0] = '';
	    if ( $firstResultTS !== null) {
		    $athleteArr[0] = date( $dtFormat, $firstResultTS );
	    }

        $retArr[$lasteventno] = $athleteArr;
        return $retArr;
    }

    public function _getKey($row) {
        return $row['athleteid'] . '_' . $row['resultkey'];
    }

    public function getBibsForComp() {
        $sql = 'select athleteid, bibno
                from ' . E4S_TABLE_BIBNO . '
                where compid = ' . $this->compId;
        $result = e4s_queryNoLog($sql);
        $bibs = array();
        while ($obj = $result->fetch_object()) {
            $bibs[$obj->bibno] = (int)$obj->athleteid;
        }
        return $bibs;
    }
}