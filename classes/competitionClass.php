<?php
include_once E4S_FULL_PATH . 'competition/compStatus.php';
include_once E4S_FULL_PATH . 'classes/e4sAgeClass.php';
include_once E4S_FULL_PATH . 'classes/e4sQualifyClass.php';

const E4S_STANDARD_ORDER_EVENT = 'event';
const E4S_STANDARD_ORDER_AGE = 'age';
const E4S_STANDARD_ORDER_COMBINED = 'ageevent';
const E4S_STANDARD_ORDER_COMBINED_JOIN = '-';
const E4S_ENTRY_PAID_NULL = -1;
class e4sCompetition {
    public $compId;
    public $row;
    public $options;
    public $isNational = FALSE;
    public $priceObj;
    public $extraInfo;
    public $config;
    public $egTypes;
    public $ceObjs;
    public $eventGroups;
    public $eventTypes;
    public $clubSecurityCleared = FALSE;
    public $ageGroups;
    public $clubComp;
    public $entries;
    public $youngestDob;
    public $oldestDob;

    public function __construct($compId = 0) {
        $this->compId = $compId;
        $this->extraInfo = TRUE;
        $this->clubComp = null;
        $this->egTypes = null;
        $this->eventGroups = array();
        $this->ceObjs = array();
        $this->entries = array();
        $this->config = e4s_getConfig();
    }

    public static function withID($compId, $extraInfo = TRUE) {
        $instance = new self($compId);
        $instance->extraInfo = TRUE;
        $instance->loadByID();
        $instance->setPrice();
        return $instance;
    }

	function useArchiveData(){
		$useArchive = get_option( 'woocommerce_archive_e4s_enabled' );
		if ( $useArchive ) {
			$max_age  = get_option( 'woocommerce_archive_e4s_older_than_days' );
			$max_age  = - 100; // for testing
			$compDate = $this->getDate();
			// if compDate is greater than $max_age days old
			$now        = time(); // timestamp
			$compDateDT = date_create( $compDate );
			$compDateTS = date_timestamp_get( $compDateDT );
			if ( ( ( $now - $compDateTS ) / 86400 ) < $max_age ) {
				$useArchive = false;
			}
		}
		return $useArchive;
	}
    public static function getNextCompForLocation($locId) {
        $sql = '
		            select distinct c.id
		            from ' . E4S_TABLE_COMPETITON . ' c,
		                 ' . E4S_TABLE_EVENTGROUPS . ' eg
		            where eg.compId = c.id
		            and c.active = true
		            and c.date >= CURRENT_DATE
		            and active = 1
				';
		if ( $locId > 0 ) {
			$sql .= " and c.locationid = {$locId}";
		}else{
			$sql .= ' and c.r4s = 1';
		}
		$sql .= ' 
                order by c.date
        ';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            return 0;
        }
        $row = $result->fetch_object();
        return (int)$row->id;
    }

	function getQrCode($type = 'results'){
		include_once E4S_FULL_PATH . "/qr/qrlib.php";
		$filename =  '/results/' . $this->getID() . '_qr.png';
		$fullfilename =  $_SERVER['DOCUMENT_ROOT'] . $filename;
		// test if filename exists
		if (!file_exists($fullfilename)) {
			QRcode::png( 'https://' . E4S_CURRENT_DOMAIN . '/' . $this->getID() . '/results', $fullfilename );
		}
		return $filename;
	}
    protected function loadByID() {
	    $this->ceObjs = [];
	    $this->eventGroups = [];
	    $this->row = null;
		$processingCompIds = $this->getID();
		if ( array_key_exists(E4S_PROCESSING_HOMEPAGE_COMPS, $GLOBALS)) {
			if ( array_key_exists(E4S_PROCESSING_COMPS_DATA, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] )) {
				if ( array_key_exists(E4S_PROCESSING_COMPS_IDS, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] )) {
					if ( array_key_exists( $this->getID(), $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_IDS ] ) ) {
						$this->row = $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_DATA ][ $this->getID() ];
						$this->updateFromRow();

						return;
					}
				}
			}else{
				$GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_DATA] = [];
				if ( array_key_exists(E4S_PROCESSING_COMPS_IDS, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] )) {
					$processingCompIds                   = $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_IDS ];
					$processingCompIds[ $this->getID() ] = $this->getID(); // ensure current is in that list
					$processingCompIds                   = implode( ",", $processingCompIds );
				}
			}
		}
        // do query
        $compSql = 'Select c.*,
                    a.name areaname,
                    curdate() today,
                    now() systemtime,
                    date compDate, 
                    (c.date - current_date) as daysToComp,
                    (date(c.entriesclose) - current_date) as daysToClose, ';

        if ($this->extraInfo) {
            $compSql .= 'l.location location,
                    l.Address1 locAddress1,
                    l.Address2 locAddress2,
                    l.town loctown,
                    l.county loccounty,
                    l.directions locdirections,
                    l.postcode locpostcode,
                    l.contact loccontact,
                    l.website locwebsite,
                    cc.club compclub, cc.logo, ';
        }
        $compSql .= 'ctc.id ctcid,
                    ctc.maxathletes,
                    ctc.maxteams,
                    ctc.maxmale,
                    ctc.maxfemale,
                    ctc.maxagegroup,
                    ctc.uniqueevents,
                    ctc.singleagegroup ';
        $compSql .= ' from ' . E4S_TABLE_COMPETITON . ' c left join ' . E4S_TABLE_COMPTEAMCONFIG . ' ctc on ctc.areaid = c.areaid left join ' . E4S_TABLE_AREA . ' a on a.id = c.areaid';
        if ($this->extraInfo) {
            $compSql .= ',
                    ' . E4S_TABLE_LOCATION . ' l,
                    ' . E4S_TABLE_COMPCLUB . ' cc ';
        }
        $compSql .= ' where c.id in (' . $processingCompIds . ')';
        if ($this->extraInfo) {
            $compSql .= ' and l.id = c.locationid
                          and cc.id = c.compclubid';
        }
        $result = e4s_queryNoLog($compSql);
        if ($result->num_rows > 0) {
	        if ( array_key_exists(E4S_PROCESSING_HOMEPAGE_COMPS, $GLOBALS)) {
				while ($row = $result->fetch_assoc()) {
					$GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_DATA][$row['ID']] = $row;
				}
		        $this->row = $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_DATA][$this->getID()];
	        }else {
		        $this->row = $result->fetch_assoc();
	        }
	        $this->updateFromRow();
        }
    }

    public function getSourceId() {
        $options = $this->getOptions();
        if ($options->sourceId > 0) {
            return $options->sourceId;
        }
        return $this->getID();
    }

    public function setMinMaxAgeGroups() {
        if (is_null($this->ageGroups)) {
            $this->getAgeGroups();
        }
        $this->youngestDob = '';
        $this->oldestDob = '';
        foreach ($this->ageGroups as $ageGroup) {
            if ($ageGroup['toDate'] > $this->youngestDob) {
                $this->youngestDob = $ageGroup['toDate'];
            }
            if ($ageGroup['fromDate'] < $this->oldestDob or $this->oldestDob === '') {
                $this->oldestDob = $ageGroup['fromDate'];
            }
        }
    }

    public function getTeamExcludeCount() {
        $excluded = array();
        $excludedCnt = 0;
        $ceObjs = $this->getCeObjs();
        foreach ($ceObjs as $ceObj) {
            $options = $ceObj->options;
            if ($options->isTeamEvent) {
                if ($options->excludeFromCntRule) {
                    if (!array_key_exists($ceObj->egId, $excluded)) {
                        $excluded[$ceObj->egId] = $ceObj->egId;
                        $excludedCnt++;
                    }
                }
            }
        }
        return $excludedCnt;
    }

    public function getIndivExcludeCount() {
        $excluded = array();
        $excludedCnt = 0;
        $ceObjs = $this->getCeObjs();
        foreach ($ceObjs as $ceObj) {
            $options = $ceObj->options;
            if (!$options->isTeamEvent) {
                if ($options->excludeFromCntRule) {
                    if (!array_key_exists($ceObj->egId, $excluded)) {
                        $excluded[$ceObj->egId] = $ceObj->egId;
                        $excludedCnt++;
                    }
                }
            }
        }
        return $excludedCnt;
    }

    public function getStandards($groupBy = E4S_STANDARD_ORDER_EVENT) {
        $options = $this->getOptions();
        $standards = $options->standards;

        if (empty($standards)) {
            return $standards;
        }
        $sql = 'select sv.*,
                       s.description,
                       ag.name ageGroupName,
                       e.name eventName,
                       e.tf
                from ' . E4S_TABLE_STANDARDVALUES . ' sv,
                     ' . E4S_TABLE_STANDARDS . ' s,
                     ' . E4S_TABLE_AGEGROUPS . ' ag,
                     ' . E4S_TABLE_EVENTS . ' e
                where sv.standardId = s.id
                and ag.id = sv.agegroupid
                and e.id = sv.eventId
                and s.id in (' . implode(',', $standards) . ')
                order by standardId, eventId, ageGroupId';

        $result = e4s_queryNoLog($sql);
        $retArr = array();
        while ($obj = $result->fetch_object(E4S_STANDARD_VALUES_OBJ)) {
            if ($obj->tf === E4S_EVENT_TRACK) {
                $obj->valueText = E4S_ENSURE_STRING . resultsClass::getResultFromSeconds($obj->value);
            } else {
                $obj->valueText = E4S_ENSURE_STRING . resultsClass::getResultInSeconds($obj->value);
            }
            $key = $obj->eventName;
            $subKey = $obj->ageGroupName;
            if ($groupBy === E4S_STANDARD_ORDER_AGE) {
                $key = $obj->ageGroupName;
                $subKey = $obj->eveventNameentId;
            }
            if ($groupBy === E4S_STANDARD_ORDER_COMBINED) {
                $key = $obj->ageGroupId . E4S_STANDARD_ORDER_COMBINED_JOIN . $obj->eventId;
                $subKey = '';
            }
            $rootKey = $obj->description;
            if (!array_key_exists($rootKey, $retArr)) {
                $retArr[$rootKey] = array();
            }
            if (!array_key_exists($key, $retArr[$rootKey])) {
                $retArr[$rootKey][$key] = array();
            }
            if ($subKey !== '') {
                if (!array_key_exists($subKey, $retArr[$rootKey][$key])) {
                    $retArr[$rootKey][$key][$subKey] = array();
                }
                $retArr[$rootKey][$key][$subKey][] = $obj;
            } else {
                $retArr[$rootKey][$key][] = $obj;
            }
        }

        return $retArr;
    }

    public function getFormattedName() {
        return self::getDisplayName($this->getID(), $this->getName());
    }

    public static function getFormattedDisplayName($id, $name) {
        return $id . ' : ' . $name;
    }

    public function getDisplayName($id = 0, $name = '') {
        if ($id === 0) {
            $id = $this->getID();
        }
        if ($name === '') {
            $name = $this->getName();
        }
        return self::getFormattedDisplayName($id, $name);
    }

    public function setPhotofinish($type) {
        $options = $this->getOptions();
        $options->pf->type = $type;
        $this->updateCompOptions($options);
    }

    public function updateSettings($action, $val) {
        switch ($action) {
            case E4S_COMP_SETTING_CALLROOM:
                $options = $this->getOptions();
                $options->cardInfo->callRoom = $val;
                $this->updateCompOptions($options);
                $data = new stdClass();
                $data->callRoom = $val;
                $this->sendSocketInfo($data, R4S_SOCKET_CALLROOM);
                break;
	        case E4S_COMP_SETTING_SYSTEM:
				$val = (int)$val;
		        if ($val < 0 or $val > 5) {
			        Entry4UIError(9266, 'Invalid PF System passed');
		        }
		        $options = $this->getOptions();
		        $options->pf->system = $val;
		        $this->updateCompOptions($options);
		        $this->sendSocketInfo($val, R4S_SOCKET_SETPFSYSTEM);
				break;
            case E4S_COMP_SETTING_PF:
                $val = strtoupper($val);
                if ($val !== E4S_PF_TT and $val !== E4S_PF_LYNX) {
                    Entry4UIError(9265, 'Invalid PF Type passed');
                }
                $options = $this->getOptions();
                $options->pf->type = $val;
                $this->updateCompOptions($options);
                $this->sendSocketInfo($val, R4S_SOCKET_SETPF);
                break;
        }
    }

    public static function getClubComps() {
        $sql = 'select id,
                       name,
                       date
                from ' . E4S_TABLE_COMPETITON . "
                where options like '%isClubComp\":true%'
                and date > date_sub(current_date, interval 7 day)
                and id > 0
                order by date desc";
        $result = e4s_queryNoLog($sql);
        $comps = array();
        while ($obj = $result->fetch_object()) {
            $obj->id = (int)$obj->id;
            $comps[] = $obj;
        }
        Entry4UISuccess($comps);
    }

    public function isClubcomp() {
        $options = $this->getOptions();
        return $options->isClubComp;
    }

    public function checkClubComp(): int {
        if (!$this->isClubcomp()) {
//            Comp not a clubcomp
            return -1;
        }

        $user = $GLOBALS[E4S_USER];
        $key = E4S_CLUBCOMP . $this->getID();
        if (isset($user->{$key})) {
            return (int)$user->{$key};
        }
        if ($this->isOrganiser()) {
            // get list of counties and set to the first one
            return 0;
        }
        // no Access to comp
        return -1;
    }

    public function setClubComp($includeEntryData = TRUE) {
        $ccId = $this->checkClubComp();
        if ($ccId < 0) {
            return null;
        }
        $loadRequired = FALSE;
        if (is_null($this->clubComp)) {
            $loadRequired = TRUE;
        } elseif ($this->clubComp->getCcId() !== $ccId) {
            $loadRequired = TRUE;
        } elseif ($includeEntryData) {
            if (!$this->clubComp->hasEntryData()) {
                $loadRequired = TRUE;
            }
        }

        if ($loadRequired) {
            $this->clubComp = new clubCompClass($this->getID(), $ccId, $includeEntryData);
        }
        return $this->clubComp;
    }

    public function getClubId() {
	    $clubComp = $this->checkClubComp();
		if ( $clubComp > -1 ) {
			$this->setClubComp( FALSE );
			if ( ! is_null( $this->clubComp ) ) {
				return $this->clubComp->getClubId();
			}
		}
        return 0;
    }

    public function getClubCompData() {
        if ($this->checkClubComp() < 0) {
            return new stdClass();
        }
        return $this->clubComp->data;
    }

    public function clubCompFullForUser() {
        if (is_null($this->clubComp)) {
            return FALSE;
        }

        return $this->clubComp->userEntryLimitReached();
    }

    public function clubCompEGFullForUser($egId) {
        if (is_null($this->clubComp)) {
            return FALSE;
        }

        return $this->clubComp->userEGLimitReached($egId);
    }

    public function allowExpiredRegistration() {
		return false;
        $options = $this->getOptions();
        e4s_addDebugForce($options);
        return $options->allowExpiredRegistration;
    }

    protected function updateFromRow() {
        if (array_key_exists('ID', $this->row)) {
            $this->row['ID'] = (int)$this->row['ID'];
        }
        if (array_key_exists('id', $this->row)) {
            $this->row['id'] = (int)$this->row['id'];
        }

        if ((int)$this->row['active'] === 1) {
            $this->row['active'] = TRUE;
        } else {
            $this->row['active'] = FALSE;
        }

        $this->row['locationid'] = (int)$this->row['locationid'];
        $this->row['compclubid'] = (int)$this->row['compclubid'];
        // fill all properties from the row
        $this->setOptions($this->row['options']);
		$this->getPricing();
        if (array_key_exists('compOrg', $this->row)) {
            if (array_key_exists('name', $this->row['compOrg'])) {
                if ($this->row['compOrg']['name'] === 'National') {
                    $this->setNationalComp(TRUE);
                }
            }
        }

        if ($this->getOrganisationName() === 'National') {
            $this->setNationalComp(TRUE);
        }

        if (is_null($this->row['lastentrymod'])) {
            $this->row['lastentrymod'] = '2000-01-01 00:00:00';
        }
        $ctcObj = new stdClass();
        if (array_key_exists('ctcid', $this->row)) {
            $ctcObj->ctcid = $this->row['ctcid'];
            $ctcObj->maxathletes = $this->row['maxathletes'];
            $ctcObj->maxteams = $this->row['maxteams'];
            $ctcObj->maxmale = $this->row['maxmale'];
            $ctcObj->maxfemale = $this->row['maxfemale'];
            $ctcObj->maxagegroup = $this->row['maxagegroup'];
            $ctcObj->uniqueevents = $this->row['uniqueevents'];
            $ctcObj->singleagegroup = $this->row['singleagegroup'];
        }
        $this->row['ctc'] = $ctcObj;

        if ($this->clubSecurityCleared) {
            // need to update the options as the period has passed for security
            $this->updateRowOptions();
        }
    }

    public function setNationalComp($val) {
        $this->isNational = $val;
    }

    public function getOrganisationName() {
        if (array_key_exists('compclub', $this->row)) {
            return $this->row['compclub'];
        }
        if (array_key_exists('club', $this->row)) {
            return $this->row['club'];
        }
        return '';
    }

    private function updateRowOptions($options = null) {
        $setOptions = FALSE;
        if (is_null($options)) {
            $options = $this->options;
            $setOptions = TRUE;
        }
        $options = $this->updateCompOptions($options);
        if ($setOptions) {
            $this->options = $options;
        }
    }

    public function getOptionsForWriting($options = null) {
        if (is_null($options)) {
            $options = $this->options;
        }
        $options = e4s_addDefaultCompOptions($options);
        $options = $this->cleanBibOptions($options);
//        $options = e4s_removeDefaultCompOptions($options);
        $options->dates = $this->getAllEventDates($options);
        // added for new comps so remove here
        unset($options->compDate);
        $options = $this->cleanAthleteSecurity($options);
        $options = $this->_cleanEntryUI($options);
        $options = e4s_removeDefaultCompOptions($options);
        return $options;
    }

    private function _cleanEntryUI($options) {
        $ui = $options->ui;
        $hasSecondary = $this->hasSecondary();

        if (!$hasSecondary) {
//            Can not select shop
            $ui->entryDefaultPanel = E4S_UI_SCHEDULE;
        }

        if ($ui->entryDefaultPanel === E4S_UI_SHOP_ONLY and $hasSecondary) {
            $ui->sectionsToHide->SHOP = FALSE;
            $ui->sectionsToHide->ATHLETES = TRUE;
            $ui->sectionsToHide->SCHEDULE = TRUE;
            $ui->sectionsToHide->TEAMS = TRUE;
        } else {
            if ($ui->entryDefaultPanel === E4S_UI_SHOP and $hasSecondary) {
                $ui->sectionsToHide->SHOP = FALSE;
            } else {
                $ui->sectionsToHide->SHOP = TRUE;
            }
            if ($this->hasIndivEvents()) {
                $ui->sectionsToHide->ATHLETES = FALSE;
                $ui->sectionsToHide->SCHEDULE = FALSE;
            } else {
                $ui->sectionsToHide->ATHLETES = TRUE;
                $ui->sectionsToHide->SCHEDULE = TRUE;
            }
            if ($this->hasTeamEvents()) {
                $ui->sectionsToHide->TEAMS = FALSE;
            } else {
                $ui->sectionsToHide->TEAMS = TRUE;
            }
        }
        return $options;
    }

    public function haveBibNosBeenGenerated() {
        $sql = "
         select max(if(isnull(bibno) or bibno = '',0,bibno)) bibNo
            from " . E4S_TABLE_ENTRYINFO . '
            where compId = ' . $this->getID();
        $result = e4s_queryNoLog($sql);
        $obj = $result->fetch_object();
        return (int)$obj->bibNo > 0;
    }

    public function cleanBibOptions($options) {
        $options->bibNos = (string)$options->bibNos;
        if ($options->bibSort1 === '') {
            unset($options->bibSort1);
        }
        if ($options->bibSort2 === '') {
            unset($options->bibSort2);
        }
        if ($options->bibSort3 === '') {
            unset($options->bibSort3);
        }
        return $options;
    }

    public function getDatesFromOptions($fromToday = FALSE) {
        $options = $this->options;
        if (!isset($options->dates)) {
            $options->dates = $this->getAllEventDates();
            $this->updateRowOptions();
        }
        if ($fromToday) {
//            $compDate = strtotime($this->getDate());
            $today = strtotime(date('Y-m-d'));
            $dates = [];
            foreach ($options->dates as $dateStr) {
                $date = strtotime($dateStr);
                if ($today <= $date) {
                    $dates[] = $dateStr;
                }
            }
            // if still at leat one date, set it else leave as is
            if (sizeof($dates) > 0) {
                $options->dates = $dates;
            }
        }
        return $options->dates;
    }

    public function getAllEventDates($options = null) {
        $dates = array();
        $uniqueDates = array();
        $egObjs = $this->getEGObjs();
        foreach ($egObjs as $eventGroup) {
            $useDate = Date('Y-m-d', strtotime($eventGroup->startDate));
            if (!array_key_exists($useDate, $uniqueDates)) {
                $dates[] = $useDate;
                $uniqueDates[$useDate] = TRUE;
            }
        }
        if (empty($dates)) {
            $date = '';
            // Handle a comp with no events ? New comp in builder
            $row = $this->getRow();
            if (!is_null($row)) {
                $date = $row['Date'];
            } else {
                if (!is_null($options)) {
                    if (isset($options->compDate)) {
                        $date = Date('Y-m-d', strtotime($options->compDate));
                    }
                }
            }
            if ($date !== '') {
                $dates = [$date];
            }
        }
        if (!empty($dates)) {
            sort($dates);
        }
        return $dates;
    }

    public function getRow() {
        $this->_checkTickets();
        return $this->row;
    }

    public function cleanAthleteSecurity($options) {
        $clubs = array();
        if (!isset($options->athleteSecurity)) {
            return $options;
        }
        if (!isset($options->athleteSecurity->clubs)) {
            $options->athleteSecurity->clubs = $clubs;
            $options->athleteSecurity->onlyClubsUpTo = '';
            return $options;
        }
        if (empty($options->athleteSecurity->clubs) or is_null($options->athleteSecurity->clubs)) {
            $options->athleteSecurity->clubs = $clubs;
            $options->athleteSecurity->onlyClubsUpTo = '';
            return $options;
        }
        // we have clubs, so check if these should now be cleared
        if (isset($options->athleteSecurity->onlyClubsUpTo) and $options->athleteSecurity->onlyClubsUpTo !== '') {
            $upTo = $options->athleteSecurity->onlyClubsUpTo;
            $upTo = e4s_iso_to_sql($upTo);
            $now = date(E4S_MYSQL_DATE);

            $uptoDt = strtotime($upTo);
            $nowDt = strtotime($now);

            if ($uptoDt < $nowDt) {
                $options->athleteSecurity->clubs = $clubs;
                $options->athleteSecurity->onlyClubsUpTo = '';
                $this->clubSecurityCleared = TRUE;
                return $options;
            }
        }

        foreach ($options->athleteSecurity->clubs as $club) {
            $newClub = new stdClass();
            $newClub->id = $club->id;
            $clubs[] = $newClub;
        }

        $options->athleteSecurity->clubs = $clubs;
        return $options;
    }

	public function getCeObjs($force = FALSE) {
        $this->_setCeObjs($force);
        return $this->ceObjs;
    }

    public function getEGObjs($force = FALSE) {
        $this->_setEventGroups($force);
        return $this->eventGroups;
    }

    private function _setCeObjs($force = FALSE) {
	    $processingCompIds = $this->getID();
	    if ( array_key_exists(E4S_PROCESSING_HOMEPAGE_COMPS, $GLOBALS)) {
		    if ( array_key_exists(E4S_PROCESSING_COMPS_CE, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] )) {
			    if ( array_key_exists($processingCompIds, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_CE]) ) {
				    $this->ceObjs = $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_CE ][ $processingCompIds ];
				    return $this->ceObjs;
			    }
		    }else if (array_key_exists(E4S_PROCESSING_COMPS_IDS, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] )) {
			    $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_CE] = [];
			    $processingCompIds                                               = $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_IDS];
			    $processingCompIds                                               = implode(",", $processingCompIds);
			    $force                                                           = true;
		    }
	    }
        if (empty($this->ceObjs) or $force) {
            $sql = 'select ce.*, ev.gender
                from ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTS . ' ev 
                where ce.compid in (' . $processingCompIds . ')
                and   ce.eventid = ev.id';
            $result = e4s_queryNoLog($sql);
            $ceObjs = [];
            while ($obj = $result->fetch_object(E4S_COMPEVENT_OBJ)) {
	            if ( !array_key_exists($obj->compId, $ceObjs)){
		            $ceObjs[$obj->compId] = [];
	            }
                $ceObjs[$obj->compId][$obj->id] = $obj;
            }
	        if ( array_key_exists($this->getID(), $ceObjs)) {
		        $this->ceObjs = $ceObjs[$this->getID()];
		        if ( array_key_exists( E4S_PROCESSING_HOMEPAGE_COMPS, $GLOBALS ) ) {
			        if ( !array_key_exists( E4S_PROCESSING_COMPS_CE, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] ) ) {
				        $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_CE ] = [];
			        }
			        foreach ($ceObjs as $compId => $ceObj) {
				        $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_CE ][ $compId ] = $ceObj;
			        }
		        }
	        }
        }
    }

    /*
     select eg.*, max(evg.eventid), evd.Name name
    from Entry4_uk_EventGroups eg,
         Entry4_uk_CompEvents ce,
         Entry4_EventGender evg,
         Entry4_EventDefs evd
    where eg.compid = 367
    and ce.maxGroup = eg.id
    and ce.eventid = evg.id
    and evd.id = evg.eventid
    group by evg.eventid
     */
    private function _setEventGroups($force = FALSE) {
        // dont use the eg Class as that gets the comp and then into a loop !
	    $processingCompIds = $this->getID();
	    if ( array_key_exists(E4S_PROCESSING_HOMEPAGE_COMPS, $GLOBALS)) {
		    if ( array_key_exists(E4S_PROCESSING_COMPS_EG, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] )) {
			    if ( array_key_exists($processingCompIds, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_EG]) ) {
				    $this->eventGroups = $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_EG ][ $processingCompIds ];
				    return $this->eventGroups;
			    }
		    }else if (array_key_exists(E4S_PROCESSING_COMPS_IDS, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] )) {
			    $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_EG] = [];
			    $processingCompIds                                               = $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_IDS];
			    $processingCompIds                                               = implode(",", $processingCompIds);
				$force                                                           = true;
		    }
	    }
        if (empty($this->eventGroups) or $force) {
            $sql = '
                select eg.*, max(evg.eventid) eventDefId, evd.name eventDefName, evg.options eventOptions
                from ' . E4S_TABLE_EVENTGROUPS . ' eg,
                     ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTGENDER . ' evg,
                     ' . E4S_TABLE_EVENTDEFS . ' evd
                where eg.compid in (' . $processingCompIds . ')
                and ce.maxGroup = eg.id
                and ce.eventid = evg.id
                and evd.id = evg.eventid
                group by eg.id, evg.eventid';
            $result = e4s_queryNoLog($sql);
            $egObjs = array();

            while ($obj = $result->fetch_object(E4S_EVENTGROUP_OBJ)) {
				if ( !array_key_exists($obj->compId, $egObjs)){
					$egObjs[$obj->compId] = [];
				}
                if (isset($obj->typeNo) and $obj->typeNo !== '') {
                    $obj->type = $obj->typeNo[0];
                }

	            $egObjs[$obj->compId][$obj->id] = $obj;
            }
			if ( array_key_exists($this->getID(), $egObjs)) {
				$this->eventGroups = $egObjs[ $this->getID() ];
				if ( array_key_exists( E4S_PROCESSING_HOMEPAGE_COMPS, $GLOBALS ) ) {
					if ( !array_key_exists( E4S_PROCESSING_COMPS_EG, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] ) ) {
						$GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_EG ] = [];
					}
					foreach ($egObjs as $compId => $egObj) {
						$GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_EG ][ $compId ] = $egObj;
					}
				}
			}
        }
        return $this->eventGroups;
    }
	public function getPriceObj() {
		return $this->priceObj;
	}
	public function getPricing(){
		$options = $this->getOptions();
		if ( isset($options->pricing) ) {
			return $options->pricing;
		}
		$options->pricing = priceClass::getDefaultPricing();
		$this->setOptions($options);
		return $options->pricing;
	}
    private function setPrice() {
        $this->priceObj = new priceClass($this->compId, $this->isNational, $this->getPricing());
    }

    public static function withRow(array $row) {
        $instance = new self();
        $instance->row = $row;

        if (array_key_exists('id', $row)) {
            $instance->compId = $row['id'];
        } else {
            if (array_key_exists('ID', $row)) {
                $instance->compId = $row['ID'];
            } else {
                $instance->compId = $row['compId'];
            }
        }
        $instance->compId = (int)$instance->compId;
        if (!array_key_exists('ctcid', $row)) {
//            var_dump($row);
            $areaId = 0;
            if (array_key_exists('area', $row)) {
                $areaId = (int)$row['area']['id'];
            }
            if (array_key_exists('areaid', $row)) {
                $areaId = (int)$row['areaid'];
            }
//            var_dump($areaId);
            if ($areaId !== 0) {
                $ctcSql = 'select  ctc.id ctcid,
                                ctc.maxathletes,
                                ctc.maxteams,
                                ctc.maxmale,
                                ctc.maxfemale,
                                ctc.maxagegroup,
                                ctc.uniqueevents,
                                ctc.singleagegroup 
                        from ' . E4S_TABLE_COMPTEAMCONFIG . ' ctc 
                        where   ctc.areaid = ' . $areaId;
                $ctcResult = e4s_queryNoLog($ctcSql);
                $ctcRow = $ctcResult->fetch_assoc();
                if (is_null($ctcRow) or $ctcResult->num_rows !== 0) {
                    $ctcRow = [];
                    $ctcRow['ctcid'] = 0;
                    $ctcRow['maxathletes'] = 0;
                    $ctcRow['maxteams'] = 0;
                    $ctcRow['maxmale'] = 0;
                    $ctcRow['maxfemale'] = 0;
                    $ctcRow['maxagegroup'] = 0;
                    $ctcRow['uniqueevents'] = 0;
                    $ctcRow['singleagegroup'] = 0;
                }
                $instance->row = array_merge($row, $ctcRow);
            }
        }
        $instance->updateFromRow();
        $instance->setPrice();
        return $instance;
    }

    public function __destruct() {

    }

    public function delete() {
        $sql = 'update ' . E4S_TABLE_COMPETITON . '
                set id = -id
                where id = ' . $this->getID();
        e4s_queryNoLog($sql);
    }

    public function getID() {
        return (int)$this->compId;
    }

    public function showTeamAthletes() {
        $options = $this->getOptions();
        return $options->showTeamAthletes;
    }

    public function socketsEnabled() {
        $options = $this->getOptions();
        if ($this->haveEntriesClosed() or isE4SUser()) {
            return $options->cardInfo->enabled;
        }
        return FALSE;
    }

	public function isTicketComp(){
		$row = $this->getRow();
		return $row['type'] === E4S_COMP_TYPE_TICKET;
	}
    public function hasSecondary() {
        $sql = 'select *
                from ' . E4S_TABLE_SECONDARYREFS . '
                where compid = ' . $this->getID();
        $result = e4s_queryNoLog($sql);
        return $result->num_rows > 0;
    }

    public function hasIndivEvents() {
        $eventGroups = $this->getEGObjs();
        foreach ($eventGroups as $eventGroup) {
            $egOptions = $eventGroup->options;
            if (!$egOptions->isTeamEvent) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function hasTeamEvents() {
        $eventGroups = $this->getEGObjs();
        foreach ($eventGroups as $eventGroup) {
            $egOptions = $eventGroup->options;
            if ($egOptions->isTeamEvent) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getOptions() {
        // not sure we need to add defaults, should have been set ??
        return e4s_addDefaultCompOptions($this->options);
    }

    public function setOptions($options) {
        $this->options = e4s_addDefaultCompOptions($options);
        return $this->options;
    }

    public function allowOrgFreeEntry(): bool {
        return userHasPermission(PERM_ADMIN, null, $this->getID()) and $this->doesOrgGetFreeEntry();
    }

    public function doesOrgGetFreeEntry() {
        $options = $this->getOptions();
        return $options->orgFreeEntry;
    }

    public function markFreeEntryPaidCheck() {
        $options = $this->getOptions();
        return $options->autoPayFreeEntry;
    }

    public function bacsOnly() {
        $options = $this->getOptions();
        if ($options->bacs->enabled) {
            $msg = $options->bacs->msg;
            $msg = str_replace('{USER_ID}', e4s_getUserName(), $msg);
            return $msg;
        }
        return '';
    }

    public function useTeamBibs() {
        $options = $this->getOptions();
        return $options->useTeamBibs;
    }

    public function getScoreboardImage() {
        $image = R4S_BLANK_PHOTO;
        $orgId = $this->getOrganisationId();
        $sql = 'select *
                from ' . E4S_TABLE_ADVERT . '
                where orgid = ' . $orgId . '
                order by modified asc';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            $image = R4S_BLANK_PHOTO;
        } else {
            $row = $result->fetch_object(E4S_ADVERT_OBJ);
            $image = $row->filePath;
            $updateSql = 'update ' . E4S_TABLE_ADVERT . '
                          set modified = CURRENT_TIMESTAMP
                          where id = ' . $row->id;
            e4s_queryNoLog($updateSql);
        }
        return $image;
    }

    public function isShortCodeOK($options = null): string {
        $stdReturn = 'Short/Friendly URL : ';
        if (is_null($options)) {
            $options = $this->getOptions();
        }
        if ($options->shortCode === '') {
            return '';
        }
        $shortCode = $options->shortCode;
        $sql = 'select   id
                        ,date
                        ,entriesClose
                        ,compclubid orgId
                from ' . E4S_TABLE_COMPETITON . "
                where options like '%shortCode\":\"" . $shortCode . "\"%'";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return '';
        }
        while ($obj = $result->fetch_object()) {
//            $entriesClosed = $this->haveEntriesClosed($obj->entriesClose);
            $compDate = $obj->date;
            $compDatePlus3Months = date('Y-m-d', strtotime($compDate . ' + 3 months'));
            $compDatePlus12Months = date('Y-m-d', strtotime($compDate . ' + 1 year'));

            // if over 12 months thats ok
            $compOver12Months = hasDatePassed($compDatePlus12Months);
            if (!$compOver12Months) {
                $compOver3Months = hasDatePassed($compDatePlus3Months);
            } else {
                $compOver3Months = TRUE;
            }

            if ((int)$obj->orgId !== $this->getOrganisationId()) {
                if (!$compOver12Months) {
                    if ($compOver3Months) {
                        return $stdReturn . 'Used within the last year by another Organisation';
                    } else {
                        return $stdReturn . 'In use by another Organisation';
                    }
                }
            }

            if (!$compOver3Months and (int)$obj->id !== $this->compId) {
                return $stdReturn . 'You have used this within the past 3 months';
            }
        }
        return '';
    }

    public function anonymizeAthletes() {
		if ( !$this->isOrganiser() ) {
			if ( $this->areEntriesOpen() ) {
				$options = $this->getOptions();

				return $options->anonymize;
			}
		}
        return FALSE;
    }

    public function getOrganisationId() {
        return (int)$this->row['compclubid'];
    }

    public function getSecurityKey() {
        $row = $this->getRow();
        if (is_null($row)) {
            Entry4UIError(9036, 'Invalid Competition requested', 400);
        }
        $compId = $row['ID'];
        $locId = $row['locationid'];
        $clubId = $row['compclubid'];
        return md5($compId . '-' . $locId . '~' . $clubId);
    }

    public function allowEventNoGeneration(): bool {
        $options = $this->getOptions();
        return $options->seqEventNo;
    }

    public function resetClonedInformation() {
        $options = e4s_getOptionsAsObj($this->row['options']);
        unset($options->categoryId);
        unset($this->options->categoryId);
        unset($this->options->shortCode);
        $this->setEventAdjustment(0);
        $this->row['options'] = e4s_getOptionsAsString($options);
        $this->row['resultsAvailable'] = false;
    }

    public function areResultsAvailable(){
        return (int)$this->row['resultsAvailable'] === 1;
    }
    public function setResultsAvailable($state = true){
        $this->row['resultsAvailable'] = $state;
        $sql = 'update ' . E4S_TABLE_COMPETITON . '
                set resultsAvailable = ' . $state . '
                where id = ' . $this->getID();
        e4s_queryNoLog($sql);
    }
    public function setEventAdjustment($adjust) {
        $options = $this->getOptions();
        $options->adjustEventNo = $adjust;
    }

    public function setNoEventNoGeneration($status = TRUE) {
        $this->options->seqEventNo = $status;
        $this->updateRowOptions();
    }

    public function isDayAfterClose(): bool {
        $daysToClose = (int)$this->getRow()['daysToClose'];
        if ($daysToClose < 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function haveEntriesClosed($closeStr = ''): bool {
        if ($closeStr === '') {
            $closeStr = $this->getEntriesCloseDate();
        }
        return hasDatePassed($closeStr);
    }

    public function getHeatOrder($options = null) {
        if (is_null($options)) {
            $options = $this->getOptions();
        }

        return $options->heatOrder;
    }

    public function informNewActiveComp() {
        $options = e4s_addDefaultConfigOptions($this->config['options']);
        if (strtoupper($this->config['env']) === 'PROD' and $options->informActive !== '') {
            $body = 'Instagram user,<br><br>';
            $body .= 'The following competition has just gone live : ' . $this->getDisplayName() . '<br><br>';
            $body .= Entry4_emailFooter();
            e4s_mail($options->informActive, 'New Competition on ' . E4S_CURRENT_DOMAIN, $body, Entry4_mailHeader('', FALSE));
        }
    }

    public function getName() {
        if (is_null($this->row)) {
            return 'Not Found';
        }
        return $this->row['Name'];
    }

	public function updatePof10ForEntries($compId = 0, $faf = true) {
		if ($compId === 0) {
			$compId = $this->getID();
		}
		if ( $faf ) {
			$protocol = $_SERVER['PROTOCOL'] = isset( $_SERVER['HTTPS'] ) && ! empty( $_SERVER['HTTPS'] ) ? 'https' : 'http';
			$url      = $protocol . '://' . E4S_CURRENT_DOMAIN . E4S_BASE_URL . 'public/checkCompPof10/' . $compId;
			e4s_fireAndForget( $url );
		}else{
			e4s_checkCompEntriesPof10($compId,0);
		}
	}
    public function informEntriesClosing() {
        $sql = '
            select c.*
            from ' . E4S_TABLE_COMPETITON . ' c
            where date_sub(curdate(), interval 3 DAY ) = date(entriesclose)
            and id > 0
            and active = 1
        ';
        $results = e4s_queryNoLog($sql);
        if ($results->num_rows === 0) {
	        return;
        }

        $options = e4s_addDefaultConfigOptions($this->config['options']);

        $body = 'Instagram user,<br><br>';
        $body .= 'The following competition(s) have just 3 days left before entries close: <br><br>';
        while ($obj = $results->fetch_object()) {
            $entriesClose = date_create($obj->EntriesClose);
            $entriesClose = date_format($entriesClose, 'd/m/Y H:i');
            $body .= $this->getDisplayName($obj->ID, $obj->Name) . ' Entries Close ' . $entriesClose . '<br>';
			$this->updatePof10ForEntries($obj->ID);
        }
	    if ($options->informActive === '') {
		    echo "No active user informing\n";
		    return;
	    }
        $body .= '<br>' . Entry4_emailFooter();
        e4s_mail($options->informActive, 'Entries closing on ' . E4S_CURRENT_DOMAIN, $body, Entry4_mailHeader('', FALSE));
        return;
    }

    public function informCompetitionHappening() {
        $sql = '
            select c.*
            from ' . E4S_TABLE_COMPETITON . ' c
            where date_sub(curdate(), interval -1 DAY ) = date
            and id > 0
        ';
        $results = e4s_queryNoLog($sql);
        if ($results->num_rows === 0) {
            return;
        }
        $options = e4s_addDefaultConfigOptions($this->config['options']);
        if ($options->informHappening === '') {
            echo "No Comp Happening user\n";
            return ;
        }
        $body = 'The following competition(s) are taking place tomorrow: <br><br>';
        while ($obj = $results->fetch_object()) {
            $date = date_create($obj->date);
            $date = date_format($date, 'd/m/Y H:i');
            $body .= $this->getDisplayName($obj->ID, $obj->Name) . ' taking place ' . $date . '<br>';
        }
        $body .= '<br>' . Entry4_emailFooter();
        e4s_mail($options->informHappening, 'Competitions Happening on ' . E4S_CURRENT_DOMAIN . ' tomorrow', $body, Entry4_mailHeader('', FALSE));
        return;
    }

    public function getEmailFooter() {
        $info = notesClass::get($this->compId, E4S_COMP_EMAILTXT);
        return e4s_htmlToUI($info);
    }

    public function ticketsEnabled() {
        $options = $this->getOptions();
        if (isset($options->ui)) {
            if (isset($options->ui->entryDefaultPanel) and $options->ui->entryDefaultPanel === E4S_UI_SHOP_ONLY) {
                return TRUE;
            }
        }
        if (isset($options->tickets)) {
            if (isset($options->tickets->enabled)) {
                return $options->tickets->enabled;
            }
        }
        return FALSE;
    }

	function getTicketCompId(){
		$id = $this->_checkTickets();
		if ( $id === 0 ){
			return $this->getID();
		}
		return $id;
	}
    private function _checkTickets(): int {
        $options = $this->getOptions();
        $ticketCompId = (int)$options->ui->ticketComp;

        if ($ticketCompId > 0) {
            $ticketCompObj = e4s_getCompObj($ticketCompId);
            if (!$ticketCompObj->areEntriesOpen()) {
                $ticketCompId = 0;
                $this->options->ui->ticketComp = $ticketCompId;
            }
        }
//	    e4s_addDebugForce("ticket id for " . $this->getID() . " : " . $ticketCompId);
        return $ticketCompId;
    }

    public function getTicketComp() {
        return $this->_checkTickets();
    }

    public function getStadium() {
        $options = $this->getOptions();
        if (!isset($options->stadium) or $options->stadium === '') {
            return '';
        }
        return $options->stadium;
    }
    public function getEntryEmails() {
        if (!userHasPermission(PERM_ADMIN, null, $this->compId)) {
            Entry4UIError(9605, 'You are not authorised to see this information');
        }
        $sql = '
            select user_email,
                   email
            from ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_ATHLETE . ' a,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_USERS . ' u
            where ce.compid = ' . $this->compId . '
            and   e.compeventid = ce.id
            and   e.paid = ' . E4S_ENTRY_PAID . '
            and   e.athleteid = a.id
            and   e.userid = u.id
        ';

        $emails = array();
        $result = e4s_queryNoLog($sql);
        while ($obj = $result->fetch_object()) {
            // key on email to ensure no duplicates
            $emails[$obj->user_email] = $obj->user_email;
            if (!is_null($obj->email) and $obj->email !== '') {
                // athlete defined with an email, so include
                $emails[$obj->email] = $obj->email;
            }
        }
        $ret = array();
        foreach ($emails as $email) {
            $ret[] = $email;
        }
        return $ret;
    }

	public function okToSendEmails():bool{
		if ( !$this->isActive() ){
			return false;
		}
//		if ( !$this->isBeforeCompDate() ){
//			return false;
//		}
		return true;
	}
	public function getEntryUserInfo() {
		if ( !e4s_isCronUser() ) {
            if (!userHasPermission(PERM_ADMIN, null, $this->compId)) {
				Entry4UIError( 9606, 'You are not authorised to see this information' );
			}
        }
        $sql = '
            select user_email,
                   email athleteEmail,
                   u.id userId,
                   a.id athleteId
            from ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_ATHLETE . ' a,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_USERS . ' u
            where ce.compid = ' . $this->compId . '
            and   e.compeventid = ce.id
            and   e.paid = ' . E4S_ENTRY_PAID . '
            and   e.athleteid = a.id
            and   e.userid = u.id
        ';
        $userObjs = array();
        $result = e4s_queryNoLog($sql);
        while ($obj = $result->fetch_object()) {
            // key on email to ensure no duplicates
            $userObjs[$obj->user_email] = $obj;
        }
        $ret = array();
        foreach ($userObjs as $userObj) {
            $userObj->userId = (int)$userObj->userId;
            $userObj->athleteId = (int)$userObj->athleteId;
            $ret[] = $userObj;
        }
        return $ret;
    }

    public function getSchedule($incEntries = FALSE) {
        $countObj = e4s_getEntryCountsByEG($this->compId);
        $this->getEGTypes();

        if ($incEntries) {
            $this->getAgeGroups();
        }
        $sql = "
            select eg.id id,
                   !isnull(eh.id) results,
                   eg.compId,
                   eg.eventNo,
                   eg.name name,
                   eg.typeno typeNo,
                   DATE_FORMAT(eg.startdate,'%Y-%m-%dT%H:%i:%s') as startDate,
                   eg.options egOptions
            from " . E4S_TABLE_EVENTGROUPS . ' eg left join ' . E4S_TABLE_EVENTRESULTSHEADER . ' eh on eg.id = eh.egid
            where eg.compid = ' . $this->compId;

        $result = e4s_queryNoLog($sql);
        $retArr = array();
        $egObjs = array();
        // preload so can populate egname in seeding if required.
        while ($egObj = $result->fetch_object()) {
            // currently the id is actually the number
            $egObj->egOptions = e4s_addDefaultEventGroupOptions($egObj->egOptions);
            if ((int)$egObj->results === 0) {
                $egObj->results = FALSE;
            } else {
                $egObj->results = TRUE;
            }
            $egObj->id = (int)$egObj->id;
            $egObjs [$egObj->id] = $egObj;
        }
        foreach ($egObjs as $egId => $obj) {
            if (!array_key_exists($obj->id, $this->egTypes)) {
//                Entry4UIError(8102, "Invalid type " . $obj->id);
                // eg group exists thats not used
                continue;
            }
            $obj->eventName = $obj->name;
            $obj->name = $obj->typeNo . ' : ' . $obj->name;
            $obj->wind = '';
            $eOptions = e4s_getOptionsAsObj($this->egTypes[$obj->id]->eOptions);
            if (isset($eOptions->wind)) {
                $obj->wind = $eOptions->wind;
            }
            $obj->type = $this->egTypes[$obj->id]->uomType;

            $egOptions = e4s_addDefaultEventGroupOptions($obj->egOptions);
            $qualifyObj = new e4sQualifyClass($egOptions);
            $qualifyId = $qualifyObj->getId();
            if ($qualifyId !== 0 and $obj->results) {
                // Does the qualify to event have entries ?
                $qSql = 'select e.id
                         from ' . E4S_TABLE_ENTRIES . ' e,
                              ' . E4S_TABLE_COMPEVENTS . ' ce
                         where e.compeventid = ce.id
                         and ce.maxgroup = ' . $qualifyId;

                $qResult = e4s_queryNoLog($qSql);
                if ($qResult->num_rows === 0) {
                    $qualifyObj->setWaiting(TRUE);
                }
            }
//            e4s_addDebug($options);
            unset($obj->compId);

            $counts = new stdClass();
            $counts->entries = 0;
            if (array_key_exists((int)$obj->id, $countObj)) {
                $counts->entries = $countObj[(int)$obj->id]->paidCount;
            }

            $counts->maxAthletes = eventGroup::getMaxAthletes($egOptions);
            $counts->maxInHeat = eventGroup::getMaxInHeat($egOptions);

            if ($qualifyId > 0) {
                if (!array_key_exists($qualifyId, $egObjs)) {
                    Entry4UIError(3014, 'Unable to get Event (' . $qualifyId . ')');
                }
                $qualifyObj->setName($egObjs[$qualifyId]->name);
            }
            $obj->seed = $qualifyObj->getSeed();
            $obj->counts = $counts;
            $obj->entries = array();
            if ($incEntries) {
                if ($egOptions->isTeamEvent) {
                    $obj->teamEntries = $this->getEntries($obj);
                    $obj->counts = new stdClass();
                    $obj->counts->entries = sizeof($obj->teamEntries);
                    $obj->counts->maxAthletes = 0;
                    $obj->counts->maxInHeat = 0;
                } else {
                    $obj->entries = $this->getEntries($obj);
                }
            }
            $obj->isTeamEvent = $obj->egOptions->isTeamEvent;
            $obj->resultsPossible = eventGroup::getResultsPossible($egOptions);
            unset($obj->egOptions);
            $retArr[] = $obj;
        }

        return $retArr;
    }

    public function getEGTypes() {
        $sql = 'select uom.uomtype uomType,
                       ce.maxGroup egId,
                       e.options eOptions
                from ' . E4S_TABLE_UOM . ' uom,
                     ' . E4S_TABLE_EVENTS . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce
                where uom.id = e.uomid
                and ce.EventID = e.ID
                and ce.CompID = ' . $this->compId . '
                group by uomtype,maxgroup';
        $results = e4s_queryNoLog($sql);
        $objs = array();
        while ($obj = $results->fetch_object()) {
            $objs[$obj->egId] = $obj;
        }
        $this->egTypes = $objs;
    }

    public function getAgeGroups() {
        $this->ageGroups = getAllCompDOBs($this->compId);
        return $this->ageGroups;
    }

	public function hasIncludedEntries(){
		$this->getEGObjs();

		$entryGroupObj = new eventGroup($this->getID());
		foreach($this->eventGroups as $egId=>$egObj){
			$includedEgId = $entryGroupObj->getIncludedEntries($egObj);
			if ( $includedEgId > 0 ){
				return true;
			}
		}
		return false;
	}
	public function addIncludedEntries($egId, $srcEgId){
		$ceObjs = $this->getCeObjs();
		$srcEntries = $this->_getSrcEntries($srcEgId);
		$includeObjs = $this->_getCeIncludedInfo($ceObjs, $egId);
		if ( sizeof($includeObjs) === 0 ){
			Entry4UIError( 9037, 'No entries to include : ' . $egId);
		}

		$this->_removeExistIncludedEntries($egId);

		foreach($srcEntries as $srcEntry){
			if ( $srcEntry->egId === $srcEgId ){
				foreach($includeObjs as $includeObj){
					if ( $includeObj->gender === $srcEntry->gender and $includeObj->ageGroupId === $srcEntry->ageGroupId ){
						$entryObj = new entryClass();
						$entryObj->copyEntry($srcEntry,$includeObj->ceId);
						break;
					}
				}
			}
		}
	}
	private function _getSrcEntries($srcEgId){
		if ( array_key_exists($srcEgId, $this->entries) ){
			return $this->entries[ $srcEgId ];
		}
		$sql = 'select *
				from ' . E4S_TABLE_ENTRYINFO . '
				where egId = ' . $srcEgId . '
				and paid = ' . E4S_ENTRY_PAID;
		$result = e4s_queryNoLog($sql);
		$entries = [];
		while ($entryRow = $result->fetch_object(E4S_ENTRYINFO_OBJ)) {
			$entries[] = $entryRow;
		}
		$this->entries[ $srcEgId ] = $entries;
		return $entries;
	}
	private function _removeExistIncludedEntries($egId){
		$sql = 'delete from ' . E4S_TABLE_ENTRIES . '
				where compEventID in (select ce.ID
				                        from ' . E4S_TABLE_COMPEVENTS . ' ce
				                        where ce.CompID = ' . $this->getID() . '
				                        and ce.maxGroup = ' . $egId . ')
				and paid = ' . E4S_ENTRY_QUALIFY;
		e4s_queryNoLog($sql);
	}
	private function _getCeIncludedInfo($ceObjs, $egId):array{
		$includeObjs = [];
		foreach($ceObjs as $ceObj){
			if ($ceObj->egId === $egId){
				$obj = new stdClass();
				$obj->ceId = $ceObj->id;
				$obj->gender = $ceObj->gender;
				$obj->ageGroupId = $ceObj->ageGroupId;
				$includeObjs[] = $obj;
			}
		}
		return $includeObjs;
	}

	public function getEntriesV2($requestedEgId = 0, $paid = E4S_ENTRY_PAID_NULL): array {
		$entries = $this->_getEntriesV2();

		if ($paid === E4S_ENTRY_PAID_NULL) {
			if ( $requestedEgId === 0 ) {
				// asked for everything
				return $entries;
			}else{
				// no processing required, return event group entries
				if ( array_key_exists($requestedEgId, $entries) ){
					return $entries[ $requestedEgId ];
				}
				return [];
			}
		}

		$returnEntries = [];
		foreach($entries as $egId=>$entryArr){
			if ( $egId === $requestedEgId or $requestedEgId === 0 ) {
				if ( ! array_key_exists( $egId, $returnEntries ) ) {
					$returnEntries[ $egId ] = [];
				}
				foreach ($entryArr as $entry) {
					if ($entry->paid === $paid) {
						$returnEntries[ $egId ][] = $entry;
					}
				}
			}
		}

		if ( $requestedEgId === 0){
			return $returnEntries;
		}else{
			return $returnEntries[ $requestedEgId ];
		}

	}
    private function _getEntriesV2(): array {
		if (! empty($this->entries) ){
			return $this->entries;
		}
        $sql = 'select  *
            from ' . E4S_TABLE_ENTRYINFO . '
            where compid = ' . $this->getID() .'
            and ceoptions not like "%parentCeid%"
            order by startdate , egid, periodstart, gender, agegroup';

        $result = e4s_queryNoLog($sql);
        $entries = array();
        while ($entryRow = $result->fetch_object(E4S_ENTRYINFO_OBJ)) {
            if (!array_key_exists($entryRow->egId, $entries)) {
                $entries[$entryRow->egId] = array();
            }
            $entries[$entryRow->egId][] = $entryRow;
        }

        $this->entries = $entries;

        return $entries;
    }

    public function getEntries($obj, $entityLevel = 0, $entityId = 0): array {
        $egId = $obj->id;
        return $this->getEntriesForEgId($egId, $entityLevel, $entityId);
    }

    public function getEntriesForEgId($egId, $entityLevel = 0, $entityId = 0, $incUnpaid = false): array {
        $eventGroups = $this->getEGObjs();
        $entryArr = array();
        if ($egId > 0) {
            $egObj = $eventGroups[$egId];
            if ($egObj->options->isTeamEvent) {
                $entries = $this->_getEntriesTeam($egId, $incUnpaid);
            } else {
                $entries = $this->_getEntriesNonTeam($egId, $entityLevel, $entityId,$incUnpaid);
            }
            foreach ($entries as $entry) {
                $entryArr[] = $entry;
            }
            return $entryArr;
        } else {
            $entries = $this->_getEntriesNonTeam($egId, $entityLevel, $entityId,$incUnpaid);
            $teamEntries = $this->_getEntriesTeam($egId,$incUnpaid);
            foreach ($teamEntries as $egId => $teamEntry) {
                $entries[$egId] = $teamEntry;
            }
            return $entries;
        }
    }

    private function _getEntriesTeam($egId, $incUnpaid = false) {
        $entries = array();
        $clubs = array();
        $areas = array();
        $entrySql = '
                    select 
                           e.id entryId,
                           e.options entryOptions,
                           e.name teamName,
                           e.userId,
                           e.entityLevel,
                           e.entityId,
						   e.paid,
                           eg.id egId,
                           ta.athleteId,
                           ta.pos,
                           u.display_name userName,
                           u.user_email userEmail,
                           u.user_login userLogin,
                           ce.ageGroupId,
                           a.name ageGroupName,
                           ev.gender gender,
                           ath.urn
                    from   ' . E4S_TABLE_EVENTGROUPS . ' eg,
                           ' . E4S_TABLE_COMPEVENTS . ' ce,
                           ' . E4S_TABLE_EVENTS . ' ev,
                           ' . E4S_TABLE_AGEGROUPS . ' a,
                           ' . E4S_TABLE_EVENTTEAMENTRIES . ' e left join ' . E4S_TABLE_EVENTTEAMATHLETE . ' ta on ( e.id = ta.teamentryid) left join ' . E4S_TABLE_ATHLETE . ' ath on (ta.athleteid = ath.id),
                           ' . E4S_TABLE_USERS . ' u
                    where  ce.maxgroup = eg.id
                    and    e.ceid = ce.id
                    and    e.userid = u.id
                    and    ev.id = ce.eventid
                    and    a.id = ce.agegroupid';
		if ( !$incUnpaid ){
			$entrySql .= ' and e.paid in ( ' . E4S_ENTRY_PAID . ',' . E4S_ENTRY_QUALIFY . ')';
        }
        if ($egId > 0) {
            $entrySql .= ' and eg.id = ' . $egId;
        } else {
            $entrySql .= ' and ce.compid = ' . $this->getID();
        }
        $entrySql .= ' order by eg.id, e.id, ta.pos';

        $result = e4s_queryNoLog($entrySql);
        if ($result->num_rows > 0) {
            $clubSql = 'select id,
                               clubname name
                        from ' . E4S_TABLE_CLUBS . '
                        where active = true';
            $clubResult = e4s_queryNoLog($clubSql);
            while ($clubObj = $clubResult->fetch_object()) {
                $clubObj->id = (int)$clubObj->id;
                $clubs[$clubObj->id] = $clubObj;
            }

            $areaSql = 'select id,
                               name
                        from ' . E4S_TABLE_AREA;
            $areaResult = e4s_queryNoLog($areaSql);
            while ($areaObj = $areaResult->fetch_object()) {
                $areaObj->id = (int)$areaObj->id;
                $areas[$areaObj->id] = $areaObj;
            }

            $entitySql = 'select id, 
                                 level,
                                 name
                          from ' . E4S_TABLE_ENTITY;
            $entityResult = e4s_queryNoLog($entitySql);
            $entities = array();
            while ($entObj = $entityResult->fetch_object()) {
                $entLevel = (int)$entObj->level;
                $entities[$entLevel] = $entObj;
            }
        }
        $autoEntryObj = new autoEntriesClass(TRUE);
        $targetIds = array();
        $targetEgIdArr = array();
        $eventGroups = array();
        while ($teamObj = $result->fetch_object()) {
            $teamObj->entryOptions = e4s_addDefaultEntryOptions($teamObj->entryOptions);
            $teamObj->entityLevel = (int)$teamObj->entityLevel;
            $teamObj->entityName = 'Unknown';
            $teamObj->entryId = (int)$teamObj->entryId;
            $teamObj->entityId = (int)$teamObj->entityId;
            $teamObj->paid = (int)$teamObj->paid;
            $teamObj->teamEntity = 'Unknown';
            $teamObj->ageGroup = new stdClass();
            $teamObj->ageGroup->id = (int)$teamObj->ageGroupId;
            unset($teamObj->ageGroupId);

            $teamObj->ageGroup->name = $teamObj->ageGroupName;
            unset($teamObj->ageGroupName);
            if ($teamObj->entityId > 0) {
                if ($teamObj->entityLevel === 1) {
                    $obj = $clubs;
                } else {
                    $obj = $areas;
                }
                if (array_key_exists($teamObj->entityLevel, $entities)) {
                    $teamObj->entityName = $entities[$teamObj->entityLevel]->name;
                }
                if (array_key_exists($teamObj->entityId, $obj)) {
                    $teamObj->teamEntity = $obj[$teamObj->entityId]->name;
                }
            }
            $teamObj = $this->_removeTeamEntityFromObj($teamObj);
            $teamObj = $this->_removeUserInfoFromEntryObj($teamObj);
            $athlete = new stdClass();
            $athlete->id = (int)$teamObj->athleteId;
            $athlete->name = 'Do we want this ?';
            $athlete->urn = $teamObj->urn;
            $athlete->pos = (int)$teamObj->pos;
            unset($teamObj->athleteId);
            unset($teamObj->pos);
            if (array_key_exists($teamObj->entryId, $entries)) {
                // processing another athlete
                $athletes = $entries[$teamObj->entryId]->athletes;
            } else {
                $athletes = array();
            }
            $athletes[] = $athlete;
            $teamObj->athletes = $athletes;
            $targetEntryId = $autoEntryObj->getTargetEntryId($teamObj->entryOptions);
            if ($targetEntryId > 0) {
                $targetIds[] = $targetEntryId;
                $targetEgId = $autoEntryObj->getTargetEGId($teamObj->entryOptions);
                $targetEgIdArr[$targetEgId] = $targetEgId;
            }

            if ($egId === 0) {
                if (!array_key_exists($teamObj->egId, $eventGroups)) {
                    $eventGroups[$teamObj->egId] = array();
                }
                $entries = $eventGroups[$teamObj->egId];
            }
            $entries[$teamObj->entryId] = $teamObj;
            if ($egId === 0) {
                $eventGroups[$teamObj->egId] = $entries;
            }
        }
        if ($egId > 0) {
            $this->_setTargetExtraInfo(TRUE, $autoEntryObj, $entries, $targetIds, $targetEgIdArr);
            return $entries;
        }
        return $eventGroups;
    }

    private function _removeTeamEntityFromObj($teamObj) {
        $entityObj = new stdClass();
        $entityObj->id = $teamObj->entityId;
        $entityObj->teamEntity = $teamObj->teamEntity;
//        $entityObj->entityLevel = $teamObj->entityLevel;
        $entityObj->entityName = $teamObj->entityName;
        unset($teamObj->entityId);
        unset($teamObj->teamEntity);
        unset($teamObj->entityLevel);
        unset($teamObj->entityName);
        $teamObj->entity = $entityObj;
        return $teamObj;
    }

    private function _removeUserInfoFromEntryObj($entryObj) {
        $user = new stdClass();
        $user->id = (int)$entryObj->userId;
        $user->email = $entryObj->userEmail;
        $user->login = $entryObj->userLogin;
        $user->name = $entryObj->userName;
        $entryObj->user = $user;

        unset($entryObj->userEmail);
        unset($entryObj->userId);
        unset($entryObj->userLogin);
        unset($entryObj->userName);
        return $entryObj;
    }

    private function _setTargetExtraInfo($isTeam, $autoEntryObj, $entries, $targetIds, $targetEgIdArr) {
        if (sizeof($targetIds) > 0) {
            $targetEntries = entryClass::getEntries($targetIds, $isTeam);
//            $targetEgArr = eventGroup::getEgObjArr($targetEgIdArr);
            $targetCompId = 0;
            foreach ($targetEntries as $targetEntry) {
                if ($targetCompId === 0) {
                    $targetCompId = $targetEntry->compId;
                }
                if ($targetEntry->compId !== $targetCompId) {
                    Entry4UIError(8011, 'Currently Only 1 target competition is supported');
                }
            }
            $targetCompObj = e4s_getCompObj($targetCompId);
            $targetEgArr = $targetCompObj->getEventGroupsForIds($targetEgIdArr);

            foreach ($entries as $entryObj) {
                $entryTargetId = $autoEntryObj->getTargetEntryId($entryObj->entryOptions);
                if ($entryTargetId !== 0) {
                    if (!array_key_exists($entryTargetId, $targetEntries)) {
                        // Target Entry does not exist
                        $autoEntryObj->setTargetName($entryObj->entryOptions, 'Target Entry Cancelled (' . $entryTargetId . ')');
                    } else {
                        $targetEgId = $autoEntryObj->getTargetEGId($entryObj->entryOptions);
                        $targetEgObj = $targetEgArr[$targetEgId];
                        $targetEntry = $targetEntries [$entryTargetId];
                        $autoEntryObj->setTargetInfoInOptions($entryObj, $targetEntry, $targetEgObj);
                    }
                }
            }
        }
    }

    public function isAthleteOnWaitingListForId($athleteId, $egId) {
        return $this->getAthleteWaitingPos($athleteId, $egId) > 0;
    }

    public function isBeforeCompDate() {
        $compDate = date_create($this->getDate())->getTimestamp();
        $now = strtotime('now');

        return $now < $compDate;
    }

    public function getAthleteWaitingPos($athleteId, $egId) {
	    $athleteId = (int)$athleteId;
		$eventGroups = $this->getEGObjs();
        if (!array_key_exists($egId, $eventGroups)) {
            Entry4UIError(9480, 'Invalid Event Group : ' . $egId);

        }
        if (!$this->isWaitingListEnabled()) {
            return 0;
        }

        $waitingObj = new waitingClass($this->getID());
        $pos = $waitingObj->getAthleteWaitingPos($athleteId, $egId);
		return $pos;
    }

	public function getCEsForEgID($egId) {
		$ceObjs = $this->getCeObjs();
		$ceArr = array();
		foreach ($ceObjs as $ceObj) {
			if ($ceObj->egId === $egId) {
				$ceArr[] = $ceObj;
			}
		}
		return $ceArr;
	}
    public function getCEByCeID($ceId) {
        $ceObjs = $this->getCeObjs();
        if (!array_key_exists($ceId, $ceObjs)) {
            Entry4UIError(9365, 'Invalid CEvent : ' . $ceId);
        }
        return $ceObjs[$ceId];
    }

    private function _getEntriesNonTeam($egId, $entityLevel = 0, $entityId = 0,$incUnpaid = false) {
        $entrySql = '
                    select a.id athleteId,
                           a.firstname,
                           a.surname,
                           a.dob,
                           a.gender,
                           a.urn,
                           c.id clubId,
                           c.clubname,
                           e.id entryId,
                           e.options entryOptions,
                           e.userid userId,
                           e.pb perf,
                           e.paid,
                           eg.id egId,
                           eg.typeNo,
                           u.display_name userName,
                           u.user_email userEmail,
                           u.user_login userLogin
                    from   ' . E4S_TABLE_ATHLETE . ' a,
                           ' . E4S_TABLE_EVENTGROUPS . ' eg,
                           ' . E4S_TABLE_COMPEVENTS . ' ce,
                           ' . E4S_TABLE_ENTRIES . ' e,
                           ' . E4S_TABLE_CLUBS . ' c,
                           ' . E4S_TABLE_USERS . ' u
                    where  ce.maxgroup = eg.id
                    and    e.compeventid = ce.id
                    and    e.athleteid = a.id
                    and    c.id = e.clubid
                    and    e.userid = u.id';
        if ($egId > 0) {
            $entrySql .= ' and eg.id = ' . $egId;
            $entries = array();
        } else {
            $entrySql .= ' and ce.compid = ' . $this->getID();
        }

        if ($entityLevel > 0 and $entityId > 0) {
            $entrySql .= " and e.entity = '" . e4s_getEntityKey($entityLevel, $entityId) . "'";
        } else if ($incUnpaid === false ) {
            $entrySql .= ' and e.paid in ( ' . E4S_ENTRY_PAID . ',' . E4S_ENTRY_QUALIFY . ')';
        }

        $result = e4s_queryNoLog($entrySql);

        $autoEntryObj = new autoEntriesClass(FALSE);
        $targetIds = array();
        $targetEgIdArr = array();
        if (is_null($this->ageGroups)) {
            $this->getAgeGroups();
        }
        $eventGroups = array();
		$editAccess = $this->isOrganiser();
		$curUserId = e4s_getUserID();
        while ($obj = $result->fetch_object()) {
            $ageGroup = getAgeGroupInfo($this->ageGroups, $obj->dob);
            $useAG = new stdClass();
            $useAG->currentAge = $ageGroup['currentAge'];
            $useAG->competitionAge = $ageGroup['competitionAge'];
            $useAG->name = $ageGroup['ageGroup']['Name'];
            $useAG->shortName = $ageGroup['ageGroup']['shortName'];
            $obj->ageGroup = $useAG;
            unset($obj->dob);
			$club = new stdClass();
			$club->id = (int)$obj->clubId;
			$club->name = $obj->clubname;
			$obj->club = $club;
            $obj->egId = (int)$obj->egId;
			$ceObjs = $this->getCEsForEgID($obj->egId);
			$eventId = $ceObjs[0]->eventId;
			$eventObj = e4sEvents::getEventInfo($eventId);

            $obj->userId = (int)$obj->userId;
            $obj->athleteId = (int)$obj->athleteId;
            $obj->entryId = (int)$obj->entryId;
            $obj->paid = (int)$obj->paid;
			$perf = new stdClass();
			$perf->perf = (float)$obj->perf;
			$type = $obj->typeNo[0];
			if ( $type === E4S_EVENT_TRACK or $type === E4S_EVENT_ROAD or $type === E4S_EVENT_XCOUNTRY ){
				$perf->perfText = e4s_ensureString(resultsClass::getResultFromSeconds($obj->perf));
			}else{
				$perf->perfText = e4s_ensureString(resultsClass::ensureDecimals($obj->perf));
			}
			if (! is_null($eventObj) ) {
				$perf->perfText .= $eventObj->uomOptions[0]->short;
			}
			$obj->perf = $perf;
            $obj->entryOptions = e4s_addDefaultEntryOptions($obj->entryOptions);

	        $obj->editAccess = $editAccess;
			if ( !$editAccess ){
				$obj->editAccess = $obj->userId === $curUserId;
			}

            $obj = $this->_removeUserInfoFromEntryObj($obj);
			if ( !$editAccess ){
				unset($obj->user);
			}
            $targetEntryId = $autoEntryObj->getTargetEntryId($obj->entryOptions);
            if ($targetEntryId > 0) {
                $targetIds[] = $targetEntryId;
                $targetEgId = $autoEntryObj->getTargetEGId($obj->entryOptions);
                $targetEgIdArr[$targetEgId] = $targetEgId;
            }
            if ($egId === 0) {
                if (!array_key_exists($obj->egId, $eventGroups)) {
                    $eventGroups[$obj->egId] = array();
                }
                $entries = $eventGroups[$obj->egId];
            }
            $entries[$obj->entryId] = $obj;
            if ($egId === 0) {
                $eventGroups[$obj->egId] = $entries;
            }
        }
        if ($egId > 0) {
            $this->_setTargetExtraInfo(FALSE, $autoEntryObj, $entries, $targetIds, $targetEgIdArr);
            return $entries;
        }
        return $eventGroups;
    }
    public function checkPaymentCode($options) {
        if ($options->paymentCode === '') {
            if ($this->config) {
                $cfgOptions = $this->config['options'];
                if (isset($cfgOptions->paymentCodeMandatory)) {
                    $mandatoryOrgs = $cfgOptions->paymentCodeMandatory;
                    foreach ($mandatoryOrgs as $orgId) {
                        if ($orgId === $this->getOrganisationId()) {
                            Entry4UIError(2889, 'Please add a payment code before saving.');
                        }
                    }
                }
            }
        }
    }

    public function checkPriorityCode($priorityCode) {
        $options = $this->getOptions();
        if (!$this->_isPriorityCheckRequired($options)) {
            return TRUE;
        }
        $deleteSQL = 'delete from ' . E4S_TABLE_PRIORITY . '
                      where compid = ' . $this->compId . '
                      and userid = ' . e4s_getUserID();
        e4s_queryNoLog($deleteSQL);

        if ($this->_hasPriorityDatePassed($options)) {
            return TRUE;
        }

        if (strtolower($priorityCode) !== strtolower($options->priority->code)) {
            return FALSE;
        }
        $insertSQL = 'insert into ' . E4S_TABLE_PRIORITY . ' (compid, userid)
                      values ( ' . $this->compId . ',' . e4s_getUserID() . ')';
        e4s_queryNoLog($insertSQL);
        return TRUE;
    }

    private function _isPriorityCheckRequired($options) {
        if (!isset($options->priority)) {
            return FALSE;
        }
        if (!isset($options->priority->required)) {
            return FALSE;
        }
        if (!isset($options->priority->code)) {
            return FALSE;
        }

        if (!$options->priority->required or $options->priority->code === '') {
            return FALSE;
        }
        if (isE4SUser()) {
            return FALSE;
        }
        if ($this->isOrganiser()) {
            return FALSE;
        }
        return TRUE;
    }

    public function isOrganiser() {
        if (isE4SUser()) {
//          dont bother checking perms
            return TRUE;
        }
        return userHasPermission(PERM_ADMIN, null, $this->getID());
    }
	public function hasCardAccess() {
		if (isE4SUser()) {
//          dont bother checking perms
			return TRUE;
		}
		if ( $this->isOrganiser() ){
			return true;
		}

		return userHasPermission(PERM_SEEDING, null, $this->getID());
	}
    private function _hasPriorityDatePassed($options) {
        $date = $options->priority->dateTime; // ISO Date Time
        if ($date === '') {
            return FALSE;
        }
        $now = new DateTime();
        $nowTS = $now->getTimestamp();
        if ($nowTS > strtotime($date)) {
            // date passed
            return TRUE;
        }
        return FALSE;
    }

    public function validatePriority() {
        $options = $this->getOptions();

        if (!$this->_isPriorityCheckRequired($options)) {
            return TRUE;
        }

        if ($this->_hasPriorityDatePassed($options)) {
            return TRUE;
        }

        $readSQL = 'select id
                    from ' . E4S_TABLE_PRIORITY . '
                    where compid = ' . $this->compId . '
                      and userid = ' . e4s_getUserID();
        $result = e4s_queryNoLog($readSQL);
        return $result->num_rows === 1;
    }

    public function areEntriesOpen() {
        $now = time();
        $entriesOpen = strtotime($this->getEntriesOpenDate());
        if ($now < $entriesOpen) {
            return FALSE;
        }

        $entriesClose = strtotime($this->getEntriesCloseDate());
        if ($now > $entriesClose) {
            return FALSE;
        }
        return TRUE;
    }

    public function getFullRow($cacheLookups = false) {
        // Set the Competition logo
        $row = $this->getRow();
        if (is_null($row)) {
            Entry4UIError(8231, 'No competition found', 200);
        }
// crappy corrections for old format
        $row['compOrgId'] = $this->getOrganisationId();
        unset($row['compclubid']);
        $row['compName'] = $row['Name'];
        unset($row['Name']);
        $row['entityid'] = null;
        $row['compDate'] = $row['Date'];
        unset($row['Date']);
        $row['opendate'] = e4s_sql_to_iso($row['EntriesOpen']);
        unset($row['EntriesOpen']);
        $row['closedate'] = e4s_sql_to_iso($row['EntriesClose']);
        unset($row['EntriesClose']);

        $location = new stdClass();
        $location->id = (int)$row['locationid'];
        $location->name = $row['location'];
        $location->address1 = $row['locAddress1'];
        $location->address2 = $row['locAddress2'];
        $location->town = $row['loctown'];
        $location->postcode = $row['locpostcode'];
        $location->county = $row['loccounty'];
        $location->map = '';
        if ($location->postcode !== '') {
            $location->map = 'https://www.google.com/maps?q=' . $location->postcode;
        }
        $location->directions = $row['locdirections'];
        $location->website = $row['locwebsite'];
        $row['location'] = $location;

        unset($row['locationid']);
        unset($row['locAddress1']);
        unset($row['locAddress2']);
        unset($row['loctown']);
        unset($row['locpostcode']);
        unset($row['loccounty']);
        unset($row['locdirections']);
        unset($row['locwebsite']);

        $row['club'] = $row['compclub'];
        unset($row['compclub']);

        $options = $this->setContactInfoInCOptions();

        $options = $this->setOptions($options);
        $options->resultsAvailable = (int)$row['resultsAvailable'] === 1;
        $row['logo'] = $this->getLogo();

        // Get all dates for the competition
        $row['dates'] = $this->getDatesFromOptions();
        // remove the comp date
        unset($row['date']);

        // TODO set this when prices are written NOT HERE
        $row['saleenddate'] = $this->getSaleEndDate(false, FALSE);

        $counters = $this->getEntryCounts();

        $teamEntryCnt = 0;
        if (array_key_exists('relayCnt', $counters)) {
            $teamEntryCnt = $counters['relayCnt'];  // No of Team entries
        }
        $totalEntries = new stdClass();
        if (!array_key_exists('eventCnt', $counters)) {
            $counters['eventCnt'] = 0;
            $counters['teamCnt'] = 0;
            $counters['indivCnt'] = 0;
            $counters['totalCnt'] = 0;
            $counters['totalUniqueAthleteCnt'] = 0;
            $counters['waitingEntries'] = 0;
            $counters['uniqueCnt'] = 0;
            $counters['relayUniqueAthleteCnt'] = 0;
            $counters['relayAthleteCnt'] = 0;
        }
        $totalEntries->eventCount = $counters['eventCnt'];
        $totalEntries->teamEventCount = $counters['relayCnt'];
        $totalEntries->indivEventCount = $counters['indivCnt'];
        $totalEntries->isTeam = (int)$counters['teamCnt'] > 0;

        // Only send counts if there are events. The ui will only show if there are properties, so its possible to have no entries
        if ($totalEntries->indivEventCount > 0) {
            $totalEntries->indiv = $counters['totalCnt']; // Number of Entries
            $totalEntries->waitingCount = 0;
            if (array_key_exists('waitingEntries', $counters)) {
                $totalEntries->waitingCount = $counters['waitingEntries']; // Number of Entries waiting
            }
            $totalEntries->uniqueIndivAthletes = $counters['uniqueCnt']; // Number of Unique Athletes in Indiv Events
        }
        if ($totalEntries->teamEventCount > 0) {
            $totalEntries->team = $teamEntryCnt;
            $totalEntries->uniqueTeamAthletes = $counters['relayUniqueAthleteCnt']; // Number of Unique Athletes in Indiv Events
            $totalEntries->teamAthletes = $counters['relayAthleteCnt']; // Number of Unique Athletes in Indiv Events
        }

        $totalEntries->athletes = $counters['totalUniqueAthleteCnt']; //Total count of Unique athletes

        if ($this->hasAthleteClubSecurity() !== '') {
            $useAccess = 'Affiliated Club';
            $row['areaname'] = $useAccess . 's Only';
            $this->populateAthleteSecurity();
        }

        $options = $this->getOptions();

        if (isset($options->noEntryCount) and !isE4SUser()) {
            $totalEntries->athleteCnt = $totalEntries->athletes;
            $totalEntries->indivCnt = $totalEntries->indiv;
            $totalEntries->teamCnt = $teamEntryCnt;
            unset($totalEntries->athletes);
            unset($totalEntries->indiv);
            unset($totalEntries->team);
        }
//        $rowOptions = e4s_getOptionsAsObj($row['options']);
        if (isset($options->priority)) {
            if (!$this->isOrganiser() and !isE4SUser()) {
                unset($options->priority->code);
            }
        }
        $row['options'] = $options;
        $row['entries'] = $totalEntries;

        // set the default access
        $row['access'] = '';

        if ((int)$row['areaid'] === 0) {
            $row['areaname'] = 'All';
        } else {
            if (isset($options->access)) {
                $useAccess = $options->access;
                if (strtolower($useAccess) === 'county') {
                    $useAccess = 'Countie';
                }
                $row['access'] = $useAccess . 's Only';
            } else {
                $row['access'] = 'Private';
            }
        }

        $perms = getPermissionObjectForComp(null, $this->getID());
        $row['permissions'] = $perms;
        $row['reportId'] = '';
        $row['reportAccess'] = FALSE;
        if (isset($perms->report)) {
            $row['reportAccess'] = TRUE;
            $row['reportId'] = '';
        }

        $row['status'] = '';
        $row['organisers'] = array();
        if (isE4SUser() === TRUE) {
            $row['status'] = getCompStatus($this->getID());
            $row['organisers'] = $this->getOrganisers();
        }
        $model = new stdClass();
        $model->id = $this->getID();
        $row['compId'] = $model->id;
		// dont get notes if from home page
//		if ( !array_key_exists(E4S_PROCESSING_HOMEPAGE_COMPS, $GLOBALS)) {
			e4s_getTextBlobs( $model, $row );
//		}

        $row['compRules'] = $this->getRules();
        $eventTypeObj = $this->_getEventTypes();
        $row['eventTypes'] = $eventTypeObj;

        $this->setClubComp(TRUE);
        $row['clubCompInfo'] = $this->getClubCompData();
        $agObj = new e4sAgeClass(E4S_AGE_DEFAULT_BASE);
//        $row['compAgeGroups'] = getAllCompDOBs($this->getID()); // too much info
        $row['compAgeGroups'] = $agObj->getAgeGroups($this->getID()); // base concrete info
        return $row;
    }
	public function hideScheduledOnly() {
		$options = $this->getOptions();
		if (isset($options->hideScheduledOnly)) {
			return $options->hideScheduledOnly;
		}
		return false;
	}

    public function setContactInfoInCOptions($cOptions = null) {
        if (is_null($cOptions)) {
            $cOptions = $this->getOptions();
        }
        $contactObj = new e4sContactClass($this->compId);
        return $contactObj->getFullInfo($cOptions);
    }

    public function getLogo() {
        $row = $this->getRow();
        $logo = $row['logo'];
        if ($logo === '') {
            $logo = $this->config['cardlogo'];
            if ($logo === '' || is_null($logo)) {
                $logo = E4S_DEFAULT_LOGO;
            }
        }

        return $logo;
    }

    // return cOptions

    public function getSaleEndDate($force = TRUE, $save = TRUE) {
        $options = $this->options;
        if (isset($options->saleEndDate) and !$force) {
            return $options->saleEndDate;
        }
        $saleEndDate = $this->getLatestSaleEndDate();
        if (is_null($saleEndDate)) {
            unset($options->saleEndDate);
        } else {
            $options->saleEndDate = $saleEndDate;
        }
        $this->options = $options;
        if ($save) {
            $this->updateRowOptions();
        }
        return $saleEndDate;
    }

    public function getLatestSaleEndDate() {
	    $processingCompIds = $this->getID();
	    if ( array_key_exists(E4S_PROCESSING_HOMEPAGE_COMPS, $GLOBALS)) {
		    if ( array_key_exists(E4S_PROCESSING_COMPS_PRICE, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] )) {
			    if ( array_key_exists($this->getID(), $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_PRICE]) ) {
				    return $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_PRICE ][ $this->getID() ];
			    }
		    }else{
			    $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_PRICE] = [];
				if ( array_key_exists(E4S_PROCESSING_COMPS_IDS, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] ) ) {
					$processingCompIds                   = $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_IDS ];
					$processingCompIds[ $this->getID() ] = $this->getID(); // ensure current is in there
					foreach ( $processingCompIds as $compId ) {
						if ( ! array_key_exists( $compId, $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_PRICE ] ) ) {
							$GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_PRICE ][ $compId ] = null;
						}
					}
					$processingCompIds = implode( ",", $processingCompIds );
				}
		    }
	    }
		$prices = [];
        $priceSql = 'select distinct compId, saleenddate
                    from ' . E4S_TABLE_EVENTPRICE . '
                    where compid in (' . $processingCompIds . ')
                    order by saleenddate desc';
        $priceresult = e4s_queryNoLog($priceSql);

        while($pricerow = $priceresult->fetch_assoc()){
	        $compId = $pricerow['compId'];
	        if ( !array_key_exists($compId, $prices)){
		        $prices[$compId] = null;
	        }
	        $latestDate = null;
	        if (!is_null($pricerow) and !is_null($pricerow['saleenddate'])) {
		        $latestDate = $pricerow['saleenddate'];
	        }
			$prices[$compId] = $latestDate;
        }
	    if ( array_key_exists( E4S_PROCESSING_HOMEPAGE_COMPS, $GLOBALS ) ) {
		    if ( !array_key_exists( E4S_PROCESSING_COMPS_PRICE, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] ) ) {
			    $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_PRICE ] = [];
		    }
		    foreach ($prices as $compId => $priceDate) {
			    $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_RULES ][ $compId ] = $priceDate;
		    }
	    }

		if ( array_key_exists($this->getID(), $prices) ) {
			return $prices[$this->getID()];
		}
        return [];
    }

    public function getEntryCounts() {
        $cacheObj = new cacheClass($this->compId);
        // if the comp has happened read from cache else get latest
        $cache = $cacheObj->readCache(E4S_CACHE_COUNTS, E4S_OPTIONS_ARRAY, $this);
        return $cache;
    }

    public function hasAthleteClubSecurity() {
        $options = $this->getOptions();
        if (!isset($options->athleteSecurity)) {
            return '';
        }
        if (is_null($options->athleteSecurity->clubs)) {
            return '';
        }
        if (sizeof($options->athleteSecurity->clubs) > 0) {
            return $this->_clubDateEnforced($options);
        }
        return '';
    }

    private function _clubDateEnforced($options) {
        $passed = E4S_CLUB_SECURE_COMP;
        if (isset($options->athleteSecurity)) {
            if (isset($options->athleteSecurity->onlyClubsUpTo)) {
                $date = $options->athleteSecurity->onlyClubsUpTo; // ISO Date Time
                if ($date !== '' and !is_null($date)) {
                    $now = new DateTime();
                    $nowTS = $now->getTimestamp();
                    if ($nowTS < strtotime($date)) {
                        // date not passed
                        $passed = $date;
                    }
                }
            }
        }

        return $passed;
    }

    public function populateAthleteSecurity(&$options = null) {
        $setThis = FALSE;
        if (is_null($options)) {
            $options = $this->getOptions();
            $this->options = $options;
            $options = $this->options;
            $setThis = TRUE;
        }
        if (empty($options->athleteSecurity->clubs)) {
            return;
        }

        $options->athleteSecurity->clubs = e4s_getFullAthleteSecurityClubObj($options->athleteSecurity->clubs);

        if ($setThis) {
            $this->setOptions($options);
        }
    }
	public function getOrganisers() {
		$orgId = $this->getOrganisationId();
		$compId = $this->getID();
		return self::getOrganisersForComp($compId, $orgId);
	}
	public static function getOrganisersForComp($compId, $orgId) {
		$compPrefix = 'c';
		$orgPrefix = 'o';
		$readFromSql = true;
		$cacheKey = E4S_PROCESSING_COMPS_ORGS;
	    if ( array_key_exists(E4S_PROCESSING_HOMEPAGE_COMPS, $GLOBALS)) {
		    if ( array_key_exists($cacheKey, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] )) {
			    $readFromSql = false;
		    }
	    }else{
		    $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] = [];
	    }
		if ( $readFromSql ) {
			$sql = '
            select p.id permId,
                   p.orgId orgId,
                   p.compId compId,
                   p.userid userId,
                   r.id roleId,
                   r.role role,
                   u.user_email userEmail,
                   u.display_name displayName
            from ' . E4S_TABLE_PERMISSIONS . ' p,
                 ' . E4S_TABLE_USERS . ' u,
                 ' . E4S_TABLE_ROLES . ' r
            where u.id = p.userid
            and   p.roleid = r.id';
//
//		(p.compid = ' . $this->compId . '
//		 or    (p.compid = 0 and p.orgid = ' . $orgId . ' ))

			$orgs   = array();
			$result = e4s_queryNoLog( $sql );
			while ( $obj = $result->fetch_object() ) {
				$obj->permId = (int) $obj->permId;
				$obj->orgId  = (int) $obj->orgId;
				$obj->compId = (int) $obj->compId;

				$userObj     = new stdClass();
				$userObj->id = (int) $obj->userId;
				unset( $obj->userId );
				$userObj->userEmail = $obj->userEmail;
				unset( $obj->userEmail );
				$userObj->displayName = $obj->displayName;
				unset( $obj->displayName );
				$obj->user = $userObj;

				$roleObj     = new stdClass();
				$roleObj->id = (int) $obj->roleId;
				unset( $obj->roleId );
				$roleObj->name = $obj->role;
				unset( $obj->role );

				$obj->role = $roleObj;
				if ( $obj->compId !== 0 ) {
					$arrKey = $compPrefix . $obj->compId;
				} else {
					$arrKey = $orgPrefix . $obj->orgId;
				}
				if ( !array_key_exists($arrKey, $orgs) ){
					$orgs[ $arrKey ] = [];
				}
				$orgs[ $arrKey ][] = $obj;
			}
			$GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][$cacheKey] = $orgs;
		}
        $retOrgs = [];
	    if ( array_key_exists($compPrefix . $compId, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][$cacheKey]) ) {
		    foreach($GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ $cacheKey ][ $compPrefix . $compId ] as $orgObj){
			    $retOrgs[] = $orgObj;
		    }
	    }
	    if ( array_key_exists($orgPrefix . $orgId, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][$cacheKey]) ) {
		    foreach($GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ $cacheKey ][ $orgPrefix . $orgId ] as $orgObj){
			    $retOrgs[] = $orgObj;
		    }
	    }
		return $retOrgs;
    }

    public function getRules() {
	    $processingCompIds = $this->getID();
	    if ( array_key_exists(E4S_PROCESSING_HOMEPAGE_COMPS, $GLOBALS)) {
		    if ( array_key_exists(E4S_PROCESSING_COMPS_RULES, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] )) {
			    if ( array_key_exists($this->getID(), $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_RULES]) ) {
				    return $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_RULES ][ $this->getID() ];
			    }
		    }elseif (array_key_exists(E4S_PROCESSING_COMPS_IDS, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS])){
				// make sure compId exists as may have no rules
			    $processingCompIds                                                  = $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_IDS];
				$processingCompIds[$this->getID()]                                  = $this->getID(); // ensure current is in the list
			    $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_RULES] = [];
			    foreach ($processingCompIds as $compId) {
				    if ( !array_key_exists($compId , $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_RULES] ) ) {
					    $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_RULES ][ $compId ] = [];
				    }
			    }
			    $processingCompIds = implode(",", $processingCompIds);
		    }
	    }
        $compRules = array();
        $ruleSQL = 'select * from ' . E4S_TABLE_COMPRULES . ' 
            where compid in (' . $processingCompIds . ')';

        $ruleResult = e4s_queryNoLog($ruleSQL);
        while ($rulerow = mysqli_fetch_array($ruleResult, TRUE)) {
			$compId = $rulerow['compID'];
            // if more than 1 row return, only interested in last row
	        if ( !array_key_exists($compId, $compRules)){
		        $compRules[$compId] = [];
	        }
            $ruleObj = array();
            $ruleObj['id'] = $rulerow['id'];
            $ruleObj['agid'] = $rulerow['ageGroupID'];
            $ruleObj['options'] = json_decode($rulerow['options']);

            $compRules[$compId][] = $ruleObj;
        }

	    if ( array_key_exists( E4S_PROCESSING_HOMEPAGE_COMPS, $GLOBALS ) ) {
		    if ( !array_key_exists( E4S_PROCESSING_COMPS_RULES, $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] ) ) {
			    $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_RULES ] = [];
		    }
		    foreach ($compRules as $compId => $compRule) {
			    if ( !array_key_exists($compId , $GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_RULES] ) ) {
				    $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_RULES ][ $compId ] = [];
			    }
			    $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_RULES ][ $compId ][] = $compRule;
		    }
	    }

		if ( array_key_exists($this->getID(), $compRules) ) {
			return $compRules[ $this->getID() ];
		}
		return [];
    }

    private function _getEventTypes() {
        $eventGroups = $this->getEGObjs();
        $eventTypes = $this->_getEventTypesDefault();
        foreach ($eventGroups as $eventGroup) {
            if ($eventGroup->options->isTeamEvent) {
                $eventTypes->teamEvents = TRUE;
            } else {
                $eventTypes->indivEvents = TRUE;
            }
        }
        return $eventTypes;
    }

    private function _getEventTypesDefault() {
        $eventTypes = new stdClass();
        $eventTypes->indivEvents = FALSE;
        $eventTypes->teamEvents = FALSE;
        $eventTypes->tickets = FALSE;
        return $eventTypes;
    }

    public function getContactObjFromCOptions($cOptions = null) {
        if (is_null($cOptions)) {
            $cOptions = $this->getOptions();
        }
        return $cOptions->contact;
    }

    public function getEventDate($ceId) {
        $ceObjs = $this->getCeObjs();
        if (array_key_exists($ceId, $ceObjs)) {
			$ceObj = $ceObjs[$ceId];
			$egObj = $this->getEventGroupByEgId($ceObj->egId);
            $date = $egObj->startDate;
            $dateArr = preg_split('~ ~', $date);
            return $dateArr[0];
        }
        return $this->getRow()['Date'];
    }

    public function getEventGroupsForIds($egIds) {
        $retArr = array();
        $eventGroups = $this->getEGObjs();
        foreach ($egIds as $egId) {
            if (isset($eventGroups[$egId])) {
                $retArr[$egId] = $eventGroups[$egId];
            }
        }
        return $retArr;
    }

    /**
     * @param int $egId
     * @param bool $allowNull
     * @return E4S_EVENTGROUP_OBJ|null or null
     */
    public function getEventGroupByEgId(int $egId, bool $allowNull = FALSE) {
        $eventGroups = $this->getEGObjs();
        if (!array_key_exists($egId, $eventGroups)) {
            if ($allowNull) {
                return null;
            } else {
                Entry4UIError(9366, 'Invalid Event Group : ' . $egId);
            }
        }
        return $eventGroups[$egId];
    }

    public function getEventGroupByNo($eventNo) {
        $eventGroups = $this->getEGObjs();
        foreach ($eventGroups as $eventGroup) {
            if ($eventGroup->eventNo === $eventNo) {
                return $eventGroup;
            }
        }
        return null;
    }

    public function getEventGroupByTypeNo($eventTypeNo) {
        $eventGroups = $this->getEGObjs();
        foreach ($eventGroups as $eventGroup) {
            if (strcasecmp($eventGroup->typeNo, $eventTypeNo) === 0) {
                return $eventGroup;
            }
        }
        return null;
    }

    public function getIdAndName() {
        return $this->getID() . ': ' . $this->getName();
    }

    public function getLocationObj() {
        $obj = new stdClass();
        $obj->id = $this->row['locationid'];
        $obj->name = $this->getLocationName();
        return $obj;
    }

    public function getLocationName() {
        return $this->row['location'];
    }

    public function getOrganisationObj() {
        $obj = new stdClass();
        $obj->id = $this->getOrganisationId();
        $obj->name = $this->getOrganisationName();
        return $obj;
    }

    public function getCompLimits() {
        $limits = $this->_getDefaultCompLimits();
        $cOptions = $this->getOptions();
        if (isset($cOptions->compLimits)) {
            $curLimits = $cOptions->compLimits;
            if (isset($curLimits->entries)) {
                $limits->entries = $curLimits->entries;
            }
            if (isset($curLimits->athletes)) {
                $limits->athletes = $curLimits->athletes;
            }
            if (isset($curLimits->teams)) {
                $limits->teams = $curLimits->teams;
            }
        }
        return $limits;
    }

    private function _getDefaultCompLimits() {
        $limits = new stdClass();
        $limits->athletes = 0;
        $limits->entries = 0;
        $limits->teams = 0;
        return $limits;
    }

    public function getLiveCompLimits() {
        $sql = 'select e.id,
                       e.athleteId
                from ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce
                where e.compeventid = ce.id
                and   ce.compid = ' . $this->getID() . '
                and e.paid = ' . E4S_ENTRY_PAID;
        $athletes = array();
        $result = e4s_queryNoLog($sql);
        $limits = $this->_getDefaultCompLimits();
        $limits->entries = $result->num_rows;
        while ($obj = $result->fetch_object()) {
            $athletes[$obj->athleteId] = TRUE;
        }
        $sql = 'select e.productId,
                       ea.athleteId
                from ' . E4S_TABLE_EVENTTEAMENTRIES . ' e,
                     ' . E4S_TABLE_EVENTTEAMATHLETE . ' ea,
                     ' . E4S_TABLE_COMPEVENTS . ' ce
                where e.ceid = ce.id
                and   ea.teamentryid = e.id
                and   ce.compid = ' . $this->getID() . '
                and   e.paid = ' . E4S_ENTRY_PAID;

        $result = e4s_queryNoLog($sql);
        $teams = array();

        while ($obj = $result->fetch_object()) {
            $teams[$obj->productId] = TRUE;
            $athletes[$obj->athleteId] = TRUE;
        }
        $limits->teams = sizeof($teams);
        $limits->athletes = sizeof($athletes);
        return $limits;
    }

    public function getCompContactEmail($cOptions = null) {
        if (is_null($cOptions)) {
            $cOptions = $this->getOptions();
        }
        $contactObj = new e4sContactClass($this->compId);
        return $contactObj->getContactEmail($cOptions);
    }

    public function addNote($athleteId, $note) {
        $sql = sprintf('select id
                        ,notes
                from %s
                where compid = %d
                and athleteid = %s', E4S_TABLE_BIBNO, $this->getID(), $athleteId);
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            return;
        }
        $bibObj = $result->fetch_object();
        if (is_null($bibObj->notes)) {
            $bibObj->notes = '';
        }
        if (!strpos($bibObj->notes, $note)) {
            // return already added
            return;
        }
        $sep = '';
        if ($bibObj->notes !== '') {
            $sep = '~';
        }
        $note = $bibObj->notes . $sep . $note;
        $sql = 'update ' . E4S_TABLE_BIBNO . "
               set notes = '" . $note . "'
               where id = " . $bibObj->id;
        e4s_queryNoLog($sql);
        $data = new stdClass();
        $data->note = $note;
        $data->athleteId = $athleteId;
        $this->sendSocketInfo($data, R4S_ADD_NOTES);
    }

    public function sendSocketInfo($data, $type) {
        $socket = new socketClass($this->getID());
        $payload = new stdClass();
        $payload->data = $data;
        $socket->setPayload($payload);
        $socket->sendMsg($type);
    }

    public function getEventAdjustment(): int {
        $options = $this->getOptions();
        if (!isset($options->adjustEventNo)) {
            return 0;
        }
        return (int)$options->adjustEventNo;
    }

    public function areCardsAvailable() {
        $options = $this->options;
        if (isset($options->cardInfo)) {
            if (isset($options->cardInfo->enabled)) {
                return $options->cardInfo->enabled;
            }
        }
        return TRUE;
    }

    public function isAthleteDataRequired() {
        $required = FALSE;
        if (isset($this->options)) {
            if (isset($this->options->athleteQrData)) {
                $required = $this->options->athleteQrData;
            }
        }
        return $required;
    }

    public function getFullOptions() {
        $options = $this->getOptions();
        $this->populateAthleteSecurity($options);
        return $options;
    }

	static function allowInternationalAthletes($options){
		if (isset($options->allowAdd)) {
			if (isset($options->allowAdd->international)) {
				return $options->allowAdd->international;
			}
		}
		return false;
	}
	public function allowInternational($options = null){
		if (is_null($options)) {
			$options = $this->getOptions();
		}
		return self::allowInternationalAthletes($options);
	}
	static function allowUnRegisteredAthletes($options){
		if (isset($options->allowAdd)) {
			if (isset($options->allowAdd->unregistered)) {
				return $options->allowAdd->unregistered;
			}
		}
		return false;
	}
	public function allowUnRegistered($options = null){
		if (is_null($options)) {
			$options = $this->getOptions();
		}
		return self::allowUnRegisteredAthletes($options);
	}
	static function allowRegisteredAthletes($options){
		if (isset($options->allowAdd)) {
			if (isset($options->allowAdd->registered)) {
				return $options->allowAdd->registered;
			}
		}
		return true;
	}
	public function allowRegistered($options = null){
		if (is_null($options)) {
			$options = $this->getOptions();
		}
		return self::allowRegisteredAthletes($options);
	}
    public function validateOptions($options = null) {
        if (is_null($options)) {
            $options = $this->getOptions();
        }
        if (!$this->allowUnRegistered($options) and !$this->allowRegistered($options) and !$this->allowInternational($options)) {
            $options->allowAdd = configClass::getDefaultAthletesAllowed($this->config);
        }
		if ( isset($options->ui) ) {
		    if ( isset($options->ui->ticketComp) and is_numeric($options->ui->ticketComp) ) {
				$ticketObj = e4s_getCompObj($options->ui->ticketComp, false, false);
				if ( is_null($ticketObj) ) {
					Entry4UIError(9036, 'Invalid Ticket Competition', 200, '');
				}
			}
		}
        return $options;
    }

    public function isCheckinEnabled($options = null) {
        if (is_null($options)) {
            $options = $this->getOptions();
        }
        if ($options->checkIn->enabled) {
            return TRUE;
        }
        return FALSE;
    }

    public function isSchool() {
        if (isset($this->getOptions()->school)) {
            return $this->getOptions()->school;
        }
        return FALSE;
    }

    public function isClubInAthleteSecurity($checkId) {
        $clubIds = $this->getAthleteClubSecurity();
        foreach ($clubIds as $clubId) {
            if ((int)$clubId === $checkId) {
                return TRUE;
            }
        }

        return FALSE;
    }

    public function getAthleteClubSecurity() {
        $athleteClubs = array();

        foreach ($this->getOptions()->athleteSecurity->clubs as $athleteClub) {
            $athleteClubs[] = $athleteClub->id;
        }
        return $athleteClubs;
    }

    public function hasAthleteAreaSecurity() {
        if (sizeof($this->getOptions()->athleteSecurity->areas) > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function isAreaInAthleteSecurity($checkId) {
        $areaIds = $this->getAthleteAreaSecurity();
        foreach ($areaIds as $areaId) {
            if ((int)$areaId === $checkId) {
                return TRUE;
            }
        }

        return FALSE;
    }

    public function getAthleteAreaSecurity() {
        $athleteAreas = array();

        foreach ($this->getOptions()->athleteSecurity->areas as $athleteArea) {
            $athleteAreas[] = $athleteArea->id;
        }
        return $athleteAreas;
    }

    private function _clearCache() {
        $cacheObj = new cacheClass($this->getID());
        $cacheObj->clearCache();
    }

    public function forceCache($cacheDate) {
        return $cacheDate < $this->row['lastentrymod'];
    }

    public function setActive($active) {
        $this->row['active'] = $active;
    }

    public function getSummary($fromToday = FALSE): stdClass {
        $summary = new stdClass();
        $summary->id = $this->compId;
        $summary->name = $this->row['Name'];
        $summary->date = $this->row['compDate'];
        $summary->dates = $this->getDatesFromOptions($fromToday);
        $summary->location = new stdClass();
        $summary->location->id = $this->row['locationid'];
        $summary->location->name = $this->row['location'];
        unset($this->row['location']);
//        $summary->organiser = $this->getCompOrg($this->row['compclubid']);
        $summary->organiser = new stdClass();
        $summary->organiser->id = $this->getOrganisationId();
        $summary->organiser->name = $this->getOrganisationName();
        unset($this->row['compclub']);
        $egObj = new eventGroup($summary->id);
        $egModel = new stdClass();
        $summary->eventGroups = $egObj->listEventGroups($egModel, FALSE);
        return $summary;
    }

    public function getLocation() {
        $locobj = new stdClass();
        $locobj->id = $this->row['locationid'];
        $locobj->name = '';
        $sql = 'select location name
                from ' . E4S_TABLE_LOCATION . '
                where id = ' . $locobj->id;

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            $row = $result->fetch_assoc();
            $locobj->name = $row['name'];
        }
        return $locobj;
    }

    public function add_Category_to_Product($productId) {
        if (!isset($this->options->categoryId)) {
            $this->set_product_category();
        }
        wp_set_object_terms($productId, $this->options->categoryId, 'product_cat');
    }

    public function set_product_category() {
        $terms = get_terms('product_cat', array('hide_empty' => FALSE, 'fields' => 'ids'));
        $fields = null;
        $id = null;
        foreach ($terms as $term_id) {
            $product_category = current($this->get_product_category($term_id, $fields));
            if ($product_category['name'] === 'comp' . $this->compId) {
                $id = $product_category['id'];
                $this->updateOptionsWithCategory($id);
                break;
            }
        }
        if (is_null($id)) {
            $this->createProductCategory();
        }
        if (!isset($this->options->categoryId)) {
            Entry4UIError(9037, 'Failed to get/add Competition Category', 200, '');
        }
    }

    public function get_product_category($id, $fields = null) {
        try {
            $id = absint($id);

            // Validate ID
            if (empty($id)) {
                Entry4UIError(9038, 'Invalid product category id.', 200, '');
            }

            $term = get_term($id, 'product_cat');

            if (is_wp_error($term) || is_null($term)) {
                Entry4UIError(9104, "Product Category ID not found ({$id}).", 200, '');
            }

            $term_id = intval($term->term_id);

            $product_category = array('id' => $term_id, 'name' => $term->name, 'slug' => $term->slug, 'parent' => $term->parent, 'description' => $term->description, 'display' => 'default', 'image' => '', 'count' => intval($term->count),);

            return array('product_category' => apply_filters('woocommerce_api_product_category_response', $product_category, $id, $fields, $term, $this));
        } catch (Exception $err) {
            Entry4UIError(9105, $err->getMessage(), 200, '');
        }
    }

    public function updateOptionsWithCategory($id) {
        $this->options->categoryId = $id;
        $this->updateRowOptions();
    }

    public function createProductCategory() {
        $data = array('name' => 'comp' . $this->compId, 'slug' => 'comp' . $this->compId, 'description' => $this->row['Name'], 'parent' => 0, 'display' => 'default', 'image' => '',);

        try {
            $data = apply_filters('woocommerce_api_create_product_category_data', $data, $this);
            $insert = wp_insert_term($data['name'], 'product_cat', $data);
            if (is_wp_error($insert)) {
                // $insert->get_error_message()
                Entry4UIError(9106, 'Product category already exists.', 200, '');
            }

            $id = $insert['term_id'];

            update_term_meta($id, 'display_type', 'default' === $data['display'] ? '' : sanitize_text_field($data['display']));

            do_action('woocommerce_api_create_product_category', $id, $data);

            $this->updateOptionsWithCategory($id);
        } catch (Exception $err) {
            Entry4UIError(9107, $err->getMessage(), 200, '');
        }

    }

    public function setPriorityCodeRequired() {
        $priorityOptions = $this->options->priority;

        // dont send up anything but the message and if required
        $retObj = new stdClass();
        $retObj->message = $priorityOptions->message;
        $retObj->code = '';
        $retObj->dateTime = '';
        $retObj->required = FALSE;

        if ($priorityOptions->code !== '') {
//        there is a code so check date which is in iso format with offset
            $goesPublic = new DateTime($priorityOptions->dateTime);
            $goesPublic = $goesPublic->getTimestamp();
            $now = time();

            if ($now > $goesPublic) {
                $retObj->required = TRUE;
            }
        }
        return $retObj;
    }

    public function createYITHConsolidatedReceivers($productObjs) {
        if (!e4s_useStripeConnect()) {
            return [];
        }
        $config = e4s_getConfig();
        $stripeObj = null;
        $defaultAO = $config['defaultao']['code'];

        $e4sStripeObject = $this->getStripeObj(E4S_COMPCLUB_ID);
        if (!is_null($e4sStripeObject)) {
            $e4s_stripe_id = $e4sStripeObject->stripeid;
            $e4s_stripe_user_id = $e4sStripeObject->userid;
        }
        // NATIONALSTRIPE
//        if (!$this->isNationalComp()) {
        // Regional
        $stripeObj = $this->getStripeObj($this->getOrganisationId());
        if (!is_null($stripeObj)) {
            $stripe_id = $stripeObj->stripeid;
            $stripe_user_id = $stripeObj->userid;
        }
//        }

        $productIds = array();
        $orderCostObj = new stdClass();
        $orderCostObj->e4sValue = 0;
        $orderCostObj->custValue = 0;
        $productId = 0;
        foreach ($productObjs as $productId => $productObj) {
            $productIds[] = $productId;
            $costObj = $this->getPriceSplit($defaultAO, $productObj->price);
            $orderCostObj->e4sValue += $costObj->e4sValue;
            $orderCostObj->custValue += $costObj->custValue;
        }
        $this->_clearYITHRecords($productIds);

        $returnIds = new stdClass();
        $returnIds->E4S = 0;
        $returnIds->Customer = 0;
        if ($orderCostObj->e4sValue > 0 and !is_null($e4sStripeObject)) {
            $this->createYITHReceiverRecord($e4s_stripe_user_id, $productId, $e4s_stripe_id, $orderCostObj->e4sValue);
            $returnIds->E4S = $e4s_stripe_user_id;
        }

        if ($orderCostObj->custValue > 0 and !is_null($stripeObj)) {
            $this->createYITHReceiverRecord($stripe_user_id, $productId, $stripe_id, $orderCostObj->custValue);
            $returnIds->Customer = $stripe_user_id;
        }
        return $returnIds;
    }

    public function getStripeObj($id) {
        $E4S_STRIPE_CACHE = 'e4s_stripe_cache';

        if (!array_key_exists($E4S_STRIPE_CACHE, $GLOBALS)) {
            $GLOBALS[$E4S_STRIPE_CACHE] = array();
        }
        $globalCache = $GLOBALS[$E4S_STRIPE_CACHE];
        $globalCache[$id] = null;

        if (!array_key_exists($id, $globalCache)) {
            return $globalCache[$id];
        }

        $compOrgObj = $this->getCompOrg($id);
        $obj = new stdClass();
        $obj->userid = $compOrgObj->stripeuserid;
        $obj->stripeid = '';
        if ($compOrgObj->stripeuserid !== 0) {
            $sql = 'select *
            from ' . E4S_TABLE_USERMETA . '
            where user_id = ' . $compOrgObj->stripeuserid . "
            and meta_key = 'stripe_user_id'";
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows === 1) {
                $row = $result->fetch_assoc();
                $obj->stripeid = $row['meta_value'];
                $globalCache[$id] = $obj;
            }
        }
        return $globalCache[$id];
    }

    public function getCompOrg($id) {
        $obj = new stdClass();
        $obj->id = $id;
        $obj->name = '';
        $obj->stripeuserid = 0;
        $sql = 'select club name, stripeuserid stripeuserid
                from ' . E4S_TABLE_COMPCLUB . '
                where id = ' . $obj->id;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            $row = $result->fetch_assoc();
            $obj->name = $row['name'];
            $obj->stripeuserid = (int)$row['stripeuserid'];
        }
        return $obj;
    }

    public function isNationalComp() {
        return $this->isNational;
    }

    public function getPriceSplit($defaultAO, $price) {
        $priceObj = $this->priceObj;
        $transactionFee = $priceObj->getFeeForPrice($price);
        $e4s_value = 0;
        $stripe_value = $priceObj->getStripeFeeForPrice($defaultAO, $price);

        if (!e4s_useStripeConnect()) { // Using E4S Bank Account
            // Not using Stripe Connect
            // transfer the entry fee minus our commission to the organiser ( UK and Regional )
            $cust_value = $price - $transactionFee;
        } else {
            $e4s_value = $transactionFee;
            if (!$this->isNationalComp()) {
                // Transfer to us minus Stripe Fee
                $e4s_value = $transactionFee - $stripe_value;
            }
            $cust_value = $price - $transactionFee;
        }
        $obj = new stdClass();
        $obj->e4sValue = $e4s_value;
        $obj->custValue = $cust_value;
        $obj->stripeValue = $stripe_value;
        return $obj;
    }

    private function _clearYITHRecords($productIds) {
        $sql = 'delete from ' . E4S_TABLE_YITHRECEIVERS . '   where product_id in (' . implode(',', $productIds) . ')';
        e4s_queryNoLog($sql);
    }

    public function createYITHReceiverRecord($stripe_user_id, $productid, $stripe_id, $yith_value) {
        if ($stripe_user_id !== 0 and $stripe_id !== '') {
            $insertSql = 'Insert into ' . E4S_TABLE_YITHRECEIVERS . "(disabled,user_id,all_products,product_id,stripe_id,commission_value,commission_type, status_receiver, order_receiver)
              values (
                0,
                $stripe_user_id,
                0,
                $productid,
                '" . $stripe_id . "',
                $yith_value,
                'fixed',
                'connect',
                0
              )";
            e4s_queryNoLog($insertSql);
        }
        return e4s_getLastID();
    }

    // productObjs is an array of objects
    // productObj->productId
    // productObj->price

    public function contactOrganiser($category, $reason, $email, $foaE4S = FALSE) {
        $request = new contactRequestClass($this, e4s_getUserID());
        $request->sendOrganiserRequest($category, $reason, $email, $foaE4S);
        Entry4UISuccess('');
    }

    public function getAthletes($summary = FALSE, $searchModel = null) {
        $usePaging = FALSE;
        $pagesize = 0;
        $page = 0;
        $sortkey = '';
        $sortorder = '';
        $startsWith = '';
        if (!is_null($searchModel)) {
            $pagesize = $searchModel->pagesize;
            $page = $searchModel->page;
            $sortkey = $searchModel->sortkey;
            $sortorder = $searchModel->sortorder;
            $startsWith = $searchModel->startswith;

            if (isset($pagesize) and isset($page)) {
                $usePaging = TRUE;
            }
        }
        $sql = '
            select distinct(e.athleteid) athleteId
                    ,a.firstName
                    ,a.surName
                    ,a.gender';

        if (!$summary) {
            $sql .= ',a.urn,a.dob,b.bibNo ';
        }

        $sql .= '   ,ce.ageGroupId
                    ,c.clubName
                    ,ag.name ageGroup
            from ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce, ';
        if (!$summary) {
            $sql .= E4S_TABLE_BIBNO . ' b,  ';
        }

        $sql .= E4S_TABLE_AGEGROUPS . ' ag,
                 ' . E4S_TABLE_ATHLETE . ' a left join ' . E4S_TABLE_CLUBS . ' c on a.clubid = c.id
            where ce.id = e.compeventid
            and ce.compid = ' . $this->getID() . '
            and e.athleteid = a.id
            and ag.id = ce.agegroupid';
        if (!$summary) {
            $sql .= ' and b.compid = ce.compid
                       and b.athleteid = a.id';
        }

        if ($startsWith !== '') {
            $sql .= " and (firstname like '" . $startsWith . "%' ";
            $sql .= " or surname like '" . $startsWith . "%') ";
        }
        if ($sortkey !== '') {
            if ($sortkey === 'id') {
                $sortkey = 'surname';
            }
            $sql .= ' order by ' . $sortkey . ' ' . $sortorder;
        }
        if ($usePaging and $pagesize !== 0) {
            $sql .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
        }

        $result = e4s_queryNoLog($sql);
        $arr = [];
        while ($row = $result->fetch_object()) {
            $arr[$row->athleteId] = $row;
        }
        Entry4UISuccess($arr);
    }

    public function waitingListEntriesExist() {
        $counts = $this->getEntryCounts();
        if (array_key_exists('waitingEntries', $counts)) {
            return $counts['waitingEntries'] > 0;
        }
        return FALSE;
    }

    public function checkAndUpdateWaitingStatus($requestedOptions) {
        $requestEnabled = $this->isWaitingListEnabled($requestedOptions, TRUE);
        $curEnabled = $this->isWaitingListEnabled(null, TRUE);
        if ($requestEnabled === $curEnabled) {
//            waitingList enabled as has not changed
            return;
        }
        $sql = '';
        if ($requestEnabled) {
            // asked to enable waiting list so open all events as there will now be a waiting list
            $sql = 'update ' . E4S_TABLE_COMPEVENTS . '
                    set isopen = ' . E4S_EVENT_OPEN . '
                    where compid = ' . $this->compId;

        } else {
            // turned off waitingllist or past enabled date so close any "full" events
            // what happens to those on waiting list but not in event. Need to ensure they are still refunded
            $waitingObj = new waitingClass($this->getID());
            $closeEgIds = $waitingObj->getFullEvents();
            if (!empty($closeEgIds)) {
                $sql = 'update ' . E4S_TABLE_COMPEVENTS . '
                        set isopen = ' . E4S_EVENT_CLOSED . '
                        where maxgroup in (' . implode(',', $closeEgIds) . ')';
            }
        }
        if ($sql !== '') {
            e4s_queryNoLog($sql);
        }
    }

    public function isWaitingListEnabled($cOptions = null, $checkDate = FALSE) {
        $enabled = FALSE;
        if (is_null($cOptions)) {
            $cOptions = $this->getOptions();
        }
        if (isset($cOptions->subscription)) {
            $enabled = $cOptions->subscription->enabled;
            if ($enabled and $checkDate) {
                $waitingOptions = $cOptions->subscription;
                if ($waitingOptions->enabled) {
                    $waitingCloseDate = strtotime($this->_getWaitingListCloseDate($cOptions));
                    $now = time();
                    if ($waitingCloseDate < $now) {
                        $enabled = TRUE;
                    }
                }
            }
            if (isset($cOptions->subscription->refunded) and $cOptions->subscription->refunded !== '') {
                // has refunds alreaded run
                $enabled = FALSE;
            }
        }
        return $enabled;
    }

    private function _getWaitingListCloseDate($options = null) {
        if (is_null($options)) {
            $options = $this->getOptions();
        }
        $waitingClosedDate = date('Y-m-d', strtotime($this->getEntriesCloseDate() . ' - 1 day'));
        if ($options->subscription->timeCloses !== '') {
            $waitingClosedDate = $options->subscription->timeCloses;
        }
        return $waitingClosedDate;
    }

    public function getEntriesCloseDate() {
        return $this->row['EntriesClose'];
    }

    public function getEntriesOpenDate() {
        return $this->row['EntriesOpen'];
    }

    public function updateCompOptions($options = null) {
        if ($options === null) {
            $options = $this->getOptions();
        }
        $options = $this->getOptionsForWriting($options);

        $sql = 'update ' . E4S_TABLE_COMPETITON . "
                set options = '" . e4s_getOptionsAsString($options) . "'
                where id = " . $this->getID();
        e4s_queryNoLog($sql);
        return $options;
    }

    public function disableWaitingListProcessing() {
        $options = $this->getOptions();
        $options->subscription->process = FALSE;
        $this->updateCompOptions($options);
    }

    public function disableWaitingList() {
        $options = $this->getOptions();
        $options->subscription->enabled = FALSE;
        $this->updateCompOptions($options);
    }

    public function lastEventDate() {
        $dates = $this->getAllEventDates();
        return end($dates);
    }

    public function canWaitingListRefundsBeProcessed() {
        if (!$this->isActive()) {
            // only active comps can process waiting lists
            return FALSE;
        }
        if (!$this->isWaitingListEnabled()) {
            // is the function enabled
            return FALSE;
        }
        $row = $this->getRow();
        $today = strtotime(date('Y-m-d'));
        $compDate = strtotime($this->lastEventDate());
        if ($today < $compDate) {
            return FALSE;
        }

        $row['waitingrefunded'] = (int)$row['waitingrefunded'];
        if ($row['waitingrefunded'] === 1) {
            return FALSE;
        }

        $refundWaitingListDateTs = $this->_getProcessWaitingRefundsDateTime();
        $now = strtotime('today');
        if ($refundWaitingListDateTs > $now) {
            return FALSE;
        }
        return TRUE;
    }

    public function isActive() {
        return $this->row['active'];
    }

    private function _getProcessWaitingRefundsDateTime() {
        $compDate = $this->getDate();
        if ($this->options->subscription->processRefundTime !== '') {
            // ISO Date Time
            $refundWaitingListDateTs = strtotime($this->options->subscription->processRefundTime);
        } else {
            $config = e4s_getConfig();
            $cfgOptions = $config['options'];
            $daysPastCompToProcessWaitingList = $cfgOptions->waitingListDays;
            $refundWaitingListDateTs = strtotime($compDate . ' + ' . $daysPastCompToProcessWaitingList . ' day');
        }
        return $refundWaitingListDateTs;
    }

    public function getFormattedDate($format){
        $date = new DateTime($this->getDate());
        return $date->format($format);
    }
    public function getDate() {
        return $this->row['Date'];
    }

    public function initWaitingList() {
        if (!$this->isWaitingListEnabled()) {
            // is the function enabled
            return FALSE;
        }
    }

    public function isShopOnly() {
        $options = $this->getOptions();
        return $options->ui->entryDefaultPanel === E4S_UI_SHOP_ONLY;
    }

    public function performWaitingListRefunds() {
        if (e4s_isLiveDomain() or e4s_isDevDomain()) {
            // get list of athletes on waiting list
            // check paid but they should only be paid anyway.
            $waitingObj = new waitingClass($this->getID());
            $waitingObj->performWaitingListRefunds();
        } else {
            Entry4UISuccess('', 'performWaitingListRefunds on non-live domain');
        }
    }

// remove cloned info
    public function removeClonedInfoFromOptions($options) {
        $options->priority->required = FALSE;
        $options->priority->code = '';
        $options->priority->dateTime = '';
        $options->saleEndDate = '';
        $options->bibNos = '';
        $options->subscription->refunded = '';
        $options->subscription->process = TRUE;
        $options->subscription->processRefundTime = '';
        $options->checkIn->checkInDateTimeOpens = '';
        $options->categoryId = 0;
        return $options;
    }

    public function hasEventSecurity(): bool {
        $ceObjs = $this->getCeObjs();

        if (sizeof($ceObjs) < 1) {
            return FALSE;
        }
        foreach ($ceObjs as $ceObj){
            $options = $ceObj->options;
            if (!$options->isTeamEvent) {
                if (!empty($options->security->clubs)) {
                    return TRUE;
                }
                if (!empty($options->security->counties)) {
                    return TRUE;
                }
                if (!empty($options->security->regions)) {
                    return TRUE;
                }
            }
        }
        return FALSE;
    }
	public function checkEventGroupDates(){
		$egObjs = $this->getEGObjs();
		$compDate = $this->getDate();
		$eventDates = [];
		foreach ($egObjs as $egObj) {
			$startDate = $egObj->startDate;
			$eventDate = substr($startDate, 0, 10);
			if ( $eventDate === $compDate ){
				$compDate = ''; // at least one event on comp date
				break;
			}
			$eventDates[$eventDate] = $eventDate;
		}
		if ( $compDate !== '' ){
			if ( sizeof($eventDates) === 1 ) {
				// The comp is a 1 day event, so update event groups to have the same date as the comp, leaving the time
				$sql = "update " . E4S_TABLE_EVENTGROUPS . "
						set startDate = concat('" . $compDate . "', ' ' , date_format(startdate, '%h:%i') )
						where compid = " . $this->getID();
				e4s_queryNoLog($sql);
			}
		}
	}
}
const E4S_MSG_INFORMATION = 1;
const E4S_MSG_WARNING = 2;
const E4S_MSG_ERROR = 3;
class competitionValidation extends e4sCompetition {
    var $msgs;
	var $qualifyingEvents;
    public function __construct($compId) {
        parent::__construct($compId);
		if ( is_null($this->row) ){
			$this->loadByID();
		}
        $this->msgs = [];
		$this->qualifyingEvents = [];
    }

    public function validate(): array {
        $this->_checkEventGroups();
        $this->_checkCompEvents();
        $this->_checkCompAges();
		$this->_checkPrices();
// check event numbers for gaps e.t.c
// events have times
// comp schedule is provisional

        // remove empty messages. As they are only added to, this should be redundant
        $msgs = array_filter($this->msgs);
		if ( sizeof($msgs) === 0 ){
			$this->_addMsg('No validation issues found.', E4S_MSG_INFORMATION);
		}
	    $msgs = array_filter($this->msgs);
		return $msgs;
    }
	private function _checkPrices():void{
		$sql = "select saleenddate
				from " . E4S_TABLE_EVENTPRICE . "
				where saleenddate is not null and
				compid = " . $this->getID();
		$result = e4s_queryNoLog($sql);
		if ( $result->num_rows > 0 ) {
			while ( $obj = $result->fetch_object() ){
				$saleEndDate = $obj->saleenddate;
				$entriesOpen = $this->getEntriesOpenDate();
				$entriesClose= $this->getEntriesCloseDate();
				$validMsg = priceClass::validatePriceDate($saleEndDate, $entriesOpen,$entriesClose);
				if ( $validMsg !== '' ){
					$this->_addMsg($validMsg, E4S_MSG_WARNING);
				}
			}
		}
	}
    private function _checkCompEvents():void {
        $ceObjs = $this->getCeObjs();
		$ceArr = [];
        foreach ($ceObjs as $ceObj) {
			$key = $ceObj->eventId . ':' . $ceObj->ageGroupId . ":" . $ceObj->egId;
			$eg = $this->getEventGroupByEgId($ceObj->egId, true);
			if ( is_null($eg) ){
				$this->_addMsg( "CompEvent : " . $ceObj->id . " Event: " . $ceObj->eventId . " is not connected to a valid Event (" . $ceObj->egId . ").", E4S_MSG_ERROR );
			}
			if ( array_key_exists($key, $ceArr) ){
				$this->_addMsg( $eg->typeNo . " : " . $eg->name . ' has duplicate gender/agegroup defined.', E4S_MSG_ERROR );
			}
			$ceArr[$key] = $key;
            $this->_checkTeamCE($ceObj);
        }
    }

    private function _checkTeamCE($ceObj){
        $options = $ceObj->options;
        if ($options->isTeamEvent) {
			if ( $options->eventTeam->disableTeamNameEdit ) {
				if ( isset($options->eventTeam->teamNameFormat)) {
					if ( $options->eventTeam->teamNameFormat === '' ) {
						// Should this be part of the update to check and ensure its NOT empty if none editable
						$egObj = $this->getEventGroupByEgId( $ceObj->egId );
						$this->_addMsg( $egObj->typeNo . ' : ' . $egObj->name . ' is a team event, but no team name format is set.', E4S_MSG_ERROR );
					}
				}
			}
        }
    }
	private function _checkFinals($egObj){
		$qualObj = new e4sQualifyClass($egObj->options);
		$egToId = $qualObj->getId();
		$egFromStartDate = $egObj->startDate;

		if ($egToId > 0 ){
			$targetEgObj = $this->getEventGroupByEgId($egToId, true);
			if ( is_null($targetEgObj) ){
				return $this->_addMsg( $egObj->typeNo . ':' . $egObj->name . ' is a qualifier with an invalid target event.', E4S_MSG_ERROR);
			}
			if ($qualObj->getseedtype() === E4S_SEED_OPEN) {
				return $this->_addMsg('Event ' . $egObj->typeNo . ':' . $egObj->name . ' is a qualifier, but seeding is set to OPEN.', E4S_MSG_WARNING);
			}
			if ( array_key_exists($egToId,$this->qualifyingEvents) ){
				return $this->_addMsg('Event ' . $egObj->typeNo . ':' . $egObj->name . ' and ' . $this->qualifyingEvents[$egToId] . " are qualifiers to the same event" , E4S_MSG_ERROR);
			}
			$this->qualifyingEvents[$egToId] = $egObj->typeNo . ':' . $egObj->name;
			$egToStartDate = $targetEgObj->startDate;
			if( is_null($egFromStartDate) ){
				$egFromStartDate = '';
			}
			if( is_null($egToStartDate) ){
				$egToStartDate = '';
			}
			if ($egFromStartDate === '' and $egToStartDate !== ''){
				$this->_addMsg($egObj->typeNo . ':' . $egObj->name . ' is a qualifier with no date set.', E4S_MSG_WARNING);
			}
			if ($egFromStartDate !== '' and $egToStartDate !== ''){
				if ( strpos($egFromStartDate, '00:00:00') !== FALSE or strpos($egToStartDate, '00:00:00') !== FALSE ) {
					// either date does not have a start time, so only check dates
				   $egFromStartDate = substr($egFromStartDate, 0, 10);
				   $egToStartDate = substr($egToStartDate, 0, 10);
				}

				if ( $egFromStartDate > $egToStartDate ){
					$this->_addMsg( $egObj->typeNo . ':' . $egObj->name . ' is a qualifier with a start date after the final.', E4S_MSG_ERROR);
				}

			}
		}else{
			if ($qualObj->getSeedType() === E4S_SEED_HEAT and $egObj->type === E4S_EVENT_TRACK) {
				$this->_addMsg($egObj->typeNo . ':' . $egObj->name . ' seeded on heats with no final set.', E4S_MSG_WARNING);
			}
		}
	}
	private function _checkOrganisation(){
		$orgId = $this->getOrganisationId();
		if ( $orgId === 0 ){
			// ever happen ?
			$this->_addMsg('No organisation set for competition.', E4S_MSG_ERROR);
		}
		
	}
    private function _checkEventGroups():void {
        $egObjs = $this->getEGObjs();
        $eventNos = [];
        $eventTypes = [];
		$compDate = $this->getDate();
		$eventDates = [];
		$isOnCompDate = FALSE;
        foreach ($egObjs as $egObj) {
			$eventDate = substr($egObj->startDate, 0, 10);
			$eventDates[$eventDate] = $eventDate;
			if ( $eventDate === $compDate ) {
				// event date is on compdate
				$isOnCompDate = TRUE;
			}

			$this->_checkFinals($egObj);
            $eventNos[$egObj->eventNo] = $egObj->eventNo;
            $eventType = $egObj->typeNo[0];
            $typeNo = (int)substr($egObj->typeNo, 1);

            if ( !array_key_exists($eventType, $eventTypes) ){
                $eventTypes[$eventType] = [$typeNo=>$typeNo];
            }else {
                if (!array_key_exists($typeNo, $eventTypes[$eventType])) {
                    $eventTypes[$eventType][$typeNo] = $typeNo;
                } else {
                    $this->_addMsg('Event ' . $eventType . $typeNo . ' is duplicated.', E4S_MSG_ERROR);
                }
            }

            $this->_checkMultiEventsEG($egObj);
        }

		if ( !$isOnCompDate ){
			$this->_addMsg('No event on competition date.', E4S_MSG_ERROR);
		}
		if ( sizeof ($eventDates) > 1 ){
			$this->_addMsg('Competition is defined for mulitple days.', E4S_MSG_INFORMATION);
		}
        for($counter = 1; $counter <= sizeof($eventNos); $counter++){
            if (!array_key_exists($counter, $eventNos)) {
                $this->_addMsg('Event numbering is not sequential', E4S_MSG_WARNING);
                break;
            }
        }
        foreach($eventTypes as $eventType => $typeNos){
            for($counter = 1; $counter <= sizeof($typeNos); $counter++){
                if (!array_key_exists($counter, $eventTypes[$eventType])) {
                    $this->_addMsg('Numbering for ' . e4s_expandEventType($eventType) . ' events is not sequential.', E4S_MSG_WARNING);
                    break;
                }
            }
        }
    }
    private function _checkMultiEventsEG($egObj):void{
        $eventDefOptions = $egObj->eventDef->options;
        if ( isset($eventDefOptions->multiEventOptions)) {
            $multiObj = new multiEventGroup($egObj->id, $this);
            $childObjs = $multiObj->getEventGroupChildEvents();
            $noOfMulti = sizeof($eventDefOptions->multiEventOptions);
            $noOfChildren = sizeof($childObjs);
            if ( $noOfMulti !== $noOfChildren ) {
                $this->_addMsg('Event ' . $egObj->typeNo . ':' . $egObj->name . ' is a multi event, but does not have the standard number of events (' . $noOfChildren .'/' .$noOfMulti .')', E4S_MSG_WARNING);
            }
        }
    }
    private function _checkCompAges(){
        // check age groups. Do they overlap. Any Gaps e.t.c
    }
    private function _addMsg($msg, $type){
		// check if message already exists
	    foreach ($this->msgs as $msgObj){
		    if ( $msgObj->msg === $msg ){
			    return;
		    }
	    }
        $msgObj = new stdClass();
        $msgObj->msg = $msg;
        $msgObj->type = $type;
        $this->msgs[] = $msgObj;
    }
}