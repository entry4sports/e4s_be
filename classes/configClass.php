<?php
define('E4S_CONFIG_CACHE', 'E4S_CONFIG_CACHE');
const E4S_MAINT_MESSAGE = 'System is being updated. Please come back shortly.';
function e4s_inMaintenance($exit = TRUE) {
    $config = e4s_getConfig();
    $return = FALSE;
    if ($config['maintenance']) {
        if (e4s_getUserID() === 18978) {
            return $return;
        }
        if (!isE4SUser()) {
            $return = $config['maintenance'];
        }
        if (!isE4SUser() and $exit) {
            Entry4UISuccess('System under Maintenance');
        }
    }
    return $return;
}

function e4s_getConfig($extended = FALSE, $messages = FALSE) {
    $configObj = e4s_getConfigObj();
    return $configObj->getConfig($extended, $messages);
}

function e4s_getConfigObj():configClass{
    if (array_key_exists(E4S_CONFIG_CACHE, $GLOBALS)) {
        $configObj = $GLOBALS[E4S_CONFIG_CACHE];
    } else {
        $configObj = new configClass();
        $GLOBALS[E4S_CONFIG_CACHE] = $configObj;
    }
    return $configObj;
}

function e4s_writeHealth($property, $seconds = 180) {
    $configObj = e4s_getConfigObj();
    return $configObj->writeHealth($property, $seconds);
}

function e4s_useStripeConnect() {
    $configObj = e4s_getConfigObj();
    return $configObj->useStripeConnect();
}
$allAOs = null;
class configClass {
    public $userId;
    public $configArrs = array();
    public $aoArrs = array();
    public $classifications = array();

    public function __construct() {
        $this->userId = (int)e4s_getUserID();
		global $allAOs;
		if ( is_null( $allAOs ) ) {
			$sql    = 'select * from ' . E4S_TABLE_AO;
			$result = e4s_queryNoLog( $sql );
			$aos    = array();
			while ( $arr = $result->fetch_assoc() ) {
				$this->aoArrs[ $arr['code'] ] = $arr;
				$aos[]                        = $arr;
			}
			$allAOs = $aos;
		}else{
			$aos = $allAOs;
		}

        $sql = 'select * from ' . E4S_TABLE_CONFIG;
        $result = e4s_queryNoLog($sql);
        while ($config = $result->fetch_assoc()) {
            $config['options'] = e4s_getOptionsAsObj($config['options']);
            if (array_key_exists('features', $config)) {
                $config['features'] = e4s_getOptionsAsObj($config['features']);
            } else {
                $config['features'] = new stdClass();
            }
            if (isset($config['options']->maintenance)) {
                $config['maintenance'] = $config['options']->maintenance;
            } else {
                $config['maintenance'] = FALSE;
            }

            $config['defaultao'] = $this->aoArrs[$config['defaultao']];
            $config['aos'] = $aos;
            $this->configArrs[$config['public']] = $config;
        }
        $this->setMaintenance($this->configArrs[E4S_CONFIG_PRIVATE]);
    }

    public function setMaintenance($config) {
        if (!is_null($config)) {
            if ($config['maintenance']) {
                if (!isset($config['options']->homePage)) {
                    $config['options']->homePage = new stdClass();
                }
                $config['options']->homePage->message = E4S_MAINT_MESSAGE;
            } else {
                if (isset($config['options']->homePage)) {
                    if ($config['options']->homePage->message === E4S_MAINT_MESSAGE) {
                        $config['options']->homePage->message = '';
                    }
                }
            }
            $config['options']->homeMessage = '';
            if (isset($config['options']->homePage)) {
                if (isset($config['options']->homePage->message)) {
                    $config['options']->homeMessage = $config['options']->homePage->message;
                }
            }
        }
    }

    public static function getDefaultAthletesAllowed($config) {
        $options = $config['options'];
        if (isset($options->allowAdd)) {
            $allowAdd = $options->allowAdd;
        } else {
            $allowAdd = new stdClass();
            $allowAdd->registered = TRUE;
            $allowAdd->unregistered = TRUE;
            $allowAdd->international = TRUE;
        }
        return $allowAdd;
    }

	public function getVersion() {
		return $this->configArrs[ E4S_CONFIG_PRIVATE ]['version'];
	}
	public function setVersion($version){
		$sql = "update " . E4S_TABLE_CONFIG . "
				set version = '" . $version . "'";
		e4s_queryNoLog($sql);
	}
    public function inMaintenance() {
        return $this->configArrs[E4S_CONFIG_PRIVATE]->maintenance;
    }

    public function hasFullAccess() {
        $privateConfig = $this->getConfig();
        $options = $privateConfig['options'];
        if (isset($options->fullAccess)) {
            $userId = e4s_getUserID();
            $fullAccess = $options->fullAccess;
            foreach ($fullAccess as $fullAccessId) {
                if ($fullAccessId === $userId) {
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

    public function getStripeApprovers() {
        $config = $this->getConfig();
        if (empty($config['options']->stripe->approvers)) {
            return 'paul@entry4sports.com';
        }
        return $config['options']->stripe->approvers;
    }

    public function getConfig($extended = FALSE, $messages = FALSE) {
        $config = $this->configArrs[E4S_CONFIG_PRIVATE];

        $config['options'] = e4s_addDefaultConfigOptions($config['options']);
        if ($extended) {
            $config = $this->_getExtendedConfig($config, $messages);
        }

        return $config;
    }

    private function _getExtendedConfig($config, $messages) {
//        $logoutURL = "/wp-login.php?action=logout";
        $logoutURL = 'https://' . E4S_CURRENT_DOMAIN . '/wp-json/e4s/v5/logout';

        $config['userInfo'] = e4s_getUserObj(TRUE, FALSE);

// TODO Remove once UserObj done
        $config['userId'] = $this->userId;
        $checkIn = new stdClass();
        $checkIn->shortUrl = $config['checkinurl'];
        $checkIn->defaultText = E4S_DEFAULT_CHECKIN_TEXT;
        unset($config['checkinurl']);

        $config['checkIn'] = $checkIn;
        $config['role'] = e4s_getUserRole();
// TODO End of removal code

        $menus = array();
        if (userHasPermission('finance', 0, 0)) {
            $reports = array();
            $menus['reports'] = $reports;
        }

        $admin = e4s_getAdminPermissionObject();

        if ($admin->adminMenu === TRUE) {
            unset ($admin->adminMenu);
            $menus['admin'] = $admin;
        }

        $config['menus'] = $menus;

        $config['classifications'] = $this->_getClassifications();
        $config['logout'] = $logoutURL;
        $helpObj = helpClass::preload();
        $config['help'] = $helpObj->getRows();
        if (e4s_getUserID() === E4S_USER_NOT_LOGGED_IN or !$messages) {
            $messages = array();
        } else {
            $msgObj = new e4SMessageClass();
            $obj = $msgObj->getDefaultObj();
//            $obj->get->read = false;
            $obj->get->messages = TRUE;
            $obj->get->emails = TRUE;
            $obj->get->deleted = FALSE;
            $messages = $msgObj->getMessages($obj);
            $checkedMessages = array();

            foreach ($messages as $message) {
                if (json_encode($message->body) !== FALSE) {
                    $checkedMessages[] = $message;
                }
            }
            $messages = $checkedMessages;

        }
        $config['messages'] = $messages;
        return $config;
    }

    private function _getClassifications() {
        $sql = 'select *
                from ' . E4S_TABLE_CLASSIFICATION;
        $result = e4s_queryNoLog($sql);
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function useStripeConnect() {
        $privateConfig = $this->getConfig();
        $options = $privateConfig['options'];
        if (isset($options->useStripeConnect)) {
            return $options->useStripeConnect;
        }
        return FALSE;
    }

    public function setLocalHost($set) {

    }

    public function healthMonitor() {
        $systemActive = FALSE; // allow the system to not be busy and therefore dont raise activity report
        $nowDt = date_create('now');
        $nowFormatted = date(E4S_FORMATTED_DATETIME);
        $output = '';
        $config = $this->getConfig();
        $options = $config['options'];
        $healthObjs = $options->health;
        $sendConfirmationEmail = FALSE;
        foreach ($healthObjs as $healthProperty => $healthObj) {
            $doubleTime = $healthObj->every * 2;
            $lastRun = e4s_iso_to_sql($healthObj->lastRun);
            $lastRunDT = date_create($lastRun);
            $lastRunFormatted = date_format($lastRunDT, E4S_FORMATTED_DATETIME);

            $diffInSeconds = $nowDt->getTimestamp() - $lastRunDT->getTimestamp();
            if ($diffInSeconds >= $doubleTime) {
                // cron job not run for 2 runs
                if ($output === '') {
                    $output = 'System Time is : ' . $nowFormatted . '. The following monitors have not run in allowable times:<br><br>';
                }
                $output .= $healthProperty . ' last run : ' . $lastRunFormatted . '. It is marked to run every ' . $healthObj->every . ' seconds.<br>';
            }
            if ($healthProperty === E4S_CRON_HEALTHCHECKER) {
                if ($diffInSeconds <= $doubleTime) {
                    // Health has been running ok
                    $systemActive = TRUE;
                }
                // are we in a new day ?
                if (date_format($lastRunDT, 'd') !== date('d')) {
                    $sendConfirmationEmail = TRUE;
                }
            }
        }
        if ($output !== '' and $systemActive) {
            $this->_raiseWarningOnHealth($output);
        }
        if ($sendConfirmationEmail) {
            $this->_raiseConfirmationOnHealth();
        }
        $this->writeHealth(E4S_CRON_HEALTHCHECKER, 360);
    }

    private function _raiseWarningOnHealth($output) {
//        logTxt("_raiseWarningOnHealth called");
        if ($this->_emailsEnabled()) {
            $healthSendTo = $this->_getHealthEmailAddrs();
            $body = 'E4S Administrators,<br><br>';
            $body .= $output;
            $body .= Entry4_emailFooter();
            e4s_mail($healthSendTo, E4S_WARNING_PREFIX . 'Health Warning', $body, Entry4_mailHeader(''));
        }
    }

    private function _emailsEnabled() {
		return false;
//        return e4s_isLiveDomain();
    }

    private function _getHealthEmailAddrs() {
        $healthSendTo = ['paul@entry4sports.com', 'nick@entry4sports.com'];
        return $healthSendTo;
    }

    private function _raiseConfirmationOnHealth() {
//        logTxt("_raiseConfirmationOnHealth called");
        if ($this->_emailsEnabled()) {
            $healthSendTo = $this->_getHealthEmailAddrs();
            $body = 'E4S Administrators,<br><br>';
            $body .= 'This email confirms E4S Health is running ok. You may delete this email.<br><br>';
            $body .= 'You should only be concerned if you DO NOT receive one of these emails daily.<br><br>';
            $body .= Entry4_emailFooter();
            e4s_mail($healthSendTo, 'Entry4Sports Health is ok.', $body, Entry4_mailHeader(''));
        }
    }

    public function writeHealth($property, $seconds = 180) {
        $nowIso = date('c');
        foreach ($this->configArrs as $config) {
            $options = $config['options'];
            $health = e4s_getDataAsType($options->health, E4S_OPTIONS_ARRAY);
            unset($health[$property]);
            $health[$property] = new stdClass();
            $health[$property]->lastRun = $nowIso;
            $health[$property]->every = $seconds;
            $options->health = e4s_getOptionsAsObj($health);
            $config['options'] = $options;
            $this->_saveConfig($config);
        }
    }

    private function _saveConfig($config) {
        $sql = 'update ' . E4S_TABLE_CONFIG . "
                set options = '" . e4s_getOptionsAsString($config['options']) . "'
                where id = " . $config['id'];
        e4s_queryNoLog($sql);
    }
}