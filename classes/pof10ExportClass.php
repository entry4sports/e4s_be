<?php
include_once E4S_FULL_PATH . 'classes/excelClass.php';
class pof10ExportClass {
    public $outputHTML;
    public $excelArray;
    public $resultObj;
    public $agObj;
    public $compObj;

    public function __construct($compId) {
        $this->outputHTML = '';
        $this->resultObj = new compResults($compId);
        $this->compObj = $this->resultObj->compObj;
        $this->excelArray = array();
        $this->agObj = new e4sAgeClass(E4S_AGE_DEFAULT_BASE);
    }

    public function generatePof10File() {
        if (!$this->compObj->isOrganiser()) {
            Entry4UIError(9062, 'You are not authorised to perform this function');
        }
        $egObjs = $this->compObj->getEGObjs();
        if (is_null($egObjs)) {
            Entry4UIError(8012, 'No Events setup for comp');
        }
        $filename = 'Competition_' . $this->compObj->getID() . '.xlsx';
        $this->_outputHeader();
        $this->_outputResults();
		if ( isE4SUser()) {
//			$this->excelArray = array_slice($this->excelArray, 1040, 1);
//			var_dump($this->excelArray);
//			exit;
		}
        $xlsx = excelClass::fromArray($this->excelArray);
        $xlsx->downloadAs($filename);
    }

    private function _outputHeader() {
        $this->_outputLineToHTML('Results of Events using Entry4Sports');
        $this->_outputLineToHTML('Competition=' . $this->compObj->getID());
        $this->_outputLineToHTML('Meet=' . $this->compObj->getName());
        $this->_outputLineToHTML('Location=' . $this->compObj->getLocationName());
        $this->_outputLineToHTML('date=' . $this->_getCompDate());
        $this->_outputLineToHTML('');
    }

    private function _outputLineToHTML($line) {
        $array = [$line];
        $this->excelArray[] = $array;
    }

    private function _getCompDate() {
        $date = $this->compObj->getDate();
        $arr = explode('-', trim($date));
        $date = $arr[2] . '-' . $arr[1] . '-' . $arr[0];

        return $date;
    }

    private function _outputResults() {
        $sql = 'select  eg.compid compId,
                        eventno eventNo,
                        eg.name eventName,
                        rh.heatno heatNo,
                        eg.startdate scheduledTime,
                        eg.startdate actualTime,
                        lane laneNo,
                        if(position=0,9999,position) position,
                        replace(wind, "\0", "") wind,
                        aocode aoCode,
                        urn urn,
                        pof10id pof10Id,
                        athlete athlete,
                        dob dob,
                        club clubName,
                        bibno bibNo,
                        a.gender gender,
                        rh.options rhOptions,
                        a.gender athleteGender,
                        score score,
                        scoretext scoreText
                from ' . E4S_TABLE_EVENTRESULTSHEADER . ' rh , ' . E4S_TABLE_EVENTRESULTS . ' rd left join ' . E4S_TABLE_ATHLETE . ' a on a.id = athleteid,
                     ' . E4S_TABLE_EVENTGROUPS . ' eg
                where eg.compid = ' . $this->compObj->getID() . '
                and rd.resultheaderid = rh.id
                and rh.egid = eg.id
                order by eventno, heatno, position';
        $result = e4s_queryNoLog($sql);
        $lastEventNo = 0;
        $lastHeatNo = 0;

        while ($resultObj = $result->fetch_object()) {
            $eventNo = $resultObj->eventNo;
            $heatNo = $resultObj->heatNo;
            $schedTime = $resultObj->scheduledTime;
            $actualTime = $resultObj->actualTime;

            if ($eventNo !== $lastEventNo or $heatNo !== $lastHeatNo) {
                $this->_outputLineToHTML('');
                $heatTitle = $resultObj->eventName;
                $this->_outputLineToHTML($heatTitle);
                if (is_null($actualTime) or $actualTime === '') {
                    $actualDT = date_create($schedTime);
                } else {
                    $actualDT = date_create($actualTime);
                }
                $time = date_format($actualDT, 'd/m/Y H:i');
                $this->_outputLineToHTML($time);

                $rhOptions = e4s_getOptionsAsObj($resultObj->rhOptions);
                if (isset($rhOptions->description) and $rhOptions->description !== '') {
                    $heatTitle = $rhOptions->description;
                } else {
                    $heatTitle = 'Race ' . $heatNo;
                }

                if (trim($resultObj->wind) !== '' and $resultObj->wind !== 'N/A m/s') {
                    $heatTitle .= ' Wind ' . $resultObj->wind;
                }
                $this->_outputLineToHTML($heatTitle);
            }
            $this->_outputResultLine($resultObj);
            $lastHeatNo = $heatNo;
            $lastEventNo = $eventNo;
        }
    }

    private function _outputResultLine($resultObj) {
        $dob = $resultObj->dob;
        $age = '';
        if (!is_null($dob)) {
            $ageGroup = $this->agObj->getBaseAGForAthleteDOB($resultObj->dob, $this->compObj->getDate());

            if (!is_null($ageGroup)) {
                $age = $ageGroup->name;
            }
        }
        $gender = '-';
        if (!is_null($resultObj->gender)) {
            $gender = $resultObj->gender;
        }

        if ($gender === '-') {
            $sql = 'select gender
                    from ' . E4S_TABLE_ATHLETE . ' a,
                         ' . E4S_TABLE_BIBNO . ' b
                         where b.compid = ' . $this->compObj->getID() . "
                         and   b.bibno = '" . $resultObj->bibNo . "'
                         and   b.athleteid = a.id";
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows === 1) {
                $obj = $result->fetch_object();
                $gender = $obj->gender;
            }
        }

        $pof10Id = '';
        $wind = '';
        if (trim($resultObj->wind) !== '' and $resultObj->wind !== 'N/A m/s') {
            $wind = trim($resultObj->wind);
        }
        $score = resultsClass::getNonNumeric($resultObj->score);
        if ($score === '') {
            $score = $resultObj->score;
            $eventNo = (int)$resultObj->eventNo;
            $type = E4S_EVENT_FIELD;
            $eventGroups = $this->compObj->getEGObjs();
            foreach ($eventGroups as $eventGroup) {
                if ($eventGroup->eventNo === $eventNo) {
                    if ($eventGroup->typeNo[0] === E4S_EVENT_TRACK) {
                        $type = E4S_EVENT_TRACK;
                    }
                }
            }
            if ($type === E4S_EVENT_TRACK) {
                $score = resultsClass::getResultFromSeconds($resultObj->score);
            }
        }
        $scoreText = $resultObj->scoreText;
		if ( is_null($resultObj->urn)){
			$resultObj->urn = '';
		}
		$position = (int)$resultObj->position;
		if ( $position === 9999 ){
			$position = '';
		}

        $array = [$position, $resultObj->bibNo, $gender, $resultObj->athlete, $resultObj->clubName, $age, $score, $scoreText, $wind, $pof10Id, $resultObj->urn];
        $this->excelArray[] = $array;
    }

    public function checkPof10Id(&$resultObj) {
        if (is_null($resultObj->aoCode) or $resultObj->aoCode !== E4S_AOCODE_EA) {
//            Not EA
            $resultObj->pof10Id = 0;
            return;
        }
        if (is_null($resultObj->urn) or $resultObj->urn === '') {
            // No URN
            $resultObj->pof10Id = 0;
            return;
        }

        if (!is_null($resultObj->pof10Id) and $resultObj->pof10Id !== '') {
            // already got an id
            return;
        }

        $pof10Obj = pof10Class::getWithURN($resultObj->aoCode, $resultObj->urn);
        $pof10Obj->checkPof10Id();
    }
}