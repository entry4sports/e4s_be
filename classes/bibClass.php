<?php
define('E4S_BIB_CLEAR', 0);
define('E4S_BIB_NEW', 1);
define('E4S_BIB_ADDITIONAL', 2);
define('E4S_BIB_OTD_ADDITIONAL', 3);
define('E4S_BIB_LAST_UNKNOWN', -1);
define('E4S_BIB_INC_STANDARD', 0);
define('E4S_BIB_INC_RESET_ONCHANGE', -1);
define('E4S_BIB_SORT_EVENTNO', 'eventNo');
define('E4S_BIB_SORT_CLUBNAME', 'clubName');
define('E4S_BIB_SORT_SURNAME', 'surName');
define('E4S_BIB_SORT_FIRSTNAME', 'firstName');
define('E4S_BIB_SORT_GENDER', 'gender');
define('E4S_BIB_SORT_DOB', 'dob');
define('E4S_BIB_SORT_AGEGROUP', 'ageGroup');
define('E4S_BIB_SORT_EVENTAGEGROUP', 'eventAgeGroup');
define('E4S_BIB_SORT_CREATED', 'created');

function e4s_getUnusedBibNos($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition Id' . E4S_CHECKTYPE_NUMERIC);
    $bibObj = new bibClass($compId);
    $str = $bibObj->getUnusedStr();
    Entry4UISuccess('"data":"' . $str . '"');
}
function e4s_updateBibCompNotes($obj){
    $compId = checkFieldForXSS($obj, 'compId:Competition Id' . E4S_CHECKTYPE_NUMERIC);
    $athleteId = checkFieldForXSS($obj, 'athleteId:Athlete Number' . E4S_CHECKTYPE_NUMERIC);
    $bibNo = checkFieldForXSS($obj, 'bibNo:Bib Number' . E4S_CHECKTYPE_NUMERIC);
    $notes = checkFieldForXSS($obj, 'notes:Notes');

    $bibObj = new bibClass($compId);
    $bibObj->updateCompNotes($athleteId, $notes);
    Entry4UISuccess();
}
function e4s_changeAthletesBib($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $athleteId = checkFieldForXSS($obj, 'athleteid:Athlete ID' . E4S_CHECKTYPE_NUMERIC);
    $newBibNo = checkFieldForXSS($obj, 'bibno:New Bib No' . E4S_CHECKTYPE_NUMERIC);
    if ($compId < 1) {
        Entry4UIError(9404, 'Invalid Competition');
    }
    $compObj = e4s_getCompObj($compId);
    if (!$compObj->isOrganiser()) {
        Entry4UIError(9405, 'Sorry, function not available');
    }
    $bibObj = new bibClass($compId);
    $bibObj->amendAthleteBib($athleteId, $newBibNo);
    $payload = new stdClass();
    $payload->athleteId = $athleteId;
    $payload->bibNo = $newBibNo;
    e4s_sendSocketInfo($compId, $payload, R4S_SOCKET_ATHLETE_BIBNO);
}

function e4s_clearBibs($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (!userHasPermission(PERM_ADMIN, null, $compId)) {
        Entry4UIError(8401, 'You do not have the correct permission to perform this function');
    }
    $bibObj = new bibClass($compId, E4S_BIB_CLEAR);
    Entry4UISuccess();
}

function e4s_createBibs($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);

    if (!userHasPermission(PERM_ADMIN, null, (int)$compId)) {
        Entry4UIError(8402, 'You do not have the correct permission to perform this function');
    }
    $bibObj = new bibClass($compId, E4S_BIB_NEW);
    Entry4UISuccess();
}

function e4s_addBibs($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (!userHasPermission(PERM_ADMIN, null, $compId)) {
        Entry4UIError(8403, 'You do not have the correct permission to perform this function');
    }
    $bibObj = new bibClass($compId, E4S_BIB_ADDITIONAL);
    Entry4UISuccess();
}

function e4s_addOTDBibs($compId, $athleteId) {
    if (!e4s_hasAnyPermForComp($compId)) {
        Entry4UIError(8404, 'You do not have the correct permission to perform this function');
    }
    $bibObj = new bibClass($compId, E4S_BIB_OTD_ADDITIONAL);
    return $bibObj->getAthleteBibInfo($athleteId);
}

class bibClass {
    public $compId;
    public $compObj;
    public $method;
    public $lastBibNo;
    public $compBibNoDefs;
    public $currentAllocatedBibs;
    public $athleteArr;
    public $countObj;
    public $increment;
    public $minIncrement;
    public $resetFor;
    public $lastSortKey1;
    public $sortKey1;
    public $sortKey2;
    public $sortKey3;
    public $sortOrder;

    public function __construct($compId, $method = null) {
        $this->compId = $compId;
        $this->_setLastNo(E4S_BIB_LAST_UNKNOWN);
        $this->currentAllocatedBibs = null;
        $this->countObj = null;
        //    If > 0 then increment to the next factor of
        //    If -1 ( E4S_BIB_INC_RESET_ONCHANGE ), reset to 1 each time the sort column 1 changes
        $this->increment = E4S_BIB_INC_STANDARD;
        // Minimum number between splits
        $this->minIncrement = 1;
        $this->compBibNoDefs = '1';
        $this->lastSortKey1 = '';
        $this->sortKey1;
        $this->sortKey2;
        $this->sortKey3;
        if (!is_null($method)) {
            switch ($method) {
                case E4S_BIB_CLEAR:
                    $this->_clearBibs();
                    break;
                case E4S_BIB_NEW:
                    $this->_createBibs();
                    break;
                case E4S_BIB_ADDITIONAL:
                    $this->_addBibs();
                    break;
                case E4S_BIB_OTD_ADDITIONAL:
                    break;
            }
        }
    }

	public function allocateBibForNewAthlete($athleteId) {
		$sql = "select max(bibno) as bibno
				from " . E4S_TABLE_BIBNO . "
				where compid = " . $this->compId;
		$result = e4s_queryNoLog($sql);
		$bibNo = 1;
		if ($result->num_rows > 0) {
			$obj = $result->fetch_object();
			$bibNo = $obj->bibno + 1;
		}
		$sql = 'insert into ' . E4S_TABLE_BIBNO . ' (compid, athleteid, bibno, collected)
				values (' . $this->compId . ', ' . $athleteId . ', ' . $bibNo . ',1)';
		e4s_queryNoLog($sql);
	}
    public function updateCompNotes($athleteId, $notes) {
        $sql = 'update ' . E4S_TABLE_BIBNO . "
                set compnotes = '" . addslashes($notes) . "'
                where compid = " . $this->compId . '
                and athleteId = ' . $athleteId;

        e4s_queryNoLog($sql);
        $payload = new stdClass();
        $payload->athleteId = $athleteId;
        $payload->notes = $notes;
        $this->_sendSocketInfo($payload);
    }

    private function _sendSocketInfo($bibData) {
        $socket = new socketClass($this->compId);
        $payload = new stdClass();
        $payload->bibInfo = $bibData;
        $socket->setPayload($payload);
        $socket->sendMsg(R4S_SOCKET_UPDATE_BIBINFO);
    }

    private function _setLastNo($no) {
        $this->lastBibNo = $no;
    }

    private function _clearBibs($logClear = TRUE) {
        $clear = 'delete from ' . E4S_TABLE_BIBNO . '
              where compid = ' . $this->compId . '
              and athleteid != 167605';
        if ($logClear) {
            e4s_queryNoLog($clear);
        } else {
            e4s_queryNoLog($clear);
        }
    }

    private function _createBibs() {
        $this->_clearBibs(FALSE);
        $this->_setAndAllocate();
    }

    private function _setAndAllocate() {
        $this->_setAthletesArr();
        $this->_allocateBibNos();
    }

    private function _setAthletesArr() {
        $this->_getCurrentBibs();
        $compObj = e4s_GetCompObj($this->compId);
        $sql = '
            select e.athleteid aId,
               e.firstName ,
               e.surName ,
               e.gender ,
               e.dob ,
               e.clubName ,
               e.ceId,
               e.egId ,
               e.entryid eId,
               e.ageGroup  as eventAgeGroup,
               e.eventName ,
               e.eventNo,
               e.bibSortNo,
               e.periodStart  as created,
               e.periodStart ,
               0 isTeamEvent
            from ' . E4S_TABLE_ENTRYINFO . ' e
            where e.paid = ' . E4S_ENTRY_PAID . '
              and e.CompID = ' . $this->compId . '
        union 
            select a.id aId,
                   a.firstName ,
                   a.surName ,
                   a.gender ,
                   a.dob ,
                   c.clubName ,
                   ce.id ceId,
                   ce.maxGroup  as egId,
                   e.id eId,
                   ag.name  as eventAgeGroup,
                   eg.name  as eventName,
                   eg.eventNo,
                   eg.bibSortNo,
                   e.periodStart  as created,
                   e.periodStart ,
                   1 isTeamEvent
            from ' . E4S_TABLE_ATHLETE . ' a,
                 ' . E4S_TABLE_AGEGROUPS . ' ag,
                 ' . E4S_TABLE_EVENTTEAMENTRIES . ' e,
                 ' . E4S_TABLE_EVENTTEAMATHLETE . ' ea,
                 ' . E4S_TABLE_CLUBS . ' c,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTGROUPS . ' eg
            where ea.athleteid = a.id
              and a.clubid = c.id
              and e.paid = ' . E4S_ENTRY_PAID . '
              and ce.AgeGroupID = ag.id
              and e.ceid = ce.ID
              and ea.teamentryid = e.id
              and ce.maxGroup = eg.id
              and ce.CompID = ' . $this->compId . '
        order by aid, eventName, periodStart';

        $results = e4s_queryNoLog($sql);

        $athleteArr = array();
        while ($obj = $results->fetch_object()) {
            $obj->aId = (int)$obj->aId;
            if (is_null($obj->clubName)) {
                $obj->clubName = E4S_UNATTACHED;
            }
            $obj->ceId = (int)$obj->ceId;
            $obj->egId = (int)$obj->egId;
            $obj->eventNo = (int)$obj->eventNo;
            $obj->isTeamEvent = (int)$obj->isTeamEvent;
            $obj->sortKey = $this->_getSortKey($obj);
            $obj->sortOrder = $this->sortOrder;
            $obj->ageGroup = $this->_getAthletesAgeGroup($obj);
            $obj->bibNo = 0;

            $process = $compObj->isOrganiser();
            if (!$process) {
                // Not an organiser so check they are not on waiting list !!!!
                $process = $compObj->isAthleteOnWaitingListForId($obj->aId, $obj->egId);
            }
            if ($process) {
                // only add if not already in the list
                if (!array_key_exists($obj->aId, $athleteArr)) {
                    // set bibNo if existing
                    if (array_key_exists($obj->aId, $this->currentAllocatedBibs)) {
                        $obj->bibNo = $this->currentAllocatedBibs[$obj->aId]->bibNo;
                    }
                    if ($obj->bibNo === 0) {
                        $athleteArr[$obj->aId] = $obj;
                    }
                } else {
                    if ($obj->sortKey < $athleteArr[$obj->aId]->sortKey) {
                        $athleteArr[$obj->aId] = $obj;
                    }
                }
            }
        }
        usort($athleteArr, 'self::_athleteCompare');
        $this->athleteArr = $athleteArr;
    }

    private function _getCurrentBibs($force = FALSE) {
        if (is_null($this->currentAllocatedBibs) or $force) {
            $this->_setCurrentBibs();
        }
        return $this->currentAllocatedBibs;
    }

    private function _setCurrentBibs() {
        $this->currentAllocatedBibs = array();
        $sql = 'select athleteId athleteId,
                       bibNo bibNo,
                       notes,
                       compNotes,
                       collected collected,
                       collectedtime collectedTime
                from ' . E4S_TABLE_BIBNO . '
                where compid = ' . $this->compId;

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return array();
        }

        $bibNoArr = array();
        $maxBibNo = 0;
        while ($obj = $result->fetch_object()) {
            $obj->athleteId = (int)$obj->athleteId;
            $obj->bibNo = (int)$obj->bibNo;

            $bibNoArr[$obj->athleteId] = $obj;
            if ($obj->bibNo > $maxBibNo) {
                $maxBibNo = $obj->bibNo;
            }
        }

        $this->_setLastNo($maxBibNo);
        $this->currentAllocatedBibs = $bibNoArr;
    }

    private function _getSortKey($obj) {
        $this->_getSortKeyInfo();
        $arr = e4s_getOptionsAsArray($obj);

        $sortKey1 = $this->_getSortKeyValue($this->sortKey1, $arr[$this->sortKey1]);
        $sortKey2 = $this->_getSortKeyValue($this->sortKey2, $arr[$this->sortKey2]);
        $sortKey3 = $this->_getSortKeyValue($this->sortKey3, $arr[$this->sortKey3]);

        $bibSort = '999';
        if (!is_null($obj->bibSortNo) and (int)$obj->bibSortNo > 0) {
            $bibSort = substr('00' . $obj->bibSortNo, -3);
        }
        $sortkey = $obj->isTeamEvent . $bibSort . $sortKey1 . $sortKey2 . $sortKey3;

        return $sortkey;
    }

    private function _getSortKeyInfo() {
        $compObj = $this->_getCompObj();
        $coptions = $compObj->getOptions();
        $bibNos = '1';

        if (property_exists($coptions, 'bibNos')) {

            $bibNos = str_replace(' ', '', $coptions->bibNos);
            if ($bibNos === '') {
                $bibNos = '1';
            }
        }

        $bibNos = explode(',', $bibNos);

        $bibSortInc = 0;
        if (property_exists($coptions, 'bibSortInc')) {
//    If > 0 then increment to the next factor of
//    If -1, reset to 1 each time the sort column 1 changes
            $bibSortInc = (int)$coptions->bibSortInc;
        }
        if ($bibSortInc !== 0) {
            // ensure the bibnos is 1 value
            $bibNos = [explode('-', $bibNos[0])[0]];
        }
        $this->compBibNoDefs = $bibNos;
        $this->increment = $bibSortInc;

        $bibIncMin = 1;
        if (property_exists($coptions, 'bibIncMin')) {
// Minimum number between splits
            $bibIncMin = (int)$coptions->bibIncMin;
            if ($bibIncMin < 1) {
                $bibIncMin = 1;
            }
        }
        $this->minIncrement = $bibIncMin;

        $bibNoResetFor = null;
        if (property_exists($coptions, 'bibSortReset')) {
            $bibNoResetFor = explode(':', $coptions->bibSortReset);
        }
        $this->resetFor = $bibNoResetFor;
        $bibSort1 = E4S_BIB_SORT_CLUBNAME;
        $bibsortOrder = 'asc';
        if (property_exists($coptions, 'bibSort1')) {
            $bibSort1 = $this->_validateBibSort($coptions->bibSort1);
        }
        if ($bibSort1 === '') {
            $bibSort1 = E4S_BIB_SORT_CLUBNAME;
        }
        if (strpos($bibSort1, ':') > -1) {
            $bibsortOrder = explode(':', $bibSort1);
            $bibSort1 = $bibsortOrder[0];
            $bibsortOrder = $bibsortOrder[1];
        }
        $this->sortKey1 = $bibSort1;
        $this->sortOrder = $bibsortOrder;

        $bibSort2 = E4S_BIB_SORT_SURNAME;
        if (property_exists($coptions, 'bibSort2')) {
            $bibSort2 = $this->_validateBibSort($coptions->bibSort2);
        }
        if ($bibSort2 === '') {
            $bibSort2 = E4S_BIB_SORT_SURNAME;
        }
        $this->sortKey2 = $bibSort2;

        $bibSort3 = E4S_BIB_SORT_FIRSTNAME;
        if (property_exists($coptions, 'bibSort3')) {
            $bibSort3 = $this->_validateBibSort($coptions->bibSort3);
        }
        if ($bibSort3 === '') {
            $bibSort3 = E4S_BIB_SORT_FIRSTNAME;
        }
        $this->sortKey3 = $bibSort3;
    }

    private function _getCompObj() {
        if (is_null($this->compObj)) {
            $this->compObj = e4s_GetCompObj($this->compId);
        }
        return $this->compObj;
    }

    private function _validateBibSort($value) {
        $retval = str_replace('athlete ', '', $value);
        if ($retval === strtolower(E4S_BIB_SORT_CLUBNAME)) {
            $retval = E4S_BIB_SORT_CLUBNAME;
        }
        if ($retval === 'maxgroup') {
            $retval = E4S_BIB_SORT_EVENTNO;
        }
        if ($retval === strtolower(E4S_BIB_SORT_FIRSTNAME)) {
            $retval = E4S_BIB_SORT_FIRSTNAME;
        }
        if ($retval === strtolower(E4S_BIB_SORT_SURNAME)) {
            $retval = E4S_BIB_SORT_SURNAME;
        }
        if ($retval === strtolower(E4S_BIB_SORT_AGEGROUP)) {
            $retval = E4S_BIB_SORT_EVENTAGEGROUP;
        }
        if ($retval === strtolower(E4S_BIB_SORT_EVENTAGEGROUP)) {
            $retval = E4S_BIB_SORT_EVENTAGEGROUP;
        }
        return $retval;
    }

    private function _getSortKeyValue($key, $value) {
        switch ($key) {
            case E4S_BIB_SORT_GENDER:
                $value = substr($value, 0, 1);
                break;
            case E4S_BIB_SORT_EVENTNO:
                $value = substr('000000' . $value, -6);
                break;
            case E4S_BIB_SORT_DOB:
            case E4S_BIB_SORT_CREATED:
                $dt = new DateTime($value);
                $value = $dt->getTimestamp();
                $value = substr('0000000000' . $value, -10);
                break;
            default:
                $value = substr($value . '               ', 0, 15);
                break;
        }

        return $value;
    }

    private function _getAthletesAgeGroup($obj) {
        return $obj->eventAgeGroup;
    }

    private function _allocateBibNos($getNewBibs = TRUE) {
        $bibSQL = '';
        $athleteArr = $this->athleteArr;

        if (empty($athleteArr)) {
            return $this->currentAllocatedBibs;
        }

        foreach ($athleteArr as $athlete) {
            if ($athlete->bibNo === 0) {
                if ($bibSQL === '') {
                    $bibSQL = 'insert into ' . E4S_TABLE_BIBNO . '(compid,athleteid, bibno)
                       values ';
                } else {
                    $bibSQL .= ',';
                }
                $athlete->bibNo = $this->_getNextBibNo($athlete);
                $this->lastBibNo = $athlete->bibNo;
                $bibSQL .= '(' . $this->compId . ',' . $athlete->aId . ',' . $athlete->bibNo . ')';
            }
        }
        if ($bibSQL !== '') {
            e4s_queryNoLog($bibSQL);
        }
        if ($getNewBibs) {
            return $this->_getCurrentBibs(TRUE);
        }
        return TRUE;
    }

    private function _getNextBibNo($athleteObj) {
        $athleteArr = e4s_getOptionsAsArray($athleteObj);
        $compBibNoDefs = $this->compBibNoDefs;

        // Athlete Row will have a bibno of - when called
        $bibNo = $this->lastBibNo;
//        e4s_addDebug($compBibNoDefs, "Bib Defs ");
//        e4s_addDebug("initial Last = " . $bibNo);
        $lastKey = $this->lastSortKey1;

        if ($lastKey === '') {
            $lastKey = $athleteArr[$this->sortKey1];
        }

        if (sizeof($compBibNoDefs) === 1) {
            // Only 1 element. This HAS to be a number
            if (strpos($compBibNoDefs[0], '-') > 0) {
                Entry4UIError(8200, 'Bib Numbers are invalid');
            }

            if ($bibNo < (int)$compBibNoDefs[0]) {
                $this->lastSortKey1 = $athleteArr[$this->sortKey1];
                return (int)$compBibNoDefs[0];
            }

            $bibNo = $bibNo + 1;

            if ($this->increment !== 0 and $lastKey !== $athleteArr[$this->sortKey1]) {
                $this->lastSortKey1 = $athleteArr[$this->sortKey1];
                $reset = FALSE;
                if (!is_null($this->resetFor)) {
                    foreach ($this->resetFor as $value) {
                        if ($athleteArr[$this->sortKey1] === $value) {
                            $reset = TRUE;
                        }
                    }
                }
                if ($this->increment > 0 and !$reset) {
                    $bibNo = ceiling($bibNo + $this->minIncrement, $this->increment);
                } else {
                    // set to 1 again
                    $bibNo = 1;
                }
            }
            return $bibNo;
        }

// ok, so Bibs still have multiple.
        while (!empty($this->compBibNoDefs)) {
            $checkBibNo = $this->_returnBibFromDefinedElement();
            if ($checkBibNo > 0) {
                return $checkBibNo;
            }
            // If here, need to remove first element and re-assess
            array_shift($this->compBibNoDefs);
        }

        Entry4UIError(2001, 'Bib Numbers are invalid');
    }

    private function _returnBibFromDefinedElement() {
        $firstSet = explode('-', $this->compBibNoDefs[0]);

        $firstNo = (int)$firstSet[0];
        $lastNo = (int)$firstSet[0];
        if (sizeof($firstSet) > 1) {
            $lastNo = (int)$firstSet[1];
        }

        if ($this->lastBibNo < $firstNo) {
            return $firstNo;
        }

        $bibNo = $this->lastBibNo + 1;
        if ($bibNo <= $lastNo) {
            return $bibNo;
        }
        if (sizeof($this->compBibNoDefs) === 1) {
            // last def
            return $bibNo;
        }
        return 0;
    }

    private function _addBibs() {
        $this->_setAndAllocate();
    }

    private static function _athleteCompare($a, $b) {
        $x = $a->sortKey;
        $y = $b->sortKey;

        if ($x === $y) return 0;
        if (($x < $y and $a->sortOrder === 'asc') or ($x > $y and $a->sortOrder !== 'asc')) {
            return -1;
        } else {
            return 1;
        }
    }

    public function getUnusedStr() {
        $arr = $this->getUnused();
        return $this->_getUnusedList($arr);
    }

    public function getUnused() {
        $sql = 'select b.*
                from ' . E4S_TABLE_BIBNO . ' b
                where b.compid = ' . $this->compId . '
                and collected = false
                order by bibno';
        $result = e4s_queryNoLog($sql);
        $retArr = array();
        while ($obj = $result->fetch_object(E4S_BIB_OBJ)) {
            $retArr[] = $obj;
        }
        return $retArr;
    }

    private function _getUnusedList($objArr) {
        $str = '';
        $lastBibNo = 0;
        $nextConsecutive = $lastBibNo + 1;
        $consecutive = FALSE;
        foreach ($objArr as $bibObj) {
            $currentBibNo = $bibObj->bibNo;
            $nextBibNo = $currentBibNo + 1;
            if ($currentBibNo < $nextConsecutive) {
                Entry4UIError(9204, 'Bib Numbers not in order');
            }
            if ($lastBibNo === 0) {
                // First Bib No
                $str = '' . $currentBibNo;
            } elseif ($currentBibNo === $nextConsecutive) {
                $consecutive = TRUE;
            } elseif ($currentBibNo > $nextConsecutive) {
                if ($consecutive) {
                    $str .= '-' . $lastBibNo;
                }
                $str .= ',' . $currentBibNo;
                $consecutive = FALSE;
            }
            $lastBibNo = $currentBibNo;
            $nextConsecutive = $lastBibNo + 1;
        }
        if ($str === '') {
            $str = '1';
        }
        return $str;
    }

    public function amendAthleteBib($athleteId, $newBibNo) {
        // check its not already in use
	    $existBib = $this->getBibRecordByBibNo($newBibNo);
        if (! is_null($existBib) ) {
			if ( $existBib->athleteId == $athleteId){
				// already allocated to this athlete
				return;
			}
            Entry4UIError(3020, 'Bib number already allocated');
        }

		$athleteInfo = $this->getAthleteBibInfo($athleteId, false);
	    if ($athleteInfo->bibNo === 0) {
		    $sql = 'insert into ' . E4S_TABLE_BIBNO . ' (compid, athleteid, bibno)
                values (' . $this->compId . ', ' . $athleteId . ', ' . $newBibNo . ')';
	    } else {
		    $sql = 'update ' . E4S_TABLE_BIBNO . '
                set   bibno = ' . $newBibNo . '
                where compid = ' . $this->compId . '
                and   athleteid = ' . $athleteId;
	    }
        e4s_queryNoLog($sql);
    }

    public function getBibRecordByBibNo($bibNo) {
        $sql = 'select *
                from ' . E4S_TABLE_BIBNO . '
                where compid = ' . $this->compId . '
                and bibno = ' . $bibNo;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            return null;
        }
        return $result->fetch_object(E4S_BIB_OBJ);
    }

    public function getAthleteBibInfo($athleteId, $generate = TRUE) {
        $bibInfo = new stdClass();
        $bibInfo->bibNo = 0;
        $bibInfo->exists = FALSE;
        $bibs = $this->_getCurrentBibs();
        if (!empty($bibs)) {
            $existBibNo = $this->getAthleteBibNo($athleteId);
            if ($existBibNo > 0) {
                $bibInfo->bibNo = $existBibNo;
                $bibInfo->exists = TRUE;
            } elseif ($generate) {
                $this->_setAndAllocate(FALSE);
                $bibInfo->bibNo = $this->currentAllocatedBibs[$athleteId]->bibNo;
            }
        }

        return $bibInfo;
    }

    public function getAthleteBibNo($athleteId) {
        $bibNo = 0;
        if (array_key_exists($athleteId, $this->currentAllocatedBibs)) {
            $bibNo = $this->currentAllocatedBibs[$athleteId]->bibNo;
        }
        return $bibNo;
    }

    public function getBibNumbers() {
        return $this->_getCurrentBibs();
    }

    private function _getLastNo() {
        return $this->lastBibNo;
    }

}