<?php

class seedingV2Class {
    public $eventGroupId;
    public $compObj;
    public $seedings;
    public $entries;
    public $entryCountObj;
    public $egAgeInfo;
    public $eventCountCache;
    public $socket;
    public function __construct($compId, $eventGroupId) {
        $this->eventGroupId = (int)$eventGroupId;
        if ($compId === 0 or is_null($compId)) {
            $sql = 'select compId
                  from ' . E4S_TABLE_EVENTGROUPS . '
                  where id = ' . $eventGroupId;
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows !== 1) {
                Entry4UIError(7022, 'Unable to find Event Group Competition:' . $eventGroupId);
            }
            $row = $result->fetch_object();
            $compId = $row->compId;
        }
        $this->compObj = e4s_GetCompObj($compId);
	    $this->socket = new socketClass($compId);
    }

    public static function css() {
        $css = '
        .e4sSpinnerValue {
            width: 45px;
            font-size:1.1em !important;
        }
       
        .dataSHRow {
            padding-top: 35px;
        }
        .dataSH {
            margin-bottom: 12px;
            position: relative;
            left: -7px;
        }
        .fieldSeedHelp {
            font-size: 12px;
            display: none;
        }
        .seedAthleteSH {
            width: 45px;
            font-size:0.8em !important;
        }
        .seedFieldSH {
            width: 45px;
            font-size:0.8em !important;
        }
        .card_menu_seed {
            padding: 1px;
            float: right;
        }
        .noSeedBib {
            width:50px;
        }
        .noSeedAthlete {
            width:300px;
        }
        .noSeedPb {
            width:100px;
        }
        .noSeedAge {
            width:100px;
        }
        .noSeedClub {
            width:300px;
        }
        .noSeedHead{
            text-align: left;
        }
        .seedFieldPosition {
            vertical-align: text-top;
            padding-top: 33px;
            line-height: 35px;
        }
        .seedFeedDrag {
            width: 26px;
        }
        .seedFieldPos {
            width:50px;
        }
        .seedFieldBib {
            width:50px;
        }
        .seedFieldStartHeight{            
            width: 53px;
        }
        .seedFieldStartHeightData{
            top: -5px;
            position: relative;
            padding-left: 10px;
        }
        .seedFieldPresent {
            width: 20px;
        }
        .seedFieldAthlete {
            width:310px;
            max-width:310px;
        }
        .seedFieldAthleteAndClub {
            width: 107%;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
            position:relative !important;
//            margin-left:20px !important;
        }
        .seedFieldName {
            position: relative !important;
            margin-left: 0px !important;
        }
        .seedFieldClub{
            font-size: 0.7rem;
            padding-left: 25px;
            padding-top: 4px;
            position:relative !important;
            margin-left:-20px !important;
        }
        .seedFieldPB {
            width:40px;
            text-align: right;
        }
        .seedNotCheckedIn{
            color: red;
        }
        .seedCheckedIn{
            color: black;
            font-weight:bolder;
        }
        .cardNotPresent{
            color: red;
        }
        .cardPresent{
            color: black;
        }
        ';
        return $css;

    }

    public static function javascript() {
        ?>
        <script>
            function isEventSeeded(entries) {
                let seeded = true;
                if (typeof entries === "undefined") {
                    entries = e4sGlobal.currentEntries;
                    if (entries.length === 0) {
                        seeded = false;
                    }
                }
                for (e in entries) {
                    if (entries[e].heatInfo.seededPosition === 9999) {
                        seeded = false;
                    }
                    break;
                }
                return seeded;
            }

            function fieldSeedOptions() {
                fieldSeedOptionsDialog();
            }

            function getSeedingOptionAthlete() {
                return "athlete";
            }

            function getSeedingOptionClub() {
                return "club";
            }

            function getSeedingOptionRandom() {
                return "random";
            }

            function getSeedingOptionPerformance() {
                return "perf";
            }

            function fieldSeedOptionsDialog() {
                var html = "";
                html += '<input type="radio" id="seedingOption" name="seedingOption" value="' + getSeedingOptionAthlete() + '">Athlete Name<br>';
                html += '<input type="radio" id="seedingOption" name="seedingOption" value="' + getSeedingOptionClub() + '">Club<br>';
                html += '<input type="radio" id="seedingOption" name="seedingOption" checked value="' + getSeedingOptionPerformance() + '">Performance<br>';
                html += '<input type="radio" id="seedingOption" name="seedingOption" value="' + getSeedingOptionRandom() + '">Random<br>';
                html += '<br><br>';
                html += '<input type="checkbox" id="seedingOptionPresent" name="seedingOptionPresent" value="1">Only Present Athletes<br>';
                html += '<input type="checkbox" id="seedingOptionRev" name="seedingOptionRev" value="desc">Reverse Order';

                let seedingOptionsDiv = $("#seedingOptionsDiv");
                seedingOptionsDiv.html(html);
                seedingOptionsDiv.dialog({
                    title: "Seeding Options",
                    resizable: false,
                    height: 350,
                    width: 300,
                    modal: true
                });
                seedingOptionsDiv.dialog("option", "buttons", {
                    "Save": function () {
                        let option = {};
                        option.type = $("#seedingOption:checked").val();
                        option.order = $("#seedingOptionRev:checked").val();
                        option.presentOnly = $("#seedingOptionPresent:checked").val();
                        displayFieldSeedingDialog(option);
                        $(this).dialog("close");
                    },

                    Cancel: function () {
                        $(this).dialog("close");
                    }
                });
            }

            function displayFieldSeedingHelp(show) {
                var helpDiv = $(".fieldSeedHelp");
                if (typeof show === "undefined") {
                    helpDiv.toggle();
                } else {
                    if (show) {
                        helpDiv.show();
                    } else {
                        helpDiv.hide();
                    }
                }
            }

            function getAthletesInPositionOrderForEvent(option) {
                var entries = getCurrentEntries();
                var entryArr = [];
                if (typeof option !== "undefined") {
                    entries = getEntriesInChosenOrder(entries, option);
                }

                for (var e in entries) {
                    var entry = entries[e];
                    var position = entry.heatInfo.position;
                    if (typeof entry.heatInfo.tempPosition !== "undefined") {
                        position = entry.heatInfo.tempPosition;
                        delete entry.heatInfo.tempPosition;
                    }
                    entryArr[position] = entry;
                }

                return entryArr;
            }

            function getEntriesInChosenOrder(entries, optionObj) {
                var option = optionObj.type;
                var order = optionObj.order;
                var presentOnly = optionObj.presentOnly;
                var sortArray = [];
                for (var e in entries) {
                    var entry = entries[e];
                    var sortValue = "";
                    var athlete = entry.athlete.name.split(" ");
                    var athleteBySurname = athlete[1] + athlete[0];
                    athlete = entry.athlete.name;
                    switch (option) {
                        case getSeedingOptionAthlete():
                            sortValue = athleteBySurname;
                            break;
                        case getSeedingOptionClub():
                            sortValue = entry.athlete.club + athleteBySurname;
                            break;
                        case getSeedingOptionRandom():
                            sortValue = Math.random();
                            break;
                        case getSeedingOptionPerformance():
                            sortValue = entry.entry.pb;
                            break;
                    }

                    sortArray.push(sortValue + ":" + e);
                }
                sortArray.sort();
                if (order === "desc") {
                    sortArray.reverse();
                }
                var returnArr = [];
                for (var pos = 0; pos < sortArray.length; pos++) {
                    var sortedElement = sortArray[pos];
                    var entryid = sortedElement.split(":")[1];
                    var usePos = pos;
                    if (presentOnly === "1") {
                        if (!entries[entryid].entry.present) {
                            usePos = usePos + 1000;
                        }
                    }
                    returnArr[usePos] = entries[entryid];
                    returnArr[usePos].heatInfo.tempPosition = usePos + 1;
                }
                if (presentOnly === "1") {
                    // reset positions
                    var returnArrCorrected = [];
                    var newpos = 1;
                    for (var ra in returnArr) {
                        returnArrCorrected[newpos] = returnArr[ra];
                        returnArrCorrected[newpos].heatInfo.tempPosition = newpos;
                        newpos++;
                    }
                    returnArr = returnArrCorrected;
                }
                return returnArr;
            }

            function showSeedingDialog() {
                var eventTf = getEventTF(0);
                switch (eventTf) {
                    case '<?php echo E4S_EVENT_TRACK ?>':
                        // displayTrackSeedingDialog();
                        displayFieldSeedingDialog();
                        break;
                    case '<?php echo E4S_EVENT_FIELD ?>':
                        displayFieldSeedingDialog();
                        break;
                }
            }

            function checkIfSeeded(eventGroup) {
                return eventGroup.seeded;
            }

            function displayNonSeededEventEntries() {
                var entries = getCurrentEntries();
                var html = "";
                if (entries.length === 0) {
                    html = "<br>No entries as yet.<br><br>";
                } else {
                    html = "<br>This event is awaiting seeding. Entries so far are:<br><br>";
                }
                html += "<table>";
                html += "<tr>";
                html += "<th class='noSeedBib noSeedHead'>Bib No</th class='noSeedBib'>";
                html += "<th class='noSeedAthlete noSeedHead'>Athlete</th>";
                html += "<th class='noSeedPb noSeedHead'>PB</th>";
                html += "<th class='noSeedAge noSeedHead'>Age Group</th>";
                html += "<th class='noSeedClub noSeedHead'>Club</th>";
                html += "</tr>";

                for (var e in entries) {
                    html += "<tr>";
                    html += "<td>" + entries[e].entry.bibNo + "</td>";
                    html += "<td>" + entries[e].athlete.name + "</td>";
                    var pb = entries[e].entry.pb;
                    if (pb === 9999) {
                        pb = 0;
                    }
                    html += "<td>" + pb + "</td>";
                    html += "<td>" + entries[e].ageGroup.ageGroup + "</td>";
                    html += "<td>" + entries[e].athlete.club + "</td>";
                    html += "</tr>";
                }
                html += "</table>";
                $("#<?php echo R4S_CARD_BODY ?>").html(html);
                hideCardFooter();
            }

            function updateStartingHeight(athleteId) {
                var curEntry = getCurEntry(athleteId);
                if (curEntry === false) {
                    alert("Error : Can not get athlete");
                }
                curEntry.entry.startHeight = parseFloat($("#seed_sh_" + athleteId).val());
                var url = "https://<?php echo E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH ?>/entry/startheight/" + curEntry.entry.id + "/" + curEntry.entry.startHeight;
                $.post(url);
                sendSocketMsg("<?php echo R4S_SOCKET_FIELD_STARTHEIGHT ?>", curEntry);
            }

            function setStartHeight(event, ui) {
                let entries = getCurrentEntries();
                for (let e in entries) {
                    let entry = entries[e];
                    if (entry.entry.startHeight < ui.value) {
                        entry.entry.startHeight = ui.value;
                        $("#seed_sh_" + entry.athlete.id).val(ui.value);
                    }
                }
            }

            function updateSeedingIncrement(event, ui) {
                let val = parseInt(ui.value);
                val = val / 100;
                $(".seedFieldSH").spinner('option', 'step', val);
            }

            function addFieldEntryToSeedingList(entry, usingCheckIn, isHeight) {
                var athlete = entry.athlete;
                html = "";
                html += '<li class="draggable ui-state-default" id=' + entry.eventGroup.id + '-' + athlete.id + '><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>';
                var checkedIn = "seedCheckedIn";
                if (!entry.entry.checkedIn && usingCheckIn) {
                    checkedIn = "seedNotCheckedIn";
                }
                html += '<table><tr style="vertical-align: top;">';
                html += '<td class="seedFieldBib ' + checkedIn + '">' + getBibNo(entry.entry.bibNo, entry.entry) + '</td>';
                html += '<td class="seedFieldPresent">';
                html += '<input type="checkbox" id="' + getResultPresent() + entry.athlete.id + '" name="' + getResultPresent() + entry.athlete.id + '" value="1"';
                if (entry.entry.present) {
                    html += ' checked ';
                }
                html += ' onchange="issueAthletePresentUpdate(' + entry.athlete.id + ');"';
                html += '>';
                html += '</td>';
                html += '<td class="seedFieldAthlete">';
                html += '<p class="seedFieldAthleteAndClub">';
                html += ' <span class="seedFieldName">' + athlete.name + '</span>';
                html += ' <span class="seedFieldClub">' + athlete.club + '</span>';
                html += '</p>';
                html += '</td>';

                var pb = entry.entry.pb;
                if (pb === 9999) {
                    pb = 0;
                }
                html += '<td class="seedFieldPB">' + pb + '</td>';
                if (isHeight) {
                    html += '<td class="seedFieldStartHeight seedFieldStartHeightData">';
                    html += '<input class="seedAthleteSH" readonly id="seed_sh_' + entry.athlete.id + '" value="' + getDecimals(entry.entry.startHeight, 2) + '" onchange="updateStartingHeight(' + entry.athlete.id + ');">';
                    html += '</td>';
                }
                html += '</tr></table>';
                html += '</li>';
                return html;
            }

            function showSeedingPositions(entryCount) {
                var html = "";
                var split = "";
                for (var e = 1; e < entryCount; e++) {
                    html += split + e;
                    split = "<br>";
                }
                $("#seedFieldPositions").html(html);
            }

            function isEventUsingCheckin(entries) {
                var checkedIn = 0;
                var notCheckedIn = 0;
                for (var e in entries) {
                    if (entries[e].entry.checkedIn) {
                        checkedIn++;
                    } else {
                        notCheckedIn++;
                    }
                }
                if (checkedIn === 0 || notCheckedIn === 0) {
                    return false;
                }
                return true;
            }

            function displayTrackSeedingDialog() {

            }

            function e4sSpinnerProcessing(obj, ui) {
                var pos = obj.attr("sh");
                var dir = obj.attr("shd");
                if (dir === "n") {
                    $("#seed_sh_3145").spinner("stepUp", 1);
                } else {
                    $("#seed_sh_3145").spinner("stepDown", 1);
                }
            }

            function displayFieldSeedingDialog(option) {
                var entriesInPosOrder = getAthletesInPositionOrderForEvent(option);
                var currentEvent = getCurrentEvent();
                var isHeight = currentEvent.type === '<?php echo E4S_UOM_HEIGHT ?>';
                showSeedingPositions(entriesInPosOrder.length);
                var usingCheckIn = isEventUsingCheckin(entriesInPosOrder);
                var html = '';
                var shHTML = "";
                var incrementHTML = "";
                let min = 1;
                let max = 3;
                let step = 5;
                if (isHeight) {
                    min = 1.5;
                    max = 7;
                    step = 10;
                }
                html += '<style>';
                html += '#sortableList { list-style-type: none; margin: 0; padding: 0; width: 60%; }';
                html += '#sortableList li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; width:500; height: 18px; }';
                html += '#sortableList li span { position: absolute; margin-left: -1.3em; }';
                html += '</style>';
                if (isHeight) {
                    shHTML = '<span>Starting Height (m): </span><input readonly class="seedFieldSH" id="seed_sh" value="' + getDecimals(min, 2) + '">';
                    incrementHTML = '<span>Increments (cm): </span><input readonly class="heightIncrement" id="seed_increment" value="' + step + '">';
                }
                html += '<table>';
                html += '<tr>';
                html += '<td class="seedFeedDrag">&nbsp;</td>';
                html += '<td class="seedFieldBib">Bib</td>';
                html += '<td class="seedFieldPresent"> </td>';
                html += '<td class="seedFieldAthlete">Athlete</td>';
                html += '<td class="seedFieldPB">Perf</td>';
                if (isHeight) {
                    html += '<td class="seedFieldStartHeight">SH</td>';
                }
                html += '</tr>';
                html += '</table>';
                html += '<ul id="sortableList" class="grid">';
                for (var e = 1; e < entriesInPosOrder.length; e++) {
                    html += addFieldEntryToSeedingList(entriesInPosOrder[e], usingCheckIn, isHeight);
                }
                html += "</ul>";

                let parentSeedingsDiv = $("#seedingsDiv");

                let seedingsDiv = $("#seedings");
                seedingsDiv.html(html);
                if (isHeight) {
                    $("#seedStartingHeightDiv").show();
                    let heightSH = $("#seedFieldSH");
                    heightSH.html(shHTML);
                    heightSH = $("#seedFieldIncrement");
                    heightSH.html(incrementHTML);

                    $(".seedFieldSH").spinner({
                        step: (step / 100),
                        numberFormat: "n",
                        min: min,
                        max: max,
                        spin: function (event, ui) {
                            setStartHeight(event, ui);
                        }
                    });

                    $("#seed_increment").spinner({
                        max: 20,
                        min: 2,
                        spin: function (event, ui) {
                            updateSeedingIncrement(event, ui);
                        }
                    });
                    $(".seedAthleteSH").on("click", function (ui) {
                        var eventSH = parseFloat($("#seed_sh").val());
                        var eventInc = parseFloat($("#seed_increment").val() / 100);
                        var curField = ui.currentTarget.id;
                        var athleteId = curField.replace("seed_sh_", "");
                        var curEntry = getCurEntry(athleteId);
                        var obj = {};
                        obj.label = "Starting Height";
                        obj.title = curEntry.athlete.name;
                        obj.srcField = "seed_sh_" + athleteId;
                        obj.value = parseFloat($("#" + obj.srcField).val());
                        obj.min = eventSH;
                        obj.max = eventSH * 3;
                        obj.inc = eventInc;
                        enterSpinnerValue.display(obj);
                    });
                } else {
                    $("#seedStartingHeightDiv").hide();
                }
                $('#sortableList').sortable();
                $('#sortableList').sortable()
                    .on('sortable:start', function (e, ui) {
                        // debugger;
                    });
                $('#sortableList').sortable()
                    .on('sortable:stop', function (e, ui) {
                        debugger;
                    });
                parentSeedingsDiv.show();
                parentSeedingsDiv.dialog({
                    title: "Seeding for " + entriesInPosOrder[1].eventGroup.groupName,
                    resizable: true,
                    height: 600,
                    width: 600,
                    modal: true
                });
                parentSeedingsDiv.dialog("option", "buttons", {
                    "Help": function () {
                        displayFieldSeedingHelp();
                    },
                    "Seed": function () {
                        fieldSeedOptions();
                    },
                    "Save Changes": function () {
                        displayFieldSeedingHelp(false);
                        updateSeedings(entriesInPosOrder);
                        $(this).dialog("close");
                    },
                    Cancel: function () {
                        displayFieldSeedingHelp(false);
                        $(this).dialog("close");
                    }
                });
            }

            function issueAthletePresentUpdate(athleteId) {
                var curEntry = getCurEntry(athleteId);
                if (curEntry === false) {
                    alert("Error : Can not get athlete");
                }
                curEntry.entry.present = !curEntry.entry.present;
                var url = "https://<?php echo E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH ?>/entry/present/" + curEntry.entry.present + "/" + curEntry.entry.id;
                $.post(url);
                sendSocketMsg("<?php echo R4S_SOCKET_ATHLETE_PRESENT ?>", curEntry);
            }

            function sendFieldSeeding(data) {
                sendSocketMsg("<?php echo R4S_SOCKET_FIELD_SEEDINGS ?>", data);
            }

            function updateSeedings(entries) {
                let sortKeys = $("#sortableList").sortable("toArray", {attribute: 'id'});
                let egId = entries[1].eventGroup.id;
                let url = "https://<?php echo E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH . '/' ?>seeding/write/" + egId;

                let data = {};
                data.data = sortKeys;
                postData(url, data, sendFieldSeeding);
            }

            function inboundFieldSeeding(payload) {
                if (!isEventSeeded()) {
                    cardChange();
                    return;
                }
                for (var p = 0; p < payload.length; p++) {
                    var athleteId = payload[p].split("-")[1];
                    var curEntry = getCurEntry(athleteId);
                    if (curEntry === false) {
                        alert("Error : Can not get athlete");
                    }
                    curEntry.heatInfo.position = p + 1;
                }
                loadFieldEntries();
            }

            var enterSpinnerValue = {
                dialog: function (obj) {
                    let useObj = {};
                    useObj.label = "Starting Height";
                    useObj.title = "Data Entry";
                    useObj.value = 1.00;
                    useObj.min = 1.00;
                    useObj.max = 3.00;
                    useObj.inc = 5;
                    useObj.width = 200;
                    useObj.height = 150;
                    useObj.srcField = "seed_sh_";
                    useObj = {...useObj, ...obj};
                    var html = "";
                    html += '<div id="spinnerEntry">';
                    html += '<label for="e4sSpinnerValue">' + useObj.label + " : </label>";
                    html += '<input id="e4sSpinnerValue" class="e4sSpinnerValue" value="' + useObj.value + '">';
                    html += '</div>';
                    $("#spinnerEntry").remove();
                    $(html).dialog({
                        width: useObj.width,
                        height: useObj.height,
                        modal: true,
                        title: useObj.title
                    });

                    $("#e4sSpinnerValue").spinner({
                        step: useObj.inc,
                        numberFormat: "n",
                        min: useObj.min,
                        max: useObj.max,
                        spin: function (event, ui) {
                            $("#" + useObj.srcField).val(getDecimals(ui.value, 2));
                        }
                    });
                },
                display: function (obj) {
                    enterSpinnerValue.dialog(obj);
                }
            }
        </script>
        <?php
    }

    public static function div() {
        ?>
        <div id='seedingOptionsDiv'></div>
        <div id="seedingsDiv" style="display:none;">
            <span class="fieldSeedHelp">
            Below are the athletes in this event. They are ( default ) shown in order of PB.
            Simply drag the athlete rows to the choosen position. The checkbox indicates that the athlete is present for the event. If the competition makes use of checkin, Bib numbers are <span
                        class="seedNotCheckedIn">Red</span> for those that have not checked in.
            </span>
            <table style="width:100%" id="seedStartingHeightDiv">
                <tr>
                    <td id="seedFieldSH" style="width:50%"></td>
                    <td id="seedFieldIncrement" style="width:50%"></td>
                </tr>
            </table>
            <table style="width:100%">
                <tr>
                    <td id="seedFieldPositions" class="seedFieldPosition"></td>
                    <td>
                        <div id="seedings"></div>
                    </td>
                    <!--                    <td id="rowSh" class="dataSHRow">-->
                    <!--                        <div class="dataSH" >-->
                    <!--                            <a sh="1" shd="n" e4sspinner=true><span class="ui-button-icon ui-icon ui-icon-triangle-1-n"></span></a>-->
                    <!--                            <a sh="1" shd="s" e4sspinner=true><span class="ui-button-icon ui-icon ui-icon-triangle-1-s"></span></a>-->
                    <!--                        </div>-->
                    <!--                        <div class="dataSH" >-->
                    <!--                            <a sh="2" shd="n" e4sspinner=true><span class="ui-button-icon ui-icon ui-icon-triangle-1-n"></span></a>-->
                    <!--                            <a sh="2" shd="s" e4sspinner=true><span class="ui-button-icon ui-icon ui-icon-triangle-1-s"></span></a>-->
                    <!--                        </div>-->
                    <!--                        <div class="dataSH" >-->
                    <!--                            <a><span class="ui-button-icon ui-icon ui-icon-triangle-1-n"></span></a>-->
                    <!--                            <a><span class="ui-button-icon ui-icon ui-icon-triangle-1-s"></span></a>-->
                    <!--                        </div>-->
                    <!--                        <div class="dataSH" >-->
                    <!--                            <a><span class="ui-button-icon ui-icon ui-icon-triangle-1-n"></span></a>-->
                    <!--                            <a><span class="ui-button-icon ui-icon ui-icon-triangle-1-s"></span></a>-->
                    <!--                        </div>-->
                    <!--                        <div class="dataSH" >-->
                    <!--                            <a><span class="ui-button-icon ui-icon ui-icon-triangle-1-n"></span></a>-->
                    <!--                            <a><span class="ui-button-icon ui-icon ui-icon-triangle-1-s"></span></a>-->
                    <!--                        </div>-->
                    <!--                    </td>-->
                </tr>
            </table>
        </div>
        <?php
    }

    public function getSeedings($clear = TRUE) {
        if ($clear) {
            $this->clear();
        }
        $obj = $this->_getEntriesForEg(TRUE);
        /*
         * $obj
         *  ->entries
         *  ->ceObjs
         *  ->egId
         *  ->egOptions
         */
        unset($obj->egId);
        unset($obj->egOptions);
        if ($clear) {
            $athleteIds = array();
            if (sizeof($obj->entries) > 0) {
                foreach ($obj->entries as $entry) {
                    $athleteIds[] = $entry['athleteId'];
                }
                $this->insertNonSeededAthletes($athleteIds);
            }
        }
        return $obj;
    }

    public function clear($uniqueEgIds = null) {
        $sql = '
            delete from ' . E4S_TABLE_SEEDING . '
            where eventgroupid ';
        if (is_null($uniqueEgIds)) {
            $sql .= '= ' . $this->eventGroupId;
        } else {
            $sql .= ' in (' . implode(',', $uniqueEgIds) . ')';
        }
        e4s_queryNoLog($sql);
    }

    private function _getEntriesForEg($format = null) {
        $sql = 'select   e.id entryId
                        ,eg.id egId
                        ,eg.options egOptions
                        ,ce.options ceOptions
                        ,ce.id ceId
                        ,e.athleteId
                        ,a.gender
                        ,e.checkedIn
                        ,ag.id ageGroupId
                        ,ag.minAge
                from ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_EVENTGROUPS . ' eg,
                     ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_ATHLETE . ' a,
                     ' . E4S_TABLE_AGEGROUPS . ' ag
                where e.compeventid = ce.id
                and e.athleteid = a.id
                and ce.maxgroup = eg.id
                and eg.id = ' . $this->eventGroupId . '
                and ce.agegroupid = ag.id
                and e.paid = ' . E4S_ENTRY_PAID;
        $result = e4s_queryNoLog($sql);
        $entries = $result->fetch_all(MYSQLI_ASSOC);
        $this->egAgeInfo = array();

        if (is_null($format)) {
            return $this->_flatEntries($entries);
        } else {
            return $this->_objEntries($entries);
        }
    }

    private function _flatEntries($entries) {
        $entryRows = array();

        foreach ($entries as $row) {
            $row['athleteId'] = (int)$row['athleteId'];
            $waitingPos = 0;
            if (!is_null($this->entryCountObj)) {
                $waitingPos = $this->entryCountObj->isAthleteOnWaitingListForEG($row['athleteId'], $row['egId']);
            }
            if ($waitingPos === 0) {
                $row['entryId'] = (int)$row['entryId'];
                $row['egId'] = (int)$row['egId'];
                $row['ageGroupId'] = (int)$row['ageGroupId'];
                $row['minAge'] = (int)$row['minAge'];
                $row['checkedIn'] = FALSE;
                if ((int)$row['checkedIn'] === 1) {
                    $row['checkedIn'] = TRUE;
                }
                $row['egOptions'] = e4s_addDefaultEventGroupV2Options($row['egOptions']);

                $entryRows[] = $row;
                $this->_setEventGroupAgeInfo($row);
            }
        }
        return $entryRows;
    }

    private function _setEventGroupAgeInfo($row): void {
        $egId = $row['egId'];
        $agId = $row['ageGroupId'];
        $minAge = $row['minAge'];

        if (!array_key_exists($egId, $this->egAgeInfo)) {
            $this->egAgeInfo[$egId] = new stdClass();
            $this->egAgeInfo[$egId]->ageOnly = array();
        }
        $this->egAgeInfo[$egId]->ageOnly[$agId] = $minAge;
    }

    private function _objEntries($entries) {
        $entryRows = array();
        $egOptions = '';
        $ceObjs = array();
        foreach ($entries as $row) {
            $row['athleteId'] = (int)$row['athleteId'];
            $waitingPos = 0;
            if (!is_null($this->entryCountObj)) {
                $waitingPos = $this->entryCountObj->isAthleteOnWaitingListForEG($row['athleteId'], $row['egId']);
            }
            if ($waitingPos === 0) {
                $row['entryId'] = (int)$row['entryId'];
                $row['egId'] = (int)$row['egId'];
                $row['ceId'] = (int)$row['ceId'];
                $row['ageGroupId'] = (int)$row['ageGroupId'];
                $row['minAge'] = (int)$row['minAge'];
                $row['checkedIn'] = FALSE;
                if ((int)$row['checkedIn'] === 1) {
                    $row['checkedIn'] = TRUE;
                }
                if (!array_key_exists($row['ceId'], $ceObjs)) {
                    $ceObjs[$row['ceId']] = e4s_addDefaultCompEventOptions($row['ceOptions']);
                }
                unset($row['ceOptions']);

                if ($egOptions === '') {
                    $egOptions = e4s_addDefaultEventGroupV2Options($row['egOptions']);
                }
                unset($row['egOptions']);

                $entryRows[] = $row;
                $this->_setEventGroupAgeInfo($row); // Do we need this line ?
            }
        }
        $retObj = new stdClass();
        $retObj->entries = $entryRows;
        $retObj->egId = $this->eventGroupId;
        $retObj->egOptions = $egOptions;
        $retObj->ceObjs = $ceObjs;
        return $retObj;
    }

    public function insertNonSeededAthletes($athleteIds) {
        if (sizeof($athleteIds) < 1) {
            return;
        }
        $sql = 'insert into ' . E4S_TABLE_SEEDING . ' (eventgroupid, athleteid, heatno,laneno,heatnocheckedin, lanenocheckedin)
                values ';
        $sep = '';
        foreach ($athleteIds as $athleteId) {
            $sql .= $sep . '(
                    ' . $this->eventGroupId . ',
                    ' . $athleteId . ',
                    0,
                    0,
                    0,
                    0
                )';
            $sep = ',';
        }
        if ($sep !== '') {
            e4s_queryNoLog($sql);
        }
    }

    public function updateSeedingsFromDragDrop($seedingInfo) {
        $this->updateHeatLaneNoForEg($seedingInfo);
    }

    public function updateHeatLaneNoForEg($entryObjs) {
        $sql = "delete from " . E4S_TABLE_SEEDING . " where eventgroupid = " . $this->eventGroupId;
        $entityIds = [];
	    foreach ($entryObjs as $entryObj) {
		    if ( array_key_exists('athleteId', $entryObj) and !is_null($entryObj['athleteId']) and $entryObj['athleteId'] !== '') {
			    $entityId = $entryObj['athleteId'];
		    }else{
			    $entityId = $entryObj['teamId'];
		    }
            $entityIds[] = $entityId;
	    }
        $sql .= ' and athleteid in (' . implode(',', $entityIds) . ')';
        e4s_queryNoLog($sql);

        $sql= "insert into " . E4S_TABLE_SEEDING . " (eventgroupid, athleteid, pos, heatno, laneno, heatnocheckedin, lanenocheckedin) values ";
        $pos = 1;
        $sep = '';
	    foreach ($entryObjs as $entryObj) {
            $heatInfo = $entryObj['heatInfo'];
            if ( array_key_exists('athleteId', $entryObj) and !is_null($entryObj['athleteId']) and $entryObj['athleteId'] !== '') {
	            $entityId = $entryObj['athleteId'];
            }else{
	            $entityId = $entryObj['teamId'];
            }
            $sql .= $sep . "(" . $this->eventGroupId . ", " . $entityId . ", " . $pos++ . ','. $heatInfo['heatNo'] . ", " . $heatInfo['laneNo'] . ", " . $heatInfo['heatNoCheckedIn'] . ", " . $heatInfo['laneNoCheckedIn'] . ")";
            $sep = ",";
	    }
        if ($pos > 1 ) {
            e4s_queryNoLog($sql);
        }
//        foreach ($entryObjs as $entryObj) {
//            $heatInfo = $entryObj['heatInfo'];
//            $entityId = $entryObj['athleteId'];
//            if ( is_null($entityId ) ){
//	            $entityId = $entryObj['teamId'];
//            }
//            $sql = 'update ' . E4S_TABLE_SEEDING . '
//                    set heatno = ' . $heatInfo['heatNo'] . ',
//                        heatnocheckedin = ' . $heatInfo['heatNoCheckedIn'] . ',
//                        laneno = ' . $heatInfo['laneNo'] . ',
//                        lanenocheckedin = ' . $heatInfo['laneNoCheckedIn'] . '
//                    where eventgroupid = ' . $this->eventGroupId . '
//                    and   athleteid = ' . $entityId;
//            e4s_queryNoLog($sql);
//        }
    }

    public function seedEventGroup() {
        $checkWaitingList = $this->_compHasWaitingEnabled();
        if ($checkWaitingList) {
            $this->entryCountObj = e4s_getEntryCountObj($this->compObj->getID());
        }
        $this->eventCountCache = array();
        $entries = $this->_getEntriesForEg();
        foreach ($entries as $entry) {
            $this->_writeEntrySeeding($entry);
        }
    }

// called from checkin class when athlete has checked in to a closed event
    private function _compHasWaitingEnabled() {
        return $this->compObj->isWaitingListEnabled(null, TRUE);
    }

    private function _writeEntrySeeding($row) {
        $this->_getHeatLaneInfo($row, FALSE);
        if ($this->compObj->isCheckinEnabled()) {
            $this->_getHeatLaneInfo($row, TRUE);
        }

        $heatInfo = $row['heatInfo'];
        $seedRow = new stdClass();
        $seedRow->eventGroupId = $row['eventGroupId'];
        $seedRow->athleteId = $row['athleteid'];
        $seedRow->laneNo = $heatInfo->laneNo;
        $seedRow->heatNo = $heatInfo->heatNo;
        $seedRow->laneNoCheckedIn = $heatInfo->laneNoCheckedIn;
        $seedRow->heatNoCheckedIn = $heatInfo->heatNoCheckedIn;
        $seedRow->confirmed = $heatInfo->confirmed;
        return $seedRow;
    }

    private function _getHeatLaneInfo($entryRow, $useCheckedIn) {
        $entryCountObj = $this->entryCountObj;
        $athleteId = $entryRow['athleteId'];
        $egId = $entryRow['egId'];
        $ageGroupId = $entryRow['ageGroupId'];
        $egOptions = $entryRow['egOptions'];
        $egOptions = e4s_addDefaultEventGroupOptions($egOptions);
        $ceOptions = $entryRow['ceOptions'];
        $ceOptions = e4s_addDefaultCompEventOptions($ceOptions);
        $gender = $entryRow['gender'];
        $heatInfo = $egOptions->heatInfo;
        $obj = new stdClass();
        $obj->heatNo = 0;
        $obj->laneNo = 0;
        $obj->position = 0;
        $obj->confirmed = null;
        $obj->maxInHeat = eventGroup::getMaxInHeat($egOptions);
        $obj->laneCount = 0;
        $obj->useLanes = $ceOptions->heatInfo->useLanes;
        $obj->firstLane = $egOptions->seed->firstLane;

        $seedGender = '';
        $cacheGender = '';
        $seedAgeGroupId = 0;
        $cacheAgeGroup = '';
        $seedType = $egOptions->seed->type;
        $heatCacheType = '';

        if ($egOptions->seed->gender) {
            $seedGender = $gender;
            $cacheGender = '_' . E4S_HEATCACHE_GENDER . $seedGender;
            $heatCacheType = E4S_HEATCACHE_GENDER;
        }

        if ($egOptions->seed->age) {
            $seedAgeGroupId = $ageGroupId;
            $cacheAgeGroup = '_' . E4S_HEATCACHE_AGE . $seedAgeGroupId;
            if ($heatCacheType === '') {
                $heatCacheType = E4S_HEATCACHE_AGE;
            } else {
                $heatCacheType = E4S_HEATCACHE_BOTH;
            }
        }

        $cacheKey = 'EventCount_' . $egId . $cacheGender . $cacheAgeGroup . '_' . $useCheckedIn;

        // has the group been read before, if so simply return save doing sql statement
        if (!array_key_exists($cacheKey, $this->eventCountCache)) {
//        $writeSeedings = false;
            if ($cacheAgeGroup . $cacheGender !== '') {
                // at least one set
                $this->_setHeatCache();
            }
            // add to the cache a default
            $this->eventCountCache[$cacheKey] = array();

            $sql = $this->_getSortingSQL($useCheckedIn, TRUE, $seedGender, $seedAgeGroupId);
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows === 0) {
                return FALSE;
            }
            $rows = $result->fetch_all(MYSQLI_ASSOC);

            $processedRows = array();
            $maxInHeatCnt = 0;
            $position = 1;

            foreach ($rows as $row) {
                $row['athleteid'] = (int)$row['athleteId'];
                $entryOptions = e4s_addDefaultEntryOptions($row['entryOptions']);
                if (!e4s_isOTDEntry($entryOptions)) {
                    $waitingPos = $entryCountObj->isAthleteOnWaitingListForEG($row['athleteId'], $egId);
                    if ($waitingPos > 0) {
                        continue;
                    }
                }
                $row['eventid'] = (int)$row['eventId'];
                $row['rowPosition'] = $position++;
                $row['coptions'] = e4s_addDefaultCompOptions($row['coptions']);
                $row['ceoptions'] = e4s_addDefaultCompEventOptions($row['ceoptions']);
                $row['useLanes'] = E4S_ALL_LANES;
                $row['maxInHeat'] = 0;
                if ($maxInHeatCnt === 0) {
                    // first time in for this event
                    $coptions = $row['coptions'];

                    $maxInHeatCnt = e4s_getHeatCount($row);

                    if ($maxInHeatCnt < 0) {
                        // No Heat Cnt specified so no calcs can be done
                        return FALSE;
                    }

                    // validate if All/Odd/Even lanes is ok
                    // Should really be in egOptions
                    $useLanes = $row['ceoptions']->heatInfo->useLanes;
                    if ($useLanes !== E4S_ALL_LANES) {
                        $maxLaneCnt = e4s_getTrackLaneCount($coptions, $egOptions);
                        if ($maxLaneCnt & 1) {
                            // Odd lanes on track defined
                            if ($useLanes === E4S_EVEN_LANES) {
                                $availableLanes = floor($maxLaneCnt / 2);
                            } else {
                                $availableLanes = ceil($maxLaneCnt / 2);
                            }
                        } else {
                            $availableLanes = $maxLaneCnt / 2;
                        }
                        if ($availableLanes < $maxInHeatCnt) {
                            Entry4UIError(2020, "Event {$row['eventid']} has an issue. The track has {$availableLanes} available lanes, but the max athletes per heat is set to {$maxInHeatCnt}.", 200, '');
                        }
                    }
                }
                $row['useLanes'] = $useLanes;
                $row['maxInHeat'] = $maxInHeatCnt;
                unset($row['eoptions']);
                unset($row['egoptions']);

                $row['heatNoIncrement'] = e4s_getIncHeatCnt($heatCacheType, $egId, $row, $seedGender, $this->egAgeInfo);
                $processedRows[$row['athleteId']] = $row;
            }

            $this->eventCountCache[$cacheKey] = $processedRows;
        }

        $entries = $this->eventCountCache[$cacheKey];

        // -----
        if (!isset($entries[$athleteId])) {
            $obj->laneCount = 0; // Lanes count for the track
            $obj->maxInHeat = 0; // Maximum athletes per heat. laneCount if not set in CE
            $obj->useLanes = E4S_ALL_LANES;
            $obj->heatNo = 0; // Heat the athlete is in
            $obj->heatNoIncrement = null; // Heat increment
            $obj->laneNo = 0; // Lane the athlete is in
            $obj->position = 1; // Athletes overall position
            $obj->heatPosition = 1; // Athlete position in heat
            $obj->confirmed = null;
        } else {
            $entry = $entries[$athleteId];
            $numEntries = sizeof($entries);
            $obj->heatNoIncrement = $entry['heatNoIncrement']; // Heat increment object
            $obj->laneCount = e4s_getTrackLaneCount($entry['coptions'], $egOptions);
            $obj->maxInHeat = $entry['maxInHeat'];
            $obj->useLanes = $entry['useLanes'];
            $obj->confirmed = $entry['confirmed'];
            if ($seedType === E4S_SEED_OPEN) {
                $obj = e4s_openSeed($entry, $numEntries, $obj, $heatOrder, $egId);
            } else {
                $obj = e4s_heatSeed($entry, $numEntries, $obj, $heatOrder, $egId);
            }
        }

        if ($obj->laneCount > 0 and $obj->laneNo > 0 and $obj->firstLane > 1) {
            $firstLane = $obj->firstLane;
            $factor = 0;
            if ($obj->laneCount > $obj->maxInHeat) {
                $factor = $obj->maxInHeat - $obj->laneCount;
            }
            if ($obj->maxInHeat + $firstLane <= $obj->laneCount) {
                $obj->laneNo += $factor + ($firstLane - 1);
            }
        }
        $useHeatPosition = 0;
        $heatInfo->heatPositionCheckedIn = $useHeatPosition;
        $heatInfo->heatPosition = $useHeatPosition;
        if (isset($obj->heatPosition)) {
            $useHeatPosition = $obj->heatPosition;
        }

        $entriesInc = 0;
        $checkedInInc = 0;
        if (isset($obj->heatNoIncrement)) {
            $entriesInc = $obj->heatNoIncrement->entriesHeats;
            $checkedInInc = $obj->heatNoIncrement->checkedInHeats;
        }
        if ($useCheckedIn) {
//        $heatInfo->heatNoCheckedIn = $obj->heatNo ;
            $heatInfo->heatNoCheckedIn = $obj->heatNo + $checkedInInc;
            $heatInfo->laneNoCheckedIn = $obj->laneNo;
            $heatInfo->positionCheckedIn = $obj->position;
            $heatInfo->heatPositionCheckedIn = $useHeatPosition;
            if ($obj->laneCount < $obj->maxInHeat) {
                $heatInfo->arrowLaneno = $obj->laneNo;
                $heatInfo->laneNoCheckedIn = $useHeatPosition;
            }
        } else {
//        $heatInfo->heatNo = $obj->heatNo;
            $heatInfo->heatNo = $obj->heatNo + $entriesInc;
            $heatInfo->laneNo = $obj->laneNo;
            $heatInfo->position = $obj->position;
            $heatInfo->heatPosition = $useHeatPosition;
            if ($obj->laneCount < $obj->maxInHeat) {
                $heatInfo->arrowLaneno = $obj->laneNo;
                $heatInfo->laneNo = $useHeatPosition;
            }
        }
        $heatInfo->confirmed = $obj->confirmed;
        $heatInfo->useLanes = $obj->useLanes;
        $heatInfo->maxInHeat = $obj->maxInHeat;
        return TRUE;
    }

    private function _setHeatCache() {
        // read through $rows for the egId
        $cachedKey = 'e4s_cached_entries';
        if (!array_key_exists($cachedKey, $GLOBALS)) {
            $GLOBALS[$cachedKey] = array();
        }
        foreach ($this->entries as $row) {
            $entryId = (int)$row['entryId'];
            if (array_key_exists($entryId, $GLOBALS[$cachedKey])) {
                // entry has already been processed. How did that get in here though ?
                continue;
            }
            $GLOBALS[$cachedKey][$entryId] = TRUE;
            $checkedIn = (int)$row['checkedIn'];
            $egId = (int)$row['maxgroup'];

            $key = strtoupper($row['gender']);
            e4s_addHeatKeyCache(E4S_HEATCACHE_GENDER, $egId, $key, $checkedIn);

            $key = $row['ageGroupId'];
            e4s_addHeatKeyCache(E4S_HEATCACHE_AGE, $egId, $key, $checkedIn);

            $key = strtoupper($row['gender'] . $row['ageGroupId']);
            e4s_addHeatKeyCache(E4S_HEATCACHE_BOTH, $egId, $key, $checkedIn);
        }

        foreach ($this->egAgeInfo as $egId => $useAgeAndGender) {

            $femaleCheckedInRolling = 0;
            $femaleRolling = 0;
            $maleCheckedInRolling = 0;
            $maleRolling = 0;
            $ages = $useAgeAndGender->ageOnly;
            $ageIds = array();
            foreach ($ages as $ageId => $minAge) {
                $ageIds[$minAge] = $ageId;
            }

            usort($ages, 'sortAges');

            $heatCache = $GLOBALS[E4S_HEAT_CACHE . $egId . E4S_HEATCACHE_BOTH];

            foreach ($ages as $minAge) {
                $sortedAge = $ageIds[$minAge];
                $useKey = E4S_GENDER_FEMALE . $sortedAge;
                if (array_key_exists($useKey, $heatCache)) {
                    $heatCache[$useKey]->rollingCount = $femaleRolling;
                    $femaleRolling += $heatCache[$useKey]->count;
                }
                $useKey .= '_checkedin';
                if (array_key_exists($useKey, $heatCache)) {
                    $heatCache[$useKey]->rollingCount = $femaleCheckedInRolling;
                    $femaleCheckedInRolling += $heatCache[$useKey]->count;
                }
                $useKey = E4S_GENDER_MALE . $sortedAge;
                if (array_key_exists($useKey, $heatCache)) {
                    $heatCache[$useKey]->rollingCount = $maleRolling;
                    $maleRolling += $heatCache[$useKey]->count;
                }
                $useKey .= '_checkedin';
                if (array_key_exists($useKey, $heatCache)) {
                    $heatCache[$useKey]->rollingCount = $maleCheckedInRolling;
                    $maleCheckedInRolling += $heatCache[$useKey]->count;
                }
            }
        }
    }

    private function _getSortingSQL($useCheckedIn, $isTime = TRUE, $seedGender = '', $seedAgeGroupId = 0, $orderByGender = FALSE, $orderByAgeId = FALSE) {

        $sql = 'Select      
                s.laneNo laneNo,
                s.heatNo heatNo,
                s.laneNocheckedin laneNoCheckedIn,
                s.heatNocheckedin heatNoCheckedIn,
                s.confirmed confirmed,
                e.athleteId,
                a.dob,
                e.checkedIn checkedIn,
                ag.minage minAge,
                ev.gender gender,
                if(e.pb=0,9999,e.pb) as eventpb,
                IFNULL(if(pb.pb=0,9999,pb.pb),9999) as pb,
                ce.eventid eventId,
                ce.options ceoptions,
                ev.options eoptions,
                e.options entryOptions,
                c.options coptions,
                eg.options egoptions
                from ' . E4S_TABLE_ENTRIES . ' e left join ' . E4S_TABLE_COMPEVENTS . ' ce on ce.id = e.compeventid left join ' . E4S_TABLE_SEEDING . ' s on ce.maxgroup = s.eventgroupid and s.athleteid = e.athleteid left join ' . E4S_TABLE_ATHLETEPB . ' pb on ce.EventID = pb.eventid and pb.athleteid = e.athleteid left join ' . E4S_TABLE_AGEGROUPS . ' ag on ce.agegroupid = ag.id,
                     ' . E4S_TABLE_EVENTS . ' ev,
                     ' . E4S_TABLE_EVENTGROUPS . ' eg,
                     ' . E4S_TABLE_COMPETITON . ' c,
                     ' . E4S_TABLE_ATHLETE . ' a
                where paid = ' . E4S_ENTRY_PAID . '
                and waitingPos = 0
                and e.athleteid = a.id
                and ev.id = ce.eventid
                and c.id = ce.compid
                and eg.id = ' . $this->egAgeInfo->egId;
        if ($seedGender !== '') {
            $sql .= "       and ev.gender = '" . $seedGender . "'";
        }
        if ($seedAgeGroupId !== 0) {
            $sql .= '       and ce.agegroupid = ' . $seedAgeGroupId;
        }
        $sql .= '           and compeventid in (
                            select id from ' . E4S_TABLE_COMPEVENTS . '
                            where compid = ' . $this->compObj->getID() . "
                            and maxgroup = '" . $this->egAgeInfo->egId . "')";
        if ($useCheckedIn) {
            $sql .= ' and e.checkedin = 1 ';
        }
        $sql .= ' order by ce.maxgroup, ';
        if ($orderByGender) {
            $sql .= ' ev.gender,a.dob desc,';
        }
        if ($orderByAgeId) {
            $sql .= ' a.dob desc,ev.gender,';
        }
        $sql .= ' if(e.pb = 0, 9999, e.pb)';
        $heatOrder = $this->compObj->getHeatOrder();
        if ($heatOrder === E4S_SLOWEST_FIRST and $isTime) {
            $sql .= ' desc';
        }
        if ($heatOrder === E4S_FASTEST_FIRST and !$isTime) {
            $sql .= ' desc';
        }
        $sql .= ', athleteid, ev.gender';

        return $sql;
    }

    public function updateUnSeededEntries($egIds) {
        foreach ($egIds as $egId) {
            if (!$egId->checkedIn) {
                $sql = 'update ' . E4S_TABLE_SEEDING . '
                              set heatNoCheckedIn = 0,
                                  laneNoCheckedIn = 0
                          where eventgroupid = ' . $egId->egId . '
                          and   athleteid = ' . $egId->athleteId;
                e4s_queryNoLog($sql);
            }
        }
    }

    public function getAllSeedRecords() {
        $sql = 'select heatno heatNo,
                        laneno laneNo,
                        heatnocheckedin heatNoCheckedIn,
                        lanenocheckedin laneNoCheckedIn,
                        athleteid athleteId,
                        eventgroupid eventGroupId,
                        s.confirmed
                from ' . E4S_TABLE_SEEDING . ' s,
                     ' . E4S_TABLE_COMPEVENTS . ' ce
                where s.eventgroupid = ce.maxgroup
                and   ce.compid = ' . $this->compObj->getID() . '
                order by eventgroupid, heatno, laneno';
        $result = e4s_queryNoLog($sql);
        $records = array();
        while ($obj = $result->fetch_object()) {
            if (!array_key_exists($obj->eventGroupId, $records)) {
                $records[$obj->eventGroupId] = array();
            }

            $records[$obj->eventGroupId][$obj->athleteId] = $obj;
        }
        return $records;
    }

    public function moveTrackPosition($obj) {
        $checkedIn = '';
        if ($obj->checkedIn) {
            $checkedIn = 'checkedin';
        }
        $isTeam = false;
        $entityId = $obj->athleteId;

        if ( $obj->teamId !== 0 ){
	        $entityId = $obj->teamId;
            $isTeam = true;
        }
        $fromObj = $this->getSeedRecordForAthlete($entityId);

        // Qualifying entry seeded immediately
//        if ( is_null($fromObj) ){
//            Entry4UIError(8027,"Failed to read seeding for current position");
//        }
        $retObj = new stdClass();
        $retObj->checkedIn = $obj->checkedIn;
        $retObj->isTeam = $isTeam;
        $entrya = new stdClass();
        $entrya->entryId = $obj->fromEntryId;
        $entrya->heatNo = $obj->toHeatNo;
        $entrya->laneNo = $obj->toLaneNo;
        $entrya->present = $obj->present;
        $retObj->entryA = $entrya;

        $toObj = $this->getSeedRecord($obj->toHeatNo, $obj->toLaneNo, $checkedIn, $obj->switchedAthleteId);
        if (is_null($fromObj)) {
            $heatNo = 0;
            $laneNo = 0;
            $heatNoCheckedIn = 0;
            $laneNoCheckedIn = 0;
            if ($obj->checkedIn) {
                $heatNoCheckedIn = $obj->toHeatNo;
                $laneNoCheckedIn = $obj->toLaneNo;
            } else {
                $heatNo = $obj->toHeatNo;
                $laneNo = $obj->toLaneNo;
            }
            $sql = 'insert into ' . E4S_TABLE_SEEDING . ' (eventgroupid, athleteid, heatno,laneno,heatnocheckedin, lanenocheckedin)
                values (
                    ' . $obj->egId . ',
                    ' . $obj->athleteId . ',
                    ' . $heatNo . ',
                    ' . $laneNo . ',
                    ' . $heatNoCheckedIn . ',
                    ' . $laneNoCheckedIn . ')';
        } else {
            $sql = 'update ' . E4S_TABLE_SEEDING . '
                set heatno' . $checkedIn . '=' . $obj->toHeatNo . ',
                    laneno' . $checkedIn . '=' . $obj->toLaneNo . '
                where id=' . $fromObj->id;
        }

        e4s_queryNoLog($sql);

        $entryb = new stdClass();
        $entryb->entryId = $obj->toEntryId;
        if (!is_null($toObj)) {
            $sql = 'update ' . E4S_TABLE_SEEDING . '
                set heatno' . $checkedIn . '=' . $obj->fromHeatNo . ',
                    laneno' . $checkedIn . '=' . $obj->fromLaneNo . '
                where id=' . $toObj->id;
            e4s_queryNoLog($sql);
            $entryb->heatNo = $obj->fromHeatNo;
            $entryb->laneNo = $obj->fromLaneNo;
        }

        $retObj->entryB = $entryb;
        $this->sendSocketInfo($retObj, R4S_SOCKET_TRACK_MOVE);
        Entry4UISuccess();
    }

    public function getSeedRecordForAthlete($athleteId) {
        $sql = 'select *
                from ' . E4S_TABLE_SEEDING . '
                where eventgroupid = ' . $this->eventGroupId . '
                and   athleteid = ' . $athleteId;
        $result = e4s_queryNoLog($sql);
        $obj = null;
        if ($result->num_rows === 1) {
            $obj = $this->_normaliseRecord($result->fetch_object());
        }
        return $obj;
    }

    /*
    $obj = new stdClass();
    $obj->entryId = $entryId;
    $obj->athleteId = $athleteId;
    $obj->present = $present;
    $obj->checkedIn = boolean;
    $obj->fromHeatNo = $fromHeatNo;
    $obj->fromLaneNo = $fromLaneNo;
    $obj->toHeatNo = $toHeatNo;
    $obj->toLaneNo = $toLaneNo;
    $obj->compId = $compId;
    $obj->egId = $egId;
*/

    private function _normaliseRecord($obj) {
        $obj->id = (int)$obj->id;
        $obj->eventgroupid = (int)$obj->eventgroupid;
        $obj->athleteid = (int)$obj->athleteid;
        $obj->pos = (int)$obj->pos;
        $obj->laneno = (int)$obj->laneno;
        $obj->heatno = (int)$obj->heatno;
        $obj->lanenocheckedin = (int)$obj->lanenocheckedin;
        $obj->heatnocheckedin = (int)$obj->heatnocheckedin;
        return $obj;
    }

    public function getSeedRecord($heatNo, $laneNo, $checkedIn, $athleteId = 0) {
        $sql = 'select *
                from ' . E4S_TABLE_SEEDING . '
                where eventgroupid = ' . $this->eventGroupId . '
                and  ';
        if ($athleteId !== 0) {
            $sql .= ' athleteid = ' . $athleteId;
        } else {
            $sql .= '
                              heatno' . $checkedIn . '=' . $heatNo . '
                        and   laneno' . $checkedIn . '=' . $laneNo;
        }


        $result = e4s_queryNoLog($sql);
        $obj = null;
        if ($result->num_rows === 1) {
            $obj = $this->_normaliseRecord($result->fetch_object());
        }
        return $obj;
    }

    public function sendSocketInfo($data, $type) {
        error_log('sendSocketInfo ' . gettype($data));
	    $this->socket->setPayload($data);
	    $this->socket->sendMsg($type);
    }

    private function _removeBlankHeats($rows) {
        $heatNos = array();
        $heatNosCheckedIn = array();
        $debugEgId = 17759;
        foreach ($rows as $egId => $row) {
            if ($egId === $debugEgId) {
                var_dump($rows);
            }
            $egId = (int)$egId;
            if (isset($row->athleteId)) {
                $row->heatNo = (int)$row->heatNo;
                $row->heatNoCheckedIn = (int)$row->heatNoCheckedIn;

                $heatNos[$row->heatNo] = $row->heatNo;
                $heatNosCheckedIn[$row->heatNoCheckedIn] = $row->heatNoCheckedIn;
            } else {
                $heatNos = array();
                $heatNosCheckedIn = array();

                foreach ($row as $data) {
                    $data->heatNo = (int)$data->heatNo;
                    $data->heatNoCheckedIn = (int)$data->heatNoCheckedIn;

                    $heatNos[$data->heatNo] = $data->heatNo;
                    if ($data->heatNoCheckedIn !== 0) {
                        $heatNosCheckedIn[$data->heatNoCheckedIn] = $data->heatNoCheckedIn;
                    }
                }
                sort($heatNos);
                $sortedHeatNos = array();
                foreach ($heatNos as $newHeatNo => $heatNo) {
                    $sortedHeatNos[$heatNo] = (int)$newHeatNo + 1;
                }

                sort($heatNosCheckedIn);
                $sortedHeatNosCheckedIn = array();
                foreach ($heatNosCheckedIn as $newHeatNo => $heatNo) {
                    $sortedHeatNosCheckedIn[$heatNo] = (int)$newHeatNo + 1;
                }

                foreach ($row as $data) {
                    $data->heatNo = $sortedHeatNos[$data->heatNo];
                    if ($data->heatNoCheckedIn !== 0) {
                        $data->heatNoCheckedIn = $sortedHeatNosCheckedIn[$data->heatNoCheckedIn];
                    }
                }
                if ($egId === $debugEgId) {
                    echo 'Post>>';
                    var_dump($rows);
                    exit;
                }
            }
        }
        return $rows;
    }

    public function clearAndWriteSeedings($rows) {
        if (sizeof($rows) === 0) {
            return;
        }
        $rows = $this->_removeBlankHeats($rows);
        $sep = '';
        $uniqueEgIds = array();
        $egArr = array();
        $sql = 'Insert into ' . E4S_TABLE_SEEDING . '(eventgroupid, athleteid, laneno, heatno, lanenocheckedin, heatnocheckedin) values ';

        $arr = array();

        $sendEach = FALSE;
        foreach ($rows as $egId => $row) {
            if (isset($row->athleteId)) {
                $sql .= $sep . '(';
                if (isset($row->egId)) {
                    $egId = $row->egId;
                }
                $sql .= $egId;
                if (!array_key_exists($egId, $uniqueEgIds)) {
                    $uniqueEgIds[$egId] = array();
                }
                $uniqueEgIds[$egId][$row->athleteId] = TRUE;

                $sql .= ',' . $row->athleteId . ',' . $row->laneNo . ',' . $row->heatNo . ',' . $row->laneNoCheckedIn . ',' . $row->heatNoCheckedIn . ')';
                $sep = ',';
                $arr[$egId . '-' . $row->athleteId] = $row;
            } else {
                foreach ($row as $data) {
                    $sql .= $sep . '(';
                    $sql .= $egId;
                    if (!array_key_exists($egId, $uniqueEgIds)) {
                        $uniqueEgIds[$egId] = array();
                    }
                    $uniqueEgIds[$egId][$data->athleteId] = TRUE;

                    $sql .= ',' . $data->athleteId . ',' . $data->laneNo . ',' . $data->heatNo . ',' . $data->laneNoCheckedIn . ',' . $data->heatNoCheckedIn . ')';
                    $sep = ',';
                    if (!array_key_exists($egId, $egArr)) {
                        $egArr[$egId] = array();
                    }
                    $egArr[$egId][$egId . '-' . $data->athleteId] = $data;
                    $sendEach = TRUE;
                }
            }
        }
        $this->clearAthletesForEventGroups($uniqueEgIds);
        if ($sep !== '') {
            e4s_queryNoLog($sql);
        }
        // issue socket msg if required
        if (!$sendEach) {
            $this->sendSocketInfo($arr, R4S_SOCKET_SEED_ENTRIES);
        } else {
        // THE ISSUE IS HERE.......
            foreach ($egArr as $egId=>$eg) {
//                var_dump($egId);
//                $this->sendSocketInfo($eg, R4S_SOCKET_SEED_ENTRIES);
            }
        }
    }

    public function clearAthletesForEventGroups($arr) {
        foreach ($arr as $egId => $athleteArr) {
            $athleteIds = array();
            foreach ($athleteArr as $athleteId => $value) {
                $athleteIds[] = $athleteId;
            }
            $this->clearAthletes($athleteIds, $egId);
        }
    }

    public function clearAthletes($athleteIds, $egId = null) {
        if (is_null($egId)) {
            $egId = $this->eventGroupId;
        }
        $sql = '
            delete from ' . E4S_TABLE_SEEDING . '
            where eventgroupid = ' . $egId . '
            and   athleteid in (' . implode(',', $athleteIds) . ')';

        e4s_queryNoLog($sql);
    }

    public function writeInboundSeedings() {
        $checkedIn = FALSE;
        $egId = $this->eventGroupId;
        if (array_key_exists($egId, $_POST)) {
            // output report
            $entries = $_POST[$egId];
        } else {
            // otd tfCards call removing
            $entries = $_POST['seedings'];
            $maxInHeat = $_POST['maxInHeat'];
            if ($_POST['checkedIn'] === 'true') {
                $checkedIn = TRUE;
            }
        }

        $this->updateSeedings($entries, $checkedIn, $maxInHeat);
	    e4s_updateMaxInHeatFromCard($egId, $maxInHeat);
        Entry4UISuccess('');
    }

    //writeSeedings calls this

    public function updateSeedings($entries, $checkedIn, $maxInHeat) {
        $socketPayload = new stdClass();
        $seedings = [];
	    $heatNoCheckedIn = 1;
	    $laneNoCheckedIn = 0;
	    $heatNo = 1;
	    $laneNo = 0;
        foreach ($entries as $sub => $entry) {
            $athleteId = str_replace($this->eventGroupId . '-', '', $entry);
            if ($checkedIn) {
                $laneNoCheckedIn += 1;
                if ( $laneNoCheckedIn > $maxInHeat ){
                    $laneNoCheckedIn = 1;
                    $heatNoCheckedIn += 1;
                }
                $socketObj = new stdClass();
                $socketObj->heatNoCheckedIn = $heatNoCheckedIn;
                $socketObj->laneNoCheckedIn = $laneNoCheckedIn;
                $socketObj->heatPositionCheckedIn = $laneNoCheckedIn;
                $sql = 'update ' . E4S_TABLE_SEEDING . '
                    set heatnocheckedin = ' . $heatNoCheckedIn . ',
                        lanenocheckedin = ' . $laneNoCheckedIn . '
                    where eventgroupid = ' . $this->eventGroupId . '
                    and   athleteid = ' . $athleteId;
            } else {
                $laneNo += 1;
                if ( $laneNo > $maxInHeat ){
                    $laneNo = 1;
                    $heatNo += 1;
                }
                $socketObj = new stdClass();
                $socketObj->heatNo = $heatNo;
                $socketObj->laneNo = $laneNo;
                $socketObj->heatPosition = $laneNo;
                $sql = 'update ' . E4S_TABLE_SEEDING . '
                    set heatno = ' . $heatNo . ',
                        laneno= ' . $laneNo . '
                    where eventgroupid = ' . $this->eventGroupId . '
                    and   athleteid = ' . $athleteId;
            }
	        $seedings[$entry] = $socketObj;
            e4s_queryNoLog($sql);
        }
        $socketPayload->seedings = $seedings;
        $socketPayload->maxInHeat = $maxInHeat;
        $this->sendSocketInfo($socketPayload, R4S_SOCKET_SEED_ENTRIES);
    }
}