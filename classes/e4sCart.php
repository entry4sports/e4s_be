<?php


class e4sCart {
    public static function cartEmptied() {
        $userid = e4s_getUserID();
        if ($userid === E4S_USER_ANON_ID) {
            // Anonyous tickets, dont have any entries
            return;
        }
		$sql = '
			select e1.id, e1.compEventID, e1.athleteid, e1.userid
			from ' . E4S_TABLE_ENTRIES . ' e1,
			     ' . E4S_TABLE_ENTRIES . ' e2
			where abs(e1.compeventid) = abs(e2.compeventid)
			  and e1.athleteid = e2.athleteid
			  and abs(e2.userid) = ' . $userid . ' and e2.paid = ' . E4S_ENTRY_NOT_PAID . '
            order by userid desc';

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows > 0) {
            $processedCeIds = array();
            $toDelete = array();
            while ($obj = $result->fetch_object()) {
                if ((int)$obj->userid > 0) {
                    $processedCeIds[(int)$obj->compEventID] = TRUE;
                } else {
                    if (array_key_exists(abs((int)$obj->compEventID), $processedCeIds)) {
                        $toDelete[] = $obj->id;
                    }
                }
            }
            if (sizeof($toDelete) > 0) {
                // id should be enough, but just to be sure add where statement
                $sql = 'delete from ' . E4S_TABLE_ENTRIES . '
                        where id in (' . implode(',', $toDelete) . ')
                        and userid < 0
                        and paid = ' . E4S_ENTRY_NOT_PAID;
                e4s_queryNoLog($sql);
            }
        }
        $sql = 'update ' . E4S_TABLE_ENTRIES . "
                set userid = -userid,
                compeventid = -compeventid,
                options = '{\"reason\":\"cartemptied\"}'
                where userid = " . $userid . '
                and paid = ' . E4S_ENTRY_NOT_PAID;
        e4s_queryNoLog($sql);

        $sql = 'update ' . E4S_TABLE_EVENTTEAMENTRIES . "
                set ceid = -ceid,
                    userid = -userid,
                    options = '{\"reason\":\"cartemptied\"}'
                where userid = " . $userid . '
                and paid = ' . E4S_ENTRY_NOT_PAID;
        e4s_queryNoLog($sql);
        e4sSeatingClass::resetSeatsForUser();
    }

    public static function itemRemovedFromCart($productId) {
        include_once E4S_FULL_PATH . 'entries/entries.php';
        e4s_removeProductFromBasket($productId, FALSE, null, FALSE);
    }

    public static function getMaxQty($qtyArray, $product) {
        $cartItem = $qtyArray['cart_item'];
        $variation = $cartItem['variation'];

        $qty = $qtyArray['max_value'];
        $productId = $product->get_parent_id();
        $variationId = $product->get_id();
        if ($productId === 0) {
            $productId = $variationId;
            $variationId = 0;
        }
        $perAcc = '';
        $maxQty = secondaryDefClass::getProductMaxQty($productId, $perAcc);
        if (is_null($maxQty)) {
            return $qty;
        }
        if ($perAcc === E4S_PERACC_ATHLETE) {
            $athleteInfo = e4sProduct::getAthleteInfoFromVariations($variation, FALSE);
            if (!is_null($athleteInfo)) {
                $purchasedQty = e4sOrders::getProductPurchasedForAthlete($productId, $variationId, $athleteInfo);
                if (!is_null($purchasedQty) and $purchasedQty > 0) {
                    echo $purchasedQty . ' of ' . $maxQty . ' allowed have already been purchased.';
                    $maxQty -= $purchasedQty;
                }
            }
        }
        return $maxQty;
    }

//    return false if nothing removed, true if something removed

    public static function addAnySurcharge() {
//        var_dump("cart");
        $GLOBALS[E4S_NOHEADERS] = TRUE;
        include_once E4S_FULL_PATH . 'dbInfo.php';
        $percentage = 0.01;
//    $surcharge = ( $woocommerce->cart->cart_contents_total + $woocommerce->cart->shipping_total ) * $percentage;
        $surcharge = 0;
        WC()->cart->add_fee('Surcharge', $surcharge, TRUE, '');
    }

    public function __contruct() {

    }

    public function addToCart($model) {
        if ($model->prod->parentId !== 0) {
            $attribs = $this->fullfillAttributes($model->prod->id, $model->attributeValues);
            WC()->cart->add_to_cart($model->prod->parentId, $model->qtyRequired, $model->prod->id, $attribs);
        } else {
            WC()->cart->add_to_cart($model->prod->id, $model->qtyRequired);
        }
    }

    private function fullfillAttributes($id, $attribs) {
        $attribPrefix = 'attribute_';
        $wcAttribs = wc_get_product_variation_attributes($id);

        foreach ($wcAttribs as $wckey => $wcAttrib) {
//            var_dump($wckey);
            if ($wcAttrib === '') {
//                Need to get the Any Attrib
                foreach ($attribs as $key => $attrib) {

                    if ($wckey === sanitize_title($attribPrefix . $attrib['name'])) {
                        $wcAttribs[$wckey] = $attrib['value'];
                        break;
                    }
                }
                if ($wcAttribs[$wckey] === '') {
                    $wckey = str_replace($attribPrefix, '', $wckey);
                    e4sSeatingClass::resetSeatsForUser();
                    Entry4UIError(7020, 'Can not add product without knowing ' . $wckey, 200, '');
                }
            }
        }

        return $wcAttribs;
    }

    public function removeSecondaryFromCartForAthlete($compId, $athleteid) {
        $retVal = FALSE;
        if (isCronUser()) {
            return $retVal;
        }
        // Get list of prods for this comp that are Athlete specific
        $defSql = 'select sd.prodid prodid
                   from ' . E4S_TABLE_SECONDARYDEFS . ' sd,
                        ' . E4S_TABLE_SECONDARYREFS . ' sr
                   where sd.refid = sr.id
                   and   sr.compid = ' . $compId . "
                   and   sd.peracc = '" . E4S_PERACC_ATHLETE . "'";
        $result = e4s_queryNoLog($defSql);
        if ($result->num_rows === 0) {
            // No secondary Athlete items defined
            return $retVal;
        }

        $athleteProds = $result->fetch_all(MYSQLI_ASSOC);
        $prods = array();
        foreach ($athleteProds as $athleteProd) {
            $prods[] = (int)$athleteProd['prodid'];
        }

        $items = WC()->cart->get_cart();
//var_dump($items);
        foreach ($items as $item => $values) {

            $prodId = (int)$values['product_id'];

            if (in_array($prodId, $prods)) {
                if ($values['variation_id'] !== 0) {
                    if (array_key_exists('variation', $values)) {
                        $variations = $values['variation'];
                        $attribAthleteId = e4sProduct::getAthleteInfoFromVariations($variations);

                        if ($attribAthleteId === $athleteid) {
                            // e4sProduct::removeFromWCCart($prodId, (int)$values['variation_id']);
                            // dont pass the variation, delete all for the product
                            e4sProduct::removeFromWCCart($prodId);
                            $retVal = TRUE;
                        }
                    }
                }
            }
        }
        return $retVal;
    }
}
