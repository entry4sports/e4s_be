<?php
include E4S_FULL_PATH . 'classes/e4sSeatingClass.php';
const E4S_ORDER_PREFIX = 'Order:';
function e4s_getESAthleteGuid($obj) {
    $ticketObj = new stdClass();
    $ticketObj->orderId = -1;
    $ticketObj->itemId = (int)checkFieldForXSS($obj, 'athleteid:Athlete ID');
    $sql = 'select id
            from ' . E4S_TABLE_TICKET . '
            where athleteid = ' . $ticketObj->itemId;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(3245, 'Unable to find athlete ticket');
    }
    $dbObj = $result->fetch_object();
    $ticketObj->ticketId = $dbObj->id;
    $ticketObj->element = 1;

    $guidObj = new e4sTicketGuid($ticketObj);
    $ticketObj->guid = $guidObj->getFullGuid($dbObj->id);
    Entry4UISuccess($ticketObj);
}

function e4s_generateAndEmailTickets($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (is_null($compId) or $compId < 1) {
        Entry4UIError(5600, 'There is an issue with this command request');
    }
    $compId = (int)$compId;
    $sql = 'select id id,
                   ticketid ticketId,
                   email email,
                   sent sent
            from ' . E4S_TABLE_TICKETONEOFF . '
            where sent = 0';
    $result = e4s_queryNoLog($sql);
    while ($oneOffObj = $result->fetch_object()) {
        $oneOffObj->ticketId = (int)$oneOffObj->ticketId;
        $parmObj = new stdClass();
        $parmObj->email = $oneOffObj->email;
        $parmObj->compId = $compId;
        $parmObj->athleteId = -1;
        $parmObj->userId = e4s_getUserID();
        $parmObj->competition = 'English Schools 2021';
        $parmObj->stadium = 'Manchester';
        $parmObj->stand = 'South';
        $parmObj->orderId = -1;
        $parmObj->itemId = (int)$oneOffObj->id;
        $parmObj->element = -1;
        $parmObj->seatsRequired = 1;

        $ticketObj = new e4sTicket();

        if ($oneOffObj->ticketId === 0) {
            // generate ticket
            $oneOffObj->ticketId = $ticketObj->insertTicket($parmObj);
        }

        $guidObj = new e4sTicketGuid($parmObj);

        $guidObj->setTicketId($oneOffObj->ticketId);
        $fullGuid = $guidObj->getFullGuid();

        // email ticket
        $parmObj->fullGuid = $fullGuid;
        $ticketObj->emailOneOff($parmObj);

        // update to mark sent
        $sql = 'update ' . E4S_TABLE_TICKETONEOFF . '
                set sent = 1,
                    ticketid = ' . $oneOffObj->ticketId . '
                where id = ' . $oneOffObj->id;
        e4s_queryNoLog($sql);
    }
    Entry4UISuccess();
}

class e4sTicket {
    public $ticketId;
    public $guid;
    public $compid;
    public $orderId;
    public $athleteId;
    public $itemId;
    public $wcItem;
    public $wcProduct;
    public $productDescObj;
    public $variations;
    public $ticket;
    public $orderPWD;
    public $compId;
    public $allocation;
    public $ticketDetails;
    public $user;
    public $tempDir = '/temp/';
    public $curUser;
    public $orderUser;
    public $compText = null;
    public $prodText = array();

    public function __construct() {
        $this->curUser = new stdClass();
        $this->curUser->id = e4s_getUserID();
        $this->curUser->name = e4s_getUserName();
        $this->curUser->email = '';
        $this->orderUser = new stdClass();
        $this->orderUser->id = 0;
        $this->orderUser->name = '';
        $this->orderUser->email = '';
    }

    public static function cntDisplay($count) {
        return $count . ' : ';
    }

    public static function getTicketIcon() {
        return E4S_PATH . '/css/images/qr.png';
    }

    public function emailOneOff($obj) {
        $sendTo = $obj->email;
        $body = 'Dear Coach/Official,<br><br>';

        $body .= 'Please find attached your ticket to ' . $obj->competition;
        $body .= '<br><br>';
        $body .= 'You will need to show this ticket at the gate to gain entry to the stadium.<br>';
        $body .= '<br>';
        $body .= "<a href='https://" . E4S_CURRENT_DOMAIN . '/#/ticket/' . $obj->compId . '/' . $obj->fullGuid . "'>" . $obj->competition . '</a>';
        if (isset($obj->date) and $obj->date !== '') {
            $body .= ' held on ' . $obj->date;
        }
        $body .= '<br>';

        $body .= Entry4_emailFooter();
        if (!e4s_isLiveDomain()) {
            $body .= '<br>TESTING EMAIL. Would have been sent to ' . $obj->email;
            $sendTo = E4S_SUPPORT_EMAIL;
        }
        e4s_mail($sendTo, 'Official/Coach Tickets for ' . $obj->competition, $body, Entry4_mailHeader(''));
    }

    // Mark all tickets for a competition as off site

    public function emailAthletes($orderId) {
        $sql = 'select a.id id, a.email email, a.firstname firstname, a.surname surname,t.itemid itemid,t.id ticketid,t.element element,c.id compId, c.name compname, c.date compdate
                from ' . E4S_TABLE_TICKET . ' t,
                     ' . E4S_TABLE_ATHLETE . ' a,
                     ' . E4S_TABLE_COMPETITON . ' c
                where orderid = ' . $orderId . "
                and   t.compid = c.id
                and   athleteid > 0
                and   athleteid = a.id
                and   a.email != '' 
                order by a.id";

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return;
        }

        $userEmail = $GLOBALS[E4S_USER]->user->user_email;
        $emails = array();

        while ($row = $result->fetch_assoc()) {
            $compObj = e4s_GetCompObj($row['compId']);
            if ($compObj->ticketsEnabled()) {

                $athleteId = $row['id'];
                if (!array_key_exists($athleteId, $emails)) {
                    $emails [$athleteId] = array();
                }
                $emails [$athleteId][] = $row;
            }
        }

        foreach ($emails as $emailArr) {
            $athleteFirstName = $emailArr[0]['firstname'];
            $athleteEmail = $emailArr[0]['email'];
            $multiple = FALSE;
            if (sizeof($emailArr) > 1) {
                $multiple = TRUE;
            }
            if (strcasecmp($athleteEmail, $userEmail) !== 0) {
                $body = 'Dear ' . $athleteFirstName . ',<br><br>';

                if ($multiple) {
                    $body .= 'Please find attached your Athlete tickets entered by ' . $userEmail;
                    $body .= '<br><br>';
                    $body .= 'If you are entered into another event at any of these competitions, you will not receive another ticket';
                } else {
                    $body .= 'Please find attached your Athlete ticket entered by ' . $userEmail;
                    $body .= '<br><br>';
                    $body .= 'If you are entered into another event at this competition, you will not receive another ticket';
                }
                $body .= ' so please keep this ticket safe.';
                $body .= '<br><br>';
                $body .= 'Due to resources and restrictions, some organisers may not make use of ticket entry.<br>';
                $body .= 'If a competition does use Entry4Sports tickets, they will be required to enter the location of the competition.<br>';
                $body .= '<br>';
                foreach ($emailArr as $email) {
                    $obj = new stdClass();
                    $obj->ticketId = (int)$email['ticketid'];
                    $obj->orderId = $orderId;
                    $obj->itemId = (int)$email['itemid'];
                    $obj->element = (int)$email['element'];
                    $guidObj = new e4sTicketGuid($obj);
                    $compdate = new DateTime($email['compdate']);
                    $compdateStr = date_format($compdate, E4S_FORMATTED_DATE);
                    $body .= "<a href='https://" . E4S_CURRENT_DOMAIN . '/#/ticket/' . $emailArr[0]['compId'] . '/' . $guidObj->getFullGuid() . "'>" . $email['compname'] . '</a> held on ' . $compdateStr . '<br>';
                }

                $body .= Entry4_emailFooter();
                if (!e4s_isLiveDomain()) {
                    $body .= '<br>TESTING EMAIL. Would have been sent to ' . $athleteEmail;
                    $athleteEmail = E4S_SUPPORT_EMAIL;
                }
                e4s_mail($athleteEmail, 'Entry4Sports Athlete Tickets', $body, Entry4_mailHeader(''));
            }
        }
    }

    public function clear($obj) {
        $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
        if (is_null($compid) or $compid < 1) {
            return $this->_outputToUser('No competition reference passed to clear');
        }
        $sql = 'update ' . E4S_TABLE_TICKET . '
                set onSite = false
                where compid = ' . $compid;
        e4s_queryNoLog($sql);
        Entry4UISuccess();
    }

    public function _outputToUser($text = '', $param1 = '', $param2 = '') {
        if (isE4SUser()) {
            if ($param1 !== '') {
                $text .= ' [' . $param1 . ']';
            }
            if ($param2 !== '') {
                $text .= ' [' . $param2 . ']';
            }
        }
        Entry4UIError(8050, $text, 200, '');
        echo "Entry4Sports Ticket System\n\n";
        if (gettype($text) === 'object') {
            var_dump($text);
        } else {
            if ($text === '') {
                $text = 'Ooops : Something has gone wrong. Please retry.';
            }
            echo $text;
        }

        exit();
    }

    public function report($obj) {
        $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
        if (!is_null($compid)) {
            $this->compId = (int)$compid;
        }
        $hasAccess = userHasPermission('ADMIN', null, $this->compId);
        if (!$hasAccess) {
            return $this->_outputToUser('You do not have access to this report.');
        }
        $where = 't.compid = ' . $this->compId;
        $result = $this->_readDBTickets($where, FALSE);

        if ($result->num_rows === 0) {
            Entry4UISuccess();
        }

        $retObj = new stdClass();

        $compObj = e4s_GetCompObj($this->compId);
        $competition = new stdClass();
        $competition->id = $this->compId;
        $competition->name = $compObj->getName();
        $retObj->competition = $competition;

        $retObj->athletes = new stdClass();
        $retObj->athletes->onSite = array();
        $retObj->athletes->offSite = array();

        $retObj->tickets = new stdClass();
        $retObj->tickets->onSite = array();
        $retObj->tickets->offSite = array();

        while ($row = $result->fetch_assoc()) {
            $obj = $this->_getDataForRow($row, TRUE);
            if (!is_null($obj)) {
                unset($obj->user->ticketMgr);
                if ($obj->athlete->id !== 0) {
                    // athlete ticket
                    if ($obj->ticket->onSite) {
                        $retObj->athletes->onSite[] = $obj;
                    } else {
                        $retObj->athletes->offSite[] = $obj;
                    }
                } else {
                    // 2ndry ticket
                    if ($obj->ticket->onSite) {
                        $retObj->tickets->onSite[] = $obj;
                    } else {
                        $retObj->tickets->offSite[] = $obj;
                    }
                }
            }
        }
        Entry4UISuccess($retObj);
//        Entry4UISuccess('
//            "data":' . e4s_encode($retArr) . ',
//            "meta":' . e4s_encode($counters)
//        );
    }

    private function _readDBTickets($where, $oneTicket = TRUE) {
        $guidSQL = e4sTicketGuid::getSQL();
        $sql = $this->_ticketSelect() . $where;
        $sql = str_replace(E4S_TICKET_GUID, $guidSQL, $sql);

        $result = e4s_queryNoLog($sql);

        if ($oneTicket) {
            if ($result->num_rows !== 1) {
                return null;
            }
            return $result->fetch_assoc();
        }
        return $result;
    }

    private function _ticketSelect() {
        $sql = 'select t.*, ta.id taId, ta.name name, ta.address address, ta.telno telNo, ta.email email, ' . E4S_TICKET_GUID . ' guid, u.user_email, u.display_name, pm.meta_value orderEmail
                from ' . E4S_TABLE_TICKET . ' t left join ' . E4S_TABLE_TICKETALLOCATION . ' ta on (ta.id = t.allocationId) 
                     left join ' . E4S_TABLE_USERS . ' u on t.userid = u.ID  
                     left join ' . E4S_TABLE_POSTMETA . " pm on (pm.post_id = t.orderId and pm.meta_key = '_billing_email')
                where ";
        return $sql;
    }

// defunct function

    private function _getDataForRow($row, $summary = FALSE) {
        if ((int)$row['orderId'] === -1 and $row['itemId'] === $row['athleteid']) {
            $obj = $this->_getDataForESAthlete($row, $summary);
        } elseif ((int)$row['orderId'] === -1 and (int)$row['itemId'] === -1) {
            $obj = $this->_getDataForOneOffRow($row, $summary);
        } else {
            $obj = $this->_getDataForWcRow($row, $summary);
        }
        return $obj;
    }

    private function _getDataForESAthlete($row, $summary = FALSE) {
        $this->compId = 265;
        $ticketOperative = $this->_getTicketoperative();
        $row['totalCount'] = 1;
        $this->variations = array();

        $this->_moveRowToThis($row);

        $retObj = $this->_getBaseObjectForTicket();
        $product = $retObj->product;
        $athlete = $retObj->athlete;

        $athlete->id = (int)$row['athleteid'];
        $sql = 'select firstname, surname, email, clubname clubname
                from ' . E4S_TABLE_ATHLETE . ' a,
                     ' . E4S_TABLE_CLUBS . ' c
                where a.id = ' . $athlete->id . '
                and c.id = a.clubid';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(8023, 'Invalid Athlete : ' . $athlete->id);
        }
        $athleteObj = $result->fetch_object();
        $athlete->club = $athleteObj->clubname;
        $athlete->name = $athleteObj->firstname . ' ' . $athleteObj->surname;
        $athlete->bibno = $athlete->id;
        $retObj->user->email = $athleteObj->email;
        // get Entries from the checkin Object and add more infor to the athlete object
        $product->name = 'English Schools 2021 Athlete';
        $product->variationId = 0;

        $retObj->athlete = $athlete;
        $retObj->product = $product;

        $retObj->variations = $this->variations;
        $retObj->ticket = $this->ticket;
        if ((int)$this->ticket->onSite === 0 or $this->ticket->onSite === FALSE) {
            $this->ticket->onSite = FALSE;
        } else {
            $this->ticket->onSite = TRUE;
        }

        $this->ticket->dataReq = FALSE;

        $retObj->data = $this->allocation;
        if (!is_null($retObj->data)) {
            $retObj->data->telNo = e4s_ensureString($retObj->data->telNo);
        }
        if (!$summary) {
            if (is_null($this->compText)) {
                $termsInfo = notesClass::get($this->compId, E4S_COMP_TANDC);
                $this->compText = e4s_htmlToUI($termsInfo);
            }

            $textObj = new stdClass();
            $textObj->terms = $this->compText;
            $textObj->ticketText = '';
            $textObj->ticketForm = '';

            $retObj->text = $textObj;
            $retObj->audit = array();
            if ($ticketOperative) {
                $retObj->audit = $this->_readAudit($retObj->id);
            }
        }

        return $retObj;
    }

    private function _getTicketoperative() {
        $ticketOperative = userHasPermission('TICKET', null, $this->compId);
        return $ticketOperative;
    }

    private function _moveRowToThis($row) {
        $this->ticketId = (int)$row['id'];
        $this->orderUser = $this->_getUser((int)$row['userId']);
        if ($this->orderUser->id === 0) {
            $this->orderUser->email = $row['orderEmail'];
            $email = preg_split('~@~', $row['orderEmail']);
            $this->orderUser->name = $email[0];
        }
        $guidObj = new e4sTicketGuid($row['guid'], (int)$row['id']);
        $this->guid = $guidObj->getGuidOnly();
        $this->orderId = (int)$row['orderId'];
        $this->itemId = (int)$row['itemId'];
        $ticket = new stdClass();
        $ticket->element = (int)$row['element'];
        $ticket->scannedCount = (int)$row['scannedCount'];
        $ticket->searchCount = (int)$row['searchCount'];
        $ticket->onSite = (int)$row['onSite'];
        $ticket->totalCount = (int)$row['totalCount'];
        $ticket->dataReq = FALSE;
        if ($this->orderId > 0) {
            $wc_order_item = new WC_Order_Item_Product($this->itemId);
            $wc_product = wc_get_product($wc_order_item->get_product_id());
            $prodObj = e4s_getWCProductDescObj($wc_product);
            if (isset($prodObj->athleteid)) {
                // Athlete ticket, get required from CompOptions
                $descObj = e4s_getWCProductDescObj($wc_product);
                $compObj = e4s_GetCompObj($descObj->compid, FALSE);
                $ticket->dataReq = $compObj->isAthleteDataRequired();
            } else {
                $ticket->dataReq = $this->_getDataReq();
            }
        }

        $ticket->seat = e4sSeatingClass::getSeatingInfoForTicket($this->ticketId);
        $this->ticket = $ticket;
        $ticketAllocation = new ticketAllocationClass(0);
        $this->allocation = $ticketAllocation->getFromRow($row);
    }

    public function _getUser($id) {
        $obj = new stdClass();
        $obj->id = 0;
        $obj->name = '';
        $obj->email = '';
        $sql = 'select *
                from ' . E4S_TABLE_USERS . '
                where id = ' . $id;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            $row = $result->fetch_assoc();
            $obj->id = $row['ID'];
            $obj->name = $row['display_name'];
            $obj->email = $row['user_email'];
        }
        return $obj;
    }

    private function _getDataReq() {
        $secondModel = new stdClass();
        $secondModel->process = '-';
        $secondModel->prodid = $this->wcProduct->get_parent_id();
        if ($secondModel->prodid === 0) {
            $secondModel->prodid = $this->wcProduct->get_id();
        }

        $secondObj = new secondaryDefClass($secondModel);
        $secondRow = $secondObj->getForProd();
        $dataReq = FALSE;
        if (!is_null($secondRow)) {
            if ($secondRow['dataReq'] === '1') {
                $dataReq = TRUE;
            }
        }
        return $dataReq;
    }

    private function _getBaseObjectForTicket() {
        $retObj = new stdClass();
        $guidObj = new e4sTicketGuid($this->guid, $this->ticketId);
        $retObj->guid = $guidObj->getFullGuid();
        $retObj->id = e4sTicketGuid::getTicketFromFullGuid($retObj->guid);

        $retObj->orderId = $this->orderId;
        $retObj->user = $this->orderUser;
        $retObj->user->ticketMgr = $this->_getTicketoperative();
        $product = new stdClass();
        $athlete = new stdClass();
        $athlete->id = 0;
        $athlete->bibno = 0;
        $athlete->name = '';
        $retObj->product = $product;
        $retObj->athlete = $athlete;
        $retObj->entries = array();
        return $retObj;
    }

    private function _readAudit($ticketId = null) {
        if (is_null($ticketId)) {
            $ticketId = $this->ticketId;
        }
        $sql = 'select ta.id, ta.action, ta.auditDate auditDate,  ta.userId, u.display_name username
                from ' . E4S_TABLE_TICKETAUDIT . ' ta,
                     ' . E4S_TABLE_USERS . ' u
                where ticketid = ' . $ticketId . '
                and u.id = ta.userid
                order by auditdate desc';
        $result = e4s_queryNoLog($sql);
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $retRows = array();
        foreach ($rows as $row) {
            $rowObj = new stdClass();
            $rowObj->id = (int)$row['id'];
            $rowObj->action = $row['action'];
            $rowObj->auditDate = e4s_sql_to_iso($row['auditDate']);

            $user = new stdClass();
            $user->id = (int)$row['userId'];
            $user->name = $row['username'];
            $rowObj->user = $user;
            $retRows[] = $rowObj;
        }
        return $retRows;
    }

    private function _getDataForOneOffRow($row, $summary = FALSE) {
        $retObj = $this->_getBaseObjectForTicket();
        $retObj->user = $this->_getUser(e4s_getUserID());
        $retObj->orderId = 0;
        $retObj->product->productId = 0;
        $retObj->product->name = 'Athlete';
        $retObj->product->variationId = 0;
        $retObj->variations = array();
        $ticket = new stdClass();
        $ticket->element = (int)$row['element'];
        $ticket->scannedCount = (int)$row['scannedCount'];
        $ticket->searchCount = (int)$row['searchCount'];
        $ticket->onSite = (int)$row['onSite'];
        $ticket->totalCount = 1;
        $ticket->dataReq = FALSE;
        $ticket->seat = null;

        $data = new stdClass();
        $data->id = 0;
        $data->name = '';
        $data->address = '';
        $data->telNo = '';
        $data->email = '';
        $retObj->data = $data;

        if ($ticket->onSite === 0 or $ticket->onSite === FALSE) {
            $ticket->onSite = FALSE;
        } else {
            $ticket->onSite = TRUE;
        }
        $retObj->ticket = $ticket;
        $text = new stdClass();
        $text->terms = '';
        $text->ticketText = '';
        $text->ticketForm = '';
        $retObj->text = $text;
        return $retObj;
    }

    private function _getDataForWcRow($row, $summary = FALSE) {
        $ticketOperative = $this->_getTicketoperative();

        $this->wcItem = new WC_Order_Item_Product($row['itemId']);
//        logTxt("_getDataForRow B : " . $row['itemId']);
        $row['totalCount'] = $this->wcItem->get_quantity();
        $vars = $this->wcItem->get_meta_data();
        $variations = array();
        foreach ($vars as $var) {
            $metaData = $var->get_data();
            if ($metaData['key'] === E4S_ATTRIB_ATHLETE or $metaData['key'][0] !== '_') {
                $variations[$metaData['key']] = $metaData['value'];
            }
        }
        $this->variations = $variations;
        $useProdID = $this->wcItem->get_variation_id();
        if ($useProdID === 0) {
            $useProdID = $this->wcItem->get_product_id();
        }
//        echo "readTicket :  $useProdID\n";
//        var_dump($row);
        $this->wcProduct = wc_get_product($useProdID);
        if ($this->wcProduct === FALSE) {
            // product deleted ???
            return null;
        }
        $this->productDescObj = e4s_getWCProductDescObj($this->wcProduct);

        //echo "readTicket\n";
//        var_dump($this->productDescObj);
        if (!isset($this->productDescObj->compid)) {
            return $this->_outputToUser('2: Can not find this ticket');
        }
        $athleteTicket = FALSE;
        if (isset($this->productDescObj->athlete)) {
            $athleteTicket = TRUE;
        }
        $this->_moveRowToThis($row);

        if (!is_null($this->compId) and $this->compId !== $this->productDescObj->compid) {
            return $this->_outputToUser('This ticket is not for this competition.[' . $this->productDescObj->compid . '/' . $useProdID . '/' . $row['orderId'] . ']');
        }

        if ($this->curUser->id !== (int)$row['userId'] and !$ticketOperative) {
            if (!e4sTicket::isPublicRead()) {
                return $this->_outputToUser('You do not have permission to read this ticket');
            }
        }
        $retObj = $this->_getBaseObjectForTicket();
        $product = $retObj->product;
        $athlete = $retObj->athlete;
        if ($athleteTicket) {
            // Athlete Ticket
            $athlete->id = $this->productDescObj->athleteid;
            $athlete->name = $this->productDescObj->athlete;
            $athlete->bibno = $this->_getAthleteBibNo($this->productDescObj);

            // get Entries from the checkin Object and add more infor to the athlete object
            $obj = new stdClass();
            $checkInObj = new checkinClass($this->compId, $obj);
            $checkInAthleteObj = $checkInObj->listCheckins($athlete->id, TRUE, FALSE, FALSE);
            if (!is_null($checkInAthleteObj) and isset($checkInAthleteObj->athletes)) {
                $retObj->entries = $checkInAthleteObj->athletes[0]->entries;
                $athlete->club = $checkInAthleteObj->athletes[0]->club;
                $athlete->gender = $checkInAthleteObj->athletes[0]->gender;
                $athlete->urn = $checkInAthleteObj->athletes[0]->urn;
                $athlete->dob = $checkInAthleteObj->athletes[0]->dob;
            }
            $product->productId = $this->wcProduct->get_id();
            $product->name = $this->_getAthleteTicketName();
            $product->variationId = 0;

        } else {
            if (!isset($this->productDescObj->ticket)) {
                return null;
            }
            $product->productId = $this->wcProduct->get_parent_id();
            $product->name = $this->productDescObj->ticket;
            $product->variationId = $this->wcProduct->get_id();
        }
        $retObj->athlete = $athlete;
        $retObj->product = $product;

        $retObj->variations = $this->variations;
        $retObj->ticket = $this->ticket;
        if ((int)$this->ticket->onSite === 0 or $this->ticket->onSite === FALSE) {
            $this->ticket->onSite = FALSE;
        } else {
            $this->ticket->onSite = TRUE;
        }
        if ($athleteTicket) {
            // Athletes do not have to enter required data
            $this->ticket->dataReq = FALSE;
        }
        $retObj->data = $this->allocation;
        if (!is_null($retObj->data)) {
            $retObj->data->telNo = e4s_ensureString($retObj->data->telNo);
        }
        if (!$summary) {
            if (is_null($this->compText)) {
                $termsInfo = notesClass::get($this->compId, E4S_COMP_TANDC);
                $this->compText = e4s_htmlToUI($termsInfo);
            }
            if (!array_key_exists($product->productId, $this->prodText)) {
                $prodTextObj = new stdClass();
                $ticketInfo = notesClass::get(E4S_TICKET_PREFIX . $product->productId, E4S_COMP_TICKET);
                $prodTextObj->ticketText = e4s_htmlToUI($ticketInfo);
                $ticketFormInfo = notesClass::get(E4S_TICKET_PREFIX . $product->productId, E4S_COMP_TICKETFORM);
                $prodTextObj->ticketFormText = e4s_htmlToUI($ticketFormInfo);
            } else {
                $prodTextObj = $this->prodText[$product->productId];
            }

            $textObj = new stdClass();
            $textObj->terms = $this->compText;
            $textObj->ticketText = $prodTextObj->ticketText;
            $textObj->ticketForm = $prodTextObj->ticketFormText;

            $retObj->text = $textObj;
            $retObj->audit = array();
            if ($ticketOperative) {
                $retObj->audit = $this->_readAudit($retObj->id);
            }
        }
        return $retObj;
    }

    public static function isPublicRead() {
        return TRUE;
    }

    private function _getAthleteBibNo($obj) {
        $bibno = 0;
        if (isset($obj->compid) and isset($obj->athleteid)) {
            $sql = 'select *
                    from ' . E4S_TABLE_BIBNO . '
                    where compid = ' . $obj->compid . ' 
                    and   athleteid = ' . $obj->athleteid;
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows === 1) {
                $row = $result->fetch_assoc();
                $bibno = (int)$row['bibno'];
            }
        }
        return $bibno;
    }

    private function _getAthleteTicketName($compid = 0, $athlete = '') {
        if ($compid === 0) {
            $compid = $this->productDescObj->compid;
        }
        if ($athlete === '') {
            $athlete = $this->productDescObj->athlete;
        }
        return $compid . E4S_TICKET_DIVIDER . $athlete;
    }
    // Will need to return an array if the purchase is for a
    // 2 seater ticket e.t.c

    public function listAll($obj) {
        $this->list($obj, FALSE, TRUE);
    }

	private function _setTicketCompId(){
		$compObj = e4s_getCompObj($this->compId);
		$this->compId = (int)$compObj->getTicketCompId();
		e4s_addDebugForce("Ticket Id : " . $this->compId);
	}
    public function list($obj, $athleteList = FALSE, $all = FALSE) {
        $retArr = array();

        $model = new stdClass();

        $orderKey = checkFieldForXSS($obj, 'order_key:Order Key');
        if (!is_null($orderKey)) {
            $orderKey = 'wc_order_' . $orderKey;
        }
        $this->orderPWD = $orderKey;

        $model->compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
        if (!is_null($model->compid)) {
            $model->compid = (int)$model->compid;
            $this->compId = $model->compid;
        }

		$this->_setTicketCompId();
	    $model->compid = $this->compId;
        $search = checkFieldForXSS($obj, 'search:General Search Field');
//        if ( strpos($search,"Order:" ) > -1 ){
//            $search = str_replace("Order:","",$search);
//        }
        $email = checkFieldForXSS($obj, 'email:Email');
        $model->email = '';
        if (!is_null($email) and $email !== '') {
            $model->email = '%' . $email . '%';
        }
        if (is_null($model->compid) and is_null($this->orderPWD)) {
            return $this->_outputToUser('Please pass a data reference to list');
        }
        e4s_addStdPageInfo($obj, $model);
        $pagesize = $model->pageInfo->pagesize;
        $page = $model->pageInfo->page;
        $sortkey = $model->pageInfo->sortkey;
        $sortorder = $model->pageInfo->sortorder;

        $usePaging = FALSE;
        if (isset($pagesize) and isset($page)) {
            $usePaging = TRUE;
        }
        if ($all and is_null($model->compid)) {
            $all = FALSE;
        }
        $searchWhere = '';

        if ($all) {
            if ($search !== '') {
                $searchWhere = " and concat('" . E4S_ORDER_PREFIX . "',t.orderid, ifnull(ta.email,''), ifnull(ta.name,''), ifnull(ta.telNo,''), ifnull(ta.address,''), ifnull(u.user_email,''), ifnull(u.display_name,''), ifnull(pm.meta_value,'')) like '%" . $search . "%' ";
            }
            // ticket SQL
            $sql = $this->_ticketSelect();
            $sql .= 't.compid = ' . $model->compid . ' 
                and athleteid < 1';
            if ($searchWhere !== '') {
                $sql .= $searchWhere;
            }

            $sql .= ' Union ';
            // athlete sql
            $sql .= $this->_getAthleteSelect();
            $sql .= ' t.compid = ' . $model->compid . ' 
                and athleteid > 0 ';
            if ($search !== '') {
                $searchWhere = " and concat(ifnull(a.urn,''), a.firstname, a.surname, c.clubname, u.user_email, a.email, ifnull(pm.meta_value,'')) like '%" . $search . "%'";
            }
            $sql .= $searchWhere;

            $sql .= '  order by ' . $sortkey . ' ' . $sortorder;

            if ($usePaging and $pagesize !== 0) {
                $sql .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
            }
e4s_addDebugForce("1: " . $sql);
            $result = $this->_readAllTickets($sql);
//            $result = null;
        } elseif (!$athleteList) {
            if ($search !== '') {
                $searchWhere = " and concat('" . E4S_ORDER_PREFIX . "',t.orderid,ifnull(ta.email, ''), ifnull(ta.name,''), ifnull(ta.telNo,''), ifnull(ta.address,'')) like '%" . $search . "%' ";
            }
            if (!is_null($model->compid)) {
                $where = 't.compid = ' . $model->compid . ' 
                and athleteid < 1';
                if ($model->email !== '') {
                    $where .= " and u.user_email like '" . $model->email . "' ";
                }
            } elseif (!is_null($this->orderPWD)) {
                $orderId = $this->_getOrderIdForKey();
                $where = ' t.orderid = ' . $orderId;
            } else {
                return $this->_outputToUser('Oops, Something has gone wrong');
            }
            $where .= $searchWhere;
            $where .= ' order by ' . $sortkey . ' ' . $sortorder;

            if ($usePaging and $pagesize !== 0) {
                $where .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
            }
e4s_addDebugForce("2: " . $where);
            $result = $this->_readDBTickets($where, FALSE);
        } else {

            if ($search !== '') {
                $searchWhere = " and concat(ifnull(a.urn,''), a.firstname, a.surname, c.clubname, u.user_email, a.email) like '%" . $search . "%'";
            }
            $urn = checkFieldForXSS($obj, 'urn:Athlete URN');
            $clubname = checkFieldForXSS($obj, 'club:Club Name');
            $firstname = checkFieldForXSS($obj, 'firstname:Athletes Firstname');
            $surname = checkFieldForXSS($obj, 'surname:Athletes Surname');

            if (is_null($model->compid)) {
                Entry4UIError(6650, 'No Competition');
            }
            $where = 't.compid = ' . $model->compid . ' 
                and athleteid > 0';
            if (!is_null($urn) and $urn !== '') {
                $where .= ' and a.urn = ' . $urn;
            } else {
                if (!is_null($firstname) and $firstname !== '') {
                    $where .= " and a.firstname like '" . $firstname . "%'";
                }
                if (!is_null($surname) and $surname !== '') {
                    $where .= " and a.surname like '" . $surname . "%'";
                }
                if (!is_null($clubname) and $clubname !== '') {
                    $where .= " and c.clubname like '" . $clubname . "%'";
                }
            }
            $where .= $searchWhere;
e4s_addDebugForce("3: " . $where);
            $result = $this->_readDBAthleteTickets($where, FALSE);
        }
        if ($result->num_rows === 0) {
            Entry4UISuccess($retArr);
        }
        while ($row = $result->fetch_assoc()) {
            if (is_null($this->compId)) {
                $this->compId = (int)$row['compid'];
            }
            $obj = $this->_getDataForRow($row, TRUE);

            if (!is_null($obj)) {
//                if ( !$athleteList or !empty($obj->entries)){
//                Valid athlete. product and ticket and if athlete, has events
                unset($obj->user->ticketMgr);
                $retArr[] = $obj;
//                }
            }
        }
        $meta = $this->getMetaInfo();

        Entry4UISuccess('
            "data":' . e4s_encode($retArr) . ',
            "meta":' . e4s_encode($meta));
    }

    private function _getAthleteSelect() {
        $sql = 'select t.*, ta.id taId, ta.name name, ta.address address, ta.telno telNo, ta.email email, ' . E4S_TICKET_GUID . ' guid, u.user_email, u.display_name, pm.meta_value orderEmail
                from ' . E4S_TABLE_TICKET . ' t left join ' . E4S_TABLE_TICKETALLOCATION . ' ta on (ta.id = t.allocationId) ,
                     ' . E4S_TABLE_USERS . ' u,
                     ' . E4S_TABLE_POSTMETA . ' pm,
                     ' . E4S_TABLE_ATHLETE . ' a left join ' . E4S_TABLE_CLUBS . ' c on (c.id = a.clubid) 
                     
                where t.userid = u.ID 
                and   t.athleteid = a.id
                and ';
        return $sql;
    }
    // $download : array of props for a WC_Product_Download, if Athlete, extras added
    // return ticket URLs

    private function _readAllTickets($sql) {
        $guidSQL = e4sTicketGuid::getSQL();
        $sql = str_replace(E4S_TICKET_GUID, $guidSQL, $sql);
        $result = e4s_queryNoLog($sql);
        return $result;
    }

    private function _getOrderIdForKey() {
        $sql = 'select *
                from ' . E4S_TABLE_POSTMETA . "
                where meta_key = '_order_key'
                and meta_value = '" . $this->orderPWD . "'";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            return $this->_outputToUser('Unable to get order for ' . $this->orderPWD);
        }
        $row = $result->fetch_assoc();
        return (int)$row['post_id'];
    }

    private function _readDBAthleteTickets($where, $oneTicket = TRUE) {
        $guidSQL = e4sTicketGuid::getSQL();
        $sql = str_replace(E4S_TICKET_GUID, $guidSQL, $this->_getAthleteSelect() . $where);

        $result = e4s_queryNoLog($sql);

        if ($oneTicket) {
            if ($result->num_rows !== 1) {
                return null;
            }
            return $result->fetch_assoc();
        }
        return $result;
    }

    private function getMetaInfo() {
        $meta = new stdClass();
        $compObj = e4s_GetCompObj($this->compId, TRUE);

        $competition = new stdClass();
        $competition->id = (int)$this->compId;
        $productName = $this->wcProduct->get_name();
        $prodDesc = $this->wcProduct->get_description();

        $competition->date = $compObj->getDate();
        if ($prodDesc !== '') {
            $prodDescObj = e4s_getDataAsType($prodDesc, E4S_OPTIONS_OBJECT);
            if (isset($prodDescObj->date)) {
                $competition->date = $prodDescObj->date;
            }
        }

        $competition->name = $compObj->getName();
        $competition->location = $compObj->getLocationObj();

        if (e4s_isCarPark($productName)) {
            $prodMeta = $this->wcProduct->get_meta_data();
            foreach ($prodMeta as $prodMetaData) {
                $prodMetaDataArr = $prodMetaData->get_data();
                if ($prodMetaDataArr['key'] === E4S_CAR_REG_INFO) {
                    $competition->location->name = $prodMetaDataArr['value'];
                }
            }
        }

        $competition->organiser = $compObj->getOrganisationObj();
        $competition->dates = $compObj->getDatesFromOptions();
        $meta->competition = $competition;

        $meta->counters = $this->getOnSiteCounts();
        return $meta;
    }

    private function getOnSiteCounts() {
        $athleteCnts = $this->getOnSiteCount(TRUE);
        $ticketCnts = $this->getOnSiteCount(FALSE);
        $ret = new stdClass();
        $ret->ticketCounters = new stdClass();
        $ret->ticketCounters->athletes = $athleteCnts;
        $ret->ticketCounters->tickets = $ticketCnts;
        return $ret;
    }

    private function getOnSiteCount($athleteTickets = FALSE) {
        $obj = new stdClass();
        $obj->ticketsOnSite = 0;
        $obj->ticketsOffSite = 0;
        $sql = 'select count(id) counter, onsite onsite
                from ' . E4S_TABLE_TICKET . '
                where compid = ' . $this->compId . '
                and athleteid ';
        if ($athleteTickets) {
            $sql .= ' > 0';
        } else {
            $sql .= ' < 1';
        }
        $sql .= ' group by onsite';

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 0) {
            while ($row = $result->fetch_assoc()) {
                if ((int)$row['onsite'] === 1) {
                    $obj->ticketsOnSite = (int)$row['counter'];
                }
                if ((int)$row['onsite'] === 0) {
                    $obj->ticketsOffSite = (int)$row['counter'];
                }
            }
        }
        return $obj;
    }

    public function allocate($obj) {
        $ticketGuid = checkFieldForXSS($obj, 'guid:Ticket GUID');
//        var_dump($ticketGuid);
//        echo "\n";
        $ticketRow = null;
        if (is_null($ticketGuid)) {
            Entry4UIError(6700, 'No ticket identification passed to update');
        } else {
            $guidObj = new e4sTicketGuid($ticketGuid);
            $this->ticketId = $guidObj->ticketId;
//            var_dump( $this->ticketId);
//            echo "\n";
            $ticketRow = $this->_readDBTickets('t.id = ' . $this->ticketId);

        }
        if (is_null($ticketRow)) {
            Entry4UIError(6705, 'No ticket found to allocate');
        }
        if (!(isE4SUser() or (int)$ticketRow['userId'] === e4s_getUserID())) {
            Entry4UIError(6715, 'You are not authorised to allocate this ticket');
        }
//        var_dump($ticketRow);
//        echo "\n";
        $allocateEmail = checkFieldForXSS($obj, 'email:Allocate to Email');
        if ($allocateEmail === '' or !strpos($allocateEmail, '@')) {
            Entry4UIError(6725, 'No email passed to allocate ticket to');
        }
        $updateSql = 'update ' . E4S_TABLE_TICKET . "
                      set allocatedEmail = '" . $allocateEmail . "'
                      where id = " . $this->ticketId;
        e4s_queryNoLog($updateSql);
        $this->_sendAllocatedEmail($allocateEmail, $ticketRow['compid'], $ticketGuid);
//
//        var_dump($allocateEmail);
//        echo "\n";
//        exit();

    }

    private function _sendAllocatedEmail($email, $compId, $fullGuid) {
        $compObj = e4s_GetCompObj($compId);
        $sendTo = $email;
        $name = preg_split('~@~', $email);
        $body = $name[0] . ',<br><br>';

        $body .= 'Please find attached a ticket to ' . $compObj->getName() . ' that has been allocated to yourself.';
        $body .= '<br><br>';
        $body .= 'You will need to show this ticket at the gate to gain entry to the stadium.<br>';
        $body .= '<br>';
        $body .= "Click here for your ticket to <a href='https://" . E4S_CURRENT_DOMAIN . '/#/ticket/' . $compObj->getID() . '/' . $fullGuid . "'>" . $compObj->getName() . '</a>';

        $body .= ' held on ' . $compObj->getDate();

        $body .= '<br>';

        $body .= Entry4_emailFooter();
//        if ( E4S_CURRENT_DOMAIN !== E4S_UK_DOMAIN and E4S_CURRENT_DOMAIN !== E4S_AAI_DOMAIN ){
//            $body .= "<br>TESTING EMAIL. Would have been sent to " . $email;
//            $sendTo = E4S_SUPPORT_EMAIL;
//        }
        e4s_mail($sendTo, 'Allocated ticket to ' . $compObj->getName(), $body, Entry4_mailHeader(''));
    }

    public function update($obj) {
        $retObj = new stdClass();
        $retObj->id = 0;
        $retObj->data = null;
        $retObj->onSite = null;

        $ticketId = checkFieldForXSS($obj, 'id:Ticket Id');
        $ticketRow = null;
        if (is_null($ticketId)) {
            Entry4UIError(6600, 'No ticket identification passed to update');
        } else {
            $this->ticketId = (int)$ticketId;
            $ticketRow = $this->_readDBTickets('t.id = ' . $this->ticketId);
        }
        if (is_null($ticketRow)) {
            Entry4UIError(6605, 'No ticket found to update');
        }
        $onSite = checkJSONObjForXSS($obj, 'onSite:On Site Status');

        $data = checkJSONObjForXSS($obj, 'data:Ticket Allocation');
        if (!is_null($data)) {
            $data = e4s_getDataAsType($data, E4S_OPTIONS_OBJECT);
            $retObj->id = $ticketId;
            $retObj->data = $this->updateAllocation($ticketRow, $data, $onSite);
            $retObj->onSite = $onSite;
        }

        if ($retObj->id === 0 and !is_null($onSite)) {
            // Not update with data allocation
            $retObj->id = $ticketId;
            $retObj->onSite = $this->updateOnSite($ticketId, $onSite);
        }
        if ($retObj->id !== 0 and !is_null($data)) {
            if ($this->isCarRegUpdate($data)) {
                $this->_updateCarReg($ticketId, $data->info);
            }
        }
        if ($retObj->id !== 0) {
            Entry4UISuccess($retObj);
        }
        Entry4UIError(6610, 'No update parameters passed to update');
    }

    private function _updateCarReg($ticketId, $reg) {
        $sql = 'update ' . E4S_TABLE_TICKET . "
                set info = '" . $reg . "'
                where id = " . $ticketId;
        e4s_queryNoLog($sql);
    }

    private function isCarRegUpdate($data): bool {
        if ($data->id === 0) {
            // is a car reg ?
            if (isset($data->info) and $data->info !== '') {
                return TRUE;
            }
        }
        return FALSE;
    }

    private function updateAllocation($ticketRow, $data, $onSite) {
        if (!is_null($onSite)) {
            if ($onSite === FALSE) {
                $onSite = '0';
            } else {
                $onSite = '1';
            }
        }

        if ($this->isCarRegUpdate($data)) {
            // is a car reg ?
            return $data;
        }
        $ticketId = $ticketRow['id'];
        $ticketAllocation = new ticketAllocationClass($data->id);
        $this->allocation = $ticketAllocation->process($data);
        $update = FALSE;

        if ((int)$ticketRow['allocationId'] !== $this->allocation->id) {
            $update = TRUE;
        }
        if ($ticketRow['onSite'] !== $onSite and !is_null($onSite)) {
            $update = TRUE;
            $this->_writeOnSiteAudit($ticketId, $onSite);
        }

        if ($update) {

            // tickets allocation id has changed
            $sql = 'update ' . E4S_TABLE_TICKET . '
                    set allocationid = ' . $this->allocation->id;
            if (!is_null($onSite)) {
                $sql .= ', onSite = ' . $onSite;
            }
            $sql .= ' where id = ' . $ticketId;
            $result = e4s_queryNoLog($sql);

        }

        return $this->allocation;
    }

    private function _writeOnSiteAudit($ticketId, $onSite) {
        $action = 'On Site';
        if (!$onSite or (int)$onSite === 0) {
            $action = 'Off Site';
        }
        $this->_writeAudit($action, $ticketId);
    }

    private function _writeAudit($action, $ticketId = null) {
        if (is_null($ticketId)) {
            $ticketId = $this->ticketId;
        }
        $sql = 'insert into ' . E4S_TABLE_TICKETAUDIT . ' (ticketid, action, userid)
                values (' . $ticketId . ",'" . $action . "'," . e4s_getUserID() . ')';
        e4s_queryNoLog($sql);
    }

    private function updateOnSite($ticketId, $onSite) {
        if ($onSite === FALSE) {
            $onSite = '0';
        } else {
            $onSite = '1';
        }
        $sql = 'update ' . E4S_TABLE_TICKET . '
                set onSite = ' . $onSite . '
                where id = ' . $ticketId;
        e4s_queryNoLog($sql);
        $this->_writeOnSiteAudit($ticketId, $onSite);
        if ($onSite === '0') {
            $onSite = FALSE;
        } else {
            $onSite = TRUE;
        }
        return $onSite;
    }

    public function getCarRegForItem($download, $item, $element) {
        if (array_key_exists('download_name', $download)) {
            $dName = $download['download_name'];
            if (e4s_isCarPark($dName)) {
                $carRegs = $item->get_meta(E4S_CAR_REG);
                $carRegs = preg_split('~,~', $carRegs);
                return $carRegs[$element - 1];
            }
        }
        return '';
    }

    public function createTicketInfo($download, $item, $exist = FALSE) {
        if (array_key_exists('order_id', $download)) {
            $orderInfo = $this->_getOrderCustomerInfo(E4S_ORDER_ID, $download['order_id']);
        } else {
            $orderInfo = $this->_getOrderCustomerInfo(E4S_ORDER_PWD, $download['order_key']);
        }

        if ($orderInfo->orderStatus === WC_ORDER_FAILED) {
            return null;
        }
        $obj = new stdClass();
        $obj->orderId = $orderInfo->orderId;
        $obj->itemId = $download['item_id'];
        $obj->element = $download['ticket_count'];
        $obj->userId = $orderInfo->user->id;
        $obj->compId = -1;
        $obj->info = $this->getCarRegForItem($download, $item, $obj->element);

        if (array_key_exists('comp_id', $download)) {
            $obj->compId = $download['comp_id'];
            $compObj = e4s_GetCompObj($obj->compId);
            if (!$compObj->ticketsEnabled()) {
                return null;
            }
        }
        $obj->athleteId = -1;
        if (array_key_exists('athlete_id', $download)) {
            $obj->athleteId = $download['athlete_id'];
        }
        $obj->linkedAthlete = '';
        if (array_key_exists('linked_athlete', $download)) {
            $obj->linkedAthlete = $download['linked_athlete'];
        }

        $ticketGuids = $this->_createDBTickets($obj, $item, $exist);

        if (is_null($ticketGuids)) {
            // return null is possible if the athlete has entered multiple events. Only give ticket to first event
//            Entry4UIError(9200,"Failed to create/Read Ticket URL");
            return null;
        }
        if ($obj->compId < 0) {
            Entry4UIError(9209, 'Failed to create/Read Ticket URL');
//            return null;
        }

        return $this->_getTicketURLs($obj->compId, $ticketGuids);
    }

    public function _getOrderCustomerInfo($type, $orderKey = null) {
        if (is_null($orderKey) and $type === E4S_ORDER_PWD) {
            $orderKey = $this->orderPWD;
        }
        $retObj = new stdClass();
        $retObj->orderId = 0;
        $retObj->orderUserId = 0;
        if ($type === E4S_ORDER_PWD) {
            $sql = 'select pm2.post_id orderId, pm2.meta_value userId, p.post_status orderStatus
                from ' . E4S_TABLE_POSTMETA . ' pm1,
                     ' . E4S_TABLE_POSTMETA . ' pm2,
                     ' . E4S_TABLE_POSTS . " p
                where pm1.meta_value = '" . $orderKey . "'
                and   pm1.post_id = pm2.post_id
                and   pm2.meta_key = '_customer_user'
                and   pm1.post_id = p.id";
        } else {
            // $type = E4S_ORDER_ID
            $sql = 'select pm.post_id orderId, pm.meta_value userId, p.post_status orderStatus
                from ' . E4S_TABLE_POSTMETA . ' pm,
                     ' . E4S_TABLE_POSTS . ' p
                where pm.post_id = ' . $orderKey . "
                and   pm.meta_key = '_customer_user'
                and   pm.post_id = p.id";
        }

        $result = e4s_queryNoLog($sql);

        if ($result->num_rows === 1) {
            $row = $result->fetch_assoc();
            $retObj->orderId = (int)$row['orderId'];
            $retObj->user = $this->_getUser((int)$row['userId']);
            $retObj->orderStatus = $row['orderStatus'];
            $this->orderUser = $retObj->user;
            $this->orderId = $retObj->orderId;
            $this->orderPWD = $orderKey;
        }
//        echo "<br>_getOrderCustomerInfo<br>";
//        var_dump($retObj);
        return $retObj;
    }

    private function _createDBTickets($obj, $wcItem, $exist = FALSE) {
        $attribs = e4sTicket::getAttribsFromWcItem($wcItem);

        $seatsRequired = 0;
        $obj->seatsRequired = $seatsRequired;
        $attribSeatKey = strtolower(E4S_SEAT_KEY);
        $attribSeatKey2 = E4S_SEAT_KEY2;
        if (array_key_exists($attribSeatKey, $attribs)) {
            $seatsRequired = (int)$attribs[$attribSeatKey] - 1;
        } else {
            if (array_key_exists($attribSeatKey2, $attribs)) {
                $seatsRequired = (int)$attribs[$attribSeatKey2];
            }
        }

//Get the Ticket Guid Object
        $guidObj = new e4sTicketGuid($obj);

        if (isset($obj->athleteId) and $obj->athleteId > 0) {
            $where = 'compid = ' . $obj->compId . ' and athleteid = ' . $obj->athleteId;
            $ticketResult = $this->_readDBTickets($where, FALSE);
        } else {
            $where = ' orderId = ' . $obj->orderId . '
                   and itemId = ' . $obj->itemId . '
                   and element = ' . $obj->element;
            $ticketResult = $this->_readDBTickets($where, FALSE);
        }

        $ticketRow = null;
        if ($ticketResult->num_rows > 0) {
            $ticketRow = $ticketResult->fetch_assoc();
        }

        if ($seatsRequired === 0) {
            if (is_null($ticketRow)) {
                $ticketId = $this->insertTicket($obj);
                $guidObj->setTicketId($ticketId);
            } elseif ($obj->orderId === (int)$ticketRow['orderId'] and $obj->itemId === (int)$ticketRow['itemId'] and $obj->element === (int)$ticketRow['element']) {
                $guidObj->setTicketId((int)$ticketRow['id']);
            }
            // will return null if no ticketid set
            return [$guidObj->getFullGuid()];
        } else {
            $seatingObj = new e4sSeatingClass($obj);

            if (($seatsRequired !== $ticketResult->num_rows) or (!is_null($ticketRow) and (int)$ticketRow['orderId'] !== $obj->orderId)) {
                // will need to update stadium seating status
                // athlete entered to this event prev. delete it so the ticket always holds the latest order/item ( Allows for failed payments )
                $deleteSql = 'delete from ' . E4S_TABLE_TICKET . '
                              where ' . $where;
                e4s_queryNoLog($deleteSql);
                $ticketRow = null;
            }
            // -1 as the ticket includes 1 for the athlete !

            if (is_null($ticketRow)) {
                $stadiumSeatInfo = $seatingObj->getSeatAllocation($wcItem, $exist);

                $obj->stadiumSeatId = $stadiumSeatInfo->stadiumSeatId;
                $obj->seatNo = $stadiumSeatInfo->seatNo;

                $fullGuids = array();

                if ($seatsRequired === 4) {
                    $seatsRequired--;
                }
                while ($seatsRequired > 0) {

                    $ticketId = 0;
                    if (is_null($ticketRow)) {
                        $ticketId = $this->insertTicket($obj);
                    } elseif ($obj->orderId === (int)$ticketRow['orderId'] and $obj->itemId === (int)$ticketRow['itemId'] and $obj->element === (int)$ticketRow['element']) {
                        $ticketId = (int)$ticketRow['id'];
                        // check there is not another row that should be used
                        $ticketRow = $ticketResult->fetch_assoc();
                    }

                    if ($ticketId !== 0) {
                        $guidObj->setTicketId($ticketId);
                        $fullGuids[] = $guidObj->getFullGuid();
                        $obj->seatNo++;
                        $seatsRequired--;
                    }
                }
            } else {

                $obj->stadiumSeatId = (int)$ticketRow['stadiumSeatId'];
                $guidObj->setTicketId($ticketRow['id']);
                $fullGuids[] = $guidObj->getFullGuid();
                while ($ticketRow = $ticketResult->fetch_assoc()) {
                    $guidObj->setTicketId($ticketRow['id']);
                    $fullGuids[] = $guidObj->getFullGuid();
                }
            }
            // will return null if no ticketid set
            if (sizeof($fullGuids) > 0) {
                $seatingObj->markSeatsPaidForId($obj->stadiumSeatId);
                return $fullGuids;
            }
        }
        return null;
    }

    public static function getAttribsFromWcItem($wcItem) {
        $attribs = array();
        foreach ($wcItem->get_formatted_meta_data('-_-', TRUE) as $meta) {
            if ($meta->key === E4S_ATTRIB_ATHLETE or $meta->key[0] !== '_') {
                $value = wp_kses_post($meta->value);
                $value = trim($value);
                $key = $meta->key;

                $uKey = str_replace(' ', '-', strtoupper($key));

                if ($uKey === E4S_SEAT_KEY) {
                    $value = (int)$value;
                }

                if ($meta->key === E4S_ATTRIB_ATHLETE) {
                    $key = 'Athlete';
                }
                $attribs[$key] = $value;
            }
        }
        return $attribs;
    }

    public function insertTicket($obj) {
        $linkedAthlete = 0;
        if (isset($obj->linkedAthlete) and $obj->linkedAthlete !== '') {
            $linkedStr = $obj->linkedAthlete;
            $linkedArr = explode(':', $linkedStr);
            $linkedAthlete = $linkedArr[0];
        }
        if (!isset($obj->stadiumSeatId)) {
            $obj->stadiumSeatId = 0;
            $obj->seatNo = 0;
        }
        $info = '';
        if (isset($obj->info)) {
            $info = $obj->info;
        }
        $insertSql = 'insert into ' . E4S_TABLE_TICKET . '(orderid, itemid, compid, athleteid,linkedathleteid, element, stadiumSeatId, seatNo, info, userid)
                  values (' . $obj->orderId . ',' . $obj->itemId . ',' . $obj->compId . ',' . $obj->athleteId . ',' . $linkedAthlete . ',' . $obj->element . ',' . $obj->stadiumSeatId . ',' . $obj->seatNo . ",'" . $info . "'," . $obj->userId . ')';
        e4s_queryNoLog($insertSql);
        return (int)e4s_getLastID();
    }

    private function _getTicketURLs($compId, $ticketGuids) {

        if ($_SERVER['HTTPS'] !== '') {
            $http = 'https://';
        } else {
            $http = 'http://';
        }
        $urls = array();
        foreach ($ticketGuids as $ticketGuid) {
            $urls[] = $http . E4S_CURRENT_DOMAIN . E4S_TICKET_BUTTON_URL . $compId . '/' . $ticketGuid;
        }
        return $urls;
    }

//Change the WC download to an E4STicket Download link

    public function readTicket($obj, $scanned = FALSE) {
        $userId = $this->curUser->id;
        if ($userId === E4S_USER_NOT_LOGGED_IN or $userId === E4S_USER_ANON_ID) {
            if (!e4sTicket::isPublicRead()) {
                return $this->_outputToUser('Please logon to gain access to this ticket.');
            }
        }
        $action = 'Scanned';
        $this->athleteId = null;
        if (!$scanned) {
            $action = 'Read';
            $this->athleteId = checkFieldForXSS($obj, 'athleteid:Athlete ID');
        }
        $this->compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
        if (is_null($this->compId)) {
            return $this->_outputToUser('1: No Competition reference found');
        }
        $this->compId = (int)$this->compId;
		$this->_setTicketCompId();

        $guidObj = null;
        $fullGuid = checkFieldForXSS($obj, 'guid:Ticket GUID');

        if (!is_null($fullGuid) and $fullGuid !== '') {
            $guidObj = new e4sTicketGuid($fullGuid);
        }
        if (is_null($this->athleteId)) {
            if (is_null($guidObj) or $guidObj->ticketId === 0) {
                return $this->_outputToUser('1: Invalid or non-existant ticket id passed');
            }

            $this->guid = $guidObj->getFullGuid();
            $where = 't.id = ' . $guidObj->ticketId;
        } else {
            $this->guid = null;
            $where = 't.athleteid = ' . $this->athleteId . ' and t.compid = ' . $this->compId;
        }
        if ($where === '') {
            return $this->_outputToUser('Unable to find a ticket with given information');
        }

        $row = $this->_readDBTickets($where);
        if (is_null($row)) {
            return $this->_outputToUser('2: Invalid or non-existant ticket id passed');
        }

        $this->_writeAudit($action, $row['id']);
        if (e4sTicketGuid::getGuidFromFullGuid($fullGuid) !== $row['guid']) {
            return $this->_outputToUser('3: Invalid or non-existant ticket id passed.', $row['guid'], $guidObj->getGuidOnly());
        }

        $retObj = $this->_getDataForRow($row);
        if (is_null($retObj)) {
            Entry4UIError(1000, 'No Ticket Found');
        }

        if (!isset($retObj->user->ticketMgr)) {
            $retObj->user->ticketMgr = $this->_getTicketoperative();
        }
        if ($retObj->user->ticketMgr) {
            $this->_updateCount($retObj->id, $scanned);
        }
        // Not required
        unset($retObj->user->ticketMgr);
        $meta = $this->getMetaInfo();

        Entry4UISuccess('
            "data":' . e4s_encode($retObj) . ',
            "meta":' . e4s_encode($meta));
    }

    private function _updateCount($ticketId, $scanned) {
        $field = 'scannedCount';
        if (!$scanned) {
            $field = 'searchCount';
        }
        $sql = 'update ' . E4S_TABLE_TICKET . '
                set ' . $field . ' = ' . $field . ' + 1
                where id = ' . $ticketId;
        e4s_queryNoLog($sql);
    }

    public function setDownloadLinks($downloads) {
//        echo "<br></br>setDownloadLinks<br>";
//        var_dump($downloads);
        $retDownloads = array();
        // E4S Tickets
        foreach ($downloads as $download) {
            $file = $download['file']['file'];
            $file = explode('/', $file);
            $file = $file[sizeof($file) - 1];

            if (strcasecmp($file, E4S_TICKET) === 0) {
                $downloadUrl = $download['download_url'];
                $arr = explode('/?', $downloadUrl);
                $download['download_url'] = $arr[0] . '/wp-json/' . E4S_NS . '/' . E4S_TICKET_ROUTE . '/generate?_' . $arr[1];
                $product = wc_get_product($download['product_id']);
                $download['download_name'] = $download['product_name'];
                $descObj = e4s_getWCProductDescObj($product);
                if (isset($descObj->name)) {
                    $download['product_name'] = $descObj->name;
                } else {
                    $download['product_name'] = $product->get_description();
                }
                if (isset($descObj->ticket)) {
                    $download['product_name'] .= ' : ' . $descObj->ticket;
                }

                $download['product_url'] = '';

            }
            $retDownloads[] = $download;
        }

        return $retDownloads;
    }

    private function _readParmsFromInfo($info) {
        $parms = explode('&', $info);
        $paramArr = array();
        foreach ($parms as $parm) {
            $arr = explode('=', $parm);
            $paramArr[$arr[0]] = $arr[1];
        }
        return $paramArr;
    }
}

class e4sTicketGuid {
    public $ticketId;
    public $guid;
    public $orderId;
    public $element;
    public $itemId;
    public $seatsRequired;

    public function __construct($guid, $ticketId = 0) {
        $this->ticketId = $ticketId;
        $this->orderId = null;
        $this->itemId = null;
        $this->element = null;
        $this->guid = null;
        $type = gettype($guid);
        if ($type === 'string' and $guid !== '') {
            if (strpos($guid, E4S_TICKET_DIVIDER) !== FALSE) {
                $this->ticketId = e4sTicketGuid::getTicketFromFullGuid($guid);
                $this->guid = e4sTicketGuid::getGuidFromFullGuid($guid);
            } else {
                $this->guid = $guid;
            }
        }
        if ($type === 'object') {

            if (isset($guid->ticketId)) {
                $this->ticketId = $guid->ticketId;
            }
            if (isset($guid->orderId)) {
                $this->orderId = $guid->orderId;
            }
            if (isset($guid->itemId)) {
                $this->itemId = $guid->itemId;
            }
            if (isset($guid->element)) {
                $this->element = $guid->element;
            }
            if (isset($guid->seatsRequired)) {
                $this->seatsRequired = $guid->seatsRequired;
            }
            $this->_makeGUID();
        }
    }

    public static function getTicketFromFullGuid($fullGuid) {
        $guidArr = explode(E4S_TICKET_DIVIDER, $fullGuid);
        return (int)$guidArr[0];
    }

    public static function getGuidFromFullGuid($fullGuid) {
        $guidArr = explode(E4S_TICKET_DIVIDER, $fullGuid);
        return $guidArr[1];
    }

    private function _makeGUID($orderId = null, $itemId = null, $element = null) {
        if (is_null($orderId)) {
            $orderId = $this->orderId;
        }
        if (is_null($orderId)) {
            $orderId = -1;
        }
        if (is_null($itemId)) {
            $itemId = $this->itemId;
        }
        if (is_null($itemId)) {
            $itemId = -1;
        }
        if (is_null($element)) {
            $element = $this->element;
        }
        if (is_null($element)) {
            $element = -1;
        }

        if (is_null($orderId) or is_null($itemId) or is_null($element)) {
            $this->guid = null;
        } else {
            $code = $orderId . E4S_TICKET_DIVIDER . $itemId . E4S_TICKET_DIVIDER . $element . E4S_TICKET_DIVIDER . $this->ticketId;
            $this->guid = md5($code);
        }
        return $this->guid;
    }

    public static function getSQL($table = 't') {
        return "md5( concat(orderid,'" . E4S_TICKET_DIVIDER . "',itemId,'" . E4S_TICKET_DIVIDER . "', element ,'" . E4S_TICKET_DIVIDER . "', " . $table . '.id) )';
    }

    public function setTicketId($id) {
        $this->ticketId = $id;
    }

    public function getFullGuid($ticketId = null, $guid = null) {
        if (is_null($ticketId)) {
            $ticketId = $this->ticketId;
        }
        if (is_null($guid)) {
//            $guid = $this->guid;
            $guid = $this->_makeGUID();
        }

        if ($ticketId === 0 or is_null($guid)) {
            return null;
        }
        return $ticketId . E4S_TICKET_DIVIDER . $guid;
    }

    public function getAllGuids() {
        $guids = array();
        $seatsRequired = $this->seatsRequired;
        while ($seatsRequired > 0) {
            $this->_makeGUID(null, null, null);
            $guids[] = $this->getGuidOnly();
            $seatsRequired--;
        }
        return $guids;
    }

    public function getGuidOnly() {
        return $this->guid;
    }
}