<?php
const E4S_FORMAT_NOCASE = '';
const E4S_FORMAT_UCASE = 'ucase';
const E4S_FORMAT_LCASE = 'lcase';
const E4S_FORMAT_PCASE = 'pcase';

const E4S_USERATHLETES_OBJ = 'e4sUserAthletes';
class e4sUserAthletes extends e4sTableClass {
	public function __construct() {
		$this->optionsSet = true;// no options required currently
		$this->setInteger('id');
		$this->setInteger('userid', 'userId');
		$this->setInteger('athleteid', 'athleteId');
	}
}
const E4S_ATHLETEPERFOBJ = 'e4sAthletePerf';
class e4sAthletePerf extends e4sTableClass {
	public function __construct() {
		$this->optionsSet = true;// no options required currently
		$this->setInteger('id');
		$this->setInteger('athleteid', 'athleteId');
		$this->setInteger('eventid', 'eventId');
		$this->setFloat('perf');
	}
}
const E4S_ATHLETERANK_OBJ = 'e4sAthleteRank';
class e4sAthleteRank extends e4sTableClass {
	public function __construct() {
		$this->optionsSet = true; // no options required currently
		$this->setInteger('id');
		$this->setInteger('athleteid', 'athleteId');
		$this->setInteger('eventid', 'eventId');
		$this->setInteger('rank');
		$this->setInteger('year');
		$this->setString('info');
	}
}
const E4S_COMBINEDCALC_OBJ = 'e4sCombinedCalc';
class e4sCombinedCalc extends e4sTableClass{
    public function __construct() {
        $this->optionsSet = true; // no options required currently
        $this->setInteger('id');
        $this->setInteger('eventId');
        $this->setInteger('calcNo');

        $this->setFloat('a');
        $this->setFloat('b');
        $this->setFloat('c');
        $this->setFloat('minVal');
        $this->setFloat('maxVal');
    }
}

const E4S_NEXTUP_OBJ = 'e4sNextUp';
class e4sNextUp extends e4sTableClass{
    public function __construct() {
        $this->optionsSet = true; // no options required currently
        $this->setInteger('egId');
        $this->setInteger('athleteId');
        $this->setInteger('heatNo');
        if ( isset($this->compId) ){
            $this->setInteger('compId');
        } elseif ( isset($this->compid) ){
            $this->setInteger('compid', 'compId');
        }
        $this->setString('firstName');
        $this->setString('surName');
    }
}
const E4S_ADVERT_OBJ = 'e4sAdvert';
class e4sAdvert extends e4sTableClass{
    public function __construct() {
        $this->optionsSet = true; // no options required currently
        $this->setInteger('id');
        $this->setInteger('orgid', 'orgId');
        $this->setString('filepath', 'filePath');
    }
}
const E4S_PHOTOFINISH_OBJ = 'e4sPhotofinish';
class e4sPhotofinish extends e4sTableClass {
    public function __construct() {
        $this->optionsSet = true; // no options required currently
        $this->setInteger('id');
        $this->setInteger('compId', 'compId');
        $this->setString('filename');
    }
}

const E4S_ATHLETE_OBJ = 'e4sAthlete';
class e4sAthlete extends e4sTableClass {
    public function __construct() {
        $this->setInteger('id');
        $this->setInteger('pof10id', 'pof10Id');
        $this->setInteger('classification');
        $this->setInteger('clubid', 'clubId');
        $this->setInteger('club2id', 'club2Id');
        $this->setInteger('schoolid', 'schoolId');

        $this->setString('aocode', 'aoCode');
    }
}
const E4S_COMBINEDPOINTS_OBJ = 'e4sCombinedPoints';
class e4sCombinedPoints extends e4sTableClass {
    public function __construct() {
        $this->setInteger('id');
        $this->setInteger('eventId');
        $this->setInteger('ageGroupId');
        $this->setInteger('combinedId');
        $this->setInteger('points');
        $this->setFloat('result');
    }
}
const E4S_AREA_OBJ = 'e4sArea';
class e4sArea extends e4sTableClass {
    public function __construct() {
        $this->setInteger('id');
        $this->setInteger('entityid', 'entityId');
        $this->setString('shortname', 'shortName');
        $this->setString('name', 'name', E4S_FORMAT_PCASE);
    }
}
const E4S_CLUB_OBJ = 'e4sClub';
class e4sClub extends e4sTableClass {
    public function __construct() {
        $this->setInteger('id');
        $this->setInteger('areaid', 'areaId');
        $this->setBoolean('active');
        $this->setString('externid', 'externId');
        $this->setString('Clubname', 'clubName', E4S_FORMAT_PCASE);
        $this->setString('shortcode', 'shortCode');
        $this->setString('Region', 'region');
        $this->setString('Country', 'country');
        $this->setString('clubtype', 'clubType');
    }
}
const E4S_STANDARD_OBJ = 'e4sStandards';
class e4sStandards extends e4sTableClass {
    public function __construct() {
        $this->optionsSet = true; // no options required currently
        $this->setInteger('id');
    }
}
const E4S_STANDARD_VALUES_OBJ = 'e4sStandardValues';
class e4sStandardValues extends e4sTableClass {
    public function __construct() {
        $this->optionsSet = true; // no options required currently
        $this->setInteger('id');
        $this->setInteger('standardId');
        $this->setInteger('eventId');
        $this->setInteger('ageGroupId');
        $this->setFloat('value');
    }
}
const E4S_CLUBCOMP_OBJ = 'e4sClubComp';
class e4sClubComp extends e4sTableClass {
    public function __construct() {
        $this->setInteger('id');
        $this->setInteger('clubId');
        $this->setInteger('compId');
        $this->setInteger('categoryId');
    }
}
const E4S_CLUBCOMPCAT_OBJ = 'e4sClubCompCat';
class e4sClubCompCat extends e4sTableClass {
    public function __construct() {
        $this->setInteger('id');
        $this->setInteger('maxEntries');
        $this->setInteger('maxRelays');
    }
}
const E4S_OPTIONS_OBJ = 'e4sOptions';
class e4sOptions extends e4sTableClass {
    public function __construct() {
        $this->setInteger('option_id');
    }
}
const E4S_ENTRYINFO_OBJ = 'e4sEntryInfo';
class e4sEntryInfo extends e4sTableClass
{
    public function __construct()
    {
        $this->setInteger('entryId');
        $this->setInteger('classification');
        $this->setInteger('athleteId');
        $this->setInteger('productId');
        $this->setInteger('orderId');
        $this->setInteger('paid');
        $this->setInteger('uomId');
        $this->setInteger('compId');
        $this->setInteger('ceId');
        $this->setInteger('maxAge');
        $this->setInteger('userId');
        $this->setInteger('ageGroupId');
        $this->setInteger('clubId');
        $this->setInteger('isOpen');
        $this->setInteger('mulitId');
        $this->setInteger('egId');
        $this->setInteger('forceClose');
        $this->setInteger('checkedin', 'checkedIn');

        $this->setFloat('price');
        $this->setFloat('split');
        $this->setFloat('entryPb');

        $this->setString('clubName');
    }
}
const E4S_EVENTRESULTS_OBJ = 'e4sResults';
class e4sResults extends e4sTableClass
{
    public function __construct()
    {
        $this->setInteger('id');
        $this->setInteger('resultheaderid', 'rhId');
        $this->setInteger('lane');
        $this->setInteger('position');
        $this->setInteger('athleteid', 'athleteId');
        $this->setInteger('pointsId');
        $this->setInteger('eaAward');

        $this->setFloat('score');

        $this->setString('wind');
        $this->setString('athlete');
        $this->setString('club');
        $this->setString('bibno', 'bibNo');
        $this->setString('agegroup', 'ageGroup');
        $this->setString('qualify');
        $this->setString('scoreText');
        $this->setString('gender');
    }
}
const E4S_EVENTRESULTSHEADER_OBJ = 'e4sResultsHeader';
class e4sResultsHeader extends e4sTableClass
{
    public function __construct()
    {
        $this->setInteger('id', 'id');
        $this->setInteger('egid', 'egId');
        $this->setInteger('heatno', 'heatNo');
    }
}
const E4S_EAAWARDS_OBJ = 'e4sEaAwards';
class e4sEaAwards extends e4sTableClass {
    public function __construct() {
        $this->setInteger('id', 'id');
        $this->setInteger('eventId', 'eventId');
        $this->setInteger('ageGroupId', 'ageGroupId');
        $this->setInteger('level', 'level');

        $this->setFloat('score', 'score');
    }
}
const E4S_POST_OBJ = 'e4sPosts';
class e4sPosts extends e4sTableClass {
    public function __construct() {
        $this->setInteger('ID', 'id');
        $this->setInteger('menu_order');
        $this->setInteger('post_author');
        $this->setInteger('post_parent');
        $this->setInteger('comment_count');
    }
}
const E4S_AGEGROUP_OBJ = 'e4sAgeGroup';
class e4sAgeGroup extends e4sTableClass {
    public function __construct() {
        $this->setInteger('id', 'id');
        $this->setInteger('MaxAge', 'maxAge');
        $this->setInteger('AtDay', 'atDay');
        $this->setInteger('AtMonth', 'atMonth');
        $this->setInteger('year', 'year');
        $this->setInteger('minAge', 'minAge');
        $this->setInteger('minAtDay', 'minAtDay');
        $this->setInteger('minAtMonth', 'minAtMonth');
        $this->setInteger('minYear', 'minYear');

        $this->setString('Name', 'name');
    }
}
const E4S_USERMETA_OBJ = 'e4sUserMeta';
class e4sUserMeta extends e4sTableClass {
    public function __construct() {
        $this->optionsSet = true; // no options required
        $this->setInteger('umeta_id', 'id');
        $this->setInteger('user_id', 'userId');

        $this->setString('meta_key', 'key');
        $this->setString('meta_value', 'value');
    }
}
const E4S_USER_OBJ = 'e4sUser';
class e4sUser extends e4sTableClass {
    public function __construct() {
        $this->setInteger('ID', 'id');
        $this->setInteger('user_status', 'userStatus');

        $this->setString('user_login', 'userLogin');
        $this->setString('user_pass', 'userPassword');
        $this->setString('user_nicename', 'userName');
        $this->setString('user_email', 'userEmail');
        $this->setString('user_url', 'userUrl');
        $this->setString('user_activation_key', 'userKey');
        $this->setString('display_name', 'userDisplayName');
    }
}
const E4S_CONFIG_OBJ = 'e4sConfig';
class e4sConfig extends e4sTableClass {
    public function __construct() {
        $this->setInteger('id');

        $this->setBoolean('public');

        $this->setString('systemName');
        $this->setString('theme');
        $this->setString('env');
    }
}
const E4S_BIB_OBJ = 'e4sBib';
class e4sBib extends e4sTableClass {
    public function __construct() {
        $this->setInteger('id');
        $this->setInteger('compid', 'compId');
        $this->setInteger('athleteid', 'athleteId');
        $this->setInteger('bibno', 'bibNo');

        $this->setBoolean('collected');
        $this->setString('collectedTime', 'collectedTime');
        $this->setString('notes');
    }
}
const E4S_EVENTGENDER_OBJ = 'e4sEventGender';
class e4sEventGender extends e4sTableClass {
    public function __construct() {
        $this->setInteger('id');
        $this->setInteger('eventid', 'eventId');
        $this->setInteger('min');
        $this->setInteger('max');

        $this->setString('gender');
    }
}
const E4S_EVENT_OBJ = 'e4sEvent';
class e4sEvent extends e4sTableClass {
    public function __construct() {
        $this->setInteger('ID', 'id');
        $this->setInteger('uomid', 'uomId');
        $this->setFloat('min');
        $this->setFloat('max');

        $this->setString('Name', 'name');
        $this->setString('tf');
        $this->setString('eventType');
        $this->setString('Gender', 'gender');
    }
}
const E4S_COMPEVENT_OBJ = 'e4sCompEvent';
class e4sCompEvent extends e4sTableClass {
    public function __construct() {
        $this->setInteger('ID', 'id');
        $this->setInteger('CompID', 'compId');
        $this->setInteger('EventID', 'eventId');

        $this->setInteger('AgeGroupID', 'ageGroupId');
        $this->setInteger('PriceID', 'priceId');
        $this->setInteger('maxathletes', 'maxAthletes');
        $this->setInteger('maxGroup', 'egId');
        $this->setFloat('split');
        $this->setBoolean('IsOpen', 'isOpen');
        $this->setString('startdate', 'startDate');
        $this->setString('sortdate', 'sortDate');
        $this->setString('Gender', 'gender');
    }
}
const E4S_ENTRY_OBJ = 'e4sEntry';
class e4sEntry extends e4sTableClass {
    public function __construct() {
        $this->setInteger('id');
        $this->setInteger('compEventID', 'compEventId');
        $this->setInteger('bibno', 'bibNo');
        $this->setInteger('athleteid', 'athleteId');
        $this->setInteger('variationID', 'productId');
        $this->setInteger('orderid', 'orderId');
        $this->setInteger('clubid', 'clubId');
        $this->setInteger('discountid', 'discountId');
        $this->setInteger('teamid', 'teamId');
        $this->setInteger('ageGroupID', 'ageGroupId');
        $this->setInteger('vetAgeGroupID', 'vetAgeGroupId');
        $this->setInteger('waitingPos');
        $this->setInteger('entryPos');
        $this->setInteger('userid', 'userId');
        $this->setInteger('paid');
        $this->setInteger('trackedPB');

        $this->setFloat('price');
        $this->setFloat('pb');

        $this->setBoolean('confirmed');
        $this->setBoolean('present');
        $this->setBoolean('checkedin', 'checkedIn');

        $this->setString('clubname', 'clubName');
        $this->setString('teambibno','teamBibNo');
    }

    public function getWriteOptions($options = null) {
        if (is_null($options)) {
            $options = $this->options;
        }
        return e4s_removeDefaultEntryOptions($options);
    }
}
const E4S_EVENTGROUP_OBJ = 'e4sEventGroup';
class e4sEventGroup extends e4sTableClass {
    public function __construct() {
        $this->setInteger('id');
        $this->setInteger('compId');
        $this->setInteger('eventNo');
        $this->setInteger('forceClose');
        $this->setInteger('bibSortNo');
        $this->setInteger('parentEgId');

        $this->setInteger('eventDefId');
        $this->setString('eventDefName');

        $eventDefObj = new stdClass();
	    if ( isset($this->eventDefId) ) {
		    $eventDefObj->id   = $this->eventDefId;
		    $eventDefObj->name = $this->eventDefName;

		    if ( isset( $this->eventOptions ) ) {
			    $eventDefObj->options = e4s_getOptionsAsObj( $this->eventOptions );
		    }
	    }
        $this->eventDef = $eventDefObj;
        unset($this->eventOptions);
        unset($this->eventDefId);
        unset($this->eventDefName);
        $this->setString('typeNo');
        $this->setString('name');
        $this->setString('notes');
        $this->setString('startdate', 'startDate');
    }
}

class e4sTableClass {
    public $optionsSet = FALSE;

    public function returnObj() {
        $obj = e4s_deepCloneToObject($this);
        unset($obj->optionsSet);
        return $obj;
    }

    public function setInteger($oldName, $newName = '') {
	    if (!isset($this->$oldName)) {
	        return;
	    }
        if ($newName === '') {
            $newName = $oldName;
        }

        if (!is_null($this->$oldName)) {
            $this->$newName = (int)$this->$oldName;
        } else {
            $this->$newName = null;
        }
        $this->_unset($oldName, $newName);
        $this->_checkOptions();
    }

    private function _unset($oldName, $newName) {
        if ($newName !== $oldName) {
            unset($this->$oldName);
        }
    }

    private function _checkOptions() {
        if ($this->optionsSet) {
            return;
        }
        $this->optionsSet = TRUE;
        if (isset($this->options)) {
            switch (get_class($this)) {
                case E4S_CONFIG_OBJ:
                    $this->options = e4s_addDefaultConfigOptions($this->options);
                    break;
                case E4S_EVENTGROUP_OBJ:
                    $this->options = e4s_addDefaultEventGroupOptions($this->options);
                    break;
                case E4S_ENTRY_OBJ:
                    $this->options = e4s_addDefaultEntryOptions($this->options);
                    break;
                case E4S_COMPEVENT_OBJ:
                    $this->options = e4s_addDefaultCompEventOptions($this->options);
                    break;
                case E4S_EVENTRESULTSHEADER_OBJ:
                    $this->options = e4s_addDefaultResultsHeaderOptions($this->options);
                    break;
                case E4S_EVENT_OBJ:
                    $this->options = e4s_getOptionsAsObj($this->options);
                    $this->uomOptions = e4s_getOptionsAsObj($this->uomOptions);
                    break;
                default:
                    $this->options = e4s_getOptionsAsObj($this->options);
                    break;
            }
        } else {
            switch (get_class($this)) {
                case E4S_POST_OBJ:
                    if (strpos($this->post_excerpt, '"guid"') > 0) {
                        $this->post_excerpt = json_decode($this->post_excerpt);
                    }
                    break;
                case E4S_CONFIG_OBJ:
                    $this->options = e4s_getConfigDefaultOptions();
                    break;
                case E4S_EVENTGROUP_OBJ:
                    $this->options = e4s_getEventGroupV2DefaultOptions();
                    break;
                case E4S_ENTRY_OBJ:
                    $this->options = e4s_getEntryDefaultOptions();
                    break;
                case E4S_COMPEVENT_OBJ:
                    $this->options = e4s_getCompEventDefaultOptions();
                    break;
                case E4S_EVENTRESULTSHEADER_OBJ:
                    $this->options = e4s_getResultsHeaderDefaultOptions();
                    break;
                case E4S_ENTRYINFO_OBJ:
					if ( isset($this->eOptions) ) {
						$this->eOptions = e4s_addDefaultEntryOptions( $this->eOptions );
					}
	                if ( isset($this->ceOptions) ) {
		                $this->ceOptions = e4s_addDefaultCompEventOptions( $this->ceOptions );
	                }
	                if ( isset($this->egOptions) ) {
		                $this->egOptions = e4s_addDefaultEventGroupV2Options( $this->egOptions );
	                }
	                if ( isset($this->uomOptions) ) {
		                $this->uomOptions = e4s_addDefaultUOMOptions( $this->uomOptions );
	                }
                    break;
                default:
                    $this->options = new stdClass();
                    break;
            }
        }
    }

    public function setFloat($oldName, $newName = '') {
	    if (!isset($this->$oldName)) {
		    return;
	    }
        if ($newName === '') {
            $newName = $oldName;
        }

        if (!is_null($this->$oldName)) {
            $this->$newName = (float)$this->$oldName;
        } else {
            $this->$newName = null;
        }
        $this->_unset($oldName, $newName);
        $this->_checkOptions();
    }

    public function setString($oldName, $newName = '', $format = E4S_FORMAT_NOCASE) {
	    if (!isset($this->$oldName)) {
		    return;
	    }
        if ($newName === '') {
            $newName = $oldName;
        }
        if (isset($this->$oldName)) {
            $this->$newName = $this->$oldName;
        } else {
            $this->$newName = null;
        }
        $this->_unset($oldName, $newName);
        if ($format !== E4S_FORMAT_NOCASE) {
            switch ($format) {
                case E4S_FORMAT_LCASE:
                    $this->$newName = strtolower($this->$newName);
                    break;
                case E4S_FORMAT_UCASE:
                    $this->$newName = strtoupper($this->$newName);
                    break;
                case E4S_FORMAT_PCASE:
                    $this->$newName = ucwords($this->$newName);
                    break;
            }
        }
        $this->_checkOptions();
    }

    public function setBoolean($oldName, $newName = '') {
        if ($newName === '') {
            $newName = $oldName;
        }
        if (isset($this->$oldName) and !is_null($this->$oldName)) {
            $this->$oldName = (int)$this->$oldName;
            $this->$newName = $this->$oldName === 1;
        } else {
            $this->$newName = null;
        }
        $this->_unset($oldName, $newName);
        $this->_checkOptions();
    }

    public function getWriteOptionsStr($options = null) {
        switch (get_class($this)) {
            case E4S_CONFIG_OBJ:
                $options = e4s_removeDefaultConfigOptions($this->options);
                break;
            case E4S_EVENTGROUP_OBJ:
                $options = e4s_removeDefaultEventGroupV2Options($this->options);
                break;
            case E4S_ENTRY_OBJ:
                $options = e4s_removeDefaultEntryOptions($this->options);
                break;
			case E4S_ENTRYINFO_OBJ:
                $options = e4s_removeDefaultEntryOptions($this->eOptions);
                break;
            case E4S_COMPEVENT_OBJ:
                $options = e4s_removeDefaultCompOptions($this->options);
                break;
            default:
                $options = $this->options;
                break;
        }
        return e4s_getOptionsAsString($options);
    }
}