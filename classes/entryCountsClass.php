<?php

class entryCountsClass {
    public $compId;
    public $compObj;
    public $entryCountsByAthleteAndEG;
    public $entryCountsByAthlete;
    public $countsByEG;
    public $egIds;
    public $egObjs;

    public function __construct($compId) {
        $this->compId = $compId;
        $this->compObj = e4s_GetCompObj($compId);
        $this->entryCountsByAthleteAndEG = null;
        $this->entryCountsByAthlete = null;
        $this->egObjs = array();
        $this->countsByEG;
        $this->_getCeIds();
    }

    private function _getCeIds() {
		$ceObjs = $this->compObj->getCeObjs();
        $egIds = array();
		foreach($ceObjs as $ceObj) {
			$egIds[(int)$ceObj->id] = (int)$ceObj->egId;
		}

        $this->egIds = $egIds;
    }

    public function getCountForAthlete() {
        if (is_null($this->entryCountsByAthlete)) {
            $this->_setEntryCounts();
        }
        return $this->entryCountsByAthlete;
    }

    private function _setEntryCounts() {
        $sql = "
        SELECT ce.maxathletes maxathletes,
               eg.id egid, 
               eg.options egOptions,
               eg.forceclose forceClose,
               e.id entryId,
               e.athleteid athleteid,
               e.paid paid,
               date_format(e.periodStart,'" . ISO_MYSQL_DATE . "') posDate,
               date_format(eg.startDate,'" . ISO_MYSQL_DATE . "') startDate,
               eg.eventNo eventNo,
               eg.typeNo typeNo,
               eg.bibSortNo bibSortNo,
               eg.name name,
               ce.isOpen isOpen,
               e.options eOptions
        FROM " . E4S_TABLE_ENTRIES . ' e,
             ' . E4S_TABLE_COMPEVENTS . ' ce,
             ' . E4S_TABLE_EVENTGROUPS . ' eg
        where e.compeventid = ce.ID
        and   ce.CompID = ' . $this->compId . '
        and   ce.maxgroup = eg.id
        order by eg.id,e.periodstart, e.athleteid
    ';
//        and   e.paid != " . E4S_ENTRY_QUALIFY . "
        $entryCountsByAthleteAndEG = array();
        $entryCountsByAthlete = array();
        $countsByEG = array();
        $result = e4s_queryNoLog($sql);
        $lastEG = 0;
        $pos = 1;

        while ($obj = $result->fetch_object()) {
            $paid = (int)$obj->paid;
            $egId = (int)$obj->egid;
            if (!array_key_exists($egId, $this->egObjs)) {
                $egOptions = e4s_addDefaultEventGroupOptions($obj->egOptions);
                $egObj = new stdClass();
                $egObj->options = $egOptions;
                $egObj->forceClose = $obj->forceClose;
                $egObj->startDate = $obj->startDate;
                $egObj->eventNo = $obj->eventNo;
                $egObj->typeNo = $obj->typeNo;
                $egObj->bibSortNo = $obj->bibSortNo;
                $egObj->name = $obj->name;
                $egObj->isOpen = $obj->isOpen;
                $this->egObjs [$egId] = $egObj;
            }
            $athleteId = (int)$obj->athleteid;
            if ($lastEG !== $egId) {
                $pos = 1;
                $lastEG = $egId;
            }
            if (!array_key_exists($egId, $countsByEG)) {
                $countsByEG[$egId] = $this->_createBlankEGObj($egId, $countsByEG);
            }
            $payload = new stdClass();
            $payload->pos = $pos;
            $payload->paid = (int)$obj->paid;
            $payload->max = (int)$obj->maxathletes;
            $payload->entryId = (int)$obj->entryId;
            $payload->entryOptions = $obj->eOptions;
            $payload->waitingPos = 0;
            if ($payload->max > 0) {
                $payload->waitingPos = $pos - $payload->max;
                if ($payload->waitingPos < 0) {
                    $payload->waitingPos = 0;
                }
            }
            if ($payload->waitingPos < 0) {
                $payload->waitingPos = 0;
            }
            $payload->posDate = $obj->posDate;
            $pos++;
            $key = $this->getKey($athleteId, $egId);
            $entryCountsByAthleteAndEG[$key] = $payload;
            if (!array_key_exists($athleteId, $entryCountsByAthlete)) {
                $entryCountsByAthlete[$athleteId] = array();
            }
            $entryCountsByAthlete[$athleteId][$egId] = $payload;
            $countsByEG[$egId]->totalCount = $countsByEG[$egId]->totalCount + 1;
            $countsByEG[$egId]->maxAthletes = $payload->max;
            if ($paid === E4S_ENTRY_PAID or $paid === E4S_ENTRY_QUALIFY) {
                $countsByEG[$egId]->paidCount = $countsByEG[$egId]->paidCount + 1;
            } else {
                $countsByEG[$egId]->unPaidCount = $countsByEG[$egId]->unPaidCount + 1;
            }
        }
        $this->entryCountsByAthleteAndEG = $entryCountsByAthleteAndEG;
        $this->entryCountsByAthlete = $entryCountsByAthlete;
        $this->countsByEG = $countsByEG;
    }

    private function _createBlankEGObj($egId, &$array) {
        if (!array_key_exists($egId, $array)) {
            $egObj = new stdClass();
            $egObj->totalCount = 0;
            $egObj->paidCount = 0;
            $egObj->unPaidCount = 0;
            $egObj->maxAthletes = 0;
            $array[$egId] = $egObj;
        }
        return $array[$egId];
    }

    public function getKey($athleteId, $egId) {
        return $athleteId . '-' . $egId;
    }

    public function isEventClosed($egId): bool {
        if ($this->isEventForcedClosed($egId)) {
            return TRUE;
        }
        return $this->isEventFull($egId);
    }

    public function isEventForcedClosed($egId): bool {
        if (is_null($this->egObjs) or sizeof($this->egObjs) === 0) {
            $this->_setEntryCounts();
        }
        if (!array_key_exists($egId, $this->egObjs)) {
            // creating a new event
            return FALSE;
        }
        $egObj = $this->egObjs[$egId];
        if ($egObj->forceClose) {
            return TRUE;
        }
        return FALSE;
    }

    public function isEventFull($egId): bool {
        $egCountObj = $this->getEGObj($egId);
        if (!is_null($egCountObj)) {
            if ($egCountObj->maxAthletes > 0) {
                if ($egCountObj->paidCount >= $egCountObj->maxAthletes) {
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

    public function getEGObj($egId) {
        $allEGCountObj = $this->getEntryCountsByEG($egId);
        if (array_key_exists($egId, $allEGCountObj)) {
            return $allEGCountObj[$egId];
        }
        return null;
    }

    public function getEntryCountsByEG() {
        if (is_null($this->countsByEG)) {
            $this->_setEntryCounts();
        }
        return $this->countsByEG;
    }

    public function getEntriesForEg($egId) {
        $egCounts = $this->getCountsByAthleteAndEG();
        $entries = array();
        foreach ($egCounts as $key => $countEntryObj) {
            $keyEgId = $this->_getEgIdFromKey($key);
            if ($keyEgId === $egId) {
                $entries[] = $countEntryObj;
            }
        }
        return $entries;
    }

    public function getCountsByAthleteAndEG() {
        if (is_null($this->entryCountsByAthleteAndEG)) {
            $this->_setEntryCounts();
        }
        return $this->entryCountsByAthleteAndEG;
    }

    private function _getEgIdFromKey($key) {
        $arr = preg_split('~-~', $key);
        return (int)$arr[1];
    }

    public function getEgInfo($egId) {
        $obj = new stdClass();
        $obj->egId = $egId;
        foreach ($this->egObjs as $id => $egObj) {
            if ($id === $egId) {
                $obj->options = $egObj->options;
                $obj->eventDateTimeISO = $egObj->startDate;
                $obj->typeNo = $egObj->typeNo;
                $obj->eventNo = $egObj->eventNo;
                $obj->bibSortNo = $egObj->bibSortNo;
                $obj->isOpen = $egObj->isOpen;
                $obj->name = $egObj->name;
            }
        }

        return $obj;
    }

    public function getLatestNonWaitingTime($egId) {
        $egCounts = $this->getCountsByAthleteAndEG();
        $latestTime = '2000-01-01T00:00:00';
        foreach ($egCounts as $key => $countEntryObj) {
            $keyEgId = $this->_getEgIdFromKey($key);
            if ($keyEgId === $egId) {
                if ($countEntryObj->waitingPos === 0) {
                    if ($countEntryObj->posDate > $latestTime) {
                        $latestTime = $countEntryObj->posDate;
                    }
                }
            }
        }
        return $latestTime;
    }

    public function getAthleteEntryForCeId($athleteId, $ceID) {
        $egId = $this->_getEGForCE($ceID, FALSE);
        if (is_null($egId)) {
            $retObj = new stdClass();
            $retObj->waitingPos = E4S_ENTRY_INVALID;
            return $retObj;
        }
        return $this->getAthleteEntryForEgId($athleteId, $egId);
    }

    private function _getEGForCE($ceId, $fail) {
        if (!array_key_exists($ceId, $this->egIds)) {
            if ($fail) {
                Entry4UIError(9601, 'EventGroup Not found for ' . $ceId);
            } else {
                return null;
            }
        }
        return $this->egIds[$ceId];
    }

    public function getAthleteEntryForEgId($athleteId, $egID) {
        $retObj = new stdClass();
        $retObj->waitingPos = 0;
        $entryCountsByAthleteAndEG = $this->getCountsByAthleteAndEG();
        if (is_null($entryCountsByAthleteAndEG)) {
            // No entries
            return $retObj;
        }
        if (!array_key_exists($egID, $this->countsByEG)) {
            // First entry to this event
            return $retObj;
        }

        $key = $this->getKey($athleteId, $egID);

        if (!array_key_exists($key, $entryCountsByAthleteAndEG)) {
            // creating an entry so get next number
            $egCount = $this->getEGObj($egID);
            if ($egCount->totalCount < $egCount->maxAthletes or $egCount->maxAthletes === 0) {
                return $retObj;
            }
            $retObj->waitingPos = $egCount->totalCount - $egCount->maxAthletes + 1;
            return $retObj;
        }
        $athletePosObj = $entryCountsByAthleteAndEG[$key];
        $athletePosObj->waitingPos = 0;
        if ($athletePosObj->max < 1) {
            return $athletePosObj;
        }
        if ($this->isOTDEntry($athletePosObj)) {
            return $athletePosObj;
        }
        if ($athletePosObj->pos > $athletePosObj->max) {
            // On waiting list, return position on waiting list
            $athletePosObj->waitingPos = $athletePosObj->pos - $athletePosObj->max;
        }
        return $athletePosObj;
    }

    public function isOTDEntry($entryObj) {
        return e4s_isOTDEntry($entryObj->entryOptions);
    }

    public function getAthletePositionforEG($athleteId, $egId) {
        $obj = $this->getAthleteInfoForEG($athleteId, $egId);
        return $obj->entryPosition;
    }

    public function getAthleteInfoForEG($athleteId, $egId) {
        $entryCountsByAthleteAndEG = $this->getCountsByAthleteAndEG();

        $entryObj = null;
        $key = $this->getKey($athleteId, $egId);
        if (array_key_exists($key, $entryCountsByAthleteAndEG)) {
            $entryObj = $this->entryCountsByAthleteAndEG[$key];
        }
        if (!array_key_exists($egId, $this->countsByEG)) {
            $this->_createBlankEGObj($egId, $this->countsByEG);
        }
        $egCountObj = $this->countsByEG[$egId];

        $obj = new stdClass();
        $obj->unpaidCount = $egCountObj->unPaidCount;
        $obj->paidCount = $egCountObj->paidCount;
        $obj->totalCount = $egCountObj->totalCount;
        if (!is_null($entryObj)) {
            $obj->entryPosition = $entryObj->pos;
            $obj->entryCreated = $entryObj->posDate . e4s_getOffset($entryObj->posDate);
            $obj->entryPositionDate = $entryObj->posDate . e4s_getOffset($entryObj->posDate);
            $obj->maxAthletes = $entryObj->max;
        } else {
            $obj->entryPosition = $egCountObj->totalCount + 1;
            $obj->entryCreated = '';
            $obj->entryPositionDate = '';
            $obj->maxAthletes = 0;
        }

        return $obj;
    }

    public function getAthletePositionforCE($athleteId, $ceId) {
        $obj = $this->getAthleteInfoForCE($athleteId, $ceId);
        return $obj->entryPosition;
    }

    public function getAthleteInfoForCE($athleteId, $ceId) {
        $egID = $this->_getEGForCE($ceId, TRUE);
        return $this->getAthleteInfoForEG($athleteId, $egID);
    }

    private function _getAthleteIdFromKey($key) {
        $arr = preg_split('~-~', $key);
        return (int)$arr[0];
    }
}