<?php


class e4sCoupon {
    public $wpdb;

    public function __construct() {
        global $wpdb;
        $this->wpdb = $wpdb;
    }

    public function generateCoupons($number_of_coupons) {
        // Verify required values
        if (!isset($args['number_of_coupons'])) {
            return;
        }

        $insert_coupon_ids = array();
        $wpdb = $this->wpdb;

        // Query coupons
        for ($i = 0; $i < $number_of_coupons; $i++) {

            $coupon_code = $this->get_randomCoupon();

            // Insert coupon post
            $wpdb->query($wpdb->prepare("INSERT INTO $wpdb->posts SET
			post_author=%d,
			post_date=%s,
			post_date_gmt=%s,
			post_title=%s,
			post_status='" . WC_POST_PUBLISH . "',
			comment_status='closed',
			ping_status='closed',
			post_name=%s,
			post_modified=%s,
			post_modified_gmt=%s,
			post_type='shop_coupon'
			", get_current_user_id(), current_time('mysql'), current_time('mysql', 1), sanitize_title($coupon_code), $coupon_code, current_time('mysql'), current_time('mysql', 1)));

            $insert_coupon_ids[] = $wpdb->insert_id;
            $coupon_id = $wpdb->insert_id;

            // Set GUID
// 			$wpdb->query( $wpdb->prepare( "UPDATE $wpdb->posts SET guid=%s WHERE ID=%d", get_permalink( $coupon_id ), $coupon_id ) ); // Slow
            $wpdb->query($wpdb->prepare("UPDATE $wpdb->posts SET guid=%s WHERE ID=%d", esc_url_raw(add_query_arg(array('post_type' => 'shop_coupon', 'p' => $coupon_id), home_url())), $coupon_id)); // 10% faster -1 query per coupon
        }
    }

    public function get_randomCoupon() {

        // Generate unique coupon code
        $random_coupon = '';
        $length = 12;
        $charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $count = strlen($charset);

        while ($length--) {
            $random_coupon .= $charset[mt_rand(0, $count - 1)];
        }

        $random_coupon = implode('-', str_split(strtoupper($random_coupon), 4));

        // Ensure coupon code is correctly formatted
        $coupon_code = apply_filters('woocommerce_coupon_code', $random_coupon);

        return $coupon_code;
    }

    public function allocateCoupon($coupon_id, $args = array()) {
        // Add/Replace data to array
        if (!isset($args['customer_email']) or $args['customer_email'] === '') {
            Entry4UIError(8500, 'Unable to allocation Coupon');
        }
        if (!isset($args['coupon_amount']) or $args['coupon_amount'] === 0) {
            Entry4UIError(8510, 'Coupon value not known');
        }
        $product_ids = isset($args['product_ids']) ? (array)$args['product_ids'] : array();
        $exclude_ids = isset($args['exclude_product_ids']) ? (array)$args['exclude_product_ids'] : array();
        $meta_array = array('discount_type' => empty($args['discount_type']) ? 'fixed_cart' : wc_clean($args['discount_type']), 'coupon_amount' => wc_format_decimal($args['coupon_amount']), 'individual_use' => isset($args['individual_use']) ? 'yes' : 'no', 'product_ids' => implode(',', array_filter(array_map('intval', $product_ids))), 'exclude_product_ids' => implode(',', array_filter(array_map('intval', $exclude_ids))), 'usage_limit' => empty($args['usage_limit']) ? '' : absint($args['usage_limit']), 'usage_limit_per_user' => empty($args['usage_limit_per_user']) ? '' : absint($args['usage_limit_per_user']), 'limit_usage_to_x_items' => empty($args['limit_usage_to_x_items']) ? '' : absint($args['limit_usage_to_x_items']), 'expiry_date' => isset($args['expiry_date']) ? wc_clean($args['expiry_date']) : '', 'free_shipping' => isset($args['free_shipping']) ? 'yes' : 'no', 'exclude_sale_items' => isset($args['exclude_sale_items']) ? 'yes' : 'no', 'product_categories' => isset($args['product_categories']) ? array_map('intval', $args['product_categories']) : array(), 'exclude_product_categories' => isset($args['exclude_product_categories']) ? array_map('intval', $args['exclude_product_categories']) : array(), 'minimum_amount' => isset($args['minimum_amount']) ? wc_format_decimal($args['minimum_amount']) : 0, 'maximum_amount' => isset($args['maximum_amount']) ? wc_format_decimal($args['maximum_amount']) : 9999, 'customer_email' => array_filter(array_map('trim', explode(',', wc_clean($args['customer_email'])))),);
        $wpdb = $this->wpdb;
        $insert_meta_values = '';
        // Insert coupon meta
        foreach ($meta_array as $key => $value) {
            $insert_meta_values .= $wpdb->prepare('(%d, %s, %s)', $coupon_id, sanitize_title(wp_unslash($key)), maybe_serialize(wp_unslash($value)));

            $meta_array_keys = array_keys($meta_array);
            if ($key == end($meta_array_keys) && $coupon_id == end($insert_coupon_ids)) {
                $insert_meta_values .= ';';
            } else {
                $insert_meta_values .= ', ';
            }
        }

        $wpdb->query("INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) VALUES $insert_meta_values");

    }

    public function startTransaction() {
        $this->wpdb->query('START TRANSACTION');
    }

    public function commitWork() {
        $this->wpdb->query('COMMIT');
    }

}