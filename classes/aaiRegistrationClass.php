<?php

//https://dev.entry4sports.co.uk/wp-json/e4s/v5/test?action=ea
class aaiRegistrationClass {

    public function __construct() {
        $x = 1;
    }

    public function validateURN($urn) {
        if (is_null($urn)) {
            return null;
        }
        $urn = trim($urn);
        if ($urn === '') {
            return null;
        }

        $aaiOutput = $this->_getEndPoint($urn);
        if (is_null($aaiOutput) or strpos($aaiOutput, '"athleteId":') === FALSE) {
            return null;
        }
        $aaiAthlete = e4s_getDataAsType($aaiOutput, E4S_OPTIONS_OBJECT);

        if (is_null($aaiAthlete) or !isset($aaiAthlete->status)) {
            return null;
        }

        $e4sAthlete = $aaiAthlete;

        if (!is_null($aaiAthlete->status)) {
            $e4sAthlete = new stdClass();
            $e4sAthlete->system = E4S_AOCODE_AAI;
            $e4sAthlete->status = $aaiAthlete->status;
            $e4sAthlete->dob = $aaiAthlete->dob;
            $e4sAthlete->clubId = (int)$aaiAthlete->clubId;
            $e4sAthlete->clubName = $aaiAthlete->clubName;
            $e4sAthlete->county = $aaiAthlete->county;
            $e4sAthlete->region = $aaiAthlete->region;
            $e4sAthlete->club2Id = 0;
            $e4sAthlete->club2Name = '';
            $e4sAthlete->firstName = $aaiAthlete->firstName;
            $e4sAthlete->surName = $aaiAthlete->lastName;
            $e4sAthlete->gender = $aaiAthlete->gender[0];
            $e4sAthlete->lastUpdated = $aaiAthlete->lastUpdated;
            $e4sAthlete->urn = (int)$aaiAthlete->athleteId;
	        $e4sAthlete->activeEndDate = '';
	        if ( isset($aaiAthlete->activeEndDate)) {
		        $e4sAthlete->activeEndDate = $aaiAthlete->activeEndDate;
	        }
			if ( isset($aaiAthlete->expiryDate)) {
		        $e4sAthlete->activeEndDate = $aaiAthlete->expiryDate;
	        }
        }
        return $e4sAthlete;
    }

    private function _getEndPoint($urn) {
        // set time out on curl request
        $envObj = $this->_getActiveInfo();
        $url = $envObj->url . $urn;

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 6);
        curl_setopt($ch, CURLOPT_TIMEOUT, 6);

        $headers = array('User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'X-KEY-ID: ' . $envObj->X_KEY_ID, 'X-KEY-SECRET: ' . $envObj->X_KEY_SECRET, 'Content-Type: application/json');

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

//Execute the request.
        ob_start();
        $valid = curl_exec($ch);
        $content = ob_get_clean();
//Check for errors.
        if (curl_errno($ch)) {
            $content = null;
        }

        return $content;
    }

    private function _getActiveInfo() {
        $obj = new stdClass();
        $obj->url = 'https://membership.athleticsireland.ie/api/athletes/';
        $obj->X_KEY_ID = 'E4S';
        $obj->X_KEY_SECRET = '2bc0b616-0699-11ee-9e44-029e1d958e41';

        return $obj;
    }
}