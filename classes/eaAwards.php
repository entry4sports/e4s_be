<?php

class eaAwards {
    public $compObj;
    public $enabled;
    public $baseAGs;
    public $awards;

    public function __construct($compId = 0) {
        if ($compId !== 0) {
            $this->compObj = e4s_getCompObj($compId, TRUE, TRUE);

            $this->enabled = !e4s_isAAIDomain();
            $this->baseAGs = null;
            $this->awards = null;
            if ($this->enabled) {
                $agObj = new e4sAgeClass(E4S_AGE_DEFAULT_BASE);
                $this->baseAGs = $agObj->getBaseAgeGroups();
                $this->baseAGs = $agObj->getAgeGroupsWithDOBs($this->baseAGs, $this->compObj->getDate());
            }
        }
        $this->_getEaAwards();
    }

    public function processResultsForEg($egId) {
        $sql = '
            select rd.id,
                   rd.athleteId,
                   a.dob,
                   rd.score,
                   ce.eventId,
                   ce.compId
            from ' . E4S_TABLE_EVENTRESULTSHEADER . ' rh,
                 ' . E4S_TABLE_EVENTRESULTS . ' rd,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_ATHLETE . ' a
            where rh.egid = ' . $egId . '
            and   rh.id = rd.resultheaderid
            and   rh.egid = ce.maxgroup
            and   e.athleteid = a.id
            and   e.compEventID = ce.id
            and   rd.athleteid = a.id
            and   rd.eaAward = 0
        ';

        $result = e4s_queryNoLog($sql);

        while ($obj = $result->fetch_object()) {
            $score = resultsClass::getResultInSeconds($obj->score);
            $level = $this->getEventLevelForAthleteDOB($obj->dob, $obj->eventId, $score);
            if ($level > 0) {
                $sql = 'update ' . E4S_TABLE_EVENTRESULTS . '
                    set eaAward = ' . $level . ' 
                    where id = ' . $obj->id;
                e4s_queryNoLog($sql);
            }
        }
    }

    private function _getEaAwards() {
        $sql = '
            select a.*, e.tf, e.name eventName, e.gender gender, ag.name ageGroup
            from ' . E4S_TABLE_EAAWARDS . ' a,
                 ' . E4S_TABLE_EVENTS . ' e,
                 ' . E4S_TABLE_AGEGROUPS . ' ag
            where a.eventId = e.id
            and   a.agegroupid = ag.id
            order by e.tf, e.name, ag.minage,a.level, e.gender desc';
        $result = e4s_queryNoLog($sql);
        $this->awards = array();
        while ($obj = $result->fetch_object(E4S_EAAWARDS_OBJ)) {
            if (!array_key_exists($obj->eventName, $this->awards)) {
                $this->awards[$obj->eventName] = array();
            }
            $eventAwards = $this->awards[$obj->eventName];
            if (!array_key_exists($obj->ageGroupId, $eventAwards)) {
                $eventAwards[$obj->ageGroupId] = array();
            }
            $this->awards[$obj->eventName][$obj->ageGroupId][] = $obj;
        }
    }

    public function getBaseAGForAthleteDOB($dob) {
        $useBaseAG = null;
        foreach ($this->baseAGs as $baseAG) {
            if ($dob >= $baseAG->fromDate and $dob <= $baseAG->toDate) {
                $useBaseAG = $baseAG;
                break;
            }
        }
        return $useBaseAG;
    }

    public function getEventLevelForAthleteDOB($dob, $eventId, $score) {
        if (!$this->enabled) {
            e4s_addDebugForce('Not Enabled');
            return 0;
        }
        if (gettype($score) === 'string') {
            $score = (float)$score;
        }

        $baseAG = $this->getBaseAGForAthleteDOB($dob);
        if (is_null($baseAG)) {
            e4s_addDebugForce('No Base Group');
            return 0;
        }
        $useAgId = $baseAG->id;
        $level = 0;
	    $eventId = (int)$eventId;
        foreach ($this->awards as $eventAwards) {
            foreach ($eventAwards as $ageAwards) {
                foreach ($ageAwards as $award) {
                    if ($award->eventId === $eventId and $award->ageGroupId === $useAgId) {
//                        e4s_addDebugForce('4-' . $award->score);
                        if (($score >= $award->score and $award->tf === E4S_EVENT_FIELD) or ($score <= $award->score and $award->tf === E4S_EVENT_TRACK)) {
//                            e4s_addDebugForce('5-' . $award->score);
                            if ($award->level > $level) {
                                $level = $award->level;
//                                break;
                            }
                        }
                    }
                }
            }
        }
        return $level;
    }

    public function check() {
        $lastEvent = '';
        ?>
        <style>
            table {
                padding-bottom: 10px;
            }

            .female {
                background-color: lightpink;
            }

            .female .col {
                background-color: #eeaaaa;
                color: black;
                font-weight: 800;
            }

            .male {
                background-color: lightblue;
            }

            .male .col {
                background-color: #00aeff;
                color: black;
                font-weight: 800;
            }

            .col {
                width: 70px;
            }

            .centre {
                display: block;
                margin-left: auto;
                /*margin-right: auto;*/
                width: 50%;
            }

            .tdCentre {
                text-align: center;
            }
        </style>
        <?php
        $oddRow = FALSE;
        $outputLine = '';
        echo '<table>
                <tr class="centre">
                    <td colspan="2"><img src="https://d54q71dvos881.cloudfront.net/eyJrZXkiOiIyMDIzLzAyL1BCLUF3YXJkcy10aXRsZTIwMDAucG5nIiwiYnVja2V0IjoiZW5nbGFuZC1hdGhsZXRpY3MtcHJvZC1hc3NldHMtYnVja2V0IiwiZWRpdHMiOnsicmVzaXplIjp7IndpZHRoIjo3NTB9fX0="></td>
                 </tr>                       
             </table>
             <table style="width: -webkit-fill-available;max-width: 1560px;">
                 <tr>
                    <td class="tdCentre">
                        <a target="eaTrack" href="https://www.englandathletics.org/athletics-and-running?media-alias=91a853ea713317e9b530">PB Awards tables for track events</a>    
                    </td>
                    <td class="tdCentre">
                        <a target="eaField" href="https://www.englandathletics.org/athletics-and-running?media-alias=cd4c3256a9ffa59741f8">PB Awards tables for field events</a>    
                    </td>
                </tr>
             </table>';
        foreach ($this->awards as $eventName => $eventAwards) {
            if ($eventName !== '100m Hurdles') {
//                continue;
            }
            if ($lastEvent !== $eventName and $lastEvent !== '') {
                if ($outputLine !== '') {
                    if (!$femaleDisplayed) {
                        $femaleLine .= outputBlankGenderRow();
                        $outputLine = '<tr class="' . $class . '">' . $maleLine . $femaleLine . '</tr>';
                    }
                    echo $outputLine;
                }
                echo '</table>';
            }
            $maleLine = '';
            $femaleLine = '';
            $lastAgeGroupId = 0;
            $femaleDisplayed = FALSE;
            foreach ($eventAwards as $ageGroupId => $ageAwards) {
                $class = '';
                foreach ($ageAwards as $levelAward) {
                    if ($lastEvent !== $eventName) {
                        echo '<table id="' . $levelAward->eventName . '" class="">';
                        echo '<tr class="header">
                                <td class="col male">' . $levelAward->eventName . '</td>
                                <td class="col male">Level 1</td>
                                <td class="col male">Level 2</td>
                                <td class="col male">Level 3</td>
                                <td class="col male">Level 4</td>
                                <td class="col male">Level 5</td>
                                <td class="col male">Level 6</td>
                                <td class="col male">Level 7</td>
                                <td class="col male">Level 8</td>
                                <td class="col male">Level 9</td>
                                <td class="col">&nbsp;</td>
                                <td class="col female">' . $levelAward->eventName . '</td>
                                <td class="col female">Level 1</td>
                                <td class="col female">Level 2</td>
                                <td class="col female">Level 3</td>
                                <td class="col female">Level 4</td>
                                <td class="col female">Level 5</td>
                                <td class="col female">Level 6</td>
                                <td class="col female">Level 7</td>
                                <td class="col female">Level 8</td>
                                <td class="col female">Level 9</td>
                              </tr>';
                    }
                    if ($lastAgeGroupId !== $ageGroupId) {
                        $ageGroup = $levelAward->ageGroup;

                        if ($oddRow) {
                            $class = 'oddRow';
                        }
                        $oddRow = !$oddRow;

                        if ($lastAgeGroupId !== 0) {
                            if (!$femaleDisplayed) {
                                $femaleLine .= outputBlankGenderRow();
                                $outputLine = '<tr class="' . $class . '">' . $maleLine . $femaleLine . '</tr>';
                            }
                            echo $outputLine;
                        }
                        $maleLine = '<td>' . $ageGroup . '</td>';
                        if ($levelAward->gender === E4S_GENDER_FEMALE) {
                            $maleLine .= outputBlankGenderRow();
                        }
                        $femaleLine = '<td>&nbsp</td><td>' . $ageGroup . '</td>';
                        $femaleDisplayed = FALSE;
                    }
                    $score = $levelAward->score;
                    if ($levelAward->tf === E4S_EVENT_TRACK) {
                        $score = resultsClass::getResultFromSeconds($levelAward->score);
                    }
//                    $score = $levelAward->level . ":" . $score;
                    if ($levelAward->gender === E4S_GENDER_FEMALE) {
                        $femaleLine .= '<td>' . $score . '</td>';
                        $femaleDisplayed = TRUE;
                    } else {
                        $maleLine .= '<td>' . $score . '</td>';
                    }
                    $outputLine = '<tr class="' . $class . '">' . $maleLine . $femaleLine . '</tr>';

                    $lastEvent = $eventName;
                    $lastAgeGroupId = $ageGroupId;
                }
            }
        }
        if ($lastAgeGroupId !== 0) {
            echo '<tr class="' . $class . '">' . $maleLine . $femaleLine . '</tr>';
        }
        if ($lastEvent !== 0) {
            echo '</table>';
        }
        exit;
    }
}

function outputBlankGenderRow(){
    return '<td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td>';
}
//$class = 'male';
//if ( $levelAward->gender === E4S_GENDER_FEMALE){
//    $class = 'female';
//}