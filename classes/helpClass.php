<?php

define('E4S_HELP_URL', 'U');
define('E4S_HELP_TEXT', 'T');
define('E4S_HELP_HTML', 'H');

class helpClass {
    public $helpRow;
    public $helpRows;
    public $helpDomain = 'https://support.entry4sports.com';

    public function __construct() {
        $this->helpRow = null;
        $this->helpRows = array();
    }

    public static function withID($keyid) {
        $instance = new self();
        $instance->loadByID($keyid);
        return $instance;
    }

    private function loadByID($keyid) {
        $this->getRowFromDB('where id = ' . $keyid);
    }

    private function getRowFromDB($where = '') {
        $sql = 'select *
                from ' . E4S_TABLE_HELP;
        if ($where !== '') {
            $sql .= ' ' . $where;
        }

        $result = e4s_queryNoLog($sql);

        $this->helpRow = null;
        if ($result->num_rows === 1) {
            $this->helpRow = $result->fetch_assoc();
            $this->helpRows = [$this->helpRow];
        } else {
            $this->helpRows = $result->fetch_all(MYSQLI_ASSOC);
        }
    }

    public static function withKey($key) {
        $instance = new self();
        $instance->loadByKey($key);
        return $instance;
    }

    private function loadByKey($key) {
        $this->getRowFromDB("where helpkey = '" . $key . "'");
    }

    public static function preload() {
        $instance = new self();
        $instance->getRowFromDB('where preload = true');
        return $instance;
    }

    public function getRows($pageInfo = null) {
        $where = '';
        if (!is_null($pageInfo)) {
//            reset helpRows
            $this->helpRows = array();
//            Build Where statement
            $where = $this->_buildWhereFromPageInfo($pageInfo);
        }
        $retArr = array();
        if (sizeof($this->helpRows) === 0) {
            $this->getRowFromDB($where);
        }
        foreach ($this->helpRows as $row) {
            $retArr[] = $this->getRow($row);
        }
        $this->helpRows = $retArr;
        return $retArr;
    }

    private function _buildWhereFromPageInfo($pageInfo) {
        $where = '';
        $pagesize = $pageInfo->pagesize;
        $page = $pageInfo->page;
        $startswith = $pageInfo->startswith;
        $sortkey = $pageInfo->sortkey;
        $sortorder = $pageInfo->sortorder;

        if ($sortkey === '') {
            $sortkey = 'title';
        }
        $usePaging = FALSE;
        if (isset($pagesize) and isset($page)) {
            $usePaging = TRUE;
        }

        if (isset($startswith)) {
            $where .= " where title like '" . $startswith . "%' ";
            $where .= " or helpkey like '" . $startswith . "%' ";
        }

        $where .= ' order by ' . $sortkey . ' ' . $sortorder;

        if ($usePaging and $pagesize !== 0) {
            $where .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
        }

        return $where;
    }

    public function getRow($row = null) {
        if (is_null($row)) {
            $row = $this->helpRow;
        }

        $retrow = array();
        $retrow['id'] = 0;
        $retrow['preload'] = e4s_ensureBoolean(FALSE);
        $retrow['key'] = '';
        $retrow['title'] = '';
        $retrow['data'] = '';
        $retrow['type'] = '';
        if (!is_null($row)) {
            $retrow['id'] = (int)$row['id'];
            $retrow['key'] = $row['helpkey'];
            $retrow['title'] = $row['title'];
            $retrow['type'] = $row['type'];
            $retrow['preload'] = e4s_ensureBoolean($row['preload']);

            $helpData = $row['data'];
//            If a URL and not to the std help domain
            if ($retrow['type'] === E4S_HELP_URL and strpos($helpData, '://') === FALSE) {
                $helpData = $this->helpDomain . $helpData;
            }
            $retrow['data'] = $helpData;
        }
        return $retrow;
    }

    public function update($model) {
        $preload = 'false';
        if ($model->preload) {
            $preload = 'true';
        }
        $sql = 'update ' . E4S_TABLE_HELP . "
                set title = '" . $model->title . "',
                    data = '" . $model->data . "',
                    type = '" . $model->type . "',
                    preload = " . $preload . '
                where id = ' . $model->id;
        e4s_queryNoLog($sql);
        $this->loadByID($model->id);
        return $this->getRow();
    }

    public function create($model) {
        $preload = 'false';
        if ($model->preload) {
            $preload = 'true';
        }
        $sql = 'Insert into ' . E4S_TABLE_HELP . "(helpkey,title,data,type,preload) 
                values (
                    '" . $model->key . "',
                    '" . $model->title . "',
                    '" . $model->data . "',
                    '" . $model->type . "',
                    " . $preload . '
                )';
        e4s_queryNoLog($sql);
        $this->loadByKey($model->key);
        return $this->getRow();
    }
}