<?php
const E4S_ALL_CONTACTS = 'e4s_allContacts';
//returns cOptions
function e4s_setContactInfo($orgId, $model) {
    $contactObj = new e4sContactClass($model->id);
    return $contactObj->createUpdateComp($orgId, $model->options);
}

class e4sContactClass {
    public $compId;
    public $contactObj;
    public $compObj;
    public $contacts;

    public function __construct($compId) {
        $this->compId = $compId;
        $this->contactObj = $this->_defaultContactObj();
        if ($compId > 0) {
            $this->compObj = e4s_GetCompObj($compId);
        }

		if ( !array_key_exists(E4S_PROCESSING_COMPS_CONTACTS, $GLOBALS)){
			$GLOBALS[ E4S_PROCESSING_COMPS_CONTACTS ]  = $this->_getAllContacts();
		}

		$this->contacts = $GLOBALS[ E4S_PROCESSING_COMPS_CONTACTS ];
    }

    private function _defaultContactObj() {
		return self::getDefaultContactObj();
    }
	public static function getDefaultContactObj(){
        $email = '';
        $userName = '';
        if (e4s_getUserID() > E4S_USER_NOT_LOGGED_IN) {
            $email = e4s_getUserEmail();
            $userName = e4s_getUserName();
        }
        $contactObj = new stdClass();
        $contactObj->email = $email;
        $contactObj->id = 0;
        $contactObj->tel = '';
        $contactObj->userName = $userName;
        $contactObj->visible = FALSE;
        return $contactObj;
    }

//    Contact Obj getters

    private function _getAllContacts() {
        if (array_key_exists(E4S_ALL_CONTACTS, $GLOBALS)) {
            return $GLOBALS[E4S_ALL_CONTACTS];
        }
        $sql = $this->_getTableSql();
        $sql .= ' order by id';
        $result = e4s_queryNoLog($sql);
        $contacts = array();
        while ($obj = $result->fetch_object()) {
            $obj = $this->_normaliseObj($obj);
            $contacts[$obj->id] = $obj;
        }
        $GLOBALS[E4S_ALL_CONTACTS] = $contacts;
        return $contacts;
    }

    private function _getTableSql() {
        return 'select id
                       ,name userName
                       ,tel
                       ,email
                       ,visible
                from ' . E4S_TABLE_CONTACTS . ' ';
    }

    private function _normaliseObj($contactObj) {
        $contactObj->id = (int)$contactObj->id;
        if ((int)$contactObj->visible === 1) {
            $contactObj->visible = TRUE;
        } else {
            $contactObj->visible = FALSE;
        }
        return $contactObj;
    }

    public static function setDefaultInCOptions($cOptions) {
        $cOptions = self::setInCoptions($cOptions);
        return $cOptions;
    }

    private function _setInCoptions($cOptions, $contactObj) {
        return self::setInCoptions($cOptions, $contactObj);
    }

	public static function setInCoptions($cOptions, $contactObj = null) {
		if ( is_null($contactObj) ){
			$contactObj = self::getDefaultContactObj();
		}
		$cOptions->contact = $contactObj;
		return $cOptions;
	}

    public function getFullInfo($cOptions) {
        $contactOptionsObj = $this->_getObjFromCoptions($cOptions);
        $id = $this->_getContactObjId($contactOptionsObj);
        if ($id === 0) {
            return $cOptions;
        }
        $contactOptionsObj = $this->_getContactObjById($id);
        $cOptions = $this->_setInCoptions($cOptions, $contactOptionsObj);
        return $cOptions;
    }

    private function _getObjFromCoptions($cOptions) {
        $defaultObj = $this->_defaultContactObj();
        if (isset($cOptions->contact)) {
            return e4s_mergeInOptions($defaultObj, $cOptions->contact);
//            return array_merge($defaultObj,$cOptions->contact);
        }
        return $defaultObj;
    }

    private function _getContactObjId($contactObj) {
        return $contactObj->id;
    }

    private function _getContactObjById($id) {
        if (!is_null($this->contacts)) {
            if (array_key_exists($id, $this->contacts)) {
                return $this->contacts[$id];
            }
        }
        $sql = $this->_getTableSql();
        $sql .= 'where id = ' . $id;
        return $this->_getObjFromSql($sql);
    }

    public function _getObjFromSql($sql) {
        $result = e4s_queryNoLog($sql);
        $contactObj = $this->_defaultContactObj();
        if ($result->num_rows === 1) {
            $contactObj = $this->_normaliseObj($result->fetch_object());
        }
        return $contactObj;
    }

    public function getContactEmail($cOptions = null) {
        if (is_null($cOptions)) {
            if (is_null($this->compObj)) {
                Entry4UIError(9033, 'Contact Email requested but no competition');
            } else {
                $cOptions = $this->compObj->getOptions();
            }
        }
        $contactOptionsObj = $this->_getObjFromCoptions($cOptions);
        $email = $this->_getContactObjEmail($contactOptionsObj);
        return $email;
    }

    private function _getContactObjEmail($contactObj) {
        return $contactObj->email;
    }

    public function getUserName($cOptions = null) {
        if (is_null($cOptions)) {
            if (is_null($this->compObj)) {
                Entry4UIError(9034, 'Contact Email requested but no competition');
            } else {
                $cOptions = $this->compObj->getOptions();
            }
        }
        $contactOptionsObj = $this->_getObjFromCoptions($cOptions);
        $name = $this->_getContactObjName($contactOptionsObj);
        return $name;
    }

    private function _getContactObjName($contactObj) {
        return $contactObj->userName;
    }

    public function createUpdateComp($orgId, $cOptions) {
        $contactOptionsOrigObj = $this->_getObjFromCoptions($cOptions);

        $contactName = $this->_getContactObjName($contactOptionsOrigObj);
        $contactEmail = $this->_getContactObjEmail($contactOptionsOrigObj);
        $contactNumber = $this->_getContactObjNumber($contactOptionsOrigObj);
        $contactVisible = $this->_getContactObjVisible($contactOptionsOrigObj);

        if ($contactName === '' and $contactNumber === '' and $contactEmail === '') {
            // get last org contact if possible
            $id = $this->_getContactObjId($contactOptionsOrigObj);
            if ($id > 0) {
                $contactOptionsObj = $this->_getContactObjById($id);
            } else {
                $contactOptionsObj = $this->_getOrgContactInfo($orgId);
            }
        } else {
            $contactOptionsObj = $this->_getContactByNameEmailNumber($contactName, $contactEmail, $contactNumber);
            if ($this->_getContactObjId($contactOptionsObj) === 0) {
                $contactOptionsObj = $this->_createContact($contactOptionsOrigObj);
            }
        }

        $cOptions = $this->_setInCoptions($cOptions, $contactOptionsObj);
        return $this->_returnCOptions($cOptions, TRUE);
    }

    private function _getContactObjNumber($contactObj) {
        return $contactObj->tel;
    }

    private function _getContactObjVisible($contactObj) {
        return $contactObj->visible;
    }

    private function _getOrgContactInfo($orgId) {
        $sql = 'select options cOptions
                from ' . E4S_TABLE_COMPETITON . '
                where compclubid = ' . $orgId . '
                order by id asc';
        $results = e4s_queryNoLog($sql);
        $id = 0;
        while ($obj = $results->fetch_object()) {
            $cOptions = e4s_getDataAsType($obj->cOptions, E4S_OPTIONS_OBJECT);
            $contactOptionsObj = $this->_getObjFromCoptions($cOptions);
            $id = $this->_getContactObjId($contactOptionsObj);
            if ($id > 0) {
                break;
            }
        }
        return $this->_getContactObjById($id);
    }

    private function _getContactByNameEmailNumber($name, $email, $number) {
        $sql = $this->_getTableSql() . 'where ';
        $cont = '';
        if ($name !== '') {
            $sql .= "name = '" . $name . "'";
            $cont = ' and ';
        }
        if ($email !== '') {
            $sql .= $cont . "email = '" . $email . "'";
            $cont = ' and ';
        }
        if ($number !== '') {
            $sql .= $cont . "tel = '" . $number . "'";
            $cont = ' and ';
        }
        return $this->_getObjFromSql($sql);
    }

    private function _createContact($contactOptionsObj) {
        $contactName = $this->_getContactObjName($contactOptionsObj);
        $contactEmail = $this->_getContactObjEmail($contactOptionsObj);
        $contactNumber = $this->_getContactObjNumber($contactOptionsObj);
        $contactVisible = $this->_getContactObjVisible($contactOptionsObj);
        if ($contactVisible) {
            $contactVisible = 1;
        } else {
            $contactVisible = 0;
        }
        $sql = 'insert into ' . E4S_TABLE_CONTACTS . "(name, tel, email, visible )
                values(
                    '" . $contactName . "',
                    '" . $contactNumber . "',
                    '" . $contactEmail . "',
                    " . $contactVisible . '
                )';
        e4s_queryNoLog($sql);
        $id = e4s_getLastID();
        $contactOptionsObj = $this->_setContactObjId($contactOptionsObj, $id);
        return $contactOptionsObj;
    }

//    return contactObj

    private function _setContactObjId($contactOptionsObj, $id) {
        $contactOptionsObj->id = $id;
        return $contactOptionsObj;
    }

    private function _returnCOptions($cOptions, $stripped) {
        $contactObj = $cOptions->contact;
        if ($stripped) {
            $contactObjStripped = new stdClass();
            $contactObjStripped->id = $this->_getContactObjId($contactObj);
            $contactObjStripped->visible = $this->_getContactObjVisible($contactObj);
            $cOptions->contact = $contactObjStripped;
        }
        return $cOptions;
    }

    public function getWritableObj($obj) {
        $retObj = new stdClass();
        $retObj->id = $obj->id;
        $retObj->visible = $obj->visible;
        return $retObj;
    }
}