<?php

use function Ratchet\Client\connect;

require_once E4S_FULL_PATH . 'e4s_constants.php';
require_once E4S_FULL_PATH . 'results/webresults.php';
require_once E4S_FULL_PATH . 'classes/e4sPostProcessing.php';

class uploadTrackInfo extends resultsClass {
    public $fileName;
    public $tmpFileName;
    public $eventNo = null;
    public $typeNo = null;
    public $athleteId = 0;
    public $athlete = '';
    public $bibNo = '';
    public $heatNo;
    public $eventName;
    public $scheduleTime;
    public $eventTime;
    public $ws;
    public $image;
    public $outputNo;
    public $results;
    public $inputType;
    public $compObj;
    public $inputFormat;
    public $bibTranslationArr;
    public ?e4sPostProcessing $postProcessObj;

    public function __construct($compId, $outputNo = 0) {
        parent::__construct($compId);
        $this->bibTranslationArr = array();
        $this->compObj = e4s_GetCompObj($compId);
        $this->scheduleTime = '2021-04-18T00:00:00';
        $this->_setImage($this->compObj->getScoreboardImage());
        $this->outputNo = $outputNo;
        $this->inputType = E4S_PF_TT;
        $this->postProcessObj = null;
    }

    public function html() {
        $html = '';
//        $html .= '<body>';
        $html .= '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>';
        if (hasScoreboardAccess()) {
            $html .= '<form action="' . E4S_ROUTE_PATH . '/otd' . E4S_SECURE_TEXT . '/track/' . $this->compId . '/manual" method="post" enctype="multipart/form-data">';
//        $html .= 'Select Track Files to upload:<br>';
            $html .= '<table cellpadding="0" cellspacing="0">';
            $html .= '<tr style="background-color: lightgray;">';
//            $html .= '<td>';
//            $html .= 'Image file';
//            $html .= '</td>';
            $html .= '<td>';
            $html .= 'Text Results file';
            $html .= '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
//            $html .= '<td>';
//            $html .= '<input type="file" accept=".png" name="imgFileToUpload" id="imgFileToUpload">&nbsp;&nbsp;';
//            $html .= '</td>';
            $html .= '<td>';
            $html .= '<input type="file" accept=".*" name="txtFileToUpload" id="txtFileToUpload"><br><br>';
            $html .= '</td>';
            $html .= '</tr>';
            $html .= '</table>';
            $html .= '<input type="submit" value="Upload Files" name="submit">';
            $html .= '</form>';
        }
        $saveEventNo = $this->eventNo;
        $saveHeatNo = $this->heatNo;
//        $html .= $this->_getPreviousImages();
        $this->eventNo = $saveEventNo;
        $this->heatNo = $saveHeatNo;

        return $html;
    }

    public function uploadAFile($sendSocketMsg = FALSE, $exit = TRUE) {
        $this->_readSingleFileName();

        if ($this->eventNo === null) {
            Entry4UIError(8300, 'Failed to process uploaded results file', 400);
        }
//        logTxt( e4s_getDataAsType($this, E4S_OPTIONS_STRING));
        if (!is_null($this->_getFileName())) {
            $this->_processUploadedFile();
            if ($sendSocketMsg) {
                $this->sendSocketMsg();
            }
            if ($exit or $this->eventNo === 0) {
                Entry4UISuccess();
            } else {
                return TRUE;
            }
        }
        Entry4UIError(8210, 'Failed to process uploaded file', 402);
    }

    private function _readSingleFileName() {
        $this->tmpFileName = null;
        if (array_key_exists('txtFileToUpload', $_FILES)) {
            $fileObj = $_FILES['txtFileToUpload'];
        } else {
//            $fileObj = $_FILES['files'];
            // dont change errno, its used in txt
            Entry4UIError(7999, 'No file to upload');
        }

        $this->_setFileName(basename($fileObj['name']));
        $tmpfilename = $fileObj['tmp_name'];
        $this->_processFileName($tmpfilename);
    }

    private function _creatDir() {
        $dir = $this->_getFullFilePath();
        if (!file_exists($dir)) {
            mkdir($dir, 0777, TRUE);
        }
    }

    private function _checkFieldVideo() {
		if ( $this->typeNo !== '' ) {
			$eventType = strtoupper( $this->typeNo[0] );
		}else {
			$eventType = '';
		}
        if ($eventType !== E4S_EVENT_FIELD) {
			if ( $this->eventNo === 0 ){
				$origFileName = $this->_getFileName();
				$origFileName = preg_split('~_copy~', $origFileName)[0];
				$this->_setFileName($origFileName . "." . $this->inputFormat);
			}
            return;
        }
        $fileName = '';
        // attempt to get BibNo
        $sql = '
            select bibno bibNo, teambibno teamBibNo
                from ' . E4S_TABLE_ENTRYINFO . '
                where compId = ' . $this->compId . '
                and eventNo = ' . $this->eventNo . '
                and athleteId = ' . $this->athleteId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            $obj = $result->fetch_object();
            if (is_null($obj->bibNo)) {
                $obj->bibNo = '';
            }
            if (is_null($obj->teamBibNo)) {
                $obj->teamBibNo = '';
            }

            if ($obj->teamBibNo !== '') {
                $fileName = $obj->teamBibNo;
            } else if ($obj->bibNo !== '') {
                $fileName = $obj->bibNo;
            }
        }
        if ($fileName !== '') {
            $this->bibNo = $fileName;
            $this->compDir .= '/' . $this->heatNo;
            $this->_creatDir();
        } else {
            $fileName = 'latest';
        }
        $this->_setFileName($fileName . '.' . $this->inputFormat);
    }

    private function _setImage($image) {
        $this->image = $image;
    }

    private function _getImage() {
        return $this->image;
    }

    private function _getFileName() {
        return $this->fileName;
    }

    private function _setFileName($name) {
		$name = str_replace('(', '-', $name);
		$name = str_replace(')', '-', $name);
        $this->fileName = $name;
    }

    private function _getFullFilePath() {
        return $this->rootDir . $this->compDir;
    }

    private function _getFullFileName() {
        return $this->_getFullFilePath() . '/' . $this->_getFileName();
    }

    private function _getRelativeFileName() {
        return $this->compDir . '/' . $this->_getFileName();
    }

    private function _processUploadedFile() {
        // ensure directory exists
        $this->_creatDir();

        $this->compDir .= '/' . $this->eventNo;
        $this->_creatDir();

        $fileName = $this->_getFileName();

        $this->inputFormat = $this->postProcessObj::getInputFormat($fileName);
        $isVideo = $this->_isVideoFile();
        if ($isVideo) {
            $this->_checkFieldVideo();
        }

        $fullFileName = $this->_getFullFileName();

        $textFile = FALSE;
        if (strpos($fileName, '.' . E4S_TT_RESULT_EXT) > 0 or strpos($fileName, '.' . E4S_LYNX_RESULT_EXT) > 0) {
            $textFile = TRUE;
        }

//        if (!move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $targetFile)) {
        if (!move_uploaded_file($this->tmpFileName, $fullFileName)) {
            $errorTxt = 'Sorry, there was an error uploading your file. (' . $fileName . ')';
            Entry4UIError(8020, $errorTxt);
        }
        $fileName = $this->_getFileName();
        if ($textFile) {
            $this->_processResultsFile();
        } else {
            $fileName = $this->postProcessObj->processFile($this->_getFullFilePath(), $fileName);
            // filename may have changed during processing
            $this->_setFileName($fileName);
            $this->_setImage($this->_getRelativeFileName());

            if ($isVideo) {
                $this->_addCardResultsRecord();
            } else {
                $this->_updateTrackResultImage();
            }
        }
    }

    public function setVideoParams($params) {
        $this->postProcessObj = new e4sPostProcessing($params);
    }

    private function _addCardResultsRecord() {
        $sql = '
            select id
            from ' . E4S_TABLE_CARDRESULTS . '
            where compid = ' . $this->compId . '
            and   eventno = ' . $this->eventNo . '
            and   athleteid = ' . $this->athleteId . "
            and   resultkey = 't" . $this->heatNo . "'";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            $obj = $result->fetch_object();
            $sql = '
                update ' . E4S_TABLE_CARDRESULTS . "
                set attachment = '" . $this->_getImage() . "'
                where id = " . $obj->id;
        } else {
            $sql = '
                insert into ' . E4S_TABLE_CARDRESULTS . '(compid, athleteid, eventno, resultkey, attachment)
                values(
                    ' . $this->compId . ',
                    ' . $this->athleteId . ',
                    ' . $this->eventNo . ",
                    't" . $this->heatNo . "',
                    '" . $this->_getImage() . "'
                )
            ";
        }
        e4s_queryNoLog($sql);
    }

    private function _processResultsFile() {
        $targetFile = $this->_getFullFileName();
        $file = fopen($targetFile, 'r');
        if ($this->inputType === E4S_PF_TT) {
            $this->_processTTResultsFile($file);
        } else {
            $this->_processLynxResultsFile($file);
        }

// save any picture information
        $this->_updateTrackResultInfo();
        e4s_uploadTrackResultsToResults($this);
    }

    private function _getBibTranslation($egId) {
        $sql = 'select bibno, teambibno
            from ' . E4S_TABLE_ENTRYINFO . '
            where egId = ' . $egId;
        $results = e4s_queryNoLog($sql);
        $bibArr = array();
        while ($obj = $results->fetch_object()) {
            $useBibNo = $obj->bibNo;
            if ($obj->teamBibNo !== '' and $obj->teamBibNo !== '0') {
                $useBibNo = $obj->teamBibNo;
            }
            $bibArr[(int)$obj->bibNo] = $useBibNo;
        }
        $this->bibTranslationArr[$egId] = $bibArr;
    }

    private function _translateBibNo($egId, $bibNo) {
        if (!array_key_exists($egId, $this->bibTranslationArr)) {
            $this->_getBibTranslation($egId);
        }
        $bibs = $this->bibTranslationArr[$egId];

        foreach ($bibs as $e4sBib => $useBibNo) {
            if ((int)$e4sBib === (int)$bibNo) {
                return $useBibNo;
            }
        }
        // if we get here then no translation found
        return $bibNo;
    }

    private function _processTTResultsFile($file) {
        $headerLine = fgets($file);
        $headerArr = preg_split("~\t~", $headerLine);

        $timePos = count($headerArr);
        $eventTime = trim($headerArr[$timePos - 1], "\r\n");
        $this->eventTime = $this->_convertTTTime($eventTime);
        $ws = $headerArr[1];
        if (strpos($ws, ':') !== FALSE) {
            $wsArr = preg_split('~: ~', $ws);
            $ws = $wsArr[1];
        }
        $this->ws = $ws;
        // ignore 2nd line
        fgets($file);
        $sqlCont = '';
        $bibs = $this->getBibsForComp();
        $results = array();
        $validBibNoAthletes = array();
        $validBibNos = array();
        $eaAwardObj = new eaAwards($this->compObj->getID());
        $egId = 0;
        $egObjs = $this->compObj->getEGObjs();
        foreach ($egObjs as $eventGroup) {
            if ($eventGroup->eventNo === $this->eventNo) {
                $egId = $eventGroup->id;
                break;
            }
        }
        if ($egId === 0) {
            Entry4UIError(9344, 'Invalid Event Result');
        }

        while ($line = fgets($file)) {
            $result = new stdClass();
            $lineArr = preg_split("~\t~", $line);
            if (sizeof($lineArr) < 2) {
                continue;
            }
            $result->place = (int)$lineArr[0];
            $result->lane = (int)$lineArr[1];
            $result->time = trim($lineArr[2]);
            $result->timeInSeconds = resultsClass::getResultInSeconds($result->time);
            $result->bibNo = $this->_translateBibNo($egId, $lineArr[3]);
            $athlete = $lineArr[4];
            $club = '';
            if (strpos($athlete, '(') > 0) {
                $a = preg_split('~\(~', $athlete);
                $club = '(' . $a[1];
                $athlete = $a[0];
            }
            $result->athlete = trim($athlete);
            $result->athleteId = 0;
            $result->eaAward = 0;
            $result->club = $club;

            if (array_key_exists($result->bibNo, $bibs)) {
                $validBibNos[$result->bibNo] = $bibs[$result->bibNo];
                $validBibNoAthletes[$bibs[$result->bibNo]] = $result->bibNo;
            }
            $ageGroup = trim($lineArr[5], "\r\n");
            $ageGroupArr = preg_split('~,~', $ageGroup);
            $result->ageGroup = $ageGroupArr[0];
            $results[$result->bibNo] = $result;
        }
        if (!empty($validBibNos)) {
            $sql = '
                select a.id athleteId, 
                       a.dob, 
                       a.gender gender,
                       a.firstname firstName,
                       a.surname surName,
                       ce.id ceId,
                       ce.eventId eventId,
                       b.bibno bibNo,
                       c.clubname clubName
                from ' . E4S_TABLE_ATHLETE . ' a,
                     ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_BIBNO . ' b,
                     ' . E4S_TABLE_CLUBS . ' c
                where b.bibno in (' . implode(',', $validBibNoAthletes) . ')
                and   a.id = e.athleteid
                and   a.id = b.athleteid
                and   if (e.clubid=0,' . E4S_UNATTACHED_ID . ',e.clubid) = c.id
                and   e.compeventid = ce.id
                and   b.compId = ce.compId
                and   ce.maxgroup = ' . $egId;

            $clubResult = e4s_queryNoLog($sql);
            while ($obj = $clubResult->fetch_object()) {
                $result = $results[$obj->bibNo];
                $result->athlete = formatAthlete($obj->firstName, $obj->surName);
                $result->athleteId = (int)$obj->athleteId;
                $result->dob = $obj->dob;
                $baseAG = $eaAwardObj->getBaseAGForAthleteDOB($obj->dob);

                $result->ceId = (int)$obj->ceId;
                $result->eventId = (int)$obj->eventId;

                if (isset($result->timeInSeconds)) {
                    $result->timeInSeconds = (float)$result->timeInSeconds;
                    $result->eaAward = $eaAwardObj->getEventLevelForAthleteDOB($result->dob, $result->eventId, $result->timeInSeconds);
                }
                $result->ageGroup = '';
                if (isset($baseAG->name)) {
                    $result->ageGroup = $baseAG->name;
                }
                $result->gender = $obj->gender;

                $result->club = $obj->clubName;
                $results[$obj->bibNo] = $result;
            }
//            $this->_checkEntriesExist($results, $validBibNos);
        }
        $sql = '';
        $this->results = $results;
        foreach ($results as $result) {
            $options = $result;
            $options->ws = $ws;
            $options->eventTime = $eventTime;
            if ($sql === '') {
                $clearSql = 'delete from ' . E4S_TABLE_CARDRESULTS . '
                             where compid = ' . $this->compId . '
                             and eventno = ' . $this->eventNo . "
                             and resultKey = 'h" . $this->heatNo . "'";
                e4s_queryNoLog($clearSql);
                $sql = 'insert into ' . E4S_TABLE_CARDRESULTS . ' (compid, athleteid, eventno, resultkey, resultvalue, options)
                        values ';
            }

            $sql .= $sqlCont . '(';
            $sql .= $this->compId . ',';
            if (array_key_exists($result->bibNo, $bibs)) {
                $sql .= $bibs[$result->bibNo] . ',';
            } else {
                // if can not find athlete put a negative bib. the results will then get athlete from options
                $sql .= '-' . $result->bibNo . ',';
            }
            $sql .= $this->eventNo . ',';
            $sql .= "'h" . $this->heatNo . "',";
            $sql .= "'" . $result->time . "',";
            $sql .= "'" . addslashes(e4s_encode($options)) . "'";
            $sql .= ')';
            $sqlCont = ',';

        }

        if ($sql !== '') {
            e4s_queryNoLog($sql);
        }
    }

    private function _convertTTTime($time) {
        $arr = preg_split('~ - ~', $time);
        $dateArr = preg_split('~/~', $arr[0]);
        return $dateArr[2] . '-' . $dateArr[1] . '-' . $dateArr[0] . ' ' . $arr[1];
    }

    private function _processLynxResultsFile($file) {
        $headerLine = fgets($file);
		$headerLine = str_replace("\0", "", $headerLine);
        $headerValues = preg_split('~,~', $headerLine);
        $time = $headerValues[10];
        $time = preg_split('~\.~', $time);
        $eventTime = $time[0];
        $sched = $this->scheduleTime;
        $schedArr = preg_split('~ ~', $sched);
        $this->eventTime = $schedArr[0] . ' ' . $eventTime;
        $ws = $headerValues[4];
        $results = array();
        while ($line = fgets($file)) {
//			remove null characters ( Loughborough )
			$line = str_replace("\0", "", $line);
			if ( is_null($line) or $line === '' or strlen($line) < 2){
				continue;
			}

            $lineArr = preg_split('~,~', $line);
//	        e4s_addDebugForce($lineArr);
            $result = new stdClass();
            $score = '';

            if ($lineArr[6] !== '') {
                $score = resultsClass::getResultInSeconds($lineArr[6]);
            }
            $result->scoreText = $lineArr[7];
            if ($result->scoreText !== '') {
                $result->scoreText = ' ' . $result->scoreText;
            }
            $place = strtoupper($lineArr[0]);
            if ($place === 'DQ' or $place === 'DNS' or $place === 'DNF') {
                $score = $place;
                $place = 0;

            }
            $result->place = $place;
            $result->bibNo = $lineArr[1];
            $result->lane = $lineArr[2];
            $result->club = $lineArr[5];
            $result->athlete = trim($lineArr[4] . ' ' . $lineArr[3]);
            $result->firstName = $lineArr[4];
            $result->surName = $lineArr[3];
	        $result->athleteId = 0;
			$egObj = $this->compObj->getEventGroupByNo($this->eventNo);

			if ( $egObj->options->isTeamEvent ){
				$result->clubId = $this->_getClubId($result->club);
				$result->teamId = $result->clubId;
				$result->teamName = $result->surName;
				if ( $result->teamName === "" ){
					$result->teamName = $result->club;
				}
				$result->athlete = "-";
			}else{
				$this->_getLynxAthlete($result);

				if (trim($result->athlete) === '' and $result->athleteId > 0) {
					$aSql = "select concat(a.firstname,' ',a.surname) athlete,
                                c.clubname club
                         from " . E4S_TABLE_ATHLETE . ' a,
                              ' . E4S_TABLE_CLUBS . ' c 
                         where c.id = a.clubid
                         and a.id = ' . $result->athleteId;
					$aResult = e4s_queryNoLog($aSql);
					if ($aResult->num_rows === 1) {
						$aObj = $aResult->fetch_object();
						$result->athlete = $aObj->athlete;
						$result->club = $aObj->club;
					}
				}
			}

            $result->time = $score;
            $result->ageGroup = 'Inters';
            $results[$result->bibNo] = $result;
            if ($lineArr[12] !== '') {
                $result->scoreText = $lineArr[12];
                $note = $this->typeNo . ':' . $this->eventName . '. ' . $lineArr[12];
                $this->compObj->addNote($result->athleteId, $note);
            }
        }

        $this->results = $results;

        $sqlCont = '';
        $sql = '';

        foreach ($results as $result) {
            $options = $result;
            $options->ws = $ws;
            $options->eventTime = $eventTime;
            if ($sql === '') {
                $clearSql = 'delete from ' . E4S_TABLE_CARDRESULTS . '
                             where compid = ' . $this->compId . '
                             and eventno = ' . $this->eventNo . "
                             and resultKey = 'h" . $this->heatNo . "'";
                e4s_queryNoLog($clearSql);
                $sql = 'insert into ' . E4S_TABLE_CARDRESULTS . ' (compid, athleteid, eventno, resultkey, resultvalue, options)
                        values ';
            }
            if ($result->athleteId !== '-') {
                $sql .= $sqlCont . '(';
                $sql .= $this->compId . ',';
				if ( $result->athleteId > 0 ) {
					$sql .= "'" . $result->athleteId . "',";
				}else{
					$sql .= "'" . $result->teamId . "',";
				}
                $sql .= $this->eventNo . ',';
                $sql .= "'h" . $this->heatNo . "',";
                $sql .= "'" . $result->time . "',";
                $sql .= "'" . addslashes(e4s_encode($options)) . "'";
                $sql .= ')';
                $sqlCont = ',';
            }
        }
        if ($sql !== '') {
            e4s_queryNoLog($sql);
        }
    }

	private function _getClubId($club){
		$sql = 'select * 
                from ' . E4S_TABLE_CLUBS . "
                where clubname = '" . addslashes($club) . "'
                and clubtype in ('T','C','A')"; // Temp, Club or Association ( ESAA )
		$result = e4s_queryNoLog($sql);
		if ($result->num_rows === 1) {
			$clubObj = $result->fetch_object(E4S_CLUB_OBJ);
			return $clubObj->id;
		} else {
			$sql = 'insert into ' . E4S_TABLE_CLUBS . " (clubname, clubtype) values ('" . addslashes($club) . "','T')";
			e4s_queryNoLog($sql);
			return e4s_getLastID();
		}
	}
    private function _getLynxAthlete(&$inboundResult) {
        $useBibNo = $inboundResult->bibNo;
        $firstName = addslashes($inboundResult->firstName);
        $surName = addslashes($inboundResult->surName);
//        $checkAgeGroupId = 214;
        $sql = 'select e.athleteId
            from Entry4_uk_Entries e left join ' . E4S_TABLE_BIBNO . ' b on e.athleteid = b.athleteid and b.compid = ' . $this->compId . ', 
                 Entry4_uk_CompEvents ce,
                 Entry4_uk_EventGroups eg
            where e.compEventID = ce.ID
            and eg.id = ce.maxGroup
            and ce.compid = ' . $this->compId . '
            and eventNo = ' . $this->eventNo . "
            and (teambibno= '" . $useBibNo . "' or b.bibno = '" . $useBibNo . "' )";
// comp will be using bibs or team/county bibs
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            $obj = $result->fetch_object();
            $inboundResult->athleteId = $obj->athleteId;
            return;
        }
        // try to find via heats
//        $sql = '
//            select distinct e.athleteId
//            from Entry4_uk_Entries e
//            where e.compEventID = (
//                select ce.id
//                from Entry4_uk_CompEvents ce
//                where ce.compid = ' . $this->compId . '
//                and AgeGroupID = ' . $checkAgeGroupId . '
//                and maxathletes != -1
//                and ce.EventID = (
//                    select  ce.EventID
//                    from  Entry4_uk_CompEvents ce,
//                          Entry4_uk_EventGroups eg
//                    where ce.compid = ' . $this->compId . '
//                    and   ce.maxGroup = eg.id
//                    and   eg.eventNo = ' . $this->eventNo . '
//                    and AgeGroupID = ' . $checkAgeGroupId . "
//                )
//            )
//            and (teambibno = '" . $useBibNo . "' or bibno = '" . $useBibNo . "')
//        ";
//
//        $result = e4s_queryNoLog($sql);
//        if ($result->num_rows === 1) {
//            $obj = $result->fetch_object();
//            $inboundResult->athleteId = $obj->athleteId;
//            return;
//        }
	    $inboundResult->clubId = $this->_getClubId($inboundResult->club);

        $sql = 'select *
                from ' . E4S_TABLE_ATHLETE . "
                where firstname = '" . $firstName . "'
                and surname = '" . $surName . "'
                and gender = 'M'
                and type = 'T'
                and clubid = " . $inboundResult->clubId;

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            $obj = $result->fetch_object(E4S_ATHLETE_OBJ);
            $inboundResult->athleteId = $obj->id;
            $inboundResult->athlete = formatAthlete($obj->firstName, $obj->surName);
        } else {
            $sql = 'Insert into ' . E4S_TABLE_ATHLETE . ' (firstname, surname, dob, gender, clubid, type) 
                    values ("' . $firstName . '", "' . $surName . '","2000-01-01","M",' . $inboundResult->clubId . ' , "T")';
            e4s_queryNoLog($sql);
            $inboundResult->athleteId = e4s_getLastID();
            $inboundResult->athlete = formatAthlete($firstName, $surName);
			// give new athlete a bib Number
	        $bibObj = new bibClass($this->compId);
	        $inboundResult->bibNo = $bibObj->allocateBibForNewAthlete($inboundResult->athleteId);
        }
    }

    private function _updateTrackResultInfo() {
        $result = $this->_getExistingTrackResult();
        if ($result->num_rows === 0) {
            $updateSql = 'insert into ' . E4S_TABLE_TRACKRESULT . ' (compid, eventno, heatno, time, ws, image)
                          values (' . $this->compId . ',' . $this->eventNo . ',' . $this->heatNo . ",'" . $this->eventTime . "','" . $this->ws . "','" . $this->_getImage() . "')";
        } else {
            $existObj = $result->fetch_object();
            if ($existObj->image === '') {
                $existObj->image = $this->compObj->getScoreboardImage();
            }
            $updateSql = 'update ' . E4S_TABLE_TRACKRESULT . "
                          set time = '" . $this->eventTime . "',
                              image = '" . $existObj->image . "',
                              ws = '" . $this->ws . "'
                          where id = " . $existObj->id;
            $this->_setImage($existObj->image);
        }

        e4s_queryNoLog($updateSql);
    }

    private function _getExistingTrackResult() {
        $sql = 'select id id,
                        image image
                from ' . E4S_TABLE_TRACKRESULT . '
                where compid = ' . $this->compId . '
                and eventno = ' . $this->eventNo . '
                and heatno = ' . $this->heatNo;
        return e4s_queryNoLog($sql);
    }

    private function _updateTrackResultImage() {
        $result = $this->_getExistingTrackResult();
        if ($result->num_rows === 0) {
            $updateSql = 'insert into ' . E4S_TABLE_TRACKRESULT . ' (compid, eventno, heatno, image)
                          values (' . $this->compId . ',' . $this->eventNo . ',' . $this->heatNo . ",'" . $this->_getImage() . "')";
        } else {
            $existObj = $result->fetch_object();
            $updateSql = 'update ' . E4S_TABLE_TRACKRESULT . "
                          set image = '" . $this->_getImage() . "'
                          where id = " . $existObj->id;
        }

        e4s_queryNoLog($updateSql);
        e4s_uploadTrackImageToResults($this);
    }

    public function sendSocketMsg($eventNo = 0, $heatNo = 0) {
        // if no results, dont send just picture ?
        // if only results, send a blank picture ?
        if ($eventNo === 0) {
            $eventNo = $this->eventNo;
        }
        if ($heatNo === 0) {
            $heatNo = $this->heatNo;
        }
        $results = $this->getResultsAndMetaForEvent($eventNo, $heatNo);

        if (sizeof($results->results) > 0 or $this->_isVideoFile()) {
            $this->buildAndSendMsg($results);
        }
    }

    public function buildAndSendMsg($results) {
        require E4S_FULL_PATH . 'vendor/autoload.php';
        $socketObj = new stdClass();
        $socketObj->action = 'sendmessage';
        $compObj = e4s_getCompObj($this->compId);

        $socketCompObj = new stdClass();
        $socketCompObj->id = $this->compId;
        $socketCompObj->name = $compObj->getName();

        $data = new stdClass();
        $data->key = $this->eventNo . '-' . $this->heatNo;
        $data->comp = $socketCompObj;

        $data->deviceKey = '';
        $data->securityKey = '';
        $data->domain = E4S_CURRENT_DOMAIN;
        $payload = new stdClass();
        $image = $this->_getImage();
        if ($image === '') {
            $image = $compObj->getScoreboardImage();
        }

        $athlete = new stdClass();
        if ($this->_isVideoFile()) {
            $data->action = R4S_SOCKET_VIDEO;
            $payload->video = $image;
            $athlete->id = $this->athleteId;
            $athlete->name = $this->athlete;
            $athlete->bibNo = '' . $this->bibNo;
        } else {
            $data->action = R4S_SOCKET_PF;
            $payload->picture = $image;
        }

        $payload->athlete = $athlete;
        $payload->comp = $socketCompObj;
        $eg = new stdClass();
		if ( $this->eventNo === 0 ){
			$egObj = new stdClass();
			$egObj->id = 0;
		}else{
			$egObj = $compObj->getEventGroupByNo($this->eventNo);
		}

        $eg->eventNo = $this->eventNo;
        $eg->heatNo = $this->heatNo;
        $eg->typeNo = $this->typeNo;
        $eg->name = $this->eventName;
        $eg->id = $egObj->id;
// duplicated for now, but Nick will clean up soon
        $payload->eventGroup = $eg;

        $payload->eventNo = $this->eventNo;
        $payload->heatNo = $this->heatNo;
        $payload->typeNo = $this->typeNo;
        $payload->eventName = $this->eventName;
        $payload->scheduleTime = $this->scheduleTime;
        $payload->eventTime = $this->eventTime;
        $results = $results->results;
        $newResults = array();
        foreach ($results as $result) {
            $result->options = e4s_getOptionsAsObj($result->options);
            if ($result->timeInSeconds > 0) {
                $result->resultvalue = e4s_ensureString(resultsClass::getResultFromSeconds($result->timeInSeconds));
            }
            if (isset($result->options->ageGroup)) {
                $result->ageGroup = $result->options->ageGroup;
            }
            $newResults[] = $result;
        }
        $payload->results = $newResults;
        $data->payload = $payload;
        $socketObj->data = e4s_getDataAsType($data, E4S_OPTIONS_STRING);
        $socketObj->data = e4s_removeEnsureString($socketObj->data);
//        $socketObj = $data;

        $GLOBALS['R4S_SOCKET_MSG'] = e4s_getDataAsType($socketObj, E4S_OPTIONS_STRING);
        connect(R4S_SOCKET_SERVER)->then(function ($conn) {

            $conn->send($GLOBALS['R4S_SOCKET_MSG']);
            $conn->close();
        }, function ($e) {
            echo "Could not connect: {$e->getMessage()}\n";
        });
    }

    public function upload() {
        $this->_readFileName('txt');
        if ($this->eventNo === null) {
            Entry4UIError(8300, 'Failed to process uploaded results file', 400);
        }

        if (!is_null($this->fileName)) {
            $this->_processUploadedFile();
        }
        $this->_readFileName('img');
        if ($this->eventNo === null) {
            Entry4UIError(8310, 'Failed to process image file', 403);
        }
        if (!is_null($this->fileName)) {
            $this->_processUploadedFile();
        }

        return '';
    }

    private function _readFileName($type) {
        $this->fileName = null;
        $this->tmpFileName = null;
        if (array_key_exists($type . 'FileToUpload', $_FILES)) {
            if ($_FILES[$type . 'FileToUpload']['name'] !== '') {
                $filename = basename($_FILES[$type . 'FileToUpload']['name']);
                $tmpfilename = $_FILES[$type . 'FileToUpload']['tmp_name'];

                $this->_processFileName($filename, $tmpfilename);
            }
        }
    }

    public function getEventInfoAndSend($egId, $heatNo) {
        $this->heatNo = $heatNo;
        $this->eventName = '';
        $this->typeNo = '';
        $this->scheduleTime = '';
        $this->eventTime = '';
        $sql = 'select eventNo
                      ,name
                      ,typeNo
                      ,startdate
                from ' . E4S_TABLE_EVENTGROUPS . '
                where compid = ' . $this->compId . '
                and id = ' . $egId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            $egObj = $result->fetch_object();
            $this->eventNo = (int)$egObj->eventNo;
            $this->eventName = $egObj->name;
            $this->typeNo = $egObj->typeNo;
            $this->scheduleTime = $egObj->startdate;
            $this->eventTime = $egObj->startdate;
            $this->sendSocketMsg((int)$egObj->eventNo, $heatNo);
        }

    }

    private function _getPreviousImages() {
        $html = '<div>';

        $dir = $this->rootDir . $this->compDir . $this->pfDir;
        if (!file_exists($dir)) {
            $html .= 'No pictures available as yet.';
            $html .= '</div>';
            return $html;
        }
        $html .= $this->_getPreviousHeader();
        $html .= '</div>';

        $files = scandir($dir, 1);
        $filesSorted = array();
        $mostHeats = 0;
        foreach ($files as $file) {
            if (strpos($file, '.' . E4S_PICTURE_PNG) !== FALSE) {
                $this->_processFileName($file);
                $obj = new stdClass();
                $obj->eventNo = (int)$this->eventNo;
                $obj->heatNo = (int)$this->heatNo;
                if ($obj->heatNo > $mostHeats) {
                    $mostHeats = $obj->heatNo;
                }
                $obj->sortKey = ($this->eventNo * 100) + $this->heatNo;
                $obj->eventName = $this->eventName;
                $obj->fileName = $file;
                $filesSorted[] = $obj;
            }
        }
        $filesSorted = $this->_array_sort($filesSorted, 'sortkey');
        $html .= "<div id='imageList'>";
        $html .= '<table cellspacing="0" cellpadding="0">';
        $html .= '<tr>';
        $html .= '<td style="width:70px;">';
        $html .= 'Event';
        $html .= '</td>';
        $html .= '<td style="width:50px;">';
        $html .= 'No.';
        $html .= '</td>';
        $html .= '<td colspan="' . $mostHeats . '">';
        $html .= 'Heat No';
        $html .= '</td>';
        $html .= '</tr>';
        $lastEventNo = 0;
        foreach ($filesSorted as $fileObj) {
//            $this->_processFileName($fileObj->fileName);
            $eventNo = $fileObj->eventNo;
            $heatNo = $fileObj->heatNo;
            $fileName = $fileObj->fileName;
            $eventName = $fileObj->eventName;
            $outputNo = $this->outputNo;
            if ($eventNo !== $lastEventNo) {
                if (0 !== $lastEventNo) {
                    $html .= '</tr>';
                }
                $html .= '<tr style="height:4vh;">';
                $html .= '<td><span style="font-weight: bold;">' . $eventName . '</span></td>';
                $html .= '<td>' . $eventNo . '</td>';
                $lastEventNo = $eventNo;
            }
            $html .= '<td style="width:40px;text-align: center;"><a href="#" onclick="getResultsAndSendTrackInfoToSocket(' . $eventNo . ',' . $heatNo . ',' . $outputNo . ',\'' . $fileName . "\') return false;\"><span style=\"padding:30px;\">'" . $heatNo . '</span></a></td>';
        }
        $html .= '<table>';
        $html .= '</div>';
        return $html;
    }

    private function _getPreviousHeader() {
        $html = '';
        if (hasScoreboardAccess()) {
            $html .= 'Upload a new image or select from one below.<br><br>';
        } else {
            $html .= 'Select Event/Heat from the list below.<br><br>';
        }

        return $html;
    }

    private function _isVideoFile() {
        $fileName = $this->_getFileName();
        return $this->postProcessObj::isVideo($fileName);
    }

    private function _processFileName($tmpFileName = null) {
        $fileName = $this->_getFileName();
        // defunct call ???? Was ESAA 2021. no longer required
        $adjustEventNo = $this->compObj->getEventAdjustment();
//        logTxt("Adjustment :: " . $adjustEventNo);
        $this->tmpFileName = $tmpFileName;
        $this->inputType = '';
        $egObj = null;
        if ($this->_isVideoFile()) {
            if (strpos($fileName, '-') === FALSE) {
//                Entry4UIError(8400, 'Videos should begin with event Type and No. eg F1-U13 Long Jump');
				$egObj = new stdClass();
				$egObj->id = 0;
				$egObj->eventNo = 0;
	            $date = date_create($this->compObj->getDate());
//				$egObj->typeNo = date_format($date, 'd M Y');
				$egObj->typeNo = '';
				$egObj->startDate = null;
				$egObj->name = $this->compObj->getName();
            }else {
	            $split       = preg_split( '~-~', $fileName );
	            $eventTypeNo = $split[0]; // Should be F99 format so we can get the event for the video
	            $egObj       = $this->compObj->getEventGroupByTypeNo( $eventTypeNo );
	            if ( is_null( $egObj ) ) {
		            Entry4UIError( 8001, 'Unable to identify the event group for ' . $eventTypeNo );
	            }
	            $this->eventNo = $egObj->eventNo;
	            $this->_getNextUpInfo( $egObj );
            }
        } elseif (strpos($fileName, ',') > 0 or strpos($fileName, '.' . E4S_TT_RESULT_EXT) > 0) {
            $this->inputType = E4S_PF_TT;
            $split = preg_split('~,~', $fileName);
            $eventName = $split[1];
            $eventNameArr = preg_split('~\.~', $eventName);
            $this->eventName = $eventNameArr[0];
            $split = preg_split('~h~', $split[0]);
            $this->heatNo = (int)$split[1];
            $split = preg_split('~E~', $split[0]);
            $this->eventNo = (int)$split[1];
        } elseif (strpos($fileName, '-') > 0 or strpos($fileName, '.' . E4S_LYNX_RESULT_EXT) > 0) {
            $this->inputType = E4S_PF_LYNX;
            $split = preg_split('~\.~', $this->fileName);
            $prefix = $split[0];
            $prefixArr = preg_split('~-~', $prefix);
            $this->eventNo = (int)$prefixArr[0];
            $this->heatNo = (int)$prefixArr[2];
        } else {
//            Entry4Error(2021,"Please only send the correctly formatted images.");
            Entry4UIError(8005, 'Unable to process file ' . $fileName);
        }
        $this->eventNo += $adjustEventNo; // should be
        if (is_null($egObj)) {
            $egObj = $this->compObj->getEventGroupByNo($this->eventNo);
        }

        if (is_null($egObj)) {
            Entry4UIError(7024, 'Unable to get event name for ' . $this->compId . ':' . $this->eventNo . '(' . $fileName . ')');
        }
        $this->typeNo = $egObj->typeNo;
        $this->eventName = $egObj->name;
        $this->scheduleTime = $egObj->startDate;
    }

    private function _getNextUpInfo($egObj) {
        include E4S_FULL_PATH . 'classes/e4sEventNextUp.php';
        $nextObj = new e4sEventNextUp($egObj->id);
        $this->heatNo = $nextObj->getHeatNo(); // trial
        $this->athleteId = $nextObj->getAthleteId();
        // perform lookup to table to get athlete up next

        if ($this->athleteId !== 0) {
            $this->athlete = $nextObj->getAthlete();
            $this->postProcessObj->params->text3 = 'Trial ' . $this->heatNo . ' - ' . $this->athlete;
        }
    }

    private function _array_sort($array, $on, $order = SORT_ASC) {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }
}