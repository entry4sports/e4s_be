<?php
define('E4S_REFUND_ALL', 0);
define('E4S_WARN_NONE', 0);
define('E4S_WARN_LEVEL_1', 1);
define('E4S_WARN_LEVEL_2', 2);
define('E4S_WARN_LEVEL_3', 3);
define('E4S_WARN_ALL', 99);
define('E4S_MAX_REFUND_COUNT', 50);

class e4sRefund {
    public $compid;
    public $compRow = null;
    public $orderIds;
    public $productid;
    public $productType;
    public $entryObj;
    public $eventids;
    public $ignoreOrders;
    public $reason = '';
    public $minE4SFee;
    public $minE4SValue;
    public $E4SPerc;
    public $removeEntry = TRUE;
    public $ignorePaid = FALSE;
    public $multiple = TRUE;
    public $refundAmount = 0;
    public $pricePaid = 0;
    public $refundE4SFee = TRUE;
    public $refundStripeFee = TRUE;
    public $perLine = FALSE;
    public $testmode = TRUE;
    public $email = TRUE;
    public $emailTxt = '';
    public $userid = E4S_USER_ANON_ID;
    public $config;
    public $logStr = '';
    public $refundOrderInfo = array();
    public $refundEmails = array();
    public $sendLogEmail = FALSE;
    public $credit = FALSE;
    public $warnLevel;
    public $tableStyle = "style='border: 1px solid #aaaaaa; border-spacing:0px;'";

    public $totals;

    public function __construct($compid = 0, $orderIds = [], $productid = 0) {
        $this->userid = e4s_getUserID();
        if ($compid === 0 and $productid === 0) {
            if (sizeof($orderIds) === 0 or $orderIds[0] === 0) {
                Entry4UIError(9037, 'You must pass at minimum a competition id, product id or at least 1 order number', 200, '');
            }
        }
        $this->compid = $compid;
        $this->orderIds = $orderIds;
        $this->ignoreOrders = array();
        $this->productid = (int)$productid;
        if ($this->compid === 0 and $this->productid > 0) {
            $this->_getCompForProduct();
        }
        $this->productType = '';
        $this->config = e4s_getConfig();
        $this->totals = new stdClass();
        $this->totals->refunded = 0;
        $this->totals->credited = 0;
        $this->totals->e4sfees = 0;
        $this->totals->stripefees = 0;
        $this->warnLevel = E4S_WARN_ALL;
        $this->setE4SFee();
    }

    public function setE4SFee() {
        if ($this->compid === 0) {
            Entry4UIError(5012, 'Failed to get Competition ID');
        }
        $compObj = e4s_GetCompObj($this->compid);
        $priceObj = $compObj->getPriceObj();
        $this->minE4SFee = $priceObj->getMinFee();
        $this->minE4SValue = $priceObj->minCost;
        $this->E4SPerc = $priceObj->getPerc();
    }

    public function performRefund($parms, $exit = TRUE) {
        $this->reason = $parms->reason;
        $this->multiple = TRUE;
        if (isset($parms->allowMultiple)) {
            $this->multiple = $parms->allowMultiple;
        }
        $this->refundAmount = 0;
        if (isset($parms->refundAmount)) {
            $this->refundAmount = $parms->refundAmount;
        }
        $this->pricePaid = 0;
        if (isset($parms->pricePaid)) {
            $this->pricePaid = $parms->pricePaid;
        }
        if (isset($parms->eventids)) {
            $this->eventids = $parms->eventids;
        }
        if (isset($parms->ignoreOrders)) {
            $this->ignoreOrders = $parms->ignoreOrders;
        }
        $this->productType = '';
        if (isset($parms->productType)) {
            $this->productType = $parms->productType;
            if (!($this->productType === 'F' or $this->productType === 'T')) {
                $this->productType = '';
            }
        }

        $this->testmode = TRUE;
        if (isset($parms->testMode)) {
            $this->testmode = $parms->testMode;
        }
        if (isset($parms->perLine)) {
            $this->perLine = $parms->perLine;
        }
        $this->credit = FALSE;
        if (isset($parms->credit)) {
            $this->credit = $parms->credit;
        }
        $this->removeEntry = TRUE;
        if (isset($parms->removeEntry)) {
            $this->removeEntry = $parms->removeEntry;
        }
        $this->ignorePaid = FALSE;
        if (isset($parms->ignorePaid)) {
            $this->ignorePaid = $parms->ignorePaid;
        }
        $this->refundE4SFee = FALSE;
        if (isset($parms->refundE4SFee)) {
            $this->refundE4SFee = $parms->refundE4SFee;
        }
        $this->refundStripeFee = FALSE;
        if (isset($parms->refundStripeFee)) {
            $this->refundStripeFee = $parms->refundStripeFee;
        }
        $this->emailTxt = '';
        if (isset($parms->emailTxt)) {
            $this->emailTxt = $parms->emailTxt;
        }
        $this->email = TRUE;
        if (isset($parms->email)) {
            $this->email = $parms->email;
        }
        $this->warnLevel = E4S_WARN_ALL;

        if (isset($parms->warnLevel)) {
            $this->warnLevel = $parms->warnLevel;
        }

        if (!$this->validateRefundParams()) {
            return FALSE;
        }

        $retval = '';

        if ($this->productid !== 0) {
// Get CompId
            $this->multiple = TRUE;
        }

        if ($this->orderIds[0] !== 0 or $this->productid !== 0) {
            foreach ($this->orderIds as $orderId) {
                $retval = $this->refundOrder($orderId);
                $retval = $this->generateReturnData('Order Processed', array($retval));
            }
        } elseif ($this->compid !== 0) {
            $retval = $this->refundOrderForCompByProduct();
            if (!empty ($this->refundEmails)) {
                $this->emailRefunds();
            }
        }

        $this->_emailOrganiser();
        if (!$this->testmode) {
            $this->_clearCache();
        }
        $retval = $this->addToReturnData($retval, 'logsent', $this->emailLog());

        if ($exit) {
            Entry4UISuccess($retval);
        }
        return $retval;
    }

    private function _clearCache() {
        if ($this->compid > 0) {
            $cacheObj = new cacheClass($this->compid);
            $cacheObj->clearCache();
        }
    }

    private function _getCompForProduct() {
        $productId = $this->productid;
        $wcProduct = wc_get_product($productId);
        $desc = $wcProduct->get_description();
        if ($desc !== '') {
            $prodDesc = e4s_getDataAsType($desc, E4S_OPTIONS_OBJECT);
            if (isset($prodDesc->compid)) {
                $this->compid = $prodDesc->compid;
            } else {
                Entry4UIError(5066, 'Failed to get Competition ID from Product');
            }
        }
        if ($this->orderIds[0] === 0) {
            $sql = 'select orderid
                    from ' . E4S_TABLE_ENTRIES . '
                    where variationid = ' . $this->productid;
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows !== 1) {
                Entry4UIError(5070, 'Failed to get Order from Product');
            }
            $row = $result->fetch_object();
            $this->orderIds[0] = (int)$row->orderid;
        }
    }

    private function validateRefundParams() {
        if (!empty($eventids)) {
            if ($this->compid === 0) {
                e4s_echo('Please specify a competition when passing an event id.');
                return FALSE;
            }
            if ($this->productid !== 0) {
                e4s_echo('You can not pass an event id and a product id.');
                return FALSE;
            }
        }
        return TRUE;
    }

    private function refundOrder($orderid, &$refundProcessed = FALSE) {

        $order = wc_get_order($orderid);
        $status = $this->getStatus();
        // If it's something else such as a WC_Order_Refund, we don't want that.
        if (!is_a($order, 'WC_Order')) {
            return $this->returnWarning("{$orderid} Is Not a WC Order", E4S_WARN_LEVEL_1);
        }

        if ('refunded' === $order->get_status()) {
            return $this->returnWarning("Order {$orderid} is already {$status}ed", E4S_WARN_LEVEL_1);
        }

        if ($order->get_status() === 'on-hold') {
            return $this->returnWarning("Order {$orderid} is on hold ( Cheque Payment )", E4S_WARN_LEVEL_1);
        }
        if ($order->get_status() === 'pending') {
            return $this->returnWarning("Order {$orderid} is pending payment", E4S_WARN_LEVEL_1);
        }
        if (!$this->multiple) {
            if (!empty($order->get_refunds())) {
                return $this->returnWarning("Order {$orderid} has already been {$status}ed. Pass Multiple {$status}s to process.", E4S_WARN_LEVEL_2);
            }
        }

        $orderTotal = floatval($order->get_total());
        if ($orderTotal < 0.01) {
            return $this->returnWarning("Order {$orderid} has a zero value", E4S_WARN_LEVEL_1);
        }
        $affectedLineTotal = 0;
        $stripeFee = floatval($order->get_meta('_stripe_fee'));

        $refundLineObjs = $this->calculateE4SFees($order, $affectedLineTotal, $affectedLineTotalE4SFees);

        $entryInfo = array();
        if (empty($refundLineObjs)) {
            $echoStr = $orderid . ' Has no entries to be processed';
            if ($this->compid !== 0) {
                $echoStr .= ' for the competition ' . $this->compid;
            }
            if ($this->ignorePaid === FALSE or $this->pricePaid !== 0) {
                return $this->returnWarning($echoStr, E4S_WARN_LEVEL_1);
            }
        } else {
            foreach ($refundLineObjs as $refundLineObj) {
                $entryInfo[] = $refundLineObj->entryInfo;
            }
        }

        $refundAmount = $this->refundAmount;
        if ($refundAmount === E4S_REFUND_ALL) {
            $refundAmount = $affectedLineTotal;
            if (!$this->refundE4SFee) {
                $refundAmount -= $affectedLineTotalE4SFees;
            } else {
                if (!$this->refundStripeFee) {
                    // This is incorrect if multiple refunds. Also if refunding indiv lines or whole order. Needs more thought. This should be
                    // Check the total refunded amount + this refund amount is not greater than the order amount - stripe fees.
                    $refundAmount -= $stripeFee;
                }
            }
        }

        $orderAlreadyRefunded = $order->get_total_refunded();
        $amountRemaining = $order->get_remaining_refund_amount();

//        if ("" . $amountRemaining === "" . $this->refundOrderInfo['amount'] ){
        if (e4s_compareDoubles($refundAmount, '=', 0)) {
            return $this->returnWarning("{$orderid} : Has a zero refund cost.", E4S_WARN_LEVEL_2);
        } elseif (e4s_compareDoubles($amountRemaining, '=', $refundAmount)) {
            $refundAmount = $amountRemaining;
        } elseif (e4s_compareDoubles($amountRemaining, '<', $refundAmount)) {
            return $this->returnWarning($orderid . ' : Attempt to refund more that order total. (Total Refunded:' . $orderAlreadyRefunded . ' + Refund Amount:' . $refundAmount . ' > Order Total:' . $amountRemaining . ')', E4S_WARN_LEVEL_3);
        }

        if ($this->perLine) {
            $refundAmount = $refundAmount * sizeof($refundLineObjs);
        }
        $this->refundOrderInfo = array('amount' => $refundAmount, 'reason' => $this->reason, 'order_id' => $orderid, 'refund_payment' => TRUE, 'productId' => $this->productid);

        $this->sendLogEmail = TRUE;

        if ($this->compid === 0 or $this->orderIds[0] === 0 or $this->productid === 0) {
            // only e4s users can do this
            if (!isE4SUser()) {
                Entry4UIError(9038, '1: You are not authorised to issue this credit/refund');
            }
        } else {
            if ($this->productid !== 0) {
                // refunding a specific entry
                $this->entryObj = $this->_getEntryObj($this->productid);
                if (is_null($this->entryObj)) {
                    Entry4UIError(9039, '2: Entry NOT found for refund !!!!');
                }
                if (!isE4SUser() and e4s_getUserID() !== $this->entryObj->userId) {
                    $compObj = e4s_GetCompObj($this->compid);
                    if (!$compObj->isOrganiser()) {
                        Entry4UIError(9040, '3: You are not authorised to issue this credit/refund');
                    }
                }
            }
        }

        $this->refundOrderInfo['sendto'] = $order->get_billing_email();
        $this->refundOrderInfo['name'] = $order->get_billing_first_name() . ' ' . $order->get_billing_last_name();
        $this->refundOrderInfo['events'] = implode('/', $entryInfo);
        $this->refundOrderInfo['affectedLineTotalE4SFees'] = $affectedLineTotalE4SFees;
        $this->refundOrderInfo['affectedLineTotal'] = $affectedLineTotal;
        $this->refundOrderInfo['totalAlreadyRefunded'] = $orderAlreadyRefunded;
        $this->refundOrderInfo['orderTotal'] = $orderTotal;
        $this->refundOrderInfo['stripeFee'] = $stripeFee;
        $this->refundOrderInfo['refundId'] = 0;
        if (!$this->testmode) {
            // Perform after wc refund due to db trigger
            $refundid = $this->auditRefund($refundLineObjs);
            $this->refundOrderInfo['refundId'] = $refundid;
            $this->markEntryRefunded($refundLineObjs);

	        if (!$this->credit) {
		        if ($this->_isLiveEnvironment()) {
			        $refund = wc_create_refund($this->refundOrderInfo);
			        if (is_wp_error($refund)) {
				        return $this->returnWarning($orderid . ' : Failed to create the refund (' . $order->get_status() . ') ', E4S_WARN_ALL);
			        }
		        }
	        } else {
		        $creditObj = new e4sCredit($order->get_customer_id());
		        $creditObj->addToCredit($refundAmount, 'Credit from Competition ' . $this->compid);
	        }
	        $refundProcessed = TRUE;
        }
	    //Dont need the following logging as this is global and could take up too much space
	    unset($this->refundOrderInfo['reason']);
	    unset($this->refundOrderInfo['refund_payment']);
// End of none logging fields
        $this->addToTotals();

        if (!isset($this->refundEmails[$this->refundOrderInfo['sendto']])) {
            $this->refundEmails[$this->refundOrderInfo['sendto']] = array();
        }
        $this->refundEmails[$this->refundOrderInfo['sendto']][] = $this->refundOrderInfo;
        return $this->outputRefund();
    }

    private function getStatus() {
        $status = 'refund';
        if ($this->credit) {
            $status = 'credit';
        }
        return $status;
    }

    private function returnWarning($warn, $warnLevel) {
        if ($this->warnLevel !== E4S_WARN_ALL) {
            if ($this->warnLevel > $warnLevel) {
                $warn = '';
            }
        }
        return $warn;
    }

    private function calculateE4SFees($order, &$orderTotal, &$e4sFees) {
        $e4sFees = 0;
        $orderTotal = 0;
        $refundObjs = array();
        $compObj = null;
        $items = $order->get_items();
        $e4sOutput = '';
        foreach ($items as $item) {
            $productid = (int)$item->get_product_id();

            $includeLine = TRUE;
            if ($this->productid === 0) {
                $productObj = $this->getCompForProduct($productid);
                $compid = $productObj->compid;
                $compObj = e4s_GetCompObj($compid, TRUE);
                $paid = $productObj->paid;

                if ($compid === 0 or $paid === E4S_ENTRY_NOT_PAID) {
                    $includeLine = FALSE;
                } else {
                    if ($this->compid !== 0 and $paid === E4S_ENTRY_PAID) {
                        if (is_null($this->compRow)) {
                            $this->getCompetitionInfo();
                        }
                        $includeLine = ((int)$this->compid === $compid);
                    }
                    if ($includeLine and $this->productType !== '') {
                        $entryObj = $this->_getEntryObj($productid);
                        if (is_null($entryObj)) {
                            $includeLine = FALSE;
                        } else {
                            if ($entryObj->productType !== $this->productType) {
                                $includeLine = FALSE;
                            }
                        }
                    }
                }
            } else {
                if ($productid !== (int)$this->productid) {
                    $includeLine = FALSE;
                } else {
                    $productObj = $this->checkProductIsPaid($productid);
                    if ($productObj->paid === E4S_ENTRY_NOT_PAID) {
                        $includeLine = FALSE;
                    }
                }
                if ($includeLine) {
                    $productObj = $this->getCompForProduct($this->productid);
                    $compid = $productObj->compid;
                    $compObj = e4s_GetCompObj($compid, TRUE);
                }
            }
            if ($includeLine) {
                $lineTotal = floatval($item->get_total());

                if ($this->pricePaid !== 0) {
                    if (!e4s_compareDoubles($lineTotal, '=', $this->pricePaid)) {
                        $includeLine = FALSE;
                    }
                }
            }
            if ($includeLine) {
                if (is_null($compObj)) {
                    Entry4UIError(9406, 'Unable to find competition for refund');
                }
                $orderTotal += $lineTotal;
//                $priceObj = new priceClass($this->compid, $compObj->isNationalComp());
                $priceObj = $compObj->getPriceObj();
                $athletePrice = $priceObj->getCostWithoutE4SFeeFromAthletePrice($lineTotal);
                $e4slinefee = $lineTotal - $athletePrice;
                $e4sFees += $e4slinefee;
                $obj = new stdClass();
                $obj->compid = $compid;
                $obj->orderid = $order->get_order_number();
                $obj->productid = $productid;
                $obj->entrytype = $productObj->type;
                $obj->entryInfo = $productObj->entryInfo;
                $obj->linevalue = $lineTotal;
                $obj->e4slinefee = $e4slinefee;
                $obj->athletePrice = $athletePrice;
                $refundObjs[] = $obj;
            }
        }
        if ($e4sOutput !== '') {
            exit($e4sOutput);
        }
        return $refundObjs;
    }

    private function getCompForProduct($productid) {
        $retObj = new stdClass();
        $retObj->compid = 0;
        $retObj->type = E4S_INDIV_ENTRY;
        $retObj->paid = E4S_ENTRY_NOT_PAID;
        $retObj->entryInfo = '';
        $row = null;
        $sql = 'select compid compid, e.id entryId, e.compEventId, e.paid paid,e.orderId, e.variationId productId, e.athlete entryName, ev.name eventName
                from ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_EVENTS . ' ev
                where e.variationid = ' . $productid . '
                and   ce.eventid = ev.id
                and   abs(e.compeventid) = ce.id';

        if (!empty($this->eventids)) {
            $sql .= ' and ce.eventid in (' . implode(',', $this->eventids) . ')';
        }
        $result = e4s_queryNoLog($sql);

        if ($result->num_rows === 0) {
            $sql = 'select compid compid, e.id entryId, ce.id compEventId, e.paid paid, e.orderId, e.productId productId, e.name entryName, ev.name eventName
                from ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTTEAMENTRIES . ' e,
                     ' . E4S_TABLE_EVENTS . ' ev
                where e.productid = ' . $productid . '
                and   ce.eventid = ev.id
                and   abs(e.ceid) = ce.id';

            if (!empty($this->eventids)) {
                $sql .= ' and ce.eventid in (' . implode(',', $this->eventids) . ')';
            }
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows === 1) {
                $retObj->type = E4S_TEAM_ENTRY;
            }
        }

        if ($result->num_rows === 1) {
            $row = $result->fetch_assoc();
        } elseif ($result->num_rows === 2) {
            $row1 = $result->fetch_object();
            $row2 = $result->fetch_object();
            if ($row1->compid === $row2->compid and $row1->paid === $row2->paid and $row1->productId === $row2->productId) {
                if ($row1->compEventId > 0) {
                    // delete row2 and send 1
                    $row = e4s_getDataAsType($row1, E4S_OPTIONS_ARRAY);
                    $this->_deleteEntry($retObj->type, $row2);
                } elseif ($row2->compEventId > 0) {
                    // delete row1 and send 2
                    $row = e4s_getDataAsType($row2, E4S_OPTIONS_ARRAY);
                    $this->_deleteEntry($retObj->type, $row1);
                }
            }
        }

        if (!is_null($row)) {
            $retObj->paid = (int)$row['paid'];
            $retObj->compid = (int)$row['compid'];
            $retObj->entryInfo = $row['entryName'] . '-' . $row['eventName'];
        }
        return $retObj;
    }

    private function _deleteEntry($type, $rowToDelete) {
        $table = E4S_TABLE_ENTRIES;
        if ($type === E4S_TEAM_ENTRY) {
            $table = E4S_TABLE_EVENTTEAMENTRIES;
        }
        $sql = 'select *
                from ' . $table . '
                where id = ' . $rowToDelete->entryId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            // fetch complete row for logging, in case needed for rebuild
            $row = $result->fetch_object();
            logObj($row);
            $sql = 'delete from ' . $table . '
                where id = ' . $rowToDelete->entryId;

            e4s_queryNoLog("REFUND - DELETE $table " . E4S_SQL_DELIM . $sql);
        }
    }

    private function getCompetitionInfo() {
        $compObj = e4s_GetCompObj($this->compid);
        $this->compRow = $compObj->getRow();
    }

    private function _getEntryObj($productId) {
        $sql = 'select e.id id,
                       e.userid userId,
                       e.orderid orderId,
                       ev.tf productType,
                       ev.gender
                from ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTS . ' ev
                where e.compEventID = ce.ID
                and ce.EventID = ev.id
                and e.variationid = ' . $productId;

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            return null;
        }
        $obj = $result->fetch_object();
        $obj->id = (int)$obj->id;
        $obj->userId = (int)$obj->userId;
        $obj->orderId = (int)$obj->orderId;
        $obj->productType = strtoupper($obj->productType);
        return $obj;
    }

    private function checkProductIsPaid($productid) {
        $retObj = new stdClass();
        $retObj->compid = 0;
        $retObj->type = E4S_INDIV_ENTRY;
        $retObj->paid = E4S_ENTRY_NOT_PAID;

        $sql = 'select paid paid
                from ' . E4S_TABLE_ENTRIES . "
                where variationid = {$productid}";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            $sql = 'select paid paid
                from ' . E4S_TABLE_EVENTTEAMENTRIES . "
                where productid = {$productid}";
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows === 1) {
                $retObj->type = E4S_TEAM_ENTRY;
            }
        }
        if ($result->num_rows === 1) {
            $row = $result->fetch_assoc();
            $retObj->paid = (int)$row['paid'];
        }
        return $retObj;
    }

    private function _isLiveEnvironment() {
        $env = strtoupper($this->config['env']);
        if ($env === 'PROD') {
            return TRUE;
        }
        return FALSE;
    }

    private function auditRefund($refundLineObjs) {
//        e4s_echo("Auditing Refund");
        $refundOrderArray = $this->refundOrderInfo;
        $orderid = $refundOrderArray['order_id'];
        $productId = $refundOrderArray['productId'];
        $refundAmount = $refundOrderArray['amount'];
        $options = new stdClass();

        $options->lines = $refundLineObjs;
        $options->order = $refundOrderArray;
        $options->stripefee = $this->refundStripeFee;
        $options->e4sfee = $this->refundE4SFee;

        $showOnReport = 1;
        // If removing the entry, no need to show on report
        if ($this->removeEntry) {
            $showOnReport = 0;
        }
        $credit = 0;
        if ($this->credit) {
            $credit = 1;
        }
        $insertSql = 'insert into ' . E4S_TABLE_REFUNDS . "
                           (compid, credit, orderid, productid, value, reason, showonreport, userid, options)
                      values (
                        {$this->compid},
                        {$credit},
                        {$orderid},
                        {$productId},
                        {$refundAmount},
                        '" . addslashes($this->reason) . "'," . $showOnReport . ',' . $this->userid . ",
                        '" . addslashes(e4s_getOptionsAsString($options)) . "'
                      )";

        e4s_queryNoLog($insertSql);

        return e4s_getLastID();
    }

    private function markEntryRefunded($refundLineObjs) {

        $productIds = array();
        foreach ($refundLineObjs as $refundLineObj) {
            $productIds[] = $refundLineObj->productid;
        }
        if (empty($productIds)) {
            // No products to refund
            return;
        }
        $this->_markEntriesTableRefunded($productIds, E4S_TABLE_ENTRIES);
        $this->_markEntriesTableRefunded($productIds, E4S_TABLE_EVENTTEAMENTRIES);
    }

    private function _markEntriesTableRefunded($productIds, $table) {
        // due to tables using different column names ! ARGGGG
        $productColumnName = 'variationid';
        if ($table === E4S_TABLE_EVENTTEAMENTRIES) {
            $productColumnName = 'productid';
        }

        $refundOrderArray = $this->refundOrderInfo;
        $sql = '
                select id id , orderid orderid, options options
                from ' . $table . '
                where ' . $productColumnName . ' in (' . implode(',', $productIds) . ')
               ';
        $result = e4s_queryNoLog($sql);
        $rows = $result->fetch_all(MYSQLI_ASSOC);

        foreach ($rows as $row) {
            $options = e4s_addDefaultEntryOptions($row['options']);

            $rowRefunds = array();
            if (isset($options->refunds)) {
                $rowRefunds = $options->refunds;
            }

            $refund = new stdClass();
            $refund->reason = addslashes(str_replace("\n", ' ', $this->reason));
            $refund->id = $refundOrderArray['refundId'];
            $refund->orderid = $row['orderid'];
            $refund->type = 'refund';
            if ($this->credit) {
                $refund->type = 'credit';
            }
            $rowRefunds[] = $refund;
            $options->refunds = $rowRefunds;
            $options = e4s_removeDefaultEntryOptions($options);
            $sql = '
                update ' . $table . '
                set ';
// set orderid and variationid to negative so they dont get updated by future multiple refunds
//                          orderid = -orderid,
//                          ". $productColumnName . " = -" . $productColumnName . ", ";
            if ($this->removeEntry) {
                $sql .= $this->_removeEntrySql();
            } else {
                $sql .= 'price = price - ' . $this->refundAmount . ',';
            }

            $sql .= " options = '" . addslashes(e4s_getOptionsAsString($options)) . "'
                where id = " . $row['id'];

            e4s_queryNoLog($sql);
        }
    }

    private function _removeEntrySql() {
        $sql = ' paid = ' . E4S_ENTRY_NOT_PAID . ',           
                          athleteid = -athleteid,
                          compeventid = -compeventid,
                          userid = -userid,';
        return $sql;
    }

    private function addToTotals() {
        if ($this->credit) {
            $this->totals->credited += $this->refundOrderInfo['amount'];
        } else {
            $this->totals->refunded += $this->refundOrderInfo['amount'];
            $this->totals->e4sfees += $this->refundOrderInfo['affectedLineTotalE4SFees'];
            $this->totals->stripefees += $this->refundOrderInfo['stripeFee'];
        }
    }

    private function outputRefund() {
        $this->addToLog(implode('</td><td ' . $this->tableStyle . '>', $this->refundOrderInfo));
        return implode(',', $this->refundOrderInfo);
    }

    private function addToLog($txt) {
        $this->logStr .= '<tr><td ' . $this->tableStyle . '>' . $txt . '</td></tr>';
    }

    private function generateReturnData($txt, $arr = array()) {
//        $retval = '"response":"' . $txt . '"';

        $retval = $this->addToReturnData('', 'response', $txt);
        if (!empty($arr)) {
            $eventIDs = implode(',', $this->eventids);
            if ($eventIDs === '') {
                $eventIDs = '"All"';
            }
            $retval .= ',"payload":{
              "competition":' . $this->compid . ',
              "multiple":' . $this->getBooleanText($this->multiple) . ',
              "refundAmount":' . $this->refundAmount . ',
              "test":' . $this->getBooleanText($this->testmode) . ',
              "eventIds":[' . $eventIDs . '],
              "email":' . $this->getBooleanText($this->email) . ',
              "emailText":"' . $this->emailTxt . '",
              "reason":"' . $this->reason . '",
              "ignorePaid":' . $this->getBooleanText($this->ignorePaid) . ',
              "removeEntry":' . $this->getBooleanText($this->removeEntry) . ',
              "refundStripeFee":' . $this->getBooleanText($this->refundStripeFee) . ',
              "refundE4SFee":' . $this->getBooleanText($this->refundE4SFee) . '
            }';

            $retval .= ',"totals":{
              "credited":' . $this->totals->credited . ',
              "refunded":' . $this->totals->refunded . ',
              "e4sFees":' . $this->totals->e4sfees . ',
              "stripeFees":' . $this->totals->stripefees . '
            }';
            $retval .= ',"msgHeader":"';
            $sep = '';
            foreach ($this->refundOrderInfo as $header => $value) {
                $retval .= $sep . $header;
                $sep = ',';
            }
            $retval .= '"';
            $retval .= ',"msgs":["';
            $retval .= implode('","', $arr);
            $retval .= '"]';
        }
        return $retval;
    }

    private function addToReturnData($retData, $key, $txt) {
        if ($retData !== '') {
            $retData .= ',';
        }
        $retData .= '"' . $key . '":"' . $txt . '"';
        return $retData;
    }

    private function getBooleanText($bool) {
        if ($bool) {
            return 'true';
        }
        return 'false';
    }

    private function refundOrderForCompByProduct() {
        $sql = '
            SELECT ID id, post_content
            FROM ' . E4S_TABLE_POSTS . " 
            WHERE post_excerpt = 'compid:" . $this->compid . "'
        ";

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            return $this->generateReturnData('Nothing to process : ' . $sql);
        }

        $ceids = array();
        if (!empty($this->eventids)) {
            $sql4 = '
                select id
                from ' . E4S_TABLE_COMPEVENTS . '
                where compid = ' . $this->compid . '
                and eventid in (' . implode(',', $this->eventids) . ')
            ';
            $result4 = e4s_queryNoLog($sql4);
            if ($result4->num_rows === 0) {
                return $this->generateReturnData('No orders for specified events found');
            }
            $rows = $result4->fetch_all(MYSQLI_ASSOC);
            foreach ($rows as $row) {
                $ceids[$row['id']] = $row['id'];
            }
        }

        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $productIds = array();
        foreach ($rows as $row) {
            $processProduct = TRUE;
            if (!empty($ceids)) {
                $obj = e4s_getOptionsAsObj($row['post_content']);
                if ($obj->ceid) {
                    if (!array_key_exists($obj->ceid, $ceids)) {
                        $processProduct = FALSE;
                    }
                }
            }

            if ($processProduct) {
                $productIds[] = $row['id'];
            }
        }

        $sql2 = '
            SELECT order_item_id
            FROM ' . E4S_TABLE_WCORDERITEMMETA . " 
            WHERE meta_key = '" . WC_POST_PRODUCT_ID . "'
            and meta_value in (" . implode(',', $productIds) . ')
        ';

        $result2 = e4s_queryNoLog($sql2);
        if ($result2->num_rows < 1) {
            return $this->generateReturnData('Nothing to process : ' . $sql2);
        }
        $rows = $result2->fetch_all(MYSQLI_ASSOC);
        $orderItemsIds = array();
        foreach ($rows as $row) {
            $orderItemsIds[$row['order_item_id']] = $row['order_item_id'];
        }

        $sql3 = '
            SELECT distinct(order_id) orderid 
            FROM ' . E4S_TABLE_WCORDERITEMS . ' 
            WHERE order_item_id in (' . implode(',', $orderItemsIds) . ')
        ';

        if (!empty($this->ignoreOrders)) {
            $sql3 .= ' and order_id not in (' . implode(',', $this->ignoreOrders) . ')';
        }
        $result3 = e4s_queryNoLog($sql3);
        if ($result3->num_rows < 1) {
            return $this->generateReturnData('Nothing to process : ' . $sql3);
        }
        $rows = $result3->fetch_all(MYSQLI_ASSOC);

        $retArr = array();
        $processCount = 0;
        $refundProcessed = FALSE;
        foreach ($rows as $row) {
            $retVal = $this->refundOrder($row['orderid'], $refundProcessed);
            if ($retVal !== '') {
                $retArr[] = $retVal;
            }
            if ($refundProcessed) {
                $processCount++;
                if ($processCount >= E4S_MAX_REFUND_COUNT) {
                    break;
                }
            }
        }
        return $this->generateReturnData('Orders Processed : ' . $result3->num_rows, $retArr);
    }

    private function emailRefunds() {
        if (!$this->_shouldEmailsBeSent()) {
            return;
        }

        foreach ($this->refundEmails as $email => $refundArray) {
            $this->sendEmail($refundArray);
        }
    }

    private function _shouldEmailsBeSent() {
        if ($this->testmode or !$this->email) {
            return FALSE;
        }
        return TRUE;
    }

    private function sendEmail($orders) {

        $firstOrder = $orders[0];
        $sendTo = $firstOrder['sendto'];
        $currency = $this->config['currency'];
        $status = $this->getStatus();
        $body = 'Dear ' . $firstOrder['name'] . ',<br><br>';
        $body .= "  You have received a {$status} for order number(s) :<br>";
        $totalAmount = 0;
        $table = '';
        foreach ($orders as $order) {
            $table .= '<tr><td>' . $order['order_id'] . '</td><td>' . $currency . $order['amount'] . '</td></tr>';
            $totalAmount += (float)$order['amount'];
        }
        if ($table !== '') {
            $body .= "<table><tr><td>Order No</td><td>{$status} Amount</td></tr>";
            $body .= $table;
            $body .= '<tr><td> </td><td>==========</td></tr>';
            $body .= '<tr><td> </td><td>' . $currency . $totalAmount . '</td></tr>';
            $body .= '</table>';
        }
        $body .= '<br>';

        if (!is_null($this->compRow)) {
            $body .= 'Competition ' . $this->compRow['Name'] . '<br>';
        }

        $body .= "This {$status} has been issued because : " . $this->reason . '.<br><br>';

        if ($this->emailTxt !== '') {
            $body .= $this->emailTxt . '<br><br>';
        }
        if ($this->refundE4SFee and $this->refundStripeFee === FALSE) {
            $body .= "Entry4Sports fees are included in this {$status}, however the 3rd party transaction fees have already been paid and the 3rd party are not {$status}ing these. This is the reason for there not being a 100% {$status}. For this we are sorry.<br><br>";
        }
        if ($this->credit) {
            $body .= 'The credit is immediate and will show up next time you visit Entry4Sports.';
        } else {
            $body .= 'The refund should show up on your original payment method within the next 2-3 business days.';
        }

        $body .= "Feel free to contact us at support@entry4sports.com if there is an issue with this {$status}.";
        $body .= '<br><br>';
//        $body .= "Please note. If you have entered other competitions that have not been run due to Covid-19, please be patient as we are processing 1 competition at a time and you may receive further credits or refunds.<br><br>";
        $body .= 'Thanks<br>E4S Support<br>';

        if (!$this->_isLiveEnvironment()) {
            $body .= '<br>Originally sent to : ' . $sendTo . '<br>';
            $sendTo = E4S_REFUNDS_EMAIL;
        }
        $body .= Entry4_emailFooter($sendTo);

        e4s_mail($sendTo, $this->config['systemName'] . ' ' . ucfirst($status) . ' Notice', $body, Entry4_mailHeader('Support', FALSE));
    }

    private function _emailOrganiser() {
        if (!$this->_shouldOrganiserBeEmailed()) {
            return;
        }

        $contactObj = new e4sContactClass($this->compid);
        $contactName = 'Entry4Sports';
        $contactEmail = E4S_REFUNDS_EMAIL;
        $orgContactEmail = $contactObj->getContactEmail();
        if ($this->_isLiveEnvironment()) {
            if ($orgContactEmail !== '') {
                $contactEmail = $orgContactEmail;
            }

            $contactName = $contactObj->getUserName();
            if ($contactName === '') {
                $contactName = 'Organiser';
            }
        }
        $titles = array();
        $body = 'Dear ' . $contactName . '<br><br>';
        $body .= 'The following athlete(s) have had their entries cancelled:<br>';
        foreach ($this->refundEmails as $email => $refunds) {
            foreach ($refunds as $refund) {
                if ($refund['productId'] > 0) {
                    $product = wc_get_product($refund['productId']);
                    $titles[] = $product->get_title();
                }
            }
        }
        if (empty($titles) or sizeof($titles) < 1) {
            return;
        }
        $body .= implode('<br>', $titles);
        $body .= '<br>Reason : ' . $this->reason . '<br><br>';
        $body .= Entry4_emailFooter();

        e4s_mail($contactEmail, 'Athlete cancellation notice', $body, Entry4_mailHeader('Support', FALSE));
    }

    private function _shouldOrganiserBeEmailed() {
        return $this->_isLiveEnvironment() and $this->testmode;
    }

    private function emailLog() {
        if (!$this->sendLogEmail) {
            return 'No';
        }
        $useSendTo = 'paul@entry4sports.com';
        $name = 'Entry4Sports';
        $testmode = 'false';
        $subjectTest = '';
        if ($this->testmode) {
            $testmode = 'true';
            $subjectTest = ' (TEST)';
        }
        $eventIDs = implode(',', $this->eventids);
        if ($eventIDs === '') {
            $eventIDs = 'All';
        }

        $orderId = implode(',', $this->orderIds);
//        if ( $this->orderIds === 0 ){
        if ((int)$orderId === 0) {
            $orderId = 'All';
        }
        $productId = $this->productid;
        if ($this->productid === 0) {
            $productId = 'All';
        }
        $body = 'Dear ' . $name . ',<br><br>';

        $body .= 'Parameters:<br>';
        $body .= '<table>';
        $body .= '<tr><td>Testmode</td><td>' . $testmode . '</td></tr>';
        $body .= '<tr><td>Competition</td><td>' . $this->compid . '</td></tr>';
        $body .= '<tr><td>Orderid</td><td>' . $orderId . '</td></tr>';
        $body .= '<tr><td>Product ID</td><td>' . $productId . '</td></tr>';
        $body .= '<tr><td>Event IDs</td><td>' . $eventIDs . '</td></tr>';
        $body .= '<tr><td>Reason</td><td>' . $this->reason . '</td></tr>';
        $body .= '<tr><td>Remove Entry</td><td>' . $this->getBooleanText($this->removeEntry) . '</td></tr>';
        $body .= '<tr><td>Multiple</td><td>' . $this->getBooleanText($this->multiple) . '</td></tr>';
        if ($this->credit) {
            $body .= '<tr><td>Credited Amount</td><td>' . $this->refundAmount . '</td></tr>';
        } else {
            $body .= '<tr><td>Refund Amount</td><td>' . $this->refundAmount . '</td></tr>';
            $body .= '<tr><td>Refund E4SFee</td><td>' . $this->getBooleanText($this->refundE4SFee) . '</td></tr>';
            $body .= '<tr><td>Refund StripeFee</td><td>' . $this->getBooleanText($this->refundStripeFee) . '</td></tr>';
        }

        $body .= '<tr><td>Email</td><td>' . $this->getBooleanText($this->email) . '</td></tr>';
        $body .= '<tr><td>Email Text</td><td>' . $this->emailTxt . '</td></tr>';
        $body .= '<tr><td>Userid</td><td>' . $this->userid . '</td></tr>';
        $body .= '<tr><td>Fees Used in Calculations</td><td>&nbsp;</td></tr>';
        $body .= '<tr><td>Min E4SFee</td><td>' . $this->minE4SFee . '</td></tr>';
        $body .= '<tr><td>Min E4SValue</td><td>' . $this->minE4SValue . '</td></tr>';
        $body .= '<tr><td>E4SPerc</td><td>' . $this->E4SPerc . '</td></tr>';
        $body .= '<tr><td>Totals</td><td>&nbsp;</td></tr>';
        if ($this->credit) {
            $body .= '<tr><td>Credited</td><td>' . $this->totals->credited . '</td></tr>';
        } else {
            $body .= '<tr><td>Refunded</td><td>' . $this->totals->refunded . '</td></tr>';
            $body .= '<tr><td>E4S Fees</td><td>' . $this->totals->e4sfees . '</td></tr>';
            $body .= '<tr><td>Stripe Fees</td><td>' . $this->totals->stripefees . '</td></tr>';
        }

        $body .= '</table>';

        $body .= '<br>';
        $body .= $this->logHeader();
        $body .= $this->logStr;
        $body .= $this->logFooter();
        $body .= Entry4_emailFooter($useSendTo);
        $status = $this->getStatus();
        $logSent = 'Yes';
        if (!e4s_mail($useSendTo, $this->config['systemName'] . ' ' . ucfirst($status) . ' Notice of Run' . $subjectTest, $body, Entry4_mailHeader('Support', FALSE))) {
            $logSent = 'No';
        }
        return $logSent;
    }

    private function logHeader() {
        $orderArray = $this->refundOrderInfo;

        $str = '<table ' . $this->tableStyle . '>';
        $str .= '<tr>';

        foreach ($orderArray as $name => $value) {
            $str .= '<td ' . $this->tableStyle . '>' . $name . '</td>';
        }
        $str .= '</tr>';
        return $str;
    }

    private function logFooter() {
        $str = '</table>';
        return $str;
    }

    public function removeEntryOnly($entryObj) {
        $sql = 'update ' . E4S_TABLE_ENTRIES . ' set ';
        $sql .= $this->_removeEntrySql();
        $options = e4s_getOptionsAsObj($entryObj->eOptions);
        $options->reason = 'Removed as on Waiting List';
        $options = e4s_removeDefaultEntryOptions($options);
        $sql .= " options = '" . e4s_getOptionsAsString($options) . "'";
        $sql .= ' where id = ' . $entryObj->entryId;
        e4s_queryNoLog($sql);
    }

    private function getProductsOrderId($productid) {
        $sql = 'select paid paid, orderid orderid
                from ' . E4S_TABLE_ENTRIES . '
                where variationid = ' . $productid;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            return 0;
        }
        $row = $result->fetch_assoc();

        if ($row['paid'] !== E4S_ENTRY_PAID) {
            return 0;
        }
        return $row['orderid'];
    }

    private function refundOrderForCompByEntries() {
        $sql = '
            select distinct(e.orderid) orderid
            from ' . E4S_TABLE_ENTRIES . ' e
            where paid = ' . E4S_ENTRY_PAID . ' 
            and orderid > 0
            and compeventid in (
                select ce.id
                from ' . E4S_TABLE_COMPEVENTS . ' ce
                where compid = ' . $this->compid;
        if (!empty($this->eventids)) {
            $sql .= ' and ce.eventid in (' . implode(',', $this->eventids) . ') ';
        }
        $sql .= '
            )
        ';
//      e4s_echo($sql);
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            e4s_echo('Nothing to process : ' . $sql);
            return;
        }
//      e4s_echo("Rows : " . $result->num_rows );
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        foreach ($rows as $row) {
            e4s_echo('Order : ' . $row['orderid'], '');
            $this->refundOrder($row['orderid']);
        }
        e4s_echo('Rows Processed : ' . $result->num_rows);
    }
}