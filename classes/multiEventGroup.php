<?php
include_once E4S_FULL_PATH . 'dbInfo.php';

class multiEventGroup {
    public int $egId;
    public e4sCompetition $compObj;

    public function __construct($egId, $compObj = null) {
        $this->egId = $egId;
        if (!is_null($compObj)) {
            $this->compObj = $compObj;
        }

        if ($egId !== 0) {
            if (is_null($compObj)) {
                $this->_getCompObj();
            }
        }
    }

    private function _getCompObj() {
        $sql = 'select compId
                from ' . E4S_TABLE_EVENTGROUPS . '
                where id = ' . $this->egId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(9642, 'Unable to find eventGroup for ' . $this->egId);
        }
        $obj = $result->fetch_object();
        $this->compObj = e4s_getCompObj((int)$obj->compId, FALSE, TRUE);
    }

//    Called when saving a CE Record
    public function writeCEOptions($model) {

        return $model;
    }

    // called to populate the ceOptions from the eventGroup ( reading )
    public function updateCEOptions($egId, $ceId, $ceOptions, $egOptions) {
        $multiEventOptions = new stdClass();
        $multiEventOptions->childEvents = [];

        $ceOptions->entriesFrom = $egOptions->entriesFrom;
        $ceOptions->entriesFrom->name = '';
        $ceOptions->entriesFrom->eventNo = 0;
        $ceOptions->entriesFrom->typeNo = '';
        if ($egOptions->entriesFrom->id !== 0) {
            $egObj = $this->compObj->getEventGroupByEgId($egOptions->entriesFrom->id, true);
            if (!is_null($egObj)) {
                $ceOptions->entriesFrom->name = $egObj->name;
                $ceOptions->entriesFrom->eventNo = $egObj->eventNo;
                $ceOptions->entriesFrom->typeNo = $egObj->typeNo;
            } else {
                $ceOptions->entriesFrom->id = 0;
                $this->_removeInvalidEntriesFrom($egId, $egOptions);
            }
        }
        if ($ceOptions->entriesFrom->id === 0) {
            // check for child events
            $multiEventOptions->childEvents = $this->getEventGroupChildEvents($egId, FALSE);
        }

        $ceOptions->multiEventOptions = $multiEventOptions;
        return $ceOptions;
    }

    private function _removeInvalidEntriesFrom(int $egId, stdClass $egOptions) {
        $egOptions->entriesFrom->id = 0;
        $this->compObj->eventGroups[$egId]->options = $egOptions;
        $egOptions = e4s_removeDefaultEventGroupOptions($egOptions);
        $sql = sprintf('update %s
                set options = \'%s\'
                where id = %d', E4S_TABLE_EVENTGROUPS, json_encode($egOptions), $egId);
        e4s_queryNoLog($sql);
    }

    // update the options in the EventGroup
    public function updateEventGroup($model) {
        // Currently comes in as object, we only need the egId
        $entriesFrom = new stdClass();
        $entriesFrom->id = $model->ceOptions->entriesFrom->id;
        $model->options->entriesFrom = $entriesFrom;
        return $model;
    }

    public function saveEventGroup($model) {
        // Currently comes in as object, we only need the egId
        if ($model->options->entriesFrom === 0) {
            $egObjs = $this->compObj->getEGObjs();
            $model->options->entriesFrom = $egObjs[$this->egId]->options->entriesFrom;
        }
    }
    // Reading comp data, so build up the child events from those defined
    // $getCeIds - if true, then return the ceId and eventId ( not currently used )
    public function getEventGroupChildEvents($egId = null) {
        if (is_null($egId)) {
            $egId = $this->egId;
        }
        $childEvents = array();
        $eventGroups = $this->compObj->getEGObjs();
        if (!array_key_exists($egId, $eventGroups)) {
            $eventGroups = $this->compObj->getEGObjs(TRUE);
            if (!array_key_exists($egId, $eventGroups)) {
                Entry4UIError(9360, 'Invalid Event Group : ' . $egId);
            }
        }

        foreach ($eventGroups as $eventGroup) {
            $options = $eventGroup->options;
            if ($options->entriesFrom->id === $egId) {
                $childEGObj = new stdClass();
                $childEGObj->egId = $eventGroup->id;
                $childEGObj->name = $eventGroup->name;
                $childEGObj->eventDefId = $eventGroup->eventDef->id;
                $childEvents[] = $childEGObj;
            }
        }
        return $childEvents;
    }
    // On saving the master, check and update any child events
    // ModelArr will be an array of CE's/AgeGroups.

    public function masterEventBeingSaved($modelArr) {
        $childEvents = $this->_getChildEventsFromOptions($modelArr[0]->options);
        $existingChildObjs = $this->getEventGroupChildEvents($this->egId, FALSE);

        // clean remove/deleted child Objs
        $this->_removeUnusedChildEvents($existingChildObjs, $childEvents);

        foreach ($childEvents as $childEvent) {
            if ($childEvent->egId === 0) {
                // create a new child EG
                $this->_createChildEvent($childEvent, $modelArr);
            } else {
                // update the child EG
//                $this->_updateChildEvent($childEvent, $modelArr);
            }
        }
    }

    private function _getChildEventsFromOptions($parentOptions): array {
        return $parentOptions->multiEventOptions->childEvents;
    }

    private function _removeUnusedChildEvents($existingChildObjs, $childObjs) {
        $childEgIds = array();
        foreach ($childObjs as $childObj) {
            $childEgIds[] = $childObj->egId;
        }
        foreach ($existingChildObjs as $existingChildObj) {
            if (!in_array($existingChildObj->egId, $childEgIds)) {
// need to remove the existing child EG

            }
        }
    }

    private function _createChildEvent($childEvent, $modelArr) {
        $childModelArr = e4s_deepCloneToObject($modelArr);
        $childModelArr[0]->auditObj = $modelArr[0]->auditObj;
        $childModelArr = $this->_updateModelArrToChild($childEvent, $childModelArr);
        e4s_processCompEventArr($childModelArr, FALSE);
    }

    private function _updateChildEvent($childEvent, $modelArr) {

    }

    private function _updateModelArrToChild($childEvent, $modelArr) {
//        get event for both genders…
        $sql = 'select * 
                from ' . E4S_TABLE_EVENTGENDER . '
                where eventid = ' . $childEvent->eventDefId;
        $result = e4s_queryNoLog($sql);
        $events = array();
        while ($obj = $result->fetch_object(E4S_EVENTGENDER_OBJ)) {
            $events[$obj->gender] = $obj->id;
        }

        $newModelArr = array();
        foreach ($modelArr as $model) {
            $model->id = 0;
            $model->maxgroup = '';
            $model->maxathletes = -1;
            $model->egoptions->maxathletes = -1;
            $model->groupname = $childEvent->name ." (" . $model->groupname . ")";
            $model->process = E4S_CRUD_CREATE;
            $options = e4s_addDefaultEventGroupOptions($model->options);
            $options->multiEventOptions->childEvents = array();
            $entriesFrom = new stdClass();
            $entriesFrom->id = $this->egId;
            $model->egoptions->entriesFrom = $entriesFrom;

            $model->options = $options;

            $gender = $model->event->gender;
            $model->event = array();
            $model->event['id'] = $events[$gender];
            $model->event['gender'] = $gender;
            $eventObj = e4sEvents::getEventInfo($model->event['id']);
            $eventType = $eventObj->eventType;
            if ($eventType === E4S_UOM_DISTANCE or $eventType === E4S_UOM_HEIGHT) {
                $eventType = E4S_EVENT_FIELD;
            }
            $model->event['tf'] = $eventType;
            // set price
            $newModelArr[] = $model;
        }
        return $newModelArr;
    }
}