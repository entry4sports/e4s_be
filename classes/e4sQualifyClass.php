<?php

class e4sQualifyClass {
    public $egOptions;

    public function __construct($options = '') {
        $this->setOptions($options);
    }

    public function setOptions($options): void {
        $this->egOptions = e4s_addDefaultEventGroupOptions($options);
    }

    /**
     * @return mixed
     */
    public function egBeingSaved($savingId = 0): stdClass {
		$clear = false;
        if ($this->getSeedType() === E4S_SEED_OPEN) {
	        $clear = TRUE;
        }else if ( $this->getId() === $savingId and $savingId !== 0) {
	        $clear = TRUE;
        }
		if ( $clear ) {
            // clear any qualify
            $this->_clearQualify();
        }
        return $this->egOptions;
    }

    /**
     * @return string
     */
    public function getSeedType() {
        $seed = $this->getSeed();
        if (is_null($seed)) {
            return E4S_SEED_OPEN;
        }
        return $seed->type;
    }

    public function getSeed() {
        if ($this->_hasSeed()) {
            return $this->egOptions->seed;
        }
        return null;
    }

    private function _hasSeed(): bool {
        if (isset($this->egOptions->seed)) {
            return TRUE;
        }
        return FALSE;
    }

	private function _getAutoQualify(){
		if ( $this->_hasAutoQualify() ){
			return $this->_getQualify()->rules;
		}
		return null;
	}
	function getAutoCount(){
		$qualify = $this->_getAutoQualify();
		if ( !is_null($qualify) ){
			return $qualify->auto;
		}
		return 0;
	}
	function getNonAutoCount(){
		$qualify = $this->_getAutoQualify();
		if ( !is_null($qualify) ){
			return $qualify->nonAuto;
		}
		return 0;
	}
	private function _hasAutoQualify(): bool {
		$qualify = $this->_getQualify();
		if ( is_null($qualify) ){
			return false;
		}
		if ( isset($qualify->rules) ){
			return true;
		}
		return false;
	}

    private function _clearQualify() {
        $this->_setId(0);
        $this->setCompId(0);
        $this->setEventNo(0);
        $this->setName('');
        return $this->egOptions;
    }

    private function _setId($id) {
        $qualify = $this->_getQualify();
        if (!is_null($qualify)) {
            $qualify->id = $id;
        }
    }

    private function _getQualify() {
        if ($this->hasQualify()) {
            $seed = $this->getSeed();
            return $seed->qualifyToEg;
        }
        return null;
    }

    public function isBaseHeat($compId, $egId): bool {
        $globalKey = 'E4S_BASE_HEAT_' . '_' . $egId;
        if (!array_key_exists($globalKey, $GLOBALS)) {

            $compObj = e4s_getCompObj($compId);
            $egObjs = $compObj->getEGObjs();
            $GLOBALS[$globalKey] = TRUE;
            foreach ($egObjs as $egObj) {
                $this->setOptions($egObj->options);
                $qualifyTo = $this->_getQualify();
                if (!is_null($qualifyTo)) {
                    if ($qualifyTo->id === $egId) {
                        $GLOBALS[$globalKey] = FALSE;
                        break;
                    }
                }
            }
        }

        return $GLOBALS[$globalKey];
    }

    public function hasQualify() {
        if (!$this->_hasSeed()) {
            return FALSE;
        }
        $seed = $this->getSeed();
        if (isset($seed->qualifyToEg)) {
            return TRUE;
        }
        return FALSE;
    }

    public function setCompId($compId) {
        $qualify = $this->_getQualify();
        if (!is_null($qualify)) {
            $qualify->compId = $compId;
        }
    }

    public function setEventNo($eventNo) {
        $qualify = $this->_getQualify();
        if (!is_null($qualify)) {
            $qualify->eventNo = $eventNo;
        }
    }

    public function setName($name) {
        $qualify = $this->_getQualify();
        if (!is_null($qualify)) {
            $qualify->name = $name;
        }
    }

    public function getOptions($stripped = TRUE) {
        if ($stripped) {
            return e4s_removeDefaultEventGroupOptions($this->egOptions);
        } else {
            return $this->egOptions;
        }
    }

    public function getId() {
        $qualify = $this->_getQualify();
        if (is_null($qualify)) {
            return 0;
        }
        return (int)$qualify->id;
    }

    public function getCompId() {
        $qualify = $this->_getQualify();
        if (is_null($qualify)) {
            return 0;
        }
        return (int)$qualify->compId;
    }

    /**
     * @return string
     */
    public function getName(): string {
        $qualify = $this->_getQualify();
        if (is_null($qualify)) {
            return '';
        }
        return $qualify->name;
    }

    public function removeName(): void {
        $qualify = $this->_getQualify();
        if (!is_null($qualify)) {
            if (isset($qualify->name)) {
                unset($qualify->name);
            }
        }
    }

    public function getWaiting() {
        $seed = $this->getSeed();
        if (is_null($seed)) {
            return FALSE;
        }
        return $seed->waiting;
    }

    public function setWaiting($waiting) {
        $seed = $this->getSeed();
        if (!is_null($seed)) {
            $seed->waiting = $waiting;
        }
    }

	// Called on results write to check if Auto Q's can be done
	public function checkAutoQualifyResults($resultHeaderObj, &$results){
		$lastHeat = 0;
		$autoCount = $this->getAutoCount();
		$nonAutoCount = $this->getNonAutoCount();

		$sql = "select max(heatno) heatNo 
							from " . E4S_TABLE_SEEDING . " where eventgroupid = " . $resultHeaderObj->egId;
		$result = e4s_queryNoLog($sql);
		$seedRow = $result->fetch_object();
		if ( !is_null($seedRow->heatNo ) ) {
			$lastHeat = (int) $seedRow->heatNo;
		}
		if ( $autoCount === 0 ){
			// can we work out the heat count and therefore the autoCount ?
			// assume 8 lane track
			if ( $lastHeat === 2 ){
				$autoCount = 3;
			}
			if ( $lastHeat === 3 ){
				$autoCount = 2;
			}
		}

		if ( $autoCount === 0 ){
			// unable to get auto qualifiers
			return;
		}
		$nonQualifyResults = [];
		foreach($results as $sub=>$result ){
			if (stripos($result['score'], 'q') !== FALSE) {
				// if results already have Q/q's in, leave and get out ( Manually Seeded )
				return;
			}
			if ( array_key_exists('qualify', $result)){
				if ( $result['qualify'] !== '' ){
					return;
				}
			}
			if ( (int)$result['position'] <= $autoCount and (int)$result['position'] > 0) {
				$results[$sub]['qualify'] = 'Q';
			}else{
				// passed auto qualifiers, add to NonQualifiers
				$nonQualifier = new stdClass();
				$nonQualifier->score = (float)$result['score'];
				$nonQualifier->type = '';
				$nonQualifier->id = $sub;
				$nonQualifyResults[] = $nonQualifier;
			}
		}

// set the non Auto Qualifiers if last heat reached
		if ( $lastHeat === 0 or $nonAutoCount === 0){
			// unable to work out last heat or no non auto qualifiers
			return;
		}

		if ( (int)$resultHeaderObj->heatNo !== $lastHeat ){
			// not the last heat
			return;
		}
// $results is an array of results for the final heat, not yet written to disk
// need to get existing results, add with $results, work out little q's and if we add to $results or write to disk
		// clear out any pre existing non qualifiers
		$sql = "update " . E4S_TABLE_EVENTRESULTS . " set qualify = '' 
			where BINARY qualify = 'q'
			and resultheaderid in (select id from " . E4S_TABLE_EVENTRESULTSHEADER . " where egid = " . $resultHeaderObj->egId . ")";
		e4s_queryNoLog($sql);

		$sql = "select rd.id, rd.score, rd.scoreText, rd.ageGroup, rd.athleteId, rd.athlete, rd.club, rd.position, rd.bibNo, rh.heatNo
		        from " . E4S_TABLE_EVENTRESULTS . " rd,
		             " . E4S_TABLE_EVENTRESULTSHEADER . " rh
		        where rh.egid = " . $resultHeaderObj->egId . "
		        and   rh.id = rd.resultheaderid
		        and   rd.qualify != 'Q'
		        and  rh.heatno != " . $resultHeaderObj->heatNo . "
		        order by score
		        limit " . $nonAutoCount;
		$result = e4s_queryNoLog($sql);
		$checkResults = array();
		while ( $row = $result->fetch_object() ){
			$row->id = (int)$row->id;
			$row->score = (float)$row->score;
			$row->scoreText = $row->scoreText;
			$row->ageGroup = $row->ageGroup;
			$row->athlete = $row->athlete;
			$row->athleteId = $row->athleteId;
			$row->position = $row->position;
			$row->club = $row->club;
			$row->bibNo = $row->bibNo;
			$row->heatNo = $row->heatNo;
//			$row->resultHeaderId = $row->resultHeaderId;
//			$row->eventNo = $row->eventNo;
			$row->type = 'db';
			$checkResults[] = $row;
		}

		// merge arrays
		$checkResults = array_merge($checkResults, $nonQualifyResults);
		// sort on score
		usort($checkResults, function($a, $b) {
			return $a->score <=> $b->score;
		});

		$nowQualified = [];

		for ( $i = 0; $i < $nonAutoCount; $i++ ){
			$processResult = $checkResults[$i];
			if ( $processResult->type === 'db' ) {
				// write to disk
				$sql = "update " . E4S_TABLE_EVENTRESULTS . " set qualify = 'q' where id = " . $processResult->id;
				e4s_queryNoLog( $sql );
				// add to nowQualified
				$qualifyObj = new stdClass();
				$qualifyObj->athleteId = $processResult->athleteId;
				$qualifyObj->athlete = $processResult->athlete;
				$qualifyObj->club = $processResult->club;
				$qualifyObj->clubId = 0;
				$qualifyObj->qualifyType = 'q';
				$qualifyObj->qualifyPosition = $processResult->position;
				$qualifyObj->score = $processResult->score;
				$qualifyObj->scoreText = $processResult->scoreText;
				$qualifyObj->eventAgeGroup = $processResult->ageGroup;
				$qualifyObj->bibNo = $processResult->bibNo;
				$qualifyObj->heatNo = $processResult->heatNo;
//				$qualifyObj->resultHeaderId = $processResult->resultHeaderId;
//				$qualifyObj->eventNo = $processResult->eventNo;
//				if ( !array_key_exists($qualifyObj->eventNo, $nowQualified) ){
//					$nowQualified[$qualifyObj->eventNo] = [];
//				}
				$nowQualified[] = $qualifyObj;
			}else{
				// add to $results
				$results[$processResult->id]['qualify'] = 'q';
			}
		}
		// check if any nowQualified require entries creating
		if ( !empty($nowQualified) ){
			$qualifyToEgId = $this->getId();
			$nonQualifiedAthletes = [];
//			foreach($nowQualified as $eventNo=>$qualifiedAthletes){
				$this->writeQualifiedEntries($resultHeaderObj, $qualifyToEgId, $nowQualified, $nonQualifiedAthletes);
//			}
		}
	}
    public function writeQualifiedEntries($headerObj, $qualifyToEgId, $qualifiedAthletes, $nonQualifiedAthletes) {

		$egObj = eventGroup::getObj($qualifyToEgId,true);
	    $isTeamEvent = $egObj->egObj->options->isTeamEvent;

		$compId = $headerObj->compId;
        if (sizeof($nonQualifiedAthletes) > 0 or sizeof($qualifiedAthletes) > 0) {
            $removeAthleteIds = array();
            foreach ($nonQualifiedAthletes as $nonQualifiedAthlete) {
                $removeAthleteIds[] = $nonQualifiedAthlete->athleteId;
            }
            foreach ($qualifiedAthletes as $qualifiedAthlete) {
                $removeAthleteIds[] = $qualifiedAthlete->athleteId;
            }
			if (!$isTeamEvent) {
				$sql = 'delete from ' . E4S_TABLE_ENTRIES . '
                    where athleteid in ( ' . implode( ',', $removeAthleteIds ) . ')
                    and   compeventid in (
                    select ce.id
                    from ' . E4S_TABLE_COMPEVENTS . " ce
                    where maxgroup = '" . $qualifyToEgId . "')";
			}else{
				$sql = 'delete from ' . E4S_TABLE_EVENTTEAMENTRIES . '
                    where entityid in ( ' . implode( ',', $removeAthleteIds ) . ')
                    and   ceid in (
                    select ce.id
                    from ' . E4S_TABLE_COMPEVENTS . " ce
                    where maxgroup = '" . $qualifyToEgId . "')";
			}

            e4s_queryNoLog($sql);
        }
		// delete any seedings
        $sql = 'delete from ' . E4S_TABLE_SEEDING . '
		where eventgroupid = ' . $qualifyToEgId;
		e4s_queryNoLog($sql);

        if (sizeof($qualifiedAthletes) === 0) {
            return;
        }

        // ok, create entries for the qualified athletes/teams
// get source info
	    if ( $isTeamEvent ){
		    $sql = 'select ce.id ceId,
                       ce.agegroupid ageGroupId,
                       e.id athleteId,
                       e.bibNo teamBibNo 
                from ' . E4S_TABLE_EVENTTEAMENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTGROUPS . ' eg
                where e.ceid = ce.id
                and   ce.maxgroup = eg.id
                and   e.paid in (' . E4S_ENTRY_PAID . ',' . E4S_ENTRY_QUALIFY . ')
                and   ce.compid = ' . $compId . '
                and   eg.eventno = ' . $headerObj->eventNo;
	    } else {
		    $sql = 'select ce.id ceId,
                       ce.agegroupid ageGroupId,
                       e.athleteid athleteId,
                       e.teamBibNo 
                from ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTGROUPS . ' eg
                where e.compeventid = ce.id
                and   ce.maxgroup = eg.id
                and   e.paid in (' . E4S_ENTRY_PAID . ',' . E4S_ENTRY_QUALIFY . ')
                and   ce.compid = ' . $compId . '
                and   eg.eventno = ' . $headerObj->eventNo;
	    }
        $result = e4s_queryNoLog($sql);
	    $sourceCEbyAGIds = array();
        if ($result->num_rows > 0) {
	        while ( $srcObj = $result->fetch_object() ) {
		        $sourceCEbyAGIds [ $srcObj->athleteId ] = $srcObj;
	        }
        }

        $targetCEbyAGIds = array();
        $sql = 'select id ceId,
                       agegroupid ageGroupId
                from ' . E4S_TABLE_COMPEVENTS . '
                where maxgroup = ' . $qualifyToEgId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            Entry4UIError(7636, 'Failed to get records of the qualifying event');
        }
		$firstTarget = null;
        while ($ceObj = $result->fetch_object()) {
            $ceObj->ceId = (int)$ceObj->ceId;
            $ceObj->ageGroupId = (int)$ceObj->ageGroupId;
            $targetCEbyAGIds[$ceObj->ageGroupId] = $ceObj;
			if ( is_null($firstTarget) ){
				$firstTarget = $ceObj;
			}
        }
        // Check we have a final price record
        $priceObj = new priceClass($compId, FALSE);
        $priceObj->getEventFreePrice();
		if ( $isTeamEvent ){
			$insertSql = 'insert into ' . E4S_TABLE_EVENTTEAMENTRIES . '(ceid, name, bibNo, productid, orderid, entitylevel, entityid, paid, price, userid, present, options)
                      values ';
		}else {
			$insertSql = 'insert into ' . E4S_TABLE_ENTRIES . '(compeventid, athleteid, athlete, teamBibNo, pb, variationid, clubid, orderid, paid, price,eventagegroup, agegroupid,userid, waitingpos,present, options)
                      values ';
		}
        $insertSep = '';

        $ageGroupObj = new e4sAgeClass(E4S_AGE_DEFAULT_COMP, $compId);
//        $compObj = e4s_getCompObj($compId);
		$addBibNos = false;
        foreach ($qualifiedAthletes as $qualifiedAthlete) {

	        $sourceCeRecord = null;
            // handle result only entries
            if (!array_key_exists($qualifiedAthlete->athleteId, $sourceCEbyAGIds)) {
	            $sourceCeRecord = new stdClass();
	            $sourceCeRecord->athleteId = $qualifiedAthlete->athleteId;
	            $sourceCeRecord->ceId = $firstTarget->ceId;
	            $sourceCeRecord->ageGroupId = $firstTarget->ageGroupId;
	            $sourceCeRecord->teamBibNo = $qualifiedAthlete->bibNo;
	            $addBibNos = true;
            }else {
	            $sourceCeRecord = $sourceCEbyAGIds[ $qualifiedAthlete->athleteId ];
            }
            $targetAgeId = (int)$sourceCeRecord->ageGroupId;
            $eventAgeGroupObj = $ageGroupObj->getAgeGroup($targetAgeId);

            $targetRecord = $targetCEbyAGIds[$targetAgeId];
            $score = resultsClass::getResultInSeconds($qualifiedAthlete->score);
            $options = new stdClass();
            $qualifyObj = new stdClass();
            $qualifyObj->heatNo = $qualifiedAthlete->heatNo;
            $qualifyObj->type = $qualifiedAthlete->qualifyType;
            $qualifyObj->position = $qualifiedAthlete->qualifyPosition;
            $qualifyObj->score = E4S_ENSURE_STRING . resultsClass::getResultFromSeconds($qualifiedAthlete->score);
            $options->qualify = $qualifyObj;

            $teamBibNo = trim($sourceCeRecord->teamBibNo);
			if ( $isTeamEvent ) {
				$insertSql .= $insertSep . '(
		            ' . $targetRecord->ceId . ',
		            "' . addslashes( $qualifiedAthlete->club ) . '",
		            "' . $teamBibNo . '",
		            0,
		            0,
		            1,
                    ' . $qualifiedAthlete->clubId . ',
		            ' . E4S_ENTRY_QUALIFY . ',
		            1, 
		            1,
		            1,
		            "' . addslashes(e4s_getOptionsAsString( $options )) . '"
		        )';
			}else {
				$insertSql .= $insertSep . '(
		            ' . $targetRecord->ceId . ',
		            ' . $qualifiedAthlete->athleteId . ",
		            '" . addslashes( $qualifiedAthlete->athlete ) . "',
		            '" . $teamBibNo . "',
		            " . $score . ',
		            0,
		            ' . $qualifiedAthlete->clubId . ',
		            0,
		            ' . E4S_ENTRY_QUALIFY . ",
		            0, 
		            '" . $eventAgeGroupObj->name . "',
		            " . $targetAgeId . ',
		            ' . E4S_USER_ANON_ID . ',
		            0,
		            1,
		            "' . addslashes(e4s_getOptionsAsString( $options )) . '"
		        )';
			}
            $insertSep = ',';
        }

        if ($insertSep !== '') {
            // at least 1 athlete found
            e4s_queryNoLog($insertSql);
        }
		if ( $addBibNos ){
			new bibClass($compId, E4S_BIB_ADDITIONAL);
		}
    }
}