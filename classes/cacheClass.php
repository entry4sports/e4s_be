<?php
//define cahce keys used
define( 'E4S_CACHE_COUNTS', 'counts' );
define( 'E4S_CACHE_ENTRIES', 'entries' );

class cacheClass {
	public $compid;
	var $compObj;
	public $checkExpired;

	public function __construct( $compid ) {
		$this->compid       = $compid;
		$this->compObj      = e4s_getCompObj( $compid );
		$this->checkExpired = $this->compObj->isWaitingListEnabled();
	}

	public static function addCompIdToCache( $compId ) {
		if ( ! isE4SUser() ) {
			return;
		}
		if ( ! array_key_exists( E4S_PROCESSING_HOMEPAGE_COMPS, $GLOBALS ) ) {
			$GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ] = [];
		}
		if ( ! array_key_exists( E4S_PROCESSING_COMPS_IDS, $GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ] ) ) {
			$GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_IDS ] = [];
		}
		$GLOBALS[ E4S_PROCESSING_HOMEPAGE_COMPS ][ E4S_PROCESSING_COMPS_IDS ][ $compId ] = $compId;
	}

	public function clearCache() {
		$sql = 'delete from ' . E4S_TABLE_CACHE . '
                 where compid = ' . $this->compid;
		e4s_queryNoLog( $sql );
	}

	public function __destruct() {
		// TODO: Implement __destruct() method.
	}

	public function readCache( $key, $type, $obj, $force = FALSE ) {
		$cache = '';
		if ( ! $force ) {
			$cacheRow = $this->getCacheRow( $key );
			if ( $cacheRow !== '' ) {
				$cache = $cacheRow['cache'];

				if ( ! is_null( $obj ) ) {
					// check if force is required
					if ( method_exists( $obj, 'forceCache' ) ) {
						if ( $obj->forceCache( $cacheRow['modified'] ) ) {
							$cache = '';
						}
					}
				}
			}
		}

		if ( $cache === '' ) {
//            e4s_log("NOT using Cache on " . $this->compid, 1);
//  there is no cache or forced for the key so get latest and write cache
			switch ( $key ) {
				case E4S_CACHE_COUNTS:
					$cache = $this->updateEntryCounts();
					break;
				case E4S_CACHE_ENTRIES:
					$cache = e4s_getEntriesData( $obj );
					break;
			}
			$this->writeCache( $key, $cache );
			$this->_checkExpired();
		}

		if ( $type === E4S_OPTIONS_STRING ) {
			return $cache;
		}

		return e4s_getDataAsType( $cache, $type );
	}

	public function getCacheRow( $key ) {
		$sql = 'select id, compid,cachekey, uncompress(cache) cache, modified
                from ' . E4S_TABLE_CACHE . '
                where compid = ' . $this->compid . "
                and cachekey = '{$key}'";
		$res = e4s_queryNoLog( $sql );
		if ( $res->num_rows === 0 ) {
			return '';
		}
		$row = $res->fetch_assoc();

		return $row;
	}

	public function updateEntryCounts() {
		// have a check to not update if comp date in past ?
		$counters = array();
		// event counts are not strictly correct, they could indiv CE records as opposed to actual events. Potential use eventGroup table ?
		// get event Counts
		$eventSql        = 'SELECT count(id) dayCount, date(startdate) date
                    FROM ' . E4S_TABLE_COMPEVENTS . '
                    where compid = ' . $this->compid . '
                    group by date
                    order by date';
		$dateResult      = e4s_queryNoLog( $eventSql );
		$totalEventCount = 0;
		while ( $eventRow = mysqli_fetch_array( $dateResult, TRUE ) ) {
			$totalEventCount += (int) $eventRow['dayCount'];
		}
		// is it a Competition with team events
		$teamsql         = 'select count(id) teamEventCount
                    FROM ' . E4S_TABLE_COMPEVENTS . '
                    where compid = ' . $this->compid . "
                    and options like '%" . e4s_optionIsTeamEvent() . "%'";
		$teamResult      = e4s_queryNoLog( $teamsql );
		$teamRow         = $teamResult->fetch_assoc();
		$teamEventCount  = (int) $teamRow['teamEventCount'];
		$indivEventCount = $totalEventCount - $teamEventCount;

		$counters['eventCnt'] = $totalEventCount;
		$counters['indivCnt'] = $indivEventCount;
		$counters['teamCnt']  = $teamEventCount;

		$useArchive = $this->compObj->useArchiveData();

		$indivEntryRows = $this->getEntriesForTable( E4S_TABLE_ENTRIES );
		if ( $useArchive ){
			$indivEntryRowsArchive = $this->getEntriesForTable( E4S_TABLE_ENTRIES_ARCHIVE, TRUE );
			$indivEntryRows        = array_merge( $indivEntryRows, $indivEntryRowsArchive );
		}
		$counters['totalCnt'] = sizeof( $indivEntryRows );
		$uniqueAthletes       = array();
		$waitingEntries       = 0;
		foreach ( $indivEntryRows as $indivEntryRow ) {
			$uniqueAthletes[ $indivEntryRow['athleteid'] ] = TRUE;
			if ( (int) $indivEntryRow['waitingPos'] > 0 ) {
//                logTxt("On waiting List : " . $indivEntryRow['entryId']);
				$waitingEntries ++;
			}
		}
		$counters['uniqueCnt']      = sizeof( $uniqueAthletes );
		$counters['waitingEntries'] = $waitingEntries;
// Check for Relay/Team Events
		$counters['relayCnt']              = 0; // Number of teams entered
		$counters['relayAthleteCnt']       = 0; // Number of relay athletes
		$counters['relayUniqueAthleteCnt'] = 0;
		$uniqueRelayAthletes = array();
		$uniqueRelayTeams = array();// Number of athletes ONLY in a relay team and not entered individually
		if ( $teamEventCount > 0 ) {
			$relayRows = $this->getTeamEntriesForTable( E4S_TABLE_EVENTTEAMENTRIES, E4S_TABLE_EVENTTEAMATHLETE , false);
			if ( $useArchive ){
				$relayRowsArchive = $this->getTeamEntriesForTable( E4S_TABLE_EVENTTEAMENTRIES_ARCHIVE, E4S_TABLE_EVENTTEAMATHLETE_ARCHIVE , true);
				$relayRows        = array_merge( $relayRows, $relayRowsArchive );
			}

			foreach ( $relayRows as $relayRow ) {
				$athleteId = $relayRow['athleteid'];
				$teamId = $relayRow['teameventid'];

				$uniqueRelayTeams[ $teamId ] = TRUE;
				if ( !is_null($athleteId) and $athleteId > 0) {
					if ( ! array_key_exists( $athleteId, $uniqueAthletes ) ) {
						$uniqueRelayAthletes[ $athleteId ] = TRUE;
					}
					$counters['relayAthleteCnt'] ++;
					$uniqueAthletes[ $athleteId ] = TRUE;
				}
			}
		}
		$counters['relayCnt'] = sizeof( $uniqueRelayTeams );
		$counters['relayUniqueAthleteCnt'] = sizeof( $uniqueRelayAthletes );
		$counters['totalUniqueAthleteCnt'] = sizeof( $uniqueAthletes );

		return $counters;
	}
	function getTeamEntriesForTable( $teamTableName, $teamAthleteTableName, $archive = FALSE ) {
		if ( $archive ) {
			$archiveSchema = e4sArchive::archiveName() . ".";
			$teamTableName = $archiveSchema . $teamTableName;
			$teamAthleteTableName = $archiveSchema . $teamAthleteTableName;
		}
		$sql = "select evt.id teameventid, evta.athleteid athleteid
				from " . $teamTableName . " evt left outer join " . $teamAthleteTableName . " evta on evt.id = evta.teamentryid,
     				 " . E4S_TABLE_COMPEVENTS . " ce
				where evt.ceid = ce.id
      			and ce.compid = " . $this->compid . "
          		and evt.paid = " . E4S_ENTRY_PAID;
		$result = e4s_queryNoLog( $sql );
		$teamEntryRows = $result->fetch_all( MYSQLI_ASSOC );
		return $teamEntryRows;
	}
	function getEntriesForTable( $tableName, $archive = FALSE ) {
		if ( $archive ) {
			$tableName = e4sArchive::archiveName() . "." . $tableName;
		}
		$sql = 'SELECT e.athleteid athleteid,
                       e.waitingPos waitingPos,
                        e.id entryId
        FROM ' . $tableName . ' e, 
             ' . E4S_TABLE_COMPEVENTS . ' ce 
        where ce.compid = ' . $this->compid . '
        and ce.id = e.compeventid 
        and e.paid = ' . E4S_ENTRY_PAID . '
        and e.athleteid > 0
        and ce.options not like \'%parentCeid%\'';

		$result    = e4s_queryNoLog( $sql );
		$entryRows = $result->fetch_all( MYSQLI_ASSOC );

		return $entryRows;
	}

	public function writeCache( $key, $cachedata ) {
//        $cachedata['force'] = rand(0, 200);
		$cachedata = e4s_getDataAsType( $cachedata, E4S_OPTIONS_STRING );
		$exists    = $this->getCache( $key );
		if ( $exists !== '' ) {
			// Add modified to FORCE update, even if cache has not changed
			$sql = 'update ' . E4S_TABLE_CACHE . "
                set cache = compress('" . addslashes( $cachedata ) . "'),
                    modified = CURRENT_TIMESTAMP
                where compid = {$this->compid}
                and cachekey = '{$key}'";
		} else {
			$sql = 'insert ' . E4S_TABLE_CACHE . " ( compid, cachekey, cache )
            values (
                {$this->compid},
                '{$key}',
                compress('" . addslashes( $cachedata ) . "')
            )";
		}
		e4s_queryNoLog( $sql );

		return $cachedata;
	}

	public function getCache( $key ) {
		$row = $this->getCacheRow( $key );
		if ( $row === '' ) {
			return '';
		}

		return $row['cache'];
	}

	private function _checkExpired() {
		// Only check once per process
		$checked = 'E4S_EXPIRED_CHECKED';
		if ( ! array_key_exists( $checked, $GLOBALS ) ) {
			$entryObj = new entryClass();
			$entryObj->checkExpiredEntries();
		}
		$GLOBALS[ $checked ] = TRUE;
	}

}