<?php


class autoEntriesClass {
    public const E4S_TYPE_SOURCE = 'source';
    public const E4S_TYPE_TARGET = 'target';
    public const E4S_EVENT_TYPE_INDIVIDUAL = 'I';
    public const E4S_EVENT_TYPE_TEAM = 'T';
    public $egIds;
    public $sourceEntries;
    public $isTeamEvent;

    public function __construct($isTeamEvent = FALSE) {
        $this->egIds = array();
        $this->isTeamEvent = $isTeamEvent;
    }

    public function addEg($egId) {
        $this->egIds[] = $egId;
    }

    public function setTargetInfoInOptions($entryObj, $targetEntryObj, $targetEgObj) {
        $this->_setTargetPaid($entryObj->entryOptions, $targetEntryObj->paid);
        $this->_setTargetOrderId($entryObj->entryOptions, $targetEntryObj->orderId);
        $this->setTargetName($entryObj->entryOptions, $targetEgObj->name);
        if (isset($targetEntryObj->compEventId)) {
            $this->_setTargetCeId($entryObj->entryOptions, $targetEntryObj->compEventId);
        }
        if (isset($targetEntryObj->ceId)) {
            $this->_setTargetCeId($entryObj->entryOptions, $targetEntryObj->ceId);
        }
        return $entryObj;
    }

    private function _setTargetPaid($eOptions, $paid) {
        $autoObj = $this->getEntryAutoObj($eOptions);
        $targetEntry = $this->getTargetEntryObj($autoObj);
        $targetEntry->paid = $paid;
        return $eOptions;
    }

    public function getEntryAutoObj($eOptions) {
        $defObj = $this->getDefaultEntryAutoOptions();

        if (isset($eOptions->autoEntries)) {
            return (object)array_merge((array)$defObj, (array)$eOptions->autoEntries);
        }
        return $defObj;
    }

    public function getDefaultEntryAutoOptions() {
        $obj = new stdClass();
        $obj->targetEntry = new stdClass();
        $obj->targetEntry->id = 0;
        $obj->targetEntry->paid = E4S_ENTRY_NOT_PAID;
        $obj->targetEntry->orderId = 0;
        $obj->targetEventGroup = new stdClass();
        $obj->targetEventGroup->id = 0;
        $obj->targetEventGroup->name = '';
        return $obj;
    }

    public function getTargetEntryObj($autoObj) {
        return $autoObj->targetEntry;
    }

    private function _setTargetOrderId($eOptions, $orderId) {
        $autoObj = $this->getEntryAutoObj($eOptions);
        $targetEntry = $this->getTargetEntryObj($autoObj);
        $targetEntry->orderId = $orderId;
        return $eOptions;
    }

    public function setTargetName($eOptions, $name) {
        $autoObj = $this->getEntryAutoObj($eOptions);
        $targetEG = $this->getTargetEventGroupObj($autoObj);
        $targetEG->name = $name;
        return $eOptions;
    }

    public function getTargetEventGroupObj($autoObj) {
        return $autoObj->targetEventGroup;
    }

    private function _setTargetCeId($eOptions, $ceId) {
        $autoObj = $this->getEntryAutoObj($eOptions);
        $targetEG = $this->getTargetEventGroupObj($autoObj);
        $this->_setCeIDOnObj($targetEG, $ceId);
        return $eOptions;
    }

    private function _setCeIDOnObj($obj, $ceId) {
        $obj->ceId = $ceId;
        return $obj;
    }

    public function getSourceEntryObj($autoObj) {
        return $autoObj->sourceEntry;
    }

    public function setTargetUserInfo($eOptions, $userId, $userName, $userEmail) {
        $autoObj = $this->getEntryAutoObj($eOptions);
        $userObj = $this->_getTargetUserObj($autoObj);
        $userObj->id = $userId;
        $userObj->name = $userName;
        $userObj->email = $userEmail;
        return $eOptions;
    }

    private function _getTargetUserObj($autoObj) {
        $entryObj = $this->getTargetEntryObj($autoObj);
        return $entryObj->user;
    }

    public function getTargetEGName($eOptions) {
        $autoObj = $this->getEntryAutoObj($eOptions);
        $targetEG = $this->getTargetEventGroupObj($autoObj);
        return $targetEG->name;
    }

    public function setExtraInfo() {
//        foreach($sourceEntryObjs as $sourceEntryObj){
//            $options = $sourceEntryObj->options;
//            $egId = $this->getTargetEGId($options);
//            if ( array_key_exists($egId,$eventNames) ) {
//                $this->setTargetName($sourceEntryObj->options, $eventNames[$egId]);
//            }
//        }
    }

    public function writeAutoEntries($autoEntries) {
        $entryObjs = $this->_getSourceEntries($autoEntries);
        $targetEGObjs = $this->_setEGNamesAndGetTargetEventGroupInfo($entryObjs);
        $msgArray = array();
        $retEntryObjArray = array();
        $compId = 0;
        $egId = 0;
        $isTeamEvent = FALSE;
        $autoEntriesKeyed = array();
        foreach ($autoEntries as $autoEntry) {
            $autoEntriesKeyed[$autoEntry['sourceEntryId']] = $autoEntry;
        }
        $userArray = array();
        foreach ($entryObjs as $entryObj) {
            $compId = $entryObj->compId;
            $egId = $entryObj->egId;
            $isTeamEvent = $entryObj->egOptions->isTeamEvent;
            $targetEntryId = $this->getTargetEntryId($entryObj->options);
            $targetEventGroupId = $this->getTargetEGId($entryObj->options);
            $targetCompId = $this->getTargetCompId($entryObj->options);
            if ($targetEntryId + $targetEventGroupId === 0) {
                // Nothing to do
                continue;
            }
            $entryStatus = '';
            if ($targetEntryId === 0) {
                // create entry in target competition
                $targetEventInfo = $this->_getTargetEventInfoFromAges($targetEGObjs, $entryObj);
                $targetCeId = $this->_getCeIdFromObj($targetEventInfo);
                $entityName = $this->_getEntityName($entryObj);
                if ($targetCeId === 0) {
                    $msgArray[] = $this->_addMessage('Unable to find age group for ' . $entityName, $entryObj->entryId);
                } else {
                    $this->_setTargetCeId($entryObj->options, $targetCeId);
                    $this->_setTargetCompId($entryObj->options, $targetCompId);
                    if (!$this->createTargetEntry($entryObj)) {
                        $msgArray[] = $this->_addMessage('Issue creating entry for ' . $entityName, $entryObj->entryId);
                    } else {
                        $entryStatus = 'Added';
                    }
                }
            } else {
                // check if it has been removed
                $autoEntry = $autoEntriesKeyed[$entryObj->entryId];
                if ($autoEntry['targetEventGroupId'] === 0) {
                    if (!$this->_deleteTargetEntry($entryObj)) {
                        $msgArray[] = $this->_addMessage('Issue deleting entry for ' . $entityName, $entryObj->entryId);
                    } else {
                        $entryStatus = 'Removed';
                    }
                }
            }
            // add to email Array by user
            if (!array_key_exists($entryObj->userId, $userArray)) {
                $userArray[$entryObj->userId] = new stdClass();
                $userArray[$entryObj->userId]->competitions = array();
            }
            if ($entryStatus !== '') {
                // entry has changed ( added or removed )
                $competitions = $userArray[$entryObj->userId]->competitions;
                $compObj = e4s_getCompObj($targetCompId);
                $compName = $compObj->getIdAndName();
                $emailObj = new stdClass();
                if (isset($entryObj->teamName)) {
                    $emailObj->athlete = '';
                    $emailObj->teamName = $entryObj->teamName;
                } else {
                    $emailObj->athlete = $entryObj->athleteName;
                    $emailObj->teamName = '';
                }
                $egObjs = $compObj->getEGObjs();
                $emailObj->event = $egObjs[$targetEventGroupId]->name;
                $emailObj->status = $entryStatus;

                if (!array_key_exists($compName, $competitions)) {
                    $competitions[$compName] = array();
                }
                $competitions[$compName][] = $emailObj;
                $userArray[$entryObj->userId]->competitions = $competitions;
            }
            // put to a standard array
            $retEntryObjArray[] = $entryObj;
        }
        if (!empty($userArray)) {
            $this->_emailTargetEntries($userArray);
        }
        $retObj = new stdClass();
        $retObj->entryObjs = $retEntryObjArray;
//        $msgArray[] = $this->_addMessage("Test Message",1234);
        $retObj->messages = $msgArray;
        $retObj->compId = $compId;
        $retObj->egId = $egId;
        $retObj->isTeamEvent = $isTeamEvent;

        return $retObj;
    }

    private function _getSourceEntries($autoEntries): array {
        $sourceIds = $this->_getSourceIds($autoEntries);
        if (sizeof($sourceIds) < 1) {
            Entry4UIError(9602, 'No source entries passed');
        }
        $sourceEntryObjs = $this->_getEntryObjs($sourceIds, self::E4S_TYPE_SOURCE);
        $msgArray = array();
        foreach ($autoEntries as $autoEntry) {
            $targetEGId = $autoEntry['targetEventGroupId'];
            $sourceEntryObj = $sourceEntryObjs[$autoEntry['sourceEntryId']];
            $sourceTargetEventGroupId = $this->getTargetEGId($sourceEntryObj->options);
            if ($targetEGId !== $sourceTargetEventGroupId) {
                $entityName = $this->_getEntityName($sourceEntryObj);
                if ($targetEGId === 0) {
                    // requested it is removed
                    if ($this->getTargetEntryPaid($sourceEntryObj->options) === E4S_ENTRY_PAID) {
                        // trying to remove a paid entry

                        $msgArray[] = $sourceEntryObj->entryId . ' for ' . $entityName . ' has been paid for and can not be removed.';
                    }
                } elseif ($sourceTargetEventGroupId === 0) {
                    // requested it is added
                    $this->_setTargetEGId($sourceEntryObj->options, $targetEGId);
                } else {
                    // not sure this should ever occur
                    $msgArray[] = $entityName . ' attempting to switch. Currently Not allowed';
                }
            }
        }
        if (!empty($msgArray)) {
            Entry4UIError(7452, implode("\n", $msgArray));
        }
        return $sourceEntryObjs;
    }

    private function _getSourceIds($autoEntries) {
        $sourceIds = array();
        foreach ($autoEntries as $autoEntry) {
            $sourceIds[] = $this->getSourceEntryId($autoEntry);
        }
        return $sourceIds;
    }

    public function getSourceEntryId($autoEntry) {
        if (!array_key_exists('sourceEntryId', $autoEntry)) {
            Entry4UIError(9401, 'Unable to get Source Entry Id');
        }
        return $autoEntry['sourceEntryId'];
    }

    private function _getEntryObjs($ids, $type) {
        $idString = implode(',', $ids);
        if ($this->isTeamEvent) {
            $sql = "select 
                       e.id entryId,
                       e.name teamName,
                       e.userid userId,
                       e.options options,
                       e.paid paid,
                       e.orderid orderId,
                       e.entityLevel,
                       e.entityId,
                       c.Clubname clubName,
                       a.name areaName,
                       ce.compId,
                       ce.agegroupid ageGroupId,
                       ce.options ceOptions,
                       eg.id egId,
                       eg.name eventName,
                       eg.options egOptions,
                       ath.id athleteId,
                       concat(ath.firstname,' ',ath.surname) athlete,
                       ea.pos athletePos
                from " . E4S_TABLE_EVENTTEAMENTRIES . ' e 
                left join ' . E4S_TABLE_CLUBS . ' c on (c.id = e.entityid) 
                left join ' . E4S_TABLE_AREA . ' a on (a.id = e.entityid) 
                join ' . E4S_TABLE_EVENTTEAMATHLETE . ' ea on ( e.id = ea.teamentryid)
                join ' . E4S_TABLE_ATHLETE . ' ath on (ath.id = ea.athleteid),
                     ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTGROUPS . ' eg
                where e.id in (' . $idString . ')
                and   e.ceid = ce.id
                and   eg.id = ce.maxgroup';

            $result = e4s_queryNoLog($sql);
            if ($result->num_rows === 0) {
                e4s_addDebugForce($sql);
                Entry4UIError(9603, 'No ' . $type . ' entries found : (' . $idString . ')');
            }
            $entryObjs = array();
            while ($obj = $result->fetch_object()) {
                $obj->entryId = (int)$obj->entryId;
                if (!array_key_exists($obj->entryId, $entryObjs)) {
                    $this->_setEventType($obj, self::E4S_EVENT_TYPE_TEAM);
                    $obj->egOptions = e4s_addDefaultEventGroupOptions($obj->egOptions);
                    $obj->options = e4s_addDefaultEntryOptions($obj->options);
                    $obj->options->autoEntries = $this->getEntryAutoObj(e4s_getOptionsAsObj($obj->options));
                    $obj->compId = (int)$obj->compId;
                    $obj->egId = (int)$obj->egId;
                    $obj->userId = (int)$obj->userId;
                    $obj->paid = (int)$obj->paid;
                    $obj->orderId = (int)$obj->orderId;
                    $obj->entityLevel = (int)$obj->entityLevel;
                    $obj->entityId = (int)$obj->entityId;
                    if ($obj->entityLevel === 1) {
                        $obj->entityName = $obj->clubName;
                    } else {
                        $obj->entityName = $obj->areaName;
                    }
                    unset($obj->clubName);
                    unset($obj->areaName);
                    $obj->ageGroupId = (int)$obj->ageGroupId;
                    $obj->athletes = array();
                } else {
                    $obj = $entryObjs[$obj->entryId];
                }
                if (!is_null($obj->athleteId)) {
                    $athlete = array();
                    $athlete['id'] = (int)$obj->athleteId;
                    $athlete['pos'] = (int)$obj->athletePos;
                    $athlete['athlete'] = $obj->athlete;
                    $obj->athletes[$athlete['pos'] - 1] = $athlete;
                }
                $entryObjs[$obj->entryId] = $obj;
            }
        } else {
            $sql = "select athleteid athleteId,
                       concat (a.firstname,' ',a.surname) athleteName,
                       a.dob dob,
                       a.gender gender,
                       userid userId,
                       e.clubid clubId,
                       e.options options,
                       e.agegroupid ageGroupId,
                       paid paid,
                       orderid orderId,
                       e.id entryId,
                       ce.compId,
                       ce.maxGroup egId,
                       eg.options egOptions
                from " . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_ATHLETE . ' a,
                     ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTGROUPS . ' eg
                where e.id in (' . $idString . ')
                and e.athleteid = a.id
                and e.compeventid = ce.id
                and eg.id = ce.maxgroup';
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows === 0) {
                Entry4UIError(9604, 'No ' . $type . ' entries found : (' . $idString . ')');
            }
            $entryObjs = array();
            while ($obj = $result->fetch_object()) {
                $this->_setEventType($obj, self::E4S_EVENT_TYPE_INDIVIDUAL);
                $obj->egOptions = e4s_addDefaultEventGroupOptions($obj->egOptions);
                $obj->options = e4s_addDefaultEntryOptions($obj->options);
                $obj->options->autoEntries = $this->getEntryAutoObj(e4s_getOptionsAsObj($obj->options));
                $obj->compId = (int)$obj->compId;
                $obj->egId = (int)$obj->egId;
                $obj->athleteId = (int)$obj->athleteId;
                $obj->userId = (int)$obj->userId;
                $obj->paid = (int)$obj->paid;
                $obj->orderId = (int)$obj->orderId;
                $obj->entryId = (int)$obj->entryId;
                $obj->clubId = (int)$obj->clubId;
                $obj->ageGroupId = (int)$obj->ageGroupId;
                $entryObjs[$obj->entryId] = $obj;
            }
        }

        return $entryObjs;
    }

    private function _setEventType($obj, $type) {
        $obj->type = $type;
    }

    public function getTargetEGId($eOptions) {
        $autoObj = $this->getEntryAutoObj($eOptions);
        $targetEG = $this->getTargetEventGroupObj($autoObj);
        return $targetEG->id;
    }

    private function _getEntityName($entryObj) {
        if ($this->_getEventType($entryObj) === self::E4S_EVENT_TYPE_TEAM) {
            $entityName = $entryObj->teamName;
        } else {
            $entityName = $entryObj->athleteName;
        }
        return $entityName;
    }

    private function _getEventType($obj) {
        return $obj->type;
    }

    public function getTargetEntryPaid($eOptions) {
        $autoObj = $this->getEntryAutoObj($eOptions);
        $targetEntry = $this->getTargetEntryObj($autoObj);
        return $targetEntry->paid;
    }

    private function _setTargetEGId($eOptions, $egId) {
        $autoObj = $this->getEntryAutoObj($eOptions);
        $targetEG = $this->getTargetEventGroupObj($autoObj);
        $targetEG->id = $egId;
        $targetEG->name = '';
        return $eOptions;
    }

    private function _setEGNamesAndGetTargetEventGroupInfo($sourceEntryObjs) {
        $targetIds = $this->_getTargetEventGroupIds($sourceEntryObjs);
        $targetEGs = array();
        if (!empty($targetIds)) {
            $sql = 'select  ce.id ceId,
                            ce.agegroupid ageGroupId,
                            ce.compId,
                            eg.id egId,
                            eg.name eventName
                    from ' . E4S_TABLE_COMPEVENTS . ' ce,
                         ' . E4S_TABLE_EVENTGROUPS . ' eg
                    where maxgroup in (' . implode(',', $targetIds) . ')
                    and eg.id = ce.maxgroup
                    order by maxgroup, agegroupid';
            $result = e4s_queryNoLog($sql);
            while ($obj = $result->fetch_object()) {
                $obj->egId = (int)$obj->egId;
                $obj->ageGroupId = (int)$obj->ageGroupId;
                $obj->ceId = (int)$obj->ceId;
                $obj->compId = (int)$obj->compId;
                if (!array_key_exists($obj->egId, $targetEGs)) {
                    $targetEGs[$obj->egId] = array();
                }
                $targetObj = new stdClass();
                $targetObj = $this->_setCompIDOnObj($targetObj, $obj->compId);
                $targetObj = $this->_setCeIDOnObj($targetObj, $obj->ceId);
                $targetEGs[$obj->egId][$obj->ageGroupId] = $targetObj;
            }
        }
        return $targetEGs;
    }

    private function _getTargetEventGroupIds($sourceEntryObjs) {
        $targetIds = array();
        foreach ($sourceEntryObjs as $sourceEntry) {
            $targetId = $this->getTargetEGId($sourceEntry->options);
            if ($targetId !== 0) {
                $targetIds[] = $targetId;
            }
        }
        return $targetIds;
    }

    private function _setCompIDOnObj($obj, $compId) {
        $obj->compId = $compId;
        return $obj;
    }

    public function getTargetEntryId($eOptions) {
        $autoObj = $this->getEntryAutoObj($eOptions);
        $targetEntry = $this->getTargetEntryObj($autoObj);
        return $targetEntry->id;
    }

    public function getTargetCompId($eOptions) {
        $autoObj = $this->getEntryAutoObj($eOptions);
        $targetEG = $this->getTargetEventGroupObj($autoObj);
        if (!isset($targetEG->compId)) {
            $sql = 'select compId
                    from ' . E4S_TABLE_EVENTGROUPS . '
                    where id = ' . $targetEG->id;
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows !== 1) {
                return 0;
            }
            $obj = $result->fetch_object();
            $targetEG->compId = (int)$obj->compId;
        }
        return $targetEG->compId;
    }

    private function _getTargetEventInfoFromAges($targetEGObjs, $entryObj) {
        $targetEGId = $this->getTargetEGId($entryObj->options);
        $targetEGAgesObj = $targetEGObjs[$targetEGId];
        if (array_key_exists($entryObj->ageGroupId, $targetEGAgesObj)) {
            return $targetEGAgesObj[$entryObj->ageGroupId];
        }

        $obj = new stdClass();
        $obj = $this->_setCompIDOnObj($obj, 0);
        $obj = $this->_setCeIDOnObj($obj, 0);
        return $obj;
    }

    private function _addMessage($msg, $key): stdClass {
        $msgObj = new stdClass();
        $msgObj->text = $msg;
        $msgObj->key = $key;
        return $msgObj;
    }

    private function _setTargetCompId($eOptions, $compId) {
        $autoObj = $this->getEntryAutoObj($eOptions);
        $targetEG = $this->getTargetEventGroupObj($autoObj);
        $this->_setCompIDOnObj($targetEG, $compId);
        return $eOptions;
    }

    public function createTargetEntry($entryObj): bool {
        include_once E4S_FULL_PATH . 'product/createProduct.php';
        include_once E4S_FULL_PATH . 'eventteams/eventTeamCRUD.php';

        $ceid = $this->_getTargetCeId($entryObj->options);
        if ($ceid === 0) {
            return FALSE;
        }
        // set Global User id to be used when creating target Entry
        $sourceId = 0;
        if ($this->_getEventType($entryObj) === self::E4S_EVENT_TYPE_INDIVIDUAL) {
            $athleteid = $entryObj->athleteId;
            $productObj = new stdClass();
            $productObj->ceId = $ceid;
            $productObj->athleteId = $athleteid;
            $productObj->sourceId = $sourceId;
            $productObj->userId = $entryObj->userId;
            $data = e4s_createProduct($productObj, FALSE);
        } else {
            // create team event
            $teamObj = array();
            $teamObj['forminfo'] = array();
            $teamObj['id'] = 0;
            $teamObj['eventteamid'] = 0;
            $teamObj['entitylevel'] = $entryObj->entityLevel;
            $teamObj['entityid'] = $entryObj->entityId;
            $teamObj['ceid'] = $ceid;
            $teamObj['compid'] = $this->_getTargetCompId($entryObj->options);
            $teamObj['formno'] = 0;
            $teamObj['teamname'] = $entryObj->teamName;
            $teamObj['athletes'] = $entryObj->athletes;
            $teamObj['formrows'] = array();
            $teamObj['type'] = E4S_TEAM_TYPE_STD;
            $teamObj['process'] = 'C';
            $teamObj['userId'] = $entryObj->userId;
            $data = createEventTeam($teamObj, FALSE);
        }
        $this->_setTargetEntryId($entryObj->options, $data->entryInfo->entryId);
        $this->_setTargetPaid($entryObj->options, $data->entryInfo->paid);
        $this->_setTargetOrderId($entryObj->options, $data->entryInfo->orderId);
        $this->_updateSourceEntryOptions($entryObj);
        return TRUE;
    }

    private function _getTargetCeId($eOptions) {
        $autoObj = $this->getEntryAutoObj($eOptions);
        $targetEG = $this->getTargetEventGroupObj($autoObj);

        return $this->_getCeIdFromObj($targetEG);
    }

    private function _getCeIdFromObj($obj) {
        if (isset($obj->ceId)) {
            return $obj->ceId;
        }
        return 0;
    }

    private function _getTargetCompId($eOptions) {
        $autoObj = $this->getEntryAutoObj($eOptions);
        $targetEG = $this->getTargetEventGroupObj($autoObj);

        return $this->_getCompIdFromObj($targetEG);
    }

    private function _getCompIdFromObj($obj) {
        if (isset($obj->compId)) {
            return $obj->compId;
        }
        return 0;
    }

    private function _setTargetEntryId($eOptions, $id) {
        $autoObj = $this->getEntryAutoObj($eOptions);
        $targetEntry = $this->getTargetEntryObj($autoObj);
        $targetEntry->id = $id;
        return $eOptions;
    }

    private function _updateSourceEntryOptions($entryObj) {
        $options = e4s_deepCloneToObject($entryObj->options);
        $options = $this->_getOptionsForWriting($options);
        if ($this->_getEventType($entryObj) === self::E4S_EVENT_TYPE_INDIVIDUAL) {
            $sql = 'update ' . E4S_TABLE_ENTRIES;
        } else {
            $sql = 'update ' . E4S_TABLE_EVENTTEAMENTRIES;
        }

        $sql .= " set options = '" . e4s_getOptionsAsString($options) . "'
                where id = " . $entryObj->entryId;
        e4s_queryNoLog($sql);
    }

    private function _getOptionsForWriting($options) {
        $autoEntryObj = $this->getEntryAutoObj($options);
        $entryObj = $this->getTargetEntryObj($autoEntryObj);
        unset($entryObj->paid);

        $egObj = $this->getTargetEventGroupObj($autoEntryObj);
        unset($egObj->name);
        unset($egObj->ceId);
        $options = e4s_removeDefaultEntryOptions($options);
        return $options;
    }

    private function _deleteTargetEntry($entryObj): bool {
        $options = $entryObj->options;
        $paid = $this->getTargetEntryPaid($options);
        $isTeamEvent = FALSE;
        if (isset($entryObj->ceOptions)) {
            $ceOptions = e4s_addDefaultCompEventOptions($entryObj->ceOptions);
            $isTeamEvent = $ceOptions->isTeamEvent;
        }

        if ($paid === E4S_ENTRY_PAID) {
            return FALSE;
        }
        $entryId = $this->getTargetEntryId($options);
        if (!$isTeamEvent) {
            $sql = 'select variationId prodId
                from ' . E4S_TABLE_ENTRIES . '
                where id = ' . $entryId;
        } else {
            $sql = 'select productId prodId
                    from ' . E4S_TABLE_EVENTTEAMENTRIES . '
                    where id = ' . $entryId;
        }
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows > 0) {
            $prodObj = $result->fetch_object();
            $prodId = (int)$prodObj->prodId;
            include_once E4S_FULL_PATH . 'entries/entries.php';
            // Need the users ID
            $userId = $entryObj->userId;
            e4s_removeProductFromUsersBasket($prodId, $userId);
        }
        $this->_removeTargetFromAutoEntries($entryObj);
        $this->_updateSourceEntryOptions($entryObj);
        return TRUE;
    }

    private function _removeTargetFromAutoEntries($entryObj) {
        $options = $entryObj->options;
        $this->_setTargetEntryId($options, 0);
        $this->_setTargetEGId($options, 0);
    }

    private function _emailTargetEntries($userArray) {
        $userIds = array();
        foreach ($userArray as $userId => $userInfo) {
            $userIds[] = $userId;
        }
        $userObj = new e4sUserClass();
        $users = $userObj->getRows($userIds);

        foreach ($userArray as $userId => $userInfo) {
            if (!empty($userInfo->competitions)) {
                $userInfo->user = $users[$userId];
                $entryObj = new entryClass();
                $entryObj->emailUserAbountEntries($userInfo);
            }
        }
    }
}
