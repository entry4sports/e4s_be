<?php

const ROSTER_COOKIE_FILE = 'getcookie.sh';
const ROSTER_STARTLIST_FILE = 'getstart.sh';
const ROSTER_STARTLIST_FILENAME = 'startlist';
const ROSTER_RESULTS_FILENAME = 'results';
const ROSTER_OLD_FILE = '_last';
const ROSTER_FILE_EXT = '.csv';
const ROSTER_RESULTS_FILE = 'getresults.sh';
const ROSTER_SOCKET_LAST = 'roster_last';
const ROSTER_SOCKET_COOKIE = 'roster_cookie';
const ROSTER_SOCKET_STARTLIST = 'roster_startlist';
const ROSTER_SOCKET_RESULTS = 'roster_result';
const ROSTER_SOCKET_EVENT = 'roster_event';
const ROSTER_TRACK = 't';
const ROSTER_FIELD = 'f';
class rosterClass {
	var $userEmail;
	var $userPassword;
	var $compId;
	var $path;
	function __construct($compId, $userEmail, $userPassword) {
		$this->userEmail = $userEmail;
		$this->userPassword = $userPassword;
		$this->compId = $compId;
		$this->path = '/var/www/vhosts/roster';
	}
	function setPath($path) {
		$this->path = $path;
	}
	function setCookie(){
		$cmd = $this->path . "/" . ROSTER_COOKIE_FILE . " " . $this->compId . " " . $this->userEmail . " " . $this->userPassword . " > " . $this->path . "/output 2>> " . $this->path . "/output";
		shell_exec($cmd);
	}
	function createStartListFileFromRoster(){
		$cmd = $this->path . "/" . ROSTER_STARTLIST_FILE . " " . $this->compId . " > " . $this->path . "/output 2>> " . $this->path . "/output";
		shell_exec($cmd);
	}
	function createStartListFile($useSockets = false){
		$this->createStartListFileFromRoster();
		$currentStartList = $this->readStartList();
		if ( $useSockets ) {
			$lastStartList = $this->readStartList( ROSTER_OLD_FILE );
			include_once E4S_FULL_PATH . 'classes/socketClass.php';
			if ( is_null( $lastStartList ) ) {
				// send all info
				$this->sendArrayToSocket( $currentStartList, ROSTER_SOCKET_STARTLIST );
			} else {
				$currentStartListStr = json_encode( $currentStartList );
				$lastStartListStr    = json_encode( $lastStartList );
				if ( $currentStartListStr !== $lastStartListStr ) {
					// send only differences
					foreach ( $currentStartList as $eventId => $event ) {
						$sendEvent = FALSE;
						if ( ! isset( $lastStartList[ $eventId ] ) ) {
							// did not exist before
							$sendEvent = TRUE;
						} else {
							$currentStr = json_encode( $event );
							$lastStr    = json_encode( $lastStartList[ $eventId ] );
							if ( $currentStr !== $lastStr ) {
								$sendEvent = TRUE;
							}
						}
						if ( $sendEvent ) {
							$this->sendToSocket( $event, ROSTER_SOCKET_STARTLIST );
						}
					}
				}
			}
		}
		$this->moveFile(ROSTER_STARTLIST_FILENAME);
		if ( !$useSockets ){
			return $currentStartList;
		}
		return null;
	}
	function moveFile($filename){
		$newfilename = $this->path . "/" . $this->compId . "-" . $filename . ROSTER_FILE_EXT;
		$lastFilename = $this->path . "/" . $this->compId . "-" . $filename . ROSTER_OLD_FILE . ROSTER_FILE_EXT;
		if (file_exists($lastFilename)) {
			// delete file
			unlink($lastFilename) ;
		}
		if (file_exists($newfilename)) {
			rename($newfilename, $lastFilename);
		}
	}
	function sendToSocket($obj, $type){
		e4s_sendSocketInfo($this->compId, $obj, $type, false);
	}
	function sendArrayToSocket($obj, $type){
		foreach($obj as $eventId => $event){
			$this->sendToSocket($event, $type);
		}
	}
	function createResultFileFromRoster(){
		$cmd = $this->path . "/" . ROSTER_RESULTS_FILE . " " . $this->compId . " > " . $this->path . "/output 2>> " . $this->path . "/output";
		shell_exec($cmd);
	}
	function createResultsFile($useSockets = false){
		$this->createResultFileFromRoster();
		$currentResults = $this->readResults();
		if ( is_null( $currentResults ) ) {
			Entry4UIError( 5900, "Unable to read Roster results" );
		}
		if ( $useSockets ) {
			include_once E4S_FULL_PATH . 'classes/socketClass.php';
			$lastResults = $this->readResults( ROSTER_OLD_FILE );
			if ( is_null( $lastResults ) ) {
				e4s_sendSocketInfo( $this->compId, $currentResults->meeting, ROSTER_SOCKET_EVENT, FALSE );
				$this->sendArrayToSocket( $currentResults->events, ROSTER_SOCKET_RESULTS );
			} else {
				foreach ( $currentResults->events as $eventId => $event ) {
					$sendEvent = FALSE;
					if ( ! isset( $lastResults->eventId ) ) {
						// did not exist before
						$sendEvent = TRUE;
					} else {
						$currentStr = json_encode( $event );
						$lastStr    = json_encode( $lastResults[ $eventId ] );
						if ( $currentStr !== $lastStr ) {
							$sendEvent = TRUE;
						}
					}
					if ( $sendEvent ) {
						$event->eventId = $eventId;
						$this->sendToSocket( $event, ROSTER_SOCKET_RESULTS );
					}
				}
			}
		}
		$this->moveFile(ROSTER_RESULTS_FILENAME);
		if ( !$useSockets ){
			return $currentResults;
		}
		return null;
	}
	function readResults($instance = ""){
		$filename = $this->path . "/" . $this->compId . "-" . ROSTER_RESULTS_FILENAME . $instance . ROSTER_FILE_EXT;

		if (!file_exists($filename)) {
			return null;
		}
		$handle = fopen($filename, "r");
		$header = array_flip(fgetcsv($handle));

		$meetingName = $header['MeetingName'];
		$venue = $header['VenueName'];
		$organiser = $header['Organiser'];
		$startListId = $header['StartListId'];
		$startStatus = $header['StartStatus'];
		$title = $header['Title'];
		$eventStart = $header['EventStart'];
		$eventCode = $header['EventCode'];
		$eventName = $header['EventName'];
		$heatFinal = $header['EventStage']; // Heat, Final
		$heat = $header['EventGroup'];
		$windReading = $header['WindReading'];
		$trackClassification = $header['ParaClassRunJump'];
		$fieldClassification = $header['ParaClassThrow'];
		$name = $header['FullName'];
		$gender = $header['Gender'];
		$ageGroup = $header['AgeGroup'];
		$clubName = $header['ClubName'];
		$bibNumber = $header['BibNumber'];
		$pb = $header['PersonalBest'];
		$sb = $header['SeasonBest'];
		$overallPosition = $header['Place'];
		$position = $header['PositionHeat'];
		$lane = $header['Lane'];
		$resultRecord = $header['ResultRecord']; // sb or pb
		$qualify = $header['Promotion']; // Q, q
		$result = $header['Result']; // to 1000th
		$resultRounded = $header['ResultRounded']; // to 100th

		$teamName = $header['TeamName'];
		$teamGender = $header['TeamGender'];
		if ( array_key_exists('TC#TFRegular#LIA 2024', $header) ){
			$teamScore = $header['TC#TFRegular#LIA 2024'];
		} else {
			$teamScore = '';
		}

		$results = new stdClass();
		$results->meeting = new stdClass();
		$results->meeting->name = "";
		$results->events = array();
		while(! feof($handle)) {
			$content = fgetcsv( $handle );
			if ( ! feof( $handle ) ) {
				$contentStartId = $content[ $startListId ];

				if ( ! array_key_exists( $contentStartId, $results->events ) ) {
					$eventObj                     = new stdClass();
					$eventObj->title              = $content[ $title ];
					$eventObj->eventId            = $content[ $startListId ];
					$eventObj->eventStart         = $content[ $eventStart ];
					$eventObj->eventCode          = $content[ $eventCode ];
					$eventObj->eventName          = $content[ $eventName ];
					$eventObj->heatFinal          = $content[ $heatFinal ];
					$eventObj->results            = array();
					$results->events[ $contentStartId ] = $eventObj;
					if ( $results->meeting->name === '' ) {
						$results->meeting->id = $this->compId;
						$results->meeting->name = $content[ $meetingName ];
						$results->meeting->venue = $content[ $venue ];
						$results->meeting->organiser = $content[ $organiser ];
					}
				}
				$heatNo = $content[$heat];
				if ( !array_key_exists($heatNo, $results->events[ $contentStartId ]->results) ) {
					$results->events[ $contentStartId ]->results[ $heatNo ] = array();
				}

				$eventResult = new stdClass();
				$eventResult->startStatus = $content[ $startStatus ];
				$eventResult->bibNo = $content[ $bibNumber ];
				$eventResult->position = (int)$content[ $position ];
				$eventResult->overallPosition = (int)$content[ $overallPosition ];
				$eventResult->laneNo = (int)$content[ $lane ];
				$eventResult->name = $content[ $name ];
				$eventResult->gender = $content[ $gender ];
				$eventResult->fieldClassification  = $content[ $fieldClassification ];
				$eventResult->trackClassification  = $content[ $trackClassification ];
				$eventResult->pb = $content[ $pb ];
				$eventResult->sb = $content[ $sb ];
				$eventResult->result = $content[ $result ];
				$eventResult->resultRounded = $content[ $resultRounded ];
				$eventResult->resultRecord = $content[ $resultRecord ];
				$eventResult->qualify = $content[ $qualify ];
				$eventResult->ageGroup = $content[ $ageGroup ];
				$eventResult->clubName = $content[ $clubName ];
				$eventResult->wind = $content[ $windReading ];
				$eventResult->teamName = $content[ $teamName ];
				$eventResult->teamGender = $content[ $teamGender ];
				$eventResult->teamScore = 0;
				if ( array_key_exists( $teamScore, $content )) {
					$eventResult->teamScore = $content[ $teamScore ];
				}
				$results->events[ $contentStartId ]->results[ $heatNo ][] = $eventResult;
			}
		}
		fclose($handle);
		return $results;
	}
	function readStartList($instance = ""){
		$filename = $this->path . "/" . $this->compId . "-" . ROSTER_STARTLIST_FILENAME . $instance . ROSTER_FILE_EXT;
		if (!file_exists($filename)) {
			return null;
		}

		$handle = fopen($filename, "r");

		// read csv file
		$header = array_flip(fgetcsv($handle));

		$startListId = $header['StartListId'];
		$title = $header['Title'];
		$name = $header['FullName'];
		$gender = $header['Gender'];
		$heatFinal = $header['EventStage']; // Heat, Final
		$eventStart = $header['EventStart'];
		$eventCode = $header['EventCode'];
		$clubName = $header['ClubName'];
		if ( array_key_exists('RelayTeamName', $header) ){
			$relayName = $header['RelayTeamName'];
		} else {
			$relayName = '';
		}
		$bibNumber = $header['BibNumber'];
		$ageGroup = $header['AgeGroup'];
		$oldestAgeGroup = $header['OldestAgeGroup'];
		$multipleAgeGroups = $header['MultipleAgeGroups'];
		$trackClassification = $header['ParaClassRunJump'];
		$fieldClassification = $header['ParaClassThrow'];
		$pb = $header['PersonalBest'];
		$sb = $header['SeasonBest'];
		$heat = $header['EventGroup'];
		$lane = $header['Lane'];
		$startList = array();

		while(! feof($handle)) {
			$content = fgetcsv($handle);
			if (! feof($handle)) {
				try {
					$contentStartId = $content[ $startListId ];

					if ( ! array_key_exists( $contentStartId, $startList ) ) {
						$eventObj                     = new stdClass();
						$eventObj->eventId            = $contentStartId;
						$eventObj->title              = $content[ $title ];
						$eventObj->eventStart         = $content[ $eventStart ];
						$eventObj->eventCode          = $content[ $eventCode ];
						$eventObj->heatFinal          = $content[ $heatFinal ];
						$eventObj->ageGroup           = $content[ $ageGroup ];
						$eventObj->oldestAgeGroup     = $content[ $oldestAgeGroup ];
						$eventObj->multiAgeGroup      = $content[ $multipleAgeGroups ] === "true" ? true : false;
						$eventObj->entries            = array();
						$startList[ $contentStartId ] = $eventObj;
					}
					$contentHeatNo = (int)$content[ $heat ];
					if ( ! array_key_exists( $contentHeatNo, $startList[ $contentStartId ]->entries ) ) {
						$startList[ $contentStartId ]->entries[ $contentHeatNo ] = array();
					}
					$contentObj                                                = new stdClass();
					$contentObj->fieldClassification                           = $content[ $fieldClassification ];
					$contentObj->trackClassification                           = $content[ $trackClassification ];
					$contentObj->laneNo                                        = (int)$content[ $lane ];
					$contentObj->bibNo                                         = $content[ $bibNumber ];
					$contentObj->name                                          = $content[ $name ];
					$contentObj->gender                                        = $content[ $gender ];
					$contentObj->clubName                                      = $content[ $clubName ];
					if ( $relayName !== '' ) {
						$contentObj->relayName = $content[ $relayName ];
					}else{
						$contentObj->relayName = '';
					}
					$contentObj->pb                                            = $content[ $pb ];
					$contentObj->sb                                            = $content[ $sb ];
					$startList[ $contentStartId ]->entries[ $contentHeatNo ][] = $contentObj;
				} catch ( Exception $e ) {
					var_dump( $e );
				}
			}
		}
		fclose($handle);

		return $startList;
	}
}