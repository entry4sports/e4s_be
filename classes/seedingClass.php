<?php
require_once E4S_FULL_PATH . 'dbInfo.php';

class seedingClass {
    public $eventgroupid;

    public function __construct($eventgroupid) {
        $this->eventgroupid = $eventgroupid;
    }

    public static function getCompSeedings($eventGroupId) {
        $sql = '
            select s.* 
            from ' . E4S_TABLE_SEEDING . ' s,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_COMPEVENTS . ' ce2 
            where ce.maxgroup = ' . $eventGroupId . '
            and ce.CompID = ce2.CompID
            and s.eventgroupid = ce2.maxgroup
            ';

        $result = e4s_queryNoLog($sql);
        $records = array();
        while ($record = $result->fetch_object()) {
            if (!array_key_exists($eventGroupId, $records)) {
                $records[$eventGroupId] = array();
            }
            $records[$eventGroupId][$record->athleteid] = $record;
        }
        return $records;
    }

    public function clearAllForComp($compId) {
        $sql = '
            delete from ' . E4S_TABLE_SEEDING . '
            where eventgroupid in (
                select id
                from ' . E4S_TABLE_EVENTGROUPS . '
                where compid = ' . $compId . '
            )';

        e4s_queryNoLog($sql);
    }

    public function writeSeedings($athleteIds) {
        $this->clear();
        $sep = '';
        if (sizeof($athleteIds) < 1) {
            return;
        }
        $sql = 'Insert into ' . E4S_TABLE_SEEDING . '(eventgroupid,athleteid) values ';
        foreach ($athleteIds as $athleteId) {
            $sql .= $sep . '(' . $this->eventgroupid . ',' . $athleteId . ')';
            $sep = ',';
        }

        e4s_queryNoLog($sql);
    }

    public function clear() {
        $sql = '
            delete from ' . E4S_TABLE_SEEDING . '
            where eventgroupid = ' . $this->eventgroupid;

        e4s_queryNoLog($sql);
    }

    public function getEventSeedings() {
        $sql = '
            select * 
            from ' . E4S_TABLE_SEEDING . '
            where eventgroupid = ' . $this->eventgroupid;

        $result = e4s_queryNoLog($sql);
        $records = array();
        while ($row = $result->fetch_object()) {
            $records[] = $row;
        }
        return $records;
    }

    public function confirmSeedings($heatNo, $confirmed, $checkedIn, $data) {
        $sql = 'update ' . E4S_TABLE_SEEDING . ' 
                set confirmed = ' . $confirmed . '
                where eventgroupid = ' . $this->eventgroupid . '
                and heatno';
        if ($checkedIn) {
            $sql .= 'checkedin';
        }
        $sql .= ' = ' . $heatNo;
        e4s_queryNoLog($sql);

        $this->_buildAndSendToSocket($data, $confirmed);
    }

    private function _buildAndSendToSocket($data, $confirmed) {
        $compId = (int)$data['competition']['id'];

        if ($confirmed === 0) {
//            unset($data['participants']);
        }
        $socket = new socketClass($compId);
        $socket->setPayload($data);

        if ($data['toScoreboard'] === TRUE or $data['toScoreboard'] === 'true') {
            if ($confirmed) {
                $socket->sendMsg(R4S_CONFIRM_HEAT);
            } else {
                $compObj = e4s_getCompObj($compId);
                $data = new stdClass();
                $data->title = $compObj->getName();
                $data->message = '<br><br><br><br><br><br>';
                e4s_sendSocketInfo($compId, $data, R4S_SOCKET_ENTRIES_MESSAGE);
            }
        } else {
            $socket->sendMsg(R4S_CONFIRM_HEAT_NO_SCOREBOARD);
        }
    }

}