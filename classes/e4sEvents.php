<?php

const E4S_EVENT_GROUP_CACHE = 'E4S_EVENT_GROUP_CACHE';
const E4S_EVENT_KEY = 'E4S_EVENT_';

class e4sEvents {
    public $egObjs;
    public $egIds;

    public function __construct($compId = 0) {
        if ($compId !== 0) {
            if (!array_key_exists(E4S_EVENT_GROUP_CACHE . $compId, $GLOBALS)) {
                $egObject = new eventGroup($compId);
                $GLOBALS[E4S_EVENT_GROUP_CACHE . $compId] = $egObject->getEventGroups();
            }
            $this->egObjs = $GLOBALS[E4S_EVENT_GROUP_CACHE . $compId];
        }
    }

    public function listEventDefsForHomePage() {
        $sql = 'select id,
                       name,
                       tf
                from ' . E4S_TABLE_EVENTDEFS . '
                where pOf10 is not null';
        $events = [];
        $result = e4s_queryNoLog($sql);
        while ($obj = $result->fetch_object()) {
            $obj->id = (int)$obj->id;
            $events[$obj->id] = $obj;
        }
        return $events;
    }

    public static function hasUniqueEvents($options): bool {
        if (!isset($options->uniqueEventGroups)) {
            return FALSE;
        }
        return sizeof($options->uniqueEventGroups) > 0;
    }

    public function populateOptions($ceOptions, $egId) {
        $uniqueEventGroups = $ceOptions->uniqueEventGroups;
        $newArray = array();
        foreach ($uniqueEventGroups as $uniqueEventGroup) {
            $id = $uniqueEventGroup;
            if (gettype($id) !== 'integer') {
                $id = $uniqueEventGroup->id;
            }
            if ($id !== $egId) {
                if (array_key_exists($id, $this->egObjs)) {
                    $newArray[] = $this->egObjs[$id];
                }
            }
        }
        $ceOptions->uniqueEventGroups = $newArray;
        return $ceOptions;
    }

    public function updateUniqueOnOtherEvents($obj) {
        $compId = $obj->compId;
        $compObj = e4s_getCompObj($compId);
        $sourceEgId = $obj->egId;
        $sourceCeId = $obj->ceId;
        $options = $obj->options;
        $currentHasUnique = e4sEvents::hasUniqueEvents($options);
        $masterUniqueEgIds = array();
        if ($currentHasUnique) {
            foreach ($options->uniqueEventGroups as $eg) {
                $masterUniqueEgIds[] = $eg->id;
            }
        }
        foreach ($compObj->getCeObjs() as $ceObj) {
            $currentOptions = e4s_addDefaultCompEventOptions($ceObj->options);
            $currentUniqueEgIds = $currentOptions->uniqueEventGroups;

            if ($ceObj->id !== $sourceCeId and in_array($ceObj->egId, $masterUniqueEgIds) and $obj->ageGroupId === $ceObj->ageGroupId and $obj->gender === $ceObj->gender){
                $update = TRUE;
                // does the current egId exist in the unique list
                foreach ($currentUniqueEgIds as $currentUniqueEgId) {
                    if ($currentUniqueEgId->id === $sourceEgId) {
                        $update = FALSE;
                    }
                }
                if ($update) {
                    $newEg = new stdClass();
                    $newEg->id = $sourceEgId;
                    $currentUniqueEgIds[] = $newEg;

                    $currentOptions->uniqueEventGroups = $currentUniqueEgIds;
                    $currentOptions = e4s_removeDefaultCompEventOptions($currentOptions);
                    $sql = 'update ' . E4S_TABLE_COMPEVENTS . "
                        set options = '" . e4s_getOptionsAsString($currentOptions) . "'
                        where id = " . $ceObj->id;
                    e4s_queryNoLog($sql);
                }
            }
        }
    }

    public function setEventOptions($model) {
//		$egId = $model->maxgroup;
		$options = $model->options;
        $uEgs = $options->uniqueEventGroups;
        $newUEgs = array();
        $processed = array();
        foreach ($uEgs as $uEg) {
            // only want the unique egId
            if (!array_key_exists($uEg->id, $processed)) {
                $newObj = new stdClass();
                $newObj->id = $uEg->id;
                $newUEgs[] = $newObj;
            }
            $processed[$uEg->id] = $uEg->id;
        }

        $options->uniqueEventGroups = $newUEgs;
        return $options;
    }

    public function addUniqueEventOptions($ceObj) {
        $options = $ceObj->options;
        $options = e4s_addDefaultCompEventOptions($options);
        $egIds = array();
        foreach ($this->egIds as $eg) {
            if ($eg->id !== $ceObj->egId) {
                $egIds[] = $eg;
            }
        }
        $options->uniqueEventGroups = $egIds;
        return $options;
    }

    public function setUniqueEvents($ceIds) {
        $obj = $this->_getEgsAndCeIds($ceIds);
        $egIds = array();
        foreach ($obj->egObjs as $egObj) {
            // we are only writing the id to the db
            $newObj = new stdClass();
            $newObj->id = $egObj->id;
            $egIds[] = $newObj;
        }
        $this->egIds = $egIds;
        $ceObjs = $obj->ceObjs;
        foreach ($ceObjs as $ceId => $ceObj) {
            $ceoptions = $this->addUniqueEventOptions($ceObj);
            $ceoptions = e4s_removeDefaultCompEventOptions($ceoptions);
            $sql = 'update ' . E4S_TABLE_COMPEVENTS . "
                  set options = '" . e4s_getOptionsAsString($ceoptions) . "'
                  where id = " . $ceId;
            e4s_queryNoLog($sql);
        }
//        return full egObj if required
        return $obj->egObjs;
    }

    private function _getEgsAndCeIds($ceIds) {
        $sql = 'select eg.id
                      ,eg.name
                      ,eg.eventNo
                      ,ce.id ceId
                      ,ce.options
                from ' . E4S_TABLE_EVENTGROUPS . ' eg,
                     ' . E4S_TABLE_COMPEVENTS . ' ce
                where ce.maxgroup = eg.id
                and   ce.id in (' . implode(',', $ceIds) . ')
                order by eg.id, ce.id';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            return array();
        }
        $egObjs = array();
        $ceObjs = array();
        while ($obj = $result->fetch_object()) {
            $egObj = new stdClass();
            $egObj->id = (int)$obj->id;
            $egObj->eventNo = (int)$obj->eventNo;
            $egObj->name = $obj->name;
            $egObjs[$egObj->id] = $egObj;

            $ceObj = new stdClass();
            $ceObj->id = (int)$obj->ceId;
            $ceObj->egId = (int)$obj->id;
            $ceObj->options = $obj->options;
            $ceObjs[$ceObj->id] = $ceObj;
        }
        $retObj = new stdClass();
        $retObj->egObjs = $egObjs;
        $retObj->ceObjs = $ceObjs;
        return $retObj;
    }

    public static function getEventInfo($id) {
        if (array_key_exists(E4S_EVENT_KEY . $id, $GLOBALS)) {
            return $GLOBALS[E4S_EVENT_KEY . $id];
        }

        $sql = 'select *
                from ' . E4S_TABLE_EVENTS . '
                where id = ' . $id;
        $result = e4s_queryNoLog($sql);
		if ( $result->num_rows !== 1){
			Entry4UIError(9068, 'Event not found for id: ' . $id);
		}
        $obj = $result->fetch_object(E4S_EVENT_OBJ);
        $GLOBALS[E4S_EVENT_KEY . $id] = $obj;
        return $obj;
    }
}