<?php

//https://dev.entry4sports.co.uk/wp-json/e4s/v5/test?action=ea
class aniRegistrationClass {

    public function __construct() {
    }

    public function validateURN($urn) {
        if (is_null($urn)) {
            return null;
        }
        $urn = trim($urn);
        if ($urn === '') {
            return null;
        }

        $aniOutput = $this->_getEndPoint($urn);
        if (is_null($aniOutput) or strpos($aniOutput, '"data":') === FALSE) {
            return null;
        }
        $aniAthlete = e4s_getDataAsType($aniOutput, E4S_OPTIONS_OBJECT);
        if (is_null($aniAthlete) or !isset($aniAthlete->data)) {
            return null;
        }

        /*
         {"data": {"first_name": "David","last_name": "Abrahams","gender": "M","date_of_birth": "1959-09-14","club": "Glens Runners","clubcode": "GLENS"},"expires": "2024-03-31","licensed": true,"message": ""}
         */
        $e4sAthlete = new stdClass();
        $e4sAthlete->system = E4S_AOCODE_ANI;
        $e4sAthlete->status = $aniAthlete->licensed ? E4S_EA_REGISTERED : '';
        $e4sAthlete->dob = $aniAthlete->data->date_of_birth;
        $e4sAthlete->clubId = $aniAthlete->data->clubcode;
	    $e4sAthlete->clubName = clubTranslate($aniAthlete->data->club);
        $e4sAthlete->county = '';
        $e4sAthlete->region = '';
        $e4sAthlete->club2Id = 0;
        $e4sAthlete->club2Name = '';
        $e4sAthlete->firstName = $aniAthlete->data->first_name;
        $e4sAthlete->surName = $aniAthlete->data->last_name;
        $e4sAthlete->gender = $aniAthlete->data->gender;
        $e4sAthlete->activeEndDate = $aniAthlete->expires;
        $e4sAthlete->urn = $urn;

        return $e4sAthlete;
    }

    private function _getEndPoint($urn) {
        // set time out on curl request
        $envObj = $this->_getActiveInfo();
        $url = $envObj->url . $urn;

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 6);
        curl_setopt($ch, CURLOPT_TIMEOUT, 6);

        $headers = array('Content-Type: application/json');

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

//Execute the request.
        ob_start();
        $valid = curl_exec($ch);
        $content = ob_get_clean();
//Check for errors.
        if (curl_errno($ch)) {
            $content = null;
        }

        return $content;
    }

    private function _getActiveInfo() {
        $obj = new stdClass();
        $obj->url = 'https://data.opentrack.run/api/licence_status/ani/?token=b3424ddc-f0b0-4832-baeb-dd0e92f2a434&national_id=ANI';
        $obj->X_KEY_ID = '';
        $obj->X_KEY_SECRET = '';

        return $obj;
    }
}