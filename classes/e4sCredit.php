<?php
include_once E4S_FULL_PATH . 'dbInfo.php';

class e4sCredit {
    public $userId;
    public $credit;

    public function __construct($userId) {
        $this->userId = $userId;
        $this->credit = $this->_formatCredit(get_user_meta($userId, E4S_CREDIT_KEY, TRUE));
    }

    private function _formatCredit($value) {
        if ($value === '') {
            $value = '0';
        }
        return number_format((float)$value, 2);
    }

    public function getAudits() {
        $sql = 'select c.*,
                        u.user_login,
                        u.user_email
                from ' . E4S_TABLE_CREDIT . ' c,
                     ' . E4S_TABLE_USERS . ' u
                where c.userid = ' . e4s_getUserID() . '
                and   c.userid = u.id
                order by date';
        $result = e4s_queryNoLog($sql);
        $audits = array();
        while ($obj = $result->fetch_object()) {
            $user = new stdClass();
            $user->id = (int)$obj->userId;
            $user->name = $obj->user_login;
            $user->email = $obj->user_email;
            $obj->user = $user;
            $obj->date = date('c', strtotime($obj->date));
            unset($obj->userId);
            unset($obj->user_login);
            unset($obj->user_email);
            $audits[] = $obj;
        }
        return $audits;
    }

    public function addToCredit($addValue, $reason) {
        $addValue = $this->_formatCredit($addValue);
        if ($addValue > 0) {
            $this->setCredit($this->getCredit() + $addValue, $reason);
        }
    }

    public function getCredit() {
        $this->credit = str_replace(',', '', $this->credit);
        return (float)$this->credit;
    }

    public function setCredit($newCreditValue, $reason) {
        $this->_audit($this->getCredit(), $newCreditValue, $reason);
        $update = update_user_meta($this->userId, E4S_CREDIT_KEY, $newCreditValue);
        if ($update) {
            $this->credit = $this->_formatCredit($newCreditValue);
            if ($this->credit < 0.00) {
                $this->_surchargeWarning();
            }
        }
        return $update;
    }

    public function addFee($WC_Cart, $cartSubTotal) {
        $fees_cost = $this->getCredit();
        if ($fees_cost > $cartSubTotal) {
            $fees_cost = $cartSubTotal;
        }
        $title = 'Credit';
        if ($fees_cost < 0) {
            $title = 'Surcharge';
            $this->_surchargeWarning();
        }

        $WC_Cart->fees_api()->add_fee(array('id' => $this->userId, 'name' => 'E4S ' . $title, 'amount' => -$fees_cost, 'taxable' => FALSE, 'tax_class' => '', 'single_use' => FALSE, 'menu_order' => 1,));
    }

    private function _surchargeWarning() {
        e4s_mail(E4S_SUPPORT_EMAIL, 'Negative Credit amount for user ' . $this->userId, 'Warning, user : ' . $this->userId . ' has a negative credit amount', Entry4_mailHeader('', FALSE));
    }

    public function updateCreditFromOrder($order) {
        $orderId = $order->get_id();
        $orderValue = $order->get_subtotal();
        $existingCredit = $this->getCredit();
        $newCredit = 0;
        if ($orderValue < $existingCredit) {
            $newCredit = $existingCredit - $orderValue;
        }
        $this->setCredit($newCredit, 'Set by Order ' . $orderId);
    }

    private function _audit($curValue, $newValue, $reason) {
        if ('' . $curValue === '' . $newValue) {
            return;
        }
        $sql = 'insert into ' . E4S_TABLE_CREDIT . ' (oldValue,newValue,reason,userid)
                values(
                    ' . $curValue . ', 
                    ' . $newValue . ", 
                    '" . $reason . "', 
                    " . e4s_getUserID() . ' 
                )';
        e4s_queryNoLog($sql);
    }
}