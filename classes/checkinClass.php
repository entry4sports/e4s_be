<?php
define('E4S_CHECKIN_ALL', 1);

class checkinClass {
    public $compid;
    public $compObj;
    public $compCheckinObj;
    public $sqldate;
    public $date;
    public $firstname;
    public $surname;
    public $club;
    public $urn;
    public $bibno;
    public $entity;
    public $entityLevel;
    public $dob;
    public $collected;
    public $eventname;
    public $egId;
    public $pagesize = 100;
    public $pagenumber = 1;
    public $userNonce = E4S_NO_NONCE;
    public $parmsUsed = FALSE;
    public $orgUpdate;
    public $all;
    public $checkedin;
    public $whois;
    public $hostnameCheckin = 'db5000542419.hosting-data.io';
    public $databaseCheckin = 'dbs520785';
    public $usernameCheckin = 'dbu511301';
    public $passwordCheckin = 'Iloveathletics!1';

    public function __construct($compid, $checkinObj) {
        $userid = e4s_getUserID();
        $this->compid = $compid;
        if ($compid !== 0) {
            $this->compObj = e4s_getCompObj($compid, TRUE);
            $this->compCheckinObj = $this->compObj->getOptions()->checkIn;
        }
        $checkinObj = e4s_getOptionsAsArray($checkinObj);
        $this->date = $this->checkParam($checkinObj, 'date');
        $this->sqldate = ' curdate() ';
        if (!is_null($this->date) and $this->date !== '') {
            $this->sqldate = "'" . $this->date . "'";
        }
        $this->orgUpdate = $this->checkParam($checkinObj, 'orgUpdate');
        if (is_null($this->orgUpdate)) {
            $this->orgUpdate = FALSE;
        }
        $this->userNonce = $this->checkParam($checkinObj, 'userNonce');
        // if they are logged in, ignore any nonce sent
        if (is_null($this->userNonce) or $userid > E4S_USER_NOT_LOGGED_IN) {
            $this->userNonce = E4S_NO_NONCE;
        }
        $this->urn = $this->checkParam($checkinObj, 'urn', TRUE);
        $this->bibno = $this->checkParam($checkinObj, 'bibno');
        $this->dob = $this->checkParam($checkinObj, 'dob', TRUE);
        $this->firstname = $this->checkParam($checkinObj, 'firstname', TRUE);
        $this->surname = $this->checkParam($checkinObj, 'surname', TRUE);
        $this->club = $this->checkParam($checkinObj, 'club', TRUE);
        $this->eventname = $this->checkParam($checkinObj, 'eventname');
        $this->egId = $this->checkParam($checkinObj, 'egId');
        $this->collected = $this->checkParam($checkinObj, 'collected');
        if (is_null($this->collected)) {
            $this->collected = 0;
        }
        $this->collected = (int)$this->collected;
        $this->whois = $this->checkParam($checkinObj, 'whois');
        if (is_null($this->whois)) {
            $this->whois = 0;
        }
        $this->whois = (int)$this->whois;
        $this->all = $this->checkParam($checkinObj, 'all');
        if (is_null($this->all)) {
            $this->all = 0;
        }
        $this->checkedin = $this->checkParam($checkinObj, 'checkedin');
        if (is_null($this->checkedin)) {
            $this->checkedin = 0;
        }
        $this->checkedin = (int)$this->checkedin;

        $this->entity = $this->checkParam($checkinObj, 'entity', TRUE);
        $this->entityLevel = $this->checkParam($checkinObj, 'entitylevel', TRUE);
//        $this->club = null;
        $this->area = null;
        $this->validateEntity();
        $this->pagenumber = $this->checkParam($checkinObj, 'page');
        $pagesize = $this->checkParam($checkinObj, 'pagesize');
        if ($pagesize > 100) {
            $pagesize = 100;
        }
        $this->pagesize = $pagesize;
    }

    public function checkParam($arr, $key, $markParms = FALSE) {
        if (array_key_exists($key, $arr)) {
            $val = $arr[$key];
            if (is_null($val) or $val === '') {
                return null;
            }
            if ($markParms) {
                $this->parmsUsed = TRUE;
                $this->userNonce = E4S_NO_NONCE;
            }
            return $arr[$key];
        }
        return null;
    }

    public function validateEntity() {
        if (e4s_getUserID() <= E4S_USER_NOT_LOGGED_IN) {
            return $this->clearEntity();
        }

        if ($this->entityLevel === E4S_CLUB_ENTITY) {
            $clubs = getUserClubs();
            foreach ($clubs as $club) {
                if ((int)$club['id'] === $this->entity) {
                    $this->club = $club;
                    return TRUE;
                }
            }
            Entry4UIError(7000, 'User does not have this club authority.', 200, '');
        }

        if ($this->entityLevel > E4S_CLUB_ENTITY) {
            $areas = getUserAreas();
            foreach ($areas as $area) {
                if ((int)$area['areaid'] === $this->entity) {
                    $this->area = $area;
                    return TRUE;
                }
            }
            Entry4UIError(7001, 'User does not have this area authority.', 200, '');
        }
        return $this->clearEntity();
    }

    public function clearEntity() {
        $this->entityLevel = 0;
        $this->entity = 0;
        return FALSE;
    }

    public function isEnabled() {
        return $this->compCheckinObj->enabled;
    }

    public function makeAvailableForCheckin() {
        $checkinConn = $this->_makeConnection();
        $sql = 'insert into ' . E4S_CHECKIN_TABLE . " ( domain, compid, expiry)
                values ('https://dev.entry4sports.com', $this->compid, '2020-06-31')
               ";
        e4s_queryNoLog($sql, $checkinConn);
    }

    public function _makeConnection() {
        return new mysqli($this->hostnameCheckin, $this->usernameCheckin, $this->passwordCheckin, $this->databaseCheckin);
    }

    public function setTextAndRemoveFromOptions(&$options) {
        $text = '';
        if (isset($options->checkIn->text)) {
            $text = $options->checkIn->text;
            unset($options->checkIn->text);
            if ($text === E4S_DEFAULT_CHECKIN_TEXT) {
                $text = '';
            }
        }
        $text = e4s_htmlFromUI($text);
        notesClass::set($this->compid, E4S_COMP_CHECKIN, $text);

        $text = '';
        if (isset($options->checkIn->terms)) {
            if (isset($options->checkIn->useTerms) and $options->checkIn->useTerms) {
                $text = $options->checkIn->terms;
            }
            unset($options->checkIn->useTerms);
            unset($options->checkIn->terms);
        }
        $text = e4s_htmlFromUI($text);
        notesClass::set($this->compid, E4S_COMP_CHECKIN_TERMS, $text);
    }

    public function getCheckinDetails() {
        $retObj = new stdClass();
        $checkinConn = $this->_makeConnection();
        $sql = '
            select * 
            from ' . E4S_CHECKIN_TABLE . '
            where compid = ' . $this->compid;
        $result = e4s_queryNoLog($sql, $checkinConn);
        if ($result->num_rows !== 1) {
            $retObj->errno = 2020;
            $retObj->error = 'This competition is not available for checkin';
        } else {
            $row = $result->fetch_assoc();

            $retObj->compid = $this->compid;
            $retObj->domain = $row['domain'];
            $retObj->expiry = 10102020293;
        }
        return $retObj;
    }

    public function _getParamFromObj($checkinObj, $param, $isInt = FALSE) {
        $returnVal = '';
        if ($isInt) {
            $returnVal = 0;
        }
        if (is_null($checkinObj)) {
            return $returnVal;
        }
        if (isset($checkinObj[$param])) {
            return $checkinObj[$param];
        }
        return $returnVal;
    }

    public function checkinEntries($data) {
        if (!$this->orgUpdate) {
            $isOrgUser = FALSE;
        } else {
            $isOrgUser = $this->isOrgUser();
        }
        $checkinPayload = array();
        $collectedArr = array();
        $expectedArr = array();
        $checkedinArr = array();
        $checkedoutArr = array();
	    $bibObj = new bibClass($this->compid);
        foreach ($data as $athlete) {
            $bibno = $athlete['bibNo'];
            $teamBibNo = 0;
            if (strpos($bibno, '(') > 0) {
                $bibArr = preg_split('~\(~', $bibno);
                $bibno = preg_split('~\)~', $bibArr[1]);
                $bibno = (int)$bibno;
                $teamBibNo = trim($bibArr[0]);
            }
			if ($bibno === 0 ){
				Entry4UIError(9030, 'Please issue this athlete with a Bib Number.',200, '');
			}

	        $bibObj->amendAthleteBib($athlete['athleteId'], $athlete['bibNo']);

            $athleteBibObj = new stdClass();
            $athleteBibObj->bibNo = $bibno;
            $athleteBibObj->teamBibNo = $teamBibNo;
            $notes = '';
            if (array_key_exists('notes', $athlete)) {
                $notes = $athlete['notes'];
            }
            $athleteBibObj->notes = $notes;
            $collected = $athlete['collected'];
            if ($collected) {
                $collectedArr[] = $athleteBibObj;
            } else {
                $expectedArr[] = $athleteBibObj;
            }
	        $obj = new stdClass();
	        $obj->bibNo = $athlete['bibNo'];
            foreach ($athlete['entries'] as $entry) {
                if ($entry['checkedIn'] === TRUE) {
                    $checkedinArr[] = $entry['id'];
					$obj->checkedIn = TRUE;
                    $checkinPayload[$entry['id']] = $obj;
                    foreach ($entry['combinedEntries'] as $combinedEntry) {
                        $checkedinArr[] = $combinedEntry;
                        $checkinPayload[$combinedEntry] = $obj;
                    }
                } else {
                    $checkedoutArr[] = $entry['id'];
	                $obj->checkedIn = False;
                    $checkinPayload[$entry['id']] = $obj;
                    foreach ($entry['combinedEntries'] as $combinedEntry) {
                        $checkedoutArr[] = $combinedEntry;
                        $checkinPayload[$combinedEntry] = $obj;
                    }
                }
            }
        }

//      check in and confirmed if a user
        if (sizeof($checkedoutArr)) {
            $this->_updateEntries($checkedoutArr, FALSE, !$isOrgUser);
        }
        if (sizeof($checkedinArr)) {
            $this->_updateEntries($checkedinArr, TRUE, !$isOrgUser);
        }

        if ($isOrgUser) {
            if (sizeof($expectedArr)) {
//            e4s_dump($expectedArr, "Expected");
                $this->_updateCollected($expectedArr, FALSE, $checkedinArr, $expectedArr);
            }
            if (sizeof($collectedArr)) {
//            e4s_dump($collectedArr, "Collected");
                $this->_updateCollected($collectedArr, TRUE, $checkedinArr, $expectedArr);
            }
        }
        $socketPayload = new stdClass();
        $socketPayload->checkins = $checkinPayload;
        $socketPayload->collected = $collected;
        $this->sendSocketInfo($socketPayload);
        Entry4UISuccess('');
    }

    public function isOrgUser($userid = null) {
        if (is_null($userid)) {
            $userid = $this->e4s_checkinGetUser();
        }
        if ($userid > E4S_USER_NOT_LOGGED_IN) {
            if (userHasPermission(PERM_CHECKIN, null, $this->compid)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function e4s_checkinGetUser() {
        $userid = e4s_getUserID();
        if ($this->userNonce !== E4S_NO_NONCE) {
            $userid = e4s_getUserFromNonce($this->userNonce);
        }
        return $userid;
    }

    public function _updateEntries($entries, $checkedIn, $isOrganiser) {

        $sql = 'select eg.startdate eventDate,
                       ce.options ceOptions,
                       ce.maxgroup egId,
                       ce.id ceId,
                       e.id entryId,
                       e.athleteId,
                       e.waitingPos
                from ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTGROUPS . ' eg
                where e.id in(' . implode(',', $entries) . ")
                and e.compeventid = ce.id
                and ce.compid = {$this->compid}
                and ce.maxgroup = eg.id
        ";

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            Entry4UIError(9035, 'Can not find Checkin Date Information', 200, '');
        }

        $entryIds = array();
        $openEgs = array(); // event group ids for those events open
        $closedEgEntries = array(); // entries that are in closed events
        // make new array in case passed in were incorrect
        $entryObj = null;
        $errors = array();
        while ($obj = $result->fetch_object()) {
            $obj->entryId = (int)$obj->entryId;
            $obj->waitingPos = (int)$obj->waitingPos;
            if ($obj->waitingPos > 0) {
                if (is_null($entryObj)) {
                    $entryObj = new entryClass();
                }
                $errors = $entryObj->moveOffWaitingList($obj->entryId);
            }
            $obj->egId = (int)$obj->egId;
            $obj->ceId = (int)$obj->ceId;
            $obj->athleteId = (int)$obj->athleteId;

            $entryIds[] = $obj->entryId;
            if ($this->isEventOpen($obj->eventDate, $obj->ceOptions)) {
                $openEgs[$obj->egId] = $obj->egId;
            } else {
                $obj->checkedIn = $checkedIn;
                $closedEgEntries[] = $obj;
            }
        }

        if ($checkedIn) {
            $checkedIn = 1;
        } else {
            $checkedIn = 0;
        }

        $update = 'update ' . E4S_TABLE_ENTRIES . "
                   set checkedin = {$checkedIn}";
        if ($isOrganiser) {
            $update .= ' ,confirmed = now()';
        }
        $whereStatement = ' where id in (' . implode(',', $entryIds) . ')';

        $update .= $whereStatement;
        e4s_queryNoLog($update);
        // if not checked in, remove any seedings !!!!!!
        // if checkin closed, create a seeding for each entry.
        // if checkin open, clear seedings
        $seedObj = new seedingV2Class($this->compid, null);
        if (!empty($openEgs)) {
            $seedObj->clear($openEgs);
        }
        if (!empty($closedEgEntries)) {
            $seedObj->updateUnSeededEntries($closedEgEntries);
        }
    }

    public function isEventOpen($eventDate, $options) {
        $options = e4s_addDefaultCompEventOptions($options);
        $fromMins = $this->getEventFrom($options);
        $toMins = $this->getEventTo($options);
        $eventDateTS = date_create($eventDate)->getTimestamp();
        $eventOpenTS = strtotime('-' . $fromMins . ' minutes', $eventDateTS);
        $eventCloseTS = strtotime('-' . $toMins . ' minutes', $eventDateTS);
        $nowTS = strtotime('now');
        if ($nowTS >= $eventOpenTS and $nowTS <= $eventCloseTS) {
            return TRUE;
        }
        return FALSE;
//        $eventOpen = date("d/m/Y G:i:s",$eventOpenTS);
//        $eventClose = date("d/m/Y G:i:s",$eventCloseTS);
    }

    public function getEventFrom($options) {
        if (isset($options->checkIn)) {
            if (isset($options->checkIn->from)) {
                return $options->checkIn->from;
            }
        }
        return $this->defaultFrom();
    }

    public function defaultFrom() {
        return $this->compCheckinObj->defaultFrom;
    }

    public function getEventTo($options) {
        if (isset($options->checkIn)) {
            if (isset($options->checkIn->to)) {
                return $options->checkIn->to;
            }
        }
        return $this->defaultTo();
    }

    public function defaultTo() {
        return $this->compCheckinObj->defaultTo;
    }

    public function _updateCollected($arr, $status, $checkedinArr, $expectedArr) {
        if ($status) {
            $status = 1;
        } else {
            $status = 0;
        }
        foreach ($arr as $bibObj) {
            $update = 'update ' . E4S_TABLE_BIBNO . "
                   set collected = {$status}, collectedtime =";
            if ($status === 1) {
                $update .= ' now()';
            } else {
                $update .= ' null';
            }
            $update .= ", notes='" . $bibObj->notes . "'";
            $update .= ' where bibno = ' . $bibObj->bibNo . '
                         and compid = ' . $this->compid;
            e4s_queryNoLog($update);
        }
    }

    public function sendSocketInfo($checkInData) {
        $socket = new socketClass($this->compid);
        $payload = $checkInData;
        $socket->setPayload($payload);
        $socket->sendMsg(R4S_SOCKET_ENTRIES_CHECKIN);
    }

    public function getAthleteForCheckin($Urn, $dob, $firstname, $surname) {
        $sql = 'select id id
                from ' . E4S_TABLE_ATHLETE . '
                where ';
        $cont = '';
        if (!is_null($Urn)) {
            $sql .= $cont . ' urn = ' . $Urn;
            $cont = ' and ';
        }
        if (!is_null($dob)) {
            $sql .= $cont . " dob = '{$dob}'";
            $cont = ' and ';
        }
        if (!is_null($firstname)) {
            $sql .= $cont . " firstname = '{$firstname}'";
        }
        if (!is_null($surname)) {
            $sql .= $cont . " surname = '{$surname}'";
        }
        $res = e4s_queryNoLog($sql);
        if ($res->num_rows !== 1) {
            Entry4UIError(2500, 'Unable to find athlete.', 200, '');
        }
        $row = $res->fetch_assoc();
        $athleteid = (int)$row['id'];
        $this->listCheckins($athleteid);
    }

    public function listCheckins($passedAthleteId, $orgRequest = FALSE, $today = TRUE, $exit = TRUE) {
        $sql = 'select e.athleteid, 
                       e.firstname,
                       e.surname,
                       e.gender gender,
                       e.urn urn,
                       e.dob dob,
                       e.egName eventName,
                       e.split,
                       e.eventstartdate startDate,
                       e.clubname club,
                       b.bibno,
                       b.collected,
                       b.collectedtime,
                       b.notes,
                       e.entryid, 
                       e.agegroup eventAgeGroup,
                       e.agegroupid ageGroupId,
                       e.checkedin,
                       e.confirmed,
                       e.teamBibNo,
                       0 waitingPos,
                       e.ceoptions ceOptions,
                       e.egId,
                       e.egoptions egOptions
                from  ' . E4S_TABLE_ENTRYINFO . ' e left join ' . E4S_TABLE_BIBNO . ' b on e.compid = b.compid and e.athleteid = b.athleteid,
                      ' . E4S_TABLE_EVENTS . " evt
                where e.compid = {$this->compid}
                and   e.eventid = evt.id
                and   e.paid in (" . E4S_ENTRY_PAID . ',' . E4S_ENTRY_QUALIFY . ')';

//        $today = false;
        if ($today) {
            $sql .= ' and DATE(e.startdate) >= ' . $this->sqldate;
        }
        if (!is_null($passedAthleteId)) {
            $sql .= " and e.athleteid = {$passedAthleteId} ";
        } else {
            $userid = $this->e4s_checkinGetUser();
            // returns true if org, false if user. Errors if not valid
            $isOrgUser = $this->validateRequest($userid, $orgRequest);
            $compare = ' = ';
            $wildcard = '';
            if ($isOrgUser or $this->whois === 1) {
                $compare = ' like ';
                $wildcard = '%';
            }

            if (!$isOrgUser && $userid > E4S_USER_NOT_LOGGED_IN) {
                // User logged on
                if ($this->entity === 0) {
                    // No entity defined so get entries for the users id if parms not passed or is a whois request
                    if (!$this->parmsUsed and $this->whois !== 1) {
                        $sql .= " and e.userid = {$userid} ";
                    }
                } else {
                    // Logged on with an entity
                    if ($this->entityLevel === E4S_CLUB_ENTITY) {
                        $sql .= ' and e.clubid = ' . $this->entity;
                    } else {
                        $sql .= ' and e.areaid = ' . $this->entity;
                    }
                    $sql .= ' ';
                }
            }

            if (!is_null($this->urn)) {
                $sql .= " and e.urn {$compare} '" . $this->urn . $wildcard . "' ";
            }
            if (!is_null($this->firstname)) {
                $sql .= " and e.firstname {$compare} '" . $this->firstname . $wildcard . "' ";
            }
            if (!is_null($this->surname)) {
                $sql .= " and e.surname {$compare} '" . $this->surname . $wildcard . "' ";
            }
            if (!is_null($this->dob)) {
                $sql .= " and e.dob = '" . $this->dob . "' ";
            }
            if (!is_null($this->club)) {
                $sql .= " and e.clubname {$compare} '" . $this->club . $wildcard . "' ";
            }
            if (!is_null($this->bibno) and $this->bibno !== '') {
                if ($this->compObj->useTeamBibs() and !is_null($this->egId)) {
                    $sql .= " and e.teamBibNo = '" . $this->bibno . "' ";
                    $sql .= ' and e.egid = ' . $this->egId . ' ';
                } else {
                    $sql .= " and b.bibno = '" . $this->bibno . "' ";
                }
            }
            if (!is_null($this->eventname)) {
                $sql .= " and e.eventname = '" . $this->eventname . "' ";
            }
            if ($this->collected !== -1) {
                $sql .= ' and b.collected = ' . $this->collected;
            }
        }

        if ($this->whois === 1) {
            $sql .= ' order by e.surname, e.firstname, b.bibno,e.teamBibNo, paid desc, e.startdate, evt.name';
        } else {
            $sql .= ' order by e.surname, e.firstname, b.bibno,e.teamBibNo, paid desc, evt.name';
        }

        $res = e4s_queryNoLog($sql);
        $rows = $res->fetch_all(MYSQLI_ASSOC);
        $noEntries = FALSE;
        if (!is_null($passedAthleteId)) {
            $noEntries = TRUE;
            if ($res->num_rows > 0) {
                $noEntries = FALSE;
            }
        }
        if ($noEntries) {
            if ($exit) {
                Entry4UIError(2510, 'Athlete not entered into any events.', 200, '');
            }
            return array();
        }

        $athletes = array();
        $athlete = new stdClass();
        $lastAthleteId = 0;
        $useConfirmedTime = null;

        $entriesFromArr = array();

        foreach ($rows as $row) {
            $egOptions = e4s_addDefaultEventGroupOptions($row['egOptions']);
            if ($egOptions->entriesFrom->id > 0) {
                $entriesFromArr[] = $row['entryId'];
                continue;
            }
			$athleteId = (int)$row['athleteId'];
            $waitingList = array();
			if ( is_null($row['bibno']) ) { // Bib No has not been generated for this athlete
				$row['bibno'] = 0;
			}
	        if (is_null($row['notes'])) {
		        $row['notes'] = '';
	        }
            $row['bibno'] = (int)$row['bibno'];
            if ($lastAthleteId !== $athleteId and $lastAthleteId !== 0) {
                $athletes[] = $athlete;
            }

            if ($lastAthleteId !== $athleteId) {
                $athlete = new stdClass();
//                $athlete->name = $row['athlete'];
                $athlete->athleteId = $athleteId;
                $athlete->firstName = formatAthleteFirstname($row['firstName']);
                $athlete->surName = formatAthleteSurname($row['surName']);
                $athlete->urn = $row['urn'];
                $athlete->dob = $row['dob'];
                $athlete->gender = $row['gender'];
                $bibNo = $row['bibno'];
                $athlete->bibNo = $bibNo;
                $athlete->teamBibNo = $row['teamBibNo'];
                $athlete->notes = $row['notes'];

                $useConfirmedTime = e4s_sql_to_iso($row['confirmed']);
                $athlete->confirmedTime = $useConfirmedTime;
                $athlete->collectedTime = null;
                if (!is_null($row['collectedtime'])) {
                    $athlete->collectedTime = e4s_sql_to_iso($row['collectedtime']);
                }
                $athlete->collected = FALSE;
                if ($row['collected'] === '1') {
                    $athlete->collected = TRUE;
                }
                $athlete->club = $row['club'];
            }

            if ($this->compObj->isAthleteOnWaitingListForId($athlete->athleteId, (int)$row['egId'])) {
//                e4s_addDebugForce("On Waiting List");
                $waitingList[] = $row['eventName'];
            }
            if ($this->compObj->useTeamBibs() and $row['teamBibNo'] !== '0') {
                $athlete->bibNo = $row['teamBibNo'] . ' (' . $athlete->bibNo . ')';
            }
            $confirmed = $row['confirmed'];
            if (!is_null($confirmed)) {
                if ($confirmed > $useConfirmedTime) {
                    $athlete->confirmedTime = e4s_sql_to_iso($confirmed);
                    $useConfirmedTime = $confirmed;
                }
            }
            $lastAthleteId = $athleteId;
            $entry = new stdClass();
            $entry->combinedEntries = $entriesFromArr;
            $split = '';

            if ($row['split'] !== '0') {
                $row['split'] = number_format($row['split'], 1);
                if ($row['split'] < 0) {
                    $char = '<';
                } else {
                    $char = '>';
                }
                $split = ' ( ' . $char . abs($row['split']) . ' )';
            }
            $entry->id = $row['entryId'];
            $entry->name = $row['eventName'] . $split;
            $entry->dateTime = e4s_sql_to_iso($row['startDate']);
            $entry->ageGroup = e4s_getVetDisplay($row['eventAgeGroup'], 'O ');
            $entry->checkedIn = FALSE;
            if ($row['checkedin'] === '1') {
                $entry->checkedIn = TRUE;
            }
            $ceoptions = e4s_addDefaultCompEventOptions($row['ceOptions']);
            $entry->checkInFrom = $ceoptions->checkIn->from;
            // -1 means there is a competition checkin Date and time override
            if (is_null($ceoptions->checkIn->from) or $ceoptions->checkIn->from === -1) {
                $entry->checkInFrom = $this->defaultFrom();
            }
            $entry->checkInTo = $ceoptions->checkIn->to;
            if (is_null($ceoptions->checkIn->to) or $ceoptions->checkIn->to === -1) {
                $entry->checkInTo = $this->defaultTo();
            }

            $athlete->entries[] = $entry;
            if (sizeof($waitingList) > 0) {
                if ($athlete->notes !== '') {
                    $athlete->notes .= '. ';
                }
                if (sizeof($waitingList) === 1) {
                    $athlete->notes .= (implode(',', $waitingList) . ' is ');
                } else {
                    $athlete->notes .= ('Events ' . implode(',', $waitingList) . ' are ');
                }
                $athlete->notes .= ' on the waiting list. Checkin will confirm their entry.';
            }
        }

        if (e4s_getOptionsAsString($athlete) !== '{}') {
            $athletes[] = $athlete;
        }

        $retObj = new stdClass();
        $retObj->athletes = $athletes;
        $retObj->meta = new stdClass();
        $this->addDataToObj($retObj->meta);
        if ($exit) {
            Entry4UISuccess('
            "data": ' . e4s_getOptionsAsString($retObj->athletes) . ', 
            "meta":' . json_encode($retObj->meta));
        }
        return $retObj;
    }

    public function validateRequest($userid, $orgRequest) {
        if ($orgRequest) {
            if ($this->isOrgUser($userid)) {
                return TRUE;
            }
            Entry4UIError(3012, 'You do not have authority to use this function.', 200, '');
        }

        if ($userid > E4S_USER_NOT_LOGGED_IN) {
            return FALSE;
        }

        $argCount = 0;
        if (!is_null($this->urn)) {
            $argCount++;
        }
        if (!is_null($this->firstname) and !is_null($this->surname)) {
            $argCount++;
        }

        if (!is_null($this->dob)) {
            $argCount++;
        }

        if ($argCount > 1) {
            return FALSE;
        }

        if ($this->whois === 1) {
            return FALSE;
        }
        Entry4UIError(3020, 'Please enter your name and AAI Number please.', 200, '');
    }

//    return false if User, True if Organiser

    public function addDataToObj(&$obj, $showcodes = FALSE) {
        $expected = 0;
        $sql = '
            SELECT collected collected, count(collected) count
            FROM ' . E4S_TABLE_BIBNO . " 
            where compid = {$this->compid} 
            group by collected
        ";
        $result = e4s_queryNoLog($sql);
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $checkInObj = new stdClass();
        $checkInObj->collected = 0;
        foreach ($rows as $row) {
            if ($row['collected'] === '1') {
                $checkInObj->collected = (int)$row['count'];
            }
            $expected += (int)$row['count'];
        }

        $checkInObj->expected = $expected;
        $checkInObj->checkInDateTimeOpens = $this->getCheckinDate();
        $checkInObj->defaultFrom = $this->defaultFrom();
        $checkInObj->defaultTo = $this->defaultTo();
        if ($showcodes) {
        }
        $obj->checkIn = $checkInObj;
        $this->addTextToOptions($obj);
        return $checkInObj;
    }

    public function getCheckinDate() {
        return $this->compCheckinObj->checkInDateTimeOpens;
    }

    public function addTextToOptions(&$options) {
        $options->checkIn->text = $this->getText();
        $terms = $this->getTerms();
        $options->checkIn->terms = $terms;
        if ($terms === '') {
            $options->checkIn->useTerms = FALSE;
        } else {
            $options->checkIn->useTerms = TRUE;
        }
    }

// Will only show athletes that HAVE A bibno so the report must have to have been run

    public function getText() {
        $text = notesClass::get($this->compid, E4S_COMP_CHECKIN);
        if ($text === '') {
            $text = E4S_DEFAULT_CHECKIN_TEXT;
        }
        return e4s_htmlToUI($text);
    }

    public function getTerms() {
        $text = notesClass::get($this->compid, E4S_COMP_CHECKIN_TERMS);
        return e4s_htmlToUI($text);
    }

    public function getSummary($fromToday = FALSE) {
//        $compObj = e4s_GetCompObj($this->compid, true);
        $summaryObj = $this->getCompObj()->getSummary($fromToday);
        $this->addDataToObj($summaryObj, TRUE);

        Entry4UISuccess('
            "data": ' . json_encode($summaryObj));
    }

    public function getCompObj() {
        return $this->compObj;
    }

    public function addCodesToOptions(&$options) {
        $sql = 'select distinct( date ( startdate )) date
                from ' . E4S_TABLE_COMPEVENTS . '
                where compid = ' . $this->compid;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            return;
        }
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $codearray = array();
        foreach ($rows as $row) {
            $codearray[$row['date']] = $this->generateCode($row['date']);
        }
        $options->checkIn->codes = $codearray;
    }

    public function generateCode($date, $length = 6) {
        $parts = explode('-', $date);
        $rand = rand(1, 100);
        $val = floor((int)$parts[0] / (int)$parts[1]) * ((int)$parts[2] * $rand);
        $min = pow(10, $length - 1) - 1;
        $max = pow(10, $length);
        if ($val > $min and $val < $max) {
            return $val;
        }
        $val = '' . $val . $val;
        $val = e4s_left($val, $length);
        return $val;
    }

    public function getCode($date = '') {
//        $compObj = e4s_getCompObj($this->compid);
        $compObj = $this->getCompObj();
//        $row = $compObj->getRow();
//        $options = e4s_getOptionsAsObj($row['options']);
        $options = $compObj->getOptions();
        if (!isset($options->checkIn->codes)) {
            Entry4UIError(8010, 'Checkin Codes not set for ' . $this->compid, 200, '');
        }

        $codes = $options->checkIn->codes;

        $today = date('Y-m-d');
        $useDate = $today;
        if ($date !== '' and !is_null($date)) {
            $useDate = $date;
        }
        foreach ($codes as $compdate => $code) {
            if ($compdate === $useDate) {
                return $code;
            }
        }
        Entry4UIError(8020, 'Can not find code for date : ' . $useDate . ' / ' . $this->compid, 200, '');
    }

    public function clearCheckins() {
        if ($this->collected === 1) {
            $sql = 'update ' . E4S_TABLE_BIBNO . '
                set collected = 0,
                    collectedtime = null
                where compid = ' . $this->compid;
            e4s_queryNoLog($sql);
        }
        if ($this->checkedin === 1) {
            $sql = 'update ' . E4S_TABLE_ENTRIES . '
                set checkedin = 0,
                    confirmed = null
                where compeventid in (
                        select id from ' . E4S_TABLE_COMPEVENTS . '
                        where compid = ' . $this->compid;
            if ($this->all !== E4S_CHECKIN_ALL) {
                $sql .= ' and startdate >= now()';
            }
            $sql .= ')';
            e4s_queryNoLog($sql);
        }
    }
}