<?php

class e4sHomePage {
    public $hpId;
    public $guid;
    public $reason;

    public function __construct() {
        $this->_setHpId();
    }

    private function _setHpId() {
        $this->hpId = (int)get_option('page_on_front');
    }

    public function setNewHomepage($id) {
//        $hpPost = $this->_getAHomePage($this->hpId, false);
        $newHpPost = $this->_getAHomePage($id, FALSE);
//        $hpPost->post_content = $newHpPost->post_content;
//        $hpPost->post_excerpt = json_encode($newHpPost->post_excerpt);
//        wp_insert_post((array)$hpPost, false);
        $sql = 'update ' . E4S_TABLE_POSTS . "
                set post_content = '" . addslashes($newHpPost->post_content) . "',
                    post_excerpt = '" . json_encode($newHpPost->post_excerpt) . "'
                where id = " . $this->hpId;
        e4s_queryNoLog($sql);
    }

    private function _getAHomePage($id, $clearId) {
        $sql = 'select *
                from ' . E4S_TABLE_POSTS . '
                where id = ' . $id;

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(4533, 'Invalid HP Id');
        }
        $post = $result->fetch_object(E4S_POST_OBJ);
        if ($clearId) {
            unset($post->id); // Remove id, wp will create new post if not set.
        }
        return $post;
    }

    public function createNewVersion($guid, $reason) {
        $this->guid = $guid;
        $this->reason = $reason;
        $hpPost = $this->_getAHomePage($this->_getHpId(), TRUE);
        return $this->_createNewDraft($hpPost);
    }

    private function _getHpId() {
        return $this->hpId;
    }

    private function _createNewDraft($post) {
        //set $nowDt as now plus 10 seconds
        $nowDt = new DateTime();
        $nowDt->modify('+10 second');
        $post->post_date = date('Y-m-d H:i:s', strtotime($nowDt->format('Y-m-d H:i:s')));

        $post->post_name = $this->guid;
        $sql = 'select *
                from ' . E4S_TABLE_POSTS . "
                where post_name = '" . $this->guid . "'";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            $excerpt = $post->post_excerpt;
            $oldGuid = $excerpt->guid;
            $excerpt->guid = $this->guid;
            $excerpt->reason = $this->reason;
            $post->post_title = $post->post_title . $this->guid;
            $post->post_excerpt = json_encode($excerpt);
            $post->post_content = str_replace($oldGuid, $this->guid, $post->post_content);
            $newId = wp_insert_post((array)$post, FALSE);
        } else {
            $postObj = $result->fetch_object(E4S_POST_OBJ);
            $newId = $postObj->id;
        }
        return $newId;
    }

    public function displayAll() {
        header('Content-Type: text/html; charset=utf-8');
        $hPages = $this->_getAllHomePages();
        $homePage = null;
        ?>
        <table>
            <?php
            //            output Home Page first
            foreach ($hPages as $hPage) {
                if ($hPage->id === $this->hpId) {
                    $homePage = $hPage;
                    $this->_outputHPage($hPage);
                }
            }
            foreach ($hPages as $hPage) {
                if ($hPage->id !== $this->hpId and $homePage->post_excerpt->guid !== $hPage->post_excerpt->guid) {
                    $this->_outputHPage($hPage);
                }
            }
            ?>
        </table>
        <?php
    }

    private function _getAllHomePages(): array {
        $homePages = array();
        $sql = 'select *
                from ' . E4S_TABLE_POSTS . '
                where id = ' . $this->hpId . "  or
                (
                   post_status in ('" . WC_POST_PUBLISH . "','future') and post_excerpt like '{\"guid\":%'     
                )
                order by post_date desc";

        $result = e4s_queryNoLog($sql);
        while ($postObj = $result->fetch_object(E4S_POST_OBJ)) {
            $homePages[] = $postObj;
        }
        return $homePages;
    }

    private function _outputHPage($hPage) {
        ?>
        <tr>
            <td><?php echo $hPage->post_date ?></td>
            <td><a href="/<?php echo $hPage->post_excerpt->guid ?>/"><?php echo $hPage->post_excerpt->guid ?></a></td>
            <?php
            if ($hPage->id === $this->hpId) {
                ?>
                <td>Current Homepage</td>
                <?php
            } else {
                ?>
                <td><a href="/wp-json/e4s/v5/public/e4s?action=sethp&hpid=<?php echo $hPage->id ?>">Set as Home Page</a>
                </td>
                <?php
            }
            ?>
            <td><?php echo $hPage->post_excerpt->reason ?></td>
        </tr>
        <?php
    }
}