<?php
include_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_updateNextUp($obj){
    $egId = checkFieldForXSS($obj, 'egId:Event Group Id' . E4S_CHECKTYPE_NUMERIC);
    $athleteId = checkFieldForXSS($obj, 'athleteId:Athlete Id' . E4S_CHECKTYPE_NUMERIC);
    $heatNo = checkFieldForXSS($obj, 'heatNo:Heat Number' . E4S_CHECKTYPE_NUMERIC);

    $nextObj = new e4sEventNextUp($egId);
    $nextObj->update($athleteId, $heatNo);
    Entry4UISuccess();
}
function e4s_deleteNextUp($obj){
    $egId = checkFieldForXSS($obj, 'egId:Event Group Id' . E4S_CHECKTYPE_NUMERIC);
    $nextObj = new e4sEventNextUp($egId);
    $nextObj->deleteNextUp();
    Entry4UISuccess();
}

class e4sEventNextUp {
    public $nextObj;

    public function __construct(int $egId) {
        $sql = 'select eg.compId, a.firstName, a.surName, n.*
                from ' . E4S_TABLE_EVENTGROUPS . ' eg
                   left join ' . E4S_TABLE_EVENTNEXTUP . ' n on eg.id = n.egId
                   left join ' . E4S_TABLE_ATHLETE . ' a on n.athleteid = a.id
                where eg.id = ' . $egId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
//            $nextObj = $this->_setDefaultObj($egId);
            Entry4UIError(8025, 'Invalid Event for nextup : ' . $egId);
        } else {
            $nextObj = $result->fetch_object(E4S_NEXTUP_OBJ);
            if (is_null($nextObj->egId)) {
                $nextObj = $this->_setDefaultObj($egId, $nextObj->compId);
            }
        }
        $this->_setObj($nextObj);
    }

    private function _getEgId() {
        return $this->nextObj->egId;
    }

    public function getAthlete() {
        $firstName = $this->nextObj->firstName;
        if (is_null($firstName)) {
            $firstName = '';
        }
        $surName = $this->nextObj->surName;
        if (is_null($surName)) {
            $surName = '';
        }
        return formatAthlete($firstName, $surName);
    }

    public function getAthleteId() {
        return $this->nextObj->athleteId;
    }

    public function getHeatNo() {
        return $this->nextObj->heatNo;
    }

    private function _getCompId() {
        return $this->nextObj->compId;
    }

    private function _setObj($nextObj) {
        $this->nextObj = $nextObj;
    }

    private function _setDefaultObj($egId, $compId = 0) {
        $nextObj = new stdClass();
        $nextObj->egId = $egId;
        $nextObj->athleteId = 0;
        $nextObj->heatNo = 0;
        $nextObj->compId = $compId;
        return $nextObj;
    }

    public function deleteNextUp() {
        $egId = $this->_getEgId();

        $sql = 'delete from ' . E4S_TABLE_EVENTNEXTUP . '
                where egid = ' . $egId;
        e4s_queryNoLog($sql);
        $this->_setObj($this->_setDefaultObj($egId));
    }

    private function _updateObj($athleteId, $heatNo) {
        $this->nextObj->athleteId = $athleteId;
        $this->nextObj->heatNo = $heatNo;
    }

    public function update($athleteId, $heatNo = 1) {
        if ($this->getAthleteId() !== 0) {
            $sql = 'update ' . E4S_TABLE_EVENTNEXTUP . '
                    set athleteId = ' . $athleteId . ',
                        heatNo = ' . $heatNo . '
                    where egid = ' . $this->_getEgId();
        } else {
            $sql = 'insert into ' . E4S_TABLE_EVENTNEXTUP . ' (egId, athleteId, heatNo )
                    values (
                      ' . $this->_getEgId() . ',
                      ' . $athleteId . ',
                      ' . $heatNo . '
                    )';
        }
        e4s_queryNoLog($sql);
        $this->_updateObj($athleteId, $heatNo);
        $this->_sendSocketInfo($heatNo);
    }

    private function _sendSocketInfo($heatNo) {
        $socket = new socketClass($this->_getCompId());
        $payload = new stdClass();
        $sql = '
            select c.name compName,
                    a.firstName,
                    a.surName,
                    eg.name eventName
            from ' . E4S_TABLE_COMPETITON . ' c,
                 ' . E4S_TABLE_ATHLETE . ' a,
                 ' . E4S_TABLE_EVENTGROUPS . ' eg
            where c.id = ' . $this->_getCompId() . '
              and a.id = ' . $this->getAthleteId() . '
              and eg.id = ' . $this->_getEgId();
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(8030, 'Failed to get next up info for socket');
        }
        $infoObj = $result->fetch_object();
        $payload->athlete = new stdClass();
        $payload->athlete->id = $this->getAthleteId();
        $payload->athlete->name = formatAthlete($infoObj->firstName, $infoObj->surName);

        $comp = new stdClass();
        $comp->id = $this->_getCompId();
        $comp->name = $infoObj->compName;
        $payload->comp = $comp;

        $event = new stdClass();
        $event->id = $this->_getEgId();
        $event->name = $infoObj->eventName;
        $event->heatNo = $heatNo;
        $payload->event = $event;

        $socket->setPayload($payload);
        $socket->sendMsg(R4S_SOCKET_EVENT_NEXT_UP);
    }
}