<?php

class scoreboardClass {
    public $compid = 0;
    public $eventno = 0;
    public $update;
    public $updates = array();
    public $athleteEvents = array();
    public $uom = 'm';
    public $uomType = 'D';
    public $eventIDs = array();
    public $eventNos = array();
    public $pbs = array();
    public $compInfo = null;
    public $eventGroupInfo = null;
    public $eventRankings = null;
    public $showLatestOnly = TRUE;

    public function __construct($compid, $eventno, $update = FALSE) {
        $this->compid = $compid;
        $this->eventno = $eventno;
        $this->update = $update;
        $this->updates = array();
        if ($eventno > 0) {
            $this->eventIDs = $this->getEventIDs();
            $this->pbs = $this->getAthletePBs();
        } elseif ($eventno === 0) {
            $this->eventNos = $this->getEventNos();
        }
    }

    public function getEventIDs() {
        $sql = '
            select distinct(ce.eventid) eventid
            from ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTGROUPS . ' eg
            where   ce.compid = ' . $this->compid . ' 
            and   ce.maxgroup = eg.id
            and eg.eventno = ' . $this->eventno;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            Entry4UIError(9207, 'Unable to get Events for a Group (' . $this->compid . '-' . $this->eventno . ')', 200, '');
        }
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $arr = array();
        foreach ($rows as $row) {
            $arr[] = (int)$row['eventid'];
        }
        return $arr;
    }

    public function getAthletePBs() {
        $sql = '
            SELECT pb.pb pb, pb.athleteid athleteid, pb.eventid eventid
            FROM ' . E4S_TABLE_ATHLETEPB . ' pb,
                 ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTGROUPS . ' eg
            WHERE ce.CompID = ' . $this->compid . '
            and ce.maxGroup = eg.id
            and eg.eventno = ' . $this->eventno . '
            and ce.id = e.compEventID
            and e.athleteid = pb.athleteid
            and pb.eventid = ce.EventID
            and e.paid in (' . E4S_ENTRY_PAID . ',' . E4S_ENTRY_QUALIFY . ')';

        $result = e4s_queryNoLog($sql);
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $arr = array();
        foreach ($rows as $row) {
            $obj = new stdClass();
            $obj->pb = (float)$row['pb'];
            $obj->eventid = $row['eventid'];
            $arr[(int)$row['athleteid']] = $obj;
        }
        return $arr;
    }

    public function getEventNos() {
        $sql = '
            select distinct(eventno) eventno
            from ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTGROUPS . ' eg
            where   ce.compid = ' . $this->compid . ' 
            and   ce.maxgroup = eg.id';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            Entry4UIError(9208, 'Unable to get Events  (' . $this->compid . ')', 200, '');
        }
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $arr = array();
        foreach ($rows as $row) {
            $arr[] = $row['eventno'];
        }
        return $arr;
    }

    public function get($exit = TRUE) {
//        $scoreboards = array();
//        $retObj = new stdClass();
//        $retObj->config = $this->getSBConfig();
        $scoreboardData = $this->getNewScoreboardObj();
        $scoreboardData = $this->getLatestScore(FALSE, $scoreboardData);
        if ($this->showLatestOnly and $this->isSBForTrack($scoreboardData)) {
            $this->addHeatScoreboards($scoreboards, $scoreboardData);
            $scoreboardData->scoreboards = $scoreboards;
        }
        Entry4UISuccess($scoreboardData);
    }

    public function getNewScoreboardObj() {
        $scoreboardData = new stdClass();
        $scoreboardData->ranking = $this->getRankings();
        $scoreboardData->config = $this->getSBConfig();
        $scoreboardData->comp = $this->getCompInfo();
        $scoreboardData->eventGroup = $this->eventGroupInfo;
        return $scoreboardData;
    }

    public function getRankings() {
        $sql = "SELECT b.bibno bibno, r.athleteid athleteid, concat(a.firstname, ' ', a.surname) athletename, max(CAST(r.resultvalue AS DECIMAL(5,2) ) ) bestscore , max(r.resultvalue) score, resultkey resultkey, r.options cardOptions
                FROM " . E4S_TABLE_CARDRESULTS . ' r,
                     ' . E4S_TABLE_ATHLETE . ' a,
                     ' . E4S_TABLE_BIBNO . ' b
                WHERE r.athleteid = a.id
                and   r.compid = ' . $this->compid . '
                and   r.eventno = ' . $this->eventno . '
                and   b.athleteid = a.id
                and   b.compid = ' . $this->compid . '
                group by r.compid, r.athleteid 
                ORDER BY bestscore desc, bibno';

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return array();
        }

        $rows = $result->fetch_all(MYSQLI_ASSOC);
        return $this->getRankingForRows($rows);
    }

    public function getRankingForRows($rows) {
        $ranking = array();
        $lastScore = 0;
        $lastPosition = 0;
        $position = 1;
        foreach ($rows as $row) {
            $row['scoreGood'] = FALSE;

            $pos = new stdClass();
            $pos->bibno = (int)$row['bibno'];
            $pos->athleteId = (int)$row['athleteid'];

            $pos->athleteName = $row['athletename'];
            // abs as times are negative to get order
            $score = abs((float)$row['bestscore']);
            $pb = 0.00;
            if (is_numeric($row['score'])) {
                $pb = $this->getHighest($pos->athleteId, $score);
            }
            $pos->pb = $this->formatScore($pb);
            $bestScore = resultsClass::ensureDecimals($score);
            $scoreText = '';
            if ($pb === $bestScore) {
                $scoreText = ' (pb)';
            }
            $currentScore = $this->formatScore($bestScore);
            if (strpos($currentScore, '.') !== FALSE) {
                $row['scoreGood'] = TRUE;
            }

            if (array_key_exists('cardOptions', $row)) {
                $cardOptions = e4s_getOptionsAsObj($row['cardOptions']);
                if (isset($cardOptions->ageGroup)) {
                    $row['ageGroup'] = $cardOptions->ageGroup;
                }
            }
            $pos->score = $currentScore . $scoreText;
            if ($currentScore !== $lastScore) {
                $pos->position = $position;
                $lastPosition = $position;
            } else {
                $pos->position = $lastPosition . '=';
                $lastSub = sizeof($ranking) - 1;

                if (strpos('' . $ranking[$lastSub]->position, '=') === FALSE) {
                    $ranking[$lastSub]->position .= '=';
                }
            }
            $pos->isTrack = $this->isTrackRace($row['resultkey']);
            $pos->heatNo = $this->getHeatNo($row['resultkey']);
            $lastScore = $currentScore;
            $ranking[] = $pos;
            $position += 1;
        }


        // Process those that dont have a score ( DNS etc)
        foreach ($rows as $row) {
            if (!is_numeric($row['score']) and !is_numeric($row['bestscore']) and !$row['scoreGood']) {
                $pos = new stdClass();
                $pos->bibno = (int)$row['bibno'];
                $pos->athleteId = (int)$row['athleteid'];
                $pos->athleteName = $row['athletename'];
                $pos->score = $row['score'];
//                e4s_dump($row,"Row",true,true,true);
                $pb = $this->getHighest($pos->athleteId, $score);
                $pos->pb = $this->formatScore($pb);
                $pos->position = 'N/A';
                $pos->isTrack = $this->isTrackRace($row['resultkey']);
                $pos->heatNo = $this->getHeatNo($row['resultkey']);
                $ranking[] = $pos;
            }
        }

        return $ranking;
    }

    public function getHighest($athleteid, $score) {
        $score = (float)$score;
        $pb = $score;

        if (array_key_exists($athleteid, $this->pbs)) {
            $existPB = $this->pbs[$athleteid]->pb;
            if ($score < $existPB) {
                $pb = $existPB;
            }
        }
        return resultsClass::ensureDecimals($pb);
    }

    public function formatScore($score) {
        $newScore = (float)$score;
        if ($newScore < 60) {
            $newScore = resultsClass::ensureDecimals($newScore);
        } else {
            $minutes = ceil($newScore / 60);
            $seconds = abs($newScore - ($minutes * 60));
            $newScore = $minutes . ':' . $seconds;
        }

        return E4S_ENSURE_STRING . $newScore;
    }

    public function isTrackRace($resultKey) {
        if (strpos($resultKey, 'h') !== FALSE) {
            return TRUE;
        }
        return FALSE;
    }

    // Not required as the idea was to get a throw or a jump not Height or Distance

    public function getHeatNo($resultkey) {
        $heat = str_replace('h', '', $resultkey);
        $heat = str_replace('t', '', $heat);
        return (int)$heat;
    }

    public function getSBConfig() {
        $conf = new stdClass();
        $conf->showScore = 10000; // Milliseconds
        $conf->showVideo = 10000; // Milliseconds
        $conf->showPicture = 10000; // Milliseconds
        $conf->polling = 3000; // Milliseconds
        $conf->ranksPerPage = 8;
        $conf->ranksPageTime = 5000; // Milliseconds
        return $conf;
    }

    public function getCompInfo() {
        if (is_null($this->compInfo)) {
            $sql = 'select c.Name name, cc.logo logo
                from ' . E4S_TABLE_COMPETITON . ' c,
                     ' . E4S_TABLE_COMPCLUB . ' cc
                where c.id = ' . $this->compid . '
                and c.compclubid = cc.id';
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows !== 1) {
                Entry4UIError(9110, 'Failed to get competition for Scoreboard', 200, '');
            }
            $row = $result->fetch_assoc();
            $sbcomp = new stdClass();
            $sbcomp->id = $this->compid;
            $sbcomp->name = $row['name'];
            $sbcomp->logo = $row['logo'];

            $this->compInfo = $sbcomp;
        }
        return $this->compInfo;
    }

    public function getLatestScore($exit = TRUE, $scoreboardObj = null) {

        $sql = "select score score, concat(firstname, ' ', surname) athletename, b.bibno, s.athleteid athleteid, s.modified modified
                from " . E4S_TABLE_SCOREBOARD . ' s,
                     ' . E4S_TABLE_ATHLETE . ' a ,
                     ' . E4S_TABLE_BIBNO . ' b
                where s.compid = ' . $this->compid . '
                and   s.eventno = ' . $this->eventno . '
                and   s.athleteid = a.id
                and   b.compid = ' . $this->compid . '
                and   b.athleteid = a.id';

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            $row = array();
            $row['athleteid'] = 0;
            $row['athletename'] = '';
            $row['score'] = '';
            $row['bibno'] = 0;
            $row['modified'] = '';
            $scoreboardObj->text = '';
            $scoreboardObj->video = '';
            $scoreboardObj->picture = '';
        } else {
            $row = $result->fetch_assoc();
            $scoreboardObj->text = 'ALL Scores displayed here are PROVISIONAL until approved by Event Referee';
            $scoreboardObj->video = '';
            $scoreboardObj->picture = '';
        }

        $scoreboardObj->athlete = new stdClass();
        $scoreboardObj->athlete->id = (int)$row['athleteid'];
        $scoreboardObj->athlete->name = $row['athletename'];
        $athleteid = $scoreboardObj->athlete->id;

        if ($athleteid === 0) {
            $scoreboardObj->athlete->pb = 0;
            $scoreboardObj->athlete->bibNo = 0;
            $scoreboardObj->score = new stdClass();
            $scoreboardObj->score->score = 0;
            $scoreboardObj->score->scoreUom = '';
            $scoreboardObj->score->modified = '';
        } else {
            $pb = $this->getHighest($athleteid, (float)$row['score']);
            $scoreboardObj->athlete->pb = $this->formatScore($pb);
            $scoreboardObj->athlete->bibNo = (int)$row['bibno'];
            $scoreboardObj->score = new stdClass();
            $pbText = '';
            $score = $this->formatScore($row['score']);
            if ('' . $pb === '' . $score) {
                $pbText = '(pb)';
            }
            $scoreboardObj->score->score = $score . ' ' . $pbText;
            $scoreboardObj->score->scoreUom = $this->uom;
            $scoreboardObj->score->modified = e4s_sql_to_iso($row['modified']);
        }
        if ($exit) {
            Entry4UISuccess($scoreboardObj);
        }
        return $scoreboardObj;
    }

    // given an athleteid, use the eventno to get the actual event record

    public function isSBForTrack($scoreboard) {

        if (!isset($scoreboard->ranking)) {
            return 0;
        }

        if (empty($scoreboard->ranking)) {
            return 0;
        }

        if ($scoreboard->ranking[0]->isTrack) {
            return 1;
        }
        return 0;
    }

    // if they dont have a pb record, create one with this score, else get the pb record
    // and check if it needs updating. There should be an event record for this athlete if we are calling this method
    // but check and error if not

    public function addHeatScoreboards(&$scoreboards, $overallObj) {
        // first get scoreboards into heat array
        $sbArr = array();
//        e4s_dump($overallObj,"overallObj", true, true, true);
        $rankings = $overallObj->ranking;
        $lastHeatNo = 1;
        foreach ($rankings as $ranking) {
            if (!array_key_exists($ranking->heatNo, $sbArr)) {
                $sbArr [$ranking->heatNo] = array();
            }
            $sbArr [$ranking->heatNo][] = $ranking;
            if ($ranking->heatNo > $lastHeatNo) {
                $lastHeatNo = $ranking->heatNo;
            }
        }
        if ($lastHeatNo === 1) {
            // no heats ( as yet ) so just add the scoreboard
            $scoreboards[] = $overallObj;
            return;
        }

        while ($lastHeatNo > 0) {
            if (array_key_exists($lastHeatNo, $sbArr)) {
                // we have results for the heat no
                $rows = $this->convertSBObjArrayToArrays($sbArr[$lastHeatNo]);

                if (!empty($rows) and !is_null($rows)) {
                    $heatScoreboard = e4s_deepCloneTo($overallObj, E4S_OPTIONS_OBJECT);
                    $heatScoreboard->ranking = $this->getRankingForRows($rows);
                    $heatname = $heatScoreboard->eventGroup->name;
                    $heatname = explode('@', $heatname)[0];
                    $heatname = explode(':', $heatname)[0];
                    $heatname .= ': Heat ' . $lastHeatNo;
                    $heatScoreboard->eventGroup->name = $heatname;
                    $scoreboards[] = $heatScoreboard;
                    if ($this->showLatestOnly) {
                        break;
                    }
                }
            }
            $lastHeatNo = $lastHeatNo - 1;
        }
    }

    public function convertSBObjArrayToArrays($scoreboardArr) {
        $returnRows = array();
        foreach ($scoreboardArr as $scoreboard) {
//            e4s_dump($scoreboard,"scoreboard", true, true,true);
            $row = array();
            $row['bibno'] = $scoreboard->bibno;
            $row['athleteid'] = $scoreboard->athleteId;
            $row['athletename'] = $scoreboard->athleteName;
            $row['pb'] = str_replace(E4S_ENSURE_STRING, '', $scoreboard->pb);
            $row['score'] = str_replace(E4S_ENSURE_STRING, '', $scoreboard->score);
            $row['bestscore'] = $row['score'];
            $row['resultkey'] = 'h' . $scoreboard->heatNo;

            $returnRows[] = $row;
        }
        return $returnRows;
    }

    public function getUOMType() {
        $sql = 'select uomtype uomtype, options options
                from ' . E4S_TABLE_EVENTGROUPS . ' eg,
                     ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTS . ' e,
                     ' . E4S_TABLE_UOM . ' u
                where eg.compid = ' . $this->compid . '
                and eg.eventno = ' . $this->eventno . '
                and ce.maxgroup = eg.id
                and ce.eventid = e.id
                and e.uomid = u.id
        ';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            $this->uom = 'm';
            $this->uomType = 'D';

        }
    }

    public function addToUpdate($athleteid,$resultkey, $score) {
		$obj = new stdClass();
		$obj->resultkey = $resultkey;
		$obj->score = $score;
	    $this->updates[ $athleteid ] = $obj;
    }

    public function updateScoreboard() {
        if (!$this->update) {
            return;
        }
        if (empty($this->updates)) {
            return;
        }
        $this->deleteScoreboard();
        $sql = 'insert into ' . E4S_TABLE_SCOREBOARD . ' (compid, athleteid, eventno, score, resultKey) values ';
        $sep = '';
        foreach ($this->updates as $athleteid => $resultObj) {
			$score = $resultObj->score;
			$resultkey = $resultObj->resultkey;
            if ($score === '-') {
                $score = 0;
            }
            $sql .= $sep . '(' . $this->compid . ',' . $athleteid . ',' . $this->eventno . ",'" . $score . "','" . $resultkey . "')";
            $this->checkAndupdatePB($athleteid, $score);
            $sep = ',';
        }
        e4s_queryNoLog($sql);
    }

    public function deleteScoreboard() {
        if (!$this->update) {
            return;
        }
        $sql = '
            delete from ' . E4S_TABLE_SCOREBOARD . '
            where  compid = ' . $this->compid . '
            and   eventno = ' . $this->eventno;
        e4s_queryNoLog($sql);
    }

    public function checkAndupdatePB($athleteid, $score) {
        if (!is_numeric($score)) {
            $score = 0;
        }
        $updatesql = '';
        if (array_key_exists($athleteid, $this->pbs)) {
            $pb = $this->pbs[$athleteid]->pb;
            $eventid = $this->pbs[$athleteid]->eventid;
            if ($pb < $score) {
                $updatesql = 'update ' . E4S_TABLE_ATHLETEPB . "
                set pb = {$score},
                    pbtext = '{$score}'
                where athleteid = {$athleteid}
                and eventid = " . $eventid;
            }
        } else {
            $eventid = $this->getEvent($athleteid);
            $updatesql = 'insert into ' . E4S_TABLE_ATHLETEPB . "(athleteid, eventid, pb, pbtext) values
                ({$athleteid}," . $eventid . ',' . $score . ',' . $score . ') 
            ';
        }

        if ($updatesql !== '') {
            e4s_queryNoLog($updatesql);
        }
    }

    public function getEvent($athleteid) {
        $sql = 'select eventid eventid
                from ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTGROUPS . " eg
                where e.athleteid = {$athleteid}
                and  e.compeventid = ce.id
                and ce.compid = " . $this->compid . '
                and eg.id = ce.maxgroup
                and eg.eventno = ' . $this->eventno;

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(7020, 'Failed to read Athletes Entry record', 200, '');
        }
        $row = $result->fetch_assoc();
        $eventid = (int)$row['eventid'];
        $this->athleteEvents[$athleteid] = $eventid;
        return $eventid;
    }

    public function getForEvents($obj) {
        $this->eventNos = $this->getEventNosFromObj($obj);
        $this->getAll();
    }

    public function getEventNosFromObj($obj) {
        $params = $obj->get_params('JSON');
        if (!array_key_exists('eventNos', $params)) {
            Entry4UIError(9041, 'Events not passed', 200, '');
        }
        $events = $params['eventNos'];
        if (is_null($events)) {
            Entry4UIError(9042, 'Valid Events not passed', 200, '');
        }
        $arr = array();
        foreach ($events as $event) {
            $arr[] = $event['eventNo'];
        }
        return $arr;
    }

    public function getAll() {
        $retObj = new stdClass();
        $retObj->config = $this->getSBConfig();

        $scoreboards = array();
        foreach ($this->eventNos as $eventNo) {
            $this->getEventInfo($eventNo);
            $newScoreboardObj = $this->getNewScoreboardObj();
            $scoreboardObj = $this->getLatestScore(FALSE, $newScoreboardObj);

            if (!empty($scoreboardObj->ranking)) {
                if (!$this->isSBForTrack($scoreboardObj) === 1) {
                    $scoreboards[] = $scoreboardObj;
                } else {
                    // If track, show each heat separately
                    $this->addHeatScoreboards($scoreboards, $scoreboardObj);
                }

            }
        }
        $retObj->scoreboards = $scoreboards;

        Entry4UISuccess($retObj);
    }

    public function getEventInfo($eventNo) {
        $this->eventno = $eventNo;
        $this->eventIDs = $this->getEventIDs();
        $this->pbs = $this->getAthletePBs();
        $this->eventGroupInfo = $this->getEventGroupInfo();
        $this->eventRankings = $this->getRankings();
    }

    public function getEventGroupInfo() {

        $sql = "select id id, name name, startdate startdate, date_format(startdate,'%h:%i %p') starttime
            from " . E4S_TABLE_EVENTGROUPS . ' 
            where compid = ' . $this->compid . '
            and eventno = ' . $this->eventno;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(9111, "Failed to get Event group for Scoreboard {$this->eventno}", 200, '');
        }
        $row = $result->fetch_assoc();
        $obj = new stdClass();
        $obj->id = $row['id'];
        $obj->name = $row['name'] . '  @ ' . $row['starttime'];
        $obj->startDateIso = e4s_sql_to_iso($row['startdate']);
        $this->eventGroupInfo = $obj;

        return $this->eventGroupInfo;
    }
}