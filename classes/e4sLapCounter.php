<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
require_once E4S_FULL_PATH . 'classes/socketClass.php';
require_once E4S_FULL_PATH . 'otd/tfConstants.php';
function e4s_displayLapScreen($obj){
    //    $screenId = (int)checkFieldFromParamsForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $compId = checkFieldFromParamsForXSS($obj, 'compid:Competition ID');
    if (is_null($compId) ){
//        exit("A competiton must be specified" );
        $compId = 310;
    }

    $screenId = checkFieldFromParamsForXSS($obj, 'screenid:Screen ID');
    new e4sLapCounter($compId,$screenId);
    exit;
}

class e4sLapCounter {
    public e4sCompetition $compObj;
    public int $screenId;
    public $isController;

    public function __construct(int $compId, $screenId) {
        $this->compObj = e4s_getCompObj($compId);
        $this->isController = FALSE;
        if (is_null($screenId)) {
            $this->isController = TRUE;
            $screenId = -1;
        }
        $this->screenId = $screenId;
        $this->displayPage();
    }

    public function displayPage() {
        e4s_webPageHeader();
        if ($this->isController) {
            $this->_displayController();
        } else {
            $this->_displayLapCounter();
        }
    }

    public function css() {
        ?>
        <style>
            .lapControllerBody {
                background-color: blue;
                font-size: 16px;
            }

            .lapDisplayBody {
                background: url(/resources/blue-background-background.gif);
                background-size: contain;
                margin: auto;
                width: 50%;
                overflow: hidden;
            }

            .lapNumber {
                font-size: 110vh;
                color: yellow;
            }

            .center {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                padding: 10px;
            }

            .controllerLapInput {
                width: 325px;
                font-size: 150px;
            }

            .controllerBtnImg {
                width: 50px;
                height: 50px;
            }

            .controllerBtn {
                border: 2px solid;
                background-color: yellow;
                width: 100px;
                text-align: center;
                height: 4vh;
            }

            .controllerUpTD {

            }

            .controllerDownTD {

            }

            .lapDisable {
                background-color: gray !important;
            }

            .controllerUp {
                width: 30vw;
                font-size: 10vh;
            }

            .controllerDown {
                width: 30vw;
                font-size: 10vh;
            }

            .playerButton {
                font-size: 7vh;
            }

            .clearButton {
                margin-top: 20px;
                margin-bottom: 20px;
                background-color: red;
                color: white;
            }

            .stdBtn {
                font-size: 5vh !important;
            }

            .ui-icon {
                font-size: 1.2em;
            }
        </style>
        <?php
    }

    private function _displayController() {
        ?>
        <head>
            <?php echo self::css(); ?>
            <link rel="stylesheet"
                  href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/<?php echo E4S_JQUERY_THEME ?>/jquery-ui.css"/>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        </head>
        <body class="lapControllerBody">
        <script>
            var changeEnabled = true;
            <?php echo socketClass::outputJavascript() ?>
            function getCompetition() {
                return {"id":<?php echo $this->compObj->getID() ?>}
            }

            function processSocketEvent(data) {

            }

            function lapDisable() {
                changeEnabled = false;
                const collection = document.getElementsByClassName("controllerBtn");
                for (let i = 0; i < collection.length; i++) {
                    collection[i].classList.add("lapDisable");
                }
            }

            function lapEnable() {
                changeEnabled = true;
                const collection = document.getElementsByClassName("controllerBtn");
                for (let i = 0; i < collection.length; i++) {
                    collection[i].classList.remove("lapDisable");
                }
            }

            function clearDisplay(display) {
                let lapNo = $("#lapNumber").val();
                if (display) {
                    lapNo = -1;
                }
                e4sSocket.send({lap: lapNo}, "<?php echo R4S_SOCKET_LAP ?>");
            }

            function changeLap(byNumber) {

                if (changeEnabled) {
                    lapDisable();
                    var element = $("#lapNumber");
                    if (typeof byNumber === "undefined") {
                        byNumber = 0;
                    }
                    var newLapNumber = parseInt(element.val()) + byNumber;
                    if (newLapNumber < 0) {
                        newLapNumber = 0;
                    }
                    if (newLapNumber > 99) {
                        newLapNumber = 99;
                    }
                    // element.value = ;
                    element.val(newLapNumber);
                    e4sSocket.send({lap: newLapNumber}, "<?php echo R4S_SOCKET_LAP ?>");
                    setTimeout(lapEnable, 1000);
                } else {
                    // alert("Please wait");
                }
            }
        </script>
        <div>
            <table>
                <tr>
                    <td rowspan="2">
                        <span id="lapCounterSpan">
                            <input class="controllerLapInput" id="lapNumber" value="1" type="number"
                                   onchange="changeLap()">
                        </span>
                    </td>
                    <td class="controllerBtn controllerUpTD" onclick="changeLap(1);">
                        <span class="controllerUp" "><img class="controllerBtnImg" src="/resources/up.gif"></span>
                    </td>
                </tr>
                <tr>
                    <td class="controllerBtn controllerDownTD" onclick="changeLap(-1);">
                        <span class="controllerDown"><img class="controllerBtnImg" src="/resources/down.gif"></span>
                    </td>
                </tr>
            </table>

            <audio id="player" src="/resources/bell2.mp3"></audio>
            <div>
                <button class="playerButton" onclick="document.getElementById('player').play()">Ring Bell</button>
                <!--                <button class="playerButton" onclick="document.getElementById('player').pause()">Pause</button>-->
                <!--                <button class="playerButton" onclick="document.getElementById('player').volume += 0.1">Vol +</button>-->
                <!--                <button class="playerButton" onclick="document.getElementById('player').volume -= 0.1">Vol -</button>-->
            </div>
            <div>
                <button class="playerButton stdBtn" onclick="clearDisplay(false)">Show Displays</button>
                <button class="clearButton stdBtn" onclick="clearDisplay(true)">Clear Displays</button>
            </div>
            <div>
                <script>
                    function setDisplay() {
                        location.href = location.origin + location.pathname + "/" + $("#selectDisplay").val() + "?compid=<?php echo $this->compObj->getID() ?>";
                    }
                </script>
                <select id="selectDisplay" onchange="setDisplay(); return false;">
                    <option value="">
                        Select as a Display Screen
                    </option>
                    <option value="0">
                        Left Screen
                    </option>
                    <option value="1">
                        Right Screen
                    </option>
                    <option value="2">
                        Single Screen
                    </option>
                </select>
            </div>

        </div>
        </body>
        <?php
    }

    private function _displayLapCounter() {
        ?>
        <head>
            <?php echo self::css(); ?>
        </head>
        <body class="lapDisplayBody">
        <script>
            var lapsToDisplay = <?php echo $this->screenId ?>;
            var numberToDisplay = <?php echo $this->screenId ?>;
            var screenId = <?php echo $this->screenId ?>;
            var globalTimeout = null;

            <?php echo socketClass::outputJavascript() ?>

            function getCompetition() {
                return {"id":<?php echo $this->compObj->getID() ?>}
            }

            function processSocketEvent(data) {
                if (typeof data.payload.lap !== "undefined") {
                    lapsToDisplay = data.payload.lap;
                }
                updateNumber();
            }

            function updateNumber() {
                if (globalTimeout !== null) {
                    clearTimeout(globalTimeout);
                }
                var element = document.getElementById("lapNumber");
                numberToDisplay = lapsToDisplay;
                if (numberToDisplay === -1) {
                    document.body.className = '';
                    document.body.style.backgroundColor = 'black';
                    element.style.display = 'none';
                } else {
                    document.body.className = 'lapDisplayBody';
                    element.style.display = '';
                    if (numberToDisplay < 10) {
                        if (screenId === 0) {
                            numberToDisplay = 0;
                        }
                    } else {
                        var numbers = "" + numberToDisplay;
                        if (screenId === 0) {
                            numberToDisplay = numbers[0];
                        } else if (screenId === 1) {
                            numberToDisplay = numbers[1];
                        }
                    }
                }
                if (element !== null) {
                    element.innerHTML = numberToDisplay;
                }
                globalTimeout = setTimeout(keepAlive, 1000);
            }

            function keepAlive() {
                e4sSocket.send("1", "");
            }

            updateNumber();
        </script>
        <div id="lapNumber" class="center lapNumber"></div>
        </body>
        <?php
    }
}