<?php

use function Ratchet\Client\connect;

include_once E4S_OTD_PATH . '/tfConstants.php';
function e4s_sendMessage($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID');
    $data = new stdClass();
    $data->title = checkFieldForXSS($obj, 'title:Title');
    $data->message = checkFieldForXSS($obj, 'message:The Content');
    e4s_sendSocketInfo($compId, $data, R4S_SOCKET_ENTRIES_MESSAGE);
}

function e4s_sendCommand($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID');
    $action = checkFieldForXSS($obj, 'action:Command Action');
    $payload = $obj->get_params('JSON');
    $compObj = e4s_getCompObj($compId);
    if ( !$compObj->isOrganiser()){
        Entry4UIError(9455, 'Sorry, you are not authorised to perform this action');
    }
    e4s_sendSocketInfo($compId, $payload, $action);
}

function e4s_sendSocketInfo($compId, $data, $type, $validateComp = true) {
    require_once E4S_FULL_PATH . 'dbInfo.php';
    $socket = new socketClass($compId, $validateComp);
    $socket->setPayload($data);
    $socket->sendMsg($type);
}

class socketClass {
    public $obj;
    public $enabled;

    public function __construct($compId, $validateComp = true) {
        $this->setHeader($compId, $validateComp);
    }

    public function setHeader($compId, $validateComp) {
		if ( $validateComp ) {
			$compObj       = e4s_getCompObj( $compId );
			$this->enabled = $compObj->socketsEnabled();
		}else{
			$this->enabled = true;
			$compObj = new stdClass();
			$compObj->id = $compId;
			$compObj->roster = true;
		}
        $socketObj = new stdClass();
        $socketObj->action = 'sendmessage';
        $compObj = new stdClass();
        $compObj->id = $compId;
        $socketObj->connectionId = $compId;
        $data = new stdClass();
        $data->key = '';
        $data->comp = $compObj;
        $data->action = '';
        $data->deviceKey = '';
        $data->securityKey = '';
        $data->payload = new stdClass();
        $socketObj->data = $data;
        $this->obj = $socketObj;
    }

    public function setPayload($payload) {
		if (!is_object($this->obj->data)) {
			$this->obj->data = new stdClass();
		}
		$this->obj->data->payload = $payload;
    }

    public function setKey($key) {
        $this->obj->data->key = $key;
    }

    public function sendMsg($action) {
//		error_log("Sending Socket Message: " . json_encode($this->obj->data->payload) );
        if ($this->enabled) {
            require_once E4S_FULL_PATH . 'vendor/autoload.php';
            $data = $this->obj->data;
            $data->domain = E4S_CURRENT_DOMAIN;
            $data->action = $action;
            $this->obj->data = e4s_getDataAsType($data, E4S_OPTIONS_STRING);
            connect(R4S_SOCKET_SERVER)->then(function ($conn) {
                $msg = e4s_getDataAsType($this->obj, E4S_OPTIONS_STRING);
                $conn->send($msg);
                $conn->close();
            }, function ($e) {
                echo "Could not connect: {$e->getMessage()}\n";
            });
        }
    }

    public static function outputJavascript() {
        echo '
        let E4S_SOCKET_INIT = "e4s_init";
        let socket_deviceKey = Math.random();
        var e4sSocket = {
            socket: null,
            timeout: null,
            init: function () {
                if ( typeof showPleaseWait !== "undefined" ){
                    showPleaseWait(true, "Getting Connection");
                }
                e4sSocket.socket = new WebSocket("' . R4S_SOCKET_SERVER . '");
                e4sSocket.socket.onclose = function () {
                };
                e4sSocket.socket.onopen = function () {
                    console.log("e4sSocket: Connection established, handle with function");
                };
                e4sSocket.socket.addEventListener("message", function (m) {                
                    var data = JSON.parse(m.data);
                    if ( data.message === "Internal server error"){
						e4sAlert("There has been a catastrophic error with the Web Socket server. Please reload or contact Entry4Sports","System Error");
						showPleaseWait(false);
						return;
					}else{
                        if (getCompetition().id === data.comp.id) {
	                        if (typeof data.payload === "string") {
	                            data.payload = JSON.parse(data.payload);
	                        }
	                        processSocketEvent(data);
	                    }
                    }
                });
                e4sSocket.setTimer();               
                window.setTimeout(e4sSocket.initPayload, 3000);
                if ( typeof showPleaseWait !== "undefined" ){
                    showPleaseWait(false);
                }
            },
            initPayload: function () {
                var payload = {
                }
                e4sSocket.send(payload,E4S_SOCKET_INIT);
            },
            send: function (payload, msgType) {
                var eventNo = 0;
                if ( typeof  getSelectedEventNo !== "undefined"){
                    getSelectedEventNo();
                }
                var compId = getCompetition().id;
                var socketObj = {};
                socketObj.action = "sendmessage";
                socketObj.connectionId = compId;
                socketObj.domain = "' . E4S_CURRENT_DOMAIN . '";
                socketObj.data = {};
                socketObj.data.key = eventNo;
                socketObj.data.comp = {};
                socketObj.data.comp.id = compId;
                socketObj.data.action = msgType;
                console.log("Sending Socket Message: " + msgType);
                socketObj.data.deviceKey = socket_deviceKey;
                socketObj.data.securityKey = "";
                socketObj.data.domain = "' . E4S_CURRENT_DOMAIN . '";
                socketObj.data.payload = payload;
                socketObj.data = JSON.stringify(socketObj.data);
                var socketPayloadStr = JSON.stringify(socketObj);
                e4sSocket.sendMessage(socketPayloadStr);
            },
            sendMessage: function(msg){
                // Wait until the state of the socket is not ready and send the message when it is...
                e4sSocket.waitForSocketConnection(function(){
//                    console.log("message sent!!!");
                    e4sSocket.socket.send(msg);
                });
			},

// Make the function wait until the connection is made...
			waitForSocketConnection: function(callback){
			let socket = e4sSocket.socket;
			    setTimeout(
			        function () {
			            if (socket.readyState === 1) {
			                if (callback != null){
			                    callback();
			                }
			            } else if (socket.readyState === 3) {
			                console.log("Connection is closed. Attempting to reconnect");
			                e4sSocket.resetSocket();
			            } else {
			                //
			       			console.log("wait for connection...");
			                e4sSocket.waitForSocketConnection(callback);
			            }
			
			        }, 500); // wait 50 millisecond for the connection...
			},
            sendRefreshRequiredMsg: function () {
                var compDate = new Date(getCompetition().date);
                var now = new Date();
                var payload = {
                    device: socket_deviceKey
                };
                var actionType = "' . R4S_SOCKET_REFRESH_REQUIRED . '";
                e4sSocket.send(payload, actionType);
                    
            },
            setTimer: function () {
                e4sSocket.clearTimer();
                e4sSocket.timeout = window.setInterval(e4sSocket.checkIfClosed, 3000);
            },
            clearTimer: function () {
                clearTimeout(e4sSocket.timeout);
            },
			checkIfClosed: function () {
                if (e4sSocket.socket.readyState === 3) {
                    // socket has closed;
                    e4sSocket.resetSocket();
                }
            },
            resetSocket: function () {
                try {
                    if (typeof e4sSocket.socket === "object") {
                        e4sSocket.socket.close();
                    }
                } catch (e) {
        
                }
                e4sSocket.socket = null;
                e4sSocket.init();
            },
            close: function () {
                try {
                    e4sSocket.socket.close();
                } catch (e) {
        
                }
            }
        };
        e4sSocket.init();
        ';
    }
}