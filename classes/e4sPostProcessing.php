<?php

class e4sPostProcessing {
    public $params;
    public $initCmd = 'ffmpeg -i ';
    public $overwrite = ' -y ';
    public $overlayCmd = ' -vf ';
    public $processDir = 'processed';
    public $originalDir = 'original';
    public $resultsDir = '';

    public function __construct($params) {
        $this->setVideoParams($params);
        $this->resultsDir = $_SERVER['DOCUMENT_ROOT'] . R4S_RESULT_DIR;
    }

    public function setVideoParams($params) {
        $this->_setDefaults();
        $videoParams = $this->params;
        $videoParams->comp = $params->comp;
        $videoParams->athlete = null;

        if (isset($params->text)) {
            $videoParams->text = $params->text;
        }
        if (isset($params->text2)) {
            $videoParams->text2 = $params->text2;
        }
        if (isset($params->text3)) {
            $videoParams->text3 = $params->text3;
        }
        if (!isset($params->text1) or $params->text1 === '') {
            // Comp name is the default for top line of text
            if (isset($params->comp->name) and $params->comp->name !== '') {
                $videoParams->text = $params->comp->name;
            }
        }

        if (isset($params->process) and $params->process === TRUE) {
            $videoParams->process = TRUE;
        }
//        if ( isset($params->output) ){
//            $videoParams->output = $params->output;
//        }

        if (isset($params->startClip)) {
            $videoParams->clip = TRUE;
            $videoParams->startClip = $params->startClip;

            if (isset($params->duration)) {
                $videoParams->duration = $params->duration;
            }
        }

        if (isset($params->reverse) and $params->reverse === TRUE) {
            $videoParams->reverse = TRUE;
        }

        if (isset($params->speed)) {
            $videoParams->speed = $params->speed;
        }

        $this->params = $videoParams;
    }

    private function _setDefaults() {
        $this->params = new stdClass();
        $this->params->process = FALSE;
        $this->params->startClip = 0.5;
        $this->params->duration = 0.5;
        $this->params->reverse = FALSE;
        $this->params->clip = TRUE;
        $this->params->output = E4S_VIDEO_MP4;
        $this->params->speed = 6.0;
        $this->params->text = '';
        $this->params->text2 = '';
        $this->params->text3 = '';
    }

    private function _getProcessedDir($fullFilePath) {
        $processedfullFilePath = $fullFilePath . '/' . $this->processDir;
        return $processedfullFilePath;
    }

    private function _setProcessedDir($fullFilePath) {
        $processedfullFilePath = $this->_getProcessedDir($fullFilePath);
        $this->_creatDir($processedfullFilePath);
        return $processedfullFilePath;
    }

    private function _saveOriginal($fullFilePath, $fileName): string {
        $time = date('h_i_s', time());
        $originalFullFilePath = $fullFilePath . '/' . $this->originalDir;
        $this->_creatDir($originalFullFilePath);

        $sourceFile = $fullFilePath . '/' . $fileName;
        $fileName = $time . '_' . $fileName;
        $cmd = 'cp ' . $sourceFile . ' ' . $originalFullFilePath . '/' . $fileName;

        $processedFullFilePath = $this->_getProcessedDir($fullFilePath);
        $this->_shellCmd($cmd, $processedFullFilePath);

        // rename file to have time prefix
        $cmd = 'mv ' . $sourceFile . ' ' . $fullFilePath . '/' . $fileName;
        $this->_shellCmd($cmd, $processedFullFilePath);

        return $fileName;
    }

    public static function isVideo($fileName) {
        $format = self::getInputFormat($fileName);
        if ($format === E4S_VIDEO_AVI or $format === E4S_VIDEO_MP4) {
            return TRUE;
        }
        return FALSE;
    }

    private function _getInputFormat($fileName) {
        return self::getInputFormat($fileName);
    }

    public static function getInputFormat($fileName) {
        $fileName = strtolower($fileName);
        $inputFormat = '';
        if (strpos($fileName, '.' . E4S_LYNX_RESULT_EXT) > 0) {
            $inputFormat = E4S_LYNX_RESULT_EXT;
        }
        if (strpos($fileName, '.' . E4S_TT_RESULT_EXT) > 0) {
            $inputFormat = E4S_TT_RESULT_EXT;
        }
        if (strpos($fileName, '.' . E4S_VIDEO_AVI) > 0) {
            $inputFormat = E4S_VIDEO_AVI;
        }
        if (strpos($fileName, '.' . E4S_VIDEO_MP4) > 0) {
            $inputFormat = E4S_VIDEO_MP4;
        }
        if (strpos($fileName, '.' . E4S_PICTURE_JPG) > 0) {
            $inputFormat = E4S_PICTURE_JPG;
        }
        if (strpos($fileName, '.' . E4S_PICTURE_PNG) > 0) {
            $inputFormat = E4S_PICTURE_PNG;
        }
        if ($inputFormat === '') {
            Entry4UIError(7025, 'Invalid input format : ' . $fileName);
        }
        return $inputFormat;
    }

    public function processFile($fullFilePath, $fileName) {
        if (self::isVideo($fileName)) {
            return $this->_processVideoFile($fullFilePath, $fileName);
        }
        return $this->_processImage($fullFilePath, $fileName);
    }

    private function _processImage($fullFilePath, $fileName) {
        $inputFormat = $this->_getInputFormat($fileName);
        $outputFormat = E4S_PICTURE_JPG;

        $processedFullFilePath = $this->_setProcessedDir($fullFilePath);
        $fileName = $this->_saveOriginal($fullFilePath, $fileName);

        $sourceFile = $fullFilePath . '/' . $fileName;
        $processedFile = $processedFullFilePath . '/' . $fileName;
        if ($inputFormat !== $outputFormat) {
            $arr = preg_split("~.{$inputFormat}~", $processedFile);
            $processedFile = $arr[0] . '.' . $outputFormat;
        }

        $overlay = ' "' . $this->_getBottomRightText() . '" ';
        $cmd = $this->initCmd . $sourceFile . $this->overlayCmd . $overlay . ' ' . $processedFile;
        $this->_shellCmd($cmd, $processedFullFilePath);
        $this->_moveFile($processedFile, $sourceFile);
        return $fileName;
    }

    private function _processVideoFile($fullFilePath, $fileName) {
        $videoParams = $this->params;
        if (!$videoParams->process) {
            return $fileName;
        }
        $processedFullFilePath = $this->_setProcessedDir($fullFilePath);

        $fileName = $this->_saveOriginal($fullFilePath, $fileName);

        $inputFormat = $this->_getInputFormat($fileName);

        $sourceFile = $fullFilePath . '/' . $fileName;
        $processedFile = $processedFullFilePath . '/' . $fileName;

        $initCmd = $this->initCmd;
        $overwrite = $this->overwrite;
        $overlay = '';
        if ($videoParams->text !== '') {
            $overlay = '"drawtext=text=\'' . $videoParams->text . '\':fontcolor=white:fontsize=24:box=1:boxcolor=black@0.5:boxborderw=5:x=(w-text_w)/2:y=text_h';
            if ($videoParams->text2 !== '') {
                $overlay .= ',drawtext=text=\'' . $videoParams->text2 . '\':fontcolor=white:fontsize=24:box=1:boxcolor=black@0.5:boxborderw=5:x=(w-text_w)/2:y=(text_h * 3)';
            }
        }
        if ($overlay !== '') {
            $overlay .= ',';
        } else {
            $overlay = '"';
        }
        $overlay .= $this->_getBottomRightText($videoParams->text3) . '"';
        $overlay = $this->overlayCmd . $overlay;

        if ($videoParams->clip) {
            $startClip = $this->_setVideoTime($videoParams->startClip);
            $duration = $this->_setVideoTime($videoParams->duration);
            $cmd = $initCmd . $sourceFile . $overwrite . $overlay . ' -ss ' . $startClip . ' -t ' . $duration . ' ' . $processedFile;
            $this->_shellCmd($cmd, $processedFullFilePath);
            $this->_moveFile($processedFile, $sourceFile);
            $overlay = ''; // text added
        }
        if ($videoParams->speed) {
            $cmd = $initCmd . $sourceFile . $overwrite . ' -filter:v "setpts=' . $videoParams->speed . '*PTS" ' . $overlay . $processedFile;
            $this->_shellCmd($cmd, $processedFullFilePath);
            $this->_moveFile($processedFile, $sourceFile);
            $overlay = ''; // text added
        }
        if ($videoParams->reverse) {
            $revTmpFile = $processedFullFilePath . '/reverse.avi';
            $cmd = $initCmd . $sourceFile . $overwrite . ' -vf reverse ' . $revTmpFile;
            $this->_shellCmd($cmd, $processedFullFilePath);
            $cmd = $initCmd . $revTmpFile . $overwrite . ' -filter:v "setpts=' . ($videoParams->speed * 2) . '*PTS" ' . $processedFile;
            $this->_shellCmd($cmd, $processedFullFilePath);
            $this->_moveFile($processedFile, $revTmpFile);
            $cmd = $initCmd . '"concat:' . $sourceFile . '|' . $revTmpFile . '" ' . $overwrite . $overlay . ' -codec copy ' . $processedFile;
            $this->_shellCmd($cmd, $processedFullFilePath);
            $this->_removeFile($revTmpFile);
            $this->_moveFile($processedFile, $sourceFile);
            $overlay = ''; // text added
        }
        if ($overlay !== '') {
            $overlay .= ' -codec:a copy ';
            $cmd = $initCmd . $sourceFile . $overwrite . $overlay . $processedFile;
            $this->_shellCmd($cmd, $processedFullFilePath);
            $this->_moveFile($processedFile, $sourceFile);
        }
        // add logo
        $cmd = $initCmd . $sourceFile . ' -i ' . $this->resultsDir . '/e4s_feather.gif' . $overwrite . ' -filter_complex "[0:v][1:v] overlay=2:2:enable=\'between(t,0,3)\'" -pix_fmt yuv420p -c:a copy ' . $processedFullFilePath . '/' . $fileName;
        $this->_shellCmd($cmd, $processedFullFilePath);
        $this->_moveFile($processedFile, $sourceFile);

        // now copy to other format
        $outputFileName = preg_replace("~.{$inputFormat}~", '.' . $videoParams->output, $sourceFile);
        $cmd = $initCmd . $sourceFile . $overwrite . ' ' . $outputFileName;
        $this->_shellCmd($cmd, $processedFullFilePath);

        return $fileName;
    }

    private function _getBottomRightText($text = '') {
        if ($text === '') {
			// get Current year
            $year = date('Y', time());
            $text = 'Entry4Sports ' . $year;
        }
        return "drawtext=text='" . $text . " ':fontcolor=white:fontsize=36:box=1:boxcolor=blue:boxborderw=20:x=w-text_w-20:y=h-text_h-30:fontfile=" . $this->resultsDir . '/rockwellbold.ttf';
    }

    private function _setVideoTime($val) {
        $init = '00:00:';
        if ($val >= 10) {
            return $init . $val;
        }

        return $init . '0' . $val;
    }

	private function _exec($cmd){
		exec($cmd,$output, $returnVar);
		if ($returnVar !== 0 ){
			Entry4UIError(9500,"Failed to issue cmd: $cmd");
		}

	}
    private function _shellCmd($cmd, $path) {
        $output = $path . '/output.txt';
        $logCmd = 'echo EXECUTE::' . $cmd . ' >> ' . $output;
        $this->_exec($logCmd);
        $cmd .= ' 2>> ' . $output;
//		e4s_dump($cmd, true);
	    $this->_exec($cmd);
    }

    private function _moveFile($tmpFile, $toFile) {
        $cmd = 'mv ' . $tmpFile . ' ' . $toFile;
	    $this->_exec($cmd);
    }

    private function _removeFile($file) {
        $cmd = 'rm ' . $file;
	    $this->_exec($cmd);
    }

    private function _creatDir($dir) {
        if (!file_exists($dir)) {
            mkdir($dir, 0777, TRUE);
        }
    }
}