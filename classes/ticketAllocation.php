<?php


class ticketAllocationClass {
    public $data;

    public function __construct($id = 0) {
        $allocationData = new stdClass();
        $allocationData->id = $id;
        $allocationData->name = '';
        $allocationData->address = '';
        $allocationData->telNo = '';
        $allocationData->email = '';
        $this->data = $allocationData;
        if ($id > 0) {
            $this->read();
        }
    }

    public function read($id = null) {
        if (is_null($id)) {
            $id = $this->data->id;
        }
        $sql = 'select *
                from ' . E4S_TABLE_TICKETALLOCATION . '
                where id = ' . $id;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            $row = $result->fetch_assoc();
            $this->data->id = (int)$row['id'];
            $this->data->name = $row['name'];
            $this->data->address = $row['address'];
            $this->data->telNo = e4s_ensureString($row['telNo']);
            $this->data->email = $row['email'];
        } else {
            $row = null;
        }
        return $row;
    }

    public function getFromRow($row) {
        $allocation = new stdClass();
        $allocation->id = (int)$row['allocationId'];
        $allocation->name = '';
        $allocation->address = '';
        $allocation->telNo = '';
        $allocation->email = '';

        if ($allocation->id > 0) {
            if (array_key_exists('name', $row)) {
                $allocation->name = $row['name'];
            }
            if (array_key_exists('address', $row)) {
                $allocation->address = $row['address'];
            }
            if (array_key_exists('telNo', $row)) {
                $allocation->telNo = $row['telNo'];
            }
            if (array_key_exists('email', $row)) {
                $allocation->email = $row['email'];
            }
        }
        $this->data = $allocation;
        return $allocation;
    }

    public function process($data) {
        if ($data->id !== 0) {
            $data = $this->update($data);
        } else {
            $data = $this->create($data);
        }
        $data->telNo = e4s_ensureString($data->telNo);
        return $data;
    }

    public function update($data) {
        if ($data->id === 0) {
            Entry4UIError(6520, 'Can not update a ticket Allocation without existing record identification');
        }
        $row = $this->read($data->id);
        if (is_null($row)) {
            Entry4UIError(6530, 'Can not find ticket Allocation with given record identification');
        }
        $info = '';
        if (isset($data->info)) {
            $info = $data->info;
        }
        $sql = 'update ' . E4S_TABLE_TICKETALLOCATION . "
                set name = '" . addslashes($data->name) . "',
                    address = '" . addslashes($data->address) . "',
                    telno = '" . $data->telNo . "',
                    email = '" . $data->email . "'
                where id = " . $data->id;
        $result = e4s_queryNoLog($sql);
        if ($result === FALSE) {
            Entry4UIError(6540, 'Failed to update ticket allocation record');
        }

        $this->data = $data;
        return $data;
    }

    public function create($data) {
        if ($data->id !== 0) {
            Entry4UIError(6500, 'Can not create a ticket Allocation with an existing record identification');
        }
        $info = '';
        if (isset($data->info)) {
            $info = $data->info;
        }
        $sql = 'insert into ' . E4S_TABLE_TICKETALLOCATION . " (name,address, telno, email, info )
                values ('" . addslashes($data->name) . "','" . addslashes($data->address) . "','" . $data->telNo . "','" . $data->email . "','" . $info . "')";
        $result = e4s_queryNoLog($sql);
        if ($result === FALSE) {
            Entry4UIError(6510, 'Failed to create a ticket allocation record');
        }
        $id = e4s_getLastID();
        $data->id = $id;
        $this->data = $data;
        return $data;
    }
}