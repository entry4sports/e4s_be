<?php
const E4S_EA_STAGE = 'stage';
const E4S_EA_LIVE = 'live';
const E4S_EA_REGISTERED = 'Registered';
const E4S_AAI_REGISTERED = 'ACTIVE';
//https://dev.entry4sports.co.uk/wp-json/e4s/v5/test?action=ea
class eaRegistrationClass {
    public $env;

    public function __construct($env = E4S_EA_STAGE) {
        $this->env = $env;
    }

    public function getURN($firstName, $surName, $dob) {
        if (is_null($firstName) or is_null($surName or is_null($dob))) {
            return null;
        }
        if ($firstName === '' or $surName === '' or $dob === '') {
            return null;
        }
        $url = 'individuals?firstname=' . trim($firstName) . '&lastname=' . trim($surName) . '&dob=' . $dob;
        $eaOutput = $this->_getEndPoint($url);
        if (is_null($eaOutput) or strpos($eaOutput, '!DOCTYPE') !== FALSE) {
            return null;
        }
        $eaAthlete = e4s_getDataAsType($eaOutput, E4S_OPTIONS_OBJECT);

        $urn = null;
        if ($eaAthlete->ResponseStatus === 'MatchFound') {
            $urn = $eaAthlete->IndividualRef;
        }
        return $urn;
    }

    public function validateURNForEventDate($urn, $eventDate = '2024-06-01') {
        if (is_null($eventDate)) {
            $eventDate = Date('Y-m-d');
        }
        if (gettype($eventDate) === 'string') {
            $eventDate = date_create($eventDate);
        }
        if (gettype($eventDate) !== 'object') {
            Entry4UIError(8035, 'Invalid date given');
        }
        if (is_null($urn)) {
            return null;
        }
        $urn = trim($urn);
        if ($urn === '') {
            return null;
        }

        $url = "individuals/{$urn}?eventDate=" . date_format($eventDate, 'd+F+Y');
        $eaOutput = $this->_getEndPoint($url);
        if (is_null($eaOutput)) {
            return null;
        }
        $eaAthlete = e4s_getDataAsType($eaOutput, E4S_OPTIONS_OBJECT);
        $e4sAthlete = $eaAthlete;
        if (is_null($eaAthlete) or !isset($eaAthlete->CompetitiveRegStatus)) {
            return null;
        }
        if (!is_null($eaAthlete->CompetitiveRegStatus)) {
            $e4sAthlete = new stdClass();
            $e4sAthlete->system = E4S_AOCODE_EA;
            $e4sAthlete->status = $eaAthlete->CompetitiveRegStatus;
            // dob will be dd/mm/yyyy so convert for our dates
            $dob = preg_split('~/~', $eaAthlete->Dob);
            $e4sAthlete->dob = $dob[2] . '-' . $dob[1] . '-' . $dob[0];
            $e4sAthlete->clubId = (int)$eaAthlete->FirstClaimClubId;
            $e4sAthlete->clubName = $eaAthlete->FirstClaimClubName;
            $e4sAthlete->club2Id = (int)$eaAthlete->SecondClaimClubId;
            $e4sAthlete->club2Name = $eaAthlete->SecondClaimClubName;
            $e4sAthlete->firstName = $eaAthlete->Firstname;
            $e4sAthlete->surName = $eaAthlete->Lastname;
            $e4sAthlete->gender = $eaAthlete->Gender[0];
            $e4sAthlete->urn = (int)$eaAthlete->Urn;
        }
        return $e4sAthlete;
    }

    private function _getEndPoint($endPoint) {

        // set time out on curl request

        $envObj = $this->_getActiveInfo();
        $url = $envObj->url . $endPoint;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $certificate = $envObj->certificate;
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        curl_setopt($ch, CURLOPT_CAINFO, $certificate);
        curl_setopt($ch, CURLOPT_CAPATH, $certificate);
        curl_setopt($ch, CURLOPT_SSLCERT, $certificate);
        curl_setopt($ch, CURLOPT_KEYPASSWD, $envObj->certificate_pwd);

        $headers = array('X-TRAPI-CALLKEY: ' . $envObj->user, 'X-TRAPI-CALLSECRET: ' . $envObj->pwd, 'X-TRAPI-USERPASSWORD: ' . $envObj->pwd, 'X-TRAPI-CALLDATETIME: ' . gmdate('Y-m-d\TH:i:s'),);
//  'date("Y-m-d\TH:i:s+0100")
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

//Execute the request.
        ob_start();
        $valid = curl_exec($ch);
        $content = ob_get_clean();
//Check for errors.
        if (curl_errno($ch)) {
            $content = null;
        }
        return $content;
    }

    private function _getActiveInfo() {
        if ($this->env === E4S_EA_LIVE) {
            return $this->_getLiveInfo();
        }
        return $this->_getStageInfo();
    }

    private function _getLiveInfo() {
        $obj = new stdClass();
//        $obj->url = "https://myathletics.uka.org.uk/TrinityAPI/TrinityAPIService.svc/race-provider/";
        $obj->url = 'https://TrinityAPI.myathletics.uk/TrinityAPIService.svc/race-provider/';
        $obj->certificate = './TrinityE4SLive.pem';
        $obj->certificate_pwd = 'Ent4R9348ev2';
        $obj->user = "E4Suser\$y";
        $obj->pwd = 'En3994)sp';
        return $obj;
    }

    private function _getStageInfo() {
        $obj = new stdClass();
//        $obj->url = "https://stagemyathletics.uka.org.uk/TrinityAPI/TrinityAPIService.svc/race-provider/";
        $obj->url = 'https://TrinityAPI.myathletics.uk/TrinityAPIService.svc/race-provider/';
        $obj->certificate = './trinitystage.pem';
        $obj->certificate_pwd = 'EvSTg@561760v2';
        $obj->user = 'eventprov2';
        $obj->pwd = '$33#(sport2';
        return $obj;
    }

    public function loadClubs() {
        $url = 'clubs';
        $eaOutput = $this->_getEndPoint($url);
        if (is_null($eaOutput)) {
            return FALSE;
        }
        $clubs = e4s_getDataAsType($eaOutput, E4S_OPTIONS_OBJECT);
        $clubs = $clubs->Clubs;
        $sql = array();
        foreach ($clubs as $club) {
            $text = '(' . $club->ClubId . ',';
            $text .= "'" . addslashes($club->ClubName) . "',";
            $text .= "'" . addslashes($club->Locality) . "',";
            $text .= "'" . addslashes($club->Region) . "')";
            $sql[] = $text;
        }
        e4s_queryNoLog('insert into ' . E4S_TABLE_EACLUBS . ' (externid,clubname,locality, region) values ' . implode(',', $sql));
        return TRUE;
    }

    public function _getRoles($urn) {
        $url = "individuals/{$urn}/roles";
    }

    private function _getAthlete($firstName, $surName, $dob) {
        $url = "individuals?firstname={$firstName}&lastname={$surName}&dob=27-july-1968";
    }
}