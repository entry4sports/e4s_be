<?php
const E4S_SEND_ONLY_FROM  = 'PROD';
const E4S_WARNING_PREFIX  = '!!! Entry4Sports : ';
const E4S_MESSAGE_DIVIDER = ',';
const E4S_MESSAGE_TO_COMP_PREFIX = '_';
const E4S_MESSAGE_TO_COMP_SUFFIX = E4S_MESSAGE_TO_COMP_PREFIX;
const E4S_MESSAGE_TO_ALL = E4S_MESSAGE_TO_COMP_PREFIX . 'ALL' . E4S_MESSAGE_TO_COMP_SUFFIX;
const E4S_MESSAGE_TO_COMP = E4S_MESSAGE_TO_COMP_PREFIX . 'COMP';
const E4S_MESSAGE_SEND_MAX = 50;
const E4S_SEND_NOW = 1;
const E4S_SEND_BATCH = 2;
const E4S_SEND_WOOCOMMERCE = E4S_SEND_NOW;
const E4S_UNMONITORED_EMAIL = 'entries@entry4sports.co.uk';
const E4S_UNMONITORED_EMAIL2 = 'entries@entry4sports.com';
const E4S_DEFAULT_FROM = 'Entries';
class e4SMessageClass {
    public $DO_NOT_SENDTO = [E4S_UNMONITORED_EMAIL2];
    public $DO_NOT_REPLYTO = [E4S_UNMONITORED_EMAIL, E4S_UNMONITORED_EMAIL2, E4S_DEFAULT_FROM];
    public int $limit = 1000;
    public $currentUser;
    public string $env;
    public string $testSendTo;
    public string $action;
    public bool $resend;
    public $userSent; // has the user sent a contact organiser
    public array $compsChecked; // used when processing entries ( cache )
    public $callWPMail; // set to false if class called from wpMail just to log the email

    public function __construct($forUserId = 0, $testSendTo = '') {
        $config = e4s_getConfig();
        $this->env = strtoupper($config['env']);
        $this->currentUser = new stdClass();
        $this->callWPMail = TRUE;
        $this->userSent = FALSE;
        if (array_key_exists(E4S_CRON_USER, $GLOBALS) and $GLOBALS[E4S_CRON_USER] === TRUE) {
//            echo "Cron";
            $this->_setCUid(1); // E4S Admin
        } else {
            if ($forUserId === 0) {
//                echo "get Current";
                $this->_setCUid(e4s_getUserID());
            } else {
//                echo "use passed";
                $this->_setCUid($forUserId); // E4S Admin
            }
        }
        $this->_getCurrentUser();
        if ($testSendTo === '') {
            $testSendTo = E4S_SUPPORT_EMAIL;
        }
        $this->testSendTo = $testSendTo;
        $this->action = '';
        $this->compsChecked = array();
        $this->resend = FALSE;
    }

    // This may be an entry or a team Entry object and include compId

    private function _setCUid($id) {
        $this->currentUser->id = $id;
    }

    // return true if a new message is created

    private function _getCurrentUser(): stdClass {
        $sql = 'select user_email email,
                       user_nicename userName,
                       user_login loginName,
                       display_name displayName
                from ' . E4S_TABLE_USERS . '
                where id = ' . $this->_getCUid();
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return new stdClass();
        }
        $obj = $result->fetch_object();
        $this->currentUser->email = $obj->email;
        $this->currentUser->userName = $obj->userName;
        $this->currentUser->loginName = $obj->loginName;
        $this->currentUser->displayName = $obj->displayName;
        return $this->currentUser;
    }

    private function _getCUid() {
        return $this->currentUser->id;
    }

    public function checkNewEntry($entryObj): bool {
        $compId = $entryObj->compId;
        if (array_key_exists($compId, $this->compsChecked)) {
            // already processed this comp
            return FALSE;
        }

        $this->compsChecked[$compId] = TRUE;
        return $this->_ensureMessageUserForCompId($compId);
    }

    private function _ensureMessageUserForCompId($compId): bool {
        $userId = e4s_getUserID();
        $sql = 'select m.id mId, mu.id muId
                from ' . E4S_TABLE_MESSAGE . ' m left join ' . E4S_TABLE_MESSAGEUSER . ' mu on m.id = mu.mailid and mu.userid = ' . $userId . "
                where sendTo = '" . $this->_setCompKey($compId) . "'";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return FALSE;
        }
        $obj = $result->fetch_object();
        if (is_null($obj->muId)) {
            // create mu record
            $sql = 'insert into ' . E4S_TABLE_MESSAGEUSER . ' ( mailid, userid )
                    values (
                    ' . $obj->mId . ',
                    ' . $userId . ' 
                    )';
            e4s_queryNoLog($sql);
            return TRUE;
        }
        return FALSE;
    }

    private function _setCompKey($compId) {
        return E4S_MESSAGE_TO_COMP . $compId . E4S_MESSAGE_TO_COMP_SUFFIX;
    }

    public function createMessage($type, $sendTo, $subject, $body) {
        $obj = $this->getDefaultObj();
        $obj->to = $sendTo;
        $this->_checkAuth($obj);
        $firstEntry = $sendTo[0];
        $subject = addslashes($subject);
        if ($firstEntry !== E4S_MESSAGE_TO_ALL) {
            $userInfoArr = $this->_getUserInfo($sendTo);

            if (sizeof($userInfoArr) === 0) {
                Entry4UIError(3420, 'Failed to find users');
            }
        } else {
            if ($type === E4S_MESSAGE_TYPE_EMAIL) {
                // dont send an email to all users !!!!
                Entry4UIError(7890, 'You can not send an email to ALL Users');
            }
            // Global messages do not add users here, but dynamic
            $userInfoArr = [];
        }
        if (!$this->resend) {
            if ($firstEntry !== E4S_MESSAGE_TO_ALL) {
                // get users not already sent to
                $userInfoArr = $this->_checkUsersOnExistingMessage($firstEntry, $userInfoArr, $subject, $body);
            } else {
                // does the global message already exist
                if ($this->_doesMsgExist(E4S_MESSAGE_TO_ALL, $subject, $body)) {
                    // clear out firstEntry so no processing is done
                    $firstEntry = '';
                }
            }
        }

        if (sizeof($userInfoArr) > 0 or $firstEntry === E4S_MESSAGE_TO_ALL) {
            switch ($type) {
                case  E4S_MESSAGE_TYPE_MSG:
                    $this->_buildMessage($firstEntry, $userInfoArr, $subject, $body);
                    break;
                case  E4S_MESSAGE_TYPE_EMAIL:
                    $this->_buildEmail($firstEntry, $userInfoArr, $subject, $body);
                    break;
            }
        }
    }

    public function getDefaultObj(): stdClass {
        $obj = new stdClass();
        $obj->process = '';
        $obj->id = 0;
        $obj->compId = 0;
        $obj->to = '';
        $obj->title = '';
        $obj->body = '';
        $obj->action = '';
        $get = new stdClass();
        $get->emails = TRUE;
        $get->messages = TRUE;
        $get->sent = null;
        $get->read = null;
        $get->deleted = null;
        $obj->get = $get;
        return $obj;
    }

    private function _checkAuth($passedObj) {
        $compId = $passedObj->compId;
        if ($passedObj->to !== '') {
            $sendTo = $passedObj->to;
            if (gettype($sendTo) === E4S_OPTIONS_ARRAY) {
                $sendTo = $sendTo[0];
            }
            if ($sendTo === E4S_MESSAGE_TO_ALL) {
                if (!isE4SUser()) {
                    Entry4UIError(8275, 'You are not authorised to perform this function.');
                }
            }
            $compId = $this->_getCompKey($sendTo);
        }
        if ($compId !== 0) {
            $compObj = e4s_GetCompObj($compId, TRUE, TRUE);
//            if (is_null($compObj->getRow())) {
//                Entry4UIError(8273, "Invalid Competition Information passed");
//            }
            // Must be an organiser to read comp messages
            if (!$compObj->isOrganiser()) {
                Entry4UIError(8274, 'You are not authorised to perform this function.');
            }
        }
    }

    private function _getCompKey($to) {
        $checkStr = $to;
        if (gettype($to) === 'array') {
            $checkStr = implode(',', $to);
        }
        if (stripos($checkStr, E4S_MESSAGE_TO_COMP) !== FALSE) {
            $compId = str_replace(E4S_MESSAGE_TO_COMP, '', $checkStr);
            $compId = str_replace(E4S_MESSAGE_TO_COMP_SUFFIX, '', $compId);
            return (int)$compId;
        }
        return 0;
    }

    // E4S replacement for wp_mail

    private function _getUserInfo($sendTo): array {
        if (is_array($sendTo)) {
            $checkSendTo = $sendTo[0];
        } else {
            $checkSendTo = $sendTo;
        }
        $compId = $this->_getCompKey($checkSendTo);
        if ($compId !== 0) {
            $userInfoArr = $this->_getCompetitionUserInfo($compId);
        } else {
            // $sendTo can be a mixture array of ids and or emails
            $emails = array();
            $ids = array();
            foreach ($sendTo as $to) {
                if (is_array($to)) {
                    $to = implode(',', $to);
                }
                if (strpos($to, '@') !== FALSE) {
                    $emails[] = $to;
                } elseif (is_numeric($to)) {
                    $ids[] = $to;
                }
            }
            $sql = 'select user_email, 
                           id userId
                from ' . E4S_TABLE_USERS . '
                where ';
            $or = '';
            if (sizeof($ids) > 0) {
                $sql .= 'id in (' . implode(',', $ids) . ')';
                $or = ' or ';
            }
            if (sizeof($emails) > 0) {
                $sql .= $or . "user_email in ('" . implode("','", $emails) . "')";
            }
            $result = e4s_queryNoLog($sql);
            $userInfoArr = array();
            if ($result->num_rows > 0) {
                while ($obj = $result->fetch_object()) {
                    $obj->athleteEmail = null;
                    $obj->athleteId = null;
                    $obj->userId = (int)$obj->userId;
                    $userInfoArr[$obj->user_email] = $obj;
                }
            }
            foreach ($emails as $email) {
                if (!array_key_exists($email, $userInfoArr)) {
                    // email not found so simply log the email and send it
                    // could put a different email in order to that of the user
                    $obj = $this->_getDefaultUserInfoObj();
                    $obj->user_email = $email;
                    if (is_array($obj->user_email)) {
                        $obj->user_email = $obj->user_email[0];
                    }
                    $userInfoArr[$obj->user_email] = $obj;
                }
            }
        }
        return $this->_returnNonAssociateArray($userInfoArr);
    }

    private function _getCompetitionUserInfo($compId): array {
        $compObj = e4s_GetCompObj($compId, TRUE, FALSE);
        if (is_null($compObj) ) {
            return [];
        }
        $orgs = $compObj->getOrganisers();
	    $userInfoArr = [];
	    if ($compObj->okToSendEmails()) {
		    $userInfoArr = $compObj->getEntryUserInfo();
	    }

        // add in organisers
//        check for no admin
        if (sizeof($orgs) > 0) {
            foreach ($userInfoArr as $userInfo) {
                foreach ($orgs as $sub => $org) {
                    if ($org->user->id === $userInfo->userId) {
                        array_splice($orgs, $sub, 1);
                        break;
                    }
                }
                if (sizeof($orgs) === 0) {
                    break;
                }
            }
        }
        // Those left need adding
        foreach ($orgs as $org) {
            if ($org->role->name === PERM_ADMIN) {
                $obj = $this->_getDefaultUserInfoObj();
                $obj->userId = $org->user->id;
                $obj->user_email = $org->user->userEmail;
                $userInfoArr[] = $obj;
            }
        }
        return $userInfoArr;
    }

    private function _getDefaultUserInfoObj() {
        $obj = new stdClass();
        $obj->user_email = null;
        $obj->userId = null;
        $obj->athleteEmail = null;
        $obj->athleteId = null;
        return $obj;
    }

    private function _returnNonAssociateArray($arr): array {
        $arr2 = array();
        foreach ($arr as $item) {
            $arr2[] = $item;
        }
        return $arr2;
    }

    private function _checkUsersOnExistingMessage($firstEntry, $userInfoArr, $subject, $body) {
        if ($firstEntry === E4S_MESSAGE_TO_ALL) {
            // global message already exists
            return array();
        }
        // check if the message already exists
        $body = base64_encode($body);
        $sql = 'select mu.userId,
                       u.user_email
                from ' . E4S_TABLE_MESSAGE . ' m,
                     ' . E4S_TABLE_MESSAGEUSER . ' mu,
                     ' . E4S_TABLE_USERS . " u
                where subject = '" . $subject . "'
                and   body = '" . $body . "'
                and   m.id = mu.mailid
                and   u.id = mu.userid";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            // message is a new one so get out
            return $userInfoArr;
        }

        $missingUserInfo = array();
        while ($obj = $result->fetch_object()) {
            $found = FALSE;
            foreach ($userInfoArr as $userInfo) {
                if ((int)$obj->userId === $userInfo->userId) {
                    $found = TRUE;
                } elseif ($this->_ignoreEmailAddress($obj->user_email)) {
                    $found = TRUE;
                }
                if ($found) {
                    break;
                }
            }
            if (!$found) {
                $missingUserInfo[] = $userInfo;
            }
        }

        return $missingUserInfo;
    }

    private function _ignoreEmailAddress($email) {
        // dont process entry4sports domains.
        if (stripos($email, E4S_LIVE_DOMAIN) === FALSE and stripos($email, E4S_OLDLIVE_DOMAIN) === FALSE) {
            return FALSE;
        }
        return TRUE;
    }

    private function _doesMsgExist($to, $subject, $body) {
        $body = base64_encode($body);
        $sql = 'select id
                from ' . E4S_TABLE_MESSAGE . " m
                where sendto = '" . $to . "'
                and   subject = '" . $subject . "'
                and   body = '" . $body . "'";
        $result = e4s_queryNoLog($sql);
        return $result->num_rows > 0;
    }

    private function _buildMessage($origTo, $sendTo, $subject, $body) {
        $this->_buildMessageAndAdd($origTo, $sendTo, $subject, $body, E4S_SEND_NOW, '', E4S_MESSAGE_TYPE_MSG);
    }

    private function _buildMessageAndAdd($origTo, $userInfoArr, $subject, $body, $priority, $from, $type) {
        // strip off the footer if its there

        $body = $this->_removeFooter($body);
        $obj = new stdClass();
        $obj->sendFrom = $from;
        $obj->origTo = $origTo;

        $obj->userInfoArr = $userInfoArr;

        $obj->subject = $subject;
        $obj->body = $body;
        $obj->priority = $priority;
        $obj->type = $type;

        return $this->_addMessage($obj);
    }

    private function _removeFooter($body) {
        $searchStr = '<br><br>Thanks for using <b>Entry4Sports</b>';
        if (strpos($body, $searchStr) !== FALSE) {
            $arr = preg_split('~' . $searchStr . '~', $body);
            $body = $arr[0];
        }
        return $body;
    }

    private function _addMessage($obj) {
        if ($this->_getCUid() <= E4S_USER_NOT_LOGGED_IN) {
//            Entry4UIError(9532, "Anonymous trying to send message");
        }
        if (gettype($obj->priority) === 'array') {
            // sent from WooCommerce
            $obj->priority = E4S_SEND_WOOCOMMERCE;
        }
        $sendTo = $obj->origTo;
        if ($sendTo === '') {
            $div = '';
            foreach ($obj->userInfoArr as $userInfo) {
                if (!is_null($userInfo->user_email) and $userInfo->user_email !== '') {
                    $sendTo .= $div . $userInfo->user_email;
                    $div = E4S_MESSAGE_DIVIDER;
                }
                if (!is_null($userInfo->athleteEmail) and $userInfo->athleteEmail !== '') {
                    $sendTo .= $div . $userInfo->athleteEmail;
                    $div = E4S_MESSAGE_DIVIDER;
                }
            }
        }
        $sendTo = addslashes($sendTo);
        $sendFrom = str_replace("\r\n", '', $obj->sendFrom);
        $sendFrom = addslashes($sendFrom);

        $sql = 'insert into ' . E4S_TABLE_MESSAGE . " ( type, sendfrom, sendto, subject, body, priority, userid)
                values(
                    '" . $obj->type . "',
                    '" . $sendFrom . "',
                    '" . $sendTo . "',
                    '" . addslashes($obj->subject) . "',
                    '" . base64_encode($obj->body) . "',
                    " . abs($obj->priority) . ',
                    ' . $this->_getCUid() . '
                )
                ';
        e4s_queryNoLog($sql);
        $obj->mailId = e4s_getLastID();

        // user info records
        $this->_addMessageUserInfo($obj);

        if ($obj->type === E4S_MESSAGE_TYPE_EMAIL) {
            if ($obj->priority === E4S_SEND_NOW) {
                return $this->sendEmail($obj->priority);
            }
        }

        return TRUE;
    }

    private function _addMessageUserInfo($obj) {
        $insertSql = 'insert into ' . E4S_TABLE_MESSAGEUSER . ' (mailid, userid, dateread)
                      values';
        $insertSep = '';
        foreach ($obj->userInfoArr as $userInfoObj) {
            $userId = $userInfoObj->userId;
            if (!is_null($userId) and $userId > 0) {
                $insertSql .= $insertSep . '(' . $obj->mailId . ',' . $userId . ',null)';
                $insertSep = ',';
            }
        }
        if ($this->userSent) {
            // create a user record and mark read
            $insertSql .= $insertSep . '(' . $obj->mailId . ',' . e4s_getUserID() . ",'" . Date('Y-m-d h:i:s') . "')";
            $insertSep = ',';
        }
        if ($insertSep !== '') {
            // At least 1 user found
            e4s_queryNoLog($insertSql);
        }

    }

    public function sendEmail($priority = E4S_SEND_NOW) {
        $sql = 'select *
                from ' . E4S_TABLE_MESSAGE . "
                where type = '" . E4S_MESSAGE_TYPE_EMAIL . "' 
                and dateSent is null ";
        if ($priority === E4S_SEND_BATCH) {
            $sql .= 'and priority = ' . E4S_SEND_BATCH . '
            order by priority, dateCreated';
        } else {
            $sql .= 'and priority = ' . $priority . '
                     and userid = ' . $this->_getCUid();
        }
        $result = e4s_queryNoLog($sql);
        $return = TRUE;
        while ($obj = $result->fetch_object()) {
            $obj->body = base64_decode($obj->body);
            $obj = $this->_checkDomain($obj);
            $body = $this->_stripeUnwantedHTML($obj->body);
            $body = $this->_replaceTags($body, TRUE);
            $body = $body . Entry4_emailFooter();
            $body .= $this->_addE4SMailCodeToFooter($obj);

            if ($priority === E4S_SEND_BATCH) {
                $sendToObjs = $this->_getUserInfo($obj->sendTo);
                $obj->sendTo = array();
                foreach ($sendToObjs as $sendToObj) {
                    $obj->sendTo[] = $sendToObj->user_email;
                    if (!is_null($sendToObj->athleteEmail)) {
                        $obj->sendTo[] = $sendToObj->athleteEmail;
                    }
                }
            }
            // send email
            $return = $this->_sendEmailWithSizeCheck($obj, $body);
            $this->_postSendEmail($obj);
        }
        return $return;
    }

    private function _checkDomain($obj) {
        $addBody = '';
        $newline = '<br>';
        if ($this->canEnvSend()) {
            $addBody .= $newline;
            $addBody .= $newline;
            $addBody .= '--------------------------------------------------------------------------------------';
            $addBody .= $newline;
            $addBody .= 'This Email has not been sent from a Production domain but from ' . E4S_CURRENT_DOMAIN . $newline;
            $addBody .= 'Please ignore any details in this email.' . $newline;
            if ($obj->sendTo !== '') {
                $addBody .= 'sendTo : ' . $obj->sendTo . $newline;
                $obj->sendTo = $this->testSendTo;
            }

            $addBody .= '--------------------------------------------------------------------------------------';
            $addBody .= $newline;
            $obj->body .= $addBody;
            $obj->subject = E4S_WARNING_PREFIX . $obj->subject;
        }
        return $obj;
    }

    public function canEnvSend() {
        return strtoupper($this->env) !== E4S_SEND_ONLY_FROM;
    }

    private function _stripeUnwantedHTML($body): string {
        if (strpos($body, '<body') === FALSE) {
            return $body;
        }
        $body = preg_split('~<body~', $body);
        $body = $body[1];
        $firstGT = strpos($body, '>');
        $body = substr($body, $firstGT + 1);
        $body = preg_split('~</body>~', $body);
        $body = $body[0];

        return $body;
    }

    private function _replaceTags($body, $forEmail): string {
        $E4S_USER = '{{E4SUSER}}';
        if ($forEmail) {
            $body = str_replace($E4S_USER, 'Entry4Sports User', $body);
        } else {
            $body = str_replace($E4S_USER, $this->_getCUName(), $body);
        }

        return $body;
    }

    private function _getCUName() {
        return $this->currentUser->displayName;
    }

    private function _addE4SMailCodeToFooter($obj): string {
        return $this->_emailFooter($obj) . '<br>{E4S-' . $obj->id . '-' . rand(1234, 5432) . '}';
    }

    private function _emailFooter($obj) {
        $body = '';
        foreach ($this->DO_NOT_REPLYTO as $doNoReplyTo) {
            if (strcasecmp($obj->sendFrom, $doNoReplyTo) === 0) {
                $body = '<br><br>Please do not reply to this message; it was sent from an unmonitored email address. This message is a service email related to your use of Entry4Sports.';
                $body .= '<br>If you have a question regarding a competition, please use the Contact Organiser function. If you have a technical question, email support@entry4sports.com';
                break;
            }
        }

        return $body;
    }

    private function _sendEmailWithSizeCheck($obj, $body) {
        $sendToArray = array();
        $return = TRUE;
        $sendField = $obj->sendTo;
        if ($sendField !== '') {
            if (gettype($sendField) === E4S_OPTIONS_STRING) {
                $sendField = explode(E4S_MESSAGE_DIVIDER, $sendField);
            }
            $bccCnt = sizeof($sendField);
            $emailCnt = 0;

            while ($emailCnt < $bccCnt) {
                $sendToArray[] = $sendField[$emailCnt];
                $emailCnt++;
                if (sizeof($sendToArray) >= E4S_MESSAGE_SEND_MAX) {
                    $return = $return & $this->_sendEmail($obj, $sendToArray, $body);
                    $sendToArray = array();
                }
            }
        }
        if (sizeof($sendToArray) > 0 or $sendField === '') {
            $return = $return & $this->_sendEmail($obj, $sendToArray, $body);
        }
        return $return;
    }

    private function _sendEmail($obj, $sendToArray, $body): bool {
        if (!$this->callWPMail) {
            // obj called from wpMail post event in functions, so dont actually send again ( recursive )
            return TRUE;
        }
        $headers = array('Content-Type: text/html; charset=UTF-8');
        if ($obj->sendFrom !== '') {
	        $sendFromName = "";
	        $sendFromEmail = $obj->sendFrom;
	        if (strpos($sendFromEmail, '<') > 0) {
		        $sendFromName = preg_split('~<~', $sendFromEmail)[0];
		        $sendFromEmail = preg_split('~<~', $sendFromEmail)[1];
	        }
	        if (strpos($sendFromEmail, '>') > 0) {
		        $sendFromEmail = preg_split('~>~', $sendFromEmail)[0];
	        }
            if (strpos($sendFromEmail, '@') > 0) {
//	            $headers[] = 'Reply-to: ' . $sendFromName . ' <' . $sendFromEmail . '>';
            }

            $headers[] = 'From: Entries <entries@' . E4S_UK_DOMAIN . '>';
        }

        if (sizeof($sendToArray) > 1) {
            foreach ($sendToArray as $sendTo) {
                if ($sendTo !== $obj->sendTo) {
                    $headers[] = 'Bcc:' . $sendTo;
                }
            }
            $obj->sendTo = E4S_UNMONITORED_EMAIL;
        } else {
            $obj->sendTo = $sendToArray[0];
        }
        foreach ($this->DO_NOT_SENDTO as $doNotSend) {
            if (strcasecmp($doNotSend, $obj->sendTo) === 0) {
                return TRUE;
            }
        }
        $body = e4s_htmlToUI(e4s_htmlFromUI($body), FALSE);
        $body = str_replace(E4S_HTML_OPEN, '<', $body);
        $body = str_replace(E4S_HTML_CLOSE, '>', $body);

        return wp_mail($obj->sendTo, $obj->subject, $body, $headers);
    }

//    private function _removeAllButCurrent($value):string{
//        if ( $value === "" ){
//            return "";
//        }
//        if ( stripos($value,$this->email) !== false ){
//            return $this->email;
//        }
//        if  ($value === E4S_MESSAGE_TO_ALL ){
//            return $this->email;
//        }
//        return "";
//    }

    private function _postSendEmail($obj) {
        $updateSql = '
                update ' . E4S_TABLE_MESSAGE . '
                set dateSent = current_timestamp
                where id = ' . $obj->id;
        e4s_queryNoLog($updateSql);
    }

//    Private functions

    private function _buildEmail($origTo, $sendTo, $subject, $body, $priority = E4S_SEND_BATCH, $from = E4S_DEFAULT_FROM) {
        return $this->_buildMessageAndAdd($origTo, $sendTo, $subject, $body, $priority, $from, E4S_MESSAGE_TYPE_EMAIL);
    }

    public function mail($sendTo, $subject, $body, $priority = E4S_SEND_BATCH, $from = 'entries'): bool {
        return $this->_buildEmail($sendTo, $subject, $body, $priority, $from);
    }

    public function resendEmail($id) {
        $sql = 'select *
                from ' . E4S_TABLE_MESSAGE . '
                where id =' . $id;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(3456, 'Invalid Email to resend');
        }
        $obj = $result->fetch_object();
        $this->_addMessage($obj);
        Entry4UISuccess();
    }

    public function markRead($id, $state = TRUE): stdClass {
        return $this->_markUserDoc($id, 'dateRead', $state);
    }

    private function _markUserDoc($id, $field, $state): stdClass {
        $row = $this->getMessage($id);

        if (is_null($row)) {
            return new stdClass();
        }
        if (sizeof($row->forUsers) !== 1) {
            Entry4UIError(7683, 'Invalid Messages found');
        }
        $muId = $this->_getMessageUserIdFromRow($row);
        $this->_updateMessageUserDoc($muId, $field, $state);
        return e4s_getMessageRow('id', $id, TRUE, $this->action);
    }

    public function getMessage($id) {
        $obj = new stdClass();
        $obj->id = $id;
        $rows = $this->getMessages($obj);

        if (sizeof($rows) !== 1) {
            return null;
        }
        return $rows[0];
    }

    public function getMessages($passedObj): array {
        $obj = e4s_mergeInOptions($this->getDefaultObj(), $passedObj);

        if (is_null($obj->compId)) {
            $obj->compId = 0;
        }
        if (is_null($obj->id)) {
            $obj->id = 0;
        }
        $this->_checkAuth($obj);
        $arr = array();
        if ($this->_getCUid() <= E4S_USER_NOT_LOGGED_IN) {
            return $arr;
        }
        $sortkey = 'dateCreated';
        $sortorder = 'desc';
        $startswith = '';
        $pagesize = 25;
        $page = 1;
        if (isset($obj->pageInfo)) {
            $startswith = $obj->pageInfo->startswith;
            $pagesize = $obj->pageInfo->pagesize;
            $page = $obj->pageInfo->page;
            $sortkey = $obj->pageInfo->sortkey;
            if ($sortkey !== 'id') {
                $sortorder = $obj->pageInfo->sortorder;
            }
        }

        $getSingleRecord = FALSE;
        // has an id been requested and not getting in order to update record
        if ($obj->id > 0) {
            $getSingleRecord = TRUE;
        }
        $sql = $this->_getAllMessagesSQL($obj);

        if (!is_null($startswith) and $startswith !== '') {
            $sql .= " and subject like '%" . $startswith . "%'";
        }
        if (!$getSingleRecord) {
            $sql .= ' order by ' . $sortkey . ' ' . $sortorder;

            if (isset($pagesize) and isset($page) and $pagesize !== 0) {
                $sql .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
            }
        }
        $msgResult = e4s_queryNoLog($sql);

        while ($msgObj = $msgResult->fetch_object()) {
            $msgObj = $this->_normaliseObj($msgObj, $getSingleRecord);
            if (array_key_exists($msgObj->id, $arr)) {
                $arr[$msgObj->id]->forUsers[] = $msgObj->forUsers[0];
            } else {
                $arr[$msgObj->id] = $msgObj;
            }
        }

        return $this->_returnNonAssociateArray($arr);
    }

    private function _getAllMessagesSQL($obj): string {
        $email = $this->_getCUEmail();
        $userId = $this->_getCUid();
        $isE4SUser = isE4SUser();
        $sql = "select  m.*,
                    date_format(m.dateSent,'" . ISO_MYSQL_DATE . "') dateSentISO,
                    mu.id muId,
                    mu.userid forUserId,
                    muu.user_nicename forUserNicename,
                    muu.user_email forUserEmail,";
        $sql .= $this->_getMessageUserSQLFieldSelect();
        $sql .= ",                    
                    date_format(m.dateCreated,'" . ISO_MYSQL_DATE . "') dateCreatedISO,
                    u.user_nicename, 
                    u.user_email                    
            from " . E4S_TABLE_MESSAGE . ' m 
                    left join ' . E4S_TABLE_MESSAGEUSER . ' mu on m.id = mu.mailid and mu.userid = ' . $userId . '
                    left join ' . E4S_TABLE_USERS . ' u on m.userid = u.id
                    left join ' . E4S_TABLE_USERS . ' muu on mu.userid = muu.id
            where ';
        if ($obj->compId !== 0) {
            $sql .= "m.sendTo = '" . $this->_setCompKey($obj->compId) . "'";
        } else {
            if ($obj->id === 0) {
                $sql .= "
                        (
                            m.sendTo like '%" . $email . "%'
                            or
                            m.sendTo = '" . E4S_MESSAGE_TO_ALL . "'
                            or
                            m.sendfrom ='" . $email . "'
                            or
                            (m.sendTo like '" . E4S_MESSAGE_TO_COMP . "%'";
                if (!$isE4SUser) {
                    $sql .= ' and (mu.userid = ' . $userId . ')';
                }
                $sql .= ' )
                        )
                ';


                if (!is_null($obj->get->read)) {
                    $sql .= ' and dateRead is';
                    if ($obj->get->read) {
                        $sql .= ' not ';
                    }
                    $sql .= ' null ';
                }
                if (!is_null($obj->get->deleted)) {
                    $sql .= ' and dateDeleted is';
                    if ($obj->get->deleted) {
                        $sql .= ' not ';
                    }
                    $sql .= ' null ';
                }
                if (!is_null($obj->get->sent)) {
                    $sql .= ' and dateSent is';
                    if ($obj->get->sent) {
                        $sql .= ' not ';
                    }
                    $sql .= ' null ';
                }

            } else {
                $sql .= ' m.id = ' . $obj->id . ' ';
            }

        }
        if ($obj->id === 0) {
            if (!$obj->get->emails or !$obj->get->messages) {
                // only want one type
                $sql .= " and m.type = '";
                if ($obj->get->emails) {
                    $sql .= E4S_MESSAGE_TYPE_EMAIL;
                }
                if ($obj->get->messages) {
                    $sql .= E4S_MESSAGE_TYPE_MSG;
                }
                $sql .= "' ";
            }
        }

        return $sql;
    }

    private function _getCUEmail() {
        return $this->currentUser->email;
    }

    private function _getMessageUserSQLFieldSelect(): string {
        return "mu.dateRead dateRead,
                    date_format(mu.dateRead,'" . ISO_MYSQL_DATE . "') dateReadISO,
                    mu.dateDeleted dateDeleted,       
                    date_format(mu.dateDeleted,'" . ISO_MYSQL_DATE . "') dateDeletedISO";
    }

    private function _normaliseObj($obj, $singleRecord = FALSE) {
        $user = new stdClass();
        $user->id = $obj->userid;
        $comp = new stdClass();
        $comp->id = $this->_getCompKey($obj->sendTo);
        $comp->name = '';
        $curEmail = strtolower($this->_getCUEmail());
        $compObj = null;
        if ($comp->id > 0) {
            $compObj = e4s_GetCompObj($comp->id, TRUE, FALSE);
            if (!is_null($compObj)) {
                $comp->name = $compObj->getName();
            }
        }
        $obj->competition = $comp;

        if ($obj->sendTo === E4S_MESSAGE_TO_ALL) {
            $user->name = 'E4S System';
        } else {
            if (!is_null($compObj)) {
                $user->name = $compObj->getFormattedName();
            } else {
                $user->name = $obj->user_nicename;
            }
        }
        if ($obj->sendFrom !== '') {
            $obj->sendFrom = $this->_formatEmail($obj->sendFrom);
        }
        $user->email = $obj->user_email;
        $obj->user = $user;
        $obj->body = $this->_replaceTags(base64_decode($obj->body), FALSE);
        $obj->body = str_replace('`nbsp;', ' ', $obj->body);
        $obj->body = str_replace('`amp;', '&', $obj->body);
        $obj->body = str_replace('&rsquo;', "'", $obj->body);
        unset($obj->userid);
        unset($obj->user_nicename);
        unset($obj->user_email);

        if (is_null($obj->error)) {
            $obj->error = '';
        }

        $muObj = null;
        if ($singleRecord) {
            if (is_null($obj->muId)) {
                //create mu record
                $muObj = $this->_createMessageUser($obj->id, $this->_getCUid(), TRUE);
            } else {
                if ($this->action === '' and is_null($obj->dateRead)) {
                    // update read
                    $muObj = $this->_updateMessageUserRead($obj->muId);
                }
            }
            if (!is_null($muObj)) {
                $obj->muId = $muObj->id;
                $obj->dateRead = $muObj->dateRead;
                $obj->dateReadISO = $muObj->dateReadISO;
                $obj->dateDeleted = $muObj->dateDeleted;
                $obj->dateDeletedISO = $muObj->dateDeletedISO;
            }
        }

        if (is_null($obj->dateSent) or $obj->dateSent === '') {
            $obj->dateSent = '';
            $obj->dateSentISO = '';
        } else {
            $obj->dateSentISO .= e4s_getOffset($obj->dateSent);
        }
        if (is_null($obj->dateRead) or $obj->dateRead === '') {
            $obj->dateRead = '';
            $obj->dateReadISO = '';
        } else {
            $obj->dateReadISO .= e4s_getOffset($obj->dateRead);
        }
        if (is_null($obj->dateDeleted) or $obj->dateDeleted === '') {
            $obj->dateDeleted = '';
            $obj->dateDeletedISO = '';
        } else {
            $obj->dateDeletedISO .= e4s_getOffset($obj->dateDeleted);
        }
        $obj->dateCreatedISO .= e4s_getOffset($obj->dateCreated);
        // only if sent to specific people and you need to hide the others.
        if ($obj->sendTo !== E4S_MESSAGE_TO_ALL and strpos($obj->sendTo, E4S_MESSAGE_TO_COMP) === FALSE and strtolower($obj->sendFrom) !== $curEmail) {
            $obj->sendTo = $this->_formatEmail($curEmail);
        }
        $body = e4s_decodeHTML($obj->body);
        if (!$singleRecord) {
            $body = strip_tags($body);
            $obj->body = $body;
            if ($body !== '' and strlen($body) > 100) {
                $body = str_replace("\n\n", '.', $body);
                $body = str_replace("\t\t", '.', $body);
                $body = str_replace("\n", '', $body);
                $body = str_replace("\t", '', $body);
                $body = str_replace('..', ' ', $body);
                $body = str_replace('  ', '', $body);
                $body = str_replace("\r", '', $body);
                $body = trim($body);
                $obj->body = substr($body, 0, 97) . '...';
            }
        } else {
            $obj->body = $body;
        }

        // MessageUser section
        $forUserObj = new stdClass();
        $forUserObj->muId = $obj->muId;
        $forUserObj->id = $obj->forUserId;
        $forUserObj->email = $obj->forUserEmail;
        $forUserObj->name = $obj->forUserNicename;
        $msg = new stdClass();
        $msg->dateDeleted = $obj->dateDeleted;
        $msg->dateDeletedISO = $obj->dateDeletedISO;
        $msg->dateRead = $obj->dateRead;
        $msg->dateReadISO = $obj->dateReadISO;
        $forUserObj->status = $msg;

        $obj->forUsers = [$forUserObj];
        unset($obj->muId);
        unset($obj->forUserId);
        unset($obj->forUserEmail);
        unset($obj->forUserNicename);
        unset($obj->dateDeleted);
        unset($obj->dateDeletedISO);
        unset($obj->dateRead);
        unset($obj->dateReadISO);
        return $obj;
    }

    private function _formatEmail($email): string {
        if (strpos($email, '<') !== FALSE) {
            $arr = preg_split('~<~', $email);
            $email = trim($arr[0]);
        }
        return $email;
    }

    private function _createMessageUser($mailId, $userId, $markRead = FALSE) {
        $sql = 'insert into ' . E4S_TABLE_MESSAGEUSER . ' (mailid, userid';
        if ($markRead) {
            $sql .= ', dateRead ';
        }
        $sql .= ')
                values (
                    ' . $mailId . ',
                    ' . $userId;
        if ($markRead) {
            $sql .= ', CURRENT_TIMESTAMP() ';
        }
        $sql .= ')';
        e4s_queryNoLog($sql);

        $muId = e4s_getLastID();
        return $this->_getMessageUserObj($muId);
    }

    private function _getMessageUserObj($muId) {
        $sql = 'select id,';
        $sql .= $this->_getMessageUserSQLFieldSelect();
        $sql .= ' from ' . E4S_TABLE_MESSAGEUSER . ' mu
                  where id = ' . $muId;
        $result = e4s_queryNoLog($sql);
        return $result->fetch_object();
    }

    private function _updateMessageUserRead($muId) {
        $this->_updateMessageUserDoc($muId, 'dateRead', TRUE);
        return $this->_getMessageUserObj($muId);
    }

    private function _updateMessageUserDoc($id, $field, $state) {
        $value = 'CURRENT_TIMESTAMP()';
        if (!$state) {
            $value = 'null';
        }
        $sql = '
            update ' . E4S_TABLE_MESSAGEUSER . '
            set ' . $field . ' = ' . $value . '
            where id = ' . $id;

        e4s_queryNoLog($sql);
    }

    private function _getMessageUserIdFromRow($row) {
        return (int)$row->forUsers[0]->muId;
    }

    public function markDeleted($id, $state = TRUE): stdClass {
        return $this->_markUserDoc($id, 'dateDeleted', $state);
    }

    public function errorReceived($wpError) {
        require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
        require_once E4S_FULL_PATH . 'dbInfo.php';
        $errorCode = $wpError->get_error_code();
        $smtpError = $wpError->get_error_messages($errorCode)[0];
        $data = $wpError->get_error_data($errorCode)['message'];
        $mailId = $this->_extractE4SMailCodeFromMessage($data);
        if ($mailId > 0) {
            $updateSql = '
            update ' . E4S_TABLE_MESSAGE . "
            set error = '" . $smtpError . "'
            where id = " . $mailId;
            e4s_queryNoLog($updateSql);
        } else {
            Entry4UISuccess($wpError);
        }
    }

    private function _extractE4SMailCodeFromMessage($message) {
        $id = 0;
        if (strpos($message, '{E4S') !== FALSE) {
            $arr = preg_split('~\{E4S~', $message);
            $code = $arr[1];
            $id = preg_split('~-~', $code)[1];
        }
        return $id;
    }

    public function postWPMail($sendTo, $subject, $body, $from) {
        $this->callWPMail = FALSE; // Do Not actually Send, just log in messages
        $headers = array();
        if ($from !== '') {
            if (strpos($from, '@') > 0) {
                $headers[] = 'From: ' . $from . ' <' . $from . '>';
            } else {
                $headers[] = 'From: ' . $from . ' <' . $from . '@' . E4S_CURRENT_DOMAIN . '>';
            }
        }
        return $this->wp_mail($sendTo, $subject, $body, $headers, E4S_SEND_NOW);
    }

    public function wp_mail($sendTo, $subject, $body, $headers, $priority = E4S_SEND_BATCH) {
        $from = '';
        $val = '';
        switch (gettype($headers)) {
            case 'array':
                foreach ($headers as $header) {
                    $split = preg_split('~:~', $header);
                    switch (strtolower($split[0])) {
                        case 'from':
                            $val = $split[1];
                            $val = preg_split('~<~', $val);
                            $val = $val[0];
                            if ($from !== '') {
                                $from .= E4S_MESSAGE_DIVIDER;
                            }
                            $from .= trim($val);
                            break;
                        case 'reply-to':
                            $val = $split[1];
                            if (strpos($val, '<')) {
                                $val = preg_split('~<~', $val);
                                $val = $val[1];
                                $val = preg_split('~>~', $val);
                                $val = $val[0];
                            }
                            if ($from !== '') {
                                $from .= E4S_MESSAGE_DIVIDER;
                            }
                            $from .= trim($val);
                            $this->userSent = TRUE;
                            break;
//                        case 'bcc':
//                            if ($bcc !== "") {
//                                $bcc .= E4S_MESSAGE_DIVIDER;
//                            }
//                            $bcc .= $val;
//                            break;
                    }
                }
                break;
            case 'string':
                // from WooCommerce
                $temp = preg_split('~Reply-to: ~', $headers);
                $from = $temp[1];
                break;
        }
        if (gettype($sendTo) !== 'array') {
            $sendTo = [$sendTo];
        }
        $userInfoArr = $this->_getUserInfo($sendTo);

        return $this->_buildEmail('', $userInfoArr, $subject, $body, $priority, $from);
    }
}