<?php


class uomClass {
    public $uomRows;

    public function __construct() {
        $this->_getUOMRows();
    }

    private function _getUOMRows() {
        $sql = 'select *
                from ' . E4S_TABLE_UOM;

        $result = e4s_queryNoLog($sql);
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $this->uomRows = array();
        foreach ($rows as $row) {
            $row = $this->getFormattedRow($row);
            $this->uomRows [$row['id']] = $row;
        }
    }

    public function getFormattedRow($row) {
        $row['options'] = e4s_addDefaultUOMOptions($row['options']);
        return $row;
    }
}