<?php
require_once E4S_FULL_PATH . 'e4s_constants.php';
const E4S_COMP_REPORT_1 = 1;
class compReportClass {
    public $compId;
    public $records;

    public function __construct($compId) {
        $this->compId = $compId;
        $this->records = $this->_getRecords();
    }

    private function _getRecords() {
        $records = array();
        $sql = 'select id,
                       compId,
                       reportType,
                       lastOrderId,
                       data
                from ' . E4S_TABLE_COMP_REPORT . '
                where compid = ' . $this->compId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows > 0) {
            while ($obj = $result->fetch_object()) {
                $obj->id = (int)$obj->id;
                $obj->compId = (int)$obj->compId;
                $obj->lastOrderId = (int)$obj->lastOrderId;
                $obj->data = e4s_getDataAsType($obj->data, E4S_OPTIONS_OBJECT);
                $records[$obj->reportType] = $obj;
            }
        }
        return $records;
    }

    public function getData($reportType, $writeRecord = FALSE) {
        $report = null;
        if ($writeRecord) {
            $this->_generateAndWriteRecord($reportType);
            $this->records = $this->_getRecords();
            $report = $this->records($reportType);
        } else {
            if (array_key_exists($reportType, $this->records)) {
                $report = $this->records($reportType);
            }
        }
        return $report;
    }

    private function _generateAndWriteRecord($reportType) {
        $obj = new stdClass();
        $obj->id = 0;
        $obj->compId = $this->compId;
        $obj->reportType = $reportType;
        switch ($reportType) {
            case E4S_COMP_REPORT_1:
                $reportObj = new compReport1Class($obj);
                $obj = $reportObj->generateReportData();
                break;
        }
        if (isset($obj->data)) {
            $this->_writeRecord($obj);
        }
    }

    private function _writeRecord($obj) {
        $sql = 'insert into ' . E4S_TABLE_COMP_REPORT . ' (compId, reportType, data, lastOrderId ) values (' . $obj->compId . ',' . $obj->reportType . ',' . "'" . e4s_getDataAsType($obj->data, E4S_OPTIONS_STRING) . "'," . $obj->lastOrderId . ')';
        if ($obj->id > 0) {
            $sql = 'update ' . E4S_TABLE_COMP_REPORT . "
                    set data = '" . e4s_getDataAsType($obj->data, E4S_OPTIONS_STRING) . "',
                        lastOrderId = " . $obj->lastOrderId;
        }
        e4s_queryNoLog($sql);
    }

    private function _getActualLastOrderId(): int {
        $sql = 'select max(orderid) orderId
                from Entry4_uk_Entries e,
                     Entry4_uk_CompEvents ce
                where ce.id = e.compEventID
                and ce.CompID = ' . $this->compId . '
                and paid = ' . E4S_ENTRY_PAID . '
                union
                select max(orderid)
                from Entry4_uk_EventTeamEntries e,
                     Entry4_uk_CompEvents ce
                where ce.id = e.ceid
                and ce.CompID = ' . $this->compId . '
                and paid = ' . E4S_ENTRY_PAID . '
                order by orderid desc';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return 0;
        } else {
            $firstObj = $result->fetch_object();
            return (int)$firstObj->orderId;
        }
    }
}

class compReport1Class {
    public $returnObj = null;

    public function __construct($obj) {
        $this->returnObj = $obj;
    }

    public function generateReportData() {
        $this->returnObj->data = '{}';
        $this->returnObj->lastOrderId = 1;
        return $this->returnObj;
    }
}

$sql = 'select id compId,
                   name competition,
                   date compDate
            from ' . E4S_TABLE_COMPETITON . '
            order by id desc';
$result = e4s_queryNoLog($sql);
$comps = array();
while ($compObj = $result->fetch_object()) {
    $compObj->compId = (int)$compObj->compId;
    $compObj->value = 0;
    $compObj->refunded = 0;
    $comps [$compObj->compId] = $compObj;
}

$sql = 'select id,
                   compId
            from ' . E4S_TABLE_COMPEVENTS;
$ceRecords = array();
$result = e4s_queryNoLog($sql);
while ($obj = $result->fetch_object()) {
    $ceRecords[(int)$obj->id] = (int)$obj->compId;
}

$itemSql = 'select  wci.order_id orderId,
                        wci.order_item_id orderItemId,
                        wci.order_item_name orderItemName,
                        wcim.meta_value liveValue,
                        p.post_status orderStatus
                from ' . E4S_TABLE_WCORDERITEMS . ' wci,
                     ' . E4S_TABLE_WCORDERITEMMETA . ' wcim,
                     ' . E4S_TABLE_POSTS . " p
                where order_item_name like '% : %'
                and p.id = wci.order_id
                and p.post_status in( '" . WC_ORDER_PAID . "', 'wc-refunded')
                and wci.order_item_id = wcim.order_item_id
                and wcim.meta_key = '_line_total'";
$result = e4s_queryNoLog($itemSql);

while ($obj = $result->fetch_object()) {
    $obj->orderId = (int)$obj->orderId;
    $obj->orderItemId = (int)$obj->orderItemId;
    $obj->liveValue = (float)$obj->liveValue;
    $nameArr = preg_split('~ :~', $obj->orderItemName);
    $ceId = (int)$nameArr[0];
    if (!array_key_exists($ceId, $ceRecords)) {
        // event changed and not found so check entry CEID
        $sql = 'select e.compEventId
                    from ' . E4S_TABLE_WCORDERITEMMETA . ' wcim,
                         ' . E4S_TABLE_ENTRIES . ' e
                    where order_item_id = ' . $obj->orderItemId . "
                    and meta_key = '_product_id'
                    and e.variationid = wcim.meta_value";
    }
    if (array_key_exists($ceId, $ceRecords)) {
        $compId = $ceRecords [$ceId];
        if (!array_key_exists($compId, $comps)) {
            Entry4UIError(8000, 'Failed to find competition ' . $compId);
        }
        if ($obj->orderStatus === WC_ORDER_PAID) {
            $comps[$compId]->value += $obj->liveValue;
        } else {
            $comps[$compId]->refunded += $obj->liveValue;
        }
    }
}
echo "Competition Id,Name,Date,Taken,Refunded\n";
foreach ($comps as $compId => $compObj) {
    echo $compId . ',' . $compObj->competition . ',' . $compObj->compDate . ',' . $compObj->value . ',' . $compObj->refunded . "\n";
}