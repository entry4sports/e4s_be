<?php
// Results Header Options
function e4s_addDefaultResultsHeaderOptions($options) {
    $options = e4s_mergeInOptions(e4s_getResultsHeaderDefaultOptions(), $options);
    return $options;
}

function e4s_removeDefaultResultsHeaderOptions($options) {
    $options = e4s_removeBaseOptions(e4s_getResultsHeaderDefaultOptions(), $options);
    $options = e4s_CodeOptions($options, E4S_ENCODE_OPTIONS);
    return $options;
}

function e4s_getResultsHeaderDefaultOptions() {
    $defoptions = new stdClass();
    return $defoptions;
}

// Athlete Options
function e4s_addDefaultAthleteOptions($options) {
    $options = e4s_mergeInOptions(e4s_getAthleteDefaultOptions(), $options);
    return $options;
}

function e4s_removeDefaultAthleteOptions($options) {
    $options = e4s_removeBaseOptions(e4s_getAthleteDefaultOptions(), $options);
    $options = e4s_CodeOptions($options, E4S_ENCODE_OPTIONS);
    return $options;
}

function e4s_getAthleteDefaultOptions() {
    $defOptions = new stdClass();
    $defOptions->urnChecked = null;
    $defOptions->noEntryReason = '';
    $emergency = new stdClass();
    $emergency->name = '';
    $emergency->tel = '';
    $emergency->relationship = '';
    $defOptions->emergency = $emergency;
    return $defOptions;
}

// Competition Options
function e4s_addDefaultCompOptions($options, $keepCheckinCodes = TRUE) {
    $options = e4s_mergeInOptions(e4s_getCompDefaultOptions(), $options);
    if (!$keepCheckinCodes) {
        unset ($options->checkIn->codes);
    }
    return $options;
}

function e4s_removeDefaultCompOptions($options) {
    $options = e4s_removeBaseOptions(e4s_getCompDefaultOptions(), $options);
    $options = e4s_CodeOptions($options, E4S_ENCODE_OPTIONS);
    return $options;
}

function e4s_getCompDefaultOptions() {
    $defOptions = new stdClass();
    $defOptions->isClubComp = false;
	$cloneInfo = new stdClass();
	$cloneInfo->fromId = 0;
	$defOptions->cloneInfo = $cloneInfo;
    $defOptions->sourceId = 0;
    $defOptions->anonymize = false;
    $defOptions->hideScheduledOnly = true;
    $defOptions->standards = array();
    $config = e4s_getConfig();
    $defOptions->allowExpiredRegistration = $config['options']->defaultAllowExpiredRegistration;
    $defOptions->useTeamBibs = FALSE;
    $bacsOptions = new stdClass();
    $bacsOptions->enabled = FALSE;
    $bacsOptions->msg = '';
    $defOptions->bacs = $bacsOptions;
    $defOptions->scoreboard = new stdClass();
    $defOptions->scoreboard->image = R4S_BLANK_PHOTO;
    $defOptions->seqEventNo = TRUE;
    $defOptions->disabled = FALSE;
    $defOptions->stripeMandatory = FALSE;

    $defOptions->homeInfo = '';
    $defOptions->shortCode = '';
    $defOptions->priority = FALSE;
    $defOptions->bibNos = '1';
    $defOptions->bibSort1 = 'surname';
    $defOptions->bibSort2 = 'firstname';
    $defOptions->bibSort3 = 'dob';
    $defOptions->heatOrder = E4S_SLOWEST_FIRST;
    $defOptions->stadium = '';
    $defOptions->adjustEventNo = 0;
    $defOptions->athleteType = 'A';
    $defOptions->tickets = new stdClass();
    $defOptions->tickets->enabled = FALSE;

	$pf = new stdClass();
    $pf->pfTargetDirectory = ''; // defunct ?
    $pf->type = '';
    $pf->system = 1;
    $defOptions->pf = $pf;

    $defOptions = e4sContactClass::setDefaultInCOptions($defOptions);

    $cardInfoObj = new stdClass();
    $cardInfoObj->enabled = !e4s_isAAIDomain();
    $cardInfoObj->availableFrom = '';
    $cardInfoObj->track = '';
    $cardInfoObj->field = '';
    $cardInfoObj->fieldCardSize = 25;
    $cardInfoObj->callRoom = true;
    $defOptions->cardInfo = $cardInfoObj;

    $subscription = new stdClass();
    $subscription->enabled = TRUE;
    $subscription->timeCloses = '';
    $subscription->organiserMessage = '';
    $subscription->e4sMessage = '';
    $subscription->refunded = '';
    $subscription->process = 0; // 0 = Do NOT Process movement of waiting lists, 1 = Process
    $subscription->processRefundTime = ''; // ISO Date time of WHEN to process refunds. Validate it can not be before comp date
    $defOptions->subscription = $subscription;

    $cardObj = new stdClass();
    $headerObj = new stdClass();
    $headerObj->field = '';
    $headerObj->track = '';
    $footerObj = new stdClass();
    $footerObj->field = '';
    $footerObj->track = '';
    $cardObj->header = $headerObj;
    $cardObj->footer = $footerObj;
    $defOptions->card = $cardObj;

    $checkinObj = new stdClass();
    $checkinObj->enabled = FALSE;
    $checkinObj->checkInDateTimeOpens = '';
    $checkinObj->defaultFrom = 240;
    $checkinObj->defaultTo = 60;
    $checkinObj->qrCode = FALSE;
    $checkinObj->text = '';
    $checkinObj->terms = '';
    $defOptions->checkIn = $checkinObj;

    $defOptions->school = FALSE;
    $defOptions->orgFreeEntry = FALSE;
    $defOptions->autoPayFreeEntry = FALSE;

    $chequeObj = new stdClass();
    $chequeObj->allow = FALSE;
    $chequeObj->ends = '';
    $defOptions->cheques = $chequeObj;

    $allowObj = new stdClass();
    $allowObj->unregistered = FALSE;
    $allowObj->international = FALSE;
    $allowObj->registered = TRUE;
    $defOptions->allowAdd = $allowObj;

    $defOptions->timetable = 'provisional';
    $defOptions->helpText = new stdClass();
    $defOptions->helpText->schedule = '';
    $defOptions->helpText->teams = '';
    $defOptions->helpText->cart = '';

    $defOptions->showTeamAthletes = FALSE;
    $defOptions->singleAge = FALSE;
    $defOptions->showAthleteAgeInEntries = FALSE;

    // TODO Remove sections as not required now
    $reportObj = new stdClass();
    $reportObj->summary = TRUE;
    $reportObj->athletes = TRUE;
    $reportObj->ttathletes = TRUE;
    $reportObj->ttentries = TRUE;
    $reportObj->individual_entries = TRUE;
    $reportObj->teams = TRUE;
    $reportObj->subscriptions = TRUE;
    $reportObj->orders = TRUE;
    $reportObj->events = TRUE;
    $defOptions->report = $reportObj;

    $athleteSecurityObj = new stdClass();
    $athleteSecurityObj->areas = array();
    $athleteSecurityObj->clubs = array();
    $defOptions->athleteSecurity = $athleteSecurityObj;

    $uiObj = new stdClass();
    $uiObj->enterButtonText = '';
    $uiObj->entryDefaultPanel = E4S_UI_SCHEDULE;
    $uiObj->ticketComp = 0;
    $uiObj->ticketCompButtonText = '';
    $sectionsToHide = new stdClass();
    $sectionsToHide->SCHEDULE = FALSE;
    $sectionsToHide->ATHLETES = FALSE;
    $sectionsToHide->SHOP = FALSE;
    $sectionsToHide->TEAMS = FALSE;
    $uiObj->sectionsToHide = $sectionsToHide;
    $defOptions->ui = $uiObj;

    $priorityObj = new stdClass();
    $priorityObj->required = FALSE;
    $priorityObj->code = '';
    $priorityObj->dateTime = '';
    $priorityObj->message = '';
    $defOptions->priority = $priorityObj;

    $defOptions->athleteQrData = FALSE;
    $defOptions->nonGuests = array();
    return $defOptions;
}

// Message Options
function e4s_addDefaultMessageOptions($options) {
    return e4s_mergeInOptions(e4s_getMessageDefaultOptions(), $options);
}

function e4s_removeDefaultMessageOptions($options) {
    return e4s_removeBaseOptions(e4s_getMessageDefaultOptions(), $options);
}

function e4s_getMessageDefaultOptions() {
    $options = new stdClass();
    $options->filter = ''; // F = Field T=Track E = Event Groups
    $options->eventGroups = arrary();
    return $options;
}

// UOM Options
function e4s_addDefaultUOMOptions($options) {
    return e4s_mergeInOptions(e4s_getUOMDefaultOptions(), $options);
}

function e4s_removeDefaultUOMOptions($options) {
    return e4s_removeBaseOptions(e4s_getUOMDefaultOptions(), $options);
}

function e4s_getUOMDefaultOptions() {
    $options = new stdClass();
    return $options;
}

// Event Group (v2) Options
function e4s_addDefaultEventGroupV2Options($options) {
    $options = e4s_getOptionsAsObj($options);
    if ( isset($options->entriesFrom) ) {
        $entriesFromType = gettype($options->entriesFrom);
        if ($entriesFromType === 'integer') {
            $entriesFrom = new stdClass();
            $entriesFrom->id = $options->entriesFrom;
            $options->entriesFrom = $entriesFrom;
        }
    }

    return e4s_mergeInOptions(e4s_getEventGroupV2DefaultOptions(), $options);
}

function e4s_removeDefaultEventGroupV2Options($options) {
    return e4s_removeBaseOptions(e4s_getEventGroupV2DefaultOptions(), $options);
}

function e4s_getEventGroupV2DefaultOptions() {
    $options = new stdClass();
    $options->maxAthletes = 0;
    $options->maxInHeat = 0;
    $options->includeEntriesFromEgId = 0;
    $options->excludeFromCntRule = FALSE;
    $registration = new stdClass();
    $registration->unregisteredAthletes = TRUE;
    $registration->registeredAthletes = TRUE;
    $registration->internationalAthletes = TRUE;
    $options->registration = $registration;

    $options->combinedId = 0;
    $entriesFrom = new stdClass();
    $entriesFrom->id = 0;
    $options->entriesFrom = $entriesFrom;

    $display = new stdClass();
    $display->helpText = '';
    $display->autoExpandHelpText = FALSE;
    $display->showPB = FALSE;
    $display->showPrice = FALSE;
    $display->showEntryCount = FALSE;
    $display->hideOnDisable = FALSE;
    $options->display = $display;

    $heatInfo = new stdClass();
    $heatInfo->heatDurationMins = 0;
    $heatInfo->useLanes = 'A';
    $heatInfo->unique = FALSE;
    $heatInfo->mandatoryPB = e4s_getDefaultMandatoryPB();
    $options->heatInfo = $heatInfo;

    $trials = new stdClass();
	$trials->max = 6; // Maximum number of trials
	$trials->min = 0; // Cut off trial when top x athletes are allowed to continue
	$trials->athleteCnt = 0; // Number of athletes to allow to continue at cutOffTrial
//	$options->trials = $trials;

    $seed = new stdClass();
    $seed->gender = FALSE;
    $seed->age = FALSE;
    $seed->type = E4S_SEED_OPEN;
    $seed->firstLane = 1;
    $seed->laneCount = 8;
    $seed->waiting = FALSE;
    $seed->seeded = FALSE;
    $qualifyToEg = new stdClass();
    $qualifyToEg->id = 0;
    $qualifyToEg->compId = 0;
    $qualifyToEg->name = '';
	$rules = new stdClass();
	$rules->auto = 0;
	$rules->nonAuto = 0;
	$qualifyToEg->rules = $rules;
    $seed->qualifyToEg = $qualifyToEg;
    $options->seed = $seed;

    $eventTeam = new stdClass();
    $eventTeam->isTeamEvent = FALSE;
    $eventTeam->min = 0;
    $eventTeam->max = 0;
    $eventTeam->minTargetAgeGroupCount = 0;
    $eventTeam->maxOtherAgeGroupCount = 0;
    $eventTeam->maxEventTeams = 0;
    $eventTeam->teamPositionLabel = '';
    $eventTeam->teamSubstituteLabel = '';
    $eventTeam->singleClub = '';
    $eventTeam->formType = '';
    $eventTeam->disableTeamNameEdit = true;
    $eventTeam->uniqueName = true;
    $eventTeam->teamNameFormat = '{{entity}} {{eventname}} {{gender}} {{agegroup}} {{unique}}';
    $options->eventTeam = $eventTeam;

    $cardInfo = new stdClass();
    $cardInfo->reportInfo = '';
    $cardInfo->trialInfo = '';
    $options->cardInfo = $cardInfo;

    $security = new stdClass();
    $security->clubs = '';
    $security->counties = '';
    $security->regions = $cardInfo;
    $options->security = $security;

    $athleteSecurityObj = new stdClass();
    $athleteSecurityObj->clubs = array();
    $athleteSecurityObj->counties = array();
    $athleteSecurityObj->regions = array();
    $options->athleteSecurity = $athleteSecurityObj;

    $availability = new stdClass();
    $availability->availableFrom = '';
    $availability->availableFromStatus = E4S_EVENT_HIDDEN;
    $availability->availableTo = '';
    $availability->availableToStatus = E4S_EVENT_HIDDEN;
    $options->availability = $availability;

    $checkIn = new stdClass();
    $checkIn->enabled = FALSE;
    $checkIn->from = '';
    $checkIn->to = '';
    $options->checkIn = $checkIn;

    return $options;
}

// Event Group Options
function e4s_addDefaultEventGroupOptions($options) {
    $options = e4s_getOptionsAsObj($options);
    if ( isset($options->entriesFrom) ) {
        $entriesFromType = gettype($options->entriesFrom);
        if ($entriesFromType === 'integer') {
            $entriesFrom = new stdClass();
            $entriesFrom->id = $options->entriesFrom;
            $options->entriesFrom = $entriesFrom;
        }
    }
    return e4s_mergeInOptions(e4s_getEventGroupDefaultOptions(), $options);
}

function e4s_removeDefaultEventGroupOptions($options) {
    return e4s_removeBaseOptions(e4s_getEventGroupDefaultOptions(), $options);
}

function e4s_getEventGroupDefaultOptions() {
    $options = new stdClass();
    $options->cardDisplay = true;
    $options->trialInfo = '';
    $options->reportInfo = '';
    $options->isTeamEvent = FALSE;
	$includeEntriesFromEgId = new stdClass();
	$includeEntriesFromEgId->id = 0;
	$options->includeEntriesFromEgId = $includeEntriesFromEgId;
    $options->maxathletes = 0;
    $options->maxInHeat = 0;
    $options->combinedId = 0;
    $entriesFrom = new stdClass();
    $entriesFrom->id = 0;
    $entriesFrom->name = '';
    $entriesFrom->eventNo = 0;
    $entriesFrom->typeNo = '';
    $options->entriesFrom = $entriesFrom;
    $multiEventOptions = new stdClass();
    $multiEventOptions->childEvents = array();
    $options->multiEventOptions = $multiEventOptions;
    $options->resultsPossible = TRUE;
    $options->resultsOpen = false;
    $options->heatDurationMins = 0;

	$trials = new stdClass();
	$trials->max = 6; // Maximum number of trials
	$trials->min = 0; // Cut off trial when top x athletes are allowed to continue
	$trials->athleteCnt = 0; // Number of athletes to allow to continue at cutOffTrial
//	$options->trials = $trials;

    $options->seed = new stdClass();
    $options->seed->firstLane = 1;
    $options->seed->laneCount = 0;
    $options->seed->gender = FALSE;
    $options->seed->age = FALSE;
    $options->seed->type = 'O';
    $options->seed->waiting = FALSE;
    $options->seed->seeded = FALSE;
    $options->seed->doubleup = [];
    $athleteSecurityObj = new stdClass();
    $athleteSecurityObj->clubs = array();
    $options->athleteSecurity = $athleteSecurityObj;
    $qualifyToEg = new stdClass();
    $qualifyToEg->id = 0;
    $qualifyToEg->compId = 0;
    $qualifyToEg->name = '';
    $options->seed->qualifyToEg = $qualifyToEg;
    return $options;
}

// Event Options
function e4s_addDefaultEventOptions($options) {
    return e4s_mergeInOptions(e4s_getEventDefaultOptions(), $options);
}

function e4s_removeDefaultEventOptions($options) {
    return e4s_removeBaseOptions(e4s_getEventDefaultOptions(), $options);
}

function e4s_getEventDefaultOptions() {
    $options = e4s_getEventDefDefaultOptions();
    $options->helpText = '';
    $options->registeredAthletes = TRUE;
    $options->unregistered = FALSE;
    $options->registered = TRUE;
    $options->international = TRUE;
    $options->isTeamEvent = FALSE;
    $options->wind = '';
    $options->excludeFromCntRule = FALSE;
    $options->unique = array();
    $options->eventTeam = new stdClass();
    $options->eventTeam->min = 0;
    $options->eventTeam->max = 0;
    $options->eventTeam->mustBeIndivEntered = FALSE;
    $options->eventTeam->minTargetAgeGroupCount = 0;
    $options->eventTeam->maxOtherAgeGroupCount = 0;
    $options->eventTeam->teamPositionLabel = '';
    $options->eventTeam->maxEventTeams = 0;
    $options->eventTeam->currCount = 0;
    $options->eventTeam->singleClub = FALSE;
    $options->eventTeam->disableTeamNameEdit = true;
    $options->eventTeam->teamNameFormat = '{{entity}} {{eventname}} {{gender}} {{agegroup}} {{unique}}';
    $options->eventTeam->teamSubstituteLabel = 'athlete';
    $options->eventTeam->showForm = FALSE;
    $options->eventTeam->formType = E4S_TEAM_TYPE_DEFAULT;
    $options->eventTeam->price = E4S_PRICE_PER_TEAM;
    $options->rowOptions = new stdClass();
    $options->rowOptions->autoExpandHelpText = FALSE;
    $options->rowOptions->showPB = TRUE;
    $options->rowOptions->showPrice = FALSE;
    $options->rowOptions->showEntryCount = TRUE;
    $options->maxInHeat = 0; //  This should be in heatInfo vvvvvvv
    // This is in eventGroup not event !!!!!
    $options->heatInfo = new stdClass();
    $options->heatInfo->useLanes = 'A';

    $options->multiEventOptions = array();
    return $options;
}

function e4s_getEventDefDefaultOptions() {
    $options = new stdClass();
    $options->min = 0;
    $options->max = 0;
    return $options;
}

//$ceoptions takes priority
function e4s_mergeAndAddDefaultCEOptions($ceoptions, $eoptions, $retType) {
    $newOptions = e4s_mergeOptions($ceoptions, $eoptions, E4S_OPTIONS_OBJECT);
    $newOptions = e4s_addDefaultCompEventOptions($newOptions);
    return e4s_getDataAsType($newOptions, $retType);
}

// CompEvent Options
function e4s_addDefaultCompEventOptions($options) {
    return e4s_mergeInOptions(e4s_getCompEventDefaultOptions(), $options);
}

function e4s_removeDefaultCompEventOptions($options) {
    return e4s_removeBaseOptions(e4s_getCompEventDefaultOptions(), $options);
}

function e4s_getCompEventDefaultOptions() {
    $options = e4s_getEventDefaultOptions();
    $options->xiText = '';
    $options->xeText = '';
    $options->xbText = '';
    $options->xrText = '';
    $options->warningMessage = '';
    $options->mandatoryPB = e4s_getDefaultMandatoryPB();
//    $options->mandatoryPB = false;
    $options->trialInfo = '';
    $options->reportInfo = '';
    $options->ageGroups = array();
    $options->maxInHeat = 0; // Needs moving to heatInfo
    $options->heatInfo = new stdClass();
    $options->heatInfo->useLanes = 'A';
    $options->heatInfo->heatDurationMins = 0;
    $options->singleAge = FALSE;
    $options->security = new stdClass();
    $options->athleteSecurity = new stdClass();
    $options->excludeFromCntRule = FALSE;
    $options->checkIn = new stdClass();
    $options->checkIn->from = -1;
    $options->checkIn->to = -1;
    $options->checkIn->seedOnEntries = FALSE;
    $options->uniqueEventGroups = array();
    $options->isTeamEvent = false;
    $eventTeam = new stdClass();
    $eventTeam->min = 0;
    $eventTeam->max = 0;
    $eventTeam->teamPositionLabel = 'Leg';
    $eventTeam->singleClub = true;
    $eventTeam->teamSubstituteLabel = 'Substitute';
    $eventTeam->formType = '';
    $eventTeam->maxTeamsForAthlete = 0;
    $eventTeam->disableTeamNameEdit = true;
    $eventTeam->mustBeIndivEntered = false;
    $options->eventTeam = $eventTeam;
    $options->unregisteredAthletes = false;
    $options->internationalAthletes = false;

    return $options;
}

// Price Options
function e4s_addDefaultPriceOptions($options) {
    $options = e4s_mergeInOptions(e4s_getPriceDefaultOptions(), $options);
    if (isset($options->displayfee)) {
        $options->displayFee = $options->displayfee;
        unset($options->displayfee);
    }
    return $options;
}

function e4s_removeDefaultPriceOptions($options) {
    return e4s_removeBaseOptions(e4s_getPriceDefaultOptions(), $options);
}

function e4s_getPriceDefaultOptions() {
    $options = new stdClass();
    $options->displayFee = FALSE;
    $options->feeIncluded = TRUE;
    $options->freeEntry = FALSE;
    $discount = new stdClass();
    $discount->count = 0;
    $discount->price = 0.00;
    $discount->validToDate = '';
    $options->discount = $discount;
    return $options;
}
function e4s_getDiscountDefaultOptions() {
    $options = new stdClass();
    $options->includeSecondClaim = false;
    $options->registered  = false;
    $options->unregistered = false;
    $options->clubs = array();
    $options->athletes = array();
    $options->users = array();
    $options->events = array();
    $options->ignoreEvents = array();

    return $options;
}
function e4s_addDefaultDiscountOptions($options) {
    return e4s_mergeInOptions(e4s_getDiscountDefaultOptions(), $options);
}
function e4s_removeDefaultDiscountOptions($options) {
    return e4s_removeBaseOptions(e4s_getDiscountDefaultOptions(), $options);
}
// Team Entry Options
function e4s_addDefaultTeamEntryOptions($options) {
    return e4s_mergeInOptions(e4s_getTeamEntryDefaultOptions(), $options);
}

function e4s_removeDefaultTeamEntryOptions($options) {
    return e4s_removeBaseOptions(e4s_getTeamEntryDefaultOptions(), $options);
}

function e4s_getTeamEntryDefaultOptions() {
    $options = new stdClass();
    $options->resultsKey = '';
    $options->price = '';
    $options->athletecnt = 0;
    $options->lastStatus = 'completed';
    return $options;
}
// Entry Options
function e4s_addDefaultEntryOptions($options) {
    return e4s_mergeInOptions(e4s_getEntryDefaultOptions(), $options);
}

function e4s_removeDefaultEntryOptions($options) {
    return e4s_removeBaseOptions(e4s_getEntryDefaultOptions(), $options);
}

function e4s_getEntryDefaultOptions() {
    $options = new stdClass();
    $options->allowAreas = FALSE;
    $options->otd = FALSE;
    $options->forceBasket = FALSE;
    $options->waitingInfo = new stdClass();
    $options->waitingInfo->originalPos = 0;
    $options->waitingInfo->wentOn = '';
    $options->waitingInfo->cameOff = '';

    $options->resultsKey = '';
    $options->trackPB = true;
    $options->checkIn = new stdClass();
    $options->checkIn->from = null;
    $options->checkIn->to = null;
    $options->autoEntries = new stdClass();
    $options->autoEntries->targetEntry = new stdClass();
    $options->autoEntries->targetEntry->id = 0;
    $options->autoEntries->targetEntry->paid = 0;
    $options->autoEntries->targetEntry->orderId = 0;
    $options->autoEntries->targetEventGroup = new stdClass();
    $options->autoEntries->targetEventGroup->id = 0;
    $options->autoEntries->targetEventGroup->name = '';
    return $options;
}

// Config Options
function e4s_addDefaultConfigOptions($options) {
    return e4s_mergeInOptions(e4s_getConfigDefaultOptions(), $options);
}

function e4s_removeDefaultConfigOptions($options) {
    return e4s_removeBaseOptions(e4s_getConfigDefaultOptions(), $options);
}

function e4s_getConfigDefaultOptions() {
    $options = new stdClass();
    $options->allowAreas = FALSE;
    $options->allowClubs = TRUE;
    $options->minFee = 0.50;
    $options->minCost = 10;
    $options->informActive = 'e4s@jessicaday.co.uk';
    $options->informHappening = 'sharon@entry4sports.com';
//    $options->paymentCodeMandatory = FALSE;
    $options->stripeMandatory = FALSE;
    $options->useStripeConnect = FALSE;
    $options->defaultAllowExpiredRegistration = FALSE;
    $options->waitingListDays = 4;

    $stripe = new stdClass();
// Move these from root at some point
// $stripe->stripeMandatory = FALSE;
// $stripe->useStripeConnect = FALSE;
    $stripe->approvers = array();
    $options->stripe = $stripe;

    $homePage = new stdClass();
    $homePage->message = '';

    $defaultFilters = new stdClass();
    $defaultFilters->fromDate = '';
    $defaultFilters->toDate = '';
    $defaultFilters->type = 'ALL';
    $defaultFilters->freeTextSearch = '';

    $event = new stdClass();
    $event->id = 0;
    $event->name = '';
    $defaultFilters->event = $event;

    $compOrg = new stdClass();
    $compOrg->id = 0;
    $compOrg->name = '';
    $defaultFilters->compOrg = $compOrg;

    $location = new stdClass();
    $location->id = 0;
    $location->name = '';
    $defaultFilters->location = $location;

    $organiser = new stdClass();
    $organiser->id = 0;
    $organiser->name = '';
    $defaultFilters->organiser = $organiser;

    $homePage->defaultFilters = $defaultFilters;

    $options->homePage = $homePage;
    return $options;
}

// Standard functions
function e4s_mergeInOptions($baseOptions, $addInOptions) {
	if ( is_null($addInOptions) ){
		$addInOptions = new stdClass();
	}
    if (gettype($addInOptions) === E4S_OPTIONS_STRING) {
        $addInOptions = trim($addInOptions);
		if ($addInOptions === 'null') {
			$addInOptions = new stdClass();
		}
    }
    if (gettype($addInOptions) !== E4S_OPTIONS_OBJECT) {
        $addInOptions = e4s_getOptionsAsObj($addInOptions );
    }
    $newOptions = new stdClass();
    if (!is_null($baseOptions)) {
        foreach ($baseOptions as $key => $value) {
            $newOptions->$key = $value;
        }
    }

    foreach ($addInOptions as $key => $value) {
        if (isset($newOptions->$key) and gettype($newOptions->$key) === E4S_OPTIONS_OBJECT) {
            $newOptions->$key = e4s_mergeInOptions($newOptions->$key, $value);
        } else {
            $newOptions->$key = $value;
        }
    }

    $encoded = e4s_CodeOptions($newOptions, E4S_DECODE_OPTIONS);
    return $encoded;
}

function e4s_removeBaseOptions($baseOptions, $SourceOptions) {
    e4s_removeBaseOptionsFromSourceOptions($baseOptions, $SourceOptions);

    if (sizeof($SourceOptions) > 0) {
        $SourceOptions = e4s_CodeOptions($SourceOptions, E4S_ENCODE_OPTIONS);
        $SourceOptions = e4s_getOptionsAsObj($SourceOptions);
    } else {
        // empty array so return object
        $SourceOptions = new stdClass();
    }
    return $SourceOptions;
}

function e4s_removeBaseOptionsFromSourceOptions($baseOptions, &$sourceOptions) {
    if (gettype($baseOptions) === E4S_OPTIONS_OBJECT) {
        $baseOptions = get_object_vars($baseOptions);
    }
    // should this simply get the array ?
    if (gettype($sourceOptions) !== E4S_OPTIONS_OBJECT) {
        $sourceOptions = e4s_getOptionsAsObj($sourceOptions);
    }
    if (gettype($sourceOptions) === E4S_OPTIONS_OBJECT) {
        $sourceOptions = get_object_vars($sourceOptions);
    }
    foreach ($sourceOptions as $key => &$value) {
        $objType = gettype($value);
        $process = TRUE;
        if ($objType === E4S_OPTIONS_OBJECT or $objType === E4S_OPTIONS_ARRAY) {
            if ($objType === E4S_OPTIONS_ARRAY) {
                // if both are arrays and empty, dont process, just unset
                if (empty($value) and empty($baseOptions[$key])) {
                    $process = FALSE;
                }
            }
            if (isset($baseOptions[$key]) and $process) {
                e4s_removeBaseOptionsFromSourceOptions($baseOptions[$key], $value);
            }
            if (empty($value)) {
                unset($sourceOptions[$key]);
            }
        } else {
            if (array_key_exists($key, $baseOptions) and $baseOptions[$key] === $value) {
                unset($sourceOptions[$key]);
            }
        }
    }
    return $sourceOptions;
}

function e4s_getOptionsAsStringNoNumCheck($options) {
    return e4s_getDataAsType($options, E4S_OPTIONS_STRING_NONUMCHECK);
}

function e4s_getOptionsAsString($options) {
    return e4s_getDataAsType($options, E4S_OPTIONS_STRING);
}

function e4s_getOptionsAsArray($options) {
//    return e4s_getOptionsAs($options, true);
    return e4s_getDataAsType($options, E4S_OPTIONS_ARRAY);
}

function e4s_getOptionsAsObj($options) {
    if (is_null($options)) {
        return new stdClass();
    }
	if ( $options === '' or $options === 'null') {
		return new stdClass();
	}

	return e4s_getDataAsType($options, E4S_OPTIONS_OBJECT);

//    return e4s_getOptionsAs($options, false);
    $retOptions = e4s_getDataAsType($options, E4S_OPTIONS_OBJECT);
    if (is_null($retOptions)) {
        if (isE4SUser() ) {
            e4s_addDebugForce(debug_backtrace());
        }
        $retOptions = e4s_getDataAsType($options, E4S_OPTIONS_OBJECT);
        Entry4UIError(9503, 'Invalid Options found : {' . (string)$options . '}', 400, '');
    }
    return $retOptions;
}

function e4s_getDataAsType($data, $typeToReturn) {
    $type = gettype($data);
//    addDebug($type . " : " . $data);
    if ($type == $typeToReturn) {
        // already have whats required
        return $data;
    }
    if ($type === E4S_OPTIONS_STRING) {
        if ($data === '' or $data === '{}' or $data === '[]') {
            $data = new stdClass();
        } else {
            if ( $data[0] !== '{' and $data[0] !== '['){
                $data = '{' . $data . '}';
            }

            $data = json_decode($data);
        }
    }
    if ($type === E4S_OPTIONS_ARRAY) {
        // encode then decode ensures all levels are objects.
        // (object($data)) only objectifies the top level
        $data = json_decode(json_encode($data));
    }

    // we should have $options now as an object

    if ($typeToReturn === E4S_OPTIONS_OBJECT) {
        return $data;
    }
    if ($typeToReturn === E4S_OPTIONS_ARRAY) {
        return (array)$data;
    }
    // return string
    if ($typeToReturn === E4S_OPTIONS_STRING_NONUMCHECK) {
        return json_encode($data);
    }

    return json_encode($data, JSON_NUMERIC_CHECK);
}

// $options1 is the priority
function e4s_mergeOptions($options1, $options2, $retType) {
    $options1Arr = e4s_getOptionsAsArray($options1);
    $options2Arr = e4s_getOptionsAsArray($options2);
    $newOptionsArr = array_merge($options2Arr, $options1Arr);
    return e4s_getDataAsType($newOptionsArr, $retType);
}

function e4s_isSchool($coptions) {
    if (isset($coptions)) {
        if (isset($coptions->school)) {
            if ($coptions->school) {
                return TRUE;
            }
        }
    }
    return FALSE;
}

function e4s_CodeOptions($options, $type) {
    $origType = gettype($options);
    $optionsArr = e4s_getOptionsAsArray($options);
    _e4s_encodeOptions($optionsArr, 'xiText', $type);
    _e4s_encodeOptions($optionsArr, 'xeText', $type);
    _e4s_encodeOptions($optionsArr, 'xbText', $type);
    _e4s_encodeOptions($optionsArr, 'xrText', $type);
    _e4s_encodeOptions($optionsArr, 'helpText', $type);
    _e4s_encodeOptions($optionsArr, 'warningMessage', $type);
    _e4s_encodeOptions($optionsArr, 'trials', $type);
    _e4s_encodeOptions($optionsArr, 'reportInfo', $type);

    _e4s_encodeObjectOption($optionsArr, 'helpText', 'schedule', $type);
    _e4s_encodeObjectOption($optionsArr, 'helpText', 'teams', $type);
    _e4s_encodeObjectOption($optionsArr, 'helpText', 'cart', $type);

    return e4s_getDataAsType($optionsArr, $origType);
}

function _e4s_encodeObjectOption($options, $element, $property, $type) {
    if (array_key_exists($element, $options)) {
        if (gettype($options[$element]) !== 'object') {
            return;
        }
        if (!isset($options[$element]->{$property})) {
            return;
        }
        if ($options[$element]->{$property} !== '') {
            $options[$element]->{$property} = e4s_encodeText($options[$element]->{$property}, $type);
        }
    }
}

function _e4s_encodeOptions(&$options, $property, $type) {
    if (array_key_exists($property, $options)) {
        if (gettype($options[$property]) !== 'string') {
            return;
        }
        if ($options[$property] !== '') {
            $options[$property] = e4s_encodeText($options[$property], $type);
        }
    }
}

function e4s_encodeText($text, $type) {
    if ($type === E4S_ENCODE_OPTIONS) {
        $text = str_replace("'", '&apos;', $text);
        $text = str_replace('"', '&quot;', $text);
    } else {
        $text = str_replace('&apos;', "'", $text);
        $text = str_replace('&quot;', '"', $text);
    }
    return $text;
}

function e4s_getDefaultMandatoryPB() {
	$config = e4s_getConfig();
	$options = $config['options'];
	if ( isset($options->defaultMandatoryPB) ) {
		return $options->defaultMandatoryPB;
	}
	return FALSE;
}