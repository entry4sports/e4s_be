<?php

namespace React\Socket;

use React\Dns\Config\Config as DnsConfig;
use React\Dns\Resolver\Factory as DnsFactory;
use React\Dns\Resolver\ResolverInterface;
use React\EventLoop\LoopInterface;
use RuntimeException;
use function ini_get;
use function is_array;
use function React\Promise\reject;
use function reset;
use function strpos;
use function substr;

/**
 * The `Connector` class is the main class in this package that implements the
 * `ConnectorInterface` and allows you to create streaming connections.
 *
 * You can use this connector to create any kind of streaming connections, such
 * as plaintext TCP/IP, secure TLS or local Unix connection streams.
 *
 * Under the hood, the `Connector` is implemented as a *higher-level facade*
 * or the lower-level connectors implemented in this package. This means it
 * also shares all of their features and implementation details.
 * If you want to typehint in your higher-level protocol implementation, you SHOULD
 * use the generic [`ConnectorInterface`](#connectorinterface) instead.
 *
 * @see ConnectorInterface for the base interface
 */
final class Connector implements ConnectorInterface {
    private $connectors = array();

    public function __construct(LoopInterface $loop, array $options = array()) {
        // apply default options if not explicitly given
        $options += array('tcp' => TRUE, 'tls' => TRUE, 'unix' => TRUE,

            'dns' => TRUE, 'timeout' => TRUE, 'happy_eyeballs' => TRUE,);

        if ($options['timeout'] === TRUE) {
            $options['timeout'] = (float)ini_get('default_socket_timeout');
        }

        if ($options['tcp'] instanceof ConnectorInterface) {
            $tcp = $options['tcp'];
        } else {
            $tcp = new TcpConnector($loop, is_array($options['tcp']) ? $options['tcp'] : array());
        }

        if ($options['dns'] !== FALSE) {
            if ($options['dns'] instanceof ResolverInterface) {
                $resolver = $options['dns'];
            } else {
                if ($options['dns'] !== TRUE) {
                    $server = $options['dns'];
                } else {
                    // try to load nameservers from system config or default to Google's public DNS
                    $config = DnsConfig::loadSystemConfigBlocking();
                    $server = $config->nameservers ? reset($config->nameservers) : '8.8.8.8';
                }

                $factory = new DnsFactory();
                $resolver = $factory->createCached($server, $loop);
            }

            if ($options['happy_eyeballs'] === TRUE) {
                $tcp = new HappyEyeBallsConnector($loop, $tcp, $resolver);
            } else {
                $tcp = new DnsConnector($tcp, $resolver);
            }
        }

        if ($options['tcp'] !== FALSE) {
            $options['tcp'] = $tcp;

            if ($options['timeout'] !== FALSE) {
                $options['tcp'] = new TimeoutConnector($options['tcp'], $options['timeout'], $loop);
            }

            $this->connectors['tcp'] = $options['tcp'];
        }

        if ($options['tls'] !== FALSE) {
            if (!$options['tls'] instanceof ConnectorInterface) {
                $options['tls'] = new SecureConnector($tcp, $loop, is_array($options['tls']) ? $options['tls'] : array());
            }

            if ($options['timeout'] !== FALSE) {
                $options['tls'] = new TimeoutConnector($options['tls'], $options['timeout'], $loop);
            }

            $this->connectors['tls'] = $options['tls'];
        }

        if ($options['unix'] !== FALSE) {
            if (!$options['unix'] instanceof ConnectorInterface) {
                $options['unix'] = new UnixConnector($loop);
            }
            $this->connectors['unix'] = $options['unix'];
        }
    }

    public function connect($uri) {
        $scheme = 'tcp';
        if (strpos($uri, '://') !== FALSE) {
            $scheme = (string)substr($uri, 0, strpos($uri, '://'));
        }

        if (!isset($this->connectors[$scheme])) {
            return reject(new RuntimeException('No connector available for URI scheme "' . $scheme . '"'));
        }

        return $this->connectors[$scheme]->connect($uri);
    }
}

