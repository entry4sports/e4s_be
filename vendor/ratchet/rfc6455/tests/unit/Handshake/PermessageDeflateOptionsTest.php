<?php

namespace Ratchet\RFC6455\Test\Unit\Handshake;

use PHPUnit\Framework\TestCase;
use Ratchet\RFC6455\Handshake\PermessageDeflateOptions;

class PermessageDeflateOptionsTest extends TestCase {
    public static function versionSupportProvider() {
        return [['7.0.17', FALSE], ['7.0.18', TRUE], ['7.0.200', TRUE], ['5.6.0', FALSE], ['7.1.3', FALSE], ['7.1.4', TRUE], ['7.1.200', TRUE], ['10.0.0', TRUE]];
    }

    /**
     * @requires function deflate_init
     * @dataProvider versionSupportProvider
     */
    public function testVersionSupport($version, $supported) {
        $this->assertEquals($supported, PermessageDeflateOptions::permessageDeflateSupported($version));
    }
}