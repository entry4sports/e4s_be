<?php

namespace Ratchet\RFC6455\Test\Unit\Handshake;

use PHPUnit\Framework\TestCase;
use Ratchet\RFC6455\Handshake\ResponseVerifier;

/**
 * @covers Ratchet\RFC6455\Handshake\ResponseVerifier
 */
class ResponseVerifierTest extends TestCase {
    /**
     * @var ResponseVerifier
     */
    protected $_v;

    public static function subProtocolsProvider() {
        return [[TRUE, ['a'], ['a']], [TRUE, ['c', 'd', 'a'], ['a']], [TRUE, ['c, a', 'd'], ['a']], [TRUE, [], []], [TRUE, ['a', 'b'], []], [FALSE, ['c', 'd', 'a'], ['b', 'a']], [FALSE, ['a', 'b', 'c'], ['d']]];
    }

    public function setUp() {
        $this->_v = new ResponseVerifier();
    }

    /**
     * @dataProvider subProtocolsProvider
     */
    public function testVerifySubProtocol($expected, $request, $response) {
        $this->assertEquals($expected, $this->_v->verifySubProtocol($request, $response));
    }
}
