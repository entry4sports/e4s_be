<?php

namespace Ratchet\RFC6455\Messaging;

use Countable;
use Traversable;

interface MessageInterface extends DataInterface, Traversable, Countable {
    /**
     * @param FrameInterface $fragment
     * @return MessageInterface
     */
    public function addFrame(FrameInterface $fragment);

    /**
     * @return int
     */
    public function getOpcode();

    /**
     * @return bool
     */
    public function isBinary();
}
