<?php

namespace Ratchet\Client;

use React\EventLoop\Factory as ReactFactory;
use React\EventLoop\LoopInterface;
use React\EventLoop\Timer\Timer;
use React\Promise\PromiseInterface;

/**
 * @param string $url
 * @param array $subProtocols
 * @param array $headers
 * @param LoopInterface|null $loop
 * @return PromiseInterface<WebSocket>
 */
function connect($url, array $subProtocols = [], $headers = [], LoopInterface $loop = null) {
    $loop = $loop ?: ReactFactory::create();

    $connector = new Connector($loop);
    $connection = $connector($url, $subProtocols, $headers);

    $runHasBeenCalled = FALSE;

    $loop->addTimer(Timer::MIN_INTERVAL, function () use (&$runHasBeenCalled) {
        $runHasBeenCalled = TRUE;
    });

    register_shutdown_function(function () use ($loop, &$runHasBeenCalled) {
        if (!$runHasBeenCalled) {
            $loop->run();
        }
    });

    return $connection;
}
