<?php
/**
 * Created by claudio on 2018-12-31
 */

use PHPUnit\Framework\TestCase;
use Ratchet\Client\Connector;
use React\EventLoop\Factory;

class RequestUriTest extends TestCase {
    public function uriDataProvider() {
        return [['ws://127.0.0.1/bla', 'http://127.0.0.1/bla'], ['wss://127.0.0.1/bla', 'https://127.0.0.1/bla'], ['ws://127.0.0.1:1234/bla', 'http://127.0.0.1:1234/bla'], ['wss://127.0.0.1:4321/bla', 'https://127.0.0.1:4321/bla']];
    }

    /**
     * @dataProvider uriDataProvider
     */
    public function testGeneratedRequestUri($uri, $expectedRequestUri) {
        $loop = Factory::create();

        $connector = new Connector($loop);

        $generateRequest = self::getPrivateClassMethod('\Ratchet\Client\Connector', 'generateRequest');
        $request = $generateRequest->invokeArgs($connector, [$uri, [], []]);

        $this->assertEquals((string)$request->getUri(), $expectedRequestUri);
    }

    protected static function getPrivateClassMethod($className, $methodName) {
        $class = new ReflectionClass($className);
        $method = $class->getMethod($methodName);
        $method->setAccessible(TRUE);
        return $method;
    }
}