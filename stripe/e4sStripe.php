<?php
//require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
require_once E4S_FULL_PATH . 'dbInfo.php';
const E4S_ORDER_INFO = 'E4S_ORDER_INFO';
const E4S_STRIPE_CONNECT_ORDER_INFO = 'E4S_STRIPE_CONNECT_ORDER_INFO';
const E4S_ORDER_COMPETITIONS = 'comps';
const E4S_ORDER_LINES = 'lines';
const E4S_STRIPE_PAYMENT = 'yith-stripe-connect';

function e4s_listStripeConnections(){
    $allowed = false;
    if ( isE4SUser() ){
        $allowed = true;
    }else{
        $allowed = isAppAdmin();
    }
    $arr = array();
    if ( $allowed ) {
        $sql = '
            select u.ID id, u.user_login login, u.user_email email,
                   um.meta_value value,um.meta_key stripeKey,
                   c.id ccId, c.club clubName
                from ' . E4S_TABLE_USERS . ' u,
                     ' . E4S_TABLE_USERMETA . ' um,
                     ' . E4S_TABLE_COMPCLUB . " c
            where u.id = um.user_id
            and um.meta_key in ('" . E4S_USER_META_STRIPE . "','" . E4S_USER_META_APPROVED_STRIPE . "')
            and u.id = c.stripeuserid
            and club != 'E4S Stripe'
            order by u.id
        ";
        $result = e4s_queryNoLog($sql);

        while ($obj = $result->fetch_object()) {
            $obj->ccId = (int)$obj->ccId;
            $arrKey = $obj->ccId;
            $stripeKey = $obj->stripeKey;
            $stripeValue = $obj->value;
            if ( !array_key_exists($arrKey, $arr) ){
                $newObj = new stdClass();

                $stripe = new stdClass();
                $stripe->requiresApproval = true;
                $stripe->connected = '';
                $stripe->approved = '';
                $newObj->stripe = $stripe;

                $userObj = new stdClass();
                $userObj->login = $obj->login;
                $userObj->email = $obj->email;
                $userObj->id = (int)$obj->id;
                $newObj->user = $userObj;

                $clubObj = new stdClass();
                $clubObj->id = $obj->ccId;
                $clubObj->name = $obj->clubName;
                $newObj->club = $clubObj;

                $arr[$arrKey] = $newObj;
            }
            if ( $stripeKey === E4S_USER_META_APPROVED_STRIPE ){
                $arr[$arrKey]->stripe->requiresApproval = false;
                $arr[$arrKey]->stripe->approved = $obj->value;
            }else{
                $arr[$arrKey]->stripe->connected = $obj->value;
            }
        }
    }
    Entry4UISuccess($arr);
}
function e4s_checkUserConnectStatus($status) {
    $stripeObj = new e4sStripeConnect(e4s_getUserID());
    $stripeObj->checkCurrentStatus($status);
}

function e4s_initialiseExistingUsers() {
    $stripeObj = new e4sStripeConnect(0);
    $stripeObj->updateExisting();
    Entry4UISuccess();
}

function e4s_approveNewStripeUser($obj) {
    $userId = checkFieldForXSS($obj, 'userid:User ID' . E4S_CHECKTYPE_NUMERIC);
    $code = checkFieldForXSS($obj, 'code:Approval Code');
    if (!(isAppAdmin() or isAdminUser())) {
        Entry4UIError(9068, 'You are not authorised to approve this user.');
    }
    $stripeObj = new e4sStripeConnect($userId);
    if (!isE4SUser() and !is_null($code)) {
        if ($stripeObj->getCode() !== $code) {
            Entry4UIError(9069, 'Invalid code.');
        }
    }
    $stripeObj->approveUser();
    Entry4UISuccess('"data":"User has been approved."');
}

function e4s_addWcProductToYITHInfo($wcProduct) {
    $desc = $wcProduct->get_description();
    $desc = str_replace("\'", "'", $desc);
    $descObj = e4s_getDataAsType($desc, E4S_OPTIONS_OBJECT);
    if (isset($descObj->compid)) {
        if (array_key_exists(E4S_STRIPE_CONNECT_ORDER_INFO, $GLOBALS)) {
            $yithObjs = $GLOBALS[E4S_STRIPE_CONNECT_ORDER_INFO];
        } else {
            $yithObjs = array();
        }

        $compId = $descObj->compid;
        if (!array_key_exists($compId, $yithObjs)) {
            $yithObjs[$compId] = new stdClass();
            $yithObjs[$compId]->entries = array();
            $yithObjs[$compId]->stripeUsers = null;
            $yithObjs[$compId]->compName = '';
            if (isset($descObj->compname)) {
                $yithObjs[$compId]->compName = $descObj->compname;
            }
        }
        $productId = $wcProduct->get_id();
        $productPrice = (float)$wcProduct->get_price();
        $descObj->title = $wcProduct->get_title();
        $yithEntryObj = new stdClass();
        $yithEntryObj->productId = $productId;
        $yithEntryObj->price = $productPrice;
        $yithEntryObj->info = $descObj;
        $yithObjs[$compId]->entries[$productId] = $yithEntryObj;

        $GLOBALS[E4S_STRIPE_CONNECT_ORDER_INFO] = $yithObjs;
    }
}

function e4s_checkForYITHInfo($wcOrder = null) {
    if (!array_key_exists(E4S_STRIPE_CONNECT_ORDER_INFO, $GLOBALS)) {
        if (is_null($wcOrder)) {
            foreach (WC()->cart->get_cart() as $values) {
                $wcProduct = $values['data'];
                e4s_addWcProductToYITHInfo($wcProduct);
            }
        } else {
            foreach ($wcOrder->get_items() as $orderItem) {
                $wcProduct = $orderItem->get_product();
                e4s_addWcProductToYITHInfo($wcProduct);
            }
        }
    }
}

function e4s_woocommerceAfterCheckoutValidation($postedData, $errors) {
    if (!e4s_useStripeConnect()) {
        return;
    }
    if ($postedData['payment_method'] !== E4S_STRIPE_PAYMENT) {
        return;
    }

    e4s_checkForYITHInfo(null);
    if (!array_key_exists(E4S_STRIPE_CONNECT_ORDER_INFO, $GLOBALS)) {
        // There has been an issue, so show an error
        $errors->add('validation', 'There is an issue setting E4S Order Information. please contact support@entry4sports.com');
    } else {
        $yithObjs = $GLOBALS[E4S_STRIPE_CONNECT_ORDER_INFO];
        foreach ($yithObjs as $compId => $yithCompObj) {
            $compObj = e4s_GetCompObj($compId);
            $stripeUsers = $compObj->createYITHConsolidatedReceivers($yithCompObj->entries);
            $yithCompObj->stripeUsers = $stripeUsers;
        }
    }

}

function e4s_getStripeconnect_charge_params($data, $name, $commission, $wcOrder) {
    // Should only be called from 1 place , but check anyway
    if ($name !== 'update_destination_payment') {
        return $data;
    }
    $yithProductId = $commission['product_id'];
//    $yithUserId = (int)$commission['user_id'];
    $orderId = (int)$commission['order_id'];
    $data['description'] = 'Order: ' . $orderId;

    e4s_checkForYITHInfo($wcOrder);

    if (!array_key_exists(E4S_STRIPE_CONNECT_ORDER_INFO, $GLOBALS)) {
        $data['description'] .= ' .';
        return $data;
    }
    try {
        $e4sOrderInfo = $GLOBALS[E4S_STRIPE_CONNECT_ORDER_INFO];
        // find productId in consolidated Info
        // All entries for a given competition are grouped under 1 productId
        $userCompOrderInfo = null;
        $useCompId = 0;
        foreach ($e4sOrderInfo as $compId => $compOrderInfo) {
            foreach ($compOrderInfo->entries as $entry) {
                if ($entry->productId === $yithProductId) {
                    $userCompOrderInfo = $compOrderInfo;
                    $useCompId = $compId;
                    break;
                }
            }
        }

        if ($useCompId === 0) {
            $data['metadata'] = ['Entries' => 'Unknown'];
        } else {
            $compName = trim($userCompOrderInfo->compName);
	        $compObj = e4s_getCompObj($useCompId);
            if ($compName === '') {
                $compName = $compObj->getName();
	            if ($compName === '') {
		            $compName = 'Unknown';
	            }
            }

			$cOptions = $compObj->getOptions();
			$paymentCode = '';
			if ( isset($cOptions->paymentCode)) {
				$paymentCode = '/' . $cOptions->paymentCode;
			}

            $data['description'] .= ' Comp: ' . $useCompId . $paymentCode . ' - ' . $compName;

            $data['metadata'] = array();
            foreach ($userCompOrderInfo->entries as $entry) {
                if (isset($entry->info->event)) {
                    if (isset($entry->info->teamName)) {
                        $metaEntry = 'Team: ' . $entry->info->teamName;
                    } else {
                        $metaEntry = 'Ath: ' . $entry->info->athlete;
                    }

                    $metaEntry .= ' Evt: ' . $entry->info->event;
                } else {
                    $metaEntry = 'Ent: ' . $entry->info->title;
                }
                $metaEntry .= ' Paid: ' . number_format($entry->price, 2);
                $data['metadata']['Entry_' . $entry->productId] = $metaEntry;
            }
        }

    } catch (Exception $e) {
        logObj($e);
    }

    return $data;
}

function e4s_genericGetDescription($description, $orderId) {

    if (!array_key_exists(E4S_ORDER_INFO, $GLOBALS)) {
        $GLOBALS[E4S_ORDER_INFO] = array();
    }
    if (!array_key_exists($orderId, $GLOBALS[E4S_ORDER_INFO])) {
        // Order not processed yet
        $GLOBALS[E4S_ORDER_INFO][$orderId] = array();
        $GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_COMPETITIONS] = array();
        $GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_LINES] = array();
        $wcOrder = wc_get_order($orderId);
        if ($wcOrder !== FALSE) {
            foreach ($wcOrder->get_items() as $orderItem) {
                $itemProductId = $orderItem->get_product_id();
                $descObj = null;
                if (array_key_exists($itemProductId, $GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_LINES])) {
                    $descObj = $GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_LINES][$itemProductId];
                } else {
                    $product = wc_get_product($itemProductId);
                    $desc = $product->get_description();
                    $descObj = e4s_getOptionsAsObj($desc);
                    $descObj->value = (float)$orderItem->get_subtotal();
                }
                $GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_LINES][$itemProductId] = $descObj;
                if (isset($descObj->compid)) {
                    $compId = (int)$descObj->compid;
                    if (isset ($GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_COMPETITIONS][$compId])) {
                        $competitionObj = $GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_COMPETITIONS][$compId];
                    } else {
                        $competitionObj = new stdClass();
                        $compObj = e4s_GetCompObj($compId);
                        $cOptions = $compObj->getOptions();
                        $competitionObj->name = $compObj->getName();
                        $competitionObj->orgName = $compObj->getOrganisationName();
                        $competitionObj->paymentCode = $cOptions->paymentCode;
                        $competitionObj->value = 0;
                    }
                    $competitionObj->value += (float)$orderItem->get_subtotal();
                    $GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_COMPETITIONS][$compId] = $competitionObj;
                }
            }
        }
    }
    $compArr = $GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_COMPETITIONS];
    if (!empty($compArr)) {
        $description = 'Order: ' . $orderId;

        foreach ($compArr as $compId => $compObj) {
            $paymentCode = $compObj->paymentCode;
            if ($paymentCode === '') {
                $paymentCode = 'NA';
            }
            $value = number_format($compObj->value, 2);
            $description .= ' Comp: ' . $compId . '/' . $paymentCode . '/' . $value;
//            $description .= " Comp: " . $compId . "/"  . $value;
        }
    }

    return $description;
}

function e4s_getMetaData_StripeConnect($metadata, $action) {
    if ($action !== 'create_payment_intent') { // and $action !== "update_destination_payment"
        return $metadata;
    }
    $orderId = $metadata['order_id'];
    return e4s_getMetaData_Generic($orderId, $metadata);
}

function e4s_getMetaData_Stripe($order, $metadata) {
    $order_data = $order->get_data();
    $orderId = $order_data['id'];
    return e4s_getMetaData_Generic($orderId, $metadata);
}

function e4s_getMetaData_Generic($orderId, $metadata) {
    if (!array_key_exists(E4S_ORDER_INFO, $GLOBALS)) {
        // should not happen
        $GLOBALS[E4S_ORDER_INFO] = array();
    }
    if (!array_key_exists($orderId, $GLOBALS[E4S_ORDER_INFO])) {
        // Order not processed yet, should not happen
        $GLOBALS[E4S_ORDER_INFO][$orderId] = array();
        $GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_COMPETITIONS] = array();
        $GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_LINES] = array();
        $wcOrder = wc_get_order($orderId);
        if ($wcOrder !== FALSE) {
            foreach ($wcOrder->get_items() as $orderItem) {
                $itemProductId = $orderItem->get_product_id();
                $descObj = null;
                if (!array_key_exists($itemProductId, $GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_LINES])) {
                    $product = wc_get_product($itemProductId);
                    $desc = $product->get_description();
                    $descObj = e4s_getOptionsAsObj($desc);
                    $descObj->value = (float)$orderItem->get_subtotal();
                    $GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_LINES][$itemProductId] = $descObj;
                } else {
                    $descObj = $GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_LINES][$itemProductId];
                }
                if (isset($descObj->compid)) {
                    $compId = (int)$descObj->compid;
                    if (isset ($GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_COMPETITIONS][$compId])) {
                        $competitionObj = $GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_COMPETITIONS][$compId];
                    } else {
                        $competitionObj = new stdClass();
                        $compObj = e4s_GetCompObj($compId);
                        $cOptions = $compObj->getOptions();
                        $competitionObj->name = $compObj->getName();
                        $competitionObj->orgName = $compObj->getOrganisationName();
                        $competitionObj->paymentCode = $cOptions->paymentCode;
                        $competitionObj->value = 0;
                    }
                    $competitionObj->value += (float)$orderItem->get_subtotal();
                    $GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_COMPETITIONS][$compId] = $competitionObj;
                }
            }
        }
    }

    $competitionObjs = $GLOBALS[E4S_ORDER_INFO][$orderId][E4S_ORDER_COMPETITIONS];
    $compSep = '';
    if (!empty($competitionObjs)) {
        $metadata['competition'] = '';
        foreach ($competitionObjs as $compId => $competitionObj) {
            $metadata['competition'] .= $compSep . $compId;
            $compSep = ',';
            $value = number_format($competitionObj->value, 2);
			$value = $value . " : " . $competitionObj->orgName . "/" . $competitionObj->name;
			if ( $competitionObj->paymentCode !== '' ){
				$value = $value . " : " . $competitionObj->paymentCode;
			}
//            $metadata['E4S_Meta_' . $compId] = $value . " : " . $competitionObj->orgName . "/" . $competitionObj->name . " : " . $competitionObj->paymentCode;
//            $metadata['E4S_Meta_' . $compId] = $value . ' : ' . $competitionObj->orgName . '/' . $competitionObj->name ;
        }
	    $metadata['E4S_Meta_' . $compId] = $value;
    }

    return $metadata;
}

function e4s_getCommissionDataForEntry($entry) {
    $retObj = new stdClass();
    $retObj->id = $entry->id;
    $retObj->ceId = $entry->ceId;
    $retObj->productId = $entry->productId;
    $retObj->entryName = $entry->entryName;
    $retObj->eventName = $entry->eventName;
    $retObj->compId = $entry->compId;
    $compObj = e4s_GetCompObj($entry->compId);
    $cOptions = $compObj->getOptions();
//    $retObj->stripeCode = $cOptions->paymentCode;
    $retObj->isTeamEvent = $entry->isTeamEvent;
    $retObj->price = $entry->activePrice;
    return $retObj;
}