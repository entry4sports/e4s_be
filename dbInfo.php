<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
require_once E4S_FULL_PATH . 'builder/messageCRUD.php';
require_once E4S_FULL_PATH . 'builder/e4sCRUD.php';
require_once E4S_FULL_PATH . 'e4s_constants.php';
require_once E4S_FULL_PATH . 'admin/userObj.php';
require_once E4S_FULL_PATH . 'commonOptions.php';
require_once E4S_FULL_PATH . 'e4sObjects.php';
require_once E4S_FULL_PATH . 'admin/e4sReturn.php';

e4s_initDebug(19828);

// set to true for Nick to have auto login on local host
$allowLocalHostAdmin = TRUE;
$localUser = 0;
$GLOBALS['E4S_LAST_LOG_ID'] = 0;
$GLOBALS[E4S_PARENT_LOG] = 0;
$hostname = DB_HOST;
$database = DB_NAME;
$username = DB_USER;
$password = DB_PASSWORD;
//// 2020 logs
//$hostnameLog='db5000299193.hosting-data.io';
//$databaseLog='dbs292344';
//$usernameLog='dbu140524';
//$passwordLog='Iloveathletics!1';

// 2019 logs
$hostnameLog = 'db5000237480.hosting-data.io';
$databaseLog = 'dbs231939';
$usernameLog = 'dbu357885';
$passwordLog = 'Iloveathletics!1';


$prevLog = '';
$useTZ = 'Europe/London';
date_default_timezone_set($useTZ);
$tz = new DateTimeZone($useTZ);

$echo = '';
if (isset($_GET['echo'])) {
    $echo = $_GET['echo'];
}
//e4s_dump($_SERVER);

$GLOBALS['perm_report'] = 'Report';
$GLOBALS['perm_userSearch'] = 'UserSearch';

$_setHeaders = FALSE;
$localhost = FALSE;
//header("Access-Control-Allow-Origin: *");
//header('Access-Control-Allow-Methods: GET, PUT, POST, OPTIONS');
//header('Access-Control-Max-Age: 1000');
//header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');

if (isset($_SERVER['HTTP_REFERER'])) {
    if (strpos(strtolower($_SERVER['HTTP_REFERER']), 'localhost') !== FALSE) {
        $localhost = TRUE;
        $_setHeaders = TRUE;
    }
}
$hostnameLog = $hostname;
$databaseLog = $database;
$usernameLog = $username;
$passwordLog = $password;
if (!$localhost) {
    switch (E4S_CURRENT_DOMAIN) {
        case E4S_DEV_DOMAIN:
            $_setHeaders = TRUE;
            break;
    }
}

if ($_setHeaders and (!array_key_exists(E4S_NOHEADERS, $GLOBALS) or !$GLOBALS[E4S_NOHEADERS])) {
    try {
        header('Access-Control-Allow-Origin: *', TRUE);
        header('Access-Control-Allow-Methods: GET, PUT, POST, OPTIONS', TRUE);
        header('Access-Control-Max-Age: 1000', TRUE);
        header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With', TRUE);
    } catch (Exception $e) {

    }
}

// Create connection
$conn = new mysqli($hostname, $username, $password, $database);
$GLOBALS['conn'] = $conn;

$GLOBALS['logconn'] = new mysqli($hostnameLog, $usernameLog, $passwordLog, $databaseLog);

if (!$conn->set_charset('utf8')) {
    printf("Error loading character set E4S utf8: %s\n", $conn->error);
    exit();
}

if ($conn->connect_error) {
    exit('Connection failed: ' . $conn->connect_error);
}

$db_selected = mysqli_select_db($conn, $database);
if (!$db_selected) {
    exit ('Can\'t select database: -2');
}

if (array_key_exists(E4S_CRON_USER, $GLOBALS)) {
    $e4sUserRow = e4sGetAdminUserRow(FALSE);
    $useUserID = (int)$e4sUserRow['ID'];
    $localhost = TRUE;
} else {
    $useUserID = 0;
}

if ($useUserID === 0) {
    $useUserID = apply_filters('determine_current_user', FALSE);
}

if (!is_null(WC())) {
    if (!isset(WC()->session)) {
        WC()->frontend_includes();
        WC()->session = new WC_Session_Handler();
        WC()->session->init();
        WC()->customer = new WC_Customer($useUserID, TRUE);
        WC()->cart = new WC_Cart();
    }
    if (!WC()->session->has_session()) {
        WC()->session->set_customer_session_cookie(TRUE);
    }
}


if ($useUserID !== 0) {
    $GLOBALS[E4S_WC_USER] = get_userdata($useUserID);
}
if (isIPBlocked(true) and !$localhost) {
    Entry4UIError(8998, 'Sorry. System unavailable.', 404);
}

if ($localhost and $allowLocalHostAdmin) {
//     always give admin access to local
    if ($localUser === 0) {
        $e4sUserRow = e4sGetAdminUserRow(FALSE);
        $GLOBALS[E4S_USER_ID] = (int)$e4sUserRow['ID'];
    } else {
        $GLOBALS[E4S_USER_ID] = $localUser;
    }
    wp_set_current_user($GLOBALS[E4S_USER_ID]);
    $GLOBALS[E4S_LOCAL_HOST] = TRUE;
} else {
    $GLOBALS[E4S_LOCAL_HOST] = FALSE;

    if ($GLOBALS[E4S_WC_USER] !== FALSE) {
        $GLOBALS[E4S_USER_ID] = (int)$GLOBALS[E4S_WC_USER]->data->ID;
    } else {
        $GLOBALS[E4S_USER_ID] = E4S_USER_ANON_ID;
    }
}

$GLOBALS[E4S_IMPERSONATE_USER_ID] = $GLOBALS[E4S_USER_ID];

e4s_getUserObj(FALSE, FALSE);
if ($GLOBALS[E4S_IMPERSONATE_USER_ID] !== $GLOBALS[E4S_USER_ID]) {
    wp_set_current_user($GLOBALS[E4S_IMPERSONATE_USER_ID]);
}

new e4sVersion();
logTxt('URI:' . $_SERVER['REQUEST_METHOD'] . ':' . $_SERVER['REQUEST_URI'] . '/' . $_SERVER['QUERY_STRING']);

if (e4s_getUserID() < 0 and !array_key_exists('e4s_check_shortcode', $GLOBALS)) {
    if (!isE4SPublicPage('')) {
	    header("Location: https://" . E4S_CURRENT_DOMAIN . "/#/login");
		exit;
//        Entry4UIError(8999, 'Please login to access this page (' . e4s_getUserID() . ')', 401);
    }
}

// Store the Parent Log Entry
$GLOBALS[E4S_PARENT_LOG] = $GLOBALS['E4S_LAST_LOG_ID'];

if (!isset($e4sIgnoreErrorHandler)) {
    set_error_handler('Entry4Error');
}

// Functions from here down
function e4s_getLastID() {
//    $newresult = e4s_queryNoLog("select LAST_INSERT_ID() as id");
//    $newrow = $newresult->fetch_assoc();
//    return $newrow['id'];
    return $GLOBALS[E4S_LAST_ID];
}

function logInsert($log = TRUE) {
    $conn = $GLOBALS['conn'];
    $insertedId = $conn->insert_id;
    if ($log) {
        logTxt('Inserted id :' . $insertedId);
    }
    return $insertedId;
}

function clubTranslate($club) {
    $club = str_replace('AAC', 'A.C.', $club);
    $club = str_replace('& AC', 'A.C.', $club);
    $club = str_replace('A,C,', 'A.C.', $club);
    $club = str_replace(' AC', ' A.C.', $club);
    $club = str_replace('R.C', 'R.C.', $club);
    $club = str_replace(' RC', ' R.C.', $club);
    $club = str_replace(' RR', ' R.R.', $club);
    $club = str_replace('R.C..', 'R.C.', $club);
    $club = str_replace('RC.', 'R.C.', $club);
    $club = str_replace('A.C', 'A.C.', $club);
    $club = str_replace('A.C..', 'A.C.', $club);
    $club = str_replace('Athletic Club', 'A.C.', $club);
    $club = str_replace('Athletics Club', 'A.C.', $club);
    $club = str_replace('Athletics A.C.', 'A.C.', $club);
    $club = str_replace('and A.C.', 'A.C.', $club);
    $club = str_replace('  ', ' ', $club);
    $club = str_replace('Running Club', 'R.C.', $club);
//    $club = str_replace(" and District", "", $club);
    if (e4s_endsWith($club, 'Athletics')) {
        $club = str_replace('Athletics', 'A.C.', $club);
    }
    if (e4s_endsWith($club, ' Club')) {
        $club = str_replace(' Club', ' A.C.', $club);
    }
    if ($club === 'Le Cheile A.C.') {
        $club = 'Le Chéile A.C.';
    }
    if ($club === 'Ace A.C.') {
        $club = 'Ace Athletics Club';
    }
    if ($club === "Metro/St. Brigid's A.C.") {
        $club = 'Metro/St. Brigids A.C.';
    }
    if ($club === 'Dover RoadRunners A.C. incorporating Dover RoadRunners A.C. Juni') {
        $club = 'Dover Roadrunners A.C.';
    }
    if ($club === 'Carmarthen & District Harriers') {
        $club = 'Carmarthen Harriers A.C.';
    }
    if ($club === 'Blaenau Gwent Athletics') {
        $club = 'Blaenau Gwent A.C.';
    }
    if ($club === 'Altrincham & District A.C. Limited') {
        $club = 'Altrincham and District A.C.';
    }
    if ($club === 'Les Croupiers R.C.') {
        $club = 'Les Croupiers';
    }
    if ($club === 'Kilmarnock Harrier A.C.') {
        $club = 'Kilmarnock Harriers A.C.';
    }
    if ($club === 'Carrigaline A.C.') {
        $club = 'Carrigaline Road Runners A.C.';
    }
    if ($club === 'Ballyroan') {
        $club = 'Ballyroan, Abbeyleix & District A.C.';
    }
    if ($club === 'Glendalough Harriers & A.C.') {
        $club = 'Glendalough Harriers A.C.';
    }
    if ($club === 'Olympian Youth & A.C.') {
        $club = 'Olympian Youth A.C.';
    }
    if ($club === 'Birmingham Running Athletics & Triathlon Club (BRAT)') {
        $club = 'BRAT Club';
    }
    if ($club === 'Inverness Harriers A A C') {
        $club = 'Inverness Harriers A.C.';
    }
    if ($club === 'Inverness Harriers') {
        $club = 'Inverness Harriers A.C.';
    }
    if ($club === 'B.M.O.H. A.C.') {
        $club = 'B.M.O.H A.C.';
    }
    if ($club === 'Hacketstown Runners A.C.') {
        $club = 'Hacketstown A.C.';
    }
    if ($club === 'An Riocht A.C.') {
        $club = 'An Ríocht A.C.';
    }
    if ($club === 'TÃ­r Chonaill A.C.') {
        $club = 'Tír Chonaill A.C.';
    }
    if ($club === 'AAI_TEST_CLUB') {
        $club = 'AAI Test Club';
    }
    if ($club === 'Ealing Southall & Middx') {
        $club = 'Ealing Southall and Middlesex A.C.';
    }
    if ($club === 'Coventry') {
        $club = 'Coventry Godiva Harriers';
    }
    if ($club === 'Polish Runners Club Ireland') {
        $club = 'Polish Runners Club Ireland';
    }
    if ($club === "Keep 'er Lit") {
        $club = "Keep 'Er Lit";
    }
    if ($club === 'Donore Harriers') {
        $club = 'Donore Harriers A.C.';
    }
    if ($club === 'TinrylA.C.') {
        $club = 'Tinryland A.C.';
    }

    // replace & with and
    $club = str_replace(' & ', ' and ', $club);

    return $club;
}

function getBaseVirtualProduct($conn): int {
    $sql = 'Select id 
            from ' . E4S_TABLE_POSTS . "
            where post_title = '" . E4S_BASE_PRODUCT . "'";
    $result = e4s_queryNoLog($sql);
    $productRow = $result->fetch_assoc();
    $sqlErrorNo = mysqli_errno($conn);
    if ($sqlErrorNo != 0) {
        logSql('Failed to get BaseVirtualProduct', 1);
        return -1;
    }
    return $productRow['id'];
}

function e4sGetGenericUserRow($userRowName, $userKey, $userKeyValue, $useCache) {
    if ($useCache === FALSE || array_key_exists($userRowName, $GLOBALS) === FALSE) {
        $sql = 'select * 
        from ' . E4S_TABLE_USERS . '
        where ' . $userKey . " = '" . $userKeyValue . "'";

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            $userRow = null;
        } else {
            $userRow = $result->fetch_assoc();
        }

        $GLOBALS[$userRowName] = $userRow;
    }
    return $GLOBALS[$userRowName];
}

function e4s_queryNoLog($sql,$useConn = null) {
	$log = false;
//	if ( e4s_getUserID() === 23173 ){
//		$log = true;
//	}
    return e4s_queryWithLog($sql, $log, $useConn);
}

function e4s_query($sql, $conn = null) {
    return e4s_queryWithLog($sql, TRUE, $conn);
}

function e4s_queryWithLog($sql, $log, $useConn = null) {
    global $conn;
//$log = true;
    if (is_null($useConn)) {
        $useConn = $conn;
    }
    $arr = explode(E4S_SQL_DELIM, $sql);
    if (count($arr) === 1) {
        $sqlToRun = $sql;
    } else {
        $sqlToRun = $arr[1];
    }

    if ($log) {
        logTxt($sql);
    }

    $GLOBALS[E4S_LAST_QUERY] = '';
    try {
        $return = mysqli_query($useConn, $sqlToRun);
    } catch (Exception $e) {
//        error_log($sqlToRun);
        exit('>> ' . $e);
    }
    $GLOBALS[E4S_LAST_ID] = $useConn->insert_id;
    $error = mysqli_error($useConn);

    if ($error === '') {
        return $return;
    }
    $GLOBALS[E4S_LAST_QUERY] = $sqlToRun;

    Entry4UIError(9048, $error, 400, '"sql":"' . str_replace("\r\n", '', $sqlToRun) . '"');
    return FALSE;
}

function e4s_getRowsAffected() {
    global $conn;
    return $conn->affected_rows;
}

function isIPBlocked($check = false): bool {
    if ( $check ) {
        $uid = e4s_getUID();
        $sql = 'select uid
            from ' . E4S_TABLE_BANNED . "
            where uid = '" . $uid . "'";
        $result = e4s_queryWithLog($sql, false);
        if ($result->num_rows > 0) {
            return TRUE;
        }
    }
    $url = strtolower($_SERVER['REQUEST_URI']);
    $points = 0;

    if (strpos($url, 'select') !== FALSE) {
        $points += 1;
    }
    if (strpos($url, 'AURORA_VERSION') !== FALSE) {
        $points += 2;
    }
    if (strpos($url, 'JSON_STORAGE_FREE') !== FALSE) {
        $points += 2;
    }
    if (strpos($url, 'BENCHMARK') !== FALSE) {
        $points += 2;
    }
    if (strpos($url, 'information_schema') !== FALSE) {
        $points += 1;
    }
    if (strpos($url, 'union') !== FALSE) {
        $points += 1;
    }
    if (strpos($url, 'char') !== FALSE) {
        $points += 1;
    }
    if (strpos($url, 'updatexml') !== FALSE) {
        $points += 2;
    }
    if (strpos($url, 'schema_name') !== FALSE) {
        $points += 2;
    }
    if (strpos($url, '.js and ') !== FALSE) {
        $points += 2;
    }
    if (strpos($url, '%27%20and') !== FALSE) {
        $points += 2;
    }
    if (strpos($url, '%27x%27') !== FALSE) {
        $points += 2;
    }

    if (strpos($url, 'OnCaT(') !== FALSE) {
        $points += 2;
    }
    if (strpos($url, '/shop/') !== FALSE) {
        $points += 2;
    }
    if (strpos($url, 'make_set') !== FALSE) {
        $points += 2;
    }
    if (strpos($url, '/?add-to-cart=') !== FALSE and strpos($url, '/add-to-cart=') !== FALSE) {
        $points += 2;
    }

    if ($points > 1) {
        e4s_addToBanned();
        return TRUE;
    }
    return FALSE;
}

function e4s_addToBanned() {
    $uid = e4s_getUID();
    $sql = 'insert into ' . E4S_TABLE_BANNED . " (uid, url)
        values('" . $uid . "','" . $_SERVER['REQUEST_URI'] . "')";
    e4s_queryNoLog($sql);
}
function e4s_traceAndExit(){
    $trace = e4s_getTrace();
    $trace = str_replace(E4S_TEXT_TITLE_DELIM, "\n", $trace);
    exit($trace);
}
function e4s_getTrace(): string {
    $traceCnt = 5;
    $output = array();

    $stackTrace = debug_backtrace();
    foreach ($stackTrace as $stack) {
        if ($stack['function'] === 'e4s_getTrace' or $stack['function'] === 'e4s_log' or strpos($stack['function'], 'e4s_query') !== FALSE or strpos($stack['function'], 'log') === 0 or strpos($stack['function'], 'debug') === 0) {
            // Ignore e4s methods that are logging
            continue;
        }
        if (!array_key_exists('file', $stack)) {
            var_dump($stack);
            exit();
        }
        if (strpos($stack['file'], '/wp-') !== FALSE or strpos($stack['file'], '/index.php') !== FALSE) {
            // stop if you get to Wordpress methods
            break;
        }
//        // reduce space used in log by removing full file path
        $file = $stack['file'];
        if (strpos($file, E4S_PATH) !== FALSE) {
            $file = explode(E4S_PATH, $file)[1];
        }
        $line = $stack['function'] . ' at line ' . $stack['line'] . ' in file ' . $file . ' ';
        if (!is_null($stack['args'])) {
            foreach ($stack['args'] as $args) {
                if (gettype($args) !== 'object' and gettype($args) !== 'array') {
                    $line .= '[' . $args . ']';
                } else {
                    $line .= '[' . gettype($args) . ']';
                }
            }
        }

        $output[] = $line;
        if ($traceCnt <= 0) {
            break;
        }
        $traceCnt = $traceCnt - 1;
    }

    return implode(E4S_TEXT_TITLE_DELIM, $output);
}

function logTxt($txt, $level = 1) {
    e4s_log($txt, $level);
}

function logObj($obj) {
    if (gettype($obj) === 'array' || gettype($obj) === 'object') {
        logTxt(json_encode($obj, JSON_NUMERIC_CHECK));
    } else {
        logTxt($obj);
    }
}

function e4s_getUID() {
    $uid = $_SERVER['REMOTE_ADDR'];
    $redirectUid = $uid;
    if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
        $redirectUid = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    if (substr($uid, 0, 6) === '10.0.0') {
        $uid = $redirectUid;
    }
    if ( strpos($uid, ':') !== false){
        $uid = str_replace(':','', $uid);
        $uid = substr($uid,0, 10) . substr($uid,-10);
    }
    return $uid;
}

function e4s_log($txt, $level) {
    if (isIPBlocked(true)) {
        exit('Oops. You have been spotted');
    }
	if ($level < 2) {
		if ( strpos( $txt, 'wp-json/view' ) !== FALSE or strpos( $txt, 'jpg' ) !== FALSE or strpos( $txt, 'png' ) !== FALSE ) {
			// dont log view files ( images on home screen) or videos
			return;
		}
	}
    if ( strpos($txt, 'otd' . E4S_SECURE_TEXT . '/track') !== false) {
        $level = -1;
    }

    if ($txt !== E4S_SUCCESS) {
        $conn = $GLOBALS['logconn'];
        if (is_null($conn)) {
            $conn = $GLOBALS['conn'];
        }
        $userid = E4S_USER_ANON_ID;
        if (isset($GLOBALS[E4S_USER])) {
            $userid = e4s_getUserID();
        }
        global $prevLog;

        // reduce space in log db
        $txt = str_replace(E4S_ROUTE_PATH, '', $txt);
        $sqlErrorNo = mysqli_errno($conn);
        $sqlError = mysqli_error($conn);
        $trace = '';
        $payload = '';
        if ( $level > -1 ) {
            $payload = file_get_contents('php://input');
            if ($level > 0) {
                $trace = addslashes(e4s_getTrace());
            }
        }
        $txt = addslashes($txt);
        $payload = addslashes($payload);
        $uid = e4s_getUID();
        $sql = 'INSERT INTO ' . E4S_TABLE_LOG . ' (id, created, userid, uid, info,trace, payload, parentid) VALUES (NULL, ' . returnNow() . ',' . $userid . ",'" . $uid . "', '" . $txt . "','" . $trace . "','" . $payload . "'," . $GLOBALS[E4S_PARENT_LOG] . ')';
        $conn->query($sql);
        $GLOBALS['E4S_LAST_LOG_ID'] = $conn->insert_id;

        if ($sqlErrorNo != 0) {
            $Error = 'Error on ' . E4S_CURRENT_DOMAIN . ' - ' . $sqlErrorNo . ': ' . $sqlError . "\n\n" . $prevLog;
            $sql = 'INSERT INTO ' . E4S_TABLE_LOG . ' (id, created, userid, uid, info, trace, parentid) VALUES (NULL, ' . returnNow() . ',' . $userid . ",'" . $uid . "', '" . $Error . "','" . $trace . "'," . $GLOBALS[E4S_PARENT_LOG] . ')';
            $conn->query($sql);
//            debugTxt($Error);
            $config = e4s_getConfig();
            if (strtoupper($config['env']) === E4S_ENV_PROD) {
                mail(E4S_ERROR_EMAIL, E4S_CURRENT_DOMAIN . ' : PHP/MySQL Error', $Error);
            }
        }
    }
//    debugTxt($txt);
    $prevLog = $txt;
}

function Entry4_StartTransaction() {
    $conn = $GLOBALS['conn'];
    $conn->autocommit(FALSE);
}

function Entry4_Commit() {
    $conn = $GLOBALS['conn'];
    $conn->commit();
    $conn->autocommit(TRUE);
}

function Entry4_Rollback() {
//    return;
    $conn = $GLOBALS['conn'];
    $conn->rollback();
    $conn->autocommit(TRUE);
}

function returnNow(): string {
    return ' now() ';
//    return " subtime(now(),'1:0:00') ";
}

function e4s_returnTextToSuccess($text): string {
    return "\"data\":\"" . $text . "\"";
}

function setUOMObj(&$row) {
	if ( array_key_exists('uomOptions', $row) ){
		$row['uom'] = json_decode( $row['uomOptions'] );
	}else{
		$row['uom'] = json_decode( $row['uomoptions'] );
	}
    if ($row['uom'] !== null) {
        foreach ($row['uom'] as $key => $value) {
            $row['uom'][$key]->uomType = $row['uomType'];
        }
    }
    unset($row['uomOptions']);
}

// make sure row has uomoptions, split
// 2nd param is the eventName name in the $row object
function getEventNameWithSplit(&$row, $eventName): string {
    setUOMObj($row);

    $useCeText = FALSE;
    if (isset($row['ceoptions']->xiText)) {
        if ($row['ceoptions']->xiText !== '') {
            $useCeText = TRUE;
        }
    }
    if ($useCeText === TRUE) {
        $row[$eventName] .= $row['ceoptions']->xiText;
    } else {
        $split = (float)$row['split'];
        if ($split !== 0.00) {
            $splitStr = ' > ';
            if ($split < 0) {
                $splitStr = ' < ';
            }
			$absSplit = abs($split);
			if ( $row['tf'] === E4S_EVENT_TRACK ){
				$absSplit = resultsClass::getResultFromSeconds($absSplit);
			}
            $splitStr .= $absSplit;
            if ($row['uom'] !== null) {
                $splitStr .= ' ' . $row['uom'][0]->short;
            }
            $row[$eventName] .= '( ' . $splitStr . ' )';
        }
    }

    return $row[$eventName];
}

function debugTxt($txt) {
    global $echo;

    if ($echo != '') {
        echo $txt . '<br>';
    }
}

function convertToCurrency($config, $value) {
    if (strpos($value, '.') === FALSE) {
        return $config['currency'] . $value . '.00';
    }

    $vals = explode('.', $value);
//    echo "Value : [" . $vals[1] . "]<br>";
    if (strlen($vals[1]) === 1) {
        return $config['currency'] . $value . '0';
    }
    return $config['currency'] . $value;
}

function getAgeFromText($startDateStr, $onDateStr, $dateFormat) {
    global $tz;
    $startDate = DateTime::createFromFormat($dateFormat, $startDateStr, $tz);
    $onDate = DateTime::createFromFormat($dateFormat, $onDateStr, $tz);
    return getAgeFromDate($startDate, $onDate);
}

function getAgeFromDate($startDate, $onDate) {
    $interval = $onDate->diff($startDate);
    return (int)$interval->y;
}

function getNoAgeGroupArr() {
    $arr = array();
    $arr['id'] = 0;
    $arr['shortName'] = 'N/A';
    $arr['Name'] = 'No events for athletes age group';
    return [$arr];
}

function getMatchingAgeGroupsInfo($allCompDOBs, $dob) {

    $useVetAgeGroup = null;
    $now = date(E4S_SHORT_DATE);
    $currentAge = getAgeFromText($dob, $now, E4S_SHORT_DATE);
    $currentCompAge = null;
    $useAge = $currentAge;
    $ageGroups = array();

    foreach ($allCompDOBs as $key => $value) {
        if ($currentCompAge === null) {
            $currentCompAge = getAgeFromText($dob, $value['compDate'], E4S_SHORT_DATE);
        }
        if ($dob >= $value['fromDate'] and $dob <= $value['toDate']) {
            $ageGroups[] = $value;
        }
    }

    if ($currentCompAge !== null) {
        $useAge = $currentCompAge;
        if ($useAge < 35) {
            $useAge = 35;
        }
    }

    $returnArr = array();

    if (sizeof($ageGroups) === 0) {
        $returnArr['ageGroups'] = getNoAgeGroupArr();
    } else {
        $getVetGroup = FALSE;
        foreach ($ageGroups as $value) {
            if ($value['Name'] === 'Masters') {
                $getVetGroup = TRUE;
            }
            $value['shortName'] = str_replace('Under ', 'U', $value['Name']);
        }

        $returnArr['ageGroups'] = $ageGroups;
//        has a valid age group, so check if a vet
        if ($getVetGroup) {
            $sql = 'SELECT id, Name FROM ' . E4S_TABLE_AGEGROUPS . "
                   where minAge <= $useAge and MaxAge >= $useAge
                   and keyname like 'VET%'";
            $result = e4s_queryNoLog(basename(__FILE__) . E4S_SQL_DELIM . $sql);

            if ($result->num_rows === 1) {
                $useVetAgeGroup = mysqli_fetch_array($result, TRUE);
            }
        }
    }
    $returnArr['vetAgeGroup'] = $useVetAgeGroup;
    $returnArr['currentAge'] = $currentAge;
    $returnArr['competitionAge'] = $currentCompAge;
    return $returnArr;
}

function getAgeGroupInfo($allCompDOBs, $dob) {
    $ageGroupsInfo = getMatchingAgeGroupsInfo($allCompDOBs, $dob);

    if (sizeof($ageGroupsInfo) > 0) {
        // default. set to first one
        $ageGroupsInfo['ageGroup'] = $ageGroupsInfo['ageGroups'][0];
    }
    $config = e4s_getConfig();
    $checkAO = $config['defaultao']['code'];

    foreach ($ageGroupsInfo['ageGroups'] as $ageGroup) {
//        check if there is a base age group
        // Not sure why an agegroup would not have an options field ?????
        if (array_key_exists('options', $ageGroup)) {
            $agoptions = e4s_getOptionsAsObj($ageGroup['options']);
//        agoptions is an array of aocode options
            foreach ($agoptions as $agoption) {

                if ($agoption->aocode === $checkAO) {
                    // got the matching ao code, so check for base
                    // base = 1. set and get out base = 2, set but keep going looking for a 1 ( Masters / Vet )
                    if (isset($agoption->base) and $agoption->base > 0) {
                        $ageGroupsInfo['ageGroup'] = $ageGroup;
                        if ($agoption->base === 1) {
                            break 2;
                        }
                    }
                }
            }
        }
    }

    return $ageGroupsInfo;
}

//defunct ?
function getAgeGroup($compid, $dob) {
    global $tz;
    $compObj = e4s_getCompObj($compid);
    $compRow = $compObj->getRow();
// Get Age Group for Athlete
    $agSql = 'select * 
          from ' . E4S_TABLE_AGEGROUPS . ' 
          where id in (select distinct(agegroupid) from ' . E4S_TABLE_COMPEVENTS . " WHERE compid = $compid) order by minAge";
//    $sql = "select ag.*
//            from " . E4S_TABLE_AGEGROUPS . " ag
//            where id in (" . implode(",",getCompAgeGroups($compid, true)) . ")
//            order by ag.minAge";
    $result = e4s_queryNoLog($agSql);
    $ageGroupsForComp = mysqli_fetch_all($result, MYSQLI_ASSOC);

    $now = date(E4S_SHORT_DATE);
    $currentAge = getAgeFromText($dob, $now, E4S_SHORT_DATE);
    $useAgeGroup = null;
    $useVetAgeGroup = null;
    $compDate = DateTime::createFromFormat(E4S_SHORT_DATE, $compObj->getDate(), $tz);
    $compUseYear = $compDate->format('Y');
    $compUseMonth = $compDate->format('m');
    $compUseDay = $compDate->format('d');

    foreach ($ageGroupsForComp as $ageGroup) {
        $maxAtDay = (int)$ageGroup['AtDay'];
        $minAtDay = $ageGroup['minAtDay'];
        if ($minAtDay === Null) {
            $minAtDay = $maxAtDay;
        }
        $minAtDay = (int)$minAtDay;

        if ($minAtDay === 0) {
            $minAtDay = $compUseDay;
        }
        if ($maxAtDay === 0) {
            $maxAtDay = $compUseDay;
        }
        $maxAtDay = (int)$maxAtDay;

        $maxAtMonth = (int)$ageGroup['AtMonth'];
        $minAtMonth = $ageGroup['minAtMonth'];
        if ($minAtMonth === Null) {
            $minAtMonth = $maxAtMonth;
        }
        $minAtMonth = (int)$minAtMonth;

        if ($minAtMonth === 0) {
            $minAtMonth = $compUseMonth;
        }
        if ($maxAtMonth === 0) {
            $maxAtMonth = $compUseMonth;
        }

        $useMaxYear = (int)$compUseYear + (int)$ageGroup['year'];
        $useMinYear = $useMaxYear;
        if ($ageGroup['minYear'] !== Null) {
            $useMinYear = (int)$compUseYear + (int)$ageGroup['minYear'];
        }
        $minDate = $useMinYear . '-' . $minAtMonth . '-' . $minAtDay;
        $maxDate = $useMaxYear . '-' . $maxAtMonth . '-' . $maxAtDay;

        $ageAtMinDate = getAgeFromText($dob, $minDate, E4S_SHORT_DATE);
        $ageAtMaxDate = getAgeFromText($dob, $maxDate, E4S_SHORT_DATE);

        if ($ageAtMinDate >= (int)$ageGroup['minAge'] and $ageAtMaxDate <= (int)$ageGroup['MaxAge']) {
            $useAgeGroup = $ageGroup;
            $useAgeGroup['shortName'] = str_replace('Under ', 'U', $useAgeGroup['Name']);

            if ($useAgeGroup['Name'] === 'Masters') {
                $sql = 'SELECT id, Name FROM ' . E4S_TABLE_AGEGROUPS . " 
                                    where minAge <= $ageAtMinDate and MaxAge >= $ageAtMaxDate 
                                    and keyname like 'VET%'";
                $result = e4s_queryNoLog(basename(__FILE__) . E4S_SQL_DELIM . $sql);
                if ($result->num_rows === 1) {
                    $useVetAgeGroup = mysqli_fetch_array($result, TRUE);
                }
            }
            break;
        }
    }

    $returnArr = array();
    if ($useAgeGroup !== null) {
        $returnArr['ageGroup'] = $useAgeGroup;
    } else {
        $returnArr['ageGroup'] = json_decode('{"id":0,"Name":"No events available"}');
    }
    $returnArr['vetAgeGroup'] = $useVetAgeGroup;
    $returnArr['currentAge'] = $currentAge;
    return $returnArr;
}

function getAgeGroupForCeId($useAgeGroupID, $compId, $dob) {
	$cacheKey = E4S_CACHE_AGE_GROUP . ":" . $compId . ":" . $useAgeGroupID;
	if ( array_key_exists($cacheKey, $GLOBALS) ){
		return $GLOBALS[$cacheKey];
	}
    global $tz;

    $compObj = e4s_getCompObj($compId);
    $compRow = $compObj->getRow();
    $compDate = DateTime::createFromFormat(E4S_SHORT_DATE, $compRow['Date'], $tz);
    $compUseYear = $compDate->format('Y');
    $compUseMonth = $compDate->format('m');
    $compUseDay = $compDate->format('d');

    $sql = 'select ag.*
            from ' . E4S_TABLE_AGEGROUPS . ' ag
            where id = ' . $useAgeGroupID;

    $result = e4s_queryNoLog($sql);
    $ageGroupsForComp = mysqli_fetch_all($result, MYSQLI_ASSOC);

    $now = date(E4S_SHORT_DATE);
    $currentAge = getAgeFromText($dob, $now, E4S_SHORT_DATE);
    $useAgeGroup = null;
    $useVetAgeGroup = null;
    foreach ($ageGroupsForComp as $ageGroup) {
//        $minAge = (int)$ageGroup['minAge'];
//        $maxAge = (int)$ageGroup['MaxAge'];
        $maxAtDay = (int)$ageGroup['AtDay'];
        $minAtDay = $ageGroup['minAtDay'];
        if ($minAtDay === Null) {
            $minAtDay = $maxAtDay;
        }
        $minAtDay = (int)$minAtDay;

        if ($minAtDay === 0) {
            $minAtDay = $compUseDay;
        }
        if ($maxAtDay === 0) {
            $maxAtDay = $compUseDay;
        }
        $maxAtDay = (int)$maxAtDay;

        $maxAtMonth = (int)$ageGroup['AtMonth'];
        $minAtMonth = $ageGroup['minAtMonth'];
        if ($minAtMonth === Null) {
            $minAtMonth = $maxAtMonth;
        }
        $minAtMonth = (int)$minAtMonth;

        if ($minAtMonth === 0) {
            $minAtMonth = $compUseMonth;
        }
        if ($maxAtMonth === 0) {
            $maxAtMonth = $compUseMonth;
        }

        $useMaxYear = (int)$compUseYear + (int)$ageGroup['year'];
        $useMinYear = $useMaxYear;
        if ($ageGroup['minYear'] !== Null) {
            $useMinYear = (int)$compUseYear + (int)$ageGroup['minYear'];
        }
        $minDate = $useMinYear . '-' . $minAtMonth . '-' . $minAtDay;
        $maxDate = $useMaxYear . '-' . $maxAtMonth . '-' . $maxAtDay;

        $ageAtMinDate = getAgeFromText($dob, $minDate, E4S_SHORT_DATE);
        $ageAtMaxDate = getAgeFromText($dob, $maxDate, E4S_SHORT_DATE);

        //if ($ageAtMinDate >= (int)$ageGroup['minAge'] and $ageAtMaxDate <= (int)$ageGroup['MaxAge'] ) {
        $useAgeGroup = $ageGroup;
        $useAgeGroup['shortName'] = str_replace('Under ', 'U', $useAgeGroup['Name']);

        if ($useAgeGroup['Name'] === 'Masters') {
            $result = e4s_queryNoLog('SELECT id, Name FROM ' . E4S_TABLE_AGEGROUPS . " 
                                    where minAge <= $ageAtMinDate and MaxAge >= $ageAtMaxDate 
                                    and name like 'VET%'");
            if ($result->num_rows === 1) {
                $useVetAgeGroup = mysqli_fetch_array($result, TRUE);
            }
        }
        break;
    }

    $returnArr = array();
    if ($useAgeGroup !== null) {
        $returnArr['ageGroup'] = $useAgeGroup;
    } else {
        $returnArr['ageGroup'] = json_decode('{"id":0,"Name":"No events available"}');
    }
    $returnArr['vetAgeGroup'] = $useVetAgeGroup;
    $returnArr['currentAge'] = $currentAge;
	$GLOBALS[$cacheKey] = $returnArr;
    return $returnArr;
}

function getCompAreaIds($compid, $applyFilter, $includeTop, $level, $userClub, $userArea) {
    $areaidsArr = getCompAreaIdsArr($compid, $applyFilter, $includeTop, $level, $userClub, $userArea);
    return implode(',', $areaidsArr);
}

function getCompAreaIdsArr($compid, $applyFilter, $includeTop, $level, $userClub, $userArea) {
    global $filterCounty;
    global $filterRegion;

    $useRow = $userClub;
    if ($useRow['Clubname'] === '') {
        $useRow = $userArea;
    } else {
        $useRow['entitylevel'] = E4S_CLUB_ENTITY;
    }

    // Get Competition Entity/Area
    if ($compid > 0) {
        $compRow = getCompArea($compid);

        if ($useRow['entitylevel'] === -1 || ($useRow['entitylevel'] > $compRow['entitylevel'] and $compRow['entitylevel'] !== -1)) {
            $useRow = $compRow;
        }
    }

    $areaIds = array();
    // ensure $userRow['areaid'] is numeric
    if ((int)$useRow['areaid'] === 0) {
        return $areaIds;
    }
    $name = '';

    if ($filterCounty !== '' and $useRow['entitylevel'] === 3) {
        $name = $filterCounty . '%';
    }
    if ($filterRegion !== '' and $useRow['entitylevel'] === 4) {
        $name = $filterRegion . '%';
    }

    $areaSql1 = 'Select id
            from ' . E4S_TABLE_AREA . '
            where parentid = ' . $useRow['areaid'];
    if ($name !== '' and $applyFilter) {
        $areaSql1 .= " and name like '$name'";
    }

    $areaSql2 = 'Select id
            from ' . E4S_TABLE_AREA . '
            where parentid in ( Select id
                                from ' . E4S_TABLE_AREA . '
                                where parentid = ' . $useRow['areaid'] . ')';
    if ($filterCounty !== '' and $applyFilter) {
        $areaSql2 .= "and name like '" . $filterCounty . "%'";
    }

    if ($includeTop) {
        $areaIds[] = $useRow['areaid'];
    }

    $result = e4s_queryNoLog($areaSql1);

    if ($result->num_rows > 0) {
        $areaRows = mysqli_fetch_all($result, MYSQLI_ASSOC);

        foreach ($areaRows as $areaRow) {
            $areaIds[] = $areaRow['id'];
        }

        if ($level > 1 || $level === 0) {
            $result = e4s_queryNoLog($areaSql2);

            if ($result->num_rows > 0) {
                $areaRows = mysqli_fetch_all($result, MYSQLI_ASSOC);

                foreach ($areaRows as $areaRow) {
                    $areaIds[] = $areaRow['id'];
                }
            }
        }
    }
    return $areaIds;
}

function getUserParentIDs($clubAndAreaIds) {
    $arr = getUserParentIDsArr($clubAndAreaIds);
    $return = '';
    $sub = '';
    foreach ($arr as $value) {
        $return .= ($sub . $value);
        $sub = ',';
    }
    return $return;
}

// Given an array of ids, return an array with ALL parents
function getAllParentIDs($ids) {
    $returnArr = array();
    $cnt = 0;

    // We have a club/area id
    foreach ($ids as $parentId) {
        if (!is_null($parentId)) {
            $returnArr[$parentId] = $parentId;
        }
    }

    $parentIds = getParentIDsArr($ids);
    foreach ($parentIds as $parentId) {
        if (!is_null($parentId)) {
            $returnArr[(int)$parentId] = (int)$parentId;
        }
    }
    while (!is_null($parentIds[0]) and $cnt < 5) {
        $parentIds = getParentIDsArr($parentIds);
        foreach ($parentIds as $parentId) {
            if (!is_null($parentId)) {
                $returnArr[(int)$parentId] = (int)$parentId;
            }
        }
        $cnt = $cnt + 1;
    }
    return $returnArr;
}

function getParentIDsArr($clubAndAreaIds) {

    $ids = '';
    $sep = '';
    foreach ($clubAndAreaIds as $value) {
        if (!is_null($value)) {
            $ids .= ($sep . $value);
            $sep = ',';
        }
    }

    $sql = 'SELECT a.parentid parent
            FROM ' . E4S_TABLE_AREA . ' a, 
                 ' . E4S_TABLE_ENTITY . ' e 
            where a.id in (' . $ids . ') 
            and   a.entityid = e.id 
            order by e.level';
    $result = e4s_queryNoLog($sql);
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    $return = array();
    foreach ($rows as $row) {
        $return[] = $row['parent'];
    }
    return $return;
}

function getUserParentIDsArr($clubAndAreaIds) {

    $ids = '';
    $sep = '';
    foreach ($clubAndAreaIds as $value) {
        $ids .= ($sep . $value);
        $sep = ',';
    }
    $sql = 'SELECT ifnull(a.parentid, a.id) parent
            FROM ' . E4S_TABLE_AREA . ' a, 
                 ' . E4S_TABLE_ENTITY . ' e 
            where a.id in (' . $ids . ') 
            and   a.entityid = e.id 
            order by e.level';
    $result = e4s_queryNoLog($sql);
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    $return = array();
    foreach ($rows as $row) {
        $return[] = $row['parent'];
    }
    return $return;
}

function getUseEventPrice($priceId) {
    // get Price Row
    $sql = 'SELECT * 
            FROM ' . E4S_TABLE_EVENTPRICE . ' 
            WHERE id = ' . $priceId;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows === 0) {
        Entry4UIError(2001, 'Unable to find price record for event', 404);
    }

    $priceRow = $result->fetch_assoc();
    $priceRow = json_decode(json_encode($priceRow, JSON_NUMERIC_CHECK), TRUE);
    $usePrice = $priceRow['price'];
    $salePrice = $priceRow['saleprice'];
    $saleDate = $priceRow['saleenddate'];
    // Active Price
    if ($saleDate != '') {
        $saleDate = strtotime($saleDate);
        $now = time();
        if ($saleDate > $now) {
            $usePrice = $salePrice;
        }
    }

    $priceRow['usePrice'] = $usePrice;

    $pOptions = e4s_addDefaultPriceOptions($priceRow['options']);
    if ($pOptions->discount->count > 0) {
        $entryObj = new entryClass();
        $priceRow = $entryObj->checkAndSetPriceDiscount($priceRow);
    }

    return $priceRow;
}

function getCompArea($compid) {
    $blankRow = array();
    $blankRow['areaid'] = -1;
    $blankRow['entityid'] = -1;
    $blankRow['areaname'] = '';
    $blankRow['areashortname'] = '';
    $blankRow['entitylevel'] = -1;
    $blankRow['entityName'] = '';
    $blankRow['entityparentid'] = -1;

    $compObj = e4s_getCompObj($compid);
    $compRow = $compObj->getRow();

    if ((int)$compRow['areaid'] === 0) {
        return $blankRow;
    }
    $compSql = 'Select e.level entitylevel, e.name entityName, e.parentid entityparentid, a.id areaid, a.entityid, a.name areaname, a.shortname areashortname
            from ' . E4S_TABLE_COMPETITON . ' c,
                 ' . E4S_TABLE_ENTITY . ' e,
                 ' . E4S_TABLE_AREA . " a
            where c.id = $compid
            and   e.id = a.entityid
            and   a.id = c.areaid";

    $result2 = e4s_queryNoLog($compSql);
    return $result2->fetch_assoc();
}

function getAgeGroupAndDOBs($compRow, $ageGroupRow) {
    global $tz;
    $dobs = array();

    $dobs['compDate'] = $compRow['Date'];
    $dobs['agid'] = $ageGroupRow['id'];
    $dobs['minAge'] = $ageGroupRow['minAge'];

    $compDate = DateTime::createFromFormat(E4S_SHORT_DATE, $compRow['Date'], $tz);
    $compYear = (int)$compDate->format('Y');
    $compYearWithFactor = (int)$compDate->format('Y') + (int)$compRow['yearFactor'];
    $compMonth = (int)$compDate->format('m');
    $compDay = (int)$compDate->format('d');

    // get Min Age
    $useMinDay = (int)$ageGroupRow['minAtDay'];
    if ($ageGroupRow['minAtDay'] === null) {
        $useMinDay = (int)$ageGroupRow['AtDay'];
    }
    if ($useMinDay === 0) {
        $useMinDay = $compDay;
    }

    $useMinMonth = (int)$ageGroupRow['minAtMonth'];
    if ($ageGroupRow['minAtMonth'] === null) {
        $useMinMonth = (int)$ageGroupRow['AtMonth'];
    }
    if ($useMinMonth === 0) {
        $useMinMonth = $compMonth;
    }

    $useMinYear = (int)$ageGroupRow['minYear'];
    if ($ageGroupRow['minYear'] === null) {
        $useMinYear = (int)$ageGroupRow['year'];
    }
    if ($useMinDay === 0 and $useMinMonth === 0) {
        $useMinYear = $compYear + $useMinYear;
    } else {
        $useMinYear = $compYearWithFactor + $useMinYear;
    }

    $minDate = ($useMinYear - (int)$ageGroupRow['minAge']) . '-' . $useMinMonth . '-' . $useMinDay;
//    $dobs['todate'] = $minDate;
    $date = new DateTime($minDate);
//    $date->add(new DateInterval('P1D'));
    $dobs['toDate'] = $date->format(E4S_SHORT_DATE);
    // get Max Age
    $useMaxDay = (int)$ageGroupRow['AtDay'];

    if ($useMaxDay === 0) {
        $useMaxDay = $compDay;
    }

    $useMaxDay = (int)$useMaxDay;

    if ($useMaxDay < 10) {
        $useMaxDay = '0' . $useMaxDay;
    }

    $useMaxMonth = (int)$ageGroupRow['AtMonth'];

    if ($useMaxMonth === 0) {
        $useMaxMonth = $compMonth;
    }
    $useMaxYear = (int)$ageGroupRow['year'];
    if ($useMaxDay === 0 and $useMaxMonth === 0) {
        $useMaxYear = $compYear + $useMaxYear;
    } else {
        $useMaxYear = $compYearWithFactor + $useMaxYear;
    }

    $maxDate = ($useMaxYear - (int)$ageGroupRow['MaxAge'] - 1) . '-' . $useMaxMonth . '-' . $useMaxDay;
    $date = new DateTime($maxDate);
    $date->add(new DateInterval('P1D'));
    $dobs['fromDate'] = $date->format(E4S_SHORT_DATE);

    return $dobs;
}

function getCompAgeGroups($compObj, $includeUpscale) {
//    set @options = concat('{"xiText":" (Female U11)","ageGroups":[{"ageGroup":', @U14,'}],"minAgeGroupCnt":2}');
    $ceObjs = $compObj->getCeObjs();

    $idsArr = array();
    foreach ($ceObjs as $ceObj) {
        $checkAgeGroup = $ceObj->ageGroupId;
        if (!in_array($checkAgeGroup, $idsArr)) {
            $idsArr[] = $checkAgeGroup;
        }
        if ($includeUpscale) {
            $options = $ceObj->options;

            if (isset($options->ageGroups)) {
                $ageGroups = $options->ageGroups;
                foreach ($ageGroups as $ageGroup) {
                    if (isset($ageGroup->ageGroup)) {
                        $checkAgeGroup = (int)$ageGroup->ageGroup;
                        if (!in_array($checkAgeGroup, $idsArr)) {
                            $idsArr[] = $checkAgeGroup;
                        }
                    }
                    if (isset($ageGroup->id)) {
                        $checkAgeGroup = (int)$ageGroup->id;
                        if (!in_array($checkAgeGroup, $idsArr)) {
                            $idsArr[] = $checkAgeGroup;
                        }
                    }
                }
            }
        }
    }
    return $idsArr;
}

// Get All age group DOBs for a competition
// return array with agegroupid a the key
// dob in format of $GLOBALS['useDateFormat']
$allAgs = null;
function e4s_getAllAgs(){
	global $allAgs;
	$sql = 'select ag.*
                from ' . E4S_TABLE_AGEGROUPS . '  ag
                order by ag.minAge';

	$ageGroupsResult = e4s_queryNoLog('e4s_getAllAgs' . E4S_SQL_DELIM . $sql);
	$ageGroupRows = mysqli_fetch_all($ageGroupsResult, MYSQLI_ASSOC);

	$allAgs = array();
	foreach ($ageGroupRows as $index => $ageGroupRow) {
		$ageGroupRow['options'] = e4s_getOptionsAsObj($ageGroupRow['options']);
		$ageGroupRow = e4s_normaliseAgeRow($ageGroupRow);
		$ageGroupRow['Name'] = e4s_getVetDisplay($ageGroupRow['Name'], 'O ');
		// V2 uses lowercase
		$ageGroupRow['name'] = $ageGroupRow['Name'];
		$ageGroupRow['shortName'] = str_replace('Under ', 'U', $ageGroupRow['Name']);
		$allAgs[$ageGroupRow['id']] = $ageGroupRow;
	}
}
function getAllCompDOBs($compid) {
    $compObj = e4s_getCompObj($compid);
    $compRow = $compObj->getRow();
	global $allAgs;
    $ags = getCompAgeGroups($compObj, TRUE);

    if (!empty($ags)) {
	    if ( is_null($allAgs) ){
		    e4s_getAllAgs();
	    }

        $ageGroupAndDOBs = array();
		foreach ( $ags as $agId ) {
			if (array_key_exists($agId, $allAgs)) {
				$ageGroupRow                                                           = $allAgs[ $agId ];
				$compAgeGroup                                                          = getAgeGroupAndDOBs( $compRow, $ageGroupRow );
				$ageGroupAndDOBs[ $compAgeGroup['minAge'] . '_' . $ageGroupRow['id'] ] = array_merge( $compAgeGroup, $ageGroupRow );
			}
		}

        return $ageGroupAndDOBs;
    }
    return array();
}
function getAllCompDOBsOrig($compid) {
    $compObj = e4s_getCompObj($compid);
    $compRow = $compObj->getRow();

    $ags = getCompAgeGroups($compObj, TRUE);

    if (!empty($ags)) {
        $sql = 'select ag.*
                from ' . E4S_TABLE_AGEGROUPS . '  ag
                where id in (' . implode(',', $ags) . ')
                order by ag.minAge';

        $ageGroupsResult = e4s_queryNoLog('getAllCompDOBs' . E4S_SQL_DELIM . $sql);
        $ageGroupRows = mysqli_fetch_all($ageGroupsResult, MYSQLI_ASSOC);

        $ageGroupAndDOBs = array();
        foreach ($ageGroupRows as $index => $ageGroupRow) {
            $ageGroupRow['options'] = e4s_getOptionsAsObj($ageGroupRow['options']);
            $ageGroupRow = e4s_normaliseAgeRow($ageGroupRow);
            $compAgeGroup = getAgeGroupAndDOBs($compRow, $ageGroupRow);
            $ageGroupRow['Name'] = e4s_getVetDisplay($ageGroupRow['Name'], 'O ');
            // V2 uses lowercase
            $ageGroupRow['name'] = $ageGroupRow['Name'];
            $ageGroupRow['shortName'] = str_replace('Under ', 'U', $ageGroupRow['Name']);
            $ageGroupAndDOBs[$compAgeGroup['minAge'] . '_' . $ageGroupRow['id']] = array_merge($compAgeGroup, $ageGroupRow);
        }
        return $ageGroupAndDOBs;
    }
    return array();
}

function e4s_normaliseAgeRow($row) {
    $row['id'] = (int)$row['id'];
    if (!is_null($row['MaxAge'])) {
        $row['MaxAge'] = (int)$row['MaxAge'];
    }
    if (!is_null($row['AtDay'])) {
        $row['AtDay'] = (int)$row['AtDay'];
    }
    if (!is_null($row['AtMonth'])) {
        $row['AtMonth'] = (int)$row['AtMonth'];
    }
    if (!is_null($row['year'])) {
        $row['year'] = (int)$row['year'];
    }
    if (!is_null($row['minAge'])) {
        $row['minAge'] = (int)$row['minAge'];
    }
    if (!is_null($row['minAtDay'])) {
        $row['minAtDay'] = (int)$row['minAtDay'];
    }
    if (!is_null($row['minAtMonth'])) {
        $row['minAtMonth'] = (int)$row['minAtMonth'];
    }
    if (!is_null($row['minYear'])) {
        $row['minYear'] = (int)$row['minYear'];
    }
    return $row;
}

function getUserSchedInfoExpand() {
    $key = 'e4s_schedInfoState';
    $userid = e4s_getUserID();

// update users pagesize
    $sql = 'select * from ' . E4S_TABLE_USERMETA . '
                where user_id = ' . $userid . "
                and meta_key = '" . $key . "'";
    $result = e4s_queryNoLog(basename(__FILE__) . E4S_SQL_DELIM . $sql);
    if ($result->num_rows === 0) {
        $sql = 'Insert into ' . E4S_TABLE_USERMETA . ' (user_id, meta_key, meta_value)
                    values (' . $userid . ",'" . $key . "','0')";
        e4s_queryNoLog(basename(__FILE__) . E4S_SQL_DELIM . $sql);
        return 1;
    } else {
        $row = $result->fetch_assoc();
        if ($row['meta_value'] === '1') {
            return 1;
        }
    }
    return 0;
}

//Email Functions

function Entry4_mailHeader($emailType, $sendBcc = TRUE) {
    $emailType = $emailType ? $emailType : 'Entries';
    $headers = array('Content-Type: text/html; charset=UTF-8');

    $headers[] = 'From: ' . $emailType . ' <' . $emailType . '@' . E4S_CURRENT_DOMAIN . '>';

    if ($sendBcc) {
        $headers[] = 'Bcc:' . E4S_SUPPORT_EMAIL;
    }

    return $headers;
}

function Entry4_emailFooter($email = '', $subsEmail = FALSE) {
    $content = '<br><br>Thanks for using <b>Entry4Sports</b>';
    $content .= "<br><img alt='E4SLogo' src=\"https://" . E4S_CURRENT_DOMAIN . E4S_PATH . "/css/images/e4s_logo_small.jpg\">";
    if ($subsEmail and $email !== '') {
        $content .= '<br><br><br>' . "To unsubscribe, please visit subscriptions <a href='https://" . E4S_CURRENT_DOMAIN . '/subscriptions?email=' . $email . '&code=' . encodeEmail($email) . "'>HERE</a>.";
    }
    return $content;
}

function e4s_returnStdLocationRow($row) {
    if (!is_null($row)) {
        $row['address1'] = addslashes($row['Address1']);
        $row['address2'] = addslashes($row['Address2']);
        $row['town'] = addslashes($row['Town']);
        $row['county'] = addslashes($row['County']);
        $row['name'] = addslashes($row['location']);
        $row['id'] = (int)$row['id'];
        unset($row['Address1']);
        unset($row['Address2']);
        unset($row['Town']);
        unset($row['County']);
        unset($row['location']);
        foreach ($row as $key => $value) {
            if (is_null($value)) {
                $row[$key] = '';
            }
        }
    }

    return $row;
}

function getLocsForOrg($id) {
    $locSql = 'select l.* 
                    from ' . E4S_TABLE_COMPETITON . ' c,
                         ' . E4S_TABLE_LOCATION . ' l
                    where c.compclubid = ' . $id . '
                    and   c.locationid = l.id';
    $locResult = e4s_queryNoLog($locSql);
    $locRows = $locResult->fetch_all(MYSQLI_ASSOC);
    $useLocRows = array();
    foreach ($locRows as $locRow) {
        $useLocRows[$locRow['id']] = e4s_returnStdLocationRow($locRow);
    }
    return $useLocRows;
}

function e4s_getVetDisplay($ageGroupName, $gender) {
//    echo "\nInbound : " . $ageGroupName . "\n";
    $config = e4s_getConfig();

    if (strtolower($config['vetDisplay']) === 'gender') {
        $show = 'O';
        if (!is_null($gender)) {
            $show = $gender;
        }
        $ageGroupName = str_replace('VET ', $show, $ageGroupName);
//        echo "\nSetting : " . $ageGroupName . "\n";
    }
//    echo "\nreturning : " . $ageGroupName . "\n";
    return $ageGroupName;
}

function encodeEmail($email) {
    $arrCnt = explode('.', $email);
    $emailArr = explode('@', $email);
    $left = strlen($emailArr[0]);
    $right = strlen($emailArr[1]);
    $code = $left * $right;
    $code = $code * 5 * sizeof($arrCnt);
    return $code;
}

function e4s_check_xss() {
    class e4s_Rest_Request extends WP_REST_Request {
        public function e4s_GET() {
            return $this->params['GET'];
        }

        public function e4s_POST() {
            return $this->params['POST'];
        }

        public function e4s_Params() {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                return $this->e4s_POST();
            }
            if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                return $this->e4s_GET();
            }
            return null;
        }
    }

    $e4sRestRequestObj = new e4s_Rest_Request();
    try {
        e4s_check_xss_parms($e4sRestRequestObj->e4s_Params());
    } catch (Exception $e) {
        echo 'Message: ' . $e->getMessage();
    }
}

function e4s_check_xss_parms($parms) {
    if (is_null($parms) or $parms === '') {
        Entry4UIError(8996, 'System Unavailable 9996');
    }
    foreach ($parms as $key => $value) {
        if ($value !== htmlentities($value)) {
            Entry4UIError(8997, 'System Unavailable : ' . $key);
        }
    }
}

function e4s_left($str, $length) {
    return substr($str, 0, $length);
}

function e4s_right($str, $length) {
    return substr($str, -$length);
}

function intToBoolean($val) {
    if ($val === 0 or $val === '0' or $val === 'false' or $val === FALSE) {
        return FALSE;
    }
    return TRUE;
}

function e4s_isoToDate($isoDate) {
    return new DateTime($isoDate);
}

function e4s_sql_to_iso($date) {
    if (is_null($date) or $date === '') {
        return null;
    }
    return str_replace(' ', 'T', $date) . e4s_getOffset($date);
}

function e4s_dateOnly($date) {
    if (strpos($date, 'T')) {
        $date = explode('T', $date)[0];
    }
    if (strpos($date, ' ')) {
        $date = explode(' ', $date)[0];
    }
    return $date;
}

function e4s_iso_to_sql($date) {
//    echo "date := " . $date;
    $date = str_replace('T', ' ', $date);
    $pos = strpos($date, '+');
    if ($pos > -1) {
        $date = e4s_left($date, $pos);
    }
    $test = explode(':', $date);
    if (sizeof($test) === 2) {
        $date .= ':00';
    }
//    echo "date := " . $date;
    return $date;
}

function renameBuilderField(&$row, $from, $to) {
    renameBuilderFieldFromObj($row, $row, $from, $to);
}

function renameBuilderFieldFromObj(&$fromRow, &$torow, $from, $to) {
    if (array_key_exists($from, $fromRow)) {
        $torow[$to] = $fromRow[$from];
        unset ($fromRow[$from]);
    }
}

function isCountryUser($userLevels, $entitylevels) {
    if (array_key_exists('Country', $entitylevels)) {
        $countryLevel = $entitylevels['Country'];
        if (array_key_exists('' . $countryLevel, $userLevels)) {
//            addDebug("Country User");
            return TRUE;
        }
    }
    if (isE4SUser()) {
        return TRUE;
    }
    return FALSE;
}

function getTargetTable($srcTable, $srcPrefix, $targetPrefix) {
    $targetTable = $srcTable;
    $targetTable = str_replace($srcPrefix, $targetPrefix, $targetTable);

    return $targetTable;
}

function e4s_getTargetEnv($aoCode) {
    $sql = 'select *
            from ' . E4S_TABLE_AO . "
            where code = '" . $aoCode . "'";
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows === 1) {
        $row = $result->fetch_assoc();
        return $row['prefix'];
    }
    return '-1';
}

function isAthleteRegistered($athleteRow, $checkExpired = true) {

    if ($athleteRow['URN'] === '' || is_null($athleteRow['URN'])) {
//  Athlete is NOT registered
        return FALSE;
    }
    if ( $checkExpired ){
        $nowStr = date('Y-m-d');
        $regDate = $athleteRow['activeEndDate'];
        if ((is_null($regDate) || $regDate < $nowStr)) {
            // Registration expired
            return FALSE;
        }
    }

//  Athlete IS registered
    return TRUE;
}

function hasUnSafeSearchchars($search) {
    if (strpos($search, "'") !== FALSE) {
        return TRUE;
    }
    if (strpos($search, '`') !== FALSE) {
        return TRUE;
    }
    if (strpos($search, ' ') !== FALSE) {
        return TRUE;
    }
    return FALSE;
}

function getSafeSearchText($search) {
    $search = str_replace("'", '', $search);
    $search = str_replace('`', '', $search);
    $search = str_replace(' ', '', $search);
    return $search;
}

function getSafeSQLSearchText($sql) {
    return 'replace(replace(replace(' . $sql . ",' ',''),'\'',''),'\`','')";
}

function e4s_checkFinanceReportAccess() {
    if (userHasPermission(PERM_FINANCE, null, null) === FALSE) {
        exit('You do not have permission to see the financial information for this competition');
    }
}

function e4s_removeEnsureString($txt) {
    return str_replace(E4S_ENSURE_STRING, '', $txt);
}
function e4s_removeDoubleDecoded($txt){
    return str_replace("\\\\'", "'", $txt);
}
function e4s_removeEnsureBoolean($txt) {
    $txt = str_replace('"' . E4S_ENSURE_BOOLEAN . '1"', 'true', $txt);
    $txt = str_replace('"' . E4S_ENSURE_BOOLEAN . '0"', 'false', $txt);
    return $txt;
}

function e4s_ensureString($txt) {
    return E4S_ENSURE_STRING . e4s_removeEnsureString($txt);
}

function e4s_ensureBoolean($bool) {
    return E4S_ENSURE_BOOLEAN . $bool;
}

function e4s_encode($obj) {
    $txt = json_encode($obj, JSON_NUMERIC_CHECK);
    $txt = e4s_removeEnsureBoolean($txt);
    $txt = e4s_removeEnsureString($txt);
    $txt = e4s_removeDoubleDecoded($txt);
    return $txt;
}

// take an array with integers as strings and return a data type with true integers
function e4s_decode($arr, $datatype) {
    $arr = e4s_getDataAsType($arr, E4S_OPTIONS_OBJECT);
    $str = e4s_encode($arr);
    return e4s_getDataAsType($str, $datatype);
}

function e4s_getEntityKey($level,$id){
    return $level . ':' . $id;
}
function e4s_doesUserHaveEntity($entity){
    $user = $GLOBALS[E4S_USER];
    $retval = false;
    foreach($user->clubs as $club){
        if ( e4s_getEntityKey($club->entityLevel, $club->id) === $entity ) {
            $retval = true;
            break;
        }
    }
    if ( !$retval ){
        foreach($user->areas as $area){
            $id = 0;
            if ( isset($area->id) ){
                $id = $area->id;
            }elseif ( isset($area->areaid) ){
                $id = $area->areaid;
            }
            if ( e4s_getEntityKey($area->entityLevel, $id) === $entity ) {
                $retval = true;
                break;
            }
        }
    }
    return $retval;
}
function e4s_getOffset($date) {
//    Dates go forward last Sunday in March
//    Fall back last Sunday in Oct
    // sql SET @@global.time_zone = '+00:00';
    $NoOffset = '+00:00';
    $addOffset = '+01:00';
    $delim = '-';

    if ($date === '') {
        return $NoOffset;
    }
    $dateArr = explode(' ', $date)[0];
    $dateArr = explode($delim, $dateArr);
    $dateYY = $dateArr[0];
    $startOfBST = $dateYY . $delim . '03' . $delim . '31';
    $lastDayOfMarch = date('w', strtotime($startOfBST));
    if ($lastDayOfMarch < 7) {
        $startOfBST = $dateYY . $delim . '03' . $delim . (31 - $lastDayOfMarch);
    }

    $endOfBST = $dateYY . $delim . '10' . $delim . '31';
    $lastDayOfOct = date('w', strtotime($endOfBST));
    if ($lastDayOfOct < 7) {
        $endOfBST = $dateYY . $delim . '10' . $delim . (31 - $lastDayOfOct);
    }

    if ($date >= $startOfBST and $date <= $endOfBST) {
        return $addOffset;
    }

    return $NoOffset;
}
function e4s_expandEventType($tf) {
    switch ($tf) {
        case E4S_EVENT_TRACK:
            return 'Track';
        case E4S_EVENT_ROAD:
            return 'Road';
        case E4S_EVENT_FIELD:
            return 'Field';
        case E4S_EVENT_XCOUNTRY:
            return 'X-Country';
        case E4S_EVENT_MULTI:
            return 'Multi';
        default:
            return $tf;
    }
}
function e4s_deepCloneToObject($object) {
    $objectStr = e4s_getDataAsType($object, E4S_OPTIONS_STRING);
    $object = e4s_getDataAsType($objectStr, E4S_OPTIONS_OBJECT);
    return $object;
}

function e4s_deepCloneTo($object, $datatype) {
    $objectStr = e4s_getDataAsType($object, E4S_OPTIONS_STRING);
    $object = e4s_getDataAsType($objectStr, $datatype);
    return $object;
}

function e4s_getBaseAgeGroups() {
    $config = e4s_getConfig();
    $aocode = $config['defaultao']['code'];

    $sql = 'select *
            from ' . E4S_TABLE_AGEGROUPS . "
            where options like '%" . $aocode . "%'";

    $res = e4s_queryNoLog($sql);
    $rows = $res->fetch_all(MYSQLI_ASSOC);
    $baseAGRows = array();
    foreach ($rows as $row) {
        // to allow V2 to work
        $row['name'] = $row['Name'];
        // check if there is a base age group
        // Not sure why an agegroup would not have an options field ?????
        if (array_key_exists('options', $row)) {
            $agoptions = e4s_getOptionsAsObj($row['options']);
//        agoptions is an array of aocode options
            foreach ($agoptions as $agoption) {
                if ($agoption->aocode === $aocode) {
                    // got the matching ao code, so check for base
                    // base = 1. set and get out base = 2, set but keep going looking for a 1 ( Masters / Vet )
                    if (isset($agoption->base)) {
                        if ($agoption->base === 1) {
                            $baseAGRows[] = $row;
                        }
                    }
                }
            }
        }
    }
    return $baseAGRows;
}

function e4s_getBaseAGForDOBForComp($compid, $dob, $baseAGRows) {
    $compObj = e4s_getCompObj($compid);
    $compRow = $compObj->getRow();
    return e4s_getBaseAGForDBFromCompRow($compRow, $dob, $baseAGRows);
}

function e4s_getBaseAGForDBFromCompRow($compRow, $dob, $baseAGRows) {
    foreach ($baseAGRows as $baseAGRow) {
        $compAGGroup = getAgeGroupAndDOBs($compRow, $baseAGRow);
        if ($dob >= $compAGGroup['fromDate'] and $dob <= $compAGGroup['toDate']) {
            $baseAGRow['shortName'] = str_replace('Under ', 'U', $baseAGRow['Name']);
            return $baseAGRow;
        }
    }
    return null;
}

function e4s_compareDoubles($a, $operator, $b) {
//    $valuesAreEqual = abs($a - $b) < E4S_DOUBLE_DELTA;
    $valuesAreEqual = ('' . floatval($a) === '' . floatval($b));
    $equalsOperator = strpos($operator, '=');
    if ($equalsOperator !== FALSE and $valuesAreEqual) {
        return TRUE;
    }
    if ($equalsOperator === FALSE and $valuesAreEqual) {
        return FALSE;
    }

    if ($operator === '<') {
        if ($a < $b) {
            return TRUE;
        }
    }

    if ($operator === '>') {
        if ($a > $b) {
            return TRUE;
        }
    }

    return FALSE;
}

function fullGender($gender) {
    if ($gender === E4S_GENDER_MALE) {
        return 'Male';
    }
    if ($gender === E4S_GENDER_FEMALE) {
        return 'Female';
    }
    if ($gender === 'O') {
        $gender = 'Open';
    }
    return $gender;
}

function e4s_addStdPageInfo($obj, &$model = null) {
    $pageInfo = new stdClass();
    $pageInfo->startswith = checkFieldForXSS($obj, 'startswith:Starts With');
    $pageInfo->exact = checkFieldForXSS($obj, 'exact:Exact match');
    if (is_null($pageInfo->exact)) {
        $pageInfo->exact = FALSE;
    }
    $pageInfo->page = checkFieldForXSS($obj, 'pagenumber:Page Number');
	if ( is_null($pageInfo->page) ){
		$pageInfo->page = 1;
	}else{
		$pageInfo->page = (int)$pageInfo->page;
	}

    $pageInfo->pagesize = checkFieldForXSS($obj, 'pagesize:Page Size');
	if ( is_null($pageInfo->pagesize) ){
		$pageInfo->pagesize = 25;
	}else{
		$pageInfo->pagesize = (int)$pageInfo->pagesize;
	}
//    SortKey can be the field name and desc or ASC separated with the E4S_SQL_DELIM const
//    e.g title<E4S_SQL_DELIM>Asc or title<E4S_SQL_DELIM>Desc
    $sortkey = checkFieldForXSS($obj, 'sortkey:Sort Key');
    $sortOrder = checkFieldForXSS($obj, 'sortorder:Sort Order');
    if (is_null($sortkey)) {
        $pageInfo->sortkey = 'id';
        $pageInfo->sortorder = 'asc';
    } else {
        if (!is_null($sortOrder)) {
            $pageInfo->sortorder = $sortOrder;
            $pageInfo->sortkey = $sortkey;
        } else {
            $sortArr = explode(E4S_SQL_DELIM, $sortkey . E4S_SQL_DELIM . 'asc');
            $pageInfo->sortkey = $sortArr[0];
            $pageInfo->sortorder = $sortArr[1];
        }
    }
    if (!is_null($model)) {
        $model->pageInfo = $pageInfo;
    }
    return $pageInfo;
}

function e4s_echo($txt, $exit = FALSE, $forceEcho = FALSE, $allUsers = FALSE) {
    if (isAdminUser() or $allUsers) {
        e4s_addDebug($txt);
        if ($exit or $forceEcho) {
            echo 'E4S Msg :' . $txt . "\n";
            if ($exit) {
                exit();
            }
        }
    }

    return TRUE;
}

function e4s_dump($objs, $exit = true, $debug = false) {
	$objType = gettype($objs);
    if ( $objType !== 'array' ){
		$objs = array($objs);
	}
	$output = '';
	$output .= 'Type : ' . $objType . "\n";
	foreach ($objs as $objName=>$obj) {
		$output .= $objName . '(' . gettype($obj) . ') : ' . print_r($obj, true);
		$output .= "\n----------\n";
	}
	if ( $debug ){
		e4s_addDebugForce($output);
	}

	if ($exit) {
		exit($output);
	}else{
		echo $output;
	}
}

function e4s_initDebug($userId = 0) {
    $GLOBALS[E4S_DEBUG] = array();
    $GLOBALS[E4S_DEBUG_USER] = $userId;
}

function e4s_addDebugForce($obj, $txt = '') {
    $GLOBALS[E4S_DEBUG_USER] = e4s_getUserID();
    e4s_addDebug($obj, $txt);
}

function e4s_addDebug($obj, $txt = '') {
    if (!isE4SUser()) {
        if (e4s_getUserID() !== $GLOBALS[E4S_DEBUG_USER]) {
            return;
        }
    }
    if (gettype($obj) === 'string') {
        $obj = str_replace("\r\n", ' ', $obj);
        $obj = str_replace("\t", ' ', $obj);
        $obj = str_replace("'", '`', $obj);
        $obj = addslashes($obj);
    }
    if ($txt === '') {
        $GLOBALS[E4S_DEBUG][] = $obj;
    } else {
        // add random code to allow for multiple of the same text
        $txt .= E4S_DEBUG_RND . (rand(1, 100) . rand(1, 200) . rand(1, 100));
//        }
        $GLOBALS[E4S_DEBUG][$txt] = $obj;
    }
}

function print_mem() {
    /* Currently used memory */
    $mem_usage = memory_get_usage();

    /* Peak memory usage */
    $mem_peak = memory_get_peak_usage();

    echo 'The script is now using: <strong>' . round($mem_usage / 1024) . 'KB</strong> of memory.<br>';
    echo 'Peak usage: <strong>' . round($mem_peak / 1024) . 'KB</strong> of memory.<br><br>';
}

//Global Objects
function e4s_clearCompObj($compid) {
    $key = 'E4S_COMP_OBJ_' . $compid;
    if (array_key_exists($key, $GLOBALS)) {
        $GLOBALS[$key] = null;
    }
}

function e4s_getCompObj($compid, $withSummary = TRUE, $fail = true) : ?e4sCompetition {
    $key = 'E4S_COMP_OBJ_' . $compid;
    if (!array_key_exists($key, $GLOBALS)) {
        if ($compid === 0) {
            $GLOBALS[$key] = new e4sCompetition();
        } else {
            $GLOBALS[$key] = e4sCompetition::withID($compid, $withSummary);
            if ( is_null($GLOBALS[$key]->getRow() )){
                if ( $fail ) {
                    Entry4UIError(5023, 'Competition does not exist : ' . $compid);
                }
                return null;
            }
        }
    }
    return $GLOBALS[$key];
}

function e4s_isOTDEntry($eOptions) {
    $eOptions = e4s_addDefaultEntryOptions($eOptions);
    return $eOptions->otd;
}

function e4s_htmlFromUI($text) {
    $text = str_replace(E4S_HTML_OPEN_ENC, E4S_HTML_OPEN, $text);
    $text = str_replace(E4S_HTML_CLOSE_ENC, E4S_HTML_CLOSE, $text);
    $text = str_replace('<', E4S_HTML_OPEN, $text);
    $text = str_replace('>', E4S_HTML_CLOSE, $text);
    $text = str_replace('&lt;', E4S_HTML_OPEN, $text);
    $text = str_replace('&gt;', E4S_HTML_CLOSE, $text);

    return $text;
}

function e4s_htmlToUI($text, $enc = TRUE) {
//    e4s_dump($text,"Text 1",false,true,true);
    $text = e4s_htmlToUITag('strong', $text, $enc);
    $text = e4s_htmlToUITag('b', $text, $enc);
    $text = e4s_htmlToUITag('u', $text, $enc);
    $text = e4s_htmlToUITag('p', $text, $enc);
    $text = e4s_htmlToUITag('s', $text, $enc);
    $text = e4s_htmlToUITag('em', $text, $enc);
    $text = e4s_htmlToUITag('h1', $text, $enc);
    $text = e4s_htmlToUITag('h2', $text, $enc);
    $text = e4s_htmlToUITag('h3', $text, $enc);
    $text = e4s_htmlToUITag('h4', $text, $enc);
    $text = e4s_htmlToUITag('h5', $text, $enc);
    $text = e4s_htmlToUITag('h6', $text, $enc);
    $text = e4s_htmlToUITag('ul', $text, $enc);
    $text = e4s_htmlToUITag('li', $text, $enc);
    $text = e4s_htmlToUITag('br', $text, $enc);
    $text = e4s_htmlToUITag('ol', $text, $enc);
    $text = str_replace(E4S_HTML_OPEN_ENC, '&lt;', $text);
    $text = str_replace(E4S_HTML_CLOSE_ENC, '&gt;', $text);
    $text = str_replace('`nbsp;', ' ', $text);
    $text = str_replace('`amp;', '&', $text);
    $text = str_replace('&amp;pound;', '£', $text);
//    e4s_dump($text,"Text 2",true,true,true);
    return $text;
}

function e4s_htmlToUITag($tag, $text, $enc = TRUE) {
    $open = E4S_HTML_OPEN_ENC;
    $close = E4S_HTML_CLOSE_ENC;
    if (!$enc) {
        $open = E4S_HTML_OPEN;
        $close = E4S_HTML_CLOSE;
    }

    $newText = str_replace($open . $tag . $close, '<' . $tag . '>', $text);
    $newText = str_replace($open . '/' . $tag . $close, '</' . $tag . '>', $newText);
    return $newText;
}

function e4s_decodeHTML($body) {
    $text = str_replace('&lt;', '<', $body);
    $text = str_replace('&gt;', '>', $text);
    return $text;
}

function e4s_getAllClubs() {
    if (!array_key_exists(E4S_ALL_CLUBS, $GLOBALS)) {
        $sql = 'select  id id, 
                        clubname clubname 
                from ' . E4S_TABLE_CLUBS . '';
        $result = e4s_queryNoLog($sql);
        $clubsFromDB = $result->fetch_all(MYSQLI_ASSOC);
        $clubsArr = array();
        foreach ($clubsFromDB as $clubRow) {
            $clubsArr[(int)$clubRow['id']] = $clubRow;
        }
        $GLOBALS[E4S_ALL_CLUBS] = $clubsArr;
    }
    return $GLOBALS[E4S_ALL_CLUBS];
}

function _e4s_isPof10Available($obj) {
    $urn = checkFieldForXSS($obj, 'urn:Athlete URN');
    if ($urn === '' or is_null($urn)) {
        Entry4UIError(4010, 'You must pass a URN');
    }
    $config = e4s_getConfig();
    if ($config['defaultao']['code'] !== E4S_AOCODE_EA) {
        Entry4UIMessage('Only available to English athletes');
    }
    return TRUE;
}

function e4s_getWCProductDescObj($product) {
    if (is_null($product) or $product === FALSE) {
        return 'Product has been deleted';
    }
    $desc = $product->get_description();
    $desc = str_replace("\'", "'", $desc);
    $retObj = e4s_getDataAsType($desc, E4S_OPTIONS_OBJECT);

    return $retObj;
}

function is_R4SCompetition() {
    if (strpos(E4S_CURRENT_DOMAIN, 'result') !== FALSE) {
        return 1;
    }
//    2 in dev is paul@entry4sports.co.uk
    if (e4s_getUserID() === 2) {
        return 1;
    }
    return 0;
}

function e4s_getCompetitionAthletes($obj, $summary = FALSE) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $compObj = e4s_GetCompObj($compId);

    $searchmodel = e4s_addStdPageInfo($obj);
    Entry4UISuccess($compObj->getAthletes($summary, $searchmodel));
}

function e4s_getSubscriptionTandC() {
    return 'Subscription Terms and Conditions. Awaiting definition.';
}

function e4s_isSecure() {
    if (e4s_getProtocol() === 'https') {
        return TRUE;
    }
    return FALSE;
}

function e4s_getProtocolAndDomain() {
    return e4s_getFullProtocol() . E4S_CURRENT_DOMAIN;
}

function e4s_getFullProtocol() {
    return e4s_getProtocol() . '://';
}

function e4s_getProtocol() {
    if (array_key_exists('HTTPS', $_SERVER)) {
        return 'https';
    } else {
        return 'http';
    }
}

function e4s_getEntryCountsByEG($compid) {
    $entryCountObj = e4s_getEntryCountObj($compid);
    return $entryCountObj->getEntryCountsByEG();
}

function e4s_getEntryCountsByAthlete($compid) {
    $entryCountObj = e4s_getEntryCountObj($compid);
    return $entryCountObj->getCountsByAthleteAndEG();
}

function e4s_getEntryCountObj($compId) {
    if (array_key_exists('EntryCount_' . $compId, $GLOBALS)) {
        return $GLOBALS['EntryCount_' . $compId];
    }
    return new entryCountsClass($compId);
}

function e4s_isAAIDomain(){
    return E4S_CURRENT_DOMAIN === E4S_AAI_DOMAIN;
}
function e4s_isUKDomain(){
    return E4S_CURRENT_DOMAIN === E4S_UK_DOMAIN;
}
function e4s_isLiveDomain() {
    return E4S_CURRENT_DOMAIN === E4S_AAI_DOMAIN or E4S_CURRENT_DOMAIN === E4S_UK_DOMAIN or E4S_CURRENT_DOMAIN === E4S_EIRE_DOMAIN;
}
function e4s_isDevDomain() {
    return E4S_CURRENT_DOMAIN === E4S_DEV_DOMAIN;
}
function e4s_isUATDomain() {
    return E4S_CURRENT_DOMAIN === E4S_UAT_DOMAIN;
}
function e4s_isEireDomain() {
	return E4S_CURRENT_DOMAIN === E4S_EIRE_DOMAIN;
}
function isCronUser() {
    if (!array_key_exists(E4S_CRON_USER, $GLOBALS)) {
        return FALSE;
    }
    return $GLOBALS[E4S_CRON_USER];
}

function e4s_startsWith($haystack, $needle) {
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

function e4s_endsWith($haystack, $needle) {
    $length = strlen($needle);
    if ($length == 0) {
        return TRUE;
    }

    return (substr($haystack, -$length) === $needle);
}

function e4s_checkHeaderFor($key, $value) {
    $headers = getallheaders();
    if (array_key_exists($key, $headers)) {
        if ($headers[$key] === $value) {
            return TRUE;
        }
    }
    return FALSE;
}

function e4s_allowOneBracket($value) {
    //     allow one angled bracket
    $lt = strpos($value, '&lt;');
    $gt = strpos($value, '&gt;');
    if ($lt === FALSE or $gt === FALSE) {
        // one is false
        $value = str_replace('&lt;', '<', $value);
        $value = str_replace('&gt;', '>', $value);
    }
    return $value;
}

function hasDatePassed($date, $checkAgainstDate = '') {
    $dateTS = date_create($date)->getTimestamp();
    $checkAgainstDateTS = strtotime('now');
    if ($checkAgainstDate !== '') {
        $checkAgainstDateTS = date_create($checkAgainstDate)->getTimestamp();
    }

    return $dateTS < $checkAgainstDateTS;
}
function e4s_getCapitalisedName($oldName) {
    $oldName = strtolower($oldName);
    $oldName = trim($oldName);
    $useName = str_replace("'", " ' ", $oldName);
    $useName = str_replace('-', ' - ', $useName);
    $useName = str_replace('`', ' ` ', $useName);
    $useName = ucwords($useName);
    $useName = str_replace(" ' ", "'", $useName);
    $useName = str_replace(' - ', '-', $useName);
    $useName = str_replace(' ` ', '`', $useName);
//    return ucwords($useName);
    return $useName;
}
function formatAthlete($firstName, $surName) {
    $athlete = formatAthleteFirstname($firstName) . ' ' . formatAthleteSurname($surName);
    return $athlete;
}
function formatAthleteFirstname($firstName) {
    $athlete = strtolower($firstName);
    $athlete = e4s_getCapitalisedName($athlete);
    return $athlete;
}
function formatAthleteSurname($surName) {
    $athlete = strtoupper($surName);
    return $athlete;
}
function e4s_fireAndForget($endpoint){
	shell_exec('curl ' . $endpoint . ' > /dev/null 2>/dev/null &');
}
function e4s_updatePof10IfRequired($force = false):void{
	$userId = e4s_getUserID();

	if ( $userId <= E4S_USER_NOT_LOGGED_IN ){
		return;
	}
	$config = e4s_getConfig();
	if ( $config['defaultao']['code'] !== E4S_AOCODE_EA ){
		// currently only EA
		return;
	}
//	if ( strtoupper($config['env']) === 'DEV' ){
//		// don't update in dev
//		return;
//	}
	$today = date(E4S_SHORT_DATE);
	$dateLastChecked = get_user_meta($userId, E4S_USER_LASTCHECKEDPB);

	if ( !empty($dateLastChecked) ){
		$dateLastChecked = $dateLastChecked[0];
	}else{
		$dateLastChecked = $today;
		$force = true;
	}

	if ( $dateLastChecked > $today and !$force){
		// already checked today
		return;
	}

	// All checks done so check/update Pof10 on a FAF
	$protocol = $_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
	$url = $protocol . '://' . E4S_CURRENT_DOMAIN . E4S_BASE_URL . 'public/checkPof10/' . $userId;
	e4s_fireAndForget($url);
}
function e4s_updatePof10ForAthletes($urns) {
	foreach($urns as $urn){
		$pof10Obj = new pof10V2Class($urn);
		$pof10Obj->updateDb();
	}
}
function e4s_isCronUser():bool{
	if ( array_key_exists(E4S_CRON_USER, $GLOBALS) ){
		return $GLOBALS[E4S_CRON_USER];
	}
	return false;
}

function e4s_write_log($log) {
	// check if wordpress functions loaded, for now is lite domain then no
	if ( TRUE === WP_DEBUG ) {
		if ( is_array( $log ) || is_object( $log ) ) {
			error_log( print_r( $log, TRUE ) );
		} else {
			error_log( $log );
		}
	}
}
function e4s_isAthleteInternational($athleteRow) {
	if ($athleteRow['aocode'] === E4S_AOCODE_INTERNATIONAL) {
		return TRUE;
	}
	return FALSE;
}
function e4s_adjustClonedDate($date){
	if ( !array_key_exists(E4S_CLONE_DATE_DIFF, $GLOBALS) ) {
		return $date;
	}
	$format = E4S_SHORT_DATE;
	if ( strpos($date, ' ') > 0 ){
		$format = E4S_MYSQL_DATE;
	}
	return date($format, strtotime($date) + $GLOBALS[E4S_CLONE_DATE_DIFF] * 86400);
}
function isUserLoggedIn() {
	$userid = e4s_getUserID();
	if ($userid <= E4S_USER_NOT_LOGGED_IN) {
		return FALSE;
	}

	return TRUE;
}
function isReserveEvent($eventName){
	return stripos($eventName,'reserves') !== false;
}