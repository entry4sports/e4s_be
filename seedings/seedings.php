<?php
include_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_removeAthleteFromEventSeedings($fromCeId, $athleteId) {
    $sql = 'delete from ' . E4S_TABLE_SEEDING . '
            where eventgroupid = ( select maxgroup from ' . E4S_TABLE_COMPEVENTS . ' where id = ' . $fromCeId . ')
            and athleteid = ' . $athleteId;
    e4s_queryNoLog($sql);
}
function e4s_updateSeedingForEg($obj) {
    include_once E4S_FULL_PATH . 'entries/commonEntries.php';
    $compId = checkFieldForXSS($obj, 'compId:Competition Id' . E4S_CHECKTYPE_NUMERIC);
    $compObj = e4s_getCompObj($compId);
    if (!$compObj->isOrganiser()) {
        Entry4UIError(9701, 'You are not authorised to call this process', 200, '');
    }
    $egId = checkFieldForXSS($obj, 'egId:Event Group' . E4S_CHECKTYPE_NUMERIC);
    if (!is_numeric($egId)) {
        Entry4UIError(9702, 'Invalid present parameters');
    }
    $egId = (int)$egId;

    $seedingInfo = checkJSONObjForXSS($obj, 'seedingInfo:New Seeding Info');
    $returnSeeding = [];
    foreach($seedingInfo as $seeding){
        $seeding['entryId'] = (int)$seeding['entryId'];
        if ( array_key_exists('athleteId', $seeding) ){
            $seeding['athleteId'] = (int)$seeding['athleteId'];
        }else{
            // set to team ID
            $seeding['teamId'] = $seeding['entryId'];
        }

        $seeding['heatInfo']['heatNo'] = (int)$seeding['heatInfo']['heatNo'];
        $seeding['heatInfo']['laneNo'] = (int)$seeding['heatInfo']['laneNo'];
        $seeding['heatInfo']['heatNoCheckedIn'] = (int)$seeding['heatInfo']['heatNoCheckedIn'];
        $seeding['heatInfo']['laneNoCheckedIn'] = (int)$seeding['heatInfo']['laneNoCheckedIn'];
        $seeding['heatInfo']['maxInHeat'] = 25;
	    $seeding['heatInfo']['confirmed'] = true;
        if ( isset($seeding['heatInfo']['maxInHeat'])) {
	        $seeding['heatInfo']['maxInHeat'] = (int) $seeding['heatInfo']['maxInHeat'];
	        $seeding['heatInfo']['confirmed'] = $seeding['heatInfo']['confirmed'] === 'true';
        }

        $seeding['heatInfo']['seeded'] = true;
        $returnSeeding[] = $seeding;
    }

    $seedObj = new seedingV2Class($compId, $egId);
    $seedObj->updateSeedingsFromDragDrop($returnSeeding);

    e4s_sendSocketInfo($compId, $returnSeeding, R4S_SOCKET_SEED_DROPPED);

    Entry4UISuccess();
}

function e4s_confirmSeedingForEg($obj) {
    include_once E4S_FULL_PATH . 'entries/commonEntries.php';

    $compId = checkFieldForXSS($obj, 'compid:Competition Id' . E4S_CHECKTYPE_NUMERIC);
    $compObj = e4s_getCompObj($compId);
    if (!$compObj->isOrganiser()) {
        Entry4UIError(5051, 'You are not authorised to call this process', 200, '');
    }
    $userid = e4s_getUserID();
    $egId = checkFieldForXSS($obj, 'eventgroupid:Event Group' . E4S_CHECKTYPE_NUMERIC);
    if (!is_numeric($egId)) {
        Entry4UIError(9301, 'Invalid present parameters');
    }
    $egId = (int)$egId;
    $present = checkFieldForXSS($obj, 'present:Presence');
    $present = $present === '1' ? 1 : $present;
    $present = $present === '0' ? 0 : $present;
    if ($present !== 0 and $present !== 1) {
        Entry4UIError(9302, 'Invalid present parameters');
    }
    $entries = checkJSONObjForXSS($obj, 'entries:Entries and Seedings');
    $entryIds = array();
    $isTeam = false;
    foreach ($entries as $entry) {
        $entryIds[] = $entry['entryid'];
        if (is_null($entry['athleteId'])) {
            $isTeam = true;
        }
    }
    e4s_markEntriesPresent($compId, $entryIds, $present, $egId, FALSE, $isTeam);
    // set seedings
    $seedObj = new seedingV2Class($compId, $egId);
    $seedObj->updateHeatLaneNoForEg($entries);
    e4s_markEventSeeded($egId);
    $payload = $_POST;
    $payload['egId'] = $egId;
    e4s_sendSocketInfo($compId, $payload, R4S_SOCKET_SEED_CONFIRMED);

    Entry4UISuccess('"present":' . ($present === 1 ? 'true' : 'false'));
}

function e4s_confirmParticipants($obj) {
    $eventGroupId = checkFieldForXSS($obj, 'eventgroupid:Event Group' . E4S_CHECKTYPE_NUMERIC);
    $heatNo = checkFieldForXSS($obj, 'heatno:Heat No' . E4S_CHECKTYPE_NUMERIC);
    $checkedIn = checkFieldForXSS($obj, 'checkedin:Checked In');

    $data = $_POST['data'];
    $data = stripslashes_deep($data);
//    $confirmed = checkFieldForXSS($obj,'confirmed:Is heat Confirmed');
    if (is_null($eventGroupId) or is_null($heatNo) or is_null($checkedIn)) {
        Entry4UIError(9065, 'Invalid options passed');
    }
    $eventGroupId = (int)$eventGroupId;
    $heatNo = (int)$heatNo;
    if ($heatNo < 0) {
        Entry4UIError(9066, 'Invalid options passed');
    }

    $checkedIn = (int)$checkedIn;
    if ($checkedIn < 0 or $checkedIn > 1) {
        Entry4UIError(9067, 'Invalid options passed');
    }
    $checkedIn = $checkedIn === 1 ? TRUE : FALSE;

    $confirmed = (int)$data['confirmed'];

    $seedingObj = new seedingClass($eventGroupId);
    $seedingObj->confirmSeedings($heatNo, $confirmed, $checkedIn, $data);
}

function e4s_listSeedingsForComp($obj, $exit = true) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $test = checkFieldForXSS($obj, 'test:Competition ID');
    if ( is_null($test) ) {
        $test = false;
    }else{
        $test = true;
    }

    $compObj = e4s_getCompObj($compId);

    $comp = new stdClass();
    $comp->id = $compId;
    $comp->name = $compObj->getName();
    $comp->date = $compObj->getDate();

    $sql = "
        select egId
         , eventName name
         , typeNo
         , DATE_FORMAT(eventstartdate,'" . E4S_MYSQL_PRETTY_DATE . "') as startDate
         , DATE_FORMAT(eventstartdate,'" . E4S_MYSQL_PRETTY_TIME . "') as startTime
         , eventNo
         , egOptions
         , firstName
         , surName
         , classification
         , e.athleteId
         , entrypb pb
         , bibNo
         , teamBibNo
         , clubname countyName
         , ageGroup
         , gender
         , s.heatNo
         , s.laneNo
         , s.heatNoCheckedIn
         , s.laneNoCheckedIn
        from " . E4S_TABLE_ENTRYINFO . ' e left join
             ' . E4S_TABLE_SEEDING . ' s on egid = s.eventgroupid and e.athleteid = s.athleteid
        where compId = ' . $compId . '
        and paid in (' . E4S_ENTRY_PAID . ',' . E4S_ENTRY_QUALIFY . ')
        order by eventNo, s.heatnocheckedin, s.lanenocheckedin, s.heatno, s.laneno
    ';

    $result = e4s_queryNoLog($sql);
    $events = array();
    $checkInEnabled = $compObj->isCheckinEnabled();
    $lastEventNo = 0;

    while ($seedingData = $result->fetch_object()) {
        $eventNo = (int)$seedingData->eventNo;
        if ( $eventNo !== $lastEventNo ) {
            $nextLaneNo = 0;
            $egOptions = e4s_addDefaultEventGroupOptions($seedingData->egOptions);
            $seeded = $egOptions->seed->seeded;
        }
        $lastEventNo = $eventNo;
        $type = E4S_EVENT_TRACK;
        if ( $seedingData->typeNo[0] !== E4S_EVENT_TRACK ) {
            // Field
            $type = E4S_EVENT_FIELD;
//            continue;
        }
        $seedingData->athlete = formatAthlete($seedingData->firstName, $seedingData->surName);
        $classification = (int)$seedingData->classification;
        if ( $classification > 0 ){
	        $seedingData->athlete .= formatClasification($type, $classification);
        }
	    if( $compObj->anonymizeAthletes()) {
		    $seedingData->athlete = 'Athlete ' . $seedingData->athleteId;
	    }
        $egId = (int)$seedingData->egId;

        if (!array_key_exists($eventNo, $events)) {
            $events[$eventNo] = new stdClass();
        }
        $event = $events[$eventNo];
        $event->code = $seedingData->typeNo;
        $event->type = $type;
        $event->id = $egId;
        $event->eventNo = $eventNo;
        $event->egOptions = $seedingData->egOptions;
        $event->name = $seedingData->name;
        $event->date = $seedingData->startDate;
        $event->time = $seedingData->startTime;
        if ( $event->time === '00:00'){
            $event->time = E4S_NO_TIME;
        }
        $event->gender = 'Male';
        if ($seedingData->gender === E4S_GENDER_FEMALE) {
            $event->gender = 'Female';
        }
        $event->ageGroup = $seedingData->ageGroup;
        if (!isset($event->participants)) {
            $event->participants = array();
        }

        $event->seeded = $seeded;
        $participant = new stdClass();
        $participant->athlete = $seedingData->athlete;
        $participant->ageGroup = $event->ageGroup;
        $participant->gender = $event->gender;
        $participant->county = $seedingData->countyName;
        if ( $seedingData->teamBibNo === '' or (int)$seedingData->teamBibNo === 0 or is_null($seedingData->teamBibNo) ){
            $participant->bibNo = $seedingData->bibNo;
        }else{
            $participant->bibNo = $seedingData->teamBibNo;
        }

        if (is_null($participant->bibNo) or (int)$participant->bibNo === 0 ){
            $participant->bibNo = E4S_NO_BIBNO;
        }

        if ( $seeded ){
            if ( $checkInEnabled ){
                $participant->heatNo = (int)$seedingData->heatNoCheckedIn;
                $participant->laneNo = (int)$seedingData->laneNoCheckedIn;
            }else {
                $participant->heatNo = (int)$seedingData->heatNo;
                $participant->laneNo = (int)$seedingData->laneNo;
            }
        }else{
                $participant->heatNo = 1;
                $participant->laneNo = ++$nextLaneNo;
        }


        if ($participant->heatNo !== 0 and $participant->laneNo !== 0 ) {
            $event->participants[$participant->heatNo . '-' . $participant->laneNo] = $participant;
        }
    }
    $comp->events = $events;

    if ( $exit) {
        Entry4UISuccess($comp);
    }
    e4s_outputSeeding($comp, $test);
}
function formatClasification($type, $classification){
	return ' <span class="athleteClassification"><img class="athleteClassificationIcon" src="/resources/para_icon_small.gif">' . $type . $classification . '</img></span>';
}
function e4s_outputSeeding($comp, $test){
    $uiTheme = E4S_JQUERY_THEME;
    $eventsByDate = array();

    foreach($comp->events as $eventNo=>$event){
        if ( !array_key_exists($event->date, $eventsByDate)) {
            $eventsByDate[$event->date] = array();
        }
        $eventsByDate[$event->date][$eventNo] = $event;
    }

?>
    <html lang="en">
<head>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/<?php echo $uiTheme ?>/jquery-ui.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <style>
        .seedingHeader {

        }
        .athleteClassificationIcon {
            width: 0.7vw;
        }
        .athleteClassification {
            font-size: 0.7vw;
            font-weight: bold;
            color: var(--e4s-input-field__text-color);
        }
        .eventHeader{
            display: flex;
            flex-wrap: wrap;
            align-content: center;
            justify-content: flex-start;
            gap: 10px;
            border-bottom: 2px solid blue;
            font-weight: 800;
        }
        .heatHeader {
            font-weight: 600;
            background-color: lightblue;
        }
        .postEvent {

        }
        .lane {
            width: 3vw;
        }
        .bib{
            width: 3vw;
        }
        .athlete {
            width: 20vw;
        }
        .ageGroup {
            width: 8vw;
        }
        .org{

        }
        .eventData{
            width: 100%;
        }
    </style>
    <script>
        $( function() {
            $( "#dates" ).tabs();
        } );
    </script>
</head>
<body>
<h1 id="seedingHeader">Seeding for Competition : <?php echo $comp->id . ' : ' . $comp->name ?></h1>
<div id="dates">
    <ul>
        <?php
        $dayNo = 1;
        foreach($eventsByDate as $date=>$events){
            ?>
            <li><a href="#day-<?php echo $dayNo++ ?>"><?php echo $date?></a></li>
            <?php
        }
        ?>
    </ul>
    <?php
        $dayNo = 1;
        foreach($eventsByDate as $events){
        ?>
            <div id="day-<?php echo $dayNo ?>">
                <?php
                    $eventType = '';
                    foreach($events as $event){
                        if ( $eventType !== $event->type){
                            if ( $eventType !== ''){
                                e4s_typeFooter();
                            }
                            e4s_typeHeader($event->type, $dayNo);
                            $eventType = $event->type;
                        }
                        e4s_displayEventHeader($event);
                        if ( $event->seeded ) {
                            e4s_displayEventHeats($event->code, $event->participants, $event->type);
                        }else{
                            e4s_displayHeatParticipants($event->code, $event->participants, $event->type, false);
                        }
                        e4s_displayPostEvent();
                    }
                ?>
            </div>
        <?php
            $dayNo++;
        }
    ?>

</div>

</body>
    </html>
<?php
    exit();
}
function e4s_typeHeader($type, $dayNo){
    return;
    $class = '';
    echo '<h2>';
    if ( $type === E4S_EVENT_TRACK ){
        echo 'Track';
    }
    if ( $type === E4S_EVENT_FIELD ){
        echo 'Field';
        $class = ' style="display:none;" ';
    }
    echo '</h2>';
    echo '<div ' . $class . ' id="day-' . $type . '_' . $dayNo . '" onclick="">';
}
function e4s_typeFooter(){
    return;
    echo '</div>';
}
function e4s_displayToBeSeeded(){
    return 'Awaiting Seeding';
}
function e4s_displayEventHeader($event){
    ?>
        <div id="header_<?php echo $event->code; ?>" class="eventHeader">
    <div>
       <?php echo $event->time; ?>
    </div>
    <div>
        <?php echo sprintf('%s :', $event->code); ?>
    </div>
    <div>
        <?php
            echo $event->name;
            if ( $event->type === E4S_EVENT_TRACK ) {
                $egOptions = e4s_getDataAsType($event->egOptions,E4S_OPTIONS_OBJECT);
	            if ( isset( $egOptions->seed->qualifyToEg->rules ) ) {
		            echo ' Qualifying : First ' . $egOptions->seed->qualifyToEg->rules->auto . " and fastest " . $egOptions->seed->qualifyToEg->rules->nonAuto;
	            }
            }
        ?>
</div>
        </div>
    <?php
}
function e4s_displayEventHeaderTable($event){
    ?>
    <table id="header_<?php echo $event->code; ?>" class="eventHeader">
        <tr>
            <td>
                <?php echo $event->time; ?>
            </td>
            <td>
                 <?php echo sprintf('%s :', $event->code); ?>
            </td>
            <td>
                 <?php echo $event->name; ?>
            </td>
        </tr>
    </table>
    <?php
}
function e4s_displayEventHeats($id, $participants, $type){
    $lastHeat = 0;
    $heatParticipants = array();
    $lastKey = '';
    foreach($participants as $key=>$heatData) {
        if ( $lastHeat !== 0 and $lastHeat !== $heatData->heatNo ){
            e4s_displayHeatParticipants($id . '-' . $key, $heatParticipants, $type);
            $heatParticipants = array();
        }
        $lastHeat =  $heatData->heatNo;
        $heatParticipants[] = $heatData;
        $lastKey = $key;
    }
    e4s_displayHeatParticipants($id . '-' . $lastKey, $heatParticipants, $type);
}
function e4s_displayPostEvent(){
    echo '<div class="postEvent">&nbsp</div>';
}
function e4s_displayHeatParticipants($id, $heatParticipants, $type, $seeded = true){
    $headerHTML = '';

    ?>
    <table id="heatParticipants_<?php echo $id; ?>" class="eventData">

        <?php
        $pos = 1;
        foreach($heatParticipants as $heatParticipant){
            if ( $headerHTML === '' ){
                $headerHTML = '<tr class="heatHeader">';
                $headerHTML .= '<td colspan="6"  >';
                if ( $seeded ) {
                    if ($type === E4S_EVENT_TRACK ) {
                        $headerHTML .= '<span id="heat_<?php echo  $id?>">Race ' . $heatParticipant->heatNo . '</span>';
                    }
                }else{
                    if ($type === E4S_EVENT_TRACK ) {
                        $headerHTML .= '<span id="heat_<?php echo  $id?>">' . e4s_displayToBeSeeded() . '</span>';
                    }
                }
                $headerHTML .= '</td>';
                $headerHTML .= '</tr>';

                $headerHTML .= '<tr class="heatHeader">';
                $headerHTML .= '<td class="heatColHeader" >';
                $headerHTML .= 'Lane/Pos';
                $headerHTML .= '</td>';
                $headerHTML .= '<td class="heatColHeader" >';
                $headerHTML .= 'Bib No';
                $headerHTML .= '</td>';
                $headerHTML .= '<td class="heatColHeader" >';
                $headerHTML .= 'Athlete';
                $headerHTML .= '</td>';
                $headerHTML .= '<td class="heatColHeader" >';
                $headerHTML .= 'Event Age Group';
                $headerHTML .= '</td>';
                $headerHTML .= '<td class="heatColHeader" >';
                $headerHTML .= 'Gender';
                $headerHTML .= '</td>';
                $headerHTML .= '<td class="heatColHeader" >';
                $headerHTML .= 'Organisation';
                $headerHTML .= '</td>';
                $headerHTML .= '</tr>';
                echo $headerHTML;
            }
            if (!$seeded ){
                $heatParticipant->laneNo = $pos++;
            }
            ?>
            <tr>
                <td class="lane">
                    <?php echo $heatParticipant->laneNo; ?>
                </td>
                <td class="bib">
                    <?php echo $heatParticipant->bibNo; ?>
                </td>
                <td class="athlete">
                    <?php echo $heatParticipant->athlete; ?>
                </td>
                <td class="ageGroup">
                    <?php echo $heatParticipant->ageGroup; ?>
                </td>
                <td class="ageGroup">
                    <?php echo $heatParticipant->gender; ?>
                </td>
                <td class="org">
                    <?php echo $heatParticipant->county; ?>
                </td>
            </tr>
            <?php
        }

        if ( sizeof($heatParticipants) === 0 ){
            // No participants
            ?>
	        <tr class="heatHeader">
                <td colspan="6">
                    <span id="heat_<?php echo  $id?>"><?php echo e4s_displayToBeSeeded() ?></span>
                </td>
	        </tr>
            <?php
        }
        ?>

    </table>
    <?php
}