<?php
/**
 * Created by PhpStorm.
 * User: pauld
 * Date: 16/01/2019
 * Time: 14:36
 */
include_once E4S_FULL_PATH . 'dbInfo.php';
define('E4S_DEFAULT_USER', 10);
define('E4S_USER_NAME', 0);
define('E4S_USER_SCHOOL', 1);
define('E4S_USER_EMAIL', 2);
define('E4S_PASSWORD', 'Ireland-');
echo 'Loading School users <br>';
$users = array('Miriam Hackett,Ard Scoil La Salle,secretary@ardscoillasalle.ie');
$schoolsSQL = 'select * from ' . E4S_TABLE_CLUBS . "
               where clubtype = 'S'";
$result = e4s_queryNoLog($schoolsSQL);
$rows = $result->fetch_all(MYSQLI_ASSOC);
$schools = array();
foreach ($rows as $row) {
    $schools[$row['Clubname']] = $row;
}
//E4S_TABLE_SCHOOLS = $schools;
$sql = 'select *
        from ' . E4S_TABLE_USERS . '
        where id = ' . E4S_DEFAULT_USER;
$result = e4s_queryNoLog($sql);
if ($result->num_rows !== 1) {
    Entry4UIError(9046, 'Failed to find default user!', 400, '');
}
$defaultUserRow = $result->fetch_assoc();

$sql = 'select *
        from ' . E4S_TABLE_USERMETA . '
        where user_id = ' . E4S_DEFAULT_USER;
$result = e4s_queryNoLog($sql);
$defaultUserMetaRows = $result->fetch_all(MYSQLI_ASSOC);


foreach ($users as $user) {
    $userArr = explode(',', $user);

//    Does the user exist ?
    $sql = 'select *
            from ' . E4S_TABLE_USERS . "
            where user_email = '" . $userArr[E4S_USER_EMAIL] . "'";

    $result = e4s_queryNoLog($sql);
    if ($result->num_rows === 0) {
        echo 'Processing ' . $user . '<br>';
        $userId = e4s_create_user($userArr, $defaultUserMetaRows);
        if ($userId === E4S_USER_NOT_LOGGED_IN) {
            Entry4UIError(9047, 'Failed to insert user (' . $userArr[E4S_USER_EMAIL] . ')', 400, '');
        }
    }
}

function e4s_create_user($userArr, $defaultUserMetaRows) {
    $username = addslashes($userArr[E4S_USER_NAME]);
    $insertSQL = '
        insert into ' . E4S_TABLE_USERS . " (user_login, user_nicename,user_email,user_registered,user_status,display_name)
        values (
            '" . $userArr[E4S_USER_EMAIL] . "',
            '" . strtolower(str_replace(' ', '', $username)) . "',
            '" . $userArr[E4S_USER_EMAIL] . "',
            now(),
            '0',
            '" . $username . "'
        )
    ";

    $insertResult = e4s_queryNoLog($insertSQL);
    if ($insertResult) {
        $userID = e4s_getLastID();
        $sql = 'update ' . E4S_TABLE_USERS . "
                set user_pass = '" . md5(E4S_PASSWORD . e4s_getLastID()) . "'
                where id = " . $userID;
        e4s_queryNoLog($sql);
        e4s_createMetaRows($userArr, $userID, $defaultUserMetaRows);
        return $userID;
    }
    return 0;
}

function e4s_createMetaRows($userArr, $userId, $defaultUserMetaRows) {
    $sql = 'insert into ' . E4S_TABLE_USERMETA . ' (user_id, meta_key, meta_value)
            values ';

    $sep = '';
    foreach ($defaultUserMetaRows as $defaultUserMetaRow) {
        $username = addslashes($userArr[E4S_USER_NAME]);
        $names = explode(' ', $username);
        $sql .= $sep . '(';
        $sql .= $userId . ',';
        $meta_key = $defaultUserMetaRow['meta_key'];
        $sql .= "'" . $meta_key . "',";
        $meta_value = $defaultUserMetaRow['meta_value'];
        if ($meta_key === 'nickname') {
            $meta_value = $userArr[E4S_USER_EMAIL];
        }
        if ($meta_key === 'first_name') {
            $meta_value = $names[0];
        }
        if ($meta_key === 'last_name') {
            if (sizeof($names) === 1) {
                $meta_value = 'School';
            } else {
                $meta_value = explode(' ', $username);
                $meta_value = $meta_value[1];
            }
        }
        $sql .= "'" . $meta_value . "'";
        $sql .= ')';
        $sep = ',';
    }
    if (!array_key_exists($userArr[E4S_USER_SCHOOL], E4S_TABLE_SCHOOLS)) {
        echo 'School : ' . $userArr[E4S_USER_SCHOOL] . ' does not exist<br>';
        $delete = 'delete from ' . E4S_TABLE_USERS . ' where id = ' . $userId;
        e4s_queryNoLog($delete);
    } else {
        $schoolRow = E4S_TABLE_SCHOOLS[$userArr[E4S_USER_SCHOOL]];
        $sql .= $sep . '(';
        $sql .= $userId . ',';
        $meta_key = E4S_CLUB_ID;
        $sql .= "'" . $meta_key . "',";
        $meta_value = $schoolRow['id'];
        $sql .= "'" . $meta_value . "'";
        $sql .= ')';
        generate_mail($userArr, $userId);
    }
    e4s_queryNoLog($sql);
}

function generate_mail($userArr, $userId) {

    $body = 'Dear ' . $userArr[E4S_USER_NAME] . ',<br><br>';

    $body .= 'You have been granted access to the Entry4Sports system for your school ' . $userArr[E4S_USER_SCHOOL];
    $body .= '<br><br>';
    $body .= 'Your password is : ' . E4S_PASSWORD . $userId;
    $body .= '<br><br>';
    $body .= "<a href='https://" . E4S_CURRENT_DOMAIN . "'>Entry4Sports</a> Website";

    $body .= Entry4_emailFooter(null);
    $headers = Entry4_mailHeader('SchoolEntries');
    $headers[] = 'Bcc: nick@entry4sports.com';
    $headers[] = 'Bcc: paul@entry4sports.com';
    e4s_mail($userArr[E4S_USER_EMAIL], 'Entry4Sports - School Access', $body, $headers);
}