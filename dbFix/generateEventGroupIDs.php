<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';

$trunc = 'truncate table ' . E4S_TABLE_EVENTGROUPS;
e4s_queryNoLog($trunc);

$sql = 'SELECT distinct(maxgroup) maxgroup, compid compid, startdate startdate
        FROM ' . E4S_TABLE_COMPEVENTS . ' 
        group by compid, cast(maxgroup as unsigned), startdate
        order by compid desc, startdate';

$res = e4s_queryNoLog($sql);
$rows = $res->fetch_all(MYSQLI_ASSOC);

$eventno = 1;
$lastCompid = 0;
foreach ($rows as $row) {
    if ((int)$row['compid'] !== $lastCompid) {
        $eventno = 1;
    }
    $row['eventno'] = $eventno++;

//    e4s_dump($row);
//    echo "<br>";
    e4s_createEventGroup($row);
    $lastCompid = (int)$row['compid'];
}

function e4s_createEventGroup($ceRow) {
    $egOptions = new stdClass();
    $egOptions->trials = '';
    $egOptions->reportInfo = '';
    $egOptions = e4s_removeDefaultEventGroupOptions($egOptions);
    $sql = 'insert into ' . E4S_TABLE_EVENTGROUPS . '(compid, eventno, name, startdate, options)
            values (
            ' . $ceRow['compid'] . ',
            ' . $ceRow['eventno'] . ",
            'Event " . $ceRow['eventno'] . "',
            '" . $ceRow['startdate'] . "',
            '" . e4s_getOptionsAsString($egOptions) . "'
            )";

    e4s_queryNoLog($sql);
    $egid = e4s_getLastID();
    $update = 'update ' . E4S_TABLE_COMPEVENTS . "
               set maxgroup = '" . $egid . "'
               where maxgroup = '" . $ceRow['maxgroup'] . "'
               and compid = " . $ceRow['compid'] . "
               and startdate = '" . $ceRow['startdate'] . "'";
    e4s_queryNoLog($update);
}