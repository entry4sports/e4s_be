<?php
/**
 * Created by PhpStorm.
 * User: pauld
 * Date: 16/01/2019
 * Time: 14:36
 */
include_once E4S_FULL_PATH . 'dbInfo.php';
echo 'Migrating users <br>';
$hostname = 'db767575732.hosting-data.io';
$username = 'dbo767575732';
$password = 'E4S2019lpn$';
$database = 'db767575732';
$oldconn = new mysqli($hostname, $username, $password, $database);
if (!$oldconn->set_charset('utf8')) {
    printf("Error loading character set utf8: %s\n", $conn->error);
    exit();
}

if ($oldconn->connect_error) {
    die('Connection failed: ' . $oldconn->connect_error);
}

$db_selected = mysqli_select_db($oldconn, $database);
if (!$db_selected) {
    die ('Can\'t select database: -2');
}

$sql = 'select * from Entry4_users';
$results = mysqli_query($oldconn, $sql);
$conn = $GLOBALS['conn'];
$rows = $results->fetch_all(MYSQLI_ASSOC);
$cnt = 0;
foreach ($rows as $row) {
    $email = $row['user_email'];
    $sql = 'select * from ' . E4S_TABLE_USERS . "
            where user_email = '" . $email . "'";
//    echo "<br>" . $sql;
    $result = mysqli_query($conn, $sql);
    if ($result->num_rows === 0) {
        echo '<br>Inserting user ' . $email;
        $insertSql = 'insert into ' . E4S_TABLE_USERS . " (user_login, user_pass, user_nicename, user_email, user_url, user_registered, user_activation_key, user_status, display_name)
        values(
            '" . addslashes($row['user_login']) . "',
            '" . $row['user_pass'] . "',
            '" . addslashes($row['user_nicename']) . "',
            '" . $row['user_email'] . "',
            '" . $row['user_url'] . "',
            '" . $row['user_registered'] . "',
            '" . $row['user_activation_key'] . "',
            '" . $row['user_status'] . "',
            '" . addslashes($row['display_name']) . "'                                                                                                
        )";

        mysqli_query($conn, $insertSql);
        $newid = $conn->insert_id;
        $metaSql = 'Select * from Entry4_usermeta
                    where user_id = ' . $row['ID'];
        $metaResults = mysqli_query($oldconn, $metaSql);
        $metaRows = $metaResults->fetch_all(MYSQLI_ASSOC);
        $newmetasql = 'Insert into ' . E4S_TABLE_USERMETA . ' (user_id, meta_key, meta_value) values ';
        $sep = '';
        foreach ($metaRows as $metaRow) {
            $newmetasql .= $sep . '(' . $newid . ",'" . $metaRow['meta_key'] . "','" . addslashes($metaRow['meta_value']) . "')";
            $sep = ',';
        }
        $newmetasql .= ';';
        mysqli_query($conn, $newmetasql);
        $cnt += 1;

    }
}
echo '<br>Finished and inserted ' . $cnt . ' users';