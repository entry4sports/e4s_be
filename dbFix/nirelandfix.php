<?php
/**
 * Created by PhpStorm.
 * User: pauld
 * Date: 18/01/2019
 * Time: 21:04
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';

$sql = 'select *
        from ' . E4S_TABLE_CLUBS . "
        where country = 'Northern Ireland' or Region = 'Ulster'
        order by Clubname, id";
$results = e4s_queryNoLog($sql);
$rows = $results->fetch_all(MYSQLI_ASSOC);
$lastRow = null;
foreach ($rows as $row) {

    if (!is_null($lastRow)) {
        if ($lastRow['Clubname'] === $row['Clubname']) {
            move_Clubs($row, $lastRow);
        }
    }
    if (is_null($lastRow) or $lastRow['Clubname'] !== $row['Clubname']) {
        // this allows for 3 of the same name ( or more )
        $lastRow = $row;
    }

}

function move_Clubs($row, $lastrow) {
    // Make sure any athletes use lastrowclub
    $sql = 'update ' . E4S_TABLE_ATHLETE . '
            set clubid = ' . $lastrow['id'] . '
            where clubid = ' . $row['id'];

    e4s_queryNoLog($sql);

    $sql = 'delete from ' . E4S_TABLE_CLUBS . '
            where id = ' . $row['id'];

    e4s_queryNoLog($sql);
}