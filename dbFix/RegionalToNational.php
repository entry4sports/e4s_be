<?php
/**
 * Created by PhpStorm.
 * User: pauld
 * Date: 16/01/2019
 * Time: 14:36
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
$debug = FALSE;
$Source_Prefix = 'Entry4_';
$Target_Prefix = 'Entry4_';
$src_compid = $_GET['src'];
$target_compid = $_GET['target'];
if (is_null($src_compid) or is_null($target_compid)) {
    echo 'Invalid parameters passed';
    exit;
}
$src_compid = (int)$src_compid;
$target_compid = (int)$target_compid;
if ($src_compid < 100 or $target_compid < 100) {
    echo 'Invalid parameters passed';
    exit;
}

//$onlyAgeGroups = "48,49";
$onlyAgeGroups = '';
const Use_New_Line = "\n";
define('E4S_POST_TABLE', 'posts');
define('E4S_POSTMETA_TABLE', 'postmeta');
define('E4S_ENTRIES_TABLE', 'Entries');
define('E4S_COMPEVENTS_TABLE', 'CompEvents');
define('E4S_EVENTTEAMENTRIES_TABLE', 'EventTeamEntries');
define('E4S_EVENTTEAMATHLETES_TABLE', 'EventTeamAthlete');
define('E4S_TRANSLATION_TABLE', 'Entry4_translation');

$importedID = date('YMd-h:i:s', time());

$targetPaid = E4S_ENTRY_PAID;

e4s_StartTransaction();

$sql = 'Select e.*, ce.* 
            from ' . $Source_Prefix . E4S_ENTRIES_TABLE . ' e, ' . $Source_Prefix . E4S_COMPEVENTS_TABLE . ' ce
            where e.compeventid = ce.id
            and e.paid = ' . E4S_ENTRY_PAID . '
            and ce.compid = ' . $src_compid;

if ($onlyAgeGroups !== '') {
    $sql .= ' and ce.agegroupid in (' . $onlyAgeGroups . ')';
}

$srcResult = e4s_queryNoLog($sql);

outputMsg('Import ID : ' . $importedID);
outputMsg('Source Event Records : ' . $srcResult->num_rows);
$importRecCnt = 0;
if ($srcResult->num_rows > 0) {
    $sourceRows = $srcResult->fetch_all(MYSQLI_ASSOC);
    $sourceCeIds = array();
    foreach ($sourceRows as $sourceRow) {
        $ceId = (int)$sourceRow['compEventID'];
        $sourceCeIds[$ceId] = $ceId;
    }

    $translations = e4s_getTranslations($sourceCeIds);

    foreach ($sourceRows as $sourceRow) {
        outputMsg('SourceRow : ');
        outputObj($sourceRow);
        $sourceCeId = (int)$sourceRow['compEventID'];
        $targetUserID = (int)$sourceRow['userid'];
        if (array_key_exists($sourceCeId, $translations)) {
            $targetCeId = $translations[$sourceCeId];
            if (is_null($targetCeId)) {
                outputMsg('Failed to get the Target CE Record(' . $sourceCeId . ')');
                continue;
            }
        } else {
            $sql = 'select id from ' . $Target_Prefix . E4S_COMPEVENTS_TABLE . '
                where compid     = ' . $target_compid . ' 
                and   eventid    = ' . $sourceRow['EventID'] . '
                and   agegroupid = ' . $sourceRow['AgeGroupID'] . '
                and   split      = ' . $sourceRow['split'];
            outputMsg($sql);

            $targetCEResult = e4s_queryNoLog($sql);

            if ($targetCEResult->num_rows !== 1) {
                outputMsg('Failed to get the Target CE Record');
                continue;
            }

            $targetCERow = $targetCEResult->fetch_assoc();
            $targetCeId = $targetCERow['id'];
        }
        $entryExistsSql = 'Select id
                           from ' . $Target_Prefix . E4S_ENTRIES_TABLE . '
                           where athleteid   = ' . $sourceRow['athleteid'] . '
                           and   compeventid = ' . $targetCeId;
        outputMsg('Check Target : ' . $sql);
        $existResult = e4s_queryNoLog($entryExistsSql);
        if ($existResult->num_rows === 0) {
            // if it does not exist
            $orderid = 0;
            $productid = '-' . $sourceRow['variationID'];
            if ($targetPaid === E4S_ENTRY_PAID) {
                $orderid = '-' . $sourceRow['orderid'];
            } else {
                outputMsg('Creating Product');
                $productid = e4s_createWooProductForImport($sourceRow, $Source_Prefix, $Target_Prefix, $targetUserID, $target_compid);
                outputMsg('Created Product : ' . $productid);
            }
            $sql = 'insert into ' . $Target_Prefix . E4S_ENTRIES_TABLE . ' (compeventid, athleteid, athlete,pb, variationid, clubname, clubid, orderid, paid, price, eventagegroup, agegroupid, vetagegroupid, teamid, created, userid, options)
                values (' . $targetCeId . ',' . $sourceRow['athleteid'] . ',' . "'" . addslashes($sourceRow['athlete']) . "'," . "'" . $sourceRow['pb'] . "'," . $productid . ',' . "'" . $sourceRow['clubname'] . "'," . "'" . $sourceRow['clubid'] . "'," . $orderid . ',' . $targetPaid . ',' //            . "0,"
                . $sourceRow['price'] . ',' . "'" . $sourceRow['eventAgeGroup'] . "'," . $sourceRow['ageGroupID'] . ',' . $sourceRow['vetAgeGroupID'] . ',' . $sourceRow['teamid'] . ',' . "'" . $sourceRow['created'] . "'," . $targetUserID . ',' . "'{\"fee\":true, \"reason\":\"Transferred\", \"src\":" . $src_compid . ",\"origEID\":" . $sourceRow['id'] . ",\"imported\":\"" . $importedID . "\"}'" . ')';
            $debug = TRUE;
            insertRecord($sql, $debug);
            $importRecCnt += 1;
            outputMsg($targetCeId . ' for ' . $sourceRow['athlete'] . ' entry Inserted');
        } else {
            outputMsg($targetCeId . ' for ' . $sourceRow['athlete'] . ' entry already exists');
        }
    }
} else {
    outputMsg('No indiv events');
}
outputMsg('Now Check TEAMS');

// Now check Event Teams
$importTeamRecCnt = 0;
$sql = 'Select e.*, e.id teamentryid, ce.* 
            from ' . $Source_Prefix . E4S_EVENTTEAMENTRIES_TABLE . ' e, ' . $Source_Prefix . E4S_COMPEVENTS_TABLE . ' ce
            where e.ceid = ce.id
            and e.paid = 1
            and ce.compid = ' . $src_compid;
if ($onlyAgeGroups !== '') {
    $sql .= ' and ce.agegroupid in (' . $onlyAgeGroups . ')';
}
$srcResult = e4s_queryNoLog($sql);
outputMsg($sql);
outputMsg('Source Team Records : ' . $srcResult->num_rows);
if ($srcResult->num_rows > 0) {
    $sourceRows = $srcResult->fetch_all(MYSQLI_ASSOC);
    $sourceCeIds = array();
    foreach ($sourceRows as $sourceRow) {
        $ceId = (int)$sourceRow['ceid'];
        $sourceCeIds[$ceId] = $ceId;
    }
    $translations = e4s_getTranslations($sourceCeIds);
    foreach ($sourceRows as $sourceRow) {
        outputObj($sourceRow);
        $sourceCeId = (int)$sourceRow['ceid'];
        $targetUserID = (int)$sourceRow['userid'];
        if (array_key_exists($sourceCeId, $translations)) {
            $targetCeId = $translations[$sourceCeId];
            if (is_null($targetCeId)) {
                outputMsg('Failed to get team Target CE Record(' . $sourceCeId . ')');
                continue;
            }
        } else {
            $sql = 'select id from ' . $Target_Prefix . E4S_COMPEVENTS_TABLE . '
            where compid     = ' . $target_compid . ' 
            and   eventid    = ' . $sourceRow['EventID'] . '
            and   agegroupid = ' . $sourceRow['AgeGroupID'] . '
            and   split      = ' . $sourceRow['split'];
            outputMsg($sql);

            $targetCEResult = e4s_queryNoLog($sql);

            if ($targetCEResult->num_rows !== 1) {
                outputMsg('Failed to get the Target CE Team Record');
                outputMsg($sql);
                continue;
            }

            $targetCERow = $targetCEResult->fetch_assoc();
            $targetCeId = $targetCERow['id'];
        }
        $entryExistsSql = 'Select id
                       from ' . $Target_Prefix . E4S_EVENTTEAMENTRIES_TABLE . "
                       where name   = '" . addslashes($sourceRow['name']) . "'
                       and   ceid = " . $targetCeId . '
                       and   productid     = -' . $sourceRow['productid'];
        outputMsg($entryExistsSql);

        $existResult = e4s_queryNoLog($entryExistsSql);
        if ($existResult->num_rows === 0) {
            $orderid = 'null';
            $productid = '-' . $sourceRow['productid'];
            if ($targetPaid === E4S_ENTRY_PAID) {
                $orderid = '-' . $sourceRow['orderid'];
            } else {
                $productid = e4s_createWooProductForImport($sourceRow, $Source_Prefix, $Target_Prefix, $targetUserID, $target_compid);
            }

            // if it does not exist
            $sql = 'insert into ' . $Target_Prefix . E4S_EVENTTEAMENTRIES_TABLE . ' (name, ceid, productid, orderid, paid, price, userid, created, options)
            values (' . "'" . addslashes($sourceRow['name']) . "'," . $targetCeId . ',' . $productid . ',' . $orderid . ',' . $targetPaid . ',' //            . "0,"
                . $sourceRow['price'] . ',' . $targetUserID . ',' . "'" . $sourceRow['created'] . "'," . "'{\"fee\":true, \"reason\":\"Transferred\", \"src\":" . $src_compid . ",\"origEID\":" . $sourceRow['id'] . ",\"imported\":\"" . $importedID . "\"}'" . ')';
            insertRecord($sql, $debug);
            $importTeamRecCnt += 1;
            // move athletes
            $newresult = e4s_queryNoLog('select LAST_INSERT_ID() as id');
            $newrow = $newresult->fetch_assoc();
            $newteamentryid = $newrow['id'];
            outputMsg('New EntryID : ' . $newteamentryid);

            $srcAthletesSql = 'select *
                           from ' . $Source_Prefix . E4S_EVENTTEAMATHLETES_TABLE . ' 
                           where teamentryid = ' . $sourceRow['teamentryid'] . '
                           ';
            outputMsg($srcAthletesSql);
            $srcAthletesResult = e4s_queryNoLog($srcAthletesSql);
            if ($srcAthletesResult->num_rows < 1) {
                outputMsg('No athletes found for ' . $sourceRow['teamentryid']);
            } else {
                if ($newteamentryid !== 0) {
                    $srcAthletes = $srcAthletesResult->fetch_all(MYSQLI_ASSOC);
                    foreach ($srcAthletes as $srcAthlete) {
                        $sql = 'insert into ' . $Target_Prefix . E4S_EVENTTEAMATHLETES_TABLE . ' (teamentryid, pos, athleteid, options)
                        values (' . $newteamentryid . ',' . addslashes($srcAthlete['pos']) . ',' . $srcAthlete['athleteid'] . ',' . "'{\"import\":" . $importedID . "}'" . ')';
                        insertRecord($sql, $debug);
                    }
                } else {
                    outputMsg('Last Insert failed');
                }
            }
            outputMsg($targetCeId . ' for ' . $sourceRow['name'] . ' Event Team Inserted');
        } else {
            outputMsg($targetCeId . ' for ' . $sourceRow['name'] . ' Event Team already exists');
        }
//    break;
    }
}
outputMsg('Imported ' . $importRecCnt . ' entries');
outputMsg('Imported ' . $importTeamRecCnt . ' team entries');
outputMsg('finished ' . time());
e4s_Commit();
exit;

function e4s_createWooProductForImport($sourceRow, $sourcePrefix, $targetPrefix, $userid, $compid) {
    $prodId = '';
    if (array_key_exists('variationID', $sourceRow)) {
        $prodId = $sourceRow['variationID'];
    } else {
        $prodId = $sourceRow['productid'];
    }
    $sourcePostSql = 'select * 
                        from ' . $sourcePrefix . E4S_POST_TABLE . ' 
                        where id = ' . $prodId;
    outputMsg($sourcePostSql);
    $sourcePostResult = e4s_queryNoLog($sourcePostSql);
    if ($sourcePostResult->num_rows !== 1) {
        e4s_Rollback();
        Entry4UIError(2022, 'Failed to find product : ' . $prodId, 400, '');
    }
    $sourcePostRow = $sourcePostResult->fetch_assoc();
    $sourceMetaSql = 'select * 
                        from ' . $sourcePrefix . E4S_POSTMETA_TABLE . ' 
                        where post_id = ' . $prodId;
    outputMsg($sourceMetaSql);
    $sourceMetaResult = e4s_queryNoLog($sourceMetaSql);
    if ($sourceMetaResult->num_rows < 1) {
        e4s_Rollback();
        Entry4UIError(2023, 'Failed to find product meta rows : ' . $prodId, 400, '');
    }

    $sourceMetaRows = $sourceMetaResult->fetch_all(MYSQLI_ASSOC);

    // Create the target product
    $sql = 'insert into ' . $targetPrefix . E4S_POST_TABLE . " (post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, 
                   post_name, post_modified, post_modified_gmt, post_parent, guid, menu_order, post_type, comment_count)
    VALUES ($userid, UTC_TIMESTAMP(), UTC_TIMESTAMP(), '{" . addslashes($sourcePostRow['post_content']) . "}', '" . addslashes($sourcePostRow['post_title']) . "','compid:" . $compid . "','" . WC_POST_PUBLISH . "','closed','closed','" . addslashes($sourcePostRow['post_name']) . "', 
        UTC_TIMESTAMP(), UTC_TIMESTAMP(),
        '" . $sourcePostRow['post_parent'] . "',
        '" . $sourcePostRow['guid'] . "',
        " . $sourcePostRow['menu_order'] . ",
        '" . $sourcePostRow['post_type'] . "',
        " . $sourcePostRow['comment_count'] . '
   )';
    outputMsg($sql);
    e4s_queryNoLog($sql);
    $newProdId = e4s_getLastID();

    foreach ($sourceMetaRows as $metaRow) {
        $sql = 'insert into ' . $targetPrefix . E4S_POSTMETA_TABLE . ' (post_id, meta_key, meta_value )
        values (
        ' . $newProdId . ",
        '" . $metaRow['meta_key'] . "',
        '" . $metaRow['meta_value'] . "'
        )";
        outputMsg($sql);
        e4s_queryNoLog($sql);
    }
    return $newProdId;
}

function insertRecord($sql, $debug) {
    if ($debug) {
        outputMsg($sql);
    }
    e4s_queryNoLog($sql);
}

function outputMsg($txt) {
    echo Use_New_Line . $txt;
}

function outputObj($obj) {
    echo Use_New_Line;
    e4s_dump($obj);
}

function e4s_StartTransaction() {
    $conn = $GLOBALS['conn'];
    $conn->autocommit(FALSE);
}

function e4s_Commit() {
    $conn = $GLOBALS['conn'];
    $conn->commit();
    $conn->autocommit(TRUE);
}

function e4s_Rollback() {
    $conn = $GLOBALS['conn'];
    $conn->rollback();
    $conn->autocommit(TRUE);
}

function e4s_getTranslations($sourceCeIds) {
    $retArr = array();
    $sql = 'select sourceceid sourceCeId,
                   targetceid targetCeId
            from ' . E4S_TRANSLATION_TABLE . '
            where sourceceid in (' . implode(',', $sourceCeIds) . ')';
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows > 0) {
        while ($obj = $result->fetch_object()) {
            $retArr[(int)$obj->sourceCeId] = (int)$obj->targetCeId;
        }
    }
    return $retArr;
}