<?php
/**
 * Created by PhpStorm.
 * User: pauld
 * Date: 18/01/2019
 * Time: 21:04
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';

$sql = 'select id, options
        from ' . E4S_TABLE_COMPEVENTS . "
        where options like '%xrText%'";
$results = mysqli_query($conn, $sql);
$rows = $results->fetch_all(MYSQLI_ASSOC);
foreach ($rows as $key => $value) {
    $options = json_decode($value['options']);
    if ($options === null) {
        echo 'Data is invalid (' . $value['options'] . '.<br>';
        continue;
    }
    $xrText = $options->xrText;
    if ($xrText === null) {
        $xrText = '';
    }
    $updateSql = 'update ' . E4S_TABLE_COMPEVENTS . "
              set eventNameExtra = '" . $xrText . "'
              where id = " . $value['id'];
    echo $updateSql . '<br>';
    mysqli_query($conn, $updateSql);
}