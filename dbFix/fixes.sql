#
// Fix dates
select startdate, date_format(startdate, '%H:%i')
from Entry4_CompEvents
where CompID = xxxx;

update Entry4_CompEvents
set startdate = concat('2021-07-31 ', date_format(startdate, '%H:%i:00'))
where CompID = xxxx;