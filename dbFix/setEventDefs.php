<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
/*
 CREATE TABLE `Entry4_EventDefs` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `tf` varchar(1) COLLATE latin1_general_ci NOT NULL,
  `options` varchar(500) COLLATE latin1_general_ci NOT NULL,
  `uomid` int(11) NOT NULL COMMENT 'uom rec id'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Entry4_EventDefs`
--
ALTER TABLE `Entry4_EventDefs`
  ADD PRIMARY KEY (`Name`,`tf`),
  ADD UNIQUE KEY `ID` (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Entry4_EventDefs`
--
ALTER TABLE `Entry4_EventDefs`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `Entry4_EventGender` (
  `id` int(11) NOT NULL,
  `eventid` int(11) NOT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `min` float NOT NULL,
  `max` float NOT NULL,
  `options` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Entry4_EventGender`
--
ALTER TABLE `Entry4_EventGender`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `gender` (`eventid`,`gender`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Entry4_EventGender`
--
ALTER TABLE `Entry4_EventGender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `Entry4_text` (
  `compid` int(11) NOT NULL,
  `e4skey` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `e4sText` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Entry4_text`
--
ALTER TABLE `Entry4_text`
  ADD UNIQUE KEY `Compid` (`compid`,`e4skey`) USING BTREE;

Run this script
Create a view on this
SELECT eg.id ID,ed.Name,ed.tf,Gender,ed.options, ed.uomid,min,max FROM `Entry4_EventDefs` ed, Entry4_EventGender eg where ed.id = eg.eventid order by name

*/

PreProcess();
echo '<br>Truncating Defs';
$sql = 'truncate table Entry4_EventDefs';
$results = e4s_queryNoLog($sql);

echo '<br>Inserting Defs 1';
$sql = 'insert INTO `Entry4_EventDefs` ( Name,tf,options,uomid) SELECT name,tf,options,uomid FROM `Entry4_Events` group by concat(name,tf) having count(concat(name,tf)) = 2';
$results = e4s_queryNoLog($sql);
echo '<br>Truncating Genders';
$sql = 'truncate table Entry4_EventGender';
$results = e4s_queryNoLog($sql);

echo '<br>Inserting Female Genders';
$sql = "insert INTO `Entry4_EventGender` (id, eventid, gender, options)
select e.id, ed.id, 'F', ed.options
from Entry4_EventDefs ed,
     Entry4_Events e 
     where ed.Name = e.Name
     and ed.tf = e.tf
     and e.gender = 'F'";
$results = e4s_queryNoLog($sql);

echo '<br>Inserting Male Genders';
$sql = "insert INTO `Entry4_EventGender` ( id, eventid, gender, options)
select e.id, ed.id, 'M', ed.options
from Entry4_EventDefs ed,
     Entry4_Events e 
     where ed.Name = e.Name
     and ed.tf = e.tf
     and e.gender = 'M'";
$results = e4s_queryNoLog($sql);

echo '<br>Inserting Defs 2';
$sql = 'insert INTO `Entry4_EventDefs` ( Name,tf,options,uomid) SELECT name,tf,options,uomid FROM `Entry4_Events` group by concat(name,tf) having count(concat(name,tf)) = 1';
$results = e4s_queryNoLog($sql);

echo '<br>Inserting Genders 2';
$sql = 'insert into Entry4_EventGender (id, eventid,gender,options) SELECT e.id, ed.id,e.gender,e.options FROM `Entry4_Events` e, Entry4_EventDefs ed where ed.name = e.name and ed.tf = e.tf group by concat(e.name,e.tf) having count(concat(e.name,e.tf)) = 1';
$results = e4s_queryNoLog($sql);

echo '<br>Updating Genders';
$sql = "update Entry4_EventGender
set options = ''
where options not like '%\"min\"%'";
$results = e4s_queryNoLog($sql);

$sql = "select *
        from Entry4_EventGender
        where options != ''
        and options not like '%\"min\":0%'
        and options not like '%\"max\":0%'";
$results = e4s_queryNoLog($sql);
$rows = $results->fetch_all(MYSQLI_ASSOC);
foreach ($rows as $row) {
    try {
        echo '<br>ID : ' . $row['id'] . ':' . $row['options'];
        $options = e4s_getOptionsAsObj($row['options']);
    } catch (Exception $err) {
        echo 'Invalid JSON for ' . $row['id'] . "\n";
    }

    $min = 0;
    $max = 0;
    if (isset($options->min)) {
        $min = $options->min;
        $minArr = explode('.', $min);
        if (sizeof($minArr) > 2) {
            $min = (float)($minArr[0] . '.' . $minArr[1]);
        }
    }
    if (isset($options->max)) {
        $max = $options->max;
        $maxArr = explode('.', $max);
        if (sizeof($maxArr) > 2) {
            $max = (float)($maxArr[0] . '.' . $maxArr[1]);
        }
    }
    echo '<br>Updating Gender : ' . $row['id'];
    $sql = 'update Entry4_EventGender
    set min = ' . $min . ',
       max = ' . $max . '
       where id = ' . $row['id'];

    e4s_queryNoLog($sql);
}
/*
 * No need to check and update now as the ids should be the same
 * SELECT e.id, e.name, et.name FROM Entry4_Events e, Entry4_EventsTable et where e.id = et.ID and e.name != et.name
 */
//$sql = "SELECT e.id newid, et.id oldid
//FROM " . E4S_TABLE_EVENTS ." e, " . E4S_TABLE_EVENTSTABLE ." et
//where e.Name = et.Name
//and e.gender = et.Gender
//and e.tf = et.tf";
//$result = e4s_queryNoLog($sql);
//$rows = $result->fetch_all(MYSQLI_ASSOC);
//$idKeys = array();
//foreach($rows as $row){
//    $idKeys[$row['oldid']] = $row['newid'];
//}
//$sql = "select id, eventid
//        from " . E4S_TABLE_COMPEVENTS;
//$ceResult = e4s_queryNoLog($sql);
//$ceRows = $ceResult->fetch_all(MYSQLI_ASSOC);
//foreach($ceRows as $ceRow){
//    if ( array_key_exists($ceRow['eventid'],$idKeys)) {
//        $sql = "update " . E4S_TABLE_COMPEVENTS ."
//            set eventid = " . $idKeys[$ceRow['eventid']] ."
//            where id = " . $ceRow['id'];
//        echo "<br>Replacing " . $ceRow['eventid'] . " with " . $idKeys[$ceRow['eventid']] ;
//    e4s_queryNoLog($sql);
//    }
//}
echo '<br>Rename the events Table';
$sql = 'RENAME TABLE Entry4_Events TO Entry4_EventsTable';
$results = e4s_queryNoLog($sql);
echo '<br>Create the Events View';
$sql = 'CREATE OR REPLACE
 ALGORITHM = UNDEFINED
 VIEW `Entry4_Events`
 AS SELECT eg.id ID,ed.Name,ed.tf,Gender,ed.options, ed.uomid,min,max FROM `Entry4_EventDefs` ed, Entry4_EventGender eg where ed.id = eg.eventid';
$results = e4s_queryNoLog($sql);

function PreProcess() {
    $sql = "update Entry4_Events   
        set options = '{}'
        where options = ''";
    e4s_queryNoLog($sql);

    $sql = "update Entry4_Events   
        set options = concat(options,'}')
        where options not like '%}'";
    e4s_queryNoLog($sql);

    $sql = "select id, options
        from Entry4_Events
        where options not like '%}'";
    $results = e4s_queryNoLog($sql);
    if ($results->num_rows > 0) {
        $rows = $results->fetch_all(MYSQLI_ASSOC);
        foreach ($rows as $row) {
            echo '<br>Issue with : ' . $row['id'];
            echo '<br>' . $row['options'];
        }
        exit();
    }

    $sql = "update Entry4_Events   
        set options = replace(options,',,',',')
        where options like '%,,%'";
    e4s_queryNoLog($sql);

    $sql = "update Entry4_Events   
        set options = replace(options,'\"60.00.00\",}','\"60.00.00\"}')
        where options like '%\"60.00.00\",}'";
    e4s_queryNoLog($sql);

    $sql = "update Entry4_Events   
        set options = replace(options,'\"max\":360\"}','\"max\":360}')
        where options like '%\"max\":360\"}%'";
    e4s_queryNoLog($sql);


    $sql = "update Entry4_Events   
        set options = replace(options,'Rule\":true\"eventTeam','Rule\":true,\"eventTeam')
        where options like '%Rule\":true\"eventTeam%'";
    e4s_queryNoLog($sql);

    $sql = 'select id, options
        from Entry4_Events';
    $results = e4s_queryNoLog($sql);
    $rows = $results->fetch_all(MYSQLI_ASSOC);
    foreach ($rows as $row) {
        echo '<br>Checking : ' . $row['id'];
        echo '<br>' . $row['options'];
        $options = e4s_getOptionsAsObj($row['options']);
    }
    echo '<br>All Good';
}