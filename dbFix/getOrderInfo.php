<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
function e4s_getOrderInfo($obj) {
    $orderNo = checkJSONObjForXSS($obj, 'orderid:Order No' . E4S_CHECKTYPE_NUMERIC);
    if (!isE4SUser()) {
        Entry4UIError(8350, 'Sorry, you are not authorised');
    }

    $sql = 'select meta_value
            from ' . E4S_TABLE_POSTMETA . '
            where post_id = ' . $orderNo . "
            and meta_key = '_order_key'";
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(8355, 'Incorrect Order number given');
    }
    $obj = $result->fetch_object();
    header('Content-Type: text/html; charset=utf-8');
    echo "<a target='e4sOrder' href='/checkout/order-received/" . $orderNo . '/?key=' . $obj->meta_value . "'>Click here to see order " . $orderNo . '</a>';
}