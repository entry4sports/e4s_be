<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';

//e4s_refund_order("62753","Covid-19");
e4s_refund_order('62757', 'Covid-19');

function e4s_refund_order($order_id, $refund_reason = '') {
    echo 'Order : ' . $order_id . "\n";
    $order = wc_get_order($order_id);

    // If it's something else such as a WC_Order_Refund, we don't want that.
    if (!is_a($order, 'WC_Order')) {
        return new WP_Error('wc-order', __('Provided ID is not a WC Order', 'entry4sports.com'));
    }

    if ('refunded' == $order->get_status()) {
        return new WP_Error('wc-order', __('Order has been already refunded', 'entry4sports.com'));
    }
//echo $order->get_status();
    echo 'order total : ' . $order->get_total();
    echo "\n";
    echo 'order item count : ' . $order->get_item_count();
    echo "\n";
    echo 'stripe Fee : ' . $order->get_meta('_stripe_fee');
    echo "\n";
    if (empty($order->get_refunds())) {
        echo 'No Refunds';
    } else {
        echo 'Refunds already done';
    }
    echo "\n";
    $items = $order->get_items();
    foreach ($items as $item) {
        echo 'Line Value : ' . $item->get_total() . "\n";
    }

    // Order Items were processed. We can now create a refund
//    $refund = wc_create_refund(array(
//        'amount' => 6.00,
//        'reason' => $refund_reason,
//        'order_id' => $order_id,
//        'refund_payment' => true
//    ));
//
//    return $refund;
    return;
}

?>