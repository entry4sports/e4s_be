<?php

include_once E4S_FULL_PATH . 'dbInfo.php';
$sql = 'select * from ' . E4S_TABLE_POSTS . "
        where post_title like '%Kent School%'";

$posts = e4s_queryNoLog($sql);
$postRows = $posts->fetch_all(MYSQLI_ASSOC);
foreach ($postRows as $postRow) {
    $postContent = e4s_getOptionsAsObj($postRow['post_content']);
    if (isset ($postContent->athletes)) {
        $athletes = '';
        $athleteSep = '';
        foreach ($postContent->athletes as $athlete) {
            $athletes .= $athleteSep . $athlete->name . '. Consent : Yes';
            $athleteSep = ', ';
        }
        $getMeta = 'select * from ' . E4S_TABLE_POSTMETA . '
                    where post_id = ' . $postRow['ID'] . "
                    and meta_key = 'e4s_athletes'";
        $meta = e4s_queryNoLog($getMeta);

        if ($meta->num_rows !== 1) {
            $insert = 'insert into ' . E4S_TABLE_POSTMETA . '
                       (post_id,meta_key, meta_value)
                       values (' . $postRow['ID'] . ",'e4s_athletes','" . str_replace("'", '`', $athletes) . "')";
//            echo $insert . "\n";
//            exit();
            e4s_queryNoLog($insert);
        }
    }
}