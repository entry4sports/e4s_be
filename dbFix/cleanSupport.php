<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
define('E4S_SUPPORT_PREFIX', 'Support_');
define('E4S_SUPPORT_POSTS', E4S_SUPPORT_PREFIX . 'posts');
define('E4S_SUPPORT_POSTMETA', E4S_SUPPORT_PREFIX . 'postmeta');
define('E4S_SUPPORT_TICKETS', E4S_SUPPORT_PREFIX . 'wpsc_ticket');
define('E4S_SUPPORT_TICKETSMETA', E4S_SUPPORT_PREFIX . 'wpsc_ticketmeta');
define('E4S_DELETE_TAG', 'E4S_DELETE');

define('E4S_SUPPORT_CLOSED', 6);

$deletePostSQL = ' from ' . E4S_SUPPORT_TICKETS . '
                where ticket_status = ' . E4S_SUPPORT_CLOSED . ' or active = 0';
$closedSQL = 'select id ' . $deletePostSQL;


//mark post ids
$postSQL = 'update ' . E4S_SUPPORT_POSTS . "
            set post_title = '" . E4S_DELETE_TAG . "'
            where ID in ( select post_id from " . E4S_SUPPORT_POSTMETA . ' where meta_value in (' . $closedSQL . ")
            and meta_key = 'ticket_id')";
$result = e4s_queryNoLog($postSQL);

//Delete ticket meta
$ticketMetaSQL = 'delete from ' . E4S_SUPPORT_TICKETSMETA . '
                  where ticket_id in (' . $closedSQL . ')';
echo $ticketMetaSQL . '<br>';
e4s_queryNoLog($ticketMetaSQL);

// delete post meta
$postmetaSQL = 'delete from ' . E4S_SUPPORT_POSTMETA . ' where post_id in ( select ID from ' . E4S_SUPPORT_POSTS . " 
                           where post_title = '" . E4S_DELETE_TAG . "' )";
echo $postmetaSQL . '<br>';
e4s_queryNoLog($postmetaSQL);

// Delete posts
$postDeleteSql = 'delete from ' . E4S_SUPPORT_POSTS . " where post_title = '" . E4S_DELETE_TAG . "'";
echo $postDeleteSql . '<br>';
e4s_queryNoLog($postDeleteSql);

// delete tickets
$deleteTicketSQL = 'delete ' . $deletePostSQL;
echo $deleteTicketSQL . '<br>';
e4s_queryNoLog($deleteTicketSQL);