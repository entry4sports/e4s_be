<?php

include_once E4S_FULL_PATH . 'dbInfo.php';

if ($key !== '' and !isE4SUser()) {
    Entry4UIError(1000, 'You are not authorised to use this call', 403, '');
}

// default is to give info on current user
$userId = e4s_getUserID();
if ($key !== '') {
    $sql = 'Select * from ' . E4S_TABLE_USERS . '
            where ';
    // get the user in question
    if (is_numeric($key)) {
        $sql .= ' id = ' . $key;
    } else {
        $sql .= " user_email='" . $key . "'";
    }
    $result = mysqli_query($conn, $sql);
    if ($result->num_rows === 1) {
        $row = $result->fetch_assoc();
        $userId = $row['ID'];
    } else {
        Entry4UIError(1001, 'Invalid key passed', 404, '');
    }
}

$userInfo = array();
$userInfo['id'] = $userId;
$userInfo['club'] = getClubForUser($userId);
$userInfo['area'] = getUserAreaForUser($userId);

$sql = "select athleteid, urn, concat(firstname, ' ', surname) Athlete 
        from " . E4S_TABLE_USERATHLETES . ' ua,
             ' . E4S_TABLE_ATHLETE . ' a
        where userid = ' . $userId . '
        and   a.id = ua.athleteid';

$result = mysqli_query($conn, $sql);

$rows = $result->fetch_all(MYSQLI_ASSOC);

$userInfo['athletes'] = $rows;
$paging = getUserPageSize();
$userInfo['pageInfo'] = $paging;

Entry4UISuccess('
    "data": ' . json_encode($userInfo, JSON_NUMERIC_CHECK));