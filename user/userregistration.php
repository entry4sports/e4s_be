<?php
function e4s_UserRegister($obj) {
    include_once E4S_FULL_PATH . 'dbInfo.php';
//    include_once E4S_FULL_PATH . 'admin/e4sReturn.php';
    $params = $obj->get_params('JSON');
    $email = checkFieldForXSS($obj, 'email:User Email');
    if (is_null($email)) {
        $email = $params['email'];
    }
    if (strpos($email, '@') === FALSE) {
        Entry4UIError(1022, 'Invalid Email address used');
    }
    if (strpos($email, '.') === FALSE) {
        Entry4UIError(1024, 'Invalid Email address used');
    }
    $username = checkFieldForXSS($obj, 'name:User Name');
    if (is_null($username)) {
        if (is_null($email)) {
            $username = trim($params['name'], " \n\r");
        } else {
            $username = $email;
        }
    }
    if (strpos($username, '@') !== FALSE) {
        $username = preg_split('~@~', $username)[0];
    }
    $pwd = checkFieldForXSS($obj, 'password:Password');
    if (is_null($pwd)) {
        $pwd = wp_generate_password(6);
    }

    $config = e4s_getConfig(TRUE, TRUE);

    if (array_key_exists('options', $config)) {
        if (isset($config['options']->pwd)) {
            if (isset($config['options']->pwd)) {
                $matches = preg_match('/' . $config['options']->pwd->regex . '/', $pwd);
                if ($matches < 1) {
                    $pwd = date('a') . $pwd;
                    $matches = preg_match('/' . $config['options']->pwd->regex . '/', $pwd);
                    if ($matches < 1) {
                        $pwd = $pwd . '$';
                        $matches = preg_match('/' . $config['options']->pwd->regex . '/', $pwd);
                        if ($matches < 1) {
                            $pwd = $pwd . 'F';
                            $matches = preg_match('/' . $config['options']->pwd->regex . '/', $pwd);
                            if ($matches < 1) {
                                e4s_addDebugForce($pwd);
                                Entry4UIError(1026, 'Password not strong enough');
                            }
                        }
                    }
                }
            }
        }
    }

    if (e4s_emailIsOnBlackList($email)) {
        Entry4UIError(1028, 'Sorry, You are not allowed to register.', 200, '');
    }
    $GLOBALS['E4S_ALLOW_REGISTRATION'] = TRUE;

    $errors = wp_create_user($username, $pwd, $email);
    if (gettype($errors) === 'integer') {
        $newline = '<br>';
        $body = 'Dear ' . $username . ',' . $newline . $newline;
        $body .= 'Thanks for registering with the Online entry system at ' . E4S_CURRENT_DOMAIN . $newline;
        $body .= 'Please use the password you registered with' . $newline . $newline;
        $body .= 'Once Logged on, please visit your account and change your password to a more memorable one' .$newline;

        e4s_mail($email, E4S_CURRENT_DOMAIN . ' : New User Registration', $body);
        Entry4UISuccess();
    } else {
        $e4sErrors = array();

        foreach ($errors as $error) {
            foreach ($error as $errorName => $values) {
                $e4sErrors[] = str_replace('_', ' ', $errorName);
            }
        }
        $retError = implode(', ', $e4sErrors);
        Entry4UIError(1030, 'Errors raised : ' . $retError);
    }
}