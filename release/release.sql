# // add lastChecked to athlete table
alter table Entry4_Athlete
    drop column activeEndDate2;

alter table Entry4_Athlete
    add lastChecked timestamp default CURRENT_TIMESTAMP after activeEndDate;

# change entryInfo ageGroupId to get from ce NOT entry

# new EventNext up Table
create table Entry4_EventNextUp
(
    egId      int           not null,
    athleteId int           not null,
    heatNo    int default 1 not null,
    constraint Entry4_EventGroupNextUp_egId_uindex
        unique (egId)
);


create table Entry4_CombinedCalc
(
    id      int auto_increment,
    eventId int   null,
    A       float null,
    B       float null,
    c       float null,
    constraint Entry4_CombinedCalc_pk
        primary key (id)
);

alter table Entry4_Bibno
    add compnotes mediumtext null;

alter table Entry4_Clubs
    modify externid varchar(11);

// remove multievents
drop table Entry_multievent

alter table Entry4_uk_CompEvents
    drop column multiid;

create or replace view Entry4_uk_EntryInfo as
select `e`.`id`                                  AS `entryId`,
       `e`.`pb`                                  AS `entryPb`,
       ifnull(`p`.`pb`, 0)                       AS `currentPb`,
       `e`.`variationID`                         AS `productId`,
       `e`.`orderid`                             AS `orderId`,
       `e`.`price`                               AS `price`,
       `e`.`paid`                                AS `paid`,
       `e`.`entity`                              AS `entity`,
       `e`.`periodStart`                         AS `periodStart`,
       `e`.`created`                             AS `created`,
       `e`.`options`                             AS `eOptions`,
       `e`.`userid`                              AS `userId`,
       `e`.`ageGroupID`                          AS `ageGroupId`,
       `e`.`eventAgeGroup`                       AS `eventAgeGroup`,
       `e`.`checkedin`                           AS `checkedin`,
       `b`.`bibno`                               AS `bibNo`,
       `e`.`teambibno`                           AS `teamBibNo`,
       `e`.`discountid`                          AS `discountid`,
       `e`.`confirmed`                           AS `confirmed`,
       `a`.`classification`                      AS `classification`,
       `a`.`id`                                  AS `athleteId`,
       `a`.`aocode`                              AS `aoCode`,
       `a`.`URN`                                 AS `urn`,
       `a`.`firstName`                           AS `firstName`,
       `a`.`surName`                             AS `surName`,
       `a`.`dob`                                 AS `dob`,
       `ev`.`uomid`                              AS `uomId`,
       if(isnull(`t`.`name`), '', `t`.`name`)    AS `teamName`,
       `c`.`id`                                  AS `clubId`,
       `c`.`areaid`                              AS `areaId`,
       `c`.`Clubname`                            AS `clubName`,
       `s`.`Clubname`                            AS `schoolName`,
       `s`.`id`                                  AS `schoolId`,
       `ce`.`CompID`                             AS `compId`,
       `ce`.`ID`                                 AS `ceId`,
       `ce`.`IsOpen`                             AS `isOpen`,
       `ce`.`startdate`                          AS `eventStartDate`,
       `ce`.`options`                            AS `ceOptions`,
       `ce`.`split`                              AS `split`,
       `ce`.`EventID`                            AS `eventid`,
       `ce`.`PriceID`                            AS `priceid`,
       `eg`.`id`                                 AS `egId`,
       date_format(`eg`.`startdate`, '%Y-%m-%d') AS `startdate`,
       date_format(`eg`.`startdate`, '%H.%i')    AS `starttime`,
       `eg`.`name`                               AS `egName`,
       `eg`.`options`                            AS `egOptions`,
       `eg`.`forceClose`                         AS `forceClose`,
       `eg`.`eventNo`                            AS `eventNo`,
       `eg`.`bibSortNo`                          AS `bibSortNo`,
       `eg`.`typeNo`                             AS `typeNo`,
       `ev`.`Gender`                             AS `gender`,
       `ev`.`Name`                               AS `eventName`,
       `ev`.`tf`                                 AS `tf`,
       `ag`.`MaxAge`                             AS `maxAge`,
       `ag`.`Name`                               AS `ageGroup`,
       `uom`.`uomoptions`                        AS `uomOptions`
from (((((((((((`Entry4_uk_Entries` `e` left join `Entry4_Athlete` `a`
                on ((`a`.`id` = `e`.`athleteid`))) left join `Entry4_uk_CompEvents` `ce`
               on ((`ce`.`ID` = `e`.`compEventID`))) left join `Entry4_Events` `ev`
              on ((`ce`.`EventID` = `ev`.`ID`))) left join `Entry4_uk_Bibno` `b`
             on (((`b`.`compid` = `ce`.`CompID`) and
                  (`b`.`athleteid` = `e`.`athleteid`)))) left join `Entry4_AthletePB` `p`
            on (((`ce`.`EventID` = `p`.`eventid`) and (`a`.`id` = `p`.`athleteid`)))) left join `Entry4_Clubs` `c`
           on ((`c`.`id` = `e`.`clubid`))) left join `Entry4_uk_Team` `t`
          on ((`t`.`id` = `e`.`teamid`))) left join `Entry4_Clubs` `s`
         on ((`s`.`id` = `a`.`schoolid`))) left join `Entry4_AgeGroups` `ag`
        on ((`ce`.`AgeGroupID` = `ag`.`id`))) join `Entry4_uk_EventGroups` `eg`) join `Entry4_uom` `uom`)
where ((`ce`.`ID` = `e`.`compEventID`) and (`ce`.`maxGroup` = `eg`.`id`) and (`a`.`id` = `e`.`athleteid`) and
       (`uom`.`id` = `ev`.`uomid`));

alter table Entry4_uk_EventGroups
    drop column aai_id;

# Not sure if this is needed
alter table Entry4_uk_EventGroups
    add parentEgID int null;

# Version controlled from here onwards
# Add ScoringSystem to multiEvents in eventGender Records
# merge UOM into events
create or replace view Entry4_Events as
select `eg`.`id`      AS `ID`,
       `ed`.`Name`    AS `Name`,
       `ed`.`tf`      AS `tf`,
       `eg`.`gender`  AS `Gender`,
       `eg`.`options` AS `options`,
       `ed`.`uomid`   AS `uomid`,
       `eg`.`min`     AS `min`,
       `eg`.`max`     AS `max`,
       u.uomtype eventType,
       u.uomOptions
from `Entry4_EventDefs` `ed`,
     `Entry4_EventGender` `eg`,
     Entry4_uom u
where `ed`.`ID` = `eg`.`eventid`
  and uomid = u.id;


#// Sprint-2
alter table Entry4_uk_Competition
    add resultsAvailable TINYINT default 0 null;

update Entry4_uk_Competition
set resultsAvailable = 1
where options like '%resultsavailable%'

# Sprint 4
alter table Entry4_uk_EventTeamEntries
    add bibNo VARCHAR(10) null after orderid;
alter table Entry4_uk_config
    add version varchar(10) null after systemName;

