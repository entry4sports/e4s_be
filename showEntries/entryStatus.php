<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
$clubHierachy = [];
function e4s_listEntryStatus($obj) {
	global $clubHierachy;
    if ( !isUserLoggedIn() ){
	    Entry4UIError(5600, 'You are not authourised to use this function');
    }

    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $test = checkFieldForXSS($obj, 'test:Competition ID');
    if ( is_null($test) ) {
        $test = false;
    }else{
        $test = true;
    }

    $compObj = e4s_getCompObj($compId);

    if ( !$compObj->isOrganiser() ){
        Entry4UIError(5601, 'You are not authourised to use this function');
    }
	$clubHierachy = e4s_getClubHierarchy();
    $comp = new stdClass();
    $comp->id = $compId;
    $comp->name = $compObj->getName();
    $comp->date = $compObj->getDate();

    $sql = "
        select egId
         , compId
         , entryId
         , entity entityLevel
         , eventName name
         , typeNo
         , DATE_FORMAT(eventstartdate,'" . E4S_MYSQL_PRETTY_DATE . "') as startDate
         , DATE_FORMAT(eventstartdate,'" . E4S_MYSQL_PRETTY_TIME . "') as startTime
         , eventNo
         , egOptions
         , firstName
         , surName
         , classification
         , e.athleteId
         , entrypb pb
         , bibNo
         , teamBibNo
         , clubId
         , clubname countyName
         , ageGroup
         , gender
         , s.heatNo
         , s.laneNo
         , s.heatNoCheckedIn
         , s.laneNoCheckedIn
         , e.paid
         , u.user_email userEmail
         , u.display_name userName
        from " . E4S_TABLE_ENTRYINFO . ' e left join
             ' . E4S_TABLE_SEEDING . ' s on egid = s.eventgroupid and e.athleteid = s.athleteid,
            ' . E4S_TABLE_USERS . ' u
        where compId = ' . $compId . '
        and e.userId = u.id
        order by eventNo, paid desc, surname
    ';

    $result = e4s_queryNoLog($sql);
    $events = array();
    $lastEventNo = 0;

    while ($entryData = $result->fetch_object()) {
        $eventNo = (int)$entryData->eventNo;
        if ( $eventNo !== $lastEventNo ) {
            $nextLaneNo = 0;
        }
        $lastEventNo = $eventNo;
        $type = E4S_EVENT_TRACK;
        if ( $entryData->typeNo[0] !== E4S_EVENT_TRACK ) {
            // Field
            $type = E4S_EVENT_FIELD;
//            continue;
        }
        $entryData->athlete = formatAthlete($entryData->firstName, $entryData->surName);
        $classification = (int)$entryData->classification;
        if ( $classification > 0 ){
	        $entryData->athlete .= formatClasification($type, $classification);
        }
	    if( $compObj->anonymizeAthletes()) {
		    $entryData->athlete = 'Athlete ' . $entryData->athleteId;
	    }
        $egId = (int)$entryData->egId;

        if (!array_key_exists($eventNo, $events)) {
            $events[$eventNo] = new stdClass();
        }
        $event = $events[$eventNo];
        $event->code = $entryData->typeNo;
        $event->type = $type;
        $event->id = $egId;
        $event->eventNo = $eventNo;
        $event->name = $entryData->name;
        $event->date = $entryData->startDate;
        $event->time = $entryData->startTime;
        if ( $event->time === '00:00'){
            $event->time = E4S_NO_TIME;
        }
        $event->gender = 'Male';
        if ($entryData->gender === E4S_GENDER_FEMALE) {
            $event->gender = 'Female';
        }
        $event->ageGroup = $entryData->ageGroup;
        if (!isset($event->participants)) {
            $event->participants = array();
        }

        $event->seeded = false;
        $participant = new stdClass();
        $participant->entryId = $entryData->entryId;
        $participant->paid = $entryData->paid;
        $participant->entityLevel = (int)explode(":",$entryData->entityLevel . ":")[0];
        $participant->athlete = $entryData->athlete;
        $participant->athleteId = $entryData->athleteId;
        $participant->clubId = $entryData->clubId;
        $participant->compId = $entryData->compId;
        $participant->payee = $entryData->userName;
        $participant->payeeEmail = $entryData->userEmail;

        $participant->ageGroup = $event->ageGroup;
        $participant->gender = $event->gender;
        $participant->county = $entryData->countyName;

        $participant->heatNo = 1;
        $participant->laneNo = ++$nextLaneNo;

        $event->participants[$participant->heatNo . '-' . $participant->laneNo] = $participant;
    }
    $comp->events = $events;

    e4s_outputEntryStatus($comp, $test);
}

function e4s_getClubHierarchy(){
    $sql = 'select   c.id clubId
                    ,c.clubname
                    ,county.id countyId
                    ,county.name county
                    ,region.id regionId
                    ,region.name region
            from ' . E4S_TABLE_CLUBS . ' c
                     left join ' . E4S_TABLE_AREA . ' county on (county.id = c.areaid)
                     left join ' . E4S_TABLE_AREA . ' region on (region.id = county.parentid)';
    $result = e4s_queryNoLog($sql);
    $clubs = array();
    while ($clubData = $result->fetch_object()) {
        $club = new stdClass();
        $club->id = $clubData->clubId;
        $club->club = $clubData->clubname;
        $club->countyId = $clubData->countyId;
        $club->county = $clubData->county;
        $club->regionId = $clubData->regionId;
        $club->region = $clubData->region;
        $clubs[$club->id] = $club;
    }
    return $clubs;
}
function formatClasification($type, $classification){
	return ' <span class="athleteClassification"><img class="athleteClassificationIcon" src="/resources/para_icon_small.gif">' . $type . $classification . '</img></span>';
}
function e4s_outputEntryStatus($comp, $test){
    $uiTheme = E4S_JQUERY_THEME;
    $eventsByDate = array();

    foreach($comp->events as $eventNo=>$event){
        if ( !array_key_exists($event->date, $eventsByDate)) {
            $eventsByDate[$event->date] = array();
        }
        $eventsByDate[$event->date][$eventNo] = $event;
    }

?>
    <html lang="en">
<head>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/<?php echo $uiTheme ?>/jquery-ui.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <style>
        .seedingHeader {

        }
        .athleteClassificationIcon {
            width: 0.7vw;
        }
        .athleteClassification {
            font-size: 0.7vw;
            font-weight: bold;
            color: var(--e4s-input-field__text-color);
        }
        .eventHeader{
            display: flex;
            flex-wrap: wrap;
            align-content: center;
            justify-content: flex-start;
            gap: 10px;
            border-bottom: 2px solid blue;
            font-weight: 800;
        }
        .heatHeader {
            font-weight: 600;
            background-color: lightblue;
        }
        .postEvent {

        }
        .lane {
            width: 3vw;
        }
        .options{
            width: 3vw;
        }
        .athlete {
            width: 15vw;
        }
        .payee {
            width: 20vw;
        }
        .ageGroup {
            width: 6vw;
        }
        .gender {
            width: 6vw;
        }
        .org{
            width: 18vw;
        }
        .county{
            width: 10vw;
        }
        .region {

        }
        .eventData{
            width: 100%;
        }
        .notPaid {
            background-color: pink;
        }
    </style>
    <script>
        function launchOptionsForEntry(data){
            var url = "/#/actions/" + data.compId + "/" + data.athleteId + "/" + data.clubId + "/" + data.entryId;
            window.open(url, '_self');
        }
        $( function() {
            $( "#dates" ).tabs();
        } );
    </script>
</head>
<body>
<h1 id="seedingHeader">Entry Status for competition : <?php echo $comp->id . ' : ' . $comp->name ?></h1>
<div id="dates">
    <ul>
        <?php
        $dayNo = 1;
        foreach($eventsByDate as $date=>$events){
            ?>
            <li><a href="#day-<?php echo $dayNo++ ?>"><?php echo $date?></a></li>
            <?php
        }
        ?>
    </ul>
    <?php
        $dayNo = 1;
        foreach($eventsByDate as $events){
        ?>
            <div id="day-<?php echo $dayNo ?>">
                <?php
                    $eventType = '';
                    foreach($events as $event){
                        if ( $eventType !== $event->type){
                            if ( $eventType !== ''){
                               // footer info
                            }
                            // header info
                            $eventType = $event->type;
                        }
                        e4s_displayEventHeaderForEntries($event);

                        e4s_displayHeatParticipants($event->code, $event->participants, $event->type, false);

                        e4s_displayPostEvent();
                    }
                ?>
            </div>
        <?php
            $dayNo++;
        }
    ?>

</div>

</body>
    </html>
<?php
    exit();
}

function e4s_displayEventHeaderForEntries($event){
    ?>
        <div id="header_<?php echo $event->code; ?>" class="eventHeader">
    <div>
       <?php echo $event->time; ?>
    </div>
    <div>
        <?php echo sprintf('%s :', $event->code); ?>
    </div>
    <div>
        <?php echo $event->name; ?>
</div>
        </div>
    <?php
}
function e4s_displayEventHeaderTable($event){
    ?>
    <table id="header_<?php echo $event->code; ?>" class="eventHeader">
        <tr>
            <td>
                <?php echo $event->time; ?>
            </td>
            <td>
                 <?php echo sprintf('%s :', $event->code); ?>
            </td>
            <td>
                 <?php echo $event->name; ?>
            </td>
        </tr>
    </table>
    <?php
}
function e4s_displayEventHeats($id, $participants, $type){
    $lastHeat = 0;
    $heatParticipants = array();
    $lastKey = '';
    foreach($participants as $key=>$heatData) {
        if ( $lastHeat !== 0 and $lastHeat !== $heatData->heatNo ){
            e4s_displayHeatParticipants($id . '-' . $key, $heatParticipants, $type);
            $heatParticipants = array();
        }
        $lastHeat =  $heatData->heatNo;
        $heatParticipants[] = $heatData;
        $lastKey = $key;
    }
    e4s_displayHeatParticipants($id . '-' . $lastKey, $heatParticipants, $type);
}
function e4s_displayPostEvent(){
    echo '<div class="postEvent">&nbsp</div>';
}
function e4s_entryGetEntity($participant, $level = 0){
    global $clubHierachy;
    if ( $level === 0 ){
        $level = $participant->entityLevel;
    }
    if ( array_key_exists($participant->clubId, $clubHierachy)){
        $club = $clubHierachy[$participant->clubId];
	    if ( $level === 1 ){
		    return $club->club;
	    }
        if ( $level === 2 ){
            return $club->county;
        }
        if ( $level === 3 ){
            return $club->region;
        }
    }
    return "-";
}
function e4s_displayHeatParticipants($id, $heatParticipants, $type, $seeded = true){
    $headerHTML = '';

    ?>
    <table id="heatParticipants_<?php echo $id; ?>" class="eventData">

        <?php
        $pos = 1;
        foreach($heatParticipants as $heatParticipant){
            if ( $headerHTML === '' ){
                $headerHTML = '<tr class="heatHeader">';
                $headerHTML .= '<td colspan="6"  >';
                $headerHTML .= '</td>';
                $headerHTML .= '</tr>';

                $headerHTML .= '<tr class="heatHeader">';
                $headerHTML .= '<td class="heatColHeader" >';
                $headerHTML .= '&nbsp;';
                $headerHTML .= '</td>';
                $headerHTML .= '<td class="heatColHeader" >';
                $headerHTML .= 'Paid';
                $headerHTML .= '</td>';
                $headerHTML .= '<td class="heatColHeader" >';
                $headerHTML .= 'Athlete';
                $headerHTML .= '</td>';
                $headerHTML .= '<td class="heatColHeader" >';
                $headerHTML .= 'Event Age Group';
                $headerHTML .= '</td>';
                $headerHTML .= '<td class="heatColHeader" >';
                $headerHTML .= 'Gender';
                $headerHTML .= '</td>';
	            $headerHTML .= '<td class="heatColHeader" >';
	            $headerHTML .= 'Payee';
	            $headerHTML .= '</td>';
                $headerHTML .= '<td class="heatColHeader" >';
                $headerHTML .= 'Organisation';
                $headerHTML .= '</td>';
	            $headerHTML .= '<td class="heatColHeader" >';
	            $headerHTML .= 'County';
	            $headerHTML .= '</td>';
	            $headerHTML .= '<td class="heatColHeader" >';
	            $headerHTML .= 'Area';
	            $headerHTML .= '</td>';
                $headerHTML .= '</tr>';
                echo $headerHTML;
            }
            $class = '';
            if ( (int)$heatParticipant->paid === E4S_ENTRY_NOT_PAID ) {
	            $class = 'notPaid';
            }
            echo "<tr class='$class'>";
            ?>
                <td class="options">
                    <button type='button' class='ui-button ui-corner-all ui-widget' style='font-size:0.6em !important;' onclick='launchOptionsForEntry(<?php echo json_encode($heatParticipant) ?>); return false;' >Options</button>
                </td>
                <td class="lane">
                    <?php echo (int)$heatParticipant->paid === E4S_ENTRY_PAID ? "Yes" : "No"; ?>
                </td>
                <td class="athlete">
                    <?php echo $heatParticipant->athlete; ?>
                </td>
                <td class="ageGroup">
                    <?php echo $heatParticipant->ageGroup; ?>
                </td>
                <td class="gender">
                    <?php echo $heatParticipant->gender; ?>
                </td>
                <td class="payee">
                    <?php echo $heatParticipant->payee ?> ( <a href="mailto:<?php echo $heatParticipant->payeeEmail ?>"><?php echo $heatParticipant->payeeEmail ?></a> )
                </td>
                <td class="org">
                    <?php echo $heatParticipant->county; ?>
                </td>
                <td class="county">
                    <?php echo e4s_entryGetEntity($heatParticipant,2); ?>
                </td>
                <td class="region">
	                <?php
                        echo e4s_entryGetEntity($heatParticipant);
                    ?>
                </td>


            </tr>
            <?php
        }

        if ( sizeof($heatParticipants) === 0 ){
            // No participants
            ?>
	        <tr class="heatHeader">
                <td colspan="6">
                    <span id="heat_<?php echo  $id?>">No Entries as yet</span>
                </td>
	        </tr>
            <?php
        }
        ?>

    </table>
    <?php
}