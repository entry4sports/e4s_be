<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
if (!isset($_GET['compid'])) {
    return;
}
$compid = $_GET['compid'];
$result = mysqli_query($conn, 'SELECT c.* FROM `' . E4S_TABLE_COMPETITON . '` as c where c.id=' . $compid);

while ($row = mysqli_fetch_array($result, TRUE)) {
    $adjustedClose = $row['EntriesClose'];

    $eventDate = date('jS F Y', strtotime($row['Date']));
    $closeDate = strtotime($adjustedClose);
    $timeTxt = 'h:ia ';
    if (time() > $closeDate) {
        $entriesCloseText = 'Entries are now closed.';
    } elseif (date('Y-m-d') == explode(' ', $row['EntriesClose'])[0]) {
        $entriesCloseText = 'Entries close today';
        if (strpos($row['EntriesClose'], '00:00:00') === FALSE) {
            $close = new DateTime();
            $close->setTimestamp($closeDate);

            $entriesCloseText .= ' at ' . date_format($close, $timeTxt);
        }

    } else {

        if (strpos($row['EntriesClose'], '00:00:00') === TRUE) {
            $timeTxt = '';
        }
        $entriesCloseText = 'Entries close : ' . date($timeTxt . 'jS F Y', $closeDate);
    }

    echo "<span class='showEntriesTitle'>Entries for " . $row['Name'] . ' on the ' . $eventDate . '. ' . $entriesCloseText . '</span>';
    exit();
}
mysqli_close($link);
?>