/**
 * Created by paul on 17/04/2017.
 * Modified 28/11/2018
 */
let compId = 0;
let prevSearch = null;
function getBaseV5Path() {
    return "/entry/v5/";
}

function correctEntryCounts() {
    jQuery(".pq-grid-cell:contains('No Entries')").parent().prev().find(".eventCount").hide();
    jQuery("noentrycnt").parent().parent().find(".eventCount").hide();
    jQuery(".eventCount:contains('(1 Entries)')").text("(1 Entry)");
}

function postGrid() {
    // correctEntryCounts();
    $("[href='http://paramquery.com/pro']").hide();
    $(".pq-group-title-cell").css("overflow", "visible");
    $(".pq-grid-col").css("border-right-color", "white");
    $("#displayMsg").html(''); // clear the message
}

function gridObj() {
    return jQuery("#grid_array");
}

function formatDate(str) {
    return jQuery.format.date(new Date(str), "ddd, D MMMM")
}

function formateTitle(ui){
    let entryCount = " (" +  ui.rowData.pq_items + " entries)"
    let plural = "";
    if ( ui.rowData.children.length > 0 ){
        if ( ui.rowData.children[0].isTeam === 1 ){
            // count the unique teams
            let teamCount = 0;
            let teamNames = [];
            for (let i = 0; i < ui.rowData.children.length; i++){
                let checkTeamName = ui.rowData.children[i].name.split(" (")[0];
                if ( teamNames.indexOf(checkTeamName) === -1 ){
                    teamNames.push(checkTeamName);
                    teamCount++;
                    if ( teamCount > 1 ){
                        plural = "s";
                    }
                }
            }
            entryCount = " (" +  teamCount + " team" + plural + ")";
        }
    }
    return ui.cellData + entryCount;
}
function showEntries(localCompId, oddRowColour, evenRowColour, coptions) {
    // set global var
    compId = localCompId;
    prevSearch = getPreviousSearch();
    //noinspection ES6ConvertVarToLetConst
    let timetableURL = getBaseV5Path() + "/showEntries/getEntriesTimetableV2.5.php?compid=" + compId;

    showCounts(compId);
    jQuery(function () {
        gridObj().on("pqgridrefresh", function (event, ui) {
            postGrid();
        });

        var thisGrid = {};
        jQuery.e4s_collapsed = false;
        jQuery.ajax({
            url: timetableURL,
            cache: false,
            async: true,
            dataType: "JSON",
            success: function (response) {
                if (response.data.length > 0) {
                    var colArr = getDataCounts(response.data),
                        column = null,
                        filter = null;
                    var dataModel = {
                        location: "local",
                        sorting: "local",
                        data: [[]]
                    };
                    var groupModel = {
                        on: true,
                        dataIndx: ['startdate', 'starttime', 'event'],
                        summaryInTitleRow: 'all',
                        titleInFirstCol: true,
                        header: false,
                        fixCols: false,
                        indent: 10,
                        collapsed: [false, false, true],
                        title: [
                            "{0}",
                            "{0}",
                            formateTitle
                        ]
                    };
                    var obj = {
                        width: "95%",
                        height: "90%",
                        colModel: createColModel(colArr, coptions),
                        filter: function (event, ui) {
                            // toggleCategories(this, false);
                        },
                        stripeRows: true,
                        complete: function (event, ui) {
                            postGrid();
                        },
                        render: function (event, ui) {
                            postGrid();
                        },
                        refresh: function (event, ui) {
                            postGrid();
                        },
                        rowInit: function (ui) {
                            if (ui.rowData.compeventid) {
                                var oddBG = 'background-color:' + oddRowColour + ';';
                                var evenBG = "";
                                if (evenRowColour !== "") {
                                    evenBG = 'background-color:' + evenRowColour + ';';
                                }
                                var style = '';

                                if (ui.rowIndx % 2) {
                                    style += oddBG;
                                } else {
                                    style += evenBG;
                                }

                                return {"style": style};
                            }
                        },
                        sortModel: {
                            sorter: [{dataIndx: 'startdate', dir: 'up'}, {
                                dataIndx: 'starttime',
                                dir: 'up'
                            }, {dataIndx: 'event', dir: 'up'}],
                            space: true,
                            multiKey: null
                        },
                        toolbar: {
                            items: [
                                {
                                    type: 'button',
                                    icon: 'ui-icon-arrow-2-n-s',
                                    label: "Toggle Categories",
                                    listener: function () {
                                        toggleCategories(this);
                                    }
                                },
                                {
                                    type: 'select',
                                    label: 'Format: ',
                                    attr: 'id="export_format"',
                                    options: [{xlsx: 'Excel', csv: 'Csv', htm: 'Html', json: 'Json'}]
                                },
                                {
                                    type: 'button',
                                    label: "Export",
                                    icon: 'ui-icon-arrowthickstop-1-s',
                                    listener: function () {
                                        var format = $("#export_format").val(),
                                            blob = this.exportData({
                                                //url: "/pro/demos/exportData",
                                                format: format,
                                                nopqdata: true, //applicable for JSON export.
                                                render: true
                                            });
                                        if (typeof blob === "string") {
                                            blob = new Blob([blob]);
                                        }
                                        saveAs(blob, "pqGrid." + format);
                                    }
                                },
                                {
                                    type: 'button',
                                    icon: 'ui-icon-print',
                                    label: 'Print',
                                    listener: function () {
                                        var exportHtml = this.exportData({
                                                title: 'Entry4Sports Entries',
                                                format: 'htm',
                                                render: true
                                            }),
                                            newWin = window.open('', '', 'width=1200, height=700'),
                                            doc = newWin.document.open();
                                        doc.write(exportHtml);
                                        doc.close();
                                        newWin.print();
                                    }
                                },
                                {
                                    type: 'button',
                                    icon: 'ui-icon-refresh',
                                    label: 'Refresh',
                                    listener: function () {
                                        gridObj().pqGrid("reset", {sort: true, filter: true});
                                        toggleCategories(this, true);
                                    }
                                },
                                {
                                    type: 'button',
                                    icon: 'ui-icon-refresh',
                                    label: 'Contact Organiser',
                                    listener: function () {
                                        window.open('/#/contact-org/' + localCompId, 'e4sContact');
                                    }
                                }
                            ]
                        },
                        dataModel: dataModel,
                        groupModel: groupModel,
                        showBottom: true,
                        editable: false,
                        selectionModel: {type: 'cell'},
                        filterModel: {header: true, type: 'local', menuIcon: true, mode: 'AND'},
                        title: "",
                        numberCell: {show: false},
                        columnBorders: false,
                        collapsible: false,
                        resizable: true,
                        wrap: false,
                        virtualX: true, virtualY: true,
                        cellDblClick: function (event, ui) {
                            e4sCellDblClick(event, ui);
                        }
                    };
                    e4s_WaitingList(obj);
                    e4s_myEntries(obj);
                    e4s_showLogin(obj);
                    thisGrid = gridObj().pqGrid(obj);
                    gridObj().pqGrid("showLoading");

                    thisGrid.pqGrid("option", "dataModel.data", response.data);

                    column = thisGrid.pqGrid("getColumn", {dataIndx: "tf"});
                    //noinspection ES6ConvertVarToLetConst
                    filter = column.filter;
                    filter.cache = null;
                    filter.options = filterTFOptions(thisGrid);
                    if ( prevSearch !== null ){
                        filter.crules = [{condition: 'equal', value: prevSearch.tf}];
                    }

                    if (colArr.genderCnt > 1) {
                        column = thisGrid.pqGrid("getColumn", {dataIndx: "gender"});
                        filter = column.filter;
                        filter.cache = null;
                        filter.options = filterGenderOptions(thisGrid);
                        if ( prevSearch !== null ){
                            filter.crules = [{condition: 'equal', value: prevSearch.gender}];
                        }
                    }

                    if (colArr.athleteCnt > 0 || colArr.teamCnt > 0) {
                        column = thisGrid.pqGrid("getColumn", {dataIndx: "name"});
                        filter = column.filter;
                        filter.cache = null;
                        filter.options = thisGrid.pqGrid("getData", {dataIndx: ["name"]});
                        if ( prevSearch !== null ){
                            filter.crules = [{condition: 'begin', value: prevSearch.name}];
                        }
                    }

                    if (colArr.schoolCnt > 0) {
                        column = thisGrid.pqGrid("getColumn", {dataIndx: "schoolName"});
                        filter = column.filter;
                        filter.cache = null;
                        filter.options = thisGrid.pqGrid("getData", {dataIndx: ["schoolName"]});
                        if ( prevSearch !== null ){
                            filter.crules = [{condition: 'begin', value: prevSearch.schoolName}];
                        }
                    } else {
                        column = thisGrid.pqGrid("getColumn", {dataIndx: "clubName"});
                        filter = column.filter;
                        filter.cache = null;
                        filter.options = thisGrid.pqGrid("getData", {dataIndx: ["clubName"]});
                        if ( prevSearch !== null ){
                            filter.crules = [{condition: 'begin', value: prevSearch.clubName}];
                        }
                    }

                    if (colArr.countyCnt > 0) {
                        column = thisGrid.pqGrid("getColumn", {dataIndx: "county"});
                        filter = column.filter;
                        filter.cache = null;
                        filter.options = thisGrid.pqGrid("getData", {dataIndx: ["county"]});
                    }

                    if (colArr.regionCnt > 0) {
                        column = thisGrid.pqGrid("getColumn", {dataIndx: "region"});
                        filter = column.filter;
                        filter.cache = null;
                        filter.options = thisGrid.pqGrid("getData", {dataIndx: ["region"]});
                    }

                    if (colArr.ageGroupCnt > 1) {
                        column = thisGrid.pqGrid("getColumn", {dataIndx: "ageGroup"});
                        filter = column.filter;
                        filter.cache = null;
                        filter.options = thisGrid.pqGrid("getData", {dataIndx: ["ageGroup"]});
                        if ( prevSearch !== null && prevSearch.ageGroup){
                            filter.crules = [{condition: 'range', value: prevSearch.ageGroup.split(",")}];
                        }
                    }
                    if (colArr.athleteAgeGroupCnt > 0) {
                        column = thisGrid.pqGrid("getColumn", {dataIndx: "athleteAgeGroup"});
                        filter = column.filter;
                        filter.cache = null;
                        filter.options = thisGrid.pqGrid("getData", {dataIndx: ["athleteAgeGroup"]});
                        if ( prevSearch !== null && prevSearch.athleteAgeGroup){
                            filter.crules = [{condition: 'range', value: prevSearch.athleteAgeGroup.split(",")}];
                        }
                    }

                    if (colArr.classificationCnt > 1) {
                        column = thisGrid.pqGrid("getColumn", {dataIndx: "classification"});
                        filter = column.filter;
                        filter.cache = null;
                        filter.options = thisGrid.pqGrid("getData", {dataIndx: ["classification"]});
                    }

                    // $grid.pqGrid("hideLoading");
                    // $grid.pqGrid("refreshDataAndView");

                    jQuery.get(getBaseV5Path() + '/showEntries/getEntriesTitleInfoV2.php?compid=' + compId, function (data) {
                        gridObj().pqGrid("option", "title", data);
                        // if (colArr.teamCnt > 0) {
                            gridObj().pqGrid("refreshDataAndView");
                        // }
                    });
                }

                gridObj().pqGrid("hideLoading");
                postGrid();
                $(".pq-toolbar > button:first").click();
                $(".pq-toolbar > button:first").click();
            }
        });
    });
}

function showCounts(compId) {
    return ;
    //noinspection ES6ConvertVarToLetConst
    var url = getBaseV5Path() + "/entries/getEntryCounts.php?compid=" + compId;
    jQuery("#compInfoSpan").hide();
    jQuery.get(url, function (data) {
        data = JSON.parse(data);
        var counters = data.counters;
        jQuery("#entryCounters").css("display", "").css("text-align", "center");
        jQuery("#athleteCount").text(counters.uniqueCnt).css("font-size", "18px").css("font-weight", "800");
        jQuery("#entryCount").text(counters.totalCnt).css("font-size", "18px").css("font-weight", "800");
        jQuery("#entryWaitingCount").text(counters.waitingEntries).css("font-size", "18px").css("font-weight", "800");
        jQuery("#relayCount").text(counters.relayCnt).css("font-size", "18px").css("font-weight", "800");
        jQuery("#relayAthleteCount").text(counters.relayAthleteCnt).css("font-size", "18px").css("font-weight", "800");
    });
}

function createColModel(colArr, coptions) {
    let colM = [
        {
            title: "Date",
            width: 10,
            dataIndx: "startdate",
            editable: false,
            hidden: true,
            dataType: "date",
            format: "dd M yy"
        },
        {title: "Time", width: 10, dataIndx: "starttime", editable: false, hidden: true, dataType: "number"},
        {
            title: 'Event', nodrag: true, nodrop: true, minWidth: 150, menuIcon: false,
            render: function (ui) {
                postGrid();
            },
            filter: {
                // crules: [{condition: 'begin'}]
                menuIcon: false
            },
            render: function (ui) {
                if (ui.cellData) {
                    if (ui.cellData.indexOf("noentrycnt") > 0) {
                        ui.rowData.pq_items = 0;
                        ui.cellData = ui.cellData.replace("&lt;noentrycnt>", "");
                        ui.formatVal = ui.cellData;
                        return ui;
                    }
                    if (ui.cellData === "00.00") {
                        ui.cellData = "TBC";
                        ui.formatVal = ui.cellData;
                        return ui;
                    }
                }
            }
        }
    ];

    if (colArr.eventCnt > 0) {
        colM.push(
            {
                title: "Event", width: 200, dataIndx: "event", editable: false, hidden: true, dataType: "string",
                filter: {
                    crules: [{condition: 'begin'}]
                }
            });
    }
    colM.push(
        {title: "isopen", width: 60, dataIndx: "isopen", editable: false, hidden: true}
    );


        colM.push(
            {
                title: "Type", width: 75, editable: false,
                dataIndx: "tf",
                exportRender: false,
                filter: {
                    crules: [
                        {condition: 'range'}
                    ]
                },
                render: function (ui) {
                    if (ui.rowData.pq_level >= 0 || (!isOrganiser && ui.rowData.userId !== userId) || ui.rowData.isTeam === 1 ) {
                        return '';
                    }
                    return "<button type='button' class='ui-button ui-corner-all ui-widget' style='font-size:0.6em !important;' onclick='launchOptionsForEntry(" + JSON.stringify(ui.rowData) + "); return false;' >Options</button>";
                }
            }
        );


    // Gender
    if (colArr.genderCnt > 0) {
        colM.push(
            {
                title: "Gender", width: 75, editable: false, dataIndx: "gender", render: function (ui) {
                    return (renderGender(ui));
                },
                filter: {crules: [{condition: 'range'}]}
            }
        );
    }

    // Athlete or Team Name
    colM.push(
        {
            title: "Name",
            width: 350,
            editable: false,
            dataIndx: "name",
            filter: {crules: [{condition: 'begin'}]}
        }
    );

    // School or club. currentl mutually exclusive
    if (colArr.schoolCnt > 0) {
        colM.push(
            {
                title: "School",
                width: 250,
                editable: false,
                dataIndx: "schoolName",
                filter: {crules: [{condition: 'begin'}]}
            }
        );
    } else {
        colM.push(
            {
                title: "Association",
                width: 250,
                editable: false,
                dataIndx: "clubName",
                filter: {crules: [{condition: 'begin'}]}
            }
        );
    }

    // County Name
    if (colArr.countyCnt > 0) {
        colM.push(
            {
                title: "County",
                width: 100,
                editable: false,
                dataIndx: "county",
                filter: {crules: [{condition: 'begin'}]}
            }
        )
    }
    if (colArr.regionCnt > 0) {
        colM.push(
            {
                title: "Region",
                width: 100,
                editable: false,
                dataIndx: "region",
                filter: {crules: [{condition: 'begin'}]}
            }
        )
    }

    // AgeGroup
    if (colArr.ageGroupCnt > 1) {
        colM.push(
            {
                title: "maxage", width: 1, dataType: "integer", dataIndx: "maxage", editable: false, hidden: true
            },
            {
                title: "Event Age Group", width: 150, editable: false, dataIndx: "ageGroup",
                filter: {crules: [{condition: 'range'}]}
            }
        );
    }

    // AthleteAgeGroup
    // if (colArr.athleteAgeGroupCnt > 0){
    if (coptions.showAthleteAgeInEntries) {
        colM.push(
            {
                title: "Athlete Age Group", width: 150, editable: false, dataIndx: "athleteAgeGroup",
                filter: {crules: [{condition: 'range'}]}
            }
        );
    }


    // PB
    colM.push({
        title: "Seed Perf",
        width: 100,
        editable: false,
        dataType: "float",
        format: "#.00",
        dataIndx: "pb",
        render: function (ui) {
            if (ui.rowData.pq_level >= 0 || (!isOrganiser && ui.rowData.userId !== userId) || ui.Export ) {
                return ui.cellData;
            }
            if (ui.rowData.isTeam === 1 ) {
                return '';
            }
            return '<a href="#" onclick="editSeedPerf(' + ui.rowData.entryId + '); return false;"><i style="font-family: \'Material Icons\' !important; color:red" class="material-icons red-text inherit">edit</i>' + ui.cellData + '</a>';
        }
    });

    // Disabled Classification
    if (colArr.classificationCnt > 1) {
        colM.push(
            {
                title: "Classification", dataType: "string", editable: false, dataIndx: "classification", filter: {
                    type: 'select',
                    condition: 'equal',
                    valueIndx: "classification",
                    labelIndx: "classification",
                    prepend: {'': 'All'},
                    listeners: ['change']
                }
            });
    }

    return colM;
}

function getPreviousSearch() {
    let savedSearch = localStorage.getItem(compId + "_search");
    if (savedSearch !== null) {
        savedSearch = JSON.parse(savedSearch);
    }
    // clear the search from storage
    localStorage.removeItem(compId + "_search");
    return savedSearch;
}
function launchOptionsForEntry(data){
    // save any search
    let savedSearch = {};
    $(".pq-grid-hd-search-field").each(function(){savedSearch[$(this).attr("name")] = $(this).val();});
    localStorage.setItem(compId + "_search", JSON.stringify(savedSearch));

    var url = "/#/actions/" + compId + "/" + data.athleteId + "/" + data.clubId + "/" + data.entryId;
    window.open(url, '_self');
}
function filterTFOptions($grid) {
    var values = $grid.pqGrid("getData", {dataIndx: ["tf"]});
    var retArr = [];
    var obj = {};
    for (v in values) {
        var tf = values[v].tf;

        switch (tf) {
            case '':
                obj = {"": "Blank"};
                break;
            case 'F':
                obj = {"F": "Field"};
                break;
            case 'T':
                obj = {"T": "Track"};
                break;
            case 'R':
                obj = {"R": "Road"};
                break;
            case 'X':
                obj = {"X": "X-Country"};
                break;
            case 'B':
                obj = {"B": "Multi"};
                break;
            case 'M':
                obj = {"M": "Multi"};
                break;
            default:
                obj = {tf: tf};
                break;
        }
        retArr.push(obj);
    }
    return retArr;
}

function renderGender(ui) {
    switch (ui.cellData) {
        case 'F':
            return 'Female';
        case 'M':
            return 'Male';
        case 'O':
            return 'Open';
    }
    return ui.cellData;
}

function filterGenderOptions($grid) {
    var values = $grid.pqGrid("getData", {dataIndx: ["gender"]});
    var retArr = [];
    var obj = {};
    for (v in values) {
        var gender = values[v].gender;

        switch (gender) {
            case '':
                obj = {"": "Blank"};
                break;
            case 'F':
                obj = {"F": "Female"};
                break;
            case 'M':
                obj = {"M": "Male"};
                break;

            default:
                obj = {gender: gender};
                break;
        }
        retArr.push(obj);
    }
    return retArr;
}

function getDataCounts(data) {
    var c = 0,
        colArr = {
            event: {},
            eventCnt: 0,
            startdate: {},
            startdateCnt: 0,
            tf: {},
            tfCnt: 0,
            gender: {},
            genderCnt: 0,
            name: {},
            athleteCnt: 0,
            teamCnt: 0,
            clubName: [],
            clubCnt: 0,
            countyname: [],
            countyCnt: 0,
            regionname: [],
            regionCnt: 0,
            schoolName: [],
            schoolCnt: 0,
            classification: {},
            classificationCnt: 0,
            athleteAgeGroup: {},
            athleteAgeGroupCnt: 0,
            ageGroup: {},
            ageGroupCnt: 0
        };

    for (c = 0; c < data.length; c += 1) {
        if (typeof colArr.event[data[c].event] === "undefined") {
            colArr.event[data[c].event] = 0;
            colArr.eventCnt += 1;
        }
        colArr.event[data[c].event] += 1;

        if (typeof colArr.startdate[data[c].startdate] === "undefined") {
            colArr.startdate[data[c].startdate] = 0;
            colArr.startdateCnt += 1;
        }
        colArr.startdate[data[c].startdate] += 1;

        if (data[c].classification !== null && typeof data[c].classification !== "undefined") {
            if (data[c].classification !== "0") {
                if (typeof colArr.classification[data[c].classification] === "undefined") {
                    colArr.classification[data[c].classification] = 0;
                    colArr.classificationCnt += 1;
                }
            }
        }
        colArr.classification[data[c].classification] += 1;

        if (typeof colArr.gender[data[c].gender] === "undefined") {
            colArr.gender[data[c].gender] = 0;
            colArr.genderCnt += 1;
        }
        colArr.gender[data[c].gender] += 1;

        if (typeof colArr.tf[data[c].tf] === "undefined") {
            colArr.tf[data[c].tf] = 0;
            colArr.tfCnt += 1;
        }
        colArr.tf[data[c].tf] += 1;

        if (data[c].name !== null && typeof data[c].name !== "undefined") {
            if (typeof colArr.name[data[c].name] === "undefined") {
                colArr.name[data[c].name] = 0;
                if ( data[c].isTeam === "1"){
                    colArr.teamCnt += 1;
                }else{
                    colArr.athleteCnt += 1;
                }
            }
        }
        colArr.name[data[c].name] += 1;

        if (data[c].county !== "") {
            if (typeof colArr.countyname[data[c].county] === "undefined") {
                colArr.countyname[data[c].county] = 0;
                colArr.countyCnt += 1;
            }
        }
        colArr.countyname[data[c].county] += 1;

        if (data[c].region !== "") {
            if (typeof colArr.regionname[data[c].region] === "undefined") {
                colArr.regionname[data[c].region] = 0;
                colArr.regionCnt += 1;
            }
        }
        colArr.regionname[data[c].region] += 1;

        if (typeof colArr.ageGroup[data[c].ageGroup] === "undefined") {
            colArr.ageGroup[data[c].ageGroup] = 0;
            colArr.ageGroupCnt += 1;
        }
        colArr.ageGroup[data[c].ageGroup] += 1;

        if (typeof colArr.athleteAgeGroup[data[c].athleteAgeGroup] === "undefined") {
            colArr.athleteAgeGroup[data[c].athleteAgeGroup] = 0;
            colArr.athleteAgeGroupCnt += 1;
        }
        colArr.athleteAgeGroup[data[c].athleteAgeGroup] += 1;

        if (data[c].schoolName !== "" && typeof data[c].schoolName !== "undefined") {
            if (typeof colArr.schoolName[data[c].schoolName] === "undefined") {
                colArr.schoolName[data[c].schoolName] = 0;
                colArr.schoolCnt += 1;
            }
        }
        colArr.schoolName[data[c].schoolName] += 1;
    }
    return colArr;
}

// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------

function toggleCategories(grid, collapse) {
    if (typeof collapse === "undefined") {
        jQuery.e4s_collapsed = !jQuery.e4s_collapsed;
    } else {
        jQuery.e4s_collapsed = collapse;
    }

    grid.Group().option({
        collapsed: [false, false, jQuery.e4s_collapsed]
    });
}