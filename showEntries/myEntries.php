<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
function e4s_myEntriesReport($obj){

    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $compObj = e4s_GetCompObj($compId, TRUE);
    $compObj->setClubComp(true);
    if ( $compObj->getClubId() !== 0 ) {
        $meta = new stdClass();
        $meta->clubCompInfo = $compObj->getClubCompData();
    }else{
        Entry4UIError(4088, 'Invalid Competition');
    }

    $entries = array();

    foreach($meta->clubCompInfo->entryData->entries as $egId){
        foreach($egId->athletes as $indivAthleteEntry){
            $dataObj = new stdClass();
            $dataObj->entryName = $indivAthleteEntry->entryName;
            $dataObj->bibNo = $indivAthleteEntry->teamBibNo;
            $dataObj->event = $egId->eventGroup;
            $dataObj->ageGroup = $egId->eventAgeGroup;
            $entries[] = $dataObj;
        }
        foreach($egId->teams as $teamEntry){
            $dataObj = new stdClass();
            $dataObj->entryName = $teamEntry;
            $dataObj->bibNo = 'Surrey Team';
            $dataObj->event = $egId->eventGroup;
            $dataObj->ageGroup = $egId->eventAgeGroup;
            $entries[] = $dataObj;
        }
    }
    usort($entries, function ($a, $b) {
        if ($a->entryName < $b->entryName){
            return -1;
        }
        if ($a->entryName > $b->entryName){
            return 1;
        }
        return 0;
    });

    ?>
    <link rel="stylesheet" href="<?php echo E4S_PATH ?>/css/pqgrid6.min.css">
    <link rel="stylesheet" href="<?php echo E4S_PATH ?>/css/pqgrid6.ui.min.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.3/themes/<?php echo E4S_JQUERY_THEME ?>/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo E4S_PATH ?>/js/pqgrid6.min.js"></script>
    <script type="text/javascript" src="<?php echo E4S_PATH ?>/js/jszip.min.js"></script>
    <script type="text/javascript" src="<?php echo E4S_PATH ?>/js/FileSaver.min.js"></script>
    <div id="myEntriesGrid">   </div>
    <table id="myEntries">
        <tr>
            <td>Firsname</td>
            <td>Surname</td>
            <td>Bib No</td>
            <td>Age Group</td>
            <td>Event</td>
        </tr>
        <?php
        foreach($entries as $entry){
            $athleteArr = preg_split('~ ~',$entry->entryName);
            $firstName = $athleteArr[0];
            $surName = str_replace($athleteArr[0] . ' ','',$entry->entryName,);
        ?>
            <tr>
                <td><?php echo $firstName; ?></td>
                <td><?php echo $surName; ?></td>
                <td><?php echo $entry->bibNo; ?></td>
                <td><?php echo $entry->ageGroup; ?></td>
                <td><?php echo $entry->event; ?></td>
            </tr>
        <?php
        }
        ?>
    </table>
    <script>
        function changeToGrid() {
            var tbl = $("#myEntries");
            var obj = $.paramquery.tableToArray(tbl);

            var newObj = { width: 2000, height: 1000, title: "My Entries", flexWidth: false };
            newObj.dataModel = { data: obj.data };
            newObj.colModel = obj.colModel;
            newObj.toolbar = {
                items: [
                    {
                        type: 'button',
                        label: "Export to Excel",
                        icon: 'ui-icon-arrowthickstop-1-s',
                        listener: function () {
                            var blob = this.exportData({
                                format: 'xlsx',
                                render: true,
                                type: 'blob'
                            });
                            saveAs(blob, "myEntries.xlsx" );
                        }
                    }]
            };
            for(let c in obj.colModel){
                let width = 200;
                if ( c === '0' ){
                    width = 400;
                }
                newObj.colModel[c].width = width;
            }
             // newObj.pageModel = { rPP: 30, type: "local" };
            $("#myEntriesGrid").pqGrid(newObj);
            tbl.css("display", "none");
        }
        changeToGrid();
    </script>
<?php
    exit();
}