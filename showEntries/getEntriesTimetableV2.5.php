<?php
//
// MulitiId will point to the ide in the MultiEvent Table
// -ID means its a multi event but dont show in Timetable the individual elements
// +ID means show the individual elements ( no actual + )
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'eventteams/commonEventTeam.php';
include_once E4S_FULL_PATH . 'classes/cacheClass.php';

if (!isset($_GET['compid'])) {
    return;
}

$compid = (int)$_GET['compid'];
$cacheObj = new cacheClass($compid);
$compObj = e4s_getCompObj($compid);

//$data = $cacheObj->readCache(E4S_CACHE_ENTRIES, E4S_OPTIONS_ARRAY, $compObj, e4s_isLiveDomain());
$data = $cacheObj->readCache(E4S_CACHE_ENTRIES, E4S_OPTIONS_ARRAY, $compObj, true);
echo '{ "data":' . e4s_encode($data) . '}';
function e4s_getEntriesData(e4sCompetition $compObj)
{
    $compId = $compObj->getID();
	$clubCompId = $compObj->getClubId();
	$anonymizeAthletes = $compObj->anonymizeAthletes();
    $closedText = 'Entries Closed';
    $waitingClosedText = $closedText;
    $useWaitingList = $compObj->isWaitingListEnabled();

    if ($useWaitingList) {
        $waitingClosedText = 'Event Full - Waiting List Available';
    }
    $entriesClosed = $compObj->haveEntriesClosed();
    $config = e4s_getConfig();
    $cfgOptions = e4s_getOptionsAsObj($config['options']);
    $baseAGRows = e4s_getBaseAgeGroups();
    // Club Info
    $clubsArr = array();
    $clubsArrById = array();
    $clubsArr['0'] = '';
    $sql = 'select * from ' . E4S_TABLE_CLUBS;
    $result = e4s_queryNoLog($sql);
    while ($clubRow = $result->fetch_array()) {
        $clubsArr[$clubRow['Clubname']] = $clubRow;
        $clubsArrById[$clubRow['id']] = $clubRow;
    }

    // Area Info
    $sql = 'select * from ' . E4S_TABLE_AREA;
    $result = e4s_queryNoLog($sql);
    $areasFromDB = $result->fetch_all(MYSQLI_ASSOC);
    $areaArr = array();
    foreach ($areasFromDB as $areaRow) {
        $areaArr[$areaRow['id']] = $areaRow;
    }

    $compOptions = $compObj->getOptions();
    $nonGuests = $compOptions->nonGuests;

    $clubOrSchool = 'club';
    $hideSchools = TRUE;
    if ($compObj->isSchool()) {
        $hideSchools = FALSE;
        $clubOrSchool = 'school';
    }

// Get array of finals Ce.id
    $finals = array();
    $ceObjs = $compObj->getCeObjs();
    foreach($ceObjs as $ceId=>$ceObj){
        if ( $ceObj->maxAthletes === -1 ){
            $finals[$ceId] = TRUE;
        }
    }
    $notTeamLabel = 'Confirmed Entry';
    $teamLabel = 'Relay Team';
    $relaySql = "select  ce.compid compid
              , ce.id ceId
              , ce.isopen isOpen
              , e.gender gender
              , date_format(eg.startdate,'%Y-%m-%d') startdate
              , date_format(eg.startdate,'%H.%i') starttime
              , e.name as event
              , eg.name as event
              , ce.options as ceOptions
              , eg.options as egOptions
              , ce.split split
              , e.tf tf
              , ete.name
              , ete.created
              , concat(ete.entitylevel,':',ete.entityid) entity
              , ce.maxgroup egId
              , ag.name ageGroup
              , ete.id entryId
     		  , ete.entityLevel
     		  , ete.entityId
              , ete.options eteOptions
              , ete.paid
              , 1 isTeam
        from " . E4S_TABLE_EVENTTEAMENTRIES . ' ete,
             ' . E4S_TABLE_EVENTS . ' e,
             ' . E4S_TABLE_EVENTGROUPS . ' eg,
             ' . E4S_TABLE_AGEGROUPS . ' ag,
             ' . E4S_TABLE_COMPEVENTS . " ce
        where ce.compid = $compId 
        and   ce.id = ete.ceid
        and   ce.maxgroup = eg.id
        and   ce.eventid = e.id
        and   ce.agegroupid = ag.id
        and   ete.paid != " . E4S_ENTRY_NOT_PAID . "
        and   e.options not like '%showInEntries\":false%'
        order by startdate";
    $rows = array();
    $result = e4s_queryNoLog('TimetableRelay' . E4S_SQL_DELIM . $relaySql);
    $relayRows = $result->fetch_all(MYSQLI_ASSOC);

    $getTeamAthletes = $compObj->showTeamAthletes();

    foreach ($relayRows as $relayRow) {
        $relayRow['starttime'] = e4s_ensureString($relayRow['starttime']);
        $ceOptions = e4s_addDefaultCompEventOptions($relayRow['ceOptions']);
        $egOptions = e4s_addDefaultEventGroupOptions($relayRow['egOptions']);
        $schoolTeam = FALSE;
        $mixedGender = FALSE;
        if (isset($ceOptions->eventTeam->formType)) {
            if ($ceOptions->eventTeam->formType === E4S_TEAM_TYPE_SCHOOL) {
                $schoolTeam = TRUE;
            }
        }
        if (isset($egOptions->mixedGender)) {
            $mixedGender = TRUE;
        }
        if ($mixedGender) {
            $relayRow['gender'] = '';
        }

        $relayRow['split'] = '0';

        $getThisTeamAthletes = FALSE;
        if ($getTeamAthletes) {
            $teamAthleteSql = 'select a.classification, a.firstName, a.surName,a.gender, a.id athleteId, c.clubName ' . $clubOrSchool . 'Name
                          from ' . E4S_TABLE_EVENTTEAMATHLETE . ' eta,
                               ' . E4S_TABLE_ATHLETE . ' a left join ' . E4S_TABLE_CLUBS . ' c on a.' . $clubOrSchool . 'id = c.id
                          where eta.teamentryid = ' . $relayRow['entryId'] . '
                          and   a.id = eta.athleteid
                          and   eta.athleteid != 0';

            $teamResult = e4s_queryNoLog('GetTeamAthletes' . E4S_SQL_DELIM . $teamAthleteSql);
            if ($teamResult->num_rows !== 0) {
	            $getThisTeamAthletes = TRUE;
                $teamAthleteRows = $teamResult->fetch_all(MYSQLI_ASSOC);
                foreach ($teamAthleteRows as $teamAthleteRow) {
                    $athleteRow = $relayRow;
					$anonymize = $anonymizeAthletes;
					$rowEntity = $athleteRow['entity'];
					if ( strpos($rowEntity, ':') !== FALSE ){
						$entityArr = preg_split('~:~', $rowEntity);
						$rowEntity = $entityArr[1];
					}
					if ( $clubCompId === $rowEntity ){
						$anonymize = false;
					}
                    if( $anonymize ) {
                        $teamAthleteRow['athlete'] = 'Athlete ' . $teamAthleteRow['athleteId'];
                    }else{
                        $teamAthleteRow['athlete'] = formatAthlete($teamAthleteRow['firstName'], $teamAthleteRow['surName']);
                    }
                    $athleteRow[$clubOrSchool . 'Name'] = $teamAthleteRow[$clubOrSchool . 'Name'];
                    $athlete = strtolower($teamAthleteRow['athlete']);
                    $teamAthleteRow['athlete'] = ucwords($athlete);
                    $athleteRow['pb'] = 0;
                    $athleteRow['athleteId'] = $teamAthleteRow['athleteId'];
                    if ($mixedGender) {
                        $athleteRow['gender'] = '-';
                    } elseif ($schoolTeam) {
                        $athleteRow['gender'] = $relayRow['gender'];
                    } else {
                        $athleteRow['gender'] = $teamAthleteRow['gender'];
                    }
                    $athleteRow['athlete'] = $teamAthleteRow['athlete'];
                    $athleteRow['classification'] = $teamAthleteRow['classification'];
                    $rows[] = $athleteRow;
                }
            }
        }

        if (!$getThisTeamAthletes) {
            $relayRow['classification'] = null;
            $relayRow[$clubOrSchool . 'Name'] = null;
            $relayRow['pb'] = 0;
            $relayRow['athleteId'] = null;
            $relayRow['athlete'] = null;
            $rows[] = $relayRow;
        }
    }

    $sql = 'select  *, 0 isTeam
        from ' . E4S_TABLE_ENTRYINFO . "
        where compid = $compId 
        and   ceoptions not like '%parentCeid%'
        and   eoptions not like '%showInEntries\":false%'
        and paid != " . E4S_ENTRY_NOT_PAID . '
        order by startdate , egid, periodstart, gender, agegroup';

    $result = e4s_queryNoLog('Timetable' . E4S_SQL_DELIM . $sql);
    $lastEgId = 0;
    $waitingPos = 0;

    while($entryRow = $result->fetch_object(E4S_ENTRYINFO_OBJ)){
        $maxAthletes = eventGroup::getMaxAthletes($entryRow->egOptions);
        if ( $lastEgId !== $entryRow->egId ) {
            $waitingPos = 0;
            if ( $useWaitingList ){

                $waitingPos = 0;
                if ( $maxAthletes > 0 ) {
                    $waitingPos = -($maxAthletes - 1);
                }
            }
        }
        $lastEgId = $entryRow->egId;

        $entryRow = e4s_getDataAsType($entryRow, E4S_OPTIONS_ARRAY);
        // entryRow has the event name and the eg Name. which to use
        $entryRow['event'] = $entryRow['egName'];

        $entryRow['waitingPos'] = $waitingPos;
        if ($waitingPos > 0) {
            $entryRow['event'] .= ' Waiting Queue';
        } elseif ($entryRow['isOpen'] === E4S_EVENT_CLOSED ){
            if (!$entriesClosed and $maxAthletes > 0) {
                $entryRow['event'] .= ' (' . $waitingClosedText . ')';
            }else{
                $entryRow['event'] .= ' (' . $closedText . ')';
            }
        }
        if ( $useWaitingList and $maxAthletes > 0) {
            $waitingPos++;
        }

        $entryRow['starttime'] = e4s_ensureString($entryRow['starttime']);

        $tf = $entryRow['tf'];
	    $rowEntity = $entryRow['entity'];
	    if ( strpos($rowEntity, ':') !== FALSE ){
		    $entityArr = preg_split('~:~', $rowEntity);
		    $rowEntity = $entityArr[1];
	    }
	    $anonymize = $anonymizeAthletes;
	    if ( "" . $clubCompId === $rowEntity ){
		    $anonymize = false;
	    }
        if( $anonymize) {
            $entryRow['name'] = 'Athlete ' . $entryRow['athleteId'];
        }else{
            $entryRow['name'] = formatAthlete($entryRow['firstName'], $entryRow['surName']);
        }

        $usePb = $entryRow['entryPb'];
        $pb = $usePb;
        $outputPb = 0;

        if (!is_null($pb) and $pb > 0) {
            $arr = preg_split('~\.~', $usePb);
            if (sizeof($arr) > 1) {
                $dec = (int)$arr[1];
                if ($dec < 10) {
                    if ('' . $dec === $arr[1]) {
                        $dec = $dec . '0';
                    } else {
                        $dec = $arr[1];
                    }
                }
            } else {
                $dec = '00';
            }
            $int = (int)$arr[0];

            if ($tf === E4S_EVENT_TRACK) {

                $time = $int;
                if ($int >= 60) {
                    $time = $int / 60;
                    $timeArr = preg_split('~\.~', '' . $time);
                    $mins = (int)$timeArr[0];
                    $secs = $int - ($mins * 60);
                    if ($secs < 10) {
                        $secs = '0' . $secs;
                    }
                    $time = $mins . ':' . $secs;
                }

                $outputPb = $time . '.' . $dec;
            } else {

                $outputPb = $int . '.' . $dec;
            }
        }
        $entryRow['pb'] = e4s_ensureString($outputPb);

        $rows[] = $entryRow;
    }

    $data = array();
    $entriesArr = array();

    foreach ($rows as $row) {
        $showRow = TRUE;
        $showTime = E4S_NO_TIME;
        $isTeam = (int)$row['isTeam'] === 1;
        $ceOptions = e4s_getCompEventDefaultOptions($row['ceOptions']);
        if (isset($ceOptions->time)) {
            $showTime = $ceOptions->time;
        }
        if (isset($ceOptions->entries)) {
            $showRow = $ceOptions->entries;
        }

        if ($showRow) {
            if ($isTeam and $getTeamAthletes) {
				if ( array_key_exists("athlete", $row) and !is_null($row['athlete']) and $row['athlete'] !== "" ) {
					$useAthlete = $row['athlete'];
					if ( $compObj->anonymizeAthletes() ){
						$useAthlete = 'Athlete:' . $row['athleteId'];
					}
					$row['name'] .= ' (' . $useAthlete . ')';
				}else{
					$eteoptions = e4s_getOptionsAsObj( $row['eteOptions'] );
					$athleteCnt = 0;
					if ( isset( $eteoptions->athletecnt ) ) {
						$athleteCnt = $eteoptions->athletecnt;
					}
					if ( $athleteCnt === 0 ) {
						$teamAthleteSql = 'select count(id) athleteCnt
                      from ' . E4S_TABLE_EVENTTEAMATHLETE . ' 
                      where teamentryid = ' . $row['entryId'] . '
                      and   athleteid != 0';
						$teamResult     = e4s_queryNoLog( 'GetTeamAthleteCnt' . E4S_SQL_DELIM . $teamAthleteSql );
						if ( $teamResult->num_rows === 1 ) {
							$teamAthleteCntRow = $teamResult->fetch_assoc();
							$athleteCnt        = (int) $teamAthleteCntRow['athleteCnt'];
						}
					}

					if ( $athleteCnt > 0 ) {
						$plural = '';
						if ( $athleteCnt > 1 ) {
							$plural = 's';
						}
						$athleteCntStr = ' (' . $athleteCnt . ' athlete' . $plural . ')';
						$row['name']   = $row['name'] . $athleteCntStr;
					}
				}
            }

            if ($row['starttime'] === '00:00') {
                $row['starttime'] = $showTime;
            }

            $useField = 'clubName';
            if ($hideSchools) {
                $name = explode(',', $row['clubName']);
                if (count($name) > 2) {
                    $country = strtolower(trim($name[2]));
                } else {
                    $country = '';
                    if (!is_null($row['athleteId'])) {
                        $sql = 'select aocode 
                        from ' . E4S_TABLE_ATHLETE . '
                        where id = ' . $row['athleteId'];
                        $res = e4s_queryNoLog($sql);
                        $athrow = $res->fetch_assoc();
                        if ( !is_null($athrow)){
                            $country = $athrow['aocode'];
                        }
                    }
                    if ($country === '') {
                        $country = E4S_UNATTACHED;
                    }
                }
                if ($row['clubName'] !== null and strtolower($row['clubName']) !== E4S_UNATTACHED and $country !== 'overseas') {
                    $row['flag'] = "<img width='20px' height='20px' src='" . E4S_PATH . '/css/images/' . $country . ".jpg'>";
                    $row['clubName'] = $name[0];
                } else {
                    $row['flag'] = null;
                }
                $row['schoolName'] = '';
            } else {
                $useField = 'schoolName';
                $row['clubName'] = '';
            }

            $county = '';
            $region = '';
            $guest = TRUE;
            if (sizeof($nonGuests) > 0) {
                if (array_key_exists('aocode', $row)) {

                    foreach ($nonGuests as $nonGuestAoCode) {
                        if ($row['aocode'] === $nonGuestAoCode) {
                            $guest = FALSE;
                            break;
                        }
                    }
                    if ($guest) {
                        $county = 'Guest';
                    }
                }
            }
            if ($county === '') {
                if (isset($cfgOptions->showAreas) and $cfgOptions->showAreas === TRUE) {
                    if (array_key_exists($useField, $row)) {
                        $areaId = 0;
                        if ( is_null($row[$useField]) ) {
                            $entity = preg_split('~:~' ,$row['entity']);
                            if ( (int)$entity[0] > 1 ){
                                $areaId = (int)$entity[1];
                            }
                        }else {
                            if (array_key_exists($row[$useField], $clubsArr)) {
                                $clubRow = $clubsArr[$row[$useField]];
                                $areaId = (int)$clubRow['areaid'];
                            }
                        }
                        if ($areaId !== 0 and array_key_exists($areaId, $areaArr)) {
                            $areaRow = $areaArr[$areaId];
                            $county = $areaRow['name'];

                            if (array_key_exists($areaRow['parentid'], $areaArr)) {
                                $areaRow = $areaArr[$areaRow['parentid']];
                                $region = $areaRow['name'];
                            }
                        }
                    }
                }
            }
            $row['county'] = $county;
            $row['region'] = $region;

            if ( array_key_exists('uomOptions', $row) ) {
                $row['uomOptions'] = e4s_getOptionsAsObj($row['uomOptions']);
                if ($row['split'] !== 0.00) {
                    $seText = '';
                    $s = (float)$row['split'];
                    if (array_key_exists('seText', $row)) {
                        $seText = ' ' . $row['seText']; // not sure if this is used anymore ????
                    } else {
                        $uom = $row['uomOptions'][0];
                        $split = Abs($s);
                        if ($row['tf'] === E4S_EVENT_TRACK) {
                            $split = resultsClass::getResultFromSeconds($split);
                            if (strpos($uom->short, '.SS') === FALSE) {
                                $split = str_replace('.00', '', $split);
                            }
                        }
                        if ($s < 0) {
                            $seText = ' ( less than ' . $split . $uom->short . ' )';
                        } elseif ($s > 0) {
                            $seText = ' ( greater than ' . $split . $uom->short . ' )';
                        }
                    }

                    $row['event'] = $row['event'] . $seText;
                }
            }

            if ($row['classification'] === '') {
                $row['classification'] = '-';
            }
            $row['ageGroup'] = e4s_getVetDisplay($row['ageGroup'], null);
            $row['athleteageGroup'] = $row['ageGroup'];
            if (array_key_exists('dob', $row)) {
                $baseGroup = e4s_getBaseAGForDOBForComp($compId, $row['dob'], $baseAGRows);
                if (!is_null($baseGroup)) {
                    $row['athleteageGroup'] = e4s_getVetDisplay($baseGroup['Name'], null);
                }
            }

            $key = getKey($row);

            if ($row['paid'] === E4S_ENTRY_AWAITING_PAYMENT) {
                $row['name'] .= ' (Pending)';
            }
            if (is_null($row['clubName'])) {
                $row['clubName'] = '';
            }

            if (!is_null($row['entity'])) {
                $entity = preg_split('~:~', $row['entity']);

                if (sizeof($entity) === 2) {
                    $entityLevel = (int)$entity[0];
                    $entityId = (int)$entity[1];
                    if ($entityLevel === 1) {
//              get Club
                        if (array_key_exists($entityId, $clubsArrById)) {
                            $row['clubName'] = $clubsArrById[$entityId]['Clubname'];
                        }
                    } else {
//              get Area
                        if (array_key_exists($entityId, $areaArr)) {
                            $row['clubName'] = $areaArr[$entityId]['name'];
                        }
                    }
                }
            }

            if (is_null($row['schoolName'])) {
                $row['schoolName'] = '';
            }

            if (!$isTeam || (!is_null($row['name']) and $row['name'] !== $teamLabel and $row['name'] !== $notTeamLabel)) {
                $entriesArr[$key] = TRUE;
                $data[] = e4s_entriesOutputData($row);
            } else {
                if (!array_key_exists($key, $entriesArr)) {
                    $entriesArr[$key] = e4s_entriesOutputData($row);
                }
            }
        }
    }

    foreach ($entriesArr as $e=>$e_value) {
        if (gettype($e_value) === 'array') {
            $e_value['event'] = $e_value['event'] . '<noentrycnt>';
            // If you change the text for athlete, also change in js file in function correctEntryCounts
            if (array_key_exists($e_value['ceId'], $finals)) {
                //$e_value['athlete'] = "<noentrycnt>";
                if (array_key_exists('fText', $e_value)) {
                    $e_value['name'] .= $e_value['fText'];
                } else {
                    $e_value['name'] .= 'Event Qualifications';
                }
            } else {
                $e_value['name'] = 'No Entries';
            }

            $e_value['ageGroup'] = '';

            $last2 = substr($e_value['maxgroup'], -2);

            $genderCheck = '-' . $e_value['gender'];
            if ($last2 != $genderCheck) {
                // multiGroup not split by gender so blank it
                $e_value['gender'] = '';
            }
            $data[] = $e_value;
        }
    }
    return $data;
}

function e4s_entriesOutputData($row){
//    return $row;

    $outputRow = new stdClass();

    if (array_key_exists('clubId', $row) ){
        $outputRow->athleteId = $row['athleteId'];
        $outputRow->clubId = $row['clubId'];
    }else{
        $outputRow->athleteId = 0;
        $outputRow->clubId = 0;
    }
    if ( !array_key_exists('userId', $row) ){
        $outputRow->userId = 0;
    }else{
        $outputRow->userId = $row['userId'];
    }

    $outputRow->entryId = $row['entryId'];
    $outputRow->startdate = $row['startdate'];
    $outputRow->starttime = $row['starttime'];
    $outputRow->event = $row['event'];
    $outputRow->tf = $row['tf'];
    $outputRow->gender = $row['gender'];
    $outputRow->clubName = $row['clubName'];
    $outputRow->schoolname = $row['schoolName'];
    $outputRow->name = $row['name'];
    $outputRow->county = $row['county'];
    $outputRow->region = $row['region'];
    $outputRow->ageGroup = $row['ageGroup'];
    $outputRow->pb = $row['pb'];
    $outputRow->classification = $row['classification'];
    $outputRow->isTeam = $row['isTeam'];
    $outputRow->waitingPos = 0;
    if ( array_key_exists('waitingPos', $row)) {
        $outputRow->waitingPos = $row['waitingPos'];
    }

    return $outputRow;
}

function getKey($row) {
    return ($row['event'] . '-' . e4s_sql_to_iso($row['startdate'] . ' ' . $row['starttime']));
}