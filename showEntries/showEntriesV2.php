<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
if ( !isset($compId) ) {
    if (array_key_exists('compid', $_GET)) {
        $compId = (int)$_GET['compid'];
    }
}
if (is_null($compId) or $compId === '') {
    Entry4UIError(5056, 'Competition Identifier has not been passed', 200, '');
}
if (!is_numeric($compId) and e4s_getUserID() === E4S_USER_ANON_ID) {
    e4s_addToBanned();
    echo 'Go elsewhere';
    die;
}

$compObj = e4s_getCompObj($compId);
$compId = $compObj->getSourceId();
$compObj = e4s_getCompObj($compId);
$row = $compObj->getRow();
if (is_null($row)) {
    echo 'Sorry. No competition found';
    exit();
}

$canMoveOffWaitingList = isE4SUser() or $compObj->isOrganiser();
$config = e4s_getConfig();
$cfgoptions = e4s_getOptionsAsObj($config['options']);
$uiTheme = E4S_JQUERY_THEME;
if (isset($cfgoptions->uiTheme)) {
    $uiTheme = $cfgoptions->uiTheme;
}
$oddRowColour = '#dfeffc';
if (isset($cfgoptions->oddRowColour)) {
    $oddRowColour = $cfgoptions->oddRowColour;
}
$evenRowColour = '';
if (isset($cfgoptions->evenRowColour)) {
    $evenRowColour = $cfgoptions->evenRowColour;
}
$env = strtoupper($config['env']);
if ($env === 'PROD') {
    $env = '';
} else {
    $env .= ' ';
}
$options = e4s_getOptionsAsObj($row['options']);

if (isset($options->noEntryCount) and userHasPermission('CHECK', null, $compId) === FALSE) {
    echo '<b>Public entries are not available for this competition</b>';
} else {
    ?>
    <html lang="en">
    <head>
        <link rel="stylesheet"
              href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/<?php echo $uiTheme ?>/jquery-ui.css"/>
        <link rel="stylesheet" href="<?php echo E4S_PATH ?>/paramquery/pqgrid.min.css">
        <link rel="stylesheet" href="<?php echo E4S_PATH ?>/paramquery/pqgrid.ui.min.css">
        <link rel="stylesheet" id="mat_icons-css" href="https://fonts.googleapis.com/icon?family=Material+Icons&amp;ver=6.4.2" type="text/css" media="all">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo E4S_PATH ?>/js/jquery-dateformat.min.js"></script>
        <script type="text/javascript" src="<?php echo E4S_PATH ?>/paramquery/pqgrid.min.js"></script>
        <script type="text/javascript" src="<?php echo E4S_PATH ?>/paramquery/pqtouch.min.js"></script>
        <script type="text/javascript" src="<?php echo E4S_PATH ?>/paramquery/jszip.min.js"></script>
        <script type="text/javascript" src="<?php echo E4S_PATH ?>/paramquery/FileSaver.js"></script>
        <script type="text/javascript" src="<?php echo E4S_PATH ?>/showEntries/showEntries-20240919.js"></script>
        <script>
            let isOrganiser = <?php echo $compObj->isOrganiser() ? 'true' : 'false' ?>;
            let userId = <?php echo e4s_getUserID() ?>;
            function editSeedPerf(entryId){
                let divName = "e4sUpdatePerf";
                $("#" + divName).remove();

                let divHTML = "<div id='" + divName + "'>";
                divHTML += '<div class="e4sHeader">';
                divHTML += '<img src="/resources/e4s_logo.png" class="e4sLogo">';
                divHTML += '</div>';
                divHTML += '<iframe id="' + divName + '" src="https://<?php echo E4S_CURRENT_DOMAIN ?>/#/athlete-pb/' + entryId + '" style="background:white; width:95.6%; height:96%; border:1px solid; padding: 2%;"></iframe>';
                divHTML += '</div>';
                $("#e4sPromptHolder").html(divHTML);
                $("#e4sPromptHolder").addClass("PromptHolder");
            }
            function e4s_showLogin(obj){
                <?php
                    if (e4s_getUserID() <= E4S_USER_NOT_LOGGED_IN) {
                        ?>
                        let loginOption = {
                            type: 'button',
                            icon: 'ui-icon',
                            label: 'Logon',
                            listener: function () {
                                window.open('/#/contact-org/' + localCompId, 'e4sContact');
                            }
                        };
                        obj.toolbar.items.push(loginOption);
                        <?php
                    }
                ?>

                return obj;
            }
            function e4s_WaitingList(obj) {
                <?php
                if ( $compObj->isOrganiser() and $compObj->isWaitingListEnabled() and $compObj->waitingListEntriesExist()){
                ?>
                let wlElement = {
                    type: 'button',
                    icon: 'ui-icon-print',
                    label: 'Waiting List Report',
                    listener: function () {
                        window.open("/wp-json/e4s/v5/competition/entries/waiting/<?php echo $compId ?>", "e4sWaiting");
                    }
                };
                obj.toolbar.items.push(wlElement);
                <?php
                }
                ?>
            }

            function e4s_myEntries(obj){
                <?php
                if ( $compObj->isClubcomp() and e4s_getUserID() > E4S_USER_NOT_LOGGED_IN){
                ?>

                let wlElement = {
                    type: 'button',
                    icon: 'ui-icon-list',
                    label: 'My Entries',
                    listener: function () {
                        window.open("/wp-json/e4s/v5/entries/myentries/<?php echo $compId ?>", "e4sMyEntries");
                    }
                };
                obj.toolbar.items.push(wlElement);
                <?php
                }
                ?>
            }
            function toTitleCase(str) {
                return str.replace(
                    /\w\S*/g,
                    function (txt) {
                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    }
                );
            }

            function e4sCellDblClick(event, ui) {
                if (ui.rowData.waitingPos > 0) {
                    // if (ui.rowData.teamname.indexOf("Valid Entry") === 0) {
                    <?php
                    if ($canMoveOffWaitingList) {
                    //   double clicked on a waiting list row
                    ?>
                    $("#dialog-moveoffwaiting").dialog({
                        resizable: false,
                        height: "auto",
                        width: 400,
                        modal: true
                    });
                    let athleteInfo = "Move " + toTitleCase(ui.rowData.name) + " off " + ui.rowData.event.trim() + ". Are you sure ?";
                    $("#waitingListAthleteInfo").text(athleteInfo);
                    $("#dialog-moveoffwaiting").dialog("option", "buttons",
                        [{
                            text: "Yes",
                            click: function () {
                                e4s_MoveOffWaitingListCheck(ui);
                                $(this).dialog("close");
                            }
                        },
                            {
                                text: "Cancel",
                                click: function () {
                                    $(this).dialog("close");
                                }
                            }]
                    );
                    <?php
                    }
                    ?>
                }
            }
            <?php
            if ($canMoveOffWaitingList) {
            ?>
            function e4s_MoveOffWaitingListCheck(ui) {
                $.post("/wp-json/e4s/v5/waitinglist/move/" + ui.rowData.entryId).
                then(
                    function(){
                        location.reload();
                    }
                );
            }
            <?php
            }
            ?>
            window.addEventListener(
                "message",
                (event) => {
                    if (event.origin !== "https://<?php echo E4S_CURRENT_DOMAIN ?>") return;
                    $("#e4sPromptHolder").html("");
                    $("#e4sPromptHolder").removeClass("PromptHolder");
                    if (event.data.type === "pbSaved") {
                        let entryId = event.data.payload.entryId;
                        let perf = event.data.payload.perfInfo;
                        // updateSeedPerfDisplay(entryId, perf);
                        location.reload();
                    }
                },
                false,
            );
        </script>
        <title>
            <?php echo $env . $compObj->getID() . ' : Entries' ?>
        </title>
        <style>
            .waiting_header_help {
                color: red;
                display:<?php
                    if ( $compObj->isWaitingListEnabled()){
                        echo ';';
                    }else{
                        echo 'none;';
                    } ?>;
            }

            .waiting_prompt_help {
                margin-block-start: 2em;
                font-size: 14px;
            }
            .e4sHeader {
                height: 45px;
                background-color: #1e40af;
            }
            .e4sLogo {
                width: 85px;
            }
            .PromptHolder {
                position: absolute;
                top: 35%;
                left: 35%;
                margin-top: -50px;
                margin-left: -50px;
                width: 30%;
                height: 30%;
                z-index: 1;
            }
        </style>
    </head>
<body>
<div id="e4sPromptHolder"></div>
<div id="displayMsg">Please wait. Loading data .....</div>
<div id="dialog-moveoffwaiting" style="display:none;" title="Move Athlete into the event ?">
    <p>
        <span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>
        <span id="waitingListAthleteInfo"></span>
    </p>
    <p class="waiting_prompt_help">Clicking Yes to this will DISABLE the E4S automatic processing of waiting lists. When
        you have completed the moving of athletes, please return to the builder and enable this function.</p>
</div
<div id="entrySelection" class="wdform_section">
    <div id="entryCounters" style="display:none">
        <span id="athleteCountD" class="" style="padding-right: 2rem;">No of Athletes : <span id="athleteCount"></span></span>
        <span id="entryCountD" class="" style="padding-right: 2rem;">No of Entries : <span
                    id="entryCount"></span></span>
        <span id="entryWaitingCountD" class="" style="padding-right: 2rem;">No on Waiting List : <span
                    id="entryWaitingCount"></span></span>
        <span id="relayCountD" class="" style="padding-right: 2rem;">No of Teams : <span id="relayCount"></span></span>
        <span id="relayAthleteCountD" class="" style="padding-right: 2rem;">No of Team Athletes : <span
                    id="relayAthleteCount"></span></span>
    </div>
</div>
<div class="waiting_header_help">* Waiting lists are active for this competition. Some events have a maximum limit set by the organiser and when entries reach this limit,
    they are automatically added to a waiting list. When an athlete/entry in the event pulls out for any reason, the next athlete in the queue will replace them.
    You are NOT a confirmed entry if you on the waiting list.
    At a certain date after entries close ( configured by the organiser ), athletes still in on the waiting list will be credited minus the booking fee.
</div>
<div id="grid_array"></div>
<script>
    var options = JSON.parse('<?php echo e4s_getDataAsType($options, E4S_OPTIONS_STRING) ?>');
    showEntries(<?php echo $compId ?>, "<?php echo $oddRowColour ?>", "<?php echo $evenRowColour ?>", options);
</script>
<div id="multiDialog">
    <div id="multi_grid"></div>
</div>
    <?php
}