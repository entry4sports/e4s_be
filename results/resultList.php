<?php
function reportList($obj)
{
    $newline = '<br>';
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $fromEventNo = checkFieldForXSS($obj, 'fromeventno:From No' . E4S_CHECKTYPE_NUMERIC);
    $type = checkFieldForXSS($obj, 'type:Event Type');
    if (is_null($fromEventNo)) {
        $fromEventNo = 0;
    } else {
        $fromEventNo = (int)$fromEventNo;
    }
    if (!is_numeric($compId)) {
        Entry4UIError(9055, 'Competition not identified', 200, '');
    }
    $sql = 'select  rh.id,
                    eg.typeNo,
                    eg.eventNo,
                    eg.id egId,
                    eg.name,
                    eg.startdate,
                    rh.heatNo,
                    rh.options rhOptions,
                    r.athlete,
                    r.lane laneNo,
                    r.position,
                    r.score,
                    c.clubName
           from ' . E4S_TABLE_EVENTRESULTSHEADER . ' rh,
                ' . E4S_TABLE_EVENTRESULTS . ' r,
                ' . E4S_TABLE_EVENTGROUPS . ' eg,
                ' . E4S_TABLE_CLUBS . ' c,
                ' . E4S_TABLE_ATHLETE . ' a
           where eg.compid = ' . $compId . '
           and rh.id = r.resultheaderid
           and eg.id = rh.egid
           and r.athleteid = a.id
           and a.clubid = c.id
           order by eg.eventno desc, rh.heatNo desc, r.position';

    $result = e4s_queryNoLog($sql);
    if ($result->num_rows < 1) {
        exit ('No results found');
    }
    $compObj = e4s_getCompObj($compId);
    $maxHeats = array();
    $results = array();
    while ($obj = $result->fetch_object()) {
        $obj->egId = (int)$obj->egId;
        $obj->heatNo = (int)$obj->heatNo;
        if (array_key_exists($obj->egId, $maxHeats)) {
            $maxHeat = $maxHeats[$obj->egId];
        } else {
            $maxHeat = 1;
        }
        if ($obj->heatNo > $maxHeat) {
            $maxHeat = $obj->heatNo;
        }
        $maxHeats[$obj->egId] = $obj->heatNo;
        $results[] = $obj;
    }
    ?>
    <html>
    <head>
        <title>Results for <?php echo $compId ?>/</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <style>
            .resultPos {
                width: 100px;
                font-size: large;
            }

            .resultAthlete {
                font-size: large;
                width: 400px;
            }

            .resultAffiliation {
                font-size: large;
                width: 400px;
            }

            .resultScore {
                font-size: large;
            }

            h2 {
                background-color: var(--e4s-form-nav-link-indicator__background);
                font-size:3vw;
            }

            h3 {
                background-color: var(--e4s-card--primary__border-color);
                font-size:1.5vw;
            }

            .resultHeader {
                font-weight: 800;
            }

            .e4s_results_content {
                overflow: scroll;
                margin: 4px, 4px;
                padding: 4px;
                width: 100%;
                height: 85%;
                overflow-x: hidden;
                overflow-y: auto;
                text-align: justify;
            }

            .results_body {
                overflow: hidden;
            }
            .results_footer{
                height: 10px;
            }
            .event_selector {
                width: 30px;
            }
            .comp_name {
                float: right;
            }
            .comp_header {
                display: flex;
                padding: 0px 10px;
                align-content: center;
                justify-content: space-evenly;
                align-items: center;
            }
            .newResults{
                padding: 25px;
                font-size: large;
                color: var(--e4s-body--error);
                text-decoration: underline;
            }
            .compHeaderNewResults {
                visibility: hidden;
                padding: 25px;
                font-size: large;
                color: var(--e4s-body--error);
                text-decoration: underline;
            }
            .links {
                display: flex;
                flex-wrap: wrap;
                justify-content: space-around;
                border: 2px solid darkblue;
                padding: 0px 0px 0px 0px;
                font-size: xx-large;
                align-content: flex-end;
                align-items: center;
                padding: 10px;
            }
            .typeSelected {
                font-weight: 700;
                color: blue;
            }
            .e4sType {
                text-decoration: underline;
            }
        </style>
        <script>
            function getCompetition(){
                return {"id":<?php echo $compId ?>};
            }
            function getSelectedEventNo(){
                return 0;
            }
            function showPleaseWait(status, text, logoColour) {
                let obj = $(".loading-wrapper");
                if (status) {
                    if (!text) {
                        text = "Loading, please wait";
                    }
                    text += "...";
                    $("#e4sLoadingText").html(text);
                    if (logoColour) {
                        $("#E4S_Logo").removeClass("colour")
                            .removeClass("all-black")
                            .removeClass("all-white")
                            .addClass(logoColour)
                    }
                    obj.show();
                } else {
                    obj.hide();
                }
            }
            <?php socketClass::outputJavascript() ?>
            function processSocketEvent(data){
                switch (data.action){
                    case '<?php echo R4S_SOCKET_MANUAL_RESULTS ?>':
                    case '<?php echo R4S_SOCKET_FIELD_RESULT ?>':
                    case '<?php echo R4S_SOCKET_PF ?>':
                        location.reload();
                        break;
                }
            }
            function eventChange(event){
                let value = $('#event_selector').find(":selected").val();
                reloadUrl(value);
            }
            function reloadUrl(eventNo){

                if ( eventNo !== "" ){
                    if(eventNo.indexOf("#") > -1) {
                        let link = "0";
                        if ( location.href.indexOf("0#") > -1 ){
                            link = "1";
                        }
                        eventNo = link + eventNo;
                    }
                    location.href = location.protocol + "//" + location.hostname + "/wp-json/e4s/v5/results/report/<?php echo $compId ?>/" + eventNo;
                }
            }
            function setNewResultsAvailable(eventNo){
                if ( $("#newResults_" + eventNo).length > 0 ) {
                    $("#newResults_" + eventNo).text("New Results Available");
                }else {
                    $(".compHeaderNewResults").css("visibility","visible");
                }
            }
            function displayTab(tab){
                $("#tab-track").hide();
                $("#tab-field").hide();

                $("#" + tab).show();
                $(".e4sType").toggleClass("typeSelected");
            }
        </script>
    </head>
    <body class="results_body">
    <?php
    include_once get_template_directory() . '-child/header-e4s.php';
    ?>
    <div id="tabs" class="links">
        <a onclick="displayTab('tab-track')" class="e4sType typeSelected">Track</a>
        <a class="e4sType" onclick="displayTab('tab-field')">Field</a>
        <a class="" onclick="location.reload();">Refresh</a>
    </div>
    <div id="compHeader" class="comp_header">
        <span class="comp_name">
            <h1>
                Results for <?php echo $compObj->getFormattedName() ?>
                <a href="#"><span id="compHeaderNewResults" class="compHeaderNewResults" onclick="reloadUrl('#t1');">New Results Available</span></a>
            </h1>
        </span>

        <span class="event_selector" style="display:none;">
            <label for="event_selector">Select Event : </label>
            <select id="event_selector" onchange="eventChange(this);">
                <option value="">Select Event</option>
<?php
                    $events = array();
                    foreach ($results as $obj) {
                        if ( !array_key_exists($obj->eventNo,$events) ){
?>
                            <option value="<?php echo $obj->eventNo ?>"><?php echo $obj->typeNo . ' - ' . $obj->name ?></option>
<?php
                            $events[$obj->eventNo] = true;
                        }
                    }
?>
            </select>
        </span>
    </div>
    <div class="e4s_results_content">
        <div style='font-size: xx-large'>
            <?php
            $lastEvent = '';
            $lastHeatId = '';
            $noScore = '';
            $fieldHTML = '';
            $trackHTML = '';
            $fieldNoScoreHTML = '';
            $trackNoScoreHTML = '';

            foreach ($results as $obj) {
                $isField = false;
                $isTrack = false;
                $isMulti = false;
                if ( $obj->typeNo[0] === E4S_EVENT_FIELD ){
                    $isField = true;
                }
                if ( $obj->typeNo[0] === E4S_EVENT_TRACK ){
	                $isTrack = true;
                }
                if ( $obj->typeNo[0] === E4S_EVENT_MULTI ){
	                $isMulti = true;
                }
                if ( (int)$obj->eventNo >= $fromEventNo ) {
                    $html = '';
                    if ($lastEvent !== $obj->egId) {
                        $fieldHTML .= $fieldNoScoreHTML;
                        $trackHTML .= $trackNoScoreHTML;
                        $html = '<h2><a name="' . $obj->typeNo . '"></a>Event: ' . $obj->typeNo . ' - ' . $obj->name ;
                        $html .= '<a href="#" onclick="reloadUrl(\'#t' . $obj->eventNo . '\');" id="newResultsLink_' . $obj->eventNo . '"><span class="newResults" id="newResults_' . $obj->eventNo . '"></span></a>';
                        $html .= '</h2>';
                        if ($isField ){
                            $fieldHTML .= $html;
                        }else{
                            $trackHTML .= $html;
                        }
                        $fieldNoScoreHTML = '';
                        $trackNoScoreHTML = '';
                        $lastHeatId = '';
                        $headerHTML = '';
                    }
                    $lastEvent = $obj->egId;
                    $rhOptions = e4s_getOptionsAsObj($obj->rhOptions);
                    $useHeatId = 'Race : ' . $obj->heatNo;
                    if (isset($rhOptions->description) and $rhOptions->description !== '') {
                        $useHeatId = 'Category : ' . $rhOptions->description;
                    }
                    if ($lastHeatId !== $useHeatId ) {
                        $fieldHTML .= $fieldNoScoreHTML;
                        $trackHTML .= $trackNoScoreHTML;

                        if ($isField ){
                            $html = '';
                            $fieldHTML .= $html;
                        }else{
                            $html = '<h3>' . $useHeatId . '</h3>';
                            $trackHTML .= $html;
                        }
                        $fieldNoScoreHTML = '';
                        $trackNoScoreHTML = '';
                        $headerHTML = '';
                    }
                    $lastHeatId = $useHeatId;
                    $score = $obj->score;

                    if ($score !== '0.00') {
                        $html = '';
                        if ( $isTrack) {
	                        $score = resultsClass::getResultFromSeconds( (float) $score );
                            if ( strpos($score,':') === false ){
                                $score .= ' s';
                            }else{
                                $score .= ' mins';
                            }
                        }
	                    if ( $isMulti) {
		                    $score .= ' pts';
	                    }
                        if ( $isField ){
		                    $score .= ' m';
	                    }
                        if ($headerHTML === '') {
                            $html .= "<table><tr class='resultHeader'><td class='resultPos'>Position</td><td class='resultAthlete'>Athlete</td><td class='resultAffiliation'>Organisation</td><td class='resultScore'>Result</td></tr></table>";
                            $headerHTML = 'displayed';
                        }
                        $html .= "<table><tr><td class='resultPos'>" . $obj->position . "</td><td class='resultAthlete'>" . $obj->athlete . "</td><td class='resultAffiliation'>" . $obj->clubName . "</td><td class='resultScore'>" . $score . '</td></tr></table>';
                        if ($isField ){
                            $fieldHTML .= $html;
                        }else{
                            $trackHTML .= $html;
                        }
                    } else {
                        $noScore = "<table><tr><td class='resultPos'>DNS</td><td class='resultAthlete'>" . $obj->athlete . "</td><td class='resultAffiliation'>" . $obj->clubName . "</td><td class='resultScore'>DNS</td></tr></table>";
                        if ($isField ){
                            $fieldNoScoreHTML .= $noScore;
                        }else{
                            $trackNoScoreHTML .= $noScore;
                        }
                    }
                }
            }
            if ($isField ){
                $fieldNoScoreHTML .= $noScore;
            }else{
                $trackNoScoreHTML .= $noScore;
            }
            ?>
            <div id="tab-track" style="display: ;">
                <?php echo $trackHTML ?>
            </div>
            <div id="tab-field" style="display: none;">
                <?php echo $fieldHTML ?>
            </div>
        </div>
    </div>
    <div class="results_footer">

    </div>
    </body>
    </html>

    <?php
    exit();
}