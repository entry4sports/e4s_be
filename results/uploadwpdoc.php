<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
$GLOBALS['E4S_Store'] = '';

$filename = basename($_FILES['fileToUpload']['name']);
if ($filename !== 'WPdoc.txt') {
    echo 'Error. This process is for the Timetronics WPDoc file.';
    exit();
}
$target_dir = 'uploads/';
$target_file = $target_dir . $filename;
if (!move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $target_file)) {
    echo 'Sorry, there was an error uploading your file.';
    exit();
}
$handle = fopen($target_file, 'r');
$compid = 0;
if ($handle) {
    // TT header line
    $line = fgets($handle);
    if ($line === FALSE) {
        echo 'Failed to read file';
        exit();
    }
    storeElement($line);
    // Competition
    $meet = getMeet(fgets($handle));
    if ($meet !== '') {
        $meet = addslashes($meet);
    }
    storeElement(fgets($handle));
    // Meet Date
    $meetdate = getCompDate(fgets($handle));

    if ($meet !== '' and $meetdate !== '') {
        $sql = 'select id id from ' . E4S_TABLE_COMPETITON . "
                    where name = '{$meet}'
                    and date = '{$meetdate}'";
        $compResult = e4s_queryNoLog($sql);
        if ($compResult->num_rows === 1) {
            $compRow = $compResult->fetch_assoc();
            $compid = (int)$compRow['id'];
        }
    }
}

if (!array_key_exists('compid', $_POST) and $compid === 0) {
    echo 'You must enter a competition number';
    exit();
}


if (array_key_exists('compid', $_POST)) {
    $compidPassed = (int)$_POST['compid'];
    if ($compid !== $compidPassed and $compidPassed !== 0) {
        echo 'Possible miss match of file for this competition!. (' . $compid . ' - ' . $compidPassed . ')';
        $compid = $compidPassed;
    }
}
$compObj = e4s_GetCompObj($compid, FALSE);
if (is_null($compObj->getRow())) {
    echo 'You must enter a valid competition number';
    exit();
}
getAllAthletes($compid);
$GLOBALS['E4S_CompID'] = $compid;
$GLOBALS['E4S_CompObj'] = $compObj;
storeElement('Processed by Entry4Sports for Competition Number : ' . $compid);
?>
    <!DOCTYPE html>
    <html>
    <head>
        <script>
            function exportF(elem, tableName) {
                var table = document.getElementById(tableName);
                var html = table.outerHTML;
                var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url
                window.open(url);
                alert("When the excel is opened, click yes to format warning.");
                // elem.setAttribute("href", url);
                // elem.setAttribute("download", "export.xls"); // Choose the file name
                return false;
            }
        </script>
    </head>
    <body>
    <button type="button" id="downloadLink" onclick="exportF(this, 'WPdoc.xls');">Export to Excel</button>
    <table id="WPdoc.xls">
        <?php
        echo $GLOBALS['E4S_Store'];
        if ($handle) {
            while (($line = fgets($handle)) !== FALSE) {
                $elements = sizeof(explode("\t", $line));
                if ($elements < 10) {
                    // Not a results line, so just print it out
                    echo '<tr>';
                    echo "<td colspan='8'>";
                    echo $line;
                    echo '</td>';
                    echo '</tr>';
                } else {
                    processLine($line);
                }
            }

            fclose($handle);
        }
        ?>
        </tr>
    </table>
    </body>
    </html>

<?php
function processLine($line) {
    $elements = explode("\t", $line);
    echo '<tr>';
    // blank col
    outputElement($elements[0]);
    // position
    outputElement($elements[1]);
    // Bib
    $bibno = $elements[2];
    outputElement($bibno);
    getAthlete($bibno, $elements);

    $result = $elements[6];
    outputElement($result);
    echo '</tr>';
}

function storeElement($line) {
    $html = '<tr>';
    $html .= "<td colspan='8'>";
    $html .= $line;
    $html .= '</td>';
    $html .= '</tr>';
    $GLOBALS['E4S_Store'] .= $html;
}

function outputElement($element, $newline = FALSE) {
    if (!$newline) {
        echo '<td>';
    }
    echo $element;
    if (!$newline) {
        echo '</td>';
    } else {
        echo '<br>';
    }
}

function getAthlete($bibNo, $elements) {
    $athletes = $GLOBALS['E4S_Athletes'];
    $athlete = $elements[3];
    $club = '';
    $age = '-';
    $gender = '';
    if (strpos($athlete, '(')) {
        $arr = explode('(', $athlete);
        $athlete = trim($arr[0]);
        $club = trim($arr[1]);
        $club = str_replace(')', '', $club);
    }

    if (array_key_exists($bibNo, $athletes)) {
        $athleteRow = $athletes[$bibNo];
        $athlete = $athleteRow['firstName'] . ' ' . $athleteRow['surName'];
        $club = $athleteRow['clubname'];
        $ageObj = e4s_getBaseAGForDOBForComp($GLOBALS['E4S_CompID'], $athleteRow['dob'], $GLOBALS['baseAGRows']);
        $age = $ageObj['Name'];
        $gender = $athleteRow['gender'];
    }
    outputElement($athlete);
    if ($club === 'Unat') {
        $club = 'Unattached';
    }
    outputElement($club);
    outputElement($age);
    outputElement($gender);
}

function getAllAthletes($compid) {
    $sql = 'select b.bibno, c.clubname clubname, a.*
     from ' . E4S_TABLE_ATHLETE . ' a,
          ' . E4S_TABLE_BIBNO . ' b,
          ' . E4S_TABLE_CLUBS . ' c
     where b.athleteid = a.id
     and   a.clubid = c.id
     and b.compid = ' . $compid;

    $athleteResult = e4s_queryNoLog($sql);
    if ($athleteResult->num_rows === 0) {
        Entry4UIError(5000, 'No athletes found ???', 200, '');
    }
    $rows = $athleteResult->fetch_all(MYSQLI_ASSOC);
    $arr = array();
    foreach ($rows as $row) {
        $arr[(int)$row['bibno']] = $row;
    }
    $GLOBALS['E4S_Athletes'] = $arr;

    $GLOBALS['baseAGRows'] = e4s_getBaseAgeGroups();
}

function getMeet($line) {
    $meet = '';
    if (strpos($line, '=') !== FALSE) {
        $arr = explode('=', $line);
        $meet = trim($arr[1]);
    }
    storeElement($line);
    return $meet;
}

function getCompDate($line) {
    $date = '';
    if (strpos($line, '=') !== FALSE) {
        $arr = explode('=', $line);
        $date = $arr[1];
        $date = explode('Time', $date);
        $arr = explode('/', trim($date[0]));
        $date = $arr[2] . '-' . $arr[1] . '-' . $arr[0];
    }
    storeElement($line);
    return $date;
}

?>