<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'classes/uploadTrackInfo.php';
include_once E4S_FULL_PATH . 'classes/eaAwards.php';
include_once E4S_FULL_PATH . 'classes/pof10ExportClass.php';

function e4s_writeHeightPositionsToDB($obj){
    $params = $obj->get_params('POST');
    $egId = (int)$params['egId'];
    $positions = $params['positions'];
    $sql = "
        select id
        from " . E4S_TABLE_EVENTRESULTSHEADER ."
        where egId = $egId
    ";
    $result = e4s_queryNoLog($sql);
    if ( $result->num_rows === 0){
        Entry4UIError(9055, 'No results header found for event group [' . $egId . ']', 200, '');
    }
    $headerRow = $result->fetch_object();
    $headerId = $headerRow->id;
    foreach($positions as $position) {
        $sql = "
            update " . E4S_TABLE_EVENTRESULTS . "
            set position = " . $position['position'] . "
            where resultheaderid = $headerId
            and athleteId = " . $position['athleteId'] . "
        ";
        e4s_queryNoLog($sql);
    };
}
function e4s_sendPhotofinishResponse($obj){
    $compId = checkFieldForXSS($obj, 'compId:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $fileName = checkFieldForXSS($obj, 'fileName:File Name');
    $pgObj = new photofinish($compId);
    $pgObj->response($fileName);
    Entry4UISuccess();
}
function e4s_createPhotofinish($obj){
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $fileName = checkFieldForXSS($obj, 'filename:File Name');
    $system = checkFieldForXSS($obj, 'system:PF System');
    $body = checkFieldForXSS($obj, 'body:PF Body');
    $pgObj = new photofinish($compId);
    $pgObj->create($fileName,$system, $body);
    Entry4UISuccess();
}
function e4s_getPhotofinishFile($obj){
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $fileName = checkFieldForXSS($obj, 'filename:File Name');
    $pgObj = new photofinish($compId);
    $pgObj->readFile($fileName);
}
function e4s_publishEventResults($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $egId = checkFieldForXSS($obj, 'egId' . E4S_CHECKTYPE_NUMERIC);
    $heatNo = checkFieldForXSS($obj, 'heatno:Event Race Number' . E4S_CHECKTYPE_NUMERIC);
    $obj = new uploadTrackInfo($compId);
    $obj->getEventInfoAndSend((int)$egId, (int)$heatNo);
}
function e4s_getEventQualifyInfo($sourceEgId, $targetEgId){
	$compResultObj = compResults::withEgId($sourceEgId);
	$compResultObj->sendQualifyInformation([$targetEgId]);
}
function e4s_getEventResults($egId, $socket = false){
	$compResultObj = compResults::withEgId($egId);
	$results = $compResultObj->readEventResults();
    if ( $socket ) {
        $compId = $compResultObj->egObj->compId;
	    e4s_sendSocketInfo($compId, $results, R4S_SOCKET_MANUAL_RESULTS);
    }
    return $results;
}
function e4s_readCompResults($obj) {
    $egId = checkFieldForXSS($obj, 'egid:Event Group Id' . E4S_CHECKTYPE_NUMERIC);
//    $compResultObj = compResults::withEgId($egId);
//    $results = $compResultObj->readEventResults();
    $results = e4s_getEventResults($egId);
    Entry4UISuccess($results);
}

function e4s_writeCompResults($obj) {
    $params = $obj->get_params('JSON');
    if (!array_key_exists('heats', $params)) {
        Entry4UIError(9054, 'Results not passed', 200, '');
    }

    $compResultObj = compResults::withData($params['comp'], $params['eventGroup'], $params['heats']);
    $compResultObj->writeData();
    $results = $compResultObj->readEventResults();

    e4s_sendSocketInfo($results->comp->id, $results, R4S_SOCKET_MANUAL_RESULTS);
    Entry4UISuccess($results);
}

function e4s_deleteHeat($obj) {
    $egId = checkFieldForXSS($obj, 'egid:Event Group Id' . E4S_CHECKTYPE_NUMERIC);
    $heatNo = checkFieldForXSS($obj, 'heatno:Event Heat Number' . E4S_CHECKTYPE_NUMERIC);
    $compResultObj = compResults::withEgId($egId);
//    $retVal = $compResultObj->deleteHeat($heatNo);
    $compResultObj->removeResults($egId, $heatNo);
    Entry4UISuccess();
}

function e4s_writeAutoEntries($obj) {
    $isTeamEvent = checkFieldForXSS($obj, 'isTeamEvent:is this a team event');
    $autoEntries = checkJSONObjForXSS($obj, 'entries:Entries to be generated');
    if (is_null($autoEntries)) {
        Entry4UIError(9407, 'Invalid parameters passed to autoEntries');
    }
    $autoEntryObj = new autoEntriesClass($isTeamEvent);
    $retObj = $autoEntryObj->writeAutoEntries($autoEntries);
    if ($retObj->compId !== 0) {
        $compObj = e4s_GetCompObj($retObj->compId);
        $useObj = new stdClass();
        $useObj->id = $retObj->egId;
        $useObj->egOptions = new stdClass();
        $useObj->egOptions->isTeamEvent = $retObj->isTeamEvent;
        $compObj->ageGroups = getAllCompDOBs($retObj->compId);
        $retObj->entryObjs = $compObj->getEntries($useObj);
    }

    Entry4UISuccess($retObj);
}

function e4s_refreshWebResults($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    //Get or have passed the compid and date in case multi day comp
//    $compid = 166;
    $date = '';
    if (!is_numeric($compId)) {
        Entry4UIError(7023, 'Competition not identified', 200, '');
    }
    $obj = new webResults($compId, $date);
}

function e4s_uploadTrackImageToResults($obj) {
    $parms = new stdClass();
    $compResults = compResults::withEventNo($obj->compId, $obj->eventNo);
    $compResults->writeResultsHeaderInfo($obj, $parms);
}

function e4s_uploadTrackResultsToResults($obj) {
    $parms = new stdClass();
    $compResults = compResults::withEventNo($obj->compId, $obj->eventNo);
    $compResults->writeTrackResultsInfo($obj, $parms);
}

function e4s_exportResultsForComp($obj) {
    include_once E4S_FULL_PATH . 'results/resultList.php';
    reportList($obj);
}

function e4s_clearHeightResults($obj){
    $egId = checkFieldForXSS($obj, 'egid:Event Group Id' . E4S_CHECKTYPE_NUMERIC);
    $height = checkFieldForXSS($obj, 'fromheight:From Height');
    $compResultObj = compResults::withEgId($egId);
    $compResultObj->clearHeightResults($height);
    Entry4UISuccess();
}
function e4s_exportResultsForPOF10($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (!is_numeric($compId)) {
        Entry4UIError(9056, 'Competition not identified', 200, '');
    }
    $compObj = e4s_getCompObj($compId);
    header('Content-Type: text/html; charset=utf-8');
    ?>
    <style>
        body {
            margin: 0px;
        }

        /*.woocommerce-MyAccount-navigation ul li {*/
        /*    -webkit-transition: background-color .3s;*/
        /*    transition: background-color .3s;*/
        /*    padding: 0;*/
        /*}*/
        .woocommerce-MyAccount-content {
            padding-right: 25px;
        }

        .woocommerce-MyAccount-navigation ul a {
            color: #000 !important;
            text-decoration: underline !important;
            line-height: 25px;
        }

        nav ul a {
            -webkit-transition: background-color .3s;
            transition: background-color .3s;
            font-size: 1rem;
            color: #fff ! important;
            display: block;
            padding: 0 0 0 15px;
            cursor: pointer;
        }

        ul {
            display: block;
            list-style-type: disc;
            margin-block-start: 1em;
            margin-block-end: 1em;
            margin-inline-start: 0px;
            margin-inline-end: 0px;
            padding-inline-start: 40px;
        }

        li {
            display: list-item;
            text-align: -webkit-match-parent;
        }

        .nav {
            background-color: #1f417e !important;
            margin-bottom: 15px
        }

        nav ul li {
            -webkit-transition: background-color .3s;
            transition: background-color .3s;
            padding: 0;
        }

        nav .white-text ul li {
            float: left;
        }

        .dropdown-content {
            background-color: #fff;
            margin: 0;
            display: none;
            min-width: 100px;
            overflow-y: auto;
            opacity: 0;
            position: absolute;
            left: 0;
            top: 0;
            z-index: 9999;
            -webkit-transform-origin: 0 0;
            transform-origin: 0 0;
        }

        .nav-wrapper {
            height: 64px;
            position: relative;
        }

        .right {
            float: right;
            color: white;
        }

        ul:not(.browser-default) {
            padding-left: 0;
            list-style-type: none;
        }

        a {
            text-decoration: none;
        }

        form.woocommerce-cart-form {
            width: 60%;
            float: left;
            margin: 2%;
        }

        .cart-collaterals {
            width: 30% !important;
            float: right;
        }

        .cart_totals {
            width: 100% !important;
            margin-right: 2% !important;
        }

        .woocommerce a.button {
            background-color: #006940 !important;
            color: #fff !important;
            margin-left: 5px !important;
        }

        .woocommerce {
            /*width: 30%;*/
        }

        .e4sButton {
            border: grey solid;
            padding: 5;
            background-color: #1f417e;
            color: white;
        }
    </style>
    <div>
        <nav class="nav">
            <div class="nav-wrapper secondary-background white-text e4s-primary-color-bg-gbr">
                <a href="#/"><img class="e4s-aai-logo" style="height: 45px;" src="/e4s_logo.png"></a>

                <ul class="right">
                    <li><a href="/#/showentries"><span>Home</span></a></li>
                    <li><a href="/my-account">My Account</a></li>
                    <li><a><span>Log out: <span
                                        style="padding-right: 20px"><?php echo wp_get_current_user()->display_name; ?></span></span></a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <br>

    <div style="margin:20;">
        <a class="e4sButton"
           href='https://<?php echo E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH ?>/results/generate/pof10/<?php echo $compId ?>'>Click
            here to download</a> the results file for competition
        "<?php echo $compId . ':' . str_replace('&', ' and ', e4s_getCompObj($compId)->getName()); ?>" in MS-Excel
        format to send to Power of Ten.<br>
        <br>Once you have checked and confirmed everything is as expected, you can then email this file to: <a
                href="mailto:admin@thepowerof10.info?subject=Results for E4S Competition <?php echo $compId; ?>&body=PowerOf10, Please find attached the results for <?php echo $compId . ':' . str_replace('&', ' and ', e4s_getCompObj($compId)->getName()); ?>">admin@thepowerof10.info</a>
        <br></br><br></br><br></br>
        <a class="e4sButton" href='https://<?php echo E4S_CURRENT_DOMAIN ?>/<?php echo $compId ?>/results'>Return to the
            results</a>

    </div>
    <?php
    exit();
}

function e4s_deleteEgResults($obj){
    $egId = checkFieldForXSS($obj, 'egid:Event Group Id' . E4S_CHECKTYPE_NUMERIC);
    $heatNo = checkFieldForXSS($obj, 'heatno:Heat No' . E4S_CHECKTYPE_NUMERIC); // may be null if ALL results to be removed
    $compResultObj = compResults::withEgId($egId);
    if ( !$compResultObj->compObj->isOrganiser()){
        Entry4UIError(9470, 'You are NOT Authorised to perform this function');
    }
    $compResultObj->removeResults($egId, $heatNo);
    Entry4UISuccess();
}
function e4s_generatePof10File($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    include_once E4S_FULL_PATH . 'classes/excelClass.php';
    $pof10Obj = new pof10ExportClass($compId);
    $pof10Obj->generatePof10File();
}

function e4s_writeCardResults($compId, $curEntry, $positions) {
    $parms = new stdClass();
//    logTxt("e4s_writeCardResults curEntry");
//    logTxt(e4s_getDataAsType($curEntry,E4S_OPTIONS_STRING));
    $compResults = compResults::withEventNo($compId, $curEntry['eventGroup']['eventNo']);
    $compResults->writeFieldResultsInfo($positions, $parms);
    Entry4UISuccess();
}

define('E4S_SCHEDULE_FILE', 'schedule.html');
define('E4S_EVENT_FILE', 'event.html');
define('E4S_OPEN_TAG', '{{');
define('E4S_CLOSE_TAG', '}}');

class compResults {
    public e4sCompetition $compObj;
    public $egObj;
    public $results;
    public $eventGroup;
    public int $entriesFromEgId;
    public int $combinedId;
    public $points;
    public $calcPoints;

    public function __construct($compId) {
        $this->compObj = e4s_GetCompObj($compId, TRUE);
        $this->results = null;
        $this->egObj = null;
        $this->points = null;
        $this->calcPoints = null;
        $this->entriesFromEgId = 0;
    }

    public static function withEgId($egId) {
        $egObj = eventGroup::getEgObj($egId);
        if (!is_null($egObj)) {
            $instance = new self($egObj->compId);
            $instance->egObj = $egObj;
        }
        return $instance;
    }

    public static function withEventNo($compId, $eventNo) {
        $instance = new self($compId);
        $instance->egObj = $instance->compObj->getEventGroupByNo($eventNo);

        return $instance;
    }

    public static function withData($compData, $egData, $resultData) {
        $egObj = eventGroup::getEgObj($egData['id']);
        if (!is_null($egObj)) {
            if ($compData['id'] !== $egObj->compId) {
                Entry4UIError(9057, 'Competition Data not valid (' . $compData['id'] . '/' . $egObj->compId . ')');
            }
            if (!userHasPermission(PERM_SCOREBOARD, null, $egObj->compId)) {
                // User does not have permission to write results
                Entry4UIError(9058, 'You are not authorised to use this function');
            }
            $instance = new self($egObj->compId);
            $instance->egObj = $egObj;
            $instance->results = $resultData;
        }
        return $instance;
    }

    function clearHeightResults($height){
        $egId = $this->egObj->id;
        $eventNo = $this->egObj->eventNo;
        $compId = $this->compObj->getID();

        $sql = "
            select id
            from " . E4S_TABLE_EVENTRESULTSHEADER ."
            where egId = $egId
        ";
        $result = e4s_queryNoLog($sql);
        if ( $result->num_rows > 0){
            $headerRow = $result->fetch_object();
            $headerId = $headerRow->id;
            $sql = "
                delete from " . E4S_TABLE_EVENTRESULTS . "
                where resultheaderid = $headerId
                and score >= $height
            ";
            e4s_queryNoLog($sql);

            $sql = "
                delete from " . E4S_TABLE_EVENTRESULTSHEADER ."
                where egId = $egId
            ";
            e4s_queryNoLog($sql);
        }
        $sql = "
            delete from " . E4S_TABLE_CARDRESULTS . "
            where compid = " . $compId . "
            and eventno = $eventNo
            and resultkey > '"  . $height . "'";

        e4s_queryNoLog($sql);
        $data = new stdClass();
        $data->egId = $this->egObj->id;
        $data->height = $height;

	    e4s_sendSocketInfo($compId, $data, R4S_SOCKET_CLEAR_HEIGHT_RESULTS);
    }
    public function removeResults($egId, $heatNo) {
        $sql = 'select id
                from ' . E4S_TABLE_EVENTRESULTSHEADER . '
                where egid = ' . $egId;
        if (!is_null($heatNo)) {
            $sql .= ' and heatno = ' . $heatNo;
        }
        $result = e4s_queryNoLog($sql);
        while ($obj = $result->fetch_object()) {
            $this->_cleanOutResults((int)$obj->id);
            $sql = 'delete from ' . E4S_TABLE_EVENTRESULTSHEADER . '
                    where id = ' . $obj->id;
            e4s_queryNoLog($sql);
        }
        $eventGroup = $this->compObj->getEventGroupByEgId($egId);
        $sql = '';
        if ($eventGroup->type === E4S_EVENT_TRACK) {
            $sql = 'delete from ' . E4S_TABLE_TRACKRESULT . '
                where compid = ' . $eventGroup->compId . '
                and   eventno = ' . $eventGroup->eventNo;
            if (!is_null($heatNo)) {
                $sql .= ' and heatno = ' . $heatNo;
            }
        }
        if ($eventGroup->type === E4S_EVENT_FIELD) {
            $sql = 'delete from ' . E4S_TABLE_CARDRESULTS . '
                where compid = ' . $eventGroup->compId . '
                and   eventno = ' . $eventGroup->eventNo;
            if (!is_null($heatNo)) {
                $sql .= " and resultkey = 'h" . $heatNo . "'";
            }
        }
        if ($sql !== '') {
            e4s_queryNoLog($sql);
        }
    }

    public function writeData() {
        if (is_null($this->results)) {
            Entry4UIError(9059, 'Must be initialised with data');
        }
        $qualifyToEgIds = [];
        foreach ($this->results as $result) {
            if (!empty($result['results'])) {
                $egId = $this->_processHeatData($result);
                if ($egId > 0) {
                    $qualifyToEgIds[$egId] = $egId;
                }
            }
        }

        // get entries for egID for socket
        if (sizeof($qualifyToEgIds) > 0) {
	        $this->sendQualifyInformation($qualifyToEgIds);
        }
        $this->compObj->setResultsAvailable(true);
    }

    function sendQualifyInformation($egIds){
	    foreach ($egIds as $qualifyToEgId) {
		    $sql = 'select e.id entryId
                          ,ce.compId
                          ,e.compEventId
                          ,e.athleteId
                          ,e.pb
                          ,e.options
                    from ' . E4S_TABLE_ENTRIES . ' e,
                         ' . E4S_TABLE_COMPEVENTS . ' ce
                    where e.compeventid = ce.id
                    and ce.maxgroup = ' . $qualifyToEgId;
		    $result = e4s_queryNoLog($sql);
		    $entries = [];
		    $athleteIds = [];
		    $compId = 0;
		    while ($obj = $result->fetch_object()) {
			    $obj->options = e4s_getOptionsAsObj($obj->options);
			    $compId = (int)$obj->compId;
			    unset($obj->compId);
			    $entries[] = $obj;
			    $athleteIds[] = (int)$obj->athleteId;
		    }
		    if ($compId !== 0) {
			    $data = new stdClass();
			    $data->sourceEgId = $this->egObj->id;
			    $data->targetEgId = $qualifyToEgId;
			    $data->entries = $entries;
			    $seedObj = new seedingV2Class($compId, $qualifyToEgId);
			    $seedObj->clear();
			    $seedObj->insertNonSeededAthletes($athleteIds);
			    e4s_sendSocketInfo($compId, $data, R4S_SOCKET_EVENT_QUALIFICATIONS);
		    }
	    }
    }
    private function _processHeatData($data) {
        $this->_cleanOutResultsAndHeader($data['heatNo']);
        $headerId = $this->_writeHeader($data);
        return $this->_writeResults($headerId, $data['results']);
    }

    private function _cleanOutResultsAndHeader($heatNo) {
        $header = $this->_readHeaderByHeatNo($heatNo);
        if (is_null($header)) {
            return;
        }
        $this->_cleanOutResults($header->id);
        $sql = 'delete from ' . E4S_TABLE_EVENTRESULTSHEADER . '
                where id = ' . $header->id;
        e4s_queryNoLog($sql);
    }

    private function _readHeaderByHeatNo($heatNo) {
        $sql = 'select rh.*
                from ' . E4S_TABLE_EVENTRESULTSHEADER . ' rh,
                     ' . E4S_TABLE_EVENTGROUPS . ' eg
                where eg.compid = ' . $this->compObj->getID() . '
                and   eg.eventno = ' . $this->egObj->eventNo . '
                and   heatno = ' . $heatNo . '
                and   eg.id = rh.egid';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            return null;
        }
        return $result->fetch_object();
    }

    private function _cleanOutResults($headerId) {
        $sql = 'delete from ' . E4S_TABLE_EVENTRESULTS . '
                where resultheaderid = ' . $headerId;
        e4s_queryNoLog($sql);
    }

    private function _writeHeader($data) {
        $options = new stdClass();
        if (array_key_exists('options', $data)) {
            if (gettype($data['options']) !== E4S_OPTIONS_STRING) {
                $options = e4s_getOptionsAsObj($data['options']);
            }
        }
        if (array_key_exists('description', $data)) {
            $options->description = $data['description'];
        }

        $actual = $data['actualTime'];
        if (is_null($actual)) {
            $actual = $data['scheduledTime'];
        }
        $sql = 'insert into ' . E4S_TABLE_EVENTRESULTSHEADER . ' (egid, heatno, scheduledtime, actualtime, image, options) values (
        ' . $this->egObj->id . ',
        ' . $data['heatNo'] . ",
        '" . $data['scheduledTime'] . "',
        '" . $actual . "',
        '" . $data['image'] . "',
        '" . e4s_getDataAsType($options, E4S_OPTIONS_STRING) . "'
        )";
        e4s_queryNoLog($sql);
        return e4s_getLastID();
    }

    private function _writeResults($headerId, $results) {
        $headerObj = null;
        $e4sQualifyObj = new e4sQualifyClass($this->egObj->options);
        $qualifyToEgId = $e4sQualifyObj->getId();
	    if ($qualifyToEgId > 0) {
            $headerObj = $this->_readHeaderById( $headerId );
	    }
        $qualifiedAthletes = array();
        $nonQualifiedAthletes = array();

        $resultSep = '';
        $athleteInfo = array();
        $newResults = array();
        $egObj = new eventGroup($this->compObj->getID());
        $this->entriesFromEgId = $egObj->getEntriesFromEG($this->egObj->id);

        foreach ($results as $result) {
            $athleteId = (int)$result['athleteId'];
            if ($athleteId === 0) {
                if ($result['bibNo'] !== '-' and $result['bibNo'] !== '0') {
                    $result['athleteId'] = $this->_createOTDAthlete($result);
                }
            } else {
                $athleteInfo[] = $athleteId;
            }
            $newResults[] = $result;
        }
        $results = $newResults;
// check if the results are coming in with Qualifying info or they need it
// if needed, will update $results with Q/q info
	    if ($qualifyToEgId > 0) {
		    $e4sQualifyObj->checkAutoQualifyResults($headerObj, $results);
	    }
        $sql = 'insert into ' . E4S_TABLE_EVENTRESULTS . '(resultheaderid, lane, agegroup, position, wind, athleteid, athlete, club, bibno, score, pointsid, qualify, scoreText, gender, eaAward) values ';
        foreach ($results as $result) {
            $athleteId = (int)$result['athleteId'];

            if (!is_numeric($result['position']) or $result['athlete'] === '') {
                continue;
            }

            $wind = '';
            if (array_key_exists('wind', $result)) {
                $wind = $result['wind'];
            }
            $laneNo = 0;
            if ($result['laneNo'] !== '') {
                $laneNo = (int)$result['laneNo'];
            }
            $scoreText = '';
            if (array_key_exists('scoreText', $result)) {
                $scoreText = $result['scoreText'];
            }

            $qualifyText = '';
            if (array_key_exists('qualify', $result)) {
                $qualifyText = $result['qualify'];
            }
            $score = '';
            if (array_key_exists('score', $result)) {
                if ((int)$result['score'] !== 0) {
                    $score = $result['score'];
                }
            }

	        if (array_key_exists('scoreValue', $result)) {
                $scoreValue = $result['scoreValue'];
                if ( resultsClass::isValidResult($scoreValue)){
                    $score = $scoreValue;
                }else{
	                $scoreText = strtoupper($scoreValue);
                    $score = '';
                }
	        }

            $gender = '';
            $ageGroup = '';
            if (array_key_exists('ageGroup', $result)) {
                $ageGroup = $result['ageGroup'];
            }
            $eaAward = 0;
            if (array_key_exists('eaAward', $result)) {
                $eaAward = $result['eaAward'];
            }

            if ($qualifyToEgId === 0 and strtolower($qualifyText) === 'q') {
                $qualifyText = '';
            }

            if (resultsClass::getNonNumeric($score) === '') {
                $score = resultsClass::getResultInSeconds($score);
            }

            $pointsId = 0;
            // if ( $this->>egObj->options->combinedId > 0){
            if ($this->entriesFromEgId > 0) {
                $pointsObj = $this->_getPointsForScore((float)$score);

                $pointsId = $pointsObj->pointsId;
                $scoreText = '(' . $pointsObj->points . ' pts)';
            }
            $club = 'Unattached';
            if ($result['clubName'] !== '') {
                $club = $result['clubName'];
            }

            $useBibNo = $result['bibNo'];
            if (array_key_exists('teambibno', $result) and !is_null($result['teambibno']) and $result['teambibno'] !== '') {
                $useBibNo = $result['teambibno'];
            } else {
                $result['teambibno'] = '0';
            }

            $sql .= $resultSep . '(
                        ' . $headerId . ',
                        ' . $laneNo . ",
                        '" . $ageGroup . "',
                        '" . $result['position'] . "',
                        '" . $wind . "',
                        " . $result['athleteId'] . ",
                        '" . addslashes($result['athlete']) . "',
                        '" . addslashes($club) . "',
                        '" . $useBibNo . "',
                        '" . $score . "',
                        " . $pointsId . ",
                        '" . $qualifyText . "',
                        '" . addslashes($scoreText) . "',
                        '" . $gender . "',
                        " . $eaAward . '
                     )';
            $resultSep = ',';
            if ($qualifyToEgId > 0) {
                if (is_null($headerObj)) {
                    $headerObj = $this->_readHeaderById($headerId);
                }
                $qualifyObj = new stdClass();
                $qualifyObj->athleteId = $athleteId;
                $qualifyObj->athlete = addslashes($result['athlete']);
                $qualifyObj->club = $club;
                $qualifyObj->clubId = $this->_getClubId($club);
                $qualifyObj->qualifyType = $qualifyText;
                $qualifyObj->qualifyPosition = $result['position'];
                $qualifyObj->score = $score;
                $qualifyObj->scoreText = $scoreText;
                $qualifyObj->eventAgeGroup = $ageGroup;
                $qualifyObj->bibNo = $result['bibNo'];
//                $qualifyObj->teamBibNo = $result['teambibno'];
                $qualifyObj->heatNo = $headerObj->heatNo;
                $qualifyObj->resultHeaderId = $headerId;
                if (strtolower($qualifyText) === 'q') {
                    $qualifiedAthletes[] = $qualifyObj;
                } else {
                    $nonQualifiedAthletes[] = $qualifyObj;
                }
            }
        }

        e4s_queryNoLog($sql);

//        update Ea Awards
        if (!e4s_isAAIDomain()) {
            $pbAwards = new eaAwards($this->compObj->getID());
            $pbAwards->processResultsForEg($this->egObj->id);
        }
        if ($this->entriesFromEgId > 0) {
            // build combined parent points
            $this->_calculateCombinedPoints();
        }
        if ($qualifyToEgId > 0) {
            $e4sQualifyObj->writeQualifiedEntries($headerObj, $qualifyToEgId, $qualifiedAthletes, $nonQualifiedAthletes);
        }
        $this->compObj->setResultsAvailable(true);
        return $qualifyToEgId;
    }

    private function _calculateCombinedPoints(): void {

        $combinedEgs = array();
        $egObj = null;
        $parentEgId = $this->entriesFromEgId;
        $egObjs = $this->compObj->getEGObjs();
        foreach ($egObjs as $eg) {
            if (is_null($egObj)) {
                $egObj = new eventGroup($eg->compId);
            }
            if ($egObj->getEntriesFromEGObj($eg) === $parentEgId) {
                $combinedEgs[] = $eg->id;
            }
        }
        if (sizeof($combinedEgs) === 0) {
            return;
        }
        // get all results
        $sql = 'select er.*
                from ' . E4S_TABLE_EVENTRESULTS . ' er,
                     ' . E4S_TABLE_EVENTRESULTSHEADER . ' erh
                where erh.id = er.resultheaderid
                 and   erh.egid in (' . implode(',', $combinedEgs) . ')
                order by erh.id';
        $result = e4s_queryNoLog($sql);
        $combinedResults = array();
        while ($obj = $result->fetch_object(E4S_EVENTRESULTS_OBJ)) {
	        $obj->points = 0;
            if ( strpos($obj->scoreText, 'pts') !== FALSE){
                $score = str_replace('(','',$obj->scoreText);
                $score = str_replace(' pts)','',$score);
	            $obj->points = (int)$score;
            }

            if (!array_key_exists($obj->athleteId, $combinedResults)) {
                $combinedResults[$obj->athleteId] = $obj;
            } else {
                $combinedResults[$obj->athleteId]->points += $obj->points;
            }
        }

        usort($combinedResults, function ($a, $b) {
	        if ($a->points === $b->points) {
		        return 0;
	        };
            if ($a->points < $b->points) {
                return 1;
            };
            return -1;
        });
        // get Header for parentEg
        $parentRHId = $this->_getResultsHeaderForEgId($parentEgId);
        $this->_cleanOutResults($parentRHId);
        $position = 1;
        $counter = 1;
        $lastPoints = 0;
        $insertSql = '';
        $sub = '';
        foreach ($combinedResults as $combinedResult) {
            if ($insertSql === '') {
                $insertSql = '
                    insert into ' . E4S_TABLE_EVENTRESULTS . ' (resultHeaderId,lane, position,athleteId, athlete, club, bibno, score, scoreText, ageGroup, gender, eaAward) values 
                ';
                $lastPoints = $combinedResult->points;
            }
            if ($lastPoints !== $combinedResult->points) {
                $position = $counter;
                $lastPoints = $combinedResult->points;
            }
            $insertSql .= $sub . '(';
            $insertSql .= $parentRHId . ',';
            $insertSql .= '0,';
            $insertSql .= $position . ',';
            $insertSql .= $combinedResult->athleteId . ',';
            $insertSql .= "'" . addslashes($combinedResult->athlete) . "',";
            $insertSql .= "'" . addslashes($combinedResult->club) . "',";
            $insertSql .= "'" .$combinedResult->bibNo . "',";
            $insertSql .= $combinedResult->points . ',';
            $insertSql .= "'pts',";
            $insertSql .= "'" . $combinedResult->ageGroup . "',";
            $insertSql .= "'" . $combinedResult->gender . "',";
            $insertSql .= '0';
            $insertSql .= ')';
            $sub = ',';
            $counter++;
        }
        e4s_queryNoLog($insertSql);
    }

    private function _getResultsHeaderForEgId($egId) {
        $eg = $this->compObj->getEventGroupByEgId($egId);
        $sql = 'select *
                from ' . E4S_TABLE_EVENTRESULTSHEADER . '
                where egid = ' . $egId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            $insertSql = 'insert into ' . E4S_TABLE_EVENTRESULTSHEADER . ' (egid, heatno, scheduledtime, actualtime, image, options) values (
            ' . $egId . ",
            1,
            '" . $eg->startDate . "',
            '" . $eg->startDate . "',
            '',
            ''
            )";
            e4s_queryNoLog($insertSql);
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows === 0) {
                Entry4UIError(6300, 'Unable to get Results header for ' . $egId);
            }
        }
        $rhObj = $result->fetch_object(E4S_EVENTRESULTSHEADER_OBJ);
        return $rhObj->id;
    }

    private function _getEventId() {
        $egId = $this->egObj->id;
        if (isset($this->egObj->eventId)) {
            return $this->egObj->eventId;
        }
        $eventId = 0;
        $ceObjs = $this->compObj->getCeObjs();
        foreach ($ceObjs as $ceObj) {
            if ($ceObj->egId === $egId) {
                $eventId = $ceObj->eventId;
                break;
            }
        }
        if ($eventId === 0) {
            Entry4UIError(6980, 'Event for ' . $this->egObj->id . ' can not be found');
        }
        $this->egObj->eventId = $eventId;
        return $eventId;
    }

    private function _getPointsForScore($score) {

        $returnObj = new stdClass();
        $returnObj->pointsId = 0;
        $returnObj->points = 0;
        if ($score < 0.01) {
            return $returnObj;
        }
	    $useCombinedId = 0;
        $checkPointsTable = false;
	    $eventId = $this->_getEventId();
        if (is_null($this->points) or empty($this->points)) {
            $useCombinedId = $this->_getPointsData();
	        if ($useCombinedId === 1) {
                $checkPointsTable = true;
            }
        }else{
            $checkPointsTable = true;
        }

        if ($checkPointsTable) {
            if (array_key_exists($eventId, $this->points)) {
                foreach ($this->points[$eventId] as $pointObj) {
                    if ($this->egObj->type === E4S_EVENT_FIELD) {
                        if ($score >= $pointObj->result) {
                            $returnObj->pointsId = $pointObj->id;
                            $returnObj->points = $pointObj->points;
                        } else {
                            break;
                        }
                    }
                    if ($this->egObj->type === E4S_EVENT_TRACK) {
                        if ($score <= $pointObj->result) {
                            $returnObj->pointsId = $pointObj->id;
                            $returnObj->points = $pointObj->points;
                            break;
                        }
                    }
                }
            }
        } else {
            // calculate points
            if (!array_key_exists($eventId, $this->calcPoints)) {
                Entry4UIError(6982, 'Calculation points for ' . $eventId . ' can not be found');
            }
            $calcData = $this->calcPoints[$eventId];
            if (( $score < $calcData->minVal and $calcData->minVal > 0) or ($score > $calcData->maxVal and $calcData->maxVal > 0)){
	            $returnObj->points = 0;
            }else {
                switch ($calcData->calcNo){
                    case 1:
                        // Math.round( A * Math.pow((Val*100)- B , C)-0.5)
	                    $returnObj->points = round($calcData->a * pow(($score * 100) - $calcData->b, $calcData->c));
                        break;
	                case 2:
                        // Math.floor( A / (Val*1+B) - C)
		                $returnObj->points = round($calcData->a / ($score + $calcData->b) - $calcData->c);
		                break;
	                case 3:
                        // Math.round( A *Math.pow(Val - B, C )-0.5)
                        $returnObj->points = round($calcData->a * pow($score - $calcData->b, $calcData->c) - 0.5);
		                break;
	                case 4:
                        // Math.round( A *Math.pow(( Val *100)- B, C)-0.5)
                        $returnObj->points = round($calcData->a * pow(($score * 100) - $calcData->b, $calcData->c) - 0.5);
		                break;
	                case 5:
                        // Math.round( A *Math.pow(B - ValInSecs), C )-0.5)
                        $returnObj->points = round($calcData->a * pow($calcData->b - $score, $calcData->c) - 0.5);
		                break;
                    default:
                        Entry4UIError(9200,"Combined points for event " . $eventId . " has not been setup");
                        break;
                }
            }
        }
        return $returnObj;
    }

    private function _getCalcPointsData() {
        $array = array();
        $sql = 'select *
                from ' . E4S_TABLE_COMBINEDCALC;
        $result = e4s_queryNoLog($sql);
        while ($obj = $result->fetch_object(E4S_COMBINEDCALC_OBJ)) {
            $array[$obj->eventId] = $obj;
        }
        return $array;
    }

    private function _getPointsData(): int {
        $points = array();
        $this->points = $points;
        $this->calcPoints = $this->_getCalcPointsData();
        $entriesFromEgId = $this->entriesFromEgId;

        if ($entriesFromEgId === 0) {
            Entry4UIError(6981, 'Entries from has not been setup for ' . $this->egObj->id);
        }
        $entriesFromEgObj = $this->compObj->getEventGroupByEgId($entriesFromEgId);
        $eventGroupObj = new eventGroup($this->compObj->getID());
        $this->combinedId = $eventGroupObj->combinedIdFromObj($entriesFromEgObj);

        if ($this->combinedId < 1) {
            // not a combined event or calculated
            return $this->combinedId;
        }

        $sql = 'select *
                from ' . E4S_TABLE_COMBINEDPOINTS . '
                where combinedid = ' . $this->combinedId . '
                order by eventid, result';

        $result = e4s_queryNoLog($sql);
        $points = array();
        while ($obj = $result->fetch_object(E4S_COMBINEDPOINTS_OBJ)) {
            if (!array_key_exists($obj->eventId, $points)) {
                $points[$obj->eventId] = array();
            }
            $points[$obj->eventId][] = $obj;
        }
        $this->points = $points;
        return $this->combinedId;
    }

    private function _createOTDAthlete($result) {
        if (!array_key_exists('gender', $result)) {
            $result['gender'] = '-';
        }
        $clubName = 'Unattached';

        if ($result['clubName'] !== '') {
            $clubName = $result['clubName'];
        }
        $clubId = $this->_getClubId($clubName);
        $athlete = preg_split('~ ~', $result['athlete'] . ' ');
        $sql = 'insert into ' . E4S_TABLE_ATHLETE . " (firstname, surname, aocode, urn, dob, gender, classification, clubid, type)
         values (
            '" . addslashes($athlete[0]) . "',
            '" . addslashes($athlete[1]) . "',
            '',
            null,
            '2000-01-01',
            '" . $result['gender'] . "',
            0,
            " . $clubId . ",
            'A'
         )";
        e4s_queryNoLog($sql);
        return e4s_getLastID();
    }

    // defunct ?

    private function _getClubId($clubName) {
        if (trim($clubName) === '') {
            $clubName = 'Unattached';
        }
        if ($clubName === 'Unattached') {
            return E4S_UNATTACHED_ID;
        }
        $sql = 'select id
                from ' . E4S_TABLE_CLUBS . "
                where clubname = '" . addslashes($clubName) . "'";
        $result = e4s_queryNoLog($sql);

        if ($result->num_rows > 0) {
            $clubRow = $result->fetch_object();
            $clubId = (int)$clubRow->id;
        } else {
            $sql = 'insert into ' . E4S_TABLE_CLUBS . " (clubname,areaid,clubtype,active)
                    values(
                    '" . addslashes($clubName) . "',
                    1,
                    'T',
                    false)";
            e4s_queryNoLog($sql);
            $clubId = e4s_getLastID();
        }
        return $clubId;
    }

    private function _readHeaderById($id) {
        $sql = 'select rh.id id,
                       egid egId,
                       compid compId,
                       eventno eventNo,
                       heatno heatNo,
                       scheduledtime scheduledTime,
                       actualTime actualTime,
                       image image, 
                       rh.options options
                from ' . E4S_TABLE_EVENTRESULTSHEADER . ' rh,
                     ' . E4S_TABLE_EVENTGROUPS . ' eg
                where rh.id = ' . $id . '
                and   eg.id = rh.egid';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            return null;
        }
        return $result->fetch_object();
    }

    public function writeTrackResultsInfo($obj) {
        $headerObj = $this->writeResultsHeaderInfo($obj);
        $this->_cleanOutResults($headerObj->id);
        $newResults = array();
        foreach ($obj->results as $result) {

            $newResult = array();
            $newResult['wind'] = $result->ws;
            $newResult['clubName'] = $result->club;
            $newResult['laneNo'] = $result->lane;
            $newResult['position'] = $result->place;
            $newResult['bibNo'] = $result->bibNo;
            $newResult['score'] = $result->time;
            if (isset($result->eaAward)) {
                $newResult['eaAward'] = $result->eaAward;
            } else {
                $newResult['eaAward'] = 0;
            }
            if (isset($result->scoreText)) {
                $newResult['scoreText'] = $result->scoreText;
            } else {
                $newResult['scoreText'] = '';
            }
	        if (!is_numeric($result->time) ){
		        $newResult['scoreText'] = $result->time;
	        }

            $newResult['athlete'] = $result->athlete;
            if ( $result->athleteId === 0) {
	            $newResult['athleteId'] = $result->teamId;
            }else {
	            $newResult['athleteId'] = $result->athleteId;
            }
            $newResults[] = $newResult;
        }

        $this->_writeResults($headerObj->id, $newResults);
    }

    public function writeResultsHeaderInfo($obj) {
        $data = array();
        $data['heatNo'] = $obj->heatNo;
        $data['scheduledTime'] = $this->egObj->startDate;
        $data['actualTime'] = $data['scheduledTime'];
        if (isset($obj->eventTime)) {
            $data['actualTime'] = $obj->eventTime;
        }

        $data['image'] = $obj->image;
        $data['options'] = new stdClass();
//        logTxt("!!!!!!!!!" . e4s_getDataAsType($data,E4S_OPTIONS_STRING));
        $row = $this->_readHeaderByHeatNo($obj->heatNo);
        if (is_null($row)) {
            $this->_writeHeader($data);
        } else {
            $data['id'] = $row->id;
//            if ($obj->image === R4S_BLANK_PHOTO && $row->image !== R4S_BLANK_PHOTO) {
//                $data['image'] = $row->image;
//            }
//            scoreboardClass::checkSBImage($data['image'], $this->compObj->getID());
            if (is_null($data['scheduledTime'])) {
                $data['scheduledTime'] = $row->scheduledTime;
            }
            $this->_updateHeader($data);
        }
        $retObj = $this->_readHeaderByHeatNo($obj->heatNo);
        if (is_null($retObj)) {
            Entry4UIError(6748, 'Failed to write results header');
        }
        return $retObj;
    }

    private function _updateHeader($data) {
        $sql = 'update ' . E4S_TABLE_EVENTRESULTSHEADER . "
                set actualtime = '" . $data['actualTime'] . "',
                    scheduledtime = '" . $data['scheduledTime'] . "',
                    image = '" . $data['image'] . "'
                where id = " . $data['id'];
        e4s_queryNoLog($sql);
    }

    public function writeFieldResultsInfo($results, $options) {
        $headerObj = $this->_writeHeaderForEG($options);
        $this->_clearAndWriteResults($headerObj->id, $results);
    }

    private function _writeHeaderForEG($options = '') {
        $egObj = $this->compObj->getEventGroupByNo($this->egObj->eventNo);
        $headerInfoObj = new stdClass();
        $headerInfoObj->heatNo = 1;
        $headerInfoObj->image = '';
        $headerInfoObj->scheduleTime = $egObj->startDate;
        $headerInfoObj->actualTime = $egObj->startDate;
        $headerInfoObj->options = $options;

        return $this->writeResultsHeaderInfo($headerInfoObj);
    }

    public function writeCardResultsToMainResults($results, $positionArr) {
        $headerObj = $this->_writeHeaderForEG();
        if (is_null($headerObj)) {
            return;
        }
        $this->_writeCardResultsForHeader($headerObj->id, $results,$positionArr);
    }

    private function _writeCardResultsForHeader($headerId, $results, $positionArr) {
        $sql = '
            select a.id athleteId,
                   e.athlete,
                   e.eventAgeGroup,
                   ce.eventId,
                   a.gender,
                   c.clubName,
                   b.bibNo,
                   e.teamBibNo,
                   a.dob
            from ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_CLUBS . ' c,
                 ' . E4S_TABLE_ATHLETE . ' a,
                 ' . E4S_TABLE_BIBNO . ' b
            where ce.compid = ' . $this->compObj->getID() . '
            and   ce.maxgroup = ' . $this->egObj->id . '
            and   e.compeventid = ce.id
            and   e.clubid = c.id
            and   e.athleteid = a.id
            and   b.compid = ce.compid
            and   b.athleteid = e.athleteid
        ';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return;
        }
        $info = array();
        while ($obj = $result->fetch_object()) {
            $athleteId = (int)$obj->athleteId;
            $obj->eventId = (int)$obj->eventId;
            if ($obj->teamBibNo !== '' and $obj->teamBibNo !== '0') {
                $obj->bibNo = $obj->teamBibNo;
            }
            $info[$athleteId] = $obj;
        }

        $deleteSql = '
            delete from ' . E4S_TABLE_EVENTRESULTS . '
            where resultHeaderId = ' . $headerId;
        e4s_queryNoLog($deleteSql);

        $egObj = new eventGroup($this->compObj->getID());
        $this->entriesFromEgId = $egObj->getEntriesFromEG($this->egObj->id);

        $insertSql = '';

        $sub = '';
        $pbAwards = new eaAwards($this->compObj->getID());

        foreach ($results as $result) {
            $useResult = $result->result;
            if ( $result->result === E4S_FIELDCARD_SUCCESS ){
                $useResult = preg_split('#:#',$result->resultKey)[0];
            }
//            $useResult = (float)$useResult;
            $scoreText = '';
            $pointsId = 0;
            if ($this->entriesFromEgId > 0) {
                $pointsObj = $this->_getPointsForScore($useResult);
                $pointsId = $pointsObj->pointsId;
                $scoreText = '(' . $pointsObj->points . ' pts)';
            }
            $athleteId = $result->athleteId;
            if (array_key_exists($athleteId, $info)) {
                $insertSql .= $sub . '(';
                $insertSql .= $headerId . ',';
                $insertSql .= '0,';
                $insertSql .= $positionArr[$athleteId] . ',';
                $insertSql .= $athleteId . ',';
                $insertSql .= "'" . addslashes($info[$athleteId]->athlete) . "',";
                $insertSql .= "'" . addslashes($info[$athleteId]->clubName) . "',";
                $insertSql .= "'" . $info[$athleteId]->bibNo . "',";
                $insertSql .= "'" . $useResult . "',";
                $insertSql .= "'" . $scoreText . "',";
                $insertSql .= $pointsId . ',';
                $insertSql .= "'" . $info[$athleteId]->eventAgeGroup . "',";
                $insertSql .= "'" . $info[$athleteId]->gender . "',";
                $insertSql .= $pbAwards->getEventLevelForAthleteDOB($info[$athleteId]->dob, $info[$athleteId]->eventId, $result->result);
                $insertSql .= ')';
                $sub = ',';
            }
        }
        if ( $insertSql !== ''){
	        $insertSql = 'insert into ' . E4S_TABLE_EVENTRESULTS . ' (resultHeaderId,lane, position,athleteId, athlete, club, bibno, score, scoreText, pointsid, ageGroup, gender, eaAward) values ' . $insertSql;
	        e4s_queryNoLog($insertSql);
        }
    }

    private function _clearAndWriteResults($headerId, $results) {
        $this->_cleanOutResults($headerId);
        $this->_writeResults($headerId, $results);
    }

    public function trackResultsType() {
        $sql = 'select id 
                from ' . E4S_TABLE_TRACKRESULT . '
                where compid = ' . $this->compObj->getID();
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            return 'Handheld timings';
        }
        return 'Photo finish';
    }

    private function _writeFieldResult($headerId, $cardEntry) {
        $cardEntry = e4s_getDataAsType($cardEntry, E4S_OPTIONS_OBJECT);
        $laneNo = 0;
        $wind = '';
        $position = $cardEntry->results->currentPosition;
        $athleteId = $cardEntry->athlete->id;
        $athlete = $cardEntry->athlete->name;
        $club = $cardEntry->athlete->club;
        $bibNo = $cardEntry->entry->bibNo;
        $score = $cardEntry->results->highScore;

        $eventResults = $this->readEventResults();

        foreach ($eventResults->heats as $eventHeatResult) {
            // Field will only have 1 heat
            $existingResults = $eventHeatResult->results;

            foreach ($existingResults as $existingResult) {
                if ($athleteId === $existingResult->athleteId) {
                    $rdId = (int)$existingResult->id;
                    break;
                }
            }
            // Field will only have 1 heat so pointless, but just in case
            if ($rdId !== 0) {
                break;
            }
        }
        if ($rdId === 0) {
            $detailResults = array();
            $detailResult = array();
            $detailResult['wind'] = $wind;
            $detailResult['laneNo'] = $laneNo;
            $detailResult['position'] = $position;
            $detailResult['athleteId'] = $athleteId;
            $detailResult['athlete'] = addslashes($athlete);
            $detailResult['clubName'] = $club;
            $detailResult['bibNo'] = $bibNo;
            $detailResult['score'] = $score;
            $detailResults[] = $detailResult;
            $this->_writeResults($headerId, $detailResults);
        }
    }

    public function readEventResults($egId = 0) {
        if ($egId !== 0) {
            $this->egObj = eventGroup::getEgObj($egId);
        }
        $this->compObj->getEGTypes();
        $retObj = new stdClass();
        $retObj->comp = $this->_getCompObj();
        $retObj->eventGroup = $this->_getEventGroupObj();
        $retObj->heats = $this->_getEventResults();
        $retObj->standards = $this->_getStandards();
        return $retObj;
    }

    private function _getStandards():array{
	    $egId = $this->egObj->id;
	    $standards = $this->compObj->getStandards();
	    if (empty($standards)) {
		    return $standards;
	    }
        $ceObjs = $this->compObj->getCEsForEgID($egId);
        $ageGroupIds = array();
        foreach ($ceObjs as $ceObj) {
            $ageGroupIds[$ceObj->ageGroupId] = $ceObj->ageGroupId;
        }
        $egObj = $this->compObj->getEventGroupByEgId($egId);
        $egEventName = $egObj->eventDef->name;
        $returnStandards = array();
	    foreach($standards as $standard=>$event) {
		    foreach ( $event as $eventName=>$ageGroups ) {
                if ( $eventName === $egEventName ) {
	                foreach ( $ageGroups as $arr ) {
		                foreach ( $arr as $std ) {
                            if ( in_array($std->ageGroupId, $ageGroupIds) ) {
                                if ( !array_key_exists($standard, $returnStandards) ) {
                                    $returnStandards[$standard] = array();
                                }
                                $returnStandards[$standard][] = $std;
                            }
		                }
	                }
                }
		    }
	    }
        return $returnStandards;
    }
    private function _getResultHeaders() {

    }

    private function _getCompObj() {
        $comp = new stdClass();
        $comp->id = $this->compObj->getID();
        $comp->name = $this->compObj->getName();
        $comp->logo = $this->compObj->getLogo();
        return $comp;
    }

    private function _getEventGroupObj() {
        $obj = new stdClass();
        $obj->id = $this->egObj->id;
        $eventPrefix = '';
        if (isset($this->egObj->typeNo)) {
            $eventPrefix = $this->egObj->typeNo . ' : ';
        }
        $obj->name = $eventPrefix . $this->egObj->name;
        $obj->eventNo = $this->egObj->eventNo;
        $obj->options = $this->egObj->options;
        $egObj = eventGroup::getObj($obj->id, TRUE);
        $obj->ageGroups = $egObj->getAgeGroups();
        $obj->wind = '';
        $obj->type = E4S_UOM_DISTANCE; // Should this be field ?
        $egOptions = e4s_addDefaultEventGroupOptions($this->egObj->options);
        $obj->seed = $egOptions->seed;
        if (!is_null($this->compObj->egTypes)) {
            $eOptions = e4s_getOptionsAsObj($this->compObj->egTypes[$obj->id]->eOptions);
            if (isset($eOptions->wind)) {
                $obj->wind = $eOptions->wind;
            }
            $obj->type = $this->compObj->egTypes[$obj->id]->uomType;
        }
        $obj->scheduledTime = e4s_sql_to_iso($this->egObj->startDate);
	    $this->eventGroup = $obj;
        return $obj;
    }

    private function _checkResultsHeader() {
        $config = e4s_getConfig();
        if ($config['defaultao']['code'] !== E4S_AOCODE_EA) {
            // Not EA site so let results load as usual
            return TRUE;
        }

        $eaAwards = new eaAwards($this->egObj->compId);
        $eaAwards->processResultsForEg($this->egObj->id);

        return TRUE;
    }

    private function _getEventResults($heatNo = 0) {
        if (!$this->_checkResultsHeader()) {
            return array();
        }

        $sql = "select  compid compId,
                        eventno eventNo,
                        heatno heatNo,
                        date_format(scheduledTime,'" . ISO_MYSQL_DATE . "') scheduledTime,
                        date_format(actualTime,'" . ISO_MYSQL_DATE . "') actualTime,
                        rh.image image,
                        lane laneNo,
                        agegroup ageGroup,
                        position position,
                        wind wind,
                        athleteid athleteId,
                        a.dob,
                        a.image athleteImage,
                        a.pof10id pof10id,
                        urn urn,
                        athlete athlete,
                        a.gender,
                        club clubName,
                        bibno bibNo,
                        score score,
                        qualify qualify,
                        scoreText scoreText,
                        rd.eaAward, 
                        rh.id rhId,
                        rd.id rdId,
                        rh.options rhOptions
                from " . E4S_TABLE_EVENTRESULTSHEADER . ' rh , ' . E4S_TABLE_EVENTRESULTS . ' rd left join ' . E4S_TABLE_ATHLETE . ' a on a.id = athleteid,
                     ' . E4S_TABLE_EVENTGROUPS . ' eg
                where compid = ' . $this->egObj->compId . '
                and   eventno = ' . $this->egObj->eventNo . '
                and   eg.id = rh.egid';
        if ($heatNo !== 0) {
            $sql .= ' and rh.heatno = ' . $heatNo;
        }
        $sql .= ' and   rh.id = rd.resultheaderid
                order by rh.heatno, if (rd.position = 0, 999, rd.position)';

        $dbResult = e4s_queryNoLog($sql);

        $heats = array();
        $lastHeatNo = 0;
        $heatResults = null;
        $baseAGRows = e4s_getBaseAgeGroups();
        while ($fetchedObj = $dbResult->fetch_object()) {
            $obj = $this->_normaliseResultObj($fetchedObj);

            if ($obj->heatNo !== $lastHeatNo) {
                if ($lastHeatNo !== 0) {
                    $heats[] = $heatResults;
                }
                $heatResults = $this->_newHeatResultsObj($obj);
            }
            $lastHeatNo = $obj->heatNo;

            $athleteResult = new stdClass();
            $athleteResult->position = $obj->position;
            $athleteResult->laneNo = $obj->laneNo;
            $athleteResult->ageGroup = '';
            if (!is_null($obj->dob)) {
                $baseGroup = e4s_getBaseAGForDOBForComp($this->egObj->compId, $obj->dob, $baseAGRows);
                $athleteResult->ageGroup = $baseGroup['Name'];
            }

            $athleteResult->bibNo = $obj->bibNo;
            // these need removing once confirmed with Nick
            $athleteResult->image = athleteClass::getImage($obj->athleteImage, $obj->pof10id);
            $athleteResult->athlete = $obj->athlete;
            $athleteResult->gender = $obj->gender;
            $athleteResult->athleteId = $obj->athleteId;
            $athleteResult->urn = $obj->urn;
            //
            $athleteResult->clubName = $obj->clubName;

            $athleteResult->scoreValue = strtoupper($obj->score);

            if ($this->eventGroup->type === E4S_EVENT_TRACK) {
                if (resultsClass::getNonNumeric($obj->score) === '') {
                    $athleteResult->scoreValue = resultsClass::getResultFromSeconds($obj->score);
                    $athleteResult->scoreValue = e4s_ensureString($athleteResult->scoreValue);
                }
            }

            if ( $this->eventGroup->type === E4S_UOM_POINTS) {
	            $athleteResult->scoreValue = str_replace( '.00', '', $athleteResult->scoreValue );
            }else{
	            if ( strpos( $athleteResult->scoreValue, '.' ) !== FALSE ) {
		            $arr               = preg_split( '~\.~', $athleteResult->scoreValue );
		            $lengthOfLastValue = strlen( $arr[ sizeof( $arr ) - 1 ] );

		            if ( $lengthOfLastValue === 1 ) {
			            $athleteResult->scoreValue = $athleteResult->scoreValue . '0';
		            }
	            } else {
		            if ( is_numeric( $athleteResult->scoreValue ) ) {
			            $athleteResult->scoreValue = $athleteResult->scoreValue . '.00';
		            }
	            }
            }
            $athleteResult->scoreValue = e4s_ensureString($athleteResult->scoreValue);

            $athleteResult->score = $obj->score;
            if ( (float)$obj->score < 0.01 ){
                $athleteResult->score = $obj->scoreText;
                $athleteResult->scoreValue = $obj->scoreText;
                $obj->scoreText = '';
            }

            $athleteResult->scoreText = $obj->scoreText;

            if ( e4s_ensureString($athleteResult->scoreText) === $athleteResult->scoreValue ){
	            $athleteResult->scoreText = '';
            }
            $qualify = $obj->qualify;
            if (is_null($qualify)) {
                $qualify = '';
            }
            $athleteResult->qualify = $qualify;
            $athleteResult->wind = $obj->wind;

            $athleteResult->id = $obj->rdId;
            $athleteResult->eaAward = (int)$obj->eaAward;

            $heatResults->results[] = $athleteResult;
        }
        if (!is_null($heatResults)) {
            $heats[] = $heatResults;
        }
//        var_dump($heatResults);
        return $heats;
    }

    private function _normaliseResultObj($obj) {
        $obj->compId = (int)$obj->compId;
        $obj->eventNo = (int)$obj->eventNo;
        $obj->heatNo = (int)$obj->heatNo;
        $obj->laneNo = (int)$obj->laneNo;
        $obj->position = (int)$obj->position;
        $obj->pof10id = (int)$obj->pof10id;

        $obj->rhId = (int)$obj->rhId;
        $obj->rdId = (int)$obj->rdId;
        $obj->rhOptions = e4s_getOptionsAsObj($obj->rhOptions);
        $obj->score = resultsClass::getResultInSeconds($obj->score);
        if (is_null($obj->athleteId)) {
            $obj->athleteId = 0;
        } else {
            $obj->athleteId = (int)$obj->athleteId;
        }
        if (is_null($obj->urn)) {
            $obj->urn = 0;
        }
        $obj->description = '';

        $options = e4s_getOptionsAsObj($obj->rhOptions);
        if (isset($options->description) and $options->description !== '') {
            $obj->description = $options->description;
        }
        return $obj;
    }

    private function _newHeatResultsObj($obj) {
        $heatResults = new stdClass();
        $heatResults->id = $obj->rhId;
        $heatResults->heatNo = $obj->heatNo;
        $heatResults->scheduledTime = $obj->scheduledTime;
        $heatResults->actualTime = $obj->actualTime;
        $heatResults->image = $obj->image;
        $heatResults->description = $obj->description;
        $heatResults->results = array();
        return $heatResults;
    }
}


class webResults {
    public $compObj;
    public $compDate;
    public $config;
    public $templates;
    public $resultsConfig;
    public $content;
    public $eventObj;
    public $urns;
    public $curURN;
    public $images = array();

    public function __construct($compid, $compdate) {
        $this->compObj = e4s_GetCompObj($compid, TRUE);
        if ($compdate === '') {
            $date = date_create($this->compObj->getRow()['Date']);
            $compdate = date_format($date, 'jS F Y');
        }
        $this->compDate = $compdate;
        $this->config = e4s_getConfig();
        $this->resultsConfig = $this->getResultsConfig();
        $this->templates = $this->getTemplates();
        $this->urns = $this->getURNs();
        $this->getImages();
        $this->getInboundFiles();
        if (!empty($this->content)) {
            $this->updateWebFiles();
            Entry4UISuccess('');
        }
        Entry4UIError(1000, 'Nothing to Upload', 200, '');
    }

    public function getResultsConfig() {
        $config = new stdClass();
//        $config->basePath = "/homepages/30/d754245643/htdocs/clickandbuilds/dev.entry4sports.com/";
        $config->basePath = ABSPATH;
        $results = 'results';
        $config->templatePath = $config->basePath . E4S_PATH . '/' . $results . '/templates/';
        $config->inboundPath = $config->basePath . $results . '/inboundresults/';
        $config->pfPath = 'pf/';
        $config->inboundTxtPath = 'txt/';
        $config->outboundPath = $config->basePath . $results . '/';
        $config->baseIncludePath = '/';
        $config->graphicsPath = $config->baseIncludePath . $results . '/images';
        $config->cssPath = $config->baseIncludePath . $results . '/css';
        $config->jsPath = $config->baseIncludePath . $results . '/js';

        return $config;
    }

    public function getTemplates() {
        $dir = $this->resultsConfig->templatePath;
        $files = scandir($dir, 1);
        $templates = array();
        foreach ($files as $file) {
            if ($file !== '..' and $file !== '.') {
                $templates[$file] = $this->readFile($dir, $file);
            }
        }
        return $templates;
    }

    public function readFile($dir, $file, $checkBrackets = FALSE) {
        $fullFile = $dir . $file;
        $handle = fopen($fullFile, 'r');
        if (!$handle) {
            Entry4UIError(9063, 'Failed to open file ' . $file, 200, '');
        }
        $fileContents = array();

        while (($line = fgets($handle)) !== FALSE) {
            if ($checkBrackets) {
                $line = $this->removeBracketsFromLine($line);
            }
            $fileContents[] = $line;
        }
        return $fileContents;
    }

    public function removeBracketsFromLine($line) {
        $pos = strpos($line, '(');
        if ($pos === FALSE) {
            return $line;
        }
        $arr = explode('(', $line);
        return $arr[0];
    }

    public function getURNs() {
        $sql = 'select bibno bibno, urn urn
                from ' . E4S_TABLE_BIBNO . ' b,
                     ' . E4S_TABLE_ATHLETE . " a
                where a.id = b.athleteid
                and   a.urn != ''
                and   b.compid = " . $this->compObj->getID();
        $result = e4s_queryNoLog($sql);
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $arr = array();
        foreach ($rows as $row) {
            if (!is_null($row['urn'])) {
                $arr[(int)$row['bibno']] = $row['urn'];
            }
        }
        return $arr;
    }

    public function getImages() {
        $images = scandir($this->resultsConfig->outboundPath . $this->compObj->getID() . '/' . $this->resultsConfig->pfPath);
        foreach ($images as $image) {
            if (strpos($image, '.png')) {
                $filename = explode(',', $image);
                $filename = str_replace('E0', 'event0', $filename[0]);
                $this->images[$filename] = $this->resultsConfig->pfPath . $image;
            }
        }
    }

    public function getInboundFiles() {
        $dir = $this->resultsConfig->inboundPath . $this->compObj->getID() . '/';
        if (!file_exists($dir)) {
            Entry4UIError(9064, 'Inbound files do not exist', 200, '');
        }
        $files = scandir($dir, 1);
        $this->content = array();
        $this->eventObj = array();
        foreach ($files as $file) {
            $checkBrackets = FALSE;
            if ($file !== E4S_SCHEDULE_FILE) {
                $checkBrackets = TRUE;
            }
            if (strpos($file, '.html') !== FALSE and $file !== '.' and $file !== '..') {
                $content = $this->readFile($dir, $file, $checkBrackets);
                if ($file === E4S_SCHEDULE_FILE) {
                    $newcontent = array_splice($content, 1);
                } else {
                    $newcontent = $content;
                    if (sizeof($newcontent) > 1) {
                        // as results
                        $this->setEventObj($file, $newcontent[0]);
                    }
                }
                if (sizeof($newcontent) > 1) {
                    // has results
                    $newcontent[0] = '<TR class="odd">';
                    $this->content[$file] = $newcontent;
                }
            }
        }
    }

    public function setEventObj($filename, $line) {
        $obj = new stdClass();
        $strArr = explode('</h1>', $line);
        $titleArr = explode(';', $strArr[0]);
        $titleArr = explode(', W=', $titleArr[0]);

        $obj->title = trim(str_replace('<h1>', '', $titleArr[0]));
        $obj->ws = '';
        if (sizeof($titleArr) > 1) {
            $obj->ws = trim($titleArr[1]);
        }
        if (sizeof($strArr) < 2) {
            // No results as yet
            $this->eventObj[$filename] = null;
            return;
        }
        $strArr = explode('<TR', $strArr[1]);
        $links = $strArr[0];
        $linksArr = explode('">', $links);
        $next = '';
        $prev = '';

        if (sizeof($linksArr) === 2) {
            // Only a prev or next
            $link = $linksArr[0];
            if (strpos($link, "'next'") !== FALSE) {
                $next = explode('HREF="', $link);
                $next = $next[1];
            } else {
                $prev = explode('HREF="', $link);
                $prev = $prev[1];
            }

        } elseif (sizeof($linksArr) > 2) {
            $link = $linksArr[0];
            $prev = explode('HREF="', $link);

            if (sizeof($prev) < 2) {
                $next = ''; // Zero Test ?
            } else {
                $prev = $prev[1];
                $link = $linksArr[1];
                $next = explode('HREF="', $link);
                $next = $next[1];

            }
        }
        $obj->nextFile = $next;
        $obj->prevFile = $prev;

        $this->eventObj[$filename] = $obj;
    }

    public function updateWebFiles() {
        $this->ensureOutboundPathExists();
//        $inboundDir = $this->resultsConfig->inboundPath;
        $inboundFiles = $this->content;
        foreach ($inboundFiles as $filename => $content) {
            if ($filename === E4S_SCHEDULE_FILE) {
                $this->processSchedule($content);
            } else {
                $this->processEvent($filename, $content);
            }
        }
    }

    public function ensureOutboundPathExists() {
        $dir = $this->resultsConfig->outboundPath . $this->compObj->getID();
        if (!file_exists($dir)) {
            mkdir($dir, 0777, TRUE);
        }
    }

    public function processSchedule($scheduleContent) {
        $scheduleTemplate = $this->templates[E4S_SCHEDULE_FILE];
        $basePath = $this->resultsConfig->outboundPath . $this->compObj->getID();
        $scheduleFile = $basePath . '/' . E4S_SCHEDULE_FILE;
        $indexFile = $basePath . '/index.html';
        $outputHandle = fopen($scheduleFile, 'w');
        foreach ($scheduleTemplate as $line) {
            $this->processLine(E4S_SCHEDULE_FILE, $outputHandle, $line, $scheduleContent);
        }
        // now copy to index.html
        copy($scheduleFile, $indexFile);
    }

    public function processLine($filename, $fileHandle, $line, $content) {
        if (strpos($line, E4S_OPEN_TAG) === FALSE) {
            fwrite($fileHandle, $line);
        } else {
            $line = $this->replaceTags($filename, $line, $content);
            fwrite($fileHandle, $line);
        }
    }

    public function replaceTags($filename, $line, $content) {
        $line = $this->replaceTag($filename, 'TITLE', $line);
        $line = $this->replaceTag($filename, 'MEET_DATE', $line);
        $line = $this->replaceTag($filename, 'MEET_TITLE', $line);
        $line = $this->replaceTag($filename, 'HEAT_TITLE', $line);
        $line = $this->replaceTag($filename, 'MEET_LOCATION', $line);
        $line = $this->replaceTag($filename, 'GRAPHICS_DIR', $line);
        $line = $this->replaceTag($filename, 'CSS_DIR', $line);
        $line = $this->replaceTag($filename, 'JS_DIR', $line);
        $line = $this->replaceTag($filename, 'PREV_FILENAME', $line, $content);
        $line = $this->replaceTag($filename, 'NEXT_FILENAME', $line, $content);
        $line = $this->replaceTag($filename, 'IMAGE_FILENAME', $line, $content);
        $line = $this->replaceTag($filename, 'SCHEDULE', $line, $content);
        $line = $this->replaceTag($filename, 'RESULT', $line, $content);

        return $line;
    }

    public function replaceTag($filename, $tag, $line, $content = '') {
        if (strpos($line, E4S_OPEN_TAG . $tag . E4S_CLOSE_TAG)) {

            switch ($tag) {
                case 'GRAPHICS_DIR':
                    $line = $this->tagGRAPHICS_DIR($tag, $line);
                    break;
                case 'CSS_DIR':
                    $line = $this->tagCSS_DIR($tag, $line);
                    break;
                case 'JS_DIR':
                    $line = $this->tagJS_DIR($tag, $line);
                    break;
                case 'TITLE':
                    $line = $this->tagTITLE($tag, $line);
                    break;
                case 'MEET_TITLE':
                    $line = $this->tagMEET_TITLE($tag, $line);
                    break;
                case 'MEET_DATE':
                    $line = $this->tagMEET_DATE($tag, $line);
                    break;
                case 'HEAT_TITLE':
                    $line = $this->tagHEAT_TITLE($tag, $line, $filename);
                    break;
                case 'MEET_LOCATION':
                    $line = $this->tagMEET_LOCATION($tag, $line);
                    break;
                case 'PREV_FILENAME':
                    $line = $this->tagPREV_FILENAME($tag, $line, $filename);
                    break;
                case 'NEXT_FILENAME':
                    $line = $this->tagNEXT_FILENAME($tag, $line, $filename);
                    break;
                case 'IMAGE_FILENAME':
                    $line = $this->tagIMAGE_FILENAME($tag, $line, $filename);
                    break;
                case 'SCHEDULE':
                    $line = $this->tagCONTENT($tag, $line, $content, FALSE);
                    break;
                case 'RESULT':
                    $line = $this->tagCONTENT($tag, $line, $content, TRUE);
                    break;
            }
        }
        return $line;
    }

    public function tagGRAPHICS_DIR($tag, $line) {
        $line = str_replace(E4S_OPEN_TAG . $tag . E4S_CLOSE_TAG, $this->resultsConfig->graphicsPath, $line);
        return $line;
    }

    public function tagCSS_DIR($tag, $line) {
        $line = str_replace(E4S_OPEN_TAG . $tag . E4S_CLOSE_TAG, $this->resultsConfig->cssPath, $line);
        return $line;
    }

    public function tagJS_DIR($tag, $line) {
        $line = str_replace(E4S_OPEN_TAG . $tag . E4S_CLOSE_TAG, $this->resultsConfig->jsPath, $line);
        return $line;
    }

    public function tagTITLE($tag, $line) {
        $line = str_replace(E4S_OPEN_TAG . $tag . E4S_CLOSE_TAG, $this->compObj->getName(), $line);
        return $line;
    }

    public function tagMEET_TITLE($tag, $line) {
        $line = str_replace(E4S_OPEN_TAG . $tag . E4S_CLOSE_TAG, $this->compObj->getName() . ' ' . $this->compDate, $line);
        return $line;
    }

    public function tagMEET_DATE($tag, $line) {
        $line = str_replace(E4S_OPEN_TAG . $tag . E4S_CLOSE_TAG, $this->compDate, $line);
        return $line;
    }

    public function tagHEAT_TITLE($tag, $line, $filename) {
        $title = $this->eventObj[$filename]->title;
        if ($this->eventObj[$filename]->ws !== '') {
            $title .= '  Windspeed:' . $this->eventObj[$filename]->ws;
        }
        $line = str_replace(E4S_OPEN_TAG . $tag . E4S_CLOSE_TAG, $title, $line);
        return $line;
    }

    public function tagMEET_LOCATION($tag, $line) {
        $line = str_replace(E4S_OPEN_TAG . $tag . E4S_CLOSE_TAG, $this->compObj->getLocation()->name, $line);
        return $line;
    }

    public function tagPREV_FILENAME($tag, $line, $filename) {
        $line = str_replace(E4S_OPEN_TAG . $tag . E4S_CLOSE_TAG, $this->eventObj[$filename]->prevFile, $line);
        return $line;
    }

    public function tagNEXT_FILENAME($tag, $line, $filename) {
        $line = str_replace(E4S_OPEN_TAG . $tag . E4S_CLOSE_TAG, $this->eventObj[$filename]->nextFile, $line);
        return $line;
    }

    public function tagIMAGE_FILENAME($tag, $line, $filename) {
        $imagePath = $this->getImageFromFileName($filename);
        $line = str_replace(E4S_OPEN_TAG . $tag . E4S_CLOSE_TAG, $imagePath, $line);
        return $line;
    }

    public function getImageFromFileName($filename) {
        $fileNoExt = explode('.', $filename);
        $imagePath = '';
        if (array_key_exists($fileNoExt[0], $this->images)) {
            $imagePath = $this->images[$fileNoExt[0]];
        }
//        echo $this->eventObj[$filename]->title;
//        exit();
        return $imagePath;
    }

    public function tagCONTENT($tag, $line, $content, $eventContent) {
        $arr = explode(E4S_OPEN_TAG . $tag . E4S_CLOSE_TAG, $line);
        $line = $arr[0];

        // Content is an array with some elements holding more than 1 row so , join then split to get correct array
        if ($eventContent) {
            foreach ($content as $value) {
                $valueArr = explode('</td>', $value);
                foreach ($valueArr as $tableRow) {
                    $tableRow = $value . '</td>';
                    if (strpos($tableRow, '"id">') !== FALSE) {
                        $this->getCurURN($tableRow);
                    } elseif (strpos($tableRow, '"info"') !== FALSE) {
                        $tableRow = $this->setCurURN($tableRow);
                    }
                    $line .= $tableRow;
                }
            }
        } else {
            foreach ($content as $value) {
                $line .= $value;
            }
        }

        $line .= $arr[1];
        return $line;
    }

    public function getCurURN($line) {

        $arr = explode('id">', $line);
        if (sizeof($arr) !== 2) {
            return;
        }
        if (is_null($this->urns)) {
            return;
        }

        $arr = explode('<', $arr[1]);
        $bib = (int)$arr[0];

        if (array_key_exists($bib, $this->urns)) {
            $this->curURN = $this->urns[$bib];
        }
    }

    public function setCurURN($line) {

        if ($this->curURN === '' or is_null($this->curURN)) {
            return $line;
        }
//        if ( sizeof($strArr) < 2 ){
//           // No results as yet
//            $this->eventObj[$filename] = null;
//            return;
//        }
        $retLine = '<TD CLASS="info">
                        <a 
                            href="https://www.thepowerof10.info/athletes/profile.aspx?ukaurn=' . $this->curURN . '"
                            target="pof10"
                        >
                            <img src="/results/images/pof10.jpg">
                        </a>
                    </TD>';

        $this->curURN = '';
        return $retLine;
    }

    public function processEvent($filename, $content) {
        $template = $this->templates[E4S_EVENT_FILE];
        $basePath = $this->resultsConfig->outboundPath . $this->compObj->getID();
        $eventFile = $basePath . '/' . $filename;
        $outputHandle = fopen($eventFile, 'w');
        foreach ($template as $line) {
            $this->processLine($filename, $outputHandle, $line, $content);
        }
    }
}