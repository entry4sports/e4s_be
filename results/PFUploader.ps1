$uri = "https://entry4sports.co.uk/wp-json/e4s/v5/results/pf/166"

Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
#Unregister-Event FileCreated

$folder = 'G:\My Drive\CurrentComp\' # Enter the root path you want to monitor.
$filter = '*.txt'  # You can enter a wildcard filter here.

# In the following line, you can change 'IncludeSubdirectories to $true if required.
$fsw = New-Object IO.FileSystemWatcher $folder, $filter -Property @{ IncludeSubdirectories = $false; NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite' }


Register-ObjectEvent $fsw Created -SourceIdentifier FileCreated -Action {
    $name = $folder + $Event.SourceEventArgs.Name;

    #$changeType = $Event.SourceEventArgs.ChangeType

    #$timeStamp = $Event.TimeGenerated

    Invoke-RestMethod -Uri $uri -Method Post -InFile $name

    $contentType = "multipart/form-data";

    $body = @{
        "fileToUpload" = Get-Content($name) -Raw
    };
    try
    {
        Invoke-WebRequest -UseBasicParsing -Uri $uri -Method Post -ContentType $contentType -Body $body
        Write-Host ($name + " has been sent to Entry4Sports");
    }
    catch
    {
        # Dig into the exception to get the Response details.
        # Note that value__ is not a typo.
        Write-Host "StatusCode:" $_.Exception
        Write-Host ($name + " has not been sent to Entry4Sports");
    }
}

