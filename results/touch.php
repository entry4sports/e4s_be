<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
//https://entry4sports.co.uk<php echo E4S_PATH >results/touch.php?compid=511&interactive=1
$clickedWait = 60000;
$defaultWait = 20000;
//$clickedWait = 10000;
//$defaultWait = 5000;
$compId = null;
$track = false;
$field = false;
$date = '';
if ( array_key_exists('date', $_GET)) {
	$date = $_GET['date'];
}
if ( array_key_exists('track', $_GET)) {
    $track = $_GET['track'];
}
if ( $track === '1' ) {
    $track = TRUE;
}else {
    $track = FALSE;
}
if ( array_key_exists('field', $_GET)) {
    $field = $_GET['field'];
}

if ( $field === '1' ) {
    $field = TRUE;
}else {
    $field = FALSE;
}

if ( array_key_exists('compid', $_GET)) {
    $compId = (int)$_GET['compid'];
}else if ( array_key_exists('locid', $_GET)){
    $locId = $_GET['locid'];
    $compId = e4sCompetition::getNextCompForLocation($locId);
}

if (!is_numeric($compId)){
    echo "Invalid competition id";
    exit;
}
$compObj = e4s_getCompObj($compId);
$qrCode = $compObj->getQrCode();
$interactive = true; // default is interactive
if ( array_key_exists('interactive', $_GET)){
    $interactive = $_GET['interactive'];
	if ( $interactive === '0' or $interactive === 'false' ) {
		$interactive = false;
	}
}

$results = e4s_getResultHeaders($compId, $track, $field, $date);
?>

<html lang="en">
<head>
    <meta charset="utf-8">
<!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title><?php echo $compObj->getName() ?> Results</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
    <style>
        .autoScroll {
            -webkit-animation-name: autoScroll;
            -webkit-animation-duration: 30s;
            -webkit-animation-iteration-count: infinite;
            -webkit-animation-direction: up;
            -webkit-animation-timing-function:linear;
            margin: 0px 10px 0px 10px;
        }

        @-webkit-keyframes autoScroll {
            0% {
                transform:translateY(0);
            }
            100% {
                transform:translateY(-50%);
            }
        }
        #tabs .ui-tabs-nav {
            margin: 16px 0 0 0 !important;
        }
        .ui-tabs-vertical { width: 55em; }
        .ui-tabs-vertical .ui-tabs-nav { padding: .2em .1em .2em .2em; float: left;  }
        .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
        .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
        .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; }
        .ui-tabs-vertical .ui-tabs-panel { padding: 1em; }
        .contentMsg {
            padding-left: 40px;
            padding-top: 20px;
        }
        .ui-state-active {
            background-color: var(--e4s-navigation-bar--primary__background) !important;
        }
        .e4sTitle {
            color: white;
            font-size: xx-large;
        }
        .resultsTable {
            /*padding-left: 20px;*/
            position: absolute;
            left: 25px;
            overflow-x: hidden;
            overflow-y: auto;
            font-size: 1.3vw;
        }
        body {
            overflow: hidden;
        }
        ul {
            height: 92vh;
            overflow-y: scroll;
            overflow-x: hidden;
        }

        .resultLine {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            border-bottom: 1px solid black;
        }
        .hHeat {
            font-weight: 900;
        }
        .headerLine {
            background-color: var(--e4s-navigation-bar--primary__background);
            color: white;
        }
        .hposition {
            width: 5em;
            padding: 3px;
        }
        .hheatNo {
            width: 5em;
            padding: 3px;
        }
        .hgender {
            width: 5em;
            padding: 3px;
        }
        .hathlete {
            width: 25em;
            padding: 3px;
        }
        .haffiliation {
            width: 25em;
            padding: 3px;
        }
        .hage {
            width: 8em;
            padding: 3px;
        }
        .hresult {
            width: 10em;
            padding: 3px;
        }
        .countdown {
            position: fixed;
            bottom: 0;
            right: 0;
            background-color: lightblue;
            padding: 5px;
            font-size: 2vh;
            height: 4vh;
            z-index: 2;
        }
        .tickerTape {
            height: 4vh;
            position: fixed;
            bottom: 0px;
            left: 0px;
            background-color: lightblue;
            padding: 5px;
            color: rgb(0, 0, 0);
            z-index: 1;
        }
        .result {
            /*font-size: 1.2vw;*/
        }
        html {
            touch-action:none;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>

    <script>
//         document.addEventListener('touchstart', function(event){
// alert(event);
//         }, {passive: false});
        function processSocketEvent(data){
            switch (data.action) {
                case "<?php echo R4S_SOCKET_FIELD_RESULT ?>":
                    inboundFieldResult(data.payload);
                    break;
                case "<?php echo R4S_SOCKET_EVENT_RESULTS_STATE ?>":
                    inboundEventStatus(data.payload);
                    break;
            }
        }
        function inboundEventStatus(payload){
            let msg = '';
            if ( !payload.open ){
                let egTab = $("#atabs-" + payload.egId);
                if ( egTab.length === 1 ){
                    // got event tab so now get name
                    let egName = egTab.attr("egName");
                    msg = egName + ' - Event Complete';
                    updateTickerTape(msg);
                }
            }
        }
        function getFormattedPerf(type, perf){
            perf = parseFloat(perf).toFixed(2);
            if (type !== '<?php echo E4S_EVENT_FIELD ?>' ){
                if (perf > 59.59) {

                    let mins = Math.floor(perf / 60);
                    let secs = perf - (mins * 60);
                    let prefix = '';
                    if (secs < 10) {
                        prefix = '0';
                    }
                    perf = mins + ':' + prefix + secs.toFixed(2) + 'mins';
                }else{
                    perf = perf.toFixed(2) + 's';
                }
            }else{
                let split = perf.split('.');
                let metre = parseInt(split[0]);
                let cm = parseInt(split[1]);
                if ( cm < 10 ){
                    cm = '0' + cm;
                }
                perf = metre + '.' + cm + 'm';
            }
            return perf;
        }
        function inboundFieldResult(payload){
            let eventGroup = payload.ranking.eventGroup;
            let eventName = eventGroup.name;
            let athlete = null;
            let result = '';
            let trial = '';
            for(let er in payload){
                if ( athlete === null ) {
                    let eventResults = payload[er];
                    for (let a in eventResults) {
                        let resultObj = eventResults[a];
                        athlete = resultObj.athlete;
                        for(let p in resultObj){
                            if ( p === 'athlete') {
                                continue;
                            }
                            if ( p === 'params') {
                                continue;
                            }
                            result = resultObj[p];
                            trial = p.replace('t', '');
                            break;
                        }
                        break;
                    }
                }
            }
            switch (result){
                case '':
                    return;
                    break;
                case '-':
                    result = 'Passed';
                    break;
                case 'r':
                    result = 'Retired';
                    break;
                case 'x':
                    result = 'Fail';
                    break;
                default:
                    result = getFormattedPerf('<?php echo E4S_EVENT_FIELD ?>', result);
                    break;
            }
            if ( result === '' ){
                return;
            }
            let currentEgId = getCurrentEgId();
            if ( eventGroup.id === currentEgId){
                newResultForCurrentEg(athlete, trial, result, currentEgId)
            }else {
                let msg = eventName + ' - T' + trial + ':<b>' + result + '</b> ' + athlete.firstName + ' ' + athlete.surName + ' (<span style="font-size:small;">' + athlete.club + '</span>)';
                updateTickerTape(msg);
            }
        }
        function newResultForCurrentEg(athlete, trial, score, egId){
            updateEgDisplay(currentEgId);
        }
        function getCompetition(){
            return {
                id: <?php echo $compId; ?>
            }
        }
	    <?php socketClass::outputJavascript() ?>
        let cycleTimeout = null;
        let countDown = 10000;
        let interactive = <?php if($interactive) {echo "true";}else{echo "false";}?>;
        let resultsHTML = '';
        function initTabs(){
            $( "#tabs" ).tabs({
            }).addClass( "ui-tabs-vertical ui-helper-clearfix" );
            $( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
        }
        $( function() {
            initTabs();

            setInterval(function () {
                displayCountDown();
            }, 1000);

            $("#e4s-nav-bar").css("z-index", 1);
            let titleObj = $(".e4s-navigation-bar-menu");
            let title = '<?php echo $compObj->getName(); ?>';
            <?php

            if ( $track !== $field) {
                if ( $track ) {
                    echo "title += ' - Track Results';";
                }
                if ( $field ) {
                    echo "title += ' - Field Results';";
                }
            }
             ?>
            titleObj.html(title);
            if ( '<?php echo $qrCode ?>' !== '') {
                titleObj.parent().append('<ul class="e4s-navigation-bar-menu" style="color:white;">Results on your phone, scan here : <img src="<?php echo $qrCode ?>" style="width:64px"></ul>');
            }
            titleObj.addClass("e4sTitle");
        } );

        function displayCountDown(tabObj){
            // display countdown bottom right of screen
            let msg = getMsg();
            $("#countdown").html(msg + countDown / 1000 + " seconds");
            if ( countDown<5001){
                $("#countdown").css("background-color", "red");
                $("#countdown").css("color", "white");
            }else{
                $("#countdown").css("background-color", "lightblue");
                $("#countdown").css("color", "black");
            }
            countDown -= 1000;
            if ( countDown < 0 ){
                moveToNext();
            }
        }

        function updateTickerTape(msg){
            // let html = '<span></span><span>' + msg + '</span>';
            if (msg !== '' ) {
                let html = '<span>' + msg + '</span>';

                $("#tickerTape").html(html);
                $("#tickerTape").effect("slide", "swing", 1000, null)
            }
        }
        function getMsg(){
            let msg = '';
            if(interactive) {
                msg = "Click/touch an event name or next will automatically show in ";
            }else{
                msg = "Next event will automatically show in ";
            }
            <?php if (sizeof($results) < 1){?>
                    msg = "No results currently available. Checking in ";
            <?php }?>
            return msg;
        }

        function cycle(delay){
            if ( typeof delay === "undefined" ){
                delay = <?php echo $defaultWait;?>;
            }
            countDown = delay;
            clearTimeout(cycleTimeout);
            cycleTimeout = window.setTimeout(function(){
                moveToNext();
            },delay);
        }
        const scrollIntoViewWithOffset = (element, offset) => {
            window.scrollTo({
                behavior: 'smooth',
                top:
                    element.getBoundingClientRect().top -
                    document.body.getBoundingClientRect().top -
                    offset,
            })
        }
        function moveToNext(){
            let activeClass = "ui-state-active";
            let current = $("." + activeClass);
            let next = current.next("li");

            if ( next.length === 0 ) {
                location.reload();
            }

            let element = next.find("a");

            current.removeClass(activeClass);
            next.addClass(activeClass);

            element[0].onclick();
            element[0].scrollIntoView({behavior: "smooth", inline: "center"});
            cycle();
        }
        function showESAAPoints(){
            if ( event){
                // action clicked so update cycle
                interactive = true;
                cycle(<?php echo $clickedWait;?>);
            }
            let activeClass = "ui-state-active";
            let current = $("." + activeClass);
            current.removeClass(activeClass);
            let clickedTab = $("#atabs-ESAA").parent();
            clickedTab.addClass(activeClass);
            let obj = $("#tabs-ESAA");
            obj.show();
            obj.html('<iframe style="" width=100% height=100% src="/<?php echo $compId ?>/esaa"></iframe>');
        }
        function showEgResults(egId){
            if ( event){
                // action clicked so update cycle
                interactive = true;
                cycle(<?php echo $clickedWait;?>);
            }

            $("div > [id|=tabs]").hide();
            updateEgDisplay(egId);
            return false;
        }
        function updateEgDisplay(egId){
            let activeClass = "ui-state-active";
            let current = $("." + activeClass);
            current.removeClass(activeClass);

            let clickedTab = $("#atabs-" + egId).parent();
            clickedTab.addClass(activeClass);
            let tabId = 'tabs-' + egId;
            let obj = $("#" + tabId);
            let egName = $("#a" + tabId).attr('egname');
            obj.show();
            obj.html("Loading " + egName + " results...");
            $.ajax({
                    url: "<?php echo E4S_BASE_URL ?>results/read/" + egId,
                    success: function (data) {
                        let useData = data.data;
                        let egId = useData.eventGroup.id;
                        resultsHTML = displayEgResults(useData);

                        // $("#content").remove();
                        let obj = $("#tabs-" + egId);
                        obj.show();
                        obj.html(returnContent(resultsHTML));

                        autoScroll(obj);
                        document.body.scrollIntoView({behavior: "smooth", inline: "center"});
                        <?php
                        if ( $compId === 482 or $compId === 617) {
                            echo '$("#atabs-ESAA").parent().removeClass("ui-state-active")';
                        }
                        ?>
                    }
                }
            );
        }
        function returnContent(html){
            let boundHtml = '<div id="content" class="content">';
            boundHtml += '<table class="resultsTable" id="resultsTable">';
            boundHtml += html;
            boundHtml += '<tr>';
            boundHtml += '<td style="height:100px;">';
            boundHtml += '</td>';
            boundHtml += '</tr>';

            boundHtml += '</table>';
            boundHtml += '</div>';

            return boundHtml;
        }
        function autoScroll(obj){
            let useHeight = 10;
            if ((obj.height() + useHeight + 5) > window.innerHeight) {
                $("body").css("overflow", "hidden");
                obj.html(returnContent(resultsHTML + resultsHTML));
                obj.addClass("autoScroll");
            }
        }
        function getCurrentEgId(){
            return parseInt($(".ui-state-active a").attr("egid"));
        }
        function isEgOpen(eg){
            return eg.options.resultsOpen && eg.options.resultsOpen === true;
        }
        function displayEgResults(data){
            let html = '';
            if ( isEgOpen(data.eventGroup) ) {
                html += '<div style="font-size: 20px; font-weight: bold; color: red;">Event is in progress and results are not complete</div>';
            }

            let lastHeatNo = 0;
            for(let h in data.heats) {
                let heat = data.heats[h];
                if ( lastHeatNo !== heat.heatNo && data.heats.length > 1 ){
                    let description = 'Race ' + heat.heatNo;
                    if ( heat.description !== '' ){
                        description = heat.description;
                    }
                    html += '<tr class="hHeat">';
                    html += '<td colspan="7" style="text-align: center;">' + description + '</td>';
                    html += '</tr>';
                }
                lastHeatNo = heat.heatNo;
                let results = heat.results;
                html += '<tr class="headerLine">';
                html += '<td class="hposition">Pos</td>';
                html += '<td class="hheatNo">Bib</td>';
                html += '<td class="hgender">M/F</td>';
                html += '<td class="hathlete">Name</td>';
                html += '<td class="haffiliation">Organisation</td>';
                html += '<td class="hage">Group</td>';
                html += '<td class="hresult">Score</td>';
                html += '</tr>';
                let colour = '';
                let gender = '';
                for (let r in results) {
                    gender = "Female";
                    let result = results[r];
                    if ( result.gender === 'M' ){
                        gender = 'Male';
                    }
                    html += '<tr style="background-color:' + colour + ';">';
                    colour = colour === '' ? 'aliceblue' : '';
                    let position = result.position;
                    if ( parseInt(result.position) === 0){
                        position = "-";
                    }
                    html += '<td class="position">' + position + '</td>';
                    html += '<td class="heatNo">' + result.bibNo + '</td>';
                    html += '<td class="gender">' + gender + '</td>';
                    html += '<td class="athlete">' + result.athlete + '</td>';
                    html += '<td class="affiliation">' + result.clubName + '</td>';
                    html += '<td class="age">' + result.ageGroup + '</td>';
                    let score = result.scoreValue;
                    if ( parseInt(result.position) === 0){
                        score = result.scoreText;
                        if (score === ''){
                            score = 'DNS';
                        }
                    }
                    if ( result.scoreText.indexOf('pts)') > -1 ){
                        score += ' ' + result.scoreText;
                    }
                    let qualify = result.qualify;
                    if ( qualify !== '' ){
                        qualify = ' ' + qualify;
                    }
                    html += '<td class="result">' + score + qualify + '</td>';
                    html += '</tr>';
                }
            }
            html += '<tr><td><span style="height: 20px;">&nbsp;</span></td></tr>';
            return html;
        }
    </script>
</head>
<body oncontextmenu="return false;">
<?php
include_once get_template_directory() . '-child/header-e4s.php';
    $divsHtml = "";
?>
<div id="tickerTapeBack" class="tickerTape" style="width:100%;">&nbsp;</div>
<div id="tickerTape" class="tickerTape"></div>
<div id="tabs" style="width:99%;height: 95%;overflow-y: hidden;">
    <ul>
        <?php
            if ( sizeof ($results) < 1 ){
                $divsHtml = '<div id="contentMsg" class="contentMsg">Awaiting Results ....</div>';
            }else {
                if ($compId === 482 or $compId === 617) {
	                echo '<li><a id="atabs-ESAA" egId="ESAA" egName="points" onclick="showESAAPoints(); return false;" href="#ESAA">ESAA Points</a></li>';
	                $divsHtml .= "<div id=\"tabs-ESAA\" style='display:none; position:relative; margin-top:3em; height: 90%; overflow-y: scroll;'>ESAA Points</div>";
                }
                foreach ($results as $egId => $egResult) {
                    echo '<li><a id="atabs-' . $egId . '" egId="' . $egId . '" egName="' . $egResult->name . '" onclick="return showEgResults(' . $egId . ',\'' . $egResult->name . '\');" href="#' . $egId . '">' . $egResult->typeNo . '-' . $egResult->name . '</a></li>';
                    $divsHtml .= "<div id=\"tabs-" . $egId . "\" style='display:none; position:relative; margin-top:3em; height: 90%; overflow-y: scroll;'>{$egResult->name}</div>";
                }
	            echo '<li style="padding-bottom:60px;"><a id="atabs-0" egId="0" egName="refreshList" onclick="location.reload();" href="#0">Refresh Event List</a></li>';
	            $divsHtml .= "<div id=\"tabs-0\" style='display:none; position:relative; margin-top:3em; height: 90%; overflow-y: scroll;'>Refreshing Event List</div>";
            }
        ?>
    </ul>
    <?php
        echo $divsHtml;
    ?>
</div>
<div id="countdown" class="countdown"></div>
<script>
    <?php
    foreach($results as $egId=>$egResult) {
        echo 'showEgResults(' . $egId . ',\'' . $egResult->name . '\');';
        break;
    }
    ?>
    cycle();
</script>
</body>
</html>
<?php
function e4s_getResultHeaders($compId, $track, $field, $date){
//    and eg.typeno like '" . E4S_EVENT_FIELD . "%'
    $sql = 'select rh.id, rh.heatNo, eg.id egId, eg.name, eg.typeNo
            from ' . E4S_TABLE_EVENTRESULTSHEADER . ' rh,
                 ' . E4S_TABLE_EVENTGROUPS . ' eg
            where eg.compId = ' . $compId . '
            and eg.id = rh.egid ';
    if ( $track !== $field ){
        $sql .= " and eg.typeNo like '";
        if ( $track ) {
             $sql .= E4S_EVENT_TRACK . "%' ";
        }
        if ( $field ) {
            $sql .= E4S_EVENT_FIELD . "%' ";
        }
    }

    if ( $date !== '' ){
        $sql .= " and eg.startDate >= '" . $date . "' ";
    }
    $sql .= 'order by eg.eventNo';

    $results = e4s_queryNoLog($sql);
    $resultsArr = array ();
    while ( $row = $results->fetch_object() ){
        $resultsArr[$row->egId] = $row;
    }
    return $resultsArr;
}