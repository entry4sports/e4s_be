<?php
include_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_createFeederEntries($obj){
    $params = $obj->get_params('JSON');

    $targetEntriesObj = new e4sTargetEntries($params['entries'], $params['entity']);
    $targetEntriesObj->processEntries();

    Entry4UISuccess($targetEntriesObj->athletesByUrn);
}

class e4sTargetEntries {
    public $entries;
    public $compObj;
    public $allCompAges;
    public $athletesById;
    public $athletesByUrn;
    public $teams;
    public $entityLevel;
    public $entityId;

    public function __construct($entries, $entity) {
        if (is_null($entity)) {
            Entry4UIError(6700, 'No entity passed to entries');
        }
        $this->entityId = $entity['id'];
        $this->entityLevel = $entity['level'];
        if ($this->entityId < 1 or $this->entityLevel < 1) {
            Entry4UIError(6701, 'No entity information passed to entries');
        }
        $this->entries = $entries;
        $this->teams = [];
        $this->_getCompInfo();
    }

    public function processEntries() {
        $this->_getAthletes();
        $this->_getExistingEntries();
        $this->_createEntries();
    }

    private function _sendEntryEmails($userIds) {
        $sql = '
            select user_email
            from ' . E4S_TABLE_USERS . '
            where id in (' . implode(',', $userIds) . ')
        ';
        $result = e4s_queryNoLog($sql);
        $emails = array();
        while ($obj = $result->fetch_object()) {
            $emails[] = $obj->user_email;
        }

        $newline = '<br>';

        $body = 'Dear Athlete,' . $newline . $newline;
        $body .= 'You have been selected and entered in to the following competition:' . $newline;
        $body .= $this->compObj->getFormattedName() . $newline . $newline;
        $body .= 'Please visit the online entry system and view your basket to complete the payment process and confirm your entries.' . $newline;
        $body .= '<a href="https://' . E4S_CURRENT_DOMAIN . '/#/cart-redirect">https://' . E4S_CURRENT_DOMAIN . '/Cart</a>';
        $body .= Entry4_emailFooter();

        $headers = Entry4_mailHeader('Support', FALSE);
        e4s_mail($emails, 'Entry to ' . $this->compObj->getDisplayName(), $body, $headers);
    }

    private function _createEntries() {
        $payeeIds = array();

        foreach ($this->entries as $entry) {
            $entered = FALSE;
            if (array_key_exists('urn', $entry)) {
                $athleteObj = $this->athletesByUrn[$entry['urn']];

                foreach ($athleteObj->entries as $egId => $athleteEntry) {
                    if ($egId === $entry['targetEgId']) {
                        $entered = TRUE;
                        break;
                    }
                }
                if (!$entered) {
                    $this->_createIndivEntry($entry);
                }
            }
            if (array_key_exists('team', $entry)) {
                if (array_key_exists($entry['targetEgId'], $this->teams)) {
                    $egTeams = $this->teams[$entry['targetEgId']];
                    if (array_key_exists($entry['team']['teamName'], $egTeams)) {
                        $entered = TRUE;
                    }
                }
                if (!$entered) {
                    $this->_createTeamEntry($entry);
                }
            }
            if (!$entered) {
                $payeeIds[$entry['ownerId']] = $entry['ownerId'];
            }
        }
        if (!empty($payeeIds)) {
            $this->_sendEntryEmails($payeeIds);
        }
    }

//    Teams do not give age group, so simply get the first ceId from the eg
    private function _getCeIdForTeamEg($entry) {
        $egId = $entry['targetEgId'];
        $ceObjs = $this->compObj->getCeObjs();
        foreach ($ceObjs as $ceId => $ceObj) {
            if ($ceObj->egId === $egId) {
                return $ceId;
            }
        }
        return 0;
    }

    private function _getTeamAthletes($entry) {
        $athletes = $entry['team']['athletes'];
        $teamAthletes = array();
        for ($p = 1; $p <= sizeof($athletes); $p++) {
            foreach ($athletes as $athlete) {
                if ($athlete['seqno'] === $p) {

                    $urn = $athlete['urn'];
                    if (!array_key_exists($urn, $this->athletesByUrn)) {
                        Entry4UIError(6870, 'Unable to find athlete for urn : ' . $urn);
                    }
                    $athleteObj = $this->athletesByUrn[$urn];
                    $arr = array();
                    $arr['id'] = $athleteObj->id;
                    $teamAthletes[] = $arr;
                    break;
                }
            }
        }
        return $teamAthletes;
    }

    private function _createTeamEntry($entry) {
        $ceId = $this->_getCeIdForTeamEg($entry);
        if ($ceId === 0) {
            Entry4UIError(6850, 'Unable to get event from the EventGroup');
        }
        $teamObj = array();
        $teamObj['forminfo'] = array();
        $teamObj['id'] = 0;
        $teamObj['eventteamid'] = 0;
        $teamObj['entitylevel'] = $this->entityLevel;
        $teamObj['entityid'] = $this->entityId;
        $teamObj['ceid'] = $ceId;
        $teamObj['compid'] = $this->compObj->getID();
        $teamObj['formno'] = 0;
        $teamObj['teamname'] = $entry['team']['teamName'];
        $teamObj['athletes'] = $this->_getTeamAthletes($entry);
        $teamObj['formrows'] = array();
        $teamObj['type'] = E4S_TEAM_TYPE_STD;
        $teamObj['process'] = 'C';
        $teamObj['userId'] = $entry['ownerId'];
        $teamObj['resultsKey'] = $entry['resultsKey'];
        $teamObj['autoEntry'] = true;
        include_once E4S_FULL_PATH . 'eventteams/eventTeamCRUD.php';
        return createEventTeam($teamObj, FALSE);
    }

    private function _createIndivEntry($entry) {
        $athleteObj = $this->athletesByUrn[$entry['urn']];
        $athleteAg = getAgeGroupInfo($this->allCompAges, $athleteObj->dob);
        $egId = $entry['targetEgId'];
        $ageGroupId = 0;
        if (array_key_exists('ageGroup', $athleteAg)) {
            $ag = $athleteAg['ageGroup'];
            if (array_key_exists('agid', $ag)) {
                $ageGroupId = $athleteAg['ageGroup']['agid'];
            }
        }
        if ($ageGroupId === 0) {
            Entry4UIError(6760, 'Unable to get age group for event : ' . $egId);
        }
        $ceId = 0;
        $ceObjs = $this->compObj->getCeObjs();
        foreach ($ceObjs as $ceObj) {
            if ($ceObj->egId === $egId) {
                if ($ceObj->ageGroupId === $ageGroupId and $ceObj->gender === $athleteObj->gender) {
                    $ceId = $ceObj->id;
                    break;
                }
            }
        }
        if ($ceId === 0) {
            Entry4UIError(6761, 'Unable to get CompEvent record for event:' . $egId . ' AgeGroupId:' . $ageGroupId . ' Gender:' . $athleteObj->gender );
        }
        $ceId = $ceId;
        $productObj = new stdClass();
        $productObj->ceId = $ceId;
        $productObj->athleteId = $athleteObj->id;
        $productObj->userId = $entry['ownerId'];
        $productObj->autoEntry = true;
        $productObj->resultsKey = $entry['resultsKey'];
        $productObj->perf = (float)$entry['result'];
        $entity = new stdClass();
        $entity->id = $this->entityId;
        $entity->level = $this->entityLevel;
        $productObj->entity = $entity;
        include_once E4S_FULL_PATH . 'product/createProduct.php';
        $data = e4s_createProduct($productObj, FALSE);
        return $data;
    }

    private function _getExistingEntries() {
        if ($this->compObj->hasIndivEvents()) {
            $this->_getExistingIndivEntries();
        }
        if ($this->compObj->hasTeamEvents()) {
            $this->_getExistingTeams();
        }
    }

    private function _getExistingTeams() {
        $sql = '
            select   e.ceId
                    ,e.id entryId
                    ,e.name teamName
                    ,e.paid
                    ,e.userId
                    ,e.entityLevel
                    ,e.entityId
                    ,ea.athleteId
                    ,ea.pos
                    ,a.urn
                    ,a.firstName
                    ,a.surName
                    ,a.gender
                    ,ce.maxGroup egId
            from ' . E4S_TABLE_EVENTTEAMENTRIES . ' e,
                 ' . E4S_TABLE_EVENTTEAMATHLETE . ' ea,
                 ' . E4S_TABLE_ATHLETE . ' a,
                 ' . E4S_TABLE_COMPEVENTS . ' ce
            where ce.id = e.ceId
            and ce.compid = ' . $this->compObj->getID() . "
            and e.entityLevel = $this->entityLevel
            and e.entityId = $this->entityId
            and ea.teamentryid = e.id
            and ea.athleteid = a.id
        ";
        $result = e4s_queryNoLog($sql);

        while ($obj = $result->fetch_object()) {
            $egId = (int)$obj->egId;
            if (!array_key_exists($egId, $this->teams)) {
                $this->teams[$egId] = [];
            }
            $egTeams = $this->teams[$egId];
            if (!array_key_exists($obj->teamName, $egTeams)) {
                $team = new stdClass();
                $team->name = $obj->teamName;
                $team->ceId = (int)$obj->ceId;
                $team->entryId = (int)$obj->entryId;
                $team->paid = (int)$obj->paid;
                $team->userId = (int)$obj->userId;
                $team->entityLevel = (int)$obj->entityLevel;
                $team->entityId = (int)$obj->entityId;
                $team->athletes = [];

                $egTeams[$obj->teamName] = $team;
            }
            $this->teams[$egId] = $egTeams;
            $team = $this->teams[$egId][$obj->teamName];
            $athletes = $team->athletes;
            $athlete = new stdClass();
            $athlete->id = (int)$obj->athleteId;
            $athlete->pos = (int)$obj->pos;
            $athlete->urn = (int)$obj->urn;
            $athlete->firstName = $obj->firstName;
            $athlete->surName = $obj->surName;
            $athletes[$athlete->id] = $athlete;
            $this->teams[$egId][$obj->teamName]->athletes = $athletes;
        }

    }

    private function _getExistingIndivEntries() {
        $sql = '
            select   e.compEventId ceId
                    ,e.id entryId
                    ,e.athleteId
                    ,e.paid
                    ,e.userId
                    ,ce.maxGroup egId
            from ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce
            where ce.id = e.compeventid
            and ce.compid = ' . $this->compObj->getID() . "
            and e.entity = '" . e4s_getEntityKey($this->entityLevel, $this->entityId) . "'
            and e.athleteid in (" . implode(',', $this->_getAthleteIds()) . ')
        ';
        $result = e4s_queryNoLog($sql);

        while ($obj = $result->fetch_object()) {
            $athleteId = (int)$obj->athleteId;
            $egId = (int)$obj->egId;
            $athleteObj = $this->athletesById[$athleteId];
            if (is_null($athleteObj)) {
                Entry4UIError(6720, 'Athlete not found : ' . $athleteId);
            }
            $entry = new stdClass();
            $entry->ceId = (int)$obj->ceId;
            $entry->egId = $egId;
            $entry->paid = (int)$obj->paid;
            $entry->userId = (int)$obj->userId;
            $athleteObj->entries[$egId] = $entry;
        }
    }

    private function _getAthletes() {
        $this->athletesById = [];
        $this->athletesByUrn = [];
        $urns = $this->_getURNs();

        $sql = 'select id
                        ,urn
                        ,dob
                        ,gender
                from ' . E4S_TABLE_ATHLETE . '
                where urn in (' . implode(',', $urns) . ')';
        $result = e4s_queryNoLog($sql);
        while ($obj = $result->fetch_object()) {
            $id = (int)$obj->id;
            $urn = (int)$obj->urn;
            $athleteObj = new stdClass();
            $athleteObj->id = $id;
            $athleteObj->urn = $urn;
            $athleteObj->gender = $obj->gender;
            $athleteObj->dob = $obj->dob;
            $athleteObj->entries = [];

            $this->athletesById[$id] = $athleteObj;
            $this->athletesByUrn[$urn] = $athleteObj;
        }
    }

    private function _getAthleteIds() {
        $ids = [];
        foreach ($this->athletesById as $id => $obj) {
            $ids[] = $id;
        }
        if (empty($ids)) {
            Entry4UIError(6720, 'No Athlete IDs found');
        }
        return $ids;
    }

    private function _getURNs() {
        $urns = [];
        foreach ($this->entries as $entry) {
            if (array_key_exists('urn', $entry)) {
                $urn = $entry['urn'];
                if (!in_array($urn, $urns)) {
                    $urns[] = $urn;
                }
            } else {
                $team = $entry['team'];
                $athletes = $team['athletes'];
                foreach ($athletes as $athlete) {
                    $urn = $athlete['urn'];
                    if (!in_array($urn, $urns)) {
                        $urns[] = $urn;
                    }
                }
            }
        }
        if (empty($urns)) {
            Entry4UIError(6710, 'No Athletes found');
        }
        return $urns;
    }

    private function _getCompInfo() {
        $egId = $this->entries[0]['targetEgId'];
        $sql = '
            select compId
            from ' . E4S_TABLE_EVENTGROUPS . '
            where id = ' . $egId;
        $result = e4s_queryNoLog($sql);
        while ($obj = $result->fetch_object()) {
            $this->compObj = e4s_getCompObj($obj->compId);
            $this->allCompAges = getAllCompDOBs($obj->compId);
            return;
        }
        Entry4UIError(4770, 'Failed to get comeptition for Entries');
    }
}

function e4s_getResultsForComp($compid) {
    $resultsObj = new resultsClass($compid);
    $results = $resultsObj->getResultsForComp();
    return $results;
}

function e4s_getResultsForEvent($obj) {

    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $eventNo = checkFieldForXSS($obj, 'eventno:Event number' . E4S_CHECKTYPE_NUMERIC);
    $heatNo = checkFieldForXSS($obj, 'heatno:Heat number' . E4S_CHECKTYPE_NUMERIC);

    $data = e4s_ReturnResultsForEvent($compId, $eventNo, $heatNo, TRUE);

    Entry4UISuccess('
        "data":' . json_encode($data->results, JSON_NUMERIC_CHECK) . ',
        "meta":' . json_encode($data->meta, JSON_NUMERIC_CHECK));
}

function e4s_ReturnResultsForEvent($compId, $eventNo, $heatNo) {
    $resultsObj = new resultsClass($compId);
    $results = $resultsObj->getResultsAndMetaForEvent($eventNo, $heatNo);
    return $results;
}

// Inbound results from a card / EDM
function e4s_postResults($obj) {
    $paramsKey = 'params';
    $currentTrialKey = 'currentTrial';
	$athleteKey = 'athlete';
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $clean = checkFieldForXSS($obj, 'clean:Clean Data');
    $updateSB = checkFieldForXSS($obj, 'scoreboard:Scoreboard');
    $egId = checkFieldForXSS($obj, 'egId:EventGroup ID');
	$positionArr = [];
	if ( array_key_exists('results', $_POST) ) {
		$resultsArr = $_POST['results'];
		if ( array_key_exists('positions', $_POST) ){
			$positionArr = $_POST['positions'];
		}
	}else{
		$resultsArr = $obj->get_params('JSON');
		if ( array_key_exists('results', $resultsArr) ){
			$resultsArr = $resultsArr['results'];
		}else{
			Entry4UIError(9755, 'Invalid Results passed for processing');
		}
	}

    $payload = $resultsArr;

    $athleteIds = array();
    $athleteKeys = array();
    $useEventNo = 0;
	$isHeightEvent = false;
    foreach ($resultsArr as $eventNo => $results) {
        if ( $useEventNo === 0 ){
            $useEventNo = $eventNo;
        }
        if ( $useEventNo !== $eventNo ){
            Entry4UIError(8950, 'Can not process multiple events as yet');
        }
        foreach ($results as $athleteId => $athleteResults) {
            if (!array_key_exists($eventNo, $athleteIds)) {
                $athleteIds[$eventNo] = array();
            }
            $athleteIds[$eventNo][] = $athleteId;
            foreach ($athleteResults as $resultKey => $resultValue) {
                if ($resultKey !== $paramsKey and $resultKey !== $currentTrialKey and $resultKey !== $athleteKey) {
                    if (!array_key_exists($eventNo, $athleteKeys)) {
                        $athleteKeys[$eventNo] = array();
                    }
					if ( $resultKey === "result"){
						$resultKey = $resultValue['height'] . ":" . $resultValue['trial'];
						$isHeightEvent = true;
					}
                    $athleteKeys[$eventNo][] = "'" . $athleteId . '-' . $resultKey . "'";
                }
            }
        }
    }

    if ( $useEventNo === 0 ){
        return;
    }
    $currentResults = e4s_getCardResultsForEvent($compId, $eventNo, $isHeightEvent);
    if ($clean) {
        // replace all card results
        $sql = '
            delete from ' . E4S_TABLE_CARDRESULTS . "
            where  compid = {$compId}
            and   eventno = {$useEventNo}
            and athleteid in (" . implode(',', $athleteIds) . ')';
        e4s_queryNoLog($sql);
    }
    $newResults = false;
    foreach ($athleteIds as $eventNo => $athleteId) {
        if (!$clean) {
            // clear only those results we have passed in
            $sql = '
                delete from ' . E4S_TABLE_CARDRESULTS . "
                where  compid = {$compId}
                and   eventno = {$eventNo}
                and concat(athleteid,'-',resultkey) in (" . implode(',', $athleteKeys[$eventNo]) . ')';
            e4s_queryNoLog($sql);
        }

        $values = array();
        $sbObj = new scoreboardClass($compId, $eventNo, $updateSB);

        foreach ($resultsArr[$eventNo] as $athleteId => $result) {
            foreach ($result as $resultkey => $resultvalue) {
                if ($resultkey !== '' and $resultvalue !== '') {
                    if ($resultkey !== $paramsKey and $resultkey !== $currentTrialKey and $resultkey !== '3pos' and $resultkey !== '6pos' and $resultkey !== $athleteKey) {
                        $attachment = e4s_getAttachmentFromResults($currentResults, $athleteId, $resultkey);
						if (gettype($resultvalue) === 'array'){
							// height result
							$resultkey= $resultvalue['height'] . ":" . $resultvalue['trial'];
							$resultvalue = $resultvalue['result'];
						}
						if ( $resultvalue !== '' ) {
							$resultvalue = preg_split('~:~', $resultvalue)[0];
							$values[] = "({$compId},{$athleteId},{$eventNo},'{$resultkey}','{$resultvalue}','{$attachment}')";
							$sbObj->addToUpdate( $athleteId, $resultkey, $resultvalue );
						}
                    }
                }
            }
        }
        if (!empty($values)) {
            $sql = '
                insert into ' . E4S_TABLE_CARDRESULTS . ' (compid, athleteid, eventno, resultkey, resultvalue, attachment)
                values ' . implode(',', $values);
            e4s_queryNoLog($sql);
            $newResults = true;
        }
        $sbObj->updateScoreboard();
    }
    if ( $newResults ) {
        // new results have been written
        e4s_writeCardResultsToMainResults($compId, $useEventNo, $isHeightEvent, $positionArr);
    }
    $payload['ranking'] = array();
    $compObj = e4s_getCompObj($compId);
    $compObj->setResultsAvailable(true);
    $egObjs = $compObj->getEGObjs();
    foreach($egObjs as $eventGroup){
        if ( $eventGroup->eventNo === $useEventNo){
			$egObj = new eventGroup($compId);
			$egObj->markResultsState($eventGroup);
            include_once E4S_FULL_PATH . 'results/webresults.php';
            $compResultObj = compResults::withEgId($eventGroup->id);
            $payload['ranking'] = $compResultObj->readEventResults();
            break;
        }
    }

    e4s_sendSocketInfo($compId, $payload, R4S_SOCKET_FIELD_RESULT);
    Entry4UISuccess();
}

function e4s_getAttachmentFromResults($results, $athleteId, $trial):string{
    foreach($results as $result){
        if ( $athleteId === $result->athleteId and $trial === $result->resultKey ){
            return $result->attachment;
        }
    }
    return '';
}
// Take the payload from a card/EDM result and write to main results table
function e4s_getCardResultsForEvent($compId, $eventNo,$isHeightEvent)
{
    $sql = '       
        select athleteId, 
               CAST(resultvalue AS decimal(5,2)) result,
               resultKey,
               attachment
        from ' . E4S_TABLE_CARDRESULTS . '
        where compid = ' . $compId . '
        and eventno = ' . $eventNo . '
        order by CAST(resultvalue AS decimal(5,2)) desc
    ';
	if ( $isHeightEvent ){
		$sql = '       
			select athleteId, 
				   resultvalue result,
				   resultKey,
				   attachment
			from ' . E4S_TABLE_CARDRESULTS . '
			where compid = ' . $compId . '
			and eventno = ' . $eventNo . '
			order by resultKey desc
		';
	}
    $results = e4s_queryNoLog($sql);
    $allResults = array();

    while ($obj = $results->fetch_object()) {
        $obj->athleteId = (int)$obj->athleteId;
		if ( $isHeightEvent ) {
			$obj->result = $obj->result;
		}else{
			$obj->result = (float) $obj->result;
		}
        $obj->eventNo = $eventNo;
        $obj->compId = $compId;
        $obj->resultKey = $obj->resultKey;
        $obj->attachment = $obj->attachment;

        $allResults[] = $obj;

    }
    return $allResults;
}
function e4s_getBestResults($cardResults){
    $bestResults = array();
    $athletes = array();
    $pos = 1;
    foreach($cardResults as $cardResult){
		if ( str_contains($cardResult->resultKey,":") ){
			if ( $cardResult->result !== E4S_FIELDCARD_SUCCESS ) {
				continue;
			}
		}
        if (!array_key_exists($cardResult->athleteId, $athletes)) {
            $cardResult->pos = $pos++;
            $bestResults[] = $cardResult;
            $athletes[$cardResult->athleteId] = true;
        }
    }
    return $bestResults;
}
function e4s_writeCardResultsToMainResults($compId, $eventNo, $isHeightEvent, $positionArr) {
    $currentResults = e4s_getCardResultsForEvent($compId, $eventNo,$isHeightEvent);
    $currentResults = e4s_getBestResults($currentResults);

    include_once E4S_FULL_PATH . 'results/webresults.php';
    $resultsObj = compResults::withEventNo($compId, $eventNo);
    $resultsObj->writeCardResultsToMainResults($currentResults, $positionArr);
}
function e4s_clearScoreboard($compid, $eventno) {
    $sbObj = new scoreboardClass($compid, $eventno, TRUE);
    $sbObj->deleteScoreboard();
    $sbObj->get();
}

function e4s_getScoreboardEventsForArray($obj) {
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $sbObj = new scoreboardClass($compid, -1, FALSE);
    $sbObj->getForEvents($obj);
}

function e4s_getScoreboardForBoard($compid, $eventno) {
    $sbObj = new scoreboardClass($compid, $eventno, TRUE);
    $sbObj->get();
}

function e4s_getAllForComp($compid) {
    $sbObj = new scoreboardClass($compid, 0, FALSE);
    $sbObj->getAll();
}
function e4s_timetronicsTrackResults($compid) {
    e4s_inboundTrackResults($compid, E4S_PF_TT);
}
function e4s_lynxTrackResults($compid) {
    e4s_inboundTrackResults($compid, E4S_PF_LYNX);
}
function e4s_inboundTrackResults($compid, $format) {
    $pfObj = new e4sPF($compid, $format);

    foreach ($_POST as $post => $value) {
        $pfObj->processResults($post);
    }
    Entry4UISuccess('');
}

class e4sPF {
    public $compid;
    public $eventno;
    public $headerInfo;
    public $resultLines = array();
    public $format;
    public $sep;

    public function __construct($compid, $format) {
        $this->compid = $compid;
        $this->format = $format;
        $this->sep = "\t";
        if ($format === E4S_PF_LYNX) {
            $this->sep = ',';
        }
    }

    public function processResults($resultsContent) {
        $firstDataLine = 1;
        if ($this->format === E4S_PF_TT) {
            $firstDataLine = 2;
        }
        $resultLines = explode("\n", $resultsContent);
        foreach ($resultLines as $sub => $data) {
            if ($sub === 0) {
                $this->processHeader($data);
            }
            // TT : ignore the 2nd line as its title data
            if ($sub >= $firstDataLine and $data !== '') {
//                e4s_dump($data,"Line",false,true,true);
                $this->processResultLine($data);
            }
        }

        if (!empty($this->resultLines)) {
            $this->updateDB();
        }
    }

    public function processHeader($data) {
        $headerLine = explode($this->sep, $data);

        $headerInfo = new stdClass();
        $headerInfo->eventNo = $this->getEventNo($headerLine[0]);
        $headerInfo->heatNo = $this->getHeatNo($headerLine[0]);
        $headerInfo->ws = $this->getWS($headerLine[1]);
        $headerInfo->dateStr = $this->getTimeOfEvent($headerLine[4]);
        $this->headerInfo = $headerInfo;
    }

    public function getEventNo($data) {
        $eventNo = 0;
        if ($this->format === E4S_PF_TT) {
            $str = explode('h', $data)[0];
            $eventNo = (int)str_replace('E', '', $str);
        }
        if ($this->format === E4S_PF_LYNX) {
            $arr = explode($this->sep, $data);
            $eventNo = $arr[0];
        }
        return $eventNo;
    }

    public function getHeatNo($data) {
        $heatNo = 0;
        if ($this->format === E4S_PF_TT) {
            $str = explode(',', $data);
            $heatNo = (int)explode('h', $str[0])[1];
        }
        if ($this->format === E4S_PF_LYNX) {
            $arr = explode($this->sep, $data);
            $heatNo = $arr[2];
        }

        return $heatNo;
    }

    public function getWS($data) {
        $ws = 'N/A';
        if ($this->format === E4S_PF_TT) {
            if (strpos($data, ':') !== FALSE) {
                $ws = str_replace('__', '_', $data);
                $ws = explode(':_', $ws)[1];
                $ws = str_replace('_m', ' m', $ws);
                $ws = str_replace('_', '.', $ws);
            }
        }
        if ($this->format === E4S_PF_LYNX) {
            $ws = '-';
        }
        return $ws;
    }

    public function getTimeOfEvent($data) {
        $time = '-';
        if ($this->format === E4S_PF_TT) {
            $time = str_replace('_-_', ' ', $data);
        }
        if ($this->format === E4S_PF_LYNX) {
            $arr = explode($this->sep, $data);
            $time = $arr[10];
        }
        return $time;
    }

    public function processResultLine($data) {
        $resultLine = explode($this->sep, $data);
//        1,491,8,Brier,Joe,Swansea,46.55,Men's 400m,46.55,,,13:46:04.40,,,,46.55,46.55,,
// place,bib, lane, last,first,club,time
        $obj = new stdClass();
        if ($this->format === E4S_PF_TT) {
            $obj->lane = $resultLine[1];
            $obj->time = $this->getTime($resultLine[2]);
            $obj->bibNo = $resultLine[3];
        }
        if ($this->format === E4S_PF_LYNX) {
            $obj->lane = $resultLine[2];
            $obj->time = $this->getTime($resultLine[6]);
            $obj->bibNo = $resultLine[1];
        }
        $this->resultLines[] = $obj;
    }

    public function getTime($data) {
        // remove first _
        if ($data[0] === '_') {
            $data = substr($data, 1);
        }
        $data = str_replace('_', '.', $data);
        $data = str_replace(':', '.', $data);

        $time = explode('.', $data);
        if (count($time) === 1) {
            return $data;
        }
        if (count($time) < 3) {
            return '' . $data;
        }
        $calcTime = (int)$time[0] * 60;
        $calcTime += (int)$time[1];

        return ('-' . $calcTime . '.' . $time[2]);
    }

    public function updateDB() {

        $clean = 'delete from ' . E4S_TABLE_CARDRESULTS . '
                 where compid = ' . $this->compid . ' 
                 and eventno = ' . $this->headerInfo->eventNo . "
                 and resultkey = 'h" . $this->headerInfo->heatNo . "'";

        e4s_queryNoLog($clean);

        $insert = 'Insert into ' . E4S_TABLE_CARDRESULTS . ' (`compid`, `athleteid`, `eventno`, `resultkey`, `resultvalue`) values ';
        $values = '';
        $sep = '';
        foreach ($this->resultLines as $resultLine) {
            $athleteid = $this->getAthleteID($resultLine->bibNo);
            if ($athleteid !== 0) {
                $values .= $sep . '(
                ' . $this->compid . ',
                ' . $athleteid . ',
                ' . $this->headerInfo->eventNo . ",
                'h" . $this->headerInfo->heatNo . "',
                '" . $resultLine->time . "'
                )";
                $sep = ',';
            }
        }
        if ($values !== '') {
//                    e4s_dump($insert . $values,"insert",true,true,true);
            e4s_queryNoLog($insert . $values);
        }

    }

    public function getAthleteID($bibno) {
        $sql = 'select athleteid athleteid
                from ' . E4S_TABLE_BIBNO . '
                where compid = ' . $this->compid . " 
                and bibno = {$bibno}";

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            $row = $result->fetch_assoc();
            return (int)$row['athleteid'];
        }
        return 0;
    }
}

/* Mock Data
{
  "entity":{
    "id":9,
    "level":3
  },
  "entries": [
  {
    "targetEgId": 5320,
    "urn": 117332,
    "ownerId": 551,
    "result":"4:22.23",
    "resultsKey":"264:1"
  },{
    "targetEgId": 5320,
    "urn": 192378,
    "ownerId": 2000,
    "result":"4:26.43",
    "resultsKey":"264:1"
  },{
    "targetEgId": 5326,
    "team": {
      "teamName":"Dundrum South Duiblin A.C. (A)",
      "name": "Dundrum South Dublin A.C.",
      "athletes":[
        {
          "urn":176657,
          "seqno":1
        },
        {
          "urn":179678,
          "seqno":2
        },
        {
          "urn":220550,
          "seqno":3
        },
        {
          "urn":154788,
          "seqno":4
        }
      ]
    },
    "ownerId": 2000,
    "result":"4:26.43",
    "resultsKey":"264:1"
  }
]
}
*/