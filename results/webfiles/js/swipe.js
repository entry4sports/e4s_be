// Detect swipe left and right motion on touchscreen.  Trigger next / previous heat navigation.
var start = null;
window.addEventListener("touchstart", function (event) {
    if (event.touches.length === 1) {
        //just one finger touched
        start = event.touches.item(0).clientX;
    } else {
        //a second finger hit the screen, abort the touch
        start = null;
    }
});
window.addEventListener("touchend", function (event) {
    var offset = 100;//at least 100px are a swipe
    if (start) {
        //the only finger that hit the screen left it
        var end = event.changedTouches.item(0).clientX;

        if (end > start + offset) {
            //a left -> right swipe
            console.log("left to right swipe detected");
            try {
                var previousURL = document.getElementsByClassName('previous')[0].getAttribute("href");
            } catch (err) {
                previousURL = "#";
            }
            window.location.href = previousURL;
        }
        if (end < start - offset) {
            //a right -> left swipe
            console.log("right to left swipe detected");
            try {
                var nextURL = document.getElementsByClassName('next')[0].getAttribute("href");
            } catch (err) {
                nextURL = "#";
            }
            window.location.href = nextURL;
        }
    }
});