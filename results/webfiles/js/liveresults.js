// Load routine for schedule page
function LoadSchedule() {
    jQuery.each(jQuery(".plannedtime"), function (x, value) {
        if (jQuery(value).html() === "") {
            jQuery(value).parent().css("display", "none");
        }
    });
    addMenuOption1();
    addMenuOption2();
    addMenuOption3();
    addMenuOption4();
    hide_headertitle();
    createHeaderLinks();
    addHeaderButton1();
    addHeaderButton2();
    addFooterButton1();
    addFooterButton2();
    setScheduleRefresh();
    //setTheme();
}

function showPFImage() {
    $("#pfImage").css("width", window.screen.availWidth / 2);
    $("#pfImage").css("height", window.screen.availHeight / 2);
    $("#image_dialog").dialog({
        width: "auto",
        height: "auto",
        modal: true,
        position: {my: "center", at: "center", of: window},
        show: {effect: 'scale', speed: 1000},
        hide: 'slide'
    });
}

// Load routine for track event page (start list and results)
function LoadRace() {
    addMenuOption1();
    addMenuOption2();
    addMenuOption3();
    addMenuOption4();
    hide_headertitle();
    createRaceTitle();
    addHeaderButton1();
    addHeaderButton2();
    imageLink();
    getPreviousHeatLink();
    getNextHeatLink();
    getWS();
    HideColumns();
    addFooterButton1();
    addFooterButton2();
    setColumnHeaders();
    setRaceRefresh();
    //setTheme();
}

// Load routine for field event page (start list and results)
function LoadField() {
    addMenuOption1();
    addMenuOption2();
    addMenuOption3();
    addMenuOption4();
    hide_headertitle();
    createRaceTitle();
    getPreviousHeatLink();
    getNextHeatLink();
    getWS();
    HideColumns();
    addFooterButton1();
    addFooterButton2();
    setFieldRefresh();
    //setTheme();
}

// Load routine for event page before start list or results exist (used for both track and field)
function LoadNoResult() {
    addMenuOption1();
    addMenuOption2();
    addMenuOption3();
    addMenuOption4();
    getHeaderTitle();
    hide_headertitle();
    createRaceTitle();
    addFooterButton1();
    addFooterButton2();
    setRaceRefresh();
    //setTheme();
}

function setColumnHeaders() {
    var x = jQuery(".lane");
    if (x.length === 0) {
        // Field
        jQuery("#col1").html("Bib No");
        jQuery("#col2").html("Athlete");
        jQuery("#col3").html("Club");
        jQuery("#col4").html("Result");
        jQuery("#col5").html("Position");
        jQuery("#col6").html("Pof10");
        jQuery(".attempt1").css("display", "none");
        jQuery(".attempt2").css("display", "none");
        jQuery(".attempt3").css("display", "none");
        jQuery(".attempt4").css("display", "none");
        jQuery(".attempt5").css("display", "none");
        jQuery(".attempt6").css("display", "none");
    } else {
        jQuery("#col1").html("Position");
        jQuery("#col2").html("Lane");
        jQuery("#col3").html("Bib No");
        jQuery("#col4").html("Athlete");
        jQuery("#col5").html("Club");
        jQuery("#col6").html("Time");
        jQuery("#col7").html("Pof10");
    }
}

// Load routine for Meeting Directory page
function LoadHeaderFooter() {
    //setTheme();
    addMenuOption1();
    addMenuOption2();
    addMenuOption3();
    addMenuOption4();
    hide_headertitle();
    addFooterButton1();
    addFooterButton2();
}

function addMenuOption1() {
    if (switch_dropdownmenu1) {
        var para = document.createElement("a");
        var node = document.createTextNode(dropdownmenulabel1);
        para.appendChild(node);
        para.classList.add("w3-bar-item");
        para.classList.add("w3-button");
        para.classList.add("w3-text-black");
        para.href = dropdownmenulink1;
        para.target = "_blank";
        var element = document.getElementById("dropdownmenu");
        element.appendChild(para);
    }
}

function addMenuOption2() {
    if (switch_dropdownmenu2) {
        var para = document.createElement("a");
        var node = document.createTextNode(dropdownmenulabel2);
        para.appendChild(node);
        para.classList.add("w3-bar-item");
        para.classList.add("w3-button");
        para.classList.add("w3-text-black");
        para.href = dropdownmenulink2;
        para.target = "_blank";
        var element = document.getElementById("dropdownmenu");
        element.appendChild(para);
    }
}

function addMenuOption3() {
    if (switch_dropdownmenu3) {
        var para = document.createElement("a");
        var node = document.createTextNode(dropdownmenulabel3);
        para.appendChild(node);
        para.classList.add("w3-bar-item");
        para.classList.add("w3-button");
        para.classList.add("w3-text-black");
        para.href = dropdownmenulink3;
        para.target = "_blank";
        var element = document.getElementById("dropdownmenu");
        element.appendChild(para);
    }
}

function addMenuOption4() {
    if (switch_dropdownmenu4) {
        var para = document.createElement("a");
        var node = document.createTextNode(dropdownmenulabel4);
        para.appendChild(node);
        para.classList.add("w3-bar-item");
        para.classList.add("w3-button");
        para.classList.add("w3-text-black");
        para.href = dropdownmenulink4;
        para.target = "_blank";
        var element = document.getElementById("dropdownmenu");
        element.appendChild(para);
    }
}

function addHeaderButton1() {
    if (switch_headerbutton1) {
        var para = document.createElement("a");
        var node = document.createTextNode(headerbuttonlabel1);
        para.appendChild(node);
        para.classList.add("w3-bar-item");
        para.classList.add("w3-button");
        para.classList.add("w3-right");
        para.classList.add("w3-theme");
        para.href = headerbuttonlink1;
        var element = document.getElementById("headerbar");
        element.appendChild(para);
    }
}

function addHeaderButton2() {
    if (switch_headerbutton2) {
        var para = document.createElement("a");
        var node = document.createTextNode(headerbuttonlabel2);
        para.appendChild(node);
        para.classList.add("w3-bar-item");
        para.classList.add("w3-button");
        para.classList.add("w3-right");
        para.classList.add("w3-theme");
        para.href = headerbuttonlink2;
        var element = document.getElementById("headerbar");
        element.appendChild(para);
    }
}

// Create shortcuts in page header (as per Meeting Manager 'Days').  Maximum 3 days assumed.
function createHeaderLinks() {
    if (switch_MMheaderlinks) {
        var stringA = document.getElementById("toplinks").innerHTML;
        var posA = stringA.indexOf("#Day1");
        if (posA > 0) {
            var stringB = stringA.slice(posA + 7);
            var posB = stringB.indexOf("<");
            var buttontext = stringB.slice(0, posB);
            var para = document.createElement("a");
            var node = document.createTextNode(buttontext);
            para.appendChild(node);
            para.classList.add("w3-bar-item");
            para.classList.add("w3-button");
            para.classList.add("w3-right");
            para.classList.add("w3-theme");
            para.href = "#Day1";
            var element = document.getElementById("headerbar");
            element.appendChild(para);
        }

        var posA = stringA.indexOf("#Day2");
        if (posA > 0) {
            var stringB = stringA.slice(posA + 7);
            var posB = stringB.indexOf("<");
            var buttontext = stringB.slice(0, posB);
            var para = document.createElement("a");
            var node = document.createTextNode(buttontext);
            para.appendChild(node);
            para.classList.add("w3-bar-item");
            para.classList.add("w3-button");
            para.classList.add("w3-right");
            para.classList.add("w3-theme");
            para.href = "#Day2";
            var element = document.getElementById("headerbar");
            element.appendChild(para)
        }

        var posA = stringA.indexOf("#Day3");
        if (posA > 0) {
            var stringB = stringA.slice(posA + 7);
            var posB = stringB.indexOf("<");
            var buttontext = stringB.slice(0, posB);
            var para = document.createElement("a");
            var node = document.createTextNode(buttontext);
            para.appendChild(node);
            para.classList.add("w3-bar-item");
            para.classList.add("w3-button");
            para.classList.add("w3-right");
            para.classList.add("w3-theme");
            para.href = "#Day3";
            var element = document.getElementById("headerbar");
            element.appendChild(para)
        }
    }
}

function addFooterButton1() {
    if (switch_footerbutton1) {
        var para = document.createElement("a");
        var node = document.createTextNode(footerbuttonlabel1);
        para.appendChild(node);
        para.classList.add("w3-bar-item");
        para.classList.add("w3-button");
        para.classList.add("w3-left");
        para.classList.add("w3-theme");
        para.href = footerbuttonlink1;
        para.target = "_blank";
        var element = document.getElementById("footerbar");
        element.appendChild(para);
    }
}

function addFooterButton2() {
    if (switch_footerbutton2) {
        var para = document.createElement("a");
        var node = document.createTextNode(footerbuttonlabel2);
        para.appendChild(node);
        para.classList.add("w3-bar-item");
        para.classList.add("w3-button");
        para.classList.add("w3-right");
        para.classList.add("w3-theme");
        para.href = footerbuttonlink2;
        para.target = "_blank";
        var element = document.getElementById("footerbar");
        element.appendChild(para);
    }
}

// Create race title - copy from original and strip out the 'official result' text on the end
// Remove everything beyond the ;
function createRaceTitle() {
    var str = document.getElementById("racetitle").innerHTML;
    var n = str.length;
    var posA = str.indexOf("W=");
    var posB = str.indexOf(";");
    if (posA > 0) {
        var x = posA - 2;
    } else {
        if (posB > 0) {
            var x = posB - 1;
        } else {
            var x = n;
        }
    }
    var shorttitle = str.slice(4, x).replace(" :", ":").replace(" .", ".");
    console.log("shorttitle: " + shorttitle);
    document.getElementById("racetitleshort").innerHTML = shorttitle;
}

// Get image URL and assign to button
function imageLink() {
    try {
        var imageURL = document.getElementsByClassName('photofinish')[0].getAttribute("href");
    } catch (err) {
        document.getElementById("pfURL").classList.add("w3-disabled");
        imageURL = "#"
    } finally {
        document.getElementById('pfURL').href = imageURL;
        if (imageURL === "") {
            document.getElementById('pfURL').style = 'visibility:hidden;';
        }
    }
}

// Get URL for previous heat and assign to button
function getPreviousHeatLink() {
    try {
        var previousURL = document.getElementsByClassName('previous')[0].getAttribute("href");
    } catch (err) {
        document.getElementById("previousheat").classList.add("w3-disabled");
        previousURL = "#";
    } finally {
        document.getElementById('previousheat').href = previousURL;
        if (previousURL === "") {
            document.getElementById('previousheat').style = 'visibility:hidden;';
        }
    }
}

// Get URL for next heat and assign to button
function getNextHeatLink() {

    try {
        var nextURL = document.getElementsByClassName('next')[0].getAttribute("href");
    } catch (err) {
        document.getElementById("nextheat").classList.add("w3-disabled");
        nextURL = "#";
    } finally {
        document.getElementById('nextheat').href = nextURL;
        if (nextURL === "") {
            document.getElementById('nextheat').style = 'visibility:hidden;';
        }
    }
}

function windspeedhide() {
    var WSval = document.getElementById('WSval').innerHTML;
    console.log("WSval: " + WSval);
    if (WSval == " ") {
        /* Leave the button there, but disable it */
        document.getElementById('WS').classList.add("w3-disabled");
        /* Remove the button */
        var elem = document.getElementById("WS");
        elem.parentElement.removeChild(elem);
    }
}

// Get windspeed reading from race header
function getWS() {
    var str = document.getElementById("racetitle").innerHTML;
    var posA = str.indexOf("W=");
    var posB = str.indexOf(";");
    if (posA > 0) {
        var x = posA + 2;
        var y = posB - 1;
        var WS = str.slice(x, y);
        document.getElementById("WS").innerHTML = "<i class=\"fa fa-flag w3-text-dark-grey w3-large\"></i><b> </b><b id=\"z-hide\">Windspeed: </b>".concat(WS);
    }
}

// Hide Result and Rank columns for participant lists.  Hide lane for result lists.
function HideColumns() {
    var resultstest = document.getElementsByClassName("result")[1].innerHTML;
    console.log("resultstest: " + resultstest);
    if (resultstest == "") {
        var fileref = document.createElement("link");
        fileref.setAttribute("rel", "stylesheet");
        fileref.setAttribute("type", "text/css");
        fileref.setAttribute("href", "css/hideresult.css");
        document.getElementsByTagName("head")[0].appendChild(fileref);
    } else if (resultstest == "...") {
        var fileref = document.createElement("link");
        fileref.setAttribute("rel", "stylesheet");
        fileref.setAttribute("type", "text/css");
        fileref.setAttribute("href", "css/hideresult.css");
        document.getElementsByTagName("head")[0].appendChild(fileref);
    } else {
        var fileref = document.createElement("link");
        fileref.setAttribute("rel", "stylesheet");
        fileref.setAttribute("type", "text/css");
        fileref.setAttribute("href", "css/hidelane.css");
        document.getElementsByTagName("head")[0].appendChild(fileref);
    }
}

// Get header title (if blank).   (Header title is blank on the standard NoData template)
function getHeaderTitle() {
    var str = document.getElementById("headertitle").innerHTML;
    var n = str.length;
    if (n = 1) {
        document.getElementById("headertitle").innerHTML = meet;
    }
}

// Hide header title (to be used if headerimage contains embedded title).
// If header title is displayed, adjust the width as per settings.js
function hide_headertitle() {
    if (switch_headertitle) {
    } else {
        document.getElementById('headertitle').classList.remove("z3-hide");
        document.getElementById('headertitle').style.width = headertitlewidth;
    }
}

// Set theme (colour).  See x for list of available theme colours.
function setTheme() {
    var themeURL = "https://www.w3schools.com/lib/w3-theme-".concat(theme).concat(".css");
    console.log(themeURL);
    var fileref = document.createElement("link");
    fileref.setAttribute("rel", "stylesheet");
    fileref.setAttribute("type", "text/css");
    fileref.setAttribute("href", themeURL);
    document.getElementsByTagName("head")[0].appendChild(fileref);
}

// Set schedule screen refresh time
function setScheduleRefresh() {
    if (schedulerefreshtime > 0) {
        var metaref = document.createElement("meta");
        metaref.setAttribute("http-equiv", "refresh");
        metaref.setAttribute("content", schedulerefreshtime);
        document.getElementsByTagName("head")[0].appendChild(metaref);
    }

}

// Set race screen refresh time
function setRaceRefresh() {
    if (racerefreshtime > 0) {
        var metaref = document.createElement("meta");
        metaref.setAttribute("http-equiv", "refresh");
        metaref.setAttribute("content", racerefreshtime);
        document.getElementsByTagName("head")[0].appendChild(metaref);
    }
}

// Set race screen refresh time
function setFieldRefresh() {
    if (fieldrefreshtime > 0) {
        var metaref = document.createElement("meta");
        metaref.setAttribute("http-equiv", "refresh");
        metaref.setAttribute("content", fieldrefreshtime);
        document.getElementsByTagName("head")[0].appendChild(metaref);
    }
}