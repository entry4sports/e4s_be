<html>
<head>
    <title>Team Results</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
    <style>
        th {
            text-align: left;
        }
        tr:nth-child(even) {
            background-color: #eee;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
	<script>
        let compInfo = <?php echo json_encode($this->getCompInfo()); ?>;
        let events = <?php echo json_encode($this->eventObj->getOutputObj()); ?>;
        let ageGroups = <?php echo json_encode($this->eventObj->ageGroups); ?>;
		let results = <?php echo json_encode($this->resultsObj->results->data); ?>;
		let countyInfo = <?php echo json_encode($this->clubCompInfo); ?>;
        let categoryCounts = <?php echo json_encode($this->categoryCounts); ?>;
        let athleteStdPoints = {};
        let teamPoints = [];

        const E4S_ENSURE_STRING = '<?php echo E4S_ENSURE_STRING ?>';
        const E4S_TYPE_FIELD = '<?php echo E4S_EVENT_FIELD ?>';
        const E4S_TYPE_TRACK = '<?php echo E4S_EVENT_TRACK ?>';
        const E4S_GENDER_FEMALE = '<?php echo E4S_GENDER_FEMALE ?>';
        const E4S_GENDER_MALE = '<?php echo E4S_GENDER_MALE ?>';
        const E4S_GENDER_OPEN = '<?php echo E4S_GENDER_OPEN ?>';
        const POINTS_TO_POSITION = 8;

        // after Document has loaded
        $(function () {
            calcAllResults();
            $( "#tabs" ).tabs();
            outputPositionResults();
            outputRawResults();
            <?php
                if ( isUserLoggedIn() ){
            ?>
                    populateLists();
            <?php
                }
            ?>
        });

        function populateLists(){
            let teamSelectObj = $('#teamSelect');
            let teams = [];
            let option;
            for(let r in countyInfo){
                teams.push(r);
            }
            teams.sort();
            for(let t of teams){
                option = $('<option>');
                option.text(t);
                teamSelectObj.append(option);
            }
            teamSelectObj.change(function(){
                applyFilters();
            });
            let genderSelectObj = $('#genderSelect');
            genderSelectObj.append(new Option('Male','<?php echo E4S_GENDER_MALE ?>'));
            genderSelectObj.append(new Option('Female','<?php echo E4S_GENDER_FEMALE ?>'));
            genderSelectObj.append(new Option('Mixed','<?php echo E4S_GENDER_OPEN ?>'));

            genderSelectObj.change(function(){
                applyFilters();
            });
            let ageSelectObj = $('#ageSelect');
            for(let a in ageGroups){
                let ageGroup = ageGroups[a];
                if ( ageGroup.indexOf('ES ') > -1) {
                    ageSelectObj.append(new Option(ageGroup, a));
                }
            }
            ageSelectObj.change(function(){
                applyFilters();
            });
        }
        function clearFilters(){
            $('#eventFilter').val('');
            $('#textFilter').val('');
            $('#teamSelect').val('');
            $('#ageSelect').val('');
            $('#genderSelect').val('');
            applyFilters();
        }
        function showAll(){
            // show all
            $("[gender]").show();
            $("[resultrow]").show();
        }
        function applyFilters(){
            let eventText = $('#eventFilter').val();
            let text = $('#textFilter').val();
            let team = $('#teamSelect').val();
            let age= $('#ageSelect').val();
            let gender= $('#genderSelect').val();

            showAll();

            if ( eventText !== ''){
                filterEvents(eventText);
            }

            if ( text !== ''){
                filterText(text);
            }
            // filter county
            if ( team !== '' ) {
                filterTeam(team);
            }

            // filter genders
            if ( gender !== "" ) {
                filterGender(gender);
            }

            // filter ages
            if ( age !== "") {
                filterAge(age);
            }
            hideBlankHeaders();
        }
        function hideBlankHeaders(){
            for(let e in events){
                let show = $("#" + e + " tr:visible").length > 2;
                if ( !show ){
                    $("#" + e).hide();
                }
            }
        }
        function filterEvents(text){
            $("[event]").hide();
            $("[event*='" + text + "']").show();
        }
        function filterText(text){
            let resultRows = $("[resultrow]");
            text = text.toLowerCase();

            resultRows.each(function (index) {
                let obj = $(this);
                let allText = obj.text().toLowerCase();
                if (allText.indexOf(text) === -1) {
                    obj.hide();
                }
            })
        }
        function filterTeam(team){
            let resultRows = $("[resultrow]");
            team = team.toLowerCase();

            if ( team !== '') {
                resultRows.each(function (index) {
                    let obj = $(this);
                    let html = obj.html().split("<td>");
                    let text = html[3].toLowerCase();
                    if (text.indexOf(team) === -1) {
                        obj.hide();
                    }
                })
            }
        }
        function filterGender(gender){
            gender = gender[0];
            if ( gender === "<?php echo E4S_GENDER_FEMALE ?>" ){
                $("[gender=<?php echo E4S_GENDER_MALE ?>]").hide();
                //$("[gender=<?php //echo E4S_GENDER_OPEN ?>//]").hide();
            }else if ( gender === "<?php echo E4S_GENDER_MALE ?>" ){
                $("[gender=<?php echo E4S_GENDER_FEMALE ?>]").hide();
                //$("[gender=<?php //echo E4S_GENDER_OPEN ?>//]").hide();
            }else if ( gender === "<?php echo E4S_GENDER_OPEN ?>" ){
                $("[gender=<?php echo E4S_GENDER_MALE ?>]").hide();
                $("[gender=<?php echo E4S_GENDER_FEMALE ?>]").hide();
            }
        }
        function filterAge(age){
            for(let a in ageGroups){
                if (a !== age ){
                    $("[age=" + a + "]").hide();
                }
            }
        }
        function calcAllResults(){
            for (let egId in results){
                calcEGResults(egId);
            }
            addToTeamPoints();
        }

        const defaultTeamPointsObj = {
            points: 0,
            femalePoints: {
                0:0,
                213:0,
                214:0,
                215:0
            },
            malePoints: {
                0:0,
                213:0,
                214:0,
                215:0
            }
        };

        function addToTeamPoints(){
            teamPoints = [];
            let result;
            for(let r in results){
                let egResults = results[r];
                for(let er in egResults) {
                    try {
                        result = egResults[er];
                        let countyObj = countyInfo[result.county.toUpperCase()];
                        let countyName = countyObj.county.name;
                        let category = countyObj.category.name;
                        if ( category.toUpperCase().indexOf('GUEST') > -1 ){
                            continue;
                        }
                        let event = events[result.egId];
                        let athleteId = (event.isTeamEvent ? 'T' : '') + result.athleteId;

                        let athleteStandardPoints = 0;
                        if (athleteStdPoints[event.ceObjs[0].ageGroupId]) {
                            if (athleteStdPoints[event.ceObjs[0].ageGroupId][event.eventDef.id]) {
                                if (athleteStdPoints[event.ceObjs[0].ageGroupId][event.eventDef.id][athleteId]) {
                                    athleteStandardPoints = athleteStdPoints[event.ceObjs[0].ageGroupId][event.eventDef.id][athleteId].points;
                                    // now clear athlete Standard Points as can not be added twice
                                    athleteStdPoints[event.ceObjs[0].ageGroupId][event.eventDef.id][athleteId].points = 0;
                                }
                            }
                        }

                        if (!teamPoints.hasOwnProperty(category)) {
                            teamPoints[category] = [];
                        }
                        if (!teamPoints[category].hasOwnProperty(countyName)) {
                            teamPoints[category][countyName] = JSON.parse(JSON.stringify(defaultTeamPointsObj));
                        }
                        let points = egResults[er].positionPoints + athleteStandardPoints;

                        teamPoints[category][countyName].points += points;

                        for (let ce in event.ceObjs) {
                            let ceObj = event.ceObjs[ce];
                            if ( !teamPoints[category][countyName].femalePoints.hasOwnProperty(0) ){
                                teamPoints[category][countyName].femalePoints[0] = 0;
                            }
                            if ( !teamPoints[category][countyName].malePoints.hasOwnProperty(0) ){
                                teamPoints[category][countyName].malePoints[0] = 0;
                            }

                            if (ceObj.gender === E4S_GENDER_OPEN && points > 0 ){
                                points = points * 0.5; // split points between genders
                            }
                            if (ceObj.gender !== E4S_GENDER_MALE) {
                                teamPoints[category][countyName].femalePoints[0] += points;

                                if ( !teamPoints[category][countyName].femalePoints.hasOwnProperty(ceObj.ageGroupId) ){
                                    teamPoints[category][countyName].femalePoints[ceObj.ageGroupId] = 0;
                                }
                                teamPoints[category][countyName].femalePoints[ceObj.ageGroupId] += points;
                            }
                            if (ceObj.gender !== E4S_GENDER_FEMALE) {
                                teamPoints[category][countyName].malePoints[0] += points;

                                if ( !teamPoints[category][countyName].malePoints.hasOwnProperty(ceObj.ageGroupId) ){
                                    teamPoints[category][countyName].malePoints[ceObj.ageGroupId] = 0;
                                }
                                teamPoints[category][countyName].malePoints[ceObj.ageGroupId] += points;
                            }
                            break; // should only be 1 ceObj
                        }
                        if ( teamPoints[category][countyName].points !== teamPoints[category][countyName].femalePoints[0] + teamPoints[category][countyName].malePoints[0] ){
                            console.log('Points do not add up');
                            debugger;
                        }
                    } catch(e) {
                        console.log(e);
                        debugger;
                    }
                }
            }
        }

        function calcEGResults(egId){
            let event = events[egId];
            if ( results.hasOwnProperty(egId)){
                let egResults = results[egId];

                sortAndPositionResults(event, egResults);
                addPointsToResults(event, egResults);
            }
        }
        function addPointsToResults(event, egResults){
            egResults.forEach(function(result){
                result.positionPoints = 0;
                if ( result.pointsPosition > 0 ) {
                    let athletesAtPosition = event.positions[result.pointsPosition];
                    let pointsForPosition = POINTS_TO_POSITION - result.pointsPosition + 1;

                    if (result.pointsPosition > 0 && result.pointsPosition <= POINTS_TO_POSITION) {
                        if (athletesAtPosition === 1) {
                            result.positionPoints = pointsForPosition;
                        } else if (result.pointsPosition === POINTS_TO_POSITION) {
                            result.positionPoints = 1 / athletesAtPosition;
                        } else {
                            result.positionPoints = ((pointsForPosition * 2) - (athletesAtPosition - 1)) / 2;
                        }
                        if ( result.positionPoints < 0.5 ){
                            result.positionPoints = 0.5;
                        }
                    }
                }
                result.standardPoints = 0;
                for(let stdName in event.standards){
                    let stdPoints = 1;
                    if ( stdName.toUpperCase() === 'NS'){
                        stdPoints = 2;
                    }
                    let standards = event.standards[stdName];
                    for(let std in standards){
                        let standard = standards[std];
                        let stdReached = event.type === E4S_TYPE_TRACK ? parseFloat(result.result) <= parseFloat(standard.value) : parseFloat(result.result) >= parseFloat(standard.value);
                        if (stdReached ){
                            if ( result.standardPoints < stdPoints ){
                                result.standardPoints = stdPoints;
                                let ageGroupId = event.ceObjs[0].ageGroupId;
                                let eventDefId = event.eventDef.id;
                                let athleteId = (event.isTeamEvent ? "T" : "") + result.athleteId;
                                if (! athleteStdPoints.hasOwnProperty(ageGroupId) ) {
                                    athleteStdPoints[ageGroupId] = {};
                                }
                                if (! athleteStdPoints[ageGroupId].hasOwnProperty(eventDefId) ) {
                                    athleteStdPoints[ageGroupId][eventDefId] = {};
                                }
                                if (! athleteStdPoints[ageGroupId][eventDefId].hasOwnProperty(athleteId) ) {
                                    athleteStdPoints[ageGroupId][eventDefId][athleteId] = {
                                        points: stdPoints
                                    };
                                }else{
                                    if ( athleteStdPoints[ageGroupId][eventDefId][athleteId].points < stdPoints ) {
                                        athleteStdPoints[ageGroupId][eventDefId][athleteId].points = stdPoints;
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
        function sortResults(results, ascending = true){
            if ( ascending ) {
                results.sort(function (a, b) {
                    return a.result - b.result;
                });
            }else{
                results.sort(function (a, b) {
                    return b.result - a.result;
                });
            }
        }
        function sortAndPositionResults(event, egResults){
            if ( event.maxHeat === 0 ){
                // allow for an event to not be seeded ???? comp 482
                event.maxHeat = event.latestHeat;
            }
            let calcPoints = event.type === '<?php echo E4S_EVENT_FIELD ?>' || (event.maxHeat === event.latestHeat && event.isFinal);
            event.positions = [];

            // sort results on performance, if more than 1 heat
            if (event.maxHeat > 1) {
                if (event.type === E4S_TYPE_TRACK) {
                    sortResults(egResults);
                }
            }

            // Need to calculate event position and points position ( points means remove any guests )
            let lastResult = egResults[0];
            lastResult.eventPosition = 1;
            let pointsPosition = isResultAGuest(lastResult) || !calcPoints ? 0 : 1;

            lastResult.pointsPosition = pointsPosition;
            addToEventPositions(event, lastResult);
            let jointPositions = 0;
            let guestCount = 0;
            for ( let i = 1; i < egResults.length; i++ ){
                let result = egResults[i];
                let guestResult = isResultAGuest(result);
                if ( guestResult ){
                    guestCount++;
                }
                // if ( result.result !== lastResult.result ){
                if ( result.heatPosition !== lastResult.heatPosition ){
                    result.eventPosition = i + 1;
                    if ( !guestResult && calcPoints ){
                        pointsPosition += jointPositions + 1;
                    }
                    jointPositions = 0;
                }else{
                    if ( !guestResult && calcPoints ){
                        jointPositions++;
                    }
                    result.eventPosition = lastResult.eventPosition;
                }
                result.pointsPosition = guestResult ? 0 : pointsPosition;
                addToEventPositions(event, result);
                lastResult = result;
            }
            // A final has not allocated all the points due to guests, so continue in the heats
            // if ( event.isFinal && pointsPosition < POINTS_TO_POSITION ){
            if ( event.isFinal && pointsPosition < POINTS_TO_POSITION && guestCount > 0 ){
                pointsPosition = POINTS_TO_POSITION - guestCount;
                let heatResults = results[event.qualifyFromEgId];
                let qualifyingEvent = events[event.qualifyFromEgId];
                if ( heatResults !== undefined ){
                    for( let r in heatResults){
                        let result = heatResults[r];
                        if ( result.qualify === '' && !isResultAGuest(result) ){
                            pointsPosition++;
                            result.pointsPosition = pointsPosition;
                            addToEventPositions(qualifyingEvent, result);
                            if ( pointsPosition >= POINTS_TO_POSITION) {
                                break;
                            }
                        }
                    }
                    addPointsToResults(qualifyingEvent, heatResults);
                }
            }
        }
        function isResultAGuest(result){
            let county = countyInfo[result.county.toUpperCase()];
            if (county === undefined){
                county = countyInfo[result.county.toUpperCase()];
                if (county === undefined){
                    return false;
                }
            }
            return county.isGuest;
        }

        function addToEventPositions(event, result){
            let positions = event.positions;
            let position = result.pointsPosition;
            if ( position > 0 ) {
                if (positions[position] === undefined) {
                    positions[position] = 0;
                }
                positions[position]++;
            }
        }

        function getStandardDisplay(event){
            let standards = event.standards;
            let display = '';
            let sep = '';
            for(let stdName in standards){
                let standard = standards[stdName];
                display += sep + stdName + ': ';
                for(let std in standard){
                    display += standard[std].valueText.replace(E4S_ENSURE_STRING,'') + ' ';
                }
                sep = ', ';
            }
            return display;
        }
        function outputRawResults(){
            let raw = $('#raw');
            raw.empty();
            let carryPoints = [];
            for (let egId in results){
                let event = events[egId];
                let egResults = results[egId];
                let table = $('<table style="width:100%">');
                let thead = $('<thead>');
                let tbody = $('<tbody>');
                let tr = $('<tr>');
                let th = $('<th colspan=2>');
                th.text(event.typeNo + " : " + event.name);
                tr.append(th);
                th = $('<th>');
                th.text(' Standards - ' + getStandardDisplay(event));
                tr.append(th);
                thead.append(tr);
                table.append(thead);
                tr = $('<tr>');
                th = $('<th style="width:3%">');
                th.text('Bib No');
                tr.append(th);
                th = $('<th style="width:25%">');
                th.text('Name');
                tr.append(th);
                th = $('<th style="width:25%">');
                th.text('County');
                tr.append(th);
                th = $('<th style="width:6%">');
                th.text('Perf');
                tr.append(th);
                th = $('<th style="width:6%">');
                let qualify = event.isFinal ? '' : 'Qualify';
                th.text(qualify);
                tr.append(th);
                th = $('<th style="width:6%">');
                let heatText = event.isFinal ? '' : 'Heat';
                th.text(heatText);
                tr.append(th);
                th = $('<th style="width:6%">');
                heatText = event.isFinal ? '' : 'Heat Position';
                th.text(heatText);
                tr.append(th);
                th = $('<th style="width:6%">');
                th.text('Event Position');
                tr.append(th);
                th = $('<th style="width:6%">');
                // let pointsInfo = event.isFinal ? 'Points Position' : '';
                let pointsInfo = 'Points Position';
                th.text(pointsInfo);
                tr.append(th);
                th = $('<th style="width:6%">');
                // pointsInfo = event.isFinal ? 'Points' : '';
                pointsInfo = 'Points';
                th.text(pointsInfo);
                tr.append(th);
                th = $('<th>');
                th.text('Standard Points');
                tr.append(th);
                tbody.append(tr);

                egResults.forEach(function(result){
                    let guest = isResultAGuest(result);
                    tr = $('<tr resultrow=true>');
                    let td = $('<td>');
                    td.text(result.bibNo);
                    tr.append(td);
                    td = $('<td>');
                    td.text(result.athlete);
                    tr.append(td);
                    td = $('<td>');
                    let county = result.county;;
                    if (guest) {
                        county += ' (Guest)';
                    }
                    td.text(county);
                    tr.append(td);
                    td = $('<td>');
                    td.text(result.resultText.replace(E4S_ENSURE_STRING, ''));
                    tr.append(td);
                    td = $('<td>');
                    td.text(result.qualify);
                    tr.append(td);
                    td = $('<td>');
                    let heatInfo = event.isFinal ? '' : result.heatNo;
                    td.text(heatInfo);
                    tr.append(td);
                    td = $('<td>');
                    heatInfo = event.isFinal ? '' : result.heatPosition;
                    td.text(heatInfo);
                    tr.append(td);
                    td = $('<td>');
                    td.text(result.eventPosition);
                    tr.append(td);
                    td = $('<td>');
                    pointsInfo = result.pointsPosition > 0 || event.isFinal ? result.pointsPosition : '';
                    td.text(pointsInfo);
                    tr.append(td);
                    td = $('<td>');
                    pointsInfo = result.pointsPosition > 0 || event.isFinal ? result.positionPoints : '';
                    td.text(pointsInfo);
                    tr.append(td);
                    td = $('<td>');
                    let standard = '';
                    if ( result.standardPoints === 1){
                        standard = 'ES';
                    }
                    if ( result.standardPoints === 2){
                        standard = 'NS';
                    }
                    if (result.qualify !== ""){
                        carryPoints[result.athleteId] = result.standardPoints;
                    }else{
                        if ( carryPoints.hasOwnProperty(result.athleteId) ) {
                            let carryPts = carryPoints[result.athleteId];
                            if ( carryPts === 1){
                                standard = 'ES';
                            }
                            if ( carryPts === 2){
                                standard = 'NS';
                            }
                        }
                    }

                    if ( guest || standard === '' || result.qualify !== "") {
                        td.text(standard);
                    } else {
                        let stdPoints = result.standardPoints;
                        let fromHeat = '';
                        if ( carryPoints.hasOwnProperty(result.athleteId) ) {
                            if ( carryPoints[result.athleteId] > stdPoints ){
                                stdPoints = carryPoints[result.athleteId];
                                fromHeat = '/qh';
                            }
                        }
                        if ( stdPoints === 1){
                            standard = 'ES';
                        }
                        if ( stdPoints === 2){
                            standard = 'NS';
                        }
                        td.text(stdPoints + ' (' + standard + fromHeat + ')');
                    }
                    tr.append(td);

                    tbody.append(tr);
                });
                table.append(tbody);
                let openEventHtml = '<div ';
                openEventHtml += 'id="' + event.id + '"';
                openEventHtml += 'gender="' + event.ceObjs[0].gender + '" ';
                openEventHtml += 'age="' + event.ceObjs[0].ageGroupId + '" ';
                openEventHtml += 'event="' + event.eventDef.name.toLowerCase() + '" ';
                openEventHtml += '>';

                openEventHtml += table.prop('outerHTML');
                openEventHtml += '<hr>';
                openEventHtml += '</div>';
                raw.append(openEventHtml);
            }

        }
        function sortTeamPoints(property, ageGroupId = 0){
            let info = [];
            for(let category in teamPoints){
                let sorted = [];

                for(let county in teamPoints[category]) {
                    let points = teamPoints[category][county][property];
                    if (typeof points === 'object'){
                        points = points[ageGroupId];
                    }
                    sorted.push({county: county, result: points});
                }
                sortResults(sorted, false);
                info[category] = sorted;
            }
            return info;
        }
        function outputPositionResults(){
            let positionResults = $('#sorted');
            positionResults.empty();
            let table = $('<table style="width:100%">');
            let thead = $('<thead>');
            let tbody = $('<tbody>');

            outputTitle('Overall Points', tbody);
            let info = sortTeamPoints('points');
            outputTeamData(info, tbody);

            outputTitle('Female Points', tbody, true);
            info = sortTeamPoints('femalePoints', 0);
            outputTeamData(info, tbody);

            outputTitle('Male Points', tbody, true);
            info = sortTeamPoints('malePoints', 0);
            outputTeamData(info, tbody);

            for(let ageGroupId in ageGroups){
                let ageGroup = ageGroups[ageGroupId];
                if (ageGroup.indexOf('ES ') > -1){
                    outputTitle('Female ' + ageGroup, tbody, true);
                    info = sortTeamPoints('femalePoints', ageGroupId);
                    outputTeamData(info, tbody);

                    outputTitle('Male ' + ageGroup, tbody, true);
                    info = sortTeamPoints('malePoints', ageGroupId);
                    outputTeamData(info, tbody);
                }
            }

            table.append(tbody);
            positionResults.append(table);
            positionResults.append('<hr>');
        }
        function addDivider(body){
            let divider = $('<tr style="line-height: 15px;background-color: white">');
            let td = $('<td colspan=12 style="border-top: 2px dotted black;">');
            td.html('&nbsp;');
            divider.append(td);
            body.append(divider);
        }
        function outputCategoryHeaders(body){
            let tr = $('<tr>');
            let categories = getCategories(teamPoints);
            for(let c of categories) {
                let th = $('<th colspan=3>');
                th.text(c);
                tr.append(th);
            }
            body.append(tr);
        }
        function outputTitle(title, body, divider = false){
            if (divider){
                addDivider(body);
            }
            let tr = $('<tr>');
            let td = $('<td colspan=12>');
            let h2 = $('<h2>');
            h2.text(title);
            td.append(h2);
            tr.append(td);
            body.append(tr);
            outputCategoryHeaders(body);
        }
        function getCategories(teamPoints){
            let categorys = [];
            for(let category in teamPoints) {
                categorys.push(category);
            }
            categorys.sort();
            return categorys;
        }
        function outputTeamData(info, tbody){
            let lastPos = 1;
            let lastResult = '';
            let categories = getCategories(info);
            for(let r = 0; r < categoryCounts['MAX'];r++){
                let tr = $('<tr>');
                for(let cat of categories) {
                    if (r < info[cat].length) {
                        // got a result for county
                        let data = {
                            county: '',
                            result: ''
                        };
                        let td = $('<td style="width:3%">');

                        data = info[cat][r];
                        if (typeof info[cat][r].result === "undefined" || info[cat][r].result === 0) {
                            td.text("-");
                        } else {
                            if (lastResult !== data.result) {
                                lastPos = r + 1;
                            }
                            td.text(lastPos);
                        }

                        tr.append(td);
                        td = $('<td style="width:10%">');
                        td.text(data.county);
                        tr.append(td);
                        td = $('<td style="width:7%">');
                        td.text(data.result);
                        lastResult = data.result;
                        tr.append(td);
                    }else{
                        tr.append($('<td>'));
                        tr.append($('<td>'));
                        tr.append($('<td>'));
                    }
                }
                tbody.append(tr);
            }
        }

	</script>
</head>
<body>
<div id="tabs">
        <ul>
            <li><a href="#tab-position">Positions</a></li>
            <li><a href="#tab-raw">Raw Results</a></li>
        </ul>
        <div id="tab-position">
            <div id="sorted"></div>
        </div>

        <div id="tab-raw">
<!--            --><?php
//                if ( isUserLoggedIn() ){
//            ?>
                    <span style="padding-right:20px"><input id="clearFilters" type="button" value="Clear Filters" onclick="clearFilters();"></input></span>
                    <span style="padding-right:20px">Event Filter : <input style="width:100px;" id="eventFilter" onkeyup="applyFilters();"></input></span>
                    <span style="padding-right:20px">Athlete Filter : <input style="width:100px;" id="textFilter" onkeyup="applyFilters();"></input></span>
                    <span style="padding-right:20px">Team Filter : <select id="teamSelect"><option value="">Select a Team</option></select></span>
                    <span style="padding-right:20px">Gender Filter : <select id="genderSelect"><option value="">Select a Gender</option></select></span>
                    <span>Age Filter : <select id="ageSelect"><option value="">Select an Age Group</option></select></span>

<!--            --><?php
//                }
//            ?>

            <div id="raw"></div>
        </div>
    </div></Points>
</body>
</html>
