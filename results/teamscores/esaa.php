<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/clubCompCRUD.php';

function e4s_esaaScores($obj){
	$compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
	$esaaObj = new teamScores($compId);
	$esaaObj->output();
	exit;
}
class teamScores{
	private $compObj;
	public $eventObj;
	public $resultsObj;
	public $clubCompInfo;
	public $categoryCounts;
	function __construct(int $compId){
		$this->compObj = e4s_getCompObj($compId);
		$this->eventObj = new teamScoreEvents($this->compObj);
		$this->getClubCompInfo($compId);
		$this->resultsObj = new teamResults($this->compObj, $this->eventObj);
		$this->_moveMaxToEvents();
	}
	private function _moveMaxToEvents(){
		$config = $this->resultsObj->results->config;

		foreach($config as $egId=>$egConfig){
			$this->eventObj->events[$egId]->maxHeat = $egConfig->maxHeat;
			$this->eventObj->events[$egId]->latestHeat = $egConfig->latestHeat;
		}
	}
	function getCompInfo(){
		$info = new stdClass();
		$info->id = $this->compObj->getID();
		$info->name = $this->compObj->getName();
		$info->startDate = $this->compObj->getDate();
		return $info;
	}
	function getClubCompInfo($compId){
		$clubCompInfo = e4s_listClubCompDefsForCompId($compId);
		$this->clubCompInfo = [];
		$this->categoryCounts = [];
		foreach($clubCompInfo as $clubComp) {
			$clubComp->isGuest = false;
			$categoryName = str_replace('&','and',$clubComp->category->name);
			if ( stripos($categoryName, 'guest') !== false ) {
				$clubComp->isGuest = true;
			}
			$clubComp->bibNos = preg_split('~,~', $clubComp->bibNos);
			$clubComp->county = $clubComp->club;
			unset($clubComp->club);
			unset($clubComp->competition);
			$clubComp->category->name = $categoryName;
			$this->clubCompInfo[ strtoupper($clubComp->county->name) ] = $clubComp;
			if ( !array_key_exists($categoryName, $this->categoryCounts) ) {
				$this->categoryCounts[ $categoryName ] = 0;
			}
			$this->categoryCounts[ $categoryName ]++;
		}
		$this->categoryCounts['MAX'] = max($this->categoryCounts);
	}
	function output(){
		e4s_webPageHeader();
		include_once E4S_FULL_PATH . 'results/teamscores/esaaOutput.php';
	}
}
class teamResults{
	public $results;
	function __construct(e4sCompetition $compObj, $eventObjs){
		$this->results = $this->getData($compObj->getID(), $eventObjs);
	}
	private function getData($compId, $eventObjs){
		$sql = 'select rh.id rhId,
       				   rh.heatNo,
				       eg.id egId,
				       rd.qualify,
				       rd.bibNo,
				       rd.athleteId,
				       rd.athlete,
				       replace(rd.club,"&","and") county,
				       rd.score result,
				       rd.position heatPosition
				from ' . E4S_TABLE_EVENTRESULTSHEADER . ' rh,
				     ' . E4S_TABLE_EVENTRESULTS . ' rd,
				     ' . E4S_TABLE_EVENTGROUPS . ' eg
				where rh.egid = eg.id
				and rd.resultheaderid = rh.id
				AND rd.score > 0
				and eg.compid = ' . $compId . '
				order by eg.id, rh.heatNo, rd.position';
		$result = e4s_queryNoLog($sql);

		$maxHeats = $this->getMaxHeats($compId);
		$config = [];
		$data = [];
		while ($row = $result->fetch_object()) {
			$row->rhId = (int)$row->rhId;
			$row->egId = (int)$row->egId;
			$eventObj = $eventObjs->events[ $row->egId ];
			$row->heatNo = (int)$row->heatNo;
			$row->athleteId = (int)$row->athleteId;
			$row->result = (float)$row->result;
			if ($eventObj->type === E4S_EVENT_TRACK) {
				$row->resultText = E4S_ENSURE_STRING . resultsClass::getResultFromSeconds($row->result);
			}else{
				// ensure 2 decimal places
				$row->result = number_format($row->result, 2, '.', '');
				$row->resultText = E4S_ENSURE_STRING . $row->result;
			}
			$row->heatPosition = (int)$row->heatPosition;
			$row->eventPosition = 0;
			if ( !array_key_exists($row->egId, $data) ) {
				$data[ $row->egId ] = [];
			}
			$data[ $row->egId ][] = $row;
			if ( !array_key_exists($row->egId, $config) ){
				$config[$row->egId] = new stdClass();
				$config[$row->egId]->latestHeat = 0;
				if ( array_key_exists($row->egId, $maxHeats)) {
					// Teams need to be seeded in E4S
					$config[ $row->egId ]->maxHeat = $maxHeats[ $row->egId ];
				}else{
					$config[ $row->egId ]->maxHeat = 0;
				}
			}
			if ( $row->heatNo > $config[$row->egId]->latestHeat ){
				$config[$row->egId]->latestHeat = $row->heatNo;
			}
		}

		$results = new stdClass();
		$results->config = $config;
		$results->data = $data;
		return $results;
	}
	function getMaxHeats($compId){
		$sql = 'select eventgroupid egId, max(heatno) maxHeatNo
				from ' . E4S_TABLE_SEEDING . ' s,
				     ' . E4S_TABLE_EVENTGROUPS . ' eg
				where eg.compid = ' . $compId .'
				and s.eventgroupid = eg.id
				group by eventgroupid';
		$result = e4s_queryNoLog($sql);
		$maxHeats = [];
		while ($row = $result->fetch_object()) {
			$maxHeats[(int)$row->egId] = (int)$row->maxHeatNo;
		}
		return $maxHeats;
	}
}
class teamScoreEvents{
	public $events;
	public $ageGroups;
	function __construct(e4sCompetition $compObj){
		$this->events = $compObj->getEGObjs();
		$ceObjs = $compObj->getCeObjs();
		$ageGroupIds = [];
		foreach ($ceObjs as $ceFullObj) {
			$egId = $ceFullObj->egId;
			$egObj = $this->events[$egId];
			$ceObj = new stdClass();
			$ceObj->id = $ceFullObj->id;
			$ceObj->ageGroupId = $ceFullObj->ageGroupId;
			$ageGroupIds[$ceFullObj->ageGroupId] = $ceFullObj->ageGroupId;
			$ceObj->gender = $ceFullObj->gender;
			if ( !isset($egObj->ceObjs) ) {
				$egObj->ceObjs = [$ceObj];
			}else {
				$egObj->ceObjs[] = $ceObj;
			}
		}
		$ageGroupIds = array_values($ageGroupIds);
		$sql = 'select id, name 
				from ' . E4S_TABLE_AGEGROUPS . '
				where id in (' . implode(',', $ageGroupIds) . ')';
		$result = e4s_queryNoLog($sql);
		$ageGroups = [];
		while ($row = $result->fetch_object()) {
			$ageGroups[ (int)$row->id ] = $row->name;
		}
		$this->ageGroups = $ageGroups;
		$this->addInStandards($compObj);
	}
	function addInStandards(e4sCompetition $compObj){
		$stds = $compObj->getStandards(E4S_STANDARD_ORDER_COMBINED);

		// ESAA has 1 ce to 1 eg and need ageGroup
		$ceObjs = $compObj->getCeObjs();

		foreach ($ceObjs as $ceObj) {
			$egId = $ceObj->egId;
			$egObj = $this->events[$egId];

			$key = $ceObj->ageGroupId . E4S_STANDARD_ORDER_COMBINED_JOIN . $ceObj->eventId;
			$egStds = [];
			foreach($stds as $stdName=>$stdArr){
				if ( array_key_exists($key, $stdArr) ){
					foreach ($stdArr[$key] as $stdKey=>$stdVal) {
						if ($stdVal->tf === E4S_EVENT_TRACK) {
							$stdVal->valueText = E4S_ENSURE_STRING . resultsClass::getResultFromSeconds($stdVal->value);
						}else{
							// ensure 2 decimal places
							$stdVal->value = number_format($stdVal->value, 2, '.', '');
							$stdVal->valueText = E4S_ENSURE_STRING . $stdVal->value;
						}
					}
					$egStds[$stdName] = $stdArr[$key];
				}
			}
			$egObj->standards = $egStds;
		}
	}
	function getOutputObj(){
		$output = [];

		foreach($this->events as $egId=>$egObj){
			$newObj = new stdClass();
			$newObj->id = $egId;
			$newObj->eventDef = $egObj->eventDef;
			$newObj->isTeamEvent = $egObj->options->isTeamEvent;
			$newObj->isFinal = $egObj->options->seed->qualifyToEg->id === 0;
			$newObj->qualifyToEgId = $egObj->options->seed->qualifyToEg->id;
			$newObj->qualifyFromEgId = 0;
			unset($newObj->eventDef->options);
			$newObj->ceObjs = $egObj->ceObjs;
			$newObj->eventNo = $egObj->eventNo;
			$newObj->name = $egObj->name;
			$newObj->startDate = $egObj->startDate;
			$newObj->type = $egObj->type;
			$newObj->typeNo = $egObj->typeNo;
			$newObj->standards = $egObj->standards;
			if ( isset($egObj->maxHeat) ) {
				$newObj->maxHeat = $egObj->maxHeat;
			}else{
				$newObj->maxHeat = 0;
			}
			if ( isset($egObj->latestHeat) ) {
				$newObj->latestHeat = $egObj->latestHeat;
			}else{
				$newObj->latestHeat = 0;
			}
			$output[$egId] = $newObj;
		}
		foreach($this->events as $egId=>$egObj){
			if ( $output[$egId]->qualifyToEgId > 0 ) {
				$output[$output[$egId]->qualifyToEgId]->qualifyFromEgId = $egId;
			}
		}
		return $output;
	}
}