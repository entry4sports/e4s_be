<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
define( 'E4S_FIELD_DISCIPLINE', 'Field' );
define( 'E4S_TRACK_DISCIPLINE', 'Track' );
define( 'E4S_OUTPUT_START', 'Start' );
define( 'E4S_OUTPUT_RESULTS', 'Results' );
define( 'E4S_OUTPUT_CALLROOM', 'Call-Room' );
define( 'E4S_OUTPUT_SCHEDULE', 'Schedule' );
define( 'E4S_OUTPUT_TOUCH', 'Touch' );
define( "ROSTER_URL", "https://" . E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH . '/public/roster/');
function getRosterPath(){
	return "/var/www/vhosts/roster";
}

function getRosterCompId($obj){
	if ( is_null($obj) ){
		return 0;
	}
	return (int)checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
}
function getRosterInfo($obj = null){
	$email = "roster@mrday.co.uk";
	$passwd = "Day1234567";
	include_once E4S_FULL_PATH . 'classes/rosterClass.php';
	$compId = getRosterCompId($obj);
	if ( $compId === 24488 or $compId === 20228 ){
		$email = "andrewhulse2017@gmail.com";
		$passwd = "Andy123456";
	}
	$rosterObj = new RosterClass($compId, $email, $passwd);
	$action = checkFieldForXSS($obj, 'action:Roster Action');
	switch ($action) {
		case ROSTER_SOCKET_COOKIE:
			$rosterObj->setCookie();
			break;
		case ROSTER_SOCKET_STARTLIST:
			$rosterObj->createStartListFile(true);
			break;
		case ROSTER_SOCKET_RESULTS:
			$rosterObj->createResultsFile(true);
			break;
		case ROSTER_SOCKET_LAST:
			$retObj = new stdClass();
			$retObj->startList = $rosterObj->readStartList( ROSTER_OLD_FILE );
			$results = $rosterObj->readResults( ROSTER_OLD_FILE );
			$retObj->results = $results->events;
			$retObj->meeting = $results->meeting;
			Entry4UISuccess($retObj);
			break;
		case null:
			$retObj = new stdClass();
			$rosterObj->setCookie();
			$retObj->startList = $rosterObj->createStartListFile();
			$results = $rosterObj->createResultsFile();
			$retObj->results = $results->events;
			$retObj->meeting = $results->meeting;
			Entry4UISuccess($retObj);
			break;
	}
}

function readAdverts($mId){
	if ( $mId === 0 ){
		return null;
	}
	$file = $_SERVER['DOCUMENT_ROOT'] . "/results/" . $mId . "/adverts.txt";
	if ( file_exists($file) ){
		$fileId = fopen($file,"r");
		$adverts = [];
		while (($line = fgets($fileId)) !== FALSE) {
			$adverts[] = rtrim($line, "\r\n");
		}
		return $adverts;
	}
	return null;
}