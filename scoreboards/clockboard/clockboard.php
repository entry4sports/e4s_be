<?php
define( "E4S_ROOT_PATH", $_SERVER['DOCUMENT_ROOT'] );
const E4S_PATH = '/entry/v5/';
if ( ! defined( 'E4S_FULL_PATH' ) ) {
	define('E4S_FULL_PATH', E4S_ROOT_PATH . E4S_PATH);
}
if ( ! defined( 'E4S_OTD_PATH' ) ) {
	define('E4S_OTD_PATH', E4S_FULL_PATH . 'otd/');
}
if ( ! defined( 'E4S_ROUTE_PATH' ) ) {
	define('E4S_ROUTE_PATH', '/wp-json/e4s/v5');
}
if ( !defined('E4S_CURRENT_DOMAIN') ) {
	define( 'E4S_CURRENT_DOMAIN', strtolower( $_SERVER['SERVER_NAME'] ) );
	define( 'R4S_UK_DOMAIN', 'result4sports.co.uk' );
	define( 'E4S_LIVE_DOMAIN', 'entry4sports.co.uk' );
	define( 'E4S_OLDLIVE_DOMAIN', 'entry4sports.com' );
	define( 'E4S_DEV_DOMAIN', 'dev.' . E4S_LIVE_DOMAIN );
	define( 'E4S_DEMO_DOMAIN', 'demo.' . E4S_OLDLIVE_DOMAIN );
	define( 'E4S_TEST_DOMAIN', 'test.' . E4S_LIVE_DOMAIN );
	define( 'E4S_LITE_DOMAIN', 'lite.' . E4S_LIVE_DOMAIN );
	define( 'E4S_UAT_DOMAIN', 'uat.' . E4S_LIVE_DOMAIN );
	define( 'E4S_UK_DOMAIN', E4S_LIVE_DOMAIN );
	define( 'E4S_IRE_DOMAIN', 'regional.' . E4S_OLDLIVE_DOMAIN );
	define( 'E4S_AAI_DOMAIN', 'entry.athleticsireland.ie' );
}
include_once E4S_FULL_PATH . 'classes/socketClass.php';
$compId = -1;
if (isset($_GET['compid'])) {
	$compId = $_GET['compid'];
}
?>
<html>
<head>
	<title>Clock Board</title>
	<script>
		<?php echo socketClass::outputJavascript() ?>

        function getCompetition() {
            return {"id":<?php echo $compId ?>};
        }

        function processSocketEvent(msg){
            messageScheduler.addMessage(msg.payload);
        }
	</script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://<?php echo E4S_CURRENT_DOMAIN . E4S_PATH ?>js/moment.min.js"></script>
    <script src="https://<?php echo E4S_CURRENT_DOMAIN . E4S_PATH ?>scoreboards/clockboard/js/common.js"></script>
    <script src="https://<?php echo E4S_CURRENT_DOMAIN . E4S_PATH ?>scoreboards/clockboard/js/clockBoard.js"></script>
	<link rel="stylesheet" type="text/css" href="https://<?php echo E4S_CURRENT_DOMAIN . E4S_PATH ?>scoreboards/clockboard/style/twobyone.css">
    <script>
        $(document).ready(function(){
            $("#clockBody").load("<?php echo E4S_PATH ?>/scoreboards/clockboard/clockBody.html");
        });
    </script>
</head>
<body>
<div class="videoDiv" t4s-element="video">
    <video autoplay muted loop name="media" class="video">
        <source src="https://<?php echo E4S_CURRENT_DOMAIN . E4S_PATH ?>scoreboards/clockboard/graphics/t4s.mp4" type="video/mp4">
    </video>
</div>
<div id="clockBody">
</div>
</body>
</html>

