REM This script requires for you to have installed curl
REM Windows 10 ships with curl preinstalled, otherwise you'll need to download it

REM Change user@server.com to your roster account email
REM Change super_secret to your password

REM Run the script with meeting id as first parameter, or hardcode it below
REM replacing %1% with the proper meeting id
REM ex. set MEETINGID=1234

set USERNAME=Andrewhulse2017@gmail.com
set PASSWORD=Andy123456
set MEETINGID=24487

curl --data "username=%USERNAME%&password=%PASSWORD%" -c cookiejar https://api.admin.rosterathletics.com/api/login

  REM This fetches start lists
  curl -b cookiejar -c cookiejar https://api.admin.rosterathletics.com/api/admin/meeting/%MEETINGID%/export-csv > start-list-%MEETINGID%.csv

  REM This fetches results in new format
  REM curl -b cookiejar -c cookiejar https://api.admin.rosterathletics.com/api/admin/meeting/%MEETINGID%/export-results-csv/v2 > results-%MEETINGID%.csv



