<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
require_once E4S_FULL_PATH . 'dbInfo.php';

$pageRefreshInSecs = 60;
$lateInSecs       = 120;
$prepareInSecs    = 299;
$prepareShowMins  = true;
$showAfterMins    = 2;
$maxRows = 20;
$pageRows = 10;
$pageNo = 1;
$topRows = 3;
$compId =
	isset($_GET['compid']) && is_numeric($_GET['compid']) ?
		$_GET['compid'] : 0;
// get eventdate from the URL
$date =
    isset($_GET['date']) && $_GET['date'] != '' ?
        $_GET['date'] : date('Y-m-d');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta http-equiv="refresh" content="<?php echo $pageRefreshInSecs ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Callroom Timetable</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        background-color: #000000;
        font-size: 3em;
        color: yellow;
    }
    .container {
        width: 90%;
        margin: auto;
        overflow: hidden;
    }
    header {
        background: #0000ff;
        color: #fff;
        padding-top: 30px;
        min-height: 70px;
        border-bottom: #77aaff 3px solid;
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding: 0 20px;
        font-size: 1em;
    }
    header h1 {
        text-transform: uppercase;
        margin: 0;
        font-size: 1em;
    }
    #current-time {
        font-size: 4vw;
    }
    table {
        width: 100%;
        margin: 20px 0;
        border-collapse: collapse;
    }
    table, th, td {
        border: 1px solid #ddd;
    }
    th, td {
        padding: 12px;
        text-align: center;
    }
    th, .pageHeader {
        background-color: #000000;
        color: white;
        font-size: 0.6em;
    }
    tr:nth-child(even) {
        background-color: #000000;
    }
    .late {
        background-color: #0000ff;
        color: yellow;
    }
    .verylate {
        background-color: red;
        color: yellow;
        font-weight: bolder;
    }
    .delayed {
        background-color: black;
        color: dimgrey;
        font-weight: bolder;
    }
    .eventNumber {
        font-size: 0.6em;
        width: 5%;
    }
    .eventName {
        width: 30%;
        font-size: 0.6em;
        text-align: left;
    }
    .callTime {
        font-size: 0.6em;
        width: 15%;
    }
    .eventTime {
        font-size: 0.6em;
        width: 15%;
    }
    .info {
        font-size: 0.6em;
        width: 35%;
    }
    .topRowSeparator {
        border-bottom: 3px solid #77aaff;
    }

    @media (max-width: 768px) {
        body {
            font-size: 1em;
        }
        header {
            flex-direction: column;
            text-align: center;
        }
        header h1 {
            font-size: 0.5em;
        }
        #current-time {
            font-size: 6vw;
        }
        th, td {
            padding: 8px;
        }
        th, .pageHeader {
            font-size: 0.6em;
        }
    }

    @media (max-width: 480px) {
        body {
            font-size: 0.5em;
        }
        header h1 {
            font-size: 0.5em;
        }
        #current-time {
            font-size: 8vw;
        }
        th, td {
            padding: 6px;
        }
        th, .pageHeader {
            font-size: 0.5em;
        }
    }
</style>
</head>
<body>

<header>
	<h1>Callroom Timetable</h1>
	<div id="current-time"></div>
</header>
<div class="container">
	<table>
		<thead>
		<tr>
			<th>Event</th>
			<th>Name</th>
			<th>Callroom Time</th>
			<th>Event Time</th>
			<th>Info</th>
		</tr>
		</thead>
		<tbody>
		<?php
        $mins = $showAfterMins;
        if ( $mins < 10 ) {
            $mins = '0' . $mins;
        }
        if ( $mins > 59 ) {
            $mins = 59;
        }
		$sql = 'SELECT id, eventNumber, eventName, TIME_FORMAT(callTime, "%H:%i") as callTime, TIME_FORMAT(eventTime, "%H:%i") as eventTime, timestampdiff(SECOND, curtime(), calltime) as secondsToCall
				FROM ' . E4S_TABLE_TIMETABLE . '
								WHERE compid = ' . $compId . '
								and eventDate = "' . $date . '"
				and addtime(calltime, "00:' . $mins . ':00") >= curtime()
				ORDER BY callTime ASC
				limit ' . $maxRows;
		$result = e4s_queryNoLog($sql);

		if ($result->num_rows > 0) {
// Output data of each row
            $pageNo = 1;
            $rowNo = 1;
            $displayClass = '';
			while($row = $result->fetch_assoc()) {
                $prop = "calltime='" . $row["callTime"] . "'";
                if ( $rowNo <= $topRows ) {
	                echo "<tr {$prop}>";
                } else {
                    if ( $pageNo === 1 ){
                        $useRow = $pageRows;
                    } else {
                        $useRow = (($pageRows - $topRows) * $pageNo) + $topRows;
                    }
                    if ( $rowNo > $useRow ) {
                        $pageNo++;
                        $displayClass = 'display:none;';
	                    echo "<tr class='pageHeader' page='" . $pageNo . "' style='" . $displayClass . "'><td colspan='5'>Page " . $pageNo . "</td></tr>";
                        $rowNo++;
                    }

                    echo "<tr {$prop} page='" . $pageNo . "' style='" . $displayClass . "'>";
                }
				$rowNo++;
				$toGo = (int)$row["secondsToCall"];
                $minutesToGo = (floor($toGo / 60)+1);
                $useClass = "";
				if ( $toGo <= 0 ) {
					$useClass = "verylate";
				} else if ( $toGo <= $lateInSecs ) {
					$useClass = "late";
				} else if ( $minutesToGo === 5 ) {
                    $useClass = "delayed";
                }
				echo "<td class='eventNumber " . $useClass . "'>" . $row["eventNumber"] . "</td>";
				echo "<td class='eventName'>" . $row["eventName"] . "</td>";
				echo "<td class='callTime'>" . $row["callTime"] . "</td>";
				echo "<td class='eventTime'>" . $row["eventTime"] . "</td>";
				echo "<td class='info " . $useClass . "'>";
                if ( $toGo <= 0 ) {
                    echo "IN CALLROOM";
                } else if ( $toGo <= $lateInSecs ) {
                    echo "Go To CallRoom";
                } else if ( $toGo <= $prepareInSecs ) {
                    if ( !$prepareShowMins ) {
	                    echo "Prepare for CallRoom";
                    } else if ($useClass === "delayed") {
                        echo "Delayed";
                    } else {
                        echo $minutesToGo . " minutes";
                    }
                }
				echo "</td>";
				echo "</tr>";
			}
		} else {
			echo "<tr><td colspan='5'>No timetable entries found</td></tr>";
		}
		?>
		</tbody>
	</table>
</div>
<span style="font-size:20px; color:white;">Click this button to reset times for testing : <button onclick="window.location.href = '/wp-json/e4s/v5/test?action=settimetable&compid=<?php echo $compId ?>';">reset data</button>
<br>
The page will refresh every <?php echo $pageRefreshInSecs ?> seconds and you should see the info for the events change.</span>
<script>
    let maxPageNo = <?php echo $pageNo; ?>;
    let currentPageNo = 0;
    function nextPage(){
        if (maxPageNo === 1) {
            return;
        }
        $("[page='" + currentPageNo + "']").hide();
        if ( currentPageNo < maxPageNo ) {
            currentPageNo++;
        } else {
            currentPageNo = 1;
        }
        $("[page='" + currentPageNo + "']").show();
    }
    function updateTime() {
        const now = new Date();
        const timeString = now.toLocaleTimeString();
        document.getElementById('current-time').textContent = timeString;
        checkInfo();
    }

    function checkInfo(){
        // get all objs with calltime property and check how many seconds to go
        let objs = $("[calltime]");
        objs.each(function(){
            let callTime = $(this).attr("calltime");
            let now = new Date();
            let callTimeParts = callTime.split(":");
            let callTimeDate = new Date();
            callTimeDate.setHours(callTimeParts[0]);
            callTimeDate.setMinutes(callTimeParts[1]);
            callTimeDate.setSeconds(0);
            let diff = callTimeDate - now;
            let seconds = Math.floor(diff / 1000);
            let minutesToGo = (Math.floor(seconds / 60) + 1 );
            if ( seconds <= 0 ) {
                $(this).find(".info").text("IN CALLROOM");
                addClass($(this), "verylate");
            } else if ( seconds <= <?php echo $lateInSecs ?> ) {
                $(this).find(".info").text("Go To CallRoom");
                addClass($(this), "late");
            } else if ( seconds <= <?php echo $prepareInSecs ?> ) {
                if (<?php echo $prepareShowMins ?> ) {
                    if (minutesToGo === 5) {
                        $(this).find(".info").addClass("delayed").text("Delayed");
                    } else if (minutesToGo < 2 ) {
                        $(this).find(".info").removeClass("delayed").text("Prepare for CallRoom");
                    }else{
                        $(this).find(".info").removeClass("delayed").text(minutesToGo + " minutes");
                    }

                }

                addClass($(this), "");
            } else {
                $(this).find(".info").text("");
                addClass($(this), "");
            }
        });
    }
    function addClass(obj, className){
        if ( className !== "late" ) {
            obj.find(".info").removeClass("late");
            obj.find(".eventNumber").removeClass("late");
        }
        if ( className !== "verylate" ) {
            obj.find(".info").removeClass("verylate");
            obj.find(".eventNumber").removeClass("verylate");
        }

        if ( className === "late" ) {
            obj.find(".info").addClass("late");
            obj.find(".eventNumber").addClass("late");
        }
        if ( className === "verylate" ) {
            obj.find(".info").addClass("verylate");
            obj.find(".eventNumber").addClass("verylate");
        }
    }
    setInterval(nextPage, 5000);
    setInterval(updateTime, 1000);
    updateTime(); // Initial call to display time immediately
    nextPage(); // Initial call to display first page
</script>
</body>
</html>
