<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
require_once E4S_FULL_PATH . 'dbInfo.php';
// if compId is passed in the URL, use it
$compId =
	isset($_GET['compId']) && is_numeric($_GET['compId']) ?
		$_GET['compId'] : 0;
// set $latestDate to the system date in php
$latestDate = date('Y-m-d');

// check for action inpost
if (isset($_POST['action']) ) {
	$sql = "";
	if ( $_POST['action'] == 'delete' ) {
		$id  = $_POST['id'];
		$sql = 'DELETE FROM ' . E4S_TABLE_TIMETABLE . ' WHERE id = ' . $id;

	}
// check action for add
	if ( $_POST['action'] == 'add' ) {
		$compId      = $_POST['compId'];
		$eventDate   = $_POST['eventDate'];
		$eventNumber = $_POST['eventNumber'];
		$eventName   = $_POST['eventName'];
		$callTime    = $_POST['callTime'];
		$eventTime   = $_POST['eventTime'];
		$sql         = 'INSERT INTO ' . E4S_TABLE_TIMETABLE . ' (compId, eventDate, eventNumber, eventName, callTime, eventTime) 
		VALUES (' . $compId . ', "' . $eventDate . '", "' . $eventNumber . '", "' . $eventName . '", "' . $callTime . '", "' . $eventTime . '")';
	}
	if ($sql !== "" ){
		e4s_queryNoLog( $sql );
	}
	exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Timetable Management</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script>
        $(document).ready(function(){
            $('button').click(function(){

            });
        });
        function addEvent(){
            // Get the form data
            var compId = $('#compId').val();
            var eventDate = $('#eventDate').val();
            var eventNumber = $('#eventNumber').val();
            var eventName = $('#eventName').val();
            var callTime = $('#callTime').val();
            var eventTime = $('#eventTime').val();

            if (compId == '' || eventDate == '' || eventNumber == '' || eventName == '' || callTime == '' || eventTime == '') {
                alert('All fields are required');
                return;
            }
            // Send the data to the server
            $.post(location.pathname, {
                compId: compId,
                eventDate: eventDate,
                eventNumber: eventNumber,
                eventName: eventName,
                callTime: callTime,
                eventTime: eventTime,
                action: 'add'
            }, function(data){
                // Refresh the page
                location.reload();
            });
        }
        function deleteEvent(id){
            // Send the data to the server
            $.post(location.pathname, {
                id: id,
                action: 'delete'
            }, function(data){
                // Refresh the page
                location.reload();
            });
        }
	</script>
</head>
<body>
<h1>Timetable Management</h1>
<form>
	<label for="compId">Competition Number:</label>
	<input readonly type="number" id="compId" name="compId" value="<?php echo $compId ?>"><br><br>
	<label for="eventDate">Event Date:</label>
	<input type="date" id="eventDate" name="eventDate"><br><br>

	<label for="eventNumber">Event Number:</label>
	<input type="text" id="eventNumber" name="eventNumber"><br><br>

	<label for="eventName">Event Name:</label>
	<input type="text" id="eventName" name="eventName"><br><br>

	<label for="callTime">Call Time:</label>
	<input type="time" id="callTime" name="callTime"><br><br>

	<label for="eventTime">Event Time:</label>
	<input type="time" id="eventTime" name="eventTime"><br><br>

	<button onclick="addEvent();return false;">Add Event</button>
</form>

<h2>Existing Events</h2>
<table border="1">
	<tr>
		<th>Competition ID</th>
		<th>Event Date</th>
		<th>Event Number</th>
		<th>Event Name</th>
		<th>Call Time</th>
		<th>Event Time</th>
		<th>Action</th>
	</tr>
	<?php
	// Database connection
	// Fetch events
	$sql = 'SELECT id, compId, date_format(eventDate,"%d/%m/%Y") as eventDate, eventNumber, eventName, TIME_FORMAT(callTime, "%H:%i") as callTime, TIME_FORMAT(eventTime, "%H:%i") as eventTime, timestampdiff(SECOND, curtime(), calltime) as secondsToCall
			FROM ' . E4S_TABLE_TIMETABLE . ' WHERE compId = ' . $compId . ' ORDER BY eventdate, calltime DESC';

	$result = e4s_queryNoLog($sql);

	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			echo "<tr>
				<td>{$row['compId']}</td>
				<td>{$row['eventDate']}</td>
				
				<td>{$row['eventNumber']}</td>
				<td>{$row['eventName']}</td>
				<td>{$row['callTime']}</td>
				<td>{$row['eventTime']}</td>
				<td><button onclick=\"deleteEvent({$row['id']});return false;\">Delete</button></td>
				</tr>";
			if ( $row['eventDate'] > $latestDate ) {
				$latestDate = $row['eventDate'];
			}
		}
	} else {
		echo "<tr><td colspan='7'>No events found</td></tr>";
	}

	?>
</table>
<script>
	// set eventDate field to latest date from the database
    // format $latestDate to yyyy-mm-dd
    latestDate = '<?php echo $latestDate ?>'.split('/').reverse().join('-');
    //latestDate = '<?php //echo $latestDate ?>//';
    $('#eventDate').val(latestDate);
</script>
</body>
</html>
