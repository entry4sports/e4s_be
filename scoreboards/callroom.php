<?php
$scheme = $_GET['scheme'];
if ( array_key_exists('size', $_GET) ) {
    $size = (int)$_GET['size'];
} else {
    $size = 6;
}
$events = $_GET['events'];
$events = str_replace('^', '&', $events);
$events = explode('~' , urldecode($events));
?>
<!DOCTYPE html>
<html>
<head>
    <script src="./fitty.min.js"></script>

	<title>Call Room</title>
	<style>
		.callRoomDiv {
			text-align: center;
            height: 90vh;
		}
        .callRoom {
			font-size: <?php echo $size ?>vw;
		}
        .nowcalling {
            font-size: 3vw;
            color:yellow;
        }
        .high {
            background-color: #092238;
        }
        .high > div {
            margin: 2vw;
            background-image: linear-gradient(#1b557b, #0c2b46);
            color: white;
        }
        .light {
            background-color: white;
            color: black;
        }
        .dark {
            background-color: black;
            color: white;
        }
	</style>
</head>
<body class="<?php echo $scheme?>">
	<div class="callRoomDiv">
        <div class="nowcalling">Now Calling</div>
        <?php
        foreach ($events as $event) {
            echo '<div class="callRoom">' . $event . '</div>';
        }
        ?>
	</div>
</body>
</html>