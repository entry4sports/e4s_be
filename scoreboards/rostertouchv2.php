<?php
//require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
//include_once E4S_FULL_PATH . 'dbInfo.php';
define( "E4S_ROOT_PATH", $_SERVER['DOCUMENT_ROOT'] );
const E4S_PATH = '/entry/v5/';
if ( ! defined( 'E4S_FULL_PATH' ) ) {
	define('E4S_FULL_PATH', E4S_ROOT_PATH . E4S_PATH);
}
if ( ! defined( 'E4S_OTD_PATH' ) ) {
	define('E4S_OTD_PATH', E4S_FULL_PATH . 'otd/');
}
if ( ! defined( 'E4S_ROUTE_PATH' ) ) {
	define('E4S_ROUTE_PATH', '/wp-json/e4s/v5');
}

if ( !defined('E4S_CURRENT_DOMAIN') ) {
	define( 'E4S_CURRENT_DOMAIN', strtolower( $_SERVER['SERVER_NAME'] ) );
	define( 'R4S_UK_DOMAIN', 'result4sports.co.uk' );
	define( 'E4S_LIVE_DOMAIN', 'entry4sports.co.uk' );
	define( 'E4S_OLDLIVE_DOMAIN', 'entry4sports.com' );
	define( 'E4S_DEV_DOMAIN', 'dev.' . E4S_LIVE_DOMAIN );
	define( 'E4S_DEMO_DOMAIN', 'demo.' . E4S_OLDLIVE_DOMAIN );
	define( 'E4S_TEST_DOMAIN', 'test.' . E4S_LIVE_DOMAIN );
	define( 'E4S_LITE_DOMAIN', 'lite.' . E4S_LIVE_DOMAIN );
	define( 'E4S_UAT_DOMAIN', 'uat.' . E4S_LIVE_DOMAIN );
	define( 'E4S_UK_DOMAIN', E4S_LIVE_DOMAIN );
	define( 'E4S_IRE_DOMAIN', 'regional.' . E4S_OLDLIVE_DOMAIN );
	define( 'E4S_AAI_DOMAIN', 'entry.athleticsireland.ie' );
}
include_once E4S_FULL_PATH . '/scoreboards/rostercommon.php';
include_once E4S_FULL_PATH . 'classes/socketClass.php';
include_once E4S_FULL_PATH . 'classes/rosterClass.php';

$scoreboard = 0;
if ( array_key_exists( 'scoreboard', $_GET ) ) {
	$scoreboard = 1;
}

if ( array_key_exists( 'name', $_GET ) ) {
	$id = $_GET['name'];
} else {
	$id = 'touchController';
}

$idStr = str_replace( ' ', '', $id );

if ( array_key_exists( 'mid', $_GET ) ) {
	$compId = $_GET['mid'];
} else {
	$rUri   = strtolower( $_SERVER['REQUEST_URI'] );
	$script = strtolower( $_SERVER['SCRIPT_NAME'] );
	if ( $rUri === $script ) {
		$compId = 0;
	} else {
		$compId = substr( $rUri, strlen( $script ) + 1 );
	}
}
$compId      = (int) $compId;
define( "ROSTER_DATA", ROSTER_URL . $compId );
$trackEvents = [];
$fieldEvents = [];
$headerBGColour = '#092238';
$headerColour = '#ffffff';
$tabBGColor = 'gray';
$gradientStart = '#1b557b';
$gradientEnd = '#0c2b46';
$gradientFill = 'linear-gradient(' . $gradientStart . ', ' . $gradientEnd . ')';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Roster Touch</title>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
	<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo E4S_PATH ?>/js/jquery-dateformat.min.js"></script>
	<style>
        body {
            background-color: <?php echo $headerBGColour ?>;
        }
		.header {
			background-color: <?php echo $headerBGColour ?>;
			color: <?php echo $headerColour ?>;
            font-family: Roboto,sans-serif;
			height: 1.25em;
            font-weight: 700;
            font-size: 1.5vw;
		}
        .compName {
            float: left;
            padding-left: 0.5em;
        }
        .compDate {
            font-size: 2em;
            color: gold;
        }
        .currentEventTitle {
            margin-left: 0.5em;
            color: white;
            font-size: 1.5em;
        }
        .notFirstDate {
            margin-top: 1em;
        }
        .orgName {
            float:right;
            padding-right: 0.5em;
        }
        #tabs-trackSL .ui-tabs-nav {
            margin: 5px 0 0 0 !important;
        }
        #tabs-fieldSL .ui-tabs-nav {
            margin: 5px 0 0 0 !important;
        }
        #tabs-trackR .ui-tabs-nav {
            margin: 5px 0 0 0 !important;
        }
        #tabs-fieldR .ui-tabs-nav {
            margin: 5px 0 0 0 !important;
        }
        ul {
            line-height: 1.5em;
            list-style-type: none;
        }
        .ui-tabs .ui-tabs-panel {
            padding: 0 !important;
        }
        .ui-tabs .ui-tabs-nav {
            padding: 0 !important;
        }
        .ui-tabs-vertical {  }
        .ui-tabs-vertical .ui-tabs-nav { padding: .2em .1em .2em .2em; float: left;  }
        .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
        .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
        .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; }
        .ui-tabs-vertical .ui-tabs-panel { padding: 0.5em; }
        }
        .ui-corner-all, .ui-corner-top, .ui-corner-left, .ui-corner-tl {
            border-top-left-radius: 0px !important;
        }
        .ui-corner-all, .ui-corner-top, .ui-corner-right, .ui-corner-tr {
            border-top-right-radius: 0px !important;
        }
        .ui-corner-all, .ui-corner-bottom, .ui-corner-left, .ui-corner-bl {
            border-bottom-left-radius: 0px !important;
        }
        .ui-corner-all, .ui-corner-bottom, .ui-corner-right, .ui-corner-br {
            border-bottom-right-radius: 0px !important;
        }
        .ui-state-active {
            background-color: var(--e4s-navigation-bar--primary__background) !important;
        }
        .eventSelected {
            color: yellow !important;
        }
        .eventName {
            font-size: 1.5em;
            font-weight: bold;
        }
        .eventTime {
            font-size: 1em;
            font-weight: normal;
        }
        .touchBody {
            display: flex;
            flex-direction: row;
            height: 86vh;
            width: 100%;
        }
        .touchContent {
            background-image: linear-gradient(#1b557b, #0c2b46);
            width: 99%;
            height: 98%;
            overflow-y: auto;
            overflow-x: hidden;
            margin-top: 1.5em;
        }
        .raceHeader {
            font-weight: bold;
            /*background-color: lightgray;*/
            font-size:  2em;
        }
        .teamTable {
            border-spacing: 0;
            display: flex;
            width: 98%;
            margin: 10px;
            padding: 10px;
            border: 1px solid black;
            background-image: linear-gradient(#ebe5c4, #f2f3e3);
        }
        .race{
            display: flex;
            flex-direction: column;
            margin: 10px;
            padding: 10px;
            border: 1px solid black;
            /*background-image: linear-gradient(#f7f7f3, #7cbdf5);*/
            background-image: linear-gradient(#ebe5c4, #f2f3e3);
        }
        .entryTitle {
            font-weight: bold;
            font-size: 0.75em;
            border-bottom: 2px solid #000000 !important;
        }
        .entryHeader {
            display: flex;
            width: 100%;
            border-bottom: 1px solid #dadada;
        }
        .teamLabels {
            font-weight: 800;
        }
        .teamLabelsTd {
            border-bottom: 2px solid black;
        }
        .teamData {
            font-size: 1.5em;
        }
        .teamDataPosition {
            width: 3vw;
            border-bottom: 1px grey dashed;
        }
        .teamDataTeam {
            width: 20vw;
            border-bottom: 1px grey dashed;
        }
        .teamDataScore {
            border-bottom: 1px grey dashed;
        }
        .teamTitle {
            display: flex;
            flex-direction: row;
            flex: 1;
            align-items: center;
            font-size: 1.25vw;
            padding: 8px 0;
            text-shadow: 1px 1px #e3e2ed;
        }
        .entry {
            display: flex;
            flex-direction: row;
            flex: 1;
            align-items: center;
            font-size: 1.25vw;
            padding: 8px 0;
            text-shadow: 1px 1px #e3e2ed;
        }
        .entryText{
            font-size: 0.75em;
            font-weight: normal;
        }
        .entrybibNo {
            max-width: 6%;
        }
        .entryPosition {
            max-width: 5%;
        }
        .entrylaneNo {
            max-width: 6%;
        }
        .entryname {
            max-width: 27%;
        }
        .entryclubNameResults {
            max-width: 27%;
        }
        .entryclubName {
            max-width: 37%;
        }
        .entrypb {
            max-width: 7%;
        }
        .entrysb {
            max-width: 7%;
        }
        .entryResult {
            max-width: 9%;
        }
        .entryQualify {
            max-width: 2%;
        }
        .touchNav .ui-widget.ui-widget-content {
            border: none;
        }
        .pageFooter {
            background-color: white;
            height: 3em;
            position: absolute;
            left: 0px;
            width: 100%;
        }
        .footer{
            color: black;
            font-size: 1.5em;
            display: flex;
            justify-content: space-around;
            flex-wrap: wrap;
            padding-top: 0.5em;
            font-style: italic;
            border-top: 6px solid #0c2b46;
        }
        div ul {
            border: none !important;
            background-color: <?php echo $gradientStart ?> !important;
        }
        .logo {
            height: 2em;
            float: right;
            position: sticky;
            padding-right: .5em;
            bottom: 1~0px;
        }
        .touchNav ul {
            background-color: transparent !important;
            color: white !important;
            font-family: Roboto,sans-serif;
            font-size: 1em;
            line-height: 2em;
            border: none !important;
            padding-left: 1em;
        }
        .touchNav {
            height: 98%;
            width: 100%;
            overflow-y: auto;
            overflow-x: hidden;
            background-image: <?php echo $gradientFill ?>;
            font-size: 0.75em;
            border-right: 2px solid white;
        }
        #touchNavigator .ui-state-active a {
            font-weight: bold;
            color: yellow;
            background-color: <?php echo $gradientStart ?> !important;
            font-size: 0.75em;
            padding: 0.25em;
        }
        .compInfo {
            color:white;
            font-weight:800;
            margin: 30px;
            font-size: 1.5em;
        }
        .ui-tabs .ui-tabs-nav .ui-tabs-anchor {
            font-size: 0.75em;
            padding: 0.25em;
        }
        #touchNavigator > ul {
            /*background-color: */<?php //echo $tabBGColor ?>/* !important;*/
            background-color: <?php echo $headerBGColour ?> !important;
        }
        #touchNavigator {
            border: none !important;
            padding: 0 !important;
            width: 33em;
            min-width: 27em;
            height: 100%;
        }
        .heat{
            margin-left: 40px;
        }
	</style>
	<?php
	include_once E4S_FULL_PATH . 'scoreboards/rosterJS.php';
	?>
	<script>
        let multiDayComp = false;
        let displayTimeout = 0;
        let teams = null;
        let scoreboard = <?php echo $scoreboard ?> === 1 ;
        let compName = '';

		<?php echo socketClass::outputJavascript() ?>
        function getCompetition() {
            return {"id":<?php echo $compId ?>}
        }

        function processSocketEvent(data) {
            let action = data.action;
            let compId = data.comp.id;
            let domain = data.domain;
            if (domain !== "<?php echo E4S_CURRENT_DOMAIN ?>") {
                return;
            }
            if (compId !== <?php echo $compId ?>) {
                return;
            }
            switch (action) {
                case ROSTER_SOCKET_RESULTS:
                    addEvent(data.payload, true);
                    break;
                case ROSTER_SOCKET_STARTLIST:
                    addEvent(data.payload);
                    break;
                case ROSTER_SOCKET_EVENT:
                    updateHeader(data.payload);
                    break;
            }
            if ( action === ROSTER_SOCKET_STARTLIST || action === ROSTER_SOCKET_RESULTS) {
                // update the tabs with debounce
                clearTimeout(displayTimeout);
                displayTimeout = window.setTimeout(displayEvents, 3000);
            }
        }

        function updateHeader(data){
            compName = data.name;
            let name = compName;
            if ( name.length > 50){
                name = new Date().toDateString();
            }
            $("#competitionName").text(name);
            $("#organisationName").text(data.organiser);
        }
        function checkMultiDay(aDate, bDate){
            if ( !multiDayComp ){
                let aDay = aDate.getDate();
                let bDay = bDate.getDate();
                if ( aDay !== bDay ){
                    multiDayComp = true;
                }
            }
        }

        function displayEvents(){
            // sort data on eventStart
            trackEvents.sort(function(a, b){
                let aDate = new Date(a.eventStart);
                let bDate = new Date(b.eventStart);
                checkMultiDay(aDate, bDate);
                return aDate - bDate;
            });
            fieldEvents.sort(function(a, b){
                let aDate = new Date(a.eventStart);
                let bDate = new Date(b.eventStart);
                checkMultiDay(aDate, bDate);
                if ( aDate - bDate === 0){
                    if (b.eventCode < a.eventCode ){
                        return -1;
                    }
                    if (b.eventCode > a.eventCode ){
                        return 1;
                    }
                    return 0;
                }
                return aDate - bDate;
            });
            let currentSelected = $(".eventSelected");
            displayStartandResults(trackEvents, ROSTER_TRACK);
            displayStartandResults(fieldEvents, ROSTER_FIELD);
            if ( currentSelected.length > 0 ){
                let id = currentSelected.attr("id");
                if ( id.indexOf("youtube") === -1 ) {
                    let eventId = parseInt(currentSelected.attr("eventid"));
                    let eventType = currentSelected.attr("eventtype");
                    showContent(id, eventId, eventType, false);
                }
            }
        }
        function showResults(id,eventId, eventType){
            showContent(id, eventId, eventType, true);
        }
        function showStartList(id, eventId, eventType) {
            showContent(id, eventId, eventType, false);
        }
        function sendToScoreboard(eventType,eventId, heatNo ){
            if ( !confirm("Send to Scoreboard ?") ) {
                return;
            }

            let events = trackEvents;
            if ( eventType === ROSTER_FIELD ){
                events = fieldEvents;
            }
            for(let e in events) {
                if (events[e].eventId === eventId) {
                    sendEventToScoreboard(eventType, events[e], heatNo);
                    break;
                }
            }
        }
        function sendEventToScoreboard(eventType, event, heatNo){
            let heatCount = 1;
            if ( event.entries.length > 1 ){
                heatCount = event.entries.length - 1;
            }
            let ageGroup = ageGroupTranslation(event.ageGroup);
            if ( event.multiAgeGroup ){
                ageGroup = "";
            }else{
                ageGroup = " " + ageGroup;
            }
            let eventGender = getEventGender(event);

            if (eventGender === "Open") {
                eventGender = "";
            } else if (eventGender !== "") {
                eventGender = " " + eventGender;
            }
            let data = {
                competition: {
                    id: getCompetition().id,
                    name: compName
                },
                domain: location.hostname,

                eventGroup: {
                    id: event.eventId,
                    eventno: 1,
                    typeno: '',
                    type: eventType.toUpperCase(),
                    date: event.eventStart,
                    event: event.eventName + ageGroup + eventGender,
                    heatCount: heatCount,
                    entries: []
                },

                config:{
                    pageSize: 8,
                    pageCycleMs: 5000
                },
                toScoreboard: "true"
            };
            let participants = [];
            let entries = event.entries[heatNo];

            let relay = false;
            let useLaneNos = true;
            for(let e in entries) {
                let entry = entries[e];
                if ( entry.relayName !== "" ){
                    relay = true;
                }
            }
            for(let e in entries) {
                let entry = entries[e];
                if ( entry.laneNo === 0 ){
                    // useLaneNos = false;
                }
                if (relay && entry.relayName === ""){
                    // dont get athletes, just teamnames
                    continue;
                }

                let fullName = entry.name;
                let firstName = fullName.split(" ")[0];
                let surname = fullName.substring(firstName.length).trim().toUpperCase();
                let athlete = {
                    id: 0,
                    firstname: firstName,
                    surname: surname,
                    gender: entry.gender,
                    dob: '',
                    urn: '',
                    county: '',
                    region: '',
                    agegroup: ageGroup,
                    ageGroupId: 0,
                    ageshortgroup: '',
                    classification: 0,
                    clubid: 0,
                    clubname: relay ? entry.relayName.replace(/_/g,' ') : entry.clubName,
                    maxgroup: 0,
                    bibno: entry.bibNo
                }
                let participant = {
                    laneNo: useLaneNos ? entry.laneNo : ' ',
                    heatNo: heatNo < 1 ? 1 : heatNo,
                    athlete: athlete
                }
                participants.push(participant);
            }
            data.participants = participants;

            e4sSocket.send(data, 'confirmHeat');
        }
        function showContent(id, eventId, eventType, results ){
            $(".eventSelected").removeClass("eventSelected");
            let currentSelected = $('#' + id);
            currentSelected.addClass("eventSelected");
            let events = trackEvents;
            let resultsClass = "";

            if ( results ){
                resultsClass = "Results";
            }
            let firstElement = 1;
            if ( eventType === ROSTER_FIELD ){
                events = fieldEvents;
                firstElement = 0;
            }
            let contentHTML = "";
            let relay = false;
            for(let e in events) {
                let event = events[e];
                let entries = event.entries;
                for( let a in entries ){
                    for( let b in entries[a] ) {
                        if (entries[a][b].relayName !== "") {
                            relay = true;
                        }
                    }
                }
            }
            for(let e in events) {
                let event = events[e];

                if (event.eventId !== eventId) {
                    continue;
                }
                let trackSeeded = true;
                let isHeat = event.heatFinal.toLowerCase() === "heat" ;
                let list = event.entries;
                if ( results ){
                    list = event.results;
                }
                if (typeof list[1] === "undefined") {
                    // if track, not seeded yet. Field usually has only 1 element unless scratch athlets or multiple pools
                    firstElement = 0;
                }
                if ( eventType === ROSTER_FIELD ) {
                    if (typeof list[1] !== "undefined") {
                        // first list are scratch athletes
                        firstElement++;
                    }
                }else{
                    if (typeof list[1] === "undefined"){
                        // trackSeeded = false;
                    }
                }
                let eventTitle = '<div id="currentEventTitle" class="currentEventTitle">' + (results ? "Results" : "Start List") + " : " + currentSelected.html() + '</div>';
                for(let heat in list){
                    heat = parseInt(heat);
                    if ( heat < firstElement ){
                        continue;
                    }
                    let entries = list[heat];
                    if ( eventType === ROSTER_FIELD ){
                        // sort entries on bibNo
                        entries.sort(function (a, b) {
                            return a.bibNo - b.bibNo;
                        });
                    }else {
                        if ( results ){
                            // sort entries on position
                            entries.sort(function (a, b) {
                                if ( a.position === 0 ){
                                    return 1;
                                }
                                if ( b.position === 0 ){
                                    return -1;
                                }
                                return a.position - b.position;
                            });
                            // check for same times
                            if ( eventType === ROSTER_TRACK ){
                                let lastResult = "";
                                let lastSub = 0;
                                for(let e in entries){
                                    if (entries[e].position > 0 ) {
                                        if (("" + entries[e].result) === lastResult) {
                                            entries[e].useFullResult = true;
                                            entries[lastSub].useFullResult = true;
                                        }
                                        lastResult = "" + entries[e].result;
                                        lastSub = e;
                                    }
                                }
                            }
                        }else {
                            // sort entries on laneno
                            entries.sort(function (a, b) {
                                return a.laneNo - b.laneNo;
                            });
                        }
                    }
                    contentHTML += eventTitle.replace("<br>","").replace("padding-left: 40px;","") + "<div class='race' id=" + eventId + "_" + heat + ">";
                    eventTitle = "";
                    if ( heat > 0 ){
                        // put out header
                        let eventTitle = "Race";
                        if ( eventType === ROSTER_FIELD ){
                            eventTitle = "Pool";
                        }
                        raceTitle = eventTitle + " " + heat;
                    }else{
                        raceTitle = "Final";
                    }
                    if ( !results && scoreboard ){
                        raceTitle = '<span onclick="sendToScoreboard(\'' + eventType + '\',' + eventId + ',' + heat + ');">' + raceTitle + '</span>';
                    }
                    contentHTML += "<div class='raceHeader'>" + raceTitle + "</div>";

                    contentHTML += "<div class='entryHeader entryTitle'>";
                    contentHTML += "<div class='entry entrybibNo'>Bib #</div>";
                    if ( results ){
                        contentHTML += "<div class='entry entryPosition'>Pos</div>";
                    }

                    if (eventType === ROSTER_TRACK && trackSeeded) {
                        contentHTML += "<div class='entry entrylaneNo'>Lane</div>";
                    }
                    if ( relay ){
                        contentHTML += "<div class='entry entryclubName" + resultsClass + "'>Organisation</div>";
                        contentHTML += "<div class='entry entryname'>Athlete</div>";
                    }else{
                        contentHTML += "<div class='entry entryname'>Athlete</div>";
                        contentHTML += "<div class='entry entryclubName" + resultsClass + "'>Organisation</div>";
                    }

                    contentHTML += "<div class='entry entrypb'>PB</div>";
                    contentHTML += "<div class='entry entrysb'>SB</div>";
                    if ( results ) {
                        contentHTML += "<div class='entry entryResult'>Result</div>";
                        if ( isHeat === true ) {
                            contentHTML += "<div class='entry entryQualify'>&nbsp;</div>";
                        }
                    }
                    contentHTML += "</div>";

                    for(let e in entries) {
                        let entry = entries[e];
                        if ( relay && (entry.relayName === "" || entry.name !== "" )){
                            continue;
                        }
                        let name = entry.name;
                        let bibNo = entry.bibNo;
                        let laneNo = entry.laneNo;
                        let clubName = entry.clubName;
                        if (  entry.relayName && entry.relayName !== "" ){
                            clubName = entry.relayName;
                        }
                        clubName = clubName.replace(/_/g, " ");
                        let classification = entry.fieldClassification;
                        let pb = ensureDecimals(entry.pb);
                        let sb = ensureDecimals(entry.sb);

                        contentHTML += "<div class='entryHeader'>";
                        contentHTML += "<div class='entry entrybibNo'>" + bibNo + "</div>";
                        if ( results === true ){
                            let pos = entry.position;
                            if ( pos === 0 ){
                                pos = entry.startStatus;
                            }
                            contentHTML += "<div class='entry entryPosition'>" + pos + "</div>";
                        }
                        if (eventType === ROSTER_TRACK) {
                            if (trackSeeded) {
                                contentHTML += "<div class='entry entrylaneNo'>" + laneNo + "</div>";
                            }
                            classification = entry.trackClassification;
                        }
                        if ( relay ){
                            contentHTML += "<div class='entry entryclubName" + resultsClass + "'>" + clubName + "</div>";
                            contentHTML += "<div class='entry entryname'>" + getAthleteName(name, classification) + "</div>";
                        }else{
                            contentHTML += "<div class='entry entryname'>" + getAthleteName(name, classification) + "</div>";
                            contentHTML += "<div class='entry entryclubName" + resultsClass + "'>" + clubName + "</div>";
                        }

                        contentHTML += "<div class='entry entrypb'>" + pb + "</div>";
                        contentHTML += "<div class='entry entrysb'>" + sb + "</div>";
                        if ( results ) {
                            let qualify = entry.qualify;
                            contentHTML += "<div class='entry entryResult'>" + getResult(entry) + "</div>";
                            if ( isHeat ) {
                                contentHTML += "<div class='entry entryQualify'>" + qualify + "</div>";
                            }
                        }
                        contentHTML += "</div>";
                    }
                    contentHTML += "</div>";
                }
            }
            $("#touchContent").html(contentHTML);
        }
        function getResult(entry){
            let displayResult = entry.result;
            let useRound = true;
            if ( entry.useFullResult ){
                useRound = false;
            }
            if ( "" + entry.resultRounded !== "" && useRound ){
                displayResult = entry.resultRounded;
            }
            displayResult = ensureDecimals(displayResult);
            if ( entry.resultRecord !== "" ){
                displayResult += '<span class="entry entryText">&nbsp;' + entry.resultRecord + '</span>' ;
            }
            return displayResult;
        }
        function ensureDecimals(result){
            let resultParts = ("" + result).split(".");
            if ( resultParts.length > 1 ){
                if ( resultParts[resultParts.length - 1].length === 1 ) {
                    result += "0";
                }
            }else if ("" + result !== ""){
                result += ".00";
            }
            return result;
        }
        function getAthleteName(name, classification){
            if ( classification !== "" ){
                name += " (" + classification + ")";
            }
            return name;
        }
        function getCompDate(eventDate, firstDate){
            let compDateClass = "";
            if ( !firstDate ){
                compDateClass = "notFirstDate";
            }
            return '<li class="' + compDateClass + '"><span class="compDate">' + formatDate(eventDate,"ddd D MMM") + '</span></li>';
        }
        function ageGroupTranslation(ageGroup){
            let disp = ageGroup;
            disp = disp.replace("Meeting_", "U");
            return disp;
        }
        function getAgeGroupDisplay(event){
            let ageGroupDisplay = " ";
            let minAgeGroup = ageGroupTranslation(event.ageGroup);
            let maxAgeGroup = ageGroupTranslation(event.oldestAgeGroup);
            if ( event.multiAgeGroup ){
                ageGroupDisplay += minAgeGroup + " - " + maxAgeGroup;
            }else{
                ageGroupDisplay += minAgeGroup;
            }
            return ageGroupDisplay;
        }
        function displayStartandResults(events, eventType){
            let startHTML = "<ul>";
            let resultsHTML = "<ul>";
            let lastDate = "";
            let startCompDate = "";
            let resultCompDate = "";
            for(let e in events){
                let event = events[e];

                let startListPrefix = "sl_";
                let resultPrefix = "r_";
                let title = event.title;
                let eventId = event.eventId;
                let eventCode = event.eventCode;
                let eventName = '';

                let heatFinal = event.heatFinal;
                let eventStart = event.eventStart;
                let eventStartDate = new Date(eventStart);
                let eventStartText = eventStartDate.getHours() + ":" + right("0" + eventStartDate.getMinutes(),2);
                if ( event.eventName ){
                    eventName = event.eventName;
                }else{
                    eventName = getEventName("" + eventCode);
                }
                eventName += getAgeGroupDisplay(event);
                if (multiDayComp && lastDate !== eventStartDate.toDateString()){
                    let compDate = getCompDate(eventStartDate, lastDate === "");
                    lastDate = eventStartDate.toDateString();
                    startCompDate = compDate;
                    resultCompDate = compDate;
                }

                let onClick = '';
                let linkIcon = '<span class="linkIcon">&gt; </span>';
                linkIcon = '';
                let eventGender = getEventGender(event);

                if (eventGender === "Open") {
                    eventGender = "";
                } else if (eventGender !== "") {
                    eventGender = " " + eventGender;
                }
                let eventText = getDisplayEventName(title, eventName, eventGender,heatFinal,event, false);

                onClick = 'onclick="showStartList(\'' + startListPrefix + eventId + '\',' + eventId + ',\'' + eventType + '\');"';

                let eventHtml1 = '<li><span id="';
                let eventHtml2 = eventId + '" eventtype="' + eventType + '"  eventid="' + eventId + '"';
                let eventHtml3 = '><span class="eventTime">' + eventStartText + ' - </span><span class="eventName">' + linkIcon + eventText + '</span></li>';

                startHTML += startCompDate + (eventHtml1 + startListPrefix + eventHtml2 + onClick + eventHtml3);
                startCompDate = "";
                if ( eventHasValidResults (event) ){
                    onClick = 'onclick="showResults(\'' + resultPrefix + eventId + '\',' + eventId + ',\'' + eventType + '\');"';
                    resultsHTML += resultCompDate + (eventHtml1 + resultPrefix + eventHtml2 + onClick + eventHtml3);
                    resultCompDate = "";
                }
            }
            startHTML += "</ul>";
            resultsHTML += "</ul>";
            if (eventType === ROSTER_TRACK) {
                $("#tabs-trackSL").html(startHTML);
                $("#tabs-trackR").html(resultsHTML);
            }  else{
                $("#tabs-fieldSL").html(startHTML);
                $("#tabs-fieldR").html(resultsHTML);
            }
        }
        function eventHasValidResults (event){
            if ( event.results ) {
                if (new Date(event.eventStart) < new Date()) {
                    for (let h in event.results) {
                        let heat = event.results[h];
                        for (let r in heat) {
                            let result = heat[r];

                            if (result.result !== "") {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        function addEvent(data, resultEvent = false){
            let events = trackEvents;
            let eventType = isTF(data);
            if ( eventType === ROSTER_FIELD ){
                events = fieldEvents;
            }

            let included = false;
            for(let e in events){
                let event = events[e];
                if (event.eventId === data.eventId){
                    event.eventName = data.eventName;
                    included = true;
                    if ( !resultEvent) {
                        // results come in first ????
                        event.entries = JSON.parse(JSON.stringify(data.entries));
                    }else{
                        console.log("Results for " + eventType + " : " + event.eventId + ":" + data.eventName + " found at element (" + e + ")");
                        event.results = JSON.parse(JSON.stringify(data.results));
                    }
                }
            }
            if (!included ){
                if ( !resultEvent) {
                    events.push(data);
                }else{
                    console.log("Results for " + data.eventName + " not found in start list");
                }
            }
        }
        function formatDate(date, format) {
            /*
yy = short year
yyyy = long year
M = month (1-12)
MM = month (01-12)
MMM = month abbreviation (Jan, Feb ... Dec)
MMMM = long month (January, February ... December)
d = day (1 - 31)
dd = day (01 - 31)
ddd = day of the week in words (Monday, Tuesday ... Sunday)
E = short day of the week in words (Mon, Tue ... Sun)
D - Ordinal day (1st, 2nd, 3rd, 21st, 22nd, 23rd, 31st, 4th...)
h = hour in am/pm (0-12)
hh = hour in am/pm (00-12)
H = hour in day (0-23)
HH = hour in day (00-23)
mm = minute
ss = second
SSS = milliseconds
a = AM/PM marker
p = a.m./p.m. marker
			 */
            return $.format.date(date, format);
        }
        function showYoutube(day){
            let youTubeBase = "#youtubeLink";
            let youTube = youTubeBase + day;
            $(youTubeBase + "1").removeClass("eventSelected");
            $(youTubeBase + "2").removeClass("eventSelected");
            $(youTubeBase + "3").removeClass("eventSelected");
            $(youTube).addClass("eventSelected");
            $("#callRoomLink").removeClass("eventSelected");
            $("#compInfoLink").removeClass("eventSelected");
            let url = 'https://youtube.com/embed/c2jrCMZrmh0?autoplay=1&mute=1';
            if ( day === 2 ){
                url = 'https://youtube.com/embed/Qo0h-9gJVdE?autoplay=1&mute=1';
            }
            if ( day == 3 ){
                url = 'https://youtube.com/embed/fZwdep1dtyc?autoplay=1&mute=1';
            }
            $('#touchContent').html('<iframe style="width: 100%;height: 99%;background-color: white;" src="' + url + '" allow="autoplay"></iframe>');
        }
        function showCallRoom(){
            $("#youtubeLink").removeClass("eventSelected");
            $("#callRoomLink").addClass("eventSelected");
            $("#compInfoLink").removeClass("eventSelected");

            $("#touchContent").html('<iframe style="width: 100%;height: 99%;background-color: white;" src="/resources/LIA/Call room Schedule v5.3.htm"></iframe>');
        }
        function showCompInfo(){
            $("#compInfoLink").addClass("eventSelected");
            $("#callRoomLink").removeClass("eventSelected");
            $("#youtubeLink").removeClass("eventSelected");
            let html = '<b>No Information Available</b>';
            if ( teams !== null ){
                html = getTeamInfo();
            }
            // $("#touchContent").html('<iframe style="width: 100%;height: 99%;background-color: white;" src="/resources/BUCS/BUCS-comp-info.htm"></iframe>');
            $("#touchContent").html(html);
        }
        function getTeamScores(){
            teams = null;
            getEventTeamScores(trackEvents);
            getEventTeamScores(fieldEvents);
        }
        function getEventTeamScores(events){
            for(let e in events){
                let event = events[e];
                if ( !event.results ){
                    continue;
                }
                let results = event.results;
                for(let r in results ){
                    let heat = results[r];
                    for(let hr in heat ){
                        let heatResult = heat[hr];
                        if ( heatResult.teamName !== "" ){
                            addToTeamScores(heatResult.teamName, heatResult.teamGender, heatResult.teamScore);
                        }
                    }
                }
            }
        }
        function addToTeamScores(teamName, teamGender, score){
            if ( teams === null ){
                teams = [];
            }
            if ( !teams[teamGender]) {
                teams[teamGender] = [];
            }
            if ( !teams[teamGender][teamName] ){
                teams[teamGender][teamName] = score;
            }else{
                teams[teamGender][teamName] += score;
            }
        }
        function getTeamInfo(){
            getTeamScores();
            if (teams === null ){
                return '';
            }
            // sort for overall
            teams.overall = [];
            for(let m in teams.Male){
                // teams.overall.push(JSON.parse(JSON.stringify(teams.Male[m])));
                teams.overall[m] = teams.Male[m];
            }
            for(let f in teams.Female){
                let fTeam = teams.Female[f];
                let teamFound = null;
                for(let o in teams.overall){
                    let oTeam = teams.overall[o];
                    if ( o === f ){
                        // mark overall team found
                        teamFound = oTeam;
                    }
                }
                if ( teamFound === null ){
                    // should not happen, but if female have a team not in males, add it
                    teams.overall[f] = fTeam;
                }else{
                    // add the points to the overall
                    teams.overall[f] += fTeam;
                }
            }

            let html = '<table class="teamTable">';
            html += getTeamHtml('Overall', teams.overall);
            html += getTeamHtml('Men', teams.Male);
            html += getTeamHtml('Women', teams.Female);
            html += '</table>';
            return html;
        }
        function getTeamHeader(title){
            let html = '<tr><td class="teamTitle" colspan=3>' + title + '</td></tr>';
            html += '<tr class="teamLabels"><td class="teamLabelsTd">Position</td><td class="teamLabelsTd">Team</td><td class="teamLabelsTd">Points</td></tr>';

            return html;
        }
        function getTeamHtml(title, teamObj){
            let teamArr = [];
            for(let teamName in teamObj){
                let obj = {};
                obj.name = teamName;
                obj.score = teamObj[teamName];
                teamArr.push(obj);
            }
            // sort teams.overall
            teamArr.sort(function(a, b){
                return b.score - a.score;
            });
            let html = getTeamHeader(title);
            let lastTeamScore = -1;
            let lastPosition = -1;
            let position = 1;
            let teamPosition = position;
            for(let t in teamArr){
                let teamName = teamArr[t].name;
                let teamScore = teamArr[t].score;
                teamPosition = position;
                if ( lastPosition > 0 ){
                    if ( lastTeamScore === teamScore ){
                        teamPosition = lastPosition;
                    }
                }
                lastTeamScore = teamScore;
                lastPosition = teamPosition;
                position++;
                if (parseInt(teamScore) === 0){
                    teamPosition = '-';
                }
                html += '<tr class="teamData"><td class="teamDataPosition">' + teamPosition + '</td><td class="teamDataTeam">' + teamName + '</td><td class="teamDataScore">' + teamScore + '</td></tr>';
            }
            return html;
        }
        function showTime(){
            var date = new Date();
            var h = date.getHours(); // 0 - 23
            var m = date.getMinutes(); // 0 - 59
            var s = date.getSeconds(); // 0 - 59
            var session = "";

            if(h == 0){
                h = 12;
            }

            if(h > 12){
                // h = h - 12;
                session = "";
            }

            h = (h < 10) ? "0" + h : h;
            m = (m < 10) ? "0" + m : m;
            s = (s < 10) ? "0" + s : s;

            var time = h + ":" + m + ":" + s ;
            document.getElementById("MyClockDisplay").innerText = time;
            document.getElementById("MyClockDisplay").textContent = time;

            setTimeout(showTime, 1000);
        }

        function initTouch(data){
            if ( data ){
                for(let sl in data.startList){
                    let event = data.startList[sl];
                    addEvent(event);
                }
                for(let r in data.results){
                    let event = data.results[r];
                    addEvent(event, true);
                }
                teams = data.teams;
                updateHeader(data.meeting);
                displayEvents();
            }
        }
        function getCurrentData(){
            // get data every 2 minutes
            setInterval(function(){
                getInitialData(initTouch, true)
            }, getThousandthsForMinutes(2));
        }
		$(function() {
			$( "#touchNavigator" ).tabs();
			$( "#tabs-trackSL" ).tabs({
            }).addClass( "ui-tabs-vertical ui-helper-clearfix" );
            $( "#tabs-trackSL li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
            $( "#tabs-fieldSL" ).tabs({
            }).addClass( "ui-tabs-vertical ui-helper-clearfix" );
            $( "#tabs-fieldSL li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
            $( "#tabs-trackR" ).tabs({
            }).addClass( "ui-tabs-vertical ui-helper-clearfix" );
            $( "#tabs-trackR li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
            $( "#tabs-fieldR" ).tabs({
            }).addClass( "ui-tabs-vertical ui-helper-clearfix" );
            $( "#tabs-fieldR li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
            $("li").show(); // show all events
            showTime();
            getInitialData(initTouch);
            // setInterval(function(){getCookieData()}, 1800000);
		} );

    </script>

</head>
<body oncontextmenu="return false;" style="overflow: hidden">
<table>
    <tr>
        <td id="header" class="header">
            <table style="width:100vw">
                <tr>
                    <td style="width:33vw; text-align: left">
                        <span id="competitionName" class="compName">Event Information</span>
                    </td>
                    <td style="width:33vw; text-align: center">
                        <span id="MyClockDisplay" class="clock" ></span>
                    </td>
                    <td style="width:33vw; text-align: right">
                        <span id="organisationName" class="orgName">Tech4Sports</span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="touchBody">
            <div id="touchNavigator">
                <ul>
                    <li><a href="#tabs-trackSL">Track Start List</a></li>
                    <li><a href="#tabs-trackR">Track Results</a></li>
<!--                    <li><a href="#tabs-fieldSL">Field Start List</a></li>-->
<!--                    <li><a href="#tabs-fieldR">Field Results</a></li>-->
<!--                    <li><a href="#tabs-info" onclick="showYoutube(3); return false;">Information</a></li>-->
                </ul>
                <div id="touchNav" class="touchNav">
                    <div id="tabs-trackSL">
                        <div style="color:white; font-weight:800; margin: 30px;">
                            Please wait. Events are loading.............
                        </div>
                    </div>
<!--                    <div id="tabs-fieldSL">-->
<!--                        <div style="color:white; font-weight:800; margin: 30px;">-->
<!--                            Please wait. Events are loading.............-->
<!--                        </div>-->
<!--                    </div>-->
                    <div id="tabs-trackR">
                        <div style="color:white; font-weight:800; margin: 30px;">
                            Please wait. Awaiting results .............
                        </div>
                    </div>
<!--                    <div id="tabs-fieldR">-->
<!--                        <div style="color:white; font-weight:800; margin: 30px;">-->
<!--                            Please wait. Awaiting results .............-->
<!--                        </div>-->
<!--                    </div>-->
                    <div id="tabs-info" style="display: none">
<!--                        <div id="compInfoLink" class="compInfo" onclick="showCompInfo(); return false;">-->
<!--                            Team Points-->
<!--                        </div>-->

<!--                        <div id="callRoomLink" class="compInfo" onclick="showCallRoom(); return false;">-->
<!--                            Call Room Schedule-->
<!--                        </div>-->

                        <div id="youtubeLink1" class="compInfo" onclick="showYoutube(1); return false;">
                            Live Stream Day 1
                        </div>
                        <div id="youtubeLink2" class="compInfo" onclick="showYoutube(2); return false;">
                            Live Stream Day 2
                        </div>
                        <div id="youtubeLink3" class="compInfo" onclick="showYoutube(3); return false;">
                            Live Stream Day 3
                        </div>
                    </div>
                </div>
            </div>
            <div id="touchContent" class="touchContent">

            </div>
        </td>
    </tr>
    <tr>
        <td id="pageFooter" class="pageFooter">
            <span class="footer">Content provided by Roster Athletics and made available on here by Tech4Sports Ltd</span>
            <a href="#" onclick="location.reload();"><img src="/resources/t4s_logo.gif" alt="Tech4Sports Logo" class="logo"/></a>

        </td>
    </tr>
</table>
</body>
</html>
<?php
//e4s_fireAndForget("https://" . E4S_CURRENT_DOMAIN . E4S_ROUTE_PATH . '/public/roster' );

