<script>
const ROSTER_SOCKET_RESULTS = "<?php echo ROSTER_SOCKET_RESULTS ?>";
const ROSTER_SOCKET_STARTLIST = "<?php echo ROSTER_SOCKET_STARTLIST ?>";
const ROSTER_SOCKET_EVENT = "<?php echo ROSTER_SOCKET_EVENT ?>";
const ROSTER_TRACK = "<?php echo ROSTER_TRACK ?>";
const ROSTER_FIELD = "<?php echo ROSTER_FIELD ?>";
const SOCKET_CONTROLLER_RESPONSE = 'ctrl_response';
const SOCKET_CONTROLLER_REQUEST = 'ctrl_request';
const SOCKET_CONTROLLER_SHOW = 'ctrl_show';
const SOCKET_SEND_UPDATE = 'ctrl_update';
const SOCKET_REQUEST_PARENT = 'req_parent';
const E4S_WINDOW_START_TYPE = 'e4sStartList';
const E4S_WINDOW_CALL_TYPE = 'e4sCallRoom';
const E4S_FIELD_DISCIPLINE = '<?php echo E4S_FIELD_DISCIPLINE ?>';
const E4S_TRACK_DISCIPLINE = '<?php echo E4S_TRACK_DISCIPLINE ?>';
const E4S_OUTPUT_START = '<?php echo E4S_OUTPUT_START ?>';
const E4S_OUTPUT_RESULTS = '<?php echo E4S_OUTPUT_RESULTS ?>';
const E4S_OUTPUT_CALLROOM = '<?php echo E4S_OUTPUT_CALLROOM ?>';
const E4S_OUTPUT_SCHEDULE = '<?php echo E4S_OUTPUT_SCHEDULE ?>';
const E4S_OUTPUT_TOUCH = '<?php echo E4S_OUTPUT_TOUCH ?>';

function getThousandthsForMinutes(mins){
    return mins * 60000;
}
function getEventName(eventCode){
	if ( eventCode.indexOf("SP") > -1){
		return "Shotput";
	}
	if ( eventCode.indexOf("HJ") > -1){
		return "High Jump";
	}
	if ( eventCode.indexOf("LJ") > -1){
		return "Long Jump";
	}
	if ( eventCode.indexOf("TJ") > -1){
		return "Triple Jump";
	}
	if ( eventCode.indexOf("PV") > -1){
		return "Pole Vault";
	}
    if ( eventCode.indexOf("HT") > -1){
		return "Hammer";
	}
	if ( eventCode.indexOf("60H") > -1){
		return "60m Hurdles";
	}
	if ( eventCode.indexOf("60") > -1){
		return "60m";
	}
	if ( eventCode.indexOf("100H") > -1){
		return "100m Hurdles";
	}
	if ( eventCode.indexOf("100") > -1){
		return "100m";
	}
	if ( eventCode.indexOf("800") > -1){
		return "800m";
	}
	if ( eventCode.indexOf("110H") > -1){
		return "110m Hurdles";
	}
	if ( eventCode.indexOf("400H") > -1){
		return "400m Hurdles";
	}
	if ( eventCode.indexOf("400") > -1){
		return "400m";
	}
	if ( eventCode.indexOf("4x100") > -1){
		return "4x100m Relay";
	}
	if ( eventCode.indexOf("4x400") > -1){
		return "4x400m Relay";
	}
    if ( eventCode.indexOf("4x1") > -1){
        return "4x100m Relay";
    }
    if ( eventCode.indexOf("4x4") > -1){
        return "4x400m Relay";
    }
	if ( eventCode.indexOf("4x200") > -1){
		return "4x200m Relay";
	}
	return eventCode;
}
function getEventGender(event){
    let eventGender = "";
    let races = event.entries;
    for ( let r in races ){
        r = parseInt(r);
        if ( eventGender !== "Open" ) {
            for (let e in races[r]) {
                let entry = races[r][e];
                if ( eventGender === "") {
                    eventGender = entry.gender;
                } else if (eventGender !== entry.gender){
                    eventGender = "Open";
                }
            }
        }
    }
    return eventGender;
}
function ageGroupTranslation(ageGroup){
    let disp = ageGroup;
    disp = disp.replace("Meeting_", "U");
    return disp;
}
function getAgeGroupDisplay(event){
    let ageGroupDisplay = " ";
    let minAgeGroup = ageGroupTranslation(event.ageGroup);
    let maxAgeGroup = ageGroupTranslation(event.oldestAgeGroup);
    if ( event.multiAgeGroup ){
        ageGroupDisplay += minAgeGroup + " - " + maxAgeGroup;
    }else{
        ageGroupDisplay += minAgeGroup;
    }
    return ageGroupDisplay;
}
function getDisplayEventName(title, eventName, eventGender, heatFinal, event, isCallRoom = false){
    let closeSpan = "";
    if ( title !== "" && !isCallRoom ){
        title = title + "<br><span style='padding-left: 40px;'>";
        closeSpan = "</span>";
    }
    if ( title === eventName ){
        title = "";
    }else{
        title = title + " ";
    }
    let ageGroup = ageGroupTranslation(event.ageGroup);
    if ( event.multiAgeGroup ){
        ageGroup = ageGroup + " - " + ageGroupTranslation(event.oldestAgeGroup);
    }
    return title + eventName + " " + heatFinal + " - " + eventGender.trim() + " " + ageGroup + closeSpan;
}

function right(str, chr){
    return str.slice(str.length-chr,str.length);
}
function left(str, chr){
    return str.slice(0, chr - str.length);
}
function isTF(data){
    if ( !isNaN(Number(("" + data.eventCode)[0])) ) {
        return ROSTER_TRACK;
    }
    if ( data.eventCode.indexOf("Mile") > -1){
        return ROSTER_TRACK;
    }
    return ROSTER_FIELD;
}
function getInitialData(fn = null, $lastFile = false){
    let action = "";
    if ( $lastFile){
        action = "<?php echo ROSTER_SOCKET_LAST ?>";
    }
    jQuery.ajax({
        url: "<?php echo ROSTER_DATA ?>?action=" + action,
        async: true,
        complete: function(response){
            if ( fn ) {
                fn(response.responseJSON.data);
            }
        }
    });
}
function getCookieData(){
    jQuery.ajax({
        url: "<?php echo ROSTER_DATA ?>?action=<?php echo ROSTER_SOCKET_COOKIE ?>",
        async: true
    });
}
function getStartListData(){
    jQuery.ajax({
        url: "<?php echo ROSTER_DATA ?>?action=<?php echo ROSTER_SOCKET_STARTLIST ?>",
        async: false
    });
}
function getResultsData(){
    jQuery.ajax({
        url: "<?php echo ROSTER_DATA ?>?action=<?php echo ROSTER_SOCKET_RESULTS ?>",
        async: false
    });
}


let trackEvents = new Array();
let fieldEvents = new Array();
</script>