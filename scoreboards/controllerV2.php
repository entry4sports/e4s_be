<?php
/*
 * To get a schedule. get the pdf version from Roster,
 * Convert to HTML online, Create a Fri,sat, sunday version
 * modify the css to use responsive tables and the show in Chrome and use the autoscroll extension
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'scoreboards/rostercommon.php';
include_once E4S_FULL_PATH . 'classes/rosterClass.php';


if ( array_key_exists( 'name', $_GET ) ) {
	$id = $_GET['name'];
} else {
	$id = 'Controller';
}
$idStr = str_replace( ' ', '', $id );
if ( array_key_exists( 'mid', $_GET ) ) {
	$mId = $_GET['mid'];
} else {
	$rUri   = strtolower( $_SERVER['REQUEST_URI'] );
	$script = strtolower( $_SERVER['SCRIPT_NAME'] );
	if ( $rUri === $script ) {
		$mId = 0;
	} else {
		$mId = substr( $rUri, strlen( $script ) + 1 );
	}
}
$mId = (int) $mId;
define( "ROSTER_DATA", ROSTER_URL . $mId );
$delay = 15;

if ( array_key_exists( 'delay', $_GET ) ) {
	$delay = $_GET['delay'];
}
if ( is_numeric( $delay ) == FALSE ) {
	$delay = 10;
} else {
	$delay = (int) $delay;
}

if ( $mId !== 0 ) {
	$trackEvents = [];
	$fieldEvents = [];
	$adverts     = readAdverts( $mId );
} else {
	$trackEvents = array();
	$trackEvent  = new stdClass();

	$trackEvent->name                 = "Demo Track 1";
	$trackEvent->startmeId            = 1;
	$trackEvent->heats                = 3;
	$trackEvents[ $trackEvent->name ] = $trackEvent;

	$trackEvent->name                 = "Demo Track 2";
	$trackEvent->startmeId            = 5;
	$trackEvent->heats                = 3;
	$trackEvents[ $trackEvent->name ] = $trackEvent;

	$fieldEvents                      = array();
	$fieldEvent                       = new stdClass();
	$fieldEvent->name                 = "Demo Field 1";
	$fieldEvent->startmeId            = 10;
	$fieldEvent->heats                = 5;
	$fieldEvents[ $fieldEvent->name ] = $fieldEvent;

	$fieldEvent->name                 = "Demo Field 2";
	$fieldEvent->startmeId            = 16;
	$fieldEvent->heats                = 5;
	$fieldEvents[ $fieldEvent->name ] = $fieldEvent;
	$adverts                          = array();
	$adverts[]                        = "https://entry4sports.com";
}

?>
    <!DOCTYPE html>
    <html>
    <head>
        <title>Controller : <?php echo $id ?></title>
        <style>
            body div #e4s-nav-bar {
                height: 100px !important;
            }
            .clock {
                color: blue;
                font-size: 10vh;
                font-family: Orbitron;
                letter-spacing: 7px;
            }
            .e4sTitle {
                color: white;
                font-size: 2vw;
            }

            .eventName {
                font-size: 1.5vw;
                font-weight: 900;
            }

            .config {
                margin: 10px;
            }

            section {
                display: table;
                width: 100%;
            }

            section > * {
                display: table-row;
                height: 30px;
            }

            section .configCol {
                display: table-cell;
            }

            .content {
                margin: 0px 50px;
                flex-direction: row;
                display: flex;
                flex-wrap: nowrap;
                align-items: flex-start;
                gap: 5vw;
                height: 70vh;
            }

            .<?php echo E4S_TRACK_DISCIPLINE ?>Content {
                display: flex;
                flex-direction: column;
                flex-wrap: nowrap;
                align-items: flex-start;
                height: 100%;
                overflow-y: scroll;
                gap: 2vh;
            }

            .<?php echo E4S_FIELD_DISCIPLINE ?>Content {
                display: flex;
                flex-direction: column;
                flex-wrap: nowrap;
                align-items: flex-start;
                height: 100%;
                overflow-y: scroll;
                gap: 2vh;
            }
            .contentGroup {
                height:85%;
                width: 45%;
            }
            .eventsTitle {
                font-size: 1.5vw;
                font-weight: 900;
                padding-bottom: 2vh;
            }

            button {
                -webkit-appearance: button;
                background: var(--e4s-button--primary__background);
                height: var(--e4s-button__height);
                padding: 0px 10px 0px 10px;
                border: var(--e4s-button__border);
                border-radius: var(--e4s-button__border-radius);
                cursor: pointer;
                /* transition: all 0.36s; */
                color: white;
                font-weight: 500;
            }

            .warnButton {
                background: var(--e4s-status-pill--error__background);
                color: var(--e4s-status-pill--error__text-color);
            }

            .e4sNumber {
                width: 4vw;
                padding: 0px 5px 0px 5px;
            }
        </style>
        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
	    <?php
	    include_once E4S_FULL_PATH . 'scoreboards/rosterJS.php';
	    ?>
        <script>

            let windowParms = "titlebar=0,toolbar=0,location=0,menubar=0,status=0,scrollbars=0";
            let controllers = [];
            let adverts = [];

			<?php
			if ( $adverts !== null ) {
				echo "adverts = " . json_encode( $adverts ) . ";\n";
			}
			?>


            function e4sAlert(msg, title) {
                alert(msg, title);
            }

            function showPleaseWait() {
            }

            function getCompetition() {
                return {
                    id: <?php echo $mId; ?>,
                    date: new Date().toISOString().split('T')[0]
                }
            }

			<?php socketClass::outputJavascript() ?>
            function processSocketEvent(data) {
                switch (data.action) {
                    case E4S_SOCKET_INIT:
                        break;
                    case SOCKET_CONTROLLER_REQUEST:
                        responseToControllerRequest(data.payload);
                        break;
                    case SOCKET_CONTROLLER_RESPONSE:
                        processResponse(data.payload);
                        break;
                    case SOCKET_SEND_UPDATE:
                        updateChildController(data.payload);
                        break;
                    case SOCKET_REQUEST_PARENT:
                        updateChild();
                        break;
                    case SOCKET_CONTROLLER_SHOW:
                        showOnChildController(data.payload);
                        break;
                    case ROSTER_SOCKET_STARTLIST:
                        addStartListEvent(data.payload);
                        break;
                }
            }

            let eventId = 1;

            function addStartListEvent(data, callDisplay = true){
                let eventName = getEventName("" + data.eventCode);
                let eventStartDate = new Date(data.eventStart);
                let dateNow = new Date();
                let eventTitle = data.title;
                if (eventTitle === "" ){
                    eventTitle = eventName;
                }
                let events = trackEvents;
                let heatCount = 1;
                if ( isTF(data) === ROSTER_FIELD ){
                    events = fieldEvents;
                }else{
                    for(let h in data.entries ){
                        heatCount = parseInt(h);
                        if ( heatCount === 0 ){
                            heatCount = 1;
                        }
                    }
                }

                let eventGender = getEventGender(data);
                let heatFinal = data.heatFinal;
                let eventNameDisplay = getDisplayEventName(eventTitle, eventName, eventGender, heatFinal, data, true);
                let eventStartText = eventStartDate.getHours() + ":" + right("0" + eventStartDate.getMinutes(),2);
                eventNameDisplay = eventStartText + "-" + eventNameDisplay;
                if ( !events[eventNameDisplay] ) {
                    events[eventNameDisplay] = {
                        name: eventNameDisplay,
                        startmeId: 1,
                        id: eventId++,
                        startTime: data.eventStart,
                        heats: heatCount
                    };
                }
                if ( callDisplay ) {
                    debounceDisplay();
                }
            }
            function getParentControllerObj() {
                return $("#parentController");
            }

            function getParentControllerName() {
                return getParentControllerObj().val();
            }

            function setParentControllerName(value) {
                if (value === "") {
                    return;
                }
                let parentObj = getParentControllerObj();
                // if does not have value as option then add it
                if (parentObj.find("option[value='" + value + "']").length === 0) {
                    let option = "<option value='" + value + "'>" + value + "</option>";
                    parentObj.append(option);
                }
                if (value !== parentObj.val()) {
                    parentObj.val(value);
                    window.setTimeout(parentChange, 500);
                }
            }

            function getControllerObj() {
                return $("#controllerName");
            }

            function getControllerName() {
                return getControllerObj().val();
            }

            function getEventDelayObj() {
                return $("#delay");
            }

            function getEventDelay() {
                return parseInt(getEventDelayObj().val());
            }

            function setEventDelay(delay) {
                getEventDelayObj().val(delay);
            }

            function getAdvertDelayObj() {
                return $("#advertdelay");
            }

            function setAdvertDelay(delay) {
                getAdvertDelayObj().val(delay);
            }

            function getAdvertDelay() {
                return parseInt(getAdvertDelayObj().val());
            }

            function getAdvertCountObj() {
                return $("#advert");
            }

            function setAdvertCount(count) {
                getAdvertCountObj().val(count);
            }

            function getAdvertCount() {
                return parseInt(getAdvertCountObj().val());
            }

            function showOnChildController(data){
                if (data.parentController === getParentControllerName()) {
                    if (getOutputDisplay() ) {
                        let target = data.target;
                        let url = data.url;
                        advertIndex = data.advertIndex;
                        let frame = '';
                        if (target === E4S_WINDOW_START_TYPE) {
                            frame = getFrameName();
                        }
                        if (target === E4S_WINDOW_CALL_TYPE) {
                            frame = getCallFrameName();
                        }
                        let windowId = openwindow(url, frame, windowParms);
                        windowId.focus();
                        if (advertIndex > -1 ){
                            showActiveAdvert();
                        }else{
                            showActiveEvent(data.event);
                        }
                    }
                }
            }
            function updateChildController(data) {
                if (data.srcController === getControllerName()) {
                    // self
                    return;
                }
                if (!inheriting()) {
                    return;
                }
                data.parentController = getParentControllerName();
                delete data.srcController;
                saveToStorage(data);
                loadFromStorage(data);
                let msg = "Inheriting from " + data.parentController;
                $("#controllerMessage").html(msg);
                // startEventCycle();
            }

            function requestParentData() {
                let payload = {
                    controllerName: getParentControllerName()
                }
                e4sSocket.send(payload, SOCKET_REQUEST_PARENT);
            }

            function renameController() {
                let name = getControllerName();
                let mId = getCompetition().id;
                let url = location.protocol + '//' + location.host + location.pathname.replace("/" + mId, '') + "?name=" + name + "&delay=" + getEventDelay() + "&mid=" + mId
                window.location.href = url;
            }

            function processResponse(data) {
                if (data.requestController !== getControllerName()) {
                    return;
                }
                if (controllers.indexOf(data.controllerName) === -1) {
                    controllers.push(data.controllerName);
                    // add to select
                    let select = getParentControllerObj();
                    let option = "<option value='" + data.controllerName + "'>" + data.controllerName + "</option>";
                    select.append(option);
                }
            }

            function responseToControllerRequest(data) {
                let controllerName = getControllerName();
                if (data.requestController !== controllerName) {
                    let payload = {
                        requestController: data.requestController,
                        controllerName: controllerName
                    }
                    e4sSocket.send(payload, SOCKET_CONTROLLER_RESPONSE);
                }
            }

            function requestControllerList() {
                initParentSelector();
                let payload = {
                    requestController: getControllerName()
                }
                e4sSocket.send(payload, SOCKET_CONTROLLER_REQUEST);
            }
            let browser = '';
            let eventCount = 0;
            let currentEvent = {id: 0};
            let screenInterval;
            let adminWindowId;
            let mainWindowId;
            let callroomWindowId;
            let scheduleWindowId;
            let touchWindowId;
            let eventObjs = {};
            let advertIndex = 0;
            let eventCounter = 0;
            let advertCount = 0;

            function updateChild() {
                if (inheriting()) {
                    return;
                }
                saveToStorage();
                sendParentData();
            }

            function setOutputDisplay(set) {
                updateNamedOutputOptions(set);
                if ( inheriting() ){
                    return;
                }
                if (set) {
                    $("#display_<?php echo $idStr ?>Yes").prop("checked", true);
                    $("#display_<?php echo $idStr ?>No").prop("checked", false);
                } else {
                    $("#display_<?php echo $idStr ?>Yes").prop("checked", false);
                    $("#display_<?php echo $idStr ?>No").prop("checked", true);
                }
            }

            function getOutputDisplay() {
                return true;
                let display = $("[name=display_<?php echo $id ?>]:checked").val() === "1";
                if (!display) {
                    if (isWindowAvailable(adminWindowId)) {
                        adminWindowId.window.close();
                    }
                    if (isWindowAvailable(mainWindowId)) {
                        mainWindowId.window.close();
                    }
                    if (isWindowAvailable(callroomWindowId)) {
                        callroomWindowId.window.close();
                    }
                    if (isWindowAvailable(touchWindowId)) {
                        touchWindowId.window.close();
                    }
                }
                return display;
            }

            function inheriting() {
                let parentController = getParentControllerName();
                if (parentController === "") {
                    return false;
                }
                return true;
            }

            function getCallroomUrl() {
                let url = location.href.split("controllerV2.php")[0] + "callroomv2.php";
                let scheme = getScheme();
                let stripeTimes = $("#callRoomStripTimes").prop("checked");
                if (scheme === "") {
                    scheme = "high";
                }
                url += "?scheme=" + scheme;
                url += "&size=" + $("#callroomSize").val();
                url += "&events=";
                let sep = '';
                $("[heat]:checked").each(
                    function( index, meId){
                        let obj = $(meId);
                        let event = obj.attr("event");
                        let heat = parseInt(obj.attr("heat"));
                        if (heat === 1) {
                            // check if there is a 2nd heat else pass 0 ( single heat )
                            if ($("[event='" + event + "']").length === 1) {
                                heat = 0;
                            }
                        }
                        if ( true ){
                            if ( event.indexOf("-") > 0 ){
                                // event = event.split("-")[1];
                                event = event.slice(6);
                            }
                        }
                        url += sep + encodeURI(event.replace("&", "^"));
                        sep = '~';

                        if (heat > 0){
                        // if (heat > 0 && event.indexOf("Heat") === -1){
                            url += " Heat " + heat;
                        }
                    }
                );

                return url;
            }

            function getFullUrl() {
                let useEvent = currentEvent;
                let type = "StartList";
                if (useEvent.type === 1) {
                    type = "Results";
                }

                let url = getRosterBaseUrl(type) + "&meId=" + useEvent.id ;
                return url;
            }
            function getRosterAdminUrl() {
                return "https://admin.rosterathletics.com/admin";
            }
            function getRosterBaseUrl(type = "StartList") {
                let scheme = getScheme();
                if (scheme !== "") {
                    scheme = "&scheme=" + scheme;
                }
                let url = getRosterAdminUrl() + "/score-boards/" + type + "?mid=<?php echo $mId?>" + scheme;
                return url;
            }
            function getFrameName() {
                return "main_<?php echo $mId . '.' . $idStr ?>";
            }

            function getCallFrameName() {
                return "callroom_<?php echo $idStr ?>";
            }
            function getScheduleFrameName() {
                return "schedule_<?php echo $idStr ?>";
            }

            function showThisOne(meId) {
                stopEventCycle();
                startEventCycle(meId);
            }

            function isWindowAvailable(windowId) {
                return windowId && windowId.window !== null;
            }
            function startEventCycle(meId = 0) {
                advertIndex = 0;
                eventCounter = 0;

                // getSelectedEvents(meId);
                clearInterval(screenInterval);

                let url = '';
                let frame = '';
                let target = '';
                let advertCount = getAdvertCount();
                let delay = getEventDelay();
                // if (getCallRoomEnabled()) {
                //     let meIds = getCallRoomMeIds();
                    // update or open callRoom window
                    url = getCallroomUrl();
                    frame = getCallFrameName();
                    target = E4S_WINDOW_CALL_TYPE;
                    // if (getOutputDisplay()) {
                        callroomWindowId = openwindow(url, frame, windowParms);
                        // if (mainWindowId) {
                        //     mainWindowId.focus();
                        // }
                    // }
                    // sendToChildren(url, target);
                // }

                // if ( isTouchEnabled() ){
                    //if (getOutputDisplay()) {
                    //    let url = "/rostertouch/<?php //echo $mId?>//";
                    //    let frame = "touch_<?php //echo $idStr ?>//";
                    //    if (!isWindowAvailable(touchWindowId)) {
                    //        touchWindowId = openwindow(url, frame, windowParms);
                    //        if (touchWindowId) {
                    //            touchWindowId.focus();
                    //        }
                    //    }
                    //}
                // }else {
                    // if (eventCount > 0 || advertCount > 0) {
                    //     if (advertCount > 0) {
                    //         url = adverts[0];
                    //         advertIndex++;
                    //         delay = getAdvertDelay();
                    //     } else {
                    //         url = getFullUrl();
                    //     }
                    //     frame = getFrameName();
                    //     target = E4S_WINDOW_START_TYPE;
                    //     if (getOutputDisplay()) {
                    //         mainWindowId = openwindow(url, frame, windowParms);
                    //     }
                        // sendToChildren(url, target);
                        // showActiveEvent();
                        // $("#controllerMessage").html("Cycling display...");
                        //
                        // resetInterval(delay);
                    // }
                // }

            }
            function namedOutputChanged(){
                if (inheriting()) {
                    return;
                }
                saveToStorage();
                updateChild();
            }
            function updateNamedOutputOptions(set){
                if ( set ){
                    $(".namedOutputOptions").show();
                }else{
                    $(".namedOutputOptions").hide();
                }
            }
            function setNamedOutput(checked){
                $("#namedOutput").prop("checked", checked);
            }
            function useNamedOutput(){
                return $("#namedOutput").prop("checked");
            }
            function openwindow(url, frame, options){
                let windowId = null;

                if ( useNamedOutput() ) {
                    windowId = window.open(url, frame, options);
                } else {
                    windowId = window.open(url, frame);
                }
                if (windowId) {
                    windowId.focus();
                }
                return windowId;
            }
            function isTouchEnabled() {
                return showingOutput(E4S_OUTPUT_TOUCH);
            }
            function sendToChildren(url, target, advertIndex = -1) {
                let payload = {
                    parentController: getControllerName(),
                    url: url,
                    target: target,
                    event: currentEvent,
                    advertIndex: advertIndex
                }
                e4sSocket.send(payload, SOCKET_CONTROLLER_SHOW);
            }

            function resetInterval(delay) {
                clearInterval(screenInterval);
                screenInterval = setInterval(openNextUrl, delay * 1000);
            }

            function openNextUrl() {
                if (getOutputDisplay()) {
                    if (!isWindowAvailable(mainWindowId) || !isWindowAvailable(adminWindowId)) {
                        // window closed
                        // alert("A Roster window has been closed. This will be re-opened but check casting options");
                        return startEventCycle();
                    }
                }
                advertCount = getAdvertCount();
                let showNext = true;
                let delay = getEventDelay();
                let url = '';
                let frame = getFrameName();
                let advertCnt = -1;
                if (advertCount > 0) {
                    if (eventCounter >= advertCount || eventCount === 0) {
                        eventCounter = 0;
                        if (advertIndex === adverts.length) {
                            advertIndex = 0;
                        }
                        url = adverts[advertIndex];
                        advertCnt = advertIndex;
                        showActiveAdvert();
                        if (getOutputDisplay()) {
                            mainWindowId.location.href = url;
                        }
                        advertIndex++;
                        showNext = false;
                        delay = getAdvertDelay();
                    }
                }
                if (showNext) {
                    getNextSelectedEvent();
                    showActiveEvent();
                    url = getFullUrl();
                    if (getOutputDisplay()) {
                        mainWindowId.location.href = url;
                    }
                }
                sendToChildren(url, E4S_WINDOW_START_TYPE, advertCnt);
                resetInterval(delay);
            }

            function updateAdvertCount() {
                // get the Advert Count
                if (getAdvertCount() === 0) {
                    // hide advertOptions
                    $(".advertOptions").hide();
                } else {
                    $(".advertOptions").show();
                }
                updateAdvertDelay();
            }

            function updateAdvertDelay() {
                const minDelay = 5;
                if (getAdvertDelay() < minDelay) {
                    setAdvertDelay(minDelay);
                }
                restartController();
            }

            function updateDelay() {
                const minDelay = 5;
                if (getEventDelay() < minDelay) {
                    setEventDelay(minDelay);
                }
                restartController();
            }

            function clearCallRoomSelected() {
                let selector = "[callroom]:checked";
                $(selector).prop("checked", false);
                updateChild();
            }

            function updateCallRoomSettings() {
                updateCallRoom({
                    "enabled": getCallRoomEnabled(),
                    "sync": getCallRoomSyncEnabled(),
                    "meId": getCallRoomMeIds(),
                    "size": $("#callroomSize").val(),
                    "times": $("#callRoomStripTimes").prop("checked")
                });
                if (!getCallRoomEnabled()) {
                    if (callroomWindowId) {
                        callroomWindowId.window.close();
                        callroomWindowId = null;
                    }
                    // hide the callRoom options
                    $(".callroomOptions").hide();
                } else {
                    $(".callroomOptions").show();
                }
                if ( !showingOutput(E4S_OUTPUT_SCHEDULE) ){
                    if (scheduleWindowId) {
                        scheduleWindowId.window.close();
                        scheduleWindowId = null;
                    }
                }
                restartController();
            }

            function getCallRoomMeIds() {
                let selector = "[callroom]:checked";
                let meIds = [];
                $(selector).each(function () {
                    meIds.push(parseInt(this.value));
                });
                return meIds;
            }

            function getCallRoomEnabled() {
                return true;
                return showingOutput(E4S_OUTPUT_CALLROOM);
            }

            function getCallRoomSyncEnabled() {
                return $("[name=callRoomSyncEnabled]:checked").val() === "1";
            }

            function setCallRoomSync(enabled) {
                if (enabled) {
                    $("#callroomSyncEnabled").prop("checked", true);
                    $("#callroomSyncDisabled").prop("checked", false);
                } else {
                    $("#callroomSyncEnabled").prop("checked", false);
                    $("#callroomSyncDisabled").prop("checked", true);
                }
            }

            function updateCallRoom(obj) {
                return "";
                let enabled = obj.enabled;
                let meIds = obj.meId;
                let size = obj.size;
                let times = obj.times;

                if (times) {
                    $("#callRoomStripTimes").prop("checked", true);
                } else {
                    $("#callRoomStripTimes").prop("checked", false);
                }
                let selector = "[callroom]";
                if (!enabled) {
                    $(selector).hide();
                } else {
                    $(selector).show();
                }
                if (enabled) {
                    $("#callroomEnabled").prop("checked", true);
                    $("#callroomDisabled").prop("checked", false);
                } else {
                    $("#callroomEnabled").prop("checked", false);
                    $("#callroomDisabled").prop("checked", true);
                }
                setCallRoomSync(obj.sync);
                $("#callroomSize").val(size);
                $("[callroom]").prop("checked", false);
                for (meId of meIds) {
                    $("#" + meId + "_call").prop("checked", true);
                }
            }

            function displayChanged() {
                updateNamedOutputOptions(getOutputDisplay());
                saveToStorage();
                restartController();
            }

            function restartController() {
                stopEventCycle();
                startEventCycle();
                if (!inheriting()) {
                    updateChild();
                }
            }

            function setAll(meIds, type, checked = true) {
                for (meId of meIds) {
                    setMeId(meId, type, checked, false);
                }
                getSelectedEvents();
                updateChild();
            }

            function setMeId(meId, type = -1, checked = false, update = true) {
                let selector = "[name=" + meId + "]";
                if (type > -1) {
                    selector += "[value=" + type + "]";
                }
                let radios = $(selector);
                radios.each(function () {
                    this.checked = checked;
                });
                if (update) {
                    getSelectedEvents();
                    updateChild();
                }
            }

            function getNextSelectedEvent() {
                getSelectedEvents();
                if (eventCount < 2) {
                    return;
                }
                let set = false;
                let nextEvent = currentEvent;
                let found = false;
                for (let key in eventObjs) {
                    if (found) {
                        nextEvent = eventObjs[key];
                        set = true;
                        break;
                    }
                    if (eventObjs[key].id === currentEvent.id) {
                        found = true;
                    }
                }
                if (nextEvent.id === 0 || !set) {
                    for (let key in eventObjs) {
                        nextEvent = eventObjs[key];
                        break;
                    }
                }

                currentEvent = nextEvent;
            }

            function getSelectedEvents(meId = 0) {
                eventObjs = {};
                eventCount = 0;
                let selector = "[e4sevent]:checked";
                $(selector).each(function () {
                    eventCount++;
                    let obj = {
                        id: parseInt(this.id),
                        type: parseInt(this.value),
                        event: this.attributes.e4sevent.value
                    }
                    eventObjs[this.id] = obj;
                    if (currentEvent.id === 0 || obj.id === meId) {
                        currentEvent = obj;
                    }
                });
            }

            function showActiveAdvert() {
                $("[e4scurrent]").html("");
                $("#controllerMessage").html("Advert " + (advertIndex + 1) + " of " + adverts.length);
            }

            function showActiveEvent(showEvent = currentEvent) {
                eventCounter++;
                $("[e4scurrent]").html("");
                $("[e4scurrent=" + showEvent.id + "]").html(" <<");
                let heat = $("#" + showEvent.id).attr("e4sheat");
                if (heat === "1") {
                    let event = $("#" + showEvent.id).attr("e4sevent");
                    if ($("[e4sevent='" + event + "']").length === 0) {
                        heat = '';
                    }
                }
                if (heat !== "") {
                    heat = " - Heat " + heat;
                }
                $("#controllerMessage").html("Current Event : " + showEvent.event + ' ' + heat);
            }

            function stopEventCycle() {
                $("#controllerMessage").html("&nbsp;");
                screenInterval = clearInterval(screenInterval);
            }

            function eventSelected() {
                let obj = event.currentTarget;
                let results = obj.value === "1";
                let checked = obj.checked;
                let meId = obj.id;
                if (results) {
                    // if results selected then clear start list/callroom
                    $("#" + meId + "_call").prop("checked", false);
                } else {
                    if (getCallRoomEnabled()) {
                        if (getCallRoomSyncEnabled()) {
                            // set callroom
                            $("#" + meId + "_call").prop("checked", true);
                        }
                    }
                }
            }

            function inCallRoomSelected(eventName, useHeat) {
                updateCallRoomSettings();
            }

            function addEvent() {
                eventSelected();
                getNextSelectedEvent();
                if (!screenInterval || eventCount < 2) {
                    startEventCycle();
                }
                updateChild();
            }

            function parentChange() {
                if (inheriting()) {
                    $("button").hide();
                    $("input:not([never])").attr("disabled", true);
                    saveToStorage();
                    requestParentData();
                } else {
                    $("button").show();
                    $("input").attr("disabled", false);
                }
            }

            function initParentSelector() {
                controllers = [];
                let parentDiv = $("#parentControllerDiv");
                parentDiv.html("Parent Controller : <select id=\"parentController\" onchange=\"parentChange(); return false;\"><option value=''>N/A</option></select>");
            }

            function getScheme() {
                return $("[name=scheme_<?php echo $idStr ?>]:checked").val();
            }

            function setScheme(scheme) {
                $("[name=scheme_<?php echo $idStr ?>]:checked").prop("checked", false);
                $("#scheme_<?php echo $idStr ?>" + scheme).prop("checked", true);
            }
            function sendParentData() {
                let payload = getControllerData();
                payload.srcController = getControllerName();

                e4sSocket.send(payload, SOCKET_SEND_UPDATE);
            }
            function saveToStorage(useData = null) {
                let data = useData;
                if (data === null) {
                    data = getControllerData();
                }
                localStorage.setItem("controller_<?php echo $idStr ?>", JSON.stringify(data));
            }
            function getControllerData(){
                let data = {
                    eventObjs: eventObjs,
                    delay: getEventDelay(),
                    useNamedOutput: useNamedOutput(),
                    scheme: getScheme(),
                    parentController: getParentControllerName(),
                    advert: {
                        count: getAdvertCount(),
                        delay: getAdvertDelay()
                    },
                    callRoom: {
                        enabled: getCallRoomEnabled(),
                        sync: getCallRoomSyncEnabled(),
                        meId: getCallRoomMeIds(),
                        size: parseInt($("#callroomSize").val()),
                        times: $("#callRoomStripTimes").prop("checked")
                    },
                    display: getOutputDisplay(),
                    disciplines:{
                        field: showingDiscipline(E4S_FIELD_DISCIPLINE),
                        track: showingDiscipline(E4S_TRACK_DISCIPLINE)
                    },
                    output:{
                        start: showingOutput(E4S_OUTPUT_START),
                        results: showingOutput(E4S_OUTPUT_RESULTS),
                        callroom: showingOutput(E4S_OUTPUT_CALLROOM),
                        schedule: showingOutput(E4S_OUTPUT_SCHEDULE),
                        touch: showingOutput(E4S_OUTPUT_TOUCH)
                    }
                }
                return data;
            }
            function loadFromStorage(data = null) {
                if (data === null) {
                    data = localStorage.getItem("controller_<?php echo $idStr ?>");
                }
                if (data === null) {
                    return;
                }
                if ( typeof data === 'string') {
                    data = JSON.parse(data);
                }
                setNamedOutput(data.useNamedOutput);
                setOutputDisplay(data.display);
                eventObjs = data.eventObjs;
                // clear All
                $("[e4sevent]").prop("checked",false);
                for (key in eventObjs) {
                    let eventObj = eventObjs[key];
                    let selector = "[id=" + eventObj.id + "][value=" + eventObj.type + "]";
                    $(selector).prop("checked", true);
                }
                setDisciplineOptions(data.disciplines);
                setOutputOptions(data.output);
                setEventDelay(data.delay);
                setScheme(data.scheme);
                setAdvertCount(data.advert.count);
                setAdvertDelay(data.advert.delay);
                updateCallRoom({
                    "enabled": data.callRoom.enabled,
                    "sync": data.callRoom.sync,
                    "meId": data.callRoom.meId,
                    "size": data.callRoom.size,
                    "times": data.callRoom.times
                });
                setParentControllerName(data.parentController);
            }

            function resetController() {
                if (confirm("Are you sure you want to RESET the controller. All settings will be cleared ?")) {
                    localStorage.removeItem("controller_<?php echo $idStr ?>");
                    location.reload();
                }
            }

            function updateTitle() {
                $("#e4s-nav-bar").css("z-index", 1);
                let titleObj = $(".e4s-navigation-bar-menu");
                let title = 'T4S Roster Controller';
                titleObj.html(title);
                titleObj.addClass("e4sTitle");
            }

            function reportWindowSize() {
                $("#content").css("height", window.innerHeight - 400);
            }

            // Discipline functions
            function onLoadDiscipline() {
                let field = $("#discipline" + E4S_FIELD_DISCIPLINE).prop("checked");
                field = true;
                let track = $("#discipline" + E4S_TRACK_DISCIPLINE).prop("checked");
                track = true;
                displayDiscipline(E4S_FIELD_DISCIPLINE, field);
                displayDiscipline(E4S_TRACK_DISCIPLINE, track);
            }
            function updateDiscipline(){
                let field = $("#discipline" + E4S_FIELD_DISCIPLINE).prop("checked");
                field = true;
                let track = $("#discipline" + E4S_TRACK_DISCIPLINE).prop("checked");
                track = true;
                displayDiscipline(E4S_FIELD_DISCIPLINE, field);
                displayDiscipline(E4S_TRACK_DISCIPLINE, track);
                saveToStorage();
                updateOutputDisplay();
            }
            function displayDiscipline(discipline, show=true) {
               if (show){
                   $("#"+discipline+"ContentGroup").show();
               }else{
                   $("#"+discipline+"ContentGroup").hide();
               }
            }
            function showingDiscipline(discipline){
                let show = false;
                let selector = "#discipline" + discipline + ":checked";
                if ($(selector).length > 0){
                    show = true;
                }
                return show;
            }
            function setDisciplineOptions(disciplines){
                setDiscipline(E4S_FIELD_DISCIPLINE, disciplines.field);
                setDiscipline(E4S_TRACK_DISCIPLINE, disciplines.track);
            }
            function setDiscipline(discipline, show){
                let selector = "#discipline" + discipline;
                $(selector).prop("checked", show);
            }
            // Output functions

            function showingOutput(output){
                let show = false;
                let selector = "#output" + output + ":checked";
                if ($(selector).length > 0){
                    show = true;
                }
                return show;
            }
            function setOutputOptions(output){
                setOutput(E4S_OUTPUT_START, output.start);
                setOutput(E4S_OUTPUT_RESULTS, output.results);
                setOutput(E4S_OUTPUT_CALLROOM, output.callroom);
                setOutput(E4S_OUTPUT_SCHEDULE, output.schedule);
                setOutput(E4S_OUTPUT_TOUCH, output.touch);
            }
            function setOutput(output, show){
                let selector = "#output" + output;
                $(selector).prop("checked", show);
            }
            function scheduleSelected(){
                saveToStorage();
                checkScheduleOutput();
            }
            function checkScheduleOutput(){
                let url = $("#schedule").val();
                if (url === ""){
                    if (scheduleWindowId) {
                        scheduleWindowId.window.close();
                        scheduleWindowId = null;
                    }
                }else{
                    if(getOutputDisplay()) {
                        url = '/results/<?php echo $mId?>/' + url;
                        scheduleWindowId = openwindow(url, getScheduleFrameName(), windowParms);
                    }
                }
            }
            function updateOutputDisplay(){
                let options = $("." + E4S_OUTPUT_START + "Options");
                if ( showingOutput(E4S_OUTPUT_START) ) {
                    options.show();
                }else{
                    options.hide();
                }
                options = $("." + E4S_OUTPUT_RESULTS + "Options");
                if ( showingOutput(E4S_OUTPUT_RESULTS) ) {
                    options.show();
                }else{
                    options.hide();
                }
                options = $("." + E4S_OUTPUT_SCHEDULE + "Options");
                if ( showingOutput(E4S_OUTPUT_SCHEDULE) ) {
                    options.show();
                }else{
                    options.hide();
                }
                updateCallRoomSettings();
            }
            function updateOutputs(){
                if (!showingOutput(E4S_OUTPUT_START)) {
                    $("[startevent]").prop("checked",false);
                }
                if (!showingOutput(E4S_OUTPUT_RESULTS)) {
                    $("[resultevent]").prop("checked",false);
                }
                if (!showingOutput(E4S_OUTPUT_CALLROOM)) {
                    $("[callroomevent]").prop("checked",false);
                }
                if (!showingOutput(E4S_OUTPUT_SCHEDULE)) {
                    $("#schedule").val("");
                }

                saveToStorage();
                updateOutputDisplay();
            }

            function getSelectedResults(){
                let meIds = [];
                $("[resultevent]:checked").each(function(){
                    meIds.push({
                        id: this.id,
                        heat:$(this).attr("e4sheat")
                    });
                });
                return meIds;
            }
            function initController() {
                setTimeout(showTime, 1000);
                updateTitle();
                advertCount = getAdvertCount();
                initParentSelector();
                loadFromStorage();
                onLoadDiscipline();
                updateOutputDisplay();
                // window.setTimeout(function(){
                //     location.reload()
                // },600000);
            }
            let displayTimeout = 0;
            function debounceDisplay(){
                // update the tabs with debounce
                clearTimeout(displayTimeout);
                displayTimeout = window.setTimeout(displayEvents, 1000);
            }
            function displayEvents(){
                let trackEventsArr = [];
                let fieldEventsArr = [];
                for(let e in trackEvents){
                    trackEventsArr.push(trackEvents[e]);
                }
                for(let f in fieldEvents){
                    fieldEventsArr.push(fieldEvents[f]);
                }
                trackEventsArr.sort(function(a, b){
                    let aDate = new Date(a.startTime);
                    let bDate = new Date(b.startTime);
                    // checkMultiDay(aDate, bDate);
                    return aDate - bDate;
                });
                fieldEventsArr.sort(function(a, b){
                    let aDate = new Date(a.startTime);
                    let bDate = new Date(b.startTime);
                    // checkMultiDay(aDate, bDate);
                    if ( aDate - bDate === 0){
                        if (b.eventCode < a.eventCode ){
                            return -1;
                        }
                        if (b.eventCode > a.eventCode ){
                            return 1;
                        }
                        return 0;
                    }
                    return aDate - bDate;
                });
                outputEvents(fieldEventsArr,E4S_FIELD_DISCIPLINE);
                outputEvents(trackEventsArr, E4S_TRACK_DISCIPLINE);
            }
            function outputEvents(events, type = "<?php echo E4S_FIELD_DISCIPLINE ?>"){
                let html = "";
                for ( let e in events ){
                    let event = events[e];
                    let dateNow = new Date();
                    let eventStartDate = new Date(event.startTime);
                    let todaysEvent = eventStartDate.getDate() === dateNow.getDate();
                    // force display ( ignore multi date competition )
                    if ( location.search.indexOf("force=1") > -1 ){
                        todaysEvent = true;
                    }
                    if ( todaysEvent ){
                        let eventHTML = "";
                        eventHTML += '<div id="eventContent" class="eventContent">';
                        eventHTML += '<span class="eventName">' + event.name + '</span>';
                        eventHTML += '<table>';
                        eventHTML += '<tr>';
                        eventHTML += '<td style="width:75px;">Heat</td>';
                        eventHTML += '<td class="<?php echo E4S_OUTPUT_CALLROOM ?>Options" callroom style="width:150px;text-align: center;">In Call Room</td>';
                        eventHTML += '</tr>';

                        for (let h = 1; h <= event.heats; h++) {
                            eventHTML += '<tr>';
                            eventHTML += '<td>' + h + '</td>';
                            eventHTML += '<td class="<?php echo E4S_OUTPUT_CALLROOM ?>Options" callroom style="text-align: center;">';
                            let useHeat = parseInt(h);
                            if (event.heats === 1) {
                                useHeat = 0;
                            }
                            eventHTML += '<input type="checkbox" id="' + event.id + '_' + h + '_call" event="' + event.name + '" value="' + h + '" heat="' + h + '" onchange="updateCallRoomSettings()">';
                            eventHTML += '</td>';
                            eventHTML += '</tr>';
                        }
                        eventHTML += '</table>';
                        eventHTML += '</div>';
                        html += eventHTML;
                    }
                }

                $("#" + type + "Content").html(html);
            }
            function showTime(){
                var date = new Date();
                var h = date.getHours(); // 0 - 23
                var m = date.getMinutes(); // 0 - 59
                var s = date.getSeconds(); // 0 - 59
                var session = "";

                if(h == 0){
                    h = 12;
                }

                if(h > 12){
                    // h = h - 12;
                    session = "";
                }

                h = (h < 10) ? "0" + h : h;
                m = (m < 10) ? "0" + m : m;
                s = (s < 10) ? "0" + s : s;

                var time = h + ":" + m + ":" + s ;
                document.getElementById("MyClockDisplay").innerText = time;
                document.getElementById("MyClockDisplay").textContent = time;

                setTimeout(showTime, 1000);

            }
            // Document Ready
            $(document).ready(function () {
                browser = (function (agent) {
                    switch (true) {
                        case agent.indexOf("edge") > -1: return "edge";
                        case agent.indexOf("edg/") > -1: return "newedge"; // Match also / to avoid matching for the older Edge
                        case agent.indexOf("opr") > -1 && !!window.opr: return "opera";
                        case agent.indexOf("chrome") > -1 && !!window.chrome: return "chrome";
                        case agent.indexOf("trident") > -1: return "ie";
                        case agent.indexOf("firefox") > -1: return "firefox";
                        case agent.indexOf("safari") > -1: return "safari";
                        default: return "other";
                    }
                })(window.navigator.userAgent.toLowerCase());
                window.onresize = reportWindowSize;
                $("[e4sevent]").click(function () {
                    addEvent();
                });
                getInitialData(processInitialData);
                // get cookie data every 30 minutes
                setInterval(function(){getCookieData()}, getThousandthsForMinutes(30));
                // get start list data every 5 minutes
                setInterval(function(){getStartListData()},getThousandthsForMinutes(5));
                // get result data every 1 minute
                setInterval(function(){getResultsData()}, getThousandthsForMinutes(1));

                initController();
            });

            function processInitialData(data){
                if ( data ){
                    for(let sl in data.startList){
                        let event = data.startList[sl];
                        this.addStartListEvent(event, false);
                    }
                    displayEvents();
                }
            }
        </script>
    </head>
    <body style="overflow:hidden;">
	<?php
	include_once get_template_directory() . '-child/header-e4s.php';
	?>
    <div id="configuration" class="config">
        <section>
            <header>
            <span class="configCol">
                Controller :
            </span>
            <span class="configCol">
                Name : <input id="controllerName" value="<?php echo $id ?>" onchange="renameController(); return false;">
            </span>
            <span class="configCol">
                Call Room :
            </span>

            <span class="configCol">
                Text size : <input class="e4sNumber" type="number" id="callroomSize" name="callroomSize" value="5"
                                   min="3" max="10" onchange="updateCallRoomSettings();return false;"> vh
            </span>
            </header>
            <div class="configRow">
            <span class="configCol">
               &nbsp;
            </span>
                <span class="configCol">
                 Scheme : <input type="radio" id="scheme_<?php echo $idStr ?>" name="scheme_<?php echo $idStr ?>"
                                 onchange="restartController()" value="" checked> Blue
                     <input type="radio" id="scheme_<?php echo $idStr ?>light" name="scheme_<?php echo $idStr ?>"
                            onchange="restartController()" value="light"> Light
                     <input type="radio" id="scheme_<?php echo $idStr ?>dark" name="scheme_<?php echo $idStr ?>"
                            onchange="restartController()" value="dark"> Dark
            </span>
                <span class="configCol" style="display:none">
                     Discipline: <input checked type="checkbox" id="discipline<?php echo E4S_TRACK_DISCIPLINE ?>" name="discipline"
                                        onchange="updateDiscipline()" value="<?php echo E4S_TRACK_DISCIPLINE ?>"> <?php echo E4S_TRACK_DISCIPLINE ?>
                            <input checked type="checkbox" id="discipline<?php echo E4S_FIELD_DISCIPLINE ?>" name="discipline"
                                   onchange="updateDiscipline()" value="<?php echo E4S_FIELD_DISCIPLINE ?>"> <?php echo E4S_FIELD_DISCIPLINE ?>

            </span>
            <span class="configCol">

            </span>
            </div>
            <div class="configRow">
            <span class="configCol">
               &nbsp;
            </span>
            <span class="configCol" style="display: none">
                Show Outputs : <input never type="radio" id="display_<?php echo $idStr ?>Yes"
                                      name="display_<?php echo $idStr ?>" onchange="displayChanged()" value="1" checked> Yes
                                 <input never type="radio" id="display_<?php echo $idStr ?>No" name="display_<?php echo $idStr ?>"
                                        onchange="displayChanged()" value="0"> No
            </span>
            <span class="configCol" style="display: none">
                Outputs :   <input type="checkbox" id="output<?php echo E4S_OUTPUT_START ?>"
                                   onchange="updateOutputs()" value="<?php echo E4S_OUTPUT_START ?>"> Start List
                            <input type="checkbox" id="output<?php echo E4S_OUTPUT_RESULTS ?>"
                                   onchange="updateOutputs()" value="<?php echo E4S_OUTPUT_RESULTS ?>"> Results
                            <input type="checkbox" id="output<?php echo E4S_OUTPUT_CALLROOM ?>"
                                   onchange="updateOutputs()" value="<?php echo E4S_OUTPUT_CALLROOM ?>" checked> Call Room
                            <input type="checkbox" id="output<?php echo E4S_OUTPUT_SCHEDULE ?>"
                                   onchange="updateOutputs()" value="<?php echo E4S_OUTPUT_SCHEDULE ?>"> Information
                            <input type="checkbox" id="output<?php echo E4S_OUTPUT_TOUCH ?>"
                                   onchange="updateOutputs()" value="<?php echo E4S_OUTPUT_TOUCH ?>"> Touch Screen
            </span>
            <span class="configCol"  style="display: none">
                <span class="namedOutputOptions">
                    Use Named Outputs: <input never type="checkbox" id="namedOutput" name="namedOutput"
                           onchange="namedOutputChanged();" value="y"> Yes
                </span>
            </span>
            </div>
            <div class="configRow"  style="display: none">
            <span class="configCol">
               Adverts :
            </span>
                <span class="configCol">
                Show Every : <input class="e4sNumber" type="number" id="advert" name="advert" value="3" min="0" max="60"
                                    onchange="updateAdvertCount();return false;"> events ( 0 disables )
            </span>
            <span class="configCol">
                <span class="advertOptions">
                    Show For : <input class="e4sNumber" type="number" id="advertdelay" name="advertdelay" value="5"
                                      min="1" max="60" onchange="updateAdvertDelay();return false;"> seconds
                </span>
            </span>
                <span class="configCol">

            </span>
            </div>
            <div class="configRow"  style="display: none">
            <span class="configCol">
                Events :
            </span>
                <span class="configCol">
                Show For : <input class="e4sNumber" type="number" id="delay" name="delay" value="<?php echo $delay ?>"
                                  min="3" max="60" onchange="updateDelay();return false;"> seconds
            </span>
            <span class="configCol">

            </span>

            <span class="configCol">

            </span>
            </div>

            <div class="configRow callroomOptions">
                <span class="configCol" style="display: none">
                    <span id="parentControllerDiv"></span>
                    <button onclick="requestControllerList()">Refresh Controllers</button>
                </span>
                <span class="configCol">
                </span>

                <span class="configCol callroomSyncOptions"  style="display: none">
                    Sync with Start List:
                    <input type="radio" id="callroomSyncDisabled" name="callRoomSyncEnabled" value="0" checked
                           onchange="updateCallRoomSettings()"> No
                    <input type="radio" id="callroomSyncEnabled" name="callRoomSyncEnabled" value="1"
                           onchange="updateCallRoomSettings()"> Yes
                </span>
                <span class="configCol"  style="display: none">
                    Hide Times : <input type="checkbox" id="callRoomStripTimes" name="callRoomStripTimes"
                                       onchange="updateCallRoomSettings()" value="Y"> Yes
                </span>
            </div>

            <div class="configRow <?php echo E4S_OUTPUT_SCHEDULE ?>Options">
                <span class="configCol">
                    Information :
                </span>

                <span class="configCol">
                    <select id="schedule" onchange="scheduleSelected();">
                        <option value="">Select Information</option>
                        <option value="callroom_friday.htm">Friday</option>
                        <option value="callroom_saturday.htm">Saturday</option>
                        <option value="callroom_sunday.htm">Sunday</option>
                        <option value="about.htm">About BUCS</option>
                    </select>
                </span>
                <span class="configCol callroomSyncOptions">

                </span>
                <span class="configCol">

                </span>
            </div>

            <div id="MyClockDisplay" class="clock" ></div>

            <div class="configRow" style="display: none">
                <span class="configCol">Actions : </span>
                <span class="configCol">
                        <button onclick="resetController()">Reset</button> &nbsp;
                        <button class="" onclick="location.reload();">Reload This Controller</button>
                    </span>
                <span class="configCol">Processing :  <button onclick="startEventCycle()">Start</button>&nbsp;
                        <button class="warnButton" onclick="stopEventCycle()">Stop</button></span>
                <span class="configCol">

                    </span>
            </div>

        </section>
        <div style="padding:20px 0px 0px 0px;">
            <span class="configCol" id="controllerMessage"></span>
        </div>
    </div>
    <hr>

    <div id="content" class="content">

            <div id="<?php echo E4S_TRACK_DISCIPLINE ?>ContentGroup" class="contentGroup" style="display: none;">
                <div class="eventsTitle">
	                <?php echo E4S_TRACK_DISCIPLINE ?> Events
                </div>
                <div id="<?php echo E4S_TRACK_DISCIPLINE ?>Content" class="<?php echo E4S_TRACK_DISCIPLINE ?>Content">

                </div>
            </div>


            <div id="<?php echo E4S_FIELD_DISCIPLINE ?>ContentGroup" class="contentGroup" style="display: none;">
                <div class="eventsTitle">
	                <?php echo E4S_FIELD_DISCIPLINE ?> Events
                </div>
                <div id="<?php echo E4S_FIELD_DISCIPLINE ?>Content" class="<?php echo E4S_FIELD_DISCIPLINE ?>Content">

                </div>

    </div>
    </body>
    </html>
<?php
function outputEvents( $events ) {
	foreach ( $events as $event => $eventObj ) {
		$meIds = [];
		for ( $i = 0; $i < $eventObj->heats; $i ++ ) {
			$meIds[ $i + 1 ] = $eventObj->startmeId + $i;
		}
		?>
        <div id="eventContent" class="eventContent">
            <span class="eventName"><?php echo $event ?></span>
            <table>
                <tr>
                    <td style="width:75px;">Heat</td>
                    <td class="<?php echo E4S_OUTPUT_START ?>Options" style="width:150px;">Start List</td>
                    <td class="<?php echo E4S_OUTPUT_RESULTS ?>Options" style="width:150px;">Results</td>
                    <td>&nbsp;</td>
                    <td class="<?php echo E4S_OUTPUT_CALLROOM ?>Options" callroom style="width:150px;text-align: center;">In Call Room</td>
                </tr>
				<?php if ( sizeof( $meIds ) > 1 ) { ?>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="<?php echo E4S_OUTPUT_START ?>Options">
                            <button class="warnButton"
                                    onclick="setAll([<?php echo implode( ',', $meIds ) ?>], 0, false); return true;">
                                Clear All
                            </button>
                            <button onclick="setAll([<?php echo implode( ',', $meIds ) ?>], 0, true); return true;">Set
                                All
                            </button>
                        </td>
                        <td class="<?php echo E4S_OUTPUT_RESULTS ?>Options">
                            <button class="warnButton"
                                    onclick="setAll([<?php echo implode( ',', $meIds ) ?>], 1, false); return true;">
                                Clear All
                            </button>
                            <button onclick="setAll([<?php echo implode( ',', $meIds ) ?>], 1, true); return true;">Set
                                All
                            </button>
                        </td>
                        <td>&nbsp;</td>
                        <td class="<?php echo E4S_OUTPUT_CALLROOM ?>Options"></td>
                    </tr>
					<?php
				}
				foreach ( $meIds as $heat => $meId ) {
					?>
                    <tr>
                        <td><span onclick="showThisOne(<?php echo $meId ?>);"><?php echo $heat ?><span
                                        e4scurrent="<?php echo $meId ?>"></span></span></td>
                        <td class="<?php echo E4S_OUTPUT_START ?>Options"><input e4sheat=<?php echo $heat ?> e4sevent="<?php echo $event ?>" startevent type="radio"
                                   id="<?php echo $meId ?>" name="<?php echo $meId ?>" value="0"></td>
                        <td class="<?php echo E4S_OUTPUT_RESULTS ?>Options"><input e4sheat=<?php echo $heat ?> e4sevent="<?php echo $event ?>" resultevent type="radio"
                                   id="<?php echo $meId ?>" name="<?php echo $meId ?>" value="1"></td>
                        <td>
                            <button class="warnButton" onclick="setMeId(<?php echo $meId ?>)">Clear</button>
                        </td>
                        <td class="<?php echo E4S_OUTPUT_CALLROOM ?>Options" style="text-align: center;"><input callroom callroomevent e4scallroomevent="<?php echo $event ?>"
                                                               heat="<?php echo $heat ?>" type="checkbox"
                                                               id="<?php echo $meId ?>_call" name="inCallRoom"
                                                               value="<?php echo $meId ?>"
                                                               onchange="inCallRoomSelected(<?php echo $meId ?>);"></td>
                    </tr>
					<?php
				}
				?>
            </table>
        </div>
		<?php
	}
}
