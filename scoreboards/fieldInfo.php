<?php
?>
<html>
<head>
	<title>Field Information</title>
</head>
<body>
	<table>
		<tr>
			<td colspan="2">
				<span id="eventTitle">Long Jump</span>
			</td>
		</tr>
		<tr>
			<td>
				<span id="trial">1</span>
			</td>
			<td>
				<span id="bib">123</span>
			</td>
		</tr>
		<tr>
			<td>
				<span id="blank"></span>
			</td>
			<td>
				<span id="attempt">3.45m</span>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<span id="timer">1:30.29</span>
			</td>
		</tr>
	</table>
</body>
</html>