<?php
$scheme = $_GET['scheme'];
if ( array_key_exists('size', $_GET) ) {
	$size = (int)$_GET['size'];
} else {
	$size = 5;
}
$events = $_GET['events'];
$events = str_replace('^', '&', $events);
$events = explode('~' , urldecode($events));
?>
<!DOCTYPE html>
<html>
<head>
    <script src="./fitty.min.js"></script>

    <title>Call Room</title>
    <style>
        .callRoomDiv {
            text-align: center;
            padding-top: 3vh;
            height: 50%;
        }
        .callRoom {
            font-size: <?php echo $size ?>vw;
            color: white;
            border-top: 1px gray dashed;
            border-bottom: 1px gray dashed;
        }
        .nowcalling {
            font-size: 3vw;
            color:yellow;
        }
        .high {
            background-color: #092238;
        }
        .high > div {
            margin: 2vw;
            background-image: linear-gradient(#1b557b, #0c2b46);
            color: white;
        }
        .light {
            background-color: white;
            color: black;
        }
        .dark {
            background-color: black;
            color: white;
        }
        .clock {
            color: white;
            font-size: 10vh;
            font-family: Orbitron;
            letter-spacing: 7px;



        }
    </style>
    <script>
        function showTime(){
            var date = new Date();
            var h = date.getHours(); // 0 - 23
            var m = date.getMinutes(); // 0 - 59
            var s = date.getSeconds(); // 0 - 59
            var session = "";

            if(h == 0){
                h = 12;
            }

            if(h > 12){
                // h = h - 12;
                session = "";
            }

            h = (h < 10) ? "0" + h : h;
            m = (m < 10) ? "0" + m : m;
            s = (s < 10) ? "0" + s : s;

            var time = h + ":" + m + ":" + s ;
            document.getElementById("MyClockDisplay").innerText = time;
            document.getElementById("MyClockDisplay").textContent = time;

            setTimeout(showTime, 1000);

        }

    </script>
</head>
<body class="<?php echo $scheme?>" onload="showTime()">
<table>
    <tr>
        <td style="width: 100vw;height: 100%;display: flex;flex-direction: column;align-items: center;">
            <div id="MyClockDisplay" class="clock" ></div>
            <div class="callRoomDiv">
				<?php
                $nowCalling = false;
				foreach ($events as $event) {
                    if ( $event !== "") {
                        if ( $nowCalling === false ) {
                            echo '<div class="nowcalling">Now Calling</div>';
                            $nowCalling = true;
                        }
	                    echo '<div class="callRoom">' . $event . '</div>';
                    }
				}
                if ( !$nowCalling ) {
	                echo '<div class="nowcalling">Welcome to the Call Area</div>';
                }
				?>
            </div>
        </td>
<!--        <td style="width:56vw; height: 100%">-->
<!--            <iframe style="width:100%; height:100vh;background: white" src="/resources/LIA/Call room Schedule v5.3.htm">-->
<!---->
<!--            </iframe>-->
<!--        </td>-->
    </tr>
</table>

</body>
</html>