<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'scoreboards/rostercommon.php';

if ( array_key_exists( 'name', $_GET ) ) {
	$id = $_GET['name'];
} else {
	$id = 'touchController';
}
$idStr = str_replace( ' ', '', $id );

if ( array_key_exists( 'mid', $_GET ) ) {
	$mId = $_GET['mid'];
} else {
	$rUri   = strtolower( $_SERVER['REQUEST_URI'] );
	$script = strtolower( $_SERVER['SCRIPT_NAME'] );
	if ( $rUri === $script ) {
		$mId = 0;
	} else {
		$mId = substr( $rUri, strlen( $script ) + 1 );
	}
}
$mId = (int) $mId;
$trackEvents = readEvtFile($mId);
$fieldEvents = readEvtFile($mId, 'field.evt');
$headerBGColour = '#092238';
$headerColour = '#ffffff';
$tabBGColor = 'gray';
$gradientStart = '#1b557b';
$gradientEnd = '#0c2b46';
$gradientFill = 'linear-gradient(' . $gradientStart . ', ' . $gradientEnd . ')';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Roster Touch</title>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
	<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
	<style>
        body {
            background-color: <?php echo $headerBGColour ?>;
        }
		.header {
			background-color: <?php echo $headerBGColour ?>;
			color: <?php echo $headerColour ?>;
            font-family: Roboto,sans-serif;
			height: 3vh;
            font-weight: 700;
            padding-left: 2vw;
            font-size: 3vh;
		}
        #tabs-trackSL .ui-tabs-nav {
            margin: 5px 0 0 0 !important;
        }
        #tabs-fieldSL .ui-tabs-nav {
            margin: 5px 0 0 0 !important;
        }
        #tabs-trackR .ui-tabs-nav {
            margin: 5px 0 0 0 !important;
        }
        #tabs-fieldR .ui-tabs-nav {
            margin: 5px 0 0 0 !important;
        }
        .ui-tabs .ui-tabs-panel {
            padding: 0 !important;
        }
        .ui-tabs-vertical { width: 20em; }
        .ui-tabs-vertical .ui-tabs-nav { padding: .2em .1em .2em .2em; float: left;  }
        .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
        .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
        .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; }
        .ui-tabs-vertical .ui-tabs-panel { padding: 0.5em; }
        .ui-state-active {
            background-color: var(--e4s-navigation-bar--primary__background) !important;
        }
        .touchNav .ui-widget.ui-widget-content {
            border: none;
        }
        div ul {
            border: none !important;
            background-color: <?php echo $gradientStart ?> !important;
        }
        .touchNav ul {
            background-color: transparent !important;
            color: white !important;
            font-family: Roboto,sans-serif;
            font-size: 2vh;
            border: none !important;
        }
        .touchNav {
            height: 92vh;
            width: 100%;
            overflow-y: scroll;
            overflow-x: hidden;
            background-image: <?php echo $gradientFill ?>;
        }
        #touchNavigator .ui-state-active a {
            font-weight: bold;
            color: yellow;
            background-color: <?php echo $gradientStart ?> !important;
        }
        #touchNavigator > ul {
            background-color: <?php echo $tabBGColor ?> !important;
        }
        #touchNavigator {
                border: none !important;
                padding: 0 !important;
            }
            .heat{
                margin-left: 40px;
            }
        }
	</style>
	<script>
        let parentWindow = window.opener;
        let controllerName = '';
        if (parentWindow && typeof parentWindow.getControllerName !== 'undefined' ){
            controllerName = parentWindow.getControllerName();
        }
		$( function() {
			$( "#touchNavigator" ).tabs();
			$( "#tabs-trackSL" ).tabs({
            }).addClass( "ui-tabs-vertical ui-helper-clearfix" );
            $( "#tabs-trackSL li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
            $( "#tabs-fieldSL" ).tabs({
            }).addClass( "ui-tabs-vertical ui-helper-clearfix" );
            $( "#tabs-fieldSL li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
            $( "#tabs-trackR" ).tabs({
            }).addClass( "ui-tabs-vertical ui-helper-clearfix" );
            $( "#tabs-trackR li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
            $( "#tabs-fieldR" ).tabs({
            }).addClass( "ui-tabs-vertical ui-helper-clearfix" );
            $( "#tabs-fieldR li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
            if ( controllerName === '' ){
                $("li").show(); // show all events as no controller is present
            }else{
                showSelectedResults();
                setInterval(showSelectedResults,5000);
            }
		} );
        function showSelectedResults(){
            let meIds = parentWindow.getSelectedResults();
            if ( meIds === null || meIds.length < 1){
                return;
            }
            $("[rostertype=Results]").hide();
            for ( let i = 0; i < meIds.length; i++ ){
                let meId = meIds[i];
                let parentMeId = parseInt(meId.id) - parseInt(meId.heat) + 1;
                $("[name=" + meId.id + "]").show();
                $("[name=" + parentMeId + "_event]").show();
            }
        }
        function showResults(meId, type){
            let windowParms = "titlebar=0,toolbar=0,location=0,menubar=0,status=0,scrollbars=0";
            let url = "https://admin.rosterathletics.com/admin/score-boards/" + type.trim() + "?mid=<?php echo $mId?>&meId=" + meId;
            let useNamedWindow = true;

            if (controllerName !== ''){
                useNamedWindow = parentWindow.useNamedOutput();
            }
            if ( useNamedWindow ) {
                window.open(url, 'rosterTouch', windowParms);
            } else {
                window.open(url, 'rosterTouch');
            }
        }
    </script>
</head>
<body oncontextmenu="return false;" style="overflow: hidden">
	<div id="header" class="header">
		Event Information
	</div>
	<div id="touchNavigator">
		<ul>
			<li><a href="#tabs-trackSL">Track Start List</a></li>
            <li><a href="#tabs-trackR">Track Results</a></li>
			<li><a href="#tabs-fieldSL">Field Start List</a></li>
			<li><a href="#tabs-fieldR">Field Results</a></li>
		</ul>
        <div id="touchNav" class="touchNav">
            <div id="tabs-trackSL">
                <?php
                outputEvents( $trackEvents,'StartList' );
                ?>
            </div>
            <div id="tabs-fieldSL">
                <?php
                outputEvents( $fieldEvents,'StartList' );
                ?>
            </div>
            <div id="tabs-trackR">
		        <?php
		        outputEvents( $trackEvents,'Results' );
		        ?>
            </div>
            <div id="tabs-fieldR">
		        <?php
		        outputEvents( $fieldEvents,'Results' );
		        ?>
            </div>
        </div>
	</div>
</body>
</html>
<?php
function outputEvents( $events, $type ) {
    echo '<ul>';
    $style = '';
    if ( $type === 'Results' ) {
        $style = 'style="display: none;"';
    }
	foreach ( $events as $event => $eventObj ) {
        if ( $eventObj->heats === 1 ) {
?>
           <li rostertype="<?php echo trim($type) ?>" name="<?php echo $eventObj->startmeId ?>" <?php echo $style ?> class="eventName" onclick="showResults(<?php echo $eventObj->startmeId ?>,'<?php echo trim($type) ?>'); return false;">> <?php echo $event ?></li>
<?php
        }else{
?>
	        <li rostertype="<?php echo trim($type) ?>" name="<?php echo $eventObj->startmeId ?>_event" <?php echo $style ?> class="eventName"><?php echo $event ?></li>
<?php
            for ( $i = 0; $i < $eventObj->heats; $i++ ) {
?>
                <li rostertype="<?php echo trim($type) ?>" event="<?php echo $eventObj->startmeId + $i?>" name="<?php echo $eventObj->startmeId + $i ?>" <?php echo $style ?> class="eventName" onclick="showResults(<?php echo $eventObj->startmeId + $i ?>,'<?php echo trim($type) ?>'); return false;"><span class="heat">> Heat <?php echo $i + 1 ?></span></li>
<?php
            }
        }
	}
	echo '</ul>';
}
