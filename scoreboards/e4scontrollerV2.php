<?php
/*
 * To get a schedule. get the pdf version from Roster,
 * Convert to HTML online, Create a Fri,sat, sunday version
 * modify the css to use responsive tables and the show in Chrome and use the autoscroll extension
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'scoreboards/rostercommon.php';
include_once E4S_FULL_PATH . 'classes/rosterClass.php';
//define( 'E4S_FIELD_DISCIPLINE', 'Field' );
//define( 'E4S_TRACK_DISCIPLINE', 'Track' );

if ( array_key_exists( 'name', $_GET ) ) {
	$id = $_GET['name'];
} else {
	$id = 'Controller';
}
$idStr = str_replace( ' ', '', $id );
if ( array_key_exists( 'mid', $_GET ) ) {
	$mId = $_GET['mid'];
} else {
	$rUri   = strtolower( $_SERVER['REQUEST_URI'] );
	$script = strtolower( $_SERVER['SCRIPT_NAME'] );
	if ( $rUri === $script ) {
		$mId = 0;
	} else {
		$mId = substr( $rUri, strlen( $script ) + 1 );
	}
}
$mId = (int) $mId;
define( "ROSTER_DATA", ROSTER_URL . $mId );
$delay = 15;

if ( array_key_exists( 'delay', $_GET ) ) {
	$delay = $_GET['delay'];
}
if ( is_numeric( $delay ) == FALSE ) {
	$delay = 10;
} else {
	$delay = (int) $delay;
}

if ( $mId !== 0 ) {
	$trackEvents = [];
	$fieldEvents = [];
	$adverts     = [];
} else {
	$trackEvents = array();
	$trackEvent  = new stdClass();

	$trackEvent->name                 = "Demo Track 1";
	$trackEvent->startmeId            = 1;
	$trackEvent->heats                = 3;
	$trackEvents[ $trackEvent->name ] = $trackEvent;

	$trackEvent->name                 = "Demo Track 2";
	$trackEvent->startmeId            = 5;
	$trackEvent->heats                = 3;
	$trackEvents[ $trackEvent->name ] = $trackEvent;

	$fieldEvents                      = array();
	$fieldEvent                       = new stdClass();
	$fieldEvent->name                 = "Demo Field 1";
	$fieldEvent->startmeId            = 10;
	$fieldEvent->heats                = 5;
	$fieldEvents[ $fieldEvent->name ] = $fieldEvent;

	$fieldEvent->name                 = "Demo Field 2";
	$fieldEvent->startmeId            = 16;
	$fieldEvent->heats                = 5;
	$fieldEvents[ $fieldEvent->name ] = $fieldEvent;
	$adverts                          = array();
	$adverts[]                        = "https://entry4sports.com";
}

?>
    <!DOCTYPE html>
    <html>
    <head>
        <title>Controller : <?php echo $id ?></title>
        <style>
            body div #e4s-nav-bar {
                height: 100px !important;
            }

            .e4sTitle {
                color: white;
                font-size: 2vw;
            }

            .eventName {
                font-size: 1.5vw;
                font-weight: 900;
            }

            .config {
                margin: 10px;
            }

            section {
                display: table;
                width: 100%;
            }

            section > * {
                display: table-row;
                height: 30px;
            }

            section .configCol {
                display: table-cell;
            }

            .content {
                margin: 0px 50px;
                flex-direction: row;
                display: flex;
                flex-wrap: nowrap;
                align-items: flex-start;
                gap: 5vw;
                height: 70vh;
            }

            .<?php echo E4S_TRACK_DISCIPLINE ?>Content {
                display: flex;
                flex-direction: column;
                flex-wrap: nowrap;
                align-items: flex-start;
                height: 100%;
                overflow-y: scroll;
                gap: 2vh;
            }

            .<?php echo E4S_FIELD_DISCIPLINE ?>Content {
                display: flex;
                flex-direction: column;
                flex-wrap: nowrap;
                align-items: flex-start;
                height: 100%;
                overflow-y: scroll;
                gap: 2vh;
            }
            .contentGroup {
                height:85%;
                width: 45%;
            }
            .eventsTitle {
                font-size: 1.5vw;
                font-weight: 900;
                padding-bottom: 2vh;
            }

            button {
                -webkit-appearance: button;
                background: var(--e4s-button--primary__background);
                height: var(--e4s-button__height);
                padding: 0px 10px 0px 10px;
                border: var(--e4s-button__border);
                border-radius: var(--e4s-button__border-radius);
                cursor: pointer;
                /* transition: all 0.36s; */
                color: white;
                font-weight: 500;
            }

            .warnButton {
                background: var(--e4s-status-pill--error__background);
                color: var(--e4s-status-pill--error__text-color);
            }

            .e4sNumber {
                width: 4vw;
                padding: 0px 5px 0px 5px;
            }
        </style>
        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>

        <script>
            let trackEvents = new Array();
            let fieldEvents = new Array();
            let E4S_EVENT_FIELD = '<?php echo E4S_EVENT_FIELD ?>';
            let E4S_EVENT_TRACK = '<?php echo E4S_EVENT_TRACK ?>';
            const E4S_FIELD_DISCIPLINE = '<?php echo E4S_FIELD_DISCIPLINE ?>';
            const E4S_TRACK_DISCIPLINE = '<?php echo E4S_TRACK_DISCIPLINE ?>';
            const E4S_WINDOW_START_TYPE = 'e4sStartList';
            const E4S_WINDOW_CALL_TYPE = 'e4sCallRoom';
            let windowParms = "titlebar=0,toolbar=0,location=0,menubar=0,status=0,scrollbars=0";
            let controllers = [];

            function e4sAlert(msg, title) {
                alert(msg, title);
            }

            function showPleaseWait() {
            }

            function getCompetition() {
                return {
                    id: <?php echo $mId; ?>,
                    date: new Date().toISOString().split('T')[0]
                }
            }

            let eventId = 1;
            function isTrackOrField(data){
                if ( data.type ){
                    return data.type;
                }
                return E4S_EVENT_TRACK;
            }
            function getE4SDisplayEventName(eventName, eventGender, heatFinal){
                let closeSpan = "";

                if (heatFinal !== "" ){
                    heatFinal = " " + heatFinal;
                }
                if (eventGender !== "" ){
                    eventGender = " - " + eventGender;
                }

                return eventName + heatFinal + eventGender + closeSpan;
            }
            function getE4SEventName(data){
                let eventName = data.eventCode + " : " + data.eventName;

                return eventName;
            }
            function addStartListEvent(data, callDisplay = true){
                let eventName = getE4SEventName(data);
                let eventStartDate = new Date(data.eventStart);
                let dateNow = new Date();

                let events = trackEvents;
                let heatCount = 1;
                if ( isTrackOrField(data) === E4S_EVENT_FIELD ){
                    events = fieldEvents;
                }else{
                    heatCount = data.heats;
                }

                let eventGender = "";
                let heatFinal = data.heatFinal;
                let eventNameDisplay = getE4SDisplayEventName(eventName, eventGender, heatFinal);
                let eventStartText = eventStartDate.getHours() + ":" + right("0" + eventStartDate.getMinutes(),2);
                eventNameDisplay = eventStartText + "-" + eventNameDisplay;
                if ( !events[eventNameDisplay] ) {
                    events[eventNameDisplay] = {
                        name: eventNameDisplay,
                        startmeId: 1,
                        id: eventId++,
                        startTime: data.eventStart,
                        heats: heatCount
                    };
                }
                if ( callDisplay ) {
                    debounceDisplay();
                }
            }
            function getParentControllerObj() {
                return $("#parentController");
            }

            function getParentControllerName() {
                return getParentControllerObj().val();
            }

            function setParentControllerName(value) {
                if (value === "") {
                    return;
                }
                let parentObj = getParentControllerObj();
                // if does not have value as option then add it
                if (parentObj.find("option[value='" + value + "']").length === 0) {
                    let option = "<option value='" + value + "'>" + value + "</option>";
                    parentObj.append(option);
                }
                if (value !== parentObj.val()) {
                    parentObj.val(value);
                    window.setTimeout(parentChange, 500);
                }
            }

            function getControllerObj() {
                return $("#controllerName");
            }

            function getControllerName() {
                return getControllerObj().val();
            }

            function getEventDelayObj() {
                return $("#delay");
            }

            function getEventDelay() {
                return parseInt(getEventDelayObj().val());
            }

            function setEventDelay(delay) {
                getEventDelayObj().val(delay);
            }

            function renameController() {
                let name = getControllerName();
                let mId = getCompetition().id;
                let url = location.protocol + '//' + location.host + location.pathname.replace("/" + mId, '') + "?name=" + name + "&delay=" + getEventDelay() + "&mid=" + mId
                window.location.href = url;
            }

            function processResponse(data) {
                if (data.requestController !== getControllerName()) {
                    return;
                }
                if (controllers.indexOf(data.controllerName) === -1) {
                    controllers.push(data.controllerName);
                    // add to select
                    let select = getParentControllerObj();
                    let option = "<option value='" + data.controllerName + "'>" + data.controllerName + "</option>";
                    select.append(option);
                }
            }

            let browser = '';
            let eventCount = 0;
            let currentEvent = {id: 0};
            let screenInterval;
            let adminWindowId;
            let mainWindowId;
            let callroomWindowId;
            let scheduleWindowId;
            let touchWindowId;
            let eventObjs = {};
            let advertIndex = 0;
            let eventCounter = 0;
            let advertCount = 0;

            function setOutputDisplay(set) {
                updateNamedOutputOptions(set);
                if ( inheriting() ){
                    return;
                }
                if (set) {
                    $("#display_<?php echo $idStr ?>Yes").prop("checked", true);
                    $("#display_<?php echo $idStr ?>No").prop("checked", false);
                } else {
                    $("#display_<?php echo $idStr ?>Yes").prop("checked", false);
                    $("#display_<?php echo $idStr ?>No").prop("checked", true);
                }
            }

            function getOutputDisplay() {
                return true;
                let display = $("[name=display_<?php echo $id ?>]:checked").val() === "1";
                if (!display) {
                    if (isWindowAvailable(adminWindowId)) {
                        adminWindowId.window.close();
                    }
                    if (isWindowAvailable(mainWindowId)) {
                        mainWindowId.window.close();
                    }
                    if (isWindowAvailable(callroomWindowId)) {
                        callroomWindowId.window.close();
                    }
                    if (isWindowAvailable(touchWindowId)) {
                        touchWindowId.window.close();
                    }
                }
                return display;
            }

            function inheriting() {
                let parentController = getParentControllerName();
                if (parentController === "") {
                    return false;
                }
                return true;
            }

            function getCallroomUrl() {
                let url = location.href.split("e4scontrollerV2.php")[0] + "e4scallroomv2.php";
                let scheme = getScheme();
                let stripeTimes = $("#callRoomStripTimes").prop("checked");
                if (scheme === "") {
                    scheme = "high";
                }
                url += "?scheme=" + scheme;
                url += "&size=" + $("#callroomSize").val();
                url += "&events=";
                let sep = '';
                $("[heat]:checked").each(
                    function( index, meId){
                        let obj = $(meId);
                        let event = obj.attr("event");
                        if ( true ){
                            if ( event.indexOf("-") > 0 ){
                                event = event.split("-")[1];
                            }
                        }
                        url += sep + encodeURI(event.replace("&", "^"));
                        sep = '~';
                        let heat = parseInt(obj.attr("heat"));
                        if (heat === 1) {
                            // check if there is a 2nd heat else pass 0 ( single heat )
                            if ($("[event*='" + event + "']").length < 2) {
                                heat = 0;
                            }
                        }
                        if (heat > 0) {
                            url += " Heat " + heat;
                        }
                    }
                );

                return url;
            }

            function getFullUrl() {
                let useEvent = currentEvent;
                let type = "StartList";
                if (useEvent.type === 1) {
                    type = "Results";
                }

                let url = getRosterBaseUrl(type) + "&meId=" + useEvent.id ;
                return url;
            }
            function getRosterAdminUrl() {
                return "https://admin.rosterathletics.com/admin";
            }
            function getRosterBaseUrl(type = "StartList") {
                let scheme = getScheme();
                if (scheme !== "") {
                    scheme = "&scheme=" + scheme;
                }
                let url = getRosterAdminUrl() + "/score-boards/" + type + "?mid=<?php echo $mId?>" + scheme;
                return url;
            }
            function getFrameName() {
                return "main_<?php echo $mId . '.' . $idStr ?>";
            }

            function getCallFrameName() {
                return "callroom_<?php echo $idStr ?>";
            }
            function getScheduleFrameName() {
                return "schedule_<?php echo $idStr ?>";
            }

            function showThisOne(meId) {
                stopEventCycle();
                startEventCycle(meId);
            }

            function isWindowAvailable(windowId) {
                return windowId && windowId.window !== null;
            }
            function startEventCycle(meId = 0) {
                advertIndex = 0;
                eventCounter = 0;

                // getSelectedEvents(meId);
                clearInterval(screenInterval);

                let url = '';
                let frame = '';
                let target = '';

                url = getCallroomUrl();
                frame = getCallFrameName();
                target = E4S_WINDOW_CALL_TYPE;

                callroomWindowId = openwindow(url, frame, windowParms);
            }
            function namedOutputChanged(){
                if (inheriting()) {
                    return;
                }
                saveToStorage();
            }
            function updateNamedOutputOptions(set){
                if ( set ){
                    $(".namedOutputOptions").show();
                }else{
                    $(".namedOutputOptions").hide();
                }
            }
            function setNamedOutput(checked){
                $("#namedOutput").prop("checked", checked);
            }
            function useNamedOutput(){
                return $("#namedOutput").prop("checked");
            }
            function openwindow(url, frame, options){
                let windowId = null;

                if ( useNamedOutput() ) {
                    windowId = window.open(url, frame, options);
                } else {
                    windowId = window.open(url, frame);
                }
                if (windowId) {
                    windowId.focus();
                }
                return windowId;
            }
            function isTouchEnabled() {
                return showingOutput(E4S_OUTPUT_TOUCH);
            }

            function resetInterval(delay) {
                clearInterval(screenInterval);
                screenInterval = setInterval(openNextUrl, delay * 1000);
            }

            function openNextUrl() {
                if (getOutputDisplay()) {
                    if (!isWindowAvailable(mainWindowId) || !isWindowAvailable(adminWindowId)) {
                        // window closed
                        // alert("A Roster window has been closed. This will be re-opened but check casting options");
                        return startEventCycle();
                    }
                }

                let showNext = true;
                let delay = getEventDelay();
                let url = '';
                let frame = getFrameName();

                if (showNext) {
                    getNextSelectedEvent();
                    showActiveEvent();
                    url = getFullUrl();
                    if (getOutputDisplay()) {
                        mainWindowId.location.href = url;
                    }
                }
                resetInterval(delay);
            }

            function updateDelay() {
                const minDelay = 5;
                if (getEventDelay() < minDelay) {
                    setEventDelay(minDelay);
                }
                restartController();
            }

            function clearCallRoomSelected() {
                let selector = "[callroom]:checked";
                $(selector).prop("checked", false);
            }

            function updateCallRoomSettings() {
                updateCallRoom({
                    "enabled": getCallRoomEnabled(),
                    "sync": getCallRoomSyncEnabled(),
                    "meId": getCallRoomMeIds(),
                    "size": $("#callroomSize").val(),
                    "times": $("#callRoomStripTimes").prop("checked")
                });
                if (!getCallRoomEnabled()) {
                    if (callroomWindowId) {
                        callroomWindowId.window.close();
                        callroomWindowId = null;
                    }
                    // hide the callRoom options
                    $(".callroomOptions").hide();
                } else {
                    $(".callroomOptions").show();
                }
                // if ( !showingOutput(E4S_OUTPUT_SCHEDULE) ){
                //     if (scheduleWindowId) {
                //         scheduleWindowId.window.close();
                //         scheduleWindowId = null;
                //     }
                // }
                restartController();
            }

            function getCallRoomMeIds() {
                let selector = "[callroom]:checked";
                let meIds = [];
                $(selector).each(function () {
                    meIds.push(parseInt(this.value));
                });
                return meIds;
            }

            function getCallRoomEnabled() {
                return true;
                return showingOutput(E4S_OUTPUT_CALLROOM);
            }

            function getCallRoomSyncEnabled() {
                return $("[name=callRoomSyncEnabled]:checked").val() === "1";
            }

            function setCallRoomSync(enabled) {
                if (enabled) {
                    $("#callroomSyncEnabled").prop("checked", true);
                    $("#callroomSyncDisabled").prop("checked", false);
                } else {
                    $("#callroomSyncEnabled").prop("checked", false);
                    $("#callroomSyncDisabled").prop("checked", true);
                }
            }

            function updateCallRoom(obj) {
                return "";
                let enabled = obj.enabled;
                let meIds = obj.meId;
                let size = obj.size;
                let times = obj.times;

                if (times) {
                    $("#callRoomStripTimes").prop("checked", true);
                } else {
                    $("#callRoomStripTimes").prop("checked", false);
                }
                let selector = "[callroom]";
                if (!enabled) {
                    $(selector).hide();
                } else {
                    $(selector).show();
                }
                if (enabled) {
                    $("#callroomEnabled").prop("checked", true);
                    $("#callroomDisabled").prop("checked", false);
                } else {
                    $("#callroomEnabled").prop("checked", false);
                    $("#callroomDisabled").prop("checked", true);
                }
                setCallRoomSync(obj.sync);
                $("#callroomSize").val(size);
                $("[callroom]").prop("checked", false);
                for (meId of meIds) {
                    $("#" + meId + "_call").prop("checked", true);
                }
            }

            function displayChanged() {
                updateNamedOutputOptions(getOutputDisplay());
                saveToStorage();
                restartController();
            }

            function restartController() {
                stopEventCycle();
                startEventCycle();
            }

            function setAll(meIds, type, checked = true) {
                for (meId of meIds) {
                    setMeId(meId, type, checked, false);
                }
                getSelectedEvents();
            }

            function setMeId(meId, type = -1, checked = false, update = true) {
                let selector = "[name=" + meId + "]";
                if (type > -1) {
                    selector += "[value=" + type + "]";
                }
                let radios = $(selector);
                radios.each(function () {
                    this.checked = checked;
                });
                if (update) {
                    getSelectedEvents();
                }
            }

            function getNextSelectedEvent() {
                getSelectedEvents();
                if (eventCount < 2) {
                    return;
                }
                let set = false;
                let nextEvent = currentEvent;
                let found = false;
                for (let key in eventObjs) {
                    if (found) {
                        nextEvent = eventObjs[key];
                        set = true;
                        break;
                    }
                    if (eventObjs[key].id === currentEvent.id) {
                        found = true;
                    }
                }
                if (nextEvent.id === 0 || !set) {
                    for (let key in eventObjs) {
                        nextEvent = eventObjs[key];
                        break;
                    }
                }

                currentEvent = nextEvent;
            }

            function getSelectedEvents(meId = 0) {
                eventObjs = {};
                eventCount = 0;
                let selector = "[e4sevent]:checked";
                $(selector).each(function () {
                    eventCount++;
                    let obj = {
                        id: parseInt(this.id),
                        type: parseInt(this.value),
                        event: this.attributes.e4sevent.value
                    }
                    eventObjs[this.id] = obj;
                    if (currentEvent.id === 0 || obj.id === meId) {
                        currentEvent = obj;
                    }
                });
            }

            function showActiveEvent(showEvent = currentEvent) {
                eventCounter++;
                $("[e4scurrent]").html("");
                $("[e4scurrent=" + showEvent.id + "]").html(" <<");
                let heat = $("#" + showEvent.id).attr("e4sheat");
                if (heat === "1") {
                    let event = $("#" + showEvent.id).attr("e4sevent");
                    if ($("[e4sevent='" + event + "']").length === 0) {
                        heat = '';
                    }
                }
                if (heat !== "") {
                    heat = " - Heat " + heat;
                }
                $("#controllerMessage").html("Current Event : " + showEvent.event + ' ' + heat);
            }

            function stopEventCycle() {
                $("#controllerMessage").html("&nbsp;");
                screenInterval = clearInterval(screenInterval);
            }

            function eventSelected() {
                let obj = event.currentTarget;
                let results = obj.value === "1";
                let checked = obj.checked;
                let meId = obj.id;
                if (results) {
                    // if results selected then clear start list/callroom
                    $("#" + meId + "_call").prop("checked", false);
                } else {
                    if (getCallRoomEnabled()) {
                        if (getCallRoomSyncEnabled()) {
                            // set callroom
                            $("#" + meId + "_call").prop("checked", true);
                        }
                    }
                }
            }

            function inCallRoomSelected(eventName, useHeat) {
                updateCallRoomSettings();
            }

            function addEvent() {
                eventSelected();
                getNextSelectedEvent();
                if (!screenInterval || eventCount < 2) {
                    startEventCycle();
                }
            }

            function parentChange() {
                if (inheriting()) {
                    $("button").hide();
                    $("input:not([never])").attr("disabled", true);
                    saveToStorage();
                    requestParentData();
                } else {
                    $("button").show();
                    $("input").attr("disabled", false);
                }
            }

            function initParentSelector() {
                controllers = [];
                let parentDiv = $("#parentControllerDiv");
                parentDiv.html("Parent Controller : <select id=\"parentController\" onchange=\"parentChange(); return false;\"><option value=''>N/A</option></select>");
            }

            function getScheme() {
                return $("[name=scheme_<?php echo $idStr ?>]:checked").val();
            }

            function setScheme(scheme) {
                $("[name=scheme_<?php echo $idStr ?>]:checked").prop("checked", false);
                $("#scheme_<?php echo $idStr ?>" + scheme).prop("checked", true);
            }

            function saveToStorage(useData = null) {
                let data = useData;
                if (data === null) {
                    data = getControllerData();
                }
                localStorage.setItem("controller_<?php echo $idStr ?>", JSON.stringify(data));
            }
            function getControllerData(){
                let data = {
                    eventObjs: eventObjs,
                    delay: getEventDelay(),
                    useNamedOutput: useNamedOutput(),
                    scheme: getScheme(),
                    parentController: getParentControllerName(),

                    callRoom: {
                        enabled: getCallRoomEnabled(),
                        sync: getCallRoomSyncEnabled(),
                        meId: getCallRoomMeIds(),
                        size: parseInt($("#callroomSize").val()),
                        times: $("#callRoomStripTimes").prop("checked")
                    },
                    display: getOutputDisplay(),
                    disciplines:{
                        field: showingDiscipline(E4S_FIELD_DISCIPLINE),
                        track: showingDiscipline(E4S_TRACK_DISCIPLINE)
                    },
                    output:{
                        start: showingOutput(E4S_OUTPUT_START),
                        results: showingOutput(E4S_OUTPUT_RESULTS),
                        callroom: showingOutput(E4S_OUTPUT_CALLROOM),
                        schedule: showingOutput(E4S_OUTPUT_SCHEDULE),
                        touch: showingOutput(E4S_OUTPUT_TOUCH)
                    }
                }
                return data;
            }
            function loadFromStorage(data = null) {
                if (data === null) {
                    data = localStorage.getItem("controller_<?php echo $idStr ?>");
                }
                if (data === null) {
                    return;
                }
                if ( typeof data === 'string') {
                    data = JSON.parse(data);
                }
                setNamedOutput(data.useNamedOutput);
                setOutputDisplay(data.display);
                eventObjs = data.eventObjs;
                // clear All
                $("[e4sevent]").prop("checked",false);
                for (key in eventObjs) {
                    let eventObj = eventObjs[key];
                    let selector = "[id=" + eventObj.id + "][value=" + eventObj.type + "]";
                    $(selector).prop("checked", true);
                }
                setDisciplineOptions(data.disciplines);
                setOutputOptions(data.output);
                setEventDelay(data.delay);
                setScheme(data.scheme);
                updateCallRoom({
                    "enabled": data.callRoom.enabled,
                    "sync": data.callRoom.sync,
                    "meId": data.callRoom.meId,
                    "size": data.callRoom.size,
                    "times": data.callRoom.times
                });
                setParentControllerName(data.parentController);
            }

            function resetController() {
                if (confirm("Are you sure you want to RESET the controller. All settings will be cleared ?")) {
                    localStorage.removeItem("controller_<?php echo $idStr ?>");
                    location.reload();
                }
            }

            function updateTitle() {
                $("#e4s-nav-bar").css("z-index", 1);
                let titleObj = $(".e4s-navigation-bar-menu");
                let title = 'E4S Callroom Controller';
                titleObj.html(title);
                titleObj.addClass("e4sTitle");
            }

            function reportWindowSize() {
                $("#content").css("height", window.innerHeight - 400);
            }

            // Discipline functions
            function onLoadDiscipline() {
                let field = $("#discipline" + E4S_FIELD_DISCIPLINE).prop("checked");
                field = true;
                let track = $("#discipline" + E4S_TRACK_DISCIPLINE).prop("checked");
                track = true;
                displayDiscipline(E4S_FIELD_DISCIPLINE, field);
                displayDiscipline(E4S_TRACK_DISCIPLINE, track);
            }
            function updateDiscipline(){
                let field = $("#discipline" + E4S_FIELD_DISCIPLINE).prop("checked");
                field = true;
                let track = $("#discipline" + E4S_TRACK_DISCIPLINE).prop("checked");
                track = true;
                displayDiscipline(E4S_FIELD_DISCIPLINE, field);
                displayDiscipline(E4S_TRACK_DISCIPLINE, track);
                saveToStorage();
                updateOutputDisplay();
            }
            function displayDiscipline(discipline, show=true) {
               if (show){
                   $("#"+discipline+"ContentGroup").show();
               }else{
                   $("#"+discipline+"ContentGroup").hide();
               }
            }
            function showingDiscipline(discipline){
                let show = false;
                let selector = "#discipline" + discipline + ":checked";
                if ($(selector).length > 0){
                    show = true;
                }
                return show;
            }
            function setDisciplineOptions(disciplines){
                setDiscipline(E4S_FIELD_DISCIPLINE, disciplines.field);
                setDiscipline(E4S_TRACK_DISCIPLINE, disciplines.track);
            }
            function setDiscipline(discipline, show){
                let selector = "#discipline" + discipline;
                $(selector).prop("checked", show);
            }
            // Output functions

            function showingOutput(output){
                let show = false;
                let selector = "#output" + output + ":checked";
                if ($(selector).length > 0){
                    show = true;
                }
                return show;
            }
            function setOutputOptions(output){
                // setOutput(E4S_OUTPUT_START, output.start);
                // setOutput(E4S_OUTPUT_RESULTS, output.results);
                // setOutput(E4S_OUTPUT_CALLROOM, output.callroom);
                // setOutput(E4S_OUTPUT_SCHEDULE, output.schedule);
                // setOutput(E4S_OUTPUT_TOUCH, output.touch);
            }
            function setOutput(output, show){
                let selector = "#output" + output;
                $(selector).prop("checked", show);
            }
            function scheduleSelected(){
                saveToStorage();
                checkScheduleOutput();
            }
            function checkScheduleOutput(){
                let url = $("#schedule").val();
                if (url === ""){
                    if (scheduleWindowId) {
                        scheduleWindowId.window.close();
                        scheduleWindowId = null;
                    }
                }else{
                    if(getOutputDisplay()) {
                        url = '/results/<?php echo $mId?>/' + url;
                        scheduleWindowId = openwindow(url, getScheduleFrameName(), windowParms);
                    }
                }
            }
            function updateOutputDisplay(){
                // let options = $("." + E4S_OUTPUT_START + "Options");
                // if ( showingOutput(E4S_OUTPUT_START) ) {
                //     options.show();
                // }else{
                //     options.hide();
                // }
                // options = $("." + E4S_OUTPUT_RESULTS + "Options");
                // if ( showingOutput(E4S_OUTPUT_RESULTS) ) {
                //     options.show();
                // }else{
                //     options.hide();
                // }
                // options = $("." + E4S_OUTPUT_SCHEDULE + "Options");
                // if ( showingOutput(E4S_OUTPUT_SCHEDULE) ) {
                //     options.show();
                // }else{
                //     options.hide();
                // }
                updateCallRoomSettings();
            }
            function updateOutputs(){
                if (!showingOutput(E4S_OUTPUT_START)) {
                    $("[startevent]").prop("checked",false);
                }
                if (!showingOutput(E4S_OUTPUT_RESULTS)) {
                    $("[resultevent]").prop("checked",false);
                }
                if (!showingOutput(E4S_OUTPUT_CALLROOM)) {
                    $("[callroomevent]").prop("checked",false);
                }
                if (!showingOutput(E4S_OUTPUT_SCHEDULE)) {
                    $("#schedule").val("");
                }

                saveToStorage();
                updateOutputDisplay();
            }

            function getSelectedResults(){
                let meIds = [];
                $("[resultevent]:checked").each(function(){
                    meIds.push({
                        id: this.id,
                        heat:$(this).attr("e4sheat")
                    });
                });
                return meIds;
            }
            function initController() {
                updateTitle();

                initParentSelector();
                loadFromStorage();
                onLoadDiscipline();
                updateOutputDisplay();
                // window.setTimeout(function(){
                //     location.reload()
                // },600000);
            }
            let displayTimeout = 0;
            function debounceDisplay(){
                // update the tabs with debounce
                clearTimeout(displayTimeout);
                displayTimeout = window.setTimeout(displayEvents, 1000);
            }
            function displayEvents(){
                let trackEventsArr = [];
                let fieldEventsArr = [];
                for(let e in trackEvents){
                    trackEventsArr.push(trackEvents[e]);
                }
                for(let f in fieldEvents){
                    fieldEventsArr.push(fieldEvents[f]);
                }
                trackEventsArr.sort(function(a, b){
                    let aDate = new Date(a.startTime);
                    let bDate = new Date(b.startTime);
                    // checkMultiDay(aDate, bDate);
                    return aDate - bDate;
                });
                fieldEventsArr.sort(function(a, b){
                    let aDate = new Date(a.startTime);
                    let bDate = new Date(b.startTime);
                    // checkMultiDay(aDate, bDate);
                    if ( aDate - bDate === 0){
                        if (b.eventCode < a.eventCode ){
                            return -1;
                        }
                        if (b.eventCode > a.eventCode ){
                            return 1;
                        }
                        return 0;
                    }
                    return aDate - bDate;
                });
                outputEvents(fieldEventsArr,E4S_FIELD_DISCIPLINE);
                outputEvents(trackEventsArr, E4S_TRACK_DISCIPLINE);
            }
            function outputEvents(events, type = "<?php echo E4S_FIELD_DISCIPLINE ?>"){
                let html = "";
                for ( let e in events ){
                    let event = events[e];
                    let dateNow = new Date();
                    let eventStartDate = new Date(event.startTime);
                    let todaysEvent = eventStartDate.getDate() === dateNow.getDate();
                    // force display ( ignore multi date competition )
                    if ( location.search.indexOf("force=1") > -1 ){
                        todaysEvent = true;
                    }
                    todaysEvent = true;
                    if ( todaysEvent ){
                        let eventHTML = "";
                        eventHTML += '<div id="eventContent" class="eventContent">';
                        eventHTML += '<span class="eventName">' + event.name + '</span>';
                        eventHTML += '<table>';
                        eventHTML += '<tr>';
                        eventHTML += '<td style="width:75px;">Heat</td>';
                        eventHTML += '<td class="<?php echo E4S_OUTPUT_CALLROOM ?>Options" callroom style="width:150px;text-align: center;">In Call Room</td>';
                        eventHTML += '</tr>';

                        for (let h = 1; h <= event.heats; h++) {
                            eventHTML += '<tr>';
                            eventHTML += '<td>' + h + '</td>';
                            eventHTML += '<td class="<?php echo E4S_OUTPUT_CALLROOM ?>Options" callroom style="text-align: center;">';
                            let useHeat = parseInt(h);
                            if (event.heats === 1) {
                                useHeat = 0;
                            }
                            eventHTML += '<input type="checkbox" id="' + event.id + '_' + h + '_call" event="' + event.name + '" value="' + h + '" heat="' + h + '" onchange="updateCallRoomSettings()">';
                            eventHTML += '</td>';
                            eventHTML += '</tr>';
                        }
                        eventHTML += '</table>';
                        eventHTML += '</div>';
                        html += eventHTML;
                    }
                }

                $("#" + type + "Content").html(html);
            }
            // Document Ready
            $(document).ready(function () {
                browser = (function (agent) {
                    switch (true) {
                        case agent.indexOf("edge") > -1: return "edge";
                        case agent.indexOf("edg/") > -1: return "newedge"; // Match also / to avoid matching for the older Edge
                        case agent.indexOf("opr") > -1 && !!window.opr: return "opera";
                        case agent.indexOf("chrome") > -1 && !!window.chrome: return "chrome";
                        case agent.indexOf("trident") > -1: return "ie";
                        case agent.indexOf("firefox") > -1: return "firefox";
                        case agent.indexOf("safari") > -1: return "safari";
                        default: return "other";
                    }
                })(window.navigator.userAgent.toLowerCase());
                window.onresize = reportWindowSize;
                $("[e4sevent]").click(function () {
                    addEvent();
                });
                processInitialData()

                initController();
            });
            function right(str, chr){
                return str.slice(str.length-chr,str.length);
            }
            function left(str, chr){
                return str.slice(0, chr - str.length);
            }
            function processInitialData(){
                <?php
                $sql = "select eventGroupId egId, max(heatNo) heatNo
                        from " . E4S_TABLE_SEEDING . " s,
                                " . E4S_TABLE_EVENTGROUPS . " eg
                            where eg.id = eventGroupId
                        and eg.compid = " . $mId . "
                        group by eventGroupId";
                $result = e4s_queryNoLog($sql);

                $heats = array();
                while ($row = mysqli_fetch_assoc($result)) {
	                $heats[(int)$row['egId']] = (int)$row['heatNo'];
                }

                $sql = "select id eventId, name eventName, startdate eventStart, typeno eventCode, options
                       from " . E4S_TABLE_EVENTGROUPS . "
                       where compid = " . $mId;
                $result = e4s_queryNoLog($sql);
                $events = array();
                while ($row = mysqli_fetch_assoc($result)) {
                    $eventObj = new stdClass();
                    $eventObj->eventId = (int)$row['eventId'];
                    $eventObj->eventName = $row['eventName'];
                    $eventObj->eventStart = $row['eventStart'];
                    $eventObj->eventCode = $row['eventCode'];
                    $eventObj->type = $row['eventCode'][0];
                    $eventObj->heatFinal = '';
                    $eventObj->entries = array();
                    $entryObj = new stdClass();
                    if ( array_key_exists( $eventObj->eventId, $heats) ) {
	                    $eventObj->heats = $heats[ $eventObj->eventId ];
                    }else {
	                    $entryObj->heat = 1;
                    }
	                $eventObj->entries[] = $entryObj;
                    $events[] = $eventObj;
                }
                echo 'data = {
                    "startList":' . json_encode($events) . ' }';
                 ?>

                    for(let sl in data.startList){
                        let event = data.startList[sl];
                        this.addStartListEvent(event, false);
                    }
                    displayEvents();

            }
        </script>
    </head>
    <body style="overflow:hidden;">
	<?php
	include_once get_template_directory() . '-child/header-e4s.php';
	?>
    <div id="configuration" class="config">
        <section>
            <header>
            <span class="configCol">
                Controller :
            </span>
            <span class="configCol">
                Name : <input id="controllerName" value="<?php echo $id ?>" onchange="renameController(); return false;">
            </span>
            <span class="configCol">
                Call Room :
            </span>

            <span class="configCol">
                Text size : <input class="e4sNumber" type="number" id="callroomSize" name="callroomSize" value="3"
                                   min="3" max="10" onchange="updateCallRoomSettings();return false;"> vh
            </span>
            </header>
            <div class="configRow">
            <span class="configCol">
               &nbsp;
            </span>
                <span class="configCol">
                 Scheme : <input type="radio" id="scheme_<?php echo $idStr ?>" name="scheme_<?php echo $idStr ?>"
                                 onchange="restartController()" value="" checked> Blue
                     <input type="radio" id="scheme_<?php echo $idStr ?>light" name="scheme_<?php echo $idStr ?>"
                            onchange="restartController()" value="light"> Light
                     <input type="radio" id="scheme_<?php echo $idStr ?>dark" name="scheme_<?php echo $idStr ?>"
                            onchange="restartController()" value="dark"> Dark
            </span>
                <span class="configCol" style="display:none">
                     Discipline: <input checked type="checkbox" id="discipline<?php echo E4S_TRACK_DISCIPLINE ?>" name="discipline"
                                        onchange="updateDiscipline()" value="<?php echo E4S_TRACK_DISCIPLINE ?>"> <?php echo E4S_TRACK_DISCIPLINE ?>
                            <input checked type="checkbox" id="discipline<?php echo E4S_FIELD_DISCIPLINE ?>" name="discipline"
                                   onchange="updateDiscipline()" value="<?php echo E4S_FIELD_DISCIPLINE ?>"> <?php echo E4S_FIELD_DISCIPLINE ?>

            </span>
            <span class="configCol">

            </span>
            </div>
            <div class="configRow">
            <span class="configCol">
               &nbsp;
            </span>
            <span class="configCol" style="display: none">
                Show Outputs : <input never type="radio" id="display_<?php echo $idStr ?>Yes"
                                      name="display_<?php echo $idStr ?>" onchange="displayChanged()" value="1" checked> Yes
                                 <input never type="radio" id="display_<?php echo $idStr ?>No" name="display_<?php echo $idStr ?>"
                                        onchange="displayChanged()" value="0"> No
            </span>
            <span class="configCol" style="display: none">
                Outputs :   <input type="checkbox" id="output<?php echo E4S_OUTPUT_START ?>"
                                   onchange="updateOutputs()" value="<?php echo E4S_OUTPUT_START ?>"> Start List
                            <input type="checkbox" id="output<?php echo E4S_OUTPUT_RESULTS ?>"
                                   onchange="updateOutputs()" value="<?php echo E4S_OUTPUT_RESULTS ?>"> Results
                            <input type="checkbox" id="output<?php echo E4S_OUTPUT_CALLROOM ?>"
                                   onchange="updateOutputs()" value="<?php echo E4S_OUTPUT_CALLROOM ?>" checked> Call Room
                            <input type="checkbox" id="output<?php echo E4S_OUTPUT_SCHEDULE ?>"
                                   onchange="updateOutputs()" value="<?php echo E4S_OUTPUT_SCHEDULE ?>"> Information
                            <input type="checkbox" id="output<?php echo E4S_OUTPUT_TOUCH ?>"
                                   onchange="updateOutputs()" value="<?php echo E4S_OUTPUT_TOUCH ?>"> Touch Screen
            </span>
            <span class="configCol"  style="display: none">
                <span class="namedOutputOptions">
                    Use Named Outputs: <input never type="checkbox" id="namedOutput" name="namedOutput"
                           onchange="namedOutputChanged();" value="y"> Yes
                </span>
            </span>
            </div>
            <div class="configRow"  style="display: none">

            </div>
            <div class="configRow"  style="display: none">
            <span class="configCol">
                Events :
            </span>
                <span class="configCol">
                Show For : <input class="e4sNumber" type="number" id="delay" name="delay" value="<?php echo $delay ?>"
                                  min="3" max="60" onchange="updateDelay();return false;"> seconds
            </span>
            <span class="configCol">

            </span>

            <span class="configCol">

            </span>
            </div>

            <div class="configRow callroomOptions">
                <span class="configCol" style="display: none">
                    <span id="parentControllerDiv"></span>
                    <button onclick="requestControllerList()">Refresh Controllers</button>
                </span>
                <span class="configCol">
                </span>

                <span class="configCol callroomSyncOptions"  style="display: none">
                    Sync with Start List:
                    <input type="radio" id="callroomSyncDisabled" name="callRoomSyncEnabled" value="0" checked
                           onchange="updateCallRoomSettings()"> No
                    <input type="radio" id="callroomSyncEnabled" name="callRoomSyncEnabled" value="1"
                           onchange="updateCallRoomSettings()"> Yes
                </span>
                <span class="configCol"  style="display: none">
                    Hide Times : <input type="checkbox" id="callRoomStripTimes" name="callRoomStripTimes"
                                       onchange="updateCallRoomSettings()" value="Y"> Yes
                </span>
            </div>

            <div class="configRow" style="display: none">
                <span class="configCol">Actions : </span>
                <span class="configCol">
                        <button onclick="resetController()">Reset</button> &nbsp;
                        <button class="" onclick="location.reload();">Reload This Controller</button>
                    </span>
                <span class="configCol">Processing :  <button onclick="startEventCycle()">Start</button>&nbsp;
                        <button class="warnButton" onclick="stopEventCycle()">Stop</button></span>
                <span class="configCol">

                    </span>
            </div>

        </section>
        <div style="padding:20px 0px 0px 0px;">
            <span class="configCol" id="controllerMessage"></span>
        </div>
    </div>
    <hr>

    <div id="content" class="content">

            <div id="<?php echo E4S_TRACK_DISCIPLINE ?>ContentGroup" class="contentGroup" style="display: none;">
                <div class="eventsTitle">
	                <?php echo E4S_TRACK_DISCIPLINE ?> Events
                </div>
                <div id="<?php echo E4S_TRACK_DISCIPLINE ?>Content" class="<?php echo E4S_TRACK_DISCIPLINE ?>Content">

                </div>
            </div>


            <div id="<?php echo E4S_FIELD_DISCIPLINE ?>ContentGroup" class="contentGroup" style="display: none;">
                <div class="eventsTitle">
	                <?php echo E4S_FIELD_DISCIPLINE ?> Events
                </div>
                <div id="<?php echo E4S_FIELD_DISCIPLINE ?>Content" class="<?php echo E4S_FIELD_DISCIPLINE ?>Content">

                </div>

    </div>
    </body>
    </html>
<?php
function outputEvents( $events ) {
	foreach ( $events as $event => $eventObj ) {
		$meIds = [];
		for ( $i = 0; $i < $eventObj->heats; $i ++ ) {
			$meIds[ $i + 1 ] = $eventObj->startmeId + $i;
		}
		?>
        <div id="eventContent" class="eventContent">
            <span class="eventName"><?php echo $event ?></span>
            <table>
                <tr>
                    <td style="width:75px;">Heat</td>
                    <td class="<?php echo E4S_OUTPUT_START ?>Options" style="width:150px;">Start List</td>
                    <td class="<?php echo E4S_OUTPUT_RESULTS ?>Options" style="width:150px;">Results</td>
                    <td>&nbsp;</td>
                    <td class="<?php echo E4S_OUTPUT_CALLROOM ?>Options" callroom style="width:150px;text-align: center;">In Call Room</td>
                </tr>
				<?php if ( sizeof( $meIds ) > 1 ) { ?>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="<?php echo E4S_OUTPUT_START ?>Options">
                            <button class="warnButton"
                                    onclick="setAll([<?php echo implode( ',', $meIds ) ?>], 0, false); return true;">
                                Clear All
                            </button>
                            <button onclick="setAll([<?php echo implode( ',', $meIds ) ?>], 0, true); return true;">Set
                                All
                            </button>
                        </td>
                        <td class="<?php echo E4S_OUTPUT_RESULTS ?>Options">
                            <button class="warnButton"
                                    onclick="setAll([<?php echo implode( ',', $meIds ) ?>], 1, false); return true;">
                                Clear All
                            </button>
                            <button onclick="setAll([<?php echo implode( ',', $meIds ) ?>], 1, true); return true;">Set
                                All
                            </button>
                        </td>
                        <td>&nbsp;</td>
                        <td class="<?php echo E4S_OUTPUT_CALLROOM ?>Options"></td>
                    </tr>
					<?php
				}
				foreach ( $meIds as $heat => $meId ) {
					?>
                    <tr>
                        <td><span onclick="showThisOne(<?php echo $meId ?>);"><?php echo $heat ?><span
                                        e4scurrent="<?php echo $meId ?>"></span></span></td>
                        <td class="<?php echo E4S_OUTPUT_START ?>Options"><input e4sheat=<?php echo $heat ?> e4sevent="<?php echo $event ?>" startevent type="radio"
                                   id="<?php echo $meId ?>" name="<?php echo $meId ?>" value="0"></td>
                        <td class="<?php echo E4S_OUTPUT_RESULTS ?>Options"><input e4sheat=<?php echo $heat ?> e4sevent="<?php echo $event ?>" resultevent type="radio"
                                   id="<?php echo $meId ?>" name="<?php echo $meId ?>" value="1"></td>
                        <td>
                            <button class="warnButton" onclick="setMeId(<?php echo $meId ?>)">Clear</button>
                        </td>
                        <td class="<?php echo E4S_OUTPUT_CALLROOM ?>Options" style="text-align: center;"><input callroom callroomevent e4scallroomevent="<?php echo $event ?>"
                                                               heat="<?php echo $heat ?>" type="checkbox"
                                                               id="<?php echo $meId ?>_call" name="inCallRoom"
                                                               value="<?php echo $meId ?>"
                                                               onchange="inCallRoomSelected(<?php echo $meId ?>);"></td>
                    </tr>
					<?php
				}
				?>
            </table>
        </div>
		<?php
	}
}
