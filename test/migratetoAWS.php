<?php
//TESTING
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';

define('E4S_PAGE_NEWLINE', '<br>');

emailLog();

function emailLog() {
    $subjectTest = 'test AAI from E4S email';

    $body = 'Dear Paul,<br>This is a test email<br>';
    $useSendTo = 'paul@apiconsultancy.com';
    $body .= Entry4_emailFooter($useSendTo);

    e4s_mail($useSendTo, $subjectTest, $body);
    echo 'Email AAI E4S Sent';
    exit();
}

function migrate_text() {
    $tableName = 'text';
    $rows = getExistingRows($tableName, 'compid');
    $newRows = array();
    $sql = 'insert into ' . E4S_TABLE_TEXT . ' ( compid,e4skey,e4sText ) values ';
    foreach ($rows as $row) {
        $str = '(';
        $str .= ((int)$row['compid'] + E4S_MIGRATE_FACTOR) . ',';
        $str .= "'" . $row['e4skey'] . "',";
        $str .= "'" . $row['e4sText'] . "'";
        $str .= ')';
        $newRows[] = $str;
    }
    $sql .= implode(',', $newRows);
    migrate_sql($sql);
}

function migrate_Permissions() {
    $tableName = 'Permissions';
    $rows = getExistingRows($tableName);
    $newRows = array();
    $sql = 'insert into ' . $GLOBALS[$tableName] . ' ( userid, roleid, orgid,compid ) values ';
    foreach ($rows as $row) {
        $str = '(';
        $str .= "'" . $row['userid'] . "',";
        $str .= "'" . $row['roleid'] . "',";
        $str .= "'" . $row['orgid'] . "',";
        $str .= ((int)$row['compid'] + E4S_MIGRATE_FACTOR) . '';
        $str .= ')';
        $newRows[] = $str;
    }
    $sql .= implode(',', $newRows);
    migrate_sql($sql);
}

function migrate_PermissionLevel() {
    $tableName = 'PermissionLevel';
    $rows = getExistingRows($tableName);
    $newRows = array();
    $sql = 'insert into ' . $GLOBALS[$tableName] . ' ( userid, roleid, levelid ) values ';
    foreach ($rows as $row) {
        $str = '(';
        $str .= "'" . $row['userid'] . "',";
        $str .= "'" . $row['roleid'] . "',";
        $str .= "'" . $row['levelid'] . "'";
        $str .= ')';
        $newRows[] = $str;
    }
    $sql .= implode(',', $newRows);
    migrate_sql($sql);
}

function migrate_MultiEvent() {
    $tableName = 'MultiEvent';
    $rows = getExistingRows($tableName);
    $newRows = array();
    $sql = 'insert into ' . $GLOBALS[$tableName] . ' (id,multiid,eventid,starttime,compid,startdate) values ';
    foreach ($rows as $row) {
        $str = '(';
        $str .= ((int)$row['id'] + E4S_MIGRATE_MULTI_FACTOR) . ',';
        $str .= ((int)$row['multiid'] + E4S_MIGRATE_MULTI_FACTOR) . ',';
        $str .= "'" . $row['eventid'] . "',";
        $str .= "'" . $row['starttime'] . "',";
        $str .= ((int)$row['compid'] + E4S_MIGRATE_FACTOR) . ',';
        $str .= "'" . $row['startdate'] . "'";
        $str .= ')';
        $newRows[] = $str;
    }
    $sql .= implode(',', $newRows);
    migrate_sql($sql);
}

function migrate_EventTeamFormDef() {
    $tableName = 'EventTeamFormDef';
    $rows = getExistingRows($tableName);
    $newRows = array();
    $sql = 'insert into ' . $GLOBALS[$tableName] . ' (id,formno,pos,label,required,options) values ';
    foreach ($rows as $row) {
        $str = '(';
        $str .= ((int)$row['id'] + E4S_MIGRATE_TEAMFORM_FACTOR) . ',';
        $str .= $row['formno'] . ',';
        $str .= "'" . $row['pos'] . "',";
        $str .= "'" . $row['label'] . "',";
        $str .= "'" . $row['required'] . "',";
        $str .= "'" . $row['options'] . "'";
        $str .= ')';
        $newRows[] = $str;
    }
    $sql .= implode(',', $newRows);
    migrate_sql($sql);
}

function migrate_CompEvents() {
    $tableName = 'CompEvents';
    $rows = getExistingRows($tableName);
    $newRows = array();
    $sql = 'insert into ' . $GLOBALS[$tableName] . ' (ID,CompID,EventID,IsOpen,AgeGroupID,PriceID,maxathletes,maxGroup,multiid,split,sortdate,eventNameExtra,startdate,options) values ';
    foreach ($rows as $row) {
        $str = '(';
        $str .= ((int)$row['ID'] + E4S_MIGRATE_CE_FACTOR) . ',';
        $str .= ((int)$row['CompID'] + E4S_MIGRATE_FACTOR) . ',';
        $str .= "'" . $row['EventID'] . "',";
        $str .= "'" . $row['IsOpen'] . "',";
        $str .= "'" . $row['AgeGroupID'] . "',";
        $str .= ((int)$row['PriceID'] + E4S_MIGRATE_PRICE_FACTOR) . ',';
        $str .= "'" . $row['maxathletes'] . "',";
        if (is_numeric($row['maxGroup'])) {
            $str .= ((int)$row['maxGroup'] + E4S_MIGRATE_EG_FACTOR) . ',';
        } else {
            $str .= "'" . $row['maxGroup'] . "',";
        }
        $str .= ((int)$row['multiid'] + E4S_MIGRATE_MULTI_FACTOR) . ',';
        $str .= "'" . $row['split'] . "',";
        $str .= "'" . $row['sortdate'] . "',";
        $str .= "'" . $row['eventNameExtra'] . "',";
        $str .= "'" . $row['startdate'] . "',";
        $str .= "'" . $row['options'] . "'";
        $str .= ')';
        $newRows[] = $str;
    }
    $sql .= implode(',', $newRows);
    migrate_sql($sql);
}

function migrate_EventTeamEntries() {
    $tableName = 'EventTeamEntries';
    $rows = getExistingRows($tableName);
    $newRows = array();
    $sql = 'insert into ' . $GLOBALS[$tableName] . ' (id,name,ceid,productid,orderid,paid,price,entitylevel,entityid,created,userid,options) values ';
    foreach ($rows as $row) {
        $str = '(';
        $str .= ((int)$row['id'] + E4S_MIGRATE_TEAMENTRY_FACTOR) . ',';
        $str .= "'" . $row['name'] . "',";
        $str .= ((int)$row['ceid'] + E4S_MIGRATE_CE_FACTOR) . ',';
        $str .= "'" . $row['productid'] . "',";
        $str .= "'" . $row['orderid'] . "',";
        $str .= "'" . $row['paid'] . "',";
        $str .= "'" . $row['price'] . "',";
        $str .= "'" . $row['entitylevel'] . "',";
        $str .= "'" . $row['entityid'] . "',";
        $str .= "'" . $row['created'] . "',";
        $str .= "'" . $row['userid'] . "',";
        $str .= "'" . $row['options'] . "'";
        $str .= ')';
        $newRows[] = $str;
    }
    $sql .= implode(',', $newRows);
    migrate_sql($sql);
}

function migrate_EventTeamAthlete() {
    $tableName = 'EventTeamAthlete';
    $rows = getExistingRows($tableName);
    $newRows = array();
    $sql = 'insert into ' . $GLOBALS[$tableName] . ' (teamentryid,pos,athleteid,options) values ';
    foreach ($rows as $row) {
        $str = '(';
        $str .= ((int)$row['teamentryid'] + E4S_MIGRATE_TEAMENTRY_FACTOR) . ',';
        $str .= "'" . $row['pos'] . "',";
        $str .= "'" . $row['athleteid'] . "',";
        $str .= "'" . $row['options'] . "'";
        $str .= ')';
        $newRows[] = $str;
    }
    $sql .= implode(',', $newRows);
    migrate_sql($sql);
}

function migrate_EventPrice() {
    $tableName = 'EventPrice';
    $rows = getExistingRows($tableName);
    $newRows = array();
    $sql = 'insert into ' . $GLOBALS[$tableName] . ' (id,compid,name,description,price,saleprice,saleenddate,salefee,fee,options) values ';
    foreach ($rows as $row) {
        $str = '(';
        $str .= ((int)$row['ID'] + E4S_MIGRATE_PRICE_FACTOR) . ',';
        $str .= ((int)$row['compid'] + E4S_MIGRATE_FACTOR) . ',';
        $str .= "'" . $row['name'] . "',";
        $str .= "'" . $row['description'] . "',";
        $str .= "'" . $row['price'] . "',";
        $str .= "'" . $row['saleprice'] . "',";
        $str .= "'" . $row['saleenddate'] . "',";
        $str .= "'" . $row['salefee'] . "',";
        $str .= "'" . $row['fee'] . "',";
        $str .= "'" . $row['options'] . "'";
        $str .= ')';
        $newRows[] = $str;
    }
    $sql .= implode(',', $newRows);
    migrate_sql($sql);
}

function migrate_EventGroups() {
    $tableName = 'EventGroups';
    $rows = getExistingRows($tableName);
    $newRows = array();
    $sql = 'insert into ' . $GLOBALS[$tableName] . ' (id,compId,eventNo,name,startdate,options) values ';
    foreach ($rows as $row) {
        $str = '(';
        $str .= ((int)$row['id'] + E4S_MIGRATE_EG_FACTOR) . ',';
        $str .= ((int)$row['compId'] + E4S_MIGRATE_FACTOR) . ',';
        $str .= "'" . $row['eventNo'] . "',";
        $str .= "'" . $row['name'] . "',";
        $str .= "'" . $row['startdate'] . "',";
        $str .= "'" . $row['options'] . "'";
        $str .= ')';
        $newRows[] = $str;
    }
    $sql .= implode(',', $newRows);
    migrate_sql($sql);
}

function migrate_CompStatusAudit() {
    $tableName = 'CompStatusAudit';
    $rows = getExistingRows($tableName);
    $newRows = array();
    $sql = 'insert into ' . $GLOBALS[$tableName] . ' (compid,statusid,invoicelink,value,paid,userid,reference,updated ) values ';
    foreach ($rows as $row) {
        $str = '(';
        $str .= ((int)$row['compid'] + E4S_MIGRATE_FACTOR) . ',';
        $str .= $row['statusid'] . ',';
        $str .= "'" . $row['invoicelink'] . "',";
        $str .= "'" . $row['value'] . "',";
        $str .= "'" . $row['paid'] . "',";
        $str .= "'" . $row['userid'] . "',";
        $str .= "'" . $row['reference'] . "',";
        $str .= "'" . $row['updated'] . "'";
        $str .= ')';
        $newRows[] = $str;
    }
    $sql .= implode(',', $newRows);
    migrate_sql($sql);
}

function migrate_CompStatus() {
    $tableName = 'CompStatus';
    $rows = getExistingRows($tableName);
    $newRows = array();
    $sql = 'insert into ' . $GLOBALS[$tableName] . ' (compid,statusid,invoicelink,value,paid,userid,reference,updated ) values ';
    foreach ($rows as $row) {
        $str = '(';
        $str .= ((int)$row['compid'] + E4S_MIGRATE_FACTOR) . ',';
        $str .= $row['statusid'] . ',';
        $str .= "'" . $row['invoicelink'] . "',";
        $str .= "'" . $row['value'] . "',";
        $str .= "'" . $row['paid'] . "',";
        $str .= "'" . $row['userid'] . "',";
        $str .= "'" . $row['reference'] . "',";
        $str .= "'" . $row['updated'] . "'";
        $str .= ')';
        $newRows[] = $str;
    }
    $sql .= implode(',', $newRows);
    migrate_sql($sql);
}

function migrate_CompRules() {
    $rows = getExistingRows('CompRules');
    $newRows = array();
    $sql = 'insert into ' . E4S_TABLE_COMPRULES . ' (compID, ageGroupID, options ) values ';
    foreach ($rows as $row) {
        $str = '(';
        $str .= ((int)$row['compID'] + E4S_MIGRATE_FACTOR) . ',';
        $str .= $row['ageGroupID'] . ',';
        $str .= "'" . $row['options'] . "'";
        $str .= ')';
        $newRows[] = $str;
    }
    $sql .= implode(',', $newRows);
    migrate_sql($sql);
}

function migrate_Competitions() {
    $rows = getExistingRows('Competition');
    $newRows = array();
    $sql = 'insert into ' . E4S_TABLE_COMPETITON . ' values ';
    foreach ($rows as $row) {
        $str = '(';
        $str .= ((int)$row['ID'] + E4S_MIGRATE_FACTOR) . ',';
        $str .= $row['locationid'] . ',';
        $str .= $row['compclubid'] . ',';
        $str .= "'" . $row['Date'] . "',";
        $str .= "'" . $row['IndoorOutdoor'] . "',";
        $str .= "'" . $row['Name'] . "',";
        $str .= "'" . $row['EntriesOpen'] . "',";
        $str .= "'" . $row['EntriesClose'] . "',";
        $str .= "'" . $row['link'] . "',";
        $str .= '' . $row['yearFactor'] . ',';
        $str .= '' . $row['areaid'] . ',';
        $str .= "'" . $row['options'] . "',";
        $str .= '' . $row['teamid'] . ',';
        $str .= "'" . $row['lastentrymod'] . "',";
        $str .= "'" . $row['teamid'] . "'";
        $str .= ')';
        $newRows[] = $str;
    }
    $sql .= implode(',', $newRows);
    migrate_sql($sql);
}

function migrate_CompDiscounts() {
    $rows = getExistingRows('CompDiscounts');
    $newRows = array();
    $sql = 'Insert into ' . E4S_TABLE_COMPDISCOUNTS . '(compid,orgid,agegroupid,sale_disc,reg_disc,type,count,options) values ';
    foreach ($rows as $row) {
        $str = '(';
        $str .= ((int)$row['compid'] + E4S_MIGRATE_FACTOR) . ',';
        $str .= '' . $row['orgid'] . ',';
        $str .= '' . $row['agegroupid'] . ',';
        $str .= "'" . $row['sale_disc'] . "',";
        $str .= "'" . $row['reg_disc'] . "',";
        $str .= "'" . $row['type'] . "',";
        $str .= '' . $row['count'] . ',';
        $str .= "'" . $row['options'] . "'";
        $str .= ')';
        $newRows[] = $str;
    }
    $sql .= implode(',', $newRows);
    migrate_sql($sql);
}

function getExistingRows($table, $colName = 'id') {
    echo E4S_PAGE_NEWLINE . 'Retrieving ' . $table . E4S_PAGE_NEWLINE;
    $sql = "SELECT min({$colName}) minID, max({$colName}) maxID , count({$colName}) countID FROM Entry4_ire_" . $table;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        echo 'min : ' . $row['minID'] . '   ->    max : ' . $row['maxID'] . '   ->    total rows : ' . $row['countID'] . E4S_PAGE_NEWLINE;
        $sql = '
        Select * 
        from Entry4_ire_' . $table;

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows > 0) {
            $rows = $result->fetch_all(MYSQLI_ASSOC);
            return $rows;
        }
    }
    return array();
}

function migrate_sql($sql) {
    $test = FALSE;

    if ($test) {
        echo $sql . E4S_PAGE_NEWLINE;
        $return = new stdClass();
        $return->num_rows = 0;
        return $return;
    } else {
        return e4s_queryNoLog($sql);
    }

}