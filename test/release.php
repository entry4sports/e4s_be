<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';

// Sort in descending order
$allFiles = scandir(ABSPATH . '/app/', 1);
$files = array();
foreach ($allFiles as $checkFile) {
    if (strpos($checkFile, 'app') > -1) {
        if (strpos($checkFile, '.js') > -1) {
            if (strpos($checkFile, '.map') === FALSE) {
                $code = explode('app', $checkFile)[1];
                $code = explode('.js', $code)[0];
                $files[filemtime(ABSPATH . '/app/' . $checkFile)] = $code;
            }
        }
    }
}
krsort($files);
foreach ($files as $time => $e4sFile) {
    echo "$e4sFile was modified: " . date('F d Y H:i:s.', $time) . '<br>';
}
exit();
$devcontent = '
<link rel="stylesheet" href="/app/app{content}.css">
<div id="app"><router-view name="header"></router-view><router-view></router-view></div>
<script>var E4S_GLOBAL_THEME_IDENTIFIER = "irl";var xxx=1;</script>
<script type="text/javascript" src="/app/vendor{content}.js"></script>
<script type="text/javascript" src="/app/app{content}.js"></script>
';
$livecontent = '
<p style="display: none;">
<script src="https://browser.sentry-cdn.com/5.12.1/bundle.min.js" integrity="sha384-y+an4eARFKvjzOivf/Z7JtMJhaN6b+lLQ5oFbBbUwZNNVir39cYtkjW1r6Xjbxg3" crossorigin="anonymous"></script> 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 
<link href="/app/app{content}.css" rel="stylesheet"></p>
<script>var E4S_GLOBAL_THEME_IDENTIFIER = "gbr";</script>
<div id="app"><router-view name="header"></router-view><router-view></router-view></div>
<script src="/app/vendor{content}.js" type="text/javascript"></script>
<script src="/app/app{content}.js" type="text/javascript"></script>
<script>Sentry.init({ dsn: \'https://9c74b78f1c544d9389d3474ffe7c3106@sentry.io/1373748\' });</script>
';

$check_page_exist = get_page_by_title('title_of_the_page', 'OBJECT', 'page');
// Check if the page already exists
if (empty($check_page_exist)) {
    $page_id = wp_insert_post(array('comment_status' => 'close', 'ping_status' => 'close', 'post_author' => 1, 'post_title' => ucwords('title_of_the_page'), 'post_name' => strtolower(str_replace(' ', '-', trim('title_of_the_page'))), 'post_status' => 'publish', 'post_content' => 'Content of the page', 'post_type' => 'page', 'post_parent' => 'id_of_the_parent_page_if_it_available'));
    echo $page_id;
}