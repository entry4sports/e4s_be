<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
#include_once E4S_FULL_PATH . 'reports/stripeReport.php';

$public = TRUE;
$private = checkFieldForXSS($obj, 'private:Private Key');
$action = checkFieldForXSS($obj, 'action:Action');
define('E4S_TEST_SOCKET_SERVER', 'wss://m9f8tsofz8.execute-api.eu-west-2.amazonaws.com/Prod');
if (!isE4SUser() and ($action !== 'ealive' and $action !== 'ealiveurn' and $action !== 'esaaentries' and $action !== 'ealist' and $action !== 'settimetable')) {
	Entry4UIError(9070, 'You are not authorised to use this function/' .$action);
}
//$a = 58.015;
//$b = 11.5;
//$c = 1.81;
//$time = 6.5;
//
//$p = $a * ($b - $time) ** $c;

switch (strtolower($action)) {
	case 'settimetable':
		$compId = checkFieldForXSS($obj, 'compid:Competition Id');
		$sql = 'SET @prev_time = curtime();';
		e4s_queryNoLog($sql);
		$sql = '
			UPDATE Entry4_uk_timetable
			SET calltime = (@prev_time := ADDTIME(@prev_time, "00:01:00")),
			    eventTime = addtime(calltime, "00:30:00")
			WHERE id;';
		e4s_queryNoLog($sql);
		echo '<button onclick="location.href=\'/entry/v5/scoreboards/timetable.php?compid=' . $compId . '&date=2024-11-24\'";>Back to Timetable</button>';
		exit;
		break;
	case "restore":
		$dbname = checkFieldForXSS($obj, 'dbname:Database Name');
		if ( is_null($dbname) ){
			$dbname = '';
		}
		$archiveObj = new e4sArchive(0, $dbname);
		$archiveObj->restore();
		break;
	case "clearrestore":
		$dbname = checkFieldForXSS($obj, 'dbname:Database Name');
		if ( is_null($dbname) ){
			$dbname = '';
		}
		$archiveObj = new e4sArchive(0, $dbname);
		$archiveObj->clearRestore();
		break;
	case "newbibno":
		$bibObj = new bibClass(632);
		$bibObj->allocateBibForNewAthlete(123456);
		break;
	case "qrcode":
		generateQrCode();
		break;
	case "validateoptions":
		validateOptions();
		break;
	case 'esaausers':
		updateComp617();
		break;
	case 'roster':
		importRosterData();
		break;
	case 'roster_cookie':
	case 'roster_startlist':
	case 'roster_result':
		getRosterInfo(strtolower($action));
		break;
	case 'addstd':
		e4s_addStandardsToResults($obj);
		break;
	case 'standards':
		e4s_loadStandards();
		break;
	case 'waitingcheck':
		e4s_waitingCheck();
		break;
	case 'esaaentries':
		esaa23Entries();
		break;
	case 'beforecomp':
		$compid = checkFieldForXSS($obj, 'compid:Competition Id' . E4S_CHECKTYPE_NUMERIC);
		$compObj = e4s_getCompObj($compId);
		var_dump($compObj->isBeforeCompDate());
		break;
	case 'uid':
		$uid = '2a00:23c6:b110:a901:e8d3:8ffb:fabd:3aef';
		var_dump($uid);
		$uid = str_replace(':','', $uid);
		var_dump(substr($uid,0, 10));
		var_dump(substr($uid,-10));
		$uid = substr($uid,0, 10) . substr($uid,-10);
		var_dump($uid);
		exit;
	case 'aaiathlete':
		$athleteObj = new athleteClass(true);
		$athleteObj->_getAthleteFromAAI('IRL', 141444);
		exit;
	case 'aniathlete':
		$athleteObj = new athleteClass(true);
		$athleteObj->_getAthleteFromAAI('ANI', 105931);
		exit;
	case 'date':
		echo date('Y-m-d_h:i:s', time());
		exit;
		break;
	case 'years':
//        $date = date_create("2021-01-01");
//        $x = date("Y");
//        $y = date_format($date,"Y");
		$date = strtotime('2023-02-20');
		var_dump($date);
		$date2 = strtotime('-1 year', $date);
		var_dump($date2);
		var_dump(date('d-m-Y',$date2));
		exit;
		break;
	case 'importschools':
		include_once E4S_FULL_PATH . 'import/importSchools.php';
		e4s_importSchools();
		break;
	case 'standards':
		$compId = checkFieldForXSS($obj, 'compid:Competition Id' . E4S_CHECKTYPE_NUMERIC);
		$compObj = e4s_getCompObj($compId);
		$standards = $compObj->getStandards(E4S_STANDARD_ORDER_COMBINED);
		var_dump($standards);
		exit();
	case 'latestdates':
		$compObj = e4s_getCompObj(310);
		$dates = $compObj->getAllEventDates();
		var_dump($dates);
		var_dump($compObj->lastEventDate());
		break;
	case 'expired':
		$entryObj = new entryClass();
		$entryObj->checkExpiredEntries();
		exit();
		break;
	case 'award';
		e4s_pbAward();
		break;
	case 'agegroups':
		e4s_testAgeGroups();
		break;
	case 'faf':
		e4s_fireAndForget('https://dev.entry4sports.co.uk/wp-json/e4s/v5/test/?action=pof10V2&urn=3590678');
		break;
	case 'athleteperf':
		$athleteid = checkFieldForXSS($obj, 'athleteid:Athlete Id' . E4S_CHECKTYPE_NUMERIC);
		$perfObj = new perfAthlete();
		$pbs = $perfObj->getPerformances([$athleteid]);
		Entry4UISuccess();
		break;
	case 'pof10v2':
		$urn = checkFieldForXSS($obj, 'urn:URN');
		$force = checkFieldForXSS($obj, 'force:Force update');
		if ( !is_null($force) ) {
			if ( $force === true or $force === 1 or $force === 'true' or $force === '1' ){
				$force = true;
			} else {
				$force = false;
			}
		}else{
			$force = false;
		}
		$obj = new pof10V2Class($urn);
		$obj->updateDb($force);
		Entry4UISuccess();
		break;
	case 'pof10':
		$obj = pof10Class::getWithURN('EA',2658202);
		break;
	case 'pof102':
		$athleteObj = athleteClass::withID(1);
		if (!is_null($athleteObj)) {
			$athleteObj->updatePBsFromPof10Info();
		}
		break;
	case 'theme':
		echo 'Theme : ' .  get_template_directory();
		break;
	case 'phpinfo':
		$flags = checkFieldForXSS($obj, 'flags:Flags');
		if ( is_null($flags) ){
			/*
 INFO_GENERAL	1	The configuration line, php.ini location, build date, Web Server, System and more.
INFO_CREDITS	2	PHP Credits. See also phpcredits().
INFO_CONFIGURATION	4	Current Local and Master values for PHP directives. See also ini_get().
INFO_MODULES	8	Loaded modules and their respective settings. See also get_loaded_extensions().
INFO_ENVIRONMENT	16	Environment Variable information that's also available in $_ENV.
INFO_VARIABLES	32	Shows all predefined variables from EGPCS (Environment, GET, POST, Cookie, Server).
INFO_LICENSE	64	PHP License information. See also the » license FAQ.
INFO_ALL	-1	Shows all of the above.
			 */
			$flags = INFO_GENERAL;
		}
		phpinfo($flags);
		break;
	case 'bibnos':
		e4s_getUnusedBibNos($obj);
		break;
	case 'sockets':
		$compId = checkFieldForXSS($obj, 'compid:Competition Id' . E4S_CHECKTYPE_NUMERIC);
		$compObj = e4s_getCompObj($compId);
		if ($compObj->socketsEnabled()) {
			echo 'true';
		} else {
			echo 'false';
		}
		break;
	case 'waiting':
		e4s_waitingListCheckerUpdate();
		break;

	case 'moveoff':
		moveOffWaitingList($obj);
		break;
	case 'getcomp':
		$compId = checkFieldForXSS($obj, 'compid:Competition Id');
		$compObj = e4s_getCompObj($compId);
		$compObj->canWaitingListRefundsBeProcessed();
		break;
	case 'xdebug':
		header('Content-Type: text/html; charset=utf-8');
		xdebug_info();
		break;
	case 'emailsize':
		emailSize($obj);
		break;
	case 'adjust':
		testAdjust(305);
		break;
	case 'hasanyperm':
		$retval = e4s_hasAnyPermForComp(305);
		var_dump($retval);
		break;
	case 'opentrack':
		load_ani_clubs();
		break;
	case 'time':
		$date = '2022-04-02 09:00:00';
		$used = date_create($date);
		echo $date . "\n";
		echo date('d/m/Y G:i:s', strtotime('-10 minutes', $used->getTimestamp()));
		break;
	case 'header':
		logTxt(addslashes(e4s_getDataAsType(getallheaders(), E4S_OPTIONS_STRING)));
		break;
	case 'modulo':
		var_dump(12 % 5);
		var_dump(12 / 5);
		var_dump((int)(13 / 5));
		var_dump((int)(12.1));
		var_dump((int)(12.9));
		break;
	case 'security':
		securityTest();
		break;
	case 'switch':
//        include_once E4S_FULL_PATH . 'classes/e4sUserSwitchClass.php';
//        $GLOBALS['user_switching']->action_plugins_loaded();
//        https://dev.entry4sports.co.uk/wp-admin/?impersonate=19828
//        switch_to_user(19828);
		header('Location: https://dev.entry4sports.co.uk/wp-admin/?impersonate=19828');
		exit();
		break;
	case 'fixactive':
		e4s_fixActive();
		break;
	case 'md5':
		echo md5(E4S_ANI_REGISTRATION);
		break;
	case 'header':
		e4s_checkHeaderFor('Ani-Key', E4S_ANI_REGISTRATION)();
		break;
	case 'eatesturn':
		ea_testURN($obj);
		break;
	case 'eatest':
		ea_test($obj);
		break;
	case 'ealist':
		e4s_eaList();
		break;
	case 'ealiveurn':
		ea_liveURN($obj);
		break;
	case 'ealive':
		ea_live($obj);
		break;
	case 'anilive':
		ani_live($obj);
		break;
	case 'aailive':
		aai_live($obj);
		break;
	case 'eaclubs':
		ea_clubs();
		break;
	case 'eaathlete':
		ea_athlete($obj);
		break;
	case 'newseed':
		$v2Obj = new seedingV2Class(197, 2801);
		$v2Obj->seedEventGroup();
		break;
	case 'testobject':
		echo 'testing';
		e4s_testObject();
		break;
	case 'checkagegroups':
		$compId = getNotNull($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
		$update = getNotNull($obj, 'update:Update And Email');
		$results = checkCompEntryAgeGroups($compId, $update);
		Entry4UISuccess($results);
		break;
	case 'finance3':
		$fromDate = getNotNull($obj, 'from:From Date');
		finance_report_3($fromDate);
		break;
	case 'finance2021':
		finance2021();
		break;
	case 'correctprodcat':
		// Check and correct a products category for its competition
		$compId = getNotNull($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
		e4s_correctProductCategory($compId);
		break;
	case 'checkprodcat':
		// Check and correct a products category for its competition
		$fromCompId = getNotNull($obj, 'fromid:From Competition ID');
		$toCompId = getNotNull($obj, 'toid:To Competition ID');
		for ($id = (int)$fromCompId; $id <= (int)$toCompId; $id++) {
			e4s_correctProductCategory($id);
		}
		break;
	case 'emailtest':
		$sendTo = array();
		$sendTo[] = 'paul_day@apiconsultancy.com';
		$sendTo[] = 'paul@entry4sports.com';
//        $sendTo = "paul_day@apiconsultancy.com; paul@entry4sports.com";
		e4s_mail($sendTo, 'Test Email 3', 'Test Body <b>Testing</b>');
		break;
	case 'websocketv1':
		e4s_websocket1();
		break;
	case 'websocketv2':
		e4s_websocket2();
		break;
	case 'trace':
//        var_dump(debug_backtrace(\DEBUG_BACKTRACE_IGNORE_ARGS));
		$checkStr = ['abc', 'def', 'egh'];
		if (gettype($checkStr) === 'array') {
			$checkStr = implode(',', $checkStr);
		}
		var_dump($checkStr);
		break;
	case 'datetest':
		echo date('d');
		break;
	case 'register':
		e4sRegisterUser($obj);
		break;
	case 'stripe':
		$obj = new e4sStripeConnect(1);
		$obj->testEmail();
		break;
	case 'config':
		$config = e4s_getConfigObj();
		$approvers = $config->getStripeApprovers();
		break;
	default:
		Entry4UIError(1000, $action . ' Not allowed');
}
Entry4UISuccess();

function validateOptions(){
	$compId = 575;
	echo "Event Results\n";
	$sql = "select rh.id, rh.options
	        from " . E4S_TABLE_EVENTRESULTSHEADER . " rh,
	             " . E4S_TABLE_EVENTGROUPS . " eg
	     where eg.compid = " . $compId . "
	     and rh.egid = eg.id";
	$result = e4s_queryNoLog($sql);
	while ( $obj = $result->fetch_object()) {
		echo $obj->id . "\n";
		$options = json_decode( $obj->options );
	}
	echo "Entries\n";
	$sql = "select entryId, egOptions, ceOptions,uomOptions,eOptions from " . E4S_TABLE_ENTRYINFO . " where compid = " . $compId;
	$result = e4s_queryNoLog($sql);
	while ( $obj = $result->fetch_object()){
		echo $obj->entryId . "\n";
		$options = json_decode($obj->egOptions);
		echo "eg good\n";
		$options = json_decode($obj->eOptions);
		echo "e good\n";
		$options = json_decode($obj->ceOptions);
		echo "ce good\n";
		$options = json_decode($obj->uomOptions);
		echo "ce good\n";
	}
	$sql = "select id, post_content
	        from " . E4S_TABLE_POSTS . "
	        where post_content like '%compid\":575%'";
	$result = e4s_queryNoLog($sql);
	while ( $obj = $result->fetch_object()) {
		echo $obj->id . "\n";
		$options = json_decode( $obj->post_content );
	}
	echo "Card Results\n";
	$sql = "select id, options
	        from " . E4S_TABLE_CARDRESULTS . "
	     where compid = " . $compId;
	$result = e4s_queryNoLog($sql);
	while ( $obj = $result->fetch_object()) {
		echo $obj->id . "\n";
		$options = json_decode( $obj->options );
	}

}
function e4s_loadStandards(){
	$sql = 'select *
            from esaa2023stds
            order by eventid, agegroup';
	$result = e4s_queryNoLog($sql);

	while ($stdObj = $result->fetch_object()){
		$useScore = $stdObj->ns;
		if ( !is_null($useScore)) {
			$score = resultsClass::getResultInSeconds($useScore);
			e4s_checkAndLoadStandard(2, $stdObj->eventid, $stdObj->ageGroup, $score);
		}
		$useScore = $stdObj->es;
		if ( !is_null($useScore)) {
			$score = resultsClass::getResultInSeconds($useScore);
			e4s_checkAndLoadStandard(1, $stdObj->eventid, $stdObj->ageGroup, $score);
		}
	}
}

function updateComp617(){
	$sql = ' select user_id, meta_value
					from ' . E4S_TABLE_USERMETA . '
			where meta_key = "e4s_clubcomp_482"';
	$userResults = e4s_queryNoLog($sql);
	var_dump("Rows : " . $userResults->num_rows);
	while ($obj = $userResults->fetch_object()){
		$userId = (int)$obj->user_id;
		$id = (int)$obj->meta_value;

		$sql = 'select bibNos
	        from ' . E4S_TABLE_CLUBCOMP .'
			where id = ' . $id ;
		$result = e4s_queryNoLog($sql);
		if ( $result->num_rows === 0 ){
			exit('ClubComp id not found : ' . $id);
		}
		$bibNos = $result->fetch_object()->bibNos;
		var_dump("User : " . $userId . " ClubComp : " . $id . " BibNos : " . $bibNos   );
		$sql = 'select id
				from ' . E4S_TABLE_CLUBCOMP . '
				where compId = 617
				 and bibnos = "' . $bibNos . '"';
		$result2 = e4s_queryNoLog($sql);
		if ( $result2->num_rows === 0 ){
			exit('Club not found in 617 for bibs : ' . $bibNos);
		}
		$sql = "update " . E4S_TABLE_USERMETA . "
				set meta_value = " . $result2->fetch_object()->id . "
				where user_id = " . $userId . "
				and meta_key = 'e4s_clubcomp_617'";
		e4s_queryNoLog($sql);
	}
}
function generateQrCode(){
	include_once E4S_FULL_PATH . "/qr/qrlib.php";
	$filename =  $_SERVER['DOCUMENT_ROOT'] . '/results/qr.png';
	// test if filename exists
	if (file_exists($filename)){
		exit("File Exists");
	}
	QRcode::png('https://entry4sports.co.uk',$filename);
	exit;
}
function e4s_checkAndLoadStandard($stdId, $eventId, $ageGroupId, $score){
	$sql= 'select id
          from ' . E4S_TABLE_STANDARDVALUES . "
          where standardid = {$stdId}
          and eventid = {$eventId}
          and agegroupid = {$ageGroupId}";
	$result = e4s_queryNoLog($sql);
	if ( $result->num_rows < 1 ){
		$sql = 'insert into ' . E4S_TABLE_STANDARDVALUES . " (standardid, eventid, agegroupid, value)
                values (
                {$stdId},
                {$eventId},
                {$ageGroupId},
                {$score}
                )";
		e4s_queryNoLog($sql);
	}
}
function e4s_waitingCheck(){
	$compId = 458;

	$compObj = e4s_GetCompObj($compId);
	$waitingObj = new waitingClass($compId);

	if ($compObj->isWaitingListEnabled()) {
		if ($compObj->areEntriesOpen()) {
			$waitingObj->processMovements();
		}
		if ($compObj->canWaitingListRefundsBeProcessed()) {
			$compObj->performWaitingListRefunds();
		}
	} else {
		// mark done to stop being processed again
		$waitingObj->markWaitingListDone($compObj);
	}
}
function esaa23Entries(){
	$sql = 'select firstName, surName, egId, egName, athleteId, eventNo, clubName
             from ' . E4S_TABLE_ENTRYINFO . '
             where compid = 482
             and paid = 1';
	$result = e4s_queryNoLog($sql);
	$entries = array();

	while ($row = $result->fetch_object()) {
		$row->heatNo = 0;
		$row->laneNo = 0;
		$entries[$row->egId . '-' . $row->athleteId] = $row;
	}
	$sql = 'select eg.eventno eventNo, eg.id egId, eg.name egName, c.clubName clubName
             from ' . E4S_TABLE_EVENTTEAMENTRIES . ' ete,
                  ' . E4S_TABLE_EVENTGROUPS . ' eg,
                  ' . E4S_TABLE_COMPEVENTS . ' ce,
                  ' . E4S_TABLE_CLUBS . ' c
             where eg.compid = 482
               and ce.maxgroup = eg.id
               and ete.ceid = ce.id
             and ete.paid = 1
             and c.id = ete.entityid';

	$result = e4s_queryNoLog($sql);
	while ($row = $result->fetch_object()) {
		if ( strpos($row->egName, 'reserve') === false) {
			$row->heatNo = 1;
			$row->laneNo = 1;
			$entries[$row->egId . '-' . $row->clubName] = $row;
		}
	}

	$sql = 'select s.*
            from ' . E4S_TABLE_SEEDING . ' s,
                 ' . E4S_TABLE_EVENTGROUPS . ' eg
            where s.eventgroupid = eg.id
            and eg.compid = 482';
	$result = e4s_queryNoLog($sql);
	while ($row = $result->fetch_object()) {
		$key = $row->eventgroupid  . '-' . $row->athleteid;
		if ( !array_key_exists($key, $entries) ) {
//            echo ($key . "\n");
			continue;
		}
		$entries[$key]->heatNo = $row->heatno;
		$entries[$key]->laneNo = $row->laneno;
	}

	usort($entries,function($a, $b){
		if ($a->eventNo < $b->eventNo ){
			return -1;
		}
		if ($a->eventNo > $b->eventNo ){
			return 1;
		}
		if ($a->heatNo < $b->heatNo ){
			return -1;
		}
		if ($a->heatNo > $b->heatNo ){
			return 1;
		}
		if ($a->laneNo < $b->laneNo ){
			return -1;
		}
		if ($a->laneNo > $b->laneNo ){
			return 1;
		}
		return 0;
	});

	$sep = ',';
	$newLine = '<br>';
	$lastEventNo = 0;
	$lastHeatNo = 0;
	foreach($entries as $entry){
		if ( $entry->eventNo !== $lastEventNo ){
			$lastHeatNo = 0;
			$egName = $entry->egName;
			$egName = str_replace(' Jump', ' QQQ', $egName);
			if ( strpos($egName, 'Junior') === false) {
				$egName = str_replace(' J', ' Junior ', $egName);
			}
			$egName = str_replace(' QQQ', ' Jump', $egName);
			if ( strpos($egName, 'Inter') === false) {
				$egName = str_replace(' I', ' Inter ', $egName);
			}
			if ( strpos($egName, 'Senior') === false) {
				$egName = str_replace(' S', ' Senior ', $egName);
			}
			if ( strpos($egName, 'Girl') === false) {
				$egName = str_replace(' G', ' Girls', $egName);
			}
			if ( strpos($egName, 'Boy') === false) {
				$egName = str_replace(' B', ' Boys', $egName);
			}
			echo $newLine . 'Event : ' . $egName . $newLine;
		}
		if ( $entry->heatNo !== $lastHeatNo ){
			if ( (int)$entry->heatNo > 1){
				echo $newLine;
			}
			echo 'Heat : ' . $entry->heatNo . $newLine;
		}
		$lastEventNo = $entry->eventNo;
		$lastHeatNo = $entry->heatNo;
		if ( isset($entry->firstName) ) {
			echo $entry->firstName . ' ' . $entry->surName . $sep;
		}
		echo $entry->clubName . $newLine;
	}
}
function e4s_pbAward(){
	include_once E4S_FULL_PATH . 'classes/eaAwards.php';
	$pbAwardObj = new eaAwards(310);
	exit ('Level : ' . $pbAwardObj->getEventLevelForAthleteDOB('2005-07-27', 3, 13.1));
}
function e4s_testAgeGroups(){
	$obj = new e4sAgeClass(E4S_AGE_DEFAULT_BASE, E4S_AOCODE_EA);
	$ags = $obj->getBaseAgeGroups();
	$ags = $obj->getAgeGroupsWithDOBs($ags,'2023-03-29');
	var_dump($ags);
}
function e4s_recalculateAgeGroups($obj){
	include_once E4S_FULL_PATH . 'entries/entries.php';
	e4s_recalculateCompAgeGroups($obj);
}
function moveOffWaitingList($obj) {
	$entryId = getNotNull($obj, 'entryid:Entry Id');
	$entryObj = new entryClass();
	$entryObj->moveOffWaitingList($entryId);
}

function emailSize($obj) {
	$max = getNotNull($obj, 'max:Number of Lines');
	$max = (int)$max;
	$body = '';
	$cnt = 1;
	while ($cnt <= $max) {
		$body .= 'Line' . $cnt . ' of ' . $max . '. This is a Very long line of text to check the email size limitation. This is a Very long line of text to check the email size limitation. This is a Very long line of text to check the email size limitation. This is a Very long line of text to check the email size limitation. <br>';
		$cnt++;
	}

	e4s_mail('paul_day@apiconsultancy.com', 'Test Email', $body);
}

function e4sRegisterUser($obj) {
	e4s_register($obj);
}

function testAdjust($compId) {
	$compObj = e4s_GetCompObj($compId);
	$x = $compObj->getEventAdjustment();
	var_dump($x);
}

function load_ani_clubs() {
	$sql = 'select *
            from ani_clubs';
	$result = e4s_queryNoLog($sql);
	while ($aniObj = $result->fetch_object()) {
		$aniObj->externid = (int)$aniObj->externid;
		$club = clubTranslate($aniObj->club);
		$club = addslashes($club);
		$sql = 'select id
                from ' . E4S_TABLE_CLUBS . "
                where clubname = '" . $club . "'";
		$clubResult = e4s_queryNoLog($sql);

		if ($clubResult->num_rows === 0) {
			echo $aniObj->club . '/' . $club . " not found\n";
//        }
		} else {
			$clubObj = $clubResult->fetch_object();

			$sql = 'update ' . E4S_TABLE_CLUBS . '
                    set externid = ' . $aniObj->externid . '
                    where id = ' . $clubObj->id;
			e4s_queryNoLog($sql);
			echo $aniObj->club . ' updated with ' . $aniObj->externid . "\n";
		}
	}
}

function securityTest() {
	echo 'Security Test on Comp 488:';
	$perm = 'No';
	if (userHasPermission(PERM_ADMIN, null, 488)) {
		$perm = 'Yes';
	}
	Entry4UISuccess('perm:' . $perm);
}

function e4s_fixActive() {
//    Entry4_Log_20220212
	$sql = 'select payload
            from ' . E4S_TABLE_LOG . "
            where info = 'URI:/private/athlete/registration/'
            and created > '2022-03-01'
            and uid = '80.76.197.151'";
	$result = e4s_queryNoLog($sql);
	while ($obj = $result->fetch_object()) {
//        firstname=Thomas&surname=Breen&dob=2000-09-28&regid=195285&club=Sliabh+Bhuide+Rovers+A.C.&clubId=381&gender=Male&regdate=2022-12-31&status=Create&class=0&system=IRL
		$arr = preg_split('~&~', $obj->payload);
		$regid = '';
		$activeDate = '';
		$aocode = E4S_AOCODE_ANI;
		foreach ($arr as $value) {
			$parms = preg_split('~=~', $value);
			if ($parms[0] === 'regid') {
				$regid = $parms[1];
				if (e4s_startsWith($regid, 'ANI')) {
					$regid = str_replace('ANI', '', $regid);
					$aocode = E4S_AOCODE_ANI;
				}
			}
			if ($parms[0] === 'regdate') {
				$activeDate = $parms[1];
			}
		}
		if ($regid !== '' and $activeDate !== '') {
			$update = 'update ' . E4S_TABLE_ATHLETE . "
                       set activeenddate = '" . $activeDate . "'
                       where urn = '" . $regid . "'
                       and aocode = '" . $aocode . "'";
//            echo "Update : " . $regid . "\n";
//            echo $update . "\n";
//            exit();
			e4s_queryNoLog($update);
		}
	}
	Entry4UISuccess();
}

function ea_athlete($obj) {
	$urn = checkFieldForXSS($obj, 'urn:URN');
	if (is_null($urn)) {
		$urn = '3590678';
	}
	$athlete = new athleteClass();
	$model = new stdClass();
	$model->aoCode = E4S_AOCODE_EA;
	$model->urn = $urn;
	$athlete->searchForAndSetRegisteredAthlete($model);
	Entry4UISuccess($athlete->getRow());
}
function ea_testURN($obj) {
	$firstName = checkFieldForXSS($obj, 'firstname:First Name');
	$surName = checkFieldForXSS($obj, 'surname:Last Name');
	$dob = checkFieldForXSS($obj, 'dob:DOB');

	$eaObj = new eaRegistrationClass();
	$obj = $eaObj->getURN($firstName, $surName, $dob);
//    $obj = $eaObj->loadClubs();
	Entry4UISuccess($obj);
}
function ea_test($obj) {
	$urn = checkFieldForXSS($obj, 'urn:URN');
	if (is_null($urn)) {
		$urn = '3590678';
	}
	$eaObj = new eaRegistrationClass();
	$obj = $eaObj->validateURNForEventDate($urn);
//    $obj = $eaObj->loadClubs();
	Entry4UISuccess($obj);
}
function e4s_eaList(){
	$sql = 'select id, firstName, surName, dob
			from ' . E4S_TABLE_ATHLETE . '
			where aocode = ""
			and urn is null
			and id between 193500 and 194000 ';
	$result = e4s_queryNoLog($sql);

	$eaObj = new eaRegistrationClass(E4S_EA_LIVE);
	while ($obj = $result->fetch_object()) {
		$urn = $eaObj->getURN($obj->firstName, $obj->surName, $obj->dob);
		echo "Checking : " . $obj->id;
		if ( $urn !== "" and !is_null($urn)){
			echo "  Urn : " . $urn;
			$sql = "select id
				from " . E4S_TABLE_ATHLETE . "
				where urn = " . $urn;
			$urnResult = e4s_queryNoLog( $sql );
			if ( $urnResult->num_rows === 0 ) {
				$upd = "update " . E4S_TABLE_ATHLETE . "
					set urn = " . $urn . ",
					    aocode = 'EA'
					where id = " . $obj->id;
				echo " updated ";
				e4s_queryNoLog( $upd );
			}
		}
		echo "\n";
	}
	Entry4UISuccess();
}
function ea_liveURN($obj) {
	$firstName = checkFieldForXSS($obj, 'firstname:First Name');
	$surName = checkFieldForXSS($obj, 'surname:Last Name');
	$dob = checkFieldForXSS($obj, 'dob:DOB');

	$eaObj = new eaRegistrationClass(E4S_EA_LIVE);
	$obj = '' . $eaObj->getURN($firstName, $surName, $dob);
//    $obj = $eaObj->loadClubs();
	Entry4UISuccess($obj);
}
function ani_live($obj) {
	$urn = checkFieldForXSS($obj, 'urn:URN');
	if ( $urn === 'null' or is_null($urn)){
		Entry4UISuccess();
	}

	$aniObj = new aniRegistrationClass();
	$obj = $aniObj->validateURN($urn);
	Entry4UISuccess($obj);
}
function aai_live($obj) {
	$urn = checkFieldForXSS($obj, 'urn:URN');
	if ( $urn === 'null' or is_null($urn)){
		Entry4UISuccess();
	}

	$aaiObj = new aaiRegistrationClass();
	$obj = $aaiObj->validateURN($urn);
	Entry4UISuccess($obj);
}
function ea_live($obj) {
	$urn = checkFieldForXSS($obj, 'urn:URN');
	if ( $urn === 'null' or is_null($urn)){
		Entry4UISuccess();
	}

	$eaObj = new eaRegistrationClass(E4S_EA_LIVE);
	$obj = $eaObj->validateURNForEventDate($urn);
	Entry4UISuccess($obj);
}

function ea_clubs() {
	///////////// delete unused clubs
	$sql = 'select id
            from ' . E4S_TABLE_CLUBS . '
            where id not in (select distinct(clubid)
            from ' . E4S_TABLE_ATHLETE . ')
            and id not in (select distinct(club2id)
                           from ' . E4S_TABLE_ATHLETE . ")
            and externid is null
            and clubtype = 'C'";
	$result = e4s_queryNoLog($sql);
	if ($result->num_rows > 0) {
		$deleteIds = array();
		while ($obj = $result->fetch_object()) {
			$deleteIds[] = $obj->id;
		}
		$sql = 'delete from ' . E4S_TABLE_CLUBS . '
            where id in (' . implode(',', $deleteIds) . ')';
		e4s_queryNoLog($sql);
	}
	//////////////////////// Set Clubname
	$sql = 'select id,
                   clubName
            from ' . E4S_TABLE_CLUBS;
	$result = e4s_queryNoLog($sql);
	while ($obj = $result->fetch_object()) {
		$clubName = clubTranslate($obj->clubName);
		if ($clubName !== $obj->clubName) {
			$sql = 'update ' . E4S_TABLE_CLUBS . "
                    set clubName = '" . addslashes($clubName) . "'
                    where id = " . $obj->id;
			e4s_queryNoLog($sql);
		}
	}
	////////////////////////////// update matched
	$sql = 'select   c.id,
                   ea.id eaId,
                   ea.externId,
                   ea.clubName
            from ' . E4S_TABLE_EACLUBS . ' ea,
                 ' . E4S_TABLE_CLUBS . ' c
            where ea.clubname = c.clubname
            and matched is null';
	$eaResult = e4s_queryNoLog($sql);
	$eaIds = array();
	while ($obj = $eaResult->fetch_object()) {
		$eaIds[] = $obj->eaId;
		$sql = 'update ' . E4S_TABLE_CLUBS . '
                set externid = ' . $obj->externId . '
                where id = ' . $obj->id;
		e4s_queryNoLog($sql);
	}
	if (!empty($eaIds)) {
		$sql = 'update ' . E4S_TABLE_EACLUBS . '
           set matched = true
           where id in (' . implode(',', $eaIds) . ' )';
		e4s_queryNoLog($sql);
	}
	///////// Get Areas
	$areas = array();
	$sql = 'select id,
                   name,
                   entityId
            from ' . E4S_TABLE_AREA . '
            order by id desc';
	$result = e4s_queryNoLog($sql);
	while ($obj = $result->fetch_object()) {
		$obj->id = (int)$obj->id;
		$obj->entityId = (int)$obj->entityId;
		$areas[$obj->name] = $obj;
	}
	///////// check not matched
	$sql = 'select id,
                   externId,
                   clubName
            from ' . E4S_TABLE_EACLUBS . '
            where matched is null';
//    $sql .= " and id = 458";
	$result = e4s_queryNoLog($sql);
	$count = 0;
	while ($obj = $result->fetch_object()) {
		$clubName = clubTranslate($obj->clubName);
		$sql = 'select id
                from ' . E4S_TABLE_CLUBS . "
                where clubname = '" . addslashes($clubName) . "'
                and country != 'Eire'";
		$result2 = e4s_queryNoLog($sql);
		if ($result2->num_rows === 1) {
			$count++;
			$obj2 = $result2->fetch_object();
			$sql = 'update ' . E4S_TABLE_EACLUBS . '
                       set matched = true
                       where id = ' . $obj->id;
			e4s_queryNoLog($sql);

			$sql = 'update ' . E4S_TABLE_CLUBS . '
                set externid = ' . $obj->externId . '
                where id = ' . $obj2->id;
			e4s_queryNoLog($sql);
		}
	}
	///////// create new clubs
	$sql = 'select id,
                   externId,
                   clubName,
                   locality,
                   region
            from ' . E4S_TABLE_EACLUBS . '
            where matched is null
            and externid not in (select externid from ' . E4S_TABLE_CLUBS . ')';
//    $sql .= " and id = 458";
	$result = e4s_queryNoLog($sql);
	$count = 0;
	while ($obj = $result->fetch_object()) {
		$clubName = clubTranslate($obj->clubName);
		$clubName = addslashes($clubName);
		$country = 'England';
		$region = $obj->locality;
		$areaId = 0;
		if (strpos($obj->region, 'Wales') !== FALSE) {
			$country = 'Wales';
		}
		if ($obj->region === 'Yorkshire & Humberside') {
			$region = 'Yorkshire';
		}
		if (array_key_exists($region, $areas)) {
			$areaId = $areas[$region]->id;
		} elseif ($region !== '') {
			$areaId = e4s_createAreaFor($region, $country, $areas);
		} else {
			$areaId = $areas[$country]->id;
		}
		$insertSql = 'insert into ' . E4S_TABLE_CLUBS . '(
        externid,
        clubname,
        region,
        country,
        areaid,
        clubtype,
        active) values (';
		$insertSql .= $obj->externId;
		$insertSql .= ',';
		$insertSql .= "'" . $clubName . "'";
		$insertSql .= ',';
		$insertSql .= "'" . $region . "'";
		$insertSql .= ',';
		$insertSql .= "'" . $country . "'";
		$insertSql .= ',';
		$insertSql .= $areaId;
		$insertSql .= ',';
		$insertSql .= "'C'";
		$insertSql .= ',';
		$insertSql .= 'false';

		$insertSql .= ')';
		e4s_queryNoLog($insertSql);
	}
	Entry4UISuccess($count);
}

function e4s_createAreaFor($region, $country, $areas) {
	$countryObj = $areas[$country];
	$sql = 'insert into ' . E4S_TABLE_AREA . ' (entityId, name, parentid)
            values (
                ' . ($countryObj->entityId - 1) . ",
                '" . $region . "',
                " . $countryObj->id . '
            )';
	e4s_queryNoLog($sql);
	return e4s_getLastID();
}

function ea_test2() {
	$opts = array('http' => array('method' => 'GET', 'ssl' => array('verify_peer' => FALSE, 'verify_peer_name' => FALSE), 'header' => array('X-TRAPI-CALLKEY' => 'E4Suser', 'X-TRAPI-CALLSECRET' => 'En3994)sp', 'X-TRAPI-CALLDATETIME' => '2022-03-01T22:22:00')));

	$context = stream_context_create($opts);

	// Open the file using the HTTP headers set above
	$file = file_get_contents('https://myathletics.uka.org.uk/TrinityAPI/TrinityAPIService.svc/race-provider/individuals?firstname=Jessica&lastname=Day&dob=03+September+2004', FALSE, $context);

	echo "Testing Files\n\n\n!";
	var_dump($_FILES);
	echo "Post Files\n\n\n!";
	var_dump($_POST);
	echo "Request Files\n\n\n!";
	var_dump($_REQUEST);

}

function e4s_websocket1() {
	require E4S_FULL_PATH . 'vendor/autoload.php';

	connect(R4S_SOCKET_SERVER)->then(function ($conn) {
		$conn->on('message', function ($msg) use ($conn) {
			echo "Received: {$msg}\n";
			$conn->close();
		});

		$conn->send('{"action":"sendmessage","data":"{\"key\":\"10-1\",\"comp\":{\"id\":193},\"action\":\"photofinish\",\"deviceKey\":\"c46e3a380ebd7e6e053c6468a38b6ce4\",\"securityKey\":\"2865c8526e264cfd672aeca95c0f09e7\"}"}');
	}, function ($e) {
		echo "Could not connect: {$e->getMessage()}\n";
	});
}

function e4s_testObject() {
	$sql = 'select *
            from ' . E4S_TABLE_COMPETITION . '
            where id = 197';
	$result = e4s_queryNoLog($sql);
	$arr = $result->fetch_array(PDO::FETCH_OBJ);
//    $arr = $result->fetch_all(PDO::FETCH_OBJ);
	var_dump($arr);
}

function e4s_websocket2() {
	include_once E4S_FULL_PATH . 'classes/webSocketClientClass.php';
	$WebSocketClient = new WebsocketClient(E4S_TEST_SOCKET_SERVER, 443);
	echo $WebSocketClient->sendData('Paul');
	unset($WebSocketClient);
}

function e4s_create_wc_category($term, $desc = '') {
	return wp_insert_term($term, 'product_cat', array('description' => $desc, // optional
	                                                  'parent' => 0, // optional
	                                                  'slug' => $term // optional
	));
}

function e4s_correctProductCategory($compId) {
	require_once ABSPATH . 'wp-admin/includes/taxonomy.php';
	$sql = 'select name
            from ' . E4S_TABLE_COMPETITION . '
            where id = ' . $compId;
	$result = e4s_queryNoLog($sql);
	if ($result->num_rows !== 1) {
		echo 'Competition ' . $compId . " NOT FOUND\n";
		return;
	}
	$compObj = $result->fetch_object();
	$compName = $compObj->name;
	$sql = 'select term_id
            from ' . E4S_TABLE_TERMS . "
            where name = 'comp" . $compId . "'";
	$result = e4s_queryNoLog($sql);
	if ($result->num_rows !== 1) {
		$newCat = e4s_create_wc_category('comp' . $compId, $compName);
		if (!array_key_exists('term_id', $newCat)) {
			$displayName = e4sCompetition::getFormattedDisplayName($compId, $compName);
			Entry4UIError(8025, 'Comp ' . $displayName . ' Category does not exist and failed to create');
		}
		$term_id = $newCat['term_id'];
	} else {
		$termObj = $result->fetch_object();
		$term_id = (int)$termObj->term_id;
	}

	$sql = 'select id
            from ' . E4S_TABLE_POSTS . "
            where post_content like '%compid\":" . $compId . "%'";
	$result = e4s_queryNoLog($sql);
	$updated = 0;
	$sameCount = 0;
	while ($obj = $result->fetch_object()) {
		$prod = wc_get_product((int)$obj->id);
		if ($prod === FALSE) {
			$displayName = e4sCompetition::getFormattedDisplayName($compId, $compName);
			echo 'Product ' . $obj->id . ' for competition ' . $displayName . " does not exist\n";
			return;
		}
		$terms = $prod->get_category_ids();
		$newTerms = [$term_id];
		if ($terms !== $newTerms) {
			$prod->set_category_ids($newTerms);
			$prod->save();
			$updated += 1;
		} else {
			$sameCount += 1;
		}
	}
	$displayName = e4sCompetition::getFormattedDisplayName($compId, $compName);
	echo $displayName . ' ' . $result->num_rows . ' processed. ' . $updated . ' updated and ' . $sameCount . " already set\n";
	return;
}

function finance2021() {
	$sql = 'select id compId,
                   name competition,
                   date compDate
            from ' . E4S_TABLE_COMPETITION;
	$result = e4s_queryNoLog($sql);
	$comps = array();
	while ($compObj = $result->fetch_object()) {
		$compObj->compId = (int)$compObj->compId;
		$compObj->value = 0;
		$compObj->refunded = 0;
		$comps [$compObj->compId] = $compObj;
	}

	$sql = 'select id,
                   compId
            from ' . E4S_TABLE_COMPEVENTS;
	$ceRecords = array();
	$result = e4s_queryNoLog($sql);
	while ($obj = $result->fetch_object()) {
		$ceRecords[(int)$obj->id] = (int)$obj->compId;
	}

	$itemSql = 'select  wci.order_id orderId,
                        wci.order_item_id orderItemId,
                        wci.order_item_name orderItemName,
                        wcim.meta_value liveValue,
                        p.post_status orderStatus
                from ' . E4S_TABLE_WCORDERITEMS . ' wci,
                     ' . E4S_TABLE_WCORDERITEMMETA . ' wcim,
                     ' . E4S_TABLE_POSTS . " p
                where order_item_name like '% : %'
                and p.id = wci.order_id
                and p.post_status in( '" . WC_ORDER_PAID . "', 'wc-refunded')
                and wci.order_item_id = wcim.order_item_id
                and wcim.meta_key = '_line_total'";
	$result = e4s_queryNoLog($itemSql);


	while ($obj = $result->fetch_object()) {
		$obj->orderId = (int)$obj->orderId;
		$obj->orderItemId = (int)$obj->orderItemId;
		$obj->liveValue = (float)$obj->liveValue;
		$nameArr = preg_split('~ :~', $obj->orderItemName);
		$ceId = (int)$nameArr[0];
		if (!array_key_exists($ceId, $ceRecords)) {
			// event changed and not found so check entry CEID
			$sql = 'select e.compEventId
                    from ' . E4S_TABLE_WCORDERITEMMETA . ' wcim,
                         ' . E4S_TABLE_ENTRIES . ' e
                    where order_item_id = ' . $obj->orderItemId . "
                    and meta_key = '" . WC_POST_PRODUCT_ID . "'
                    and e.variationid = wcim.meta_value";
//            $result = e4s_queryNoLog($sql);
//            if ( $result->num_rows !== 1 ){
//                echo "failed to find event for Order " . $obj->orderId . "/" . $obj->orderItemId . "\n";
//            } else {
//                $obj = $result->fetch_object();
//                $ceId = (int)$obj->compEventId;
//            }
		}
		if (array_key_exists($ceId, $ceRecords)) {
			$compId = $ceRecords [$ceId];
			if (!array_key_exists($compId, $comps)) {
				var_dump($obj);
				Entry4UIError(8000, 'Failed to find competition ' . $compId);
			}
			if ($obj->orderStatus === WC_ORDER_PAID) {
				$comps[$compId]->value += $obj->liveValue;
			} else {
				$comps[$compId]->refunded += $obj->liveValue;
			}
		}
	}
	echo "Competition Id,Name,Date,Taken,Refunded\n";
	foreach ($comps as $compId => $compObj) {
		echo $compId . ',' . $compObj->competition . ',' . $compObj->compDate . ',' . $compObj->value . ',' . $compObj->refunded . "\n";
	}
	exit();
}

function getNotNull($obj, $param) {
	$prop = checkFieldForXSS($obj, $param);
	$fieldArr = explode(':', $param);
	$fieldName = $fieldArr[0];
	$displayField = $fieldArr[1];
	if (is_null($prop)) {
		Entry4UIError(2010, $displayField . ' is null');
	}

	return $prop;
}

exit();
function checkCompEntryAgeGroups($compId, $update) {
	$allCompDOBs = getAllCompDOBs($compId);
	$sql = 'select ce.id ceId,
                   ce.eventId,
                   ce.ageGroupId,
                   eg.name eventName,
                   eg.startDate
            from ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTGROUPS . ' eg
            where ce.compid = ' . $compId . '
            and   ce.maxgroup = eg.id';
	$result = e4s_queryNoLog($sql);
	$compEvents = array();
	while ($obj = $result->fetch_object()) {
		$obj->ceId = (int)$obj->ceId;
		$obj->eventId = (int)$obj->eventId;
		$obj->ageGroupId = (int)$obj->ageGroupId;
		$compEvents[$obj->eventId . '-' . $obj->ageGroupId] = $obj;
	}
	$sql = 'select e.id, 
                   e.athlete,
                   a.dob dob,
                   ce.compId,
                   ce.ageGroupId,
                   ce.eventId,
                   u.user_email userEmail,
                   c.name competition
            from ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_ATHLETE . ' a,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_COMPETITION . ' c,
                 ' . E4S_TABLE_USERS . ' u
            where ce.id = e.compeventid
            and e.athleteid = a.id
            and e.userid = u.id
            and e.paid = ' . E4S_ENTRY_PAID . '
            and c.id = ce.compid
            and ce.compid = ' . $compId;
	$result = e4s_queryNoLog($sql);
	$results = array();
	while ($obj = $result->fetch_object()) {
		$obj->id = (int)$obj->id;
		$obj->eventId = (int)$obj->eventId;
		$obj->ageGroupId = (int)$obj->ageGroupId;
		$athleteAg = getAgeGroupInfo($allCompDOBs, $obj->dob);
		if (array_key_exists('agid', $athleteAg['ageGroup'])) {
			if ($obj->ageGroupId !== (int)$athleteAg['ageGroup']['agid']) {
				$updated = entryIncorrectAge($obj, $compEvents, $athleteAg['ageGroup'], $update);
				if ($updated !== '') {
					$results[] = $updated;
				}
			}
		}
	}
	return $results;
}

function entryIncorrectAge($entryObj, $compEvents, $targetAgObj, $update) {
	$currentKey = $entryObj->eventId . '-' . $entryObj->ageGroupId;
	if (!array_key_exists($currentKey, $compEvents)) {
		return 'Current Event Not found for ' . $entryObj->athlete;
	}
	$targetKey = $entryObj->eventId . '-' . $targetAgObj['agid'];
	if (!array_key_exists($targetKey, $compEvents)) {
		return 'Target Event Not found for ' . $entryObj->athlete;
	}
	$currentEvent = $compEvents[$currentKey];
	$targetEvent = $compEvents[$targetKey];
//    if ( $entryObj->athlete === "Alice Bates" ){
//        var_dump($entryObj);
//        var_dump($targetAgObj);
//        var_dump($currentEvent);
//        var_dump($targetEvent);
//        exit();
//    }
	$update = 'update ' . E4S_TABLE_ENTRIES . '
               set compeventid = ' . $targetEvent->ceId . ",
                   eventagegroup = '" . $targetAgObj['Name'] . "'
               where id = " . $entryObj->id;
	if ($update) {
		e4s_queryNoLog($update);
	}
	if ($currentEvent->eventName === $targetEvent->eventName and $currentEvent->startDate === $targetEvent->startDate) {
		// Dont send email if the same event and time
		return 'Moved ' . $entryObj->athlete . ' in the ' . $targetEvent->eventName . '(' . $currentEvent->ceId . '-' . $targetEvent->ceId . ') but no email required.';
	}

	$currentDate = date_create($currentEvent->startDate);
	$currentStartDate = date_format($currentDate, E4S_FORMATTED_DATETIME);
	$targetDate = date_create($targetEvent->startDate);
	$targetStartDate = date_format($targetDate, E4S_FORMATTED_DATETIME);
	$body = 'Dear ' . $entryObj->athlete . ",<br><br>We are sorry to say there has been a mistake in the competition '" . $entryObj->competition . "'.<br>";
	$body .= 'The age groups associated to events had been configured incorrectly and a small number of athletes have entered the wrong event. We have corrected this as follows :<br>';
	$body .= 'You were originally in the ' . $currentEvent->eventName . ' at ' . $currentStartDate . '.<br>';
	$body .= 'You are now in the ' . $targetEvent->eventName . ' at ' . $targetStartDate . '.<br><br>';
	$body .= 'We are sorry about this issue but if you have any issues regarding this, please contact us at support@entry4sports.com <br><br>';
	$body .= 'Please do not reply to this email as it has been system generated and inbound emails are not monitored.<br><br>';
	if ($update) {
		e4s_mail($entryObj->userEmail, 'Entry correction for ' . $entryObj->athlete, $body);
	}
	return 'Moved entry for ' . $entryObj->athlete . ' from ' . $currentEvent->eventName . ' to ' . $targetEvent->eventName;
}

function finance_report_2($fromDate) {
	$sql = "
select id id
from " . E4S_TABLE_POSTS . "
where post_type = '" . WC_POST_ORDER . "'
and post_date_gmt > '" . $fromDate . "'
and id > 83270
order by post_date_gmt
";
	$result = e4s_queryNoLog($sql);
	echo $sql . "\nRows : " . $result->num_rows . "\n";
	$count = 5;
	while ($obj = $result->fetch_object()) {
		outputOrder2($obj->id);
//    $count--;
//    if ( $count < 1 ){
////        exit();
//    }
	}
	echo 'finished';
	exit();
}

function finance_report_1($fromDate) {
	$sql = "
select id id
from " . E4S_TABLE_POSTS . "
where post_type = '" . WC_POST_ORDER . "'
and post_date_gmt > '" . $fromDate . "'
order by post_date_gmt
";
	$result = e4s_queryNoLog($sql);
	echo $sql . "\nRows : " . $result->num_rows . "\n";
	$count = 5;
	while ($obj = $result->fetch_object()) {
		outputOrder($obj->id);
//    $count--;
//    if ( $count < 1 ){
////        exit();
//    }
	}
	echo 'finished';
	exit();
}

function finance_report_3($fromDate) {
	$sql = "
        select id id
        from " . E4S_TABLE_POSTS . "
        where post_type = '" . WC_POST_ORDER . "'
        and post_date_gmt > '" . $fromDate . "'
        order by post_date_gmt
        ";
	$result = e4s_queryNoLog($sql);
	$compInfo = array();
	while ($obj = $result->fetch_object()) {
		$order = wc_get_order($obj->id);
		$items = $order->get_items();
		$status = $order->get_status();
		if ($status !== 'pending' and $status !== 'cancelled') {
			foreach ($items as $item) {
				$product = $item->get_product();
				if ($product === FALSE) {
					continue;
				}
				$desc = $product->get_description();
				$descObj = e4s_getDataAsType($desc, E4S_OPTIONS_OBJECT);
				if (isset($descObj->compid)) {
					$compId = $descObj->compid;
					if (!array_key_exists($compId, $compInfo)) {
						$obj = new stdClass();
						$obj->subTotal = 0;
						$compInfo[$compId] = $obj;
					} else {
						$obj = $compInfo[$compId];
					}
					$obj->subTotal += $item->get_subtotal();
				}
			}
		}
	}
	echo 'finished';
	exit();
}

function outputOrder($orderId) {
	$order = wc_get_order($orderId);
	$orderDate = $order->get_date_paid();
	$orderDate = e4s_iso_to_sql($orderDate);
	$items = $order->get_items();
	$status = $order->get_status();
	$compInfo = array();
	foreach ($items as $item) {
		if ($status === 'pending' or $status === 'cancelled') {
			continue;
		}
		$product = $item->get_product();
		if ($product === FALSE and $status === 'completed') {
			continue;
		}
		if ($product === FALSE) {
			echo $orderId . ' - ' . $status;
			exit();
		}
		$desc = $product->get_description();
		$descObj = e4s_getDataAsType($desc, E4S_OPTIONS_OBJECT);
		if (isset($descObj->compid)) {
			$compId = $descObj->compid;
		} else {
			echo $desc;
			exit();
		}
		if (!array_key_exists($compId, $compInfo)) {
			$obj = new stdClass();
			$obj->orderId = $orderId;
			$obj->status = $status;
			$obj->orderDate = $orderDate;
			$obj->compId = $compId;
			if (!isset($descObj->compname)) {
				// team events dont store compname ???
				$compObj = e4s_GetCompObj($compId);
				$obj->compName = $compObj->getName();
			} else {
				$obj->compName = $descObj->compname;
			}
			$obj->subTotal = 0;
			$compInfo[$compId] = $obj;
		}
		$obj->subTotal += $item->get_subtotal();
	}
	foreach ($compInfo as $compLineObj) {
		e4s_report_output($compLineObj);
	}

}

// {"compid":199,"compname":"Senior and U20 Championships","athleteid":160843,"athlete":"Samuel Devlin",
//  "ceid":23441,"event":"1500m","compclubid":82,"locationid":20,"teamid":0}
function outputOrder2($orderId) {
	$order = wc_get_order($orderId);
	$orderDate = $order->get_date_paid();
	$orderDate = e4s_iso_to_sql($orderDate);
	$items = $order->get_items();
	$status = $order->get_status();

	foreach ($items as $item) {
		if ($status === 'pending' or $status === 'cancelled') {
			continue;
		}
		$product = $item->get_product();
		if ($product === FALSE and $status === 'completed') {
			continue;
		}
		if ($product === FALSE) {
			echo $orderId . ' - ' . $status;
			exit();
		}
		$desc = $product->get_description();
		$descObj = e4s_getDataAsType($desc, E4S_OPTIONS_OBJECT);
		if (isset($descObj->compid)) {
			$compId = $descObj->compid;
		} else {
			echo $desc;
			exit();
		}

		$obj = new stdClass();
		$obj->orderId = $orderId;
		$obj->status = $status;
		$obj->orderDate = $orderDate;
		$obj->compId = $compId;
		if (!isset($descObj->compname)) {
			// team events dont store compname ???
			$compObj = e4s_GetCompObj($compId);
			$obj->compName = $compObj->getName();
			$sql = 'select et.name teamName,
                           eg.name eventName
                    from ' . E4S_TABLE_EVENTTEAMENTRIES . ' et,
                         ' . E4S_TABLE_COMPEVENTS . ' ce,
                         ' . E4S_TABLE_EVENTGROUPS . ' eg
                    where et.productid = ' . $product->get_id() . '
                    and   ce.id = et.ceid
                    and ce.maxgroup = eg.id';
			$result = e4s_queryNoLog($sql);
			if ($result->num_rows !== 1) {
				// Product does not exist ?????
				$teamObj = new stdClass();
				$itemName = $item->get_name();
				$nameArr = preg_split('~.~', $itemName);
				$teamObj->teamName = $nameArr[1];
				$event = $nameArr[2];
				$event = preg_split('~ at ~', $event);
				$teamObj->eventName = $event[0];
			} else {
				$teamObj = $result->fetch_object();
			}
			$obj->athlete = $teamObj->teamName;
			$obj->event = $teamObj->eventName;
		} else {
			$obj->compName = $descObj->compname;
			if (!isset($descObj->athlete)) {
				if (isset($descObj->ticket)) {
					$obj->athlete = $descObj->ticket;
					$obj->event = 'Ticket';
				} else {
					echo $orderId . ' : ' . $desc;
					exit();
				}
			} else {
				$obj->athlete = $descObj->athlete;
				$obj->event = $descObj->event;
			}
		}

		$obj->subTotal = 0;

		$obj->subTotal = $item->get_subtotal();
		e4s_report_output2($obj);
	}
}

function e4s_report_output2($obj) {
	$sep = ',';
	$quote = '"';
	echo $obj->orderId . $sep;
	echo $quote . $obj->status . $quote . $sep;
	echo $quote . $obj->orderDate . $quote . $sep;
	echo $obj->compId . $sep;
	echo $quote . addslashes($obj->compName) . $quote . $sep;
	echo $quote . addslashes($obj->athlete) . $quote . $sep;
	echo $quote . $obj->event . $quote . $sep;
	echo $obj->subTotal . "\n";
}

function e4s_report_output($obj) {
	$sep = ',';
	$quote = '"';
	echo $obj->orderId . $sep;
	echo $quote . $obj->status . $quote . $sep;
	echo $quote . $obj->orderDate . $quote . $sep;
	echo $obj->compId . $sep;
	echo $quote . addslashes($obj->compName) . $quote . $sep;
	echo $obj->subTotal . "\n";
}

$order = wc_get_order(135744);
$order->add_order_note('Hello Nick');
exit();

e4s_runStripeReport();

exit();
#include_once E4S_FULL_PATH . 'admin/cronFunctions.php';
read_email(136);
function read_email($id) {
	$model = new stdClass();
	$model->action = '';
	$model->process = 'C';
	$model->public = FALSE;
	$model->id = $id;
	$model->to = '';
	$model->title = '';
	$model->body = '';
	$model->options = null;
	$model->type = 'M';
	$model->resend = FALSE;
	$get = new stdClass();
	$get->messages = FALSE;
	$get->emails = FALSE;
	$get->read = null;
	$get->sent = null;
	$get->deleted = null;
	$model->get = $get;
	e4s_readMessage($model, TRUE);
}

function test_sendEmail() {
	$model = new stdClass();
	$model->action = '';
	$model->process = 'C';
	$model->public = FALSE;
	$model->id = null;
	$model->to = ['_COMP242_'];
	$model->title = 'Test Message..';
	$model->body = 'Test Body Message..';
	$model->options = null;
	$model->type = 'M';
	$model->resend = FALSE;
	$get = new stdClass();
	$get->messages = FALSE;
	$get->emails = FALSE;
	$get->read = null;
	$get->sent = null;
	$get->deleted = null;
	$model->get = $get;

	e4s_createMessage($model, TRUE);
	exit();
}

//  ["pageInfo"]=>
//  object(stdClass)#15521 (6) {
//  ["startswith"]=>
//    NULL
//    ["exact"]=>
//    bool(false)
//    ["page"]=>
//    NULL
//    ["pagesize"]=>
//    NULL
//    ["sortkey"]=>
//    string(2) "id"
//["sortorder"]=>
//    string(3) "asc"
//  }
//  ["action"]=>
//  string(0) ""
//}
exit();
$messageobj = new e4SMessageClass(24540);
$obj = $messageobj->getDefaultObj();
$obj->get->emails = TRUE;
$obj->get->messages = TRUE;
Entry4UISuccess($messageobj->getMessages($obj));

exit();
e4s_CheckForEntriesClosing();
exit();
$body = 'This is some <b>html</b> content';
$body .= Entry4_emailFooter();
e4s_mail('paul.day@apiconsultancy.com,pday@apiconsultancy.com', 'This is a test subject', $body, Entry4_mailHeader('entries', TRUE), 1);
Entry4UISuccess();
exit();

$mailObj = new e4SMessageClass(0, 'paul.day@apiconsultancy.com');
$body = 'This is some <b>html</b> content';
$body .= Entry4_emailFooter();
$return = $mailObj->wp_mail('paul.day@apiconsultancy.com', 'This is a test subject', $body, Entry4_mailHeader('entries', TRUE), 1);
//$mailObj->wp_mail("paul@errortestingfore4s.com","This is a subject",$body, Entry4_mailHeader("testFrom",true), 1);
Entry4UISuccess($return);
exit();


$createdDate = date_create('2021-07-16 21:00:00');
$nowDate = date_create('2021-07-16 20:30:33');
$diffInSeconds = $nowDate->getTimestamp() - $createdDate->getTimestamp();
echo $diffInSeconds . ' - ';
if ($diffInSeconds > 0) {
	echo 'passed';
}
exit();


correctESAAResults();
exit();

function correctESAAResults() {

	$sql = '
        select e.id,
               e.athleteid,
               e.athlete,
               e.clubid,
               c.clubname clubName,
               a.gender,
               e.teambibno,
               a.type,
               e.agegroupid
        from ' . E4S_TABLE_ENTRIES . ' e,
             ' . E4S_TABLE_CLUBS . ' c,
             ' . E4S_TABLE_ATHLETE . " a
        where teambibno > 0
        and c.id = e.clubid
        and e.athleteid = a.id
        and a.type = 'ES'
        ";

	$result = e4s_queryNoLog($sql);
	$entries = array();
	while ($obj = $result->fetch_object()) {
		$entries [$obj->athlete . '-' . $obj->clubName] = $obj;
	}
	$sql = '
        select r.*
        from ' . E4S_TABLE_EVENTRESULTS . ' r,
             ' . E4S_TABLE_EVENTRESULTSHEADER . ' rh
        where rh.compid = 265
        and   rh.id = r.resultheaderid
    ';
	$result = e4s_queryNoLog($sql);
	while ($obj = $result->fetch_object()) {
		$key = $obj->athlete . '-' . $obj->club;
		$debug = FALSE;
		if ($obj->athlete === 'Zoe Smith' and $obj->club === 'Nottinghamshire') {
			$debug = TRUE;
		}
		if (array_key_exists($key, $entries)) {
			$ageGroup = 'Junior';
			$entry = $entries[$key];
			if ((int)$entry->agegroupid === 214) {
				$ageGroup = 'Inter';
			}
			if ((int)$entry->agegroupid === 215) {
				$ageGroup = 'Senior';
			}
//            if(
//                $entry->athleteid !== $obj->athleteid
//                or $entry->gender !== $obj->gender
//                or $entry->teambibno !== $obj->bibno
//                or $ageGroup !== $obj->agegroup
//            ){
			if ($debug) {
				echo $key . ' -> ' . $entry->athleteid . ' , ' . $obj->athleteid . "\n";
			}
			$uSql = 'update ' . E4S_TABLE_EVENTRESULTS . '
                         set athleteid = ' . $entry->athleteid . ",
                             gender = '" . $entry->gender . "',
                             bibno = '" . $entry->teambibno . "',
                             agegroup = '" . $ageGroup . "'
                         where id = " . $obj->id;
//                echo $uSql . "\n";
			e4s_queryNoLog($uSql);
//            }
		} else {
			if ($debug) {
				var_dump($obj);
			}
		}
	}
}

function generateEsAthleteTickets() {
	$sql = 'select id athleteId
            from ' . E4S_TABLE_ATHLETE . "
            where type = 'ES'";

	$result = e4s_queryNoLog($sql);
	while ($obj = $result->fetch_object()) {
		$sql = 'select *
                from ' . E4S_TABLE_TICKET . '
                where athleteid = ' . $obj->athleteId . '
                and compid = 265';
		$ticketResult = e4s_queryNoLog($sql);
		if ($ticketResult->num_rows === 0) {
			$sql = 'insert into ' . E4S_TABLE_TICKET . ' (orderid, itemid, compid, athleteid, linkedathleteid, element, stadiumseatid, seatno, userid) 
                    values (
                        -1,
                        ' . $obj->athleteId . ',
                        265,
                        ' . $obj->athleteId . ',
                        0,
                        1,
                        0,
                        0,
                        1
                    )';
			e4s_queryNoLog($sql);
		}
	}
//    emailEsAthletes();

	emailEsAthletesClarity();
}

function emailEsAthletes() {
	$sql = "select a.id id, 
                   a.email email, 
                   a.firstname firstname, 
                   a.surname surname,
                   t.itemid itemid,
                   t.id ticketid,
                   t.element element,
                   t.compid compId,
                   date_format(eg.startdate,'%D July 2021') date
                    
                from " . E4S_TABLE_TICKET . ' t,
                     ' . E4S_TABLE_ATHLETE . ' a,
                     ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTGROUPS . " eg
                where t.itemid = t.athleteid
                and   t.orderid = -1
                and   t.compid = 265
                and   t.athleteid = a.id
                and   e.athleteid = a.id
                and   e.compeventid = ce.id
                and   ce.maxgroup = eg.id
                and   a.email != ''
                and   a.options not like '%emailsent%'
                order by a.id
                limit 500";

	$result = e4s_queryNoLog($sql);
	if ($result->num_rows === 0) {
		return;
	}

	$emails = array();

	while ($row = $result->fetch_assoc()) {
		$athleteId = $row['id'];
		if (!array_key_exists($athleteId, $emails)) {
			$emails [$athleteId] = array();
		}
		$emails [$athleteId][] = $row;
	}

	foreach ($emails as $emailArr) {
		$athleteFirstName = $emailArr[0]['firstname'];
		$athleteEmail = $emailArr[0]['email'];

		$body = 'Dear ' . $athleteFirstName . ',<br><br>';

		$body .= 'Please find attached your Athlete ticket for English Schools 2021';
		$body .= '<br><br>';
		$body .= 'You will need this to gain access to the athletes entrance.<br>';
		$body .= 'When you are not competing you are expected to find your parent/guardian or coach and sit with them in designated covid secure seats they have purchased.<br>';
		$body .= '<br>';
		foreach ($emailArr as $email) {
			$obj = new stdClass();
			$obj->ticketId = (int)$email['ticketid'];
			$obj->orderId = -1;
			$obj->itemId = (int)$email['itemid'];
			$obj->element = (int)$email['element'];
			$guidObj = new e4sTicketGuid($obj);
			$compdateStr = $email['date'];
			$body .= "Click here for your <a href='https://" . E4S_CURRENT_DOMAIN . '/#/ticket/' . $emailArr[0]['compId'] . '/' . $guidObj->getFullGuid() . "'>English Schools Athlete Ticket 2021</a> held on " . $compdateStr . '<br>';
		}

		$body .= Entry4_emailFooter();
		if (!e4s_isLiveDomain()) {
			$body .= '<br>TESTING EMAIL. Would have been sent to ' . $athleteEmail;
			$athleteEmail = E4S_SUPPORT_EMAIL;
		}

		logTxt('Emailing : ' . $email['itemid'] . '-' . $athleteEmail);
		e4s_mail($athleteEmail, 'ESAA 2021 Athlete Ticket', $body, Entry4_mailHeader('', TRUE));
		$sql = 'update ' . E4S_TABLE_ATHLETE . "
                set options = '{\"emailsent\":1}'
                where id = " . $email['itemid'];
		e4s_queryNoLog($sql);

	}
}

function emailEsAthletesClarity() {
	$sql = "select a.id id, 
                   a.email email, 
                   a.firstname firstname, 
                   a.surname surname,
                   t.itemid itemid,
                   t.id ticketid,
                   t.element element,
                   t.compid compId,
                   date_format(eg.startdate,'%D July 2021') date
                    
                from " . E4S_TABLE_TICKET . ' t,
                     ' . E4S_TABLE_ATHLETE . ' a,
                     ' . E4S_TABLE_ENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTGROUPS . " eg
                where t.itemid = t.athleteid
                and   t.orderid = -1
                and   t.compid = 265
                and   t.athleteid = a.id
                and   e.athleteid = a.id
                and   ce.maxgroup = eg.id
                and   e.compeventid = ce.id
                and   a.email != ''
                and   a.options not like '%emailsent2%'
                order by a.id
                limit 400";

	$result = e4s_queryNoLog($sql);
	if ($result->num_rows === 0) {
		return;
	}

	$emails = array();
	$ids = array();
	while ($row = $result->fetch_assoc()) {
		$athleteId = $row['id'];
		$ids[] = $athleteId;
		if (!array_key_exists($athleteId, $emails)) {
			$emails [$athleteId] = array();
		}
		$emails [$athleteId][] = $row;
	}

	$body = 'Dear Sir/Madam,<br><br>';

	$body .= 'A number of parents have emailed in to say there is no attachment in the previous email for English Schools 2021 athlete tickets.';
	$body .= '<br>';
	$body .= 'Please accept our apologies as the email did not make it clear.<br><br>';
	$body .= "Please click the dynamically generated link attached in the original email that says <br>'English Schools 2021 held on 9/10/11th July 2021'<br><br>";
	$body .= 'After authenticating, you will be taken to the ticket for your athlete.<br>';
	$body .= '<br>';
	$body .= 'Below is the example email and we have highlighted the link in the original email that you should use<br><br>';
	$body .= '-----------------------------------------------------------------------------------------<br><br>';
	$body .= 'Dear Athlete,<br><br>';
	$body .= 'Please find attached your Athlete ticket for English Schools 2021<br>';
	$body .= 'You will need this to gain access to the athletes entrance.<br>';
	$body .= 'When you are not competing you are expected to find your parent/guardian or coach and sit with them in designated covid secure seats they have purchased.<br><br>';
	$body .= 'Click this link in the original email - ><b>English Schools 2021 held on 9/10/11th July 2021</b><br>';
	$body .= '-----------------------------------------------------------------------------------------<br><br>';
	$body .= Entry4_emailFooter();
	$headers = Entry4_mailHeader('', TRUE);

	echo "Emailing : \n";
	if (!e4s_isLiveDomain()) {
		$body .= '<br>TESTING EMAIL. Would have been sent to everyone';
	} else {
		foreach ($emails as $emailObj) {
			$headers[] = 'Bcc:' . $emailObj[0]['email'];
			echo $emailObj[0]['email'] . "\n";
		}
	}

	e4s_mail('entries@entry4sports.com', 'ESAA 2021 Athlete Ticket. Adendum', $body, $headers);
	$sql = 'update ' . E4S_TABLE_ATHLETE . "
                set options = '{\"emailsent2\":1}'
                where id in (" . implode(',', $ids) . ')';
	e4s_queryNoLog($sql);
}

function emailESReminder() {
	$sql = 'select user_email email
            from ' . E4S_TABLE_USERS . ' u,
                 ' . E4S_TABLE_STADIUMSEATING . ' s
            where u.id = s.userid
            and s.status = ' . E4S_SEAT_STATUS_PAID;
	$result = e4s_queryNoLog($sql);
	$emails = array();
	$blockCount = 100;
	$counter = $blockCount;
	while ($obj = $result->fetch_object()) {
		$emails[] = $obj->email;
		$counter--;
		if ($counter < 0) {
			sendEsEmailReminder($emails);
			$emails = array();
			$counter = $blockCount;
		}
	}
	if (sizeof($emails) > 0) {
		sendEsEmailReminder($emails);
	}
	Entry4UISuccess();
}

function sendEsEmailReminder($emails) {

	$body = 'Hello,<br><br>';
	$body .= 'Your tickets are now available from your order history in the Account area of our system. Login to Entry4sports.co.uk and from the top menu, choose My Account.<br><br>';
	$body .= 'You will then have a sub menu display to the left of the screen where you can select to view your previous orders.<br>';
	$body .= "Next to your order for English Schools Tickets, there will be a \"View\" button and this will display information about your order along with links for each of your tickets.<br><br>";
	$body .= 'You can print these off or if you have a smartphone, show these tickets to the person on the gate to gain entry to the event.<br><br>';
	$body .= 'Included is a direct link for users to ease access to this area.<br>';
	$body .= 'https://entry4sports.co.uk/my-account/orders<br><br><br>';
	$body .= 'Please do not reply to this email as this inbox is not monitored. If you have any issues please contact support@entry4sports.com.<br>';
	$body .= Entry4_emailFooter();
	$headers = Entry4_mailHeader('ESAA_Tickets');
	$headers[] = 'Bcc:' . implode(',', $emails);
//    $headers[] = 'Bcc:paul@entry4sports.com';
	if (sizeof($emails) < 90) {
		e4s_mail('tickets@entry4sports.com', 'How to retrieve your English School Tickets', $body, $headers, FALSE);
	}

}

function updateTicketsToPaid() {
	$sql = 'select oi.order_id,
            oi.order_item_id,
            pm.meta_value userId
            from ' . E4S_TABLE_WCORDERITEMMETA . ' oim,
                 ' . E4S_TABLE_WCORDERITEMS . ' oi,
                 ' . E4S_TABLE_POSTS . ' p,
                 ' . E4S_TABLE_POSTMETA . " pm
            where oim.meta_key='" . WC_POST_PRODUCT_ID . "'
              and oim.meta_value = '124734'
            and oi.order_item_id = oim.order_item_id
            and p.id = oi.order_id
            and p.post_status = 'wc-completed'
            and pm.post_id = p.ID
            and pm.meta_key = '_customer_user'";
	$result = e4s_queryNoLog($sql);
	if ($result->num_rows < 1) {
		Entry4UISuccess();
	}
	$userIds = array();
	while ($obj = $result->fetch_object()) {
		$userIds[] = $obj->userId;
	}
	$sql = 'select *
            from ' . E4S_TABLE_STADIUMSEATING . '
            where userid in (' . implode(',', $userIds) . ')
            and status = 0';
	$result = e4s_queryNoLog($sql);
	$updateIds = array();
	while ($obj = $result->fetch_object()) {
		$updateIds[] = $obj->id;
	}
	$sql = 'update ' . E4S_TABLE_STADIUMSEATING . '
            set status = ' . E4S_SEAT_STATUS_PAID . '
            where id in (' . implode(',', $updateIds) . ')';
	e4s_queryNoLog($sql);
	Entry4UISuccess();
}

function es_emailUnpaid() {
	$sql = '
    select user_email email
from ' . E4S_TABLE_USERS . '
where id in ( select userid from ' . E4S_TABLE_STADIUMSEATING . "
    where status = 0
    and updated < '2021-07-06')";
	$result = e4s_queryNoLog($sql);
	while ($obj = $result->fetch_object()) {
		$email = $obj->email;
		sendEsEmail($email);
	}
}

function sendEsEmail($email) {
	$name = explode('@', $email)[0];
	$body = "Dear $name,<br><br>";
	$body .= 'You still have tickets in your basket for English Schools 9th-11th July. <br>We are reminding you that they are due to expire and the seats re-allocated if they remain unpaid.<br><br>';
	$body .= 'These tickets will expire at 15:00 on 6th July.<br><br>';
	$body .= 'Please do not reply to this email as this inbox is not monitored. If you have any issues please contact support@entry4sports.com.<br>';
	$body .= Entry4_emailFooter();
	e4s_mail($email, 'English School Tickets Reminder', $body, Entry4_mailHeader(''));

}

$date = DateTime::createFromFormat('jS M Y', '9th July 2021');
echo $date->format('Y-m-d');
exit();
$obj = new stdClass();
$obj->ticketId = 13055;
$obj->orderId = 117816;
$obj->itemId = 53526;
$obj->element = 1;
$guidObj = new e4sTicketGuid($obj);
echo $guidObj->getFullGuid();
exit();

include_once E4S_FULL_PATH . 'classes/configClass.php';
//e4s_writeHealth('Something', 900);
e4s_cronHealthMonitor();
exit();

include_once E4S_FULL_PATH . 'classes/excelClass.php';
header('Content-Type: text/html; charset=utf-8');
$books = [['ISBN', 'title', 'author', 'publisher', 'ctry'], [618260307, 'The Hobbit', 'J. R. R. Tolkien', 'Houghton Mifflin', 'USA'], [908606664, 'Slinky Malinki', 'Lynley Dodd', 'Mallinson Rendel', 'NZ']];
$xlsx = excelClass::fromArray($books);
$xlsx->downloadAs('books.xlsx');
exit();

include_once E4S_FULL_PATH . 'admin/cronFunctions.php';


$entriesSql = 'select e.id eId,
                      e.clubid eClubId, 
                      athleteid athleteId,
                      schoolid schoolId
from ' . E4S_TABLE_ENTRIES . ' e,
     ' . E4S_TABLE_COMPEVENTS . ' ce,
     ' . E4S_TABLE_ATHLETE . ' a
where ce.compid = 211
and e.compeventid = ce.id
and a.id = e.athleteid';
$entries = e4s_queryNoLog($entriesSql);
while ($obj = $entries->fetch_object()) {
	if ($obj->eClubId !== $obj->schoolId) {
		$update = 'update ' . E4S_TABLE_ENTRIES . '
                   set clubid = ' . $obj->schoolId . ' 
                   where id = ' . $obj->eId;
		e4s_queryNoLog($update);
	}
}
exit();


exit();
include_once E4S_FULL_PATH . 'dbInfo.php';
$compObj = e4s_GetCompObj(177);
echo $compObj->getSecurityKey();
exit();

$obj = new deDupClass();
Entry4UISuccess();

include_once E4S_FULL_PATH . 'classes/webSocketClientClass.php';
$compObj = e4s_GetCompObj(193);
echo $compObj->getSecurityKey();
exit();

$comp = e4s_GetCompObj(193);
$compDate = $comp->getEventDate(25528);
echo $compDate;
exit();
require 'vendor/autoload.php';

use Amp\Delayed;
use Amp\Websocket;
use Amp\Websocket\Client;
use function Ratchet\Client\connect;

Amp\Loop::run(function () {
	/** @var Client\Connection $connection */
	$connection = yield Client\connect('ws://demos.kaazing.com/echo');
	yield $connection->send('Hello!');

	$i = 0;

	/** @var Websocket\Message $message */
	while ($message = yield $connection->receive()) {
		$payload = yield $message->buffer();

		printf("Received: %s\n", $payload);

		if ($payload === 'Goodbye!') {
			$connection->close();
			break;
		}

		yield new Delayed(1000);

		if ($i < 3) {
			yield $connection->send('Ping: ' . ++$i);
		} else {
			yield $connection->send('Goodbye!');
		}
	}
});
exit();
$WebSocketClient = new WebsocketClient('wss://m9f8tsofz8.execute-api.eu-west-2.amazonaws.com/Prod', 443);
echo $WebSocketClient->sendData('Paul');
unset($WebSocketClient);

exit();

include_once E4S_FULL_PATH . 'admin/cronFunctions.php';
e4s_setWaitingPosOnOpenComps();

exit();

$upTo = '2021-05-13T09:05:00+01:00';
$upTo = e4s_iso_to_sql($upTo);

echo $upTo;
echo "\n";
$now = date(E4S_MYSQL_DATE);
echo $now . "\n";

$uptoDt = strtotime($upTo);
$nowDt = strtotime($now);

if ($uptoDt < $nowDt) {
	echo 'Passed';
} else {
	echo 'Not passed';
}
exit();


$compObj = e4s_GetCompObj(351);
echo $compObj->getSecurityKey();
exit();

//fixProductCategories();
function fixProductCategories() {
	$categoryArr = [];
	$compCategoryObjs = get_terms('product_cat', array('hide_empty' => FALSE, 'fields' => 'ids'));
	foreach ($compCategoryObjs as $compCategoryObj) {
		$product_category = current(get_product_category($compCategoryObj, null));
		$categoryArr[$product_category['name']] = $product_category;
	}
//    foreach($categoryArr as $cat=>$obj){
//        var_dump($cat);
//        var_dump($obj);
//        exit();
//    }
	$sql = 'select e.*, ce.compid compid
            from ' . E4S_TABLE_ENTRIES . ' e,
                ' . E4S_TABLE_COMPEVENTS . " ce
            where e.created >= '2021-01-01'
            and e.compeventid = ce.id
            order by id desc ";
	$result = e4s_queryNoLog($sql);
	while ($obj = $result->fetch_object()) {
		if ($obj->variationID > 0) {
			$compId = $obj->compid;
			if (!array_key_exists('comp' . $compId, $categoryArr)) {
				$categoryArr['comp' . $compId] = array();
				$categoryArr['comp' . $compId]['id'] = createProductCategory($compId);
			}
			$catId = $categoryArr['comp' . $compId]['id'];
			wp_set_object_terms($obj->variationID, $catId, 'product_cat');
		}
	}
}

exit();
function createProductCategory($compId) {
	$data = array('name' => 'comp' . $compId, 'slug' => 'comp' . $compId, 'description' => 'Competition ' . $compId, 'parent' => 0, 'display' => 'default', 'image' => '',);

	try {
		$data = apply_filters('woocommerce_api_create_product_category_data', $data, null);
		$insert = wp_insert_term($data['name'], 'product_cat', $data);
		if (is_wp_error($insert)) {
			// $insert->get_error_message()
			Entry4UIError(9125, 'Product category already exists.', 200, '');
		}

		$id = $insert['term_id'];

		update_term_meta($id, 'display_type', 'default' === $data['display'] ? '' : sanitize_text_field($data['display']));

		do_action('woocommerce_api_create_product_category', $id, $data);
		return $id;

	} catch (Exception $err) {
		Entry4UIError(9126, $err->getMessage(), 200, '');
	}

}

function get_product_category($id, $fields = null) {
	try {
		$id = absint($id);

		// Validate ID
		if (empty($id)) {
			Entry4UIError(9127, 'Invalid product category id.', 200, '');
		}

		$term = get_term($id, 'product_cat');

		if (is_wp_error($term) || is_null($term)) {
			Entry4UIError(9128, "Product Category ID not found ({$id}).", 200, '');
		}

		$term_id = intval($term->term_id);

		$product_category = array('id' => $term_id, 'name' => $term->name, 'slug' => $term->slug, 'parent' => $term->parent, 'description' => $term->description, 'display' => 'default', 'image' => '', 'count' => intval($term->count),);

		return array('product_category' => apply_filters('woocommerce_api_product_category_response', $product_category, $id, $fields, $term, null));
	} catch (Exception $err) {
		Entry4UIError(9129, $err->getMessage(), 200, '');
	}
}

//var_dump($orders);
//exit();

//$customer = wp_get_current_user();
// Get all customer orders
//$customer_orders = get_posts(array(
//    'numberposts' => -1,
//    'meta_key' => '_customer_user',
//    'orderby' => 'date',
//    'order' => 'DESC',
//    'meta_value' => e4s_getUserID(),
//    'post_type' => wc_get_order_types(),
//    'post_status' => array_keys(wc_get_order_statuses()),
//    'post_status' => array('wc-completed'),
//));

//$productArr = []; //
foreach ($customer_orders as $customer_order) {
	$orderq = wc_get_order($customer_order);
	$items = $orderq->get_items();
	foreach ($items as $item) {
		$product = $item->get_product();
		$useId = $product->get_id();

		$parentId = $product->get_parent_id();
		if ($parentId !== 0) {
			$useId = $parentId;
		}
		$terms = get_the_terms($useId, 'product_cat');
		foreach ($terms as $term) {
			if (strpos($term->name, '-351') > 0) {
				$productArr[$useId] = $product;
			}
		}
	}
}
foreach ($productArr as $key => $prod) {
	var_dump($key);
}
exit();
//include_once E4S_FULL_PATH . 'pdf/fpdf.php';
//var_dump(wc_get_products(array(
//    'category' => array('comp351','test'),
//)));
//exit();
//
//$args = array(
//    'status' => 'completed',
//);
//$orders = wc_get_orders( $args );
//foreach($orders as $order){
////    echo $order->get_customer_id();
////    echo "\n";
//    echo "Order : " . $order->get_id();
//    echo "\n";
//    echo $order->get_subtotal()  ;
//    echo "\n";
//
//}
//
//exit();
//$name = $_GET['name'];
function e4s_category_exists($cat_name, $parent = null) {
	$id = term_exists($cat_name, 'product_cat', $parent);
	if (is_array($id)) {
		$id = $id['term_id'];
	}
	return $id;
}

exit();
function get_orders_ids_by_product_id($product_id, $order_status = array('wc-completed')) {
	global $wpdb;

	$results = $wpdb->get_col("
        SELECT order_items.order_id
        FROM {$wpdb->prefix}woocommerce_order_items as order_items
        LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta as order_item_meta ON order_items.order_item_id = order_item_meta.order_item_id
        LEFT JOIN {$wpdb->posts} AS posts ON order_items.order_id = posts.ID
        WHERE posts.post_type = '" . WC_POST_ORDER . "'
        AND posts.post_status IN ( '" . implode("','", $order_status) . "' )
        AND order_items.order_item_type = 'line_item'
        AND order_item_meta.meta_key = '" . WC_POST_PRODUCT_ID . "'
        AND order_item_meta.meta_value = '$product_id'
    ");

	return $results;
}

//e4s_queryNoLog("select * from order by name");
//$terms = array( 'exclude-from-catalog', 'exclude-from-search' );
//$terms = array(  );
//wp_set_object_terms( 35481, $terms, 'product_visibility' );
//echo do_shortcode('[products limit="6" columns="6" orderby="popularity" class="quick-sale" on_sale="true" ]');
exit();
//$model = new stdClass();
//$product = wc_get_product( 23379);
//$model->price = $product->get_regular_price();
//$model->salePrice = $product->get_sale_price();
//$model->saleEndDate = get_post_meta(23379, '_sale_price_dates_to', true);
//
//$iso8601 = date('c',$model->saleEndDate);
//var_dump($iso8601);
//exit();


woocommerce_mini_cart();
exit();

Entry4UISuccess('
                "data":' . json_encode($cart, JSON_NUMERIC_CHECK));

function e4s_add_product() {
	$article_name = 'SimpleProduct';

	$post_id = wp_insert_post(array('post_author' => 1, 'post_title' => $article_name, 'post_content' => 'Lorem ipsum', 'post_status' => WC_POST_PUBLISH, 'post_type' => 'product',));
	wp_set_object_terms($post_id, 'simple', 'product_type');
	update_post_meta($post_id, '_manage_stock', 'yes');
	update_post_meta($post_id, '_stock', 10);
	update_post_meta($post_id, '_regular_price', 2);
	update_post_meta($post_id, '_sale_price', 2);
	update_post_meta($post_id, '_price', 2);
	exit();
	$attr_label = 'Test attribute';
	$attr_slug = sanitize_title($attr_label);

	$attributes_array[$attr_slug] = array('name' => $attr_label, 'value' => 'alternative 1 | alternative 2', 'is_visible' => '1', 'is_variation' => '1', 'is_taxonomy' => '0' // for some reason, this is really important
	);
	update_post_meta($post_id, '_product_attributes', $attributes_array);

	$parent_id = $post_id;
	$variation = array('post_title' => $article_name . ' (variation)', 'post_content' => '', 'post_status' => WC_POST_PUBLISH, 'post_parent' => $parent_id, 'post_type' => 'product_variation');

	$variation_id = wp_insert_post($variation);
	update_post_meta($variation_id, '_regular_price', 2);
	update_post_meta($variation_id, '_sale_price', 2);
	update_post_meta($variation_id, '_price', 2);
	update_post_meta($variation_id, '_manage_stock', 'yes');
	update_post_meta($variation_id, '_stock', 10);
	update_post_meta($variation_id, '_virtual', 'no');
	update_post_meta($variation_id, '_downloadable', 'no');
	update_post_meta($variation_id, '_variation_description', 'Variation description');
	update_post_meta($variation_id, 'attribute_' . $attr_slug, 'alternative 1');
	WC_Product_Variable::sync($parent_id);
}
function importRosterData() {
	$compId = 322;
	$allCompDOBs = getAllCompDOBs($compId);
	$sql = 'select id, clubname
            from ' . E4S_TABLE_CLUBS;
	$result = e4s_queryNoLog($sql);
	$clubs = array();
	while ($obj = $result->fetch_object()) {
		$clubs[$obj->clubname] = $obj;
	}

	$sql = 'select *
            from ' . E4S_TABLE_EVENTGROUPS . '
            where compid = ' . $compId;
	$result = e4s_queryNoLog($sql);
	$egs = array();
	while ($obj = $result->fetch_object()) {
		$obj->id = (int)$obj->id;
		$egs[$obj->name] = $obj;
	}

	$sql = 'select *
            from ' . E4S_TABLE_COMPEVENTS . '
            where compid = ' . $compId;
	$result = e4s_queryNoLog($sql);
	$ces = array();
	while ($obj = $result->fetch_object()) {
		$obj->id = (int)$obj->ID;
		$ces[$obj->maxGroup . '-' . $obj->AgeGroupID] = $obj;
	}

	$sql = 'select *
            from rosterImport';
	$rosterResult = e4s_queryNoLog($sql);
	while ($rosterObj = $rosterResult->fetch_object()) {
		$importedDOB = $rosterObj->dob;
		$club = clubTranslate($rosterObj->club);
		if ($club === 'Dacorum A.C.') {
			$club = 'Dacorum and Tring A.C.';
		}
		echo 'From ' . $rosterObj->id . ' (' . $rosterObj->fullname . ' - ' . $club . ")\n";
		if (array_key_exists($club, $clubs)) {
			$clubId = (int)$clubs[$club]->id;
		} else {
			echo 'Unable to find ' . $club;
			exit;
		}

		$arr = preg_split('~/~', $importedDOB);
		$dob = $arr[2] . '-' . $arr[1] . '-' . $arr[0];
		$ag = getAgeGroupInfo($allCompDOBs, $dob);
		$ageGroupId = 0;
		$eventAgeGroup = '';
		if (array_key_exists('ageGroup', $ag)) {
			if (array_key_exists('agid', $ag['ageGroup'])) {
				$ageGroupId = $ag['ageGroup']['agid'];
				$eventAgeGroup = $ag['ageGroup']['Name'];
			}
		}
		if ($ageGroupId === 0) {
			echo $importedDOB . "\n";
			echo $dob . "\n";
			var_dump($allCompDOBs);
			exit;
		}
		$athleteObj = null;
		if (!is_null($rosterObj->urn)) {
			$athleteSql = 'select a.*
                                  ,c.clubname club
                            from ' . E4S_TABLE_ATHLETE . ' a,
                                 ' . E4S_TABLE_CLUBS . ' c
                            where urn = ' . $rosterObj->urn . '
                            and clubid = c.id';
			$athleteResult = e4s_queryNoLog($athleteSql);
			if ($athleteResult->num_rows === 1) {
				$athleteObj = $athleteResult->fetch_object();
			}
		}
		if (is_null($athleteObj)) {
			$athleteSql = 'select a.*
                                  ,c.clubname club
                            from ' . E4S_TABLE_ATHLETE . ' a,
                                 ' . E4S_TABLE_CLUBS . " c
                            where firstname = '" . $rosterObj->firstname . "'
                            and surname = '" . $rosterObj->lastname . "'
                            and dob = '" . $dob . "'
                            and clubid = c.id";
			$athleteResult = e4s_queryNoLog($athleteSql);
			if ($athleteResult->num_rows === 1) {
				$athleteObj = $athleteResult->fetch_object();
			}
		}
		$urn = $rosterObj->urn;
		if (is_null($urn)) {
			$urn = 'null';
		}
		$gender = $rosterObj->gender[0];

		if (is_null($athleteObj)) {
			$sql = 'insert into ' . E4S_TABLE_ATHLETE . "(firstname, surname, gender, dob, aocode,urn,activeenddate, clubid, type)
                    values (
                        '" . $rosterObj->firstname . "',
                        '" . $rosterObj->lastname . "',
                        '" . $gender . "',
                        '" . $dob . "',
                        'EA',
                        " . $urn . ",
                        '2022-12-31',
                        " . $clubId . ",
                        'A'
                    )";

			e4s_queryNoLog($sql);
			$athleteId = e4s_getLastID();
			echo 'Inserted : ' . $athleteId . "\n";
		} else {
			$athleteId = $athleteObj->id;
			$sql = 'update ' . E4S_TABLE_ATHLETE . "
                    set firstname = '" . $rosterObj->firstname . "',
                        surname = '" . $rosterObj->lastname . "',
                        gender = '" . $gender . "',
                        dob = '" . $dob . "',
                        urn = " . $urn . ',
                        clubid = ' . $clubId . '
                    where id = ' . $athleteId;
			e4s_queryNoLog($sql);
			echo 'Updated : ' . $athleteId . "\n";
		}
		$eventGroup = $egs[$rosterObj->eventName];
		$egId = $eventGroup->id;
		$key = $egId . '-' . $ageGroupId;
		if (!array_key_exists($key, $ces)) {
			echo "Failed to get CE:\n";
			var_dump($key);
			exit;
		}
		$ceRecord = $ces[$key];
		echo "Checking Entry\n";
		// check for entry
		$sql = 'select *
                from ' . E4S_TABLE_ENTRIES . '
                where athleteid = ' . $athleteId . '
                and compeventid = ' . $ceRecord->id;
		$result = e4s_queryNoLog($sql);
		if ($result->num_rows !== 0) {
			echo 'found ' . $sql . "\n";
		} else {
			echo "inserting entry\n";
			$sql = 'insert into ' . E4S_TABLE_ENTRIES . ' ( compeventid,athleteid,athlete, pb, variationid, clubid, orderid, paid, 
                                                            price, eventAgeGroup, ageGroupid, userid, discountid,vetagegroupid,teamid,waitingpos,
                                                             entrypos,options,present)
                    values (
                        ' . $ceRecord->id . ',
                        ' . $athleteId . ",
                        '" . $rosterObj->fullname . "',
                        '" . $rosterObj->pb . "',
                        0,
                        " . $clubId . ',
                        0,
                        ' . E4S_ENTRY_PAID . ",
                        0.00,
                        '" . $eventAgeGroup . "',
                        " . $ageGroupId . ",
                        1,
                        0,
                        0,
                        0,
                        0,
                        0,
                        '',
                        1
                    )";
			e4s_queryNoLog($sql);
		}
	}
	echo 'done';
}

function getTime($data) {
	$data = str_replace('_', '.', $data);
	$data = str_replace(':', '.', $data);
	$time = explode('.', $data);
	if (count($time) === 1) {
		return $data;
	}
	if (count($time) < 3) {
		return '-' . $data;
	}
	$calcTime = (int)$time[0] * 60;
	$calcTime += (int)$time[1];
	$ret = '-' . $calcTime . '.' . $time[2];
	var_dump($ret);
	exit();
}

//extractTest
function extractTest() {
	$x = 1;
	$y = 2;

	if ($x < $y) {
		$y = 3;
	}
	echo $x . ' is x and ' . $y . ' is y';
}

function e4s_addStandardsToResults($obj){
	$compId = checkFieldForXSS($obj, 'compid:Competition');
	$compObj = e4s_GetCompObj($compId);
	$egObjs = $compObj->getEGObjs();
	$ceObjs = $compObj->getCeObjs();
	$ceObjsByEG = array();
	foreach($ceObjs as $ceObj){
		// ESAA is 1 ce to 1 eg
		$ceObjsByEG[$ceObj->egId] = $ceObj;
	}
	$standards = $compObj->getStandards();
	$newStandards = array();
	foreach($standards as $standard=>$event){
		if ( !array_key_exists($standard, $newStandards) ){
			$newStandards[$standard] = array();
		}
		foreach($event as $ageGroups){
			foreach($ageGroups as $arr){
				foreach($arr as $std){
					$key = $std->eventId . '-' . $std->ageGroupId;
					if ( !array_key_exists($key, $newStandards) ){
						$newStandards[$standard][$key] = $std;
					}
				}
			}
		}
	}
	$egIds = array();
	foreach ($egObjs as $egObj) {
		$egIds[] = $egObj->id;
	}
	$sql = "select rh.id rhId,
       				rh.egId,
       				rd.id rdId,
       				rd.score
       		from " . E4S_TABLE_EVENTRESULTSHEADER . " rh,
       		     " . E4S_TABLE_EVENTRESULTS . " rd
			where rh.egid in (" . implode(',', $egIds) . ")
			and rh.id = rd.resultheaderid";

	$result = e4s_queryNoLog($sql);
	while($obj = $result->fetch_object()){
		$obj->rhId = (int)$obj->rhId;
		$obj->egId = (int)$obj->egId;
		$obj->rdId = (int)$obj->rdId;
		$obj->score = (float)$obj->score;
		$text = '';
		$stdScore = 0;
		$eventType = $egObjs[$obj->egId]->type;
		$ceObj = $ceObjsByEG[$obj->egId];
		$key = $ceObj->eventId . '-' . $ceObj->ageGroupId;
		if ( $obj->score < 0.01 ){
			$text = 'DNS';
		}else {
			foreach ( $newStandards as $standard => $events ) {
				if ( array_key_exists( $key, $events ) ) {
					$std = $events[ $key ];
					if ( $eventType === E4S_EVENT_TRACK ) {
						if ( $obj->score <= $std->value and ($stdScore === 0 or $obj->score < $stdScore)) {
							$text = $standard;
							$stdScore = $std->value;
						}
					} else {
						if ( $obj->score >= $std->value and ($stdScore === 0 or $obj->score > $stdScore)) {
							$text = $standard;
							$stdScore = $std->value;
						}
					}

				}
			}
		}
		if ( $text !== '' ){
			$sql = 'update ' . E4S_TABLE_EVENTRESULTS . "
					set scoreText = '" . $text . "'
					where id = " . $obj->rdId;
			e4s_queryNoLog($sql);
		}
	}
}