<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<?php
define('R4S_SOCKET_ANNOUNCEMENT', 'announcement');
define('R4S_SOCKET_ADD_ATHLETE', 'athlete-add');
define('R4S_SOCKET_ATHLETE_PRESENT', 'athlete-present');
define('R4S_SOCKET_ATHLETE_RESULT', 'athlete-result');
define('R4S_SOCKET_FIELD_SEEDINGS', 'field-seedings');
?>
<script>
    function processSocketEvent(data) {
        if (data.key !== getEventNoSelected()) {
            // ignore result as not currently showing
            return;
        }
        switch (data.action) {
            case "<?php echo R4S_SOCKET_FIELD_RESULT ?>":
                inboundAthleteResult(data.payload);
                break;
            case "<?php echo R4S_SOCKET_ATHLETE_PRESENT ?>":
                inboundAthletePresent(data.payload);
                break;
            case "<?php echo R4S_SOCKET_ADD_ATHLETE ?>":
                inboundAddAthlete(data.payload);
                break;
            case "<?php echo R4S_SOCKET_ANNOUNCEMENT ?>":
                r4s_announcement(data.payload);
                break;
            case "<?php echo R4S_SOCKET_FIELD_SEEDINGS ?>":
                inboundFieldSeeding(data.payload);
                break;
        }
    }

    function sendSocketMsg(r4sAction, payload) {
        if (payload === null) {
            return;
        }
        var socketObj = {};
        socketObj.action = "sendmessage";
        socketObj.data = {};
        socketObj.data.key = getEventNoSelected();
        socketObj.data.action = r4sAction;
        socketObj.data.deviceKey = r4sGlobal.deviceKey;
        socketObj.data.securityKey = r4sGlobal.securityKey;
        socketObj.data.payload = JSON.stringify(payload);

        if (r4sGlobal.socket.readyState !== WebSocket.OPEN) {
            alert("Socket Closed. Attempting to reopen");
            initSockets(r4sGlobal.socketListen);
        }
        r4sGlobal.socket.send(JSON.stringify(socketObj));
    }

    function initSockets(listen, endpoint) {
        if (typeof endpoint === "undefined") {
            endpoint = 'wss://mfjctn4hg3.execute-api.eu-west-2.amazonaws.com/Prod';
        }
        r4sGlobal.socket = new WebSocket(endpoint);

        r4sGlobal.socket.addEventListener('open', function () {
            console.log("Connection established, handle with event");
        });

        r4sGlobal.socket.onopen = function () {
            console.log("Connection established, handle with function");
        };
        r4sGlobal.socketListen = listen;
        if (listen) {
            r4sGlobal.socket.addEventListener('message', function (m) {
                console.log('message recieved :' + m);

                var socketPayload = JSON.parse(m.data).data;
                socketPayload.payload = JSON.parse(socketPayload.payload);
                processSocketEvent(socketPayload);
            });
        }
    }

    function sendAnnouncement(text) {
        var payload = {};
        payload.text = text;
        sendSocketMsg("announcement", payload)
    }

    function getEventNoSelected() {
        return 0;
    }

    var r4sGlobal = {};
    initSockets(false);
</script>

<body>
<button type="button" onclick="sendAnnouncement('Can athlete 286 please report to the 100 metre start'); return false;">
    Call 286
</button>
<br>
<button type="button" onclick="sendAnnouncement('Can Jared report to the ECC please'); return false;">
    Request Jared
</button>
<br>
<button type="button" onclick="sendAnnouncement('Can the field referee please report to the ECC'); return false;">
    Field Referee
</button>
</body>
