<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_OTD_PATH . '/tfUiCommon.php';
include_once E4S_FULL_PATH . 'classes/uploadTrackInfo.php';
include_once E4S_FULL_PATH . 'classes/uploadTrackInfo.php';
include_once E4S_FULL_PATH . 'results/results.php';
header('Content-Type: text/html; charset=utf-8');
?>

    <!--<!DOCTYPE html>-->
    <!--<html>-->
    <!--<head>-->
    <script>
        function sendTrackMessage(uploadObj) {
            let socketObj = {};
            let comp = {"id": uploadObj.comp};
            socketObj.action = "sendmessage";
            socketObj.data = {};
            socketObj.data.key = uploadObj.key;
            socketObj.data.domain = "<?php echo E4S_CURRENT_DOMAIN ?>";
            if (uploadObj.outputNo > 0) {
                socketObj.data.outputNo = uploadObj.outputNo;
            }
            socketObj.data.comp = comp;
            socketObj.data.action = '<?php echo R4S_SOCKET_PF ?>';
            socketObj.data.deviceKey = "c46e3a380ebd7e6e053c6468a38b6ce4";
            socketObj.data.securityKey = "2865c8526e264cfd672aeca95c0f09e7";
            let payload = {};

            payload.picture = uploadObj.image;
            payload.comp = comp;
            payload.eventNo = uploadObj.eventNo;
            payload.heatNo = uploadObj.heatNo;
            payload.eventName = uploadObj.eventName;
            payload.scheduleTime = uploadObj.scheduleTime;
            payload.results = uploadObj.results;
            socketObj.data.payload = JSON.stringify(payload);
            // socketObj.data.payload = payload;
            socketObj.data = JSON.stringify(socketObj.data);
            let sendSocketObj = JSON.stringify(socketObj);
            if (r4sGlobal.socket.readyState !== WebSocket.OPEN) {
                // alert("Socket Closed. Attempting to reopen");
                initSockets(false, sendSocketObj);
            } else {
                r4sGlobal.socket.send(sendSocketObj);
            }
        }
    </script>
<?php
include_once E4S_OTD_PATH . '/tfSockets.php';
?>
    <!--</head>-->
<?php
function e4s_requestTrackInfo($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID');
    $outputNo = checkFieldForXSS($obj, 'outputno:Output Number');
    $uploadObj = new uploadTrackInfo($compId, $outputNo);
    echo $uploadObj->html();
    exit();
}

function e4s_uploadTrackFile($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID');
    $outputNo = checkFieldForXSS($obj, 'outputno:Output Number');
    $uploadObj = new uploadTrackInfo($compId, $outputNo);
    $imgPath = $uploadObj->upload();
    echo $uploadObj->html();
    if ($imgPath !== '') {
//        var_dump();
        ?>
        <script>
            var response = <?php echo e4s_getDataAsType(e4s_ReturnResultsForEvent($compId, $uploadObj->eventNo, $uploadObj->heatNo, TRUE), E4S_OPTIONS_STRING) ?>;
            sendTrackInfoToSocket(<?php echo $uploadObj->eventNo ?>,<?php echo $uploadObj->heatNo ?>,<?php echo $outputNo ?>, '<?php echo $imgPath ?>', response);
        </script>
        <?php
    }
    exit();
//    Entry4UISuccess();
}