<?php
require_once E4S_FULL_PATH . 'classes/socketClass.php';

function e4s_receiveLynx($obj){
	include E4S_FULL_PATH . '/dbInfo.php';
	if ( array_key_exists('compid', $_GET) ) {
		$compId = (int)$_GET['compid'];
	} else {
		$compId = -1;
	}
	$prop = accessProtected($obj,'body');
	$log = e4s_getDataAsType($prop, E4S_OPTIONS_OBJECT);
	if (is_array($log) || is_object($log)) {
		e4s_stdLog(print_r($log, true));
	} else {
		e4s_stdLog( $log );
	}
	e4s_sendSocketInfo($compId, $log, R4S_SOCKET_LYNX, false);
	Entry4UISuccess();
}
function e4s_stdLog($msg){
	$outputFile = '/var/www/vhosts/dev.entry4sports.co.uk/httpdocs/wp-content/lynx.log';
//	$newline = "\n";
	// set $newline to char(13) and char(10) for windows
	$newline = chr(13) . chr(10);

	$date = date('Y-m-d H:i:s');
	error_log($date . " : " . $msg. $newline, 3, $outputFile);
}