<?php
define('E4S_CARD_RESULTS_DIV', 'cardResults');
define('E4S_CARD_ATTEMPT_DIV', 'cardAttempt');
function hasScoreboardAccess() {
    $compId = $GLOBALS['R4S_COMP_ID'];
    if (array_key_exists(R4S_DEVICE_CODE, $GLOBALS)) {
        $deviceCode = $GLOBALS[R4S_DEVICE_CODE];
        $day = (int)date('d');
        $month = (int)date('m');
        $checkCode = $compId + ($day * $month);

        if ($deviceCode === $checkCode) {
            return TRUE;
        }
    }
    if (!e4s_isUserLoggedIn()) {
        return FALSE;
    }

    $hasPerm = userHasPermission(PERM_SCOREBOARD, null, $compId);
    return $hasPerm;
}

function hasSecureAccess() {
    if (!e4s_isUserLoggedIn()) {
        return FALSE;
    }

    return userHasPermission(PERM_ADMIN, null, $GLOBALS['R4S_COMP_ID']);
}

function e4s_isUserLoggedIn() {
    $userid = e4s_getUserID();
    if ($userid === E4S_USER_NOT_LOGGED_IN) {
        return FALSE;
    }

    return TRUE;
}

function e4s_SetStartHeight($obj) {
    $entryId = checkFieldForXSS($obj, 'entryid:Entry ID');
    $startHeight = checkFieldForXSS($obj, 'startheight:start Height');
    $obj = new stdClass();
    $obj->entryId = $entryId;
    $obj->startHeight = $startHeight;
    $entryObj = new tfEntriesClass($obj);
    $entryObj->updateStartHeight();
}