<?php

class tfCardsClass {
    public $compObj;
    public $ageGroups;
    public $compEvents;
    public $compEventRows;
    public $eventGroups;
    public $config;
    public $seedings;

    public function __construct($compid) {
        $this->config = e4s_getConfig();
        $this->compObj = e4s_GetCompObj($compid);
        $this->compEvents = null;
        $this->compEventRows = null;
        $this->eventGroups = null;
        $this->ageGroups = null;
        $this->seedings = null;
    }

    function getBaseInfo() {
        $retObj = new stdClass();
        $retObj->competition = $this->getCompetitionInfo();
        $retObj->ageGroups = $this->getAgeGroups();
        $retObj->eventGroups = $this->getEventGroups();
        $retObj->compEvents = $this->getCompEvents();
        Entry4UISuccess($retObj);
    }

    public function getCompetitionInfo() {
        $compObj = new stdClass();
        $compObj->id = $this->compObj->getID();
        $compObj->name = $this->compObj->getName();
        $compObj->date = $this->compObj->getDate();
        $compObj->organiser = $this->compObj->getOrganisationName();
        $compObj->location = $this->compObj->getLocationName();
        $coptions = $this->compObj->getOptions();
//        var_dump($coptions);
        $options = new stdClass();
        $options->checkIn = $coptions->checkIn;
        $options->heatOrder = $coptions->heatOrder;
        $options->laneCount = $coptions->laneCount;
        $options->timetable = $coptions->timetable;
        $compObj->options = $options;
        return $compObj;
    }

    /**
     * @return array
     */
    public function getAgeGroups() {
        if (is_null($this->ageGroups)) {
            $this->_setAgeGroups();
            if (is_null($this->ageGroups)) {
                Entry4UIError(7510, 'No age groups found');
            }
        }

        return $this->ageGroups;
    }

    private function _setAgeGroups() {
        $ao = $this->config['defaultao']['code'];

        $sql = 'SELECT id id,
                       Name name,
                       options options
        FROM ' . E4S_TABLE_AGEGROUPS . "
        where options like '%aocode\":\"" . $ao . "\"%'
        order by minage";

        $result = e4s_queryNoLog($sql);

        $thisCompId = $this->compObj->getID();
        $this->ageGroups = array();
        while ($row = $result->fetch_assoc()) {

            $options = e4s_getDataAsType($row['options'], E4S_OPTIONS_OBJECT);
            $add = TRUE;
            if (isset($options->compids)) {
                $add = FALSE;
                foreach ($options->compids as $compid) {
                    if ($compid === $thisCompId) {
                        $add = TRUE;
                        break;
                    }
                }
            }
            if ($add) {
                $ageObj = new stdClass();
                $ageObj->id = (int)$row['id'];
                $ageObj->name = $row['name'];
                $this->ageGroups[$ageObj->id] = $ageObj;
            }
        }
    }

    /**
     * @return array
     */
    public function getEventGroups() {
        if (is_null($this->eventGroups)) {
            $this->_setEventGroups();
            if (is_null($this->eventGroups)) {
                Entry4UIError(7530, 'No Event groups found');
            }
        }

        return $this->eventGroups;
    }

    private function _setEventGroups(): void {
        $types = $this->_getEventGroupTypes();
        $compId = $this->compObj->getID();
        $sql = "select id id,
                       eventno eventNo,
                       name name,
                       startdate startDate,
                       ifnull(typeNo,'') typeNo,
                       options options
                from " . E4S_TABLE_EVENTGROUPS . '
                where compid = ' . $compId . '
                order by eventno';
        $result = e4s_queryNoLog($sql);
        $egArr = array();
        while ($row = $result->fetch_assoc()) {
//            var_dump($types [(int)$row['id']] );
//            if ( $types [(int)$row['id']] !== "T" ) {
            if (array_key_exists((int)$row['id'], $types)) {
                $egObj = new stdClass();
                $egObj->egId = (int)$row['id'];
                $egObj->egName = $row['name'];
                $egObj->type = $types [$egObj->egId];
                $egObj->typeno = $row['typeNo'];
                $egObj->seeded = $this->isEventSeeded($egObj->egId);
                $egObj->egNo = (int)$row['eventNo'];
                $egObj->egStartDate = $row['startDate'];
                $egObj->egOptions = e4s_getDataAsType($row['options'], E4S_OPTIONS_OBJECT);
                if (!isset($egObj->egOptions->increment)) {
                    if ($egObj->type === E4S_UOM_HEIGHT) {
                        $egObj->egOptions->increment = 5;
                        if (strpos($egObj->egName, 'pole') !== FALSE) {
                            $egObj->egOptions->increment = 10;
                        }
                    }
                }
                $egArr[$egObj->egId] = $egObj;
            }
        }

        $this->eventGroups = $egArr;
    }

    private function _getEventGroupTypes(): array {
        if (is_null($this->compEventRows)) {
            $this->_setCompEventRows();
        }
        $ceRows = $this->compEventRows;

        // get list of eventids by egroup
        $eIdsByEgId = array();
        foreach ($ceRows as $ceRow) {
//            var_dump($ceRow);
            $eId = (int)$ceRow['eventId'];
            $egId = (int)$ceRow['eventGroupId'];
            $eIdsByEgId [$egId] = $eId;
        }
        $events = $this->_getEvents($eIdsByEgId);
//        echo "<Br>Events<br>";
//        var_dump($events);
        $uoms = $this->_getUOMRecords();
        $egTypes = array();

        foreach ($eIdsByEgId as $egId => $eId) {
            $event = $events[$egId];
            $uomId = $event->uomId;
            $eUom = $uoms[$uomId];
            $egTypes[$egId] = $eUom->type;
        }
        return $egTypes;
    }

    private function _setCompEventRows(): void {
        $compId = $this->compObj->getID();

        $sql = 'select  ce.id ceId,
                        eventid eventId,
                        maxgroup eventGroupId, 
                        agegroupid ageGroupId,
                        ce.options options,
                        e.name eventName,
                        e.tf tf,    
                        e.gender gender
       
        from ' . E4S_TABLE_COMPEVENTS . ' ce,
             ' . E4S_TABLE_EVENTS . ' e,
             ' . E4S_TABLE_EVENTGROUPS . ' eg
        where ce.compid = ' . $compId . "
        and   ce.eventid = e.id
        and   eg.compid = ce.compid
        and tf != 'T'";

        $result = e4s_queryNoLog($sql);
        $this->compEventRows = $result->fetch_all(MYSQLI_ASSOC);
    }

    private function _getEvents($eIdsByEgId): array {
        if (sizeof($eIdsByEgId) === 0) {
            return $eIdsByEgId;
        }
        $sql = 'select id id,
                       uomid uomid,
                       tf tf
                from ' . E4S_TABLE_EVENTS . '
                where id in (' . implode(',', $eIdsByEgId) . ')';
        $result = e4s_queryNoLog($sql);
        $retArr = array();
        while ($row = $result->fetch_assoc()) {
            $eId = (int)$row['id'];
//            echo "<br>eIdsByEgId<br>";
//            var_dump($eIdsByEgId);
            foreach ($eIdsByEgId as $egId => $item) {
                if ($item === $eId) {
                    $obj = new stdClass();
                    $obj->eventId = (int)$row['id'];
                    $obj->uomId = (int)$row['uomid'];
                    $obj->tf = $row['tf'];
                    $retArr[$egId] = $obj;
                }
            }
        }
        return $retArr;
    }

    private function _getUOMRecords(): array {
        $sql = 'select id id,
                       uomtype uomType,
                       uomoptions uomOptions
                from ' . E4S_TABLE_UOM;
        $result = e4s_queryNoLog($sql);
        $objArr = array();
        while ($row = $result->fetch_assoc()) {
            $obj = new stdClass();
            $obj->id = (int)$row['id'];
            $obj->type = $row['uomType'];
            $obj->options = $row['uomOptions'];
            $objArr[$obj->id] = $obj;
        }
        return $objArr;
    }

    public function isEventSeeded($eventGroupId) {
        if (is_null($this->seedings)) {
            $this->getSeedings($eventGroupId);
        }
    }

    public function getSeedings($eventGroupId) {
        $egObj = new seedingClass($eventGroupId);
        $this->seedings = $egObj->getEventSeedings();
        return $this->seedings;
    }

    /**
     * @return array
     */
    public function getCompEvents() {
        // Need these to be set
        $this->getAgeGroups();
        $this->getEventGroups();

        if (is_null($this->compEvents)) {
            $this->_setCompEvents();
            if (is_null($this->compEvents)) {
                Entry4UIError(7510, 'No Events found');
            }
        }

        return $this->compEvents;
    }

    private function _setCompEvents(): void {
        if (is_null($this->compEventRows)) {
            $this->_setCompEventRows();
        }
        $rows = $this->compEventRows;
        $retArr = array();
        $egs = $this->getEventGroups();
        foreach ($rows as $row) {
            $ceId = (int)$row['ceId'];
            $rowObj = new stdClass();
            $rowObj->ceId = $ceId;
            $rowObj->tf = $row['tf'];
            $egs[(int)$row['eventGroupId']]->tf = $rowObj->tf;
            $rowObj->gender = $row['gender'];
            $ageObj = new stdClass();
            $ageObj->ageId = (int)$row['ageGroupId'];
            $ageObj->name = '';
            if (array_key_exists($ageObj->ageId, $this->ageGroups)) {
                $ageObj->name = $this->ageGroups[$ageObj->ageId]->name;
            }
            $rowObj->ageGroup = $ageObj;
            $eventObj = new stdClass();
            $eventObj->eventId = (int)$row['eventId'];
            $eventObj->eventGroup = $egs[(int)$row['eventGroupId']];
            $eventObj->eventName = $row['eventName'];
            $rowObj->event = $eventObj;
            $retArr[$ceId] = $rowObj;
        }
        $this->eventGroups = $egs;
        $this->compEvents = $retArr;
    }
}