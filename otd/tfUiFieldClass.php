<?php
include_once E4S_OTD_PATH . '/tfUiDistanceClass.php';
include_once E4S_OTD_PATH . '/tfUiHeightClass.php';
include_once E4S_OTD_PATH . '/tfFieldResults.php';

class tfUiFieldClass {
    public $compId;
    public $tfUiDistanceObj;
    public $tfUiHeightObj;
    public $tfFieldResultsObj;

    public function __construct($compId) {
        $this->compId = $compId;
        $this->tfUiDistanceObj = new tfUiDistanceClass($compId);
        $this->tfUiHeightObj = new tfUiHeightClass($compId);
        $this->tfFieldResultsObj = new tfFieldResultsClass();
    }

    public function css($echo = FALSE) {
        $css = '<style>
        .pulse {
          cursor: pointer;
          box-shadow: 0 0 0 rgba(22,22,22, 0.6);
          animation: pulse 2s infinite;
        }
        @-webkit-keyframes pulse {
          0% {
            -webkit-box-shadow: 0 0 0 0 rgba(22,22,22, 0.6);
          }
          70% {
              -webkit-box-shadow: 0 0 0 10px rgba(22,22,22, 0);
          }
          100% {
              -webkit-box-shadow: 0 0 0 0 rgba(22,22,22, 0);
          }
        }
        @keyframes pulse {
          0% {
            -moz-box-shadow: 0 0 0 0 rgba(22,22,22, 0.6);
            box-shadow: 0 0 0 0 rgba(22,22,22, 0.6);
          }
          70% {
              -moz-box-shadow: 0 0 0 10px rgba(22,22,22, 0);
              box-shadow: 0 0 0 10px rgba(22,22,22, 0);
          }
          100% {
              -moz-box-shadow: 0 0 0 0 rgba(22,22,22, 0);
              box-shadow: 0 0 0 0 rgba(22,22,22, 0);
          }
        }
        .card_field_judge_row{
            height: 25px;
        }
        .field_info_row {
            border:1px solid;
        }
        .field_info_row{
            height: 60px;
            vertical-align: top;
            text-align: left;
            border: 2px solid black;
            font-style: italic;
            font-size: small;
            padding: 2px;
            background-image: url(/resources/entry4sports-small.gif);
            background-size: 80px 40px;
            background-position: bottom left;
            background-repeat: no-repeat;
        }
        
        .field_data {
            font-size: 0.8vw;
            font-weight: 500;
        }
        .field_data_large {
            font-size: 1vw;
            font-weight: 500;
        }
        </style>';
        if ($echo) {
            echo $css;
        }
        return $css;
    }

    public function javascript() {
        $this->tfUiDistanceObj->javascript();
        $this->tfUiHeightObj->javascript();
        $this->tfFieldResultsObj->javascript();
        ?>
        <script>
            function getAthleteRowId(entry) {
                var eventGroup = getEventGroup(entry.eventGroup.eventNo);
                var row = entry.athlete.id;
                if (eventGroup.type === '<?php echo E4S_UOM_DISTANCE ?>') {
                    row = entry.heatInfo.position;
                }
                return row;
            }

            function getFieldCardFooterHTML() {
                return '<?php
                    echo $this->generateFieldFooter();
                    ?>';
            }

            function populateFieldCard(eventGroup) {
                getEntriesForEvent(eventGroup, displayAthletesForFieldEvent);
            }

            function displayAthletesForFieldEvent(entries, eventGroup) {
                setCurrentEntries(entries);
                var fieldType = eventGroup.type;
                var seeded = isEventSeeded();
                // get Event Header Here
                if (getObjLength(entries) > 0) {
                    populateCardEventInfo(eventGroup);
                }
                if (!seeded) {
                    // No seeding
                    displayNonSeededEventEntries();
                } else {
                    if (fieldType === '<?php echo E4S_UOM_HEIGHT ?>') {
                        buildAndPopulateHeightCard();
                    } else {
                        setDisplayedCardHTML(getDistanceCardHTML());
                        loadFieldDistanceEntries();
                    }
                    setDisplayedCardFooterHTML(getFieldCardFooterHTML());
                    <?php if (hasScoreboardAccess() ) { ?>
                    bindSignatures();
                    <?php } ?>
                    showSignatures();
                    if (fieldType === '<?php echo E4S_UOM_DISTANCE ?>') {
                        evaluateFieldPositions();
                    }
                }
            }

            function loadFieldEntries() {
                var currentEvent = getCurrentEvent();
                var fieldType = currentEvent.type;
                if (fieldType === '<?php echo E4S_UOM_HEIGHT ?>') {
                    buildAndPopulateHeightCard();
                } else {
                    loadFieldDistanceEntries();
                    evaluateFieldPositions();
                }
            }

            function getPresetClass() {
                return "cardPresent";
            }

            function getNotPresetClass() {
                return "cardNotPresent";
            }

            function markUIFieldRowPresent(obj, present) {
                var presentClass = getPresetClass();
                var notPresentClass = getNotPresetClass();
                var entryRow = obj.closest("tr");
                if (present) {
                    entryRow.removeClass(notPresentClass);
                    entryRow.addClass(presentClass);
                } else {
                    entryRow.addClass(notPresentClass);
                    entryRow.removeClass(presentClass);
                }
            }

            function clearFieldInfo(row) {
                var bibSelect = "#field_bib_" + row;
                var ageSelect = "#field_age_" + row;
                var genderSelect = "#field_gender_" + row;
                var athleteSelect = "#field_name_" + row;
                var clubSelect = "#field_club_" + row;
                $(bibSelect).text("");
                $(ageSelect).text("");
                $(genderSelect).text("");
                $(athleteSelect).text("");
                $(clubSelect).text("");
                updateUIFieldResults([], row);
            }

            function setFieldInfo(entry) {
                var gender = "Male";
                var athlete = entry.athlete;
                if (athlete.gender.toUpperCase() === "F") {
                    gender = "Female";
                }

                var useHeatNo = entry.heatInfo.heatNo;
                var position = entry.heatInfo.position;

                var bibSelect = "#field_bib_" + position;
                var ageSelect = "#field_age_" + position;
                var genderSelect = "#field_gender_" + position;
                var athleteSelect = "#field_name_" + position;
                var clubSelect = "#field_club_" + position;

                markUIFieldRowPresent($(bibSelect), entry.entry.present);

                $(bibSelect).text(getBibNo(entry.entry.bibNo, entry.entry));
                $(genderSelect).text(gender);
                var ageGroup = entry.ageGroup.eventAgeGroup;
                if (ageGroup === "") {
                    ageGroup = entry.ageGroup.ageGroup;
                }
                $(ageSelect).text(ageGroup);
                $(athleteSelect).html(getAthleteNameForCard(entry));

                $(clubSelect).text(athlete.club);

                updateFieldSheetWithResults(entry);
            }

            function updateFieldSheetWithResults(entry) {
                var results = entry.results;
                var rowNo = entry.heatInfo.position;
                if (typeof results['data'] !== "undefined") {
                    updateUIFieldResults(results['data'], rowNo);
                }
            }

            function updateUIFieldResults(results, rowNo) {
                for (var trial = 1; trial < 10; trial++) {
                    var key = "t" + trial;
                    var fullkey = "field_" + key + "m_" + rowNo;
                    var exists = $("#" + fullkey).length > 0;
                    if (exists) {
                        if (typeof results[key] !== "undefined") {
                            var curVal = results[key];

                            if (!isValValidOption(curVal)) {
                                var curValF = parseFloat(curVal);
                                if (isNaN(curValF)) {
                                    curVal = "";
                                }
                                setFieldResultOnCard(key, rowNo, curVal, false);
                            } else {
                                setFieldOptionOnCard(key, rowNo, curVal, "");
                            }
                        } else {
                            setFieldOptionOnCard(key, rowNo, "", "");
                        }
                    } else {
                        trial = 11;
                        break;
                    }
                }

                setBestScores("t", rowNo);
            }

            function resetAthleteHighScore(entry) {
                entry.results.highScore = 0;
                entry.results.isPb = false;
                entry.results.isSb = false;
                entry.results.scoreText = "";
            }

            function evaluateFieldPositions() {
                var entries = getCurrentEntries(false);
                var sortedRoundScores = [];
                // get an array of scores by trial
                var maxTrialsArr = [];
                var highScores = [];
                var splitScores = [];
                for (const e in entries) {
                    var entry = entries[e];
                    var athleteKey = entry.athlete.id;
                    resetAthleteHighScore(entry);
                    if (typeof entry.results !== "undefined" && typeof entry.results.data !== "undefined") {
                        for (let r in entry.results.data) {
                            maxTrialsArr[r] = r;
                            if (typeof sortedRoundScores[r] === "undefined") {
                                sortedRoundScores[r] = [];
                            }
                            let result = entry.results.data[r];
                            if (typeof result === "string") {
                                result = 0;
                            }
                            sortedRoundScores[r][result] = {"athleteId": athleteKey, "score": result};
                            if (typeof highScores[athleteKey] === "undefined") {
                                highScores[athleteKey] = {"athleteId": athleteKey, "scores": []};
                            }
                            highScores[athleteKey].scores.push(result);

                            if (typeof splitScores[athleteKey] === "undefined") {
                                splitScores[athleteKey] = {"athleteId": athleteKey, "scores": []};
                            }
                            if (r === 't1' || r === 't2' || r === 't3') {
                                splitScores[athleteKey].scores.push(result);
                            }
                        }
                        entry.results.highScore = 0;
                        if (typeof splitScores[athleteKey] !== "undefined") {
                            splitScores[athleteKey].scores.sort(function (a, b) {
                                return b - a
                            });
                        }
                        if (typeof highScores[athleteKey] !== "undefined") {
                            highScores[athleteKey].scores.sort(function (a, b) {
                                return b - a
                            });

                            entry.results.highScore = highScores[athleteKey].scores[0];
                        }
                    }
                }

                highScores.sort(function (a, b) {
                    return b.scores[0] - a.scores[0]
                });
                splitScores.sort(function (a, b) {
                    return b.scores[0] - a.scores[0]
                });

                updatePositions(splitScores, highScores);
                let positions = updateExtraResultInfo(entries);
                return positions;
            }

            function updateExtraResultInfo(entries) {
                let positions = [];
                for (const e in entries) {
                    var entry = entries[e];
                    var athleteKey = entry.athlete.id;
                    entry.results.isPb = false;
                    entry.results.isSb = false;
                    entry.results.scoreText = "";
                    if (entry.results.highScore > 0) {
                        if (entry.results.highScore >= entry.athlete.sb) {
                            entry.results.isSb = true;
                            entry.results.scoreText = "(sb)";
                            entry.athlete.sb = entry.results.highScore;
                        }
                        if (entry.results.highScore >= entry.athlete.pb) {
                            entry.results.isPb = true;
                            entry.results.scoreText = "(pb)";
                            entry.athlete.pb = entry.results.highScore;
                        }
                    }
                    let entryPosition = {};
                    entryPosition.athleteId = entry.athlete.id;
                    entryPosition.athlete = entry.athlete.name;
                    entryPosition.bibNo = entry.entry.bibNo;
                    entryPosition.teambibno = getBibNo(entry.entry.bibNo, entry.entry);
                    entryPosition.position = entry.results.currentPosition;
                    entryPosition.score = entry.results.highScore;
                    entryPosition.scoreText = entry.results.scoreText;
                    entryPosition.wind = "";
                    entryPosition.clubName = entry.athlete.club;
                    entryPosition.laneNo = 0;
                    positions.push(entryPosition);
                }
                return positions;
            }

            function updatePositions(splitScores, highScores) {
                setPosition(splitScores, "3");
                setPosition(highScores, "");
                for (let r = 1; r < 25; r++) {
                    let hs = $("#field_b3m_" + r).text();
                    if (hs === "") {
                        $("#field_3pos_" + r).html("&nbsp;");
                        // $("#field_pos_" + r).html("&nbsp;");
                    }
                    hs = $("#field_b6m_" + r).text();
                    if (hs === "") {
                        $("#field_pos_" + r).html("&nbsp;");
                    }
                }
            }

            function sortIncludingCountBack(data) {
                let maxPower = 1;
                for (let c in data) {
                    let scores = data[c].scores;
                    if (scores.length > maxPower) {
                        maxPower = scores.length;
                    }
                }
                for (let e in data) {
                    let powerOf = maxPower;
                    // let powerOf = scores.length;
                    let scores = data[e].scores;
                    let total = 0;
                    for (let s = 0; s < scores.length; s++) {
                        let score = scores[s];
                        if (score > 0) {
                            total += Math.pow(score, powerOf);
                        }
                        powerOf--;
                    }
                    data[e].total = total;
                }
                data.sort(function (a, b) {
                    return b.total - a.total
                });
                return data;
            }

            function setPosition(posData, labelPos) {
                posData = sortIncludingCountBack(posData);
                var lastScore = 0;
                var lastPosition = 0;
                for (t in posData) {
                    var position = parseInt(t) + 1;
                    var usePosition = position;
                    var data = posData[t];
                    var curEntry = getCurEntry(data.athleteId);
                    curEntry.results.highScore = data.scores[0];
                    if (data.scores[0] === lastScore) {
                        usePosition = lastPosition;
                        lastScore = data.highScore;
                    } else {
                        usePosition = position;
                        lastPosition = position;
                        lastScore = data.score;
                    }
                    if (curEntry.results.highScore > 0) {
                        // $("#field_" + labelPos + "pos_" + curEntry.heatInfo.position).text(usePosition);
                        checkObjDiffHTML($("#field_" + labelPos + "pos_" + curEntry.heatInfo.position), usePosition);
                    } else {
                        // $("#field_" + labelPos + "pos_" + curEntry.heatInfo.position).text("-");
                        checkObjDiffHTML($("#field_" + labelPos + "pos_" + curEntry.heatInfo.position), "-");
                    }

                    curEntry.results.currentPosition = usePosition;
                }
            }


            function addPulseToObj(obj) {
                if (typeof r4sGlobal.inboundMessage === "undefined") {
                    return;
                }
                if (r4sGlobal.inboundMessage) {
                    obj.addClass("pulse");
                    setTimeout(function () {
                        obj.removeClass("pulse")
                    }, 500);
                }
            }

            function checkObjDiffText(obj, newValue) {
                if (obj.text() !== newValue) {
                    obj.text(newValue);
                    addPulseToObj(obj);
                }
            }

            function checkObjDiffHTML(obj, newValue) {
                if (obj.html() !== "" + newValue) {
                    obj.html(newValue);
                    addPulseToObj(obj);
                }
            }

            function setFieldOptionOnCard(key, row, curVal, curVal2) {
                checkObjDiffText($("#field_" + key + "m_" + row), curVal);
                checkObjDiffText($("#field_" + key + "c_" + row), curVal2);
                // $("#field_3pos_" + row).html("&nbsp;");
                // $("#field_pos_" + row).html("&nbsp;");
            }
        </script>
        <?php
    }

    public function generateFieldFooter() {
        $idName = R4S_SIGNATURE;
        $html = '<table class="card_table" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="field_info_row" colspan="22">';
        $html .= '<span id="field_info">Information</span>';
        $html .= '</td>';
        $html .= '</tr>';

        $html .= '<tr class="card_field_judge_row">';
        $html .= '<td class="card_judges_label border_top_strong ' . $idName . '" colspan="2">';
        $html .= '<div>Officials</div>';
        if (hasScoreboardAccess()) {
            $html .= '<div style="font-size: 0.7em;">Click official Number to enter details</div>';
        }
        $html .= '</td>';
        $html .= '<td class="card_judges_count_label signature-background border_top_strong ' . $idName . '" signature=1 >';
        $html .= '<span>1 : </span>';
        $html .= '<span id="' . $idName . '1"></span>';
        $html .= '</td>';
        $html .= '<td class="card_judges_count_label signature-background border_top_strong ' . $idName . '" signature=2>';
        $html .= '<span>2 : </span>';
        $html .= '<span id="' . $idName . '2"></span>';
        $html .= '</td>';
        $html .= '<td class="card_judges_count_label signature-background border_top_strong ' . $idName . '" signature=3 colspan="9">';
        $html .= '<span>3 : </span>';
        $html .= '<span id="' . $idName . '3"></span>';
        $html .= '</td>';
        $html .= '<td class="card_judges_count_label signature-background border_top_strong ' . $idName . '" signature=10 colspan="9">';
        $html .= '<span>Ref : </span>';
        $html .= '<span id="' . $idName . '0"></span>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="card_link_text" colspan="22">';
        $html .= '<span class="card_link_text_span">Entry4Sports Field Card. For more information https://entry4sports.co.uk</span>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';

        return $html;
    }
}