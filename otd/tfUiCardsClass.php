<?php
include_once E4S_OTD_PATH . '/tfUiFieldClass.php';
include_once E4S_OTD_PATH . '/tfUiTrackClass.php';
include_once E4S_OTD_PATH . '/uiSignature.php';
define('R4S_CARD_PRINT', 'r4s_card_to_print');
define('R4S_EVENT_HEADER', 'event-header');
define('R4S_EVENT_HEADER_ROW2', 'event-header-row2');
define('R4S_CARD_HEADER', 'card-header');
define('R4S_CARD_BODY', 'card_body');
define('R4S_CARD_FOOTER', 'card-footer');
define('R4S_CARD_EMPTY', 'card-empty');

class tfUiCardsClass
{
    public $compId;
    public $config;
    public $tfCardObj;
    public $tfUiFieldObj;
    public $tfUiTrackObj;
    public $announcementObj;
    public $signatureObj;
    public function __construct($compId){
        $this->compId = $compId;
        $this->tfCardObj = new tfCardsClass($compId);
        $this->tfUiFieldObj = new tfUiFieldClass($compId);
        $this->tfUiTrackObj = new tfUiTrackClass($compId);
        $this->signatureObj = new uiSignature($compId);
        $this->announcementObj = new tfAnnouncementsClass($compId);
        $this->init();
    }
    private function init(){
        $config = e4s_getConfig();
        $config = e4s_getDataAsType($config, E4S_OPTIONS_OBJECT);
        $this->config = $config;
        $this->generateHeader();
        $this->displayBody();
        $this->displayFooter();
        $this->jsInit();

    }

    public function generateHeader(){
        $this->getHTMLHeader();
        ?>
            <body>
        <?php
    }

    public function getHTMLHeader(){
        if (isset($cfgoptions->uiTheme)) {
            $uiTheme = $cfgoptions->uiTheme;
        }
        header('Content-Type: text/html; charset=utf-8');
        ?>
        <html>
            <head>
                <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/<?php echo E4S_JQUERY_THEME ?>/jquery-ui.css" />
<!--                <link rel="stylesheet" href="--><?php //echo E4S_PATH ?><!--/css/theme-default.css" />-->
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
                <script type="text/javascript" src="<?php echo E4S_PATH ?>/js/jquery-dateformat.min.js"></script>
                <script type="text/javascript" src="<?php echo E4S_PATH ?>/js/touch-dnd.js"></script>
            <?php
                $this->pageCSS(TRUE);
                $this->javascript();
            ?>
            </head>
        <?php
    }

    public function pageCSS( $echo = TRUE){
        $css = '
        <style>
        .ui-state-default.ui-sortable-handle {
            touch-action:auto;
            appearance: auto;
        }
        @media print
        {    
            .no-print, .no-print *
            {
                display: none !important;
            }
        }
        .navbar {
          overflow: hidden;
          background-color: #333;
          font-family: Arial, Helvetica, sans-serif;
        }

        .navbar a {
          float: left;
          font-size: 16px;
          color: white;
          text-align: center;
          padding: 14px 16px;
          text-decoration: none;
        }
        
        .dropdown {
          float: left;
          overflow: hidden;
        }

        .dropdown .dropbtn {
          cursor: pointer;
          font-size: 16px;  
          border: none;
          outline: none;
          color: white;
          padding: 14px 16px;
          background-color: inherit;
          font-family: inherit;
          margin: 0;
        }
        
        .navbar a:hover, .dropdown:hover .dropbtn, .dropbtn:focus {
          background-color: red;
        }
        
        .dropdown-content {
            display: none;
            position: absolute;
            color: #2e6e9e;
            background-color: #dfeffc;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
            right: 12px;
        }
        
        .dropdown-content a {
          float: none;
          color: black;
          padding: 12px 16px;
          text-decoration: none;
          display: block;
          text-align: left;
        }
        
        .dropdown-content a:hover {
          background-color: #ddd;
        }
        
        .show {
          display: block;
        }
        .center-screen {
            display: inline;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            text-align: center;
            position: absolute;
            top: 50%;
            left: 50%;
        }
        .loader {
            position: absolute;
            width: 80px;
            margin: 100px auto;
        }
        .addAthleteErrors{
            color:red;
            font-weight:800;
            font-size: 14px;
        }
        .duo {
            height: 20px;
            width: 50px;
            background: hsla(0, 0%, 0%, 0.0);
            position: absolute;
        }
        .duo, .dot {
            animation-duration: 0.8s;
            animation-timing-function: ease-in-out;
            animation-iteration-count: infinite;
        }
        .duo1 {
            left: 0;
        }
        .duo2 {
            left: 30px
        }
        .dot {
            width: 20px;
            height: 20px;
            border-radius: 10px;
            background: #333;
            position: absolute;
        }
        .dot-a {
            left: 0px;  
        }
        .dot-b {
            right: 0px;
        }
        @keyframes spin {
            0% { transform: rotate(0deg) }
            50% { transform: rotate(180deg) }
            100% { transform: rotate(180deg) }
        }
        @keyframes onOff {
            0% { opacity: 0; }
            49% { opacity: 0; }
            50% { opacity: 1; }
            100% { opacity: 1; }
        }
        .duo1 {
            animation-name: spin;
        }
        .duo2 {
            animation-name: spin;
            animation-direction: reverse;
        }
        .duo2 .dot-b {
            animation-name: onOff;
        }  
        .duo1 .dot-a {
            opacity: 0;
            animation-name: onOff;
            animation-direction: reverse;
        }
        #card_title {
            float:right;
            padding-right: 10px;
        }
        .e4s_header_label {
            width:10%;
            font-size: 20px;
        
        }
        .e4s_header_data {
            width:40%;
            font-size: 20px;
            font-weight: 700;
        }
        .e4s_summary_table {
            text-align: center;
        }
        .e4s_summary_gender {
            font-size: 16px;
        } 
        .e4s_summary_label {
            background-color: #dfeffc;
            color: #2e6e9e;
        }
        .e4s_summary_table {
            border-collapse: collapse;
        }
        .e4s_summary_table td {
            border: 1px solid black;
        }
        .e4s_total_data {
            font-weight: bold;
            font-style: oblique; 
            background-color: lightcyan;
        }
        .e4s_total_label {
            font-weight: bold;
            background-color: #dfeffc;
            color: #2e6e9e;
        }
        .e4s_grand_totals {
            border-top-style: double !important;
            border-top-width: 4px !important;
        }
        .event_selector {
            padding-bottom: 10px;
        }
        .event_selector {
            float: left;
        }
        
        .entries_page {
//            position: absolute;
//            top: 73px;
//            left: 335px;
              float: left;
        }
        
        .entries_confirmed {
//            position: absolute;
//            left: 455px;
//            top: 73px;
              float: left;
        }
        .card_table {
            border-collapse: collapse;
            width: 100%;
        }
        .card_header {
            background-color: lightgrey !important;
        }
        .card_header_e4s {
            font-weight: 900;
        }
        .card_header_label {
            
        }
        .card_header_data {
            font-weight: 700;
        }
        .card_header_gap {
            line-height: 2px;
            background-color: white;
        }
        .card_menu_bar {
            height: 40px;
        }
        .card_action_menu {
//            position: absolute;
//            top: 75px;
//            left: 880px;
//            padding-right: 60px;
            float: right;
        }

        .card_data {
            text-align: center;
        } 
        .card_data_left {
            text-align: left;
            padding-left: 5px;
        }
        .card_bib {
            width: 30px;
        }
        .card_age {
            width: 60px;
        }
        .card_gender {
            width: 60px;
        }
        .card_name {
        }
        .card_club {
        }

        .trialResultsTd{
            
        }
        .trialAltRow {
            background-color: lightyellow;
        }
        .trialFieldCol{
            border-bottom: 1px solid black;
            min-width: 205px;
        }
        .trialAthleteCol {
            border-bottom: 1px solid black;
            font-size: 0.85vw;
            width: 150px;
        }
        .trialBibCol{
            border-bottom: 1px solid black;
            font-weight: 600;
            width: 40px;
            font-size: 1em;
        }
        .overTable {
            border-spacing: 0;
        }
        .overOddRow {
            background-color: #eee;
        }
        .overHeat {
            font-size: large;
            font-weight: 800;
        }
        .overPos{
            width: 50px;
        }
        .overName{
            width: 250px;
        }
        .overGender{
            width: 50px;
        }
        .overBib{
            width: 50px;
        }
        .overClub{
            width: 350px;
        }
        .overAge{
            width: 100px;
        }
        .overPB{
            text-align: right;
            width: 50px;
        }
        
       
        .floatLeft {
            float: left;
        }
        .floatRight {
            float: right;
        }
        .clearSB {
            position: absolute;
            left: 20px;
        }
        .resultButton {
            font-size: 0.6em !important;
        }
        .resultShow {
            font-size: 0.6em !important;
            padding: 7px;
        }
        .resultClear {
            font-size: 0.6em !important;
            width: 150px;
        }
        .border_top_strong {
            border-top: 2px solid !important;
        }
        .border_right_strong {
            border-right: 2px solid !important;
        }
        .border_left_strong {
            border-left: 2px solid !important;
        }
        .border_bottom_strong {
            border-bottom: 2px solid !important;
        }       
        .card_judges_label {
            text-align: center;
            vertical-align: middle;
            font-size: 0.6vw;
            border: 1px solid;
            width: 30px;
        }       
        .card_judges_count_label {
            vertical-align: top;
            text-align: left;
            font-size: 0.6vw;
            border: 1px solid;
        }   
        .card_judges_area {
            border: 1px solid;
            height: 30px;
        }
        .card_table_header {
            background-color: lightgrey;
            border:1px solid;
        }
        .track_lane_header{
        
        }
        .track_data_grid {
            border: 1px solid black;
        }
        .track_lane_header_label {
            width:12%;
            text-align: center;
            border: 1px solid;
        }
        .track_heatno_label {
        }
        .track_heat_labels {
        }
        .track_heat_divider {
            border-bottom: 5px double black;
        }
        .track_bibno_label {
            font-size: small;
        } 
        .track_bibno_data {
            width: 40px;
            text-align: center;
            font-weight: 600;
        }
        .track_age_label {
        }
        .track_age_data {
            width: 60px;
            font-size: .8vw;
            text-align: center;
        }
        .track_athlete_label {
            font-size: small;
        }
        .track_athlete_data_text{
            max-width: 120px;
        }
        .track_athlete_data {
            font-size: 0.8vw;
            font-family: inherit;
            font-weight: 600;
            text-align: center;
        }
        .athletepb {
            font-size:0.70vw;
        }
        .track_club_label {
            font-size: small;
        }            
        .track_club_data {
            text-align: center;
            font-size: .75vw;
        }
        .track_info_row {
            height: 80px;
            vertical-align: top;
            text-align: left;
            border: 2px solid black;
            font-style: italic;
            font-size: small;
            padding: 2px;
            background-image: url(/resources/entry4sports-small.gif);
            background-size: 80px 40px;
            background-position: bottom left;
            background-repeat: no-repeat;
        }
        .card_link_text {
            text-align: center;
            font-weight: 500;
            font-size: 0.6rem;
            
        }
        .card_link_text_span {
//            position: relative;
            top: -3px;
            background-color: white;
            padding: 0px 10px 3px 10px;
        }
        ';
        $css .= seedingV2Class::css();
        $css .= '</style>';
        $css .= $this->tfUiFieldObj->css(FALSE);
        $css .= $this->tfUiFieldObj->tfUiDistanceObj->css(FALSE);
        $css .= $this->tfUiFieldObj->tfUiHeightObj->css(FALSE);
        $css .= $this->signatureObj->css(FALSE);
        if ( $echo ){
            echo $css;
        }
        return $css;
    }

    public function javascript(){
        include_once E4S_OTD_PATH . '/tfSockets.php';
        include_once E4S_OTD_PATH . '/tfFieldResults.php';
        seedingV2Class::javascript();
        $this->announcementObj->javascript();
        $this->tfUiFieldObj->javascript();
        $this->tfUiTrackObj->javascript();
        $this->signatureObj->javascript();
        if ( hasScoreboardAccess() ){
            ?>
        <script>
            function cardResults(trialOrHeight){
                var curEvent = getCurrentEvent();
                switch (curEvent.tf){
                    case '<?php echo E4S_EVENT_FIELD ?>':
                        switch (curEvent.type){
                            case '<?php echo E4S_UOM_DISTANCE ?>':
                                trialDistanceResults(trialOrHeight);
                                break;
                            case '<?php echo E4S_UOM_HEIGHT ?>':
                                trialHeightResults();
                                break;
                        }
                        break;
                    case '<?php E4S_EVENT_TRACK  ?>':
                        break;
                }
            }
            function cardActions() {
                // document.getElementById("cardActionMenu").classList.toggle("show");
                $("#cardActionMenu").toggle();
            }
            function showEventActions(show){
                if ( show === true) {
                    $("#cardActions").show();
                    // $("#seeding").show();
                } else {
                    $("#cardActions").hide();
                    // $("#seeding").hide();
                }
            }
            window.onclick = function(e) {
                if (!e.target.matches('.cardAction')) {
                    $("#cardActionMenu").hide();
                }
            };
            function cardPrint(){
                var eventno = getEventNoSelected();
                if ( isNaN(eventno) ){
                    alert("Please select an event");
                    return;
                }
                window.print();
                //var cardTableHTML = $("#<?php //echo R4S_CARD_PRINT ?>//").html();
                //var cssHtml = '<?php //$this->pageCSS(false) ?>//';
                //cardPrintLaunch("" + cssHtml + cardTableHTML);
            }

            function cardPrintLaunch(sourceHTML){
                var newWin = window.open("", "", "width=1200, height=700"),
                    doc = newWin.document.open();
                doc.write(sourceHTML);
                doc.close();
                newWin.print();
            }
            function setAthletesAndLoadDialog(athletes){
                setAthletes(athletes);
                showAddAthleteDialog();
            }
            function addAthleteDialogByBib() {
                var athletes = getAthletes();
                if ( typeof athletes === "undefined") {
                    loadAthletes(setAthletesAndLoadDialog);
                } else {
                    showAddAthleteDialog();
                }
            }
            function showAddAthleteDialog(){
                var html='';

                html += "Enter Bib Number : <input style='width:90px' id='addBibNo'>";
                html += "<div>";
                html += "<span id='addAthleteInfo'></span>";
                html += "</div>";

                var parentAddDiv = $("#addAthleteParentDiv");
                var addDiv = $("#addAthleteDiv");
                addDiv.html(html);

                parentAddDiv.dialog({
                    title: "Add Athlete to " ,
                    resizable: false,
                    height: 360,
                    width: 400,
                    modal: true
                });
                $("#addBibNo").on("keyup" , function(e){
                    var athletes = getAthletes();
                    var bibNo = parseInt($("#addBibNo").val());
                    displayAthleteToAdd();
                    if (bibNo > 0) {
                        for (var x in athletes) {
                            if (athletes[x].bibNo === bibNo){
                                displayAthleteToAdd(athletes[x]);
                            }
                        }
                    }
                });
                parentAddDiv.show();
                parentAddDiv.dialog("option", "buttons", {
                    "Add": function() {
                        addAthleteToEvent();
                    },
                    Close: function() {
                        $( this ).dialog( "close" );
                    }
                });
            }
            function addAthleteToEvent(){
                var athleteId = parseInt($("#athleteIdToAdd").val());
                var entries = getCurrentEntries();
                if ( typeof entries[athleteId] !== "undefined"){
                    return;
                }
                var ageGroupId = parseInt($("#ageGroupIdToAdd").val());
                var ageGroup = $("#ageGroupToAdd").val();
                var bibNo = parseInt($("#bibNoToAdd").val());
                var eventNo = getEventNoSelected();
                var eventGroupId = getEventGroup(eventNo).egId;
                var payload = {
                    "athleteId":athleteId,
                    "bibNo":bibNo,
                    "eventGroupId":eventGroupId,
                    "ageGroupId":ageGroupId,
                    "ageGroup":ageGroup
                };
                function addAthleteDisplayError(text){
                    $("#addAthleteErrors").text( text );
                }

                function addAthleteToComp(payload,callBack, failureCallBack){
                    var action = '<?php echo E4S_POST ?>';
                    var endPoint = '<?php echo E4S_ROUTE_PATH ?>/event/addAthlete';
                    postData(endPoint,payload, callBack, failureCallBack);
                }
                function addAthleteError(response){
                    addAthleteDisplayError(response.error);
                }
                // Add to database
                addAthleteToComp(payload, sendSocketAthleteAdded, addAthleteError);
            }
            function displayAthleteToAdd(athlete){
                var html = "";
                if ( typeof athlete !== "undefined" ){
                    html += "<br>";
                    var style = "border: 1px solid lightgray;border-collapse: collapse;";
                    html += "<table style='width:100%; " + style + "'>";
                    html += "<tr>";
                    html += "<td style='width:100px;" + style + "'>Athlete</td><td style='" + style + "'>" + athlete.firstName + " " + athlete.surName + "</>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td style='" + style + "'>Gender</td><td style='" + style + "'>" + (athlete.gender === "M" ? "Male" : "Female") + "</>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td style='" + style + "'>Age Group</><td style='" + style + "'>" + athlete.ageGroup + "</>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td style='" + style + "'>Club</><td style='" + style + "'>" + athlete.clubName + "</>";
                    html += "</tr>";
                    html += "</table>";
                    html += "<br><div id='addAthleteErrors' class='addAthleteErrors'>";
                    var entries = getCurrentEntries();
                    if ( typeof entries[athlete.athleteId] !== "undefined"){
                        html += "Athlete already in event.";
                    }
                    html += "</div>";
                    html += "<input readonly style='display:none;' id='athleteIdToAdd' value='" + athlete.athleteId + "'>";
                    html += "<input readonly style='display:none;' id='ageGroupIdToAdd' value='" + athlete.ageGroupId + "'>";
                    html += "<input readonly style='display:none;' id='ageGroupToAdd' value='" + athlete.ageGroup + "'>";
                    html += "<input readonly style='display:none;' id='bibNoToAdd' value='" + athlete.bibNo + "'>";
                }
                $("#addAthleteInfo").html(html);
            }
            function sendSocketAthleteAdded(payload, eventGroup, origPayload){
                // if ( payload.errNo === 0) {
                //     addAthleteDisplayError("");
                // }else{
                //     addAthleteDisplayError(payload.error);
                // }
                sendSocketMsg("<?php echo R4S_SOCKET_ADD_ATHLETE ?>",payload);
            }
            function postData(endPoint, payload, callBack, failureCallBack, obj){
                tfAjaxData('<?php echo E4S_POST ?>', payload, endPoint, callBack,failureCallBack,null, obj);
            }
            </script>
            <?php
        }
        ?>
            <script>
                var validOptions = [getResultClear() + ":Clear",getResultPass() +":Pass",getResultRetired() + ":Retire",getResultVoid()+ ":Void"];
                function getResultRetired(){
                    return "r";
                }
                function getResultClear(){
                    return "";
                }
                function getResultPass(){
                    return "-";
                }
                function getResultVoid(){
                    return "x";
                }
                function getResultPresent(){
                    return "seedPresent_";
                }
                function getResultSavedValue(){
                    return "e4sVal";
                }
                function getResultAthleteIdAttr(){
                    return "athleteid";
                }
                function getResultSubAttr(){
                    return "e4ssub";
                }
                function getResultTrialAttr(){
                    return "e4strial";
                }
                function getResultPrefixAttr(){
                    return "e4sprefix";
                }
                function getBibNo(bibno, entry){
                    if ( entry.teambibno !== 0 ){
                        return entry.teambibno;
                    }
                    if ( bibno === 0 ){
                        return '<?php echo E4S_NO_BIBNO ?>'
                    }
                    return bibno;
                }
                function getResultAthleteField(prefix, trial, element){
                    return prefix + element + "Athlete";
                }
                // Close the dropdown if the user clicks outside of it
                Object.size = function(obj) {
                    var size = 0,
                        key;
                    for (key in obj) {
                        if (obj.hasOwnProperty(key)) size++;
                    }
                    return size;
                };

                function isValValidOption(val){
                    for(var v=0; v < validOptions.length; v++){
                        var validOption = validOptions[v].split(":")[0];
                        if (validOption === val){
                            return true;
                        }
                    }
                    return false;
                }

                function getAthleteNameForCard(entry){
                    var athlete = entry.athlete;
                    retval = athlete.name;
                    if ( entry.entry.pb !== 0 && entry.entry.pb !== 9999) {
                        retval += "<span class='athletepb'> (PB:" + entry.entry.pb + ")</span>";
                    }
                    return retval;
                }

                function cardChange(){
                    var eventno = getEventNoSelected();
                    <?php
                    if (hasScoreboardAccess() ){
                    ?>
                    showEventActions(eventno > 0);
                    <?php
                    }
                    ?>
                    if ( eventno > 0 ) {
                        populateCard(eventno);
                        displayHeaderRow2(true);
                    }else{
                        populateCardHeader();
                        displayEmptyCard();
                    }
                }
                function displayHeader(show, row2){
                    var header = $("#<?php echo R4S_EVENT_HEADER ?>");
                    if ( show ){
                        header.show();
                        $("#card_title").hide();
                    } else {
                        header.hide();
                    }
                    if ( typeof row2 === "undefined" ){
                        row2 = false;
                    }
                    displayHeaderRow2(row2);
                }
                function displayHeaderRow2(show){
                    var header = $("#<?php echo R4S_EVENT_HEADER_ROW2 ?>");
                    if ( show ){
                        header.show();
                        $("#card_title").show();
                    } else {
                        header.hide();
                    }
                }
                function displayEmptyCard(){
                    displayHeader(true,false);
                    setDisplayedCardDiv('<?php echo R4S_CARD_EMPTY ?>');
                    setDisplayedCardFooterHTML("");
                }
                function setDisplayedCardDiv(cardDiv){
                    var html = $("#" + cardDiv).html();
                    setDisplayedCardHTML(html);
                }
                function setDisplayedCardHTML(html){
                    $("#<?php echo R4S_CARD_BODY ?>").html (html);
                }
                function setDisplayedCardFooterDiv(div){
                    var html = $("#" + div).html();
                    setDisplayedCardFooterHTML(html);
                }
                function setDisplayedCardFooterHTML(html){
                    $("#<?php echo R4S_CARD_FOOTER ?>").html(html).show();
                }
                function hideCardFooter (){
                    $("#<?php echo R4S_CARD_FOOTER ?>").hide();
                }
                function getEventTF(eventNo){
                    if ( eventNo === 0 ) {
                        eventNo = getEventNoSelected();
                    }
                    if (isNaN(eventNo) ){
                        // Nothing selected
                        return "";
                    }
                    var eventGroup = getEventGroup(eventNo);
                    if ( eventGroup === false){
                        return "";
                    }

                    return eventGroup.tf;
                }

                function getEventGroup(eventNo){
                    var eventGroups = getEventGroups();
                    var eventGroup = null;
                    for(var eg in eventGroups){
                        if (eventGroups[eg].egNo === eventNo){
                            eventGroup = eventGroups[eg];
                            break;
                        }
                    }
                    if ( eventGroup === null){
                        // raise an error
                        alert( "Error retrieving Track event");
                        return false;
                    }
                    return eventGroup;
                }

                function populateCard(eventNo){
                    var eventTf = getEventTF(eventNo);
                    if ( eventTf === "" ){
                        return;
                    }
                    var eventGroup = getEventGroup(eventNo);
                    if ( eventGroup === false){
                        return;
                    }

                    // sendSocketMsg("eventChange", eventGroup);
                    switch ( eventTf ){
                        case '<?php echo E4S_EVENT_TRACK ?>':
                            populateTrackCard(eventGroup);
                            break;
                        case '<?php echo E4S_EVENT_FIELD ?>':
                            displayFieldCard(eventGroup);
                            break;
                    }
                }
                function populateCardEventInfo( eventGroup ){
                    var cardType = "Track";
                    if ( eventGroup.tf === "<?php echo E4S_EVENT_FIELD ?>"){
                        cardType = "Field";
                    }
                    $("#card_title").text("E4S " + cardType + " Card");
                    var dateStr = formatDate(eventGroup.egStartDate,'D MMM yyyy HH:mm');
                    dateStr = dateStr.replace("00:00","");
                    $("#card_date").text( dateStr );
                    $("#card_event").text(eventGroup.egName);
                    if ( eventGroup.typeno !== "" ) {
                        $("#card_eventno").text(eventGroup.typeno + " / " + eventGroup.egNo);
                    } else {
                        $("#card_eventno").text(eventGroup.egNo);
                    }
                    // $("#card_eventno").text(eventGroup.egNo);
                    $("#card_trials").text(eventGroup.egOptions.trialInfo);
                }
                function populateCardHeader(){
                    var competition = getCompetition();

                    $("#card_competition").text(competition.name);
                    $("#card_date").text( formatDate(competition.date + ' 00:00','D MMM yyyy') );
                    $("#card_venue").text(competition.location);

                    displayHeader(true, false);
                }
                function formatDate(date, format){
                    /*
        yy = short year
        yyyy = long year
        M = month (1-12)
        MM = month (01-12)
        MMM = month abbreviation (Jan, Feb ... Dec)
        MMMM = long month (January, February ... December)
        d = day (1 - 31)
        dd = day (01 - 31)
        ddd = day of the week in words (Monday, Tuesday ... Sunday)
        E = short day of the week in words (Mon, Tue ... Sun)
        D - Ordinal day (1st, 2nd, 3rd, 21st, 22nd, 23rd, 31st, 4th...)
        h = hour in am/pm (0-12)
        hh = hour in am/pm (00-12)
        H = hour in day (0-23)
        HH = hour in day (00-23)
        mm = minute
        ss = second

        SSS = milliseconds
        a = AM/PM marker
        p = a.m./p.m. marker
                     */
                    return $.format.date(date,format);
                }
                function getEntriesForEvent(eventGroup, callBack){
                    var competition = getCompetition();
                    var endPoint = '/wp-json/e4s/v5/public/uicard/entries/' + competition.id + '/' + eventGroup.tf + '/' + eventGroup.egId;
                    getData(endPoint, callBack, null, eventGroup);
                }

                function getData(endPoint, callBack, failureCallBack, eventGroup) {
                    tfAjaxData('<?php echo E4S_GET ?>', {}, endPoint, callBack,failureCallBack, eventGroup);
                }

                function tfAjaxData(action, payload, endPoint, callBack, failureCallBack, eventGroup, extraObj){
                    r4sGlobal.payload = payload;
                    showPleaseWait(true);
                    let obj = {
                        type: action,
                        url: endPoint,
                        data: payload,
                        cache: false,
                        error: function( jqXHR, textStatus, errorThrown ){
                            showPleaseWait(false);
                            alert("An error has occurred or the connection to E4S has failed (Error Returned : " + textStatus + "). Please check your internet connection and re-send the data to retry.");
                        },
                        success: function( response ) {
                            showPleaseWait(false);
                            if ( response.errNo === 0 ){
                                if ( callBack ){
                                    var payload = response.data;
                                    if ( typeof payload === "undefined"){
                                        payload = r4sGlobal.payload;
                                    }

                                    callBack(payload, eventGroup, r4sGlobal.payload);
                                    r4sGlobal.payload = null;
                                }
                            }else{
                                if ( failureCallBack ){
                                    failureCallBack(response);
                                } else {
                                    alert(response.error);
                                }
                            }
                        }
                    };
                    let mergedObj = obj;
                    if ( typeof extraObj === "object"){
                        mergedObj = {...obj, ...extraObj};
                    }

                    $.ajax( mergedObj );
                }

                function getEventNoSelected(){
                    var eventno = parseInt($("#event_selector :selected").val());
                    return eventno;
                }
                function showPleaseWait(status) {
                    if ( status ) {
                        $("#pleaseWait").show();
                    } else {
                        $("#pleaseWait").hide();
                    }
                }

                function getCompetition(){

                }
                function getCompEvents(){

                }
                function getEventGroups(){

                }
                // Socket Events
                function inboundAthletePresent(payload){
                    var athleteId = payload.athlete.id;

                    if ( typeof athleteId === "undefined" ){
                        alert("Error : Failed to retrieve athlete id from inbound message.");
                        return;
                    }
                    var curEntry = getCurEntry(athleteId);
                    if ( curEntry === false ){
                        alert("Error getting athletes entry");
                        return;
                    }
                    curEntry = updateCurEntryPresent(curEntry, payload);

                    markUICardPresent(curEntry);
                }
                function markUICardPresent(entry){
                    let row = getAthleteRowId(entry);
                    markUIFieldRowPresent($("#field_bib_" + row), entry.entry.present);
                }
                function inboundAthleteResult(payload){
                    if ( payload === null ){
                        alert("No Payload");
                        return;
                    }
                    // setCurrentEntries(payload.entries);
                    updateAthletesInboundScore(payload);
                    loadFieldEntries();
                }
                function updateCurEntryPresent(curEntry, newEntry){
                    if ( typeof newEntry.entry.present === "undefined" ){
                        newEntry.entry.present = false;
                    }

                    updateCurrentEntry(newEntry);
                    return newEntry;
                }
                function getCurEntry(athleteId){
                    let curEntries = getCurrentEntries();
                    athleteId = parseInt(athleteId);

                    let curEntry = curEntries[athleteId];
                    if ( typeof curEntry === "undefined" ){
                        return false;
                    }
                    if ( typeof curEntry.results === "undefined"){
                        curEntry.results = {};
                    }
                    if ( typeof curEntry.results.data === "undefined"){
                        curEntry.results.data = {};
                    }
                    return curEntry;
                }
                function updateCurrentEntry(entry){
                    r4sGlobal.currentEntries[entry.athlete.id] = entry;
                }
                function updateAthletesInboundScore(payload){
                    let athleteId = payload.score.athlete.id;
                    let score = payload.score.score;
                    let trial = payload.score.trial;
                    let curEntry = r4sGlobal.currentEntries[athleteId];
                    curEntry.results.data['t' + trial] = score;
                    r4sGlobal.heightObj = null;
                }
                function setCurrentEntries(entries){
                    r4sGlobal.currentEntries = entries;
                    r4sGlobal.heightObj = null;
                }
                function getCurrentEntries(sorted){
                    var entries = r4sGlobal.currentEntries;
                    var sortedEntries = {};
                    if ( typeof sorted === "undefined") {
                        return entries;
                    }

                    for (var e in entries ){
                        var entry = entries[e];
                        var pos = entry.heatInfo.position;
                        while (typeof sortedEntries[pos] !== "undefined") {
                            pos++;
                        }
                        sortedEntries[pos] = entry;
                    }
                    return sortedEntries;
                }
                function getCurrentEvent(){
                    return getEventGroup( getEventNoSelected());
                }
                function getAthletes(){
                    return r4sGlobal.athletes;
                }
                function setAthletes(athletes){
                    r4sGlobal.athletes = athletes;
                }
                function loadAthletes(callBack){
                    var competition = getCompetition();
                    var endPoint = '<?php echo E4S_ROUTE_PATH ?>/competition/' + competition.id + '/athletes';
                    getData(endPoint, callBack);
                }

                function inboundAddAthlete(entries){
                    // for(const e in entries){
                    //     var entry = entries[e];
                    //     r4sGlobal.currentEntries[entry.athlete.id] = entry;
                    // }
                    r4sGlobal.currentEntries = entries;
                    loadFieldEntries();
                }

                function getDecimals(val, numOfDecimals){
                    if ( typeof val === "undefined" ){
                        return "";
                    }
                    return parseFloat(val).toFixed(numOfDecimals);
                }
                function getFloatDecimals(val, numOfDecimals){
                    val = getDecimals(val, numOfDecimals);
                    return parseFloat(val);
                }
                function getObjLength(obj){
                    let len = 0;
                    for ( let o in obj ){
                        len++;
                    }
                    return len;
                }
            </script>
        <?php
    }

    public function getCompetitionInfo(){
        return $this->tfCardObj->getCompetitionInfo();
    }

    public function getCompEvents(){
        return $this->tfCardObj->getCompEvents();
    }

    public function getEventGroups(){
        return $this->tfCardObj->getEventGroups();
    }

    public function displayBody(){
        $this->pleaseWaitDiv();
        seedingV2Class::div();
        tfFieldResultsClass::div();
        $compInfo = $this->getCompetitionInfo();
        tfUiTrackClass::div($compInfo->options->laneCount);
        $this->announcementObj->div();
        $this->displayAddAthlete();
        $this->displayTabs();
        $this->signatureObj->html();
    }

    public function pleaseWaitDiv(){
        ?>
        <div id="pleaseWait" class="center-screen">
            <div class="loader">
                <div class="duo duo1">
                    <div class="dot dot-a"></div>
                    <div class="dot dot-b"></div>
                </div>
                <div class="duo duo2">
                    <div class="dot dot-a"></div>
                    <div class="dot dot-b"></div>
                </div>
            </div>

            Loading.. Please wait.....
        </div>
        <?php
    }

    public function displayAddAthlete(){
        ?>
            <div id="addAthleteParentDiv" style="display:none;">
                <span class="fieldSeedHelp">
                Enter here the Bib number of the athlete you are adding to this event.
                </span>
                <div id="addAthleteDiv"></div>
            </div>
        <?php
    }

    public function displayTabs(){
        ?>
            <div id="tabs" style="display:none;">
                <ul>
                    <li><a href="#Card-tab">Track/Field Cards</a></li>
                </ul>
            </div>
        <div id="Card-tab" class="no-print">
            <div id="cardMenuBar" class="card_menu_bar pq-grid-top ui-widget-header  ui-corner-top pq-toolbar">
                <div id="event_selectorDIV" class="event_selector">
                    Choose Event :
                    <select id="event_selector" onchange="cardChange();">
                        <?php echo $this->getEventSelectorHTML() ?>
                    </select>
                </div>

                <div id="entries_pageDIV" style="display:none;" class="entries_page">
                    Page :
                    <select id="entries_page" onchange="cardChange();">
                        <option value=1>1</option>
                    </select>
                </div>
                <?php

                if ( hasSecureAccess() or hasScoreboardAccess() ) {
                    ?>

                    <div id="cardActions" class="card_action_menu" style="display:none;">
                        <div class="dropdown">
                            <button class="cardAction ui-button ui-corner-all ui-widget" onclick="cardActions()">Actions
                                <i class="fa fa-caret-down"></i>
                            </button>
                            <div class="dropdown-content" id="cardActionMenu">
                                <a href="#" onclick="cardPrint(); return false;">Print</a>
                                <a href="#" onclick="addAthleteDialogByBib(); return false;">Add Athlete</a>
                                <a href="#" onclick="showSeedingDialog(); return false;">Seeding</a>
                                <a href="#" onclick="cardResults(1); return false;">Results</a>
                                <a href="#" onclick="makeACardAnnouncement(); return false;">Announcements</a>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

            </div>
        </div>
<div id="<?php echo R4S_CARD_PRINT ?>">
        <div id="<?php echo R4S_EVENT_HEADER ?>" style="display: none;">
            <table class='card_table card_header' cellpadding='0' cellspacing='0'>
                <tr>
                    <td>
                        <span class='card_header_label'>Meeting : </span>
                        <span  id='card_competition' class='card_header_data'></span>
                    </td>

                    <td>
                        <span class='card_header_label'>Date : </span>
                        <span id='card_date' class='card_header_data'></span>
                       </td>
                    <td>
                        <span class='card_header_label'>Venue : </span>
                        <span id='card_venue' class='card_header_data'></span>
                    </td>
                    <td>
                        <span class='card_header_label card_header_e4s' id='card_title'></span>
                    </td>
                </tr>
                <tr id="<?php echo R4S_EVENT_HEADER_ROW2 ?>">
                    <td>
                        <span class='card_header_label'>Event : </span>
                        <span  id='card_event' class='card_header_data'></span>
                    </td>
                    <td>
                        <span class='card_header_label'>Event Number : </span>
                        <span  id='card_eventno' class='card_header_data'></span>
                    </td>
                    <td colspan='2'>
                        <span id="card_header_trial" style="display: none;">
                            <span class='card_header_label'>Trials</span>
                            <span id='card_trials' class='card_header_data'></span>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class='card_header_gap' colspan='6'>
                        <span>&nbsp</span>
                    </td>
                </tr>
            </table>
        </div>
        <div id="<?php echo R4S_CARD_BODY ?>"></div>
        <div id="<?php echo R4S_CARD_EMPTY ?>" style="display: none;">
            <b>Please select an event</b>
        </div>
        <?php
    }

    public function getEventSelectorHTML(){
        $eventGroups =  $this->getEventGroups();
        $html = '';
        $html .= "<option value=''>";
        $html .= 'Select an Event';
        $html .= '</option>';
        usort($eventGroups, function($a, $b){
            if ( $a->typeno !== '') {
                $aType = substr($a->typeno, 0, 1);
                $aNumber = substr($a->typeno, 1);
                $useA = $aType . substr('000' . $aNumber, -4);
                $bType = substr($b->typeno, 0, 1);
                $bNumber = substr($b->typeno, 1);
                $useB = $bType . substr('000' . $bNumber, -4);
            } else {
                $useA = substr('000' . $a->egNo, -4);
                $useB = substr('000' . $b->egNo, -4);
            }

            return strcmp($useA, $useB);
        });

        foreach($eventGroups as $eventGroup){
//            $eventGroupDateArr = preg_split("~ ~",$eventGroup->egStartDate);
//            $eventGroupDate = $eventGroupDateArr[0];
//            $today = date("Y-m-d");
//            if ( $eventGroupDate === $today ){
                $html .= "<option value='" . $eventGroup->egNo . "'>";
                if ( $eventGroup->typeno !== '') {
                    $html .= $eventGroup->typeno . ' : ' . $eventGroup->egName;
                }else {
                    $html .= $eventGroup->egNo . ' : ' . $eventGroup->egName;
                }
                $html .= '</option>';
//            }
        }
        return $html;
    }

    public function displayFooter(){
        ?>
            <div id="<?php echo R4S_CARD_FOOTER ?>"></div>
</div>
            </body>
        <?php
    }

    public function jsInit(){
        $security = null;
        if (hasScoreboardAccess()) {
            $security = new r4sSecurityClass($this->compId, e4s_getUserID() );
        }
        ?>
            <script>
                var r4sGlobal = {};
                r4sGlobal.deviceKey='<?php
                    if (!is_null($security)) {
                        echo $security->registerDevice();
                    }
                    ?>';
                r4sGlobal.securityKey='<?php
                    if (!is_null($security)) {
                        echo $security->getSecurityKey();
                    }
                    ?>';
                function checkSetEvent(){
                    let eventNo = location.search.slice(1).split("&")[0].split("=")[1];
                    if ( typeof eventNo !== "undefined" ){
                        $("#event_selector").val(eventNo);
                        cardChange();
                        $("#Card-tab").hide();
                    }
                }
                $( function() {
                    //cardChange();
                    initSockets(true);
                    populateCardHeader();
                    displayEmptyCard();
                    showPleaseWait(false);
                    <?php if ( hasScoreboardAccess() ){
                        ?>
                            siginit();
                        <?php
                    }
                    ?>
                    checkSetEvent();
                } );
            </script>
            </html>
        <?php
    }

    public function getSecurityMarker(){

    }

    public function isCheckInEnabled(){
        $obj = $this->getCompetitionInfo();
        $options = e4s_addDefaultCompOptions($obj->options);

        return $this->isCheckinEnabledFromOptions($options);
    }

    public function isCheckinEnabledFromOptions($options){
        if ( $options->checkIn->enabled ){
            return TRUE;
        }
        return FALSE;
    }

    public function formatDateForReport($date){
        $useDate = explode('-',$date);
        return $useDate[2] . '/' . $useDate[1] . '/' . $useDate[0];
    }

    public function getEventSelectorHTMLold(){
        $eventGroups =  $this->getEventGroups();
        echo "<option value=''>Select an Event</option>";

        foreach($eventGroups as $eventGroup){
            echo "<option value='" . $eventGroup->egNo . "'>";
            echo $eventGroup->egNo . ' : ' . $eventGroup->egName;
            echo '</option>';
        }
    }

}