<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_OTD_PATH . '/tfUiCommon.php';
include_once E4S_FULL_PATH . 'classes/uploadTrackInfo.php';
include_once E4S_FULL_PATH . 'results/results.php';
include_once E4S_FULL_PATH . 'results/webresults.php';

function e4s_postTrackFile($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID');
    if (is_null($compId) or $compId === '') {
        Entry4UIError(9000, 'No competition passed!', 400);
    }
    $compObj = e4s_GetCompObj($compId);

    $videoParams = checkFieldForXSS($obj, 'videoparams:Video Parameters');
    if (!is_null($videoParams) ) {
        $videoParams = e4s_getDataAsType($videoParams, E4S_OPTIONS_OBJECT);
    }

    $key = checkFieldForXSS($obj, 'key:Comp Key');
    if (is_null($key)) {
        Entry4UIError(9001, 'No Security Key passed!', 400);
    }
    if ($key === 'manual') {
        if (!$compObj->isOrganiser()) {
            if (!userHasPermission(PERM_SCOREBOARD, null, $compId)) {
                exit('You are not authorised to perform this function');
            }
        }
    }
    if ($key !== 'manual' and $key !== $compObj->getSecurityKey()) {
        Entry4UIError(9002, 'Invalid Security Key passed!', 400);
    }

    $uploadObj = new uploadTrackInfo($compId);

    if ( !is_null($videoParams) ) {
        $videoParams = e4s_getDataAsType($videoParams, E4S_OPTIONS_OBJECT);
    }else{
        $videoParams = new stdClass();
    }
    $comp = new stdClass();
    $comp->id = $compObj->getID();
    $comp->name = $compObj->getName();
    $comp->date = $compObj->getDate();
    $videoParams->comp = $comp;
    $uploadObj->setVideoParams($videoParams);
    $uploadObj->uploadAFile(TRUE, $key !== 'manual');

    header('Content-Type: text/html; charset=utf-8');
	$eg = $compObj->getEventGroupByNo($uploadObj->eventNo);
	$sourceEgId = $eg->id;
	$qualObj = new e4sQualifyClass($eg->options);
	$targetEgId = $qualObj->getId();
	if ( $targetEgId !== 0 ) {
		e4s_getEventQualifyInfo( $sourceEgId, $targetEgId );
	}
}