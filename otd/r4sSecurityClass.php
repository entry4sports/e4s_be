<?php


class r4sSecurityClass {
    public $compId;
    public $userId;
    public $deviceKey;
    public $ip;

    public function __construct($compId, $userId) {
        $this->compId = $compId;
        $this->userId = $userId;
        $this->deviceKey = $this->getDeviceKey();
        $this->ip = $_SERVER['REMOTE_ADDR'];
    }

    public function getDeviceKey() {
        $deviceKey = 'Unknown ' . rand(1, 10000);
        $cookies = explode('; ', $_SERVER['HTTP_COOKIE']);

        foreach ($cookies as $value) {
            $cookie = explode('=', $value);
            if (strpos($cookie[0], 'wp_woocommerce_session_') !== FALSE) {
                $arr = explode('%7C', $cookie[1]);
                $deviceKey = $arr [sizeof($arr) - 1];
            }
        }
        return $deviceKey;
    }

    public function registerDevice() {
        $this->createSecurityRecord();
        return $this->deviceKey;
    }

    public function createSecurityRecord() {
        return;
        $sql = 'select *
                from ' . E4S_TABLE_R4SSECURITY . "
                where device = '" . $this->deviceKey . "'";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 0) {
            // create a record
            $insertSql = 'insert into ' . E4S_TABLE_R4SSECURITY . ' (compid,ip,device,userid )
            values (' . $this->compId . ",'" . $this->ip . "','" . $this->deviceKey . "'," . $this->userId . ')';
            $result = e4s_queryNoLog($insertSql);
            if ($result === FALSE) {
                Entry4UIError(9103, 'Failed to register Results Device');
            }
        }
    }

    public function getSecurityKey() {
        return $this->getEncryptedKey();
    }

    public function getEncryptedKey() {
        $div = '_';
        return md5($this->ip . $div . $this->deviceKey);
    }
}