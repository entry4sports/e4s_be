<?php

class uiSignature {
    public $compId;
    public $official;
    public $name;

    public function __construct($compId) {
        $this->compId = $compId;
    }

    public function html() {
        ?>
        <div id="signature-pad-div" class="signature-pad-div">
            <?php
            if (hasScoreboardAccess()) {
                ?>
                <div id="signature-pad-area" class="signature-pad-area">
                    <div id="signature-pad" class="signature-pad">
                        <div class="signature-pad--body">
                            <canvas></canvas>
                        </div>
                        <div class="signature-pad--footer">
                            <div class="description">Sign above</div>
                        </div>
                    </div>
                    <div>
                        <span id="officialNameLabel" class="officialNameLabel">Officials Name :</span>
                        <input id="officialName" class="officialNameInput">
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
    }

    public function css($echo = FALSE) {
        $css = '<style>
            .officialSig {
                font-size: 0.7em;
                width: 20%;
            }
            .signature-background {
                background-image: url();
                background-repeat: no-repeat;
                background-size: contain;
                background-position-x: 135px;
            }
            .officialNameLabel {
                font-size: 0.8em;
            }
            .signature-pad {
                position: relative;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: vertical;
                -webkit-box-direction: normal;
                -ms-flex-direction: column;
                flex-direction: column;
                font-size: 10px;
                width: 90%;
                height: 75%;
                max-width: 700px;
                max-height: 460px;
                border: 1px solid #e8e8e8;
                background-color: #fff;
                box-shadow: 0 1px 4px rgba(0, 0, 0, 0.27), 0 0 40px rgba(0, 0, 0, 0.08) inset;
                border-radius: 4px;
                padding: 16px;
            }

            .signature-pad::before,
            .signature-pad::after {
                position: absolute;
                z-index: -1;
                content: "";
                width: 40%;
                height: 10px;
                bottom: 10px;
                background: transparent;
                box-shadow: 0 8px 12px rgba(0, 0, 0, 0.4);
            }

            .signature-pad::before {
                left: 20px;
                -webkit-transform: skew(-3deg) rotate(-3deg);
                transform: skew(-3deg) rotate(-3deg);
            }

            .signature-pad::after {
                right: 20px;
                -webkit-transform: skew(3deg) rotate(3deg);
                transform: skew(3deg) rotate(3deg);
            }

            .signature-pad--body {
                position: relative;
                -webkit-box-flex: 1;
                -ms-flex: 1;
                flex: 1;
                border: 1px solid #f4f4f4;
            }

            .signature-pad--body
            canvas {
                position: absolute;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                border-radius: 4px;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.02) inset;
            }

            .signature-pad--footer {
                color: #C3C3C3;
                text-align: center;
                font-size: 1.2em;
                margin-top: 8px;
            }

            .signature-pad--actions {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                margin-top: 8px;
            }
        </style>';
        if ($echo) {
            echo $css;
        }
        return $css;
    }

    public function javascript() {
        if (hasScoreboardAccess()) {
            $this->editorJavascript();
        }
        ?>
        <script>
            function showSignatures() {
                let eg = getCurrentEvent();
                let egOptions = eg.egOptions;
                if (typeof egOptions.officials === "undefined") {
                    return;
                }
                for (let o in egOptions.officials) {
                    let official = egOptions.officials[o];
                    showSignatureName(official.id, official.name);
                    showSignature(official.id, official.filename)
                }
            }

            function inboundFieldSignature(payload) {
                showSignature(payload.official.official, payload.official.filename);
                showSignatureName(payload.official.official, payload.official.name);
            }

            function showSignatureName(official, name) {
                if (official === 0) {
                    official = 10;
                }
                $("#<?php echo R4S_SIGNATURE?>" + official).text(name);
            }

            function showSignature(official, filename) {
                if (official === 0) {
                    official = 10;
                }
                filename += "?" + Math.random();
                $("[signature=" + official + "]").css("background-image", "url('/<?php echo R4S_SIGNATURE_DIR ?>/" + filename + "')")
            }
        </script>
        <?php
    }

    public function editorJavascript() {
        ?>
        <script src="<?php echo E4S_PATH ?>/js/signature_pad.js"></script>
        <script src="<?php echo E4S_PATH ?>/js/signature_pad_app.js"></script>
        <script>
            function bindSignatures() {
                $(".officialSig").on("click", function (event) {
                    sigLaunch($(this).attr("signature"));
                });
            }

            function sigLaunch(official) {
                var title = "Signature for Official " + official;
                if (official === "10") {
                    title = "Signature for Field Referee";
                }
                var sigObj = $("#signature-pad-div");
                $("#officialName").val($("#<?php echo R4S_SIGNATURE?>" + official).text());
                sigObj.dialog({
                    title: title,
                    modal: true,
                    width: 400,
                    height: 400,
                    close: function () {
                        $("#signature-pad-area").addClass("hidden");
                    },
                    buttons: {
                        Cancel: function () {
                            jQuery(this).dialog("close");
                        },
                        Clear: function () {
                            signaturePad.clear();
                        },
                        Save: function () {
                            saveSignature(official, this);
                        },
                    }
                });
                $("#signature-pad-area").removeClass("hidden");
                resizeCanvas();
            }

            function saveSignature(official, dialog) {
                if (signaturePad.isEmpty()) {
                    alert("Please provide a signature first.");
                    return false;
                } else if ($("#officialName").val() === "") {
                    alert("Please provide a name.");
                    return false;
                } else {
                    let eg = getCurrentEvent();
                    let dataURL = signaturePad.toDataURL("image/jpeg");
                    let blob = dataURLToBlob(dataURL);
                    let endPoint = "/wp-json/e4s/v5/uicard/signature";
                    let payload = new FormData();
                    let compId = getCompetition().id;
                    let name = $("#officialName").val();
                    let useOfficial = parseInt(official);
                    if (useOfficial === 10) {
                        useOfficial = 0;
                    }
                    payload.append('signature', blob);
                    payload.append('compid', compId);
                    payload.append('official', useOfficial);
                    payload.append('egid', eg.egId);
                    payload.append('filename', getSignatureFilename(compId, eg.egId, useOfficial, name));
                    payload.append('name', name);
                    let extraObj = {
                        contentType: false,
                        processData: false
                    };
                    postData(endPoint, payload, sendOfficialToSocket, null, extraObj);
                    jQuery(dialog).dialog("close");
                }
            }

            function getSignatureFilename(compId, egId, official, name) {
                let useOfficial = parseInt(official);
                if (useOfficial === 10) {
                    useOfficial = 0;
                }
                let codedName = name.replace(/ /g, '-');
                codedName = name.replace(/'/g, '');
                codedName = name.replace(/"/g, '');
                let filename = compId + "_" + egId + "_" + useOfficial + "_" + codedName + ".jpg";
                return filename;
            }

            function sendOfficialToSocket(origPayload, eventGroup) {
                let useOfficial = parseInt(origPayload.get("official"));
                if (useOfficial === 10) {
                    useOfficial = 0;
                }
                var payload = getBaseResultObj();
                payload.official = {};
                payload.official.egId = parseInt(origPayload.get("egid"));
                payload.official.official = useOfficial;
                payload.official.name = origPayload.get("name");
                payload.official.filename = origPayload.get("filename");

                sendSocketMsg("<?php echo R4S_SOCKET_SIGNATURE ?>", payload);
            }
        </script>
        <?php
    }

    public function saveOfficial($model) {
        $this->saveOfficialSignature($model);
        if ($this->updateEgRecord($model)) {
//            Entry4UISuccess('"data":{"filename":"' . $model->filename . '"}');
            Entry4UISuccess();
        }
        Entry4UIError(9201, 'Failed to update Officials');
    }

    public function saveOfficialSignature($model) {
        $fileName = $model->filename;
        $verifyimg = getimagesize($_FILES['signature']['tmp_name']);

        if ($verifyimg['mime'] != 'image/jpeg') {
            Entry4UIError(9112, 'Only JPeg images are allowed!');
        }

        $uploaddir = R4S_SIGNATURE_DIR . '/';

        $uploadfile = $uploaddir . $fileName;

        if (move_uploaded_file($_FILES['signature']['tmp_name'], $uploadfile)) {
            return $fileName;
        } else {
            Entry4UIError(9113, 'Image uploading failed.');
        }
    }

    public function updateEgRecord($model) {
        // get existing record
        $sql = 'select options
                from ' . E4S_TABLE_EVENTGROUPS . '
                where id = ' . $model->egId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(9114, 'Failed to get Event for Signature');
        }
        $egRow = $result->fetch_object();
        $egOptions = e4s_getOptionsAsObj($egRow->options);
        if (!isset($egOptions->officials)) {
            $egOptions->officials = array();
        }
        $official = new stdClass();
        $official->id = $model->official;
        $official->name = $model->name;
        $official->filename = $model->filename;

        $updated = FALSE;
        foreach ($egOptions->officials as $key => $officialElement) {
            if ($officialElement->id === $model->official) {
                $updated = TRUE;
                $egOptions->officials[$key] = $official;
            }
        }
        if (!$updated) {
            $egOptions->officials[] = $official;
        }
        $egOptions = e4s_removeDefaultEventGroupOptions($egOptions);
        $sql = 'update ' . E4S_TABLE_EVENTGROUPS . "
                set options = '" . e4s_getOptionsAsString($egOptions) . "'
                where id = " . $model->egId;
        return e4s_queryNoLog($sql);
    }

    public function clearSignatureFromEventGroup($egId) {
        // get existing record
        $sql = 'select options
                from ' . E4S_TABLE_EVENTGROUPS . '
                where id = ' . $egId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(9115, 'Failed to get Event for Signature');
        }
        $egRow = $result->fetch_object();
        $egOptions = e4s_getOptionsAsObj($egRow->options);
        $egOptions->officials = array();
        $egOptions = e4s_removeDefaultEventGroupOptions($egOptions);
        $sql = 'update ' . E4S_TABLE_EVENTGROUPS . "
                set options = '" . e4s_getOptionsAsString($egOptions) . "'
                where id = " . $egId;
        return e4s_queryNoLog($sql);

    }
}

function e4s_saveSignature($obj) {
    $model = new stdClass();
    $model->compId = checkJSONObjForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (is_null($model->compId)) {
        Entry4UIError(9116, 'Invalid Competition passed');
    }
    $model->compId = (int)$model->compId;
    $model->official = checkJSONObjForXSS($obj, 'official:Official ID');
    if (is_null($model->official)) {
        Entry4UIError(9117, 'Invalid Official passed');
    }
    $model->official = (int)$model->official;
    $model->egId = checkJSONObjForXSS($obj, 'egid:Event Group ID');
    if (is_null($model->egId)) {
        Entry4UIError(9118, 'Invalid Event passed');
    }
    $model->egId = (int)$model->egId;
    $model->name = checkJSONObjForXSS($obj, 'name:Officials Name');
    $model->filename = checkJSONObjForXSS($obj, 'filename:File Name');

    $sigObj = new uiSignature($model->compId);
    $sigObj->saveOfficial($model);
    Entry4UISuccess();
}