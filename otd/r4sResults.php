<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'results/webresults.php';

define('E4S_PF_TT', 'T');
function e4s_getResultsForComp($compid) {
    $resultsObj = new resultsClass($compid);
    $results = $resultsObj->getResultsForComp();
    return $results;
}

function r4s_clearResults() {
    if (!e4s_isDevDomain()) {
        Entry4UIError(9901, 'Not allowed in this domain');
    }
    if (e4s_getUserID() !== 1633 and !isE4SUser()) {
        Entry4UIError(9902, 'Not allowed in this context');
    }
    $sql = 'delete from ' . E4S_TABLE_CARDRESULTS . '
            where compid = 351
            and eventno in ( 8, 9, 12 )';

    e4s_queryNoLog($sql);
    $sql = 'select id, options
            from ' . E4S_TABLE_ENTRIES . "
            where options like '%startheight%'";
    $result = e4s_queryNoLog($sql);
    while ($row = $result->fetch_object()) {
        $options = e4s_getOptionsAsObj($row->options);
        unset($options->startHeight);
        $sql = 'update ' . E4S_TABLE_ENTRIES . "
                set options = '" . e4s_getOptionsAsString($options) . "'
                where id = " . $row->id;
        e4s_queryNoLog($sql);
    }
    $sql = 'delete from ' . E4S_TABLE_SEEDING;
    e4s_queryNoLog($sql);

    Entry4UISuccess();
}

function r4s_postResults($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);

    $curEntry = $_POST['entry'];
    $positions = $_POST['positions'];

    $eventNo = $curEntry['eventGroup']['eventNo'];
    $athleteId = $curEntry['athlete']['id'];

    $sql = '
        delete from ' . E4S_TABLE_CARDRESULTS . "
        where  compid = {$compId}
        and   eventno = {$eventNo}
        and athleteid = {$athleteId}
    ";
    e4s_queryNoLog($sql);

    $values = array();
    $results = $curEntry['results'];
    $heightResult = FALSE;
    if (isset($results['height'])) {
        $heightResult = TRUE;
        $height = $results['height'];
    }

    foreach ($results['data'] as $key => $result) {
        if ($heightResult) {
            if (gettype($result) === 'array') { // shuld always be array now
                foreach ($result as $attempt => $resultvalue) {
                    if ($attempt > 0) {
                        // If a height result. ignore the 0
                        $resultkey = 'h' . $key . '_a' . $attempt;
                        $values[] = "({$compId},{$athleteId},{$eventNo},'{$resultkey}','{$resultvalue}')";
                    }
                }
            }
        } else {
            // Distance Results
            if (substr($key, 0, 1) === 't' and $key !== 'trial') {
                if ($key !== '') {
                    $values[] = "({$compId},{$athleteId},{$eventNo},'{$key}','{$result}')";
                }
            }
        }
    }

    if (!empty($values)) {
        $sql = '
            insert into ' . E4S_TABLE_CARDRESULTS . ' (compid, athleteid, eventno, resultkey, resultvalue)
            values ' . implode(',', $values);
        e4s_queryNoLog($sql);
        if (!$heightResult) {
            e4s_writeCardResults($compId, $curEntry, $positions);
        }
    }

    Entry4UISuccess();
}

function e4s_clearScoreboard($compid, $eventno) {
    $sbObj = new scoreboardClass($compid, $eventno, TRUE);
    $sbObj->deleteScoreboard();
    $sbObj->get();
}

function e4s_getScoreboardEventsForArray($obj) {
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $sbObj = new scoreboardClass($compid, -1, FALSE);
    $sbObj->getForEvents($obj);
}

function e4s_getScoreboardForBoard($compid, $eventno) {
    $sbObj = new scoreboardClass($compid, $eventno, TRUE);
    $sbObj->get();
}

function e4s_getAllForComp($compid) {
    $sbObj = new scoreboardClass($compid, 0, FALSE);
    $sbObj->getAll();
}

function e4s_inboundTrackResults($compid, $format) {
    $pfObj = new e4sPF($compid, $format);

    foreach ($_POST as $post => $value) {
        $pfObj->processResults($post);
    }
    Entry4UISuccess('');
}

class e4sPF {
    public $compid;
    public $eventno;
    public $headerInfo;
    public $resultLines = array();
    public $format;
    public $sep;

    public function __construct($compid, $format) {
        $this->compid = $compid;
        $this->format = $format;
        $this->sep = "\t";
        if ($format === E4S_PF_LYNX) {
            $this->sep = ',';
        }
    }

    public function processResults($resultsContent) {
        $firstDataLine = 1;
        if ($this->format === E4S_PF_TT) {
            $firstDataLine = 2;
        }
        $resultLines = explode("\n", $resultsContent);
        foreach ($resultLines as $sub => $data) {
            if ($sub === 0) {
                $this->processHeader($data);
            }
            // TT : ignore the 2nd line as its title data
            if ($sub >= $firstDataLine and $data !== '') {
                $this->processResultLine($data);
            }
        }

        if (!empty($this->resultLines)) {
            $this->updateDB();
        }
    }

    public function processHeader($data) {
        $headerLine = explode($this->sep, $data);

        $headerInfo = new stdClass();
        $headerInfo->eventNo = $this->getEventNo($headerLine[0]);
        $headerInfo->heatNo = $this->getHeatNo($headerLine[0]);
        $headerInfo->ws = $this->getWS($headerLine[1]);
        $headerInfo->dateStr = $this->getTimeOfEvent($headerLine[4]);
        $this->headerInfo = $headerInfo;
    }

    public function getEventNo($data) {
        $eventNo = 0;
        if ($this->format === E4S_PF_TT) {
            $str = explode('h', $data)[0];
            $eventNo = (int)str_replace('E', '', $str);
        }
        if ($this->format === E4S_PF_LYNX) {
            $arr = explode($this->sep, $data);
            $eventNo = $arr[0];
        }
        return $eventNo;
    }

    public function getHeatNo($data) {
        $heatNo = 0;
        if ($this->format === E4S_PF_TT) {
            $str = explode(',', $data);
            $heatNo = (int)explode('h', $str[0])[1];
        }
        if ($this->format === E4S_PF_LYNX) {
            $arr = explode($this->sep, $data);
            $heatNo = $arr[2];
        }

        return $heatNo;
    }

    public function getWS($data) {
        $ws = 'N/A';
        if ($this->format === E4S_PF_TT) {
            if (strpos($data, ':') !== FALSE) {
                $ws = str_replace('__', '_', $data);
                $ws = explode(':_', $ws)[1];
                $ws = str_replace('_m', ' m', $ws);
                $ws = str_replace('_', '.', $ws);
            }
        }
        if ($this->format === E4S_PF_LYNX) {
            $ws = '-';
        }
        return $ws;
    }

    public function getTimeOfEvent($data) {
        $time = '-';
        if ($this->format === E4S_PF_TT) {
            $time = str_replace('_-_', ' ', $data);
        }
        if ($this->format === E4S_PF_LYNX) {
            $arr = explode($this->sep, $data);
            $time = $arr[10];
        }
        return $time;
    }

    public function processResultLine($data) {
        $resultLine = explode($this->sep, $data);
//        1,491,8,Brier,Joe,Swansea,46.55,Men's 400m,46.55,,,13:46:04.40,,,,46.55,46.55,,
// place,bib, lane, last,first,club,time
        $obj = new stdClass();
        if ($this->format === E4S_PF_TT) {
            $obj->lane = $resultLine[1];
            $obj->time = $this->getTime($resultLine[2]);
            $obj->bibNo = $resultLine[3];
        }
        if ($this->format === E4S_PF_LYNX) {
            $obj->lane = $resultLine[2];
            $obj->time = $this->getTime($resultLine[6]);
            $obj->bibNo = $resultLine[1];
        }
        $this->resultLines[] = $obj;
    }

    public function getTime($data) {
        // remove first _
        if ($data[0] === '_') {
            $data = substr($data, 1);
        }
        $data = str_replace('_', '.', $data);
        $data = str_replace(':', '.', $data);

        $time = explode('.', $data);
        if (count($time) === 1) {
            return $data;
        }
        if (count($time) < 3) {
            return '' . $data;
        }
        $calcTime = (int)$time[0] * 60;
        $calcTime += (int)$time[1];

        return ('-' . $calcTime . '.' . $time[2]);
    }

    public function updateDB() {

        $clean = 'delete from ' . E4S_TABLE_CARDRESULTS . '
                 where compid = ' . $this->compid . ' 
                 and eventno = ' . $this->headerInfo->eventNo . "
                 and resultkey = 'h" . $this->headerInfo->heatNo . "'";

        e4s_queryNoLog($clean);

        $insert = 'Insert into ' . E4S_TABLE_CARDRESULTS . ' (`compid`, `athleteid`, `eventno`, `resultkey`, `resultvalue`) values ';
        $values = '';
        $sep = '';
        foreach ($this->resultLines as $resultLine) {
            $athleteid = $this->getAthleteID($resultLine->bibNo);
            if ($athleteid !== 0) {
                $values .= $sep . '(
                ' . $this->compid . ',
                ' . $athleteid . ',
                ' . $this->headerInfo->eventNo . ",
                'h" . $this->headerInfo->heatNo . "',
                '" . $resultLine->time . "'
                )";
                $sep = ',';
            }
        }
        if ($values !== '') {
//                    e4s_dump($insert . $values,"insert",true,true,true);
            e4s_queryNoLog($insert . $values);
        }

    }

    public function getAthleteID($bibno) {
        $sql = 'select athleteid athleteid
                from ' . E4S_TABLE_BIBNO . '
                where compid = ' . $this->compid . " 
                and bibno = {$bibno}";

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            $row = $result->fetch_assoc();
            return (int)$row['athleteid'];
        }
        return 0;
    }
}



