<?php
define('E4S_HEIGHT_ATHLETE_CNT', 30);
define('E4S_HEIGHT_TRIAL_CNT', 14);
define('E4S_HEIGHT_SUCCESS', 'O');
define('E4S_HEIGHT_FAILURE', 'X');
define('E4S_HEIGHT_PASS', '-');
define('E4S_HEIGHT_RETIRE', 'r');
define('E4S_HEIGHT_INVALID_HEIGHT', 999);
define('E4S_HEIGHT_COMPLETE_STYLE', 'height-complete');

class tfUiHeightClass {
    public $compId;

    public function __construct($compId) {
        $this->compId = $compId;
    }

    public function css($echo = FALSE) {
        $css = '<style>
        .hidden {
            display:none !important;
        }
        .seedFieldStartHeightData .ui-widget .ui-widget {
            font-size: 0.8em !important;
        }
        .seedFieldStartHeightData .ui-widget .ui-widget span {
            margin-left: -8px !important;
        }
        .seedFieldStartHeightData span {
            top: -3px;
        }
        .NonVisible {
            visibility: hidden;
        }
        .centre {
            text-align: center;
        }
        .attemptMessages {
            color: red;
        }
        .heightErrorMsg {
            color: red;
            float: right;
        }
        .' . E4S_HEIGHT_COMPLETE_STYLE . '::before {
            content: "\00bb";
        }
        #heightResults tr:nth-child(even){
            background-color: cornsilk;
        }
        .heightResultBib {
            width: 40px;
        }
        .height_athlete {
            font-size: 0.8em;
        }
        .height_club {
            font-size: 0.8em;
        }
        .height_sh {
            font-size: 0.8em;
        }
        .height_trials {
            font-size: 0.8em;
        }
        .height_failures  {
            font-size: 0.8em;
        }
        .height_position  {
            font-size: 0.8em;
        }
        .height_marker {
            font-size: 0.8em !important;
        }
        .heightResultAthlete {
            width: 355px;
        }
        .heightAttemptOption {
            min-width: 100px;
        }
        .heighAttempt {
            width: 40px;
        }
        .heighAttemptField {
            width: 18px;
        }
        .heighAttemptDiv {
            border: 1px solid black;
            height: 22px;
            width: 17px;
            margin-left: 10px;  
        }
        .heightIncrement{
            width: 35px;
            font-size: 0.8em !important;
        }
        .write_verticle {
//          transform: rotate(270deg);
//          letter-spacing: 1px;
//          font: bold 0.8em serif;
          width: 1vw;
        }
        .height_header {
            background-color: cornsilk;
        }
        .height_trial{
            width:1vw;
        }
        .height_best {
            font-size: 0.8em;
            border-left: 2px solid !important;
        }
        .height_totals {
            width: 1vw;
        }
        .height_bib {
            width: 2vw;
            text-align: center;
        }
        .centre {
            text-align: center;
        }
        .height_start {
            width: 3vw;
        }
       
        .height_card_cell {
            border: 1px;
            border-style: solid;
            border-color: black;
        }
        .height_column_header_label {
            font-size: 10px;
            text-align: center;
            background-color: lightgrey;
            border: 1px solid;
        }
        .height_result_1 {
            border-left: 2px solid;
            font-size: 1em;
        }
        .height_result_2 {
            font-size: 1em;
        }
        .height_result_3 {
            font-size: 1em;
        }
       
        .height_title {
            border-bottom: 2px solid;
            font-size: 0.8em;
        }
        </style>';
        if ($echo) {
            echo $css;
        }
        return $css;
    }

    public function javascript() {
        if (hasScoreboardAccess()) {
//        Results Entry only
            ?>
            <script>
                function clearHeightMsg() {
                    $("#heightErrorMsg").hide();
                }

                function showHeightMsg(msg) {
                    $("#heightErrorMsg").text(msg).show();
                    if (typeof r4sGlobal.errorMsg !== "undefined") {
                        clearTimeout(r4sGlobal.errorMsg);
                    }
                    r4sGlobal.errorMsg = setTimeout(function () {
                        clearHeightMsg()
                    }, 2000);
                }

                function trialHeightResults(useHeight) {
                    var divName = 'cardHeightResults';
                    var heightObj = this.getHeightObj();
                    var height = heightObj.setCurrentHeight(useHeight);
                    if (height === 0) {
                        alert("Please Set starting heights");
                        showSeedingDialog();
                        return;
                    }
                    var increment = heightObj.increment;

                    // remove if already been shown before
                    $("#" + divName).remove();
                    var divHTML = '<div id="' + divName + '">';
                    divHTML += '<span id="incrementDiv" class="NonVisible">Increment : <input readonly class="heightIncrement" id="increment" value="' + increment + '"> cm</span>';
                    divHTML += ' <span id="heightErrorMsg" class="heightErrorMsg"></span>';
                    var athletesForHeight = heightObj.getAthletesForCurrentHeight(useHeight);
                    if (athletesForHeight.length === 0) {
                        divHTML += '<div class="noAthletesForHeight">No Athletes !</div>';
                    } else {
                        divHTML += '<table id="heightResults" cellspacing="0" cellpadding="0">';
                        divHTML += '<tr class="card_header">';
                        divHTML += '<td colspan="2">&nbsp</td>';
                        divHTML += '<td colspan="3" class="centre" >Attempt</td>';
                        divHTML += '</tr>';
                        divHTML += '<tr class="card_header">';
                        divHTML += '<td class="heightResultBib">Bib</td>';
                        divHTML += '<td class="heightResultAthlete">Athlete</td>';
                        divHTML += '<td class="heighAttempt centre">1</td>';
                        divHTML += '<td class="heighAttempt centre">2</td>';
                        divHTML += '<td class="heighAttempt centre">3</td>';
                        divHTML += '</tr>';
                        for (var a = 0; a < athletesForHeight.length; a++) {
                            var entry = athletesForHeight[a];
                            divHTML += '<tr>';
                            divHTML += '<td class="heightResultBib">' + getBibNo(entry.entry.bibNo, entry.entry) + '</td>';

                            var attempt1 = '';
                            var attempt2 = '';
                            var attempt3 = '';
                            var lastAttempt = "";
                            var lastAttemptNo = 0;
                            let lastThreeFailures = false;
                            if (typeof entry.results['data'] !== "undefined" && typeof entry.results['data'][height] !== "undefined") {
                                if (typeof entry.results['data'][height][1] !== "undefined") {
                                    attempt1 = entry.results['data'][height][1];
                                    lastAttempt = attempt1;
                                    lastAttemptNo = 1;
                                }
                                if (typeof entry.results['data'][height][2] !== "undefined") {
                                    attempt2 = entry.results['data'][height][2];
                                    lastAttempt = attempt2;
                                    lastAttemptNo = 2;
                                }
                                if (typeof entry.results['data'][height][3] !== "undefined") {
                                    attempt3 = entry.results['data'][height][3];
                                    lastAttempt = attempt3;
                                    lastAttemptNo = 3;
                                }
                                lastThreeFailures = lastThreeAreFailures(entry, height);
                            }
                            var useClass = "";
                            var useResultClass = "";
                            if ((lastAttempt !== "" && lastAttempt !== '<?php echo E4S_FIELDCARD_FAILURE ?>') || lastThreeFailures) {
                                useClass = " <? echo E4S_HEIGHT_COMPLETE_STYLE ?>";
                                entry.results.complete = true;
                            } else {
                                entry.results.complete = false;
                            }
                            divHTML += '<td id="heightResultAthlete_' + entry.athlete.id + '" class="heightResultAthlete' + useClass + '">' + entry.athlete.name + '</td>';
                            divHTML += '<td class="heighAttempt centre"><div onclick="trialGetAttemptResult(' + height + ',' + entry.athlete.id + ', 1);" class="heighAttemptDiv" heightAttempt=1 athleteId=' + entry.athlete.id + ' id="a1_' + entry.athlete.id + '">' + attempt1 + '</div></td>';
                            if (useClass !== "" && lastAttemptNo === 1) {
                                useResultClass = "hidden";
                            }
                            divHTML += '<td class="heighAttempt centre"><div onclick="trialGetAttemptResult(' + height + ',' + entry.athlete.id + ', 2);" class="heighAttemptDiv ' + useResultClass + '" heightAttempt=2 athleteId=' + entry.athlete.id + ' id="a2_' + entry.athlete.id + '">' + attempt2 + '</div></td>';
                            if (useClass !== "" && lastAttemptNo === 2) {
                                useResultClass = "hidden";
                            }
                            divHTML += '<td class="heighAttempt centre"><div onclick="trialGetAttemptResult(' + height + ',' + entry.athlete.id + ', 3);" class="heighAttemptDiv ' + useResultClass + '" heightAttempt=3 athleteId=' + entry.athlete.id + ' id="a3_' + entry.athlete.id + '">' + attempt3 + '</div></td>';
                            divHTML += '</tr>';
                        }
                        divHTML += '</table>';
                    }
                    divHTML += '</div>';

                    $("#<?php echo E4S_CARD_RESULTS_DIV ?>").html(divHTML);
                    var dialog = $("#" + divName).dialog({
                        resizable: false,
                        title: "Current Height : " + getDecimals(height, 2) + "m",
                        height: "auto",
                        width: 550,
                        modal: true
                    });
                    buttons = [
                        {
                            text: "Close",
                            id: "heightResultOkBtn",
                            class: "resultButton",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ];

                    buttons.push({
                        text: heightObj.getPreviousHeightText(height),
                        id: "backHeightButton",
                        class: "resultButton floatLeft previousHeightBtn hidden",
                        click: function () {
                            $(this).dialog("close");
                            trialHeightResults(heightObj.getPreviousHeight(height));
                        }
                    });

                    buttons.push({
                        text: heightObj.getNextHeightText(height),
                        id: "forwardHeightButton",
                        class: "resultButton floatLeft nextHeightBtn hidden",
                        click: function () {
                            $(this).dialog("close");
                            trialHeightResults(heightObj.getNextHeight(height));
                        }
                    });

                    buttons.push({
                        text: "Current",
                        id: "currentHeightButton",
                        class: "resultButton floatLeft nextHeightBtn hidden",
                        click: function () {
                            $(this).dialog("close");
                            trialHeightResults(heightObj.getLastHeight());
                        }
                    });

                    buttons.push({
                        text: "Next",
                        id: "nextHeightButton",
                        class: "resultButton floatLeft nextHeightBtn hidden",
                        click: function () {
                            $(this).dialog("close");
                            trialHeightResults(getFloatDecimals(getHeightObj().getNextHeight(), 2));
                        }
                    });

                    dialog.dialog("option", "buttons", buttons);
                    displayResultButtons(height);
                    $("#increment").spinner({
                        max: 20,
                        min: 2,
                        spin: function (event, ui) {
                            r4sGlobal.heightObj.changeHeightIncrement(event, ui);
                        }
                    })
                }

                function displayNextHeightButton(show) {
                    if (!show) {
                        $("#nextHeightButton").addClass("hidden");
                    } else {
                        var heightObj = getHeightObj();
                        var obj = heightObj.getCurrentStatus();
                        if (obj.currentHeightFinished) {
                            if (!obj.atLeastOneSuccessAtHeight) {
                                if (obj.nextStartingHeight === <?php echo E4S_HEIGHT_INVALID_HEIGHT ?> ) {
                                    return;
                                }
                            }
                        }
                        var nextBtnObj = $("#nextHeightButton");
                        nextBtnObj.text(heightObj.getNextHeightText());
                        nextBtnObj.removeClass("hidden");

                        if (obj.atLeastOneSuccessAtHeight) {
                            displayIncrement(true);
                        }
                    }
                }

                function showForwardHeightButton(show) {
                    if (show) {
                        $("#forwardHeightButton").removeClass("hidden");
                        displayIncrement(false);
                    } else {
                        $("#forwardHeightButton").addClass("hidden");
                    }
                }

                function showBackHeightButton(show) {
                    if (show) {
                        $("#backHeightButton").removeClass("hidden");
                    } else {
                        $("#backHeightButton").addClass("hidden");
                    }
                }

                function showCurrentHeightButton(show) {
                    if (show) {
                        $("#currentHeightButton").removeClass("hidden");
                    } else {
                        $("#currentHeightButton").addClass("hidden");
                    }
                }

                function displayIncrement(show) {
                    if (show) {
                        $("#incrementDiv").removeClass("NonVisible");
                    } else {
                        $("#incrementDiv").addClass("NonVisible");
                    }
                }

                function displayResultButtons(height) {
                    let available = true;
                    let heightObj = getHeightObj();
                    if (heightObj.showForwardHeight(height)) {
                        showForwardHeightButton(true);
                        showCurrentHeightButton(true);
                        available = false;
                    } else {
                        showForwardHeightButton(false);
                        if (height < heightObj.getLastHeight()) {
                            showCurrentHeightButton(true);
                            available = false;
                        } else {
                            showCurrentHeightButton(false);
                        }
                    }
                    if (heightObj.isPreviousHeightAvailable(height)) {
                        showBackHeightButton(true);
                    } else {
                        showBackHeightButton(false);
                    }
                    if (!available) {
                        displayIncrement(false);
                        return;
                    }

                    height = heightObj.getCurrentHeight();
                    let entries = heightObj.getAthletesForCurrentHeight();
                    for (var e = 0; e < entries.length; e++) {
                        var checkEntry = entries[e];
                        var heightPassed = false;
                        for (var a = 1; a <= 3; a++) {
                            if (typeof checkEntry.results === "undefined") {
                                break;
                                // } else if ( checkEntry.results.complete ) {
                                //     heightPassed = true;
                                //     break;
                            } else if (typeof checkEntry.results.data === "undefined" || typeof checkEntry.results.data[height] === "undefined") {
                                break;
                            } else if (typeof checkEntry.results.data[height][a] === "undefined") {
                                break;
                            } else if (heightObj.isAttemptRetired(checkEntry.results.data[height][a])) {
                                break;
                            } else if (!heightObj.isAttemptFailed(checkEntry.results.data[height][a])) {
                                heightPassed = true;
                                break;
                            } else if (a === 3) {
                                heightPassed = true;
                            }
                        }
                        if (!heightPassed) {
                            available = false;
                            break;
                        }
                    }
                    displayNextHeightButton(available);
                }

                function isValidForAthleteToAttempt(curEntry, height, attempt) {
                    clearHeightMsg();
                    var heightObj = this.getHeightObj();
                    var resultsData = null;
                    if (attempt === 1) {
                        // CHECK RESULTS
                        // if ( typeof curEntry.results === "undefined" || typeof curEntry.results['data'] === "undefined" ){
                        //     return true;
                        // }
                        resultsData = curEntry.results['data'];
                        if (typeof resultsData[height] === "undefined") {
                            // new height
                            resultsData[height] = {};
                            return true;
                        }
                        if (typeof resultsData[height][attempt + 1] !== "undefined" && resultsData[height][attempt + 1] !== '') {
                            showHeightMsg("Attempt out of sequence");
                            return false;
                        }
                        return true;
                    }
                    resultsData = curEntry.results['data'];
                    if (typeof resultsData[height] === "undefined") {
                        // if (attempt !== 1){
                        showHeightMsg("Attempt out of sequence");
                        return false;
                        // }
                    } else {

                        if (typeof resultsData[height][attempt - 1] === "undefined") {
                            showHeightMsg("Attempt out of sequence");
                            return false;
                        }
                        if (!heightObj.isAttemptFailed(resultsData[height][attempt - 1])) {
                            showHeightMsg("Attempt Complete");
                            return false;
                        }
                        if (attempt < 3) {
                            if (typeof resultsData[height][attempt + 1] !== "undefined" && resultsData[height][attempt + 1] !== '') {
                                showHeightMsg("Attempt out of sequence");
                                return false;
                            }
                        }
                    }
                    // we know we are trying to enter a 2 or great attempt, so check previous attempt results
                    attempt = attempt - 1;

                    var entries = heightObj.getAthletesForCurrentHeight();
                    for (var a = 0; a < entries.length; a++) {
                        var otherAthlete = entries[a];
                        var otherResultsData = otherAthlete.results['data'];
                        if (otherAthlete.athlete.id !== curEntry.athlete.id && !otherAthlete.results.complete) {
                            if (typeof otherResultsData[height] === "undefined") {
                                showHeightMsg("Previous attempt not complete.");
                                return false;
                            }
                        }
                        for (let ca = 1; ca <= attempt; ca++) {
                            if (typeof otherResultsData[height][ca] !== "undefined") {
                                if (!heightObj.isAttemptFailed(otherResultsData[height][ca])) {
                                    return true;
                                }
                            } else if (!otherAthlete.results.complete) {
                                showHeightMsg("Previous attempt not complete.");
                                return false;
                            }
                        }
                    }
                    return true;
                }

                function trialGetAttemptResult(height, athleteId, attempt) {
                    var curEntry = getCurEntry(athleteId);
                    getHeightObj().loseFocus();

                    // check its valid to put a result on for this attempt
                    if (!isValidForAthleteToAttempt(curEntry, height, attempt)) {
                        return;
                    }

                    var divName = 'cardHeightAttempt';
                    var attemptText = "Third";
                    if (attempt === 1) {
                        attemptText = "First";
                    }
                    if (attempt === 2) {
                        attemptText = "Second";
                    }
                    attemptText += ' Attempt at ' + getDecimals(height, 2) + 'm';
                    // remove if already been shown before
                    $("#" + divName).remove();

                    var cChecked = "";
                    var fChecked = "";
                    var pChecked = "";
                    var rChecked = "";

                    if (typeof curEntry.results['data'][height] !== "undefined") {
                        var resultsData = curEntry.results['data'];
                        if (typeof resultsData[height][attempt] !== "undefined") {
                            switch (resultsData[height][attempt]) {
                                case '<?php echo E4S_FIELDCARD_SUCCESS ?>':
                                    cChecked = "checked";
                                    break;
                                case '<?php echo E4S_FIELDCARD_FAILURE ?>':
                                    fChecked = "checked";
                                    break;
                                case '<?php echo E4S_HEIGHT_PASS ?>':
                                    pChecked = "checked";
                                    break;
                                case '<?php echo E4S_FIELDCARD_RETIRE ?>':
                                    rChecked = "checked";
                                    break;
                            }
                        }
                    }

                    var divHTML = '<div id="' + divName + '">';
                    divHTML += '<table>';
                    divHTML += '<tr>';
                    divHTML += '<td class="">' + curEntry.entry.bibNo + ' : ' + curEntry.athlete.name + '</td>';
                    divHTML += '</tr>';
                    divHTML += '<tr>';
                    divHTML += '<tr>';
                    divHTML += '<td class="heightAttemptOption"><input type="radio" ' + cChecked + ' id="attemptResultC" name="attemptResult" value="<?php echo E4S_FIELDCARD_SUCCESS ?>">Cleared</td>';
                    divHTML += '<td class="heightAttemptOption"><input type="radio" ' + fChecked + ' id="attemptResultF" name="attemptResult" value="<?php echo E4S_FIELDCARD_FAILURE ?>">Failed</td>';
                    divHTML += '<td class="heightAttemptOption"><input type="radio" ' + pChecked + ' id="attemptResultP" name="attemptResult" value="<?php echo E4S_HEIGHT_PASS ?>">Pass</td>';
                    divHTML += '<td class="heightAttemptOption"><input type="radio" ' + rChecked + ' id="attemptResultR" name="attemptResult" value="<?php echo E4S_FIELDCARD_RETIRE ?>">Retired</td>';
                    divHTML += '</tr>';
                    divHTML += '<tr>';
                    divHTML += '<td><span id="attemptMessage" class="attemptMessages"></span></td>';
                    divHTML += '</tr>';
                    divHTML += '</table>';
                    divHTML += '</div>';

                    $("#<?php echo E4S_CARD_ATTEMPT_DIV ?>").html(divHTML);
                    var dialog = $("#" + divName).dialog({
                        resizable: false,
                        title: attemptText,
                        height: "auto",
                        width: 550,
                        modal: true
                    });

                    buttons = [
                        {
                            text: "Ok",
                            class: "resultButton",
                            click: function () {
                                if (validateOK(height, athleteId, attempt)) {
                                    displayResultButtons(height);
                                    $(this).dialog("close");
                                }
                            }
                        },
                        {
                            text: "Cancel",
                            class: "resultButton",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ];

                    dialog.dialog("option", "buttons", buttons);

                    function setMessage(msg) {
                        $("#attemptMessage").text(msg);
                    }

                    function validateOK(height, athleteId, attempt) {
                        var result = $("#attemptResultC").prop("checked") ? "<?php echo E4S_FIELDCARD_SUCCESS ?>" :
                            ($("#attemptResultF").prop("checked") ? "<?php echo E4S_FIELDCARD_FAILURE ?>" :
                                ($("#attemptResultP").prop("checked") ? "<?php echo E4S_HEIGHT_PASS ?>" :
                                    ($("#attemptResultR").prop("checked") ? "<?php echo E4S_FIELDCARD_RETIRE ?>" :
                                        "")));

                        if (result === "") {
                            setMessage("Please select a result");
                            return false;
                        }
                        setMessage("");
                        processHeightResult(height, athleteId, attempt, result);
                        return true;
                    }
                }

                function lastThreeAreFailures(curEntry, height) {
                    // CHECK RESULTS
                    // if ( typeof curEntry.results === "undefined" ){
                    //     return false;
                    // }
                    // if ( typeof curEntry.results.data === "undefined" ){
                    //     return false;
                    // }
                    let heightObj = getHeightObj();
                    let sortedHeights = heightObj.barHeightsSorted;
                    let failures = 0;
                    let results = curEntry.results.data;

                    for (let h = (sortedHeights.length - 1); h >= 0; h--) {
                        if (sortedHeights[h] <= height) {
                            let checkHeight = sortedHeights[h];
                            for (let a = 3; a > 0; a--) {
                                let checkResult = results[checkHeight][a];
                                if (typeof checkResult !== "undefined") {
                                    if (heightObj.isAttemptCleared(checkResult)) {
                                        return false;
                                    }
                                    if (heightObj.isAttemptRetired(checkResult)) {
                                        return false;
                                    }
                                    if (heightObj.isAttemptFailed(checkResult)) {
                                        failures++;
                                    }
                                }
                                if (failures >= 3) {
                                    return true;
                                }
                            }
                        }
                    }
                    return failures >= 3;
                }

                function processHeightResult(height, athleteId, attempt, result) {
                    height = parseFloat(getDecimals(height, 2));
                    // update curEntry
                    var curEntry = getCurEntry(athleteId);
                    if (typeof curEntry.results.data[height] === "undefined") {
                        curEntry.results.data[height] = {};
                    }
                    curEntry.results.data[height][attempt] = result;

                    // update underlying result dialog
                    $("#a" + attempt + "_" + athleteId).text(result);
                    $("#a2_" + athleteId).removeClass("hidden");
                    $("#a3_" + athleteId).removeClass("hidden");

                    var heightObj = getHeightObj();
                    var hideFutureAttempts = false;
                    curEntry.results.complete = false;
                    // if height finished for athlete. add class
                    if (heightObj.isAttemptFailed(result)) {
                        $("#heightResultAthlete_" + curEntry.athlete.id).removeClass("<?php echo E4S_HEIGHT_COMPLETE_STYLE ?>");
                        hideFutureAttempts = lastThreeAreFailures(curEntry, height);
                    } else {
                        $("#heightResultAthlete_" + curEntry.athlete.id).addClass("<?php echo E4S_HEIGHT_COMPLETE_STYLE ?>");
                        hideFutureAttempts = true;
                    }
                    if (hideFutureAttempts) {
                        curEntry.results.complete = true;
                        if (attempt < 3) {
                            // hide 3rd attempt
                            $("#a3_" + athleteId).addClass("hidden");
                        }
                        if (attempt < 2) {
                            // hide 2nd attempt
                            $("#a2_" + athleteId).addClass("hidden");
                        }
                    }
                    curEntry.results.height = height;
                    if (heightObj.isAttemptCleared(height)) {
                        this.setAthleteHighScore(curEntry);
                    }
                    curEntry.results.attempt = attempt;

                    var payload = {
                        "compid": getCompetition().id,
                        "entry": curEntry
                    };

                    var endPoint = '<?php echo R4S_POST_RESULT ?>';
                    postData(endPoint, payload, sendHeightResultToSocket);
                }

                function sendHeightResultToSocket(payload) {
                    var curEntry = payload.entry;
                    if (curEntry === null) {
                        return; // Need to track why this happens
                    }

                    var newPayload = getBaseResultObj();
                    var score = {};
                    score.athleteId = curEntry.athlete.id;
                    score.athlete = curEntry.athlete;
                    score.eventGroup = curEntry.eventGroup;
                    score.attempt = curEntry.results.attempt;
                    score.height = curEntry.results.height;
                    score.score = curEntry.results["data"][score.height][score.attempt];
                    score.nextAthleteId = getNextAthleteForHeight(curEntry.athlete.id);
                    newPayload.score = score;

                    sendSocketMsg("<?php echo R4S_SOCKET_HEIGHT_RESULT ?>", newPayload);
                }

                function getNextAthleteForHeight(athleteId) {
                    var curEntries = getCurrentEntries();
                    var curAthletePosition = curEntries[athleteId].heatInfo.position;

                    for (e in curEntries) {
                        var checkAthleteId = parseInt(e);
                        var entry = curEntries[checkAthleteId];
                        if (entry.entry.present) {

                        }
                    }

                    return 0;
                }
            </script>
        <?php }
////        Read Only Card functions
        ?>
        <script>
            function getHeightObj() {
                if (r4sGlobal.heightObj === null) {
                    r4sGlobal.heightObj = heightClass;
                    r4sGlobal.heightObj.init();
                }
                return r4sGlobal.heightObj;
            }

            function getHeightCardHTML() {
                var rowCnt = <?php echo E4S_HEIGHT_ATHLETE_CNT ?>;
                var heightObj = this.getHeightObj();
                var sortedEntries = heightObj.sortedEntries;
                heightObj.setHeightSections();
                html = '<table id="heightCardTable" class="card_table" cellpadding="0" cellspacing="0">';
                // height Card Column Titles
                var sectionNo = 0;
                var sections = heightObj.sections;
                html += generateHeightTitles(sections[sectionNo]);

                var startHeight = isNaN(sortedEntries[0].entry.startHeight) ? 0 : sortedEntries[0].entry.startHeight;
                var fromSh = getFloatDecimals(startHeight, 2);
                var trials = <?php echo E4S_HEIGHT_TRIAL_CNT ?>;
                var entryCnt = sortedEntries.length;
                var athleteCnt = sections[sectionNo].athletes.length;
                athleteCnt = 0;
                for (var i = 1; i < rowCnt; i++) {
                    var showHeader = false;
                    var athleteId = 0;
                    if (startHeight === 0 && i === sortedEntries.length + 1 && i < (rowCnt - 6)) {
                        // No start heights specified
                        showHeader = true;
                    } else {
                        if (sectionNo < sections.length) {
                            if (athleteCnt >= sections[sectionNo].athletes.length) {
                                sectionNo++;
                                athleteCnt = 0;
                                if (sectionNo < sections.length) {
                                    showHeader = true;
                                }
                            }
                            if (sectionNo < sections.length) {
                                if (athleteCnt < sections[sectionNo].athletes.length) {
                                    athleteId = sections[sectionNo].athletes[athleteCnt++].athlete.id;
                                }
                            }
                        }
                    }
                    if (showHeader) {
                        rowCnt -= 2;
                        // blank Line
                        html += generateHeightAthleteRow(0);
                        html += generateHeightAthleteRow(0);
                        html += generateHeightTitles(sections[sectionNo]);
                    }
                    html += generateHeightAthleteRow(athleteId);
                }
                html += '</table>';
                return html;
            }

            function generateHeightTitles(section) {
                var heightObj = this.getHeightObj();

                html = '<tr class="height_header">';
                html += '<td rowspan="2" class="height_card_cell height_title height_trial">&nbsp;</td>';
                html += '<td rowspan="2" class="height_card_cell height_title height_bib">Bib</td>';
                html += '<td rowspan="2" class="height_card_cell height_title">Name</td>';
                html += '<td rowspan="2" class="height_card_cell height_title">Club</td>';
                html += '<td rowspan="2" class="height_card_cell height_title height_start centre">SH</td>';
                //for(i=1; i <= <?php //echo E4S_HEIGHT_TRIAL_CNT ?>//; i++) {
                //    html += '<td colspan="3" class="height_card_cell height_result_1 centre">Metres</td>';
                //}

                var trial = 0;
                if (typeof section !== "undefined") {
                    if (typeof section.heights !== "undefined") {
                        var heightsSorted = [];
                        for (let h in section.heights) {
                            heightsSorted.push(parseFloat(h));
                        }
                        heightsSorted.sort();
                        for (let h = 0; h < heightsSorted.length; h++) {
                            var height = getDecimals(heightsSorted[h], 2);
                            html += '<td colspan="3" class="height_card_cell height_result_1 height_marker centre"><span id="height_trial_height_' + section.id + "_" + h + '">' + height + 'm</span></td>';
                            trial++;
                        }
                    }
                }
                for (let i = trial; i < <?php echo E4S_HEIGHT_TRIAL_CNT ?>; i++) {
                    html += '<td colspan="3" class="height_card_cell height_result_1 height_marker centre"><span id="height_trial_height_' + section.id + "_" + i + '">&nbsp;</span></td>';
                }

                html += '<td rowspan="2" class="height_card_cell height_title height_totals height_best centre">Best Height</td>';
                html += '<td rowspan="2" class="height_card_cell height_title height_totals centre">Best Trials</td>';
                html += '<td rowspan="2" class="height_card_cell height_title height_totals centre">Total Fails</td>';
                html += '<td rowspan="2" class="height_card_cell height_title height_totals centre">Pos</td>';
                html += '<td rowspan="1" colspan="2" class="height_card_cell">&nbsp;</td>';
                html += "</tr>";

                html += '<tr class="height_header">';
                for (i = 1; i <= <?php echo E4S_HEIGHT_TRIAL_CNT ?>; i++) {
                    html += '<td class="height_card_cell height_trial height_title height_result_1 centre">1</td>';
                    html += '<td class="height_card_cell height_trial height_title centre">2</td>';
                    html += '<td class="height_card_cell height_trial height_title centre">3</td>';
                }

                html += '<td class="height_card_cell  height_trial height_title centre">A</td>';
                html += '<td class="height_card_cell  height_trial height_title centre">B</td>';
                html += "</tr>";

                return html;
            }

            function displayAthleteResults() {
                var heightObj = getHeightObj();
                var entries = heightObj.sortedEntries;

                for (var e in entries) {
                    var bestHeight = "";
                    var bestHeightTrials = 0;
                    var totalFailures = 0;
                    var athleteId = entries[e].athlete.id;
                    if (typeof entries[e].results !== "undefined") {
                        if (typeof entries[e].results['data'] !== "undefined") {
                            let resultData = entries[e].results['data'];
                            for (let h in resultData) {
                                let heightData = resultData[h];
                                for (let attempt in heightData) {
                                    $("#" + getHeightResultId(athleteId, h, attempt)).text(heightData[attempt]);
                                    if (heightObj.isAttemptCleared(heightData[attempt])) {
                                        if (bestHeight < h) {
                                            bestHeight = h;
                                            bestHeightTrials = attempt;
                                        }
                                    }
                                }
                            }
                            for (let h in resultData) {
                                let heightData = resultData[h];
                                for (let attempt in heightData) {
                                    if (heightObj.isAttemptFailed(heightData[attempt])) {
                                        if (h <= bestHeight) {
                                            totalFailures++;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (bestHeight === "") {
                        bestHeightTrials = "";
                        totalFailures = "";
                    } else {
                        bestHeight = getDecimals(bestHeight, 2);
                    }
                    $("#height_best_" + athleteId).text(bestHeight);
                    $("#height_trials_" + athleteId).text(bestHeightTrials);
                    $("#height_failures_" + athleteId).text(totalFailures);
                }
            }

            function getHeightResultId(athleteId, height, attempt) {
                height = height.replace(".", "-");
                return 'height_' + athleteId + '_' + height + '_' + attempt;
            }

            function generateHeightAthleteRow(athleteId) {
                html = "<tr>";
                html += '<td class="height_card_cell height_row"><span id="field_row_' + athleteId + '">&nbsp;</span></td>';
                html += '<td class="height_card_cell height_bib centre"><span id="field_bib_' + athleteId + '"></span></td>';
                html += '<td class="height_card_cell height_athlete"><span id="height_athlete_' + athleteId + '"></span></td>';
                html += '<td class="height_card_cell height_club"><span id="height_club_' + athleteId + '"></span></td>';
                html += '<td class="height_card_cell height_sh centre"><span id="height_sh_' + athleteId + '"></span></td>';

                let trial = 1;
                if (athleteId > 0) {
                    let heightObj = this.getHeightObj();
                    let sectionNo = heightObj.getSectionForAthlete(athleteId);
                    let heights = heightObj.getHeightsForSection(sectionNo);
                    let heightsSorted = [];
                    for (let h in heights) {
                        heightsSorted.push(parseFloat(heights[h]));
                    }
                    heightsSorted.sort();
                    for (let h = 0; h < heightsSorted.length; h++) {
                        for (let a = 1; a <= 3; a++) {
                            // dont use . in id names
                            var height = heightsSorted[h];
                            var heightStr = ("" + height).replace(".", "-");
                            html += '<td class="height_card_cell height_result_' + a + ' centre"><span id="' + getHeightResultId(athleteId, heightStr, a) + '"></span></td>';
                        }
                        trial++;
                    }
                }
                for (let t = trial; t <= <?php echo E4S_HEIGHT_TRIAL_CNT ?>; t++) {
                    for (let a = 1; a <= 3; a++) {
                        html += '<td class="height_card_cell height_result_' + a + '">&nbsp;</td>';
                    }
                }
                html += '<td class="height_card_cell height_best centre"><span id="height_best_' + athleteId + '"></span></td>';
                html += '<td class="height_card_cell height_trials centre"><span id="height_trials_' + athleteId + '"></span></td>';
                html += '<td class="height_card_cell height_failures centre"><span id="height_failures_' + athleteId + '"></span></td>';
                html += '<td class="height_card_cell height_position"><span id="height_position_' + athleteId + '"></span></td>';
                html += '<td class="height_card_cell height_a"><span id="height_a"></span></td>';
                html += '<td class="height_card_cell height_b"><span id="height_b"></span></td>';
                html += "</tr>";
                return html;
            }

            function loadFieldHeightEntries(maxRows) {
                var fromRow = 1;
                var heightObj = this.getHeightObj();
                var sortedEntries = heightObj.sortedEntries;

                for (var x = 0; x < sortedEntries.length; x++) {
                    var entry = sortedEntries[x];
                    entry.heatInfo.position = x + 1;
                    setFieldHeightInfo(entry);
                    fromRow = x + 1;
                }

                displayAthleteResults();
            }

            function setFieldHeightInfo(entry) {
                var athlete = entry.athlete;
                var position = athlete.id;
                var rowSelect = "#field_row_" + position;
                var bibSelect = "#field_bib_" + position;
                // var ageSelect = "#field_age_" + position;
                var athleteSelect = "#height_athlete_" + position;
                var clubSelect = "#height_club_" + position;
                var sh = "#height_sh_" + position;

                markUIFieldRowPresent($(bibSelect), entry.entry.present);

                $(rowSelect).text(entry.heatInfo.position);

                $(bibSelect).text(entry.entry.bibNo);
                var ageGroup = entry.ageGroup.eventAgeGroup;
                if (ageGroup === "") {
                    ageGroup = entry.ageGroup.ageGroup;
                }
                // $(ageSelect).text(ageGroup);
                $(athleteSelect).html(getAthleteNameForCard(entry));

                $(clubSelect).text(athlete.club);

                if (entry.entry.startHeight > 0) {
                    $(sh).text(getDecimals(entry.entry.startHeight, 2));
                } else {
                    $(sh).text("");
                }
            }

            function inboundFieldStartHeight(payload) {
                updateCurrentEntry(payload);
                let position = getAthleteRowId(payload);
                let shObj = $("#height_sh_" + position);
                shObj.text(getDecimals(payload.entry.startHeight, 2));
                buildAndPopulateHeightCard();
            }

            function displayHeightMarkers() {
                var heightObj = this.getHeightObj();
                var sections = heightObj.sections;
                for (var s = 0; s < sections.length; s++) {
                    var section = sections[s];
                    for (var r = 1; r <= section.heights; r++) {
                        $("#height_trial_height_" + s + "_" + r).html(section.heights[r]);
                    }
                }
            }

            function inboundHeightResult(payload) {
                r4sGlobal.currentEntries = payload.entries;
                buildAndPopulateHeightCard();
            }

            function buildAndPopulateHeightCard() {
                setDisplayedCardHTML(getHeightCardHTML());
                loadFieldHeightEntries(<?php echo E4S_HEIGHT_ATHLETE_CNT ?>);
            }

            var heightClass = {
                currentHeight: 0,
                sectionTrials: <?php echo E4S_HEIGHT_TRIAL_CNT ?>,
                eventGroup: null,
                increment: 5,
                sortedEntries: null,
                sections: [],
                barHeights: [],
                barHeightsSorted: [],
                statusObj: null,
                init: function () {
                    this.sortedEntries = this.sortHeightEntries();
                    this.currentHeight = this.getFirstHeight();
                    this.eventGroup = getCurrentEvent();
                    if (typeof this.eventGroup.egOptions !== "undefined") {
                        if (typeof this.eventGroup.egOptions.increment !== "undefined") {
                            this.increment = this.eventGroup.egOptions.increment;
                        }
                    }
                    this.loadBarHeights();
                    this.setHighScores();
                    this.getCurrentStatus();
                    this.sections = [];
                },
                setHighScores: function () {
                    let entries = this.sortedEntries;

                    for (let e in this.sortedEntries) {
                        let curEntry = this.sortedEntries[e];
                        this.setAthleteHighScore(curEntry);
                    }
                },
                setAthleteHighScore: function (curEntry) {
                    let highScore = 0;
                    let highestHeightAttempted = 0;
                    let results = curEntry.results;
                    let data = results.data;
                    for (let h in data) {
                        let height = parseFloat(h);
                        if (height > highestHeightAttempted) {
                            highestHeightAttempted = height;
                        }
                        if (this.isHeightCleared(height)) {
                            if (height > highScore) {
                                highScore = height;
                            }
                        }
                    }


                    curEntry.results.highScore = highScore;
                    curEntry.results.highestHeightAttempted = highestHeightAttempted;
                    if (highScore > curEntry.athlete.pb) {
                        curEntry.athlete.pb = highScore;
                        curEntry.results.isPb = true;
                    }
                    if (highScore > curEntry.athlete.sb) {
                        curEntry.athlete.sb = highScore;
                        curEntry.results.isSb = true;
                    }
                },
                loadBarHeights: function () {
                    this.barHeights = [];
                    this.barHeightsSorted = [];
                    for (var e in this.sortedEntries) {
                        var data = this.sortedEntries[e].results.data;
                        if (typeof data !== "undefined") {
                            for (var r in data) {
                                this.addBarHeight(r);
                            }
                        }
                    }
                    this.setBarHeightSorted();
                },
                addBarHeight: function (height) {
                    var floatR = parseFloat(height);
                    this.barHeights[floatR] = floatR;
                    this.setBarHeightSorted();
                },
                setBarHeightSorted: function () {
                    this.barHeightsSorted = [];
                    for (var h in this.barHeights) {
                        this.barHeightsSorted.push(parseFloat(h));
                    }
                    this.barHeightsSorted.sort();
                },
                getSectionForAthlete: function (athleteId) {
                    var sectionNo = 0;
                    for (var s in this.sections) {
                        var section = this.sections[s];
                        if (typeof section.athletes !== "undefined") {
                            for (var a = 0; a < section.athletes.length; a++) {
                                var entry = section.athletes[a];
                                if (entry.athlete.id === athleteId) {
                                    return sectionNo;
                                }
                            }
                        }
                        sectionNo++;
                    }
                },
                getHeightsForSection: function (section) {
                    return this.sections[section].heights;
                },
                setHeightSections: function () {
                    for (let e = 0; e < this.sortedEntries.length; e++) {
                        var curEntry = this.sortedEntries[e];
                        var sectionObj = null;
                        if (this.sections.length === 0) {
                            sectionObj = {};
                            sectionObj.id = e;
                            sectionObj.athletes = [];
                            // sectionObj.startHeight = curEntry.entry.startHeight;
                            if (this.barHeightsSorted.length <= this.sectionTrials) {
                                sectionObj.heights = this.barHeights;
                                sectionObj.full = false;
                            } else {
                                sectionObj.full = true;
                                sectionObj.heights = [];
                                sectionObj.lastHeight = 0;
                                for (var h = 0; h < this.sectionTrials; h++) {
                                    let sectionHeight = parseFloat(this.barHeightsSorted[h]);
                                    sectionObj.heights[sectionHeight] = sectionHeight;
                                    if (sectionHeight > sectionObj.lastHeight) {
                                        sectionObj.lastHeight = sectionHeight;
                                    }
                                }
                            }
                            this.sections.push(sectionObj);
                        }
                        var athleteAdded = false;
                        for (let section = 0; section < this.sections.length; section++) {
                            sectionObj = this.sections[section];
                            if (sectionObj.full === false) {
                                // add athlete to this section
                                sectionObj.athletes.push(curEntry);
                                athleteAdded = true;
                                break;
                            } else {
                                // check if fits in this section
                                let lastSectionHeight = sectionObj.lastHeight;
                                if (typeof curEntry.results !== "undefined") {
                                    // get athletes highest attempted Height
                                    if (curEntry.entry.startHeight <= lastSectionHeight) {
                                        if (typeof curEntry.results.highestHeightAttempted !== "undefined") {
                                            if (curEntry.results.highestHeightAttempted < lastSectionHeight) {
                                                // add athlete to this section as highest score less than last section height
                                                sectionObj.athletes.push(curEntry);
                                                athleteAdded = true;
                                                break;
                                            }
                                        }
                                    }
                                } else if (curEntry.entry.startHeight === lastSectionHeight) {
                                    sectionObj.athletes.push(curEntry);
                                    athleteAdded = true;
                                    break;
                                }
                            }
                        }
                        // if athlete not added, create a new section
                        if (athleteAdded === false) {
                            sectionObj = {};
                            sectionObj.id = e;
                            sectionObj.athletes = [curEntry];
                            sectionObj.lastHeight = 0;
                            // sectionObj.startHeight = curEntry.entry.startHeight;
                            var sectionCnt = this.sections.length;
                            var barHeights = [];
                            var sectionTrialCount = 0;
                            if (typeof curEntry.results.data !== "undefined") {
                                for (let h in curEntry.results.data) {
                                    let floatH = parseFloat(h);
                                    barHeights[floatH] = floatH;
                                    if (floatH > sectionObj.lastHeight) {
                                        sectionObj.lastHeight = floatH;
                                    }
                                }
                            }
                            sectionObj.heights = barHeights;
                            sectionObj.full = true;
                            if (getObjLength(barHeights) <= this.sectionTrials) {
                                sectionObj.full = false;
                            }
                            this.sections.push(sectionObj);
                        }
                    }
                },
                loseFocus: function () {
                    setTimeout(function () {
                        $("#heightResultOkBtn").focus().blur()
                    }, 1);
                },
                changeHeightIncrement: function (event, ui) {
                    this.increment = ui.value;
                    $(".nextHeightBtn").text(this.getNextHeightText());
                    this.loseFocus();
                    this.writeHeightIncrement();
                },
                writeHeightIncrement: function () {
                    let event = getCurrentEvent();
                    let endPoint = '<?php echo E4S_ROUTE_PATH ?>/event/setinc/' + event.egId;
                    postData(endPoint, {});
                },
                getNextHeightText: function (height) {
                    let text = "Go to ";
                    if (typeof height !== "undefined") {
                        text = "Forward to ";
                    }
                    return text + getDecimals(this.getNextHeight(height), 2) + "m";
                },
                getPreviousHeightText: function (useHeight) {
                    return "Back to " + getDecimals(this.getPreviousHeight(useHeight), 2) + "m";
                },
                sortHeightEntries: function () {
                    var entries = getCurrentEntries();
                    var minSH = 0;
                    var sortedArray = [];
                    // if ( typeof this.sortedEntries !== "undefined" && this.sortedEntries !== null ){
                    //     minSH = this.sortedEntries[0].entry.startHeight;
                    // }
                    for (c in entries) {
                        if (typeof entries[c].entry.startHeight === "undefined") {
                            entries[c].entry.startHeight = minSH;
                        } else if (entries[c].entry.startHeight < minSH) {
                            entries[c].entry.startHeight = minSH;
                        }
                        sortedArray.push(entries[c]);
                    }
                    sortedArray.sort(function (a, b) {
                        return a.entry.startHeight - b.entry.startHeight;
                    });
                    return sortedArray;
                },
                isPreviousHeightAvailable: function (useHeight) {
                    if (typeof useHeight === "undefined") {
                        useHeight = this.getCurrentHeight();
                    }

                    if (useHeight > this.getFirstHeight()) {
                        return true;
                    }
                    return false;
                },
                getFirstHeight: function () {
                    if (typeof this.sortedEntries[0].entry.startHeight === "undefined") {
                        return 0;
                    }
                    return this.sortedEntries[0].entry.startHeight;
                },
                getPreviousHeight: function (useHeight) {
                    if (typeof useHeight === "undefined") {
                        useHeight = this.getCurrentHeight();
                    }
                    for (var h = this.barHeightsSorted.length - 1; h > 0; h--) {
                        if (this.barHeightsSorted[h] === useHeight) {
                            return this.barHeightsSorted[h - 1];
                        }
                    }
                    return this.getFirstHeight();
                },
                getNextHeight: function (height) {
                    if (typeof height !== "undefined") {
                        if (this.barHeightsSorted.length > 0) {
                            for (let h = 0; h < this.barHeightsSorted.length; h++) {
                                if (height === this.barHeightsSorted[h]) {
                                    height = this.barHeightsSorted[h + 1];
                                    break;
                                }
                            }
                        }
                    } else {
                        let currentHeight = this.getCurrentHeight();
                        height = getFloatDecimals(currentHeight + (this.increment / 100), 2);

                        // if all entries have failed, get next startheight
                        var obj = this.getCurrentStatus();
                        if (obj.currentHeightFinished && !obj.atLeastOneSuccessAtHeight) {
                            height = obj.nextStartingHeight;
                        }
                    }
                    return getDecimals(height, 2);
                },
                getLastHeight: function () {
                    let height = this.getCurrentHeight();
                    if (this.barHeightsSorted.length > 0) {
                        height = parseFloat(this.barHeightsSorted[this.barHeightsSorted.length - 1]);
                    }
                    return height;
                },
                showForwardHeight: function (height) {
                    let penHeight = height;
                    if (this.barHeightsSorted.length > 2) {
                        penHeight = parseFloat(this.barHeightsSorted[this.barHeightsSorted.length - 2]);
                    }
                    return height < penHeight;
                },
                getCurrentStatus: function () {
                    let height = this.getLastHeight();
                    let lastHeight = height;
                    let nextStartHeight = <?php echo E4S_HEIGHT_INVALID_HEIGHT ?>;
                    let heightFinished = true;
                    let atLeastOneSuccess = false;
                    let allRetired = true;

                    for (let s = 0; s < this.sortedEntries.length; s++) {
                        let curEntry = this.sortedEntries[s];
                        if (typeof curEntry.results === "undefined" || typeof curEntry.results.data === "undefined") {
                            // Athlete has not results as yet, so check Start Height is greater than current height
                            if (curEntry.entry.startHeight < nextStartHeight && curEntry.entry.startHeight > height) {
                                nextStartHeight = curEntry.entry.startHeight;
                            }
                        }
                    }
                    this.currentHeight = getFloatDecimals(height, 2);
                    if (this.getCurrentHeight() === 0) {
                        lastHeight = 0;
                        height = 0;
                        allRetired = false;
                        heightFinished = false;
                        atLeastOneSuccess = false;
                        nextStartHeight = 0;
                    } else {
                        var athletesAtCurrentHeight = this.getAthletesForCurrentHeight();
                        this.athletesAtCurrentHeight = athletesAtCurrentHeight;
                        for (let s = 0; s < athletesAtCurrentHeight.length; s++) {
                            let curEntry = athletesAtCurrentHeight[s];
                            if (typeof curEntry.results !== "undefined") {
                                let results = curEntry.results;
                                if (typeof results.data !== "undefined") {
                                    let data = results.data;
                                    let lastResult = 1;
                                    if (typeof data[height] !== "undefined") {
                                        // Athlete has at least 1 result at this height
                                        if (typeof results.highestHeightAttempted === "undefined") {
                                            results.highestHeightAttempted = 0;
                                        }
                                        if (results.highestHeightAttempted < height) {
                                            results.highestHeightAttempted = height;
                                        }
                                        for (let r in data[height]) {
                                            if (lastResult < r) {
                                                lastResult = r;
                                            }
                                        }
                                        if (!this.isAttemptRetired(data[height][lastResult])) {
                                            allRetired = false;
                                        }
                                        if (lastResult < 3 && this.isAttemptFailed(data[height][lastResult])) {
                                            heightFinished = false;
                                        }
                                        if (this.canAthleteGoToNextHeight(data, height)) {
                                            atLeastOneSuccess = true;
                                        }
                                    } else {
                                        heightFinished = false;
                                    }
                                } else {
                                    heightFinished = false;
                                }
                            }
                        }
                    }
                    var obj = {};
                    obj.lastHeightWithResults = lastHeight;
                    obj.currentHeight = height;
                    obj.allRetiredAtCurrentHeight = allRetired;
                    obj.currentHeightFinished = heightFinished;
                    obj.atLeastOneSuccessAtHeight = atLeastOneSuccess;
                    obj.nextStartingHeight = nextStartHeight;
                    this.statusObj = obj;
                    return obj;
                },
                setCurrentHeight: function (useHeight) {
                    var obj = this.getCurrentStatus();

                    if (typeof useHeight !== "undefined") {
                        this.currentHeight = useHeight;
                    } else if (!obj.currentHeightFinished) {
                        this.currentHeight = obj.currentHeight;
                    } else if (obj.atLeastOneSuccessAtHeight) {
                        this.currentHeight = obj.currentHeight + (this.increment / 100);
                    } else if (obj.nextStartingHeight !== <?php echo E4S_HEIGHT_INVALID_HEIGHT ?> ) {
                        this.currentHeight = obj.nextStartingHeight;
                    } else {
                        this.currentHeight = obj.lastHeightWithResults;
                    }
                    if (this.currentHeight === 0) {
                        let curEntries = getCurrentEntries();
                        for (c in curEntries) {
                            let entry = curEntries[c];
                            if (entry.entry.startHeight < this.currentHeight || this.currentHeight === 0) {
                                this.currentHeight = entry.entry.startHeight;
                            }
                        }
                    }
                    this.currentHeight = getFloatDecimals(this.currentHeight, 2);
                    this.addBarHeight(this.currentHeight);
                    return this.currentHeight;
                },
                getCurrentHeight: function () {
                    return this.currentHeight;
                },
                getAthletesForCurrentHeight(useHeight) {
                    var athletes = [];
                    if (typeof useHeight === "undefined") {
                        useHeight = this.getCurrentHeight();
                    }
                    useHeight = getFloatDecimals(useHeight, 2);
                    let prevHeight = getFloatDecimals(this.getPreviousHeight(useHeight), 2);
                    for (var e = 0; e < this.sortedEntries.length; e++) {
                        var entry = this.sortedEntries[e];
                        if (entry.entry.present) {
                            if (this.canAthleteJumpHeight(entry, prevHeight, useHeight)) {
                                athletes.push(entry);
                            }
                        }
                    }
                    return athletes;
                },
                isAttemptFailed: function (attemptResult) {
                    if (this.isAttempt(attemptResult, '<?php echo E4S_FIELDCARD_FAILURE ?>')) {
                        return true;
                    }
                    return false;
                },
                isHeightFailed: function (height) {
                    for (let h = 3; h > 0; h--) {
                        if (typeof height[h] !== "undefined") {
                            if (this.isAttemptFailed(height[h])) {
                                return true;
                            }
                            return false;
                        }
                    }
                    return false;
                },
                isAttemptCleared: function (attemptResult) {
                    if (this.isAttempt(attemptResult, '<?php echo E4S_FIELDCARD_SUCCESS ?>')) {
                        return true;
                    }
                    return false;
                },
                isHeightCleared: function (height) {
                    let cleared = false;
                    for (let d in height) {
                        if (this.isAttemptCleared(height[d])) {
                            cleared = true;
                        }
                    }
                    return cleared;
                },
                isAttemptRetired: function (attemptResult) {
                    if (this.isAttempt(attemptResult, '<?php echo E4S_FIELDCARD_RETIRE ?>')) {
                        return true;
                    }
                    return false;
                },
                isHeightRetired: function (height) {
                    let retired = false;
                    for (let d in height) {
                        if (this.isAttemptRetired(height[d])) {
                            retired = true;
                        }
                    }
                    return retired;
                },
                isAttempt: function (attemptResult, checkValue) {
                    if (attemptResult === checkValue) {
                        return true;
                    }
                    return false;
                },
                canAthleteGoToNextHeight: function (data, height) {
                    if (typeof data[height] !== "undefined") {
                        for (let h in data[height]) {
                            if (!this.isAttemptFailed(data[height][h]) && !this.isAttemptRetired(data[height][h])) {
                                return true;
                            }
                        }
                    }
                    return false;
                },
                canAthleteJumpHeight: function (entry, prevHeight, curHeight) {
                    if (entry.entry.startHeight > curHeight) {
                        return false;
                    }
                    if (entry.entry.startHeight === curHeight) {
                        return true;
                    }
                    if (typeof entry.results === "undefined" && typeof entry.results.data === "undefined") {
                        return false; // Not sure if we would ever get here
                    }
                    // has results for curHeight ?
                    var data = entry.results.data;
                    if (typeof data === "undefined") {
                        return true;
                    }
                    if (typeof data[curHeight] !== "undefined") {
                        return true;
                    }
                    // can athlete progress
                    return this.canAthleteGoToNextHeight(data, prevHeight);
                }
            }
        </script>
        <?php
    }
}