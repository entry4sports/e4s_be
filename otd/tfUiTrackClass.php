<?php

class tfUiTrackClass {
    public $compId;

    public function __construct($compId) {
        $this->compId = $compId;
    }

    public static function div($laneCount) {
        ?>
        <div id="dialogTrackResults" style="display:none;"></div>
        <?php
    }

    public function css() {
        ?>
        <style>

        </style>
        <?php
    }

    public function javascript() {
        ?>
        <script>
            function getTrackCardFooterHTML() {
                return '<?php
                    echo $this->generateTrackFooter();
                    ?>';
            }

            function populateTrackCard(eventGroup) {
                getEntriesForEvent(eventGroup, displayAthletesForTrackEvent);
            }

            function displayAthletesForTrackEvent(entries, eventGroup) {
                setCurrentEntries(entries);
                populateCardEventInfo(eventGroup);

                if (isEventSeeded() === false) {
                    // No seeding
                    displayNonSeededEventEntries();
                } else {
                    showTrackCard(eventGroup);
                    setDisplayedCardFooterHTML(getTrackCardFooterHTML());
                    loadTrackEntries();
                }
            }

            function showTrackCard(eventGroup) {
                var competition = getCompetition();
                var laneCount = competition.options.laneCount;
                var trackCardHTML = "";
                if (eventGroup.egOptions.maxInHeat > laneCount) {
                    trackCardHTML = getTrackCardOverHTML();
                } else {
                    trackCardHTML = getTrackCardHTML();
                }
                setDisplayedCardHTML(trackCardHTML);
            }

            function getTrackCardHTML() {
                return '<?php
                    echo $this->generateTrackCardHTML();
                    ?>';
            }

            function getTrackCardOverHTML() {
                return '<?php
                    echo $this->generateTrackCardOverHTML();
                    ?>';
            }

            function setTrackInfo(entry, page) {
                var useHeatNo = entry.heatInfo.heatno;
                var useLaneNo = entry.heatInfo.laneno;
                var pageSize = getTrackHeatCount();
                var pageFactor = (page * pageSize) - pageSize;

                if (confirmed === 1) {
                    useHeatNo = entry.heatInfo.heatnoCheckedIn;
                    useLaneNo = entry.heatInfo.lanenoCheckedIn;
                }
                if (useLaneNo === 0) {
                    return false;
                }

                var minHeatNo = pageFactor + 1;
                var maxHeatNo = (page * pageSize);
                if (useHeatNo < minHeatNo || useHeatNo > maxHeatNo) {
                    return false;
                }
                useHeatNo -= pageFactor;

                var heat_lane = useHeatNo + "_" + useLaneNo;
                var bibSelect = "#bib_" + heat_lane;
                var ageSelect = "#age_" + heat_lane;
                var athleteSelect = "#athlete_" + heat_lane;
                var clubSelect = "#club_" + heat_lane;

                if (entry.athleteid !== 0) {
                    $(bibSelect).text(getBibNoToUse(athlete, entry));
                    $(ageSelect).text(athlete.ageshortgroup);
                    $(athleteSelect).html(getAthleteNameForCard(athlete, entry));
                    $(clubSelect).text(athlete.clubname);
                } else {
                    $(bibSelect).text("");
                    $(ageSelect).text("");
                    if (typeof entry.emptyLane !== "undefined") {
                        $(athleteSelect).text(entry.emptyLane);
                    } else {
                        $(athleteSelect).text("");
                    }

                    $(clubSelect).text("");
                }
                return true;
            }

            function loadTrackEntries() {
                var eventno = getCurrentEvent().egNo;
                var entries = getCurrentEntries();
                var firstEntry = null;
                // var page = getCardPage();
                var eventEntryCnt = entries.length;
                for (var x = 0; x < entries.length; x++) {
                    var entry = entries[x];

                    if (setTrackInfo(entry, athlete, confirmed, page)) {
                        if (firstEntry === null) {
                            firstEntry = entry;
                        }
                    }
                }

                // set Heat Numbers
                $("[trackHeat]").each(function () {
                    var page = getCardPage();
                    var factor = (page - 1) * 10;
                    var heatNo = parseInt($(this).attr("trackHeat"));
                    $(this).text(factor + heatNo);
                });

                // set PageSelector
                if (firstEntry === null) {
                    setPageCounter(1);
                } else {
                    var heatCnt = getTrackHeatCount();
                    // maxInHeat for entries shows track size not maxInHeat count
                    var maxInHeat = eventGroup.options.maxInHeat;
                    if (maxInHeat === 0) {
                        maxInHeat = firstEntry.heatInfo.maxInHeat;
                    }
                    var lanes = maxInHeat;
                    if (firstEntry.heatInfo.useLanes.toUpperCase() !== "A") {
                        lanes = Math.ceil(lanes / 2);
                    }
                    var pageCnt = Math.ceil(eventEntryCnt / (heatCnt * lanes));
                    setPageCounter(pageCnt);

                    var useLanes = firstEntry.heatInfo.useLanes.toUpperCase();
                    if (useLanes !== "A") {
                        for (var lane = 1; lane <= maxInHeat; lane++) {
                            var emptyLane = false;
                            if (useLanes === "E" && lane % 2 === 1) {
                                emptyLane = true;
                            }
                            if (useLanes === "O" && lane % 2 === 0) {
                                emptyLane = true;
                            }
                            if (emptyLane) {
                                entry = {
                                    athleteid: 0,
                                    heatInfo: {
                                        heatno: ((heatCnt * page) - heatCnt) + 1,
                                        laneno: lane
                                    },
                                    emptyLane: "Lane Empty"
                                };
                                setTrackInfo(entry, null, false, page);
                            }
                        }
                    }
                }
            }
        </script>
        <?php
    }

    public function generateTrackFooter() {
        $html = '<table class="card_table" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="track_info_row">';
        $html .= '<span id="track_info">Information</span>';
        $html .= '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td class="card_link_text">';
        $html .= '<span class="card_link_text_span">Entry4Sports Track Card. For more information https://entry4sports.com</span>';
        $html .= '</td>';
        $html .= '</tr>';

        return $html;
    }

    public function generateTrackCardHTML($laneCnt = 8) {
        $labelTDCount = 2;
        $dataTDCount = 2;
        $dataRowCount = 3;
        $heatCnt = 20;
        if (hasSecureAccess()) {
            $heatCnt = 10;
        }

        $html = '<table class="card_table" cellpadding="0" cellspacing="0">';
        // Lane Headers Row
        $html .= '<tr class="card_table_header">';
        $html .= '<td colspan="' . $labelTDCount . '">';
        $html .= '<span>&nbsp;</span>';
        $html .= '</td>';

        for ($x = 1; $x <= $laneCnt; $x++) {
            $html .= '<td colspan="' . $dataTDCount . '" class="track_lane_header_label">';
            $html .= '<span>' . $x . '</span>';
            $html .= '</td>';
        }
        $html .= '</tr>';

//      Heat Row
        for ($heat = 1; $heat <= $heatCnt; $heat++) {
            $html .= '<tr>';
            $html .= '<td rowspan="' . $dataRowCount . '" class="track_data_grid track_heatno_label track_heat_divider">';
            $html .= '<span trackHeat="' . $heat . '">' . $heat . '</span>';
            $html .= '</td>';
            $html .= '<td class="track_data_grid track_heat_labels track_bibno_label">';
            $html .= '<span>Bib #</span>';
            $html .= '</td>';
            for ($laneRow = 1; $laneRow <= $laneCnt; $laneRow++) {
                $html .= '<td class="track_data_grid track_bibno_data">';
                $html .= '<span id="bib_' . $heat . '_' . $laneRow . '">&nbsp;</span>';
                $html .= '</td>';
                $html .= '<td class="track_data_grid track_age_data">';
                $html .= '<span id="age_' . $heat . '_' . $laneRow . '">&nbsp;</span>';
                $html .= '</td>';
            }
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td class="track_data_grid track_heat_labels track_athlete_label">';
            $html .= '<span>Athlete</span>';
            $html .= '</td>';
            for ($laneRow = 1; $laneRow <= $laneCnt; $laneRow++) {
                $html .= '<td colspan="2" class="track_data_grid track_data track_athlete_data">';
                $html .= '<span class="track_athlete_data_text" id="athlete_' . $heat . '_' . $laneRow . '">&nbsp;</span>';
                $html .= '</td>';
            }
            $html .= '</tr>';

            $html .= '<tr class="track_heat_divider">';
            $html .= '<td class="track_data_grid track_heat_labels track_club_label">';
            $html .= '<span>Club</span>';
            $html .= '</td>';
            for ($laneRow = 1; $laneRow <= $laneCnt; $laneRow++) {
                $html .= '<td colspan="2" class="track_data_grid track_data track_club_data">';
                $html .= '<span id="club_' . $heat . '_' . $laneRow . '">&nbsp;</span>';
                $html .= '</td>';
            }
            $html .= '</tr>';
        }

        $html .= '</table>';

        return $html;
    }

    public function generateTrackCardOverHTML($echo = TRUE) {

        $useColCount = 18;
        $html = '<table>';
        $html .= '<tr>';
        $html .= '<td colspan="{$useColCount}">';
        $html .= '<span id="trackOverTableInfo"></span>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div>';

        if ($echo) {
            echo $html;
        }
        return $html;
    }
}