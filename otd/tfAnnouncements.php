<?php

class tfAnnouncementsClass {
    public $compId;

    public function __construct($compId) {
        $this->compId = $compId;
    }

    public function player() {
        require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
        include_once E4S_FULL_PATH . 'dbInfo.php';
        header('Content-Type: text/html; charset=utf-8');

        ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo E4S_PATH ?>/js/articulate.min.js"></script>
        <script>
            function getEventNoSelected() {
                return -1;
            }

            function getCompetition() {
                var comp = {};
                comp.id = <?php echo $this->compId ?>;
                return comp;
            }
        </script>
        <?php
        $this->javascript();
        include_once E4S_FULL_PATH . 'otd/tfSockets.php';
        ?>
        <body>
        <div style="height: 35px;background-color: blue; color: white;">
            Comp <?php echo $this->compId ?> Announcer
        </div>
        <hr>
        <div>
            Listening for inbound messages..... ( Dont forget to set a voice )
        </div>
        <div>When I receive a message, I will display it here and the read it back to you.</div>
        <br>

        <input id="inboundText" style="width:100%;">
        <br>
        <div id="readText" style="display:none;"></div>
        <br>
        <button id="speak" type="button" onclick="speak(true); return false;">
            Speak
        </button>
        <br><br>
        <div id="voiceSelect"></div>
        <script>
            $(document).ready(function () {
                var defaultVoice = 'Google UK English Male';
                $().articulate('getVoices', '#voiceSelect', "Please choose a voice");
            })
        </script>
        <br>
        <div id="historyHeader" style="text-decoration:underline;font-weight: bolder; ">History</div>
        <div id="history"></div>
        </body>
        <script>
            var r4sGlobal = {};
            initSockets(true);
        </script>
        <?php
        exit();
    }

    public function javascript() {
        ?>
        <script>
            function sendAnnouncement(text) {
                var payload = {};
                payload.text = text;
                sendSocketMsg("announcement", payload)
            }

            function makeACardAnnouncement() {
                var curEntries = r4sGlobal.currentEntries;
                var currentEvent = getCurrentEvent();
                var html = "";
                for (var e in curEntries) {
                    html += '<input type="checkbox" id="callAthlete" name="callAthlete" value="' + curEntries[e].athlete.id + '">' + curEntries[e].athlete.name + '<br>';
                }
                html += '<div>';
                html += '<br>';
                html += '<div >Standard Annoucements</div>';
                if (currentEvent.tf === "<?php echo E4S_EVENT_TRACK ?>") {
                    html += '<input type="checkbox" id="callRequest" name="callRequest" value="Track Referee">Track Referee<br>';
                } else {
                    html += '<input type="checkbox" id="callRequest" name="callRequest" value="Field Referee">Field Referee<br>';
                }
                html += '</div>';
                html += '<div>';
                html += '<br>';
                html += '<div >Free Text Annoucements</div>';
                html += '<textarea  id="request" rows="4" cols="30"></textarea><br>';
                html += '</div>';

                var annoucementsDiv = $("#annoucementsDiv");
                annoucementsDiv.html(html);
                annoucementsDiv.dialog({
                    title: "Announcements",
                    resizable: false,
                    height: 600,
                    width: 400,
                    modal: true
                });
                annoucementsDiv.dialog("option", "buttons", {
                    "Make Call": function () {
                        var option = {};
                        option.athlete = [];
                        $("#callAthlete:checked").each(function () {
                            option.athlete.push($(this).val());
                        });
                        option.callRequest = [];
                        $("#callRequest:checked").each(function () {
                            option.callRequest.push($(this).val());
                        });

                        option.request = $("#request").val();
                        makeAnnouncement(option);
                        $(this).dialog("close");
                    },

                    Close: function () {
                        $(this).dialog("close");
                    }
                });

                function makeAnnouncement(option) {
                    athleteAnnouncement(option.athlete);
                    callAnnouncement(option.callRequest);
                    freeTextAnnouncement(option.request);
                }

                function freeTextAnnouncement(message) {
                    if (message === "") {
                        return;
                    }
                    sendAnnouncement(message);
                }

                function getLocationOfRequest() {
                    var currentEvent = getCurrentEvent();
                    var location = "";
                    if (currentEvent.tf === "<?php echo E4S_EVENT_TRACK ?>") {
                        location = "track start";
                    } else {
                        location = currentEvent.egName;
                    }
                    return location;
                }

                function callAnnouncement(request) {
                    if (typeof request === "undefined") {
                        return;
                    }
                    if (request.length === 0) {
                        return;
                    }

                    message = "Could the " + request + " please come to the " + getLocationOfRequest() + " as soon as possible";
                    sendAnnouncement(message);
                }

                function athleteAnnouncement(athlete) {
                    if (typeof athlete === "undefined") {
                        return;
                    }
                    if (athlete.length === 0) {
                        return;
                    }
                    var message = "";
                    var curEntries = getCurrentEntries();

                    if (athlete.length === 1) {
                        var entrySelected = curEntries[parseInt(athlete[0])];
                        message = "Could " + entrySelected.athlete.name + ", bib number " + entrySelected.entry.bibNo + ", please come to the " + getLocationOfRequest() + " immediately";
                    } else {
                        var entries = [];
                        for (var a = 0; a < athlete.length; a++) {
                            entries.push(curEntries[parseInt(athlete[a])]);
                        }
                        entries.sort(function (a, b) {
                            a.entry.bibNo - b.entry.bibNo
                        });
                        message = "Could the following athletes please come to the " + getLocationOfRequest() + " immediately. ";
                        var sep = "";

                        for (var e = 0; e < entries.length; e++) {
                            message += sep;
                            message += "Bib number " + entries[e].entry.bibNo + ", " + entries[e].athlete.name;
                            sep = ", ";
                            if (e === entries.length - 2) {
                                sep = ", and ";
                            }
                        }
                    }
                    if (message !== "") {
                        sendAnnouncement(message);
                    }
                }
            }

            function inboundAnnouncement(payload) {
                setText(payload.text);
                announcementPlay(true);
            }

            function announcementPlay(addToHistory) {
                r4sGlobal.addToHistory = addToHistory;
                $('#speak').click();
            }

            function replay(text) {
                setText(text);
                announcementPlay(false);
            }

            function speak(addToHistory) {
                setText("");
                sayText();
            }

            function sayText() {
                var voiceObj = $('#voiceSelect');
                var textObj = $('#inboundText');
                var readObj = $('#readText');

                $().articulate("setVoice", "name", voiceObj.val());
                readObj.text(textObj.val());
                $().articulate('stop');
                window.setTimeout(function () {
                    readObj.articulate('speak');
                }, 500);
                if (r4sGlobal.addToHistory) {
                    addToHistory();
                }
            }

            function addToHistory() {
                var historyObj = $('#history');
                var textObj = $('#inboundText');
                var text = textObj.val();
                var html = "<a href='#' onclick='replay(\"" + text + "\");'>Replay</a>";
                html += " : " + text + "<br>";
                historyObj.append(html);
            }

            function setText(text) {
                if (text !== "") {
                    $("#inboundText").val(text);
                }
                $('#readText').text($("#inboundText").val());
            }
        </script>
        <?php
    }

    public function div() {
        ?>
        <div id="annoucementsDiv"></div>
        <?php
    }

}