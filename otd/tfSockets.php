<?php
include_once E4S_FULL_PATH . 'e4s_constants.php';
?>
<script>
    function processSocketEvent(data) {
        if (data.payload.domain !== '<?php echo E4S_CURRENT_DOMAIN ?>') {
            // ignore. not for this domain
            return;
        }
        delete data.payload.domain; // dont need it now its checked ok
        if (data.comp.id !== getCompetition().id) {
            return; // not for this competition
        }
        if (data.key !== getEventNoSelected()) {
            if (data.action === "<?php echo R4S_SOCKET_ANNOUNCEMENT ?>") {
                return inboundAnnouncement(data.payload);
            }

            // ignore result as not currently showing
            return;
        }
        r4sGlobal.inboundMessage = true;
        switch (data.action) {
            case "<?php echo R4S_SOCKET_FIELD_RESULT ?>":
                inboundAthleteResult(data.payload);
                break;
            case "<?php echo R4S_SOCKET_ATHLETE_PRESENT ?>":
                inboundAthletePresent(data.payload);
                break;
            case "<?php echo R4S_SOCKET_ADD_ATHLETE ?>":
                inboundAddAthlete(data.payload);
                break;
            case "<?php echo R4S_SOCKET_FIELD_SEEDINGS ?>":
                inboundFieldSeeding(data.payload.data);
                break;
            case "<?php echo R4S_SOCKET_FIELD_STARTHEIGHT ?>":
                inboundFieldStartHeight(data.payload);
                break;
            case "<?php echo R4S_SOCKET_HEIGHT_RESULT ?>":
                inboundHeightResult(data.payload);
                break;
            case "<?php echo R4S_SOCKET_SIGNATURE ?>":
                inboundFieldSignature(data.payload);
                break;
        }
        r4sGlobal.inboundMessage = false;
    }

    function getBaseResultObj() {
        var comp = getCompetition();
        var newPayload = {};
        newPayload.entries = getCurrentEntries();
        newPayload.competition = {};
        newPayload.competition.id = comp.id;
        newPayload.competition.name = comp.name;
        newPayload.competition.logo = "/e4s_logo.png";
        return newPayload;
    }

    function sendSocketMsg(r4sAction, payload) {
        if (payload === null) {
            return;
        }
        var socketObj = {};
        socketObj.action = "sendmessage";
        socketObj.data = {};
        socketObj.data.key = getEventNoSelected();
        socketObj.data.comp = getCompetition();
        socketObj.data.action = r4sAction;
        socketObj.data.deviceKey = r4sGlobal.deviceKey;
        socketObj.data.securityKey = r4sGlobal.securityKey;
        payload.domain = '<?php echo E4S_CURRENT_DOMAIN ?>';
        socketObj.data.payload = JSON.stringify(payload);
        socketObj.data = JSON.stringify(socketObj.data);
        let sendSocketObj = JSON.stringify(socketObj);
        if (r4sGlobal.socket.readyState !== WebSocket.OPEN) {
            // alert("Socket Closed. Attempting to reopen");
            initSockets(r4sGlobal.socketListen, sendSocketObj);
        } else {
            r4sGlobal.socket.send(sendSocketObj);
        }
    }

    function initSockets(listen, sendObj) {
        endpoint = '<?php echo R4S_SOCKET_SERVER ?>'; // E4S

        r4sGlobal.socket = new WebSocket(endpoint);

        r4sGlobal.socket.addEventListener('open', function () {
            console.log("Connection established, handle with event");
            if (typeof sendObj !== "undefined") {
                r4sGlobal.socket.send(sendObj);
            }
        });

        r4sGlobal.socket.onopen = function () {
            console.log("Connection established, handle with function");
        };
        r4sGlobal.socketListen = listen;
        if (listen) {
            r4sGlobal.socket.addEventListener('message', function (m) {
                console.log('message recieved :' + m);

                var socketPayload = JSON.parse(m.data);
                socketPayload.payload = JSON.parse(socketPayload.payload);
                processSocketEvent(socketPayload);
            });
        }
        setTimeout(function () {
            // Amazon timeout is 10 mins
            initSockets(true);
        }, 60000)
    }
</script>

