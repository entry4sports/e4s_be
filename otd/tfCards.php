<?php
//testing api1
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_OTD_PATH . '/tfCardsClass.php';
include_once E4S_OTD_PATH . '/tfUiCardsClass.php';
include_once E4S_OTD_PATH . '/tfUiCommon.php';
include_once E4S_OTD_PATH . '/tfEntriesClass.php';
include_once E4S_OTD_PATH . '/r4sSecurityClass.php';
include_once E4S_OTD_PATH . '/tfAnnouncements.php';
include_once E4S_OTD_PATH . '/uiSignature.php';

function e4s_displayCards($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $deviceCode = checkFieldForXSS($obj, 'device:Device Code');
    if (!is_null($deviceCode)) {
        $GLOBALS[R4S_DEVICE_CODE] = (int)$deviceCode;
    }
    if (is_null($compId)) {
        Entry4UIError(8050, 'No Competition passed');
    }
    $compId = (int)$compId;
    $GLOBALS['R4S_COMP_ID'] = $compId;
    $ui = new tfUiCardsClass($compId);
    exit();
}

function e4s_addAthleteToEvent($obj) {
    $athleteId = checkFieldForXSS($obj, 'athleteId:Athlete ID');
    if (is_null($athleteId)) {
        Entry4UIError(8051, 'No Athlete passed');
    }
    $eventGroupId = checkFieldForXSS($obj, 'eventGroupId:Event Group ID');
    if (is_null($eventGroupId)) {
        Entry4UIError(8052, 'No Event Group passed');
    }
    $ageGroupId = checkFieldForXSS($obj, 'ageGroupId:Age Group ID');
    if (is_null($ageGroupId)) {
        Entry4UIError(8053, 'No Age Group passed');
    }
    $bibNo = checkFieldForXSS($obj, 'bibNo:Bib Number');
    if (is_null($bibNo)) {
        Entry4UIError(8054, 'No Bib Number passed');
    }
    $force = checkFieldForXSS($obj, 'force:Force Addition');
    if (is_null($force)) {
        $force = FALSE;
    }

    $obj = new stdClass();
    $obj->athleteId = (int)$athleteId;
    $obj->eventGroupId = (int)$eventGroupId;
    $obj->bibNo = $bibNo;
    $obj->ageGroupId = (int)$ageGroupId;
    $obj->force = $force;
    $tfEntryObj = new tfEntriesClass($obj);
    $tfEntryObj->addOnDayTrackEntry();
}

function e4s_getBaseCardInfo($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (is_null($compId)) {
        Entry4UIError(7700, 'No Competition passed');
    }
    $cardObj = new tfCardsClass($compId);
    Entry4UISuccess($cardObj->getBaseInfo());
}

function e4s_getEntriesForEventGroupIdArray($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (is_null($compId)) {
        Entry4UIError(7701, 'No Competition passed');
    }
    $tf = 'F';
    $params = $obj->get_params('JSON');
    if (array_key_exists('egIds', $params)) {
        $egIds = $params['egIds'];
    } else {
        $egIds = array();
        foreach ($params as $key => $param) {
            if ($key !== 'compid') {
                $egIds[] = $param;
            }
        }
        if (empty($egIds)) {
            Entry4UIError(7702, 'No Event Groups passed');
        }
    }

    $obj = new stdClass();
    $obj->compId = $compId;
    $obj->tf = $tf;
    $obj->egIds = $egIds;
    $entryObj = new tfEntriesClass($obj);
    $entryObj->processEntriesForEventGroups();
}

function e4s_getEntriesForEventGroupId($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (is_null($compId)) {
        Entry4UIError(7600, 'No Competition passed');
    }
    $tf = checkFieldForXSS($obj, 'tf:Event Type');
    if (is_null($tf)) {
        Entry4UIError(7601, 'No Event Type passed');
    }
    $eventGroupId = checkFieldForXSS($obj, 'eventgroupid:Event Number');
    if (is_null($eventGroupId)) {
        Entry4UIError(7602, 'No Event passed');
    }
    $obj = new stdClass();
    $obj->compId = $compId;
    $obj->tf = $tf;
    $obj->eventGroupId = $eventGroupId;

    $entryObj = new tfEntriesClass($obj);
    $entryObj->processEntriesForEvent();
}
