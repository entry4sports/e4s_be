<?php


class tfUiDistanceClass {
    public $compId;

    public function __construct($compId) {
        $this->compId = $compId;
    }

    public function css($echo = FALSE) {
        $css = '<style>
            .distance_column_header_label{
                border: 1px solid;
                border-bottom: 2px solid;
                font-size: 1vw;
                text-align: center;
                font-weight: 500;
                vertical-align: bottom;
                word-spacing: 1px;
            }
            .distance_card_col_4 {
            }
            .distance_card_col_5 {
            }
            .distance_card_col_14 {
                width:50px;
            }
            .distance_card_col_23 {
                width:50px;
            }
            .distance_data_row {
                line-height: 22px
            }
            .distance_data_col{
                border: 1px solid;
            }
            .distance_data_best {
                font-weight: bolder !important;
                font-size: 1vw !important;
            }
            .distance_data_right_dotted {
                border-right: 1px dotted;
                width: 30px;
                font-size: 0.9vw;
                text-align: right;
                padding-right: 3px;
                font-weight: normal;
            }
            .distance_data_left_dotted {
                border-left: 1px dotted;
                width: 30px;
                font-size: 0.9vw;
                text-align: center;
                font-weight: normal;
            }
            .distance_data_pos {
                font-size: 1vw;
                text-align: center;
                font-weight: bolder;
            }
            .distanceEntry {
                width: 60px;
                font-size: 0.8em !important;
            }
        </style>';
        if ($echo) {
            echo $css;
        }
        return $css;
    }

    public function javascript() {
        if (hasScoreboardAccess()) {
            ?>
            <script>
                function trialDistanceResults(trial) {
                    if (typeof trial === "undefined") {
                        trial = 1;
                    }
                    var entries = getCurrentEntries(true);
                    if (!isEventSeeded(entries)) {
                        return showSeedingDialog();
                    }
                    var entriesCount = Object.size(entries);
                    var prefix = "resultByt";
                    var divName = 'resultsByTrial';
                    var divHTML = "<div id='" + divName + "'>";
                    divHTML += "<table>";
                    var sub = 1;
                    for (var e in entries) {
                        var entry = entries[e];
                        var fieldid = prefix + sub;
                        var value = returnFieldResult("t" + trial, sub);
                        var last = entriesCount === sub;
                        var rowClass = "";
                        if (sub % 2 == 0) {
                            rowClass = "trialAltRow";
                        }
                        divHTML += "<tr class='" + rowClass + "'>";
                        divHTML += "<td class='trialResultsTd' >";
                        var present = entry.entry.present;
                        var displayPresent = "";
                        var displayAbsent = "none";
                        var disabled = "";
                        if (!present) {
                            displayPresent = "none";
                            displayAbsent = "";
                            disabled = " disabled=true ";
                        }
                        divHTML += '<input type="checkbox" id="' + getResultPresent() + entry.athlete.id + '" name="' + getResultPresent() + entry.athlete.id + '" value="1"';
                        if (entry.entry.present) {
                            divHTML += ' checked ';
                        }
                        divHTML += ' onchange="fieldResultPresent(' + entry.athlete.id + ',\'' + prefix + '\',' + trial + ',' + sub + ');"';
                        divHTML += '>';
                        divHTML += "</td>";
                        divHTML += "<td class='trialBibCol trialResultsTd'>";
                        var athlete = entry.athlete;
                        divHTML += getBibNo(entry.entry.bibNo, entry.entry);
                        divHTML += "</td>";
                        divHTML += "<td class='trialAthleteCol trialResultsTd ";
                        if (present) {
                            divHTML += getPresetClass();
                        } else {
                            divHTML += getNotPresetClass();
                        }
                        divHTML += "'>";
                        divHTML += "<span id='" + getResultAthleteField(prefix, trial, sub) + "' name='" + getResultAthleteField(prefix, trial, sub) + "'>" + athlete.name + "</span>";
                        divHTML += "</td>";
                        divHTML += "<td class='trialFieldCol trialResultsTd'>";
                        divHTML += "<input " +
                            " " + getResultAthleteIdAttr() + "='" + athlete.id + "' " +
                            " lastathlete=" + last +
                            " autocomplete='off'" +
                            " " + getResultSubAttr() + "=" + sub +
                            " " + getResultTrialAttr() + "=" + trial +
                            " " + getResultPrefixAttr() + "=" + prefix +
                            " e4sEntryId=" + entry.entry.id +
                            disabled +
                            " value='" + value + "'" +
                            " class='distanceEntry' tabindex=" + sub +
                            " id='" + getResultScoreField(prefix, trial, sub) + "'" +
                            " onkeypress=\"resultKeyPress('" + prefix + "'," + trial + "," + sub + "," + last + ")\" " +
                            " onpaste=\"resultPasted('" + prefix + "'," + trial + "," + sub + "," + last + ")\" " +
                            " onblur=\"distanceResultBlur('" + prefix + "'," + trial + "," + sub + ")\" " +
                            " onfocus=\"distanceResultFocus('" + prefix + "'," + trial + "," + sub + ")\" " +
                            ">";
                        divHTML += "<span id=\"" + getResultOptionsArea(prefix, trial, sub) + "\"";
                        if (!present) {
                            divHTML += " style=\"display:none;\"";
                        }
                        divHTML += ">";
                        divHTML += " <button id='" + getResultShowButton(prefix, trial, sub) + "' class='ui-button ui-corner-all ui-widget resultShow' ";
                        divHTML += " onclick='writeDistanceResult(\"" + prefix + "\"," + trial + "," + sub + "); return false;'";
                        divHTML += ">Show</button>";
                        divHTML += "<select ";
                        divHTML += "e4sOptions=true class='resultClear' name='" + getResultOptions(prefix, trial, sub) + "' id='" + getResultOptions(prefix, trial, sub) + "'></select>";
                        divHTML += "</span>";
                        divHTML += "</td>";
                        divHTML += "</tr>";
                        sub++;
                    }
                    divHTML += "</table>";
                    divHTML += "</div>";
                    // remove if already been shown before
                    $("#" + divName).remove();
                    $("#<?php echo E4S_CARD_RESULTS_DIV ?>").html(divHTML);
                    dialog = $("#" + divName).dialog({
                        resizable: true,
                        title: "Results for Trial " + trial,
                        height: "auto",
                        width: 550,
                        modal: true
                    });
                    buttons = [
                        {
                            text: "Ok",
                            class: "resultButton",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ];
                    if (trial > 1) {
                        buttons.push({
                            text: "Previous Trial",
                            class: "resultButton floatLeft",
                            click: function () {
                                $(this).dialog("close");
                                cardResults(trial - 1);
                            }
                        })
                    }
                    if (trial < 6) {
                        buttons.push({
                            text: "Next Trial",
                            class: "resultButton floatLeft",
                            click: function () {
                                $(this).dialog("close");
                                cardResults(trial + 1);
                            }
                        })
                    }
                    dialog.dialog("option", "buttons", buttons);
                    setValidOptions(false);
                    moveToFirstResult(prefix, trial);
                }

                function distanceResultFocus(prefix, trial, element) {
                    var fieldObj = $("#" + getResultScoreField(prefix, trial, element));
                    fieldObj.val(fieldObj.val().trim());
                    fieldObj.attr(getResultSavedValue(), fieldObj.val());
                }

                function distanceResultBlur(prefix, trial, element) {
                    var fieldObj = $("#" + getResultScoreField(prefix, trial, element));
                    if (typeof fieldObj.attr(getResultSavedValue()) !== "undefined") {
                        fieldObj.val(fieldObj.val().trim());
                        var curVal = fieldObj.val();
                        if (fieldObj.attr(getResultSavedValue()) !== curVal) {
                            writeDistanceResult(prefix, trial, element);
                        }
                    }
                }

                function writeDistanceResult(prefix, trial, row) {
                    var fieldObj = $("#" + getResultScoreField(prefix, trial, row));
                    var val = fieldObj.val().trim().toLowerCase();
                    var validChars = getValidOptionChars();
                    var included = false;
                    for (var v = 0; v < validChars.length; v++) {
                        if (validChars[v] === val) {
                            included = true;
                            break;
                        }
                    }
                    if (!included) {
                        val = getFloatDecimals(fieldObj.val(), 2);
                        if (isNaN(val)) {
                            fieldObj.val(fieldObj.attr(getResultSavedValue()));
                            fieldObj.focus();
                            return;
                        }
                        fieldObj.val(val);
                    }

                    var eventno = getEventNoSelected();
                    var athleteId = parseInt(fieldObj.attr("athleteid"));

                    writeDistanceResults(athleteId, eventno, val, trial);
                }

                function writeDistanceResults(athleteId, eventNo, result, trial) {
                    var curEntry = getCurEntry(athleteId);
                    curEntry.results.trial = trial;
                    // curEntry.results.data['t' + trial] = getFloatDecimals(result,2);
                    curEntry.results.data['t' + trial] = result;
                    let positions = evaluateFieldPositions();
                    var payload = {
                        "compid": getCompetition().id,
                        "entry": curEntry,
                        "positions": positions
                    };

                    var endPoint = '<?php echo R4S_POST_RESULT ?>';
                    postData(endPoint, payload, sendDistanceResultToSocket);
                }

                function sendDistanceResultToSocket(payload) {
                    if (payload === null) {
                        return; // Need to track why this happens
                    }
                    var maxTrials = 6; // needs to be got from config somewhere
                    var newPayload = getBaseResultObj();
                    var score = {};
                    var entry = payload.entry;
                    score.athleteId = entry.athlete.id;
                    score.athlete = entry.athlete;
                    score.eventGroup = entry.eventGroup;
                    score.trial = entry.results.trial;
                    score.score = entry.results.data['t' + score.trial];
                    score.scoreText = "";
                    if (score.score === entry.results.highScore) {
                        score.scoreText = entry.results.scoreText;
                    }
                    score.nextAthleteId = getNextAthleteForDistance(score.athleteId, score.trial, maxTrials);
                    newPayload.score = score;

                    sendSocketMsg("<?php echo R4S_SOCKET_FIELD_RESULT ?>", newPayload);
                }

                function getNextAthleteForDistance(athleteId, trial, maxTrials) {
                    var curEntries = getCurrentEntries();
                    var curAthletePosition = curEntries[athleteId].heatInfo.position;
                    var firstAthleteId = athleteId;

                    for (e in curEntries) {
                        var checkAthleteId = parseInt(e);
                        var entry = curEntries[checkAthleteId];
                        if (entry.entry.present) {
                            if (entry.heatInfo.position === curAthletePosition + 1) {
                                return checkAthleteId;
                            }
                            if (entry.heatInfo.position < curEntries[firstAthleteId].heatInfo.position) {
                                firstAthleteId = checkAthleteId;
                            }
                        }
                    }
                    if (trial < maxTrials) {
                        return firstAthleteId;
                    }
                    return 0;
                }
            </script>
            <?php
        }
        ?>
        <script>
            function getDistanceCardHTML() {
                return '<?php
                    echo $this->generateDistanceCardHTML();
                    ?>';
            }

            function loadFieldDistanceEntries() {
                var fromRow = 1;
                var maxRows = 30;
                var entries = getCurrentEntries();

                for (const x in entries) {
                    var entry = entries[x];
                    setFieldDistanceInfo(entry);
                    if (entry.heatInfo.position >= fromRow) {
                        fromRow = entry.heatInfo.position + 1;
                    }
                }

                for (var r = fromRow; r < maxRows; r++) {
                    var bibSelect = "#field_bib_" + r;
                    if ($(bibSelect).length > 0) {
                        clearFieldDistanceInfo(r);
                    } else {
                        // row does not exist
                        r = maxRows + 1;
                        break;
                    }
                }
            }

            function clearFieldDistanceInfo(row) {
                var bibSelect = "#field_bib_" + row;
                var ageSelect = "#field_age_" + row;

                var athleteSelect = "#field_name_" + row;
                var clubSelect = "#field_club_" + row;
                $(bibSelect).text("");
                $(ageSelect).text("");

                $(athleteSelect).text("");
                $(clubSelect).text("");
                updateUIFieldResults([], row);
            }

            function setFieldDistanceInfo(entry) {
                var gender = "Male";
                var athlete = entry.athlete;
                if (athlete.gender.toUpperCase() === "F") {
                    gender = "Female";
                }

                var useHeatNo = entry.heatInfo.heatNo;
                var position = entry.heatInfo.position;

                var bibSelect = "#field_bib_" + position;
                var ageSelect = "#field_age_" + position;
                var genderSelect = "#field_gender_" + position;
                var athleteSelect = "#field_name_" + position;
                var clubSelect = "#field_club_" + position;

                markUIFieldRowPresent($(bibSelect), entry.entry.present);

                $(bibSelect).text(getBibNo(entry.entry.bibNo, entry.entry));
                $(genderSelect).text(gender);
                var ageGroup = entry.ageGroup.eventAgeGroup;
                if (ageGroup === "") {
                    ageGroup = entry.ageGroup.ageGroup;
                }
                $(ageSelect).text(ageGroup);
                $(athleteSelect).html(getAthleteNameForCard(entry));

                $(clubSelect).text(athlete.club);

                updateDistanceCardWithResults(entry);
            }
        </script>
        <?php
    }

    public function generateDistanceCardHTML($athleteCount = 24) {
        $html = '<table id="distanceCardTable" class="card_table" cellpadding="0" cellspacing="0">';
        // distance Card Column Titles
        $html .= "<tr class=\"card_table_header\">";
        $html .= $this->generateDistanceCardColHTML('Bib', 1, 1, 'card_bib');
//    $html .= $this->generateFieldDistanceCardColHTML("Gender", 2,1,"card_gender");
        $html .= $this->generateDistanceCardColHTML('Age', 3, 1, 'card_age');
        $html .= $this->generateDistanceCardColHTML('Athlete', 4, 1, 'card_name');
        $html .= $this->generateDistanceCardColHTML('Club', 5, 1, 'card_club');
        $html .= $this->generateDistanceCardColHTML('1st Trial', 6, 2, '', 1);
        $html .= $this->generateDistanceCardColHTML('2nd Trial', 8, 2, '', 2);
        $html .= $this->generateDistanceCardColHTML('3rd Trial', 10, 2, '', 3);
        $html .= $this->generateDistanceCardColHTML('Best Trial', 12, 2);
        $html .= $this->generateDistanceCardColHTML('3 Trial Position', 14, 1);
        $html .= $this->generateDistanceCardColHTML('4th Trial', 15, 2, '', 4);
        $html .= $this->generateDistanceCardColHTML('5th Trial', 17, 2, '', 5);
        $html .= $this->generateDistanceCardColHTML('6th Trial', 19, 2, '', 6);
        $html .= $this->generateDistanceCardColHTML('Best Trial', 21, 2);
        $html .= $this->generateDistanceCardColHTML('Final Position', 23, 1);
        $html .= '</tr>';

        $leftDotted = 'distance_data_left_dotted';
        $rightDotted = 'distance_data_right_dotted';
        $best = ' distance_data_best ';
        $pos = 'distance_data_pos';
        for ($x = 1; $x <= $athleteCount; $x++) {
            $html .= "<tr class=\"distance_data_row\">";
            $html .= $this->generateDistanceCardDataHTML('bib', $x, 'card_bib field_data card_data');
//        $html .= $this->generateDistanceCardDataHTML("gender", $x,"card_gender field_data card_data");
            $html .= $this->generateDistanceCardDataHTML('age', $x, 'card_age field_data card_data');
            $html .= $this->generateDistanceCardDataHTML('name', $x, 'card_name field_data_large card_data_left');
            $html .= $this->generateDistanceCardDataHTML('club', $x, 'card_club field_data_large card_data_left');
            $html .= $this->generateDistanceCardDataHTML('t1m', $x, $rightDotted);
            $html .= $this->generateDistanceCardDataHTML('t1c', $x, $leftDotted);
            $html .= $this->generateDistanceCardDataHTML('t2m', $x, $rightDotted);
            $html .= $this->generateDistanceCardDataHTML('t2c', $x, $leftDotted);
            $html .= $this->generateDistanceCardDataHTML('t3m', $x, $rightDotted);
            $html .= $this->generateDistanceCardDataHTML('t3c', $x, $leftDotted);
            $html .= $this->generateDistanceCardDataHTML('b3m', $x, $rightDotted . $best);
            $html .= $this->generateDistanceCardDataHTML('b3c', $x, $leftDotted . $best);
            $html .= $this->generateDistanceCardDataHTML('3pos', $x, $pos);
            $html .= $this->generateDistanceCardDataHTML('t4m', $x, $rightDotted);
            $html .= $this->generateDistanceCardDataHTML('t4c', $x, $leftDotted);
            $html .= $this->generateDistanceCardDataHTML('t5m', $x, $rightDotted);
            $html .= $this->generateDistanceCardDataHTML('t5c', $x, $leftDotted);
            $html .= $this->generateDistanceCardDataHTML('t6m', $x, $rightDotted);
            $html .= $this->generateDistanceCardDataHTML('t6c', $x, $leftDotted);
            $html .= $this->generateDistanceCardDataHTML('b6m', $x, $rightDotted . $best);
            $html .= $this->generateDistanceCardDataHTML('b6c', $x, $leftDotted . $best);
            $html .= $this->generateDistanceCardDataHTML('pos', $x, $pos);
            $html .= '</tr>';
        }

        $html .= '</table>';

        return $html;
    }

    public function generateDistanceCardColHTML($text, $colNumber, $colSpan = 1, $extraClass = '', $trial = 0) {
        $html = '';
        $html .= "<td colspan=\"{$colSpan}\" class=\"distance_column_header_label distance_card_col_{$colNumber} {$extraClass}\">";
//     Needs EDM Access not full Access
        $html .= "<span>{$text}</span>";
        $html .= '</td>';

        return $html;
    }

    public function generateDistanceCardDataHTML($text, $rowNumber, $extraClass = '') {
        $html = '';
        $html .= "<td class=\"distance_data_col distance_data_{$text} {$extraClass}\">";
        $html .= "<span id=\"field_{$text}_{$rowNumber}\">&nbsp;</span>";
        $html .= '</td>';

        return $html;
    }
}