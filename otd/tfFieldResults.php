<?php

class tfFieldResultsClass {
    public function __construct() {
    }

    public static function div() {
        ?>
        <div id="<?php echo E4S_CARD_RESULTS_DIV ?>" style="display:none;"></div>
        <div id="<?php echo E4S_CARD_ATTEMPT_DIV ?>" style="display:none;"></div>
        <?php
    }

    public static function javascript() {
        if (hasScoreboardAccess()) {
            ?>
            <script>

                function getResultScoreField(prefix, trial, element) {
                    return setScoreFieldName(prefix + element);
                }

                function setScoreFieldName(field) {
                    return field + "Score";
                }

                function getResultShowButton(prefix, trial, element) {
                    return prefix + element + "ShowButton";
                }

                function getResultOptions(prefix, trial, element) {
                    return prefix + element + "Options";
                }

                function getResultsScoreFieldFromOptionsField(optionsField) {
                    var strippedFieldName = optionsField.replace(/Options/g, "");
                    return setScoreFieldName(strippedFieldName);
                }

                function getResultOptionsArea(prefix, trial, element) {
                    return getResultOptions(prefix, trial, element) + "Span";
                }

                function fieldResultPresent(athleteId, prefix, trial, row) {
                    // NB: As this is called on change, the present flag will be what it WAS before the change
                    var curEntry = getCurEntry(athleteId);
                    disableResultRow(athleteId, prefix, trial, row, !curEntry.entry.present);
                    issueAthletePresentUpdate(athleteId);
                }

                function disableResultRow(athleteId, prefix, trial, row, present) {
                    $("#" + getResultScoreField(prefix, trial, row)).prop("disabled", !present);
                    $("#" + getResultPresent() + athleteId).prop("checked", present);
                    var athleteElement = $("#" + getResultAthleteField(prefix, trial, row));
                    if (present) {
                        athleteElement.removeClass(getNotPresetClass());
                        athleteElement.addClass(getPresetClass());
                    } else {
                        athleteElement.addClass(getNotPresetClass());
                        athleteElement.removeClass(getPresetClass());
                    }
                    $("#" + getResultOptionsArea(prefix, trial, row)).toggle();
                }

                function getValidOptionChars() {
                    var arr = [];
                    for (var v = 0; v < validOptions.length; v++) {
                        var option = validOptions[v].split(":");
                        arr.push(option[0]);
                    }
                    return arr;
                }

                function resultKeyPress(prefix, trial, element) {
                    if (event.keyCode === 13 || event.keyCode === 9) {
                        moveToNextResult(prefix, trial, element);
                        return false;
                    }
                    return true;
                }

                function setResultFromOptions(field, value) {
                    var fieldObj = $("#" + getResultsScoreFieldFromOptionsField(field));
                    // save old value
                    fieldObj.attr(getResultSavedValue(), fieldObj.val());

                    // Does the field have attribs that means move to next
                    var trial = parseInt(fieldObj.attr(getResultTrialAttr()));
                    if (typeof trial === "undefined") {
                        return;
                    }
                    fieldObj.focus();
                    fieldObj.val(value);
                    var prefix = fieldObj.attr(getResultPrefixAttr());
                    if (typeof prefix === "undefined") {
                        return;
                    }
                    var sub = parseInt(fieldObj.attr(getResultSubAttr()));
                    if (typeof sub === "undefined") {
                        return;
                    }
                    var athleteId = parseInt(fieldObj.attr(getResultAthleteIdAttr()));
                    if (typeof athleteId === "undefined") {
                        return;
                    }
                    if (value === getResultRetired()) {
                        fieldResultPresent(athleteId, prefix, trial, sub);
                    }
                    moveToNextResult(prefix, trial, sub);
                }

                function isLastElement(fieldObj) {
                    return fieldObj.attr("lastathlete") === "true";
                }

                function moveToFirstResult(prefix, trial) {
                    var curObj = $("#" + getResultScoreField(prefix, trial, 1));
                    if (!curObj.prop("disabled")) {
                        curObj.focus();
                        curObj.select();
                    } else {
                        moveToNextResult(prefix, trial, 1);
                    }
                }

                function moveToNextResult(prefix, trial, element) {

                    var curObj = $("#" + getResultScoreField(prefix, trial, element));
                    var lastValidObj = $("#" + getResultScoreField(prefix, trial, element++));
                    var finished = isLastElement(curObj);
                    var movetoNextTrial = finished;
                    while (!finished) {
                        curObj = $("#" + getResultScoreField(prefix, trial, element++));

                        if (curObj.length === 0) {
                            finished = true; // Stop, something is wrong
                        } else {
                            if (!curObj.prop("disabled")) {
                                curObj.focus();
                                curObj.select();
                                finished = true;
                            } else {
                                finished = isLastElement(curObj);
                                movetoNextTrial = finished;
                            }
                        }
                    }

                    // Number of trials to be configured
                    if (trial === 6 && finished) {
                        lastValidObj.blur(); // force update
                    }
                    if (trial < 6 && movetoNextTrial) {
                        cardResults(trial + 1);
                    }

                    return element;
                }

                function resultPasted(prefix, trial, element, isLastElement) {
                    setTimeout(function () {
                        // timeout to allow paste to occur
                        moveToNextResult(prefix, trial, element, isLastElement);
                        setTimeout(function () {
                            $(this).select();
                        }, 100);
                    }, 4);
                }

                function returnFieldResult(key, row) {
                    var addM = "m";
                    var m = $("#field_" + key + addM + "_" + row).text().trim();
                    var dot = "";
                    var c = "";
                    if (!isValValidOption(m)) {
                        // must be a number
                        if (isNaN(parseInt(m))) {
                            m = "";
                        } else {
                            if (m !== "") {
                                dot = ".";
                            }
                            c = $("#field_" + key + "c_" + row).text().trim();
                        }
                    }
                    return m + dot + c;
                }

                function setValidOptions() {
                    var optionsSelector = $("[e4sOptions=true]");
                    optionsSelector.children("option").remove();
                    optionsSelector.append("<option disabled selected value=''>Select option</option>");

                    for (v = 0; v < validOptions.length; v++) {
                        options = validOptions[v].split(":");
                        optionsSelector.append("<option value='" + options[0] + "'>" + options[1] + "</option>");
                    }
                    try {
                        optionsSelector.selectmenu("destroy");
                    } catch (e) {

                    }

                    optionsSelector.selectmenu({
                        classes: {
                            "ui-selectmenu-button": "resultClear"
                        },
                        change: function (event, data) {
                            var val = data.item.value;
                            var field = event.target.id;
                            setResultFromOptions(field, val);
                        }
                    });

                }
            </script>
            <?php
        }
    }
}
