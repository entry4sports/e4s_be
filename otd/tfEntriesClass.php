<?php

class tfEntriesClass {
    public $athleteId;
    public $athleteRow;
    public $bibNo;
    public $eventGroupId;
    public $eventNo;
    public $egIds;
    public $eventGroupRow;
    public $ageGroupId;
    public $tf;
    public $ageGroup;
    public $ceRow;
    public $compId;
    public $entryId;
    public $startHeight;
    public $force;

    public function __construct($obj) {
        if (isset($obj->athleteId)) {
            $this->athleteId = (int)$obj->athleteId;
        } else {
            $this->athleteId = 0;
        }
        $this->force = FALSE;
        if (isset($obj->force)) {
            $this->force = $obj->force;
        }
        if (isset($obj->bibNo)) {
            $this->bibNo = (int)$obj->bibNo;
        }
        if (isset($obj->ageGroupId)) {
            $this->ageGroupId = (int)$obj->ageGroupId;
        }
        if (isset($obj->ageGroup)) {
            $this->ageGroup = (int)$obj->ageGroup;
        }
        if (isset($obj->eventGroupId)) {
            $this->eventGroupId = (int)$obj->eventGroupId;
        }
        if (isset($obj->egIds)) {
            $this->egIds = $obj->egIds;
        }
        if (isset($obj->tf)) {
            $this->tf = $obj->tf;
        }
        if (isset($obj->compId)) {
            $this->compId = (int)$obj->compId;
        }
        if (isset($obj->entryId)) {
            $this->entryId = (int)$obj->entryId;
        }
        if (isset($obj->startHeight)) {
            $this->startHeight = $obj->startHeight;
        }
    }

    public function addOnDayTrackEntry() {
        $this->getAthlete();
        $this->getCEvent();
        $this->getEventGroup();
        $entryId = $this->addEntry();
        $data = new stdClass();
        $data->athleteId = $this->athleteId;
        $data->entryId = $entryId;
        $data->clubName = $this->athleteRow['club'];
        $data->ageGroupId = $this->ageGroupId;
        $data->eventGroup = $this->eventGroupRow;
        $data->ceObj = $this->ceRow;
        $this->_createNewSeeding();
        $this->sendSocketInfo($data, R4S_SOCKET_ENTRIES_ADD_ATHLETE);
        Entry4UISuccess();
    }

    private function getAthlete() {
        $athleteObj = athleteClass::withID($this->athleteId);
        $this->athleteRow = $athleteObj->getRow(FALSE);
    }

    private function getCEvent() {
        $results = $this->_getCEEvents();

        if ($results->num_rows === 0 and $this->force) {
            $results = $this->_createCEEvent();
        }
        if ($results->num_rows !== 1) {
            Entry4UIError(8500, 'Event is not defined for athletes gender/age');
        }
        $row = $results->fetch_assoc();
        $row['compid'] = (int)$row['compid'];
        $this->compId = $row['compid'];
        $row['id'] = (int)$row['id'];
        $this->ceRow = $row;
    }

    private function _getCEEvents() {
        $sql = 'select ce.id id, ce.compid compid, eg.startdate startdate
                from ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTGROUPS . ' eg,
                     ' . E4S_TABLE_EVENTS . ' e
                where agegroupid = ' . $this->ageGroupId . '
                and   maxgroup = ' . $this->eventGroupId . "
                and   ce.maxgroup = eg.id
                and   ce.eventid = e.id
                and   e.gender = '" . $this->athleteRow['gender'] . "'";
        return e4s_queryNoLog($sql);
    }

    private function _createCEEvent() {
//        create the CE Record
        // get all/any ce records for eventgroup
        $sql = 'select ce.*
                from ' . E4S_TABLE_COMPEVENTS . ' ce,
                     ' . E4S_TABLE_EVENTS . ' e
                where ce.maxgroup = ' . $this->eventGroupId . "
                and   ce.eventid = e.id
                and   e.gender = '" . $this->athleteRow['gender'] . "'";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows < 1) {
            return $result;
        }
//        get first row
        $row = $result->fetch_assoc();
//        create with new agegroup
        $sql = 'insert into ' . E4S_TABLE_COMPEVENTS . '(';
        $sep = '';
        $values = '';
        foreach ($row as $col => $value) {
            if (strtolower($col) !== 'id') {
                $sql .= $sep . $col;
                if (strtolower($col) === 'options') {
                    $value = addslashes($value);
                }
                if (strtolower($col) === 'agegroupid') {
                    $value = $this->ageGroupId;
                }
                $values .= $sep . "'" . $value . "'";
                $sep = ',';
            }
        }
        $sql .= ') values (';
        $sql .= $values;
        $sql .= ')';
        e4s_queryNoLog($sql);
        return $this->_getCEEvents();
    }

    private function getEventGroup() {
        $sql = 'select * 
                from ' . E4S_TABLE_EVENTGROUPS . '
                where id = ' . $this->eventGroupId;
        $result = e4s_queryNoLog($sql);
        $this->eventGroupRow = array();
        if ($result->num_rows !== 1) {
            $this->eventGroupRow['id'] = 0;
            $this->eventGroupRow['name'] = 'unknown';
            $this->eventGroupRow['eventno'] = 0;
        } else {
            $row = $result->fetch_assoc();
            $this->eventGroupRow['id'] = (int)$row['id'];
            $this->eventGroupRow['name'] = $row['name'];
            $this->eventGroupRow['eventno'] = (int)$row['eventNo'];
        }
    }

    private function addEntry() {
        $error = '';
        $newId = null;
// Check it does not already exist
        $sql = 'select id 
                from ' . E4S_TABLE_ENTRIES . '
                where athleteid = ' . $this->athleteId . '
                and   compeventid = ' . $this->ceRow['id'];
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows > 0) {
            $error = 'Duplicate entry.';
        } else {
            $options = new stdClass();
            $options->fee = TRUE;
            $options->otd = TRUE;
            $options->reason = 'At event entry';
            $insertSQL = 'INSERT INTO ' . E4S_TABLE_ENTRIES . " ( options,compEventID, athlete, athleteid, pb, variationid, clubid, paid, price, discountid, agegroupid, vetagegroupid, eventAgeGroup, created,teamid, userid, checkedin, present) VALUES 
                  ('" . e4s_getOptionsAsString($options) . "'," . $this->ceRow['id'] . ",'" . $this->athleteRow['firstName'] . ' ' . $this->athleteRow['surName'] . "'," . $this->athleteId . ',0,0,' . $this->athleteRow['clubid'] . ',' . E4S_ENTRY_PAID . ",0,0,$this->ageGroupId,0,'" . $this->ageGroup . "'," . returnNow() . ',0,' . e4s_getUserID() . ',1,1)';
            if (e4s_queryNoLog($insertSQL) !== TRUE) {
                $error = 'Error inserting Entry Competition';
            } else {
                $newId = e4s_getLastID();
                logInsert();
            }
        }
        if ($error !== '') {
            Entry4UIError(8550, $error, 200);
        }
        return $newId;
    }

    private function _createNewSeeding() {
        $compId = $this->compId;
        $egId = $this->eventGroupRow['id'];
        $seedingObj = new seedingV2Class($compId, $egId);
        $seedingObj->insertNonSeededAthletes([$this->athleteId]);
    }

    public function sendSocketInfo($data, $socketType) {
        $socket = new socketClass($this->compId);
//        $payload = new stdClass();
//        $payload->data = $data;
        $socket->setPayload($data);
        $socket->sendMsg($socketType);
    }

    public function addOnDayEntry() {
        $this->getAthlete();
        $this->getCEvent();
        $this->getEventGroup();
        $this->addEntry();
        $this->athleteId = 0;
        $this->processEntriesForEvent();
    }

    public function processEntriesForEvent($exit = TRUE) {
        $sql = $this->getCompEntrySQL();
        $results = e4s_queryNoLog($sql);
        $posArray = array();
        $rows = array();
        $position = 1;
        $pbKeys = array();
        $heightEvent = FALSE;
        $eventType = '';
        while ($row = $results->fetch_object()) {
            $key = (int)$row->athleteId;
            if (!array_key_exists($key, $rows)) {
                $pbKeys[] = $key . '-' . $row->eventId;
                $addRow = new stdClass();
                $entry = new stdClass();
                $entry->pb = $row->pb;
                $entry->id = $row->entryId;
                $entry->bibNo = $row->bibNo;
                $entry->teambibno = $row->teambibno;
                if ($entry->bibNo === 0 or is_null($entry->bibNo)) {
                    $entry->bibNo = E4S_NO_BIBNO;
                }

                $entry->checkedIn = (bool)$row->checkedIn;
                $entry->collected = (bool)$row->collected;
                $entry->present = (bool)$row->present;
                $eOptions = e4s_getDataAsType($row->options, E4S_OPTIONS_OBJECT);
                if ($eventType === '') {
                    $eventType = $this->getEventType($row->eventId);
                }
                if ($eventType === E4S_UOM_HEIGHT) {
                    if (isset($eOptions->startHeight)) {
                        $entry->startHeight = $eOptions->startHeight;
                    }
                    $heightEvent = TRUE;
                }
                $addRow->entry = $entry;

                $eventGroup = new stdClass();
                $eventGroup->id = $this->eventGroupId;
                $eventGroup->eventNo = (int)$row->eventNo;
                $this->eventNo = $eventGroup->eventNo;
                $eventGroup->groupName = $row->eventGroupName;
                $eventGroup->type = E4S_EVENT_FIELD;
                $addRow->eventGroup = $eventGroup;

                $athlete = new stdClass();
                $athlete->id = $row->athleteId;
                $athlete->name = $row->athleteName;
                $athlete->aocode = $row->aocode;
                $athlete->urn = $row->urn;
                $athlete->gender = $row->gender;
                $athlete->club = $row->club;
                if (is_null($athlete->club)) {
                    $athlete->club = 'Unregistered';
                }
                $addRow->athlete = $athlete;

                $ageGroup = new stdClass();
                $ageGroup->ageGroupID = $row->ageGroupID;
                $ageGroup->eventAgeGroup = $row->eventAgeGroup;
                $ageGroup->ageGroup = $row->ageGroup;
                $addRow->ageGroup = $ageGroup;

                $heatInfo = new stdClass();
                $heatInfo->position = $position++;
                $heatInfo->heatNo = 0;
                $heatInfo->laneNo = 0;
                $heatInfo->heatNoCheckedIn = 0;
                $heatInfo->laneNoCheckedIn = 0;
//                $heatInfo->seededPosition = (int)$row->seededPosition;
                $addRow->heatInfo = $heatInfo;

                $addRow->results = array();
                $addRow->results['highScore'] = 0;
                $addRow->results['scoreText'] = '';
                $addRow->results['currentPosition'] = 0;
                $addRow->results['heatPosition'] = 0;
                $addRow->results['isPb'] = FALSE;
                $addRow->results['isSb'] = FALSE;

                $rows[$key] = $addRow;
            } else {
                $addRow = $rows [$key];
            }

            if (!is_null($row->resultKey) and $row->resultKey !== '') {
                // Athlete has results for this event
                $trackResult = FALSE;
                $heightResult = FALSE;
                if (!array_key_exists('data', $addRow->results)) {
                    $addRow->results['data'] = array();
                    $addRow->results['dataOptions'] = array();
                }
//TODO HARD CODED !!!!!
                if ($heightEvent === 72) {
                    $heightResult = TRUE;
                    // result key in format h(height)_a(attempt)
                    $heightResultKey = explode('_', $row->resultKey);
                    $height = str_replace('h', '', $heightResultKey[0]);
                    $attempt = (int)str_replace('a', '', $heightResultKey[1]);
                    if (!array_key_exists($height, $addRow->results['data'])) {
                        $addRow->results['data'][$height] = array();
                    }
                    // will be a pass, retire, void or fail
                    $addRow->results['data'][$height][$attempt] = $row->resultValue;
                } else {
                    if ($row->resultKey[0] === 'h') {
                        $trackResult = TRUE;
                        $addRow->eventGroup->type = E4S_EVENT_TRACK;
                    }
                    $resultOptions = e4s_getOptionsAsObj($row->resultOptions);
                    if (isset($resultOptions->place)) {
                        $addRow->results['heatPosition'] = (int)$resultOptions->place;
                    }
                    if (isset($resultOptions->timeInSeconds)) {
                        $addRow->results['resultInSeconds'] = (float)$resultOptions->timeInSeconds;
                    } else {
                        if ($trackResult) {
                            if (strpos($row->resultValue, ':') !== FALSE) {
                                $arr = preg_split('~:~', $row->resultValue);
                                $mins = (int)$arr[0];
                                $secs = (float)($arr[1]);
                                $addRow->results['resultInSeconds'] = ($mins * 60) + $secs;
                            } else {
                                $addRow->results['resultInSeconds'] = (float)$row->resultValue;
                            }
                        }
                    }
                    if ($trackResult) {
                        // Track Result
                        $info = preg_split('~h~', $row->resultKey);
                        $addRow->heatInfo->heatNo = (int)$info[1];
                    }
                    $addRow->results['data'][$row->resultKey] = $row->resultValue;
                    $addRow->results['dataOptions'][$row->resultKey] = $row->resultOptions;
                }
                if ($trackResult) {
                    $addRow->results['highScore'] = $row->resultValue;
                    $posArray[$key] = $addRow->results['highScore'];
                } else {
                    if (!$heightResult) {
                        if (is_numeric($row->resultValue)) {
                            if ($row->resultValue > $addRow->results['highScore']) {
                                $addRow->results['highScore'] = $row->resultValue;
                                $posArray[$key] = $addRow->results['highScore'];
                            }
                        }
                    }
                }
            }
        }
//        var_dump($addRow);
//        exit();
        arsort($posArray);
        $currentPosition = 0;
        foreach ($posArray as $athleteId => $position) {
            $currentPosition++;
            $rows[$athleteId]->results['currentPosition'] = $currentPosition;
            if ($rows[$athleteId]->results['heatPosition'] === 0) {
                $rows[$athleteId]->results['heatPosition'] = $currentPosition;
            }
        }
        $rows = $this->addPBInfo($rows, $pbKeys);

        if ($exit) {
            Entry4UISuccess($rows);
        }
        return $rows;
    }

    private function getCompEntrySQL() {
//        ,if (s.pos is null, 9999,s.pos) seededPosition
        $sql = "
            select  e.id entryId
                ,eg.name eventGroupName
                ,eg.eventNo
                ,eg.id
                ,ce.eventId
                ,e.athleteId
                ,e.options
                ,concat(a.firstName,' ',a.surName) athleteName
                ,a.aocode aocode
                ,a.urn urn
                ,a.gender gender
                ,c.Clubname club
                ,if (e.pb = 0, 9999,e.pb) pb
                ,e.ageGroupID
                ,e.eventAgeGroup
                ,ag.Name ageGroup
                ,e.checkedIn
                ,e.present
                ,e.teambibno teambibno
                ,b.bibNo
                ,b.collected
                ,r.resultkey resultKey
                ,r.resultValue resultValue
                ,r.options resultOptions 
                ,s.heatNo
                ,s.laneNo
                ,s.heatNoCheckedIn
                ,s.laneNoCheckedIn
            
        from " . E4S_TABLE_COMPEVENTS . " ce left join " . E4S_TABLE_EVENTGROUPS . " eg 
                 on ce.maxGroup = eg.id left join " . E4S_TABLE_ENTRIES . " e 
                     on e.compEventID = ce.ID left join " . E4S_TABLE_CARDRESULTS . " r 
                        on e.athleteid = r.athleteid 
                        and r.compid = " . $this->compId . " 
                        and r.eventno = eg.eventNo,
             " . E4S_TABLE_BIBNO . " b,
             " . E4S_TABLE_AGEGROUPS . " ag,
             " . E4S_TABLE_ATHLETE . " a left join " . E4S_TABLE_CLUBS . " c 
                on a.clubid = c.id left join " . E4S_TABLE_SEEDING . " s 
                  on a.id = s.athleteid and s.eventgroupid = " . $this->eventGroupId . "
        where eg.compId = " . $this->compId . "
        and eg.id = " . $this->eventGroupId . "
        and e.ageGroupId = ag.id
        and b.compid = eg.compid
        and b.athleteid = e.athleteid
        and b.athleteid = a.id
        and e.paid in (" . E4S_ENTRY_PAID . "," . E4S_ENTRY_QUALIFY . ")";

        if ($this->athleteId !== 0) {
            $sql .= ' and e.athleteid = ' . $this->athleteId;
        } else {
            $sql .= ' order by s.heatno, s.laneno, if (e.pb = 0, 9999,e.pb) ';
            if ($this->tf === E4S_EVENT_FIELD) {
                $sql .= ' desc ';
            }
            $sql .= ', a.surname, a.firstname';
        }

        return $sql;
    }

    public function getEventType($id) {
        $sql = 'select uomtype
                from ' . E4S_TABLE_UOM . ' u,
                     ' . E4S_TABLE_EVENTS . ' e
                where e.id = ' . $id . '
                and   e.uomid = u.id';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
//            var_dump("Not found or multiple");
            return '';
        }
        $obj = $result->fetch_object();
        return $obj->uomtype;
    }

    private function addPBInfo($rows, $pbKeys) {
        $sql = 'select athleteid, pof10pb, pb, sb
                from ' . E4S_TABLE_ATHLETEPB . "
                where concat(athleteid,'-', eventid) in ('" . implode("','", $pbKeys) . "')";

        $result = e4s_queryNoLog($sql);
        $pbArr = array();
        while ($row = $result->fetch_object()) {
            $pbArr[(int)$row->athleteid] = $row;
        }

        foreach ($rows as $athleteId => $row) {
            if (isset($pbArr[$athleteId])) {
                $row->athlete->pb = (float)$pbArr[$athleteId]->pof10pb;
                $row->athlete->sb = (float)$pbArr[$athleteId]->sb;
                if ($row->results['highScore'] > 0) {
                    if ($row->eventGroup->type === E4S_EVENT_TRACK) {
                        if ($row->results['highScore'] < $row->athlete->sb) {
                            $row->results['scoreText'] = '(sb)';
                            $row->results['isSb'] = TRUE;
                        }
                        if ($row->results['highScore'] < $row->athlete->pb) {
                            $row->results['scoreText'] = '(pb)';
                            $row->results['isPb'] = TRUE;
                        }
                    } else {
                        if ($row->results['highScore'] > $row->athlete->sb) {
                            $row->results['scoreText'] = '(sb)';
                            $row->results['isSb'] = TRUE;
                        }
                        if ($row->results['highScore'] > $row->athlete->pb) {
                            $row->results['scoreText'] = '(pb)';
                            $row->results['isPb'] = TRUE;
                        }
                    }
                }
            } else {
                $row->athlete->pb = 0;
                $row->athlete->sb = 0;
            }
        }

        return $rows;
    }

    public function processEntriesForEventGroups() {
        $arr = array();
        $eventNos = array();
        $egIds = array();
        foreach ($this->egIds as $egIdArr) {
            $egId = $egIdArr['id'];
            $this->eventGroupId = $egId;
            $rows = $this->processEntriesForEvent(FALSE);
            $eventNos[] = $this->eventNo;
            $egIds[$this->eventNo] = $egId;
            $arr[$egId] = $rows;
        }

        if (empty($eventNos) or sizeof($eventNos) === 0 or is_null($eventNos[0])) {
            Entry4UIError(4200, 'Bib numbers have not been generated yet.');
        }

        $sql = 'select eventNo eventNo
                       ,heatno heatNo
                       ,time eventTime
                       ,ws ws
                       ,image image
                from ' . E4S_TABLE_TRACKRESULT . '
                where compid = ' . $this->compId . '
                and   eventno in (' . implode(',', $eventNos) . ')
                order by eventNo, heatNo';
        $result = e4s_queryNoLog($sql);
        $photos = array();
        while ($obj = $result->fetch_object()) {
            $obj->compId = $this->compId;
            $heatNo = $obj->heatNo;
            $eventNo = $obj->eventNo;
            if (!array_key_exists($egIds[$eventNo], $photos)) {
                $photos [$egIds[$eventNo]] = array();
            }

            $photos[$egIds[$eventNo]][$heatNo] = $obj;
        }
        Entry4UISuccess('
            "data":' . json_encode($arr, JSON_NUMERIC_CHECK) . ',
            "meta":' . json_encode($photos, JSON_NUMERIC_CHECK));
    }

    public function updateStartHeight() {
        $sql = 'select options
                from ' . E4S_TABLE_ENTRIES . '
                where id = ' . $this->entryId;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows !== 1) {
            Entry4UIError(5010, 'Failed to get Entry : ' . $this->entryId);
        }
        $obj = $result->fetch_object();
        $options = e4s_getDataAsType($obj->options, E4S_OPTIONS_OBJECT);
        $options->startHeight = $this->startHeight;

        $update = 'update ' . E4S_TABLE_ENTRIES . "
                   set options = '" . e4s_getDataAsType($options, E4S_OPTIONS_STRING) . "'
                   where id = " . $this->entryId;
        e4s_queryNoLog($update);
        Entry4UISuccess();
    }

    private function getHeatInfo() {
        $sql = 'select count(id) entryCount
                from ' . E4S_TABLE_ENTRIES . '
                where compeventid in (
                    select id
                    from ' . E4S_TABLE_COMPEVENTS . '
                    where maxgroup = ' . $this->eventGroupId . '
                )
                and paid = ' . E4S_ENTRY_PAID;
        $result = e4s_queryNoLog($sql);
        $row = $result->fetch_assoc();
        return (int)$row['entryCount'];
    }
}