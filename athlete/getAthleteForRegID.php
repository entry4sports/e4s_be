<?php
// Passed a URN. return the athlete from our system
// or find in alternative system
//$regid = $_GET['regid'];
//$dob = $_GET['dob'];
// TODO Is THis file Deprecated ?????
//Is this function deprecated ?
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'athlete/userAthletes.php';

if ($aocode === '') {
    Entry4UIError(1000, 'Please pass a registration code.', 200, '');
}
if ($regid === '' or $dob === '') {
    Entry4UIError(1001, 'Please pass a registration number and date of birth.', 200, '');
}

$now = date(E4S_SHORT_DATE);
$dobSplit = explode('-', $dob);
if (!checkdate($dobSplit[1], $dobSplit[2], $dobSplit[0])) {
    Entry4UIError(1000, 'Invalid Date of Birth given.', 200, '');
}
$currentAge = (int)getAgeFromText($dob, $now, E4S_SHORT_DATE);
if ($currentAge < 6 || $currentAge > 110) {
    Entry4UIError(1000, 'Invalid Date of Birth. Age :' . $currentAge, 200, '');
}
$model = new stdClass();
$model->urn = $regid;
$model->aoCode = $aocode;
$model->dob = $dob;

checkForAthlete($model);
//
//// if returned from function, athlete not found so check external
//if (checkExtSystem($arr)) {
//// if returned, try to get athlete.
//    checkForAthlete($arr);
//}
//Entry4UIError(1000,"Can not find athlete for " . $regid  . " with a dob of " . $dob , 200, "");

function checkForAthlete($model) {
    $aObj = new athleteClass();
    $aObj->searchE4SDataForRegisteredAthlete($model);
    Entry4UISuccess($aObj);
}

function getClubID($club) {
    // try to get club
    $clubid = 0;
    $club = addslashes($club);
    $sql = 'select id from ' . E4S_TABLE_CLUBS . "
            where clubname like '" . $club . "%'";
    $return = e4s_queryNoLog($sql);
    if ($return->num_rows === 1) {
        $row = $return->fetch_assoc();
        $clubid = $row['id'];
    }
    return $clubid;
}

function getEnglishAthleteFromEA($arr) {
    $objClient = new SoapClient('https://stagemyathletics.uka.org.uk/LicenceCheckService/LicenceCheck.svc?singleWsdl');
    $webKey = '78251D2A-BBFE-47F4-9632-B26EE279C003';
    $urnMethod = 'CheckRegistrationStatus_Urn';
    $dobMethod = 'CheckRegistrationStatus_Urn_Ln_Dob';
    // get info for URN
    $payload = array('webUserKey' => '78251D2A-BBFE-47F4-9632-B26EE279C003', 'firstName' => 'Jessica', 'lastName' => 'Day', 'urn' => '3590678', 'dateOfBirth' => '2004-09-03');
    $payload = array();
    $payload['webUserKey'] = $webKey;
    $payload['urn'] = $arr['regid'];
    $objResponse = $objClient->__soapCall($urnMethod, array($payload));
    if (is_null($objResponse->result)) {
        return FALSE;
    }

    // valid URN, so get name, add our dob and try again
    $payload['dateOfBirth'] = $arr['dob'];
    $payload['lastName'] = $objResponse->result->LastName;

    $objResponse = $objClient->__soapCall($dobMethod, array($payload));
    if (is_null($objResponse->result)) {
        return FALSE;
    }

    $obj = new stdClass();
    $obj->firstName = '';
    $obj->lastName = '';
    $obj->regNumber = $arr['regid'];
    $obj->gender = '?';
    $obj->birthDate = $arr['dob'];
    $obj->disability = 0;
    // reg Date ????
}

