<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
//$aid = $_GET['athleteid'];
//$ceid = $_GET['ceid'];

define('E4S_SEP_OPENER', '^^');
define('E4S_SEP_CLOSER', '^^');

$athleteRow = e4s_getEligableAthlete($aid);
$ceRow = e4s_getEligableCE($ceid);
e4s_checkAthlete($athleteRow, $ceRow);

function e4s_formatDate($dateStr) {
    $date = date_create($dateStr);
    return date_format($date, 'd/M/Y');
}

function e4s_returnSep(&$sepCnt) {
    if ($sepCnt === 0) {
        $sepCnt += 1;
        return '';
    } else {
        return E4S_SEP_OPENER . ($sepCnt += 1) . E4S_SEP_CLOSER;
    }
}


function e4s_checkAthlete($athleteRow, $ceRow) {
    $reason = '';
    $sepCnt = 0;
    $matchAgeGroup = FALSE;
    $ageGroupText = '';
    $ageSep = '';

    foreach ($ceRow['dobs'] as $agid => $dob) {
        if ($athleteRow['dob'] >= $dob['fromDate'] and $athleteRow['dob'] <= $dob['toDate']) {
            $matchAgeGroup = TRUE;
        } else {
            if ($athleteRow['dob'] < $dob['fromDate'] || $athleteRow['dob'] > $dob['toDate']) {
                $ageGroupName = e4s_getVetDisplay($ceRow['ageGroups'][$agid]['Name'], 'O');
                $ageGroupText .= $ageSep . $ageGroupName . ' [' . e4s_formatDate($dob['fromDate']) . ' - ' . e4s_formatDate($dob['toDate']) . ']';
                $ageSep = ', ';
            }
        }
    }
    if ($matchAgeGroup === FALSE) {
        $reason .= e4s_returnSep($sepCnt) . 'has dob of ' . e4s_formatDate($athleteRow['dob']) . ' and this is not within range : ' . $ageGroupText . '';
    }

    if ($athleteRow['gender'] !== $ceRow['gender']) {
        $reason .= e4s_returnSep($sepCnt) . 'not the correct gender for this event';
    }

    $compAllowsRegistered = FALSE;
    $compAllowsUnRegistered = FALSE;
    $compAllowsInternational = FALSE;
    $compDate = '';
    if (array_key_exists('competition', $ceRow)) {
        $compDate = $ceRow['competition']['Date'];
        if (array_key_exists('options', $ceRow['competition'])) {
            $coptions = e4s_addDefaultCompOptions($ceRow['competition']['options']);
            $compAllowsRegistered = e4sCompetition::allowRegisteredAthletes($coptions);
            $compAllowsUnRegistered = e4sCompetition::allowUnRegisteredAthletes($coptions);
	        $compAllowsInternational = $compAllowsUnRegistered;
			if ( !$compAllowsUnRegistered ) {
				$compAllowsInternational = e4sCompetition::allowInternationalAthletes( $coptions );
			}
		}
    }
	$isIntAthlete = e4s_isAthleteInternational($athleteRow);
	if ($isIntAthlete) {
		if (!$compAllowsInternational) {
			$reason .= e4s_returnSep( $sepCnt ) . ' International Athlete';
		}
	}else {
		$regDate = $athleteRow['activeEndDate'];
		if ( $compDate !== '' ) {
			if ( $regDate < $compDate and $compAllowsUnRegistered === FALSE ) {
				$reason .= e4s_returnSep( $sepCnt ) . 'registration is not valid/expired';
			}
			if ( $regDate >= $compDate and $compAllowsRegistered === FALSE ) {
				$reason .= e4s_returnSep( $sepCnt ) . ' a registered athlete';
			}
		}
	}

    if ($ceRow['options']->eventTeam->mustBeIndivEntered) {
        // check the athlete is entered into an individual event
        $enteredSQL = 'select athleteid
        from ' . E4S_TABLE_ENTRIES . ' e,
             ' . E4S_TABLE_COMPEVENTS . ' ce 
        where ce.id = e.compeventid
        and   compid = ' . $ceRow['CompID'] . '
        and   paid = ' . E4S_ENTRY_PAID . '
        and   athleteid = ' . $athleteRow['id'];

        $res = e4s_queryNoLog($enteredSQL);
        if ($res->num_rows < 1) {
            $reason .= e4s_returnSep($sepCnt) . 'not currently entered into an individual event';
        }
    }

    if ($reason !== '') {
        $reason = $athleteRow['firstName'] . "'s " . $reason;
        $reason = str_replace(E4S_SEP_OPENER . $sepCnt . E4S_SEP_CLOSER, ' and ', $reason);
        $sepCnt -= 1;
        while ($sepCnt > 0) {
            $reason = str_replace(E4S_SEP_OPENER . $sepCnt . E4S_SEP_CLOSER, ', ', $reason);
            $sepCnt -= 1;
        }
    } else {
        $reason = $athleteRow['firstName'] . ' is eligible for this event';
    }
    $reason .= '.';
    Entry4UISuccess(e4s_returnTextToSuccess($reason));
}

function e4s_getEligableAthlete($aid) {
    $sql = 'select * 
            from ' . E4S_TABLE_ATHLETE . '
            where id = ' . $aid;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(1000, 'Failed to get athlete', 400, '');
    }
    return $result->fetch_assoc();
}

function e4s_getEligableCE($ceid) {
    $sql = 'select ce.*, e.gender gender 
            from ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTS . ' e 
            where ce.id = ' . $ceid . '
            and   ce.eventid = e.id';
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(1000, 'Failed to get Event', 400, '');
    }
    $ceRow = $result->fetch_assoc();
//    $sql = "select *
//            from " . E4S_TABLE_COMPETITON . "
//            where id = " . $ceRow['CompID'];
//    $result = e4s_queryNoLog($sql);
//    if ( $result->num_rows !== 1){
//        Entry4UIError(1000,"1000: Failed to get Competition",400,"");
//    }
    $compObj = e4s_getCompObj($ceRow['CompID']);
    $ceRow['competition'] = $compObj->getRow();
//    $ceRow['competition'] = $result->fetch_assoc();

    $options = e4s_addDefaultCompEventOptions($ceRow['options']);
    $ceRow['options'] = $options;
    $agegroupids = $ceRow['AgeGroupID'];
    if (isset($options->ageGroups)) {
        // event has upscaling
        foreach ($options->ageGroups as $ageGroup) {
            $agegroupids .= ',' . $ageGroup->ageGroup;
        }
    }
    $sql = 'select * 
            from ' . E4S_TABLE_AGEGROUPS . '
            where id in (' . $agegroupids . ')
            order by minage';
//    echo "\n" . $sql;
//    exit();
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows < 1) {
        Entry4UIError(1000, 'Failed to get age Age Groups', 400, '');
    }
    $ageGroups = $result->fetch_all(MYSQLI_ASSOC);
    $ageArr = array();
    foreach ($ageGroups as $ageGroup) {
        $ageArr[$ageGroup['id']] = $ageGroup;
    }
    $ceRow['ageGroups'] = $ageArr;
    $dobs = array();
    foreach ($ceRow['ageGroups'] as $ageGroup) {
        $dobs[$ageGroup['id']] = getAgeGroupAndDOBs($ceRow['competition'], $ageGroup);
    }
    $ceRow['dobs'] = $dobs;
    return $ceRow;
}

Entry4UISuccess('
    "data": ' . json_encode($athleteRecord, JSON_NUMERIC_CHECK));