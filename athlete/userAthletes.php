<?php
include_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_checkUserAthletesPof10($userId, $aoCode = E4S_AOCODE_EA, $force = false):void{
	$uaObj = new e4s_userAthletes();
	$ua = $uaObj->getData($uaObj->userType, $userId);
	$athleteIds = [];
	foreach ($ua as $uaItem){
		$athleteIds[] = $uaItem->athleteId;
	}
	if ( !empty($athleteIds) ){
		$athleteIdsStr = implode(',', $athleteIds);
		$sql = 'select *
				from ' . E4S_TABLE_ATHLETE . '
				where id in (' . $athleteIdsStr . ')
				and urn is not null
				and aocode = "' . $aoCode . '"';
		$results = e4s_queryNoLog($sql);

		$athleteObj = new athleteClass();
		while ($athlete = $results->fetch_object(E4S_ATHLETE_OBJ)) {
			$athleteObj->addAthleteToCache($athlete);
			if ( $aoCode === E4S_AOCODE_EA) {
				$pof10Obj = new pof10V2Class( $athlete->URN, $athlete->id );
				$pof10Obj->updateDb($force);
			}
		}
	}
	update_user_meta($userId, E4S_USER_LASTCHECKEDPB, date(E4S_MYSQL_DATE));

	exit();
}
function e4s_getUsersForAthlete($obj) {
    $athleteId = checkFieldForXSS($obj, 'athleteid:Athlete ID');
    $athleteObj = athleteClass::withID($athleteId);
    $users = $athleteObj->getUserAthletes();
    Entry4UISuccess($users);
}

function e4s_addUserAthlete($obj) {
    $athleteId = checkFieldForXSS($obj, 'athleteid:Athlete ID');
    if (is_null($athleteId)) {
        Entry4UIError(9013, 'Please pass the correct parameters');
    }
    $athleteId = (int)$athleteId;
    $userId = checkFieldForXSS($obj, 'userid:User ID');
    if (is_null($userId)) {
        $userId = e4s_getUserID();
    }
    $userId = (int)$userId;
    addUserAthlete($athleteId, $userId);
    Entry4UISuccess();
}

function addUserAthlete($athleteId, $userId = 0) {
    if ($userId === 0) {
        $userId = e4s_getUserID();
    }
    $sql = 'select * 
            from ' . E4S_TABLE_USERATHLETES . "
            where athleteid = $athleteId
            and userid = $userId";

    $result = e4s_queryNoLog($sql);
    if ($result->num_rows === 0) {
        $sql = 'insert into ' . E4S_TABLE_USERATHLETES . " ( athleteid, userid )
                values ($athleteId, $userId)";

        e4s_queryNoLog($sql);
    }
}

function getUserAthlete_athleteIds_str() {
    $arr = getUserAthlete_athleteIds();

    return (implode(',', $arr));
}

function getUserAthlete_athleteIds() {
    $userid = e4s_getUserID();

    $sql = 'select athleteid
            from ' . E4S_TABLE_USERATHLETES . "
            where userid = $userid";

    $results = e4s_queryNoLog($sql);
    $data = array();
    while ($row = mysqli_fetch_array($results, TRUE)) {
        $data[] = $row['athleteid'];
    }
    return $data;
}