<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
define('E4S_COL_SCHOOL', 'school');
define('E4S_COL_CLUB', 'club');
//e4s_getUserObj(false);
$userid = e4s_getUserID();
$config = e4s_getConfig();

if (e4s_inMaintenance(FALSE)) {
    Entry4UISuccess('"data":[],"meta":[]');
}

$allAthletes = FALSE;
if (isE4SUser() or isAppAdmin()) {
    $allAthletes = TRUE;
}
// Not sure of this line
if ($compid === '') {
    Entry4UIError(100, 'No competition id supplied', 400, '');
}

$noComp = FALSE;

if ((int)$compid === 0 or is_null($compid)) {
    $compid = 0;
    $showAllAthletes = TRUE;
    $noComp = TRUE;
}
$compid = (int)$compid;
$paging = getUserPageSize();
if ($paging['pageSize'] !== $pagesize) {
    // Users page size has changed so update config
    $paging['pageSize'] = $pagesize;
    updateUserPageSize($pagesize);
}
$allowRegistered = true;
$allowUnregistered = FALSE;
$allowInternational = false;
// Used only by team ce, should only already entered athletes be allowed in a team
$areaIds = '';
$cOptions = new stdClass();
//$clubIDs = null;
$isSchool = FALSE;
$userType = E4S_USERTYPE_NONE;
$compObj = null;
$compRow = array();
$isOrg = FALSE;
$isAAI = FALSE;
$athleteType = 'A';
if (!$noComp) {
// Get Competition Entity/Area
    $compObj = e4s_GetCompObj($compid);
	if (isAppAdmin() and !$compObj->isClubcomp()) {
		$allAthletes = TRUE;
	}

    if (!$compObj->validatePriority()) {
        Entry4UIError(8500, 'You must enter a valid priority code', 200, 'athletes:[]');
    }
    if ( $compObj->isClubcomp() and $compObj->checkClubComp() < 0 ){
        Entry4UIError(8610, 'This competition is currently not available to you.', 200);
    }

    $compRow = $compObj->getRow();
    $cOptions = $compObj->getOptions();
    if ( isset($cOptions->athleteType) and $cOptions->athleteType !== '') {
        $athleteType = $cOptions->athleteType;
    }
    $isSchool = $compObj->isSchool();

//    $isOrg = userHasPermission(PERM_ADMIN,null,$compid);
    $isOrg = $compObj->isOrganiser();
    if ($isOrg) {
//        $allAthletes = $showAllAthletes;
        $allAthletes = TRUE;
    }
    if ($config['theme'] === E4S_AOCODE_AAI and $isOrg) {
//        $showAllAthletes = TRUE;
        $isAAI = TRUE;
    }
	$allowUnregistered = $compObj->allowUnregistered();
	$allowRegistered = $compObj->allowRegistered();
	$allowInternational = $compObj->allowInternational();

    if (!$allAthletes or e4s_isUserBeingImpersonated()) {
        $userType = e4s_getUserType($compid, $areaIds);
    }
} else {
    if (!$allAthletes or e4s_isUserBeingImpersonated()) {
        $userType = e4s_getCurrentUserType();
    }
}

$ignoreGender = FALSE;

// If passed a CEID ( Event Teams competition type ahead )
// Check if there is any age upscaling

if ($filterAgeGroupID === '') {
    $filterAgeGroupIDs = '0';
} else {
    $filterAgeGroupIDs = $filterAgeGroupID;
}
$ceidPassed = FALSE;
$mustBeIndivEntered = FALSE;
if (isset($ceid)) {
    if ((int)$ceid !== 0) {
        $ceidPassed = TRUE;
        $ceoptions = '';
        $filterAgeGroupIDs = getEventAgeGroupIds($ceid, $ignoreGender, $ceoptions);
        include_once E4S_FULL_PATH . 'eventteams/commonEventTeam.php';
        if (isEventATeamEvent($ceoptions)) {
            $mustBeIndivEntered = $ceoptions->eventTeam->mustBeIndivEntered;
        }
    }
}

$useAgeGroups = array();
$allCompDOBs = array();
if (!$noComp) {
    $allCompDOBs = getAllCompDOBs($compid);
}
$oldestDob = null;
$youngestDob = null;
$filterAgeGroupIDs = explode(',', $filterAgeGroupIDs);
foreach ($allCompDOBs as $compDOB) {
    // do not rely on the sort of allcompdobs and allow for mulitple agegroups to span an age
    if ($youngestDob === null) {
        $youngestDob = $compDOB;
    }
    if ($compDOB['toDate'] > $youngestDob['toDate']) {
        $youngestDob = $compDOB;
    }
    if ($oldestDob === null) {
        $oldestDob = $compDOB;
    }
    if ($compDOB['fromDate'] < $oldestDob['fromDate']) {
        $oldestDob = $compDOB;
    }
    if (in_array($compDOB['agid'], $filterAgeGroupIDs)) {
        $useAgeGroups[] = $compDOB;
    }
}
if (!$noComp) {
    if (($oldestDob === null || $youngestDob === null) and $ceid !== 0) {
        Entry4UIError(1000, 'Unable to get Competition Age Range', 400, '');
    }
}

$clubOrSchool = E4S_COL_CLUB;
if ($isSchool) {
    $clubOrSchool = E4S_COL_SCHOOL;
}
//set nextExtCheck to CURRENT_TIMESTAMP to force update
$sql1 = 'select current_timestamp serverNow,DATE_ADD(lastChecked, INTERVAL 10 MINUTE ) nextExtCheck, a.*, c.Clubname clubname, ar_country.id countryid, ar_country.name country, ar_county.id countyid, ar_county.name county,ar_region.name region, ar_region.id regionid, c.Clubname club from ' . E4S_TABLE_ATHLETE . ' a left join ' . E4S_TABLE_CLUBS . ' c on (a.';
$sql2 = 'select count(a.id) totalCount from ' . E4S_TABLE_ATHLETE . ' a left join ' . E4S_TABLE_CLUBS . ' c on (a.';
$commonSql = $clubOrSchool . 'id = c.id';
if ($noComp) {
//    $commonSql = "clubid = c.id or a.schoolid = c.id";
}
$commonSql .= ') left join ' . E4S_TABLE_AREA . ' ar_county on c.areaid = ar_county.id left join ' . E4S_TABLE_AREA . ' ar_region on ar_county.parentid = ar_region.id left join ' . E4S_TABLE_AREA . ' ar_country on ar_region.parentid = ar_country.id ';

$sql1 .= $commonSql;
$sql2 .= $commonSql;

$sql = '';
$sqlOp = ' and ';
$sql .= " a.type = '" . $athleteType . "' ";

// validation after adding athlete to team, we know athleteid, but need to validate against event
if ((int)$athleteid > 0) {
    $sql .= $sqlOp . ' a.id = ' . $athleteid . ' ';
    $sqlOp = ' and ';
}

if ((int)$teamid === 0) {
    if ($filterGender !== '' and $ignoreGender === FALSE) {
        $sql .= $sqlOp . " gender = '{$filterGender}' ";
        $sqlOp = ' and ';
    }
}
// Dont show all Athletes and we have a comp
// so check if there is any athlete security
if (!$showAllAthletes and $compid > 0) {
    if ($compObj->hasAthleteAreaSecurity()) {
        $athleteAreas = implode(',', $compObj->getAthleteAreaSecurity());
        $sql .= $sqlOp . " (ar_county.id in ({$athleteAreas}) or ar_region.id in( {$athleteAreas}) or ar_country.id in( {$athleteAreas})) ";
        $sqlOp = ' and ';
    } else {
//        e4s_addDebug("Has NO Area Security");
    }
}

if ($userType === E4S_USERTYPE_AREA) {
    if ($areaIds !== '') {
        $sql .= $sqlOp . " areaid in ({$areaIds})";
        $sqlOp = ' and ';
    } elseif ($showAllAthletes) {
        $userArea = getUserArea();

        if ((int)$userArea['entitylevel'] === E4S_REGION_ENTITY) {
            $sql .= $sqlOp . ' ar_region.id = ' . $userArea['areaid'] . ' ';
            $sqlOp = ' and ';
        }
        if ((int)$userArea['entitylevel'] === E4S_COUNTY_ENTITY) {
            $sql .= $sqlOp . ' ar_county.id = ' . $userArea['areaid'] . ' ';
            $sqlOp = ' and ';
        }
    }
} elseif ($userType === E4S_USERTYPE_CLUB) {
//    $clubs = getUserClubs();
    $clubs = getClubsForUser($userid);
    $clubarr = '';
    $clubarrsep = '';
    foreach ($clubs as $club) {
        if (isset($club->id)) {
            $clubarr .= $clubarrsep . $club->id;
        } else {
            $clubarr .= $clubarrsep . $club['id'];
        }

        $clubarrsep = ',';
    }
    if ($clubarrsep !== '') {
        // there are clubs
        $clubOrSchool = E4S_COL_CLUB;
        if ($isSchool) {
            $clubOrSchool = E4S_COL_SCHOOL;
        }
        $sql .= $sqlOp . '(';
        if ($noComp) {
            $sql .= ' a.clubid in (' . $clubarr . ') or a.schoolid in (' . $clubarr . ')';
        } else {
            $sql .= ' a.' . $clubOrSchool . 'id in (' . $clubarr . ')';
        }
        $sql .= ')';
        $sqlOp = ' and ';
    }
}

if (is_null($urn)) {
    $urn = '';
}
// handle International Athletes
if (!$allowUnregistered and !$showAllAthletes and $urn === '') {
	$sql .= $sqlOp;
	if ($allowInternational){
		$sql .= '((';
	}
    $sql .= ' activeenddate > now() ';
    $sqlOp = ' and ';
    $sql .= $sqlOp . " a.urn != '' ";
	if ($allowInternational){
		$sql .= ') or a.aocode = "' . E4S_AOCODE_INTERNATIONAL . '")';
	}
}

if ($urn !== '') {
    $sql .= $sqlOp . " (a.urn = '" . $urn . "' or a.id = '" . $urn . "') ";
    $sqlOp = ' and ';
}
if ($userType !== E4S_USERTYPE_NONE) {
    if ((int)$teamid !== 0) {
        $teamSql = 'select *
                from ' . E4S_TABLE_COMPTEAM . '
                where id = ' . $teamid;
        $teamResult = e4s_queryNoLog($teamSql);
        $teamRow = $teamResult->fetch_assoc();
        if ($ignoreGender === FALSE) {
            if ($teamRow['gender'] !== '') {
                $sql .= $sqlOp . " gender = '" . $teamRow['gender'] . "' ";
                $sqlOp = ' and ';
            } else {
                if ($filterGender !== '') {
                    $sql .= $sqlOp . " gender = '" . $filterGender . "' ";
                    $sqlOp = ' and ';
                }
            }

            if ($teamRow['gender'] !== '' and $filterGender !== '' and $teamRow['gender'] !== $filterGender) {
                Entry4UISuccess('
            "data":[], 
            "meta":' . json_encode($paging, JSON_NUMERIC_CHECK));
            }
        }

        $sql .= $sqlOp . ' a.id not in (
            select distinct(athleteid)
            from ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce
            where e.compeventid = ce.id
            and ce.compid = ' . $compid . '
            and e.teamid not in(' . $teamid . ',0) )';
        $sqlOp = ' and ';
    }
} else {
    if (!$allAthletes) {
        $sql .= $sqlOp . ' a.id in (';
        $sql .= ' select athleteid from ' . E4S_TABLE_USERATHLETES . ' where userid = ' . e4s_getUserID();
        $sql .= ')';
        $sqlOp = ' and ';
    }
}

if (!$noComp) {
    if (sizeof($useAgeGroups) > 0) {
        $sql .= $sqlOp . '(';
        $agsqlOp = '';
        foreach ($useAgeGroups as $useAgeGroup) {
            $sql .= $agsqlOp . " dob BETWEEN '" . $useAgeGroup['fromDate'] . "' and '" . $useAgeGroup['toDate'] . "' ";
            $agsqlOp = ' or ';
        }
        $sql .= ')';
        $sqlOp = ' and ';
    }
}
$safeSearch = '';
if (isset($search)) {
    if ($search !== '') {
        $search = ltrim($search, ' ');
        $safeSearch = getSafeSearchText($search);
        $sql .= $sqlOp . ' (' . getSafeSQLSearchText('concat(firstname,surname)') . " like '%" . $safeSearch . "%' or urn like '" . $safeSearch . "%' or clubname like '" . $safeSearch . "%')";
        $sqlOp = ' and ';
    }
}

$enteredAthletesSQL = ' a.id in (
        select distinct(athleteid)
        from ' . E4S_TABLE_ENTRIES . ' e,
             ' . E4S_TABLE_COMPEVENTS . " ce 
        where ce.id = e.compeventid
        and   compid = {$compid}
        and   athleteid = a.id 
    )";
//and   paid = " . E4S_ENTRY_PAID . "
// Only return athletes already entered ( Team Events )
if ($ceidPassed and $mustBeIndivEntered) {
    $sql .= $sqlOp . $enteredAthletesSQL;
    $sqlOp = ' and ';
}

$sortSql = '';
// Add any sorting
if ($sort !== '') {
    $sort = strtolower($sort);
    if ($sort === 'club') {
        $sort = 'clubname';
    }

    $sortSql .= ' order by ' . $sort . ' ';
    if ($sortdesc === '1') {
        $sortSql .= 'desc ';
    }

    if ($sort === 'firstname') {
        $sortSql .= ' ,surname  ';
        if ($sortdesc === '1') {
            $sortSql .= 'desc ';
        }
    }
    if ($sort === 'surname') {
        $sortSql .= ' , firstname ';
        if ($sortdesc === '1') {
            $sortSql .= 'desc ';
        }
    }
}

$sqlLimit = '';
if (!isset($pagesize)) {
    $pagesize = 25;
}
if (is_null($pagesize)) {
    $pagesize = 25;
}
if (!isset($page)) {
    $pagesize = 1;
}
if (is_null($page)) {
    $pagesize = 1;
}
if ($pagesize !== 0) {
    $sqlLimit = ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
}
//Not e4s and not appadmin
if (!$allAthletes) {
    if ($sql === '') {
//        Entry4UIError(8201, "Error Getting Athletes.", 200, "");
        Entry4UISuccess();
    }
}

//  Filter fields
$filterSql = '';
$filterOp = '';
// Athlete Filter fields
if ($filterFirstname !== '') {
    $filterSql .= $filterOp . " firstname like '" . $filterFirstname . "%' ";
    $filterOp = ' and ';
}
if ($filterSurname !== '') {
    $filterSql .= $filterOp . ' (' . getSafeSQLSearchText('surname') . " like '" . getSafeSearchText($filterSurname) . "%' or surname like '" . $filterSurname . "%' )";
}else{
	$filterSql .= $filterOp . ' surname != "" ';
}
$filterOp = ' and ';

if ($filterGender !== 'F' and $filterGender !== 'M') {
    $filterGender = '';
}
$checkAreaInfo = true;
if (!is_null($compObj) ){
    $checkAreaInfo = !$compObj->isClubcomp();
}
if ( $checkAreaInfo) {
    if ($filterRegion !== '') {
        $filterSql .= $filterOp . " ar_region.name like '" . $filterRegion . "%' ";
        $filterOp = ' and ';
    }
    if ($filterCounty !== '') {
        $filterSql .= $filterOp . " ar_county.name like '" . $filterCounty . "%' ";
        $filterOp = ' and ';
    }
    if ($filterClub !== '') {
//    $filterClub = str_replace("`","\'", $filterClub);
        $filterSql .= $filterOp . " c.Clubname like '" . $filterClub . "%' ";
        $filterOp = ' and ';
    }
}
if ($sql !== '') {
    $sql = ' (' . $sql . ')';
}
//var_dump($isOrg, $allAthletes);
if (!isE4SUser() and !$isAAI and !$isOrg) {
    if ($isOrg and !$allAthletes) {
        if ($sql === '') {
            $sql = $enteredAthletesSQL;
        } else {
            $sql = $sql . ' or (' . $enteredAthletesSQL . ')';
        }
    } else {
        if ($isOrg) {
            if ($sql === '') {
                $sql = $enteredAthletesSQL;
            } else {
                $sql = $sql . ' and (' . $enteredAthletesSQL . ')';
            }
        }
    }
}
if ($sql !== '') {
    $sql = ' (' . $sql . ')';
}

if ($filterSql !== '') {
    if ($sql === '') {
        $filterOp = '';
    } else {
        $filterOp = ' and ';
    }
    $filterSql = $filterOp . $filterSql;
}
$where = '';
if ($sql !== '' or $filterSql !== '') {
    $where = ' where ';
}

$sqlToExecute = $sql1 . $where . $sql . $filterSql . $sortSql . $sqlLimit;
// Perform SQL Statement to get Athletes
$result = e4s_queryNoLog($sqlToExecute);

//$athletes = $result->fetch_all(MYSQLI_ASSOC);
$retAthletes = array();
$athleteObj = new athleteClass(FALSE);
$baseAGRows = e4s_getBaseAgeGroups();
//foreach ( $athletes as $athlete ){
$date = null;
if ( !is_null($compObj)){
    $date = $compObj->getDate();
}
while ($athlete = $result->fetch_assoc()) {

    $athlete = $athleteObj->checkAndUpdateExtAthlete($athlete, $date);

    $aOptions = e4s_getOptionsAsObj($athlete['options']);
    $override = FALSE;
    if (isset($aOptions->compOverride)) {
        $override = $aOptions->compOverride === $compid;
    }
    $reason = '';
    $continue = FALSE;
    $useClubID1 = TRUE;
    $useClubID2 = TRUE;
    if ((int)$athlete['club2id'] === 0 or $isSchool) {
        $useClubID2 = FALSE;
        // set to zero so in a school comp you dont get the option to switch
        $athlete['club2id'] = 0;
    }

    if (!$noComp and !$override) {
        $athleteSecurity = e4s_getAthleteSecurityForComp($compObj, $athlete);
        if (!$athleteSecurity->allowed) {
            if ($athleteSecurity->inClub === FALSE) {
                if ($athleteSecurity->clubPublicDate !== '' and $athleteSecurity->clubPublicDate !== E4S_CLUB_SECURE_COMP) {
                    $dt = strtotime($athleteSecurity->clubPublicDate);
                    $reason = 'Public Entry opens ' . date('jS F Y h:ia', $dt);
                } else {
                    $clubName = $athlete['clubname'];
                    if (is_null($clubName)) {
                        $clubName = E4S_UNATTACHED;
                    }
                    $reason = 'Not Open to ' . $clubName . ' athletes';
                }

            } elseif ($athleteSecurity->inArea === FALSE) {
                $county = $athlete['county'];
                if (is_null($county)) {
                    $county = '';
                }
                $region = $athlete['region'];
                if (is_null($region)) {
                    $region = '';
                }
                $displayArea = 'athletes from ' . $county . '/' . $region;
                if ($county === '' and $region === '') {
                    $displayArea = E4S_UNATTACHED . ' athletes';
                }
                if ($county === '' and $region !== '') {
                    $displayArea = 'athletes from ' . $region;
                }
                if ($county !== '' and $region === '') {
                    $displayArea = 'athletes from ' . $county;
                }
                if ($county === $region and $county !== '') {
                    $displayArea = 'athletes from ' . $county;
                }
                $reason = 'Not Available to ' . $displayArea;
            }
        }

        $useClubID1 = $athleteSecurity->inClub1;
        $useClubID2 = $athleteSecurity->inClub2;
    }
    $uid = $_SERVER['REMOTE_ADDR'];
    $athlete['infoText'] = '';

    if ($reason !== '') {
        if ($showAllAthletes === TRUE) {
            $athlete['infoText'] = $reason;
            $ag = array();
            $ag['ageGroups'] = getNoAgeGroupArr();
            $ag['ageGroups'][0]['id'] = 12;
            $ag['ageGroups'][0]['Name'] = $reason;
            $ag['ageGroups'][0]['competitionAge'] = 0;
            $ag['ageGroups'][0]['currentAge'] = 0;
            $ag['ageGroups'][0]['compDate'] = '';
            $ag['ageGroups'][0]['agid'] = 0;
            $ag['ageGroups'][0]['minAge'] = 0;
            $ag['ageGroups'][0]['toDate'] = '';
            $ag['ageGroups'][0]['fromDate'] = '';
            $ag['ageGroups'][0]['MaxAge'] = 0;
            $ag['ageGroups'][0]['AtDay'] = 0;
            $ag['ageGroups'][0]['AtMonth'] = 0;
            $ag['ageGroups'][0]['year'] = 0;
            $ag['ageGroups'][0]['minAtDay'] = null;
            $ag['ageGroups'][0]['minAtMonth'] = null;
            $ag['ageGroups'][0]['minYear'] = null;
            $ag['ageGroups'][0]['keyName'] = '';
            $ag['ageGroups'][0]['options'] = '[]';
            $ag['ageGroup'] = $ag['ageGroups'][0];
            $ag['vetAgeGroup'] = null;
            $ag['currentAge'] = 0;
            $ag['competitionAge'] = 0;
            $athlete['ageInfo'] = $ag;
        } else {
            continue;
        }
    } else {
        $ag = getAgeGroupInfo($allCompDOBs, $athlete['dob']);

        if ((int)$ag['ageGroup']['id'] === 0 and !$showAllAthletes) {
//            addDebug("No Age Group");
            continue;
        }

        if (!$noComp) {
            $baseGroup = e4s_getBaseAGForDOBForComp($compid, $athlete['dob'], $baseAGRows);
            if (!is_null($baseGroup)) {
                // $ag['ageGroup'] = $baseGroup;
            }
        }
        if (array_key_exists('ageGroup', $ag)) {
            $ag['ageGroup']['Name'] = e4s_getVetDisplay($ag['ageGroup']['Name'], '');
            $ag['ageGroup']['shortName'] = e4s_getVetDisplay($ag['ageGroup']['shortName'], '');
        }
        // when on VPS this is returning null ?
        if (array_key_exists('vetAgeGroup', $ag) and !is_null($ag['vetAgeGroup'])) {
            if (!is_null($ag['vetAgeGroup']['Name'])) {
                $ag['vetAgeGroup']['Name'] = e4s_getVetDisplay($ag['vetAgeGroup']['Name'], '');
            }
        }

        $nowStr = date('Y-m-d');
        if ($noComp) {
            $compDate = $nowStr;
        } else {
            $compDate = $compRow['Date'];
        }

        $regDate = $athlete['activeEndDate'];
        $regDateFormatted = '';
		$isIntAthlete = e4s_isAthleteInternational($athlete);
        if ($athlete['URN'] !== '' and !is_null($athlete['URN'])) {
            // Registered Athlete
            if ($allowRegistered === FALSE) {
                $regDateFormatted = ' (Registered not allowed)';
            }
            if ($allowUnregistered === FALSE) {
                if (is_null($regDate)) {
                    $regDateFormatted = ' ( No Registration Info )';
                } else {
                    if ($regDate < $compDate) {
//                        $regDateFormatted = explode("-", $regDate);
//                        $regDateFormatted = $regDateFormatted[2] . "/" . $regDateFormatted[1] . "/" . $regDateFormatted[0];
                        $regDateFormatted = ' (Reg Expired)';
                    }
                }
            }
        } else {
            if ($allowUnregistered === FALSE and !$isIntAthlete){
                $regDateFormatted = ' (Un-Registered not allowed)';
            }
	        if ($allowInternational === FALSE and $isIntAthlete){
		        $regDateFormatted = ' (International not allowed)';
	        }
        }

        if ($regDateFormatted !== '' and $override) {
            $regDateFormatted = '';
        }
        if ($regDateFormatted !== '' and e4s_isUKDomain()) {
            if ( $ag['currentAge'] < 11 ){
                $regDateFormatted = ' (U11 allowed un-registered)';
            }
        }

        // if show only eligable andissue with registration. ignore athlete
        if ($regDateFormatted !== '' and !$showAllAthletes) {
            continue;
        }

        $ag['ageGroup']['Name'] .= $regDateFormatted;
        $ag['ageGroups'][0]['Name'] .= $regDateFormatted;
        $athlete['ageInfo'] = $ag;
        if ($regDateFormatted !== '') {
            $athlete['infoText'] = $regDateFormatted;
        }
    }
    $athlete['allowDelete'] = FALSE;
    if ($userType === E4S_USERTYPE_NONE) {
        $athlete['allowDelete'] = TRUE;
    }
    $athlete['inTeam'] = FALSE;
    if (!$noComp) {
        // deprecated ????
        if ($compRow['teamid'] !== '0') {
            // only check if the comp is a team comp
            $athlete['inTeam'] = getInTeam($compid, $athlete['id']);
        }
    }

    unset($athlete['club_donotuse']);
    if (is_null($athlete['clubname'])) {
        $athlete['clubname'] = E4S_UNATTACHED . ' to a ' . $clubOrSchool;
    }
    if (is_null($athlete['club'])) {
        $athlete['club'] = $athlete['clubname'];
    }

    $athlete['club2'] = '';
    $athlete['club2name'] = '';

    // if 2nd claim club
    if (!$noComp) {
        if (isset($athleteSecurity)) {
            if ($athleteSecurity->inClub2) {
                $claimSQL = 'select *
                     from ' . E4S_TABLE_CLUBS . '
                     where id = ' . $athlete['club2id'];
                $claimresult = e4s_queryNoLog($claimSQL);
                if ($claimresult->num_rows === 1) {
                    $claimClub = $claimresult->fetch_assoc();
                    $athlete['club2'] = $claimClub['Clubname'];
                    $athlete['club2name'] = $claimClub['Clubname'];
                }
            }
        }
    }
    $searchFirstName = '';
    $searchSurName = '';
    if ($search !== '') {
        $searchArr = explode(' ', $search);
        $searchFirstName = $searchArr[0];
        if (sizeof($searchArr) > 1) {
            $searchSurName = trim(str_replace($searchFirstName . ' ', '', $search));
        }
    }

    $athlete['firstName'] = formatAthleteFirstname($athlete['firstName']);
    if (hasUnSafeSearchchars($athlete['firstName']) and $searchFirstName !== '') {
        // returned surname has a special character in it
        if (!hasUnSafeSearchchars($searchFirstName)) {
            // user NOT added chars in search
            if (strpos(strtolower($athlete['firstName']), strtolower($searchFirstName)) === FALSE) {
                // returned name does not match search so get rid of chars
                $athlete['firstName'] = getSafeSearchText($athlete['firstName']);
            }
        }
    }

    $athlete['surName'] = formatAthleteSurname($athlete['surName']);
    if (hasUnSafeSearchchars($athlete['surName']) and $searchSurName !== '') {
        // returned surname has a special character in it
        if (!hasUnSafeSearchchars($searchSurName)) {
            // user NOT added chars in search
            if (strpos(strtolower($athlete['surName']), strtolower($searchSurName)) === FALSE) {
                // returned name does not match search so get rid of chars
                $athlete['surName'] = getSafeSearchText($athlete['surName']);
            }
        }
    }

    $retAthletes[] = $athlete;
}
$where = '';
if ($sql !== '') {
    $where = ' where ';
}
$totalRowsSql = $sql2 . $where . $sql;

$totalresult = e4s_queryNoLog('GetAthletesTotalRows' . E4S_SQL_DELIM . $totalRowsSql);

$totalRow = mysqli_fetch_array($totalresult, TRUE);
$paging['totalCount'] = $totalRow['totalCount'];
$paging['page'] = $page;
$meta = $paging;

// if a UK comp and user is organiser or E4S, check PBs of athletes
//if ( E4S_CURRENT_DOMAIN !== E4S_AOCODE_AAI && !e4s_isDevDomain()){
if ( E4S_CURRENT_DOMAIN !== E4S_AAI_DOMAIN ) {
	if (!is_null($compObj)) {
		if ( isE4SUser() or $compObj->isOrganiser() ) {
			$URNs = array();
			foreach ( $retAthletes as $athlete ) {
				if ( ! is_null( $athlete['URN'] ) ) {
					if ( pof10V2Class::shouldAthleteBeUpdated( $athlete['lastCheckedPB'] )) {
						$URNs[] = $athlete['URN'];
					}
				}
			}
			if ( sizeof( $URNs ) > 0 ) {
				$protocol = $_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
				$url = $protocol . '://' . E4S_CURRENT_DOMAIN . E4S_BASE_URL . 'public/checkPof10forathletes?urns=' . implode( '~', $URNs );
				e4s_fireAndForget($url);
			}
		}
	}
}

Entry4UISuccess('
    "data":' . json_encode($retAthletes, JSON_NUMERIC_CHECK) . ', 
    "ageGroupInfo":' . json_encode($allCompDOBs, JSON_NUMERIC_CHECK) . ', 
    "meta":' . json_encode($meta, JSON_NUMERIC_CHECK));

// Depricated ?
function getInTeam($compid, $athleteid) {
    $sql = 'select e.* 
            from ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce
            where ce.compid = ' . $compid . '
            and ce.id = e.compEventID
            and athleteid = ' . $athleteid . '
            and e.teamid > 0';
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows > 0) {
//        addDebug("Athelete : " . $athleteid . " in team");
        return TRUE;
    }
//    addDebug("Athelete : " . $athleteid . " NOT in team");
    return FALSE;
}

function getEventAgeGroupIds($ceid, &$ignoreGender, &$ceoptions) {

    $sql = 'Select ce.*, e.options eoptions, e.gender gender
            from ' . E4S_TABLE_COMPEVENTS . ' ce, 
                 ' . E4S_TABLE_EVENTS . ' e
            where ce.id = ' . $ceid . '
            and   ce.eventid = e.id';
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(1000, '1000: Failed to get Competition Event for ' . $ceid, 400, '');
    }

    $ceRow = $result->fetch_assoc();
    $returnAgeGroups = $ceRow['AgeGroupID'];
    $gender = $ceRow['gender'];

    $eoptions = e4s_getOptionsAsObj($ceRow['eoptions']);

    if (isset($eoptions->gender) ) {
        $ignoreGender = !$eoptions->gender;
    }

    if ($gender === E4S_GENDER_OPEN) {
        $ignoreGender = true;
    }
    $ceoptions = e4s_addDefaultCompEventOptions($ceRow['options']);

    if (isset($ceoptions->ageGroups)) {
        foreach ($ceoptions->ageGroups as $ageGroup) {
            $returnAgeGroups .= ',' . $ageGroup->ageGroup;
        }
    }

    return $returnAgeGroups;
}
