<?php
include_once E4S_FULL_PATH . 'dbInfo.php';

if ($id === 0) {
    if ($urn !== null and $urn !== '' and $urn !== 0) {
//        addDebug ( "Urn is [". $urn . "]");
        $sql = 'select * from ' . E4S_TABLE_ATHLETE . "
            where urn = '" . $urn . "'
            and   aocode = '" . $aocode . "'";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows > 0) {
            Entry4UIError(8700, 'Duplicate URN', 400, '"id":' . $id);
        }
        $urn = "'" . $urn . "'";
    } else {
//        addDebug("URN set to null");
        $urn = 'null';
    }
    $dobSplit = explode('-', $dob);
    if (!checkdate($dobSplit[1], $dobSplit[2], $dobSplit[0])) {
        Entry4UIError(1000, 'Invalid Date of Birth given.', 200, '');
    }

    $now = date(E4S_SHORT_DATE);
    $currentAge = (int)getAgeFromText($dob, $now, E4S_SHORT_DATE);
    if ($currentAge < 6 || $currentAge > 110) {
        Entry4UIError(1000, 'Invalid Date of Birth. Age :' . $currentAge, 200, '');
    }

    $sql = 'insert into ' . E4S_TABLE_ATHLETE . " (firstname, surname, aocode, urn, dob,clubid, gender,classification,schoolid, type)
            values ('$firstName','$surName','" . $aocode . "'," . $urn . ", '$dob',$clubid,'$gender',$class,$schoolid,'A')";

    $result = e4s_queryNoLog($sql);
    $errno = mysqli_errno($conn);
    $sqlError = mysqli_error($conn);
    $header = 0;
    if ($errno !== 0) {
        $header = 400;
    } else {
        $athleteId = $conn->insert_id;
        include_once E4S_FULL_PATH . 'athlete/userAthletes.php';
        addUserAthlete($athleteId);        // Add to user Athlete table
    }
    Entry4UIError($errno, $sqlError, $header, '"id":' . $athleteId);
}

Entry4UIError(-1, 'id is NOT zero', 400, '"id":' . $id);
