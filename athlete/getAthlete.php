<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
//$id = $_GET['id'];

function e4s_getAthlete($id, $compId) {
    $athleteRecord = null;
    $athleteObj = athleteClass::withID($id);
    if (!is_null($athleteObj)) {
        $athleteRecord = $athleteObj->getRow(TRUE);
    }
    $agObj = new e4sAgeClass(E4S_AGE_DEFAULT_BASE);
    $sendAGs = $agObj->getAgeGroups($compId);

    $meta = new stdClass();
    $meta->ageGroups = $sendAGs;

    Entry4UISuccess('
        "data": ' . json_encode($athleteRecord, JSON_NUMERIC_CHECK) . ',
        "meta": ' . json_encode($meta, JSON_NUMERIC_CHECK)
    );
}

function e4s_checkAndAddAthlete($obj) {
    $userId = e4s_getUserID();
    if ($userId === E4S_USER_NOT_LOGGED_IN or $userId === E4S_USER_ANON_ID) {
        Entry4UIError(6754, 'Please login to run this function');
    }
    $athleteId = checkFieldForXSS($obj, 'id:Athlete ID');
    $dob = checkFieldForXSS($obj, 'dob:Athlete DOB');
    $toUserId = checkFieldForXSS($obj, 'userid:User to link');

    if (isE4SUser() and !is_null($toUserId)) {
        $userId = $toUserId;
    }
    $athleteObj = athleteClass::withID($athleteId);

    if (is_null($athleteObj->athleteRow)) {
        Entry4UIError(6756, 'Sorry, Athlete not found');
    }
    $athleteDob = $athleteObj->getDOB();

    if ($dob !== $athleteDob) {
        Entry4UIError(6780, 'Sorry, Data can not be validated');
    }
    $sql = 'select *
            from ' . E4S_TABLE_USERATHLETES . '
            where userid = ' . $userId . '
            and athleteid = ' . $athleteId;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows === 0) {
        $sql = 'insert into ' . E4S_TABLE_USERATHLETES . ' (userid, athleteid )
            values (
                ' . $userId . ',
                ' . $athleteId . '
            )';
        e4s_queryNoLog($sql);
    }
    Entry4UISuccess($athleteObj->athleteRow);
}