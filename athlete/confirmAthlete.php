<?php
// Passed a URN. return the athlete from our system
// or find in alternative system
//$regid = $_GET['regid'];
//$dob = $_GET['dob'];

include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'athlete/userAthletes.php';

if ($regid === '' or $dob === '') {
    Entry4UIError(1001, 'Please pass registration number and date of birth.', 200, '');
}

$dobSplit = explode('-', $dob);
if (!checkdate($dobSplit[1], $dobSplit[2], $dobSplit[0])) {
    Entry4UIError(1000, 'Invalid Date of Birth given.', 200, '');
}
$now = date(E4S_SHORT_DATE);
$currentAge = (int)getAgeFromText($dob, $now, E4S_SHORT_DATE);
if ($currentAge < 6 || $currentAge > 110) {
    Entry4UIError(1000, 'Invalid Date of Birth. Age :' . $currentAge, 200, '');
}
$arr = array();
$arr['dob'] = $dob;
$arr['regid'] = $regid;
$arr['aocode'] = $aocode;
$arr['clubid'] = $clubid;
$arr['classification'] = $classification;
if ($clubid !== 0) {
    updateAthlete($arr);
}
checkForAthlete($arr);

Entry4UIError(1000, 'Can Not find athlete for ' . $regid . '.', 200, '');

function updateAthlete($arr) {

    $sql = 'update ' . E4S_TABLE_ATHLETE . '
            set clubid = ' . $arr['clubid'] . ',
                classification = ' . $arr['classification'] . "
            where aocode = '" . $arr['aocode'] . "'
            and urn = " . $arr['regid'];
    e4s_queryNoLog($sql);
}

function checkForAthlete($arr) {

    $sql = 'Select a.*, c.Clubname clubname
        from ' . E4S_TABLE_ATHLETE . ' a,
             ' . E4S_TABLE_CLUBS . ' c
        where urn = ' . $arr['regid'] . "
        and aocode = '" . $arr['aocode'] . "'
        and a.clubid = c.id";

    $result = e4s_queryNoLog($sql);
    if ($result->num_rows > 0) {

        // athlete found for URN. So Does the DOB match
        $athleteRec = mysqli_fetch_array($result, TRUE);
        logSql('4.checkForAthlete: dobs=[' . $athleteRec['dob'] . '][' . $arr['dob'] . ']', 1);
        if ($athleteRec['dob'] === $arr['dob']) {
            unset($athleteRec['club_donotuse']);
            addUserAthlete($athleteRec['id']);
            Entry4UISuccess('');
        } else {
            Entry4UIError(1002, 'Can not find athlete with the date of birth supplied.', 200, '');
        }
    }
    return FALSE;
}