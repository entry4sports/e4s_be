<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'entries/commonEntries.php';

define('E4S_NO_NONCE', '');
define('E4S_CHECKIN_TABLE', 'Entry4_Checkin');

function e4s_list_Checkin($obj, $isOrgRequest) {
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);

    $model = new stdClass();
    e4s_addStdPageInfo($obj, $model);

    $parmsObj = new stdClass();
    $parmsObj->date = checkFieldForXSS($obj, 'date:Competition Date');

    $parmsObj->urn = checkFieldForXSS($obj, 'urn:Athlete URN');
    $parmsObj->bibno = checkFieldForXSS($obj, 'bibno:Athletes Bib number');
    $parmsObj->club = checkFieldForXSS($obj, 'club:Club Name');
    $parmsObj->dob = checkFieldForXSS($obj, 'dob:Athlete DOB');
    $parmsObj->entity = checkFieldForXSS($obj, 'entity:Entity');
    $parmsObj->entitylevel = checkFieldForXSS($obj, 'entitylevel:Entity Level');
    $parmsObj->firstname = checkFieldForXSS($obj, 'firstname:Athletes Firstname');
    $parmsObj->surname = checkFieldForXSS($obj, 'surname:Athletes Surname');
    $parmsObj->eventname = checkFieldForXSS($obj, 'event:Event name');
    $parmsObj->egId = checkFieldForXSS($obj, 'egid:Event Group ID');
    $parmsObj->collected = (int)checkFieldForXSS($obj, 'collected:Bib Collected Status');
    $parmsObj->userNonce = checkFieldForXSS($obj, 'nonce:User Nonce');
    $parmsObj->whois = checkFieldForXSS($obj, 'whois:Who is here');
    $parmsObj->pagesize = (int)$model->pageInfo->pagesize;
    $parmsObj->page = (int)$model->pageInfo->page;

    $checkInObj = new checkinClass($compid, $parmsObj);
    $checkInObj->listCheckins(null, $isOrgRequest, false);
}

function e4s_veryifyEmailIdAndCompCode($obj) {
    $emailid = checkFieldForXSS($obj, 'emailid:Email ID');
    $compcode = checkFieldForXSS($obj, 'compcode:Competition Code');
    $date = checkFieldForXSS($obj, 'date:Competition Date');

    $sql = '
        select id, userid , compid
        from ' . E4S_TABLE_ENTRYUSERCODES . "
        where emailid = '" . $emailid . "';
    ";

    $result = e4s_queryNoLog($sql);
    if ($result->num_rows === 0) {
        Entry4UIError(6010, 'Invalid email id passed.', 200, '');
    }
    $row = $result->fetch_assoc();

    $codeId = (int)$row['id'];
    $compid = (int)$row['compid'];
    $userid = (int)$row['userid'];
    $checkinObj = e4s_getCheckInObj($compid);
    $todaysCode = $checkinObj->getCode($date);
    if ($todaysCode !== $compcode) {
        Entry4UIError(6020, 'Invalid code entered.', 200, '');
    }
    // If we are here. we have a valid email id and competition code
    $nonce = e4s_generateUserCompNonce($compid, $userid, 'Y');
    e4s_updateUserNonce($codeId, $nonce);
    $retObj = new stdClass();
    $retObj->compId = $compid;
    $retObj->nonce = $nonce;
    Entry4UISuccess('
        "data":' . e4s_encode($retObj));
}

function e4s_getCheckinSummary($obj) {
    $checkinObj = new stdClass();
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $checkinObj->date = checkFieldForXSS($obj, 'date:Competition Date');

    $checkObj = new checkinClass($compid, $checkinObj);
    $checkObj->getSummary(true);
}

function e4s_updateCheckins($obj, $orgUpdate) {
    $params = $obj->get_params('JSON');
    $data = $params['data'];
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $parmsObj = new stdClass();
    $parmsObj->orgUpdate = $orgUpdate;
    $checkObj = new checkinClass($compid, $parmsObj);
    $checkObj->checkinEntries($data);
}

function e4s_getCheckInObj($compid) {
    $checkinObj = new stdClass();
    return new checkinClass($compid, $checkinObj);
}

function e4s_clear_Checkin($obj) {
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (is_null($compid)) {
        Entry4UIError(6020, 'No Competition ID has been passed in ', 200, '');
    }
    $compid = (int)$compid;

    $all = checkFieldForXSS($obj, 'all:All Checkins');
    if (is_null($all)) {
        $all = 0;
    }
    $all = (int)$all;
    $collected = checkFieldForXSS($obj, 'collected:Bib Collections');
    if (is_null($collected)) {
        $collected = 0;
    }
    $collected = (int)$collected;
    $checkedin = checkFieldForXSS($obj, 'checkedin:Checked in');
    if (is_null($checkedin)) {
        $checkedin = 0;
    }
    $checkedin = (int)$checkedin;
    $parmsObj = new stdClass();
    $parmsObj->all = $all;
    $parmsObj->collected = $collected;
    $parmsObj->checkedin = $checkedin;
    $checkObj = new checkinClass($compid, $parmsObj);
    $checkObj->clearCheckins();
    Entry4UISuccess();
}