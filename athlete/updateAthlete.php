<?php
include_once E4S_FULL_PATH . 'dbInfo.php';

if ($id !== 0) {
    if ((int)$clubid === 0) {
        $clubid = E4S_UNATTACHED_ID;
    }

    $sql = 'update ' . E4S_TABLE_ATHLETE . " 
             set firstname = '$firstName'
             , surname = '$surName'
             , urn = $urn
             , dob = '$dob'
             , clubid = $clubid
             , gender = '$gender'
             , classification = $class
             , schoolid = $schoolid
            where id = $id";

    $result = e4s_queryNoLog($sql);
    $errno = mysqli_errno($conn);
    $sqlError = mysqli_error($conn);
    $header = 0;
    if ($errno !== 0) {
        $header = 400;
    }
    Entry4UIError($errno, $sqlError, $header, '"id":' . $id);
}

Entry4UIError(-1, 'id is zero', 400, '"id":' . $id);
