<?php

include_once E4S_FULL_PATH . 'dbInfo.php';
$conn = $GLOBALS['conn'];
// Get Clubs
$sql = 'Select * from ' . E4S_TABLE_CLUBS . "
        where Country = 'Eire'";
$result = mysqli_query($conn, $sql);
$clubsArr = mysqli_fetch_all($result, MYSQLI_ASSOC);
$clubs = array();
foreach ($clubsArr as $key => $value) {
    $club = addslashes($value['Clubname']);
    $clubs[$club] = $value['id'];
}
//e4s_dump($clubs);
//echo "<Br>";
$sql = 'select *
        from ' . E4S_TABLE_ATHLETELOAD;
//        limit 1,10";

$result = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($result, TRUE)) {
    $club = addslashes($row['club']);
    $firstname = addslashes($row['firstname']);
    $surname = addslashes($row['surname']);
    if ($club !== '') {
        $club = clubTranslate($club);
        if (isset ($clubs[$club])) {
            $athletesql = 'select * from ' . E4S_TABLE_ATHLETE . '
                      where urn = ' . $row['urn'];
            $athleteresult = mysqli_query($conn, $athletesql);
            $sql = '';
            if ($athleteresult->num_rows === 1) {
//                echo "<br>Updating " . $row['urn'];
                $sql = 'update ' . E4S_TABLE_ATHLETE . "
                            set firstname = '" . $firstname . "',
                                surname = '" . $surname . "',
                                gender = '" . $row['gender'] . "',
                                dob = '" . $row['dob'] . "',
                                clubid = " . $clubs [$club] . '
                         where urn = ' . $row['urn'];
                $sql = '';
            } else {
//                echo "<br>Inserting " . $row['urn'];
                $sql = 'insert into ' . E4S_TABLE_ATHLETE . ' (urn, firstname, surname, gender,dob, clubid)
                        values (' . $row['urn'] . ",'" . $firstname . "','" . $surname . "','" . $row['gender'] . "','" . $row['dob'] . "', " . $clubs [$club] . ')';
            }
            if ($sql !== '') {
                logSql($sql, 1);
                $updateresult = mysqli_query($conn, $sql);
            }
        } else {
            echo '<br>Failed to insert ' . $row['urn'] . ' Club :[' . $club . ']:[' . $row['club'] . ']';
        }
    } else {
        echo '<br>No club ! ';
//        e4s_dump($row);
    }
}

echo '<br>done';