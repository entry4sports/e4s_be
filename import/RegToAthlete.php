<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
define('E4S_VALIDATION_ERROR', 400);

function e4s_athleteRegistration($system, $status, $firstname, $surname, $dob, $regid, $gender, $regdate, $club, $externId, $schoolId, $class, $exit = TRUE) {
    echo $regid . ' ' . $firstname . ' ' . $surname . "\n";
    global $conn;
    $aocode = E4S_AOCODE_AAI;
    if ($system === E4S_SYSTEM_ANI) {
        $regid = str_replace('ani', '', strtolower($regid));
        $aocode = E4S_AOCODE_ANI;
    }
    if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
        $uid = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $uid = $_SERVER['REMOTE_ADDR'];
    }
//logtxt("Athlete Live Initiated from : " . strtolower($_SERVER['SERVER_NAME']) . "--" . $uid);
    if (!e4s_checkHeaderFor('Ani-Key', md5(E4S_ANI_REGISTRATION))) {
        if (!isSecureRequestorIP() and !isE4SUser()) {
            Entry4UIError(9501, 'You do not have access to this function.', E4S_VALIDATION_ERROR, '');
        }
    }
// Get the first character
    $fullStatus = $status;
    $status = e4s_left($status, 1);
    if ($status === E4S_CRUD_INSERT) {
        $status = E4S_CRUD_CREATE;
    }
    if ($status === '') {
        Entry4UIError(1000, 'Must pass a valid status value.', E4S_VALIDATION_ERROR, '');
    }

    if ($dob !== '') {
        // ensure its a valid date
        if ( strpos($dob, '-') !== false ) {
            $date = explode('-', $dob);
        }else{
            if ( strpos($dob, '/') !== false ) {
                $date = explode('/', $dob);
            }
        }
        if (checkdate($date[1], $date[2], $date[0]) === FALSE) {
            Entry4UIError(1001, 'Must pass a valid Date of birth.[' . $dob . ']', E4S_VALIDATION_ERROR, '');
        }
    }
    $firstname = checkName($firstname);
    $surname = checkName($surname);
    if ($regdate === null) {
        Entry4UIError(1002, 'Must pass a valid registration date active to.', E4S_VALIDATION_ERROR, '');
    } else {
//        e4s_addDebugForce($regdate);
        if ( strpos($regdate, '-') !== false ) {
            $date = explode('-', $regdate);
        }else{
            if ( strpos($regdate, '/') !== false ) {
                $date = explode('/', $regdate);
            }
        }
//        e4s_addDebugForce($date);
        if (checkdate($date[1], $date[2], $date[0]) === FALSE) {
            Entry4UIError(1003, 'Must pass a valid registration date.[' . $regdate . ']', E4S_VALIDATION_ERROR, '');
        }
        if ($aocode === E4S_AOCODE_ANI) {
//        e4s_adjustRegDate($regdate);
        }
    }
    if ($regid === null) {
        Entry4UIError(1004, 'Invalid Registration ID passed', E4S_VALIDATION_ERROR, '');
    } else {
        if (strlen($regid) !== 6) {
            Entry4UIError(1005, 'Invalid Registration number passed (' . $regid . ')', E4S_VALIDATION_ERROR, '');
        }
    }

    $clubid = 0;
    if (is_null($externId)) {
        $externId = 0;
    }else{
        $externId = $aocode . '-' . $externId;
    }
    if ($club !== '') {
        $clubid = getClub($club, $externId);
    } else {
        Entry4UIError(1007, 'Club passed is empty', E4S_VALIDATION_ERROR, '');
    }

    if ($status !== E4S_CRUD_DELETE) {
        $sql = 'select * from ' . E4S_TABLE_ATHLETE . '
                where urn = ' . $regid . "
                and aocode = '" . $aocode . "'";
        $result = e4s_queryNoLog($sql);

        $row = $result->fetch_assoc();
//    if (( is_null($row) and $status !== E4S_CRUD_CREATE) || (!is_null($row) and $status === E4S_CRUD_CREATE)){
        // allow for Insert when actually its now an update.
        if ((is_null($row) and $status !== E4S_CRUD_CREATE)) {
            e4s_addDebugForce($sql);
            Entry4UIError(1008, 'Invalid Status/Registration ID passed. ' . $fullStatus, E4S_VALIDATION_ERROR, '');
        }
        $status = E4S_CRUD_CREATE;
        if ($result->num_rows > 0) {
            $status = E4S_CRUD_UPDATE;

            $content = '{';
            $sep = '';
            foreach ($row as $key => $value) {
                $content .= $sep . '"' . $key . '":"' . $value . '"';
                $sep = ', ';
            }
            $content .= '}';
//        logSql($content, 1);
        }
        // dont need to log the search
//    logAthlete($status, $regid, $aocode, $sql);
    }

    if ($status === E4S_CRUD_UPDATE) {
        $sql = 'update ' . E4S_TABLE_ATHLETE . " 
             set firstname = '$firstname'
             , surname = '$surname'
             , dob = '$dob'
             , clubid = $clubid
             , gender = '$gender'
             , classification = $class
             , schoolid = $schoolId
             , activeEndDate = '$regdate'
           where urn = '" . $regid . "'
            and aocode = '" . $aocode . "'";
    }
    if ($status === E4S_CRUD_CREATE) {

        $sql = 'insert into ' . E4S_TABLE_ATHLETE . " (firstname, surname, aocode, urn, dob,clubid, gender,classification,schoolid, activeEndDate)
            values ('$firstname','$surname','$aocode','" . $regid . "', '$dob',$clubid,'$gender','$class',$schoolId, '$regdate')";
    }

    if ($status === E4S_CRUD_DELETE) {
        $sql = 'update ' . E4S_TABLE_ATHLETE . "
            set activeEndDate = '1970-01-01'
            where urn = '" . $regid . "'
            and aocode = '" . $aocode . "'";
    }

    logAthlete($status, $regid, $aocode, $sql);
    $result = e4s_queryNoLog($sql);

    $errno = mysqli_errno($conn);
    $sqlError = mysqli_error($conn);
    $header = 0;
    if ($errno !== 0) {
        $header = E4S_VALIDATION_ERROR;
    }

//Entry4UIError ($errno, $sqlError, $header,'"id":' . $regid );
    if ($exit) {
        Entry4UISuccess('');
    }
}

function e4s_adjustRegDate(&$regdate) {
//    YY-MM-DD
    $date = explode('-', $regdate);
    if ($regdate < ($date[0] . '-03-31')) {
        $regdate = $date[0] . '-03-31';
    } else {
        $regdate = ((int)$date[0] + 1) . '-03-31';
    }
}

function logAthlete($status, $regid, $aocode, $sql) {
    $insertsql = 'INSERT INTO ' . E4S_TABLE_ATHLETELOG . ' ( created, aocode, urn, status, sql_cmd, uid) VALUES (' . returnNow() . ",'" . $aocode . "'," . $regid . ",'" . $status . "', '" . addslashes($sql) . "','" . $_SERVER['REMOTE_ADDR'] . "')";
    e4s_queryNoLog($insertsql);
}


function getClub($club, $externId) {
    $getByName = TRUE;
    $updateExternID = FALSE;
    $club = clubTranslate($club);
    if (!is_null($externId)) {
        $clubsql = 'select * from ' . E4S_TABLE_CLUBS . " 
             where externid = '" . $externId . "'";
        $resultclub = e4s_queryNoLog($clubsql);
        if ($resultclub->num_rows === 1) {
            $getByName = FALSE;
        }
    }
    if ($getByName) {
        $clubsql = 'select * from ' . E4S_TABLE_CLUBS . " 
             where Clubname = '" . addslashes($club) . "'";
        $resultclub = e4s_queryNoLog($clubsql);
        if ($resultclub->num_rows === 0) {
            return createClub($club, $externId);
        } else {
            $updateExternID = TRUE;
        }
    }

    $row = $resultclub->fetch_assoc();
    $clubid = (int)$row['id'];
    if ($updateExternID or $club !== $row['Clubname']) {
        updateClub($clubid, $club, $externId);
    }
    return $clubid;
}

function updateClub($id, $club, $externId) {
    if (isE4SUser()) {
//        var_dump($externId);
    }
    if (is_null($externId) or $externId === '') {
        $externId = 0;
    }
    $sql = 'update ' . E4S_TABLE_CLUBS . "
            set externid = '" . $externId . "',
                clubname = '" . addslashes($club) . "'
            where id = " . $id;
    e4s_queryNoLog($sql);
}

function createClub($club, $externId) {
//    return the clubs id form our system
    $areaid = 6;
    $club = addslashes($club);
    $sql = 'insert into ' . E4S_TABLE_CLUBS . " (externid, clubname, areaid,clubtype,country,active)
            values (
                '" . $externId ."',
                '" . $club . "',
                $areaid,
                'C',
                'EIRE',
                1
            )";
    e4s_queryNoLog($sql);
    $newId = e4s_getLastID();
    sendNewClubEmail($newId . ':' . $club);
    return $newId;
}

function sendNewClubEmail($reference) {
    $bccUs = FALSE;
    $config = e4s_getConfig();
    $configOptions = $config['options'];
    $email = 'paul@entry4sports.com';
    if ( isset($configOptions->newClubEmail) ){
        $email = $configOptions->newClubEmail;
    }

    $fromEmail = $email;
    $body = E4S_CURRENT_DOMAIN . ' New Club generated.' . '<br><br>Generated from the Live import, club ' . $reference . ' has been created. This will need updating with the correct area.';
    $body .= "<br>If you haven't already, let E4S know what county this new club is located within otherwise athletes from this new club may not be able to enter competitions.";
    $body .= Entry4_emailFooter($email);

    $response = e4s_mail($email, 'New Club. ' . E4S_CURRENT_DOMAIN . '/' . $reference, $body, Entry4_mailHeader($fromEmail, $bccUs));
    if (!$response) {
        Entry4UIError(9309, 'Failed to send email of new club : ' . $reference, 200, '');
    }
}

function checkName($name) {
    $name = addslashes(e4s_getCapitalisedName($name));
    $name = str_replace('&oacute;', 'Ó', $name);
    $name = str_replace('&aacute;', 'Á', $name);
    $name = str_replace('&eacute;', 'É', $name);
    return $name;
}