<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';

$newline = "\n\n";
$subjectTest = 'Roscommon X-Country 2022. ';

$body1 = 'You have been nominated as your school contact for entering athletes/pupils in to the Roscommon 2022 X-Country competition.' . $newline;
$body1 .= 'Your username to logon is {USERNAME}' . $newline;
$body1 .= 'Password is : {PASSWORD}' . $newline;
$body1 .= 'Priority Code : Ros2022' . $newline;
$body1 .= 'You will be sent a further email with the priority code required and instructions on how to enter when the competition is configured and made active.' . $newline;
$body1 .= 'Please use the link below to go direct to the competition. You can also find it by typing the competition number 637 or Roscommon in the filter area at the top of the entries home page.' . $newline;
$body1 .= 'Direct Link : https://entry.athleticsireland.ie/roscommon2022' . $newline;
$body1 .= 'General Link : https://entry.athleticsireland.ie' . $newline;
$footer = $newline . 'Thanks' . $newline . 'Entry4Sports Support Team' . $newline;
$footer .= 'https://entry4sports.com' . $newline . $newline;
$footer .= 'Do NOT reply to this email as it is sent from an unmonitored e-mail. If you have any questions, please contact the organiser at roscommoncountyboard@gmail.com.';
//
//$body2 = "The priority code required to enter the competition is :\n";
//$body2 .= "AchRels21\n";
//$body2 .= "\nPlease DO NOT share this code with others.\n";
//$body2 .= $footer;
include_once E4S_FULL_PATH . 'admin/userMaint.php';
include_once E4S_FULL_PATH . 'builder/clubCRUD.php';
$sql = 'select *
        from importUsers';
$result = e4s_queryNoLog($sql);
while ($obj = $result->fetch_object()) {
    $email = $obj->email;
    $arr = preg_split('~@~', $email);
    $userName = $obj->info;

    $password = $userName . '-' . $obj->id;

    echo 'Processing : ' . $email . ':' . $password . "\n";
    $wpUserId = wp_create_user($userName, $password, $email);
    if (gettype($wpUserId) === 'object') {
        $wpUserObj = get_user_by('email', $email);
        $wpUserId = $wpUserObj->ID;
        wp_set_password($password, $wpUserId);
    }

//    $entries = "select clubid
//                from " . E4S_TABLE_ENTRIES . "
//                where userid = " . $wpUserId;
//    $entriesResult = e4s_queryNoLog($entries);
//    if ( $entriesResult->num_rows > 0 ){
//        $entry = $entriesResult->fetch_object();
//        $clubId = (int)$entry->clubid;
//    }
//    if ( $clubId === 0){
    $club = addslashes($obj->name);
    $clubSql = 'select *
                from ' . E4S_TABLE_CLUBS . "
                where clubname = '" . $club . "'
                and   clubtype = 'S'";
    $clubResult = e4s_queryNoLog($clubSql);
    if ($clubResult->num_rows === 1) {
        $clubObj = $clubResult->fetch_object();
        $clubId = $clubObj->id;
    } else {
        $clubmodel = new stdClass();
        $clubmodel->id = 0;
        $clubmodel->clubname = $club;
        $clubmodel->region = 'Roscommon';
        $clubmodel->country = 'Eire';
        $clubmodel->clubtype = 'S';
        $clubmodel->areaid = 22;
        $clubmodel->active = TRUE;
        $clubRow = e4s_createClub($clubmodel, FALSE);
        $clubId = (int)$clubRow['id'];
    }
//    }
    if ($clubId !== 0) {
        addUserToClub($wpUserId, $clubId, TRUE);
    }

    $body = $newline . str_replace('{USERNAME}', $userName, $body1);
    $body = str_replace('{PASSWORD}', $password, $body);
    $body .= $footer;
    wp_mail($email, $subjectTest . 'E4S/Roscommon 2022 X-Country User Information', 'Dear ' . $arr[0] . ',' . $body);
}
Entry4UISuccess();

