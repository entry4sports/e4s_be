<?php
/**
 * Created by PhpStorm.
 * User: pauld
 * Date: 18/12/2018
 * Time: 20:44
 */
///////////////
// This is the end point to call to Manually load athletes 10/2022
// https://entry.athleticsireland.ie/wp-json/e4s/v5/v3flatimport
// which calls import_irish_athletes_loadflatv3 in functions.php
// If you want to run manually, as it sets the following
// $GLOBALS['e4s_import_id'] = 1; // (AAI) or 2 (NI);
// and put the filename in the outputfile in the import config record
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
importDebug('Import Started Manually');
define('E4S_IMPORT_DISABLED', 0);
define('E4S_IMPORT_ENABLED', 1);
// get a runtime code for setting the config to
define('E4S_IMPORT_CODE', getProcessCode(10));
define('E4S_IMPORT_EMAIL', 'paul@entry4sports.com');
define('E4S_OUTPUT_FILE', 'diff_');

$GLOBALS['e4s_import_email'] = E4S_IMPORT_EMAIL;
$config = getImportConfig();

$config['processing'] = E4S_IMPORT_CODE;
$GLOBALS['e4s_import_email'] = $config['email'];
if ($GLOBALS['e4s_import_email'] === '') {
    $GLOBALS['e4s_import_email'] = E4S_IMPORT_EMAIL;
}

$importFromDir = $config['importfromdir'];
$importedDir = $config['importeddir'];
$filePrefix = $config['fileprefix'];
$outputfile = $importFromDir . $config['outputfilename'];

importDebug('About to processFile : ' . $outputfile);

processFile($config, $outputfile);
exit();

function importDebug($txt) {
//    logTxt("Athlete Import : " . $txt);
    echo $txt . "\n";
}

function markConfigReady(&$config) {
    $config['active'] = E4S_IMPORT_ENABLED;
    $config['processing'] = '';
}

function reset_config($config) {
    $config['currentfilename'] = '';
    $config['startcnt'] = 0;
    markConfigReady($config);
    updateImportConfig($config);
}

function processFile($config, $fileName) {
    importDebug('Processing : ' . $fileName);
    $clubs = getClubs();
//    $fileName = $config['importfromdir'] . "/" . E4S_OUTPUT_FILE;
    $errorsOccurred = '';
    $file = fopen($fileName, 'r');
    $cnt = 1;
    $insertCnt = 0;
    $updatedCnt = 0;

    $processingDiff = FALSE;
    if (strpos($fileName, E4S_OUTPUT_FILE) !== FALSE) {
        $processingDiff = TRUE;
        importDebug('Processing Diff file');
    } else {
        importDebug('Processing FULL file');
    }

    $startCnt = (int)$config['startcnt'];
    if ($startCnt < 2 and !$processingDiff) {
        $startCnt = 2;
    }
    $loadCnt = (int)$config['loadcnt'];
    $endCnt = $startCnt + $loadCnt;

    while ($line = fgets($file)) {
        if ($processingDiff) {
            if (substr($line, 0, 1) !== '>') {
                continue;
            }

            $line = str_replace('> ', '', $line);
        }
        $Error = '';

        if ($cnt >= $startCnt) {
            if ($cnt % 100 === 0) {
                $config['processcnt'] = $cnt;
                importDebug('Processed Count : ' . ($cnt - $startCnt));
                updateImportConfig($config);
            }
            $line = explode(',', $line);

            $regid = $line[0];
//            importDebug("Checking:" . $regid);
            $regid = str_replace($config['aocode'], '', $regid);

            $firstname = trim(addslashes($line[1]), ' ');
            $surname = trim(addslashes($line[2]), ' ');
            $club = trim($line[3], ' ');
            $gender = $line[4];
            $dob = convertDate($line[5]);
            $disability = $line[6];
            $active = convertDate($line[7]);

            if ($cnt >= $endCnt and $loadCnt > 0) {
                importDebug('Max counter reached');
                $config['startcnt'] = $endCnt;
                $config['processcnt'] = $cnt;

                importComplete($config, $file, FALSE);
                exit();
            }

            if ($club !== '') {
                $origClub = $club;
                $club = trim(clubTranslate($club));

                if (!isset ($clubs[$club])) {
                    importDebug('[' . $club . '][' . $origClub . ']');
                    createClubFlat($club, 0, $clubs);
                }

                if (isset ($clubs[$club])) {
                    $athletesql = 'select * from ' . E4S_COMMON_PREFIX . 'Athlete
                      where urn = ' . $regid . "
                      and aocode = '" . $config['aocode'] . "'";
//                    logSql($athletesql,1);
                    $athleteresult = e4s_queryNoLog($athletesql);
                    $sql = '';
                    $status = 'Import';
                    $row = null;

                    if ($athleteresult->num_rows === 1) {
//                        importDebug("Found single Athlete Record");
                        $row = $athleteresult->fetch_assoc();
                        $reason = recordChanged($line, $row, $clubs [$club]);

                        if ($reason !== '') {
                            importDebug('Updating:' . $reason);
                            importDebug('Original: ' . e4s_getOptionsAsString($row));
                            $row['reason'] = $reason;
                            $sql = 'update ' . E4S_COMMON_PREFIX . "Athlete
                            set firstname = '" . $firstname . "',
                                surname = '" . $surname . "',
                                gender = '" . $gender[0] . "',
                                dob = '" . $dob . "',
                                clubid = " . $clubs [$club] . ",
                                aocode = '" . $config['aocode'] . "',
                                activeEndDate = '" . $active . "',
                                where urn = " . $regid . "
                                and   aocode = '" . $config['aocode'] . "'";

                            $updatedCnt = $updatedCnt + 1;
                        } else {
                            $sql = '';
                        }
                    } else {
                        importDebug('Inserting:' . $athletesql);
                        importDebug('Original: ' . e4s_getOptionsAsString($row));
                        $sql = 'insert into ' . E4S_COMMON_PREFIX . "Athlete (aocode, urn, firstname, surname, gender,dob)
                        values ('" . $config['aocode'] . "'," . $regid . ",'" . $firstname . "','" . $surname . "','" . $gender[0] . "','" . $dob . "', " . $clubs [$club] . ')';

                        $insertCnt = $insertCnt + 1;
                    }

                    if ($sql !== '') {
                        logAthlete($status, $regid, $sql, $row, $config);
                        e4s_queryNoLog('Import' . E4S_SQL_DELIM . $sql);
                    }
                } else {
                    $Error .= 'Failed to get club [' . $club . '] - [' . $origClub . ']';
                }
            } else {
                $Error = 'No club ! ';
            }
        } else {
            if ($cnt % 1000 === 0) {
                $config['processcnt'] = $cnt;
                importDebug('Read Count : ' . $cnt);
            }
        }
        if ($Error !== '') {
            errorOutput('Error :' . $Error);
            $errorsOccurred = $Error;
        }
        $cnt = $cnt + 1;
    }
    $config['processcnt'] = $cnt;
    if ($errorsOccurred !== '') {
        debug_email('Errors have occurred in this import. Please check the logs. Last Error was :' . $errorsOccurred . '. ' . getConfigData($config));
    }
    importComplete($config, $file, FALSE);
    exit();
}

function createClubFlat($club, $externalId, &$clubs) {
//    return the clubs id form our system
    $areaid = 6;
//    var_dump($club);
//    var_dump($clubs);
    $sql = 'insert into ' . E4S_TABLE_CLUBS . " (externid, clubname, areaid,clubtype,country,active)
            values (
                $externalId,
                '" . addslashes($club) . "',
                $areaid,
                'C',
                'EIRE',
                1
            )";
    e4s_queryNoLog($sql);
    $newId = e4s_getLastID();
    $clubs[$club] = $newId;
    sendNewClubEmailFlat($newId . ':' . $club);
    return $newId;
}

function getClubs() {
// Get Clubs
    $sql = 'Select * from ' . E4S_COMMON_PREFIX . "Clubs
        where Country = 'Eire' or Country = 'Northern Ireland'";
    $result = e4s_queryNoLog($sql);

    $clubsArr = mysqli_fetch_all($result, MYSQLI_ASSOC);
    $clubs = array();
    foreach ($clubsArr as $value) {
        $club = trim($value['Clubname']);
        $clubs[$club] = $value['id'];
    }

    return $clubs;
}

function importComplete($config, $file, $rename) {
    echo "\n\nImport Done\n";
    $config['processing'] = '';
    updateImportConfig($config);
}

function getImportConfig() {
    $sql = 'Select ai.*, ao.code aocode 
            from ' . E4S_COMMON_PREFIX . 'AthleteImport ai,
                 ' . E4S_COMMON_PREFIX . 'ao ao
            where ai.aoid = ao.id
            and ai.id = ' . $GLOBALS['e4s_import_id'];
    $result = e4s_queryNoLog($sql);

    if ($result === FALSE or $result->num_rows < 1) {
        debug_email('Somehow got to exit ???');
        exit();
    }
    $row = $result->fetch_assoc();
    return $row;
}

function updateImportConfig($config) {
    $config['enddatetime'] = date('Y-m-d H:i:s', time());

    if ((int)$config['loadcnt'] === 0) {
        $config['startcnt'] = $config['processcnt'];
    }
//    // read it in case now not active
//    getImportConfig();

    $sql = 'update ' . E4S_COMMON_PREFIX . "AthleteImport 
            set lastfilename = '" . $config['lastfilename'] . "',
                startcnt = " . $config['startcnt'] . ',
                loadcnt = ' . $config['loadcnt'] . ',
                processcnt = ' . $config['processcnt'] . ",
                currentfilename = '" . $config['currentfilename'] . "',
                startdatetime = '" . $config['startdatetime'] . "',
                active = '" . $config['active'] . "',
                processing = '" . $config['processing'] . "',
                enddatetime = '" . $config['enddatetime'] . "' 
            where id = " . $GLOBALS['e4s_import_id'];

    e4s_queryNoLog($sql);
}

function recordChanged($line, $row, $clubid) {
    $firstName = trim($line[1], ' ');
    $surName = trim($line[2], ' ');

    $gender = $line[4][0];
    $dob = $line[5];
    $disability = $line[6];
    $active = trim($line[7]);

    $msg = $row['URN'] . ':';
    $msgSep = '';

    if ($row['activeEndDate'] !== $active) {
        $msg .= $msgSep . 'Active:' . $row['activeEndDate'] . ' - ' . $active;
        $msgSep = '. ';
    }
    if ($row['firstName'] !== $firstName) {
        $msg .= $msgSep . 'firstName:' . $row['firstName'] . ' - ' . $firstName;
        $msgSep = '. ';
    }
    if ($row['surName'] !== $surName) {
        $msg .= $msgSep . 'Surname:' . $row['surName'] . ' - ' . $surName;
        $msgSep = '. ';
    }
    if ($row['clubid'] !== $clubid) {
        $msg .= $msgSep . 'Club:' . $row['clubid'] . ' - ' . $clubid;
        $msgSep = '. ';
    }
    if ($row['gender'] !== $gender) {
        $msg .= $msgSep . 'Gender:' . $row['gender'] . ' - ' . $gender;
        $msgSep = '. ';
    }
    if ($row['dob'] !== $dob) {
        $msg .= $msgSep . 'DOB: ' . $row['dob'] . ' - ' . $dob;
        $msgSep = '. ';
    }

    if ($msgSep === '') {
        return '';
    }

    return $msg;
}

function errorOutput($msg) {
    importDebug($msg);
}

function echoOutput($msg, $log) {
    return;
    global $showOutput;
    $echo = FALSE;
    if (isset($_GET['echo'])) {
        if (!$log) {
            // if echo set but also going to log. dont echo as the log will do it
            $echo = TRUE;
        }
    }
    if ($showOutput === 1 and !$echo) {
        if (gettype($msg) === 'array') {
            e4s_dump($msg);
            return;
        }
        echo $msg . '<br>';
    }
    if ($log) {
        logSql($msg, 1);
    }
}

function convertDate($date) {
    $date = str_replace('/', '-', $date);
    return date('Y-m-d', strtotime($date));
}

function logAthlete($status, $regid, $sql, $row, $config) {
    $reason = 'New Athlete';
    if (!is_null($row)) {
        if (array_key_exists('reason', $row)) {
            $reason = addslashes($row['reason']);
            if (is_null($reason)) {
                $reason = '-';
            }
        }
    }
    $conn = $GLOBALS['logconn'];
    $insertsql = 'INSERT INTO ' . E4S_LOG_PREFIX . 'AthleteLog ( created, urn, status, sql_cmd, reason, filename) 
                    VALUES (' . returnNow() . ',' . $regid . ",'" . $status . "', '" . addslashes($sql) . "','" . $reason . "','" . $config['currentfilename'] . "')";

    mysqli_query($conn, $insertsql);
}

function debug_email($body) {
    $to = $GLOBALS['e4s_import_email'];
//    $to = "paul.day@apiconsultancy.com";
    $subject = 'Athlete Import msg :' . $body;
    $headers = array('Content-Type: text/html; charset=UTF-8');

    wp_mail($to, $subject, $body, $headers);
}

function getConfigData($config) {
    return ('[' . $config['currentfilename'] . ':' . $config['startcnt'] . ':' . $config['processcnt'] . ':' . $config['active'] . ':' . $config['startdatetime'] . ':' . $config['enddatetime'] . ']');
}

function canImportRun($row) {
    if (!is_null($row)) {

        if ((int)$row['active'] === E4S_IMPORT_ENABLED and $row['processing'] === E4S_IMPORT_CODE) {
            return TRUE;
        }
        if ((int)$row['active'] === E4S_IMPORT_ENABLED and $row['processing'] === '') {
            return TRUE;
        }
        // Check if stalled

        if ((int)$row['active'] === E4S_IMPORT_ENABLED) {
            //        60 seconds x 10 minutes
            $timeout = time() - (60 * 10);
            $timeoutStr = date('Y-m-d H:i:s', $timeout);

            if ($row['enddatetime'] < $timeoutStr) {
                debug_email('Timed out. resetting : ' . $timeoutStr . ':' . $row['enddatetime']);
                // System timed out
                return TRUE;
            }
        }
    }
    return FALSE;
}

function getProcessCode($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function sendNewClubEmailFlat($reference) {
    $bccUs = FALSE;

    $email = E4S_SUPPORT_EMAIL;
    $fromEmail = $email;
    $body = E4S_CURRENT_DOMAIN . ' New Club generated.' . '<br><br>Generated from the Live import, club ' . $reference . ' has been created. This will need updating with the correct area.';

    $body .= Entry4_emailFooter($email);

    $response = wp_mail($email, 'New Club. ' . E4S_CURRENT_DOMAIN . '/' . $reference, $body, Entry4_mailHeader($fromEmail, $bccUs));
    if (!$response) {
        Entry4UIError(9310, 'Failed to send email of new club : ' . $reference, 200, '');
    }
}