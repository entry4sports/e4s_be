<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'admin/userMaint.php';
include_once E4S_FULL_PATH . 'builder/clubCRUD.php';
/*
 * This file is used to load athletes from 2022 into the system for 2023 and associate them to users
 * It also has the ability to add users alongside the default esaa-COUNTY users
 * Has to currently run twice to load Athletes for new users ????????
 */
const E4S_TABLE_ENGLISHSCHOOLS = 'es2022';
function testEmail(string $user, string $email, string $password = "Password") {
	$newline = '<br>';
	$body = 'Dear ' . $user . ',' . $newline . $newline;
	$body .= '<b>English Schools Entry System</b>' . $newline;
	$body .= '!!!! N.B. This is an email that has previously been sent, however some users reported to have not received it. If you have already received this email, please ignore it.' . $newline;
	$body .= 'ESAA have for the third year running chosen Entry4Sports to manage their entries for the 2024 Track & Field Championships. We have tried hard to ensure that the system is both comprehensive and easy to use for those involved with the actual entries. I have included a User guide, which I hope does cover all the main areas of its use, however, if you do have any difficulty or have questions regarding this system please contact me, Andy Hulse, (andrewhulse2017@gmail.com) rather than Entry4Sports themselves.';
	$body .= $newline;
	$body .= $newline;
	$body .= 'Each County has one or more people who will be able to access the system with the username and password provided by Entry4Sports. We did ask for details of those involved for each County but we can add additional people if necessary. We would ask that you do not share usernames and passwords for obvious reasons.';
	$body .= $newline;
	$body .= $newline;
	$body .= 'As part of the set-up we have pre-populated each County with all their athletes who competed last year and who are still eligible for any of the 3 age groups.';
	$body .= $newline;
	$body .= $newline;
	$body .= 'The deadline for entries this year is 10:00pm on Sunday 21st June, which should allow any changes/additions as a result of the inter-county matches. This deadline is very tight and we need the entries finalised to send details to the printers and to be able to produce the event timetable and associated Call Room Schedules so they can be made available to all as soon as possible.';
	$body .= $newline;
	$body .= $newline;
	$body .= $newline;
	$body .= '<b>ESAA Track & Field Championships Mixed Relays</b>' . $newline;
	$body .= 'Mixed relays for all age groups are being offered to all B,C & D Counties again this year, you may wish to try and include this event in your inter-County event , or persuade someone else to stage them at some stage to give the chance to run one in advance of the championship itself. The teams must be 2 boys and 2 girls with the girls running first (ESAA Handbook 2023 p.40 Events (g) )';
	$body .= $newline;
	$body .= $newline;
	$body .= '<b>Your personnal logon details:</b>';
	$body .= $newline;
	$body .= 'Username :<b> ' . $user . '</b>';
	$body .= $newline;
	$body .= 'Password : If you are an existing user, you can use your existing password. If you have forgotten this, use the reset link on the login page.' . $newline;
	$body .= 'New users will be emailed a password in a separate email for security purposes.<br>';
	$body .= $newline;
	$body .= $newline;
	$body .= 'Help document(s) can be found by clicking the link below.';
	$body .= $newline;
	$body .= "<a href='https://www.dropbox.com/scl/fi/kirzau75dioopfo8zbw62/User-Guide-for-Counties.pdf?rlkey=iruzy5he4wbd8xamxbs9njzgx&st=48zzepwm&dl=0'>County Online Entry Help</a>" . $newline;

	$headers = Entry4_mailHeader('ESAA24');
	$headers[] = 'Bcc:paul@tech4sports.co.uk';

	e4s_mail($email, 'ESAA 2024 County Registration/Entry', $body, $headers);

}
function e4s_importESAA24($compId){
	testEmail('testUser','andrewhulse2017@gmail.com');
	exit;
    $esaaObj = new importESAA($compId);
    $esaaObj->load23Info();
    $users = [];
//    $users[] = ["county"=>"Avon","user"=>"","email"=>"jim.strudwick100@gmail.com"];
//    $users[] = ["county"=>"Avon","user"=>"","email"=>"kenholmes200@yahoo.com"];
//    $users[] = ["county"=>"Avon","user"=>"","email"=>"karis.overton@CSET.co.uk"];
//    $users[] = ["county"=>"Bedfordshire","user"=>"","email"=>"dennisjohnson@ntlworld.com"];
//    $users[] = ["county"=>"Bedfordshire","user"=>"","email"=>"gfordham@kempston academy.co.uk"];
//    $users[] = ["county"=>"Berkshire","user"=>"","email"=>"Debbie.brennan5@btinternet.com"];
//    $users[] = ["county"=>"Buckinghamshire","user"=>"","email"=>"carolinebird68@msn.com"];
//    $users[] = ["county"=>"Cambridgeshire","user"=>"","email"=>"siobhanskinner89@hotmail.com"];
//    $users[] = ["county"=>"Channel Islands","user"=>"","email"=>"Sadie.Addlesee@stsampsonshigh.sch.gg"];
//    $users[] = ["county"=>"Cheshire","user"=>"","email"=>"944snodgrass@gmail.com"];
//    $users[] = ["county"=>"Cleveland","user"=>"","email"=>"dbunn@nunthorpe.co.uk"];
//    $users[] = ["county"=>"Cornwall","user"=>"","email"=>"mo.pearson1@mypostoffice.co.uk"];
//    $users[] = ["county"=>"Cornwall","user"=>"","email"=>"beetleboy7273@gmail.com"];
//    $users[] = ["county"=>"Cumbria","user"=>"","email"=>"jpcsaa@gmail.com"];
//    $users[] = ["county"=>"Derbyshire","user"=>"","email"=>"rannyanny80@gmail.com"];
//    $users[] = ["county"=>"Devon","user"=>"","email"=>"terryobrien9896@btinternet.com"];
//    $users[] = ["county"=>"Dorset","user"=>"","email"=>"gavin.rusling@parkstone.poole.sch.uk"];
//    $users[] = ["county"=>"Durham","user"=>"","email"=>"gemsy_brown@yahoo.co.uk"];
//    $users[] = ["county"=>"Essex","user"=>"","email"=>"rc@shsb.org.uk"];
//    $users[] = ["county"=>"Essex","user"=>"","email"=>"richard.casey073@gmail.com"];
//    $users[] = ["county"=>"Gloucestershire","user"=>"","email"=>"suepadfield@btinternet.com"];
//    $users[] = ["county"=>"Gloucestershire","user"=>"","email"=>"suepadfield@btinternet.com"];
//    $users[] = ["county"=>"Greater Manchester","user"=>"","email"=>"sueexon@hotmail.co.uk"];
//    $users[] = ["county"=>"Hampshire","user"=>"","email"=>"Kevin Maguire <kmaguire@churcherscollege.com>"];
//    $users[] = ["county"=>"Hereford and Worcestershire","user"=>"","email"=>"mikebennett78@googlemail.com","exist"=>true];
//    $users[] = ["county"=>"Hereford and Worcestershire","user"=>"","email"=>"ldhammond1979@outlook.com","exist"=>true];
//    $users[] = ["county"=>"Hereford and Worcestershire","user"=>"","email"=>"atriptree_ttd@msn.com","exist"=>true];
//    $users[] = ["county"=>"Hertfordshire","user"=>"","email"=>"lowefamily04@icloud.com"];
//    $users[] = ["county"=>"Hertfordshire","user"=>"","email"=>"chrisbajak@saracens.net"];
//    $users[] = ["county"=>"Humberside","user"=>"","email"=>"cyeoda@gmail.com"];
//    $users[] = ["county"=>"Isle of Man","user"=>"","email"=>"charlotte.christian@sch.im"];
//    $users[] = ["county"=>"Kent","user"=>"","email"=>"GJones@dartfordgrammarschool.org.uk"];
//    $users[] = ["county"=>"Kent","user"=>"","email"=>"ken.burkett@yahoo.co.uk"];
//    $users[] = ["county"=>"Lancashire","user"=>"","email"=>"stephenwmort@gmail.com"];
//    $users[] = ["county"=>"Leicestershire & Rutland","user"=>"","email"=>"shatherley.crosscountry@outlook.com"];
//    $users[] = ["county"=>"Leicestershire & Rutland","user"=>"","email"=>"mlynch@bosworthacademy.org.uk"];
//    $users[] = ["county"=>"Lincolnshire","user"=>"","email"=>"badger53@btinternet.com"];
//    $users[] = ["county"=>"London","user"=>"","email"=>"marilyn.athletics@gmail.com"];
//    $users[] = ["county"=>"London","user"=>"","email"=>"a_soalla_bell@yahoo.co.uk"];
//    $users[] = ["county"=>"Merseyside","user"=>"","email"=>"audragoodall@gmail.com"];
//    $users[] = ["county"=>"Middlesex","user"=>"","email"=>"c.henderson@nhehs.gdst.net"];
//    $users[] = ["county"=>"Middlesex","user"=>"","email"=>"paul.bolton@highgateschool.org.uk "];
//    $users[] = ["county"=>"Norfolk","user"=>"","email"=>"sbowman5nry@yare-edu.or.uk"];
//    $users[] = ["county"=>"North Yorkshire","user"=>"","email"=>"davepaver@hotmail.com"];
//    $users[] = ["county"=>"Northamptonshire","user"=>"","email"=>"louiseogden50@hotmail.com"];
//    $users[] = ["county"=>"Northamptonshire","user"=>"","email"=>"lsharp@nsg.northants.sch.uk"];
//    $users[] = ["county"=>"Northumberland","user"=>"","email"=>"Kieran_flannery@hotmail.co.uk"];
//    $users[] = ["county"=>"Nottinghamshire","user"=>"","email"=>"anitaspray0528@gmail.com"];
//    $users[] = ["county"=>"Oxfordshire","user"=>"","email"=>"celiaoh@icloud.com"];
//    $users[] = ["county"=>"Shropshire","user"=>"","email"=>"IRawlings@ttsonline.net"];
//    $users[] = ["county"=>"Somerset","user"=>"","email"=>"brianbakergfw@gmail.com"];
//    $users[] = ["county"=>"Somerset","user"=>"","email"=>"brian@somersetschoolsathletics.org.uk"];
//    $users[] = ["county"=>"South Yorkshire","user"=>"","email"=>"rupert400mh@hotmail.com"];
//    $users[] = ["county"=>"Staffordshire","user"=>"Staff_2350","email"=>"debcash@icloud.com"];
//    $users[] = ["county"=>"Suffolk","user"=>"","email"=>"bwilson@framlinghamcollege.co.uk "];
//    $users[] = ["county"=>"Suffolk","user"=>"","email"=>"hayleybestley@hotmail.co.uk"];
//    $users[] = ["county"=>"Surrey","user"=>"","email"=>"SSAAhonsecretary@gmail.com"];
//    $users[] = ["county"=>"Sussex","user"=>"","email"=>"leannebuxton@hotmail.com"];
//    $users[] = ["county"=>"Warwickshire","user"=>"","email"=>"warks2024@mrday.co.uk"];
//    $users[] = ["county"=>"West Midlands","user"=>"","email"=>"dalewis86@icloud.com"];
//    $users[] = ["county"=>"West Yorkshire","user"=>"","email"=>"Smann32@outlook.com"];
//    $users[] = ["county"=>"Wiltshire","user"=>"","email"=>"HunterC@lydiardparkacademy.org.uk"];
//
//	$users[] = ['county' => 'North Yorkshire', 'user' => '', 'email' => 'milnera@ripongrammar.com'];
//	$users[] = ['county' => 'Oxfordshire', 'user' => '', 'email' => 'MXClementsFoster@bartholomew.epatrust.org'];
//	$users[] = ['county' => 'Channel Islands', 'user' => '', 'email' => 'mandycampbelld@hotmail.com'];

//	$users[] = ["county"=>"Berkshire","user"=>"","email"=>"Debbie.Brennan@furzeplatt.net"];
	$users[] = ["county"=>"Essex","user"=>"","email"=>"richard.casey073@gmail.com"];
	$users[] = ["county"=>"Greater Manchester","user"=>"","email"=>"sueexon@hotmail.co.uk"];
	$users[] = ["county"=>"Hampshire","user"=>"","email"=>"jack.messenger@yateley.hants.sch.uk"];
	$users[] = ["county"=>"Hertfordshire","user"=>"","email"=>"lowefamily04@icloud.com"];
	$users[] = ["county"=>"Kent","user"=>"","email"=>"josephohara96@gmail.com"];
	$users[] = ["county"=>"London","user"=>"","email"=>"a_soalla_bell@yahoo.com"];
	$users[] = ["county"=>"Middlesex","user"=>"","email"=>"paul.bolton@highgateschool.org.uk"];
	$users[] = ["county"=>"Surrey","user"=>"","email"=>"ssaahonsecretary@gmail.com"];
	$users[] = ["county"=>"Sussex","user"=>"","email"=>"leannebuxton@hotmail.com"];
	$users[] = ["county"=>"Avon","user"=>"","email"=>"jim.strudwick100@gmail.com"];
	$users[] = ["county"=>"Buckinghamshire","user"=>"","email"=>"carolinebird68@msn.com"];
	$users[] = ["county"=>"Cheshire","user"=>"","email"=>"944snodgrass@gmail.com"];
	$users[] = ["county"=>"Derbyshire","user"=>"","email"=>"rannyanny80@gmail.com"];
	$users[] = ["county"=>"Devon","user"=>"","email"=>"eileenwrmander@gmail.com"];
	$users[] = ["county"=>"Devon","user"=>"","email"=>"terryobrien9896@btinternet.com"];
	$users[] = ["county"=>"Lancs","user"=>"","email"=>"stephenwmort@gmail.com"];
	$users[] = ["county"=>"Leicestershire","user"=>"","email"=>"shatherley@stathern.leics.sch.uk"];
	$users[] = ["county"=>"Merseyside","user"=>"","email"=>"audragoodall@gmail.com"];
	$users[] = ["county"=>"North Yorkshire","user"=>"","email"=>"milnera@ripongrammar.com"];
	$users[] = ["county"=>"Nottinghamshire","user"=>"","email"=>"anitaspray0528@gmail.com"];
	$users[] = ["county"=>"Somerset","user"=>"","email"=>"richard@rbowden.eclipse.co.uk"];
	$users[] = ["county"=>"South Yorkshire","user"=>"","email"=>"helena.brown70@gmail.com"];
	$users[] = ["county"=>"West Midlands","user"=>"","email"=>"dalewis86@icloud.com"];
	$users[] = ["county"=>"West Yorkshire","user"=>"","email"=>"tonykingham@blueyonder.co.uk"];
	$users[] = ["county"=>"Bedfordshire","user"=>"","email"=>"dennisjohnson@ntlworld.com"];
	$users[] = ["county"=>"Cambridgeshire","user"=>"","email"=>"siobhan.bright@coleridgecc.org.uk"];
	$users[] = ["county"=>"Dorset","user"=>"","email"=>"gavinrusling@hotmail.com"];
	$users[] = ["county"=>"Durham","user"=>"","email"=>"gbrown@st-bedes.org"];
	$users[] = ["county"=>"Gloucestershire","user"=>"","email"=>"ABeadle@pittville.gloucs.sch.uk"];
	$users[] = ["county"=>"Humberside","user"=>"","email"=>"schofield12@schofield12.karoo.co.uk"];
	$users[] = ["county"=>"Lincolnshire","user"=>"","email"=>"badger53@btinternet.com"];
	$users[] = ["county"=>"Northantonshire","user"=>"","email"=>"lsharp@nsg.northants.sch.uk"];
	$users[] = ["county"=>"Shropshire","user"=>"","email"=>"irawlings@ttsonline.net"];
	$users[] = ["county"=>"Staffordshire","user"=>"","email"=>"debcash@me.com"];
	$users[] = ["county"=>"Suffolk","user"=>"","email"=>"BWilson@framlinghamcollege.co.uk"];
	$users[] = ["county"=>"Warwickshire","user"=>"","email"=>"gwilprice@btinternet.com"];
	$users[] = ["county"=>"Wiltshire","user"=>"","email"=>"daf@clarendonacademy.com"];
	$users[] = ["county"=>"Cleveland","user"=>"","email"=>"dazbunn@hotmail.com"];
	$users[] = ["county"=>"Cornwall","user"=>"","email"=>"mo.pearson1@mypostoffice.co.uk"];
	$users[] = ["county"=>"Cumbria","user"=>"","email"=>"jpcsaa@gmail.com"];
	$users[] = ["county"=>"Hereford and Worcestershire","user"=>"","email"=>"atriptree_ttd@msn.com"];
	$users[] = ["county"=>"Hereford and Worcestershire","user"=>"","email"=>"lhammond@shs.worcs.sch.uk"];
	$users[] = ["county"=>"Norfolk","user"=>"","email"=>"sbowman5nry@yare-edu.org.uk"];
	$users[] = ["county"=>"Northumberland","user"=>"","email"=>"kevinjflannery@outlook.com"];
	$users[] = ["county"=>"Oxfordshire","user"=>"","email"=>"MXClementsFoster@bartholomew.epatrust.org"];
	$users[] = ["county"=>"Channel Islands","user"=>"","email"=>"a.campbell@hautlieu.sch.je"];
	$users[] = ["county"=>"Isle of Man","user"=>"","email"=>"charlotte.christian@sch.im"];
	$users[] = ["county"=>"UAE","user"=>"","email"=>"Umar@aisathletics.ae"];

    $cnt = 80;
    foreach($users as $user){
        $userName = $user['user'];
        if ( $userName === '' ){
            $userName = substr($user['county'],0,5) . '_23' . $cnt++;
            $userName = str_replace(' ','', $userName);
        }
        $pwd = $esaaObj->addUserForClubName($user['county'],$userName,$user['email']);
        echo $userName . ' > ' . $user['email'] . ' [' . $pwd . "]\n";
        if ($pwd !== '' or array_key_exists('exists', $user)){
//            User has been created so email them
	        echo "Emailing User : " . $user['email'] . "\n";
			$esaaObj->emailUser($userName,$user['email'], $pwd);
        }
    }
}

class importESAA {
    public $rows;
    public $compId;
    public $compObj;
    public $youngestDob;
    public $oldestDob;
    public $eaObj;
    public $clubOwners;
    public $clubComps;

    public function __construct($compId) {
        $this->compId = $compId;
        $this->compObj = e4s_getCompObj($compId);
        $this->compObj->setMinMaxAgeGroups();
        $this->youngestDob = $this->compObj->youngestDob;
        $this->oldestDob = $this->compObj->oldestDob;
        $this->eaObj = new eaRegistrationClass(E4S_EA_LIVE);
        $this->clubComps = [];
        $this->_getClubComps();
        $this->clubOwners = [];
        $this->_getClubOwners();
        if ($this->_checkStdUsers()) {
            $this->_getClubOwners();
        }
    }

    public function emailUser(string $user, string $email, string $password) {
		$sendPwdEmail = $password !== "";
        $newline = '<br>';
        $body = 'Dear ' . $user . ',' . $newline . $newline;
        $body .= '<b>English Schools Entry System</b>' . $newline;
		$body .= '!!!! N.B. This is an email that has previously been sent, however some users reported to have not received it. If you have already received this email, please ignore it.' . $newline;
        $body .= 'ESAA have for the third year running chosen Entry4Sports to manage their entries for the 2024 Track & Field Championships. We have tried hard to ensure that the system is both comprehensive and easy to use for those involved with the actual entries. I have included a User guide, which I hope does cover all the main areas of its use, however, if you do have any difficulty or have questions regarding this system please contact me, Andy Hulse, (andrewhulse2017@gmail.com) rather than Entry4Sports themselves.';
        $body .= $newline;
        $body .= $newline;
        $body .= 'Each County has one or more people who will be able to access the system with the username and password provided by Entry4Sports. We did ask for details of those involved for each County but we can add additional people if necessary. We would ask that you do not share usernames and passwords for obvious reasons.';
        $body .= $newline;
        $body .= $newline;
        $body .= 'As part of the set-up we have pre-populated each County with all their athletes who competed last year and who are still eligible for any of the 3 age groups.';
        $body .= $newline;
        $body .= $newline;
        $body .= 'The deadline for entries this year is 10:00pm on Sunday 21st June, which should allow any changes/additions as a result of the inter-county matches. This deadline is very tight and we need the entries finalised to send details to the printers and to be able to produce the event timetable and associated Call Room Schedules so they can be made available to all as soon as possible.';
        $body .= $newline;
        $body .= $newline;
        $body .= $newline;
        $body .= '<b>ESAA Track & Field Championships Mixed Relays</b>' . $newline;
        $body .= 'Mixed relays for all age groups are being offered to all B,C & D Counties again this year, you may wish to try and include this event in your inter-County event , or persuade someone else to stage them at some stage to give the chance to run one in advance of the championship itself. The teams must be 2 boys and 2 girls with the girls running first (ESAA Handbook 2023 p.40 Events (g) )';
        $body .= $newline;
        $body .= $newline;
        $body .= '<b>Your personnal logon details:</b>';
        $body .= $newline;
        $body .= 'Username :<b> ' . $user . '</b>';
        $body .= $newline;
		$body .= 'Password : If you are an existing user, you can use your existing password. If you have forgotten this, use the reset link on the login page.' . $newline;
		$body .= 'New users will be emailed a password in a separate email for security purposes.<br>';
        $body .= $newline;
        $body .= $newline;
        $body .= 'Help document(s) can be found by clicking the link below.';
        $body .= $newline;
        $body .= "<a href='https://www.dropbox.com/scl/fi/kirzau75dioopfo8zbw62/User-Guide-for-Counties.pdf?rlkey=iruzy5he4wbd8xamxbs9njzgx&st=48zzepwm&dl=0'>County Online Entry Help</a>" . $newline;

		$headers = Entry4_mailHeader('ESAA24');
	    $headers[] = 'Bcc:andrewhulse2017@gmail.com';

        e4s_mail($email, 'ESAA 2024 County Registration/Entry', $body, $headers);
		if ( $sendPwdEmail ){
			$body = 'Dear ' . $user . ',' . $newline . $newline;
			$body .= 'Your password for the ESAA 2024 County Registration/Entry system is ' . $password . $newline;
			$body .= 'Please keep this safe and do not share it with anyone.' . $newline;
			$body .= 'If you have any problems logging in, please contact Andy Hulse, (andrewhulse2017@gmail.com).' . $newline;
			e4s_mail($email, 'ESAA 2024 County Registration/Entry', $body, $headers);
		}
    }

    public function load22Info() {
        $this->rows = $this->_get22AthleteInfo();
        $this->_processAthletes();
    }
	public function load23Info() {

//		$this->_processAthletes();
	}
    private function _processAthletes() {
        // loop through Athletes
        $useAthlete = null;
        // If has URN, check we dont have it already
        foreach ($this->rows as $athleteRow) {
            $useAthlete = null;
            if (!$this->_acceptDob($athleteRow)) {
                // do not load athlete outside age range required
                continue;
            }
            if (!is_null($athleteRow->URN) and $athleteRow->URN !== '') {
                $useAthlete = $this->_checkForAthlete('urn', $athleteRow->URN);
            }
            if (is_null($useAthlete)) {
                // attempt to get URN
                $athleteRow->URN = $this->eaObj->getURN($athleteRow->firstName, $athleteRow->surName, $athleteRow->dob);
                if (!is_null($athleteRow->URN) and $athleteRow->URN !== '') {
                    $useAthlete = $this->_checkForAthlete('urn', $athleteRow->URN);
                } else {
                    $useAthlete = $this->_checkForAthlete('', '', $athleteRow);
                }
                if (is_null($useAthlete)) {
                    $useAthlete = $this->_createAthlete($athleteRow);
                }
            }

            if (!is_null($useAthlete)) {
                // check and link to users.
                $this->_createUserAthletes($athleteRow->clubId, $useAthlete);
            }
        }
    }

    private function _createUserAthletes($clubId, $athlete) {
        // does it already exist
        if (!array_key_exists($clubId, $this->clubComps)) {
            Entry4UIError(8022, 'Unable to get Club info : ' . $clubId);
        }
        $ccId = $this->clubComps[$clubId]->id;
        $owners = [];
        if (!array_key_exists($ccId, $this->clubOwners)) {
            Entry4UIError(8022, 'Unable to get Club Owner info : ' . $ccId);
        }
        foreach ($this->clubOwners[$ccId] as $clubOwner) {
            $owners[$clubOwner->user_id] = $clubOwner->user_id;
        }

        // get any existing
        $sql = 'select *
                from ' . E4S_TABLE_USERATHLETES . '
                where athleteid = ' . $athlete->id . '
                and userId in ( ' . implode(',', $owners) . ')';

        $result = e4s_queryNoLog($sql);
        while ($obj = $result->fetch_object(E4S_USERATHLETES_OBJ)) {
            unset($owners[$obj->userId]);
        }
        // ensure E4S not there
        unset($owners[e4s_getUserID()]);
        if (sizeof($owners) > 0) {
            $sql = 'insert into ' . E4S_TABLE_USERATHLETES . ' (athleteid, userid)
                    values ';
            $sep = '';
            foreach ($owners as $owner) {
                $sql .= $sep . '(' . $athlete->id . ',' . $owner . ')';
                $sep = ',';
            }
            e4s_queryNoLog($sql);
        }
    }

    private function _getClubOwners() {
        $clubCompKey = E4S_CLUBCOMP . $this->compId;
        $sql = 'select *
                from ' . E4S_TABLE_USERMETA . "
                where meta_key = '" . $clubCompKey . "'";
        $result = e4s_queryNoLog($sql);
        $clubComps = array();
        while ($obj = $result->fetch_object()) {
            $obj->umeta_id = (int)$obj->umeta_id;
            $obj->user_id = (int)$obj->user_id;
            $obj->meta_value = (int)$obj->meta_value;
            if (!array_key_exists($obj->meta_value, $clubComps)) {
                $clubComps[$obj->meta_value] = array();
            }
            $clubComps[$obj->meta_value][$obj->user_id] = $obj;
        }
        $this->clubOwners = $clubComps;
    }

    private function _getClubComps() {
        $sql = 'select cc.*, c.clubName
                from ' . E4S_TABLE_CLUBCOMP . ' cc,
                     ' . E4S_TABLE_CLUBS . ' c
                where cc.compid = ' . $this->compId . '
                and cc.clubid = c.id';
        $result = e4s_queryNoLog($sql);
        $clubComps = [];
        while ($obj = $result->fetch_object(E4S_CLUBCOMP_OBJ)) {
            $clubComps[$obj->clubId] = $obj;
        }
        $this->clubComps = $clubComps;
    }

    private function _acceptDob($athleteRow): bool {
        if ($athleteRow->dob < $this->oldestDob or $athleteRow->dob > $this->youngestDob) {
            return FALSE;
        }
        return TRUE;
    }

    private function _createAthlete($athleteRow): ?e4sAthlete {
        $athleteRow->aoCode = '';
        $urn = 'null';
        if (!is_null($athleteRow->URN) and $athleteRow->URN !== '') {
            $athleteRow->aoCode = E4S_AOCODE_EA;
            $urn = $athleteRow->URN;
        }
        $gender = E4S_GENDER_MALE;
        if (strpos(strtolower($athleteRow->ageGroup), 'girls') !== FALSE) {
            $gender = E4S_GENDER_FEMALE;
        }

        $sql = 'insert into ' . E4S_TABLE_ATHLETE . " (firstname, surname, aocode, urn, dob, gender, clubid, type, activeEndDate)
                values(
                '" . addslashes($athleteRow->firstName) . "',
                '" . addslashes($athleteRow->surName) . "',
                '" . $athleteRow->aoCode . "',
                " . $urn . ",
                '" . $athleteRow->dob . "',
                '" . $gender . "',
                " . $athleteRow->clubId . ",
                'A',
                '2023-12-31'
                )";
        e4s_queryNoLog($sql);
        return $this->_checkForAthlete('id', e4s_getLastID());
    }

    private function _checkForAthlete(string $field, string $key, $athlete = null): ?e4sAthlete {
        $sql = 'select *
                from ' . E4S_TABLE_ATHLETE;
        if ($field === '') {
            $sql .= " where surname = '" . addslashes($athlete->surName) . "'
                      and   firstname = '" . addslashes($athlete->firstName) . "'
                      and   dob = '" . $athlete->dob . "'
                      and   clubid = " . $athlete->clubId;
        } else {
            $sql .= ' where ' . $field . " = '" . $key . "'";
            if (strtolower($field) === 'urn') {
                $sql .= " and aocode='" . E4S_AOCODE_EA . "'";
            }
        }
        $sql .= ' order by URN desc';
        $result = e4s_queryNoLog($sql);

        $athlete = $result->fetch_object(E4S_ATHLETE_OBJ);
        return $athlete;
    }

    private function _get22AthleteInfo() {
        $sql = 'select e.id, c.id clubId, e.county, e.ageGroup, e.firstName, e.surName, e.dob, e.URN, e.school schoolName 
                from ' . E4S_TABLE_ENGLISHSCHOOLS . ' e,
                     ' . E4S_TABLE_CLUBS . ' c
                where e.county = c.clubname';
        $result = e4s_queryNoLog($sql);
        $info = array();
        while ($obj = $result->fetch_object()) {
            $obj->id = (int)$obj->id;
            $obj->clubId = (int)$obj->clubId;
            $obj->firstName = trim($obj->firstName);
            $obj->surName = trim($obj->surName);
            $info[$obj->id] = $obj;
        }

        return $info;
    }

    public function addUserForClubId(int $clubId, string $userName, string $email): string {
        $password = 'esaa' . random_int(1000, 9999);
        if ($this->_checkAndCreateUser($clubId, $userName, $password, $email)) {
            return $password;
        }
        return '';
    }

    public function addUserForClubName(string $clubName, string $userName, string $email): string {
        foreach ($this->clubComps as $clubComp) {
            if (strtolower($clubComp->clubName) === strtolower($clubName)) {
                return $this->addUserForClubId($clubComp->clubId, $userName, $email);
            }
        }
        return '';
    }

    private function _checkStdUsers(): bool {
        $newUsers = FALSE;
        foreach ($this->clubComps as $clubId => $clubComp) {
            $useClub = str_replace(' ', '', $clubComp->clubName);
            if ($this->_checkAndCreateUser($clubId, 'esaa-' . $useClub, 'esaa')) {
                $newUsers = TRUE;
            }
        }
        return $newUsers;
    }

    private function _checkAndCreateUser($clubId, $user, $password, $email = ''): bool {
        $useUser = str_replace('&', '', $user);
        if ($email === '') {
            $email = $useUser . '@esaa.co.uk';
        }

        $sql = 'select id
                from ' . E4S_TABLE_USERS . "
                where user_login = '" . $useUser . "'
                or user_email = '" . $email . "'";
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            $obj = $result->fetch_object();
            $userId = (int)$obj->id;
        } else {
            $userId = wp_create_user($useUser, $password, $email);
            if (gettype($userId) === 'object') {
                $wpUserObj = get_user_by('email', $email);
                echo $user . ': ' . $email . "\n";
                $userId = $wpUserObj->ID;
                wp_set_password($password, $userId);
            }
        }
        echo 'Checking -> ' . $email . ' - ' . $userId . "\n";

        return $this->_addClubComp($userId, $clubId);
    }
	private function _copyUserAthletes($fromUserId, $toUserId){
		$sql = "select ua1.athleteid
        from " . E4S_TABLE_USERATHLETES . " ua1
		where ua1.userid = " . $fromUserId . "
		and ua1.athleteid not in (select ua2.athleteid from " . E4S_TABLE_USERATHLETES . " ua2 where ua2.userid = " . $toUserId . ")";
		$result = e4s_queryNoLog($sql);
		if ( $result->num_rows === 0 ){
			return;
		}
		$athletes = [];
		while ($obj = $result->fetch_object()) {
			$athletes[] = $obj->athleteid;
		}
		if (sizeof($athletes) > 0) {
			$sql = 'insert into ' . E4S_TABLE_USERATHLETES . ' (athleteid, userid)
					values ';
			$sep = '';
			foreach ($athletes as $athlete) {
				$sql .= $sep . '(' . $athlete . ',' . $toUserId . ')';
				$sep = ',';
			}
			e4s_queryNoLog($sql);
		}
	}
    private function _addClubComp($userId, $clubId): bool {
        $createMeta = TRUE;
        $clubCompKey = E4S_CLUBCOMP . $this->compId;
        if (array_key_exists($clubId, $this->clubComps)) {
            $ccId = $this->clubComps[$clubId]->id;
            $clubOwners = [];
            if (array_key_exists($ccId, $this->clubOwners)) {
                $clubOwners = $this->clubOwners[$ccId];
            }
            foreach ($clubOwners as $clubOwner) {
                if ($clubOwner->user_id === $userId) {
                    $createMeta = FALSE;
                }else{
					// copy userAthletes
	                $this->_copyUserAthletes($clubOwner->user_id, $userId);
                }
            }
        }

        if ($createMeta) {
            $sql = 'insert into ' . E4S_TABLE_USERMETA . '(user_id, meta_key, meta_value)
                    values (
                        ' . $userId . ",
                        '" . $clubCompKey . "',
                        " . $this->clubComps[$clubId]->id . '
                       )';
            e4s_queryNoLog($sql);
        }
        return $createMeta;
    }
}