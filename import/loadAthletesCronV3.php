<?php
/**
 * Created by PhpStorm.
 * User: pauld
 * Date: 18/12/2018
 * Time: 20:44
 */
///////////////
/// Set in functions for the cron. If you want to run manually, set this
$GLOBALS['e4s_import_id'] = 1; // 1 (AAI) or 2 (NI);
$GLOBALS[E4S_CRON_USER] = TRUE;
include_once E4S_FULL_PATH . 'dbInfo.php';

define('E4S_IMPORT_DISABLED', 0);
define('E4S_IMPORT_ENABLED', 1);
define('E4S_IMPORT_CODE', getProcessCode(10));
define('E4S_IMPORT_EMAIL', 'paul@entry4sports.com');
define('E4S_OUTPUT_FILE', 'diff_');
define('E4S_OUTPUT_FILE_PREFIX', '.txt');
$GLOBALS['e4s_import_email'] = E4S_IMPORT_EMAIL;

$config = getImportConfig();

$config['processing'] = E4S_IMPORT_CODE;
$GLOBALS['e4s_import_email'] = $config['email'];
if ($GLOBALS['e4s_import_email'] === '') {
    $GLOBALS['e4s_import_email'] = E4S_IMPORT_EMAIL;
}

$importFromDir = $config['importfromdir'];
$importedDir = $config['importeddir'];
$filePrefix = $config['fileprefix'];
$currentfilename = $config['currentfilename'];

if ($currentfilename === '') {
    // No file being processed
    $currentfilename = getLatestFile($config);
    if ($currentfilename === '') {
        exit();
    } else {
        // mark as in progress
        $config['currentfilename'] = $currentfilename;
        $config['enddatetime'] = '';
        $config['startcnt'] = 0;
        $config['processcnt'] = 0;
    }
} else {
    // check the current file is not older than 2 hours
    $dateFromCurrentFile = str_replace($filePrefix, '', $config['currentfilename']);

    $dateFromCurrentFile = str_replace('.csv', '', $dateFromCurrentFile);

    $dateOfCurrentFile = DateTime::createFromFormat('d-m-Y_G-i-s', $dateFromCurrentFile, $tz);
    $allowedHours = 3;
    $date = (new DateTime())->modify('-' . $allowedHours . ' hours');
    if ($dateOfCurrentFile < $date) {
        $to = 'paul@entry4sports.com';
        $subject = 'Import is over ' . $allowedHours . ' hours old !!!!';
        $headers = array('Content-Type: text/html; charset=UTF-8');

        e4s_mail($to, $subject, 'The AAI Import requires looking at. It is over ' . $allowedHours . ' hours old.', $headers);
        reset_config($config);
        exit();
    }
}

$outputfile = str_replace($filePrefix, E4S_OUTPUT_FILE, $currentfilename);
$outputfile = str_replace('.csv', E4S_OUTPUT_FILE_PREFIX, $outputfile);
$outputfile = $importFromDir . $outputfile;
$x = 'sudo diff ' . $importedDir . $config['lastfilename'] . ' ' . $importFromDir . $config['currentfilename'] . ' > ' . $outputfile;
//shell_exec( $x );
//e4s_dump($x,"Diff Command", true, true, true);
$config['startdatetime'] = date('Y-m-d H:i:s', time());
updateImportConfig($config);
processFile($config, $outputfile);
exit();

function markConfigReady(&$config) {
    $config['active'] = E4S_IMPORT_ENABLED;
    $config['processing'] = '';
}

function reset_config($config) {
    $config['currentfilename'] = '';
    $config['startcnt'] = 0;
    markConfigReady($config);
    updateImportConfig($config);
}

function processFile($config, $fileName) {
    global $conn;
    $clubs = getClubs();
//    $fileName = $config['importfromdir'] . "/" . E4S_OUTPUT_FILE;
    $errorsOccurred = '';
    $file = fopen($fileName, 'r');

    $cnt = 1;
    $insertCnt = 0;
    $updatedCnt = 0;

    $startCnt = (int)$config['startcnt'];
    $loadCnt = (int)$config['loadcnt'];
    $endCnt = $startCnt + $loadCnt;
    while ($line = fgets($file)) {
        if (substr($line, 0, 1) !== '>') {
            continue;
        }
        $line = str_replace('> ', '', $line);
        $Error = '';

        if ($cnt % 100 === 0) {
            $config['processcnt'] = $cnt;
            updateImportConfig($config);
        }
        if ($cnt >= $startCnt) {
            $line = explode(',', $line);

            $regid = $line[0];
            $regid = str_replace($config['aocode'], '', $regid);

            $firstname = trim(addslashes($line[1]), ' ');
            $surname = trim(addslashes($line[2]), ' ');
            $club = trim(addslashes($line[3]), ' ');
            $gender = $line[4];
            $dob = convertDate($line[5]);
            $disability = $line[6];
            $active = convertDate($line[7]);

            if ($cnt >= $endCnt and $loadCnt > 0) {

                $config['startcnt'] = $endCnt;
                $config['processcnt'] = $cnt;
                importComplete($config, $file, FALSE);
                exit();
            }

            if ($club !== '') {
                $origClub = $club;
                $club = clubTranslate($club);

                if (isset ($clubs[$club])) {
                    $athletesql = 'select * from ' . E4S_COMMON_PREFIX . 'Athlete
                      where urn = ' . $regid . "
                      and aocode = '" . $config['aocode'] . "'";
//                    logSql($athletesql,1);
                    $athleteresult = mysqli_query($conn, $athletesql);
                    $sql = '';
                    $status = 'Import';
                    $row = null;
                    if ($athleteresult->num_rows === 1) {
                        $row = $athleteresult->fetch_assoc();
                        $reason = recordChanged($line, $row, $clubs [$club]);
                        if ($reason !== '') {
                            $row['reason'] = $reason;
                            $sql = 'update ' . E4S_COMMON_PREFIX . "Athlete
                            set firstname = '" . $firstname . "',
                                surname = '" . $surname . "',
                                gender = '" . $gender[0] . "',
                                dob = '" . $dob . "',
                                clubid = " . $clubs [$club] . ",
                                aocode = '" . $config['aocode'] . "',
                                activeEndDate = '" . $active . "'
                                where urn = " . $regid . "
                                and   aocode = '" . $config['aocode'] . "'";

                            $updatedCnt = $updatedCnt + 1;
                        } else {
                            $sql = '';
                        }
                    } else {
                        $sql = 'insert into ' . E4S_COMMON_PREFIX . "Athlete (aocode, urn, firstname, surname, gender,dob, clubid, activeEndDate )
                        values ('" . $config['aocode'] . "'," . $regid . ",'" . $firstname . "','" . $surname . "','" . $gender[0] . "','" . $dob . "', " . $clubs [$club] . ",'$active')";

                        $insertCnt = $insertCnt + 1;
                    }

                    if ($sql !== '') {
                        logAthlete($status, $regid, $sql, $row, $config);
                        e4s_queryNoLog('Import' . E4S_SQL_DELIM . $sql);
                    }
                } else {
                    $Error .= 'Failed to get club [' . $club . '] - [' . $origClub . ']';
                }
            } else {
                $Error = 'No club ! ';
            }
        }
        if ($Error !== '') {
            errorOutput('Error :' . $Error);
            $errorsOccurred = $Error;
        }
        $cnt = $cnt + 1;
    }
    $config['processcnt'] = $cnt;
    if ($errorsOccurred !== '') {
        debug_email('Errors have occurred in this import. Please check the logs. Last Error was :' . $errorsOccurred . '. ' . getConfigData($config));
    }
    importComplete($config, $file, TRUE);
}

function getClubs() {
// Get Clubs
    $sql = 'Select * from ' . E4S_COMMON_PREFIX . "Clubs
        where Country = 'Eire' or Country = 'Northern Ireland'";
    $result = e4s_queryNoLog($sql);

    $clubsArr = mysqli_fetch_all($result, MYSQLI_ASSOC);
    $clubs = array();
    foreach ($clubsArr as $key => $value) {
        $club = addslashes($value['Clubname']);
        $clubs[$club] = $value['id'];
    }
    return $clubs;
}

function getLatestFile($config) {
    global $tz;

    $filePrefix = $config['fileprefix'];
    $ImportFromDir = $config['importfromdir'];
    $returnFile = '';
//    $files = scandir("/var/www/import/from_em",1);
//    var_dump($files);
//    exit();
    $files = scandir($ImportFromDir, 1);

    foreach ($files as $file) {
        $prefix = substr($file, 0, strlen($filePrefix));
        if ($prefix === $filePrefix) {
            if ($returnFile === '') {
                if ($config['lastfilename'] === '') {
                    $returnFile = $file;
                } else {
                    // check the latest file is later than the one last processed

                    $dateFromDisk = str_replace($filePrefix, '', $file);

                    $dateFromDisk = str_replace('.csv', '', $dateFromDisk);

                    $diskFileDate = DateTime::createFromFormat('d-m-Y_G-i-s', $dateFromDisk, $tz);

                    $dateFromLastFile = str_replace($filePrefix, '', $config['lastfilename']);

                    $dateFromLastFile = str_replace('.csv', '', $dateFromLastFile);

                    $dateOfLastFile = DateTime::createFromFormat('d-m-Y_G-i-s', $dateFromLastFile, $tz);
                    if ($diskFileDate > $dateOfLastFile) {
                        $returnFile = $file;
                    }
                }
            }
            if ($returnFile !== $file) {
                // rename file as an old one not processed
//                $newFileName = explode(".csv", $file)[0];
//                $newFileName = $newFileName . "_imported.csv";

                rename($ImportFromDir . '/' . $file, $config['importeddir'] . '/' . $file);
            }
        }
    }

    return $returnFile;
}

function importComplete($config, $file, $rename) {

    if ($rename) {
        $currentFullFileName = $config['importfromdir'] . '/' . $config['currentfilename'];
        $newFullFileName = $config['importeddir'] . '/' . $config['currentfilename'];
        rename($currentFullFileName, $newFullFileName);

        $config['lastfilename'] = $config['currentfilename'];
        $config['currentfilename'] = '';
        $config['startcnt'] = 0;
    }
    markConfigReady($config);
    updateImportConfig($config);
    debug_email('Import Complete :- ' . getConfigData($config));
    fclose($file);
}

function getImportConfig() {
    $sql = 'Select ai.*, ao.code aocode 
            from ' . E4S_COMMON_PREFIX . 'AthleteImport ai,
                 ' . E4S_COMMON_PREFIX . 'ao ao
            where ai.aoid = ao.id
            and ai.id = ' . $GLOBALS['e4s_import_id'];
    $result = e4s_queryNoLog($sql);
    $row = null;

    if ($result !== FALSE and $result->num_rows === 1) {
        $row = $result->fetch_assoc();
        $canRun = canImportRun($row);

        if (!$canRun) {
            debug_email('Import checked but will not run: ' . $row['active'] . ':' . $row['processing']);
            exit();
        }
    } else {
        debug_email('Somehow got to exit ???');
        exit();
    }
    return $row;
}

function updateImportConfig($config) {
    $config['enddatetime'] = date('Y-m-d H:i:s', time());

    if ((int)$config['loadcnt'] === 0) {
        $config['startcnt'] = $config['processcnt'];
    }
    // read it in case now not active
    getImportConfig();

    $sql = 'update ' . E4S_COMMON_PREFIX . "AthleteImport 
            set lastfilename = '" . $config['lastfilename'] . "',
                startcnt = " . $config['startcnt'] . ',
                loadcnt = ' . $config['loadcnt'] . ',
                processcnt = ' . $config['processcnt'] . ",
                currentfilename = '" . $config['currentfilename'] . "',
                startdatetime = '" . $config['startdatetime'] . "',
                active = '" . $config['active'] . "',
                processing = '" . $config['processing'] . "',
                enddatetime = '" . $config['enddatetime'] . "' 
            where id = " . $GLOBALS['e4s_import_id'];

    e4s_queryNoLog($sql);
}

function recordChanged($line, $row, $clubid) {
    $firstName = trim($line[1], ' ');
    $surName = trim($line[2], ' ');

    $gender = $line[4][0];
    $dob = $line[5];
    $disability = $line[6];
    $active = $line[7];

    $msg = $row['URN'] . ':';
    $msgSep = '';
    if ($row['activeEndDate'] !== $active) {
        $msg .= $msgSep . 'Active:' . $row['activeEndDate'] . ' - ' . $active;
        $msgSep = '. ';
    }
    if ($row['firstName'] !== $firstName) {
        $msg .= $msgSep . 'firstName:' . $row['firstName'] . ' - ' . $firstName;
        $msgSep = '. ';
    }
    if ($row['surName'] !== $surName) {
        $msg .= $msgSep . 'Surname:' . $row['surName'] . ' - ' . $surName;
        $msgSep = '. ';
    }
    if ($row['clubid'] !== $clubid) {
        $msg .= $msgSep . 'Club:' . $row['clubid'] . ' - ' . $clubid;
        $msgSep = '. ';
    }
    if ($row['gender'] !== $gender) {
        $msg .= $msgSep . 'Gender:' . $row['gender'] . ' - ' . $gender;
        $msgSep = '. ';
    }
    if ($row['dob'] !== $dob) {
        $msg .= $msgSep . 'DOB: ' . $row['dob'] . ' - ' . $dob;
        $msgSep = '. ';
    }

    if ($msgSep === '') {
        return '';
    }
    return $msg;
}

function errorOutput($msg) {
    logSql($msg, 1);
}

function echoOutput($msg, $log) {
    return;
    global $showOutput;
    $echo = FALSE;
    if (isset($_GET['echo'])) {
        if (!$log) {
            // if echo set but also going to log. dont echo as the log will do it
            $echo = TRUE;
        }
    }
    if ($showOutput === 1 and !$echo) {
        if (gettype($msg) === 'array') {
            e4s_dump($msg);
            return;
        }
        echo $msg . '<br>';
    }
    if ($log) {
        logSql($msg, 1);
    }
}

function convertDate($date) {
    $date = str_replace('/', '-', $date);
    return date('Y-m-d', strtotime($date));
}

function logAthlete($status, $regid, $sql, $row, $config) {
    $reason = 'New Athlete';
    if (!is_null($row)) {
        $reason = addslashes($row['reason']);
        if (is_null($reason)) {
            $reason = '-';
        }
    }
    $conn = $GLOBALS['logconn'];
    $insertsql = 'INSERT INTO ' . E4S_LOG_PREFIX . 'AthleteLog ( created, urn, status, sql_cmd, reason, filename) 
                    VALUES (' . returnNow() . ',' . $regid . ",'" . $status . "', '" . addslashes($sql) . "','" . $reason . "','" . $config['currentfilename'] . "')";

    mysqli_query($conn, $insertsql);
}

function debug_email($body) {
    $to = $GLOBALS['e4s_import_email'];
//    $to = "paul.day@apiconsultancy.com";
    $subject = 'Athlete Import msg :' . $body;
    $headers = array('Content-Type: text/html; charset=UTF-8');

    e4s_mail($to, $subject, $body, $headers);
}

function getConfigData($config) {
    return ('[' . $config['currentfilename'] . ':' . $config['startcnt'] . ':' . $config['processcnt'] . ':' . $config['active'] . ':' . $config['startdatetime'] . ':' . $config['enddatetime'] . ']');
}

function canImportRun($row) {
    if (!is_null($row)) {

        if ((int)$row['active'] === E4S_IMPORT_ENABLED and $row['processing'] === E4S_IMPORT_CODE) {
            return TRUE;
        }
        if ((int)$row['active'] === E4S_IMPORT_ENABLED and $row['processing'] === '') {
            return TRUE;
        }
        // Check if stalled

        if ((int)$row['active'] === E4S_IMPORT_ENABLED) {
            //        60 seconds x 10 minutes
            $timeout = time() - (60 * 10);
            $timeoutStr = date('Y-m-d H:i:s', $timeout);

            if ($row['enddatetime'] < $timeoutStr) {
                debug_email('Timed out. resetting : ' . $timeoutStr . ':' . $row['enddatetime']);
                // System timed out
                return TRUE;
            }
        }
    }
    return FALSE;
}

function getProcessCode($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}