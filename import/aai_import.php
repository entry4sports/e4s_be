<?php

add_action('AAI_EXPORT', 'aai_export');
add_action('rest_api_init', function () {
    register_rest_route('e4s', '/import/', array('methods' => 'GET', 'callback' => 'e4sTest', 'permission_callback' => '__return_true'));
});
function e4sTest($obj) {
    // pass the diff file to process. leave blank for std processing
    $diffFile = $obj->get_param('diff');
    if (is_null($diffFile)) {
        $diffFile = '';
        $processAllLines = FALSE;
    } else {
        // if diff file passed, should all lines be processed. default only those that begin > ( true diff file )
        $processAllLines = $obj->get_param('all');
        if (is_null($processAllLines) or $processAllLines === 'false') {
            $processAllLines = FALSE;
        } else {
            $processAllLines = TRUE;
        }
    }

    exit(aai_export($diffFile, $processAllLines));
}
function e4s_output($logFile, $text, $append = true){
    if ( $append ){
        file_put_contents($logFile, $text . "\n", FILE_APPEND);
    }else{
        file_put_contents($logFile, $text . "\n");
    }
}
function aai_export($diffFile = '', $processAllLines = false): string {
    $useTZ = 'Europe/London';
    date_default_timezone_set($useTZ);
    $filePrefix = 'allmembers_';
    $ImportFromDir = 'from_em/';
    $diffDir = 'processed/';
    $importedDir = 'imported/';
    $basePath = '/kunden/homepages/30/d754245643/htdocs/clickandbuilds/import/';
    $logFile = $basePath . date('YmdGis') . '.log';
    $logFile = $basePath . 'output.log';
    e4s_output($logFile, 'Starting', false);

    if ($diffFile === '') {
        e4s_output($logFile, 'No Diff File passed');
        $importLastFile = getLastFile($basePath);
        e4s_output($logFile, 'Last File : ' . $importLastFile);
        $importNewFile = getNextFile($basePath, $filePrefix, $ImportFromDir, $importLastFile);
        e4s_output($logFile, 'New File : ' . $importNewFile);
        if ($importNewFile === '') {
            // No file to process
            echo 'No File to process';
            e4s_output($logFile, 'No File to process');
            return '';
        }

        $dateOfLast = str_replace($filePrefix, '', $importLastFile);
        $dateOfLast = str_replace('.csv', '', $dateOfLast);
        $dateOfLast = str_replace('-', '_', $dateOfLast);
        e4s_output($logFile, 'dateOfLast : ' . $dateOfLast);
        $dateOfNext = str_replace($filePrefix, '', $importNewFile);
        $dateOfNext = str_replace('.csv', '', $dateOfNext);
        $dateOfNext = str_replace('-', '_', $dateOfNext);
        e4s_output($logFile, 'dateOfNext : ' . $dateOfNext);
        $outputFile = 'diff_' . $dateOfLast . '-' . $dateOfNext . '.txt';
        $x = 'diff ' . $basePath . $importLastFile . ' ' . $basePath . $ImportFromDir . $importNewFile . ' > ' . $basePath . $diffDir . $outputFile;
//    echo $x . "\n";
        shell_exec($x);
    } else {
        $outputFile = $diffFile;
    }
    e4s_output($logFile, 'outputFile : ' . $outputFile);

    processFile($basePath . $diffDir . $outputFile, $processAllLines, $logFile);

    if ($diffFile === '') {
//    echo "remove : " . $basePath . $importLastFile . "\n";
        // move current file to imported
        e4s_output($logFile, 'rename : ' . $basePath . $importLastFile  . ' to ' .  $basePath . $importedDir . $importLastFile);
        rename($basePath . $importLastFile, $basePath . $importedDir . $importLastFile);
//    echo "Rename : " . $basePath . $ImportFromDir . $importNewFile . "\n";
//    echo "To : " . $basePath . $importNewFile . "\n";
        // move the newest file to the current file ready for next time
        e4s_output($logFile, 'rename : ' . $basePath . $ImportFromDir . $importNewFile  . ' to ' .  $basePath . $importNewFile);
        rename($basePath . $ImportFromDir . $importNewFile, $basePath . $importNewFile);
    }
    return 'done';
}

function getNextFile($basePath, $filePrefix, $importNewDir, $lastFile): string {
    $fileSuffix = '.csv';
    $dateFormat = 'd-m-Y_G-i-s';
    $ImportFromDir = $basePath . $importNewDir;
    $ignoredDir = $basePath . 'ignored/';
    $returnFile = '';

    $files = scandir($ImportFromDir, 1);

    $dateFromLastFile = str_replace($filePrefix, '', $lastFile);
    $dateFromLastFile = str_replace($fileSuffix, '', $dateFromLastFile);
    $dateFromLastFile = str_replace($basePath, '', $dateFromLastFile);

    $dateOfLastFile = DateTime::createFromFormat($dateFormat, $dateFromLastFile)->getTimestamp();

    foreach ($files as $file) {
        $prefix = substr($file, 0, strlen($filePrefix));

        if ($prefix === $filePrefix) {
            if ($returnFile === '') {
                if ($lastFile === '') {
                    $returnFile = $file;
                } else {
                    // check the latest file is later than the one last processed
                    $dateFromDisk = str_replace($filePrefix, '', $file);
                    $dateFromDisk = str_replace($fileSuffix, '', $dateFromDisk);
                    $diskFileDate = DateTime::createFromFormat($dateFormat, $dateFromDisk)->getTimestamp();

                    if ($diskFileDate > $dateOfLastFile) {
                        $returnFile = $file;
                    }
                }
            }
            if ($returnFile !== $file) {
                // rename file as an old one not processed
                rename($ImportFromDir . $file, $ignoredDir . $file);
            }
        }
    }

    return $returnFile;
}

function getLastFile($baseDir): string {
    $retFile = '';
    $files = scandir($baseDir, 1);
    foreach ($files as $file) {
        if (strpos($file, '.csv') > 0) {
            $retFile = $file;
        }
    }

    return $retFile;
}

function processFile($fileName, $processAllLines, $logFile) {
    $file = fopen($fileName, 'r');

    while ($line = fgets($file)) {
        if (!$processAllLines) {
            if (substr($line, 0, 1) !== '>') {
                continue;
            }
        }
        $line = str_replace('> ', '', $line);
        e4s_output($logFile, 'Processing Line : ' . $line);
        $line = explode(',', $line);

        $data = '?regid=' . $line[0] . '&status=I' . '&regdate=' . date('Y') . '-12-31' . '&firstname=' . urlencode(trim($line[1], ' ')) . '&surname=' . urlencode(trim($line[2], ' ')) . '&club=' . urlencode(trim($line[3], ' ')) . '&gender=' . urlencode($line[4]) . '&dob=' . urlencode(convertDate($line[5])) . '&active=' . urlencode(convertDate($line[7]));
        e4s_callApi('POST', 'https://entry.athleticsireland.ie/wp-json/e4s/v5/private/athlete/registration' . $data);
    }
    fclose($file);
}

function convertDate($date) {
    $date = str_replace('/', '-', $date);
    return date('Y-m-d', strtotime($date));
}

function e4s_callApi($method, $url, $data = FALSE) {
    $curl = curl_init();

    switch ($method) {
        case 'POST':
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }
            break;
        case 'PUT':
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        default:
            if ($data) $url = sprintf('%s?%s', $url, http_build_query($data));
    }

    // Optional Authentication:
//    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
//    curl_setopt($curl, CURLOPT_USERPWD, "username:password");

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);

    curl_close($curl);

    return $result;
}
