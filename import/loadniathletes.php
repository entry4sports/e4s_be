<?php
/**
 * Created by PhpStorm.
 * User: pauld
 * Date: 18/12/2018
 * Time: 20:44
 */

$rename_file = FALSE;
if (isset($_GET['rename'])) {
    if ($_GET['rename'] === '1') {
        $rename_file = TRUE;
    }
}
$START_CNT = 0;
if (isset($_GET['start'])) {
    $START_CNT = $_GET['start'] + 0;
}
$END_CNT = $START_CNT + 20;
if (isset($_GET['end'])) {
    // Send -1 to go to the end
    $END_CNT = $_GET['end'] + 0;
}
if ($END_CNT > 0 and $END_CNT < $START_CNT) {
    echo 'End before Start !';
    exit();
}
$showOutput = 0;
if (isset($_GET['debug'])) {
    $showOutput = $_GET['debug'] + 0;
}
$fileName = 'ni_athletes.csv';
if (isset($_GET['file'])) {
    $fileName = $_GET['file'];
}
$aocode = 'ANI';
include_once E4S_FULL_PATH . 'import/athleteImport.php';