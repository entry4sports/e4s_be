<?php
/**
 * Created by PhpStorm.
 * User: pauld
 * Date: 09/01/2019
 * Time: 14:03
 */
include_once E4S_FULL_PATH . 'dbInfo.php';
$conn = $GLOBALS['conn'];

$errorEmail = 'importErrors@entry4sports.com';
// Get all Clubs first
$clubSql = 'Select * from ' . E4S_TABLE_CLUBS . "
            where country = 'Eire'";
$clubResult = mysqli_query($conn, $clubSql);
$clubs = mysqli_fetch_all($clubResult, MYSQLI_ASSOC);
$useClubArr = array();
foreach ($clubs as $key => $club) {
    $useClubArr[$club['Clubname']] = $club;
}
//   CLUBS
$clubUserSql = 'select *
            from ' . E4S_TABLE_USERMETA . " 
            where meta_key = 'userType'
            and meta_value = 'CLUB USER'";
$clubUserResult = mysqli_query($conn, $clubUserSql);
$invalidClubs = array();
while ($row = mysqli_fetch_array($clubUserResult, TRUE)) {
    $userClubSql = 'select *
            from ' . E4S_TABLE_USERMETA . " 
            where meta_key in('club','" . E4S_CLUB_ID . "')
            and user_id = " . $row['user_id'] . '
            order by meta_key';
    $userClubResult = mysqli_query($conn, $userClubSql);
    $userClub = $userClubResult->fetch_assoc();
    $userArea = null;
    if ($userClubResult->num_rows === 2) {
        $userArea = $userClubResult->fetch_assoc();
    }

    $club = clubTranslate($userClub['meta_value']);

//echo "Club : " . $club . "<br>";
    if (array_key_exists($club, $useClubArr)) {
        $clubid = $useClubArr[$club]['id'];
        // club exists. check areaid
        if ($userArea === null) {
            // need to create the e4s_area id record
            create_e4s_record($row['user_id'], $clubid, E4S_CLUB_ID);
        } else {
            if ($clubid !== $userArea['meta_value']) {
                update_e4s_record($row['user_id'], $clubid, E4S_CLUB_ID);
            }
        }
    } else {
        if (array_key_exists($club, $invalidClubs) === FALSE) {
            warn_org_does_not_exist($club);
        }
        $invalidClubs[$club] = TRUE;
    }
}

echo 'Club imported : Invalid clubs<br>';
//e4s_dump($invalidClubs);

// County
$invalidCounties = [];
// Get all Counties first
$countySql = 'Select * from ' . E4S_TABLE_AREA . '
            where entityid = 2';
$countyResult = mysqli_query($conn, $countySql);
$counties = mysqli_fetch_all($countyResult, MYSQLI_ASSOC);
$useCountyArr = array();
foreach ($counties as $key => $county) {
    $useCountyArr[$county['name']] = $county;
}

$areaUserSql = 'select *
            from ' . E4S_TABLE_USERMETA . " 
            where meta_key = 'userType'
            and meta_value = 'COUNTY USER'";
$areaUserResult = mysqli_query($conn, $areaUserSql);
$county = '';
while ($row = mysqli_fetch_array($areaUserResult, TRUE)) {
    $userCountySql = 'select *
            from ' . E4S_TABLE_USERMETA . " 
            where meta_key in('county','" . E4S_AREA_ID . "')
            and user_id = " . $row['user_id'] . '
            order by meta_key';
    $userCountyResult = mysqli_query($conn, $userCountySql);
    $userCountyRow = $userCountyResult->fetch_assoc();
    $userArea = null;
    if ($userCountyResult->num_rows === 2) {
        $userArea = $userCountyResult->fetch_assoc();
    }

    $county = countyTranslate($userCountyRow['meta_value']);

    if (array_key_exists($county, $useCountyArr)) {
        $countyid = $useCountyArr[$county]['id'];
        // county area exists. check areaid
        if ($userArea === null) {
            // need to create the e4s_area id record
            create_e4s_record($row['user_id'], $countyid, E4S_AREA_ID);
        } else {
            if ($countyid !== $userArea['meta_value']) {
                update_e4s_record($row['user_id'], $countyid, E4S_AREA_ID);
            }
        }
    } else {
        if (array_key_exists($county, $invalidCounties) === FALSE) {
            warn_org_does_not_exist($county);
        }
        $invalidCounties[$county] = TRUE;
    }
}

echo '<br>County imported : Invalid Counties<br>';
//e4s_dump($invalidCounties);

// Regions

$invalidRegions = [];
// Get all Regions first
$regionSql = 'Select * from ' . E4S_TABLE_AREA . '
            where entityid = 3';
$regionResult = mysqli_query($conn, $regionSql);
$regions = mysqli_fetch_all($regionResult, MYSQLI_ASSOC);
$useRegionArr = array();
foreach ($regions as $key => $region) {
    $useRegionArr[$region['name']] = $region;
}

$areaUserSql = 'select *
            from ' . E4S_TABLE_USERMETA . " 
            where meta_key = 'userType'
            and meta_value = 'REGION USER'";
$areaUserResult = mysqli_query($conn, $areaUserSql);
$region = '';
while ($row = mysqli_fetch_array($areaUserResult, TRUE)) {
    $useRegionSql = 'select *
            from ' . E4S_TABLE_USERMETA . " 
            where meta_key in('region','" . E4S_AREA_ID . "')
            and user_id = " . $row['user_id'] . '
            order by meta_key desc';
    $userRegionResult = mysqli_query($conn, $useRegionSql);
    $userRegionRow = $userRegionResult->fetch_assoc();
//    echo "<br>Region Info<br>";
//    e4s_dump($userRegionRow);
//    echo "<br>";
    $userArea = null;
    if ($userRegionResult->num_rows === 2) {
        $userArea = $userRegionResult->fetch_assoc();
//        echo "Area<br>";
//        e4s_dump($userArea);
//        echo "<br>";
    }

    $region = regionTranslate($userRegionRow['meta_value']);
//    echo "Region : " . $region . "<br>";
    if (array_key_exists($region, $useRegionArr)) {
        $regionid = $useRegionArr[$region]['id'];
        // region area exists. check areaid
        if ($userArea === null) {
            // need to create the e4s_area id record
            create_e4s_record($row['user_id'], $regionid, E4S_AREA_ID);
        } else {
            if ($regionid !== $userArea['meta_value']) {
                update_e4s_record($row['user_id'], $regionid, E4S_AREA_ID);
            }
        }
    } else {
        if (array_key_exists($region, $invalidRegions) === FALSE) {
            warn_org_does_not_exist($region);
        }
        $invalidRegions[$region] = TRUE;
    }
}

echo '<br>Region imported : Invalid Regions<br>';
//e4s_dump($invalidRegions);
exit();

function regionTranslate($region) {
    return $region;
}

function countyTranslate($county) {
    return $county;
}

function create_e4s_record($id, $value, $key) {
    $sql = 'insert into ' . E4S_TABLE_USERMETA . ' (user_id, meta_key, meta_value)
                  values (' . $id . ",'" . $key . "','" . $value . "')";
    importLog($sql);
    e4s_queryNoLog($sql);
}

function update_e4s_record($id, $value, $key) {
    $sql = 'update ' . E4S_TABLE_USERMETA . " 
                  set meta_value = '" . $value . "'
                  where user_id = " . $id . "
                  and meta_key = '" . $key . "'";
    importLog($sql);
    e4s_queryNoLog($sql);
}

function warn_org_does_not_exist($org) {
    importLog($org . ' users can  not be imported as there is no club/area defined');
}

function importLog($text) {
    logSql('Import Users: ' . $text, 1);
}