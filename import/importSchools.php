<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
const E4S_TABLE_ALLSCHOOLS = 'Entry4_schools';

function e4s_importSchools(){
    $clubs = e4s_getClubs();
    $areas = e4s_getAreas();
    $schools = e4s_getSchools();
    foreach($schools as $school){
        if ( !array_key_exists($school->region, $areas)) {
            $areas = e4s_insertArea($school->region, $areas);
        }
        $area = $areas[$school->region];
        $schoolName = trim(strtoupper($school->name));
        $schoolKey = $schoolName . '-' . $area->id;

        if ( !array_key_exists($schoolKey, $clubs)) {
            e4s_insertClub($school, $area);
            $clubs[$schoolKey] = true;
        }
    }
}
function e4s_insertClub($schoolObj, $area){

    $sql = 'insert into ' . E4S_TABLE_CLUBS . " (clubname,region,country,areaid, clubtype,active)
            values(
                '" . addslashes($schoolObj->name) . "',
                '" . $schoolObj->region . "',
                'England',
                " . $area->id . ",
                'S',
                true
            )";
    e4s_queryNoLog($sql);
}
function e4s_insertArea($area, $areas){
    $sql = 'insert into ' . E4S_TABLE_AREA . " (entityId,name, shortname, parentId)
            values (
                2,
                '" . addslashes($area) . "',
                '',
                4
            )";
    e4s_queryNoLog($sql);
    $areaObj = new stdClass();
    $areaObj->id = e4s_getLastID();
    $areaObj->name = $area;
    $areas[$area] = $areaObj;
    return $areas;
}
function e4s_getSchools(){
    $schools = array();

    $sql = 'select *
            from ' . E4S_TABLE_ALLSCHOOLS;

    $result = e4s_queryNoLog($sql);
    while ( $obj = $result->fetch_object()){
        $obj->id = (int)$obj->id;
        if ( is_null($obj->town)){
            $obj->town = $obj->region;
        }
        $schools[] = $obj;
    }
    return $schools;
}

function e4s_getClub($id){
    $sql = 'select *
            from ' . E4S_TABLE_CLUBS . '
            where id = ' . $id;

    $result = e4s_queryNoLog($sql);
    return $result->fetch_object(E4S_CLUB_OBJ);
}
function e4s_getClubs(){
    $clubs = array();

    $sql = 'select *
            from ' . E4S_TABLE_CLUBS . "
            where clubtype = 'S'";

    $result = e4s_queryNoLog($sql);
    while ( $obj = $result->fetch_object(E4S_CLUB_OBJ)){
        $key = e4s_getSchoolKey($obj);
        $clubs[$key] = $obj;
    }

    return $clubs;
}
function e4s_getSchoolKey($obj){
    return trim(strtoupper($obj->clubName)) . '-' . $obj->areaId;
}
function e4s_getAreas(){
    $areas = array();

    $sql = 'select *
            from ' . E4S_TABLE_AREA ;

    $result = e4s_queryNoLog($sql);
    while ( $obj = $result->fetch_object(E4S_AREA_OBJ)){
        $areas[$obj->name] = $obj;
    }
    return $areas;
}