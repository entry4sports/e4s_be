<?php
/**
 * Created by PhpStorm.
 * User: pauld
 * Date: 18/12/2018
 * Time: 20:44
 */
///////////////
// This is the end point to call to Manually check athletes 10/2022
// https://entry.athleticsireland.ie/wp-json/e4s/v5/athletecheck
// $GLOBALS['e4s_import_id'] = 1; // (AAI) or 2 (NI);

require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';

processFile();
exit();


function processFile() {
    $config = getImportConfig();

    $GLOBALS['e4s_import_email'] = $config['email'];
    if ($GLOBALS['e4s_import_email'] === '') {
        $GLOBALS['e4s_import_email'] = E4S_IMPORT_EMAIL;
    }

    $importFromDir = $config['importfromdir'];
//    $importedDir = $config['importeddir'];
//    $filePrefix = $config['fileprefix'];
//    $outputfile = $importFromDir . $config['outputfilename'];

    $URNs = getAthletes();

    $fileName =$importFromDir . '/allmembers_07-02-2023_15-01-20.csv';
    $file = fopen($fileName, 'r');
$count = 0;
// Ignore first line
    fgets($file);
    $fileUrns = array();
    while ($line = fgets($file)) {
        $line = explode(',', $line);
        $urn = $line[0];
        $count++;
        $sep = '';
        if ( !array_key_exists($urn,$URNs)) {
            echo $sep . $urn . ' DOES NOT EXIST';
        }else{
            $fileUrns[$urn] = $urn;
        }
        $sep = "\n";
    }
    $sep = "\n";
    foreach ($URNs as $athleteURN){
        if ( !array_key_exists($athleteURN,$fileUrns)) {
            echo $sep . $athleteURN;
            $sep = ',';
        }
    }
    exit('Done. ' . $count . ' checked');
}

function getAthletes() {
// Get Clubs
    $sql = 'Select urn
            from ' . E4S_TABLE_ATHLETE . "
            where aocode = 'IRL'
            and urn is not null
            and activeenddate = '2023-12-31'";
    $result = e4s_queryNoLog($sql);

    $urns = array();
    $allURNs = mysqli_fetch_all($result, MYSQLI_ASSOC);

    foreach ($allURNs as $urnArr) {
        $urn = $urnArr['urn'];
        $urns[$urn] = $urn;
    }

    return $urns;
}

function getImportConfig() {
    $sql = 'Select ai.*, ao.code aocode 
            from ' . E4S_COMMON_PREFIX . 'AthleteImport ai,
                 ' . E4S_COMMON_PREFIX . 'ao ao
            where ai.aoid = ao.id
            and ai.id = ' . $GLOBALS['e4s_import_id'];
    $result = e4s_queryNoLog($sql);

    if ($result === FALSE or $result->num_rows < 1) {
        debug_email('Somehow got to exit ???');
        exit();
    }
    $row = $result->fetch_assoc();
    return $row;
}

function updateImportConfig($config) {
    $config['enddatetime'] = date('Y-m-d H:i:s', time());

    if ((int)$config['loadcnt'] === 0) {
        $config['startcnt'] = $config['processcnt'];
    }
//    // read it in case now not active
//    getImportConfig();

    $sql = 'update ' . E4S_COMMON_PREFIX . "AthleteImport 
            set lastfilename = '" . $config['lastfilename'] . "',
                startcnt = " . $config['startcnt'] . ',
                loadcnt = ' . $config['loadcnt'] . ',
                processcnt = ' . $config['processcnt'] . ",
                currentfilename = '" . $config['currentfilename'] . "',
                startdatetime = '" . $config['startdatetime'] . "',
                active = '" . $config['active'] . "',
                processing = '" . $config['processing'] . "',
                enddatetime = '" . $config['enddatetime'] . "' 
            where id = " . $GLOBALS['e4s_import_id'];

    e4s_queryNoLog($sql);
}

function recordChanged($line, $row, $clubid) {
    $firstName = trim($line[1], ' ');
    $surName = trim($line[2], ' ');

    $gender = $line[4][0];
    $dob = $line[5];
    $disability = $line[6];
    $active = trim($line[7]);

    $msg = $row['URN'] . ':';
    $msgSep = '';

    if ($row['activeEndDate'] !== $active) {
        $msg .= $msgSep . 'Active:' . $row['activeEndDate'] . ' - ' . $active;
        $msgSep = '. ';
    }
    if ($row['firstName'] !== $firstName) {
        $msg .= $msgSep . 'firstName:' . $row['firstName'] . ' - ' . $firstName;
        $msgSep = '. ';
    }
    if ($row['surName'] !== $surName) {
        $msg .= $msgSep . 'Surname:' . $row['surName'] . ' - ' . $surName;
        $msgSep = '. ';
    }
    if ($row['clubid'] !== $clubid) {
        $msg .= $msgSep . 'Club:' . $row['clubid'] . ' - ' . $clubid;
        $msgSep = '. ';
    }
    if ($row['gender'] !== $gender) {
        $msg .= $msgSep . 'Gender:' . $row['gender'] . ' - ' . $gender;
        $msgSep = '. ';
    }
    if ($row['dob'] !== $dob) {
        $msg .= $msgSep . 'DOB: ' . $row['dob'] . ' - ' . $dob;
        $msgSep = '. ';
    }

    if ($msgSep === '') {
        return '';
    }

    return $msg;
}

function errorOutput($msg) {
    importDebug($msg);
}

function echoOutput($msg, $log) {
    return;
    global $showOutput;
    $echo = FALSE;
    if (isset($_GET['echo'])) {
        if (!$log) {
            // if echo set but also going to log. dont echo as the log will do it
            $echo = TRUE;
        }
    }
    if ($showOutput === 1 and !$echo) {
        if (gettype($msg) === 'array') {
            e4s_dump($msg);
            return;
        }
        echo $msg . '<br>';
    }
    if ($log) {
        logSql($msg, 1);
    }
}

function convertDate($date) {
    $date = str_replace('/', '-', $date);
    return date('Y-m-d', strtotime($date));
}

function logAthlete($status, $regid, $sql, $row, $config) {
    $reason = 'New Athlete';
    if (!is_null($row)) {
        if (array_key_exists('reason', $row)) {
            $reason = addslashes($row['reason']);
            if (is_null($reason)) {
                $reason = '-';
            }
        }
    }
    $conn = $GLOBALS['logconn'];
    $insertsql = 'INSERT INTO ' . E4S_LOG_PREFIX . 'AthleteLog ( created, urn, status, sql_cmd, reason, filename) 
                    VALUES (' . returnNow() . ',' . $regid . ",'" . $status . "', '" . addslashes($sql) . "','" . $reason . "','" . $config['currentfilename'] . "')";

    mysqli_query($conn, $insertsql);
}

function debug_email($body) {
    $to = $GLOBALS['e4s_import_email'];
//    $to = "paul.day@apiconsultancy.com";
    $subject = 'Athlete Import msg :' . $body;
    $headers = array('Content-Type: text/html; charset=UTF-8');

    wp_mail($to, $subject, $body, $headers);
}

function getConfigData($config) {
    return ('[' . $config['currentfilename'] . ':' . $config['startcnt'] . ':' . $config['processcnt'] . ':' . $config['active'] . ':' . $config['startdatetime'] . ':' . $config['enddatetime'] . ']');
}

function canImportRun($row) {
    if (!is_null($row)) {

        if ((int)$row['active'] === E4S_IMPORT_ENABLED and $row['processing'] === E4S_IMPORT_CODE) {
            return TRUE;
        }
        if ((int)$row['active'] === E4S_IMPORT_ENABLED and $row['processing'] === '') {
            return TRUE;
        }
        // Check if stalled

        if ((int)$row['active'] === E4S_IMPORT_ENABLED) {
            //        60 seconds x 10 minutes
            $timeout = time() - (60 * 10);
            $timeoutStr = date('Y-m-d H:i:s', $timeout);

            if ($row['enddatetime'] < $timeoutStr) {
                debug_email('Timed out. resetting : ' . $timeoutStr . ':' . $row['enddatetime']);
                // System timed out
                return TRUE;
            }
        }
    }
    return FALSE;
}

function getProcessCode($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function sendNewClubEmailFlat($reference) {
    $bccUs = FALSE;

    $email = E4S_SUPPORT_EMAIL;
    $fromEmail = $email;
    $body = E4S_CURRENT_DOMAIN . ' New Club generated.' . '<br><br>Generated from the Live import, club ' . $reference . ' has been created. This will need updating with the correct area.';

    $body .= Entry4_emailFooter($email);

    $response = wp_mail($email, 'New Club. ' . E4S_CURRENT_DOMAIN . '/' . $reference, $body, Entry4_mailHeader($fromEmail, $bccUs));
    if (!$response) {
        Entry4UIError(9310, 'Failed to send email of new club : ' . $reference, 200, '');
    }
}