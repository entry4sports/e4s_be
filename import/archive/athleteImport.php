<?php
/**
 * Created by PhpStorm.
 * User: pauld
 * Date: 18/12/2018
 * Time: 20:44
 */
include_once E4S_FULL_PATH . 'dbInfo.php';
//sendEmail("paul@entry4sports.com", "Starting Imports." );
$filePath = '/homepages/30/d754245643/htdocs/clickandbuilds/EireE4S/import/';
$conn = $GLOBALS['conn'];
$fileName = $filePath . $fileName;
$msg = 'File : ' . $fileName . '[' . ($rename_file === TRUE ? 'true' : 'false') . ']<br>Starting at ' . $START_CNT . '<BR>Ending at ' . $END_CNT . '<br>';
$msg .= 'Params are : start={starting line number}<br>end={ending line number, -1=to the end}<br>file=filename.csv { default is athletes.csv }<br>debug=0 or 1 { Output debug lines }<br>rename=0 or 1 { rename the file on finish, default is false unless end of file reached }<br>echo=0 or 1 { Output debug and sql}<br>';
echoOutput($msg, TRUE);
if (!file_exists($fileName)) {
    $msg = $fileName . ' does not exist.';
    echoOutput($msg, TRUE);
    exit();
}

// Get Clubs
$sql = 'Select * from ' . E4S_TABLE_CLUBS;
logSql($sql, 1);
$result = mysqli_query($conn, $sql);

$clubsArr = mysqli_fetch_all($result, MYSQLI_ASSOC);
$clubs = array();
foreach ($clubsArr as $key => $value) {
    $club = addslashes($value['Clubname']);
    $clubs[$club] = $value['id'];
}
//$file = fopen('https://dev.entry4sports.com/import/athletes.csv', 'r');
$file = fopen($fileName, 'r');
// Ignore the first line ( Header data )
$line = fgetcsv($file);
$cnt = 1;
$insertCnt = 0;
$updatedCnt = 0;

while (($line = fgetcsv($file)) !== FALSE) {
    $Error = '';
    if ($cnt >= $START_CNT) {
//        Entry4_StartTransaction();
        $regid = $line[0];
        $regid = str_replace($aocode, '', $regid);
        $firstname = trim(addslashes($line[1]), ' ');
        $surname = trim(addslashes($line[2]), ' ');
        $club = trim(addslashes($line[3]), ' ');
        $gender = $line[4];
        $dob = convertDate($line[5]);
        $disability = $line[6];
        $active = convertDate($line[7]);

        if ($cnt >= $END_CNT and $END_CNT > 0) {
            closeAndRename($file, $fileName, $rename_file, $cnt);
        }
        if ($club !== '') {
            $club = clubTranslate($club);

            if (isset ($clubs[$club])) {
                $athletesql = 'select * from ' . E4S_TABLE_ATHLETE . '
                      where urn = ' . $regid . "
                           and aocode = '" . $aocode . "'";
                $athleteresult = mysqli_query($conn, $athletesql);
                $sql = '';
                if ($athleteresult->num_rows === 1) {
                    $row = $athleteresult->fetch_assoc();
                    if (recordChanged($line, $row, $clubs [$club])) {
                        $sql = 'update ' . E4S_TABLE_ATHLETE . "
                            set firstname = '" . $firstname . "',
                                surname = '" . $surname . "',
                                gender = '" . $gender[0] . "',
                                dob = '" . $dob . "',
                                clubid = " . $clubs [$club] . ",
                                activeEndDate = '" . $active . "'
                         where urn = " . $regid . "
                           and aocode = '" . $aocode . "'";
                        $updatedCnt = $updatedCnt + 1;
                    } else {
                        $sql = '';
                    }
                } else {

                    $sql = 'insert into ' . E4S_TABLE_ATHLETE . " (aocode, urn, firstname, surname, gender,dob, clubid, activeEndDate )
                        values ('" . $aocode . "'," . $regid . ",'" . $firstname . "','" . $surname . "','" . $gender[0] . "','" . $dob . "', " . $clubs [$club] . ",'$active')";
                    $insertCnt = $insertCnt + 1;
                }
                if ($sql !== '') {
                    echoOutput('URN : ' . $aocode . $regid . '<br>' . $sql, FALSE);
                    $updateresult = mysqli_query($conn, $sql);
                }
            } else {
                $Error = 'Failed to insert ' . $regid . ' Club :[' . $club . ']';
            }
        } else {
            $Error = 'No club ! ';
        }
    }
    if ($Error !== '') {
        errorOutput('Error :' . $Error);
    }
    $cnt = $cnt + 1;
}
// if not set, default on eof is to rename the file
if (!isset($_GET['rename'])) {
    $rename_file = TRUE;
}
closeAndRename($file, $fileName, $rename_file, ($cnt - 1));
exit();

function recordChanged($line, $row, $clubid) {
    $regid = $line[0];
    $firstName = trim(addslashes($line[1]), ' ');
    $surName = trim(addslashes($line[2]), ' ');
    $club = trim(addslashes($line[3]), ' ');
    $gender = $line[4][0];
    $dob = $line[5];
    $disability = $line[6];
    $active = $line[7];
    echoOutput('<br>Line ', FALSE);
    echoOutput($line, FALSE);

    echoOutput('<br>Row ', FALSE);
    echoOutput($row, FALSE);

    if ($row['activeEndDate'] !== $active) {
        echoOutput('<br>Active date Changed', FALSE);
        return TRUE;
    }
    if ($row['firstName'] !== $firstName) {
        echoOutput('<br>firstName Changed', FALSE);
        return TRUE;
    }
    if ($row['surName'] !== $surName) {
        echoOutput('<br>surName Changed', FALSE);
        return TRUE;
    }
    if ($row['clubid'] !== $clubid) {
        echoOutput('<br>clubid Changed', FALSE);
        return TRUE;
    }
    if ($row['gender'] !== $gender) {
        echoOutput('<br>Gender Changed', FALSE);
        return TRUE;
    }
    if ($row['dob'] !== $dob) {
        echoOutput('<br>DOB Changed', FALSE);
        return TRUE;
    }
    if ($row['classification'] === '0' and $disability === 'No') {
        echoOutput('<br>Record Not Changed', FALSE);
        return FALSE;
    }
    if ($row['classification'] !== $disability) {
        echoOutput('<br>Disability Changed', FALSE);
        return TRUE;
    }
    echoOutput('<br>Record Not Changed', FALSE);
    return FALSE;
}

function errorOutput($msg) {
    echo '<br>' . $msg;
    logSql($msg, 1);
}

function echoOutput($msg, $log) {
    global $showOutput;
    $echo = FALSE;
    if (isset($_GET['echo'])) {
        if (!$log) {
            // if echo set but also going to log. dont echo as the log will do it
            $echo = TRUE;
        }
    }
    if ($showOutput === 1 and !$echo) {
        if (gettype($msg) === 'array') {
            e4s_dump($msg);
            return;
        }
        echo $msg . '<br>';
    }
    if ($log) {
        logSql($msg, 1);
    }
}

function convertDate($date) {
    $date = str_replace('/', '-', $date);
    return date('Y-m-d', strtotime($date));
}

function closeAndRename($file, $fileName, $rename, $cnt) {
    global $insertCnt;
    global $updatedCnt;
//    Entry4_Commit();
    fclose($file);
    $content = '<br>Inserted : ' . $insertCnt . '. Updated : ' . $updatedCnt;
    $content .= '. Processed : ' . $cnt . ' for file ' . $fileName . '<br>';
    if ($rename) {
        $newFileName = explode('.csv', $fileName)[0];
        $newFileName = $newFileName . '_imported.csv';
        $newFileName = str_replace('/import/', '/import/imported/', $newFileName);
        $content .= 'File renamed from ' . $fileName . ' to : ' . $newFileName . '<br>';
        rename($fileName, $newFileName);
    }
    errorOutput($content);
//    sendEmail("luke@entry4sports.com", $content );
    exit();
}

function sendEmail($email, $content) {
    $body = 'Dear ' . $email . ',<br><br>';

    $body .= $content;
    $body .= '<br><br>';
    $body .= "<a href='https://" . E4S_CURRENT_DOMAIN . "'>Entry4Sports</a> Website";

    $body .= Entry4_emailFooter($email);

    e4s_mail($email, 'Entry4Sports Import', $body, Entry4_mailHeader(''));
}