<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';

e4s_processEMFile();


function e4s_processEMFile() {
    $filename = e4s_getUploadedFile();
    e4s_readEMFile($filename);
    echo "\n\nDONE\n\n";
    exit();
}

function e4s_getUploadedFile() {
    $filename = basename($_FILES['fileToUpload']['name']);

    if (strpos($filename, 'allmembers') === FALSE) {
        Entry4UIError(8010, 'Please pass an Event masters file', 200, '');
    }
    echo $filename;
    return $filename;
}

function e4s_readEMFile($filename) {
    $target_dir = 'uploads/';
    $target_file = $target_dir . $filename;
//    if (!move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
//        echo "Sorry, there was an error uploading your file.";
//        exit();
//    }
    $handle = fopen($target_file, 'r');

    if ($handle) {
        // TT header line
        fgets($handle);
    } else {
        Entry4UIError(8020, 'Failed to open the file', 200, '');
    }

    $urns = array();
    $loopCount = 1;
    $maxCount = 500;
    while (($line = fgets($handle)) !== FALSE) {
        $elements = explode(',', $line);
        $urns[] = $elements[0];
        if ($loopCount > $maxCount) {
            e4s_updateURNs($urns);
            $urns = array();
            $loopCount = 1;
        }

        $loopCount++;
    }
    e4s_updateURNs($urns);
    fclose($handle);
    Entry4UISuccess('');
}

function e4s_updateURNs($urns) {
    $sql = 'update ' . E4S_TABLE_ATHLETE . "
            set activeenddate = '" . date('Y') . "-12-31'
            where aocode = 'IRL'
            and urn in (" . implode(',', $urns) . ')';
    e4s_queryNoLog($sql);
    echo 'Updated : ' . sizeof($urns) . "\n";
}