<?php
$backup = '';

include_once E4S_FULL_PATH . 'dbInfo.php';
$conn = $GLOBALS['conn'];
// Get Clubs
$sql = 'Select * from ' . E4S_TABLE_CLUBS . $backup . "
        where Country = 'Northern Ireland'";
$result = e4s_queryNoLog($sql);
$clubsArr = $result->fetch_all(MYSQLI_ASSOC);
$clubs = array();
foreach ($clubsArr as $key => $value) {
    $club = trim($value['Clubname']);
    $clubs[strtolower($club)] = $value;
}

//e4s_dump($clubs);
//echo "<Br>";
$sql = 'select *
        from ' . E4S_TABLE_ATHLETENILOAD;
//        limit 1,10";

$result = e4s_queryNoLog($sql);

while ($row = mysqli_fetch_array($result, TRUE)) {
    $club = trim($row['club']);
    $clubid = $row['clubid'];
    $firstname = trim(addslashes($row['firstName']));
    $surname = trim(addslashes($row['surName']));
    $URN = $row['URN'];

    $athletesql = 'select * from ' . E4S_TABLE_ATHLETE . $backup . '
              where urn = ' . $URN . "
                and aocode = 'ANI'";
    $athleteresult = e4s_queryNoLog($athletesql);
    $sql = '';
    if ($athleteresult->num_rows === 1) {
        $existRow = $athleteresult->fetch_assoc();
        $useId = $existRow['id'];

        $sql = 'update ' . E4S_TABLE_ATHLETE . $backup . "
                    set firstname = '" . $firstname . "',
                        surname = '" . $surname . "',
                        gender = '" . $row['gender'] . "',
                        dob = '" . $row['dob'] . "',
                        activeEndDate = '" . $row['activeEndDate'] . "',
                        clubid = " . $clubid . '
                 where id = ' . $useId;
    } else {
//                echo "<br>Inserting " . $row['urn'];
        $sql = 'insert into ' . E4S_TABLE_ATHLETE . $backup . " (aocode, urn, firstname, surname, gender,dob, clubid, activeenddate)
                values ('ANI'," . $URN . ",'" . $firstname . "','" . $surname . "','" . $row['gender'] . "','" . $row['dob'] . "', " . $clubid . ",'" . $row['activeEndDate'] . "')";
    }
    if ($sql !== '') {
        $updateresult = e4s_queryNoLog($sql);
    }
}


echo '<br>done';

function e4s_checkClubs(&$clubs, $externid, $club, $backup) {
    $insert = FALSE;
    $clubLwr = strtolower($club);
    if (array_key_exists($clubLwr, $clubs)) {
//        Club exists
        if ((int)$clubs[$clubLwr]['externid'] === (int)$externid) {
            return;
        }
        // externid not set
        $sql = 'update ' . E4S_TABLE_CLUBS . $backup . '
                set externid = ' . $externid . '
                where id = ' . $clubs[$clubLwr]['id'];
    } else {
        $sql = 'insert into ' . E4S_TABLE_CLUBS . $backup . ' (externid, clubname, country, areaid, clubtype, active )
                values (' . $externid . ",'" . trim(addslashes($club)) . "','Northern Ireland',7,'C',1)";
        $insert = TRUE;
    }
    if ($sql !== '') {
        e4s_queryNoLog($sql);
        if ($insert) {
            $newRow = array();
            $newRow['id'] = e4s_getLastID();
            $newRow['Clubname'] = $club;
            $newRow['externid'] = $externid;

            $clubs [strtolower($club)] = $newRow;
        }
    }
}