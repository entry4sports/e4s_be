<?php
include_once E4S_FULL_PATH . 'dbInfo.php';

if (is_null($clubtype) or empty($clubtype) or $clubtype === '') {
    $clubtype = 'C';
}
$sql = 'select *
        from ' . E4S_TABLE_AREA;
$result = e4s_queryNoLog($sql);
$allareas = array();
while ($row = mysqli_fetch_array($result, TRUE)) {
    $allareas[$row['id']] = $row;
}

$areas = array();
$areaStr = '';
$areaStrSep = '';

if ((int)$useAreaId === 0) {
    $areas = $allareas;
} else {
//    echo "<br>" . $useAreaId;
    foreach ($allareas as $key => $value) {
        if ($value['id'] === $useAreaId) {
            $areas[$value['id']] = $value;
            $areaStr .= $areaStrSep . $value['id'];
            $areaStrSep = ',';
        } else {
            if ($value['parentid'] === $useAreaId) {
                $areas[$value['id']] = $value;
                $areaStr .= $areaStrSep . $value['id'];
                $areaStrSep = ',';
                foreach ($allareas as $key2 => $value2) {
                    if ($value2['parentid'] === (int)$value['id']) {
                        $areas[$value2['id']] = $value2;
                        $areaStr .= $areaStrSep . $value2['id'];
                        $areaStrSep = ',';
                    }
                }
            }
        }
    }
}

if (sizeof($areas) === 0) {
    // no areas for passed value
    Entry4UISuccess('
        "data":[]
    ');
}
$sql = 'select id, Clubname clubName,Clubname name, areaid areaId, clubType
          from ' . E4S_TABLE_CLUBS . '
          where active = 1';
if ($clubtype !== '') {
  $sql .= " and clubtype = '" . $clubtype . "'";
}
if ($search !== '') {
    $sql .= " and clubname like '%" . addslashes($search) . "%'";
}
if ($areaStr !== '') {
    $sql .= ' and areaid in (' . addslashes($areaStr) . ')';
}
if ($pagesize > 500) {
    $pagesize = 500;
}
//$sql .= " order by clubname ";
$sql .= ' limit ' . ($page * $pagesize);
//addDebug($sql);
$result = e4s_queryNoLog($sql);
//echo "<br> returned " . $result->num_rows;

$data = array();
$unattached = array('id' => 0, 'clubName' => E4S_UNATTACHED, 'County' => '', 'Region' => '', 'Country' => '');

$data[] = $unattached;
while ($row = mysqli_fetch_array($result, TRUE)) {
    $includesAccented = FALSE;
    $str = $row['clubName'];

    for ($pos = 0; $pos < strlen($str); $pos++) {
        $byte = substr($str, $pos);
        if (ord($byte) === 237) {
            $includesAccented = TRUE;
        }
        if (ord($byte) > 230) {
            $includesAccented = TRUE;
        }
//        echo 'Byte ' . $pos . ' of $str has value ' . ord($byte) . PHP_EOL;
    }
    $row['county'] = '';
    $row['region'] = '';
    $row['country'] = '';

    $areaId = (int)$row['areaId'];
    if ($areaId !== 0 and array_key_exists($row['areaId'], $areas) and $includesAccented === FALSE) {
        $row['clubName'] = normaliseStr($row['clubName']);
        $countyArea = $areas[$areaId];
        $row['county'] = normaliseStr($countyArea['name']);
        $regionId = (int)$countyArea['parentid'];
        if ($regionId !== 0 and array_key_exists($countyArea['parentid'], $allareas)) {
            $regionArea = $allareas[$regionId];
            $row['region'] = normaliseStr($regionArea['name']);
            $countryId = (int)$regionArea['parentid'];
            if ($countryId !== 0 and array_key_exists($regionArea['parentid'], $allareas)) {
                $countryArea = $allareas[$countryId];
                $row['country'] = normaliseStr($countryArea['name']);
            }
        }

        $data[] = $row;
    }
}
//e4s_dump($data);
Entry4UISuccess('
    "data":' . json_encode($data, JSON_NUMERIC_CHECK));

function normaliseStr($str) {
    $normalizeChars = array('Š' => 'S', 'š' => 's', 'Ð' => 'Dj', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ń' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ń' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y', 'ƒ' => 'f', 'ă' => 'a', 'î' => 'i', 'â' => 'a', 'ș' => 's', 'ț' => 't', 'Ă' => 'A', 'Î' => 'I', 'Â' => 'A', 'Ș' => 'S', 'Ț' => 'T',);
    return strtr($str, $normalizeChars);
}