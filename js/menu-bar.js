// Listen to click events throughout the document and close popups if click is not within the popup.
document.addEventListener("click", (e) => {
    // Variable to determine if click event came from popup trigger button.
    let isTriggerButton = false;

    // Identify if the event came from a button.
    if (DisplayPopup.getTriggerElement(e) != null) {
        // Identify if the button has a data-button-type data tag.
        if (DisplayPopup.getTriggerElement(e).hasAttribute("data-button-type")) {
            // Identify if the data tag has a TriggerPopup value, if it does,
            // set isTriggerButton to true.
            if (DisplayPopup.getTriggerElement(e).getAttribute("data-button-type") === "TriggerPopup") {
                isTriggerButton = true;
            }
        }
    }

    // Identify if the event is a trigger button.
    if (!isTriggerButton) {
        // Find the closest popup displayed.
        let popupContainer = e.target.closest(".e4s-menu-popup--container");

        // If the target does not contain the class attributed to a popup element.
        if (!popupContainer) {
            // Array containing all popup identifiers.
            let popupElements = ['FilterPopup', 'cardActionMenu'];

            // Loop through each item in the popupElements array.
            popupElements.forEach(popupElement => {
                // Find each popup listed in the array.
                // let popup = document.querySelector('#${popupElement}');
                let popup = $("#" + popupElement);
                // Identify if any of the popups do not have the hidden class, i.e. it is visible/active.
                ClosePopup(null, popup);
            });
        }
    } else {
        return false;
    }
});

// Listen for resize events and close all popups should a resize event occur.
window.addEventListener("resize", (event) => {
    let popupElements = document.querySelectorAll(".e4s-menu-popup--container");
    popupElements.forEach(popupElement => {
        $(popupElement).hide();
    });
});

// Show popup.
function ShowPopup(e, popupTarget) {
    // Get the popup trigger.
    let triggerElement = DisplayPopup.getTriggerElement(e);

    // Find all popup elements.
    let popupElements = document.querySelectorAll(".e4s-menu-popup--container");

    // Loop through all popups and hide visible popups.
    popupElements.forEach(popup => {
        // Identify if the popup doesn't have the hidden class,
        // if it doesn't, add it.
        let popupObj = $(popup);
        if (!popupObj.hasClass("hidden")) {
            popupObj.toggleClass("hidden");
        }
    });

    // Show the triggered popup.
    DisplayPopup.setPopupPosition(triggerElement, popupTarget);
}

// Close popup.
function ClosePopup(e, element) {
    // Identify if an event was passed in, if it was, prevent the default action.
    if (e !== null) {
        e.preventDefault();
    }
    $(".e4s-menu-popup--container").each(function (popup) {
        $(this).hide();
    })
}

const DisplayPopup = {
    // Get the button that triggered the event, if the button content was clicked,
    // find the button parent and return the button.
    getTriggerElement: (e) => {
        // Variable to hold the button that triggered the event.
        let elementTrigger;

        // Content inside the button can be an svg/path, find the parent button element.
        if (e.target.localName != "button") {
            elementTrigger = e.target.closest('button');
        } else {
            elementTrigger = e.target;
        }

        // Return the button element.
        return elementTrigger;
    },
    // Get the trigger element coordinates.
    getElementCoordinates: (element) => {
        return element.getBoundingClientRect();
    },
    // Set the popup position.
    setPopupPosition: (triggerElement, popupTarget) => {
        // Return if required parameters are not provided.
        if (triggerElement == null || (popupTarget == null || popupTarget == "")) {
            return false;
        }

        // Find the popup in the DOM.
        // let popupElement = document.querySelector('#${popupTarget}');
        let popupElement = $("#" + popupTarget);
        if (popupElement == null) {
            return false;
        }

        // Get the coordinates of the button launching the popup.
        let triggerElementCoords = DisplayPopup.getElementCoordinates(triggerElement);

        // Get the coordinates of where to display the popup.
        let displayPopupAtCoords = DisplayPopup.calculatePopupPosition(triggerElement, triggerElementCoords, popupTarget);

        // Set absolute positioning on the popup.
        if (popupElement.css("position") != "absolute") {
            popupElement.css("position", "absolute");
        }

        // Set the popup top and left values based on coordinates.
        popupElement.css("top", displayPopupAtCoords.top);
        popupElement.css("left", displayPopupAtCoords.left);

        // Unhide the popup.
        popupElement.show();
    },
    // Work out the position where the popup should be displayed; align with the edge of the trigger button and 8px below.
    calculatePopupPosition: (triggerElement, triggerElementCoords, popupTarget) => {
        // Return if required parameters are not provided.
        if (triggerElement == null || triggerElementCoords == null || (popupTarget == null || popupTarget == "")) {
            return false;
        }

        // Find the popup in the DOM.
        // let popupElement = document.querySelector("#${popupTarget}");
        let popupElement = $("#" + popupTarget);

        // Return if popup can't be found.
        if (popupElement == null) {
            return false;
        }

        // Get the width of the popup set within css.
        let popupElementWidth = parseInt(popupElement.css("width"));

        // Get the gap size between each button.
        let gapSize = parseInt($(triggerElement.parentElement).css("gap"));

        // Set the popup coordinates to return.
        // Note; left is defined by viewport width, set to null until viewport width is determined.
        let displayPopupAtCoordinates = {
            top: (triggerElementCoords.y + triggerElementCoords.height + gapSize) + "px",
            left: null
        };

        // Get the viewport width.
        let viewportWidth = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);

        // Identify if the viewport width is mobile (less than 500px).
        // If it is, center the popup, else place below and align along the right edge of trigger button.
        if (viewportWidth < 480) {
            displayPopupAtCoordinates.left = ((viewportWidth - popupElementWidth) / 2) + "px";
        } else {
            displayPopupAtCoordinates.left = (triggerElementCoords.x - popupElementWidth + triggerElementCoords.width) + "px";
        }

        // Return the popup coordinates.
        return displayPopupAtCoordinates;
    }
};
