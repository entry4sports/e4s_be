<style>
.e4s-menu-bar {
    justify-content: space-between;
    padding: 10px;
    background-color: var(--e4s-navigation-bar--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__background);
    color: var(--e4s-navigation-link--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__text-color);
    gap: 20px;
    border-top-right-radius: var(--e4s-card--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__border-radius);
    border-top-left-radius: var(--e4s-card--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__border-radius);
}

.e4s-menu-bar label {
    color: var(--neutral-0);
}

.e4s-menu-bar--input-container {
    flex-grow: 1;
}

.e4s-menu-bar--input-container > select {
    flex-grow: 1;
}

.e4s-menu-bar--input-container,
.event-selectors-container {
    gap: 8px;
}

.e4s-menu-bar .e4s-button > .icon-container {
    height: 20px;
    width: 20px;
}

.e4s-menu-bar .e4s-button > .icon-container > svg {
    fill: var(--e4s-navigation-bar--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__background);
}

.e4s-menu-bar .e4s-menu-bar-quick-actions--container > .e4s-button.active {
    background: var(--e4s-button--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__disabled-background);
}

.e4s-menu-bar .e4s-menu-bar-quick-actions--container > .e4s-button.active svg {
    fill: var(--neutral-0);
}

.e4s-menu-bar .event-information-container {
    padding: 8px 0;
    gap: 4px;
}

.e4s-menu-bar .event-information-container > .icon-container svg {
    fill: var(--neutral-0);
}

.e4s-menu-bar-quick-actions--container {
    position: relative;
    top: -9px;
    gap: 8px;
}

.e4s-menu-popup--container {
    width: 280px;
    min-width: 280px;
    max-width: 280px;
    padding: var(--e4s-card--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__padding);
    background: var(--e4s-card--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__background);
    border: var(--e4s-card--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__border);
    border-color: var(--e4s-card--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__border-color);
    border-radius: var(--e4s-card--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__border-radius);
    box-shadow: var(--SmallShadow);
    z-index: 1000;
}

.e4s-menu-popup--items-container {
    padding: 20px 0 0 0;
}

.e4s-menu-popup--section-container {
    color: var(--e4s-input-field__text-color);
}

.e4s-menu-popup--section-container > summary {
    margin: 8px 0;
}

.e4s-hr {
    margin: 4px 0;
    border-top: var(--e4s-card--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__border);
    border-color: var(--e4s-card--<?php echo $GLOBALS[E4S_ENVIRONMENT] ?>primary__border-color);
    border-bottom: 0;
}

.e4s-settings--label,
.e4s-settings--label:hover,
.e4s-settings--label:focus,
.e4s-settings--label:active {
    text-decoration: none;
    color: var(--e4s-input-field__text-color);
}

.hidden {
    display: none;
}

@media screen and (min-width: 581px) and (max-width: 780px) {
    .event-selectors-container {
        display: none;
    }
}

@media screen and (max-width: 580px) {
    .e4s-menu-bar > div {
        width: 100%;
        justify-content: flex-start;
        flex-grow: 1;
    }

    .event-selectors-container {
        display: none;
    }

    .e4s-menu-bar-quick-actions--container {
        top: 0;
        flex-grow: 1;
    }

    .e4s-menu-bar-quick-actions--container button {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-grow: 1;
    }
}
</style>