<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
$compid = $_GET['compid'];
$config = e4s_getConfig();
// Get Info about Comp
$allCompDOBs = getAllCompDOBs($compid);
$compRow = null;
$schoolsComp = FALSE;
$sql = 'Select c.*, cc.club
        from ' . E4S_TABLE_COMPETITON . ' c,
            ' . E4S_TABLE_COMPCLUB . ' cc
        where c.id = ' . $compid . '
        and   c.compclubid = cc.id';
$result = mysqli_query($conn, $sql);

if ($result->num_rows == 1) {
    $compRow = mysqli_fetch_array($result, TRUE);
    if (strpos($compRow['options'], 'school') != FALSE) {
        $schoolsComp = TRUE;
    }
}
//Get Club and Area Information
$sql = 'select * from ' . E4S_TABLE_CLUBS;
$result = mysqli_query($conn, $sql);
$clubsFromDB = $result->fetch_all(MYSQLI_ASSOC);
$clubsArr = array();
foreach ($clubsFromDB as $clubRow) {
    $clubsArr[$clubRow['id']] = $clubRow;
}
$sql = 'select * from ' . E4S_TABLE_AREA;
$result = mysqli_query($conn, $sql);
$areasFromDB = $result->fetch_all(MYSQLI_ASSOC);
$areaArr = array();
foreach ($areasFromDB as $areaRow) {
    $areaArr[$areaRow['id']] = $areaRow;
}

$data = array();

// Age Groups
$sql = 'SELECT id, Name FROM ' . E4S_TABLE_AGEGROUPS . ' where id in (Select agegroupid from ' . E4S_TABLE_COMPEVENTS . ' where compid = ' . $compid . ') order by minage';
$result = mysqli_query($conn, $sql);
$sep = '';
echo '[{';
echo '"agegroups" :';
echo '[';
while ($row = mysqli_fetch_array($result, TRUE)) {
    echo $sep;
    echo '{';
    echo '"id": ' . $row['id'] . ',';
    echo '"name": "' . $row['Name'] . '"';
    echo '}';
    $sep = ',';
}
echo ']';
echo '},';
// ?? Not sure why I changed this
$sql = 'select e.*
        from ' . E4S_TABLE_COMPEVENTS . ' ce
           , ' . E4S_TABLE_EVENTS . " e
        where compid = $compid
        and e.id = ce.eventid
        and ce.options not like '%secondarySpend%'";
// ??????
$sql = 'SELECT * FROM ' . E4S_TABLE_EVENTS . ' e
where id in (select eventid from ' . E4S_TABLE_COMPEVENTS . " ce where compid = $compid and ce.options not like '%secondarySpend%' )";

$sql = 'SELECT * FROM ' . E4S_TABLE_EVENTS . ' e
where id in (select eventid from ' . E4S_TABLE_COMPEVENTS . " ce where compid = $compid)";


$result = mysqli_query($conn, $sql);
echo '{';
echo '"events" :';
echo '[';
$sep = '';
while ($row = mysqli_fetch_array($result, TRUE)) {
    echo $sep;
    echo '{';
    echo '"id": ' . $row['ID'] . ',';
    echo '"name": "' . $row['Name'] . '",';
    echo '"gender": "' . $row['Gender'] . '"';
    echo '}';
    $sep = ',';
}

$sql = "select `ce`.`CompID` AS `compid`,
       `ce`.`startdate` AS `startdate`,
       `e`.`id` AS `entryid`,
       `e`.`periodStart` AS `created`,
       `a`.`schoolid` AS `schoolid`,
       `e`.`compEventID` AS `compeventid`,
       concat(`ev`.`Name`,' ',ifnull(`ce`.`eventNameExtra`,'')) AS `event`,
       `ev`.`tf` AS `tf`,
       `e`.`athleteid` AS `athleteid`,
       `a`.`gender` AS `gender`,
       `a`.`firstName` AS `firstname`,
       `a`.`surName` AS `surname`,
       `a`.`classification` AS `classification`,
       `pb`.`pb` AS `pb`,
       `e`.`orderid` AS `orderid`,
       `e`.`variationID` AS `variationid`,
       `a`.`clubid` AS `clubid`,
       `e`.`eventAgeGroup` AS `agegroup`,
       `a`.`dob` AS `dob`,
       `a`.`URN` AS `urn`,
       `ce`.`maxGroup` AS `maxgroup`,
       `e`.`price` AS `price`,
       `ep`.`fee` AS `fee` ,
       c.coupon,
       c.value,
       s.name as school,
       p.post_excerpt as notes,
       pm.meta_value as email,
       `cl`.`Clubname` AS `clubname`,
       u.uomtype,
       u.uomoptions
        from " . E4S_TABLE_ENTRIES . ' `e`
        left join ' . E4S_TABLE_ATHLETE . ' `a` on `a`.`id` = `e`.`athleteid`
        left join ' . E4S_TABLE_COMPEVENTS . ' `ce` on `ce`.`ID` = `e`.`compEventID`
        left join ' . E4S_TABLE_EVENTS . ' `ev` on `ce`.`EventID` = `ev`.`ID`
        left join ' . E4S_TABLE_ATHLETEPB . ' pb on ev.id = pb.eventid and pb.athleteid = a.id
        left join ' . E4S_TABLE_UOM . ' `u` on u.id = `ev`.`uomid`
        left join ' . E4S_TABLE_EVENTPRICE . ' `ep` on `ep`.`ID` = `ce`.`PriceID`
        left join ' . E4S_TABLE_POSTMETA . " as pm on e.orderid = pm.post_id and pm.meta_key = '_billing_email'
        left join " . E4S_TABLE_POSTS . ' as p on p.id = e.orderid
        left join ' . E4S_TABLE_SCHOOLS . ' as s on s.id = a.schoolid
        left join ' . E4S_TABLE_COUPONS . ' as c on e.orderid = c.orderid
        left join ' . E4S_TABLE_CLUBS . ' as cl on e.clubid = cl.id
        where `e`.`paid` = 1
        and ce.compid = ' . $compid . '
        order by tf, event, dob DESC';
logSql($sql, 1);
$result = mysqli_query($conn, $sql);
echo '{';
echo '"entries" :';
echo '[';
$sep = '';
$orders = array();
while ($row = mysqli_fetch_array($result, TRUE)) {
    echo $sep;
    echo '{';
    echo '"entryid": "' . $row['entryid'] . '",';
    echo '"compeventid": "' . $row['compeventid'] . '",';
    echo '"event": "' . $row['event'] . '",';
    echo '"tf": "' . $row['tf'] . '",';
    echo '"athleteid": "' . $row['athleteid'] . '",';
    echo '"firstname": "' . e4s_RemoveQuotes($row['firstname']) . '",';

    echo '"surname": "' . e4s_RemoveQuotes($row['surname']) . '",';
    echo '"gender": "' . $row['gender'] . '",';
    echo '"orderid": "' . $row['orderid'] . '",';
    echo '"variationid": "' . $row['variationid'] . '",';
    $club = $row['clubname'];

    $county = $row['clubid'];
    $region = '-';
    if (array_key_exists($row['clubid'], $clubsArr)) {
        $clubRow = $clubsArr[$row['clubid']];
        $club = $clubRow['Clubname'];

        if (array_key_exists($clubRow['areaid'], $areaArr)) {
            $areaRow = $areaArr[$clubRow['areaid']];
            $county = $areaRow['name'];

            if (array_key_exists($areaRow['parentid'], $areaArr)) {
                $areaRow = $areaArr[$areaRow['parentid']];
                $region = $areaRow['name'];
            }
        }
    }
    echo '"clubname": "' . $club . '",';
    echo '"county": "' . $county . '",';
    echo '"region": "' . $region . '",';
    if ($schoolsComp) {
        echo '"school": "' . $row['school'] . '",';
    }

    $ag = getAgeGroupInfo($allCompDOBs, $row['dob']);
    $ageGroupName = $ag['vetAgeGroup']['Name'];
    if (isset($ag['vetAgeGroup'])) {
        $ageGroupName = e4s_getVetDisplay($ageGroupName, $row['gender']);

        echo '"agegroup": "' . $ageGroupName . '",';
    } else {
        echo '"agegroup": "' . $row['agegroup'] . '",';
    }

    $pb = '' . $row['pb'];
    if ($row['uomtype'] === E4S_UOM_TIME) {
        if ($pb !== '' and $pb !== '0') {
            $pbs = explode('.', $pb);
            $pb_100secs = '00';
            if (sizeof($pbs) > 1) {
                $pb_100secs = $pbs[1];
//                if ($pb_100secs < 10) {
//                    $pb_100secs = $pb_100secs * 10;
//                }
            }
            if ((float)$pb > 60) {
                $pbTotalSecs = (int)$pbs[0];

                $pb1 = '' . ($pbTotalSecs / 60);
                $pb2 = explode('.', $pb1);
                $pb_mins = (int)$pb2[0];
                if (sizeof($pb2) === 1) {
                    $pb = $pb_mins . '.' . $pb_100secs;
                } else {
                    $pb_secs = $pbTotalSecs - ($pb_mins * 60);
                    if ($pb_secs < 10) {
                        $pb_secs = '0' . $pb_secs;
                    }
                    $pb = $pb_mins . '.' . $pb_secs . '.' . $pb_100secs;
                }
            }
        } else {
            $pb = '0.00';
        }
    }
//    echo '"pb": "'. $pb . '(' . $row['pb'] .')",';
    echo '"pb": "' . $pb . '",';
    echo '"dob": "' . $row['dob'] . '",';
    echo '"urn": "' . $row['urn'] . '",';
    echo '"classification": "' . $row['classification'] . '",';
    echo '"maxgroup": "' . $row['maxgroup'] . '",';
    if ($row['fee'] !== null) {
        $row['price'] = $row['price'] - $row['fee'];
    }
    echo '"price": "' . $row['price'] . '",';
    echo '"coupon": "' . $row['coupon'] . '",';
    echo '"value": "' . $row['value'] . '",';
    echo '"email": "' . $row['email'] . '",';
    $sSQL = 'select ev.Name
             from ' . E4S_TABLE_EVENTS . ' ev,
                  ' . E4S_TABLE_ENTRIES . ' e,
                  ' . E4S_TABLE_COMPEVENTS . ' ce
             where ce.compid = 107
             and   ce.eventid = ev.id
             and   ce.id = e.compeventid
             and   e.athleteid = ' . $row['athleteid'] . '
             and   e.orderid = ' . $row['orderid'] . '
             and   e.paid = 1
             and   ev.id in (368,369,370,371,372,373)';
    $sResults = mysqli_query($conn, $sSQL);
    if ($sResults->num_rows === 1) {
        $sRow = $sResults->fetch_assoc();
        echo '"secondary": "' . $sRow['Name'] . '",';
    } else {
        echo '"secondary": "-",';
    }
    // Get T-Shirt
    $tSQL = 'select ev.Name
             from ' . E4S_TABLE_EVENTS . ' ev,
                  ' . E4S_TABLE_ENTRIES . ' e,
                  ' . E4S_TABLE_COMPEVENTS . ' ce
             where ce.compid = 107
             and   ce.eventid = ev.id
             and   ce.id = e.compeventid
             and   e.athleteid = ' . $row['athleteid'] . '
             and   e.orderid = ' . $row['orderid'] . '
             and   e.paid = 1
             and   ev.id in (356,357,358,359,360,361,362,363,364,365,366,367)';
    $tResults = mysqli_query($conn, $tSQL);
    if ($tResults->num_rows === 1) {
        $tRow = $tResults->fetch_assoc();
        echo '"tshirt": "' . $tRow['Name'] . '",';
    } else {
        echo '"tshirt": "-",';
    }
    $addressSql = 'select meta_key, meta_value
                   from ' . E4S_TABLE_POSTMETA . "
                   where meta_key like '_billing%'
                   and   post_id = " . $row['orderid'];
    $addressResults = mysqli_query($conn, $addressSql);
    $addr1 = '-';
    $addr2 = '-';
    $addrCity = '-';
    $addrState = '-';
    $addrPostcode = '-';
    $addrPhone = '-';
    if ($addressResults->num_rows > 0) {
        $addressRows = $addressResults->fetch_all(MYSQLI_ASSOC);
        foreach ($addressRows as $addRow) {
            if ($addRow['meta_key'] === '_billing_address_1') {
                $addr1 = $addRow['meta_value'];
            }
            if ($addRow['meta_key'] === '_billing_address_2') {
                $addr2 = $addRow['meta_value'];
            }
            if ($addRow['meta_key'] === '_billing_city') {
                $addrCity = $addRow['meta_value'];
            }
            if ($addRow['meta_key'] === '_billing_state') {
                $addrState = $addRow['meta_value'];
            }
            if ($addRow['meta_key'] === '_billing_postcode') {
                $addrPostcode = $addRow['meta_value'];
            }
            if ($addRow['meta_key'] === '_billing_phone') {
                $addrPhone = $addRow['meta_value'];
            }
        }
    }
    echo '"addr1": "' . $addr1 . '",';
    echo '"addr2": "' . $addr2 . '",';
    echo '"addrcity": "' . $addrCity . '",';
    echo '"addrstate": "' . $addrState . '",';
    echo '"addrpostcode": "' . $addrPostcode . '",';
    echo '"addrphone": "' . $addrPhone . '",';

    echo '"created": "' . $row['created'] . '"';
    echo '}';
    $sep = ',';
    if ($row['notes'] != '') {
        $orders[] = $row;
    }
}

$subSql = "SELECT concat(a.firstName, ' ', a.surName) athlete, c.clubname club, s.email, ev.name event, ev.Gender gender, ag.Name agegroup, ce.split
           FROM " . E4S_TABLE_EVENTS . ' ev
           ,    ' . E4S_TABLE_COMPEVENTS . ' ce
           ,    ' . E4S_TABLE_AGEGROUPS . ' ag
           ,    ' . E4S_TABLE_SUBSCRIPTIONS . ' s Left Join ' . E4S_TABLE_ATHLETE . ' a on s.athleteid = a.id
           ,    ' . E4S_TABLE_CLUBS . ' c
           where s.compeventid = ce.id
           and ev.id = ce.eventid
           and ce.agegroupid = ag.id
           and c.id = a.clubid
           and ce.compid = ' . $compid;
//logSql($subSql, 1);

$result = mysqli_query($conn, $subSql);
//while($row = mysqli_fetch_array($result, true)){
//
//}
echo ']},';
echo '{"subscriptions" :';
echo json_encode(mysqli_fetch_all($result, MYSQLI_ASSOC));
echo '},';
echo '{"competition":{';
echo '  "name":"' . $compRow['Name'] . '",';
echo '  "club":"' . $compRow['club'] . '",';
echo '  "date":"' . $compRow['Date'] . '",';
echo '  "entriesclose":"' . $compRow['EntriesClose'] . '",';
//$compOptions = json_decode($compRow['options'], JSON_NUMERIC_CHECK);
echo '  "options":' . $compRow['options'];
echo '}},';
echo '{"orders" :';
echo '[';
if (count($orders) > 10000) {
    $sep = '';
    foreach ($orders as $orderSub => $order) {
        echo $sep;
        echo '{';
        echo '"orderid": "' . $order['orderid'] . '",';
        echo '"athlete": "' . e4s_RemoveQuotes($order['firstname'] . ' ' . $order['surname']) . '",';
        echo '"notes": "' . str_replace('"', "'", $order['notes']) . '"';
        echo '}';
        $sep = ',';
    }
}
echo ']}';

echo ']';

function e4s_RemoveQuotes($text) {
    return str_replace("'", '', $text);
}