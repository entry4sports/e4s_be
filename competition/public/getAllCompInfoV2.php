<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'competition/compStatus.php';
include_once E4S_FULL_PATH . 'competition/commonComp.php';
include_once E4S_FULL_PATH . 'entries/commonEntries.php';
include_once E4S_FULL_PATH . 'eventteams/commonEventTeam.php';

function e4s_getLiteCompsForFilter($obj){
    e4s_getCompsForFilter($obj, true);
}
function e4s_getFullPageForFilter($filter){
	if ( $filter->freeText !== '' ) {
		return FALSE;
	}
	if ($filter->orgId > 0) {
		return FALSE;
	}
	if ($filter->locationId > 0) {
		return FALSE;
	}
	return true;
}
function e4s_getCompsForFilter($obj, $lite = false) {
    $isE4SUser = isE4SUser();

	// check if in Maintenance
    if (e4s_inMaintenance(FALSE)) {
        Entry4UISuccess('"data":[],"meta":[]');
    }

    $filter = e4s_getFilterV2($obj);
    $filter->pageInfo = e4s_addStdPageInfo($obj);
    $sql = e4s_getFilteredCompetitionSql($filter);
    $result = e4s_queryNoLog($sql);
	if ( $result->num_rows < 10 ) {
		if ( e4s_getFullPageForFilter($filter) ){
			$sql = e4s_getFilteredCompetitionSql( $filter, FALSE, TRUE );
			$result = e4s_queryNoLog( $sql );
		}
	}
    $comps = array();
    if ( $result->num_rows > 0 ) {
        if ($lite) {
            while ($compObj = $result->fetch_object()) {
                $comps[] = e4s_getLiteComp($compObj);
            }
        } else {
			$contacts = array();
			$sql = "select id,email, tel, name userName, visible from " . E4S_TABLE_CONTACTS;
			$contactResult = e4s_queryNoLog($sql);
			while ($contactObj = $contactResult->fetch_object()) {
				$contactObj->id = (int)$contactObj->id;
				$contacts[$contactObj->id] = $contactObj;
			}
	        $ticketComps = array();
	        $compArr = array();
	        while ($row = e4s_normaliseCompForHomeScreen($result)) {
		        $compId = (int)$row->competition->id;
		        $compObj = e4s_getCompObj($compId, false, true);
		        if ( !is_null($compObj) ) {
			        $ticketId = $compObj->getTicketComp();
			        if ( $ticketId > 0 ) {
				        $ticketComps[ $ticketId ] = $ticketId;
			        }
			        $compArr[$compId] = $row;
		        }
	        }

			foreach($compArr as $compId=>$compObj) {
			   if ( ! isset( $ticketComps[ $compId ] ) ) {
			       if ( array_key_exists($compObj->competition->options->contact->id, $contacts) ){
			           $compObj->competition->options->contact = $contacts[$compObj->competition->options->contact->id];
			       }
			       cacheClass::addCompIdToCache( $compId );
				   $realCompObj = e4s_getCompObj($compId, false, true);
				   $realCompObj->populateAthleteSecurity($compObj->competition->options);
			       $compObj->organisers = [];
			       if ( $isE4SUser ){
			           // get Organisers for each comp
			           $orgs = e4sCompetition::getOrganisersForComp($compId, $compObj->organiser->id);
			           $compObj->status = getCompStatus($compId);
			           $compObj->organisers = $orgs;
			       }
			       $comps[] = $compObj;
			   }
			}
        }
    }
    $meta = new stdClass();
    $meta->page = $filter->pageInfo->page;
    $meta->pageSize = $filter->pageInfo->pagesize;;
    $sql = e4s_getFilteredCompetitionSql($filter, true);
    $result = e4s_queryNoLog($sql);
    $countObj = $result->fetch_object();
    $meta->totalCount = $countObj->count;
//	e4s_dump(isE4SUser(), true);
	if ( $isE4SUser ) {
		$workflows = getAllWorkflow(FALSE);
		$meta->workflow = $workflows;
	}else{
		$meta->workflow = [];
	}
    Entry4UISuccess('
        "data":' . e4s_encode($comps) . ',
        "meta":' . e4s_encode($meta));
}

function e4s_getLiteComp($obj){
    $retObj = new stdClass();
    if ( !is_null($obj)) {
        $retObj->id = (int)$obj->compId;
        $retObj->name = $obj->compName;
        $retObj->date = $obj->compDate;
        $options = e4s_addDefaultCompOptions($obj->cOptions);
        $retObj->dates = $options->dates;
        $retObj->organiser = new stdClass();
        $retObj->organiser->id = (int)$obj->compOrgId;
        $retObj->organiser->name = $obj->club;
        $retObj->organiser->status = $obj->orgStatus;

        $locName = $obj->location;
        $location = new stdClass();
        $location->id = (int)$obj->locationId;
        $location->name = $locName;
        $retObj->location = $location;
    }
    return $retObj;
}
function e4s_normaliseCompForHomeScreen($result) {
    if ($obj = $result->fetch_object()) {
        $obj->competition = new stdClass();
        $obj->competition->id = (int)$obj->compId;
        $active = false;
        if ( (int)$obj->active === 1  ){
            $active = true;
        }
        $obj->competition->active = $active;
        $obj->competition->name = $obj->compName;
        $obj->competition->link = $obj->link;
        if ($obj->logo !== '') {
            $obj->competition->logo = $obj->logo;
        } else {
            $obj->competition->logo = E4S_DEFAULT_LOGO;
        }
        $obj->competition->date = $obj->compDate;
        $obj->competition->closeDate = $obj->closeDate;
        $obj->competition->openDate = $obj->openDate;
        $obj->competition->modified = $obj->lastEntryMod;
        $obj->competition->options = e4s_addDefaultCompOptions($obj->cOptions);
		if ( (int)$obj->resultsAvailable === 1 ){
			$obj->competition->options->resultsAvailable = true;
		}else{
			$obj->competition->options->resultsAvailable = false;
		}
        unset($obj->resultsAvailable);
        unset($obj->compId);
        unset($obj->active);
        unset($obj->status);
        unset($obj->compName);
        unset($obj->compDate);
        unset($obj->lastEntryMod);
        unset($obj->cOptions);
        unset($obj->closeDate);
        unset($obj->openDate);
        unset($obj->link);
        unset($obj->logo);

        $obj->organiser = new stdClass();
        $obj->organiser->id = (int)$obj->compOrgId;
        $obj->organiser->name = $obj->club;
        $obj->organiser->status = $obj->orgStatus;
        unset($obj->compOrgId);
        unset($obj->orgStatus);
        unset($obj->club);

        $locName = $obj->location;
        $obj->location = new stdClass();
        $obj->location->id = (int)$obj->locationId;
        $obj->location->name = $locName;
        $obj->location->postcode = $obj->locPostCode;
        $obj->location->website = $obj->locWebsite;
        $obj->location->address1 = $obj->locAddress1;
        $obj->location->address2 = $obj->locAddress2;
        $obj->location->town = $obj->locTown;
        $obj->location->directions = $obj->locDirections;
        unset($obj->locationId);
        unset($obj->locPostCode);
        unset($obj->locWebsite);
        unset($obj->locAddress1);
        unset($obj->locAddress2);
        unset($obj->locTown);
        unset($obj->locDirections);

        if (isE4SUser()) {
            $obj->status = new stdClass();
            $obj->status->status = 'PAY'; // TODO get valid status
        }else{
	        $obj->status = '';
        }
	    $compId = $obj->competition->id;
        $obj->competition->text = notesClass::getNotes($compId);

        $cacheObj = new cacheClass($obj->competition->id);
        $obj->counts = $cacheObj->readCache(E4S_CACHE_COUNTS, E4S_OPTIONS_ARRAY, null, false);
    }
    return $obj;
}

function e4s_getFilterV2($obj) {
    $filter = new stdClass();
    $filter->fromDate = checkFieldForXSS($obj, 'fromdate:From Competition Date');
    if ($filter->fromDate === '' or is_null($filter->fromDate)) {
//        $filter->fromDate = 'curdate()';
        $filter->fromDate = '';
    } else {
        $filter->fromDate = "'" . $filter->fromDate . "'";
    }
    $filter->toDate = checkFieldForXSS($obj, 'todate:To Competition Date');
    if ($filter->toDate === '' or is_null($filter->toDate)) {
//        $filter->toDate = "(DATE_SUB(CURDATE(), INTERVAL 6 MONTH))";
        $filter->toDate = '';
    } else {
        $filter->toDate = "'" . $filter->toDate . "'";
    }
    $filter->freeText = checkFieldForXSS($obj, 'freetextsearch:Free Text');
    if ( is_null($filter->freeText)) {
        $filter->freeText = '';
    }else{
		$filter->freeText = trim($filter->freeText);
	    $filter->freeText = addslashes($filter->freeText);
    }
    $filter->orgId = checkFieldForXSS($obj, 'organiserid:Competitions For Current Organiser');
    if ($filter->orgId === '') {
        $filter->orgId = 0;
    } else {
        $filter->orgId = (int)$filter->orgId;
    }
    $filter->locationId = checkFieldForXSS($obj, 'locationid:Location ID');
    if ($filter->locationId === '') {
        $filter->locationId = 0;
    } else {
        $filter->locationId = (int)$filter->locationId;
    }
    $filter->compOrgId = checkFieldForXSS($obj, 'comporgid:Competitions From Organiser');
    if ($filter->compOrgId === '') {
        $filter->compOrgId = 0;
    } else {
        $filter->compOrgId = (int)$filter->compOrgId;
    }
    $filter->type = checkFieldForXSS($obj, 'type:Event Type');
    if ($filter->type !== '') {
        $filter->type = strtoupper($filter->type);
        if ($filter->type !== E4S_EVENT_FIELD and $filter->type !== E4S_EVENT_TRACK) {
            $filter->type = '';
        }
    }
    $filter->eventId = checkFieldForXSS($obj, 'eventid:Event Discipline ID');
    if ($filter->eventId === '') {
        $filter->eventId = 0;
    } else {
        $filter->eventId = (int)$filter->eventId;
    }

    $filter->version = checkFieldForXSS($obj, 'version:Competition Version');
    if ($filter->version === '') {
        $filter->version = 2;
    } else {
        $filter->version = (int)$filter->version;
    }
    $types = checkFieldForXSS($obj, 'compTypes:Competition Types');

	if (is_null($types) or $types === '') {
		$types = [];
	} else {
        $types = explode('~',$types);
		foreach ($types as $key=>$type){
			$type = strtoupper($type)[0];
			$types[$key] = $type;
		}
    }
	$filter->types = $types;

    return $filter;
}

function e4s_getFilteredCompetitionSql($filter, $countOnly = false, $getFullPage = false) {
	$compNumberSearch = false;
	if (is_numeric($filter->freeText)) {
		$compNumberSearch = true;
	}
    $sqlSelect = "select c.id compId, 
                    active,                 
                    date compDate,
                    lastEntryMod,
                    c.compclubid compOrgId,
                    c.options cOptions,
                    c.name compName,
                    c.locationId,
                    c.resultsAvailable,
                    date_format(entriesClose,'" . ISO_MYSQL_DATE . "') closeDate,
                    date_format(entriesOpen,'" . ISO_MYSQL_DATE . "') openDate,
                    link,
                    cl.logo,
                    cl.status orgStatus,
                    club,
                    location,
                    l.postcode locPostCode,
                    l.website locWebsite,
                    l.directions locDirections,
                    l.address1 locAddress1,
                    l.address2 locAddress2,
                    l.town locTown";
    if ( $countOnly ) {
        $sqlSelect = "select count(*) count";
    }
    $sqlSelect .= "
        from " . E4S_TABLE_COMPCLUB . ' cl
           , ' . E4S_TABLE_LOCATION . ' l
           , ' . E4S_TABLE_COMPETITON . ' c
       where c.locationid = l.id
       and c.id > 0
       and cl.id = c.compclubid';

	if ( !$compNumberSearch ) {
		if ( $filter->toDate !== '' ) {
			$sqlSelect .= ' and c.date between ' . $filter->fromDate . ' and ' . $filter->toDate;
		} elseif ( $filter->fromDate !== '' and !$getFullPage) {
			$sqlSelect .= ' and c.date >= ' . $filter->fromDate;
		}
	}else{
		$sqlSelect .= ' and c.id = ' . $filter->freeText;
	}

//    User has permission for org
    if ($filter->orgId > 0) {
        $sqlSelect .= ' and c.compclubid = ' . $filter->orgId;
    }

	$userId = e4s_getUserId();
	if ( $userId === E4S_USER_NOT_LOGGED_IN){
		$sqlSelect .= ' and c.active = true ';
	}else{
		if (!isE4SUser()) {
			$adminPermissions = e4s_getAdminInfo();
			if ( empty($adminPermissions) ) {
				$sqlSelect .= ' and c.active = true ';
			}else {
				$orgs = array();
				$compIds = array();
				foreach ($adminPermissions as $adminPermission) {
					if ( $adminPermission->compId > 0) {
						$compIds[] = $adminPermission->compId;
					}else {
						$orgs[] = $adminPermission->orgId;
					}
				}
				$sqlSelect .= ' and (
									c.active = true 
									';
				if ( !empty($compIds) ) {
					$sqlSelect .= ' or c.id in ( ' . implode(',', $compIds) . ' ) ';
				}
				if ( !empty($orgs) ) {
					$sqlSelect .= ' or c.compclubid in ( ' . implode(',', $orgs) . ' ) ';
				}
				$sqlSelect .= ' )';
			}
		}
    }

	if ( !$compNumberSearch ) {
		if ( $filter->locationId > 0  ) {
			$sqlSelect .= ' and c.locationId = ' . $filter->locationId;
		}

		if ( $filter->freeText !== '') {
			$sqlSelect .= ' and (';
			$sqlSelect .= " club like '%" . $filter->freeText . "%'";
			$sqlSelect .= ' or ';
			$sqlSelect .= " c.name like '%" . $filter->freeText . "%'";
			$sqlSelect .= ' or ';
			$sqlSelect .= " c.id like '%" . $filter->freeText . "%'";
			$sqlSelect .= ' ) ';
		}
			if ( $filter->type !== '' or $filter->eventId > 0 ) {
				$sqlSelect .= ' and c.id in (select distinct ce.CompID
                            from ' . E4S_TABLE_COMPEVENTS . ' ce,
                                ' . E4S_TABLE_EVENTS . ' e
                            where ce.EventID = e.id
                            and ce.CompID = c.id ';
				if ( $filter->type !== '' ) {
					$sqlSelect .= " and tf = '" . $filter->type . "'";
				}
				if ( $filter->eventId > 0 ) {
					$sqlSelect .= ' and e.id = ' . $filter->eventId;
				}
				$sqlSelect .= ')';
			}
	}

	if ( count( $filter->types ) > 0 ) {
		$sqlSelect .= ' and c.type in (';
		$sqlSelect .= "'" . implode( "','", $filter->types ) . "'";
		$sqlSelect .= ')';
	}

    if ( $countOnly or $compNumberSearch ){
        return $sqlSelect;
    }
	$desc = "";
	if ( $getFullPage ){
		$desc = " desc ";
	}
    $sqlSelect .= ' order by c.date' . $desc . ',c.name, c.id ';
//    exit($sqlSelect);
    $sqlLimit = '';
    $pagesize = $filter->pageInfo->pagesize;
    if (is_null($pagesize)) {
        $pagesize = 25;
    }
    $page = $filter->pageInfo->page;
    if (is_null($page)) {
        $page = 1;
    }

    if ($pagesize !== 0) {
        $sqlLimit = ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
    }

//exit($sqlSelect);
    return $sqlSelect . $sqlLimit;
}