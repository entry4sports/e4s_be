<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'admin/commonSecurity.php';

$GLOBALS['check_AgeGroups'] = getAgeGroups();
$GLOBALS['check_Events'] = getEvents($compId);
$GLOBALS['check_UOM'] = getUOM();

if (userHasPermission('CHECK', null, $compId) === FALSE) {
    echo 'You do not have permission to check this competition';
    exit();
}
outputHTMLInfo();
echo '<div id="tabs">
  <ul>
    <li><a href="#tabs-1">Competition Information</a></li>
    <li><a href="#tabs-2">Age Groups</a></li>
    <li><a href="#tabs-3">Rules</a></li>
    <li><a href="#tabs-4">Prices</a></li>
    <li><a href="#tabs-5">Discounts</a></li>
    <li><a href="#tabs-6">Events</a></li>
    <li><a href="#tabs-7">Definitions</a></li>
  </ul>';
echo '<div id="tabs-1">';
outputCompHeader($compId);
echo '</div><div id="tabs-2">';
compAges($compId);
echo '</div><div id="tabs-3">';
compRules($compId);
echo '</div><div id="tabs-4">';
compPrices($compId);
echo '</div><div id="tabs-5">';
compDiscounts($compId);
echo '</div><div id="tabs-6">';
compCompEvents($compId);
echo '</div><div id="tabs-7">';
compEvents($compId);
echo '</div>';
echo '</div>';
outputJS();
exit();

function outputDivider() {
    outputTableData('----------', TRUE, FALSE);
    outputTableData('----------', FALSE, FALSE);
    outputTableData('----------', FALSE, FALSE);
    outputTableData('----------', FALSE, TRUE);
}

function outputOptions($compId, $id, $rowoptions) {
    /*
     * {"min":"60.00", "max":"240","unit":"mins", "eventTeam":{"maxEventTeams":3,"teamPositionLabel":"Leg:","min":4,"max":6}, "unique":[{"e":284},{"e":301}]}
     */

    if ($rowoptions === '' || $rowoptions === '{}') {
        return;
    }
    $options = json_decode($rowoptions);
    $ageGroups = $GLOBALS['check_AgeGroups'];
    $text = '';
    $textSep = '';
    if (is_null($options)) {
        $text .= $textSep . 'ID : ' . $id . '. <b>Options are INVALID </b>!!!!! : ' . $rowoptions;
        $textSep = '<br>';
    } else {
        $disabledStatus = '';
        if (isset($options->disabled)) {
            if ($options->disabled) {
                $disabledStatus = ' DISABLED';
            }
        }
        $status = 'Not Active';
        $compObj = e4s_getCompObj($compId);
        if ($compObj->isActive()) {
            $status = 'Active';
        }

        $text .= $textSep . 'Status : ' . $status . $disabledStatus;
        $textSep = '<br>';

        if (isset($options->access)) {
            if ($options->access !== '') {
                $useAccess = $options->access;
                if ($useAccess === 'County') {
                    $useAccess = 'Countie';
                }
                $text .= $textSep . 'Access to : ' . $useAccess . 's only';
                $textSep = '<br>';
            }
        }
        if (isset($options->timetable)) {
            if ($options->timetable !== '') {
                $text .= $textSep . 'Timetable : ' . $options->timetable;
                $textSep = '<br>';
            }
        }
        if (isset($options->bibStart)) {
            if ($options->bibStart !== '') {
                $text .= $textSep . 'Starting Bib : ' . $options->bibStart;
                $textSep = '<br>';
            }
        }
        if (isset($options->photoFinish)) {
            $photoFinish = strtoupper($options->photoFinish);
            if ($photoFinish === 'TT1') {
                $text .= $textSep . 'Timetronics V1';
            }
            if ($photoFinish === 'TT2') {
                $text .= $textSep . 'Timetronics V2';
            }
            if ($photoFinish === 'LYNX') {
                $text .= $textSep . 'Lynx';
            }
        }
        if (isset($options->clubids)) {
            if ($options->clubids !== '') {
                $sql = 'select * from ' . E4S_TABLE_CLUBS;
                $result = e4s_queryNoLog($sql);
                $clubRow = array();
                $rows = $result->fetch_all(MYSQLI_ASSOC);
                foreach ($rows as $row) {
                    $clubRow[$row['id']] = $row;
                }
                $clubs = array();
                foreach ($options->clubids as $clubid) {
                    $clubs[] = $clubRow[$clubid]['Clubname'];
                }
                $text .= $textSep . 'Applies to clubs : ' . implode(', ', $clubs);
                $textSep = '<br>';
            }
        }
        $allowRegistered = FALSE;
        $allowUnRegistered = FALSE;
        if (isset($options->allowAdd)) {
            if (isset($options->allowAdd->unregistered)) {
                $allowUnRegistered = $options->allowAdd->unregistered;
//                if () {
//                    $text .= $textSep . "Available to Unregistered Athletes";
//                    $textSep = "<br>";
//                } else {
//                    $text .= $textSep . "Not available to Unregistered Athletes";
//                    $textSep = "<br>";
//                }
            }
            if (isset($options->allowAdd->registered)) {
                $allowRegistered = $options->allowAdd->registered;
            }
            if ($allowRegistered and !$allowUnRegistered) {
                $text .= $textSep . 'Available ONLY to Registered Athletes';
                $textSep = '<br>';
            } else {
                if (!$allowRegistered and $allowUnRegistered) {
                    $text .= $textSep . 'Available ONLY to Un-registered Athletes';
                    $textSep = '<br>';
                } else {
                    if ($allowRegistered and $allowUnRegistered) {
                        $text .= $textSep . 'Available to both Registered and Un-registered Athletes';
                        $textSep = '<br>';
                    } else {
                        $text .= $textSep . 'Not available to Registered or Un-regsitered ??????';
                        $textSep = '<br>';
                    }
                }
            }
        }
        if (isset($options->secondarySpend)) {
            if (isset($options->secondarySpend->isParent)) {
                if ($options->secondarySpend->isParent) {
                    $text .= $textSep . 'Secondary spend Parent';
                    $textSep = '<br>';
                }
            }
            if (isset($options->secondarySpend->alwaysShow)) {
                if ($options->secondarySpend->alwaysShow) {
                    $text .= $textSep . 'Secondary spend always shown';
                    $textSep = '<br>';
                }
            }
            if (isset($options->secondarySpend->parentCeid)) {
                $text .= $textSep . 'Secondary spend parent record id : ' . $options->secondarySpend->parentCeid;
                $textSep = '<br>';
            }
            if (isset($options->secondarySpend->mandatoryGroupId)) {
                $text .= $textSep . 'Secondary spend Mandatory Group : ' . $options->secondarySpend->mandatoryGroupId;
                $textSep = '<br>';
            }
        }
        if (isset($options->xiText)) {
            if ($options->xiText !== '') {
                $text .= $textSep . 'Extra text in Schedule : ' . $options->xiText;
                $textSep = '<br>';
            }
        }
        if (isset($options->xbText)) {
            if ($options->xbText !== '') {
                $text .= $textSep . 'Extra text in Basket : ' . $options->xbText;
                $textSep = '<br>';
            }
        }
        if (isset($options->xeText)) {
            if ($options->xeText !== '') {
                $text .= $textSep . 'Extra text in Entries list : ' . $options->xeText;
                $textSep = '<br>';
            }
        }
        if (isset($options->xrText)) {
            if ($options->xrText !== '') {
                $text .= $textSep . 'Extra text in R ?? : ' . $options->xrText;
                $textSep = '<br>';
            }
        }
        if (isset($options->availableFrom)) {
            $text .= $textSep . 'Available from : ' . e4s_sql_to_iso($options->availableFrom);
            $textSep = '<br>';
        }
        if (isset($options->availableTo)) {
            $text .= $textSep . 'Available till : ' . e4s_sql_to_iso($options->availableTo);
            $textSep = '<br>';
        }
        if (isset($options->hideOnDisable)) {
            if ($options->hideOnDisable) {
                $text .= $textSep . 'Option will be hidden rather than disabled';
            } else {
                $text .= $textSep . 'Option will be disabled rather than hidden';
            }
            $textSep = '<br>';
        }

        if (isset($options->security)) {
            $text .= $textSep . 'Security:';
            $textSep = '<br>';
            if (isset($options->security->clubs)) {
                $text .= $textSep . 'Club : ';
                $security = $options->security->clubs;
                if ($security[0] === E4S_IDS_NOT_ALLOWED) {
                    $text .= ' Not Allowed';
                } elseif ($security[0] === E4S_OPEN_TO_ALL_IDS) {
                    $text .= ' All Users';
                } else {
                    $objs = e4s_getFullSecurityClubObj($security);
                    foreach ($objs as $obj) {
                        $text .= $textSep . '&nbsp;&nbsp;&nbsp;&nbsp;' . $obj->name;
                    }
                }
            }
            if (isset($options->security->counties)) {
                $text .= $textSep . 'County : ';
                $security = $options->security->counties;
                if ($security[0] === E4S_IDS_NOT_ALLOWED) {
                    $text .= ' Not Allowed';
                } elseif ($security[0] === E4S_OPEN_TO_ALL_IDS) {
                    $text .= ' All Users';
                } else {
                    $objs = e4s_getFullSecurityAreaObj($security);
                    foreach ($objs as $obj) {
                        $text .= $textSep . '&nbsp;&nbsp;&nbsp;&nbsp;' . $obj->name;
                    }
                }
            }
            if (isset($options->security->regions)) {
                $text .= $textSep . 'Region : ';
                $security = $options->security->regions;
                if ($security[0] === E4S_IDS_NOT_ALLOWED) {
                    $text .= ' Not Allowed';
                } elseif ($security[0] === E4S_OPEN_TO_ALL_IDS) {
                    $text .= ' All Users';
                } else {
                    $objs = e4s_getFullSecurityAreaObj($security);
                    foreach ($objs as $obj) {
                        $text .= $textSep . '&nbsp;&nbsp;&nbsp;&nbsp;' . $obj->name;
                    }
                }
            }
            $text .= $textSep;
        }

        if (isset($options->eventTeam)) {
            if (!isset($options->isTeamEvent) or $options->isTeamEvent) {
                $text .= $textSep . 'Event Team:';
                $textSep = '<br>';
                if (isset($options->eventTeam->min)) {
                    $text .= $textSep . 'Min team members : ' . $options->eventTeam->min;
                    $textSep = '<br>';
                }
                if (isset($options->eventTeam->max)) {
                    $text .= $textSep . 'Max team members : ' . $options->eventTeam->max;
                    $textSep = '<br>';
                }
                if (isset($options->eventTeam->maxEventTeams)) {
                    $maxTeams = (int)$options->eventTeam->maxEventTeams;
                    if ($maxTeams === 0) {
                        $maxTeams = 'Unlimited';
                    }
                    $text .= $textSep . 'Max teams : ' . $maxTeams;
                    $textSep = '<br>';
                }
                if (isset($options->eventTeam->teamPositionLabel) and $options->eventTeam->teamPositionLabel !== '') {
                    $text .= $textSep . 'Relay label : ' . $options->eventTeam->teamPositionLabel;
                    $textSep = '<br>';
                }
                if (isset($options->eventTeam->teamSubstituteLabel) and $options->eventTeam->teamSubstituteLabel !== '') {
                    $text .= $textSep . 'Relay Sub label : ' . $options->eventTeam->teamSubstituteLabel;
                    $textSep = '<br>';
                }
            }
        }
        if (isset($options->minAgeGroupCnt)) {
            if ($options->minAgeGroupCnt !== '') {
                $text .= $textSep . 'Max upscale Athletes : ' . $options->minAgeGroupCnt;
                $textSep = '<br>';
            }
        }
        if (isset($options->excludeFromCntRule)) {
            if ($options->excludeFromCntRule === TRUE) {
                $text .= $textSep . 'Not included in event count';
                $textSep = '<br>';
            }
        }
        if (isset($options->ageGroups)) {
            $text .= $textSep . 'Upscale ages : ';
            $ageSep = '';
            foreach ($options->ageGroups as $ageGroup) {
                $text .= $ageSep . $ageGroups [$ageGroup->ageGroup]['Name'];
                $ageSep = ', ';
            }
        }

        if (isset($options->class)) {
            if ($options->class !== '') {
                $text .= $textSep . 'Classifications : ' . $options->class;
                $textSep = '<br>';
            }
        }
        if (isset($options->rowOptions)) {
            if (isset($options->rowOptions->autoExpandHelpText)) {
                if ($options->rowOptions->autoExpandHelpText === TRUE) {
                    $text .= $textSep . 'AutoExpand the help text : ' . json_encode($options->rowOptions->autoExpandHelpText);
                    $textSep = '. ';
                }
            }
            if (isset($options->rowOptions->showPrice)) {
                if ($options->rowOptions->showPrice === FALSE) {
                    $text .= $textSep . 'Show event price : ' . json_encode($options->rowOptions->showPrice);
                    $textSep = '. ';
                }
            }
            if (isset($options->rowOptions->showPB)) {
                if ($options->rowOptions->showPB === FALSE) {
                    $text .= $textSep . 'Show pb field : ' . json_encode($options->rowOptions->showPB);
                    $textSep = '. ';
                }
            }
            if (isset($options->rowOptions->singleClub)) {
                if ($options->rowOptions->singleClub) {
                    $text .= $textSep . 'Team must be from single club.';
                } else {
                    $text .= $textSep . 'Team do not have to be from single club.';
                }

                $textSep = '. ';
            }
//            $text .= $textSep . "Row options :" ;
//            $textSep = ". ";
        }
    }
    if ($text !== '') {
        outputTableData('Options:', TRUE, FALSE);
        outputBlankRows(2, FALSE);
        outputTableData($text, FALSE, TRUE);
    }
}

function getAgeGroups() {
    $sql = 'select * from ' . E4S_TABLE_AGEGROUPS;
    $results = e4s_queryNoLog($sql);
    $rows = $results->fetch_all(MYSQLI_ASSOC);
    $returnArr = array();
    foreach ($rows as $row) {
        $returnArr[$row['id']] = $row;
    }
    return $returnArr;
}

function getEvents($compid) {
    $sql = 'select * from ' . E4S_TABLE_EVENTS . ' e
            where id in (select eventid from ' . E4S_TABLE_COMPEVENTS . ' ce where compid = ' . $compid . ')';
    $results = e4s_queryNoLog($sql);
    $rows = $results->fetch_all(MYSQLI_ASSOC);
    $returnArr = array();
    foreach ($rows as $row) {
        $returnArr[$row['ID']] = $row;
    }
    return $returnArr;
}

function getUOM() {
    $sql = 'select * from ' . E4S_TABLE_UOM;
    $results = e4s_queryNoLog($sql);
    $rows = $results->fetch_all(MYSQLI_ASSOC);
    $returnArr = array();
    foreach ($rows as $row) {
        $returnArr[$row['id']] = $row;
    }
    return $returnArr;
}

function compEvents($compid) {

    echo '<table>';
//    outputTableData("<b>Events:</b>", true, false);
//    outputBlankRows(2, true);

    $rows = $GLOBALS['check_Events'];
    $lastEvent = '';
    foreach ($rows as $row) {
        if ($lastEvent !== $row['Name']) {
            outputDivider();
            //========================
            outputTableData('Name:', TRUE, FALSE);
            outputTableData($row['Name'], FALSE, FALSE);
            outputTableData('Type:', FALSE, FALSE);
            $type = $row['tf'];
            if ($type === 'T') {
                $type = 'Track';
            }
            if ($type === 'F') {
                $type = 'Field';
            }
            if ($type === 'R') {
                $type = 'Road';
            }
            outputTableData($type, FALSE, TRUE);
            $lastEvent = '';
        }
        //===================
        if ($lastEvent === $row['Name']) {
            outputTableData('', TRUE, FALSE);
        } else {
            outputTableData('Gender:', TRUE, FALSE);
        }

        $gender = fullGender($row['Gender']);

        outputTableData($gender . ' (' . $row['ID'] . ')', FALSE, FALSE);
        $uomArr = $GLOBALS['check_UOM'];
        $uom = $row['uomid'] . ' is INVALID !!!';
        if (array_key_exists($row['uomid'], $uomArr)) {
            $uom = $uomArr [$row['uomid']]['uomoptions'];
            $options = $uomArr [$row['uomid']]['uomoptions'];
            if ($options !== '') {
                $options = json_decode($options);
                if (is_null($options)) {
                    $uom = 'Options are INVALID !!!!! : ' . $uomArr [$row['uomid']]['uomoptions'];
                } else {
                    $uom = '';
                    $uomSep = '';
                    foreach ($options as $option) {
                        if (isset($option->pattern)) {
                            $uom .= $uomSep . $option->pattern;
                            $uomSep = ', ';
                        }
                    }
                }
            }
        }
        outputTableData('UOM:', FALSE, FALSE);
        outputTableData($uom, FALSE, TRUE);
        $lastEvent = $row['Name'];
        //===================
        outputOptions($compid, $row['ID'], $row['options']);
    }

    echo '</table>';
}

function compCompEvents($compid) {
    echo '<table>';

    $sql = "Select ce.*,
       date_format(eg.startdate,'%D %b %Y') as startdate_formatted,
       date_format(eg.startdate,'%H:%i') as starttime_formatted,
       a.Name agegroup,
       e.Name eventname,
       e.id eventid,
       e.gender gender,
       p.description pricename
            from " . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTGROUPS . ' eg,
                 ' . E4S_TABLE_AGEGROUPS . ' a,
                 ' . E4S_TABLE_EVENTPRICE . ' p,
                 ' . E4S_TABLE_EVENTS . ' e
            where ce.compid = ' . $compid . '
              and ce.agegroupid = a.id
              and ce.maxgroup = eg.id
              and ce.eventid = e.id
              and ce.PriceID = p.id
            order by sortdate,ce.maxgroup, eg.startdate, a.minage, e.gender';

    $results = e4s_queryNoLog($sql);
    if ($results === FALSE or $results->num_rows === 0) {
        outputTableData('<b>Competition Events:</b>', TRUE, FALSE);
        outputTableData('No Events Found !!!!', FALSE, FALSE);
        outputBlankRows(1, TRUE);
        return;
    }

    $rows = $results->fetch_all(MYSQLI_ASSOC);
    $lastEvent = '';
    $lastStartDate = '';
    $lastAgeGroup = '';
    $lastGender = '';
    $lastKey = '';
    $lastSplit = '';
    $lastPrice = '';
    foreach ($rows as $key => $row) {
        $key = $row['maxathletes'] . $row['maxGroup'];
        if ($lastEvent !== $row['eventname'] or $lastStartDate !== $row['startdate'] or $lastSplit !== $row['split'] or $lastPrice !== $row['PriceID']) {
            outputDivider();
//===================
            $split = $row['split'];
            if ($split === '0') {
                $split = '';
            } else {
                $split = ' (' . $row['split'] . ')';
            }
            outputTableData($row['startdate_formatted'], TRUE, FALSE);
            outputTableData($row['starttime_formatted'], FALSE, FALSE);
            outputTableData('Event:', FALSE, FALSE);
            outputTableData($row['eventname'] . $split . ' (' . $row['eventid'] . ')', FALSE, TRUE);
            //===================
            if ((int)$row['IsOpen'] === E4S_EVENT_CLOSED) {
                outputTableData('EVENT CLOSED', TRUE, FALSE);
                outputTableData('', FALSE, FALSE);
                outputTableData('', FALSE, FALSE);
                outputTableData('EVENT CLOSED', FALSE, TRUE);
            }
            outputTableData('Price: ', TRUE, FALSE);
            outputTableData($row['pricename'] . ' (' . $row['PriceID'] . ')', FALSE, FALSE);

            //===================
            outputTableData('Max Group: ', TRUE, FALSE);
            outputTableData($row['maxGroup'], FALSE, FALSE);
            outputTableData('Max Athletes:', FALSE, FALSE);
            if ((int)$row['maxathletes'] > 0) {
                outputTableData($row['maxathletes'], FALSE, TRUE);
            }
            if ((int)$row['maxathletes'] === 0) {
                outputTableData('No Limit', FALSE, TRUE);
            }
            if ((int)$row['maxathletes'] < 0) {
                outputTableData('No Entries allowed', FALSE, TRUE);
            }
            //===================
            outputTableData('Age Groups', TRUE, FALSE);
            outputBlankRows(2, TRUE);

            $lastEvent = '';
            $lastStartDate = '';
            $lastAgeGroup = '';
            $lastGender = '';
            $lastKey = '';
            $lastSplit = '';
            $lastPrice = '';
        }
        //===================
        $gender = fullGender($row['gender']);

        if ($gender === $lastGender) {
            outputTableData('', TRUE, FALSE);
        } else {
            outputTableData($gender, TRUE, FALSE);
        }
        $lastGender = $gender;

        if ($lastAgeGroup !== $row['agegroup']) {
            outputTableData($row['agegroup'], FALSE, FALSE);
        } else {
            outputTableData('', FALSE, FALSE);
        }

        outputTableData('(' . $row['ID'] . ')', FALSE, TRUE);
        //===================
        outputOptions($compid, $row['ID'], $row['options']);

        //=================
        $lastEvent = $row['eventname'];
        $lastStartDate = $row['startdate'];
        $lastAgeGroup = $row['agegroup'];

        $lastKey = $row['split'] . $row['maxathletes'] . $row['maxGroup'];
        $lastSplit = $row['split'];
        $lastPrice = $row['PriceID'];
    }

    echo '</table>';

}

function compDiscounts($compid) {
    echo '<table>';

    $sql = 'Select * from ' . E4S_TABLE_COMPDISCOUNTS . '
            where compid = ' . $compid;
    $results = e4s_queryNoLog($sql);
    if ($results->num_rows === 0) {
        outputTableData('<b>Competition Discounts:</b>', TRUE, FALSE);
        outputTableData('', FALSE, FALSE);
        outputTableData('', FALSE, FALSE);
        outputTableData('None', FALSE, TRUE);
        echo '</table>';
        return;
    }

    $rows = $results->fetch_all(MYSQLI_ASSOC);
    foreach ($rows as $row) {
        if ((int)$row['agegroupid'] === 0) {
            outputTableData('All Age Groups:', TRUE, FALSE);
            outputTableData('', FALSE, FALSE);
        } else {
            outputTableData('Age Group ID:', TRUE, FALSE);
            outputTableData($row['agegroupid'], FALSE, FALSE);
        }
        outputTableData('Type:', FALSE, FALSE);
        $discType = 'Value';
        if ($row['type'] === 'P') {
            $discType = 'Percentage';
        }
        outputTableData($discType, FALSE, TRUE);

        outputTableData('After Count:', TRUE, FALSE);
        if ((int)$row['count'] === 0) {
            outputTableData('Always apply if criteria matched', FALSE, FALSE);
        } else {
            outputTableData($row['count'], FALSE, FALSE);
        }

        outputTableData('', FALSE, FALSE);
        outputTableData('', FALSE, TRUE);

        outputTableData('Sale Disc:', TRUE, FALSE);
        if ((int)$row['sale_disc'] < 0) {
            outputTableData('Price ' . $row['sale_disc'], FALSE, FALSE);
        } else {
            outputTableData($row['sale_disc'], FALSE, FALSE);
        }

        outputTableData('Regular Disc:', FALSE, FALSE);
        if ((int)$row['reg_disc'] < 0) {
            outputTableData('Price ' . $row['reg_disc'], FALSE, FALSE);
        } else {
            outputTableData($row['reg_disc'], FALSE, FALSE);
        }

        outputTableData('----------------', TRUE, FALSE);
        outputTableData('----------------', FALSE, FALSE);
        outputTableData('----------------', FALSE, FALSE);
        outputTableData('----------------', FALSE, TRUE);

        outputOptions($compid, $row['id'], $row['options']);
    }
    echo '</table>';
}

function compPrices($compid) {
    echo '<table>';
    //====================
    $sql = 'select * from ' . E4S_TABLE_EVENTPRICE . '
            where compid = ' . $compid;
    $results = e4s_queryNoLog($sql);
    if ($results->num_rows === 0) {
        outputTableData('<b>Competition Prices:</b>', TRUE, FALSE);
        outputTableData('', FALSE, FALSE);
        outputTableData('None Set', FALSE, FALSE);
        outputTableData('!!!!!', FALSE, TRUE);
        return;
    }

    $rows = $results->fetch_all(MYSQLI_ASSOC);
    foreach ($rows as $key => $row) {
        outputTableData('Price :', TRUE, FALSE);
        $desc = $row['description'];
        if ($desc === '') {
            $desc = $row['name'];
        }
        outputTableData($row['ID'], FALSE, FALSE);
        outputTableData('Desc:', FALSE, FALSE);
        outputTableData($desc, FALSE, TRUE);

        $priceOptions = new stdClass();
        $displayFee = FALSE;
        if ($row['options'] !== '') {
            $priceOptions = json_decode($row['options']);
            if (isset($priceOptions->displayfee)) {
                $displayFee = $priceOptions->displayfee;
            }
        }
        //====================
        $saledate = $row['saleenddate'];
        if (!is_null($saledate)) {
            $saledate = new DateTime($saledate);
            $saledate = date_format($saledate, E4S_FORMATTED_DATETIME);
            outputTableData('Price Increase at :', TRUE, FALSE);
            outputTableData($saledate, FALSE, FALSE);
            outputTableData('', FALSE, FALSE);
            outputTableData('', FALSE, TRUE);
            //====================
            outputTableData('Sale Price:', TRUE, FALSE);
            outputTableData($row['saleprice'], FALSE, FALSE);
            $fee = $row['salefee'];
            if (is_null($fee)) {
                $fee = 'Included';
            }
            outputTableData('Sale Fee', FALSE, FALSE);
            outputTableData($fee, FALSE, TRUE);
            //====================
        }
        outputTableData('Std Price:', TRUE, FALSE);
        outputTableData($row['price'], FALSE, FALSE);
        $fee = $row['fee'];
        if (is_null($fee)) {
            $fee = 'Included';
        }
        outputTableData('Std Fee', FALSE, FALSE);
        outputTableData($fee, FALSE, TRUE);
        //====================
        outputTableData('', TRUE, FALSE);
        outputTableData('', FALSE, FALSE);
        outputTableData('Fee:', FALSE, FALSE);
        if ($displayFee === TRUE) {
            outputTableData('Displayed in basket', FALSE, TRUE);
        } else {
            outputTableData('Hidden from basket', FALSE, TRUE);
        }
        outputTableData('--------', TRUE, FALSE);
        outputTableData('--------', FALSE, FALSE);
        outputTableData('--------', FALSE, FALSE);
        outputTableData('--------', FALSE, TRUE);
    }

    echo '</table>';
}

function compRules($compid) {
    echo '<table>';
    outputTableData('<b>Competition Rules:</b>', TRUE, FALSE);
    outputTableData('', FALSE, FALSE);

    //====================
    $sql = 'select r.*, a.Name agegroupname, a.minage
            from ' . E4S_TABLE_COMPRULES . ' r
                 left join ' . E4S_TABLE_AGEGROUPS . ' a on r.agegroupid = a.id
            where compid = ' . $compid . '
            order by a.minage';

    $results = e4s_queryNoLog($sql);
    if ($results->num_rows === 0) {
        outputTableData('None set', FALSE, FALSE);
        outputTableData('', FALSE, TRUE);
        echo '</table>';
        return;
    }
    outputTableData('Total', FALSE, FALSE);
    outputTableData($results->num_rows, FALSE, TRUE);

    $rows = $results->fetch_all(MYSQLI_ASSOC);
    foreach ($rows as $key => $row) {

        if ($row['options'] !== '') {
            $ageGroupName = $row['agegroupname'];
            if ($ageGroupName === null) {
                $ageGroupName = 'All Age Groups';
            }

            $options = json_decode($row['options']);

            $text = '';
            $textSep = '';
            if (is_null($options)) {
                $text .= $textSep . 'Options are INVALID !!!!! : ' . $row['options'];
                $textSep = '. ';
            }

            if (isset($options->maxTeamsForAthlete)) {
                $text .= $textSep . 'Max events an athlete can be in is ' . $options->maxTeamsForAthlete;
                $textSep = '. ';
            }
            if (isset($options->maxEvents)) {
                $text .= $textSep . 'Max events for the day is ' . $options->maxEvents;
                $textSep = '. ';
            }
            if (isset($options->maxCompEvents)) {
                $text .= $textSep . 'Max events for the competition is ' . $options->maxCompEvents;
                $textSep = '. ';
            }
            if (isset($options->maxExcludedEvents)) {
                $text .= $textSep . 'Max events excluded from counts is ' . $options->maxExcludedEvents;
                $textSep = '. ';
            }
            outputTableData($ageGroupName . ':', TRUE, FALSE);
            outputTableData('', FALSE, FALSE);
            outputTableData('', FALSE, FALSE);
            outputTableData($text, FALSE, TRUE);
        }
        //====================
    }

    echo '</table>';
}

function compAges($compid) {
    $allCompDOBs = getAllCompDOBs($compid);
    echo '<table>';

//    outputTableData("<b>Age Groups:</b>", true, false);
//    outputTableData("", false, false);
//    outputTableData("", false, false);
//    outputTableData("", false, true);
    //====================
    foreach ($allCompDOBs as $key => $compage) {
        outputTableData('Age Group:', TRUE, FALSE);
        $ageGroupName = e4s_getVetDisplay($compage['Name'], 'O');
        outputTableData($ageGroupName, FALSE, FALSE);
        outputTableData('ID', FALSE, FALSE);
        outputTableData($compage['agid'], FALSE, TRUE);
        //====================
        outputTableData('Min Age:', TRUE, FALSE);
        outputTableData($compage['minAge'], FALSE, FALSE);
        outputTableData('Max Age:', FALSE, FALSE);
//        if ((int) $compage['year'] === -1 ){
//            outputTableData((int)$compage['MaxAge'] + 1, false, true);
//        } else {
        outputTableData($compage['MaxAge'], FALSE, TRUE);
//        }

        //====================
        $to = new DateTime($compage['toDate']);
        $toDate = date_format($to, 'd M Y');
        $from = new DateTime($compage['fromDate']);
        $fromDate = date_format($from, 'd M Y');

        outputTableData('D.O.B:', TRUE, FALSE);
        outputTableData($toDate, FALSE, FALSE);
        outputTableData('D.O.B', FALSE, FALSE);
        outputTableData($fromDate, FALSE, TRUE);
        //====================
        outputDivider();
    }
    echo '</table>';
}

function outputCompHeader($compid) {
    $format = '%D %b %Y %H:%i';
    $shortformat = '%D %b %Y';
    $sql = "select  date_format(date,'" . $shortformat . "') compdate,
                    comp.name compname,
                    date_format(entriesOpen,'" . $format . "') opendate,
                    date_format(entriesClose,'" . $format . "') closedate,
                    link,
                    club,
                    location,
                    comp.areaid,
                    area.entityid,
                    area.name areaname,
                    comp.options,
                    comp.id compid
        from " . E4S_TABLE_COMPCLUB . ' club
           , ' . E4S_TABLE_LOCATION . ' loc
           , ' . E4S_TABLE_COMPETITON . ' comp
           left join ' . E4S_TABLE_AREA . ' area on area.id = comp.areaid
           where comp.id =' . $compid;

    $sql .= ' and comp.locationid = loc.id';
    $sql .= ' and club.id = comp.compclubid';

    $result = e4s_queryNoLog('CheckCompHeader' . E4S_SQL_DELIM . $sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(1000, 'Failed to find competition.', 404, '');
    }
    $compRow = $result->fetch_assoc();
    echo '<table>';

    outputTableData('<b>Competition :</b>', TRUE, FALSE);
    $compObj = e4s_getCompObj($compid);
    outputTableData($compObj->getDisplayName(), FALSE, FALSE);
    outputTableData('Competition Date :', FALSE, FALSE);
    outputTableData($compRow['compdate'], FALSE, TRUE);
    //====================
    outputTableData('Entries Open :', TRUE, FALSE);
    outputTableData($compRow['opendate'], FALSE, FALSE);
    outputTableData('Entries Close :', FALSE, FALSE);
    outputTableData($compRow['closedate'], FALSE, TRUE);
    //====================
    outputTableData('Club :', TRUE, FALSE);
    outputTableData($compRow['club'], FALSE, FALSE);
    outputTableData('Location :', FALSE, FALSE);
    outputTableData($compRow['location'], FALSE, TRUE);
    //====================
    $area = 'Any Area';
    if ((int)$compRow['areaid'] !== 0) {
        $area = $compRow['areaname'] . ' only';
    }
    outputTableData('Athletes From :', TRUE, FALSE);
    outputTableData($area, FALSE, FALSE);
    outputTableData('Link :', FALSE, FALSE);
    $link = 'No Flyer set';
    if ($compRow['link'] !== '') {
        $link = "<a href='" . $compRow['link'] . "'>Flyer</a>";
    }
    outputTableData($link, FALSE, TRUE);
    outputOptions($compRow['compid'], $compRow['compid'], $compRow['options']);
    //====================
    echo '</table>';
}

function outputBlankRows($count, $finishWithEnd) {
    for ($c = 0; $c < $count; $c += 1) {
        outputTableData('', FALSE, FALSE);
    }
    if ($finishWithEnd) {
        outputTableData('', FALSE, TRUE);
    }
}

function outputTableData($txt, $start, $end) {
    if ($start) {
        echo '<tr>';
    }
    echo '<td>';
    echo $txt;
    echo '</td>';
    if ($end) {
        echo '</tr>';
    }
}

function outputHTMLInfo() {
//    <link rel="stylesheet" href="/resources/demos/style.css">
    echo '
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>';
}

function outputJS() {
    echo '<script>
    $( function() {
        $( "#tabs" ).tabs();
    } );
    </script>
    ';
}