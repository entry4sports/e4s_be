<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'competition/compStatus.php';
include_once E4S_FULL_PATH . 'competition/commonComp.php';
include_once E4S_FULL_PATH . 'entries/commonEntries.php';
include_once E4S_FULL_PATH . 'eventteams/commonEventTeam.php';
$useStatus = isE4SUser();
//e4s_echo(date("Y-m-d H:i:s"));
//e4s_echo( e4s_getOffset(date("Y-m-d H:i:s")), true);
if (!isset($orgid)) {
    $orgid = 0;
}

$config = e4s_getConfig();

// check if in Maintenance
if (e4s_inMaintenance(FALSE)) {
    Entry4UISuccess('"data":[],"meta":[]');
}

$GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS] = [];
$rows                                   = getCompsForUser($orgid, 0, TRUE, FALSE, FALSE);

if (sizeof($rows) > 0) {
//    usort($rows, 'date_compare');
    $ticketComps = array();
    $compArr = array();
    foreach ($rows as $row) {
        $compId = (int)$row['compId'];
        $compObj = e4s_getCompObj($compId, true, false);
        if ( !is_null($compObj) ) {
            $ticketInfo = $compObj->getTicketComp();
			if ( $ticketInfo > 0 ) {
				$ticketComps[ $ticketInfo ] = $ticketInfo;
			}
            $row = $compObj->getFullRow(true);
            $compArr[$compId] = $row;
        }
    }
    $data = array();
    // dont Show any ticket comps
    foreach($compArr as $ticketId=>$compObj) {
        if (!isset($ticketComps[$ticketId])) {
            $data[] = $compObj;
        }
    }

} else {
    $row = array();
    $row['compId'] = 0;
    $row['date'] = array();
    $row['name'] = 'None Available';
    $row['entriesClose'] = '';
    $row['entriesOpen'] = '';
    $row['link'] = '';
    $row['information'] = '';
    $row['club'] = '';
    $row['location'] = '{}';
    $row['areaname'] = '';
    $row['saleenddate'] = '';
    $row['access'] = '';
    $data[] = $row;
}
$meta = array();
if ($useStatus) {
    $workflows = getAllWorkflow(FALSE);
    $meta['workflow'] = $workflows;
}

//e4s_dump($newData, "returned", true);
Entry4UISuccess('
    "data":' . e4s_encode($data) . ',
    "meta":' . e4s_encode($meta));

function date_compare($a, $b) {
    $t1 = strtotime($a['date']);
    $t2 = strtotime($b['date']);
    return $t1 - $t2;
}
