<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'competition/commonComp.php';
include_once E4S_FULL_PATH . 'eventteams/commonEventTeam.php';
//$clubid = $_GET['club']; // already exists in calling Rest ( functions.php  )
$getdate = getdate();

$useDate = $getdate['year'] . '-';
$useDate .= $getdate['mon'] . '-';
$useDate .= $getdate['mday'] . 'T';
$useDate .= $getdate['hours'] . ':';
$useDate .= $getdate['minutes'] . ':';
$useDate .= $getdate['seconds'];
$inMaintenance = e4s_inMaintenance(FALSE);
// $club is the club selected before coming to get the dates. $club SHOULD NOT be blank to get here, but check anyway
if ($club === '') {
    Entry4UISuccess('
     "compDates" : [],
     "systemTime" : "' . $useDate . '",
     "clubInfo" : ""
   ');
    exit();
}

$rows = getCompsForUser($club, (int)$compId, FALSE, TRUE, TRUE);

if (empty($rows)) {
    Entry4UIError(8812, 'Competition Not found or Available');
}
$contactObj = new e4sContactClass(0);
$data = array();
$clubLogo = '';
$agObj = new e4sAgeClass(E4S_AGE_DEFAULT_BASE);
foreach ($rows as $row) {
    $tandc = array();
    $tandc['link'] = '';

    $row['name'] = $row['compName'];
    unset($row['compName']);
    if ($inMaintenance) {
        $row['name'] = 'SYSTEM MAINTENANCE IN PROGRESS. Competition unavailable';
    }

    $tandc['description'] = 'I agree that entry fees paid are non transferable and non refundable for this event.';
    $row['tandc'] = $tandc;

    $clubLogo = $row['logo'];
    if ($row['areaid'] > 0) {
        // get Location information
        $areaSQL = 'select a.name areaname, e.name entity, e.id entityid
                    from ' . E4S_TABLE_AREA . ' a,
                         ' . E4S_TABLE_ENTITY . ' e
                    where a.id = ' . $row['areaid'] . '
                    and   a.entityid = e.id';
        $arearesult = e4s_queryNoLog($areaSQL);
        $areaRow = mysqli_fetch_array($arearesult, TRUE);
        $row['areaname'] = $areaRow['areaname'];
        $row['entity'] = $areaRow['entity'];
        $row['entityid'] = $areaRow['entityid'];
    } else {
        $row['areaname'] = '';
        $row['entity'] = '';
        $row['entityid'] = 0;
    }
    $compObj = e4s_GetCompObj($row['compId'], TRUE);
    $options = $compObj->getOptions();
    $compObj->setClubComp(true);
    e4s_readSelfService($row['compId'], $options);

    $compObj->populateAthleteSecurity($options);
    $row['options'] = $contactObj->getFullInfo($options);

    // CompTeamConfig
    $ctc = array();
    $ctc['ctcid'] = $row['ctcid'];
    $ctc['maxMale'] = $row['maxmale'];
    $ctc['maxFemale'] = $row['maxfemale'];
    $ctc['maxTeams'] = $row['maxteams'];
    $ctc['maxAthletes'] = $row['maxathletes'];
    $ctc['maxAgeGroup'] = $row['maxagegroup'];
    $ctc['uniqueEvents'] = $row['uniqueevents'];
    $ctc['singleAgeGroup'] = $row['singleagegroup'];
    unset($row['id']);
    unset($row['malemale']);
    unset($row['maxfemale']);
    unset($row['maxteams']);
    unset($row['maxathletes']);
    unset($row['maxagegroup']);
    unset($row['uniqueevents']);
    unset($row['singleagegroup']);
    // correct the compid
    $row['ctc'] = $ctc;

    // get Any Comp Rules
    $row['compRules'] = $compObj->getRules();

    $row['id'] = $row['compId'];
    unset($row['compId']);

    $sql = "select sum(if(options like '%" . e4s_optionIsTeamEvent() . "%',1,0)) teamCount , sum(if(options not like '%" . e4s_optionIsTeamEvent() . "%',1,0)) indivCount 
            from " . E4S_TABLE_COMPEVENTS . '
            where compid = ' . $row['id'];

    $eventsResult = e4s_queryNoLog($sql);
    $indivTeamRow = $eventsResult->fetch_assoc();

    if ((int)$indivTeamRow['indivCount'] > 0) {
        $row['indivEvents'] = TRUE;
        $row['hasSecureEvents'] = $compObj->hasEventSecurity();
    } else {
        $row['hasSecureEvents'] = FALSE;
        $row['indivEvents'] = FALSE;
    }

    if ((int)$indivTeamRow['teamCount'] > 0) {
        $row['teamEvents'] = TRUE;
    } else {
        $row['teamEvents'] = FALSE;
    }
    $row['clubCompInfo'] = $compObj->getClubCompData();
    if ( $compObj->isClubcomp() ) {
        if ('{}' === e4s_getDataAsType($row['clubCompInfo'], E4S_OPTIONS_STRING)) {
            Entry4UIError(6700, 'You do not have access to enter this competitions.');
        }
    }
    $sendAGs = $agObj->getAgeGroups($row['id']);

    $row['compAgeGroups'] = $sendAGs;
    $data[] = $row;
}

Entry4UISuccess('
    "data": {
         "compDates" : ' . e4s_encode($data) . ',
         "systemTime" : "' . $useDate . '",
         "clubInfo" : "' . $clubLogo . '"
     }
');