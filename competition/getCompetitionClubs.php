<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'competition/commonComp.php';
include_once E4S_FULL_PATH . 'builder/compClubCRUD.php';

if (is_null($orgId)) {
    $rows = getCompsForUser(0, 0, FALSE, TRUE, FALSE);
} else {
    $model = new stdClass();
    $model->id = $orgId;
    $compOrg = e4s_readCompClub($model, FALSE);
    $data = new stdClass();
    $data->clubid = $compOrg['id'];
    $data->club = $compOrg['name'];
    Entry4UISuccess([$data]);
}

$distinct = array();
$data = array();
if (count($rows) > 0) {
    foreach ($rows as $row) {
        if (!array_key_exists($row['compOrgId'], $distinct)) {
            $insertrow = array();
            $insertrow['clubid'] = $row['compOrgId'];
            $insertrow['club'] = $row['club'];
            $data[] = $insertrow;
            $distinct[$row['compOrgId']] = TRUE;
        }
    }
} else {
    $row = array();
    $row['clubid'] = 0;
    $row['club'] = 'None Available';
    $data[] = $row;
}

// getUserPaging Info
$config = array();
if (!$public) {
    $config['paging'] = getUserPageSize();
}

Entry4UISuccess('
    "data":' . json_encode($data, JSON_NUMERIC_CHECK) . ',
    "meta":' . json_encode($config, JSON_NUMERIC_CHECK));
