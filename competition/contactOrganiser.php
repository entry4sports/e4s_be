<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_contactOrganiser($obj) {
    $compid = checkFieldFromParamsForXSS($obj, 'compId:Competition Indentifier');
    if ($compid === '' or is_null($compid)) {
        Entry4UIError(9307, 'No Competition reference passed to contact organiser', 200, '');
    }
    $email = checkFieldFromParamsForXSS($obj, 'email:From Email');
    if ($email === '' or is_null($email)) {
        Entry4UIError(9308, 'No email entered for contact organiser', 200, '');
    }
    $FOAE4S = checkFieldFromParamsForXSS($obj, 'foaE4s:For E4S');
    $contactCategory = checkFieldFromParamsForXSS($obj, 'category:Contact category');
    $body = checkFieldForXSS($obj, 'body:Contact Information');
    if (!is_null($body)) {
        $body = addslashes($body);
    }
    $compObj = e4s_GetCompObj($compid);
    $compObj->contactOrganiser($contactCategory, $body, $email, $FOAE4S);
}