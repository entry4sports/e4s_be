<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

function e4s_checkCompEntriesPof10($compId, $egId = 0, $aoCode = E4S_AOCODE_EA):void{
	$compObj = e4s_getCompObj($compId);
	if ( !$compObj->isOrganiser() ){
		Entry4UISuccess();
	}
	$entries = $compObj->getEntriesV2(0,E4S_ENTRY_PAID);
	$athleteIds = [];
	foreach ($entries as $entryEgId=>$egEntries){
		if ( $egId === 0 or $egId === $entryEgId) {
			foreach ( $egEntries as $entry ) {
				if ( $entry->aoCode === $aoCode and $entry->urn !== '' ) {
					if ( in_array( $entry->athleteId, $athleteIds ) === FALSE ) {
						$athleteIds[] = $entry->athleteId;
					}
				}
			}
		}
	}

	if ( !empty($athleteIds) ){

		$athleteIdsStr = implode(',', $athleteIds);
		$sql = 'select *
				from ' . E4S_TABLE_ATHLETE . '
				where id in (' . $athleteIdsStr . ')
				and urn is not null
				and aocode = "' . $aoCode . '"';
		$results = e4s_queryNoLog($sql);

		$athleteObj = new athleteClass();
		while ($athlete = $results->fetch_object(E4S_ATHLETE_OBJ)) {
			$athleteObj->addAthleteToCache($athlete);
			if ( $aoCode === E4S_AOCODE_EA) {
				$pof10Obj = new pof10V2Class( $athlete->URN, $athlete->id );
				$pof10Obj->updateDb();
			}
		}
	}
}

function e4s_getClubComps(){
    e4sCompetition::getClubComps();
}
function e4s_getCompMoreInfo($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $compObj = e4s_getCompObj($compId);
    $ags = $compObj->getAgeGroups();
    $retObj = new stdClass();
    $retObj->ageGroups = $ags;
	$retObj->text = notesClass::getNotes($compId);
    Entry4UISuccess($retObj);
}

function e4s_getShortCode($shortCode) {
    $sql = 'select id compId
            from ' . E4S_TABLE_COMPETITON . "
            where options like '%shortcode\":\"" . $shortCode . "\"%'";
    $result = e4s_queryNoLog($sql);
    $compId = 0;
    if ($result->num_rows === 1) {
        $obj = $result->fetch_object();
        $compId = (int)$obj->compId;
    }
    return $compId;
}

function e4s_markQualifiedEntries($obj) {
    $egArr = checkJSONObjForXSS($obj, 'egArr:Qualified athletes');
    $x = 1;
}

function e4s_updateCompSettings($obj){
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $compObj = e4s_GetCompObj($compId, TRUE);
    if ( !$compObj->isOrganiser()) {
        Entry4UIError(9330, 'You are not authorised to use this function');
    }
    $action = checkFieldForXSS($obj, 'action:Action');
    $val = checkFieldForXSS($obj, 'val:Action Value');
    $compObj->updateSettings($action, $val);
    Entry4UISuccess();
}
function e4s_getEventGroupEntries($obj, $incUnpaid = FALSE) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $egId = checkFieldForXSS($obj, 'egid:EventGroup ID' . E4S_CHECKTYPE_NUMERIC);
    if ( is_null($egId) ){
        $egId = 0;
    }
    $entityId = checkFieldForXSS($obj, 'entityId:Entity ID' . E4S_CHECKTYPE_NUMERIC);
    $entityLevel = checkFieldForXSS($obj, 'entityLevel: Entity Level' . E4S_CHECKTYPE_NUMERIC);
    if ( is_null($entityLevel) ){
        $entityLevel = 0;
    }
    if ( is_null($entityId) ){
        $entityId = 0;
    }
    $compObj = e4s_GetCompObj($compId, TRUE);

    Entry4UISuccess($compObj->getEntriesForEgId($egId, $entityLevel, $entityId, $incUnpaid));

}
function e4s_getCompInfo($obj){
    if ( e4s_getUserID() === E4S_USER_NOT_LOGGED_IN ){
        Entry4UIError(9050, 'Unauthorised Access');
    }
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $compObj = e4s_GetCompObj($compId, TRUE);
    $compObj->setClubComp(true);
    if ( $compObj->getClubId() !== 0 ) {
        $meta = new stdClass();
        $meta->clubCompInfo = $compObj->getClubCompData();
        Entry4UISuccess('"meta":' . json_encode($meta, JSON_NUMERIC_CHECK));
    }
    Entry4UISuccess();
}
function e4s_getSchedule($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $entries = checkFieldForXSS($obj, 'entries:Include entries');
    $getEntries = FALSE;
    if (!is_null($entries)) {
        if ($entries === TRUE or $entries === 'true' or $entries === '1') {
            $getEntries = TRUE;
        }
    }
    if (is_null($compId or (int)$compId < 1)) {
        Entry4UIError(3600, 'No competition found');
    }
    $compObj = e4s_GetCompObj($compId, TRUE);
    if (is_null($compObj->getRow())) {
        Entry4UIError(3601, 'No competition found');
    }
    $row = $compObj->getRow();

    $obj = new stdClass();
    $obj->compId = $compId;
    $obj->org = $compObj->getOrganisationObj();
    $obj->name = $row['Name'];
    $obj->date = $row['Date'];
    $obj->location = $row['location'];
    $obj->club = $row['compclub'];
    $obj->logo = $row['logo'];
    $options = $compObj->getOptions();
    $obj->autoEntries = null;
    if (isset($options->autoEntries)) {
        $obj->autoEntries = $options->autoEntries;
    }
    $obj->schedule = $compObj->getSchedule($getEntries);

    Entry4UISuccess($obj);
}

function getCompsForUser($orgid, $compId, $includeNotAuthorised, $entriesOpenOnly, $compDateSelection) {

    $userAreaIDs = array();
    $userLevels = array();
    $entitylevels = array();
    $authentication = '';
    $config = e4s_getConfig();
    $includeNotLive = FALSE;
    if (e4s_getUserID() > E4S_USER_NOT_LOGGED_IN) {
        $includeNotLive = TRUE;
        $daysPast = $config['userDaysPast'];
        if (isCountryUser($userLevels, $entitylevels)) {
            $daysPast = $config['adminDaysPast'];
        }

        $clubs = getUserClubs();
        foreach ($clubs as $club) {
            $clubid = (int)$club->areaid;
            if ($clubid !== 0 and !array_key_exists($clubid, $userAreaIDs)) {
                $userAreaIDs[$clubid] = $clubid;
                $userLevels[1] = 1;
            }
        }
        unset($clubs);

        $areas = getUserAreas();
        foreach ($areas as $area) {
            $areaid = (int)$area->areaid;
            $entitylevel = (int)$area->entitylevel;

            if ($areaid !== 0) {
                $userAreaIDs[$areaid] = $areaid;
                $userLevels[$entitylevel] = $entitylevel;
            }
        }
        unset($areas);
        if (!empty($userAreaIDs)) {
            $userAreaIDs = getAllParentIDs($userAreaIDs);
        }
    } else {
        $daysPast = $config['userDaysPast'];
        $authentication = ' Please login to check/gain Access';
    }

    $sql = 'select *
            from ' . E4S_TABLE_ENTITY;
    $result = e4s_queryNoLog($sql);
    $entityRows = $result->fetch_all(MYSQLI_ASSOC);
    foreach ($entityRows as $entityRow) {
        $entitylevels [$entityRow['name']] = (int)$entityRow['level'];
    }
    $entityRows = null;
    $result = null;

    if ($compDateSelection) {
        $sql = getCompetitionDatesSelect($orgid, $compId);
    } else {
        $sql = getCompetitionForHomeScreen($daysPast);
    }
//e4s_addDebugForce($sql);
    $result = e4s_queryNoLog('getCompsForUser' . E4S_SQL_DELIM . $sql);

    $returnRows = $result->fetch_all(MYSQLI_ASSOC);
	$compIds = [];
	foreach($returnRows as $row){
		$compIds[(int)$row['compId']] = (int)$row['compId'];
	}
	$GLOBALS[E4S_PROCESSING_HOMEPAGE_COMPS][E4S_PROCESSING_COMPS_IDS] = $compIds;
    foreach($returnRows as $row){
        $addComp = TRUE;
        $row['compId'] = (int)$row['compId'];
        $row['compOrgId'] = (int)$row['compOrgId'];
        $compObj = e4s_getCompObj($row['compId'], true, false);
        if ( is_null($compObj)){
            continue;
        }
        $options = $compObj->getOptions();
        $isOrganiser = $compObj->isOrganiser();
        $options->organiser = $isOrganiser;

        // If on home screen, if active show irrespective of disabled
        if (!$compDateSelection and $compObj->isActive()) {
            $addComp = TRUE;
        } else {
            if (!$compObj->isActive() or $options->disabled) {
                $addComp = FALSE;
                if ($includeNotLive and $isOrganiser) {
                    // live is false but they are an organiser of this event
                    $addComp = TRUE;
                } elseif (isE4SUser()) {
                    $addComp = TRUE;
                }
            }
        }

        if ($entriesOpenOnly and !$isOrganiser) {
            if ($compObj->haveEntriesClosed()) {
                $addComp = FALSE;
            }
        }
        if (array_key_exists('visibledate', $row)) {
            if ($row['compDate'] < $row['visibledate']) {
                if (!$isOrganiser and !$compObj->areResultsAvailable()) {
                    $addComp = FALSE;
                }
            }
        }

        if (!$addComp) {
            continue;
        }
        // check area and user access
        $row['areaid'] = (int)$row['areaid'];

        if (array_key_exists('areaname', $row)) {
            $availableToAccess = 'Available to ' . $row['areaname'] . ' users only.';
            if (isset($options->access)) {
                $availableToAccess = 'Available to ' . $row['areaname'] . '/' . $options->access . 's only.';
                if ($options->access === 'County') {
                    $availableToAccess = 'Available to ' . $row['areaname'] . '/' . 'Counties only.';
                }
            }
        } else {
            $availableToAccess = '';
        }

        // TODO Clubs ????
        if ($row['areaid'] !== 0 and !isE4SUser() and !isCountryUser($userLevels, $entitylevels)) {
            if (!array_key_exists($row['areaid'], $userAreaIDs)) {
                // The comp is an area comp and user is not in that area
                $options->live = $availableToAccess . $authentication . '.';
                if (!$includeNotAuthorised) {
                    $addComp = FALSE;
                }
            } else {
                // Comp area specified and user is in the area, so now check access
                if (isset($options->access)) {
                    if (array_key_exists($options->access, $entitylevels)) {
                        $entitylevel = $entitylevels [$options->access];
                        if (!array_key_exists($entitylevel, $userLevels)) {
                            $options->live = $availableToAccess . $authentication . '.';
                            if (!$includeNotAuthorised) {
                                $addComp = FALSE;
                            }
                        }
                    } else {
                        $options->live = $availableToAccess . $authentication . '..';

                        if (!$includeNotAuthorised) {
                            $addComp = FALSE;
                        }
                    }
                }
            }
        }

        if ($addComp) {
            $row['options'] = $options;
            $returnRows[] = $row;
        }
    }

    return $returnRows;
}

function getCompetitionDatesSelect($orgid, $compId) {
    $sqlSelect = "select DATE_FORMAT(date,'" . ISO_MYSQL_DATE . "') as date,
                    curdate() today,
                    now() systemtime,
                    date compDate,
                    date visibledate,
                    lastentrymod,
                    comp.id compId,
                    locationid,
                    name compName,
                    link,
                    yearFactor,
                    comp.compclubid compOrgId,
                    DATE_FORMAT(entriesClose,'" . ISO_MYSQL_DATE . "') as entriesClose,
                    comp.options,
                    cc.logo logo,
                    comp.areaid,
                    teamid,
                    ctc.id ctcid,
                    ctc.maxathletes,
                    ctc.maxteams,
                    ctc.maxmale,
                    ctc.maxfemale,
                    ctc.maxagegroup,
                    ctc.uniqueevents,
                    ctc.singleagegroup
                from  " . E4S_TABLE_COMPETITON . ' comp left join ' . E4S_TABLE_COMPTEAMCONFIG . ' ctc on ctc.areaid = comp.areaid,
                    ' . E4S_TABLE_COMPCLUB . " cc
                where compclubid = $orgid
                and comp.id > 0
                and   cc.id = compclubid";
    if ($compId > 0) {
        $sqlSelect .= '
                    and comp.id = ' . $compId . ' 
               ';
    }
//    $organiser = userHasPermission(E4S_ROLE_ADMIN, $orgid);

    $sqlSelect .= e4s_getCompSelectdateRange();

    $sqlSelect .= ' order by comp.name, comp.date asc, comp.id';
//    order by date asc, entriesclose asc, comp.id asc";

    return $sqlSelect;
}

// used by v1
function getCompetitionForHomeScreen($daysPast) {
    $sqlSelect = "select comp.id compId,
                    curdate() today,
                    now() systemtime,
                    date compDate,
                    DATE_ADD(CURRENT_DATE, INTERVAL -{$daysPast} DAY) visibledate,
                    lastentrymod,
                    if(curdate()<date,'1','0') status,
                    date,
                    comp.compclubid compOrgId,
                    comp.options,
                    comp.name compName,
                    comp.locationid,
                    date_format(entriesClose,'" . ISO_MYSQL_DATE . "') closedate,
                    date_format(entriesOpen,'" . ISO_MYSQL_DATE . "') opendate,
                    link,
                    club.logo,
                    club,
                    location,
                    comp.areaid,
                    area.entityid,
                    area.name areaname
        from " . E4S_TABLE_COMPCLUB . ' club
           , ' . E4S_TABLE_LOCATION . ' loc
           , ' . E4S_TABLE_COMPETITON . ' comp
           left join ' . E4S_TABLE_AREA . ' area on area.id = comp.areaid
           where comp.locationid = loc.id
           and comp.id > 0
           and club.id = comp.compclubid 
           and comp.date >= CURRENT_DATE';

    if (!isE4SUser()) {
        $userId = e4s_getUserID();
        if ( $userId <= E4S_USER_NOT_LOGGED_IN ){
            $sqlSelect .= ' and ' . e4s_sqlForGetComps();
        }else {
            $e4sUserObj = e4sUserClass::withId($userId);
            $adminPerms = $e4sUserObj->getAdminPerms();

			if ( sizeof( $adminPerms ) === 0 ) {
				$sqlSelect .= ' and ' . e4s_sqlForGetComps();
			} else {
				$compSpecificIds = [];
				$orgAdminIds     = [];

				foreach ( $adminPerms as $perm ) {
					if ( $perm->comp->id === 0 ) {
						$orgAdminIds[] = $perm->org->id;
					} else {
						$compSpecificIds[] = $perm->comp->id;
					}
				}
				$hasSpecificComps = sizeof( $compSpecificIds ) > 0;
				$hasOrgs          = sizeof( $orgAdminIds ) > 0;
				$sqlSelect        .= ' and ( ';
				$sqlSelect        .= e4s_sqlForGetComps(false);
				if ( $hasSpecificComps or $hasOrgs ) {
					$sqlSelect .= ' or ';
					$sqlSelect .= ' ( ';
					if ( $hasSpecificComps ) {
						$sqlSelect .= '   comp.id in (' . implode( ',', $compSpecificIds ) . ') ';
					}
					if ( $hasSpecificComps and $hasOrgs ) {
						$sqlSelect .= '   or ';
					}
					if ( $hasOrgs ) {
						$sqlSelect .= '(   comp.compclubid in (' . implode( ',', $orgAdminIds ) . ') ';
						$sqlSelect .= ' and date_sub(CURRENT_DATE, INTERVAL 18 month) < comp.date ) ';
					}
					$sqlSelect .= ' ) ';
				}
				$sqlSelect .= ') ';
			}
        }
    }else{
		$sqlSelect .= ' and date_sub(CURRENT_DATE, INTERVAL 1 year) < comp.date ';
    }

    $sqlSelect .= ' order by comp.date desc,comp.name, comp.id';
    return $sqlSelect;
}

function e4s_sqlForGetComps($withResults = true):string{
	if (isE4SUser()) {
		return ' ';
	}
	$sqlSelect = ' (';
	$sqlSelect .= 'comp.active = 1 ';
	$sqlSelect .= ' and (' ;
	$sqlSelect .= ' CURRENT_DATE < comp.date ';
	if ($withResults ){
		$sqlSelect .= ' or ';
		$sqlSelect .= " (resultsAvailable = 1 and date_sub(CURRENT_DATE, INTERVAL 1 year) < comp.date)";
	}
	$sqlSelect .= ')';
	$sqlSelect .= ') ';
	return $sqlSelect;
}
function e4s_getCompSelectdateRange() {
	$isOrganiser = false;
    $sqlSelect = '';
    if (!isE4SUser()) {
        $sqlSelect = ' and (' . returnNow();
        $sqlSelect .= ' between EntriesOpen and DATE_ADD(comp.date, INTERVAL 1 DAY) ';
        $sqlSelect .= ' or ';
        $sqlSelect .= " resultsAvailable = 1 ";
        $sqlSelect .= ')';
    }
    $sqlSelect .= ' and ' . returnNow() . ' <= DATE_ADD(comp.date, INTERVAL 1 MONTH) ';
    return $sqlSelect;
}