<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
$gender = strtoupper($gender);
$sql = 'select *
        from ' . E4S_TABLE_COMPTEAM . '
        where compid = ' . $compId . "
        and   name = '" . $teamname . "'";

$result = e4s_queryNoLog($sql);
if ($result->num_rows > 0) {
    $teamRow = $result->fetch_assoc();
    if ($teamRow['gender'] === $gender) {
        Entry4UIError(1001, 'Duplicate team name with different gender', 412, '');
    }
    Entry4UIError(1002, 'Duplicate team name', 412, '');
}

// CTC can be for a specific area. 0 means for all areas not specifically defined
// so get the area and 0. order in desc and take first record. this allows for no area specific record
$ctcSql = 'Select ctc.*
        from ' . E4S_TABLE_COMPTEAM . 'Config ctc,
             ' . E4S_TABLE_COMPETITON . ' c 
        where c.id = ' . $compId . '
        and   ctc.id = c.teamid
        and   ctc.areaid in (0,' . $areaid . ')
        order by ctc.areaid desc
        ';

$ctcresult = e4s_queryNoLog($ctcSql);
if ($ctcresult->num_rows < 1) {
    Entry4UIError(1003, 'No Team configuration information found', 400, '');
}
if ($ctcresult->num_rows > 2) {
    Entry4UIError(1003, 'Too many Team configurations found for area', 400, '');
}
// get first record
$ctcRow = $ctcresult->fetch_assoc();
// check MaxCount
$sql = 'select count(*) count
        from ' . E4S_TABLE_COMPTEAM . '
        where compid = ' . $compId . '
        and   areaid = ' . $areaid;
$compTeamResult = e4s_queryNoLog($sql);
$compTeamRow = $compTeamResult->fetch_assoc();
if ($compTeamRow['count'] >= $ctcRow['maxteams']) {
    Entry4UIError(1004, 'Maximum teams exist for this area', 400, '');
}

$sql = 'select count(*) count
        from ' . E4S_TABLE_COMPTEAM . '
        where compid = ' . $compId . '
        and   areaid = ' . $areaid . "
        and   gender = '" . $gender . "'";
$compTeamResult = e4s_queryNoLog($sql);
$compTeamRow = $compTeamResult->fetch_assoc();
$maxField = 'male';
if ($gender === 'F') {
    $maxField = 'fe' . $maxField;
}
$maxField = 'max' . $maxField;

if ($compTeamRow['count'] >= $ctcRow[$maxField]) {
    Entry4UIError(1005, 'Maximum teams exist for this gender', 400, '');
}

$insertSql = 'Insert into ' . E4S_TABLE_COMPTEAM . ' ( compid, areaid, gender,name)
              values (' . $compId . ',' . $areaid . ",'" . $gender . "','" . $teamname . "')";

$result = e4s_queryNoLog($insertSql);
Entry4UISuccess('');

