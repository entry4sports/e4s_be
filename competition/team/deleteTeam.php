<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
if ($teamid === '') {
    Entry4UIError(1000, 'No team passed. ( teamId )', 400, '');
}
// check Entries
$sql = 'Select count(*) count
        from ' . E4S_PREFIX . 'Entries
        where teamid = ' . $teamid;
$result = e4s_queryNoLog($sql);
$row = $result->fetch_assoc();
if ($row['count'] > 0) {
//    addDebug("Entries:" . $row['count']);
    Entry4UIError(1001, 'Entries exist for athletes of this team.', 400, '');
}
$sql = 'delete from ' . E4S_PREFIX . 'CompTeam
        where id = ' . $teamid;

$result = e4s_queryNoLog($sql);
Entry4UISuccess('');
