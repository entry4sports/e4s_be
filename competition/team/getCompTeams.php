<?php
/**
 * Created by PhpStorm.
 * User: pauld
 * Date: 04/12/2018
 * Time: 21:10
 */

include_once E4S_FULL_PATH . 'dbInfo.php';

//$sql = "select *
//            from " . E4S_TABLE_COMPETITON ."
//            where id = " . $compid;
//$result = mysqli_query($conn, $sql);
//if ($result->num_rows < 1){
//    Entry4UIError(1002,"Can not find competition " . $compid, 404, '');
//}
//$row = $result->fetch_assoc();
$compObj = e4s_getCompObj($compId);
$row = $compObj->getRow();
if ($row['teamid'] === '0') {
    $teams = array();
} else {
    $teams = getUserAreasForComp($compId);
}
//addDebug($teams);
foreach ($teams as $key => $value) {
    $sql = 'Select ct.id teamId, ct.name teamName, ct.gender
            from ' . E4S_TABLE_COMPTEAM . ' ct,
                 ' . E4S_TABLE_AREA . ' a
            where ct.compid = ' . $compId . '
            and   ct.areaid = ' . $value['areaId'] . '
            and   ct.areaid = a.id';

    $result = e4s_queryNoLog($sql);
    $teamRows = mysqli_fetch_all($result, MYSQLI_ASSOC);
    $teams[$key]['teams'] = $teamRows;
}

Entry4UISuccess('
    "data":' . json_encode($teams, JSON_NUMERIC_CHECK));

function getUserAreasForComp($compid) {

    $newArray = array();
    $userRow = getUserArea();
    $compRow = getCompArea($compid);

    if (sizeof($compRow) === 0) {
        return $newArray;
    }
    $sql = 'select a.id areaId, a.name areaName, a.shortname shortName
                from ' . E4S_TABLE_AREA . ' a ';

    if ($userRow['entitylevel'] > $compRow['entitylevel']) {
        // get all areas below compRow
        $sql .= 'where parentid = ' . $compRow['areaid'];
    } elseif ($userRow['areaid'] == $compRow['areaid']) {
        // get all areas below either as they are the same
        $sql .= 'where parentid = ' . $compRow['areaid'];
    } else {
        // ensure user area is within compArea ( Should be else comp should not be in their list

        $returnArray = array();
        $newArray['areaId'] = $userRow['areaid'];
        $newArray['areaName'] = $userRow['areaname'];
        $newArray['shortName'] = $userRow['areashortname'];
        $returnArray[] = $newArray;
        return $returnArray;
    }

    $result = e4s_queryNoLog($sql);
    $returnArray = mysqli_fetch_all($result, MYSQLI_ASSOC);
    return $returnArray;
}