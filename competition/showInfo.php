<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
require_once E4S_FULL_PATH . 'dbInfo.php';
if (!isset($_GET['compid'])) {
    return;
}
$compid = $_GET['compid'];
if (is_null($compid) or $compid === '' or $compid === 'undefined') {
    exit();
}
$sql = 'select c.date as eventdate, c.indooroutdoor, c.name as eventname, c.entriesopen, c.entriesclose, c.link, 
        l.location, l.postcode, l.address1, l.address2, l.town, l.county, l.website, l.directions, l.map, l.contact, c.information
        from ' . E4S_TABLE_COMPETITON . ' as c, ' . E4S_TABLE_LOCATION . " as l
        where c.id = $compid
        and c.locationid = l.id";

$result = mysqli_query($conn, $sql);
if ($result->num_rows !== 1) {
    Entry4UISuccess();
}
$compInfoRow = mysqli_fetch_array($result, TRUE);
echo '<style>';
echo '.SectionTitle{
font-weight:800;
font-size:20px;
background-color: #efefef;
border-bottom-style: ridge;
}
.LocationName{
font-weight:800;
font-size:20px;
}
';
echo '</style>';

echo "<table style='height:800px'>";

$map = $compInfoRow['map'];

echo '<tr>';
//echo "<td rowspan='4'>";
//include_once E4S_FULL_PATH . 'competition/schedule.php';
//echo "</td>";
//echo "</tr>";
echo "<td style='vertical-align: top;'>";
$eventDate = date('jS F Y', strtotime($compInfoRow['eventdate']));
$eventopen = date('jS F Y h:ia', strtotime($compInfoRow['entriesopen']));
$eventclose = date('jS F Y h:ia', strtotime($compInfoRow['entriesclose']));

echo "<p class='SectionTitle'>Location</p>";
$location = $compInfoRow['location'];
echo "<p class='LocationName'>" . $location . '</p>';
echo "<span class='Address'>";
displayIfNotNull2($compInfoRow['address1']);
displayIfNotNull2($compInfoRow['address2']);
displayIfNotNull2($compInfoRow['town']);
displayIfNotNull2($compInfoRow['county']);
displayIfNotNull2($compInfoRow['postcode']);
echo '</span>';
$txt = $compInfoRow['contact'];
if ($txt !== '' and $txt !== null) {
    echo "<div class='Contact'>$txt</div>";
}
$txt = $compInfoRow['website'];
if ($txt !== '' and $txt !== null) {
    echo "<br><a target='Entry4Compsite' href='$txt'>" . $location . "'s Website</a>";
}
echo '</td>';
echo '</tr>';
$txt = $compInfoRow['directions'];

if (($txt !== '' and $txt !== null) || ($map !== null and $map !== '')) {
    echo '<tr>';
    echo '<td>';
    echo "<p class='SectionTitle'>Directions</p>";
    echo '</td>';
    echo '</tr>';
}
if ($txt !== '' and $txt !== null) {
    echo '<tr>';
    echo '<td>';
    echo '<div >' . $txt . '</div>';
    echo '</td>';
    echo '</tr>';
}

if ($map !== null and $map !== '') {
    echo '<tr>';
    echo "<td style='width:50%;vertical-align: top;'>";
    echo '<iframe src=' . $map . " width='100%' height='400' frameborder='0' style='border:0' allowfullscreen></iframe>";
    echo '</td>';
    echo '</tr>';
}
echo '<tr>';
echo "<td style='vertical-align: top;'>";

$compInfo = $compInfoRow['information'];
if ($compInfo !== '') {
    echo "<br><p class='SectionTitle'>Competition Information</p>" . $compInfo;
}
echo '</td>';
echo '</tr>';

echo '</table>';

function displayIfNotNull2($txt) {
    if ($txt !== '' and $txt !== null) {
        echo $txt . '<br>';
    }
}