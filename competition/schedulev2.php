<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
if (!isset($_GET['compid'])) {
    return;
}
$compid = $_GET['compid'];

$sql = "SELECT distinct(date_format(startdate,'%d %m %y')) FROM " . E4S_TABLE_COMPEVENTS . " where compid = $compid";
$result = mysqli_query($conn, $sql);
$multiDates = FALSE;
if ($result->num_rows > 1) {
    $multiDates = TRUE;
}
$sql = 'select * from ' . E4S_TABLE_AGEGROUPS;
$result = e4s_queryNoLog($sql);
$ageGroupDefRows = $result->fetch_all(MYSQLI_ASSOC);
$ageGroupDefs = array();
foreach ($ageGroupDefRows as $ageGroupDefRow) {
    $ageGroupDefs[$ageGroupDefRow['id']] = $ageGroupDefRow;
}

$sql = "SELECT ce.id ceid, 
       ce.maxathletes, 
       date_format(eg.startdate,'%d/%m/%Y') as startdate,
       date_format(eg.startdate,'%a, %D %b') as startdate2,
       date_format(eg.startdate,'%H:%i') as starttime, 
       ce.split, ce.agegroupid, 
       e.name as event,
       eg.name as eventGroupName,
       e.gender, 
       ce.maxGroup, 
       ce.options
FROM " . E4S_TABLE_COMPEVENTS . ' ce,
     ' . E4S_TABLE_EVENTS . ' e,
     ' . E4S_TABLE_EVENTGROUPS . " eg
where compid = $compid
and e.id = ce.eventid
and eg.id = ce.maxgroup
and ce.maxgroup != ''
order by eg.startdate, e.name, eventid, split";

$result = mysqli_query($conn, $sql);

$eventSaved = '';
$timeSaved = '';
$splitSaved = '';
$allData = array();
$allAgeGroups = array();
//
echo '<link rel="stylesheet" href="' . E4S_PATH . '/competition/css/competitionDialog.css">';
echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>';
echo '<script>';
echo 'function Entry4scrollTo(selectObj){';
echo '    if (selectObj.selectedIndex == 0) return;';
echo '    let loc = selectObj.options[selectObj.selectedIndex].value;';
echo "    let obj = jQuery('#showInfoDialog');";
echo '    obj.animate({ ';
echo "        scrollTop:  obj.scrollTop() - obj.offset().top + jQuery('#' + loc).offset().top ";
echo '    }, 1000);';
echo '}';
echo '</script><br>';
echo "<div style='height:5%; width:100%'>";
//echo "Jump to : <select style='width:15%;' id='eventSelect' onchange=\"Entry4scrollTo(this)\">";
//echo "<option value=''>Select Event</option>";
while ($row = mysqli_fetch_array($result, TRUE)) {
    $ageGroupRow = $ageGroupDefs[$row['agegroupid']];
    $row['maxage'] = $ageGroupRow['MaxAge'];
    $row['agegroup'] = $ageGroupRow['Name'];
    $row['agegroupid'] = (int)$row['agegroupid'];
    $row['upscalefromid'] = (float)0;
//    $allAgeGroups[str_pad($ageGroupRow['MaxAge'],3,"0",STR_PAD_LEFT) .'-'.$ageGroupRow['Name']] ='';

//    echo "Array Key = " . str_pad($row['maxage'],3,"0",STR_PAD_LEFT) .'-'.$row['agegroup'] . "<br>";
    $allAgeGroups[str_pad($row['maxage'], 3, '0', STR_PAD_LEFT) . '-' . $row['agegroup']] = '';
    $allData[$row['ceid']] = $row;

    // Check for upscaling
    if ($row['options'] !== '') {
        $options = json_decode($row['options'], FALSE);
        if (isset($options->ageGroups)) {
            foreach ($options->ageGroups as $ageGroup) {
                $ageGroupRow = $ageGroupDefs[$ageGroup->ageGroup];
                $allAgeGroups[str_pad($ageGroupRow['MaxAge'], 3, '0', STR_PAD_LEFT) . '-' . $ageGroupRow['Name']] = '';
                $newRow = $row;
                $newRow['upscalefromid'] = (float)$row['ceid'];
                $newRow['agegroupid'] = $ageGroup->ageGroup;
                $newRow['agegroup'] = $ageGroupRow['Name'];
                $allData[$row['ceid'] . $newRow['agegroupid']] = $newRow;
            }
        }
    }
}

$rowData = array();
ksort($allAgeGroups);
$html = '';

foreach ($allData as $rowkey => $row) {
    if ($row['event'] !== $eventSaved || $row['starttime'] !== $timeSaved || $row['split'] !== $splitSaved) {
        // New Event
        if ($eventSaved !== '') {
            $html .= outputAgeEventData($rowData);
        }
        $starttime = $row['starttime'];
        if ($row['starttime'] == '00:00') {
            $starttime = E4S_NO_TIME;
        }
        $key = '';
        if ($multiDates) {
            $key = $row['startdate'] . ' ';
        }
        $key .= $starttime . ' - ' . $row['event'];
        $split = '';
        if ($row['split'] !== '0') {
            $lg = '>';
            $s = $row['split'];
            if ((int)$s < 0) {
                $lg = '<';
                $s = str_replace('-', '', $s);
            }
            $split = ' (' . $lg . $s . ')';
        }
        $keyValue = str_replace(' ', '', $key);
        $keyValue = str_replace(':', '', $keyValue);
        $keyValue = str_replace('/', '', $keyValue);
        $keyChoice = $key . ' ' . $split;
        $keyDisplay = $row['eventGroupName'] . ' ' . $split;
//        var_dump($row);

//        echo "<option value='" . $keyValue ."'>" . $keyChoice . "</option>";
        $html .= "<table name='" . $keyValue . "' id='" . $keyValue . "' class='fullInfoTable fullInfoTableTitle'>";
        $html .= "<tr class='fullInfoTitleRow'>";

        $html .= "<td class='fullInfo fullTitle fullInfoLabel'>Time</td><td class='fullInfo fullTitle fullInfoData'>";
        if ($multiDates) {
            $html .= $row['startdate2'] . ' ';
        }
        $html .= $starttime . '</td>';
        $html .= "<td class='fullInfo fullTitle fullInfoLabel'>Event</td><td class='fullInfo fullTitle fullInfoData'>" . $keyDisplay . '</td>';

        $countSQL = 'Select count(*) as entries from ' . E4S_PREFIX . 'Entries 
                     where paid = 1 
                     and compeventid in (
                            select id from ' . E4S_PREFIX . "CompEvents where compid = $compid and maxgroup = '" . $row['maxGroup'] . "')";
        $result = e4s_queryNoLog($countSQL);
        $countResult = $result->fetch_assoc();

        if ($row['maxathletes'] > 0) {
            $html .= "<td class='fullInfo fullTitle fullInfoLabel'>Num/Max Entries</td><td class='fullInfo fullTitle fullInfoData'>" . $countResult['entries'] . '/' . $row['maxathletes'] . '</td>';
        } else {
            if ($row['maxathletes'] === 0) {
                $html .= "<td class='fullInfo fullTitle fullInfoLabel'>Num Entries</td><td class='fullInfo fullTitle fullInfoData'>" . $countResult['entries'] . '</td>';
            } else {
                $html .= "<td class='fullInfo fullTitle fullInfoLabel'>Finals</td><td class='fullInfo fullTitle fullInfoData'>&nbsp;</td>";
            }
        }
        $html .= '</tr>';
        $html .= '</table>';
        $eventSaved = $row['event'];
        $timeSaved = $row['starttime'];
        $splitSaved = $row['split'];
        $rowData = $allAgeGroups;
    }
    $rowData[str_pad($row['maxage'], 3, '0', STR_PAD_LEFT) . '-' . $row['agegroup']] .= $row['gender'];
    // Check for upscaling
    if ((float)$row['upscalefromid'] > 0 and (float)$rowkey !== (float)$row['ceid']) {
        echo 'RowKey = ' . $rowkey . '<br>';
        echo 'CEID = ' . $row['ceid'] . '<br>';
        $rowData[str_pad($row['maxage'], 3, '0', STR_PAD_LEFT) . '-' . $row['agegroup']] .= $allData[$row['upscalefromid']]['gender'];
        echo '<br>';
        exit();
    }

}
echo '</select>';
echo '</div>';
echo "<div id='showInfoDialog' style='height:100%; width:100%; overflow:wrap'>";
echo $html;
echo outputAgeEventData($rowData);
echo '</div>';


function outputAgeEventData($rowData) {
    $retHTML = '';
    $retHTML .= "<table class='fullInfoTable fullInfoTableData'>";
    $retHTML .= "<tr class='fullInfoAgeRow'>";
    $retHTML .= "<td class='fullInfo fullInfoGender'>&nbsp</td>";
    foreach ($rowData as $key => $value) {
        $keyArr = explode('-', $key);
        $age = str_replace('Under', 'U', $keyArr[1]);
        $retHTML .= "<td class='fullInfo fullData fullInfoAgeLabel'>" . $age . '</td>';
    }
    $retHTML .= '</tr>';
    $retHTML .= "<tr class='fullInfoGenderRow'>";
    $retHTML .= "<td class='fullInfo fullData fullInfoLabel'>Female</td>";
    foreach ($rowData as $key => $value) {
        $retHTML .= "<td class='fullInfo fullData fullInfoTick'>";
        if (strpos($value, 'F') !== FALSE) {
            $retHTML .= '&#10003;';
        }
        $retHTML .= '</td>';
    }
    $retHTML .= '</tr>';
    $retHTML .= "<tr class='fullInfoGenderRow'>";
    $retHTML .= "<td class='fullInfo fullData fullInfoLabel'>Male</td>";
    foreach ($rowData as $key => $value) {
        $retHTML .= "<td class='fullInfo fullData fullInfoTick'>";
        if (strpos($value, 'M') !== FALSE) {
            $retHTML .= '&#10003;';
        }
        $retHTML .= '</td>';
    }
    $retHTML .= '</tr>';
    $retHTML .= '</table>';

    return $retHTML;
}

?>