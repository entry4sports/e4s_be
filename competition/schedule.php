<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';

if (!isset($_GET['compid'])) {
    return;
}
$compid = (int)$_GET['compid'];
$compObj = e4s_GetCompObj($compid);
$userid = e4s_getUserID();
$public = '0';
if ((int)$userid > 0) {
    $public = '1';
}

$sql = "SELECT distinct(date_format(startdate,'%d %m %y')) FROM " . E4S_TABLE_COMPEVENTS . " where compid = $compid";

$result = mysqli_query($conn, $sql);
$multiDates = FALSE;
if ($result->num_rows > 1) {
    $multiDates = TRUE;
}
$sql = 'select * from ' . E4S_TABLE_AGEGROUPS;
$result = e4s_queryNoLog($sql);
$ageGroupDefRows = $result->fetch_all(MYSQLI_ASSOC);
$ageGroupDefs = array();
foreach ($ageGroupDefRows as $ageGroupDefRow) {
    $ageGroupDefs[$ageGroupDefRow['id']] = $ageGroupDefRow;
}

$sql = "SELECT ce.id ceid, 
       ce.maxathletes, 
       date_format(eg.startdate,'%d/%m/%Y') as startdate,
       date_format(eg.startdate,'%a, %D %b') as startdate2,
       date_format(eg.startdate,'%H:%i') as starttime, 
       ce.split, ce.agegroupid, 
       e.name as event, 
       eg.name as eventGroupName, 
       eg.typeNo,
       eg.options egOptions,
       e.gender gender, 
       ce.maxGroup, 
       e.options as eoptions, 
       ce.options
FROM " . E4S_TABLE_COMPEVENTS . ' ce,
     ' . E4S_TABLE_EVENTS . ' e,
     ' . E4S_TABLE_EVENTGROUPS . " eg
where ce.compid = $compid
and e.id = ce.eventid
and ce.maxgroup = eg.id
order by eg.startdate, eg.typeno, eg.name, eventid, split";

$result = e4s_queryNoLog($sql);
if ($result->num_rows === 0) {
    exit('Competition has no events setup.');
}
$eventSaved = '';
$timeSaved = '';
$splitSaved = '';
$allData = array();
$allAgeGroups = array();
$config = e4s_getConfig();
$env = strtoupper($config['env']);
if ($env === 'PROD') {
    $env = '';
} else {
    $env .= ' ';
}

$config = e4s_getConfig();
$types = [];
while ($row = mysqli_fetch_array($result, TRUE)) {
    $egOptions = e4s_addDefaultEventGroupOptions($row['egOptions']);
    if ( $egOptions->cardDisplay === false){
        continue;
    }
    $newRow = $row;
	$newRow['heatCount'] = "";

    $ageGroupRow = $ageGroupDefs[$row['agegroupid']];
    $newRow['maxage'] = $ageGroupRow['MaxAge'];
    $newRow['agegroup'] = e4s_getVetDisplay($ageGroupRow['Name'], 'O ');
    $newRow['agegroupid'] = (int)$row['agegroupid'];
    $newRow['upscalefromid'] = (float)0;
    $newRow['eventTeam'] = '0';
    $newRow['type'] = $row['typeNo'][0];
    $types[$newRow['type']] = $newRow['type'];
    $allAgeGroups[str_pad($newRow['maxage'], 3, '0', STR_PAD_LEFT) . '-' . $newRow['agegroup']] = '';

    $options = new stdClass();

    if ($newRow['eoptions'] !== '') {
        $options = json_decode($newRow['eoptions'], FALSE);
    }
    if ($egOptions->isTeamEvent) {
        $newRow['eventTeam'] = '1';
    }
    if ($newRow['options'] !== '') {
        $options = json_decode($newRow['options'], FALSE);
    }
    if (isset($options->eventTeam)) {
        // Using the new team option
        if (isset($options->eventTeam->teamPositionLabel)) {
            $newRow['eventTeam'] = '1';
        }
    }

    $allData[$row['ceid']] = $newRow;

    if (isset($options->ageGroups)) {
        foreach ($options->ageGroups as $ageGroup) {
            if (isset($ageGroup->ageGroup)) {
                $ageGroupRow = $ageGroupDefs[$ageGroup->ageGroup];
                $allAgeGroups[str_pad($ageGroupRow['MaxAge'], 3, '0', STR_PAD_LEFT) . '-' . $ageGroupRow['Name']] = '';
                $newRow2 = $newRow;
                $newRow2['upscalefromid'] = (float)$row['ceid'];
                $newRow2['agegroupid'] = $ageGroup->ageGroup;
                $newRow2['agegroup'] = $ageGroupRow['Name'];
                $newRow2['maxage'] = $ageGroupRow['MaxAge'];
                $allData[$newRow['ceid'] . $newRow2['agegroupid']] = $newRow2;
            }
        }
    }
}

$rowData = array();
ksort($allAgeGroups);
$html = '';
$dates = [];
$keyValue = '';
$hasScheduleOnly = false;

foreach ($allData as $rowkey => $row) {
    if (isReserveEvent($row['eventGroupName'] ) ){
        continue;
    }
	$row['maxathletes'] = (int)$row['maxathletes'];
    if ($row['eventGroupName'] !== $eventSaved || $row['starttime'] !== $timeSaved || $row['split'] !== $splitSaved) {
        if ( $eventSaved !== ''){
            $html .= outputAgeEventData($rowData, $row, $keyValue);
        }
        $starttime = $row['starttime'];
        if ($row['starttime'] == '00:00') {
            $starttime = E4S_NO_TIME;
        }
        $key = '';
        if ($multiDates) {
            $key = $row['startdate'] . ' ';
        }
        $key .= $starttime . ' - ' . $row['event'];
        $split = '';

        if ($row['split'] !== '0') {
            $lg = '>';
            $s = $row['split'];
            if ((int)$s < 0) {
                $lg = '<';
                $s = str_replace('-', '', $s);
            }
            $split = ' (' . $lg . $s . ')';
        }
        $keyValue = str_replace(' ', '', $key);
        $keyValue = str_replace(':', '', $keyValue);
        $keyValue = str_replace('/', '', $keyValue);

        $keyChoice = $key . ' ' . $split;
        $keyDisplay = '<span class="e4s_event_type_span">' . $row['typeNo'] . ' : </span>' . $row['eventGroupName'] . ' ' . $split;
        $html .= "<div class='";
        if ( $row['maxathletes'] < 0 ){
            $html .= "scheduleOnly";
            $hasScheduleOnly = true;
        }
        $html .= "'>";
        $html .= "<table name='" . $keyValue . "' id='" . $keyValue . "' class='fullInfoTable fullInfoTableTitle eventType eventType_" . $row['type'] . "'>";
        $html .= "<tr class='fullInfoTitleRow'>";

//        $html .= "<td class='fullInfo fullTitle fullInfoLabel'></td><td class='fullInfo fullTitle fullInfoData'>";
        if ($multiDates) {
            $html .= "<td class='fullInfo fullTitle dateCol'>";
            $date = $row['startdate'];
            $dates[$row['startdate']] = str_replace('/', '', $row['startdate']);
            $html .= '<span e4sSelect="1" e4sSelectType="' . $row['type'] . '" e4sSelectDate="' . $dates[$row['startdate']] . '" style="display:none;">' . $row['startdate2'] . '</span>';
            $html .= '</td>';
        }
        $html .= "<td class='fullInfo fullTitle timeCol'>";
        $html .= $starttime . '</td>';
//        $html .= "<td class='fullInfo fullTitle fullInfoLabel'>Event</td><td class='fullInfo fullTitle fullInfoData'>" . $keyDisplay . '</td>';
        $html .= "<td class='fullInfo fullTitle eventCol'>" . $keyDisplay . '</td>';
	    $html .= '<td class="fullInfo fullTitle eventCol">';
        $qualifyObj = new e4sQualifyClass($row['egOptions']);
        $qualifyTo = $qualifyObj->getName();
        if ($qualifyTo !== "" ){
            $qualText = "";
            $qualEgObj = $compObj->getEventGroupByEgId($qualifyObj->getId());
	        $qualDateDT = new DateTime($qualEgObj->startDate);
	        $qualDate  = $qualDateDT->format('Y-m-d');
	        $startDate = explode('/',$row['startdate']);
            $startDate = $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0];
            $startDateDT = new DateTime($startDate);
            $startDate = $startDateDT->format('Y-m-d');
            if ( $startDate !== $qualDate ){
                $qualText = " (" . $qualDateDT->format('D') . ")";
            }

            $qualifyTo = "Q > " . $qualEgObj->typeNo . ' : ' . $qualifyObj->getName() . $qualText;
        }
	    $html .= $qualifyTo ;
	    $html .= '</td>';
        $countSQL = '';

        if ($row['eventTeam'] === '1') {
            $countSQL = 'Select count(*) as entries
                         from ' . E4S_TABLE_EVENTTEAMENTRIES . '
                         where paid in (' . E4S_ENTRY_PAID . ',' . E4S_ENTRY_QUALIFY . ')
                         and ceid in (
                            select id from ' . E4S_TABLE_COMPEVENTS . "
                            where compid = $compid
                            and maxgroup = '" . $row['maxGroup'] . "')";
        } else {
            $countSQL = 'Select count(*) as entries
                    from ' . E4S_TABLE_ENTRIES . '
                     where paid in (' . E4S_ENTRY_PAID . ',' . E4S_ENTRY_QUALIFY . ')
                    and compeventid in (
                        select id from ' . E4S_TABLE_COMPEVENTS . "
                        where compid = $compid
                        and maxgroup = '" . $row['maxGroup'] . "')";
        }
        $result = e4s_queryNoLog($countSQL);
        $countResult = $result->fetch_assoc();
        $title = "";
	    $row['heatCount'] = "";

        $egOptions = e4s_getOptionsAsObj($row['egOptions']);
        if ( $row['type'] === E4S_EVENT_TRACK ) {
	        if ( isset( $egOptions->maxInHeat ) and $egOptions->maxInHeat > 0 and $countResult['entries'] > 0 ) {
		        $title            = 'Max Races : ';
		        $row['heatCount'] = ceil( $countResult['entries'] / $egOptions->maxInHeat );
	        }
        }
	    $html .= "<td class='fullInfo fullTitle fullCountLabel'>" . $title . "</td>";
        $html .= "<td class='fullCount fullTitle'>" . $row['heatCount'] . '</td>';
        if ($row['maxathletes'] > 0) {
            $html .= "<td class='fullInfo fullTitle fullInfoLabel'>Entries :</td><td class='fullInfo fullTitle'>" . $countResult['entries'] . '</td>';
            $html .= "<td class='fullInfo fullTitle fullInfoLabel'>Max :</td><td class='fullInfo fullTitle'>" . $row['maxathletes'] . '</td>';
        } else {

            if ($row['maxathletes'] === 0) {
                $html .= "<td class='fullInfo fullTitle fullInfoLabel'>Entries :</td>";
                $html .= "<td class='fullInfo fullTitle'>" . $countResult['entries'] . '</td>';
            } else {
                $html .= "<td class='fullInfo fullTitle fullInfoLabel'>-</td>";
                $html .= "<td class='fullInfo fullTitle'>&nbsp;</td>";
            }
	        $html .= "<td class='fullInfo fullTitle'>&nbsp;</td>";
	        $html .= "<td class='fullInfo fullTitle'>&nbsp;</td>";
        }

        $html .= '</tr>';
        $html .= '</table>';

        $eventSaved = $row['eventGroupName'];
        $timeSaved = $row['starttime'];
        $splitSaved = $row['split'];
        $rowData = $allAgeGroups;
    }

    $rowData[str_pad($row['maxage'], 3, '0', STR_PAD_LEFT) . '-' . $row['agegroup']] .= $row['gender'];
}
?>
    <html lang="en">
    <head>
        <link rel="stylesheet" href="<?php echo E4S_PATH ?>/competition/css/competitionDialog.css">
        <link rel="stylesheet" href="<?php echo E4S_PATH ?>/css/entry4sports.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<!--        <script src="https://--><?php //echo E4S_CURRENT_DOMAIN . '/' . E4S_PATH ?><!--/reports/commonReports.js"></script>-->
        <style>
            .e4s_generated {
                font-size: 0.7vw !important;
                font-weight: normal;
            }
            .e4sCompNotes {
                font-size: 0.8vw !important;
                font-weight: bold;
                width: 25% !important;
            }
            .e4s_header_label {
                width: 10%;
                font-size: 1vw;
            }
            .fullCountLabel {
                width: 4vw;
                max-width: 4vw;
                min-width: 4vw;
            }
            .fullCount {
                width: 4vw;
                max-width: 4vw;
                min-width: 4vw;
            }
            .fullInfoLabel {
                width: 4vw;
                max-width: 4vw;
                min-width: 4vw;
            }
            .fullInfo {
                width: 4vw;
                max-width: 4vw;
                min-width: 4vw;
            }
            .dateCol {
                color: black;
                width: 7vw;
                min-width: 7vw;
                font-weight: bold;
            }
            .timeCol {
                color: black;
                width: 5vw;
                min-width: 5vw;
                max-width: 5vw;
                font-family: 'arial' !important;
            }
            .e4s_event_type_span {
                display: inline-block;
                width: 2.5vw;
            }
            .eventCol {
                color: black;
                width: 20vw;
                min-width: 20vw;
            }
            .e4s_header_data {
                width: 35%;
                font-size: 1.2vw;
                font-weight: 700;
            }
            .fullInfoTableData {
                display: none;
            }
            .fullTitle {
                font-size: 0.8vw !important;
            }
            @media print
            {
                .no-print, .no-print *
                {
                    display: none !important;
                }
            }
        </style>
        <title>
            <?php echo $env . $compObj->getID() . ' : Schedule' ?>
        </title>
        <script>
            let showOnly = '';
            function toggleAgeGroup(){
                $(".fullInfoTableData").toggle();
            }
            function toggleScheduleOnly(){
                $(".scheduleOnly").toggle();
                showOnlyType(showOnly);
            }
            function showEvery(obj){
                obj.each(
                    function(index, element){
                        // show every 10
                        if ( index % 10 === 0) {
                            $(element).show();
                        }
                    }
                )
            }
            function showOnlyType(showType){
                showOnly = showType;
                resetToAll(showType);
                let obj;
                if ( showType !== '') {
                    $(".eventType_" + showType).show();
                    <?php
                    if ( sizeof($dates) > 1 ){
                        foreach ($dates as $date){
                        ?>
                            obj = $("[e4sselecttype=" + showType + "][e4sselectdate=<?php echo $date ?>]");
                            showEvery(obj);
                        <?php
                        }
                    }else{
                        ?>
                            obj = $("[e4sselecttype=" + showType + "]");
                            showEvery(obj);
                        <?php
                    }
                    ?>
                }else{
                    <?php
                    if ( sizeof($dates) > 1 ){
                        foreach ($dates as $date){
                        ?>
                            obj = $("[e4sselectdate=<?php echo $date ?>]");
                            showEvery(obj);
                        <?php
                        }
                    }
                    ?>
                }
            }
            function resetToAll(showType){
                if ( showType === ''){
                    $(".eventType").show();
                }else{
                    $(".eventType").hide();
                }
                $('.fullInfoTableData').hide();
                $("[e4sSelect=1]").hide();
            }
            function onLoad(){
                <?php
                if ( $hasScheduleOnly ){
                    echo 'showOnlyType(showOnly);';
                }else{
                    echo 'showOnlyType("");';
                }
                ?>
            }
            function exportToExcel(){
                let obj = $(".eventType_T").each(function(x){
                    return $(this).html();
                });
                let html = '';
                for(let h in obj){
                    if ( !isNaN(h) ) {
                        html += obj[h].outerHTML;
                    }
                }
                obj = $(".eventType_F").each(function(x){
                    return $(this).html();
                });

                for(let h in obj){
                    if ( !isNaN(h) ) {
                        html += obj[h].outerHTML;
                    }
                }
                let a = document.createElement('a');
                let data_type = 'data:application/vnd.ms-excel';
                let table_html = html.replace(/<a\/?[^>]+>/g, '');
                let table = table_html.replace(/<table/g, '<table border="2"');
                a.href = data_type + ', ' + escape(table);
                a.download = 'schedule.xls';
                a.click();
            }
        </script>
    </head>
    <body onload="onLoad();">
    <div style='height:5%; width:100%'>
<table>
    <tr>
        <td class="e4s_header_label">Competition</td>
        <td class="e4s_header_data"><?php echo $compObj->getFormattedName() ?></td>
        <td class="e4s_header_label">Date</td>
        <td class="e4s_header_data"><?php echo $compObj->getFormattedDate("D, j M Y") ?></td>
    </tr>
    <tr>
        <td class="e4s_header_label">Organiser</td>
        <td class="e4s_header_data"><?php echo $compObj->getOrganisationName() ?></td>
        <td class="e4s_header_label">Location</td>
        <td class="e4s_header_data"><?php echo $compObj->getLocationName() ?></td>
    </tr>
    <tr>
       <td colspan="4" class="e4s_header_label e4s_generated">Schedule Generated : <?php echo date("D, d M Y H:i") ?></td>
    </tr>
    <?php
    if ( $compid === 617 ){
    ?>
    <tr>
        <td colspan="4" class="e4s_header_label e4sCompNotes">(O) Events that will take place at the External Outdoor Throws Area.<br>(BS) Race to be run on the Back Straight.</td>
    </tr>
    <?php
    }
    ?>
</table>
<table>
    <tr>
        <?php
        if ( sizeof($types) > 1){
            ?>
            <td><button class="no-print e4s-button--primary" onclick="showOnlyType('')"><span>All</span></button></td>
            <?php
            foreach($types as $type){
                switch ($type){
                    case E4S_EVENT_TRACK:
                        $type_full = E4S_EVENT_TRACK_FULL;
                        break;
                    case E4S_EVENT_FIELD:
                        $type_full = E4S_EVENT_FIELD_FULL;
                        break;
                    case E4S_EVENT_ROAD:
                        $type_full = E4S_EVENT_ROAD_FULL;
                        break;
                    case E4S_EVENT_XCOUNTRY:
                        $type_full = E4S_EVENT_XCOUNTRY_FULL;
                        break;
                    case E4S_EVENT_MULTI:
                        $type_full = E4S_EVENT_MULTI_FULL;
                        break;
                    default:
                        $type_full = $type;
                        break;
                }
                ?>
                    <td><button class="no-print e4s-button--primary" onclick="showOnlyType('<?php echo $type ?>')"><span><?php echo $type_full ?></span></button></td>
                <?php
            }
        }
        if ( $hasScheduleOnly ){
            ?>
            <td><button class="no-print e4s-button--primary" onclick="toggleScheduleOnly();"><span>Toggle Schedule Only</span></button></td>
            <?php
        }
        ?>
        <td><button class="no-print e4s-button--primary" onclick="toggleAgeGroup();"><span>Toggle Age Groups</span></button></td>
        <td><button class="no-print e4s-button--primary" onclick="window.print();"><span>Print</span></button></td>
        <td><button class="no-print e4s-button--primary" onclick="alert('On opening the Excel spreadsheet, a warning will be displayed. Click Yes');exportToExcel();"><span>Export To Excel</span></button></td>
    </tr>
</table>
<hr>
<div id='scheduleContent' style="height:100%; width:100%; overflow:wrap; padding-left: 15px;">
<?php
echo $html;
echo outputAgeEventData($rowData, $row, $keyValue);
?>
</div>
</body>
</html>
<?php
exit();

function outputAgeEventData($rowData, $row, $keyValue){
    $ignoreGender = FALSE;
    if (array_key_exists('eoptions', $row)) {
        $eoptions = $row['eoptions'];
        if ($eoptions !== '') {
            $eoptions = json_decode($eoptions);
            if (isset($eoptions->gender)) {
                $ignoreGender = !$eoptions->gender;
            }
        }
    }
    $retHTML = '';
    $retHTML .= "<table class='fullInfoTable fullInfoTableData' id='" . $keyValue . "_age'>";
    $retHTML .= "<tr class='fullInfoAgeRow'>";
    $retHTML .= "<td class='fullInfo fullInfoGender'>&nbsp</td>";
    foreach ($rowData as $key => $value) {
        $keyArr = explode('-', $key);
        $age = str_replace('Under', 'U', $keyArr[1]);
        $retHTML .= "<td class='fullInfo fullData fullInfoAgeLabel'>" . $age . '</td>';
    }
    $retHTML .= '</tr>';
    if ($ignoreGender == FALSE) {
        $retHTML .= "<tr class='fullInfoGenderRow'>";
        $retHTML .= "<td class='fullInfo fullData fullInfoLabel'>Female</td>";
        foreach ($rowData as $key => $value) {
            $retHTML .= "<td class='fullInfo fullData fullInfoTick'>";
            if (strpos($value, 'F') !== FALSE) {
                $retHTML .= '&#10003;';
            }
            $retHTML .= '</td>';
        }
        $retHTML .= '</tr>';
    }
    $retHTML .= "<tr class='fullInfoGenderRow'>";
    if ($ignoreGender) {
        $retHTML .= "<td class='fullInfo fullData fullInfoLabel'>Male/Female</td>";
    } else {
        $retHTML .= "<td class='fullInfo fullData fullInfoLabel'>Male</td>";
    }

    foreach ($rowData as $key => $value) {
        $retHTML .= "<td class='fullInfo fullData fullInfoTick'>";
        if (strpos($value, E4S_GENDER_MALE) !== FALSE || $ignoreGender) {
            $retHTML .= '&#10003;';
        }
        $retHTML .= '</td>';
    }
    $retHTML .= '</tr>';
    $retHTML .= '</table>';
    $retHTML .= '</div>';

    return $retHTML;
}

?>