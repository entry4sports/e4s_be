<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
if (!isset($_GET['compid'])) {
    return;
}
$compid = $_GET['compid'];

$sql = "SELECT distinct(date_format(startdate,'%d %m %y')) FROM `" . E4S_PREFIX . "CompEvents` where compid = $compid";
$result = mysqli_query($conn, $sql);
$multiDates = FALSE;
if ($result->num_rows > 1) {
    $multiDates = TRUE;
}

$sql = "SELECT ce.maxathletes, date_format(startdate,'%d/%m/%Y') as startdate,date_format(startdate,'%a, %D %b') as startdate2,date_format(startdate,'%H:%i') as starttime, ce.split, a.maxage, a.Name as agegroup, e.name as event, e.gender, ce.maxGroup, ce.options
FROM `" . E4S_PREFIX . 'CompEvents` as ce,
      ' . E4S_COMMON_PREFIX . 'AgeGroups as a,
      ' . E4S_COMMON_PREFIX . 'Events as e,
      ' . E4S_TABLE_EVENTGROUPS . " eg
where compid = $compid
and a.id = ce.agegroupid
and e.id = ce.eventid
and   ce.maxgroup = eg.id
and ce.maxgroup != ''
order by eg.startdate, e.name, eventid, split, a.maxage";

$result = mysqli_query($conn, $sql);

$eventSaved = '';
$timeSaved = '';
$splitSaved = '';
$allData = array();
$allAgeGroups = array();
//
echo '<link rel="stylesheet" href="' . E4S_PATH . '/competition/css/competitionDialog.css">';
echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>';
echo '<script>';
echo 'function Entry4scrollTo(selectObj){';
echo ' debugger;';
echo '    if (selectObj.selectedIndex == 0) return;';
echo '    let loc = selectObj.options[selectObj.selectedIndex].value;';
echo "    let obj = jQuery('#showInfoDialog');";
echo '    obj.animate({ ';
echo "        scrollTop:  obj.scrollTop() - obj.offset().top + jQuery('#' + loc).offset().top ";
echo '    }, 1000);';
echo '}';
echo '</script><br>';
echo "<div style='height:5%; width:100%'>";
//echo "Jump to : <select style='width:15%;' id='eventSelect' onchange=\"Entry4scrollTo(this)\">";
//echo "<option value=''>Select Event</option>";
while ($row = mysqli_fetch_array($result, TRUE)) {
    $allData[] = $row;
    $allAgeGroups[str_pad($row['maxage'], 3, '0', STR_PAD_LEFT) . '-' . $row['agegroup']] = '';
}

$rowData = array();
ksort($allAgeGroups);
$html = '';
//while(list($key, $row) = each($allData)) {
foreach ($allData as $key => $row) {
    if ($row['event'] != $eventSaved || $row['starttime'] != $timeSaved || $row['split'] != $splitSaved) {
        // New Event
        if ($eventSaved != '') {
            $html .= outputAgeEventData($rowData);
        }
        $starttime = $row['starttime'];
        if ($row['starttime'] == '00:00') {
            $starttime = E4S_NO_TIME;
        }
        $key = '';
        if ($multiDates) {
            $key = $row['startdate'] . ' ';
        }
        $key .= $starttime . ' - ' . $row['event'];
        $split = '';
        if ($row['split'] !== '0') {
            $lg = '>';
            $s = $row['split'];
            if ((int)$s < 0) {
                $lg = '<';
                $s = str_replace('-', '', $s);
            }
            $split = ' (' . $lg . $s . ')';
        }
        $keyValue = str_replace(' ', '', $key);
        $keyValue = str_replace(':', '', $keyValue);
        $keyValue = str_replace('/', '', $keyValue);
        $keyChoice = $key . ' ' . $split;
        $keyDisplay = $row['event'] . ' ' . $split;
//        echo "<option value='" . $keyValue ."'>" . $keyChoice . "</option>";
        $html .= "<table name='" . $keyValue . "' id='" . $keyValue . "' class='fullInfoTable fullInfoTableTitle'>";
        $html .= "<tr class='fullInfoTitleRow'>";

        $html .= "<td class='fullInfo fullTitle fullInfoLabel'>Time</td><td class='fullInfo fullTitle fullInfoData'>";
        if ($multiDates) {
            $html .= $row['startdate2'] . ' ';
        }
        $html .= $starttime . '</td>';
        $html .= "<td class='fullInfo fullTitle fullInfoLabel'>Event</td><td class='fullInfo fullTitle fullInfoData'>" . $keyDisplay . '</td>';

        $countSQL = 'Select count(*) as entries from ' . E4S_PREFIX . 'Entries 
                     where paid = 1 
                     and compeventid in (
                            select id from ' . E4S_PREFIX . "CompEvents where compid = $compid and maxgroup = '" . $row['maxGroup'] . "')";
        $result = mysqli_query($conn, $countSQL);
        $countResult = $result->fetch_assoc();

        if ($row['maxathletes'] > 0) {
            $html .= "<td class='fullInfo fullTitle fullInfoLabel'>Num/Max Entries</td><td class='fullInfo fullTitle fullInfoData'>" . $countResult['entries'] . '/' . $row['maxathletes'] . '</td>';
        } else {
            if ($row['maxathletes'] == 0) {
                $html .= "<td class='fullInfo fullTitle fullInfoLabel'>Num Entries</td><td class='fullInfo fullTitle fullInfoData'>" . $countResult['entries'] . '</td>';
            } else {
                $html .= "<td class='fullInfo fullTitle fullInfoLabel'>Finals</td><td class='fullInfo fullTitle fullInfoData'>&nbsp;</td>";
            }
        }
        $html .= '</tr>';
        $html .= '</table>';
        $eventSaved = $row['event'];
        $timeSaved = $row['starttime'];
        $splitSaved = $row['split'];
        $rowData = $allAgeGroups;
    }
    $rowData[str_pad($row['maxage'], 3, '0', STR_PAD_LEFT) . '-' . $row['agegroup']] .= $row['gender'];
}
echo '</select>';
echo '</div>';
echo "<div id='showInfoDialog' style='height:100%; width:100%; overflow:wrap'>";
echo $html;
echo outputAgeEventData($rowData);
echo '</div>';


function outputAgeEventData($rowData) {
    $retHTML = '';
    $retHTML .= "<table class='fullInfoTable fullInfoTableData'>";
    $retHTML .= "<tr class='fullInfoAgeRow'>";
    $retHTML .= "<td class='fullInfo fullInfoGender'>&nbsp</td>";
    foreach ($rowData as $key => $value) {
        $keyArr = explode('-', $key);
        $age = str_replace('Under', 'U', $keyArr[1]);
        $retHTML .= "<td class='fullInfo fullData fullInfoAgeLabel'>" . $age . '</td>';
    }
    $retHTML .= '</tr>';
    $retHTML .= "<tr class='fullInfoGenderRow'>";
    $retHTML .= "<td class='fullInfo fullData fullInfoLabel'>Female</td>";
    foreach ($rowData as $key => $value) {
        $retHTML .= "<td class='fullInfo fullData fullInfoTick'>";
        if (strpos($value, E4S_GENDER_FEMALE) !== FALSE) {
            $retHTML .= '&#10003;';
        }
        $retHTML .= '</td>';
    }
    $retHTML .= '</tr>';
    $retHTML .= "<tr class='fullInfoGenderRow'>";
    $retHTML .= "<td class='fullInfo fullData fullInfoLabel'>Male</td>";
    foreach ($rowData as $key => $value) {
        $retHTML .= "<td class='fullInfo fullData fullInfoTick'>";
        if (strpos($value, E4S_GENDER_MALE) !== FALSE) {
            $retHTML .= '&#10003;';
        }
        $retHTML .= '</td>';
    }
    $retHTML .= '</tr>';
    $retHTML .= '</table>';

    return $retHTML;
}

?>