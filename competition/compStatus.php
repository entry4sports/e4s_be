<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';

define('E4S_COMP_STATUS_NOTES', 'e4sstatus');

function getCompSecurityCode($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (is_null($compId)) {
        echo 'Really ?';
        exit();
    }
    $compObj = e4s_GetCompObj($compId);
    if ($compObj->isOrganiser()) {
        echo $compObj->getSecurityKey();
    } else {
        echo 'Sorry, You are not authorised to use this function';
    }
    exit();
}

function checkWorkflowAccess() {

    if (!isE4SUser()) {
        return emptyStatus(9000);
    }
    if (!userHasPermission('e4s', 0, 0)) {
        return emptyStatus(9001);
    }
    return null;
}

function getAllWorkflow($cronjob) {
    // caching mechanism

    if (array_key_exists('e4sWorkflow', $GLOBALS)) {
        return ($GLOBALS['e4sWorkflow']);
    }
    if (!$cronjob) {
        if (!is_null(checkWorkflowAccess())) {
            return array();
        }
    }
    $sql = 'Select *
            from ' . E4S_TABLE_WORKFLOW;
    $result = e4s_queryNoLog($sql);
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    $GLOBALS['e4sWorkflow'] = $rows;
    return $rows;
}

function emptyStatus($code) {
    $retval = array();
    $retval['id'] = 0;
    $retval['compid'] = 0;
    $retval['description'] = 'No Status';
    $retval['status'] = 'NO_STATUS';
    $retval['invoicelink'] = '';
    $retval['reference'] = '';
    $retval['notes'] = '';
    $retval['value'] = 0;
    $retval['code'] = $code;
    $retval['wfid'] = 0;
    $retval['previd'] = 0;
    $retval['prevdescription'] = '';
    $retval['nextdescription'] = '';
    $retval['nextid'] = 0;
    return $retval;
}

function getCompStatus($compid) {

    $row = checkWorkflowAccess();
    $workflowRows = getAllWorkflow(FALSE);

    if (is_null($row)) {
        $sql = 'select cs.*, wf.id wfid, wf.status, wf.description, wf.previd, wf.nextid
            from ' . E4S_TABLE_COMPSTATUS . ' cs,
                 ' . E4S_TABLE_WORKFLOW . ' wf
            where statusid = wf.id
            and compid = ' . $compid;

        $result = e4s_queryNoLog($sql);

        if ($result->num_rows !== 1) {
            $errorRow = emptyStatus(9010);
            $errorRow['compid'] = $compid;
            return $errorRow;
        }
        $row = $result->fetch_assoc();

        if (strpos(strtolower($row['invoicelink']), E4S_CURRENT_DOMAIN) === FALSE) {
            $protocol = $_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
            $row['invoicelink'] = $protocol . '://' . E4S_CURRENT_DOMAIN . '/' . trim($row['invoicelink']);
        }
        unset($row['statusid']);
        $row['code'] = 0;
        if (array_key_exists('reference', $row)) {
            $row['ref'] = $row['reference'];
            unset($row['reference']);
        }
        $previd = (int)$row['previd'] - 1;
        $row['prevdescription'] = '';
        if ($previd >= 0) {
            $row['prevdescription'] = $workflowRows[$previd]['description'];
        }
        $nextid = (int)$row['nextid'] - 1;
        $row['nextdescription'] = '';
        if ($nextid >= 0) {
            $row['nextdescription'] = $workflowRows[$nextid]['description'];
        }

        $row['history'] = getCompStatusHistory($compid);

        $row['notes'] = notesClass::get($compid, E4S_COMP_STATUS_NOTES);
    }

    return $row;
}

function getCompStatusHistory($compid) {

    $sql = 'select *
            from ' . E4S_TABLE_COMPSTATUSAUDIT . '
            where compid = ' . $compid . '
            order by updated desc';
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows === 0) {
        return array();
    }
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    return $rows;
}

function moveToStatus($obj) {
    $access = checkWorkflowAccess();
    if (!is_null($access)) {
        Entry4UIError(9043, 'You are not authorised.', 401, '');
    }

    $model = getCompStatusModel($obj);

    if ($model->moveto < 1) {
        Entry4UIError(9044, 'No Workflow status sent', 400, '');
    }
    if ((int)$model->id === 0) {
        $sql = 'Insert into ' . E4S_TABLE_COMPSTATUS . ' (compid, statusid, invoicelink, reference, value, updated, userid)
             values (
             ' . $model->compid . ',
             ' . $model->moveto . ",
             '" . $model->invoicelink . "',
             '" . $model->reference . "',
             " . $model->value . ',
             ' . returnNow() . ',
             ' . e4s_getUserID() . '
             )';
    } else {
        $sql = 'update ' . E4S_TABLE_COMPSTATUS . '
            set statusid    = ' . $model->moveto . ',
                value        = ' . $model->value . ",
                invoicelink = '" . $model->invoicelink . "',
                reference = '" . $model->reference . "',
                userid      = " . e4s_getUserID() . ',
                updated     = ' . returnNow() . '
            where compid    =' . $model->compid;
    }

    e4s_queryNoLog($sql);

    notesClass::set($model->compid, E4S_COMP_STATUS_NOTES, $model->notes);

    $fields = 'compid, statusid, invoicelink, reference, value, userid, updated';
    $sql = 'insert into ' . E4S_TABLE_COMPSTATUSAUDIT . '(' . $fields . ' )
     select  ' . $fields . ' from ' . E4S_TABLE_COMPSTATUS . '
     where compid = ' . $model->compid;
    e4s_queryNoLog($sql);
    Entry4UISuccess('');
}

function getCompStatusModel($obj) {

//    $params = $obj->get_params("JSON");

    $model = new stdClass();
    $model->id = checkFieldForXSS($obj, 'id:Competition Status ID' . E4S_CHECKTYPE_NUMERIC);
    $model->compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if ($model->compid <= 0) {
        Entry4UIError(9045, 'Invalid competition code sent', 400, '');
    }
    $model->invoicelink = checkFieldForXSS($obj, 'invoicelink:Finance Link');
    $model->reference = checkFieldForXSS($obj, 'ref:Bank Reference');
    $model->notes = checkFieldForXSS($obj, 'notes:Notes');
    $model->moveto = checkFieldForXSS($obj, 'moveto:Workflow target id');
    $model->value = checkFieldForXSS($obj, 'value:Value of Invoice');

    return $model;
}