<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'entries/commonEntries.php';

function e4s_updateEntryTeamBibNos($obj){
    $params = $obj->get_params('JSON');
    $entries = null;
    if ( array_key_exists('entries', $params )) {
        $entries = $params['entries'];
    }
    $ids = array();
    foreach($entries as $entry){
        $ids[] = $entry['id'];
    }
    if ( sizeof($ids) === 0 ){
        Entry4UIError(9780,'Incorrect Entries passed.' );
    }
    $sql = '
        select id,
               teamBibNo,
               entity
        from ' . E4S_TABLE_ENTRIES . '
        where id in (' . implode(',',$ids) . ')
    ';
    $result = e4s_queryNoLog($sql);
    if ( $result->num_rows === 0 ){
        Entry4UIError(9781,'Incorrect Entries passed. [' . implode(',',$ids) . ']' );
    }
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    if ( sizeof($ids) !== sizeof($rows)){
        Entry4UIError(9784,'Incorrect Entries passed. [' . implode(',',$ids) . ']' );
    }
    foreach($rows as $row){
        $entity = $row['entity'];
        if ( $entity === '' ){
            Entry4UIError(9782,'Incorrect Entries passed. [' . implode(',',$ids) . ']' );
        }
        if ( !e4s_doesUserHaveEntity($entity)){
            Entry4UIError(9783,'Incorrect Entries passed. [' . implode(',',$ids) . ']' );
        }
    }
    // If we get here, all valid and correct to update
    foreach($entries as $entry){
        $sql = '
            update ' . E4S_TABLE_ENTRIES . "
            set teamBibNo = '" . $entry['teamBibNo'] . "'
            where id = " . $entry['id'];
        e4s_queryNoLog($sql);
    }
    Entry4UISuccess('', 'Team Bib Nos moved');
}
function e4s_updateEntryTeamBibNo($obj) {
    $entryId = checkFieldForXSS($obj, 'entryid:Entry ID' . E4S_CHECKTYPE_NUMERIC);
    $teamBibNo = checkFieldForXSS($obj, 'teambibno:Bib No');
    if (is_null($teamBibNo) or $teamBibNo === '0') {
        $teamBibNo = '';
    }
    $sql = 'update ' . E4S_TABLE_ENTRIES . "
            set teambibno = '" . $teamBibNo . "'
            where id = " . $entryId;
    e4s_queryNoLog($sql);
}

function e4s_checkUnPaid() {
//    $entryObj = new entryClass();
//    $entryObj->checkExpiredEntries();
    Entry4UISuccess();
}

function e4s_removeProductFromUsersBasket($prodId, $userId, $exit = FALSE) {
    e4s_removeProductFromBasket($prodId, TRUE, $userId, $exit);
}

function e4s_removeProductFromBasket($prodId, $removeFromBasket = TRUE, $userId = null, $exit = TRUE) {
    entryClass::removeProductAndEntry($prodId, $removeFromBasket, $userId, $exit);
    if ($exit){
        Entry4UIMessage('Removed');
    }
}

function e4s_markOrderPaid($orderId) {
    $entryObj = new entryClass();
    $entryObj->WCPaymentMadeForOrder($orderId);
}

function e4s_getEntryList($obj) {
    $entryObj = new entryClass();
    $entryObj->list($obj);
}

function e4s_getEntriesForAthlete($obj) {
    $athleteId = checkFieldForXSS($obj, 'athleteid:Athlete ID' . E4S_CHECKTYPE_NUMERIC);
    $athleteObj = athleteClass::withID($athleteId);
    $entries = $athleteObj->getAllEvents();
    Entry4UISuccess($entries);
}

function e4s_moveEntryOffWaitingList($obj) {
    $entryId = checkFieldForXSS($obj, 'entryid:Entry ID' . E4S_CHECKTYPE_NUMERIC);
    if (!is_numeric($entryId)) {
        Entry4UIError(6654, 'No information passed to waiting list');
    }

    $entryObj = new entryClass();
    $errors = $entryObj->moveOffWaitingList((int)$entryId);
    if (sizeof($errors) === 0) {
        Entry4UISuccess();
    } else {
        Entry4UIError(3670, join(', ', $errors), 200);
    }
}
function e4s_moveEntriesVertical($obj){
    $params = $obj->get_params('JSON');

    $compId = (int)$params['compId'];
    $egId = (int)$params['egId'];
    $checkIn = $params['checkIn'];
    if ( $checkIn === 'true'){
        $checkIn = true;
    }else{
        $checkIn = false;
    }
    $factor = (int)$params['factor'];
    $heatNo = (int)$params['heatNo'];
    $heatNoTo = $heatNo + $factor;

    $heatNoColumn = 'heatNo';
    if ( $checkIn ){
        $heatNoColumn .= 'checkedin';
    }
    // get current seedings
    $sql = 'select id, ' . $heatNoColumn . '
            from ' . E4S_TABLE_SEEDING . '
            where eventgroupid = ' . $egId . '
            and ' . $heatNoColumn . ' in (' . $heatNo . ',' . $heatNoTo . ')
            order by ' . $heatNoColumn;
    $result = e4s_queryNoLog($sql);
    if ( $result->num_rows === 0 ){
        Entry4UIError(4570, 'Failed to get seedings');
    }

    while ($obj = $result->fetch_object() ){
        $obj->$heatNoColumn = (int)$obj->$heatNoColumn;
        if ( $obj->$heatNoColumn === $heatNo ) {
            $obj->$heatNoColumn = $heatNoTo;
        }else{
            $obj->$heatNoColumn = $heatNo;
        }
        $sql = 'Update ' . E4S_TABLE_SEEDING . ' set ' . $heatNoColumn . ' = ' . $obj->$heatNoColumn . '
                where id = ' . $obj->id;
        e4s_queryNoLog($sql);
    }
    $data = new stdClass();
    $data->checkedIn = $checkIn;
    $data->factor = $factor;
    $data->egId = $egId;
    $data->heatNo = $heatNo;
    e4s_sendSocketInfo($compId, $data, R4S_SOCKET_TRACK_MOVEVERT);
    Entry4UISuccess();
}
function e4s_moveEntriesHorizontal($obj){
    $params = $obj->get_params('JSON');
    $entryInfo = null;
    if ( array_key_exists('entryInfo', $params )) {
        $entryInfo = $params['entryInfo'];
    }else{
        Entry4UIError(6935, 'Invalid parameters passed to move lanes');
    }
    $entityIds = [];

    foreach($entryInfo as $entryItem){
		$entityId = (int)$entryItem['athleteId'];
		if ( $entityId === 0 ){
			$entityId = (int)$entryItem['teamId'];
		}
        $entityIds[] = $entityId;
    }
    $compId = (int)$params['compId'];
    $egId = (int)$params['egId'];
    $checkIn = $params['checkIn'];
    if ( $checkIn === 'true'){
        $checkIn = true;
    }else{
        $checkIn = false;
    }
    $factor = (int)$params['factor'];
    $sql = '
        select *
        from ' . E4S_TABLE_SEEDING . '
        where eventgroupid = ' . $egId . '
        and athleteid in (' . implode(',',$entityIds) . ')
        ';
    $result = e4s_queryNoLog($sql);
    if ( $result->num_rows === 0 ){
        Entry4UISuccess();
    }
    $ids = [];
    while ($obj = $result->fetch_object()){
        $ids[] = (int)$obj->id;
    }
    $sql = '
        update ' . E4S_TABLE_SEEDING . '
        set ';
    if ( $checkIn ){
        $sql .= ' lanenocheckedin = lanenocheckedin ';
    }else{
        $sql .= ' laneno = laneno ';
    }
    $sql .= ' + ' . $factor . '
    where id in (' . implode(',',$ids ) . ')';

    e4s_queryNoLog($sql);
    $data = new stdClass();
    $data->checkedIn = $checkIn;
    $data->factor = $factor;
    $data->entryInfo = $entryInfo;
    e4s_sendSocketInfo($compId, $data, R4S_SOCKET_TRACK_MOVEHORIZ);
    Entry4UISuccess();
}
function e4s_addIncludedEntries($obj){
	$compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
	if ( is_null($compId) or $compId < 1){
		Entry4UIError(9780,'Incorrect Competition ID passed.' );
	}

	$entryObj = new entryClass();
	$entryObj->addIncludedEntries($compId);
	Entry4UISuccess();
}
function e4s_createEntriesForEg($obj) {
    $sourceEgId = checkFieldForXSS($obj, 'egid:Source Event Group ID' . E4S_CHECKTYPE_NUMERIC);
    $targetEgId = checkFieldForXSS($obj, 'targetegid:Target Event Group ID' . E4S_CHECKTYPE_NUMERIC);
    if (!is_null($sourceEgId)) {
        $sourceEgId = (int)$sourceEgId;
        $entryObj = new entryClass();
        $entryObj->createEntriesForEg($sourceEgId, $targetEgId);
    }
    Entry4UISuccess();
}

function e4s_recalculateCompAgeGroups($obj){
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $compObj = e4s_getCompObj($compId);
    $compAges = getAllCompDOBs($compId);
    $entries = e4s_getCompEntries($compId);
}
function e4s_getCompEntries($compId){
    $sql = 'select e.id
                   ,e.compEventID ceid
                   ,e.athleteId
                   ,e.variationID productId
                   ,e.ageGroupId ageGroupId
            from ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce
            where e.compeventid = ce.id
            and ce.compid = ' . $compId;
    $result = e4s_queryNoLog($sql);
    $entries = array();
    while ( $obj = $result->fetch_object() ){
        $obj->id = (int)$obj->id;
        $obj->ceid = (int)$obj->ceid;
        $obj->productId = (int)$obj->productId;
        $obj->ageGroupId = (int)$obj->ageGroupId;
        $obj->athleteId = (int)$obj->athleteId;
        $entries[] = $obj;
    }
    return $entries;
}

function e4s_getEntryPerf($obj){
	$entryId = checkFieldForXSS($obj, 'entryid:Entry ID' . E4S_CHECKTYPE_NUMERIC);
	$entryObj = new entryClass($entryId);
	Entry4UISuccess($entryObj->getPerf());
}
function e4s_updateEntryPerf($obj){
//	$athleteMiniObj = checkJSONObjForXSS($obj, 'athleteMini:Athlete Mini Obj' );
	$perfObj = checkJSONObjForXSS($obj, 'performance:performance Obj' );
	$entryId = checkFieldForXSS($obj, 'entryId:Entry Id' . E4S_CHECKTYPE_NUMERIC);
	$entryObj = new entryClass($entryId);
	$entryObj->updatePB($perfObj['perf'], $perfObj['perf'] === $perfObj['sb']);
}