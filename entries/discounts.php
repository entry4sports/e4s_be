<?php

function checkDiscOptions($discount, $athleteId, $eventId) {
    // if there are rules in the options field
    // ALL MUST BE MATCHED to apply discount
    $retval = $discount;
    static $athleteRow = null;
    $indivEvent = $athleteId > 0;
    if (is_null($discount['options'])) {
        $discount['options'] = '';
    }
    if ($discount['options'] === '') {
        // No options so good to go
        return $discount;
    }
    $options = e4s_addDefaultDiscountOptions($discount['options']);

    if ($indivEvent) {
        if (is_null($athleteRow)) {
            $sql = 'select *
                from ' . E4S_TABLE_ATHLETE . '
                where id = ' . $athleteId;
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows === 1) {
                $athleteRow = $result->fetch_assoc();
            }
        }
        $isRegistered = isAthleteRegistered($athleteRow);

        if ($options->registered !== $options->unregistered) {
            if ($options->registered === TRUE and $isRegistered === FALSE) {
                $retval = null;
            }

            if ($options->unregistered === TRUE and $isRegistered === TRUE) {
                $retval = null;
            }
        }
        // discount only for certain clubs
        if (!empty($options->clubs)) {
            $processDiscount = FALSE;
            $athleteclubid = (int)$athleteRow['clubid'];
            foreach ($options->clubs as $club) {
                if ((int)$club->id === $athleteclubid) {
                    $processDiscount = TRUE;
                }
            }
            if ($options->includeSecondClaim) {
                $athleteclubid = (int)$athleteRow['club2id'];
                foreach ($options->clubs as $club) {
                    if ((int)$club->id === $athleteclubid) {
                        $processDiscount = TRUE;
                    }
                }
            }
            if ($processDiscount === FALSE) {
                //            e4s_dump("clubs","Clubs",true);
                $retval = null;
            }
        }
        // discounts for certain athletes
        if (!empty($options->athletes)) {
            $processDiscount = FALSE;
            foreach ($options->athletes as $athleteToCheck) {
                if ((int)$athleteId === (int)$athleteToCheck->id) {
                    $processDiscount = TRUE;
                }
            }

            if ($processDiscount === FALSE) {
                $retval = null;
            }
        }
    }

    // discounts on events
    if ($eventId > 0) {
        if (!empty($options->events)) {
            $events = $options->events;
            $processEvent = FALSE;
            foreach ($events as $eventToCheck) {
                if ($eventToCheck->id === $eventId) {
                    $processEvent = TRUE;
                    break;
                }
            }
            if ($processEvent === FALSE) {
                $retval = null;
            }
        }

        if (!empty($options->ignoreEvents)) {
            $events = $options->ignoreEvents;
            $processEvent = TRUE;
            foreach ($events as $eventToCheck) {
                if ($eventToCheck->id === $eventId) {
                    $processEvent = FALSE;
                    break;
                }
            }
            if ($processEvent === FALSE) {
                $retval = null;
            }
        }
    }

    return $retval;
}

function getActiveDisc($cnt, $discounts, $athleteid, $eventid) {
    $retDiscount = null;

    foreach ($discounts as $discount) {
        if ($cnt >= (int)$discount['count']) {

            $processDiscount = checkDiscOptions($discount, $athleteid, $eventid);

            if (!is_null($processDiscount)) {
                if (is_null($retDiscount)) {
                    $retDiscount = $processDiscount;
                } else {
                    $p_options = $processDiscount['options'];
                    if ($p_options === '') {
                        $p_options = new stdClass();
                        $p_options->order = 9999;
                    } else {
                        $p_options = json_decode($p_options);
                        if (!isset($p_options->order)) {
                            $p_options->order = 9999;
                        }
                    }
                    $r_options = e4s_getOptionsAsObj($retDiscount['options']);
                    if (!isset($r_options->order)) {
                        $r_options->order = 9999;
                    }

                    if ($p_options->order < $r_options->order) {
                        $retDiscount = $processDiscount;
                    }
                }
            }
        }
    }
    return $retDiscount;
}

function returnDiscPrice($price, $discount, $type) {
    if (is_null($discount)) {
        return $price;
    }

    $useDisc = $discount['reg_disc'];
    if ($type === 'S') {
        $useDisc = $discount['sale_disc'];
    }

    // return price based on Discount Type
    return e4s_returnBasedOnType($discount, $useDisc, $price);
}

function e4s_returnBasedOnType($discount, $useDisc, $price) {
    // Type = V means explicit Value
    //        A positive value is to replace current value
    //        A negative value is subtracted from current value
    //        P means a percentage

    if ($discount['type'] == 'V') {
        if ($useDisc >= 0) {
            return $useDisc;
        } else {
            // disc price is neg
            $retPrice = ($price + $useDisc);
            if ($retPrice > 0) {
                return $retPrice;
            }
            return 0;
        }
    }
    if ($discount['type'] === 'P') {
        return $price - (($price / 100) * $useDisc);
    }
    return 0;
}

function getPriceForCEEntry($ceid) {

    $sql = 'SELECT * 
    FROM ' . E4S_TABLE_EVENTPRICE . ' p,
         ' . E4S_TABLE_COMPEVENTS . ' ce
    WHERE ce.PriceID = p.ID
    AND   ce.ID = ' . $ceid;

    $result = e4s_queryNoLog($sql);
    $priceRecord = $result->fetch_assoc();

    //logSql(print_r($priceRecord, true), $DEBUG_INFO);
    $saleDate = $priceRecord['saleenddate'];
    $regPrice = $priceRecord['price'];
    $salePrice = $priceRecord['saleprice'];
    $usePrice = $regPrice;

    // Active Price
    if ($saleDate !== '' and !is_null($saleDate) ) {
        $saleDate = strtotime($saleDate);
        $now = time();
        if ($saleDate > $now) {
            $usePrice = $salePrice;
        }
    }

    $priceRecord['usePrice'] = $usePrice;
    return $priceRecord;
}

function applyTeamDiscounts($ceids, $pids, $incPaidOnly) {
    $returnArray = array();

    // get std price
    $compRows = array();
    $compId = 0;
    $eag = 0;

    foreach ($ceids as $key => $value) {
        // Get price ID Record
        if ($value !== '') {
            $priceRecord = getPriceForCEEntry($value);

            $compId = $priceRecord['compid'];
            $eag = $priceRecord['AgeGroupID'];
            $saleEndDate = $priceRecord['saleenddate'];
            $regPrice = $priceRecord['price'];
            $salePrice = $priceRecord['saleprice'];

            $priceRecord['discountid'] = 0;
            $returnArray[$pids[$key]] = $priceRecord;
        }
    }

    if (array_key_exists($compId, $compRows)) {
        $compRow = $compRows[$compId];
    } else {
        $compObj = e4s_getCompObj($compId);
        $compRow = $compObj->getRow();
        $compRows[$compId] = $compRow;
    }

    $sql = 'select cd.* 
            from ' . E4S_TABLE_COMPDISCOUNTS . ' as cd 
            where (cd.compid = ' . $compId . ' or cd.orgid = ' . $compRow['compclubid'] . ')   
            and (cd.agegroupid = 0 or cd.agegroupid = ' . $eag . ')  
            ORDER BY agegroupid desc, count desc, id desc';

    $result = e4s_queryNoLog($sql);

    if ($result->num_rows === 0) {
        // no Discounts so exit
        return $returnArray;
    } else {
        $discounts = $result->fetch_all(MYSQLI_ASSOC);
//        addDebug("Discounts : " . $result->num_rows);
    }

    // Get any existing entries if required
    $existSQL = ' SELECT count(e.id)  as count
                  FROM `' . E4S_TABLE_EVENTTEAMENTRIES . '` as e
                      , ' . E4S_TABLE_COMPEVENTS . ' as ce
                  where e.userid = ' . e4s_getUserID() . '
                  and ce.CompID = ' . $compId . '
                  and e.ceid = ce.ID ';
    if ($incPaidOnly) {
        $existSQL .= 'and e.paid = ' . E4S_ENTRY_PAID;
    }

    $existResult = e4s_queryNoLog($existSQL);
    $existRow = $existResult->fetch_assoc();
    $existCnt = (int)$existRow['count'];

    $cnt = 1;
    // set any discounts
    foreach ($pids as $value) {
        if ($value !== '') {
            $useCnt = $cnt;
            $eventid = (int)$returnArray[$value]['EventID'];

            $discount = getActiveDisc($useCnt + $existCnt, $discounts, -1, $eventid);
            if (!is_null($discount)) {
                $useSalePrice = returnDiscPrice($salePrice, $discount, 'S');
                $useRegPrice = returnDiscPrice($regPrice, $discount, 'R');
                $discountid = $discount['id'];
            } else {
                $useSalePrice = $salePrice;
                $useRegPrice = $regPrice;
                $discountid = 0;
            }
//            $row = $result->fetch_assoc();

            $usePrice = updateWooCommercePrices($value, $useSalePrice, $useRegPrice, $saleEndDate);
            $returnArray [$value]['discountid'] = $discountid;
            $returnArray [$value]['usePrice'] = $usePrice;
            $cnt = $cnt + 1;
        }
    }

    return $returnArray;
}

function applyDiscounts($athleteID, $ceRows, $pids, $teams, $incPaidOnly) {
    $returnArray = array();

    // get std price
    $compRows = array();
    $compid = 0;
    $eag = 0;

    foreach ($ceRows as $key => $ceRow) {
        // Get price ID Record
        $ceId = $ceRow['ID'];
        if ($ceId !== '') {
            $priceRecord = getPriceForCEEntry($ceId);

            $compid = $priceRecord['compid'];
            $eag = $priceRecord['AgeGroupID'];
            $saleEndDate = $priceRecord['saleenddate'];
            $regPrice = $priceRecord['price'];
            $salePrice = $priceRecord['saleprice'];

            $priceRecord['discountid'] = 0;
            $returnArray[$pids[$key]] = $priceRecord;
        }
    }

    if (array_key_exists($compid, $compRows)) {
        $compRow = $compRows[$compid];
    } else {
        $compObj = e4s_getCompObj($compid);
        $compRow = $compObj->getRow();
        $compRows[$compid] = $compRow;
    }

    $sql = 'select cd.* 
            from ' . E4S_TABLE_COMPDISCOUNTS . ' as cd 
            where (cd.compid = ' . $compid . ' or cd.orgid = ' . $compRow['compclubid'] . ')   
            and (cd.agegroupid = 0 or cd.agegroupid = ' . $eag . ')  
            ORDER BY agegroupid desc, count desc, id desc';

    $result = e4s_queryNoLog($sql);

    if ($result->num_rows === 0) {
        // no Discounts so exit
        return $returnArray;
    } else {
        $discounts = $result->fetch_all(MYSQLI_ASSOC);
//        addDebug("Discounts : " . $result->num_rows);
    }

    // Get any existing entries if required
    $existSQL = ' SELECT count(e.id)  as count
                  FROM ' . E4S_TABLE_ENTRIES . ' as e
                      , ' . E4S_TABLE_COMPEVENTS . ' as ce
                  where e.athleteid = ' . $athleteID . '
                  and ce.CompID = ' . $compid . '
                  and e.compEventID = ce.ID ';
    if ($incPaidOnly) {
        $existSQL .= 'and e.paid = ' . E4S_ENTRY_PAID;
    }

    $existResult = e4s_queryNoLog($existSQL);
    $existRow = $existResult->fetch_assoc();
    $existCnt = (int)$existRow['count'];

    $cnt = 1;
    // set any discounts
    foreach ($pids as $value) {
        if ($value !== '') {
            $useCnt = $cnt;
            $eventid = $returnArray[(int)$value]['EventID'];
            // If a team entry, get team count rather than loop through the entries passed to this script
            if ($teams[$cnt - 1] !== 0) {
                $teamSql = 'Select count(e.id as count, ce.eventid eventid
                        from ' . E4S_TABLE_ENTRIES . ' e
                        , ' . E4S_TABLE_COMPEVENTS . ' as ce
                        where teamid = ' . $teams[$cnt - 1] . '
                        and e.compEventID = ce.ID 
                        and paid = ' . E4S_ENTRY_NOT_PAID;
                $teamResult = e4s_queryNoLog($teamSql);
                $teamRow = $teamResult->fetch_assoc();
                $useCnt = (int)$teamRow['count'];
                $eventid = (int)$teamRow['eventid'];
                // Add this entry to the numbers
                $useCnt = $useCnt + 1;
            }
            $discount = getActiveDisc($useCnt + $existCnt, $discounts, $athleteID, $eventid);
            if (!is_null($discount)) {
                $useSalePrice = returnDiscPrice($salePrice, $discount, 'S');
                $useRegPrice = returnDiscPrice($regPrice, $discount, 'R');
                $discountid = $discount['id'];
            } else {
                $useSalePrice = $salePrice;
                $useRegPrice = $regPrice;
                $discountid = 0;
            }
//            $row = $result->fetch_assoc();

            $usePrice = updateWooCommercePrices($value, $useSalePrice, $useRegPrice, $saleEndDate);
            $returnArray [$value]['discountid'] = $discountid;
            $returnArray [$value]['usePrice'] = $usePrice;
            $cnt = $cnt + 1;
        }
    }

    return $returnArray;
}

function reApplyTeamDiscounts($compid) {
    $userId = e4s_getUserID();
    $sql = 'select cd.* 
            from ' . E4S_TABLE_COMPDISCOUNTS . ' as cd 
            where cd.compid = ' . $compid;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows === 0) {
        // No discounts to reapply
        e4s_addDebug('No Discounts');
        return;
    }// Get any paid existing entries
    $existSQL = ' SELECT count(e.id) as count
                  FROM ' . E4S_TABLE_EVENTTEAMENTRIES . ' as e
                      ,' . E4S_TABLE_COMPEVENTS . ' as ce
                  where ce.CompID = ' . $compid . '
                  and e.ceid = ce.ID 
                  and e.paid = ' . E4S_ENTRY_PAID . '
                  and e.userid = ' . $userId;
    $existResult = e4s_queryNoLog($existSQL);
    $existRow = $existResult->fetch_assoc();
    $paidCnt = (int)$existRow['count'];

    $sql = 'Select * 
            from ' . E4S_TABLE_EVENTTEAMENTRIES . ' e,
                ' . E4S_TABLE_COMPEVENTS . ' ce
            where ce.id = e.ceid
            and   ce.compid = ' . $compid . '
            and   paid = ' . E4S_ENTRY_NOT_PAID . '
            and   e.userid = ' . $userId;
    $unpaidResult = e4s_queryNoLog($sql);
    $unpaidRows = $unpaidResult->fetch_all(MYSQLI_ASSOC);
    $unpaidCnt = 0;
    foreach ($unpaidRows as $unpaidRow) {
        $unpaidCnt++;
//        logTxt( e4s_getDataAsType($unpaidRow, E4S_OPTIONS_STRING));
        // Get price ID Record
        $ceId = $unpaidRow['ceid'];
        $productId = $unpaidRow['productid'];
        $priceRecord = getPriceForCEEntry($ceId);
        $eag = $priceRecord['AgeGroupID'];
        $saleDate = $priceRecord['saleenddate'];
        $regPrice = $priceRecord['price'];
        $salePrice = $priceRecord['saleprice'];

        $sql = 'select cd.* 
        from ' . E4S_TABLE_COMPDISCOUNTS . ' as cd 
        where cd.compid = ' . $compid . ' 
        and (cd.agegroupid = 0 or cd.agegroupid = ' . $eag . ')  
        ORDER BY agegroupid desc, count desc';
        $discResult = e4s_queryNoLog($sql);
        $discounts = $discResult->fetch_all(MYSQLI_ASSOC);

        if ($discResult->num_rows > 0) {
            $discount = getActiveDisc($paidCnt + $unpaidCnt, $discounts, -1, 0);
//            logTxt( e4s_getDataAsType($discount, E4S_OPTIONS_STRING));
            if (!is_null($discount)) {
                $useSalePrice = returnDiscPrice($salePrice, $discount, 'S');
                $useRegPrice = returnDiscPrice($regPrice, $discount, 'R');
            } else {
                $useSalePrice = $salePrice;
                $useRegPrice = $regPrice;
            }

            $usePrice = updateWooCommercePrices($productId, $useSalePrice, $useRegPrice, $saleDate);

            $sql = 'update ' . E4S_TABLE_EVENTTEAMENTRIES . '
                    set price = ' . $usePrice . '
                    where id = ' . $unpaidRow['id'];
            e4s_queryNoLog($sql);
        }
    }
}

function reApplyDiscounts($athleteID, $compid) {
    $sql = 'select cd.* 
            from ' . E4S_TABLE_COMPDISCOUNTS . ' as cd 
            where cd.compid = ' . $compid;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows === 0) {
        // No discounts to reapply
        return;
    }
    $sql = 'Select * 
            from ' . E4S_TABLE_ENTRIES . ' e,
                ' . E4S_TABLE_COMPEVENTS . ' ce
            where athleteid = ' . $athleteID . '
            and   ce.id = e.compeventid
            and   ce.compid = ' . $compid . '
            and   paid = ' . E4S_ENTRY_NOT_PAID;
    $unpaidResult = e4s_queryNoLog($sql);
    $unpaidRows = $unpaidResult->fetch_all(MYSQLI_ASSOC);

    foreach ($unpaidRows as $key => $valueRow) {
        // Get price ID Record
        $value = $valueRow['compEventID'];
        if ($value !== '') {
            $priceRecord = getPriceForCEEntry($value);
            $eag = $priceRecord['AgeGroupID'];
            $saleDate = $priceRecord['saleenddate'];
            $regPrice = $priceRecord['price'];
            $salePrice = $priceRecord['saleprice'];

            $sql = 'select cd.* 
            from ' . E4S_TABLE_COMPDISCOUNTS . ' as cd 
            where cd.compid = ' . $compid . ' 
            and (cd.agegroupid = 0 or cd.agegroupid = ' . $eag . ')  
            ORDER BY agegroupid desc, count desc';

            $result = e4s_queryNoLog($sql);
            if ($result->num_rows > 0) {
                // If more than one row returned, dont use the 0 which means any age
                $useAge = ($result->num_rows > 1);
                $agegroupid = '';
                $discounts = array();
                // Create array of discounts by ageGroup
                while ($row = $result->fetch_assoc()) {
                    if ($agegroupid == '') {
                        $agegroupid = $row['agegroupid'];
                    }
                    if (FALSE == $useAge or $agegroupid !== '0') {
                        if ($agegroupid == $row['agegroupid']) {
                            $discounts[] = $row;
                        }
                    }
                }
                // Get any paid existing entries
                $existSQL = ' SELECT count(e.id)  as count, ce.eventid eventid
                              FROM `' . E4S_TABLE_ENTRIES . '` as e
                                  , ' . E4S_TABLE_COMPEVENTS . ' as ce
                              where e.athleteid = ' . $athleteID . '
                              and ce.CompID = ' . $compid . '
                              and e.compEventID = ce.ID 
                              and e.paid = ' . E4S_ENTRY_PAID;
                $existResult = e4s_queryNoLog($existSQL);
                $existRow = $existResult->fetch_assoc();
                $existCnt = (int)$existRow['count'];
                $eventid = (int)$existRow['eventid'];

                $cnt = 1;
                // set any discounts
                foreach ($unpaidRows as $unpaidValueRow) {
                    // get the product from the unpaid row
                    $value = $unpaidValueRow['variationID'];

                    if ($value !== '') {
                        $useCnt = $cnt;

                        $discount = getActiveDisc($useCnt + $existCnt, $discounts, $athleteID, $eventid);
                        if (!is_null($discount)) {
                            $useSalePrice = returnDiscPrice($salePrice, $discount, 'S');
                            $useRegPrice = returnDiscPrice($regPrice, $discount, 'R');
                            $discountId = $discount['id'];
                        } else {
                            $useSalePrice = $salePrice;
                            $useRegPrice = $regPrice;
                            $discountId = 0;
                        }
//                        $row = $result->fetch_assoc();

                        $usePrice = updateWooCommercePrices($value, $useSalePrice, $useRegPrice, $saleDate);

                        $sql = 'update ' . E4S_TABLE_ENTRIES . '
                                set price = ' . $usePrice . ',
                                    discountid = ' . $discountId . '
                                where id = ' . $unpaidValueRow['id'];
                        e4s_queryNoLog($sql);
                        $cnt = $cnt + 1;
                    }
                }
            }
        }
    }
}

function updateWooCommercePrices($postid, $useSalePrice, $useRegPrice, $saleDate) {

    $usePrice = $useRegPrice;

    // Sale Price
    $sql = 'UPDATE ' . E4S_TABLE_POSTMETA . '
            SET meta_value = ' . $useSalePrice . '
            WHERE post_id = ' . $postid . "
            AND meta_key = '_sale_price'";
    e4s_queryNoLog($sql);

    // Reg Price
    $sql = 'UPDATE ' . E4S_TABLE_POSTMETA . '
            SET meta_value = ' . $useRegPrice . '
            WHERE post_id = ' . $postid . "
            AND meta_key = '_regular_price'";
    e4s_queryNoLog($sql);

    // Active Price
    if ($saleDate !== '') {
        $saleDate = strtotime($saleDate);
        $now = time();
        if ($saleDate > $now) {
            $usePrice = $useSalePrice;
        }
    }

    $sql = 'UPDATE ' . E4S_TABLE_POSTMETA . '
            SET meta_value = ' . $usePrice . '
            WHERE post_id = ' . $postid . "
            AND meta_key = '_price'";
    e4s_queryNoLog($sql);

    return $usePrice;
}