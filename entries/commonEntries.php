<?php
include_once E4S_FULL_PATH . 'eventteams/commonEventTeam.php';

function e4s_getAthleteSecurityForComp($compObj, $data) {
    $inArea = E4S_DEFAULT_ATHLETE_SECURITY; // default is there is no security

    if ($compObj->hasAthleteAreaSecurity()) {
        $inArea = $compObj->isAreaInAthleteSecurity((int)$data['countyid']);
        if (!$inArea) {
            $inArea = $compObj->isAreaInAthleteSecurity((int)$data['regionid']);
            if (!$inArea) {
                $inArea = $compObj->isAreaInAthleteSecurity((int)$data['countryid']);
            }
        }
    }
    $inClub = E4S_DEFAULT_ATHLETE_SECURITY;
    $inClub1 = TRUE;
    $inClub2 = FALSE;
    if ((int)$data['club2id'] > 0) {
        $inClub2 = TRUE;
    }
    $clubPublicDate = $compObj->hasAthleteClubSecurity();
    if ($clubPublicDate !== '') {
        $inClub = $compObj->isClubInAthleteSecurity((int)$data['clubid']);
        if (!$inClub) {
            $inClub1 = FALSE;
            if ($inClub2) {
                $inClub = $compObj->isClubInAthleteSecurity((int)$data['club2id']);
                if (!$inClub) {
                    $inClub2 = FALSE;
                }
            }
        }
    }

    $passedSecurity = TRUE;
    if ($inArea === E4S_DEFAULT_ATHLETE_SECURITY and $inClub === E4S_DEFAULT_ATHLETE_SECURITY) {
        $passedSecurity = TRUE;
    } elseif ($inArea === E4S_DEFAULT_ATHLETE_SECURITY) {
        $passedSecurity = $inClub;
    } elseif ($inClub === E4S_DEFAULT_ATHLETE_SECURITY) {
        $passedSecurity = $inArea;
    } elseif (!$inArea and !$inClub) {
        $passedSecurity = FALSE;
    }
    $returnObj = new stdClass();
    $returnObj->clubPublicDate = $clubPublicDate;
    $returnObj->inClub = $inClub;
    $returnObj->inClub1 = $inClub1;
    $returnObj->inClub2 = $inClub2;
    $returnObj->inArea = $inArea;
    $returnObj->allowed = $passedSecurity;

    return $returnObj;
}

function e4s_moveTrackPosition($obj) {
    $userid = e4s_getUserID();
    if ($userid === E4S_USER_NOT_LOGGED_IN) {
        Entry4UIError(5051, 'You are not authorised to call this process', 200, '');
    }
    $fromEntryId = checkFieldForXSS($obj, 'fromentryid:Entry ID' . E4S_CHECKTYPE_NUMERIC);
    $toEntryId = checkFieldForXSS($obj, 'toentryid:Entry ID' . E4S_CHECKTYPE_NUMERIC);
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $egId = checkFieldForXSS($obj, 'egid:Event Group ID' . E4S_CHECKTYPE_NUMERIC);
    $athleteId = checkFieldForXSS($obj, 'athleteid:Athlete ID' . E4S_CHECKTYPE_NUMERIC);
    $teamId = checkFieldForXSS($obj, 'teamid:Team ID' . E4S_CHECKTYPE_NUMERIC);
    $switchedAthleteId = checkFieldForXSS($obj, 'switchedathleteid:Switched Athlete ID' . E4S_CHECKTYPE_NUMERIC);
    $checkedIn = checkFieldForXSS($obj, 'checkedin:Using Checked-in values');

    if ((int)$checkedIn === 1) {
        $checkedIn = TRUE;
    } else {
        $checkedIn = FALSE;
    }
    $present = checkFieldForXSS($obj, 'present:Entity Present');
    $fromHeatNo = checkFieldForXSS($obj, 'fromheatno:From HeatNo' . E4S_CHECKTYPE_NUMERIC);
    $fromLaneNo = checkFieldForXSS($obj, 'fromlaneno:From LaneNo' . E4S_CHECKTYPE_NUMERIC);
    $toHeatNo = checkFieldForXSS($obj, 'toheatno:To HeatNo' . E4S_CHECKTYPE_NUMERIC);
    $toLaneNo = checkFieldForXSS($obj, 'tolaneno:To LaneNo' . E4S_CHECKTYPE_NUMERIC);
    $unseeded = checkFieldForXSS($obj, 'unseeded: UnSeeded');
    $payloadObj = new stdClass();
    $payloadObj->fromEntryId = (int)$fromEntryId;
    $payloadObj->toEntryId = (int)$toEntryId;
    $payloadObj->athleteId = (int)$athleteId;
    $payloadObj->teamId = (int)$teamId;
    $payloadObj->switchedAthleteId = (int)$switchedAthleteId;
    $payloadObj->present = $present;
    $payloadObj->checkedIn = $checkedIn;
    $payloadObj->fromHeatNo = (int)$fromHeatNo;
    $payloadObj->fromLaneNo = (int)$fromLaneNo;
    $payloadObj->toHeatNo = (int)$toHeatNo;
    $payloadObj->toLaneNo = (int)$toLaneNo;
    $payloadObj->compId = (int)$compId;
    $payloadObj->egId = (int)$egId;
    $payloadObj->unseeded = (int)$unseeded;

    $seedObj = new seedingV2Class($compId, $egId);
    $seedObj->moveTrackPosition($payloadObj);
}

function e4s_MarkTeamEntryPresent($obj){
	e4s_markAnyEntryPresent($obj, true);
}
function e4s_MarkEntryPresent($obj) {
	e4s_markAnyEntryPresent($obj);
}
function e4s_markAnyEntryPresent($obj, $team = false){
    $userid = e4s_getUserID();
    if ($userid === E4S_USER_NOT_LOGGED_IN) {
        Entry4UIError(5050, 'You are not authorised to call this process', 200, '');
    }
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (is_null($compId)) {
        $compId = 0;
    } else {
        $compId = (int)$compId;
    }
    $entryid = checkFieldForXSS($obj, 'entryid:Entry ID' . E4S_CHECKTYPE_NUMERIC);
    if (is_null($entryid)) {
        $entryid = checkFieldForXSS($obj, 'fromentryid:Entry ID' . E4S_CHECKTYPE_NUMERIC);
    }
    $present = checkFieldForXSS($obj, 'present:Athlete Present');

    if ($present === FALSE) {
        $present = 0;
    } else {
        $present = 1;
    }
    e4s_markEntriesPresent($compId, [$entryid], $present, 0, true, $team);

    Entry4UISuccess();
}

function e4s_markEventSeeded($egId) {
    $sql = 'select options
            from ' . E4S_TABLE_EVENTGROUPS . '
            where id = ' . $egId;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(9071, 'Unable to find event to mark seeded');
    }
    $obj = $result->fetch_object();
    $egOptions = e4s_addDefaultEventGroupOptions($obj->options);
    $egOptions->seed->seeded = TRUE;
    $egOptions = e4s_removeDefaultEventGroupOptions($egOptions);
    $sql = 'update ' . E4S_TABLE_EVENTGROUPS . "
            set options = '" . e4s_getOptionsAsString($egOptions) . "'
            where id = " . $egId;
    e4s_queryNoLog($sql);
}

function e4s_markEntriesPresent($compId, $entryIds, $present, $egId, $sendSocket,$team = false) {
    $checkedIn = 'checkedin';
    if ( $present === true or $present === 1 ){
        $checkedIn = 1;
    }
    if ( !$team ) {
	    $sql = 'update ' . E4S_TABLE_ENTRIES . '
            set present = ' . $present . ',
                checkedin = ' . $checkedIn . '
            where id in (' . implode( ',', $entryIds ) . ')';
    }else{
        $sql = 'update ' . E4S_TABLE_EVENTTEAMENTRIES . '
            set present = ' . $present . '
            where id in (' . implode( ',', $entryIds ) . ')';
    }

    e4s_queryNoLog($sql);
    $payload = new stdClass();
    $payload->entryIds = $entryIds;
    $payload->isTeam = $team;
    $payload->egId = $egId;
    $payload->present = $present;

    if ($sendSocket) {
        e4s_sendSocketInfo($compId, $payload, R4S_SOCKET_ENTRIES_PRESENT);
    }
}

function e4s_cancelEntry($obj) {
    $userid = e4s_getUserID();
    if ($userid === E4S_USER_NOT_LOGGED_IN) {
        Entry4UIError(5051, 'You are not authorised to call this process', 200, '');
    }
    $entryId = checkFieldForXSS($obj, 'entryid:Entry ID');
    $reason = checkFieldForXSS($obj, 'reason:Cancel Reason');
    $reason = preg_replace("~\n~", ', ', $reason);
    $sql = 'select *
            from ' . E4S_TABLE_ENTRYINFO. "
            where entryid = {$entryId}";
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(5060, 'Can not find Entry to cancel', 200, '');
    }
    $row = $result->fetch_assoc();
    if ((int)$row['paid'] !== E4S_ENTRY_PAID or (int)$row['userId'] < 1) {
        Entry4UIError(5055, 'Entry not in a position to be cancelled.', 200, '');
    }
    $options = e4s_getOptionsAsObj($row['eOptions']);
    $options->reason = str_replace("'",'`',$reason);
    $options->cancelUser = $userid;
    $options = e4s_removeDefaultEntryOptions($options);
    $sql = 'update ' . E4S_TABLE_ENTRIES . '
            set paid = ' . E4S_ENTRY_NOT_PAID . ",
                userid = -userid,
                athleteid = -athleteid,
                compeventid = -compeventid,
                options = '" . e4s_getOptionsAsString($options) . "'
            where id = " . $entryId;

    e4s_queryNoLog($sql);
	$payload = new stdClass();
	$payload->entryId = $entryId;
	$payload->isTeam = false;
	$payload->egId = $row['egId'];
    e4s_sendSocketInfo($row['compId'], $payload, R4S_SOCKET_ENTRY_CANCEL);

    Entry4UISuccess('');
}

function e4s_getUserFromNonce($nonce) {
    $sql = '
            select * 
            from ' . E4S_TABLE_ENTRYUSERCODES . "
            where nonce = '" . $nonce . "'
        ";
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        return E4S_USER_NOT_LOGGED_IN;
    }
    $row = $result->fetch_assoc();
    return (int)$row['userid'];
}

function e4s_generateEntryEmailId($compid) {
    $sql = '
        delete from ' . E4S_TABLE_ENTRYUSERCODES . "
        where compid = {$compid}";
    e4s_queryNoLog($sql);

    $sql = '
        SELECT distinct(userid) userid
        FROM ' . E4S_TABLE_ENTRIES . ' e, 
             ' . E4S_TABLE_COMPEVENTS . " ce 
        WHERE   e.compeventid = ce.ID 
        and     ce.compid = {$compid} 
        and     e.paid = " . E4S_ENTRY_PAID;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows === 0) {
        return;
    }
    $rows = $result->fetch_all(MYSQLI_ASSOC);

    $insertSql = ' 
        Insert into ' . E4S_TABLE_ENTRYUSERCODES . ' (userid, compid, emailid)
        values';
    $insertRows = array();
    foreach ($rows as $row) {
        $insertRows[] = '(' . $row['userid'] . ",{$compid},'" . e4s_generateUserCompNonce($compid, $row['userid']) . "')";
    }
    $insertSql .= implode(',', $insertRows);
    e4s_queryNoLog($insertSql);
}

function e4s_updateUserNonce($id, $nonce) {
    $sql = '
        update ' . E4S_TABLE_ENTRYUSERCODES . "
        set nonce = '{$nonce}'
        where id = {$id}
    ";
    e4s_queryNoLog($sql);
}

function e4s_generateUserCompNonce($compid, $userid, $verify = '') {
    return md5($verify . '_' . $compid . '-' . $userid);
}

function e4s_generateOptionsLink() {
    $obj = $GLOBALS[E4S_ORDER_ITEM];

    if (!isset($obj->ceid) or !isset($obj->athleteid)) {
        return; // Not an actual entry
    }
    $compId = $obj->compid;
    $ceId = $obj->ceid;
    $athleteId = $obj->athleteid;

    $sql = 'select id entryId, clubId clubId
            from ' . E4S_TABLE_ENTRIES . '
            where compeventid = ' . $ceId . '
            and   athleteid = ' . $athleteId . '
            and paid = ' . E4S_ENTRY_PAID;
    $result = e4s_queryNoLog($sql);

    if ($result->num_rows === 1) {
        $row = $result->fetch_object();
        $clubId = $row->clubId;
        $entryId = $row->entryId;
        ?>
        <a href="<?php echo e4s_getProtocolAndDomain() ?>/#/actions/<?php echo $compId ?>/<?php echo $athleteId ?>/<?php echo $clubId ?>/<?php echo $entryId ?>"
           class="e4s_75 button">Entry Options</a>
        <?php
    }
    unset($GLOBALS[E4S_ORDER_ITEM]);
}

function e4s_processFailedOrders() {
// get list of unpaid entries
    $sql = '
        select e.id id,
               e.variationid variationId,
               e.orderid orderId,
               e.options options,
               e.paid paid
        from ' . E4S_TABLE_ENTRIES . ' e,
            ' . E4S_TABLE_POSTS . ' p
        where e.orderid > 0
        and e.paid = ' . E4S_ENTRY_NOT_PAID . "
        and e.userid > 0
        and p.id = e.orderid
        and p.post_status = '" . WC_ORDER_FAILED . "'
        order by orderId desc";
//    and options not like '%orderchecked\":true%'

    $entryResult = e4s_queryNoLog($sql);
    while ($entryObj = $entryResult->fetch_object()) {
        $origOrderId = $entryObj->orderId;
        $oiSql = '
            select order_item_id order_item_id
            from ' . E4S_TABLE_WCORDERITEMMETA . "
            where meta_key = '" . WC_POST_PRODUCT_ID . "' and meta_value = '" . $entryObj->variationId . "'
        ";
//            echo $oiSql;
//            echo "\n";
        $oiResult = e4s_queryNoLog($oiSql);
        if ($oiResult->num_rows > 1) {
            $orderItemsIds = array();
            while ($oiObj = $oiResult->fetch_object()) {
                $orderItemsIds[] = $oiObj->order_item_id;
            }
            // get Orders
            $oSql = '
                select max(order_id) orderId
                from ' . E4S_TABLE_WCORDERITEMS . '
                where order_item_id in (' . implode(',', $orderItemsIds) . '  )
            ';
//                echo $oSql;
//                echo "\n";
            $oResult = e4s_queryNoLog($oSql);
            if ($oResult->num_rows === 1) {
                $oObj = $oResult->fetch_object();
//                echo "oObj->orderId : " . $oObj->orderId . "\n";
//                echo "entryObj->orderId : " . $entryObj->orderId . "\n";
                if ((int)$oObj->orderId > (int)$entryObj->orderId) {
                    $entryObj->orderId = $oObj->orderId;
                    // check latest order is complete
                    $pSql = '
                        select post_status postStatus
                        from ' . E4S_TABLE_POSTS . '
                        where id = ' . $entryObj->orderId;
//                        echo $pSql;
//                        echo "\n";
                    $pResult = e4s_queryNoLog($pSql);
                    if ($pResult->num_rows === 1) {
                        $pObj = $pResult->fetch_object();
                        if ($pObj->postStatus === WC_ORDER_PAID) {
//                            echo "Mark paid\n";
                            $entryObj->paid = E4S_ENTRY_PAID;
                        }
                    }
                }
            }
        }
        e4s_markEntryChecked($entryObj, $origOrderId);
    }
    Entry4UISuccess();
}

function e4s_markEntryChecked($entryObj, $origOrderId) {
    $options = e4s_getOptionsAsObj($entryObj->options);
    if ($entryObj->paid === E4S_ENTRY_PAID) {
        $options->paidCorrected = TRUE;
    }
    $options->entryChecked = TRUE;
    $options->originalOrderId = $origOrderId;
    $sql = '
        update ' . E4S_TABLE_ENTRIES . "
        set options = '" . e4s_getOptionsAsString($options) . "',
            orderid = " . $entryObj->orderId . ',
            paid = ' . $entryObj->paid . '
        where id = ' . $entryObj->id;
    e4s_queryNoLog($sql);
//    echo $sql;
}