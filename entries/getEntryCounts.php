<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'entries/commonEntries.php';
if (!isset($_GET['compid'])) {
    return;
}
$compid = $_GET['compid'];
$compObj = e4s_GetCompObj($compid);
$counters = $compObj->getEntryCounts();
$flyer = 'false';
$link = $compObj->getRow()['link'];
if ($link != '' and $link != '-') {
    $flyer = 'true';
}

$return = '{
             "counters" : ' . json_encode($counters) . ',
             "compFlyer" : ' . $flyer . '
           }';
echo $return;
