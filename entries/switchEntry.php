<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
// requires $ceid CE to move to
// $entryid entry being moved from

function e4s_switch($entryid, $ceid, $sendEmail = TRUE) {
    $entryRow = e4s_getEntryRow($entryid);
    $ceRow = e4s_getCERow($ceid);
    e4s_validate($entryRow, $ceRow);
    e4s_switchEntry($entryRow, $ceRow, $sendEmail);
}

function e4s_switchAthlete($entryid, $athleteid, $ceid) {
    $sql = 'select * 
            from ' . E4S_TABLE_ENTRIES . '
            where compeventid = ' . $ceid . '
            and athleteid = ' . $athleteid;

    $result = e4s_queryNoLog($sql);
    if ($result->num_rows > 0) {
        Entry4UIError(8500, 'Athlete already in this event.');
    }

    $sql = 'select firstname firstName,
                   surname surName,
                   clubid clubId
            from ' . E4S_TABLE_ATHLETE . '
            where id = ' . $athleteid;
//    var_dump($sql);
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(8501, 'Unable to find athlete');
    }
    $athleteObj = $result->fetch_object();

    $sql = 'select athleteid athleteId,
                   options options,
                   paid paid
            from ' . E4S_TABLE_ENTRIES . '
            where id = ' . $entryid;
//    var_dump($sql);
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(8502, 'Unable to find entry');
    }
    $entryObj = $result->fetch_object();
    if ((int)$entryObj->paid !== E4S_ENTRY_PAID) {
        Entry4UIError(8503, 'Entry is not marked as paid.');
    }
    $options = e4s_getOptionsAsObj($entryObj->options);
    $sql = 'select ag.name eventAgeGroup
            from ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_AGEGROUPS . ' ag
            where ce.id = ' . $ceid . '
            and   ag.id = ce.agegroupid';
//    var_dump($sql);
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(8503, 'Unable to find CE record');
    }
    $agObj = $result->fetch_object();
    $options->reason = 'Switched from athlete ' . $entryObj->athleteId;
    $options->switchUser = e4s_getUserID();
    $sql = 'update ' . E4S_TABLE_ENTRIES . '
            set athleteid = ' . $athleteid . ",
                athlete = '" . $athleteObj->firstName . ' ' . $athleteObj->surName . "',
                compeventid = " . $ceid . ',
                clubid = ' . $athleteObj->clubId . ",
                eventAgeGroup = '" . $agObj->eventAgeGroup . "',
                options = '" . e4s_encode($options) . "'
            where id = " . $entryid;
//    var_dump($sql);
    e4s_queryNoLog($sql);
    Entry4UISuccess();
}

function e4s_getEntryRow($id) {
    $sql = 'Select e.*, p.price, p.saleprice, ce.compid, ce.isopen, ce.maxgroup, ev.Name eventname, eg.name egName
            from ' . E4S_TABLE_ENTRIES . ' e,
                 ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTS . ' ev,
                 ' . E4S_TABLE_EVENTGROUPS . ' eg,
                 ' . E4S_TABLE_EVENTPRICE . ' p
            where ce.priceid = p.id
            and   e.compeventid = ce.id
            and   eg.id = ce.maxgroup 
            and   ce.eventid = ev.id
            and   e.id = ' . $id;

    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(9052, 'Failed to get Entry', 400, '');
    }
    return $result->fetch_assoc();
}

function e4s_getCERow($ceid) {
    $sql = 'Select ce.*, p.price, p.saleprice, ev.Name eventname, eg.name egName
            from ' . E4S_TABLE_COMPEVENTS . ' ce,
                 ' . E4S_TABLE_EVENTS . ' ev,
                 ' . E4S_TABLE_EVENTGROUPS . ' eg,
                 ' . E4S_TABLE_EVENTPRICE . ' p
            where ce.priceid = p.id
            and   ce.eventid = ev.id
            and   eg.id = ce.maxgroup 
            and   ce.id = ' . $ceid;

    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(9053, '9001:Failed to get Competition Entry', 400, '');
    }
    return $result->fetch_assoc();
}

function e4s_validate($entryRow, $ceRow) {
    if (isE4SUser()) {
        return TRUE;
    }
    $compObj = e4s_GetCompObj($ceRow['CompID']);
    if ( $compObj->isOrganiser()) {
        return TRUE;
    }

    if ((float)$entryRow['saleprice'] < (float)$ceRow['saleprice']) {
        Entry4UIError(8020, 'Can not move to a more expensive Entry.', 400, '');
    }
    if ((float)$entryRow['price'] < (float)$ceRow['price']) {
        Entry4UIError(8021, 'Can not move to a more expensive Entry.', 400, '');
    }
    if ($entryRow['compid'] !== $ceRow['CompID']) {
        Entry4UIError(8022, 'Can not move to a different competition.', 400, '');
    }
    if ((int)$entryRow['paid'] === E4S_ENTRY_NOT_PAID) {
        Entry4UIError(8023, 'Entry not paid for.', 400, '');
    }
    if ((int)$ceRow['IsOpen'] === E4S_EVENT_CLOSED) {
        $compObj = e4s_GetCompObj($ceRow['CompID']);
        if (!$compObj->isOrganiser()) {
            Entry4UIError(8023, 'Can not move to a closed competition.', 400, '');
        }
    }
    return TRUE;
}

function e4s_emailSwitch($entryRow, $ceRow, $waitingPos) {

    $compObj = e4s_GetCompObj($ceRow['CompID']);

    if ($compObj->isOrganiser()) {
        return;
    }
    $contactEmail = E4S_SUPPORT_EMAIL;
    $contactName = 'E4S Support';
    $userEmail = E4S_SUPPORT_EMAIL;

    $curUserId = (int)e4s_getUserID();
    $entryUserId = (int)$entryRow['userid'];

    if (E4S_CURRENT_DOMAIN === E4S_UK_DOMAIN) {
        $contactEmail = $compObj->getCompContactEmail();

        if ($curUserId === $entryUserId) {
            $userEmail = $GLOBALS[E4S_WC_USER]->data->user_email;
            $contactName = $GLOBALS[E4S_WC_USER]->data->display_name;
        } else {
            $userInfo = e4s_getAccountForID($entryUserId);
            $contactName = $userInfo['display_name'];
            $userEmail = $userInfo['user_email'];
        }
    }

    $body = 'Dear ' . $contactName . '<br><br>';
    $body .= 'The following entry for ' . $entryRow['athlete'] . ' at ' . $compObj->getName() . ' has been switched :<br>';

    $body .= 'From : ' . $entryRow['egName'] . '<br>';
    $body .= 'To : ' . $ceRow['egName'] . '<br>';


    if (E4S_CURRENT_DOMAIN !== E4S_UK_DOMAIN) {
        $body .= 'Contact Email : ' . $contactEmail . '<br>';
        $body .= 'User Email : ' . $userEmail . '<br>';
    }

    $body .= Entry4_emailFooter();

    if ($contactEmail === E4S_SUPPORT_EMAIL) {
        $headers = Entry4_mailHeader('Support', FALSE);
    } else {
        $headers = Entry4_mailHeader('Support', TRUE);
        $headers[] = 'cc:' . $contactEmail;
    }
//    $headers = Entry4_mailHeader("Support", false);
//    $userEmail = 'paul.day@apiconsultancy.com';
    e4s_mail($userEmail, 'Athlete switched event notification', $body, $headers);
}

function e4s_switchEntry($entryRow, $ceRow, $sendEmail) {
    $newCeId = (int)$ceRow['ID'];
    $fromCeId = (int)$entryRow['compEventID'];
    $athleteId = (int)$entryRow['athleteid'];
    $options = $entryRow['options'];
    if ($options === '') {
        $options = new stdClass();
    } else {
        $options = json_decode($options);
    }
    // check PB for switched Event
    $sql = 'select *
            from ' . E4S_TABLE_ATHLETEPB . '
            where athleteid = ' . $athleteId . '
            and  eventid = ' . $ceRow['EventID'];
    $result = e4s_queryNoLog($sql);
    $newPB = 0;
    if ($result->num_rows === 1) {
        $pbObj = $result->fetch_object();
        $newPB = $pbObj->pb;
    }
    $countObj = e4s_getEntryCountObj($entryRow['compid']);
    $switched = new stdClass();
    $switched->fromEvent = $entryRow['egName'];
    $switched->toEvent = $ceRow['egName'];
    $options->switched = $switched;

    $waitingPos = $countObj->getAthletePositionforCE($athleteId, $newCeId);
    $options = e4s_removeDefaultEntryOptions($options);
    $sql = 'update ' . E4S_TABLE_ENTRIES . '
            set compeventid = ' . $newCeId . ',
                pb = ' . $newPB . ",
                options = '" . json_encode($options) . "',
                waitingPos = " . $waitingPos . ',
                periodStart = now()
            where id = ' . $entryRow['id'];
    e4s_queryNoLog($sql);

    if ($sendEmail) {
        e4s_emailSwitch($entryRow, $ceRow, $waitingPos);
    }

    $currentEntryPosObj = $countObj->getAthleteInfoForCE($athleteId, $fromCeId);

    // Is it closed with an entry limit
    if ((int)$entryRow['isopen'] === E4S_EVENT_CLOSED and $currentEntryPosObj->maxAthletes > 0) {
        $entryObj = $countObj->getAthleteEntryForCeId($athleteId, $fromCeId);

        // reopen the original event if the athlete is not on waiting list
        if ($entryObj->waitingPos === 0) {
            $sql = 'update ' . E4S_TABLE_COMPEVENTS . '
                set isopen = ' . E4S_EVENT_OPEN . '
                where compid = ' . $entryRow['compid'] . "
                and   maxgroup = '" . $entryRow['maxgroup'] . "'";
            e4s_queryNoLog($sql);
        }
    }

    // remove from seeding
    include_once E4S_FULL_PATH . 'seedings/seedings.php';
    e4s_removeAthleteFromEventSeedings($fromCeId, $athleteId);

    // send a socket message .......
}