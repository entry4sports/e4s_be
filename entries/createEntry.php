<?php
include_once E4S_FULL_PATH . 'entries/discounts.php';
//function e4s_createEntry($productId, $cOptions, $ceOptions, $priceRow, $useClubId, $teamId, $sourceId, $addToCart = TRUE) {
function e4s_createEntry($productObj, $addToCart = TRUE) {
    $productId = $productObj->productId;
    $cOptions = $productObj->cOptions;
    $ceOptions = $productObj->ceOptions;
    $priceRow = $productObj->priceRow;
    $compId = $productObj->compId;
    $perf = $productObj->perf;
    $compObj = e4s_GetCompObj($compId);
    $useClubId = 0;
    if ( isset($productObj->useClubId)) {
        $useClubId = $productObj->useClubId;
    }
    $teamId = 0;
    if ( isset($productObj->teamId)) {
        $teamId = $productObj->teamId;
    }
    $sourceId = 0;
    if ( isset($productObj->sourceId)) {
        $sourceId = $productObj->sourceId;
    }

    $meta = new stdClass();
    $prodPrice = $priceRow['usePrice'];
    $GLOBALS['useProdPrice'] = $prodPrice;
    $useUserId = e4s_getUserID();
    if ( isset($productObj->userId)) {
        // used by auto entries to add a product to another user ( put in their cart )
        $useUserId = $productObj->userId;
    }
    try {
        if ($productId === null || $productId === 0) {
            Entry4UIError(1025, 'Invalid Product ID past to Cart', 200);
        }
        if ( isset($productObj->wcProduct) ){
            $wcProduct = $productObj->wcProduct;
        }else{
            $postSQL = 'Select * 
            from ' . E4S_TABLE_POSTS . '
            where id = ' . $productId;
            $result = e4s_queryNoLog($postSQL);

            if ($result->num_rows !== 1) {
                Entry4UIError(1021, 'Invalid Product ID past to Cart', 200);
            }
            $wcProduct = $result->fetch_assoc();
        }

        $meta->postRecord = $wcProduct;
// get the content field from the record
// should have {"compid": 999,"athleteid": 999,"ceid": 99999,"compclubid":999,"locationid":999}
        $postContent = json_decode($wcProduct['post_content']);
        if ( is_null($postContent)){
            Entry4UIError(9688, 'Unable to decode product: ' . $wcProduct['post_content']);
        }
//    Get Athlete
        $athleteId = $postContent->athleteid;
        if (!is_null($athleteId)) {
            $athleteRow = null;
            if (isset($productObj->athleteRow) ){
                $athleteRow = $productObj->athleteRow;
            }else{
                $sql = 'SELECT * 
                FROM ' . E4S_TABLE_ATHLETE . ' 
                WHERE id = ' . $athleteId;
                $result = e4s_queryNoLog($sql);
                if ($result->num_rows == 1) {
                    $athleteRow = $result->fetch_assoc();
                    $athleteRow['firstName'] = formatAthleteFirstname($athleteRow['firstName']);
                    $athleteRow['surName'] = formatAthleteSurname($athleteRow['surName']);
                }
            }

            if (!is_null($athleteRow)) {
                $meta->athleteRecord = $athleteRow;
                $athlete = $athleteRow['firstName'] . ' ' . $athleteRow['surName'];
            } else {
                Entry4UIError(1000, 'Invalid athlete found for entry', 400, '');
            }
            if ( $useClubId === 0 ){
                $useClubId = (int)$athleteRow['clubid'];
            }
            if ($cOptions->school === TRUE) {
                $useClubId = (int)$athleteRow['schoolid'];
            }
        } else {
            Entry4UIError(1001, 'No athlete found for entry', 400, '');
        }

        // get Competition Event
        $ceId = $postContent->ceid;

        if ($ceId === '') {
            Entry4UIError(1100, 'Blank CompEvent ID passed', 400, '');
        }
        $ceObj = $compObj->getCEByCeID($ceId);

        if( isset($productObj->ceRow) ){
            $ceRow = $productObj->ceRow;
        }else{
            $ceRow = e4s_getDataAsType($ceObj,E4S_OPTIONS_ARRAY);
            $ceRow = e4s_convertCeArray($ceRow);
        }
        $egObj = $compObj->getEventGroupByEgId($ceObj->egId);
        $ceRow['eventNo'] = $egObj->eventNo;
        $ceRow['eventName'] = $egObj->name;

        if (is_null($ceRow)) {
            Entry4UIError(1100, 'Invalid CompEvent ID', 400, '');
        }

        $meta->ceRecord = $ceRow;

// get Club
        $clubRow = array();
        $clubRow['id'] = E4S_UNATTACHED_ID;

        if ($useClubId !== 0) {
            $sql = 'SELECT * 
                FROM ' . E4S_TABLE_CLUBS . ' 
                WHERE id = ' . $useClubId;
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows == 1) {
                $clubRow = $result->fetch_assoc();
            } else {
                Entry4UIError(1002, 'Invalid club for athlete : ' . $athleteId . '-' . $useClubId, 400, '');
            }
        }
        $meta->clubRecord = $clubRow;

//    get AgeGroup
        $ageGroupArr = getAgeGroupForCeId($ceRow['AgeGroupID'], $compId, $athleteRow['dob']);
        if ($ageGroupArr === null) {
            Entry4UIError(1003, 'Invalid age group for athlete : ' . $athleteId, 400, '');
        }

        $ageGroup = $ageGroupArr['ageGroup'];
        $vetAgeGroup = $ageGroupArr['vetAgeGroup'];
        if ($vetAgeGroup === null) {
            $vetAgeGroup = array();
            $vetAgeGroup['id'] = 0;
        }
        $meta->ageGroup = $ageGroupArr;
        $discountid = 0;
        // defined in the createProduct file. Poor coding and needs to be refactored

        if (!isset($ceOptions->isTeamEvent)) {
            $ceOptions->isTeamEvent = FALSE;
        }

        $checkDiscounts = TRUE;
        if (array_key_exists('applyDiscounts', $priceRow)) {
            $checkDiscounts = $priceRow['applyDiscounts'];
        }
        // team events dont currently attract discounts
        if ( $ceOptions->isTeamEvent ){
            $checkDiscounts = false;
        }

        if ($prodPrice !== 0 and $checkDiscounts) {
            $priceArray = applyDiscounts($athleteId, array($ceRow), array($productId), array(0), FALSE);
            $prodPrice = $priceArray[$productId]['usePrice'];
            $discountid = $priceArray[$productId]['discountid'];
        }
        // use Globals here as the price is not returned
        $GLOBALS['useProdPrice'] = $prodPrice;

        $checkSql = 'Select id id,
                            paid paid,
                            options
                 from ' . E4S_TABLE_ENTRIES . '    
                 where compEventID = ' . $ceId . '
                 and   athleteid = ' . $athleteId . '
                 and   variationid = ' . $productId . '
                 and   teamid = ' . $teamId;

        $result = e4s_queryNoLog($checkSql);
        $insertedEntryId = 0;
        $insertedEntryPaid = E4S_ENTRY_NOT_PAID;
        $bibInfo = null;

        if ($result->num_rows === 0) {
            // Entry does not already Exist
            $eOptions = e4s_getEntryDefaultOptions();
            if ( isset($productObj->resultsKey) ) {
                $eOptions->resultsKey = $productObj->resultsKey;
            }
			if ( isset($productObj->autoEntry) ) {
                $eOptions->autoEntry = $productObj->autoEntry;
            }
            $waitingPos = 0;
            $freeEntry = $compObj->allowOrgFreeEntry();
            if ( $prodPrice === 0 and !$freeEntry){
//                Check if the comp is autoPay for free entries
                $freeEntry = $compObj->markFreeEntryPaidCheck();
            }
            // Does the current user have any permissions set for this comp
            if ($freeEntry) {
                $eOptions->fee = TRUE;
                $prodPrice = 0;
                $insertedEntryPaid = E4S_ENTRY_PAID;
                $addToCart = FALSE;
                if (e4s_otdEntry($compObj, $ceRow['startdate'])) {
                    $eOptions->otd = TRUE;
                }
            } else {
                $waitingPos = $compObj->getAthleteWaitingPos($athleteId, $egObj->id);
            }
            if ( $waitingPos > 0 ) {
                $eOptions = entryClass::addWaitingPos($eOptions, $waitingPos);
            }
            $eOptionsForWriting = e4s_removeDefaultEntryOptions($eOptions);
            $eOptionsStr = e4s_getOptionsAsString($eOptionsForWriting);
            $teamBibNo = '';
            $entity = '';
            if ( isset($productObj->entity) ){
                $entity = $productObj->entity;
                if ( gettype($entity) === E4S_OPTIONS_OBJECT){
                    $entity = e4s_getEntityKey($entity->level ,$entity->id);
                }
            }

            $compClubInfo = $compObj->setClubComp(true);
            if (!is_null($compClubInfo)){
                // get next Bib Number
                $teamBibNo = $compClubInfo->getNextBibForEG($entity,$egObj->id, $ceRow);
            }
            $insertSQL = 'INSERT INTO ' . E4S_TABLE_ENTRIES . ' ( compEventID, athlete, athleteid, teambibno, pb, variationid, clubid, paid, price, discountid, agegroupid, vetagegroupid, eventAgeGroup, periodStart, created, options, teamid, entity, userid) VALUES 
                  (' . $ceId . ",'" . addslashes($athlete) . "'," . $athleteId . ",'" . $teamBibNo . "','" . $perf . "'," . $productId . ',' . $useClubId . ',' . $insertedEntryPaid . ',' . $prodPrice . ',' . $discountid . ',' . $ageGroup['id'] . ',' . $vetAgeGroup['id'] . ",'" . $ageGroup['Name'] . "'," . returnNow() . ',' . returnNow() . ",'" . $eOptionsStr . "'," . $teamId . ",'" . $entity . "'," . $useUserId . ')';
            if (e4s_queryNoLog($insertSQL) !== TRUE) {
                Entry4UIError(1004, 'Error inserting Entries for ' . $productId, 400, '');
            } else {
                $insertedEntryId = logInsert(FALSE);
            }

            if (is_null($compClubInfo) and $freeEntry) {
                // give new entry a bib if needed
                $bibInfo = e4s_addOTDBibs($compId, $athleteId);
                $bibInfo->teamBibNo = '';
            }
        } else {
            $obj = $result->fetch_object();
            $eOptions = e4s_addDefaultEntryOptions($obj->options);
            $insertedEntryId = (int)$obj->id;
            $insertedEntryPaid = (int)$obj->paid;
        }
        $meta->eOptions = $eOptions;
        $meta->bibInfo = $bibInfo;
        if ($addToCart) {
            $product_cart_id = WC()->cart->generate_cart_id($productId);
            $in_cart = WC()->cart->find_product_in_cart($product_cart_id);
            if (!$in_cart) {
                WC()->cart->add_to_cart($productId, 1);
            }
        }
        $retObj = new stdClass();
        $retObj->entryId = $insertedEntryId;
        $retObj->paid = $insertedEntryPaid;
        $retObj->meta = $meta;
        return $retObj;
//    Entry4_Commit();
    } catch (Exception $e) {
//    Entry4_Rollback();
        return 0;
    }
}

function e4s_otdEntry($compObj, $ceDate): bool {
    $otd = FALSE;

    if ($compObj->haveEntriesClosed()) {
        $today = date('d/m/Y');
        $ceDate = date('d/m/Y', strtotime($ceDate));
        if ($today === $ceDate) {
            $otd = TRUE;
        }
    }

    return $otd;
}
function e4s_convertCeArray($ceArr){
    $ceArr['EventID'] = $ceArr['eventId'];
    $ceArr['AgeGroupID'] = $ceArr['ageGroupId'];
    return $ceArr;
}