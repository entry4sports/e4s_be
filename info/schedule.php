<?php
require_once E4S_FULL_PATH . 'dbInfo.php';
function e4s_getInfoSchedule($compId){
	if ( $compId !== 482 and $compId !== 617 ){
		Entry4UIError(9001," Invalid competition");
	}

	$obj = new e4sCompInfo($compId,0);
	Entry4UISuccess($obj->getScheduleOutput());
}
function e4s_getInfoEvent($egId){
	$obj = new e4sCompInfo(0,$egId);
	Entry4UISuccess($obj->getEventOutput($egId));
}
function e4s_getInfoResults($egId){
	$obj = new e4sCompInfo(0,$egId);
	Entry4UISuccess($obj->getResultsOutput($egId));
}
class e4sCompInfo {
	var $egId;
	var $compObj;
	var $output;
	function __construct($compId = 0, $egId = 0){
		if ($compId > 0 ) {
			$this->compObj = e4s_getCompObj( $compId );
		}
		$this->egId = $egId;
		$this->output = new stdClass();
	}
	function getScheduleOutput(){
		$this->buildComp();
		$this->buildSchedule();
		return $this->output;
	}
	function buildComp(){
		$obj = new stdClass();
		$obj->id = $this->compObj->getID();
		$obj->name = $this->compObj->getName();
		$obj->date = $this->compObj->getDate();
		$obj->location = $this->compObj->getLocation();
		$this->output->competition = $obj;
	}
	function buildSchedule(){
		$obj = [];
		$eventGroups = $this->compObj->getEGObjs();
		foreach($eventGroups as $eventGroup){
			$egObj = new stdClass();
			$egObj->id = $eventGroup->id;
			$egObj->eventNo = $eventGroup->eventNo;
			$egObj->type = $eventGroup->typeNo;
			$egObj->name = $eventGroup->name;
			$egObj->startDate = $eventGroup->startDate;
			$obj[$egObj->id] = $egObj;
		}
		uasort($obj, 'sortEg');
		$this->output->schedule = $obj;
	}
	function getResultsOutput($egId){
		$sql = "select heatNo, lane, position,bibno, athlete, score
		        from " . E4S_TABLE_EVENTRESULTSHEADER . " rh,
		             " . E4S_TABLE_EVENTRESULTS . " rd
		        where rh.egid = " . $egId . "
		        and rd.resultheaderid = rh.id";
		$result = e4s_queryNoLog($sql);
		$results = [];
		while ($row = $result->fetch_object()){
			$row->result = resultsClass::getResultFromSeconds($row->score);
			$results[] = $row;
		}
		$this->output->results = $results;
		return $this->output;
	}
	function getEventOutput($egId){
		$sql = "select id, name eventName, startDate, typeNo type, eventNo, options
		        from " . E4S_TABLE_EVENTGROUPS . "
		        where id = " . $egId;
		$result = e4s_queryNoLog($sql);
		$egObj = $result->fetch_object(E4S_EVENTGROUP_OBJ);
		if ( $egObj->options->isTeamEvent ){
			$this->getTeamEntries($egId);
		}else{
			$this->getIndivEntries($egId);
		}
		unset($egObj->optionSet);
		unset($egObj->options);
		$this->output->event = $egObj;
		return $this->output;
	}
	function getTeamEntries($egId){
		$sql = "select e.id,clubname, name teamName, heatno, laneno
		        from " . E4S_TABLE_EVENTTEAMENTRIES . " e left join " . E4S_TABLE_COMPEVENTS . " ce on (e.ceid = ce.id and ce.maxgroup = " . $egId . "),
		             " . E4S_TABLE_SEEDING . " s ,
		             " . E4S_TABLE_CLUBS . " c
		        where c.id = e.entityid
		        and   s.eventgroupid = ce.maxgroup and s.athleteid = e.id";
		$result = e4s_queryNoLog($sql);
		$entries = [];
		while ($row = $result->fetch_object()){
			$entries[] = $row;
		}
		$this->output->entries = $entries;
	}
	function getIndivEntries($egId){
		$sql = "select entryId, bibNo, teamBibNo, clubname, firstname, surname, heatno, laneno
		        from " . E4S_TABLE_ENTRYINFO . " e left join " . E4S_TABLE_SEEDING . " s on (s.eventgroupid = e.egid and s.athleteid = e.athleteid)
		        where egid = " . $egId;
		$result = e4s_queryNoLog($sql);
		$entries = [];
		while ($row = $result->fetch_object()){
			if ( $row->teamBibNo !== "" ){
				$row->bibNo = $row->teamBibNo;
				unset($row->teamBibNo);
			}
			$entries[] = $row;
		}
		$this->output->entries = $entries;
	}
}
// Comparison function
function sortEg($a, $b) {
	if ($a == $b) {
		return 0;
	}
	return ($a->eventNo < $b->eventNo) ? -1 : 1;
}
