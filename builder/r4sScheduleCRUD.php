<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';
include_once E4S_FULL_PATH . 'builder/r4sDesignCRUD.php';
include_once E4S_FULL_PATH . 'builder/r4sScheduleEventCRUD.php';

function e4s_R4SScheduleCRUD($obj, $process) {
    $model = e4s_getR4SScheduleModel($obj, $process);

    switch ($model->process) {
        case E4S_CRUD_LIST:
            e4s_listR4SSchedules($model, TRUE);
            break;
        case E4S_CRUD_CREATE:
            e4s_createR4SSchedule($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readR4SSchedule($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_updateR4SSchedule($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteR4SSchedule($model, TRUE);
            break;
    }
}

function e4s_getR4SScheduleModel($obj, $process) {
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;

    $model->id = checkFieldForXSS($obj, 'id:R4S Schedule Id');
    $model->compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $model->startDt = checkFieldForXSS($obj, 'startdt:Start Date Time');
    $model->endDt = checkFieldForXSS($obj, 'enddt:End Date Time');
    $model->designId = checkFieldForXSS($obj, 'designid:Design Id' . E4S_CHECKTYPE_NUMERIC);
    $model->outputId = checkFieldForXSS($obj, 'outputid:Output Id' . E4S_CHECKTYPE_NUMERIC);
    $model->options = checkJSONObjForXSS($obj, 'options:Options');
    if (is_null($model->options)) {
        $model->options = new stdClass();
    } else {
        $model->options = e4s_getDataAsType($model->options, E4S_OPTIONS_OBJECT);
    }
    $model->events = checkJSONObjForXSS($obj, 'events:Events');
    if (!is_null($model->events)) {
        $model->events = e4s_getDataAsType($model->events, E4S_OPTIONS_OBJECT);
    }
    $model->full = checkFieldForXSS($obj, 'full:Full Request');

    e4s_addStdPageInfo($obj, $model);
    return $model;
}

function e4s_getR4SScheduleCRUD($key, $value, $mustExist) {
    $row = e4s_getR4SSchedule($key, $value, $mustExist);
    return $row;
}

function e4s_getR4SSchedule($key, $value, $mustExist) {
    return e4s_getCRUDRow(E4S_TABLE_R4SSCHEDULE, $key, $value, $mustExist);
}

function e4s_readR4SSchedule($model, $exit) {
    $row = e4s_getR4SSchedule('id', $model->id, TRUE);
    if ($exit) {
        Entry4UISuccess($row);
    }
    return $row;
}

function e4s_validateR4SScheduleEvents($model): bool {
    if (!isset($model->events)) {
        // non passed so carry on
        return TRUE;
    }
    $events = $model->events;
    // if updating, ignore as those that are to be created will, those with an exoisting id will be ignored
    // currently there is no update of schedule event just create/delete
    // if creating, the ids must be zero

    if ($model->process === E4S_CRUD_CREATE) {
        foreach ($events as $event) {
            if (isset($event->id)) {
                if ($event->id !== 0) {
                    Entry4UIError(7550, 'When creating a Schedule, the events should not exist');
                }
            }
        }
    }

    // check the number match the design
    $dModel = new stdClass();
    $dModel->id = $model->designId;

    $designObj = e4s_readR4SDesign($dModel, FALSE);

    if ($designObj->maxEvents === R4S_NO_EVENTS and sizeof($events) > 0) {
        Entry4UIError(7560, 'Design does not allow for events');
    }
    if ($designObj->maxEvents > sizeof($events)) {
        Entry4UIError(7570, 'Design requires at least ' . $designObj->maxEvents . ' events');
    }
    return TRUE;
}

function e4s_updateScheduleEvents($model) {
    // delete any existing events
    e4s_deleteR4SScheduleEventsForSchedule($model->id);
    if (!isset($model->events)) {
        // non passed so carry on
        return;
    }
    $events = $model->events;
    // if updating, ignore as those that are to be created will, those with an exoisting id will be ignored
    // currently there is no update of schedule event just create/delete
    // if creating, the ids must be zero

    foreach ($events as $event) {
        $seModel = new stdClass();
        $seModel->id = 0;
        $seModel->scheduleId = $model->id;
        $seModel->eventGroupId = $event->eventgroupid;
        e4s_createR4SScheduleEvent($seModel, FALSE);
    }
}

function e4s_createR4SSchedule($model, $exit) {
    $id = $model->id;
    $startDt = $model->startDt;
    $endDt = $model->endDt;
    $designId = $model->designId;
    $outputId = $model->outputId;
    $options = e4s_getDataAsType($model->options, E4S_OPTIONS_STRING);

    if ((int)$id !== 0) {
        Entry4UIError(7510, 'ID of non zero passed to create');
    }
    $retVal = e4s_validateR4SScheduleEvents($model);
    $sql = 'insert into ' . E4S_TABLE_R4SSCHEDULE . " (startdt, enddt, outputid, designid, options)
            values ('" . $startDt . "','" . $endDt . "'," . $outputId . ',' . $designId . ",'" . $options . "')";
    e4s_queryNoLog($sql);
    $model->id = e4s_getLastID();
    e4s_updateScheduleEvents($model);
    return e4s_readR4SSchedule($model, $exit);
}

function e4s_updateR4SSchedule($model, $exit) {
    $id = $model->id;
    $startDt = $model->startDt;
    $endDt = $model->endDt;
    $designId = $model->designId;
    $outputId = $model->outputId;
    $options = e4s_getDataAsType($model->options, E4S_OPTIONS_STRING);

    e4s_getR4SSchedule('id', $id, TRUE);
    $retVal = e4s_validateR4SScheduleEvents($model);
    $sql = 'update ' . E4S_TABLE_R4SSCHEDULE . "
            set startdt ='" . $startDt . "',
                enddt ='" . $endDt . "',
                designid = " . $designId . ',
                outputid = ' . $outputId . ",
                options  = '" . $options . "'
            where id = " . $id;
    e4s_queryNoLog($sql);
    e4s_updateScheduleEvents($model);
    $row = e4s_getR4SSchedule('id', $id, TRUE);
    if ($exit) {
        Entry4UISuccess($row);
    }
    return $row;
}

function e4s_deleteR4SSchedule($model, $exit) {

    $id = $model->id;
    // check Row exists
    $row = e4s_getR4SSchedule('id', $id, TRUE);

    $sql = 'delete from ' . E4S_TABLE_R4SSCHEDULE . '
            where id = ' . $id;

    e4s_queryNoLog($sql);
    if ($exit) {
        Entry4UISuccess($row);
    }
    return $row;
}

function e4s_listR4SSchedules($model, $exit) {

    $pagesize = $model->pageInfo->pagesize;
    $page = $model->pageInfo->page;

    $usePaging = FALSE;
    if (isset($pagesize) and isset($page)) {
        $usePaging = TRUE;
    }

    if ($model->full) {
        $sql = '
            SELECT    s.*
                     ,e.eventgroupid
                     ,eg.name
                     ,d.design
                     ,d.description designdesc
                     ,o.output outputNo
                     ,o.description outputdesc
            FROM 
             ' . E4S_TABLE_R4SDESIGN . ' d,
             ' . E4S_TABLE_R4SSCHEDULE . ' s left join ' . E4S_TABLE_R4SSCHEDULEEVENT . ' e on (s.id = e.scheduleid)
              left join ' . E4S_TABLE_EVENTGROUPS . ' eg on (e.eventgroupid = eg.id),
             ' . E4S_TABLE_R4SOUTPUT . ' o
            where s.designid = d.id
            and s.outputid = o.id
            and o.compid = ' . $model->compId;
        if (!is_null($model->outputId)) {
            $sql .= ' and outputid = ' . $model->outputId;
        }
    } else {
        $sql = 'select   s.id
                            ,startdt
                            ,enddt
                            ,designid
                            ,outputid
            from ' . E4S_TABLE_R4SSCHEDULE . ' s';
        if (is_null($model->outputId)) {
            $sql .= ' where outputid in (
                            select distinct(o.id)
                            from ' . E4S_TABLE_R4SOUTPUT . ' o
                            where compid = ' . $model->compId . '
                         )';
        } else {
            $sql .= ' where outputid = ' . $model->outputId;
        }

    }

    $sql .= ' order by startdt ';

    if ($usePaging and $pagesize !== 0) {
        $sql .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
    }

    $result = e4s_queryNoLog($sql);
    $rows = array();
    while ($row = $result->fetch_object()) {
        if ($model->full) {
            if (!array_key_exists($row->id, $rows)) {
                $obj = new stdClass();
                $obj->id = $row->id;
                $obj->startdt = $row->startdt;
                $obj->enddt = $row->enddt;
                $design = new stdClass();
                $design->id = $row->designid;
                $design->designNo = $row->design;
                $design->description = $row->designdesc;
                $obj->design = $design;
                $output = new stdClass();
                $output->id = $row->outputid;
                $output->description = $row->outputdesc;
                if (isset($row->outputNo)) {
                    $output->outputNo = $row->outputNo;
                }
                $obj->output = $output;
                $obj->events = array();
                $rows[$obj->id] = $obj;
            }
            if (!is_null($row->eventgroupid)) {
                $event = new stdClass();
                $event->id = $row->eventgroupid;
                $event->name = $row->name;
                $rows[$obj->id]->events[] = $event;
            }
        } else {
            $rows[] = $row;
        }
    }
    if ($exit) {
        Entry4UISuccess($rows);
    }
    return $rows;
}
