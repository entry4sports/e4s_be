<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

function e4s_R4SOutputCRUD($obj, $process) {
    $model = e4s_getR4SOutputModel($obj, $process);

    switch ($model->process) {
        case E4S_CRUD_LIST:
            e4s_listR4SOutputs($model, TRUE);
            break;
        case E4S_CRUD_CREATE:
            e4s_createR4SOutput($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readR4SOutput($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_updateR4SOutput($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteR4SOutput($model, TRUE);
            break;
    }
}

function e4s_getR4SOutputModel($obj, $process) {
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;

    $model->id = checkFieldForXSS($obj, 'id:R4S Output Id');
    $model->compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $model->output = checkFieldForXSS($obj, 'output:R4S Output');
    $model->description = checkFieldForXSS($obj, 'description:R4S Output Description');
    if (is_null($model->description) or $model->description === '') {
        $model->description = 'Output to be defined';
    }
    $model->options = checkFieldForXSS($obj, 'options:R4S Output Options');
    if ($model->options === '' or is_null($model->options)) {
        $model->options = new stdClass();
    } else {
        $model->options = e4s_getOptionsAsObj($model->options);
    }

    e4s_addStdPageInfo($obj, $model);
    return $model;
}

function e4s_getR4SOutput($key, $value, $mustExist) {
    $row = e4s_getCRUDRow(E4S_TABLE_R4SOUTPUT, $key, $value, $mustExist);
    if ( array_key_exists('options', $row) ) {
        $row['options'] = e4s_getOptionsAsObj($row['options']);
    }
    return $row;
}

function e4s_readR4SOutput($model, $exit) {
    $row = e4s_getR4SOutput('id', $model->id, TRUE);
    if ($exit) {
        Entry4UISuccess($row);
    }
    return $row;
}

function e4s_getNextOutputNo($model) {
    $sql = 'select max(output) output
            from ' . E4S_TABLE_R4SOUTPUT . '
            where compid = ' . $model->compId;

    $result = e4s_queryNoLog($sql);
    if ($result->num_rows < 1) {
        return 1;
    }
    $row = $result->fetch_object();
    if (is_null($row->output)) {
        return 1;
    }
    $lastOutput = (int)$row->output;
    return $lastOutput + 1;
}

function e4s_createR4SOutput($model, $exit) {
    $id = $model->id;
    $output = e4s_getNextOutputNo($model);
    $description = $model->description;
    $compId = $model->compId;

    if ((int)$id !== 0) {
        Entry4UIError(7510, 'ID of non zero passed to create');
    }
    $sql = 'insert into ' . E4S_TABLE_R4SOUTPUT . ' (compid, description, output, options)
            values (' . $compId . ",'" . $description . "'," . $output . ",'" . e4s_encode($model->options) . "')";
    e4s_queryNoLog($sql);
    $model->id = e4s_getLastID();
    return e4s_readR4SOutput($model, $exit);
}

function e4s_updateR4SOutput($model, $exit) {
    $id = $model->id;
    $description = $model->description;
    $compId = $model->compId;

    e4s_getR4SOutput('id', $id, TRUE);
    $sql = 'update ' . E4S_TABLE_R4SOUTPUT . "
            set description ='" . $description . "',
                compid = " . $compId . ",
                options = '" . e4s_encode($model->options) . "'
            where id = " . $id;
    e4s_queryNoLog($sql);
    $row = e4s_getR4SOutput('id', $id, TRUE);
    if ($exit) {
        Entry4UISuccess($row);
    }
    return $row;
}

function e4s_deleteR4SOutput($model, $exit) {

    $id = $model->id;
    // check Row exists
    $row = e4s_getR4SOutput('id', $id, TRUE);

    $sql = 'delete from ' . E4S_TABLE_R4SOUTPUT . '
            where id = ' . $id;

    e4s_queryNoLog($sql);
    if ($exit) {
        Entry4UISuccess($row);
    }
    return $row;
}

function e4s_listR4SOutputs($model, $exit) {

    $startswith = $model->pageInfo->startswith;
    $pagesize = $model->pageInfo->pagesize;
    $page = $model->pageInfo->page;

    $usePaging = FALSE;
    if (isset($pagesize) and isset($page)) {
        $usePaging = TRUE;
    }

    $sql = 'select   id
                    ,compid
                    ,output
                    ,description
                    ,options
        from ' . E4S_TABLE_R4SOUTPUT . '
        where compid = ' . $model->compId;

    if (isset($startswith)) {
        $sql .= " and description like '" . $startswith . "%' ";
    }

    $sql .= ' order by output ';

    if ($usePaging and $pagesize !== 0) {
        $sql .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
    }

    $result = e4s_queryNoLog($sql);
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    $data = new stdClass();
    $outputs = array();
    foreach($rows as $row) {
        if ( array_key_exists('options', $row) ) {
            $row['options'] = e4s_getOptionsAsObj($row['options']);
        }
        $outputs[] = $row;
    }
    $data->outputs = $outputs;
    $compObj = e4s_GetCompObj($model->compId);
    $data->comp = $compObj->getRow();
    if ($exit) {
        Entry4UISuccess('"data":' . e4s_encode($data));
    }
    return $rows;
}
