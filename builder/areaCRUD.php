<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

function e4s_actionArea($obj, $process) {
    $model = e4s_getAreaModel($obj, $process);

    switch ($model->process) {
        case E4S_CRUD_LIST:
            e4s_listAreas($model, TRUE);
            break;
        case E4S_CRUD_CREATE:
            e4s_createArea($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readArea($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_updateArea($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteArea($model, TRUE);
            break;
    }
}

function e4s_getAreaModel($obj, $process) {
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;

    e4s_addStdPageInfo($obj, $model);
    return $model;
}

function e4s_returnStdAreaRow($row) {
    return $row;
}

function e4s_getAreaRowCRUD($key, $value, $mustExist) {
    $row = e4s_getAreaRow($key, $value, $mustExist);
    return e4s_returnStdAreaRow($row);
}

function e4s_getAreaRow($key, $value, $mustExist) {
    return e4s_getCRUDRow(E4S_TABLE_AREA, $key, $value, $mustExist);
}

function e4s_readArea($model, $exit) {
    $row = e4s_getAreaRow('id', $model->id, TRUE);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_createArea($model, $exit) {
Entry4UISuccess('','Create Area not implemented');
}

function e4s_updateArea($model, $exit) {
    $id = $model->id;
    $name = $model->name;

    $row = e4s_getAreaRow('id', $id, TRUE);
    $sql = 'update ' . E4S_TABLE_AREA . "
            set name ='" . $name . "'
            where id = " . $id;
    e4s_queryNoLog($sql);
    $row = e4s_getAreaRow('id', $id, TRUE);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_deleteArea($model, $exit) {
    $id = $model->id;
    // check Row exists
    $row = e4s_getAreaRow('id', $id, TRUE);
    $sql = 'select *
            from ' . E4S_TABLE_AREA . '
            where  id = ' . $id;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows > 0) {
        Entry4UIError(2003, 'Unable to delete as in use', 400, '');
    }
    $sql = 'delete from ' . E4S_TABLE_AREA . '
            where id = ' . $id;

    e4s_queryNoLog($sql);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_listAreas($model, $exit) {

    $startswith = $model->pageInfo->startswith;
    $pagesize = $model->pageInfo->pagesize;
    $page = $model->pageInfo->page;

    $usePaging = FALSE;
    if (isset($pagesize) and isset($page)) {
        $usePaging = TRUE;
    }

    $sql = 'select id, name
        from ' . E4S_TABLE_AREA;

    if (isset($startswith)) {
        $sql .= " where name like '" . $startswith . "%' ";
    }

    $sql .= ' order by name ';

    if ($usePaging and $pagesize !== 0) {
        $sql .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
    }

    $result = e4s_queryNoLog($sql);
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($rows, JSON_NUMERIC_CHECK));
    }
    return $rows;
}
