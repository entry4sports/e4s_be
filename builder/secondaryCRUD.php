<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

function e4s_actionSecondary($obj, $process) {
    $model = e4s_getSecondaryModel($obj, $process);
    new secondaryDefClass($model);
}

// for the actual variation product
function e4s_actionSecondaryVariation($obj, $process) {
    $model = e4s_getSecondaryModelVariation($obj, $process);
    Entry4UISuccess();
    new e4sProduct($model);
}

function e4s_purchaseProduct($obj) {
    $model = e4s_getSecondaryPurchaseModel($obj);
    $cart = new e4sCart();
    $cart->addToCart($model);
    Entry4UISuccess();
}

function e4s_getPurchasedProdModel($obj) {
    $prodobj = checkJSONObjForXSS($obj, 'prod:Product Object');
    $prod = null;
    if (!is_null($prodobj)) {
        $prod = new stdClass();
        if (array_key_exists('id', $prodobj)) {
            $prod->id = (int)$prodobj['id'];
        }
        if (array_key_exists('parentId', $prodobj)) {
            $prod->parentId = (int)$prodobj['parentId'];
        }
    }
    return $prod;
}

function e4s_getSecondaryAthletePurchases($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (is_null($compId)) {
        Entry4UIError(5010, 'Unable to get purchases due to no competition');
    }
    $athleteId = checkFieldForXSS($obj, 'athleteid:Athlete ID' . E4S_CHECKTYPE_NUMERIC);
    if (is_null($athleteId)) {
        Entry4UIError(5020, 'Unable to get purchases due to no athlete');
    }
    $orderObj = e4sOrders::athleteID($compId, $athleteId);
    $prods = $orderObj->getProductsPurchasedForAthlete();
    Entry4UISuccess('
            "data":' . e4s_encode($prods));
}

function e4s_getSecondaryPurchases($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (is_null($compId)) {
        Entry4UIError(4010, 'Unable to get purchases');
    }

    $orderObj = new e4sOrders((int)$compId);
    $prods = $orderObj->getProductsPurchased();
    Entry4UISuccess('
            "data":' . e4s_encode($prods));
}

function e4s_getProdModel($obj) {

    $prodobj = checkJSONObjForXSS($obj, 'prod:Product Object');
    $prod = new stdClass();
    $prod->id = 0;
    $prod->perAcc = checkJSONObjForXSS($obj, 'perAcc:Per AthleteOrUser');
    $prod->parentId = 0;
    $prod->stockQty = 0;
    $prod->menuOrder = 1;
    $prod->name = '';
    $prod->image = '';
    $prod->description = '';

    $price = new stdClass();
    $price->price = 0;
    $price->salePrice = 0;
    $price->saleEndDate = '';

    if (!is_null($prodobj)) {
        if (array_key_exists('id', $prodobj)) {
            $prod->id = (int)$prodobj['id'];
        }
        if (array_key_exists('stockQty', $prodobj)) {
            $prod->stockQty = (int)$prodobj['stockQty'];
        }
        if (array_key_exists('name', $prodobj)) {
            $prod->name = $prodobj['name'];
        }
        if (array_key_exists('image', $prodobj)) {
            $prod->image = $prodobj['image'];
        }
        if (array_key_exists('description', $prodobj)) {
            $prod->description = $prodobj['description'];
        }
        if (array_key_exists('virtual', $prodobj)) {
            $prod->virtual = $prodobj['virtual'];
        }
        if (array_key_exists('download', $prodobj)) {
            $prod->download = $prodobj['download'];
            // to get the download link, the prod has to be virtual ??????
            $prod->virtual = $prod->download;
        }
        $ticketObj = new stdClass();
        $ticketObj->ticketText = '';
        $ticketObj->ticketFormText = '';
        $ticketObj->dataReq = FALSE;
        if ($prod->download and array_key_exists('ticket', $prodobj)) {
            $prod->download = TRUE;
            $prod->virtual = TRUE;
            $ticketArr = $prodobj['ticket'];

            if (array_key_exists('ticketText', $ticketArr)) {
                $ticketObj->ticketText = $ticketArr['ticketText'];
            }
            if (array_key_exists('ticketFormText', $ticketArr)) {
                $ticketObj->ticketFormText = $ticketArr['ticketFormText'];
            }
            if (array_key_exists('dataReq', $ticketArr)) {
                $ticketObj->dataReq = $ticketArr['dataReq'];
            }
        }
        $prod->ticket = $ticketObj;
        $prod->parentId = 0;
        if (array_key_exists('parentId', $prodobj)) {
            $prod->parentId = (int)$prodobj['parentId'];
        }

        if (array_key_exists('menuOrder', $prodobj)) {
            $menuOrder = (int)$prodobj['menuOrder'];
            if ($menuOrder > 1) {
                $prod->menuOrder = $menuOrder;
            }
        }
        if (array_key_exists('price', $prodobj)) {
            $priceArr = $prodobj['price'];
            if (array_key_exists('price', $priceArr)) {
                $price->price = (float)$priceArr['price'];
            }
            if (array_key_exists('salePrice', $priceArr)) {
                $price->salePrice = (float)$priceArr['salePrice'];
            } else {
                $price->salePrice = $price->price;
            }
            if ($price->salePrice !== $price->price) {
                if (array_key_exists('saleEndDate', $priceArr)) {
                    $price->saleEndDate = $priceArr['saleEndDate'];
                }
            }
        }

        if (array_key_exists('attributes', $prodobj)) {
            $prod->attributes = $prodobj['attributes'];
        }

//        if ( e4s_isPerAcc($perAcc , E4S_PERACC_ATHLETE) ){
//            // Ensure there is an attribute for athletes
//
//            if ( !isset($prod->attributes)){
//                $prod->attributes = array();
//            }
//            $prod->attributes = e4s_ensureAthleteInAttributes($prod->attributes);
//        }
        if (array_key_exists('attributeValues', $prodobj)) {
            $prod->attributeValues = $prodobj['attributeValues'];
        }
//        if ( $prod->parentId > 0 ){
//            $prod->attributeValues = e4s_checkParentForAttributes($prod);
//        }
    }
    $prod->price = $price;

    return $prod;
}

function e4s_ensureAthleteInAttributes($attributeArray) {
    $athleteAttributeKey = null;
    $nameKey = 'name';
    $valueKey = 'values';
    $value = 'Please select athlete';

    foreach ($attributeArray as $key => $attribute) {
        if (strcasecmp($attribute[$nameKey], E4S_ATTRIB_ATHLETE)) {
            $athleteAttributeKey = $key;
            break;
        }
    }
    if (!is_null($athleteAttributeKey)) {
        $attributeArray[$athleteAttributeKey][$valueKey] = $value;
    } else {
        $attributeArray[] = array($nameKey => E4S_ATTRIB_ATHLETE, $valueKey => $value);
    }
    return $attributeArray;
}

function e4s_listOnlyMode($obj, &$model) {
    // Only for listing
    $model->compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (!is_null($model->compId)) {
        $model->compId = (int)$model->compId;
    } else {
        $model->compId = checkFieldForXSS($obj, 'compId:Competition ID' . E4S_CHECKTYPE_NUMERIC);
        if (!is_null($model->compId)) {
            $model->compId = (int)$model->compId;
        } else {
            $model->compId = 0;
        }
    }
    $model->objType = checkFieldForXSS($obj, 'objtype:Object Type');
    if (is_null($model->objType)) {
        $model->objType = '';
    }
    $model->objId = checkFieldForXSS($obj, 'objid:Object ID');
    if (is_null($model->objId) or $model->objType === E4S_SECONDREF_SYSTEM) {
        $model->objId = 0;
    } else {
        $model->objId = (int)$model->objId;
    }
}

function e4s_getMaxAllowed($obj) {
    $maxAllowed = checkFieldForXSS($obj, 'maxAllowed:Maximum allowed to purchase');
    if (is_null($maxAllowed)) {
        $maxAllowed = 0;
    } else {
        $maxAllowed = (int)$maxAllowed;
    }
    return $maxAllowed;
}

function e4s_getSecondaryModel($obj, $process) {
    $model = new stdClass();
    $model->process = $process;
    $model->id = checkFieldForXSS($obj, 'id:SecondaryID');
    if (is_null($model->id)) {
        $model->id = checkFieldForXSS($obj, 'refid:SecondaryID');
        if (is_null($model->id)) {
            $model->id = 0;
        }
    }
    $model->id = (int)$model->id;
    $model->prod = e4s_getProdModel($obj);

    $model->perAcc = checkFieldForXSS($obj, 'perAcc:Per Account');
    if (is_null($model->perAcc)) {
        $model->perAcc = '';
    }

    $model->maxAllowed = e4s_getMaxAllowed($obj);

    // Only for listing
    e4s_listOnlyMode($obj, $model);

//    ignore any name sent
    $refid = 0;
    $refParm = checkJSONObjForXSS($obj, 'refObj:Reference Obj');
    $refObj = new stdClass();
    $refObj->id = $refid;
    $refObj->objId = 0;
    $refObj->objType = '';
    $refObj->objKey = '';
    $refObj->compId = 0;
    if (!is_null($refParm)) {
        if (array_key_exists('id', $refParm)) {
            $refObj->id = (int)$refParm['id'];
        }
        $refObj->compId = (int)$refParm['compId'];
        if ($model->compId === 0) {
            $model->compId = $refObj->compId;
        }
        $refObj->objId = (int)$refParm['objId'];
        $refObj->objType = $refParm['objType'];
    }
    if ($refObj->objType === E4S_SECONDREF_SYSTEM) {
        $refObj->objId = 1;
    }
    $model->refObj = $refObj;
    e4s_addStdPageInfo($obj, $model);

    return $model;
}

function e4s_getSecondaryModelVariation($obj, $process) {

    $model = new stdClass();
    $model->process = $process . 'Variation';

    if ($process !== E4S_CRUD_DELETE) {
        $model->prod = e4s_getProdModel($obj);
        $model->maxAllowed = e4s_getMaxAllowed($obj);

        e4s_listOnlyMode($obj, $model);

        e4s_addStdPageInfo($obj, $model);
    } else {
        $id = checkFieldForXSS($obj, 'prodid:Product ID');
        if (is_null($id)) {
            Entry4UIError(5000, 'No product id sent for deletion', 200, '');
        }
        $model->id = $id;
    }
    return $model;
}

function e4s_getSecondaryPurchaseModel($obj) {
    $prod = checkJSONObjForXSS($obj, 'prod:Product');
    $compid = 0;
    if (!is_null($prod) and array_key_exists('name', $prod)) {
        $compid = 179;
    }

    $model = new stdClass();
    $model->prod = e4s_getPurchasedProdModel($obj);
    $model->compId = $compid;
    $qtyRequired = checkJSONObjForXSS($obj, 'qtyRequired:Product Quantity');
    $model->qtyRequired = 0;
    if (!is_null($qtyRequired)) {
        $model->qtyRequired = (int)$qtyRequired;
    }
    $model->attributeValues = checkJSONObjForXSS($obj, 'attributeValues:Product Attributes');
    // check if the athlete attribute is required
    $checkAthlete = e4s_isAthleteRequiredInAttributeValues($model->prod);

    // We must be passed an athlete
    if ($checkAthlete) {
        $athlete = checkJSONObjForXSS($obj, 'athlete:Athlete');
        if (is_null($athlete)) {
            Entry4UIError(5010, 'No athlete passed for the purchased item');
        }
        if ((int)$athlete['id'] <= 0) {
            Entry4UIError(5020, 'No athlete id passed for the purchased item');
        }
        if ($athlete['name'] === '') {
            Entry4UIError(5030, 'No athlete name passed for the purchased item');
        }

        $model->attributeValues[E4S_ATTRIB_ATHLETE] = e4sProduct::getAthleteAttributeKey(FALSE, $athlete['id'], $athlete['name']);
    } else {
        unset($model->attributeValues[E4S_ATTRIB_ATHLETE]);
    }
    if ($compid !== 0) {
        $compObj = e4s_GetCompObj($compid);
        $options = $compObj->getOptions();
        if ($options->stadium !== '') {
            $seatObj = new e4sSeatingClass($model);
            $seatObj->checkSeatAvailablity();
        }
    }
    return $model;
}

function e4s_isAthleteRequiredInAttributeValues($prod) {
    $peracc = 'peracc';
    $sql = 'select peracc ' . $peracc . '
        from ' . E4S_TABLE_SECONDARYDEFS . '
        where prodid = ' . $prod->parentId;
    $result = e4s_queryNoLog($sql);

    if ($result->num_rows === 1) {
        $row = $result->fetch_assoc();
        return e4s_isPerAcc($row[$peracc], E4S_PERACC_ATHLETE);
    }
    return FALSE;
}