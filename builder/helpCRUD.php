<?php

include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

function e4s_actionHelp($obj, $process) {
    if ($process !== E4S_CRUD_READ) {
        if (!userHasPermission(PERM_ADMIN, null, null)) {
            // User does not have permission to build this comp
            Entry4UIError(5000, 'You are not authorised to use this function', 401, '');
        }
    }
    $model = e4s_getHelpModel($obj, $process);

    if ($model->id !== 0) {
        $model->obj = helpClass::withID($model->id);
    } elseif ($model->key !== '') {
        $model->obj = helpClass::withKey($model->key);
    } else {
        $model->obj = new helpClass();
    }

    switch ($model->process) {
        case E4S_CRUD_LIST:
            e4s_listHelpDocs($model, TRUE);
            break;
        case E4S_CRUD_CREATE:
            e4s_createHelpDoc($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readHelpDoc($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_updateHelpDoc($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteHelpDoc($model, TRUE);
            break;
    }
}

function e4s_readHelpDoc($model, $exit) {
    if (!is_null($model->obj)) {
        $row = $model->obj->getRow();

        if ($exit) {
            Entry4UISuccess('
            "data":' . e4s_encode($row));
        }
        return $row;
    }
    Entry4UIError(5010, 'Failed to get help for : ' . $model->key, 200, '');
}

function e4s_listHelpDocs($model, $exit) {
    $rows = $model->obj->getRows($model->pageInfo);
    if ($exit) {
        Entry4UISuccess('
            "data":' . e4s_encode($rows));
    }
    return $rows;
}

function e4s_validateHelpModel($model) {
    if ($model->id !== 0 and $model->process === E4S_CRUD_CREATE) {
        Entry4UIError(5020, 'ID of non zero passed to create', 200, '');
    }
    if ($model->id === 0 and $model->process !== E4S_CRUD_CREATE) {
        Entry4UIError(5025, 'ID of zero passed', 200, '');
    }
    if ($model->key === '' or is_null($model->obj)) {
        Entry4UIError(5030, 'Blank Key passed to create', 200, '');
    }
    if ($model->title === '') {
        Entry4UIError(5040, 'Blank Title passed to create', 200, '');
    }
    if ($model->data === '') {
        Entry4UIError(5050, 'Blank data passed to create', 200, '');
    }
    if ($model->type === '') {
        Entry4UIError(5060, 'Blank type passed to create', 200, '');
    }
}

function e4s_createHelpDoc($model, $exit) {
    $row = null;
    e4s_validateHelpModel($model);
    $row = $model->obj->create($model);
    if (!is_null($row)) {
        if ($exit) {
            Entry4UISuccess('
                "data":' . e4s_encode($row));
        }
    }
    return $row;
}

function e4s_updateHelpDoc($model, $exit) {
    $row = null;
    e4s_validateHelpModel($model);
    $row = $model->obj->update($model);
    if (!is_null($row)) {
        if ($exit) {
            Entry4UISuccess('
                "data":' . e4s_encode($row));
        }
    }
    return $row;
}

function e4s_deleteHelpDoc($model, $exit) {
    if ($model->id === 0) {
        Entry4UIError(5070, 'ID of zero passed', 200, '');
    }
    $sql = 'delete from ' . E4S_TABLE_HELP . '
            where id = ' . $model->id;
    e4s_queryNoLog($sql);
    if ($exit) {
        Entry4UISuccess('');
    }
    return TRUE;
}

function e4s_getHelpModel($obj, $process) {
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;
    $model->obj = null;
    $model->id = checkJSONObjForXSS($obj, 'id:Help ID');
    if (is_null($model->id)) {
        $model->id = (int)checkFieldFromParamsForXSS($obj, 'keyid:Help ID');
    }
    if (is_null($model->id)) {
        $model->id = 0;
    }
    $model->id = (int)$model->id;

    $model->key = checkJSONObjForXSS($obj, 'key:Help Key');
    if (is_null($model->key)) {
        $model->key = '';
    }
    $model->title = checkJSONObjForXSS($obj, 'title:Title');
    $model->data = checkJSONObjForXSS($obj, 'data:Data');
    $model->type = checkJSONObjForXSS($obj, 'type:Type');
    $model->preload = checkJSONObjForXSS($obj, 'preload:PreLoad for config');

    e4s_addStdPageInfo($obj, $model);

    return $model;
}