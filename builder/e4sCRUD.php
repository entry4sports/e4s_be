<?php
include_once E4S_FULL_PATH . 'builder/compCRUD.php';

function e4s_getBaseCRUDRow($table, $sql, $key, $mustExist) {
    $row = null;
    $result = e4s_queryNoLog($sql);
    if (!is_null($result->num_rows)) {
        $row = $result->fetch_assoc();
    }

    if (!is_null($mustExist)) {
        if ($mustExist) {
            if ($result->num_rows === null or $result->num_rows === 0) {
                Entry4UIError(2030, 'Invalid ' . $key . '(' . str_replace('Entry4_', '', $table) . ')', 400, '');
            }
//            $row = $result->fetch_assoc();
        }

        if (!$mustExist) {
//            if ($result->num_rows !== 0) {
//                Entry4UIError(2031, "Duplicate " . $key . "(" . str_replace("Entry4_", "", $table) . ")", 400, '');
//            }
        }
    }
    return $row;
}

function e4s_getCRUDRow($table, $key, $value, $mustExist, $delimeter = '') {
    return e4s_getmultiCRUDRow($table, $key, $value, $mustExist, $delimeter);
}

function e4s_getmultiCRUDRow($table, $key, $value, $mustExist, $delimeter) {
    if ($delimeter !== '') {
        $keys = explode($delimeter, $key);
        $values = explode($delimeter, $value);
        $sep = '';
        $sql = 'select *
            from ' . $table . '
            where ';

        foreach ($keys as $i => $v) {
            $sql .= $sep . $v . " = '" . $values[$i] . "'";
            $sep = ' and ';
        }
    } else {
        $sql = 'select *
            from ' . $table . '
            where ' . $key . " = '" . $value . "'";
    }
    $return = e4s_getBaseCRUDRow($table, $sql, $key, $mustExist);
    return $return;
}

function e4s_getStartsWith($model, $name) {
    if (isset($model->pageInfo) and isset($model->pageInfo->startswith)) {
        return ' ' . $name . " like '" . $model->pageInfo->startswith . "%' ";
    }
    return '';
}

function e4s_getPageLimit($model) {
    $usePaging = FALSE;
    if (isset($model->pageInfo->pagesize) and isset($model->pageInfo->page)) {
        $usePaging = TRUE;
    }

    if ($usePaging and $model->pageInfo->pagesize !== 0) {
        return ' limit ' . (($model->pageInfo->page - 1) * $model->pageInfo->pagesize) . ', ' . $model->pageInfo->pagesize;
    }
    return '';
}
