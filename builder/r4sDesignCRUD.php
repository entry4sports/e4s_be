<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

function e4s_R4SDesignCRUD($obj, $process) {
    $model = e4s_getR4SDesignModel($obj, $process);

    switch ($model->process) {
        case E4S_CRUD_LIST:
            e4s_listR4SDesigns($model, TRUE);
            break;
        case E4S_CRUD_CREATE:
            e4s_createR4SDesign($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readR4SDesign($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_updateR4SDesign($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteR4SDesign($model, TRUE);
            break;
    }
}

function e4s_getR4SDesignModel($obj, $process) {
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;

    $design = checkFieldForXSS($obj, 'design:R4S Design Type');
    $designId = checkFieldForXSS($obj, 'id:R4S Design Id');
    if ($process === E4S_CRUD_UPDATE) {
        if (is_null($design)) {
            Entry4UIError(7501, 'No design type');
        }
        if (is_null($designId)) {
            Entry4UIError(7510, 'No design identification');
        }
    }
    $model->design = $design;

    $designId = (int)$designId;
    if ($designId < 0) {
        Entry4UIError(7520, 'No valid design identification');
    }
    $model->id = $designId;
    $model->description = checkFieldForXSS($obj, 'description:R4S Design Description');
    if (is_null($model->description) or $model->description === '') {
        $model->description = 'Design to be defined';
    }
    $model->maxEvents = checkFieldForXSS($obj, 'maxEvents:R4S Design Max Events');
    if (is_null($model->maxEvents)) {
        $model->maxEvents = 0;
    }
    $model->options = checkJSONObjForXSS($obj, 'options:Options');
    if (is_null($model->options)) {
        $model->options = new stdClass();
    } else {
        $model->options = e4s_getDataAsType($model->options, E4S_OPTIONS_OBJECT);
    }
    e4s_addStdPageInfo($obj, $model);
    return $model;
}

function e4s_getR4SDesignCRUD($key, $value, $mustExist) {
    $row = e4s_getR4SDesign($key, $value, $mustExist);
    return $row;
}

function e4s_getR4SDesign($key, $value, $mustExist) {
    $row = e4s_getCRUDRow(E4S_TABLE_R4SDESIGN, $key, $value, $mustExist);
    $row['id'] = (int)$row['id'];
    $row['maxEvents'] = (int)$row['maxevents'];
    unset($row['maxevents']);
    return $row;
}

function e4s_readR4SDesign($model, $exit) {
    $row = e4s_getR4SDesign('id', $model->id, TRUE);
    if ($exit) {
        Entry4UISuccess($row);
    }

    return e4s_getDataAsType($row, E4S_OPTIONS_OBJECT);
}

function e4s_createR4SDesign($model, $exit) {
    $id = $model->id;
    $design = $model->design;
    $description = $model->description;
    $maxEvents = $model->maxEvents;
    $options = e4s_getDataAsType($model->options, E4S_OPTIONS_STRING);

    if ((int)$id !== 0) {
        Entry4UIError(2018, 'ID of non zero passed to create', 400, '');
    }
    $sql = 'insert into ' . E4S_TABLE_R4SDESIGN . ' (design, description, maxevents, options)
            values (' . $design . ",'" . $description . "'," . $maxEvents . ",'" . $options . "')";
    e4s_queryNoLog($sql);
    $model->id = e4s_getLastID();
    return e4s_readR4SDesign($model, $exit);
}

function e4s_updateR4SDesign($model, $exit) {
    $id = $model->id;
    $design = $model->design;
    $description = $model->description;
    $maxEvents = $model->maxEvents;
    $options = e4s_getDataAsType($model->options, E4S_OPTIONS_STRING);

    e4s_getR4SDesign('id', $id, TRUE);
    $sql = 'update ' . E4S_TABLE_R4SDESIGN . "
            set description ='" . $description . "',
                maxevents = " . $maxEvents . ',
                design = ' . $design . ",
                options = '" . $options . "'
            where id = " . $id;
    e4s_queryNoLog($sql);
    $row = e4s_getR4SDesign('id', $id, TRUE);
    if ($exit) {
        Entry4UISuccess($row);
    }
    return $row;
}

function e4s_deleteR4SDesign($model, $exit) {
    $id = $model->id;
    // check Row exists
    $row = e4s_getR4SDesign('id', $id, TRUE);

    $sql = 'delete from ' . E4S_TABLE_R4SDESIGN . '
            where id = ' . $id;

    e4s_queryNoLog($sql);
    if ($exit) {
        Entry4UISuccess($row);
    }
    return $row;
}

function e4s_listR4SDesigns($model, $exit) {

    $startswith = $model->pageInfo->startswith;
    $pagesize = $model->pageInfo->pagesize;
    $page = $model->pageInfo->page;

    $usePaging = FALSE;
    if (isset($pagesize) and isset($page)) {
        $usePaging = TRUE;
    }

    $sql = 'select   id
                    ,design
                    ,description
                    ,maxEvents
        from ' . E4S_TABLE_R4SDESIGN;

    if (isset($startswith)) {
        $sql .= " where description like '" . $startswith . "%' ";
    }

    $sql .= ' order by design ';

    if ($usePaging and $pagesize !== 0) {
        $sql .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
    }

    $result = e4s_queryNoLog($sql);
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    if ($exit) {
        Entry4UISuccess($rows);
    }
    return $rows;
}
