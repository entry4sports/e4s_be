<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

function e4s_actionAge($obj, $process) {
    $model = e4s_getAgeModel($obj, $process);

    switch ($model->process) {
        case E4S_CRUD_LIST . 'Def':
            e4s_returnDefaults($model, TRUE);
            break;
        case E4S_CRUD_LIST:
            e4s_listAges($model, TRUE);
            break;
        case E4S_CRUD_CREATE:
            e4s_createAge($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readAge($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_updateAge($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteAge($model, TRUE);
            break;
    }
}

function e4s_returnStdAgeRow($row) {
    if (is_null($row) === FALSE) {
        $row['name'] = e4s_getVetDisplay($row['Name'], null);
        unset ($row['Name']);
        $row['maxAge'] = (int)$row['MaxAge'];
        unset ($row['MaxAge']);
        $row['maxAtDay'] = (int)$row['AtDay'];
        unset ($row['AtDay']);
        $row['maxAtMonth'] = (int)$row['AtMonth'];
        unset ($row['AtMonth']);
        $row['maxAtYear'] = (int)$row['year'];
        unset ($row['year']);
        $row['minAtYear'] = (int)$row['minYear'];
        if (is_null($row['minAtYear'])) {
            $row['minAtYear'] = $row['maxAtYear'];
        }
        if (is_null($row['minAtMonth'])) {
            $row['minAtMonth'] = $row['maxAtMonth'];
        }
        if (is_null($row['minAtDay'])) {
            $row['minAtDay'] = $row['maxAtDay'];
        }
        unset ($row['minYear']);

        $row['options'] = e4s_getOptionsAsObj($row['options']);
    }
    return $row;
}

function e4s_getAgeRowCRUD($key, $value, $mustExist) {
    $row = e4s_getAgeRow($key, $value, $mustExist);
    return e4s_returnStdAgeRow($row);
}

function e4s_getAgeRow($keys, $values, $mustExist) {
    return e4s_getCRUDRow(E4S_TABLE_AGEGROUPS, $keys, $values, $mustExist);
}

function e4s_getAgeShortName($name) {
    return $name;
}

function e4s_readAge($model, $exit) {
    $row = e4s_getAgeRowCRUD('id', $model->id, TRUE);
    $row['shortName'] = e4s_getAgeShortName($row['name']);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_createAge($model, $exit) {
    $compid = $model->compid;
    $config = e4s_getConfig();
    $aocode = $config['defaultao']['code'];
    $options = $model->options;
    $addOptions = TRUE;
//    $updateOptions = false;
    foreach ($options as $option) {
        if ($option['aocode'] === $aocode) {
            $addOptions = FALSE;
            if (array_key_exists('compids', $option)) {
//                $updateOptions = true;
                foreach ($option['compids'] as $agcompid) {
                    if ((int)$agcompid === $compid) {
//                        $updateOptions = false;
                        $option['compids'][] = $compid;
                    }
                }
            }
        }
    }
    if ($addOptions) {
        $newOption = new stdClass();
        $newOption->default = FALSE;
        $newOption->aocode = $aocode;
        $newOption->compids = array();
        $newOption->compids[] = $compid;
        $options[] = $newOption;
    }

    if ((int)$model->id !== 0) {
        Entry4UIError(2001, 'ID of non zero passed to create', 400, '');
    }
    e4s_getAgeRow('keyname', $model->name, FALSE);
    $sql = 'Insert into ' . E4S_TABLE_AGEGROUPS . " ( name, keyname, minAge, minatday, minatmonth, minyear, maxage, atday, atmonth, year, options )
            values(
                '" . $model->name . "',
                '" . $model->keyname . "',
                " . $model->minAge . ',
                ' . $model->minatday . ',
                ' . $model->minatmonth . ',
                ' . $model->minatyear . ',
                ' . $model->maxage . ',
                ' . $model->maxatday . ',
                ' . $model->maxatmonth . ',
                ' . $model->maxatyear . ",
                '" . json_encode($options) . "'
            )";

    e4s_queryNoLog($sql);
    $row = e4s_getAgeRowCRUD('keyname', $model->keyname, TRUE);

    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_updateAge($model, $exit) {
    $row = e4s_getAgeRowCRUD('id', $model->id, TRUE);
    $sql = 'update ' . E4S_TABLE_AGEGROUPS . "
            set name ='" . $model->name . "',
                keyname ='" . $model->keyname . "',
                minage =  " . $model->minage . ',
                minatday =' . $model->minatday . ',
                minatmonth =' . $model->minatmonth . ',
                minyear =' . $model->minatyear . ',
                maxage =' . $model->maxage . ',
                atday =' . $model->maxatday . ',
                atmonth =' . $model->maxatmonth . ',
                year =' . $model->maxatyear . ",
                options = '" . $model->options . "' 
            where id = " . $model->id;
    e4s_queryNoLog($sql);
    $row = e4s_getAgeRowCRUD('id', $model->id, TRUE);

    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_deleteAge($model, $exit) {
    // check Row exists
    $row = e4s_getAgeRowCRUD('id', $model->id, TRUE);
    $sql = 'select *
            from ' . E4S_TABLE_COMPEVENTS . '
            where  agegroupip = ' . $model->id;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows > 0) {
        Entry4UIError(2002, 'Unable to delete as in use', 400, '');
    }
    $sql = 'delete from ' . E4S_TABLE_AGEGROUPS . '
            where id = ' . $model->id;

    e4s_queryNoLog($sql);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_returnDefaults($model, $exit) {
    $compId = (int)$model->compid;
    $compObj = e4s_GetCompObj($compId);
    $compDate = $compObj->getDate();
    if (!isset($model->aocode) || $model->aocode === '') {
        $model->aocode = E4S_AOCODE_EA;
//        Entry4UIError(1000,"Parameters are incorrect", 400, '');
    }
    $useAoCode = $model->aocode;
    $sql = 'select *
        from ' . E4S_TABLE_AGEGROUPS;
    $result = e4s_queryNoLog($sql);
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    $newRows = array();

    foreach ($rows as $row) {
        if ($row['options'] === '') {
            continue;
        }
        $row = e4s_normaliseAgeRow($row);
        $options = e4s_getOptionsAsObj($row['options']);
        foreach ($options as $option) {
            if ($option->aocode === $useAoCode) {
                $forComp = FALSE;
                if (isset($option->compids)) {
                    foreach ($option->compids as $cid) {
                        if ($cid === $compId) {
                            $forComp = TRUE;
                            break;
                        }
                    }
                } else {
                    $forComp = TRUE;
                }
                if ($forComp) {
                    $newRow = e4s_returnStdAGRowWithDates($compDate, $row);
                    $newRows[] = $newRow;
                    break;
                }
            }
        }
    }

    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($newRows, JSON_NUMERIC_CHECK));
    }
    return $newRows;
}

function e4s_returnStdAGRowWithDates($compDate, $row) {
    $compDate = date_create($compDate);
    $compDay = (int)$compDate->format('d');
    $compMonth = (int)$compDate->format('m');
    $compYear = (int)$compDate->format('Y');
    $ageGroup = e4s_returnStdAgeRow($row);

    $maxAtDay = (int)$ageGroup['maxAtDay'];
    $minAtDay = (int)$ageGroup['minAtDay'];
    if ($minAtDay === Null) {
        $minAtDay = $maxAtDay;
    }
    $minAtDay = $minAtDay;

    if ($minAtDay === 0) {
        $minAtDay = $compDay;
    }
    if ($maxAtDay === 0) {
        $maxAtDay = $compDay;
    }

    $maxAtMonth = (int)$ageGroup['maxAtMonth'];
    $minAtMonth = (int)$ageGroup['minAtMonth'];
    if ($minAtMonth === Null) {
        $minAtMonth = $maxAtMonth;
    }

    if ($minAtMonth === 0) {
        $minAtMonth = $compMonth;
    }
    if ($maxAtMonth === 0) {
        $maxAtMonth = $compMonth;
    }

    $useMaxYear = $compYear + (int)$ageGroup['maxAtYear'];
    $useMinYear = $useMaxYear;
    if ($ageGroup['minAtYear'] !== Null) {
        $useMinYear = $compYear + (int)$ageGroup['minAtYear'];
    }
    $toDate = ($useMinYear - $row['minAge']) . '-' . $minAtMonth . '-' . $minAtDay;
    $ageGroup['toDate'] = date(E4S_SHORT_DATE, strtotime($toDate));
    $fromDate = ($useMaxYear - $row['MaxAge'] - 1) . '-' . $maxAtMonth . '-' . $maxAtDay;
    $ageGroup['fromDate'] = date(E4S_SHORT_DATE, strtotime($fromDate . ' + 1 days'));

    return $ageGroup;
}

function e4s_listAges($model, $exit) {

    $usePaging = FALSE;
    if (isset($model->pageInfo->pagesize) and isset($model->pageInfo->page)) {
        $usePaging = TRUE;
    }

    $sql = 'select *
        from ' . E4S_TABLE_AGEGROUPS;

    $joinDesc = ' where ';
    if (isset($model->pageInfo) and isset($model->pageInfo->startswith)) {
        $sql .= $joinDesc . " keyname like '" . $model->pageInfo->startswith . "%' ";
        $joinDesc = ' and ';
    }

    if (isset($model->compid) and (int)$model->compid > 0) {
        $sql .= $joinDesc . ' id in ( select distinct(agegroupid) from ' . E4S_TABLE_COMPEVENTS . ' where compid = ' . $model->compid . ' ) ';

        $joinDesc = ' and ';
    }

    $sql .= ' order by name ';
    if ($usePaging and $model->pageInfo->pagesize !== 0) {
        $sql .= ' limit ' . (($model->pageInfo->page - 1) * $model->pageInfo->pagesize) . ', ' . $model->pageInfo->pagesize;
    }

    $result = e4s_queryNoLog($sql);
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    $newRows = array();
    foreach ($rows as $row) {
        $newRows[] = e4s_returnStdAgeRow($row);
    }
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($newRows, JSON_NUMERIC_CHECK));
    }
    return $newRows;
}

function e4s_getCompAges($obj) {
    $compId = checkFieldForXSS($obj, 'compId:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $compObj = e4s_getCompObj($compId);
    $meta = new stdClass();
    $meta->compName = $compObj->getName();
    Entry4UISuccess('
        "data":' . json_encode(e4s_getAgesForComp($compId), JSON_NUMERIC_CHECK) . ',
        "meta": ' . json_encode($meta) . '
    ');
}

function e4s_getAgesForComp($compId) {
    $ageGroups = getAllCompDOBs($compId);
    $retVal = array();
    foreach ($ageGroups as $ageGroup) {
        $ageGroup['Name'] = e4s_getVetDisplay($ageGroup['Name'], '');
        $retVal[] = e4s_returnStdAgeRow($ageGroup);
    }
    return $retVal;
}

function e4s_listAgesForComp($model, $exit) {
    $ag = e4s_getAgesForComp($model->compid);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($ag, JSON_NUMERIC_CHECK));
    }
    return $ag;
}

function e4s_getAgeModel($obj, $process) {
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;

    $model->id = checkJSONObjForXSS($obj, 'id:Agegroup ID' . E4S_CHECKTYPE_NUMERIC);
    $model->compid = checkJSONObjForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (is_null($model->compid)) {
        $model->compid = checkJSONObjForXSS($obj, 'compId:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    }
    $model->aocode = checkJSONObjForXSS($obj, 'aocode:AO Code');
    $model->name = checkJSONObjForXSS($obj, 'name:Agegroup name');
    $model->keyname = checkJSONObjForXSS($obj, 'keyName:Agegroup name');
    $model->maxage = checkJSONObjForXSS($obj, 'maxAge:Max Age' . E4S_CHECKTYPE_NUMERIC);
    $model->maxatday = checkJSONObjForXSS($obj, 'maxAtDay:Max at Day' . E4S_CHECKTYPE_NUMERIC);
    $model->maxatmonth = checkJSONObjForXSS($obj, 'maxAtMonth:Max at Month' . E4S_CHECKTYPE_NUMERIC);
    $model->maxatyear = checkJSONObjForXSS($obj, 'maxAtYear:Max at Year' . E4S_CHECKTYPE_NUMERIC);
    $model->minAge = checkJSONObjForXSS($obj, 'minAge:Min Age' . E4S_CHECKTYPE_NUMERIC);
    $model->minatday = checkJSONObjForXSS($obj, 'minAtDay:Min at Day' . E4S_CHECKTYPE_NUMERIC);
    $model->minatmonth = checkJSONObjForXSS($obj, 'minAtMonth:Min at Month' . E4S_CHECKTYPE_NUMERIC);
    $model->minatyear = checkJSONObjForXSS($obj, 'minAtYear:Min at Year' . E4S_CHECKTYPE_NUMERIC);
    $options = checkJSONObjForXSS($obj, 'options:Age Group Options');
    if ($options === '' || is_null($options)) {
        $options = new stdClass();
    } else {
//        $options = json_decode($options);
        $options = e4s_getOptionsAsObj($options);
    }
    $model->options = $options;

    e4s_addStdPageInfo($obj, $model);

    return $model;
}

function e4s_getNumberParam($obj, $key) {
    $field = checkFieldFromParamsForXSS($obj, $key);
    $numReturned = 0;
    if (is_null($field)) {
        $numReturned = 0;
    }
    if ($field === '') {
        $numReturned = 0;
    }
    if ($numReturned !== 0) {
        $numReturned = (int)$numReturned;
    }
    return $numReturned;
}