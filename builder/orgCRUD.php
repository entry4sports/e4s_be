<?php
include_once E4S_FULL_PATH . 'dbInfo.php';

switch ($process) {
    case E4S_CRUD_LIST:
        $listObj = array();
        $listObj['pagesize'] = $pagesize;
        $listObj['page'] = $page;
        $listObj['startswith'] = $startswith;

        listOrgs($listObj);
        break;
    case E4S_CRUD_CREATE:
        createOrg($name);
        break;
    case E4S_CRUD_READ:
        readOrg();
        break;
    case E4S_CRUD_UPDATE:
        updateOrg();
        break;
    case E4S_CRUD_DELETE:
        deleteOrg();
        break;
}

function getRow($key, $value, $mustExist) {
    global $conn;

    $row = null;
    $sql = 'select * from ' . E4S_COMMON_PREFIX . 'CompClub
            where ' . $key . ' = ' . $value;
    $result = mysqli_query($conn, $sql);
    if ($mustExist) {
        if ($result->num_rows === null) {
            Entry4UIError(2015, 'Invalid ' . $key, 400, '');
        }
        $row = $result->fetch_assoc();
    }

    if (!$mustExist) {
        if ($result->num_rows !== 0) {
            Entry4UIError(2016, 'Duplicate ' . $key, 400, '');
        }
    }

    return $row;
}

function readOrg() {
    global $id;

    $row = getRow('id', $id, TRUE);
    Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
}

function createOrg($name) {
    global $conn;
    global $id;

    if ((int)$id !== 0) {
        Entry4UIError(2017, 'ID of non zero passed to create', 400, '');
    }
    getRow('club', '"' . $name . '"', FALSE);
    $sql = 'Insert into ' . E4S_COMMON_PREFIX . "CompClub ( club )
            values('" . $name . "')";
    logSql($sql, 1);
    mysqli_query($conn, $sql);
    $row = getRow('club', '"' . $name . '"', TRUE);
    $row['name'] = $row['club'];
    unset ($row['club']);
    Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
}

function updateOrg() {
    global $conn;
    global $id;
    global $name;
    $row = getRow('id', $id, TRUE);
    $sql = 'update ' . E4S_COMMON_PREFIX . "CompClub
            set name ='" . $name . "'
            where id = " . $id;
    mysqli_query($conn, $sql);
    $row['name'] = $name;
    Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
}

function deleteOrg() {
    global $id;
    global $conn;
    // check Row exists
    $row = getRow('id', $id, TRUE);
    $sql = 'select * from ' . E4S_PREFIX . 'Competition
            where  compclubid = ' . $id;
    $result = mysqli_query($conn, $sql);
    if ($result->num_rows > 0) {
        Entry4UIError(2011, 'Unable to delete as in use', 400, '');
    }
    $sql = 'delete from ' . E4S_COMMON_PREFIX . 'CompClub
            where id = ' . $id;

    mysqli_query($conn, $sql);
    Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
}

function listOrgs($listObj) {

    global $conn;

    $usePaging = FALSE;
    if (isset($listObj['pagesize']) and isset($listObj['page'])) {
        $usePaging = TRUE;
    }

    $sql = 'select id, club name
        from ' . E4S_COMMON_PREFIX . 'CompClub ';

    if (isset($listObj['startswith'])) {
        $sql .= "where club like '" . $listObj['startswith'] . "%' ";
    }

    $sql .= 'order by club ';

    if ($usePaging and $listObj['pagesize'] !== 0) {
        $sql .= ' limit ' . (($listObj['page'] - 1) * $listObj['pagesize']) . ', ' . $listObj['pagesize'];
    }

    logSql($sql, 1);
    $result = mysqli_query($conn, $sql);
    $orgs = $result->fetch_all(MYSQLI_ASSOC);
    Entry4UISuccess('
    "data":' . json_encode($orgs, JSON_NUMERIC_CHECK));
}