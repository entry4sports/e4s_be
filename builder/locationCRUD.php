<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

function e4s_actionLocation($obj, $process) {
    $model = e4s_getLocationModel($obj, $process);

    switch ($model->process) {
        case E4S_CRUD_LIST:
            e4s_listLocations($model, TRUE);
            break;
        case E4S_CRUD_CREATE:
            e4s_createLocation($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readLocation($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_updateLocation($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteLocation($model, TRUE);
            break;
    }
}

// declared in dbinfo ! DOH Why Need a location Class
//function e4s_returnStdLocationRow($row){
//    if (! is_null($row)){
//        $row['address1'] = addslashes($row['Address1']);
//        $row['address2'] = addslashes($row['Address2']);
//        $row['town'] = addslashes($row['Town']);
//        $row['county'] = addslashes($row['County']);
//        $row['name'] = addslashes($row['location']);
//        unset($row['Address1']);
//        unset($row['Address2']);
//        unset($row['Town']);
//        unset($row['County']);
//        unset($row['location']);
//    }
//    return $row;
//}

function e4s_getLocRowCRUD($key, $value, $mustExist) {
    $row = e4s_getLocRow($key, $value, $mustExist);
    $return = e4s_returnStdLocationRow($row);
    return $return;
}

function e4s_getLocRow($keys, $values, $mustExist) {
    return e4s_getCRUDRow(E4S_TABLE_LOCATION, $keys, $values, $mustExist);
}

function e4s_readLocation($model, $exit) {
    $row = e4s_getLocRowCRUD('id', $model->id, TRUE);
    if ($exit) {
        Entry4UISuccess('
        "data":' . e4s_encode($row));
    }
    return $row;
}

function e4s_createLocation($model, $exit) {
    $row = e4s_getLocRowCRUD('location', $model->name, FALSE);

    $sql = 'Insert into ' . E4S_TABLE_LOCATION . " ( location, postcode, website, directions,address1, address2, town, county, contact )
            values(
                '" . $model->name . "',
                '" . $model->postcode . "',
                '" . $model->website . "',
                '" . $model->directions . "',
                '" . $model->address1 . "',
                '" . $model->address2 . "',
                '" . $model->town . "',
                '" . $model->county . "',
                '" . $model->contact . "'
            )";
    e4s_queryNoLog($sql);

    $row = e4s_getLocRowCRUD('location', $model->name, TRUE);
    $auditObj = new e4sAudit();
    // reason format is important so audit can get the id !!!!!
    $auditObj->writeAudit('Created Location : ' . $row['id'], TRUE, E4S_TABLE_LOCATION, $row['id']);
    if ($exit) {
        Entry4UISuccess('
        "data":' . e4s_encode($row));
    }
    return $row;
}

function e4s_updateLocation($model, $exit) {
    $row = e4s_getLocRowCRUD('id', $model->id, TRUE);
    $auditObj = new e4sAudit();
    // reason format is important so audit can get the id !!!!!
    $auditObj->writeAudit('Update Location : ' . $model->id, TRUE, E4S_TABLE_LOCATION, $model->id);
    $sql = 'update ' . E4S_TABLE_LOCATION . "
                set location = '" . $model->name . "',
                postcode = '" . $model->postcode . "',
                website = '" . $model->website . "',
                directions = '" . $model->directions . "',
                address1 = '" . $model->address1 . "',
                address2 = '" . $model->address2 . "',
                town = '" . $model->town . "',
                county = '" . $model->county . "',
                contact = '" . $model->contact . "'
            where id = " . $model->id;
    e4s_queryNoLog('UPDATE' . E4S_SQL_DELIM . $sql);
    $row = e4s_getLocRowCRUD('id', $model->id, TRUE);
    if ($exit) {
        Entry4UISuccess('
        "data":' . e4s_encode($row));
    }
    return $row;
}

function e4s_deleteLocation($model, $exit) {

    // check Row exists
    $row = e4s_getLocRow('id', $model->id, TRUE);
    $sql = 'select * from ' . E4S_TABLE_COMPETITON . '
            where  locationid = ' . $model->id;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows > 0) {
        Entry4UIError(2520, 'Unable to delete as in use', 400, '');
    }
    $sql = 'delete from ' . E4S_TABLE_LOCATION . '
            where id = ' . $model->id;

    e4s_queryNoLog($sql);
    if ($exit) {
        Entry4UISuccess('
        "data":' . e4s_encode($row));
    }
    return $row;
}

function e4s_listLocations($model, $exit) {
    $pagesize = $model->pageInfo->pagesize;
    $page = $model->pageInfo->page;
    $startswith = $model->pageInfo->startswith;

    $usePaging = FALSE;
    if (isset($pagesize) and isset($page)) {
        $usePaging = TRUE;
    }

    $sql = 'select *
        from ' . E4S_TABLE_LOCATION;

    if (isset($startswith)) {
        $sql .= " where location like '" . $startswith . "%' ";
    }

    $sql .= ' order by location ';

    if ($usePaging and $pagesize !== 0) {
        $sql .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
    }

    $result = e4s_queryNoLog($sql);
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    $newRows = array();
    foreach ($rows as $row) {
        $newRows[] = e4s_returnStdLocationRow($row);
    }
    if ($exit) {
        Entry4UISuccess('
             "data":' . e4s_encode($newRows));
    }
    return $newRows;
}


function e4s_getLocationModel($obj, $process) {
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;
    $model->id = checkFieldForXSS($obj, 'id:Location ID');
    $model->location = addslashes(checkFieldFromParamsForXSS($obj, 'name:Location name'));
    $model->directions = addslashes(checkFieldFromParamsForXSS($obj, 'directions:Directions'));
    $model->website = checkFieldFromParamsForXSS($obj, 'website:Website');
    $model->address1 = addslashes(checkFieldFromParamsForXSS($obj, 'address1:Addres Line 1'));
    $model->address2 = addslashes(checkFieldFromParamsForXSS($obj, 'address2:Addres Line 2'));
    $model->town = addslashes(checkFieldFromParamsForXSS($obj, 'town:Town'));
    $model->county = addslashes(checkFieldFromParamsForXSS($obj, 'county:County'));
    $model->postcode = checkFieldFromParamsForXSS($obj, 'postcode:Post Code');
    $model->contact = addslashes(checkFieldFromParamsForXSS($obj, 'contact:Contact Information'));
    $model->name = addslashes(checkFieldForXSS($obj, 'name:Location name'));
    $model->pageInfo = new stdClass();
    $model->pageInfo->startswith = checkFieldForXSS($obj, 'startswith:Starts With');
    $model->pageInfo->page = checkFieldForXSS($obj, 'pagenumber:Page Number');
    $model->pageInfo->pagesize = checkFieldForXSS($obj, 'pagesize:Page Size');
    return $model;
}