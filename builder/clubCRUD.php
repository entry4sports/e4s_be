<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

function e4s_actionClub($obj, $process, $isSchool = false) {
    $model = e4s_getClubModel($obj, $process, $isSchool);

    switch ($model->process) {
        case E4S_CRUD_LIST:
            e4s_listClubs($model, TRUE);
            break;
        case E4S_CRUD_CREATE:
            e4s_createClub($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readClub($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_updateClub($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteClub($model, TRUE);
            break;
    }
}

/**
 * @param $obj
 * @param $process
 * @return stdClass
 */
function e4s_getClubModel($obj, $process, $isSchool): stdClass {
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;
    $model->isSchool = $isSchool;
    $model->id = checkFieldForXSS($obj, 'id:Club ID');
    $model->clubname = checkFieldForXSS($obj, 'clubname:Club Name');
    if (!is_null($model->clubname)) {
        $model->clubname = addslashes($model->clubname);
    }
    $model->clubtype = checkFieldForXSS($obj, 'clubtype:Club Type');
	if ( is_null($model->clubtype) ){
		$model->clubtype = checkFieldForXSS($obj, 'type:Club Type');
	}

    if ( $model->clubtype === 'S'){
        $model->isSchool = true;
    }
    $model->region = checkFieldForXSS($obj, 'region:Club Region');
    $model->country = checkFieldForXSS($obj, 'country:Club Country');
    $model->areaid = checkFieldForXSS($obj, 'areaid:Club AreaID');
    $model->active = checkFieldForXSS($obj, 'active:Club Active');
    $model->option = checkFieldForXSS($obj, 'option:Club Option Selection');
	if ( is_null($model->option) ){
		$model->option = '';
	}
    e4s_addStdPageInfo($obj, $model);
    return $model;
}

function e4s_returnStdClubRow($row) {
    return $row;
}

function e4s_getClubRowCRUD($key, $value, $mustExist) {
    $row = e4s_getClubRow($key, $value, $mustExist);
    return e4s_returnStdClubRow($row);
}

function e4s_getClubRow($key, $value, $mustExist) {
    $row = e4s_getCRUDRow(E4S_TABLE_CLUBS, $key, $value, $mustExist);
    if (!is_null($row)) {
        $row['clubname'] = $row['Clubname'];
        unset($row['Clubname']);
        $row['region'] = $row['Region'];
        unset($row['Region']);
        $row['country'] = $row['Country'];
        unset($row['Country']);
        if ($row['areaid'] !== '' and (int)$row['areaid'] !== 0) {
            $sql = 'select name
                    from ' . E4S_TABLE_AREA . '
                    where id = ' . $row['areaid'];
            $result = e4s_queryNoLog($sql);
            if ($result->num_rows !== 1) {
                $row['areaname'] = 'unknown';
            } else {
                $obj = $result->fetch_object();
                $row['areaname'] = $obj->name;
            }
        }
    }
    return $row;
}

function e4s_getClubReps($clubId) {
	$sql = "
		select u.id, u.user_login userLogin, u.user_email userEmail
		from " . E4S_TABLE_USERMETA . " um,
		     " . E4S_TABLE_USERS . " u
		where meta_key = '" . E4S_CLUB_ID . "' 
		and meta_value = " . $clubId . "
		and u.id = um.user_id;
	";
	$result = e4s_queryNoLog($sql);
	$rows = $result->fetch_all(MYSQLI_ASSOC);
	return $rows;
}
function e4s_readClub($model, $exit) {
    $row = e4s_getClubRow('id', $model->id, TRUE);
	$row['reps'] = e4s_getClubReps($model->id);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_createClub($model, $exit) {
    $id = $model->id;
    $clubName = $model->clubname;

    if ((int)$id !== 0) {
        Entry4UIError(2100, 'ID of non zero passed to create');
    }
    e4s_getClubRow('clubName', $clubName, FALSE);

    $sql = 'Insert into ' . E4S_TABLE_CLUBS . " ( clubname,region,country,areaid,clubtype,active)
            values (
                '" . addslashes($model->clubname) . "',
                '" . $model->region . "',
                '" . $model->country . "',
                " . $model->areaid . ",
                '" . $model->clubtype . "',
                " . $model->active . '
            )';
    e4s_queryNoLog($sql);
    $row = e4s_getClubRow('clubName', $clubName, TRUE);

    if ($exit) {
        Entry4UISuccess($row);
    }
    return $row;
}

function e4s_updateClub($model, $exit) {
    $id = $model->id;

    e4s_getClubRow('id', $id, TRUE);
    $sql = 'update ' . E4S_TABLE_CLUBS . "
            set clubname ='" . $model->clubname . "',
                areaid = " . $model->areaid . ",
                clubtype = '" . $model->clubtype . "',
                region = '" . $model->region . "',
                country = '" . $model->country . "',
                active = '" . $model->active . "'
            where id = " . $id;
    e4s_queryNoLog($sql);
    $row = e4s_getClubRow('id', $id, TRUE);
    if ($exit) {
        Entry4UISuccess($row);
    }
    return $row;
}

function e4s_deleteClub($model, $exit) {
    $id = $model->id;
    // check Row exists
    $row = e4s_getClubRow('id', $id, TRUE);
    $sql = 'select id
            from ' . E4S_TABLE_ATHLETE . '
            where clubid = ' . $id . ' 
            or    club2id = ' . $id;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows > 0) {
        Entry4UIError(2104, 'Unable to delete as in use');
    }
    $sql = 'delete from ' . E4S_TABLE_CLUBS . '
            where id = ' . $id;

    e4s_queryNoLog($sql);
    if ($exit) {
        Entry4UISuccess($row);
    }
    return $row;
}

function e4s_listClubs($model, $exit) {
    $startswith = $model->pageInfo->startswith;
    $pagesize = $model->pageInfo->pagesize;
    $page = $model->pageInfo->page;

    $usePaging = FALSE;
    if (isset($pagesize) and isset($page)) {
        $usePaging = TRUE;
    }

    $sql = 'select 	c.id id, 
                    c.clubname clubname,
                    c.region region,
                    c.country country,
                    c.areaid areaid,
                    a.name areaname,
                    c.active active,
                    c.options options,
					c.clubtype
        from ' . E4S_TABLE_CLUBS . ' c join ' . E4S_TABLE_AREA . ' a on c.areaid = a.id';

    if (isset($startswith)) {
        $sql .= " where clubname like '" . addslashes($startswith) . "%' ";
    }

    if ( $model->clubtype !== '') {
        $sql .= " and clubtype = '" . $model->clubtype . "'";
    }else{
		if ( $model->isSchool ){
			$sql .= " and clubtype = 'S'";
		}else {
			$sql .= " and clubtype = 'C'";
		}
    }
	if ($model->option !== ''){
		$sql .= ' and options like \'%' . $model->option . '":true%\'';
	}
    $sql .= ' order by clubname ';

    if ($usePaging and $pagesize !== 0) {
        $sql .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
    }
	e4s_addDebugForce($sql);
    $result = e4s_queryNoLog($sql);
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    if ($exit) {
        Entry4UISuccess($rows);
    }
    return $rows;
}
