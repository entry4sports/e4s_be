<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'competition/commonComp.php';
include_once E4S_FULL_PATH . 'eventteams/selfService.php';
include_once E4S_FULL_PATH . 'builder/eventGroupCRUD.php';
include_once E4S_FULL_PATH . 'athlete/checkin.php';
include_once E4S_FULL_PATH . 'admin/commonSecurity.php';
include_once E4S_FULL_PATH . 'classes/contactClass.php';

function e4s_getCompetitionInfo($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $compObj = e4s_GetCompObj($compId);
    Entry4UISuccess($compObj->getFullRow());
}

function e4s_isCompIDActive($compid, $stopIfActive = TRUE) {
    $compObj = e4s_GetCompObj($compid);
    return e4s_isCompActive($compObj, $stopIfActive);
}

function e4s_getCompEmails($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $compObj = e4s_GetCompObj($compId);
    Entry4UISuccess($compObj->getEntryEmails());
}

function e4s_isCompActive($compObj, $stopIfActive = TRUE) {
    $status = $compObj->isActive();
    if ($stopIfActive and $status) {
        if (e4s_isDevDomain()) {
            // dont audit Dev or test
            return FALSE;
        }
        if (isAdminUser()) {
            return FALSE;
        }
        Entry4UIError(5000, 'No changes allowed whilst Active. Please disable the competition, make changes and then re-enable. This is to allow the system to take backups should anything go wrong.', 200, '');
    }

    return $status;
}

function e4s_actionComp($obj, $process) {
    $model = e4s_getCompModel($obj, $process);
    $useOrg = null;
    if ($model->id === 0) {
	    // -1 for org means has the user got the perm and are creating the competition
        $useOrg = -1;
    }else {
	    if ( ! userHasPermission( PERM_BUILDER, $useOrg, $model->id ) ) {
		    // User does not have permission to build this comp
		    Entry4UIError( 5001, 'You are not authorised to use this function (' . $model->id . ')', 401, '' );
	    }
    }

    if ( $model->name === "" ){
        Entry4UIError(5003, 'Competition name cannot be blank');
    }
    switch ($model->process) {
        case E4S_CRUD_LIST:
//            e4s_listComps($model, true);
            break;
        case E4S_CRUD_CREATE:
            e4s_createComp($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readComp($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_updateComp($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteComp($model, TRUE);
            break;
        case 'Clone':
            e4s_cloneComp($model, TRUE);
            break;
        case 'Validate':
            $msgs = e4s_validateComp($model->id);
            Entry4UISuccess($msgs);
            break;
    }
}

function e4s_validateComp($compId) : array{
    if ( $compId < 1 ) {
        Entry4UIError(5002, 'Invalid Competition ID', 400, '');
    }
    $compValObj = new competitionValidation($compId);
    $msgs = $compValObj->validate();
    return $msgs;
}

function e4s_getCompMetaData(&$row, $model) {

    $meta = array();
    e4s_getTextBlobs($model, $row);

    $metaModel = new stdClass();
    $metaModel->compid = $model->id;
    include_once E4S_FULL_PATH . 'builder/compEventCRUD.php';
    $meta['compEvents'] = e4s_listCompEvents($metaModel, FALSE);
    include_once E4S_FULL_PATH . 'builder/priceCRUD.php';
    $meta['prices'] = e4s_listPrices($metaModel, FALSE);

    include_once E4S_FULL_PATH . 'builder/ageCRUD.php';
    $meta['ageGroups'] = e4s_listAgesForComp($metaModel, FALSE);

    include_once E4S_FULL_PATH . 'builder/discCRUD.php';
    $meta['discounts'] = e4s_listDiscs($metaModel, FALSE);

    include_once E4S_FULL_PATH . 'builder/compRuleCRUD.php';
    $meta['rules'] = e4s_listCompRules($metaModel, FALSE);

    $access = array();
    if (array_key_exists(PERM_BUILDER . PERM_LEVELS_SUFFIX, $GLOBALS)) {
        $access = $GLOBALS[PERM_BUILDER . PERM_LEVELS_SUFFIX];
    }
    $contactObj = new e4sContactClass($metaModel->compid);
    $row['options'] = $contactObj->getFullInfo($row['options']);
    $meta['access'] = $access;
    $row['meta'] = $meta;
    return $row;
}

function e4s_getTextBlobs($model, &$row) {
	$compId = $model->id;
	if ( !array_key_exists(E4S_CACHE_TEXT, $GLOBALS) ){
		$GLOBALS[E4S_CACHE_TEXT] = [];
	}
	if ( !array_key_exists($compId, $GLOBALS[E4S_CACHE_TEXT]) ) {
		$textObj            = new stdClass();
		$textObj->compNotes = notesClass::get( $compId, E4S_COMP_NOTES );

		$info              = notesClass::get( $compId, E4S_COMP_INFO );
		$textObj->compInfo = e4s_htmlToUI( $info );

		$info              = notesClass::get( $compId, E4S_COMP_NEWS );
		$textObj->compNews = e4s_htmlToUI( $info );

		$info               = notesClass::get( $compId, E4S_COMP_TANDC );
		$textObj->compTandC = e4s_htmlToUI( $info );

		$info                  = notesClass::get( $compId, E4S_COMP_EMAILTXT );
		$textObj->compEmailTxt = e4s_htmlToUI( $info );

		$textObj->options           = e4s_addDefaultCompOptions( $row['options'] );
		$info                       = notesClass::get( $compId, E4S_COMP_HOMEINFO );
		$textObj->options->homeInfo = e4s_htmlToUI( $info );

		$textObj->checkinObj = e4s_getCheckInObj( $compId );

		$GLOBALS[E4S_CACHE_TEXT][$compId] = $textObj;
	}
//Put into Row
	$textObj = $GLOBALS[E4S_CACHE_TEXT][$compId];
	if (isE4SUser()) {
		$row[E4S_COMP_NOTES] = $textObj->compNotes;
	}else{
		$row[E4S_COMP_NOTES] = '';
	}
	$row[E4S_COMP_INFO] = $textObj->compInfo;
	$row[E4S_COMP_NEWS] = $textObj->compNews;
	$row[E4S_COMP_TANDC] = $textObj->compTandC;
	$row[E4S_COMP_EMAILTXT] = $textObj->compEmailTxt;
	$textObj->checkinObj->addTextToOptions($textObj->options);
	$row['options'] = $textObj->options;
}

function e4s_getTextFromObj($obj, &$model) {
    notesClass::getValues($obj, $model);
}

function e4s_updateAndRemoveTextObjs(&$model) {
    notesClass::setText($model);
    $checkinObj = e4s_getCheckInObj($model->id);
    $checkinObj->setTextAndRemoveFromOptions($model->options);
    unset($model->options->homeInfo);
}

function e4s_readComp($model, $exit) {

    $compObj = e4s_GetCompObj($model->id);

    $row = e4s_getCompRowCRUD('id', $model->id, TRUE);
    $row = e4s_getCompMetaData($row, $model);
    $row['organisers'] = $compObj->getOrganisers();
    if ($compObj->isOrganiser()) {
        $pfObj = new stdClass();
        $pfObj->key = $compObj->getSecurityKey();
        $compObj->getID() . '/' . $pfObj->key;
        $row['pfInfo'] = $pfObj;
    }
    if ($exit) {
        Entry4UISuccess('
        "data":' . e4s_encode($row));
    }
    return $row;
}

function e4s_compOrgHasStripe($orgID) {
    $retval = 0;
    $sql = 'select *
            from ' . E4S_TABLE_COMPCLUB . '
            where id = ' . $orgID;
    $result = e4s_queryNoLog($sql);
//    var_dump($sql);
    if ($result->num_rows === 1) {
        $row = $result->fetch_assoc();
        $row['stripeuserid'] = (int)$row['stripeuserid'];
//	    var_dump("User : " . $row['stripeuserid'] . " . ". $retval);
        if ($row['stripeuserid'] > 0) {
            $stripeObj = new e4sStripeConnect($row['stripeuserid']);
//	        var_dump($stripeObj);
            if ($stripeObj->isApproved()) {
//	            var_dump("Approved");
                $retval = 1;
            } elseif ($stripeObj->isConnected()) {
//	            var_dump("Connected");
                $retval = -1;
            }else{
				$retval = -2; // new user/new org
            }
        }
    }
//	var_dump($retval);
//	exit;
    return $retval;
}

function e4s_createComp($model, $exit) {
    e4s_getCompRowCRUD('id', $model->id, FALSE);

    $config = e4s_getConfig();

    if (isset($config['options']->stripeMandatory)) {
        if ($config['options']->stripeMandatory) {
            $stripeStatus = e4s_compOrgHasStripe($model->comporg['id']);
            switch ($stripeStatus) {
                case 0:
                    // Not setup yet. This should not happen in new onboard where the new org will be linked to the user
                    Entry4UIError(3011, 'Please Set up Stripe for this Organisation before creating a competition (' . $model->comporg['id'] . ')', 200, '');
                    break;
                case -1:
                    // Connected but not approved
                    Entry4UIError(3012, 'Awaiting AAI Stripe approval for this Organisation before competitions can be activated', 200, '');
                    break;
	            case -2:
		            // New user/new org
		            break;
                case 1:
//                    National or Approved, so continue
                    break;
            }
        }
    }

    $model->options = e4s_setContactInfo($model->comporg['id'], $model);

    $compObj = e4s_GetCompObj(0);
    $shortCodeCheck = $compObj->isShortCodeOK($model->options);
    if ($shortCodeCheck !== '') {
        Entry4UIError(4679, $shortCodeCheck);
    }
    e4s_updateAndRemoveTextObjs($model);
    // new comps will not have a date set
    $model->options->compDate = $model->date;
    $model->options = $compObj->getOptionsForWriting($model->options);
    $options = e4s_getOptionsAsString($model->options);
    $options = str_replace("'", '', $options);
    $sql = 'Insert into ' . E4S_TABLE_COMPETITON . '(
        active,
        areaid,
        compclubid,
        date,
        entriesclose,
        entriesopen,
        indooroutdoor,
        link,
        locationid,
        name,
        options,
        teamid,
        yearfactor,
        r4s
    )
    values(
    false,
    ' . $model->area['id'] . ',
    ' . $model->comporg['id'] . ",
    '" . $model->date . "',
    '" . e4s_iso_to_sql($model->entriesclose) . "',
    '" . e4s_iso_to_sql($model->entriesopen) . "',
    '" . $model->indooroutdoor . "',
    '" . $model->link . "',
    " . $model->location['id'] . ",
    '" . addslashes($model->name) . "',
    '" . $options . "',
    " . '0' . ',
    ' . $model->yearfactor . ',
    ' . is_R4SCompetition() . '
    )';

    e4s_queryNoLog('CreateComp' . E4S_SQL_DELIM . $sql);
    $model->id = e4s_getLastID();
    if (is_R4SCompetition() === 1) {
        e4s_createFreeEntryForR4S($model->id);
    }
    $row = e4s_getCompRowCRUD('id', $model->id, TRUE);
    // add to the Entry creation to ensure it is created
    $compObj = e4s_GetCompObj($model->id);
    $compObj->set_product_category();
    $row = e4s_getCompMetaData($row, $model);
    if ($exit) {
        Entry4UISuccess('
        "data":' . e4s_encode($row));
    }
    return $row;
}

function e4s_removeUnusedEventGroups($compId) {
    $sql = 'delete from ' . E4S_TABLE_EVENTGROUPS . '
            where id not in ( select maxgroup from ' . E4S_TABLE_COMPEVENTS . ' ce where ce.CompID = ' . $compId . ')
            and compid = ' . $compId;
    e4s_queryNoLog($sql);
}
// test laptop bb
function e4s_updateComp($model, $exit) {
    $compId = $model->id;

	// for now, disable waiting lists.
	if ( $compId !== 644 and $compId !== 548 ) {
		if ( isset( $model->options->subscription ) ) {
			if ( isset( $model->options->subscription->enabled ) ) {
				if ( $model->options->subscription->enabled ) {
//					Entry4UIError( 8201, 'Waiting Lists are currently unavailable. Please disable.', 200, '' );
					$model->options->subscription->enabled = false;
				}
			}
		}
	}
    e4s_removeUnusedEventGroups($compId);
    e4s_generateEntryEmailId($compId);
    $checkinObj = e4s_getCheckInObj($compId);
    $compObj = e4s_GetCompObj($compId);
	$hasSecondary = false;
    if ( $model->active ){
        $hasIndiv = $compObj->hasIndivEvents();
        $hasSecondary = $compObj->hasSecondary();
        $hasTeam = $compObj->hasTeamEvents();
        if ( !$hasIndiv and !$hasSecondary and !$hasTeam ) {
            Entry4UIError(8201, 'Please create at least 1 event or secondary item before marking active', 200, '');
        }
    }

    $currentStatus = $compObj->isActive();
    $compObj->checkPaymentCode($model->options);

    $row = $compObj->getRow();
    if (is_null($row)) {
        Entry4UIError(9202, 'Error finding competition to update (' . $compId . ')', 200, '');
    }
//    $row = e4s_getCompRowCRUD("id", $model->id, true);
    $audit = e4sAudit::withRow($row);

    $shortCodeCheck = $compObj->isShortCodeOK($model->options);
    if ($shortCodeCheck !== '') {
        Entry4UIError(4788, $shortCodeCheck);
    }

    $newStatus = $model->active;

    e4s_writeSelfService($model->id, $model->options);
    $reason = '';
    $table = E4S_TABLE_COMPETITON;
    if ($newStatus) {
        $reason = 'Competition Updated';
    }
	$validationMsgs = [];
    if ($currentStatus !== $newStatus) {
        // ensure this audit is written
        $audit->setActive(TRUE);

        $reason = 'Activated';
        if (!$newStatus) {
            $reason = 'De-activated';
        } else {
	        $validationMsgs = e4s_validateComp($model->id);
            $compObj->informNewActiveComp();
//            Snapshot all tables if making active
            $table = null;
            if ($checkinObj->isEnabled()) {
                // get Codes for checkin if enabled
                $checkinObj->addCodesToOptions($model->options);
            }
        }
    }

    if ($reason !== '') {
        // Write audit if active and changed or status changed
        $audit->writeAudit($reason, TRUE, $table, null);
    }

    $model->options = e4s_setContactInfo($model->comporg['id'], $model);

    e4s_checkCompDate($row, $model);
	$compObj->checkEventGroupDates();
    $saleEndDate = $compObj->getSaleEndDate(TRUE, FALSE);
    if (is_null($saleEndDate)) {
        unset($model->options->saleEndDate);
    } else {
        $model->options->saleEndDate = $saleEndDate;
    }
    $compObj->checkAndUpdateWaitingStatus($model->options);
    $model->options = $compObj->getOptionsForWriting($model->options);

    // This should be moved to Comp Obj too
    e4s_updateAndRemoveTextObjs($model);

    $active = $model->active ? 'true' : 'false';
	$saveMsg = "";

	// validation msg are set if moving to active
	foreach ($validationMsgs as $msg){
		$saveMsg = "Saved with Messages";
		if ( $msg->type === E4S_MSG_ERROR ){
			$saveMsg = "Saved with Errors";
			if (!isE4SUser()) {
				// if there is a E4S_MSG_ERROR msg and not E4S, dont set active
				$active = 'false';
			}
			break;
		}
	}

	$type = is_null($row['type']) ? '-' : $row['type'];
	if ( $hasSecondary ){
		$type = E4S_COMP_TYPE_TICKET; // Tickets only
	}
    $sql = 'update ' . E4S_TABLE_COMPETITON . '
            set locationid = ' . $model->location['id'] . ',
               areaid = ' . $model->area['id'] . ',
               compclubid = ' . $model->comporg['id'] . ",
               date = '" . $model->date . "',
               indooroutdoor = '" . $model->indooroutdoor . "',
               name = '" . addslashes($model->name) . "',
               entriesopen = '" . e4s_iso_to_sql($model->entriesopen) . "',
               entriesclose = '" . e4s_iso_to_sql($model->entriesclose) . "',
               link = '" . $model->link . "',
               type = '" . $type . "',
               yearFactor = " . $model->yearfactor . ",
               options = '" . json_encode($model->options) . "',
               active = " . $active . '
            where id = ' . $model->id;
    e4s_queryNoLog($sql);

    $row = e4s_getCompRowCRUD('id', $model->id, TRUE);
    $row = e4s_getCompMetaData($row, $model);
	// add any validation messages to the meta
	$row['meta']['validation'] = $validationMsgs;
    $compObj->populateAthleteSecurity($row['options']);
    $audit->addToRow($row);

    if ($exit) {
        Entry4UISuccess('
        "data":' . e4s_encode($row), $saveMsg);
    }
    return $row;
}

function e4s_checkCompDate($row, $model) {
//    only do if there are no entries
    $sql = 'select count(e.entryid) cnt
                from ' . E4S_TABLE_ENTRYINFO . ' e            
                where e.CompID = ' . $model->id . '
            union
            select count(e.id)
                from ' . E4S_TABLE_EVENTTEAMENTRIES . ' e,
                     ' . E4S_TABLE_COMPEVENTS . ' ce  
                where e.ceid = ce.id
                and ce.CompID = ' . $model->id;

    $result = e4s_queryNoLog($sql);
    while ($countObj = $result->fetch_object()){
        if ( $countObj->cnt > 0 ) {
            // entries exist so dont update dates.
            return false;
        }
    }

    $oldDate = $row['Date'];
    $newDate = $model->date;
    if (strpos($newDate, 'T') > 0) {
        $newDate = explode('T', $newDate);
        $newDate = $newDate[0];
    }

    if ($oldDate === $newDate) {
        return;
    }
    $newDateDt = strtotime($newDate);
    $oldDateDt = strtotime($oldDate);
    $daysDiff = ($newDateDt - $oldDateDt) / 86400;

    // Comp date has changed so update all events accordingly
    $uSql = '
        update ' . E4S_TABLE_COMPEVENTS . '
        set startdate = date_add(startdate, interval ' . $daysDiff . ' day),
            sortdate = date_add(sortdate, interval ' . $daysDiff . ' day)
        where compid = ' . $model->id;
//    e4s_addDebug($uSql);
    e4s_queryNoLog($uSql);
    $uSql = '
        update ' . E4S_TABLE_EVENTGROUPS . '
        set startdate = date_add(startdate, interval ' . $daysDiff . ' day)
        where compid = ' . $model->id;
    e4s_queryNoLog($uSql);
}

function e4s_deleteComp($model, $exit) {
    $compObj = e4s_GetCompObj($model->id);
    $compObj->delete();
    Entry4UISuccess();
}

function e4s_listComp($model, $exit) {
}

function e4s_getCompRowCRUD($key, $value, $mustExist, $options = null) {
    $row = e4s_getCompRow($key, $value, $mustExist);

    if ($mustExist === FALSE and is_null($row)) {
        return $row;
    }
    if ($mustExist === TRUE and is_null($row)) {
        Entry4UIError(5004, 'Failed to get Competition :' . $value, 400, '');
    }

    $row['id'] = $row['ID'];
    unset($row['ID']);

    $row['date'] = $row['Date'];
    unset($row['Date']);

    $row['name'] = $row['Name'];
    unset($row['Name']);

    $row['indoorOutdoor'] = $row['IndoorOutdoor'];
    unset($row['IndoorOutdoor']);

//    $row['entriesOpenDateTime'] = str_replace(" ","T",$row['EntriesOpen']);
    $row['entriesOpenDateTime'] = e4s_sql_to_iso($row['EntriesOpen']);
    unset($row['EntriesOpen']);

//    $row['entriesCloseDateTime'] = str_replace(" ","T",$row['EntriesClose']);
    $row['entriesCloseDateTime'] = e4s_sql_to_iso($row['EntriesClose']);
    unset($row['EntriesClose']);

//    $row['reportLastChecked'] = str_replace(" ","T",$row['reportLastChecked']);
//    $row['reportLastChecked'] = e4s_sql_to_iso($row['reportLastChecked']);

    include_once E4S_FULL_PATH . 'builder/compClubCRUD.php';
    $ccRow = e4s_getCompClubRowCRUD('id', $row['compclubid'], TRUE);
    $row['compOrg'] = $ccRow;

    include_once E4S_FULL_PATH . 'builder/locationCRUD.php';
    $locRow = e4s_getLocRowCRUD('id', $row['locationid'], TRUE);
    $row['location'] = $locRow;

    $areaid = (int)$row['areaid'];
    $area = array();
    $area['id'] = $areaid;
    $area['name'] = '';

    if ($areaid !== 0) {
        include_once E4S_FULL_PATH . 'builder/areaCRUD.php';
        $areaRow = e4s_getAreaRowCRUD('id', $areaid, TRUE);
        $area['name'] = $areaRow['name'];
    }
    unset($row['areaid']);
    $row['area'] = $area;

    if (is_null($options)) {
        $compObj = e4s_GetCompObj($row['id'], FALSE);
        $options = e4s_getOptionsAsObj($row['options']);
        $compObj->populateAthleteSecurity($options);
    } else {
        $options = e4s_getOptionsAsObj($options);
    }
    e4s_readSelfService($row['id'], $options);
    $row['options'] = $options;
    if ($row['active'] === '1' or $row['active'] === TRUE or $row['active'] === 1) {
        $row['active'] = TRUE;
    } else {
        $row['active'] = FALSE;
    }
    $audit = e4sAudit::withRow($row);

    $audit->addToRow($row);
    return $row;
}

function e4s_getCompRow($key, $value, $mustExist) {
    $row = e4s_getCRUDRow(E4S_TABLE_COMPETITON, $key, $value, $mustExist);
    if (!is_null($row)) {
        $options = e4s_getOptionsAsObj($row['options']);
        $row['options'] = e4s_getOptionsAsStringNoNumCheck($options);
    }
    return $row;
}


function e4s_getCompModel($obj, $process) {
    global $tz;

    $params = $obj->get_params('JSON');
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;
    $model->overideValidation = checkFieldForXSS($obj, 'override:Override Validation');
    $model->id = checkFieldForXSS($obj, 'id:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (array_key_exists('active', $params)) {
        $model->active = $params['active'];
    }
    $model->date = checkFieldForXSS($obj, 'date:Competition Date');
    $model->name = checkFieldForXSS($obj, 'name:Competition name');
    $model->entriesopen = checkFieldForXSS($obj, 'entriesOpenDateTime:Entries open');
    $model->entriesclose = checkFieldForXSS($obj, 'entriesCloseDateTime:Entries close');
    $model->indooroutdoor = checkFieldForXSS($obj, 'indoorOutdoor:Indoor or Outdoor');

    if (!is_null($model->name)) {
        $model->name = trim($model->name);
    }
    if (!empty($model->date)) {
        $useDate = $model->date;
		if ( strpos($useDate, 'T') > 0 ) {
			$useDate = explode( 'T', $useDate )[0];
		}
        $compDate = DateTime::createFromFormat(E4S_SHORT_DATE, $useDate, $tz);
        $compYear = $compDate->format('Y');
    }

    if (array_key_exists('area', $params)) {
        $model->area = $params['area'];
    }

    if (array_key_exists('compOrg', $params)) {
        $model->comporg = $params['compOrg'];
    }

    if (array_key_exists('location', $params)) {
        $model->location = $params['location'];
    }

    if (array_key_exists('options', $params)) {
        $model->options = e4s_addDefaultCompOptions($params['options']);
        $compObj = e4s_getCompObj($model->id);
        $model->options = $compObj->validateOptions($model->options);
        e4s_checkBibNumbers($model->options);
    }
    e4s_getTextFromObj($obj, $model);

    $model->link = checkFieldForXSS($obj, 'link:Competition Flyer');

    $model->yearfactor = (int)checkFieldForXSS($obj, 'yearFactor:Competition year factor');
    if ($model->yearfactor > 10) {
        $model->yearfactor = $model->yearfactor - $compYear;
    }

    $model->targetAOCode = checkFieldForXSS($obj, 'env:Environment');

    e4s_addStdPageInfo($obj, $model);
    return $model;
}

// Clone competition methods
function e4s_cloneComp($model, $exit) {
    $row = array();
    $targetAOCode = $model->targetAOCode;
    $compId = $model->id;

    $srcPrefix = str_replace('Entry4_', '', E4S_PREFIX);
    $targetPrefix = e4s_getTargetEnv($targetAOCode);

    if ($targetPrefix === '-1') {
//        Entry4UIError(1020,"Invalid Environment [" . $targetAOCode . "]",400,'');
        // send to current environment
        $targetPrefix = $srcPrefix;
    }

    $compObj = e4s_GetCompObj($compId, FALSE);
    $compObj->resetClonedInformation();
    $row = $compObj->getRow();
    $row['ID'] = null;
    $row['active'] = FALSE;
    $useDateFormat = ISO_MYSQL_DATE;
//    $now = str_replace("T"," " ,date($useDateFormat));
    $now = e4s_iso_to_sql(date($useDateFormat));
    $row['Name'] = 'Cloned ' . $compId . ' ' . $row['Name'];
    $targetOptions = e4s_addDefaultCompOptions($row['options']);
    $targetOptions = $compObj->removeClonedInfoFromOptions($targetOptions);
    $targetOptions = e4s_removeDefaultCompOptions($targetOptions);
    $row['options'] = json_encode($targetOptions);
    $targetCompetition = getTargetTable(E4S_TABLE_COMPETITON, $srcPrefix, $targetPrefix);

    $insertSQL = 'Insert into ' . $targetCompetition . '(';
    $values = ' values (';
    $sep = '';
//    fields not required
    unset($row['id']);
    unset($row['today']);
    unset($row['systemtime']);
    unset($row['compDate']);
    $compFields = [];
    $compFields['locationid'] = 'locationid';
    $compFields['compclubid'] = 'compclubid';
    $compFields['date'] = 'date';
    $compFields['name'] = 'name';
    $compFields['options'] = 'options';
    $compFields['information'] = 'information';
    $compFields['entriesopen'] = 'entriesopen';
    $compFields['entriesclose'] = 'entriesclose';

	$useDate = $model->date;
	if ( is_null($useDate) ){
		$useDate = $row['Date'];
	}
// diff between $useDate and $row['Date']
	$useDateDt = strtotime($useDate);
	$rowDateDt = strtotime($row['Date']);
	$GLOBALS[E4S_CLONE_DATE_DIFF] = ($useDateDt - $rowDateDt) / 86400;
	$targetOptions = null;
    foreach ($row as $colName => $value) {
        $colNameLower = strtolower($colName);
        if (array_key_exists($colNameLower, $compFields)) {
            $insertSQL .= $sep . $colName;
			if ( $colNameLower === "date" ){
				$value = $useDate;
			}
			if ( $colNameLower === "entriesopen" or $colNameLower === "entriesclose" ){
				// addjust the date
				$value = e4s_adjustClonedDate($value);
			}
			if ($colNameLower === 'options') {
				$targetOptions = e4s_getOptionsAsObj($value);
				if ( isset($targetOptions->dates) ){
					foreach($targetOptions->dates as $dateKey => $dateValue){
						$targetOptions->dates[$dateKey] = e4s_adjustClonedDate($dateValue);
					}

					$targetOptions->cloneInfo = new stdClass();
					$targetOptions->cloneInfo->fromId = $compId;
					$value = json_encode($targetOptions);
				}
			}
            $values .= $sep . "'" . addslashes($value) . "'";
            $sep = ',';
        }
    }

    $insertSQL .= ')' . $values . ')';
    e4s_queryNoLog('Clone Comp' . E4S_SQL_DELIM . $insertSQL);

    $targetId = e4s_getLastID();
    if ($targetId === 0) {
        Entry4UIError(5006, 'Failed to clone Competition [' . $compId . ']', 400, '');
    }


    // eventPrice can be empty if ticket only comp
    e4s_cloneCompTable(E4S_TABLE_EVENTPRICE, $compId, $targetId, $srcPrefix, $targetPrefix, TRUE);
    $priceObj = $compObj->getPriceObj();
    $priceObj->recalculatePrices(TRUE);
    e4s_cloneCompTable(E4S_TABLE_COMPEVENTS, $compId, $targetId, $srcPrefix, $targetPrefix, TRUE);
    $egObj = new eventGroup($compId);
    $egObj->cloneEventGroups($targetId);
    e4s_updateTargetPrices($compId, $targetId, $srcPrefix, $targetPrefix);
    e4s_cloneCompTable(E4S_TABLE_COMPDISCOUNTS, $compId, $targetId, $srcPrefix, $targetPrefix, TRUE);
    e4s_cloneCompTable(E4S_TABLE_COMPRULES, $compId, $targetId, $srcPrefix, $targetPrefix, TRUE);
    e4s_cloneCompTable(E4S_TABLE_PERMISSIONS, $compId, $targetId, $srcPrefix, $targetPrefix, TRUE);
    e4s_cloneTextObjs($compId, $targetId);

    include_once E4S_FULL_PATH . 'builder/clubCompCRUD.php';
    e4s_cloneClubComp($compId, $targetId, false);

//    e4s_cloneSecondary($compObj, $targetId);
    $row = $compObj->getRow();
	$targetOptions = e4s_getOptionsAsObj($row['options']);
	$row['options'] = $targetOptions;

    $newrow = e4s_deepCloneTo($row, E4S_OPTIONS_ARRAY);
    $newrow['id'] = $targetId;
    unset($newrow['ID']);

    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($newrow, JSON_NUMERIC_CHECK));
    }
    return $newrow;
}

function e4s_cloneSecondary($compObj, $targetId) {
    // get secondary first
    $secModel = new stdClass();
    $secModel->process = E4S_CRUD_LIST;
    $secModel->exit = FALSE;
    $secModel->objType = E4S_SECONDREF_COMP;
    $secModel->compId = $compObj->getId();
    $secModel->objId = $secModel->compId;
    $secModel->pageInfo = new stdClass();
    $secModel->pageInfo->pagesize = null;
    $secModel->pageInfo->page = null;
    $secondaryObj = new secondaryDefClass($secModel);
    $x = 1;
    /*
     object(stdClass)#15963 (10) {
  ["process"]=>
  string(1) "C"
  ["id"]=>
  int(0)
  ["prod"]=>
  object(stdClass)#15964 (13) {
    ["id"]=>
    int(0)
    ["perAcc"]=>
    string(0) ""
    ["parentId"]=>
    int(0)
    ["stockQty"]=>
    int(100)
    ["menuOrder"]=>
    int(1)
    ["name"]=>
    string(20) "another test ticket2"
    ["image"]=>
    string(0) ""
    ["description"]=>
    string(24) "another test ticket desc"
    ["download"]=>
    bool(true)
    ["virtual"]=>
    bool(true)
    ["ticket"]=>
    object(stdClass)#15953 (3) {
      ["ticketText"]=>
      string(0) ""
      ["ticketFormText"]=>
      string(0) ""
      ["dataReq"]=>
      bool(false)
    }
    ["attributes"]=>
    array(0) {
    }
    ["price"]=>
    object(stdClass)#15985 (3) {
      ["price"]=>
      float(3)
      ["salePrice"]=>
      float(0)
      ["saleEndDate"]=>
      string(0) ""
    }
  }
  ["perAcc"]=>
  string(0) ""
  ["maxAllowed"]=>
  int(0)
  ["compId"]=>
  int(294)
  ["objType"]=>
  string(0) ""
  ["objId"]=>
  int(0)
  ["refObj"]=>
  object(stdClass)#15952 (5) {
    ["id"]=>
    int(0)
    ["objId"]=>
    int(294)
    ["objType"]=>
    string(4) "COMP"
    ["objKey"]=>
    string(0) ""
    ["compId"]=>
    int(294)
  }
  ["pageInfo"]=>
  object(stdClass)#15951 (6) {
    ["startswith"]=>
    NULL
    ["exact"]=>
    bool(false)
    ["page"]=>
    NULL
    ["pagesize"]=>
    NULL
    ["sortkey"]=>
    string(2) "id"
    ["sortorder"]=>
    string(3) "asc"
  }
}

    new secondaryDefClass($model);
     */
}

function e4s_cloneTextObjs($srcId, $targetId) {
    $sql = 'select * 
            from ' . E4S_TABLE_TEXT . '
            where guid = ' . $srcId;
    $result = e4s_queryNoLog($sql);
    while ($obj = $result->fetch_object()) {
        $insertSql = 'insert into ' . E4S_TABLE_TEXT . ' (guid, e4skey, e4stext) values (
            ' . $targetId . ",
            '" . $obj->e4skey . "',
            '" . $obj->e4sText . "'
        )";
        e4s_queryNoLog($insertSql);
    }
}

function e4s_cloneCompTable($srcTable, $srcId, $targetId, $srcPrefix, $targetPrefix, $allowNone) {
    $targetTable = getTargetTable($srcTable, $srcPrefix, $targetPrefix);
//    echo "Cloning " . $targetTable . "\n";
    $srcSql = 'select *
                 from ' . $srcTable . ' 
                 where compid = ' . $srcId;

    $srcResult = e4s_queryNoLog($srcSql);
    if ($srcResult->num_rows < 1) {
        if ($allowNone) {
            return;
        } else {
            Entry4UIError(8001, 'Unable to get SRC ' . $srcTable, 400, '');
        }
    }
    $srcRows = $srcResult->fetch_all(MYSQLI_ASSOC);
    $insertSQL = 'Insert into ' . $targetTable . '(';
    $values = ' values ';
    $rowSep = '';
//    $translationArr = array();
    foreach ($srcRows as $rowCnt => $srcRow) {
        $values .= $rowSep . '(';
        $rowSep = ',';
        $sep = '';
        foreach ($srcRow as $colName => $value) {
			$colNameLC = strtolower($colName);
//        echo $colName . " -> " . $value . "<br>";
            if ( $colNameLC === 'compid') {
                if ($rowCnt === 0) {
                    $insertSQL .= $sep . $colName;
                }
                $values .= $sep . "'" . $targetId . "'";
                $sep = ',';
            } elseif ($colNameLC !== 'id') {
                if ($rowCnt === 0) {
                    $insertSQL .= $sep . $colName;
                }

                if ($value === null) {
                    $values .= $sep . 'null';
                } else {
                    if ($colNameLC === 'isopen') {
                        $value = E4S_EVENT_OPEN;
                    }
	                if ($colNameLC === 'saleenddate' or $colNameLC === 'startdate' or $colNameLC === 'sortdate') {
		                $value = e4s_adjustClonedDate($value);
	                }
                    $values .= $sep . "'" . $value . "'";
                }

                $sep = ',';
            }
        }
        $values .= ')';
    }
    $insertSQL .= ')' . $values;

    e4s_queryNoLog($insertSQL);
}

// once New Prices and Events have been made, update the price IDs in the target CompEvents table
function e4s_updateTargetPrices($srccompid, $targetcompid, $srcPrefix, $targetPrefix) {
    $srcCETable = E4S_TABLE_COMPEVENTS;
    $targetCETable = getTargetTable($srcCETable, $srcPrefix, $targetPrefix);
    $srcEPTable = E4S_TABLE_EVENTPRICE;
    $targetEPTable = getTargetTable($srcEPTable, $srcPrefix, $targetPrefix);

    $sql = 'Select *
            from ' . $srcEPTable . '
            where compid = ' . $srccompid . '
            order by name';
    $result = e4s_queryNoLog($sql);
    $srcPriceRows = $result->fetch_all(MYSQLI_ASSOC);
    $sql = 'Select *
            from ' . $targetEPTable . '
            where compid = ' . $targetcompid . '
            order by name';
    $result = e4s_queryNoLog($sql);
    $targetPriceRows = $result->fetch_all(MYSQLI_ASSOC);
    if (sizeof($srcPriceRows) !== sizeof($targetPriceRows)) {
        Entry4UIError(5007, 'Invalid Price Counts', '404', '');
    }
    foreach ($srcPriceRows as $key => $srcPriceRow) {
        $sql = 'update ' . $targetCETable . '
                set priceid = ' . $targetPriceRows[$key]['ID'] . '
                where compid = ' . $targetcompid . '
                and priceid = ' . $srcPriceRow['ID'];
        e4s_queryNoLog($sql);
    }
}

function e4s_checkBibNumbers($options) {

    $biggestNo = 0;
    $currentBiggestNo = 0;
    if ($options->bibNos === '') {
        $options->bibNos = '1';
        return $options;
    }
    $bibnos = explode(',', $options->bibNos);

    if (strpos($bibnos[sizeof($bibnos) - 1], '-') !== FALSE) {
        Entry4UIError(2050, 'Invalid Bib Nos. You can not end with a range of numbers. Please ensure bib numbers have the last element as a single number to increment from.', 200, '');
    }
    $bibNoArr = array();

    foreach ($bibnos as $bibno) {
        if (trim($bibno) === '') {
            Entry4UIError(2050, 'Invalid Bib Nos. Please remove double commas/empty values from the field (' . $options->bibNos . ')', 200, '');
        }
        $bibRange = new stdClass();
        if ($currentBiggestNo > $biggestNo) {
            $biggestNo = $currentBiggestNo;
        }
        $elements = explode('-', $bibno);

        if (sizeof($elements) > 2) {
            Entry4UIError(2051, 'Invalid Bib Nos. Bib number ranges must be from one number to another number. (' . $bibno . ')', 200, '');
        }
        if (sizeof($elements) > 1) {
            $bibRange->min = (int)$elements[0];
            $bibRange->max = (int)$elements[1];

            $currentBiggestNo = (int)$elements[1];
            if ((int)$elements[0] >= (int)$elements[1]) {
                Entry4UIError(2052, 'Invalid Bib Nos. Bib number ranges must be from one number to a greater number. (' . $bibno . ')', 200, '');
            }
        }
        if (sizeof($elements) === 1) {
            $currentBiggestNo = (int)$elements[0];
            $bibRange->min = (int)$elements[0];
            $bibRange->max = (int)$elements[0];
        }
        if ($bibRange->min < 1 or $bibRange->max < 1) {
            Entry4UIError(2053, 'Invalid Bib Nos. Bib number must be positive numbers. (' . $bibno . ')', 200, '');
        }
        foreach ($bibNoArr as $range) {
            if ($range->min === $bibRange->min or $range->min === $bibRange->max or $range->max === $bibRange->min or $range->max === $bibRange->max) {
                Entry4UIError(2054, 'Invalid Bib Nos. Bib number crosses another range. (' . $bibno . ')', 200, '');
            }
            if ($bibRange->min > $range->min and $bibRange->min < $range->max) {
                Entry4UIError(2055, 'Invalid Bib Nos. Bib number crosses another range. (' . $bibno . ')', 200, '');
            }
            if ($bibRange->max > $range->min and $bibRange->max < $range->max) {
                Entry4UIError(2056, 'Invalid Bib Nos. Bib number crosses another range. (' . $bibno . ')', 200, '');
            }
        }
        $bibNoArr[] = $bibRange;
    }

    if ($currentBiggestNo <= $biggestNo) {
        Entry4UIError(2058, 'Invalid Bib Nos. Last number must be larger than others specified. (' . $options->bibNos . ')', 200, '');
    }
    return $options;
}

function e4s_checkPriority($obj) {
    $compId = checkFieldForXSS($obj, 'compId:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $priorityCode = checkFieldForXSS($obj, 'code:Code');

    if (is_null($compId)) {
        Entry4UIError(9015, 'Invalid Competition');
    }
    if (is_null($priorityCode)) {
        Entry4UIError(9016, 'Invalid Priority');
    }

    $compObj = e4s_GetCompObj($compId);
    if ($compObj->checkPriorityCode($priorityCode)) {
        Entry4UISuccess();
    }
    Entry4UIError(8020, 'Invalid Code');
}