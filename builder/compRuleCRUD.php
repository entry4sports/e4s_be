<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

function e4s_actionCompRule($obj, $process) {
    $model = e4s_getCompRuleModel($obj, $process);
    switch ($model->process) {
        case E4S_CRUD_LIST:
            e4s_listCompRules($model, TRUE);
            break;
        case E4S_CRUD_CREATE:
            e4s_createCompRule($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readCompRule($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_updateCompRule($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteCompRule($model, TRUE);
            break;
    }
}

function e4s_CompRuleInUse($model) {
    Entry4UIError(9020, 'Unwritten method', 200, '');
    return 0;
}

function e4s_getCompRuleRowCRUD($key, $value, $mustExist, $delimeter) {
    $row = e4s_getCompRuleRow($key, $value, $mustExist, $delimeter);
    return e4s_returnStdCompRuleRow($row);
}

function e4s_returnStdCompRuleRow($row) {

    if (!is_null($row)) {
        renameBuilderField($row, 'ID', 'id');
        renameBuilderField($row, 'compid', 'compId');

        if (is_null($row['options']) || $row['options'] === '') {
            $row['options'] = '{}';
        }
        $options = e4s_getOptionsAsObj($row['options']);
        if (isset($options->unique) === FALSE) {
            $options->unique = array();
        }
        if (!isset($options->maxEvents)) {
            $options->maxEvents = $options->maxCompEvents;
        }
        if (!isset($options->maxCompTrack)) {
            $options->maxCompTrack = $options->maxEvents;
        }
        if (!isset($options->maxCompField)) {
            $options->maxCompField = $options->maxEvents;
        }
        if (!isset($options->maxTrack)) {
            $options->maxTrack = $options->maxCompTrack;
        }
        if (!isset($options->maxField)) {
            $options->maxField = $options->maxCompField;
        }
        $row['options'] = $options;
        e4s_getCompRuleMetaData($row);
    }
    return $row;
}

function e4s_getCompRuleRow($key, $value, $mustExist, $delimeter) {
    return e4s_getmultiCRUDRow(E4S_TABLE_COMPRULES, $key, $value, $mustExist, $delimeter);
}

function e4s_getCompRuleMetaData(&$row) {
    $metaModel = new stdClass();
    $metaModel->id = (int)$row['ageGroupID'];

    if ($metaModel->id !== 0) {
        include_once E4S_FULL_PATH . 'builder/ageCRUD.php';
        $row['ageGroup'] = e4s_readAge($metaModel, FALSE);
    } else {
        $metaModel->name = 'All Age Groups';
        $metaModel->keyName = 'All Age Groups';
        $metaModel->shortName = 'All';
        $row['ageGroup'] = $metaModel;
    }

    unset($row['ageGroupID']);
    $options = $row['options'];

    if (isset($options->unique) and !empty($options->unique)) {
        $uniqueArr = $options->unique;
        include_once E4S_FULL_PATH . 'builder/eventCRUD.php';
        foreach ($uniqueArr as $unique) {
            $events = array();
            // TODO Code for a comma sep string rather than array
            foreach ($unique->ids as $id) {
                $model = new stdClass();
                $model->id = $id;
                $events[] = e4s_readEvent($model, FALSE);
            }
            $unique->events = $events;
        }
    }
}

function e4s_readCompRule($model, $exit) {
    $row = e4s_getCompRuleRowCRUD('id', $model->id, TRUE, '');

    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_normaliseRuleOptions($options) {
    $options = e4s_getOptionsAsObj($options);

    if (isset($options->unique)) {
        $unique = $options->unique;
        $ids = array();
        $idCnt = 0;
        foreach ($unique as $oldUnique) {
            $uniqueLine = new stdClass();
            $uniqueLine->ids = $oldUnique->ids;
            $uniqueLine->text = $oldUnique->text;
            $ids[] = $uniqueLine;
            $idCnt += 1;
        }
        if ($idCnt === 0) {
            unset($options->unique);
        } else {
            $options->unique = $ids;
        }
    }

    // Competition Events

    if (!isset($options->maxCompEvents)) {
        $options->maxCompEvents = 3;
    }

    // Track Events
    if (!isset($options->maxCompTrack)) {
        $options->maxCompTrack = 3;
    }

    // Field Events
    if (!isset($options->maxCompField)) {
        $options->maxCompField = 3;
    }

// Day MAx's
    if (isset($options->maxEvents) and ($options->maxEvents === $options->maxCompEvents or $options->maxEvents === 0)) {
        unset($options->maxEvents);
    }

    // Track Events
    if (isset($options->maxTrack) and ($options->maxTrack === $options->maxCompTrack or $options->maxTrack === 0)) {
        unset($options->maxTrack);
    }

    // Field Events
    if (isset($options->maxField) and ($options->maxField === $options->maxCompField or $options->maxField === 0)) {
        unset($options->maxField);
    }
    unset($options->type);
    return $options;
}

function e4s_createSingleCompRule($model) {
    // check it not going to create a duplicate
    e4s_getCompRuleRow('compid:agegroupid', $model->compid . ':' . $model->agegroupid, FALSE, ':');

    $options = e4s_normaliseRuleOptions($model->options);
    $sql = 'Insert into ' . E4S_TABLE_COMPRULES . " (compid, agegroupid, options)
            values(
                '" . $model->compid . "',
                '" . $model->agegroupid . "'," . "'" . json_encode($options) . "')";

    e4s_queryNoLog($sql);
    $row = e4s_getCompRuleRowCRUD('id', e4s_getLastID(), TRUE, '');
    $model->auditObj->writeAudit('Create Rule Record : ' . e4s_getLastID(), TRUE, E4S_TABLE_COMPRULES, e4s_getLastID(), $row);
    return $row;
}

function e4s_createCompRule($model, $exit) {
    if ((int)$model->id !== 0) {
        Entry4UIError(2009, 'ID of non zero passed to create', 400, '');
    }

    $returnData = array();
    if (isset($model->agegroups)) {
        foreach ($model->agegroups as $agegroup) {
            $model->agegroupid = $agegroup['id'];
            $returnData[] = e4s_createSingleCompRule($model);
        }
    } else {
        if (isset($model->agegroup)) {
            $returnData = e4s_createSingleCompRule($model);
        }
    }

    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($returnData, JSON_NUMERIC_CHECK));
    }
    return $returnData;
}

function e4s_updateSingleCompRule($model) {
    $row = e4s_getCompRuleRowCRUD('id', $model->id, TRUE, '');
    $model->auditObj->writeAudit('update Rule Record : ' . $model->id, TRUE, E4S_TABLE_COMPRULES, $model->id, $row);
    $options = e4s_normaliseRuleOptions($model->options);

    $sql = 'update ' . E4S_TABLE_COMPRULES . '
            set compid =' . $model->compid . ',
            agegroupid =' . $model->agegroupid . ",
            options ='" . json_encode($options) . "'
            where id = " . $model->id;
    e4s_queryNoLog($sql);
    $row = e4s_getCompRuleRowCRUD('id', $model->id, TRUE, '');
    return $row;
}

function e4s_updateCompRule($model, $exit) {
    if (isset($model->agegroups)) {
        foreach ($model->agegroups as $agegroup) {
            $row = e4s_getCompRuleRow('compid:agegroupid', $model->compid . ':' . $agegroup['id'], TRUE, ':');
            $model->id = (int)$row['id'];
            $model->agegroupid = $agegroup['id'];
            if (is_null($row)) {
                e4s_createSingleCompRule($model);
            } else {
                e4s_updateSingleCompRule($model);
            }
        }
    } else {
        if (isset($model->agegroup)) {
            $row = e4s_getCompRuleRow('compid:agegroupid', $model->compid . ':' . $model->agegroup['id'], TRUE, ':');
            $model->id = (int)$row['id'];
            $model->agegroupid = $model->agegroup['id'];
            $row = e4s_updateSingleCompRule($model);
        }
    }

    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_deleteCompRule($model, $exit) {
    // check Row exists
    $row = e4s_getCompRuleRow('id', $model->id, TRUE, '');
    if (!isset($model->auditObj)) {
        $model->auditObj = e4sAudit::withID($row['compID']);
    }

    $model->auditObj->writeAudit('Delete Rule Record : ' . $model->id, TRUE, E4S_TABLE_COMPRULES, $model->id, $row);
    $sql = 'delete from ' . E4S_TABLE_COMPRULES . '
            where id = ' . $model->id;

    e4s_queryNoLog($sql);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }

    return $row;
}

function e4s_listCompRules($model, $exit) {

    try {
        $usePaging = FALSE;

        if (isset($model->pageInfo) === FALSE) {
            $model->pageInfo = new stdClass();
            $model->pageInfo->startswith = '';
            $model->pageInfo->page = 0;
            $model->pageInfo->pagesize = 0;
        }
        if (isset($model->pageInfo->pagesize) and isset($model->pageInfo->page)) {
            $usePaging = TRUE;
        }

        $sql = 'select *
        from ' . E4S_TABLE_COMPRULES . '
        where compid = ' . $model->compid . ' ';

        if (is_null($model->pageInfo->startswith) === FALSE and $model->pageInfo->startswith !== '') {
            $sql .= " and type like '" . $model->pageInfo->startswith . "%' ";
        }

        $sql .= ' order by id ';

        if ($usePaging and $model->pageInfo->pagesize !== 0) {
            $sql .= ' limit ' . (($model->pageInfo->page - 1) * $model->pageInfo->pagesize) . ', ' . $model->pageInfo->pagesize;
        }

        $result = e4s_queryNoLog($sql);
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $newRows = array();
        foreach ($rows as $row) {
            $newRows[] = e4s_returnStdCompRuleRow($row);
        }

        if ($exit) {
            Entry4UISuccess('
                "data":' . json_encode($newRows, JSON_NUMERIC_CHECK));
        }
        return $newRows;
    } catch (mysqli_sql_exception $ex) {
        throw new Exception("Can't connect to the database! \n" . $ex);
    }
    return array();
}

function e4s_getCompRuleModel($obj, $process) {

    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;
    $model->id = checkFieldForXSS($obj, 'id:Competition Rule ID');
    $model->compid = checkFieldForXSS($obj, 'compId:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (!is_null($model->compid)) {
        $model->auditObj = e4sAudit::withID($model->compid);
    }
    $model->options = checkJSONObjForXSS($obj, 'options:CompRule options');
    if (is_null($model->options)) {
        $model->options = '{}';
    }

    $model->agegroupid = 0;
    $model->agegroup = checkJSONObjForXSS($obj, 'ageGroup:Age Group Object');
    if (!is_null($model->agegroup)) {
        $model->agegroupid = $model->agegroup['id'];
    }
    $model->agegroups = checkJSONObjForXSS($obj, 'ageGroups:Age Group array');

    e4s_addStdPageInfo($obj, $model);
    return $model;
}