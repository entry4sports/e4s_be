<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';
include_once E4S_FULL_PATH . 'events/commonEvent.php';
include_once E4S_FULL_PATH . 'builder/eventGroupCRUD.php';
include_once E4S_FULL_PATH . 'builder/compCRUD.php';

function e4s_actionCE($obj, $process) {

    $model = e4s_getCompEventModel($obj, $process);
    if (!userHasPermission(PERM_BUILDER, null, $model->compid)) {
        // User does not have permission to build this comp
        Entry4UIError(1000, 'You are not authorised to use this function', 401, '');
    }
    switch ($model->process) {
        case E4S_CRUD_LIST:
            e4s_listCompEvents($model, TRUE);
            break;
        case E4S_CRUD_CREATE:
            e4s_createCompEvent($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readCompEvent($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_updateCompEvent($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteCompEvent($model, TRUE);
            break;
        case 'RESET_TIMES':
            e4s_resetCompEventTimes($model);
            break;
    }
}

function e4s_getFullCompSql() {

    return "select ce.*, 
    DATE_FORMAT(eg.startdate,'" . ISO_MYSQL_DATE . "') as startdateiso,
    DATE_FORMAT(eg.startdate,'" . ISO_MYSQL_DATE . "') as sortdateiso, 
    p.name pricename, p.description,price,saleprice,saleenddate, 
    salefee,fee, 
    p.options poptions, 
    u.id uomid, 
    u.uomtype, 
    u.uomoptions, 
    e.name eventname,
    tf,
    e.gender gender,
    e.options eoptions
        from " . E4S_TABLE_COMPEVENTS . ' ce,
             ' . E4S_TABLE_EVENTGROUPS . ' eg,
             ' . E4S_TABLE_EVENTS . ' e,
             ' . E4S_TABLE_UOM . ' u,
             ' . E4S_TABLE_EVENTPRICE . ' p
        where ce.priceid = p.id
        and   ce.maxgroup = eg.id
        and   e.uomid = u.id
        and   ce.eventid = e.id ';
}

function getFullModelSqlForRow($ceid) {
    $sql = e4s_getFullCompSql();
    $sql .= 'and ce.id = ' . $ceid;
    return $sql;
}

function getFullModelSql($model) {
    $sql = e4s_getFullCompSql();
    $sql .= 'and  ce.compid = ' . $model->compid;
    return $sql;
}

function e4s_returnStdCERow($row, $expandOptions) {

    if (array_key_exists('startdateiso', $row)) {
        $row['startdate'] = $row['startdateiso'];
        unset($row['startdateiso']);
    }
    if (array_key_exists('sortdateiso', $row)) {
        $row['sortdate'] = $row['sortdateiso'];
        unset($row['sortdateiso']);
    }
    if (!is_null($row)) {
        $options = e4s_addDefaultCompEventOptions($row['options']);
        e4s_injectIntoCERowOtherSources((int)$row['CompID'], (int)$row['maxGroup'], $row, $options);
        if ($expandOptions) {
            $row['options'] = e4s_returnFullOptions($options);
        } else {
            $row['options'] = $options;
        }
        if (array_key_exists('eoptions', $row)) {
            $row['eoptions'] = e4s_getOptionsAsObj($row['eoptions']);
        } else {
            $row['eoptions'] = new stdClass();
        }
        if (array_key_exists('poptions', $row)) {
            $row['poptions'] = e4s_getOptionsAsObj($row['poptions']);
        } else {
            $row['poptions'] = new stdClass();
        }
        if (array_key_exists('uomoptions', $row)) {
            $row['uomoptions'] = e4s_getOptionsAsObj($row['uomoptions']);
        } else {
            $row['uomoptions'] = new stdClass();
        }
        renameBuilderField($row, 'ID', 'id');
        renameBuilderField($row, 'CompID', 'compId');
        renameBuilderField($row, 'EventID', 'eventId');
        renameBuilderField($row, 'IsOpen', 'isOpen');
        renameBuilderField($row, 'PriceID', 'priceId');
        renameBuilderField($row, 'AgeGroupID', 'ageGroupID');
        renameBuilderField($row, 'uomid', 'uomId');
        renameBuilderField($row, 'maxathletes', 'maxAthletes');
        // ISO Times
        renameBuilderField($row, 'startdate', 'startDateTime');
        renameBuilderField($row, 'sortdate', 'sortDateTime');
        $eventObj = array();
        renameBuilderFieldFromObj($row, $eventObj, 'eventId', 'id');
        renameBuilderFieldFromObj($row, $eventObj, 'eventname', 'name');
        renameBuilderFieldFromObj($row, $eventObj, 'eventNameExtra', 'eventnameextra');
        renameBuilderFieldFromObj($row, $eventObj, 'gender', 'gender');
        renameBuilderFieldFromObj($row, $eventObj, 'tf', 'tf');

        $row['event'] = $eventObj;

        e4s_getAgeGroupInfo($row);

        $priceObj = array();
        $priceObj['id'] = $row['priceId'];
        renameBuilderFieldFromObj($row, $priceObj, 'fee', 'fee');
        renameBuilderFieldFromObj($row, $priceObj, 'salefee', 'saleFee');
        renameBuilderFieldFromObj($row, $priceObj, 'price', 'price');
        renameBuilderFieldFromObj($row, $priceObj, 'saleprice', 'salePrice');
        renameBuilderFieldFromObj($row, $priceObj, 'saleenddate', 'saleEndDate');

        $priceObj['options'] = e4s_addDefaultPriceOptions($row['poptions']);
        unset($row['poptions']);

        renameBuilderFieldFromObj($row, $priceObj, 'priceId', 'id');
        renameBuilderFieldFromObj($row, $priceObj, 'pricename', 'name');
        renameBuilderFieldFromObj($row, $priceObj, 'description', 'description');

        $row['price'] = $priceObj;

        $uomObj = array();
        renameBuilderFieldFromObj($row, $uomObj, 'uomId', 'id');
        renameBuilderFieldFromObj($row, $uomObj, 'uomtype', 'type');
        renameBuilderFieldFromObj($row, $uomObj, 'uomoptions', 'options');

        $row['uom'] = $uomObj;
    }
    return $row;
}

function e4s_getCERowCRUD($key, $value, $mustExist) {
    $row = e4s_getCERow($key, $value, $mustExist);
    return e4s_returnStdCERow($row, TRUE);
}

function e4s_getCEFullRowCRUD($key, $value, $mustExist) {
    $row = e4s_getFullCERow($key, $value, $mustExist);
    return e4s_returnStdCERow($row, FALSE);
}

function e4s_readCEForUnique($model, $exit) {
    $row = e4s_getCEFullRowCRUD('id', $model->id, TRUE);

    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_injectIntoCERowOtherSources($compId, $egId, &$row, &$options) {
    e4s_checkFormRows($options);
    $egObj = new eventGroup($compId);
    $egObj->checkAndInjectEventGroupInfo($egId, $row, $options);
    $eventObj = new e4sEvents($compId);
    $options = $eventObj->populateOptions($options, $egId);
}

function e4s_getFullCERow($key, $value, $mustExist) {
    $row = null;
    $sql = getFullModelSqlForRow($value);
    $result = e4s_queryNoLog($sql);
    if ($mustExist) {
        if ($result->num_rows < 1) {
            Entry4UIError(2521, 'Invalid ' . $key . ':' . $value, 400, '');
        }
        $row = $result->fetch_assoc();
    }
    if (!$mustExist and $result->num_rows > 0) {
        Entry4UIError(2002, 'Duplicate ' . $key . ':' . $value, 400, '');
    }
    $options = e4s_addDefaultCompEventOptions($row['options']);
    e4s_injectIntoCERowOtherSources((int)$row['CompID'], (int)$row['maxGroup'], $row, $options);
    $options = e4s_CodeOptions($options, E4S_DECODE_OPTIONS);
    $row['options'] = $options;

    return $row;
}

function e4s_getCEActualRow($key, $value, $delim) {
    $row = null;

    $keys = explode($delim, $key);
    $values = explode($delim, $value);
    $sep = '';
    $sql = 'select ce.*, eg.options egOptions
            from ' . E4S_TABLE_COMPEVENTS . ' ce left join ' . E4S_TABLE_EVENTGROUPS . ' eg on (ce.maxgroup = eg.id)
            where ';
    foreach ($keys as $i => $v) {
        $sql .= $sep . 'ce.' . $v . " = '" . $values[$i] . "'";
        $sep = ' and ';
    }

    $result = e4s_queryNoLog('getCERow' . E4S_SQL_DELIM . $sql);
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $egOptions = e4s_addDefaultEventGroupOptions($row['egOptions']);
        $ceOptions = e4s_getOptionsAsObj($row['options']);
        $ceOptions->seed = $egOptions->seed;
        $row['options'] = $ceOptions;
    }

    return $row;
}

function e4s_getCERow($key, $value, $mustExist) {
    $row = e4s_getCEActualRow($key, $value, '!');
    if ($mustExist) {
        if (is_null($row)) {
            Entry4UIError(2007, 'Invalid ' . $key . ':' . $value, 400, '');
        }
    }
    if (!$mustExist and !is_null($row)) {
        Entry4UIError(2008, 'Duplicate ' . $key . ':' . $value, 400, '');
    }

    return $row;
}

function e4s_readCompEvent($model, $exit) {
    $row = e4s_getCERowCRUD('id', $model->id, TRUE);
    if ($exit) {
        e4s_returnCompEventData(0, $row);
    }
    return $row;
}

// Inbound

function e4s_normaliseCEOptions($options) {

    $options = e4s_getOptionsAsObj($options);

	if ( isset($options->includeEntriesFromEgId) ){
		unset($options->includeEntriesFromEgId);
	}

    if (property_exists($options, 'ageGroups')) {
        $ageGroups = $options->ageGroups;

        $ids = array();
        foreach ($ageGroups as $ageGroup) {
            $ageGrp = new stdClass();
            if (isset($ageGroup->id)) {
                $ageGrp->ageGroup = $ageGroup->id;
            }
            $ids[] = $ageGrp;
        }

        if (empty($ids)) {
            unset($options->ageGroups);
        } else {
            $options->ageGroups = $ids;
        }
    }

    if (isset($options->unique)) {
        $ids = array();
        $uniques = $options->unique;

        foreach ($uniques as $unique) {
            $uObj = new stdClass();
            $uObj->ce = 0;
            if (isset($unique->ce)) {
                $uObj->ce = $unique->ce;
            }
            $uObj->e = 0;
            if (isset($unique->e)) {
                $uObj->e = $unique->e;
            }
            $uObj->eg = 0;
            if (isset($unique->eg)) {
                $uObj->eg = $unique->eg;
            }
            $ids[] = $uObj;
        }
        if (empty($ids)) {
            unset($options->unique);
        } else {
            $options->unique = $ids;
        }
    }

    if (isset($options->security)) {
        $options->security = e4s_setFullSecurityObj($options->security);
    }

    if (e4s_isCEALeague($options)) {
        if (!isset($options->eventTeam->formRows)) {
            Entry4UIError(1001, 'No formRows passed for league', 400, '');
        }
        e4s_processFormRows($options);
    }

    return $options;
}
function e4s_checkMaxInHeat(&$model){
//    event comes in as array or object !!!!
    $eventObj = e4s_getDataAsType($model->event,E4S_OPTIONS_OBJECT);
    if ( $model->egoptions->maxInHeat === 0){
        if ( isset($eventObj->options)){
            $model->egoptions->maxInHeat = $eventObj->options->maxInHeat;
        }else{
            $eventObj = e4sEvents::getEventInfo($eventObj->id);
            $model->egoptions->maxInHeat = 8;
            if ( isset($eventObj->options->maxInHeat) ){
                $model->egoptions->maxInHeat = $eventObj->options->maxInHeat;
            }
        }
    }
    if ( $model->egoptions->maxInHeat === 8){
        $model->egoptions->maxInHeat = $model->comp->options->laneCount;
    }
    $model->options->maxInHeat = $model->egoptions->maxInHeat;
}
function e4s_modifyCompEvent($state, $model, $exit) {
    $mustExist = FALSE;
    if ( $state === E4S_CRUD_CREATE ){
        if ((int)$model->age->id === 0) {
            return null;
        }
        e4s_checkMaxInHeat($model);
    }
    if ( $state === E4S_CRUD_UPDATE ){
        $mustExist = TRUE;
        e4s_checkMaxInHeat($model);
    }
    if (!isset($model->verified)) {
        // verified means we already know it does not exist so dont bother checking
        e4s_getCERow('id', $model->id, $mustExist);
    }

    if ($model->split === ''){
        $model->split = 0;
    }
    if ( $model->split !== 0){
        $model->options->mandatoryPB = true;
    }
    $model->options = e4s_normaliseCEOptions($model->options);
    $model->options = e4s_removeDefaultCompEventOptions($model->options);
    unset($model->options->seed);
    if (isset($model->maxgroupFromArr)) {
        $model->maxgroup = $model->maxgroupFromArr;
    } else {
        $egObj = new eventGroup($model->compid);
        $model->maxgroup = (int)$egObj->updateEGFromCEModel($model);
    }

    if (e4sEvents::hasUniqueEvents($model->options)) {
        $eventObj = new e4sEvents($model->compid);
        $model->options = $eventObj->setEventOptions($model);
    }
    $model->auditObj->writeAudit('Update CE Record : ' . $model->id, TRUE, E4S_TABLE_COMPEVENTS, $model->id);
    if (is_null($model->maxathletes)){
        $model->maxathletes = 0;
    }
    $egId = (int)$model->maxgroup;
    $multiEGObj = new multiEventGroup($egId, null);
    $model = $multiEGObj->writeCEOptions($model);
    if ( $state === E4S_CRUD_CREATE ) {
        $sql = 'Insert into ' . E4S_TABLE_COMPEVENTS . ' ( agegroupid,compid,eventid,isopen,maxathletes,maxgroup, options,priceid,sortdate,split,startdate )
            values(
                ' . $model->age->id . ',
                ' . $model->compid . ',
                ' . $model->event->id . ',
                ' . $model->isopen . ',
                ' . $model->maxathletes . ',
                ' . $egId . ",
                '" . json_encode($model->options) . "',
                " . $model->price->id . ",
                '" . $model->sortdate . "',
                '" . $model->split . "',
                '" . e4s_iso_to_sql($model->startdate) . "'
            )";
    }
    if ( $state === E4S_CRUD_UPDATE ){
        $sql = 'update ' . E4S_TABLE_COMPEVENTS . "
                set agegroupid = '" . $model->age->id . "',
                compid = '" . $model->compid . "',
                eventid = '" . $model->event->id . "',
                isopen = '" . $model->isopen . "',
                maxathletes = '" . $model->maxathletes . "',
                maxgroup = " . $egId . ",
                options = '" . json_encode($model->options) . "',
                priceid = '" . $model->price->id . "',
                sortdate = '" . $model->sortdate . "',
                split = '" . $model->split . "',
                startdate = '" . $model->startdate . "'
            where id = " . $model->id;
    }
    e4s_queryNoLog('CreateUpdateCE' . E4S_SQL_DELIM . $sql);

    $useId = $model->id;
    if ( $state === E4S_CRUD_CREATE ) {
        $useId = e4s_getLastID();
    }
    // update other ce's with unique info if set, or check and remove current
    $obj = new stdClass();
    $obj->ceId = $useId;
    $obj->compId = $model->compid;
    $obj->ageGroupId = $model->age->id;
    $obj->egId = $model->maxgroup;
    $obj->gender = $model->event->gender;
    $obj->options = $model->options;
    $eventObj = new e4sEvents($model->compid);
    $eventObj->updateUniqueOnOtherEvents($obj);

    $row = e4s_getFullCERow('id', $useId, TRUE);
    $model->auditObj->writeAudit('CreatedUpdate CE Record : ' . $useId, TRUE, E4S_TABLE_COMPEVENTS, $useId);
    if ($exit) {
        if ( $state === E4S_CRUD_CREATE ) {
            // dont pass compid as not interested in agegroups
            e4s_returnCompEventData(0, $row);
        }
        if ( $state === E4S_CRUD_UPDATE ){
            Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
        }
    }
    return $row;
}
function e4s_returnCompEventData($compId, $obj) {
    $meta = new stdClass();
    if ($compId !== 0) {
        $metaModel = new stdClass();
        $metaModel->compid = $compId;

        include_once E4S_FULL_PATH . 'builder/ageCRUD.php';
        $meta->ageGroups = e4s_listAgesForComp($metaModel, FALSE);
    } else {
        $meta->ageGroups = null;
    }

    Entry4UISuccess('
        "data":' . json_encode($obj, JSON_NUMERIC_CHECK) . ',
        "meta":' . json_encode($meta, JSON_NUMERIC_CHECK));
}

function e4s_updateCompEvent($model, $exit) {
    return e4s_modifyCompEvent(E4S_CRUD_UPDATE, $model, $exit);
}
function e4s_createCompEvent($model, $exit) {
    return e4s_modifyCompEvent(E4S_CRUD_CREATE, $model, $exit);
}
function e4s_updateCompEventBulkUpdate($row, $model) {
    $ceoptions = e4s_getOptionsAsObj($row['options']);
    if (array_key_exists('options', $model)) {
        $newOptions = e4s_getOptionsAsObj($model['options']);
        $ceoptions = e4s_mergeOptions($newOptions, $ceoptions, E4S_OPTIONS_OBJECT);
        $ceoptions = e4s_normaliseCEOptions($ceoptions);
    }

    $ceoptions = e4s_removeDefaultCompEventOptions($ceoptions);

    if (array_key_exists('isopen', $model)) {
        $row['IsOpen'] = $model['isopen'];
    }
    if (array_key_exists('maxathletes', $model)) {
        $row['maxAthletes'] = $model['maxathletes'];
    }
    if (array_key_exists('maxgroup', $model)) {
        $row['maxGroup'] = $model['maxgroup'];
    }
    if (array_key_exists('startdate', $model)) {
        $row['startdate'] = $model->startdate;
    }
    if (array_key_exists('sortdate', $model)) {
        $row['sortdate'] = $model->sortdate;
    }
    if (array_key_exists('event', $model)) {
        $row['EventID'] = $model['event']['id'];
    }
    if (array_key_exists('split', $model)) {
        $row['split'] = $model['split'];
    }

    if (array_key_exists('price', $model)) {
        $row['PriceID'] = $model['price']['id'];
    }

    $sql = 'update ' . E4S_TABLE_COMPEVENTS . "
                set agegroupid = '" . $row['AgeGroupID'] . "',
                compid = '" . $row['CompID'] . "',
                eventid = '" . $row['EventID'] . "',
                isopen = '" . $row['IsOpen'] . "',
                maxathletes = '" . $row['maxathletes'] . "',
                maxgroup = " . $row['maxGroup'] . ",
                options = '" . json_encode($ceoptions) . "',
                priceid = " . $row['PriceID'] . ",
                sortdate = '" . $row['sortdate'] . "',
                split = '" . $row['split'] . "',
                startdate = '" . $row['startdate'] . "'
            where id = " . $row['ID'];

    e4s_queryNoLog('Bulk Update CE' . E4S_SQL_DELIM . $sql);
    return e4s_getFullCERow('id', $row['ID'], TRUE);
}

function e4s_updateCERecordsEventGroup($id, $egID) {
    $sql = 'update ' . E4S_TABLE_COMPEVENTS . "
            set maxgroup = '{$egID}'
            where id = " . $id;

    e4s_queryNoLog('update EgID in CERecord' . E4S_SQL_DELIM . $sql);
}

function e4s_deleteCompEvent($model, $exit) {
    $ids = array();
    $row = null;

    if (isset($model->id)) {
        // check Row exists
        $row = e4s_getCERow('id', $model->id, TRUE);
        $ceids = $model->id;
        $ids[] = $model->id;
        $sql = 'select * from ' . E4S_TABLE_ENTRIES . '
            where CompEventid = ' . $model->id;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows > 0) {
            Entry4UIError(2006, 'Unable to delete as in use', 400, '');
        }
        $wherestatement = 'where id = ' . $ceids;
    } else {

        if (is_array($model)) {
            $model = $model[0];
        }

        foreach ($model->ceids as $ceidObj) {
            // Not sure if it always now comes in as an array or object so handle both till I can find out
            if (isset($ceidObj->id)) {
                $ids[] = $ceidObj->id;
            } else {
                $ids[] = $ceidObj['id'];
            }
        }

        $ceids = implode(',', $ids);
        $sql = 'select * 
                from ' . E4S_TABLE_ENTRIES . '
                where CompEventid in (' . $ceids . ')';
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows > 0) {
            Entry4UIError(2007, 'Unable to delete as in use', 400, '');
        }
        $wherestatement = 'where id in (' . $ceids . ')';
    }

    // Get Compid before deleting
    $compsql = 'select distinct(compid) compid from ' . E4S_TABLE_COMPEVENTS . ' ' . $wherestatement;
    $res = e4s_queryNoLog($compsql);

    if ($res->num_rows !== 1) {
        Entry4UIError(9019, 'Unable to find competition for delete record (' . $ceids . ')', 400, '');
    }
    $ceRow = $res->fetch_assoc();
    // Audit the deletion
    $compId = (int)$ceRow['compid'];
    $model->auditObj = e4sAudit::withID($compId);
    $model->auditObj->writeAudit('Deleting CE(s) :' . implode(',', $ids), TRUE, E4S_TABLE_COMPEVENTS, $ids);

    // Now delete
    $deletesql = 'delete from ' . E4S_TABLE_COMPEVENTS . ' ' . $wherestatement;
    e4s_queryNoLog($deletesql);

    // check event Groups
    $eventGroupObj = new eventGroup($compId);
    $eventGroupObj->cleanUpUnUsed();

    if ($exit) {
        e4s_returnCompEventData(0, $row);
    }
    return $row;
}

function e4s_listCompEvents($model, $exit) {
    if (isset($model->pageInfo) === FALSE) {
        $model->pageInfo = new stdClass();
        $model->pageInfo->startswith = '';
        $model->pageInfo->page = 0;
        $model->pageInfo->pagesize = 0;
    }

    $pagesize = $model->pageInfo->pagesize;
    $page = $model->pageInfo->page;
    $startswith = $model->pageInfo->startswith;

    $usePaging = FALSE;
    if (!is_null($startswith) and isset($pagesize) and isset($page) and $page !== 0) {
        $usePaging = TRUE;
    }
    $sql = getFullModelSql($model);

    if (isset($startswith)) {
        if ($startswith !== '') {
            $sql .= " and e.name like '" . $startswith . "%' ";
        }
    }

    $sql .= ' order by sortdate ';

    if ($usePaging and $pagesize !== 0) {
        $sql .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
    }
    $result = e4s_queryNoLog($sql);

    // get entry Counts
    $entriesSql = '
        select compeventid ceid, 
               count(compEventID) total,
               if (waitingPos> 0,1,0) waiting
        from ' . E4S_TABLE_ENTRIES . ' e,
             ' . E4S_TABLE_COMPEVENTS . ' ce
        where e.compEventID = ce.ID
        and ce.CompID = ' . $model->compid . '
        and e.paid = ' . E4S_ENTRY_PAID . '
        group by compEventID, if (waitingPos> 0,1,0)
    ';

    $entryResult = e4s_queryNoLog($entriesSql);

    $entryCount = array();
    while ($obj = $entryResult->fetch_object()) {
        $ceId = (int)$obj->ceid;
        if (!array_key_exists((int)$obj->ceid, $entryCount)) {
            $entryCount[$ceId] = new stdClass();
            $entryCount[$ceId]->total = 0;
            $entryCount[$ceId]->waiting = 0;
        }
        if ((int)$obj->waiting === 0) {
            $entryCount[$ceId]->total = $obj->total;
        } else {
            $entryCount[$ceId]->waiting = $obj->total;
        }
    }
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    $newRows = array();

    foreach ($rows as $row) {
//	    e4s_write_log("e4s_listCompEvents 5a : " . $row['ID']);
        $newRow = e4s_returnStdCERow($row, TRUE);
//		e4s_write_log("e4s_listCompEvents 5b : " . $row['ID']);

        if (array_key_exists($row['ID'], $entryCount)) {
            $newRow['entryCount'] = $entryCount[$row['ID']];
        } else {
            $defObj = new stdClass();
            $defObj->total = 0;
            $defObj->waiting = 0;
            $newRow['entryCount'] = $defObj;
        }

        $newRows[] = $newRow;
    }
    if ($exit) {
        e4s_returnCompEventData($model->compid, $newRows);
    }
    return $newRows;
}

// outbound
function e4s_returnFullOptions($options) {

    $options = e4s_addDefaultCompEventOptions($options);
    $options = e4s_CodeOptions($options, E4S_DECODE_OPTIONS);

    if (!property_exists($options, 'secondarySpend')) {
        $secSpend = new stdClass();
        $secSpend->isParent = FALSE;
        $secSpend->parentCeid = 0;
        $options->secondarySpend = $secSpend;
    }

    $options->security = e4s_getFullSecurityObj($options->security);

    if (!property_exists($options, 'unique')) {
        $options->unique = array();
    } else {
        $uCEs = $options->unique;
        $fullUnique = array();
        $metaModel = new stdClass();

        foreach ($uCEs as $key => $uCE) {
            $metaModel->id = (int)$uCE->ce;
            if ($metaModel->id !== 0) {
                $fullCE = e4s_readCEForUnique($metaModel, FALSE);
                $fullUnique[] = $fullCE;
            }
        }
        $options->unique = $fullUnique;
    }

    if (!property_exists($options, 'ageGroups')) {
        $options->ageGroups = array();
    } else {
        $ageGroups = $options->ageGroups;
        $fullAgeGroups = array();
        include_once E4S_FULL_PATH . 'builder/ageCRUD.php';
        $metaModel = new stdClass();

        foreach ($ageGroups as $key => $ageGroup) {
            if (!isset($ageGroup->ageGroup)) {
                $fullAgeGroups = array();
                break;
            }
            $metaModel->id = (int)$ageGroup->ageGroup;
            if ($metaModel->id !== 0) {
                $fullAge = e4s_readAge($metaModel, FALSE);
                $fullAgeGroups[] = $fullAge;
            }
        }
        $options->ageGroups = $fullAgeGroups;
    }

    return $options;
}

function removeUnwantedOptions($options) {
    if (is_null($options)) {
        return $options;
    }
    if ($options['isTeamEvent'] === FALSE || empty($options['eventTeam'])) {
        unset($options['eventTeam']);
    }
    if (array_key_exists('secondarySpend', $options)) {
        $secSpend = $options['secondarySpend'];
        if (array_key_exists('isParent', $secSpend) and $secSpend['isParent'] === FALSE and (int)$secSpend['parentCeid'] === 0) {
            unset($options['secondarySpend']);
        }
    }
    if (empty($options['ageGroups'])) {
        unset($options['ageGroups']);
    }
    if (empty($options['unique'])) {
        unset($options['unique']);
    }

    if ($options['helpText'] === '') {
        unset($options['helpText']);
    }
    if ($options['xeText'] === '') {
        unset($options['xeText']);
    }
    if ($options['xiText'] === '') {
        unset($options['xiText']);
    }
    //    unset($options['isTeamEvent']);
    return e4s_CodeOptions($options, E4S_ENCODE_OPTIONS);
}

function e4s_getCompEventModel($obj, $process) {
    $compRow = null;
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;

    $model->id = checkFieldForXSS($obj, 'id:CompEvent ID');
    $model->compid = checkFieldForXSS($obj, 'compId:Comp ID');
    if (is_null($model->compid)) {
        $model->compid = checkFieldForXSS($obj, 'compid:Comp ID');
    }
    if (!is_null($model->compid) and $model->compid > 0) {
        // Get competition information
        $compRow = e4s_getCompRow('id', $model->compid, TRUE);
        $compRow['options'] = e4s_addDefaultCompOptions(e4s_getOptionsAsObj($compRow['options']));
        $model->comp = e4s_getDataAsType($compRow, E4S_OPTIONS_OBJECT);
    }

    $model->event = checkJSONObjForXSS($obj, 'event:Event');
    $model->startdate = checkFieldForXSS($obj, 'startDateTime:Event Start DateTime');
    $model->sortdate = checkFieldForXSS($obj, 'sortDateTime:Event Sort DateTime');
    if ($model->sortdate === '') {
        $model->sortdate = $model->startdate;
    }

    $model->isopen = checkFieldForXSS($obj, 'isOpen:isOpen');
    $model->split = checkFieldForXSS($obj, 'split:Split');
    $model->price = checkJSONObjForXSS($obj, 'price:Event Price');
    $model->age = checkJSONObjForXSS($obj, 'ageGroup:Age Group');
    $options = checkJSONObjForXSS($obj, 'options:Comp Event Options');
    $model->options = removeUnwantedOptions($options);
    $model->ceids = checkJSONObjForXSS($obj, 'ceids:Array of CE ids');
    $model->maxathletes = checkFieldForXSS($obj, 'maxAthletes:Maximum Athletes');
    $model->maxgroup = checkFieldForXSS($obj, 'maxGroup:Event Group');
    $model->groupname = checkFieldForXSS($obj, 'eventGroup:Event Group Name');
// allow a < or a > but not both
	if ( is_null($model->groupname)){
		$model->groupname = '';
	}else {
		if ( strpos( $model->groupname, '<' ) !== FALSE and strpos( $model->groupname, '>' ) !== FALSE ) {
			$model->groupname = str_replace( '<', '&lt;', $model->groupname );
		}
		if ( strpos( $model->groupname, '&lt;' ) !== FALSE ) {
			$model->groupname = str_replace( '&lt;', '<', $model->groupname );
		} else {
			if ( strpos( $model->groupname, '&gt;' ) !== FALSE ) {
				$model->groupname = str_replace( '&gt;', '>', $model->groupname );
			}
		}
		$model->groupname = str_replace( ',', '.', $model->groupname );
	}
    if (!is_null($compRow)) {
        $egObj = new eventGroup($compRow['ID']);
        $egObj->generateEventGroupOptions($model);
    }
    if (!is_null($model->groupname) and strlen($model->groupname) > 30) {
        // FE should only let 30 characters through, but check here too
        $model->groupname = substr($model->groupname, 0, 30);
    }

    if ($process !== E4S_BULK_UPDATE) {
        e4s_addStdPageInfo($obj, $model);
    }
    $model = e4s_deepCloneToObject($model);
    if (!is_null($compRow)) {
        $model->auditObj = e4sAudit::withRow($compRow);
    } else {
        $model->auditObj = e4sAudit::withID($model->compid);
    }

    return $model;
}

// If the singleAge property changes on the competition. Update all CE's to match
function e4s_updateCESingleAge($compid, $compSingleAge) {
    $sql = 'select id, options
            from ' . E4S_TABLE_COMPEVENTS . '
            where compid = ' . $compid;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows < 1) {
        return;
    }
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    foreach ($rows as $row) {
        $options = e4s_addDefaultCompEventOptions($row['options']);
        if ($options->singleAge !== $compSingleAge) {
            $options->singleAge = $compSingleAge;
            $options = e4s_getOptionsAsString($options);
            $updateSql = 'update ' . E4S_TABLE_COMPEVENTS . "
                          set options = '" . $options . "'
                          where id = " . $row['id'];
            e4s_queryNoLog($updateSql);
        }
    }
}

function e4s_processFormRows(&$options) {
    $formRows = $options->eventTeam->formRows;
    $formNo = $options->eventTeam->showForm;
    if ($formNo === FALSE) {
        // get next form to use
        $formNo = e4s_getNextFormNo();
    }
    $options->eventTeam->showForm = $formNo;
    e4s_clearFormDefs($formNo);
    e4s_createFormDefs($formNo, $formRows);
    unset($options->eventTeam->formRows);
}

function e4s_getNextFormNo() {
    $sql = 'select max(formno) lastformno 
            from ' . E4S_TABLE_EVENTTEAMFORMDEF;

    $result = e4s_queryNoLog($sql);
    $row = $result->fetch_assoc();
    return ((int)$row['lastformno'] + 1);
}

function e4s_clearFormDefs($formNo) {
    $sql = 'delete from ' . E4S_TABLE_EVENTTEAMFORMDEF . ' where formno = ' . $formNo;
    e4s_queryNoLog($sql);
}

function e4s_createFormDefs($formno, $formRows) {
    $rowSep = '';
    $sql = 'insert into ' . E4S_TABLE_EVENTTEAMFORMDEF . ' (formno,pos,label,required,options) values ';
    $pos = 1;
    foreach ($formRows as $formRow) {
        $sql .= $rowSep . '(';
        $sql .= $formno . ',';
        $sql .= $pos++ . ',';
        $sql .= "'" . $formRow->eventDef->name . "',";
        $sql .= '1' . ',';
        $sql .= '\'{"edid":' . $formRow->eventDef->id . ', "datetime":"' . $formRow->dateTime . '"}\'';
        $sql .= ')';
        $rowSep = ',';
    }

    e4s_queryNoLog($sql);
}

function e4s_resetCompEventTimes($model) {
    $compid = $model->compid;
    $update = 'update ' . E4S_TABLE_COMPEVENTS . ' set startdate = date(startdate) ' . ' where compid = ' . $compid;
    e4s_queryNoLog($update);
    Entry4UISuccess('');
}