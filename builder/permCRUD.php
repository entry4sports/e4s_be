<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

function deletePermission($payload) {
    $obj = getPermPayload($payload);

    if ($obj->id === 0) {
        Entry4UIError(9027, 'Invalid Permission ID', 400, '');
    }
    $sql = 'select *
            from ' . E4S_TABLE_PERMISSIONS . '
            where id = ' . $obj->id;

    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1) {
        Entry4UIError(9028, 'Invalid Permission ID', 400, '');
    }
    $permRow = $result->fetch_assoc();
    $userid = (int)$permRow['userid'];
    if ((int)$permRow['roleid'] !== 0) {
        deleteAllPermLevelsForUser($userid, $permRow['roleid']);
    }
    $sql = 'delete from ' . E4S_TABLE_PERMISSIONS . '
            where id = ' . $obj->id;
    $result = e4s_queryNoLog($sql);
    $perms = e4s_getPermissionsForUser($userid);
    Entry4UISuccess('
        "data":' . json_encode($perms, JSON_NUMERIC_CHECK));
}

function e4s_returnUserPermissions($obj) {
    $payload = getPermPayload($obj);
    $userid = $payload->userid;
    $perms = e4s_getPermissionsForUser($userid);
    Entry4UISuccess('
        "data":' . e4s_encode($perms));
}

function e4s_getPermissionsForUser($userid) {
    $sql = 'SELECT p.*, c.name compname, cc.club orgname, r.role
            FROM ((' . E4S_TABLE_PERMISSIONS . ' p left join ' . E4S_TABLE_COMPETITON . ' c on p.compid = c.id) left join ' . E4S_TABLE_COMPCLUB . ' cc on p.orgid = cc.id) left join ' . E4S_TABLE_ROLES . ' r on p.roleid = r.id
            WHERE userid = ' . $userid;

    $result = e4s_queryNoLog($sql);
    if ($result->num_rows < 1) {
        return array();
    }

    $permRows = $result->fetch_all(MYSQLI_ASSOC);

    $permLevelSql = 'select pl.roleid, r.Role role, pl.levelid id, l.level name
                     from   ' . E4S_TABLE_PERMISSIONLEVEL . ' pl,
                            ' . E4S_TABLE_ROLES . ' r,
                            ' . E4S_TABLE_ROLELEVEL . ' l
                     where  userid = ' . $userid . '
                     and    pl.roleid = r.id
                     and    pl.levelid = l.id';

    $result = e4s_queryNoLog($permLevelSql);
    $levelRows = $result->fetch_all(MYSQLI_ASSOC);
    $retArr = array();
    foreach ($permRows as $permRow) {
        $levelArr = array();
        foreach ($levelRows as $levelRow) {
            if ($levelRow['roleid'] === $permRow['roleid']) {
                $levelArr[] = $levelRow;
            }
        }
        $roleObj = new stdClass();
        $roleObj->id = $permRow['roleid'];
        $roleObj->name = $permRow['role'];
        if (is_null($roleObj->name)) {
            $roleObj->name = 'All';
        }
        unset($permRow['role']);
        unset($permRow['roleid']);
        $permRow['role'] = $roleObj;

        $compObj = new stdClass();
        $compObj->id = $permRow['compid'];
        $compObj->name = $permRow['compname'];
        if (is_null($compObj->name)) {
            $compObj->name = 'All';
        }else{
			$compObj->name = addslashes($compObj->name);
        }
        unset($permRow['compname']);
        unset($permRow['compid']);
        $permRow['comp'] = $compObj;

        $orgObj = new stdClass();
        $orgObj->id = $permRow['orgid'];
        $orgObj->name = $permRow['orgname'];
        if (is_null($orgObj->name)) {
            $orgObj->name = 'All';
        }
        unset($permRow['orgname']);
        unset($permRow['orgid']);
        $permRow['org'] = $orgObj;

        $permRow['permLevels'] = $levelArr;
        $retArr[] = $permRow;
    }
    return $retArr;
}
function e4s_addUserToOrg($obj) {
	$userId = e4s_getUserID();
	$orgId = checkJSONObjForXSS($obj, 'orgid:Org Id', E4S_CHECKTYPE_NUMERIC);
	if ( !is_null($orgId) ) {
		$orgId = (int)$orgId;
	}
	$addUserId = checkJSONObjForXSS($obj, 'userid:User Id');
	$isAdmin = userHasPermission($userId, $orgId, PERM_ADMIN);
	if (!$isAdmin) {
		Entry4UIError(9421, 'You do not have permission to add a user to this organisation.', 200, '');
	}
	$key = 'user_login';
	if ( strpos($addUserId, '@') !== false ) {
		$key = 'user_email';
	}
	// decode $addUserId
	$addUserId = urldecode($addUserId);
	$sql = 'select id
			from ' . E4S_TABLE_USERS . '
			where ' . $key . ' = "' . $addUserId . '"';
	$result = e4s_queryNoLog($sql);
	if ($result->num_rows !== 1) {
		Entry4UIError(9422, 'User not found', 200, '');
	}
	$userRow = $result->fetch_assoc();
	$sql = 'select *
			from ' . E4S_TABLE_ROLES . '
			where role = "Admin"';
	$result = e4s_queryNoLog($sql);
	if ($result->num_rows !== 1) {
		Entry4UIError(9423, 'Admin role not found.', 200, '');
	}
	$roleRow = $result->fetch_assoc();
	$payload = new stdClass();
	$payload->id = 0;
	$payload->userid = $userRow['id'];
	$payload->org = array('id' => $orgId);
	$payload->role = array('id' => $roleRow['id']);
	$payload->comp = array('id' => 0);
	$payload->permLevels = array();
	$permArr = updatePermissionID($payload, false, false);
	$usePerm = null;
	// loop through the output and get the correct values for the record just added
	foreach($permArr as $perm){
		if ((int)$perm['userid'] !== (int)$userRow['id']) {
			continue;
		}
		if ((int)$perm['role']->id !== (int)$roleRow['id']) {
			continue;
		}
		if ((int)$perm['org']->id !== $orgId) {
			continue;
		}
		$usePerm = $perm;
		break;
	}

	if (is_null($usePerm)) {
		Entry4UIError(9424, 'Error adding user to organisation.', 200, '');
	}
	$newOutput = new stdClass();
	$newOutput->permId = $usePerm['id'];
	$newOutput->compId = $usePerm['comp']->id;
	$newOutput->orgId = $usePerm['org']->id;
	$newOutput->role = $usePerm['role'];
	// get the user details for the response
	$userObj = e4sUserClass::withId($usePerm['userid']);
	$newOutput->user = $userObj->getRow();
	$newOutput->user->userEmail = $newOutput->user->email;
	unset($newOutput->user->email);
	Entry4UISuccess('
        "data":' . json_encode( $newOutput, JSON_NUMERIC_CHECK )
		,'Administrator has been added to your organisation.'
	);
}
function updatePermissionID($payload, $translatePayload = TRUE, $exit = TRUE) {
	if ($translatePayload) {
		$obj = getPermPayload($payload);
	}else{
		$obj = $payload;
	}

    $roleId = (int)$obj->role['id'];
    if ($roleId < 0) {
        Entry4UIError(1001, 'Please select a Role.', 400, '');
    }
    $orgId = (int)$obj->org['id'];
    if ($orgId < 0) {
        $orgId = 0;
    }

    $compId = (int)$obj->comp['id'];
    if ($compId < 0) {
        $compId = 0;
    }

    $id = (int)$obj->id;
    $userId = (int)$obj->userid;
    if ($id === 0) {
        // Create
        // check it does not already exit
        $sql = 'select *
                from ' . E4S_TABLE_PERMISSIONS . '
                where userid = ' . $userId . '
                and   roleid = ' . $obj->role['id'];

        $sql .= ' and orgid = ' . $orgId;
        $sql .= ' and compid = ' . $compId;

        $result = e4s_queryNoLog($sql);
        if ($result->num_rows > 0) {
            Entry4UIError(9029, 'Permission already exists.', 400, '');
        }
        $sql = 'Insert into ' . E4S_TABLE_PERMISSIONS . ' (userid,roleid, orgid,compid)
                values (' . $userId . ',' . $roleId . ',' . $orgId . ',' . $compId . ')';
    } else {
        // update
        $sql = 'update ' . E4S_TABLE_PERMISSIONS . '
                set userid =' . $userId . ',
                    roleid =' . $roleId . ',
                    orgid  =' . $orgId . ',
                    compid =' . $compId . '
                where id = ' . $id;
    }

    e4s_queryNoLog($sql);

    // Now easier to delete all PermLevels and re-create
    deleteAllPermLevelsForUser($userId, $roleId);
    $permLevels = $obj->permLevels;

    $permLevelValues = '';
    $valueSep = '';
    foreach ($permLevels as $permLevel) {
        $permLevelValues .= ($valueSep . '(' . $userId . ',' . $roleId . ',' . $permLevel['id'] . ')');
        $valueSep = ',';
    }
    if ($permLevelValues !== '') {
        e4s_queryNoLog('Insert into ' . E4S_TABLE_PERMISSIONLEVEL . '(userid, roleid, levelid)
                        values ' . $permLevelValues);
    }
    $perms = e4s_getPermissionsForUser($userId);
    // clear user cache of perms
    e4s_clearUserMeta($userId);
	if ($exit) {
		Entry4UISuccess( '
        "data":' . json_encode( $perms, JSON_NUMERIC_CHECK ) );
	}else{
		return $perms;
	}
}

function deleteAllPermLevelsForUser($userid, $roleid) {
    $plSql = 'delete from ' . E4S_TABLE_PERMISSIONLEVEL . '
              where userid = ' . $userid . '
              and   roleid = ' . $roleid;
    e4s_queryNoLog($plSql);
}

function getPermPayload($obj) {
    $model = new stdClass();

    $model->public = FALSE;
    $model->id = checkJSONObjForXSS($obj, 'id:Permission Id');
    if (is_null($model->id)) {
        $model->id = checkFieldForXSS($obj, 'id:Permission ID');
    }
    $model->userid = checkJSONObjForXSS($obj, 'userid:User Id');
    $model->role = checkJSONObjForXSS($obj, 'role:Role Object');
    $model->org = checkJSONObjForXSS($obj, 'org:Organisation Object');
    $model->comp = checkJSONObjForXSS($obj, 'comp:Competition Object');
    $model->permLevels = checkJSONObjForXSS($obj, 'permLevels:Permission Levels Object');

    e4s_addStdPageInfo($obj, $model);
    return $model;
}

function listAllDefinedPermissions() {
    if (array_key_exists(E4S_ALL_PERMS, $GLOBALS)) {
        return ($GLOBALS[E4S_ALL_PERMS]);
    }
    $roleSql = 'Select id, role name
                from ' . E4S_TABLE_ROLES;
    $result = e4s_queryNoLog($roleSql);
    $roleRows = $result->fetch_all(MYSQLI_ASSOC);
    $roleArr = array();
    foreach ($roleRows as $roleRow) {
        $roleArr [$roleRow ['id']] = $roleRow;
    }

    $levelSql = 'select id, roleid, level
                 from ' . E4S_TABLE_ROLELEVEL . '
                 order by roleid';
    $result = e4s_queryNoLog($levelSql);
    $levelRows = $result->fetch_all(MYSQLI_ASSOC);

    foreach ($levelRows as $levelRow) {
        $roleid = $levelRow['roleid'];
        if (isset($roleArr[$roleid])) {
            $row = $roleArr[$roleid];
            // Valid Role ID.
            $rowobj = new stdClass();
            $rowobj->id = $levelRow['id'];
            $rowobj->name = $levelRow['level'];
            if (array_key_exists('permLevels', $row)) {
                $row['permLevels'][] = $rowobj;
            } else {
                $newArr = array();
                $newArr[] = $rowobj;
                $row['permLevels'] = $newArr;
            }
            $roleArr[$roleid] = $row;
        } else {
            Entry4UIError(9030, 'Invalid permission : ' . $roleid);
        }
    }
    $GLOBALS[E4S_ALL_PERMS] = $roleArr;
    return $roleArr;
}