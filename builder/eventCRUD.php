<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

function e4s_updateEventsWithUniqueCeIds($obj) {
    $ceIds = checkJSONObjForXSS($obj, 'ceids:CeIDs to make unique');
    $eventObj = new e4sEvents();
    $eventObj->setUniqueEvents($ceIds);
}

function e4s_actionEvent($obj, $process) {
    $model = e4s_getEventModel($obj, $process);

    switch ($model->process) {
        case E4S_CRUD_LIST . 'defs':
            e4s_listEventDefs($model, TRUE);
            break;
        case E4S_CRUD_LIST:
            e4s_listEvents($model, TRUE);
            break;
        case E4S_CRUD_CREATE:
            e4s_createOrUpdateEventDef($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readEvent($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_createOrUpdateEventDef($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteEventDefAndGender($model, TRUE);
            break;
    }
}

function e4s_returnStdEventRow($row) {
    if (!is_null($row)) {
        $row['id'] = $row['ID'];
        unset($row['ID']);
        $row['name'] = $row['Name'];
        unset($row['Name']);
        $row['gender'] = $row['Gender'];
        unset($row['Gender']);
        $options = e4s_getOptionsAsObj($row['options']);
        $options = e4s_addDefaultEventOptions($options);
        $options = e4s_getEventDefInfo($options);
        $row['options']= $options;
        $uom = array();
        $uom['id'] = $row['uomid'];
        $uom['type'] = $row['eventType'];
        $uom['options'] = e4s_getOptionsAsObj($row['uomOptions']);
        unset($row['uomid']);
        unset($row['eventType']);
        unset($row['uomOptions']);
        $row['uom'] = $uom;
    }
    return $row;
}

function e4s_getEventDefInfo($options){
    if ( sizeof($options->multiEventOptions) === 0){
        return $options;
    }
    $globalKey = 'e4sEventDefIds';
    $multiEventDefIds = array();
    if ( !array_key_exists($globalKey, $GLOBALS )){
        $GLOBALS[$globalKey] = array();
    }
    foreach($options->multiEventOptions as $eventOption){
        if ( !array_key_exists($eventOption->eventDefId, $GLOBALS[$globalKey])) {
            $multiEventDefIds[] = $eventOption->eventDefId;
        }
    }
    $newCount = sizeof($multiEventDefIds);
    if ( $newCount > 0 ){
        $sql = 'select id,name
                from ' . E4S_TABLE_EVENTDEFS . '
                where id in (' . implode(',', $multiEventDefIds) . ')';
        $result = e4s_queryNoLog($sql);
        if ( $result->num_rows !== $newCount ){
            Entry4UIError(6833, 'Invalid count of events returned');
        }
        $newMultiOptions = [];
        while ($obj = $result->fetch_object()) {
            $obj->eventDefId = (int)$obj->id;
            unset($obj->id);
            $newMultiOptions[] = $obj;
        }
        $options->multiEventOptions = $newMultiOptions;
    }
    return $options;
}
function e4s_getEventRowCRUD($key, $value, $mustExist) {
    $row = e4s_getEventRow($key, $value, $mustExist);
    return e4s_returnStdEventRow($row);
}

function e4s_getEventRow($key, $value, $mustExist) {
    $row = null;
    $sql = 'select e.*
            from ' . E4S_TABLE_EVENTS . ' e
            where e.' . $key . " = '" . $value . "'";
    $result = e4s_queryNoLog('GetEventRecord' . E4S_SQL_DELIM . $sql);
    if ($mustExist) {
        if ($result->num_rows < 1) {
            Entry4UIError(2013, 'Invalid ' . $key, 400, '');
        }
        $row = $result->fetch_assoc();
    }
    if (!$mustExist and $result->num_rows > 0) {
        Entry4UIError(2014, 'Duplicate ' . $key, 400, '');
    }

    return $row;
}

function e4s_readEvent($model, $exit) {
    $row = e4s_getEventRowCRUD('id', $model->id, TRUE);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_createOrUpdateEventDef($model, $exit) {

    // Does the event definition exist
    $eventDefRow = e4s_getmultiCRUDRow(E4S_TABLE_EVENTDEFS, 'name' . E4S_MULTI_KEY_DELIM . 'tf', $model->name . E4S_MULTI_KEY_DELIM . $model->tf, null, E4S_MULTI_KEY_DELIM);

    if (is_null($eventDefRow)) {
        $eventDefRow = e4s_createEventDef($model);
    } else {
        $eventDefRow = e4s_updateEventDef($model, $eventDefRow);
    }

    // Now check and create the gender record
    $genderRow = e4s_getmultiCRUDRow(E4S_TABLE_EVENTGENDER, 'eventid' . E4S_MULTI_KEY_DELIM . 'gender', $eventDefRow['ID'] . E4S_MULTI_KEY_DELIM . $model->gender, null, E4S_MULTI_KEY_DELIM);

    if (is_null($genderRow)) {
        $genderRow = e4s_createEventGender($model, $eventDefRow);
    } else {
        $genderRow = e4s_updateEventGender($model, $eventDefRow);
    }
    $eventRow = e4s_getmultiCRUDRow(E4S_TABLE_EVENTS, 'id', $genderRow['id'], TRUE, '');
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($eventRow, JSON_NUMERIC_CHECK));
    }
    return $eventRow;
}

function e4s_getEventGenderOptions($model) {
    $options = e4s_getOptionsAsObj($model->options);
    $genderOptions = new stdClass();
    $genderOptions->min = 0;
    $genderOptions->max = 0;
    if (isset($options->min)) {
        $genderOptions->min = $options->min;
    }
    if (isset($options->max)) {
        $genderOptions->max = $options->max;
    }
    return $genderOptions;
}

function e4s_createEventGender($model, $eventDefRow) {
    $options = e4s_getEventGenderOptions($model);

    $min = $options->min;
    $max = $options->max;
    $options = e4s_removeBaseOptions(e4s_getEventDefDefaultOptions(), $options);
    $sql = 'Insert into ' . E4S_TABLE_EVENTGENDER . " (eventid, gender,min,max,options)
            values(
                '" . $eventDefRow['ID'] . "',
                '" . $model->gender . "',
                " . $min . ',
                ' . $max . ",
                '" . json_encode($options) . "'
            )";

    e4s_queryNoLog($sql);
    $eventDefRow = e4s_getmultiCRUDRow(E4S_TABLE_EVENTGENDER, 'id', e4s_getLastID(), TRUE, '');
    return $eventDefRow;
}

function e4s_updateEventGender($model, $eventDefRow) {
    $readSql = '
        select id 
        from ' . E4S_TABLE_EVENTGENDER . '
         where eventid = ' . $eventDefRow['ID'] . "
            and gender = '" . $model->gender . "'";
    $result = e4s_queryNoLog($readSql);
    if ($result->num_rows === 0) {
        Entry4UIError(5010, 'Failed to get Event for gender', 200, '');
    }
    $row = $result->fetch_assoc();
    $id = $row['id'];

    $options = e4s_getEventGenderOptions($model);
    // These need converting to secs before storing
    $min = $options->min;
    $max = $options->max;
    $options = e4s_removeBaseOptions(e4s_getEventDefDefaultOptions(), $options);

    $sql = 'update ' . E4S_TABLE_EVENTGENDER . "
            set eventid = '" . $eventDefRow['ID'] . "',
                gender = '" . $model->gender . "',
                options = '" . json_encode($options) . "',
                min = " . $min . ',
                max = ' . $max . '
            where id = ' . $id;

    e4s_queryNoLog($sql);

    $row = e4s_getmultiCRUDRow(E4S_TABLE_EVENTGENDER, 'id', $id, TRUE, '');
    return $row;
}

function e4s_updateEventDef($model, $row) {
    $options = e4s_removeBaseOptions(e4s_getEventDefDefaultOptions(), $model->options);
    $sql = 'update ' . E4S_TABLE_EVENTDEFS . "
                set name = '" . $model->name . "',
                    tf = '" . $model->tf . "',
                    options = '" . json_encode($options) . "',
                    uomid = " . $model->uom['id'] . '
                where id = ' . $row['ID'];

    e4s_queryNoLog($sql);
    $row = e4s_getmultiCRUDRow(E4S_TABLE_EVENTDEFS, 'id', $row['ID'], TRUE, '');
    return $row;
}

function e4s_createEventDef($model) {

    if (empty($model->uom)) {
        Entry4UIError(2556, 'Please select a UOM value');
    }
    $options = e4s_removeBaseOptions(e4s_getEventDefDefaultOptions(), $model->options);

    $sql = 'Insert into ' . E4S_TABLE_EVENTDEFS . " ( name,tf,options,uomid)
            values(
                '" . $model->name . "',
                '" . $model->tf . "',
                '" . json_encode($options) . "',
                " . $model->uom['id'] . '
            )';

    e4s_queryNoLog($sql);

    $row = e4s_getmultiCRUDRow(E4S_TABLE_EVENTDEFS, 'id', e4s_getLastID(), TRUE, '');

    return $row;
}

function e4s_createEvent($model, $exit) {
    e4s_getEventRow('id', $model->id, FALSE);
    $options = e4s_removeDefaultEventOptions($model->options);
    $sql = 'Insert into ' . E4S_TABLE_EVENTS . " ( name,tf,gender,options,uomid)
            values(
                '" . $model->name . "',
                '" . $model->tf . "',
                '" . $model->gender . "',
                '" . json_encode($options) . "',
                " . $model->uom['id'] . '
            )';

    e4s_queryNoLog($sql);

    $row = e4s_getEventRowCRUD('id', e4s_getLastID(), TRUE);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_updateEvent($model, $exit) {
    e4s_getEventRowCRUD('id', $model->id, TRUE);
    $options = e4s_removeDefaultEventOptions($model->options);
    $sql = 'update ' . E4S_TABLE_EVENTS . "
                set name = '" . $model->name . "',
                tf = '" . $model->tf . "',
                gender = '" . $model->gender . "',
                options = '" . $options . "',
                uomid = " . $model['uom']->id . '
            where id = ' . $model->id;
    e4s_queryNoLog('UPDATE' . E4S_SQL_DELIM . $sql);
    $row = e4s_getEventRowCRUD('id', $model->id, TRUE);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_deleteEventDefAndGender($model, $exit) {
    // delete the gender record
    $genderRow = e4s_getmultiCRUDRow(E4S_TABLE_EVENTGENDER, 'name' . E4S_MULTI_KEY_DELIM . 'tf', $model->name . E4S_MULTI_KEY_DELIM . $model->tf, null, E4S_MULTI_KEY_DELIM);
    if (is_null($genderRow)) {
        // Nothing to delete
        if ($exit) {
            Entry4UISuccess();
        }
        return null;
    }
    $deleteSql = 'delete from ' . E4S_TABLE_EVENTGENDER . '
                  where id = ' . $genderRow['ID'];
    e4s_queryNoLog($deleteSql);

    // if no more gender records for eent exist, delete the event
    $genderRow = e4s_getmultiCRUDRow(E4S_TABLE_EVENTGENDER, 'name', $model->name, null, '');
    if (!is_null($genderRow)) {
        // more genders exist
        if ($exit) {
            Entry4UISuccess();
        }
        return null;
    }
    $deleteSql = 'delete from ' . E4S_TABLE_EVENTDEFS . "
                  where name = '" . $model->name . "'
                  and   tf = '" . $model->tf . "'";
    e4s_queryNoLog($deleteSql);
    if ($exit) {
        Entry4UISuccess();
    }
    return null;
}

function e4s_deleteEvent($model, $exit) {

    // check Row exists
    $row = e4s_getLocRow('id', $model->id, TRUE);
    $sql = 'select id
            from ' . E4S_TABLE_COMPEVENTS . '
            where eventid = ' . $model->id;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows > 0) {
        Entry4UIError(2009, 'Unable to delete as in use', 400, '');
    }
    $sql = 'delete from ' . E4S_TABLE_EVENTS . ' 
            where id = ' . $model->id;

    e4s_queryNoLog($sql);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_listEventDefs($model, $exit) {
    $pagesize = $model->pageInfo->pagesize;
    $page = $model->pageInfo->page;
    $startswith = $model->pageInfo->startswith;

    $usePaging = FALSE;

    if (isset($pagesize) and isset($page)) {
        $usePaging = TRUE;
    }

    $sql = 'select ID id, Name name
        from ' . E4S_TABLE_EVENTDEFS . "
        where name not like ' %' ";

    if (isset($startswith)) {
        $sql .= " and name like '" . $startswith . "%' ";
    }

    $sql .= ' order by name ';

    if ($usePaging and $pagesize !== 0) {
        $sql .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
    }

    $result = e4s_queryNoLog($sql);
    $rows = $result->fetch_all(MYSQLI_ASSOC);

    if ($exit) {
        Entry4UISuccess('
             "data":' . json_encode($rows, JSON_NUMERIC_CHECK));
    }
    return $rows;
}

function e4s_listEvents($model, $exit) {
    $pagesize = $model->pageInfo->pagesize;
    $page = $model->pageInfo->page;
    $startswith = $model->pageInfo->startswith;

    $usePaging = FALSE;
    if (isset($pagesize) and isset($page)) {
        $usePaging = TRUE;
    }

    $sql = 'select e.*
        from ' . E4S_TABLE_EVENTS . ' e ';

    if (isset($startswith)) {
        $sql .= " where name like '%" . $startswith . "%' ";
    }

    $sql .= ' order by name ';

    if ($usePaging and $pagesize !== 0) {
        $sql .= ' limit ' . (($page - 1) * $pagesize) . ', ' . ($pagesize * 2);
    }

    $result = e4s_queryNoLog($sql);
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    $newRows = array();
    foreach ($rows as $row) {
        $newRows[] = e4s_returnStdEventRow($row);
    }
    if ($exit) {
        Entry4UISuccess('
             "data":' . json_encode($newRows, JSON_NUMERIC_CHECK));
    }
    return $newRows;
}


function e4s_getEventModel($obj, $process) {

    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;
    $model->id = checkFieldForXSS($obj, 'id:Event ID');
    $model->name = checkFieldForXSS($obj, 'name:Event name');
    $model->tf = checkFieldForXSS($obj, 'tf:Event type');
    $model->gender = checkFieldForXSS($obj, 'gender:Event gender');
    $model->options = checkJSONObjForXSS($obj, 'options:Event options');
    $model->uom = checkJSONObjForXSS($obj, 'uom:Event UOM');

    $model->pageInfo = new stdClass();
    $model->pageInfo->startswith = checkFieldForXSS($obj, 'startswith:Starts With');
    $model->pageInfo->page = checkFieldForXSS($obj, 'pagenumber:Page Number');
    $model->pageInfo->pagesize = checkFieldForXSS($obj, 'pagesize:Page Size');
    return $model;
}