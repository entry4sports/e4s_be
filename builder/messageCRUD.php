<?php
require_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_actionMessage($obj, $process, $action = '') {
    $model = e4s_getMessageModel($obj, $process);

    $model->action = $action;
    switch ($model->process) {
        case E4S_CRUD_LIST:
            e4s_listMessages($model, TRUE);
            break;
        case E4S_CRUD_CREATE:
            e4s_createMessage($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readMessage($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_updateMessage($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteMessage($model, TRUE);
            break;
    }
}

function e4s_getMessageModel($obj, $process): stdClass {
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;

    $model->compId = checkFieldForXSS($obj, 'compId:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $model->id = checkFieldForXSS($obj, 'id:Message ID');
    if (!is_null($model->id)) {
        $model->id = (int)$model->id;
    }
    $model->to = checkJSONObjForXSS($obj, 'to:Message To'); // array !
    $model->title = checkFieldForXSS($obj, 'subject:Message Subject/Title');
    $model->body = checkFieldForXSS($obj, 'body:Message');
    $model->options = checkJSONObjForXSS($obj, 'options:Message Options');

    $type = checkFieldForXSS($obj, 'type:Type of Message to Create');
    if (is_null($type)) {
        $type = E4S_MESSAGE_TYPE_MSG;
    }
    $model->type = $type;
    $resend = checkFieldForXSS($obj, 'resend:Resend Message');
    if (is_null($resend)) {
        $resend = FALSE;
    }
    $model->resend = $resend;

    $get = new stdClass();

    $get->messages = FALSE;
    $messages = checkFieldForXSS($obj, 'messages:Get Message');

    if ($messages) {
        $get->messages = TRUE;
    }
    $get->emails = FALSE;
    $emails = checkFieldForXSS($obj, 'emails:Get Emails');
    if ($emails) {
        $get->emails = TRUE;
    }

    $get->read = null;
    $read = checkFieldForXSS($obj, 'read:Get Read');
    if (!is_null($read)) {
        $get->read = $read;
    }

    $get->sent = null;
    $sent = checkFieldForXSS($obj, 'sent:Get Sent');
    if (!is_null($sent)) {
        $get->sent = $sent;
    }

    $get->deleted = FALSE;
    $deleted = checkFieldForXSS($obj, 'deleted:Get Deleted');
    if (!is_null($deleted)) {
        $get->deleted = $deleted;
    }

    $model->get = $get;
    e4s_addStdPageInfo($obj, $model);
    return $model;
}

function e4s_getMessageRow($key, $value, $mustExist, $action = '') {
    $rows = e4s_getMessageRowsWithCheck($key, $value, $mustExist, $action);
    if (sizeof($rows) !== 1) {
        Entry4UIError(5676, 'More than 1 Message exists');
    }
    return $rows[0];
}

//function e4s_getMessageRows($key, $value, $mustExist):array{
//    return e4s_getMessageRowsWithCheck($key, $value, $mustExist, "");
//}
function e4s_getMessageRowsWithCheck($key, $value, $mustExist, $action = ''): array {

    $delimiter = E4S_MULTI_KEY_DELIM;
    $values = explode($delimiter, $value);

    // current only id is passed as a key
    $msgObj = new e4SMessageClass();
    $msgObj->action = $action;

    $obj = $msgObj->getDefaultObj();
    $obj->id = (int)$values[0];

    $rows = $msgObj->getMessages($obj);

    if ($mustExist and sizeof($rows) === 0) {
        Entry4UIError(5674, 'Message does not exist');
    }
    if (!$mustExist and sizeof($rows) > 0) {
        Entry4UIError(5675, 'Message already exist');
    }
    return $rows;
}

function e4s_readMessage($model, $exit) {
    $row = e4s_getMessageRow('id', $model->id, TRUE);
    $arr = array();
    $arr[] = $row;

    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($arr[0], JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_createMessage($model, $exit) {
    $msgObj = new e4SMessageClass();
    $toArr = $model->to;
    $title = $model->title;
    $body = $model->body;
    $msgObj->resend = $model->resend;
    $msgObj->createMessage($model->type, $toArr, $title, $body);
    if ($exit) {
        Entry4UISuccess();
    }
}

function e4s_updateMessage($model, $exit) {
    $id = $model->id;
    $action = $model->action;

    $msgObj = new e4SMessageClass();
    $msgObj->action = $action;

    $row = null;
    switch ($action) {
        case 'UNREAD':
            $row = $msgObj->markRead($id, FALSE);
            break;
        case 'READ':
            $row = $msgObj->markRead($id);
            break;
        case 'UNDELETE':
            $row = $msgObj->markDeleted($id, FALSE);
            break;
        case 'DELETE':
            $row = $msgObj->markDeleted($id);
            break;
    }

//    $row = e4s_getMessageRow("id", $id, true, $action);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_deleteMessage($model, $exit) {
    $id = $model->id;
    if ($model->action === 'MARK') {
        $msgObj = new e4SMessageClass();
        $msgObj->markDeleted($id, FALSE);
        Entry4UISuccess();
    } else {
        return e4s_physicalDelete($model, $exit);
    }
    return FALSE;
}

function e4s_physicalDelete($model, $exit) {
    $id = $model->id;
    // check Row exists
    $row = e4s_getMessageRow('id', $id, TRUE);
    $sql = 'select *
            from ' . E4S_TABLE_MESSAGE . '
            where  id = ' . $id;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows > 0) {
        Entry4UIError(2001, 'Unable to delete as in use');
    }
    $sql = 'delete from ' . E4S_TABLE_MESSAGE . '
            where id = ' . $id;

    e4s_queryNoLog($sql);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_listMessages($model, $exit): array {
    $msgObj = new e4SMessageClass();
    $messages = $msgObj->getMessages($model);
    // ui wants array not associated
    $arr = array();
    foreach ($messages as $message) {
        $arr[] = $message;
    }
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($arr, JSON_NUMERIC_CHECK));
    }
    return $messages;
}

function e4s_mail($sendTo, $subject, $body, $headers = null, $priority = 1, $testUser = E4S_SUPPORT_EMAIL) {
    if (is_null($headers)) {
        $headers = Entry4_mailHeader('', FALSE);
    }
    // stop the post wp_mail calling e4s_postWPMail
    remove_action('wp_mail_smtp_mailcatcher_smtp_send_after', 'e4s_afterWPMail', 10);
    $obj = new e4SMessageClass(0, $testUser);
    return $obj->wp_mail($sendTo, $subject, $body, $headers, $priority);
}

function e4s_postWPMail($sendTo, $subject, $body, $from) {
    $obj = new e4SMessageClass(0, E4S_SUPPORT_EMAIL);
    return $obj->postWPMail($sendTo, $subject, $body, $from);
}