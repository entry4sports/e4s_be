<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

// This is actually competition Organisations NOT ClubCompetitions ( ESAA )
function e4s_actionCompClub($obj, $process, $admin = false) {

    $model = e4s_getCompClubModel($obj, $process, $admin);

    switch ($model->process) {
	    case E4S_CRUD_LIST . "My":
			$model->myOrgs = true;
		    e4s_listCompClubs($model, TRUE);
		    break;
        case E4S_CRUD_LIST:
            e4s_listCompClubs($model, TRUE);
            break;
        case E4S_CRUD_CREATE:
            e4s_createCompClub($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readCompClub($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_updateCompClub($model, TRUE);
            break;
		case E4S_CRUD_UPDATE . 'Approve':
            e4s_approveCompClub($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteCompClub($model, TRUE);
            break;
    }
}

function e4s_getCompClubRowCRUD($key, $value, $mustExist) {
    $row = e4s_getCompClubRow($key, $value, $mustExist);
    $row['name'] = $row['club'];
    unset ($row['club']);
    $stripeuserid = (int)$row['stripeuserid'];
    unset($row['stripeuserid']);
    $obj = new stdClass();
    $obj->id = $stripeuserid;
    $obj->name = '';
    $obj->showPaymentCode = false;
    if ($stripeuserid !== 0) {
        $sql = 'select user_nicename name
                from ' . E4S_TABLE_USERS . '
                where id = ' . $stripeuserid;
        $result = e4s_queryNoLog($sql);
        if ($result->num_rows === 1) {
            $userRow = $result->fetch_assoc();
            $obj->name = $userRow['name'];
        }
		$sql = "select stripeuserid
				from " . E4S_TABLE_COMPCLUB . "
				where club = 'National'";
		$result = e4s_queryNoLog($sql);
		$nationalRow = $result->fetch_assoc();
		if ( (int)$nationalRow['stripeuserid'] === $stripeuserid ){
			$obj->showPaymentCode = true;
		}

    }
    $row['stripeUser'] = $obj;
    $row['options'] = e4s_getOptionsAsObj($row['options']);
    return $row;
}

function e4s_getCompClubRow($keys, $values, $mustExist) {
    return e4s_getCRUDRow(E4S_TABLE_COMPCLUB, $keys, $values, $mustExist);
}

function e4s_readCompClub($model, $exit) {
    $row = e4s_getCompClubRowCRUD('id', $model->id, TRUE);

//	if ( $model->admin ){
//		$row['permissions'] = e4sCompetition::getOrganisersForComp(0, $model->id);
//	}
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_createCompClub($model, $exit) {
	// check for linkUserId to create userOrg link
    if ((int)$model->id !== 0) {
        Entry4UIError(2006, 'ID of non zero passed to create', 400, '');
    }
	if ( e4s_checkStatusOfExistingCompClub() > 0 ) {
		Entry4UIError( 2007, 'You already have an organisation awaiting approval. Please allow this to be processed before creating another or contact support@entry4sports.com.', 200, '' );
	}
    e4s_getCompClubRow('club', $model->name, FALSE);
    $stripeUserID = $model->stripeUser['id'];
	if ( $stripeUserID === 0 ) {
		$config = e4s_getConfig();

		if (isset($config['options']->stripeMandatory)) {
			if ( $config['options']->stripeMandatory ) {
				$stripeUserID = e4s_getUserID();
			}
		}
	}
    $sql = 'Insert into ' . E4S_TABLE_COMPCLUB . " ( club, logo, stripeuserid, options )
            values('" . $model->name . "','" . $model->logo . "'," . $stripeUserID . ",'" . $model->options . "')";
    e4s_queryNoLog($sql);
    $row = e4s_getCompClubRowCRUD('club', $model->name, TRUE);

	e4s_checkForOrgLinkage($model->linkUserId, $row['id']);
	e4s_getUserObj(true, false);

    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}
function e4s_checkStatusOfExistingCompClub(){
	$userId = e4s_getUserID();

	$sql = 'select club
			from ' . E4S_TABLE_COMPCLUB . ' cc,
				 ' . E4S_TABLE_PERMISSIONS . ' p
			where userId = ' . $userId . ' 
			and p.orgId = cc.id
			and status != "' . E4S_ORG_APPROVED . '"';

	$result = e4s_queryNoLog($sql);
	return $result->num_rows;
}

function e4s_checkForOrgLinkage($userId, $orgId){
	if ( isE4SUser() and $userId === 0 ) {
		return;
	}
	if ( !isE4SUser() or $userId === 0 ) {
		$userId = e4s_getUserID();
	}

	$sql = 'select *
			from ' . E4S_TABLE_PERMISSIONS . '
			where userId = ' . $userId . ' and orgId = ' . $orgId;
	$result = e4s_queryNoLog($sql);
	if ( $result->num_rows === 0 ) {
		$sql = "select id
                from " . E4S_TABLE_ROLES . "
				where role = 'admin'";
		$result = e4s_queryNoLog($sql);
		$row = $result->fetch_assoc();
		$roleId = $row['id'];
		$sql = 'insert into ' . E4S_TABLE_PERMISSIONS . ' (userId, orgId, roleid)
				values (' . $userId . ', ' . $orgId . ',' . $roleId . ')';
		e4s_queryNoLog($sql);
	}
}
function e4s_approveCompClub($model, $exit) {
	if (!isAppAdmin()) {
		Entry4UIError(2004, 'Not authorised', 400, '');
	}
	$sql = 'update ' . E4S_TABLE_COMPCLUB . "
			set status = '" . E4S_ORG_APPROVED . "'
			where id = " . $model->id;
	e4s_queryNoLog($sql);

	if ($exit) {
		Entry4UISuccess('
		"data":{
			"id":' . $model->id . ',
			"status":"' . E4S_ORG_APPROVED . '"
		}');
	}
	return E4S_ORG_APPROVED;
}
function e4s_updateCompClub($model, $exit) {
    $row = e4s_getCompClubRowCRUD('id', $model->id, TRUE);
    $sql = 'update ' . E4S_TABLE_COMPCLUB . "
            set club ='" . $model->name . "',
                logo ='" . $model->logo . "',
                options ='" . $model->options . "',
                stripeuserid = " . $model->stripeUser['id'] . '
            where id = ' . $model->id;
    e4s_queryNoLog($sql);

    $row['name'] = $model->name;

    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_deleteCompClub($model, $exit) {
    // check Row exists
    $row = e4s_getCompClubRowCRUD('id', $model->id, TRUE);
    $sql = 'select *
            from ' . E4S_TABLE_COMPETITON . '
            where  compclubid = ' . $model->id;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows > 0) {
        Entry4UIError(2005, 'Unable to delete as in use', 400, '');
    }
    $sql = 'delete from ' . E4S_TABLE_COMPCLUB . '
            where id = ' . $model->id;

    e4s_queryNoLog($sql);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_listCompClubs($model, $exit) {
    $usePaging = FALSE;
    if (isset($model->pageInfo->pagesize) and isset($model->pageInfo->page)) {
        $usePaging = TRUE;
    }
	$orgs = array();
	if ( $model->myOrgs ) {
		$userId = e4s_getUserID();
		if ( isE4SUser() and $model->linkUserId !== 0 ) {
			$userId = $model->linkUserId;
		}
		$sql = 'select orgId
				from ' . E4S_TABLE_PERMISSIONS . '
				where userId = ' . $userId;

		$result = e4s_queryNoLog($sql);
		$rows = $result->fetch_all(MYSQLI_ASSOC);

		foreach ($rows as $row) {
			$orgs[] = $row['orgId'];
		}
		if ( count($orgs) === 0 ) {
			if ( $exit ) {
				Entry4UISuccess( '
                    "data":[]' );
			}

			return [];
		}
	}
    $sql = 'select id, club name, status
        from ' . E4S_TABLE_COMPCLUB . '
        where id != ' . E4S_COMPCLUB_ID;

	if ( count($orgs) > 0 ) {
		$sql .= ' and id in (' . implode(',', $orgs) . ')';
	}
	if (isset($model->status) and $model->status !== '') {
		$sql .= " and status = '" . $model->status . "' ";
	}
	if (isset($model->pageInfo) and isset($model->pageInfo->search)) {
		$sql .= " and club like '%" . $model->pageInfo->search . "%' ";
	}
    if (isset($model->pageInfo) and isset($model->pageInfo->startswith)) {
        $sql .= " and club like '" . $model->pageInfo->startswith . "%' ";
    }
    $sql .= ' order by club ';
    if ($usePaging and $model->pageInfo->pagesize !== 0) {
        $sql .= ' limit ' . (($model->pageInfo->page - 1) * $model->pageInfo->pagesize) . ', ' . $model->pageInfo->pagesize;
    }

    $result = e4s_queryNoLog($sql);
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    $useRows = array();
    foreach ($rows as $row) {
		$row = e4s_getCompClubRowCRUD('id', $row['id'], true);
        $row['locations'] = getLocsForOrg($row['id']);
        $useRows[] = $row;
    }
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($useRows, JSON_NUMERIC_CHECK));
    }
    return $rows;
}

function e4s_getCompClubModel($obj, $process, $admin = false) {

    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;
	$model->admin = $admin;
    $model->id = checkFieldForXSS($obj, 'id:Organiser ID');
    $model->name = checkFieldFromParamsForXSS($obj, 'name:Organiser name');
    $model->logo = checkFieldFromParamsForXSS($obj, 'logo:Organiser logo');
    $model->status = checkFieldFromParamsForXSS($obj, 'status:Organiser status');
//    $model->options = checkFieldFromParamsForXSS($obj, 'options:Organiser options');
    $model->options = '{}';
    $model->stripeUser = checkJSONObjForXSS($obj, 'stripeUser:Stripe User');
	$model->myOrgs = false;
    $model->linkUserId = checkJSONObjForXSS($obj, 'linkUserId:linkUserId');
	if ( is_null($model->linkUserId) ){
		$model->linkUserId = 0;
	}
    e4s_addStdPageInfo($obj, $model);
    return $model;
}