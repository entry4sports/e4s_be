<?php
include_once E4S_FULL_PATH . 'dbInfo.php';

if (isset($pagesize) and isset($page)) {
    $usePaging = TRUE;
} else {
    $usePaging = FALSE;
}

$sql = 'select id, name, shortname
        from ' . E4S_COMMON_PREFIX . 'Area ';

if (isset($startswith)) {
    $sql .= " where name like '" . $startswith . "%'";
}


$sql .= 'order by name';

if ($usePaging and $pagesize !== 0) {
    $sql .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
}

logSql($sql, 1);
$result = mysqli_query($conn, $sql);
$orgs = $result->fetch_all(MYSQLI_ASSOC);
Entry4UISuccess('
    "data":' . json_encode($orgs, JSON_NUMERIC_CHECK));
