<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/compEventCRUD.php';

function e4s_actionCEArr($obj, $process) {
    $modelArr = e4s_getCompEventArrModel($obj, $process);

    if (gettype($modelArr) === 'array') {
        if (isset($modelArr[0]->compid)) {
            $compid = $modelArr[0]->compid;
            $modelArr[0]->auditObj = e4sAudit::withID($compid);
        }
    }

    switch ($process) {
        case E4S_CRUD_LIST:
            e4s_listCompEventArrs($modelArr, TRUE);
            break;
        case E4S_CRUD_CREATE:
        case E4S_CRUD_UPDATE:
            e4s_processCompEventArrWithSplitCheck($modelArr, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readCompEventArr($modelArr, TRUE);
            break;

        case E4S_CRUD_DELETE:
            e4s_deleteCompEventArr($modelArr, TRUE);
            break;
        case E4S_BULK_UPDATE:
            e4s_bulkUpdate($modelArr);
            break;
    }
}

function e4s_readCompEventArr($modelArr, $exit) {
    $arr = array();
    foreach ($modelArr as $model) {
        $row = e4s_readCompEvent($model, FALSE);
        $arr[] = $row;
    }
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($arr, JSON_NUMERIC_CHECK));
    }
    return $arr;
}
function e4s_processCompEventArrWithSplitCheck($modelArr, $exit) {
    $updateArr = [];
    $createArr = [];

    $auditObj = $modelArr[0]->auditObj;
    foreach($modelArr as $model){
        if ( $model->process === E4S_CRUD_CREATE and isset($model->createOptions) ) {
            $key = "";
            if ($model->createOptions->addGender){
                $key .= $model->event['gender'];
            }
            if( $model->createOptions->addAge ){
                if ( $key !== "" ){
                    $key .= "-";
                }
                $key .= $model->age['name'];
            }
            if ( !array_key_exists($key, $createArr)){
                $createArr[$key] = [];
            }
			if ( $key !== "" ){
				$model->groupname .= ' ' . $key;
			}

//            $model->maxgroup = "";
            $createArr[$key][] = $model;
        } else {
            $updateArr[] = $model;
        }
    }

    if ( sizeof($createArr) > 0 ){
        $returnArr = [];
        // split out all the creates and create them individually, All others ( updates/deletes ) can be done in one go
        if ( sizeof($updateArr) > 0 ){
            $updateArr[0]->auditObj = $auditObj;
            $returnArr = e4s_processCompEventArr($updateArr, FALSE);
        }
        foreach($createArr as $key=>$createModels){
            $createModels[0]->auditObj = $auditObj;
            $createdArr = e4s_processCompEventArr($createModels, FALSE);
            $returnArr = array_merge($returnArr, $createdArr);
        }
        if ($exit) {
            Entry4UISuccess('
            "data":' . json_encode($returnArr, JSON_NUMERIC_CHECK));
        }
        return $returnArr;
    } else {
        return e4s_processCompEventArr($modelArr, $exit);
    }
}
// from builder. an EG has been created/updated
function e4s_processCompEventArr($modelArr, $exit) {
    $arr = array();

    foreach($modelArr as $model){
        if ( $model->maxathletes > -1 and $model->price->name === E4S_QUALIFY_PRICE ){
            Entry4UIError(5060, 'Please select a price definition.');
        }
    }

    $compId = $model->compid;

    $eventGroupObj = new eventGroup($compId);
    $firstModel = $modelArr[0];

    if ($firstModel->crud !== E4S_CRUD_DELETE) {
        e4s_checkMaxInHeat($firstModel);
		$isPartialUpdate = e4s_IsEventArrPartial($modelArr);
        $eventGroupID = $eventGroupObj->updateEGFromCEModel($firstModel, $isPartialUpdate);
    } else {
        $eventGroupID = $firstModel->maxgroup;
    }

    $entryObj = new entryCountsClass($compId);

    foreach ($modelArr as $origModel) {
        $model = e4s_deepCloneToObject($origModel);
        if ($entryObj->isEventClosed($eventGroupID)) {
            // the limit has been reached with paid entries or event forced Closed
            $model->isopen = E4S_EVENT_CLOSED;
//        } else {
//            $model->isopen = E4S_EVENT_OPEN;
        }
        $model->auditObj = $modelArr[0]->auditObj;

        if ($model->id !== 0) {
            // use the id as this has been "injected" from the getmodel method
            $keys = 'id';
            $values = $model->id;
        } else {
            // passed 0 ( Create ) so check if it exists
            $keys = 'compid' . E4S_MULTI_KEY_DELIM . 'eventid' . E4S_MULTI_KEY_DELIM . 'agegroupid' . E4S_MULTI_KEY_DELIM . 'maxgroup' . E4S_MULTI_KEY_DELIM . 'startdate';
            $values = $model->compid . E4S_MULTI_KEY_DELIM . $model->event->id . E4S_MULTI_KEY_DELIM . $model->age->id . E4S_MULTI_KEY_DELIM . $model->groupname . E4S_MULTI_KEY_DELIM . $model->startdate;
        }
        $row = e4s_getCEActualRow($keys, $values, E4S_MULTI_KEY_DELIM);

        $model->verified = TRUE;
        // stop the individual create/updates from doing the eventGroup stuff
        $model->maxgroupFromArr = $eventGroupID;
        if (is_null($row)) {
            if ($model->crud !== E4S_CRUD_DELETE) {
                $row = e4s_createCompEvent($model, FALSE);
                $row['e4sStatus'] = 'Created';
            }
        } else {
            $model->id = (int)$row['ID'];
            if ($model->crud === E4S_CRUD_DELETE) {
                $row = e4s_deleteCompEvent($model, FALSE);
                $row['eventname'] = $model->event->name;
                $row['e4sStatus'] = 'Deleted';
            } else {
                $row = e4s_updateCompEvent($model, FALSE);
                $row['e4sStatus'] = 'Updated';
            }
        }

        $retModel = new stdClass();
        $retModel->eventname = $row['eventname'];
        $retModel->status = $row['e4sStatus'];
        $retModel->ageGroup = $model->age->name;
        $retModel->id = (int)$row['ID'];
        $arr[] = $retModel;
    }
    if ( isset($model->options->multiEventOptions) ) {
        if (sizeof($model->options->multiEventOptions->childEvents) > 0) {
            $multiObj = new multiEventGroup($eventGroupID, null);
            $multiObj->masterEventBeingSaved($modelArr);
        }
    }
    $compObj = e4s_getCompObj($compId);
    $compObj->updateCompOptions();
    $eventGroupObj->cleanUpUnUsed();
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($arr, JSON_NUMERIC_CHECK));
    }
    return $arr;
}
function e4s_IsEventArrPartial($modelArr) : bool{
	$sql = "select count(*) as cnt
			from " . E4S_TABLE_COMPEVENTS . "
			where maxgroup = '" . $modelArr[0]->maxgroup . "'";
	$result = e4s_queryNoLog($sql);
	$row = $result->fetch_assoc();
	return (int)$row['cnt'] !== sizeof($modelArr);
}

function e4s_deleteCompEventArr($model, $exit) {
    $rows = e4s_deleteCompEvent($model, FALSE);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($rows, JSON_NUMERIC_CHECK));
    }
    return $rows;
}

function e4s_listCompEventArrs($modelArr, $exit) {
// ??
}

function e4s_getCompEventArrModel($obj, $process) {
    $action = checkFieldForXSS($obj, 'action:Action');
    $createOptions = checkJSONObjForXSS($obj, 'createOptions:Create Options');

    if ( !is_null($createOptions) ){
        $createOptions = e4s_getDataAsType($createOptions, E4S_OPTIONS_OBJECT);
    }
    if (!is_null($action) or $process === E4S_CRUD_DELETE) {
        // Moving/add event to another
        $model = new stdClass();
        $model->ceids = checkJSONObjForXSS($obj, 'ceids:CompEvent IDs');
        $model->action = $action;
        $model->eventno = checkFieldForXSS($obj, 'eventNo:Event Number'); // Is this used ?
        $model->auditObj = null;
        return array($model);
    }

    $model = e4s_getCompEventModel($obj, $process);

    if ( $model->maxathletes === ""){
        $model->maxathletes = 0;
    }else{
        $model->maxathletes = (int)$model->maxathletes;
    }
    if ($process === E4S_BULK_UPDATE) {
        $model->ceids = checkJSONObjForXSS($obj, 'ceids:CompEvent IDs');
        $model->compevent = checkJSONObjForXSS($obj, 'compEvent:CompEvent Object');
        return $model;
    }

    unset($model->age);
    unset($model->ceids);
    $ageCeidLinkGender = checkJSONObjForXSS($obj, 'ageCeidGenders:Genders');

    $events = checkJSONObjForXSS($obj, 'events:Events');
    $useEvents = array();
    foreach ($events as $event) {
        $useEvents [$event['id']] = $event;
    }
// Get list of age groups
    $sql = 'select id, name
            from ' . E4S_TABLE_AGEGROUPS;
    $ageResult = e4s_queryNoLog($sql);
    $ageGroupRows = $ageResult->fetch_all(MYSQLI_ASSOC);
    $aggArr = array();
    foreach ($ageGroupRows as $ageGroupRow) {
        $aggArr[$ageGroupRow['id']] = $ageGroupRow;
    }
    $arr = array();
    foreach ($ageCeidLinkGender as $key => $link) {
        $indivModel = clone $model;
        $model->auditObj = null;
        unset($indivModel->events);
        unset($indivModel->event);
        $indivModel->event = $useEvents [$link['eventId']];

        $indivModel->age = $aggArr [$link['ageId']];
        $indivModel->id = $link['ceid'];
        $indivModel->crud = $link['crud'];

        if ($indivModel->id === 0) {
            if ($indivModel->crud === E4S_CRUD_DELETE) {
                Entry4UIError(9017, 'Can not delete id of 0', 400, '');
            }
            $indivModel->crud = E4S_CRUD_CREATE;
        } else {
            $indivModel->crud = $link['crud'];
        }
        $indivModel->createOptions = $createOptions;
        $arr[] = $indivModel;
    }

    return $arr;
}

function e4s_updateCommonCEFields($model) {
// Certain values should be the same on all records for the same eventno ( maxGroup )
// this should really be a linked table, but ...
    $startdate = e4s_iso_to_sql($model->startdate);
    $sortdate = e4s_iso_to_sql($model->sortdate);

    $options = $model->options;

    $model = eventGroup::getMaxInHeat($options, 8);

    $sql = 'select *
            from ' . E4S_TABLE_COMPEVENTS . '
            where compid = ' . $model->compid . "
            and   maxgroup = '" . $model->maxgroup . "'";
    $results = e4s_queryNoLog($sql);

    if ($results->num_rows < 1) {
        return;
    }
    $rows = $results->fetch_all(MYSQLI_ASSOC);
    foreach ($rows as $row) {
        $options = e4s_getOptionsAsObj($row['options']);
        $options = eventGroup::setMaxInHeat($options, $model->maxInHeat);
        $options = e4s_removeDefaultCompEventOptions($options);
        $options = json_encode($options);

        $sql = 'update ' . E4S_TABLE_COMPEVENTS . "
            set startdate = '" . $startdate . "',
                sortdate = '" . $sortdate . "',
                split = " . $model->split . ',
                maxathletes = ' . $model->maxathletes . " ,
                options = '" . $options . "'
            where id = " . $row['ID'];

        e4s_queryNoLog($sql);
    }
}

function e4s_bulkUpdate($model) {
    $ceids = $model->ceids;
    unset($model->ceids);
    unset($model->process);
    unset($model->public);

    foreach ($ceids as $ceid) {
        $keys = 'id';
        $values = $ceid;
        $row = e4s_getCEActualRow($keys, $values, E4S_MULTI_KEY_DELIM);
        if (is_null($row)) {
            Entry4UIError(9018, 'Invalid CEID passed', 400, '');
        }
        $model->auditObj->writeAudit('Bulk update', TRUE, E4S_TABLE_COMPEVENTS, $ceid);
        e4s_updateCompEventBulkUpdate($row, $model->compevent);
    }

    Entry4UISuccess('');
}