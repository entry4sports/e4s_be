<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

function e4s_actionDisc($obj, $process) {

    $model = e4s_getDiscModel($obj, $process);

    switch ($model->process) {
        case E4S_CRUD_LIST:
            e4s_listDiscs($model, TRUE);
            break;
        case E4S_CRUD_CREATE:
            e4s_createDisc($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readDisc($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_updateDisc($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteDisc($model, TRUE);
            break;
    }
}

function e4s_DiscInUse($model) {
    $sql = 'select id
            from ' . E4S_TABLE_ENTRIES . '
            where  discountid = ' . $model->id;
    $result = e4s_queryNoLog($sql);
    return $result->num_rows;
}

function e4s_getDiscRowCRUD($key, $value, $mustExist, $delimeter) {
    $row = e4s_getDiscRow($key, $value, $mustExist, $delimeter);
    return e4s_returnStdDiscRow($row);
}

function e4s_returnStdDiscRow($row) {
    if (is_null($row) === FALSE) {
        renameBuilderField($row, 'ID', 'id');
        renameBuilderField($row, 'compid', 'compId');
        renameBuilderField($row, 'reg_disc', 'regDisc');
        renameBuilderField($row, 'sale_disc', 'saleDisc');
//        $row['id'] = $row['ID'];
//        unset ($row['ID']);

        if ($row['options'] === '') {
            $row['options'] = '{}';
        }
        $options = json_decode($row['options']);
        $row['options'] = e4s_discAddUIOptions($options);
    }
    $row = e4s_getDiscMetaData($row);
    return $row;
}

function e4s_getDiscRow($key, $value, $mustExist, $delimeter) {
    return e4s_getmultiCRUDRow(E4S_TABLE_COMPDISCOUNTS, $key, $value, $mustExist, $delimeter);
}

function e4s_getDiscMetaData(&$row) {
    $metaModel = new stdClass();
    $metaModel->id = (int)$row['agegroupid'];
    if ($metaModel->id !== 0) {
        include_once E4S_FULL_PATH . 'builder/ageCRUD.php';
        $row['ageGroup'] = e4s_readAge($metaModel, FALSE);
    } else {
        $row['ageGroup'] = $metaModel;
    }

    unset($row['agegroupid']);
    return $row;
}

function e4s_readDisc($model, $exit) {
    $row = e4s_getDiscRowCRUD('id', $model->id, TRUE, '');

    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_getDiscs($model) {

    $sql = 'select *
            from ' . E4S_TABLE_COMPDISCOUNTS . "
            where compid = $model->compid
            and   agegroupid in ()
    ";
}

function e4s_createDisc($model, $exit) {

    if ((int)$model->id !== 0) {
        Entry4UIError(2515, 'ID of non zero passed to create', 400, '');
    }
	$created = array();
	foreach($model->agegroupids as $agegroupid) {
		// check it not going to create a duplicate
		e4s_getDiscRow( 'compid' . E4S_MULTI_KEY_DELIM . 'agegroupid', $model->compid . E4S_MULTI_KEY_DELIM . $agegroupid, FALSE, E4S_MULTI_KEY_DELIM );
		$options = e4s_discRemoveUnwantedOptions( $model->options );
		$sql     = 'Insert into ' . E4S_TABLE_COMPDISCOUNTS . " (compid, agegroupid, sale_disc, reg_disc, type, count, options)
            values(
                '" . $model->compid . "',
                '" . $agegroupid . "',
                " . $model->saledisc . ',
                ' . $model->regdisc . ",
                '" . $model->type . "',
                " . $model->count . ',' . "'" . json_encode( $options ) . "')";

		e4s_queryNoLog( $sql );

		$row = e4s_getDiscRowCRUD( 'id', e4s_getLastID(), TRUE, '' );
		$created[] = $row;
		$model->auditObj->writeAudit( 'Create Disc Record : ' . $row['id'], TRUE, E4S_TABLE_COMPDISCOUNTS, $row['id'], $row );
	}
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($created, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_updateDisc($model, $exit) {
    $row = e4s_getDiscRow('id', $model->id, TRUE, '');

    $options = e4s_discRemoveUnwantedOptions($model->options);

    $model->auditObj->writeAudit('update Disc Record : ' . $model->id, TRUE, E4S_TABLE_COMPDISCOUNTS, $model->id, $row);
    $sql = 'update ' . E4S_TABLE_COMPDISCOUNTS . '
            set compid =' . $model->compid . ',
            agegroupid =' . $model->agegroupid . ",
            options ='" . json_encode($options) . "',
            sale_disc = " . $model->saledisc . ',
            reg_disc = ' . $model->regdisc . ",            
            type = '" . $model->type . "',
            count = " . $model->count . '
            where id = ' . $model->id;
    e4s_queryNoLog($sql);
    $row = e4s_getDiscRowCRUD('id', $model->id, TRUE, '');
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_deleteDisc($model, $exit) {
    // check Row exists
    $row = e4s_getDiscRow('id', $model->id, TRUE, '');

    if (e4s_DiscInUse($model) > 0) {
        Entry4UIError(2008, 'Unable to delete as in use (' . $model->id . ')', 400, '');
    }

    if (is_null($model->auditObj)) {
        $model->auditObj = e4sAudit::withID($row['compid']);
    }
    $model->auditObj->writeAudit('Delete Disc Record : ' . $model->id, TRUE, E4S_TABLE_COMPDISCOUNTS, $model->id, $row);
    $sql = 'delete from ' . E4S_TABLE_COMPDISCOUNTS . '
            where id = ' . $model->id;

    e4s_queryNoLog($sql);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }

    return $row;
}

function e4s_listDiscs($model, $exit) {
    try {
        $usePaging = FALSE;

        if (isset($model->pageInfo) === FALSE) {
            $model->pageInfo = new stdClass();
            $model->pageInfo->startswith = '';
            $model->pageInfo->page = 0;
            $model->pageInfo->pagesize = 0;
        }
        if (isset($model->pageInfo->pagesize) and isset($model->pageInfo->page)) {
            $usePaging = TRUE;
        }

        $sql = 'select *
        from ' . E4S_TABLE_COMPDISCOUNTS . '
        where compid = ' . $model->compid . ' ';

        if (is_null($model->pageInfo->startswith) === FALSE and $model->pageInfo->startswith !== '') {
            $sql .= " and type like '" . $model->pageInfo->startswith . "%' ";
        }

        $sql .= ' order by id ';

        if ($usePaging and $model->pageInfo->pagesize !== 0) {
            $sql .= ' limit ' . (($model->pageInfo->page - 1) * $model->pageInfo->pagesize) . ', ' . $model->pageInfo->pagesize;
        }

        $result = e4s_queryNoLog($sql);
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $newRows = array();
        foreach ($rows as $row) {
            $newRows[] = e4s_returnStdDiscRow($row);
        }
        if ($exit) {
            Entry4UISuccess('
                "data":' . json_encode($newRows, JSON_NUMERIC_CHECK));
        }
        return $newRows;
    } catch (mysqli_sql_exception $ex) {
        throw new Exception("Can't connect to the database! \n" . $ex);
    }
//    return [];
}

function e4s_getDiscModel($obj, $process) {
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;
    $model->id = checkFieldForXSS($obj, 'id:discount ID');
    $model->compid = checkFieldForXSS($obj, 'compId:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $model->auditObj = null;
    if (!is_null($model->compid)) {
        $model->auditObj = e4sAudit::withID($model->compid);
    }

    $model->saledisc = checkFieldForXSS($obj, 'saleDisc:Sale Discount');
    $model->regdisc = checkFieldForXSS($obj, 'regDisc:Regular Discount');
    $model->type = checkFieldForXSS($obj, 'type:Discount Type');
    $model->count = checkFieldForXSS($obj, 'count:Count');

    $model->options = checkJSONObjForXSS($obj, 'options:Discount options');
    $model->agegroupids = array();
    $params = $obj->get_params('JSON');
    if (array_key_exists('ageGroups', $params)) {
        $model->agegroups = $params['ageGroups'];
        $model->agegroupid = $model->agegroups[0]['id'];
        foreach ($model->agegroups as $agegroup) {
            $model->agegroupids[] = $agegroup['id'];
        }
    } else {
        $model->agegroup = checkJSONObjForXSS($obj, 'ageGroup:Age Group Object');
        if (!is_null($model->agegroup)) {
            if (array_key_exists('id', $model->agegroup)) {
                $model->agegroupid = $model->agegroup['id'];
            }
        }
    }
    $model->pageInfo = new stdClass();
    $model->pageInfo->startswith = checkFieldForXSS($obj, 'startswith:Starts With');
    $model->pageInfo->page = checkFieldForXSS($obj, 'pagenumber:Page Number');
    $model->pageInfo->pagesize = checkFieldForXSS($obj, 'pagesize:Page Size');

    return $model;
}

function e4s_discRemoveUnwantedOptions($options) {
    $options = e4s_getOptionsAsObj($options);
    $options->clubs = e4s_discCleanUpClubs($options);
    $options->athletes = e4s_discCleanUpAthletes($options);
    $options->users = e4s_discCleanUpUsers($options);
    $options->events = e4s_discCleanUpEvents($options);
    $options->ignoreEvents = e4s_discCleanUpIgnoreEvents($options);
    return e4s_removeDefaultDiscountOptions($options);
}

function e4s_discAddUIOptions($options) {
    $options = e4s_getOptionsAsObj($options);
    $options->clubs = e4s_discAddInClubInfo($options);
    $options->users = e4s_discAddInUserInfo($options);
    $options->athletes = e4s_discAddInAthleteInfo($options);
    $options->events = e4s_discAddInEventInfo($options);
    $options->ignoreEvents = e4s_discAddInIgnoreEventInfo($options);
    return e4s_addDefaultDiscountOptions($options);
}

function e4s_discAddInClubInfo($options) {
    if (!isset($options->clubs) or empty($options->clubs)) {
        return array();
    }
    $clubIds = array();
    $clubs = $options->clubs;
    foreach ($clubs as $club) {
        $clubIds[] = $club->id;
    }
    $sql = 'select id, clubname clubName
            from ' . E4S_TABLE_CLUBS . '
            where id in (' . implode(',', $clubIds) . ')
    ';
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows < 1) {
        Entry4UIError(9021, 'Failed to read clubs for discount', 200, '');
    }
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    return $rows;
}

function e4s_discCleanUpClubs($options) {
    if (!isset($options->clubs)) {
        return array();
    }
    $cleanClubs = array();
    $clubs = $options->clubs;
    foreach ($clubs as $club) {
        $cleanClub = new stdClass();
        $cleanClub->id = $club->id;
        $cleanClubs[] = $cleanClub;
    }

    return $cleanClubs;
}

function e4s_discAddInAthleteInfo($options) {
    if (!isset($options->athletes) or empty($options->athletes)) {
        return array();
    }
    $ids = array();
    $athletes = $options->athletes;
    foreach ($athletes as $athlete) {
        $ids[] = $athlete->id;
    }
    $sql = 'select  a.id id,
                    firstname firstName,
                    surname surName,
                    URN,
                    clubid,
                    Clubname club
            from ' . E4S_TABLE_ATHLETE . ' a,
                 ' . E4S_TABLE_CLUBS . ' c
            where a.id in (' . implode(',', $ids) . ') and
                  a.clubid = c.id
    ';
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows < 1) {
        Entry4UIError(9022, 'Failed to read athletes for discount', 200, '');
    }
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    return $rows;
}

function e4s_discAddInUserInfo($options) {
    if (!isset($options->users) or empty($options->users)) {
        return array();
    }
    $userIds = array();
    $users = $options->users;
    foreach ($users as $user) {
        $userIds[] = $user->id;
    }
    $sql = 'select id, user_nicename name
            from ' . E4S_TABLE_USERS . '
            where id in (' . implode(',', $userIds) . ')
    ';
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows < 1) {
        Entry4UIError(9023, 'Failed to read users for discount', 200, '');
    }
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    return $rows;
}

function e4s_discCleanUpUsers($options) {
    if (!isset($options->users)) {
        return array();
    }
    $cleanUsers = array();
    $users = $options->users;
    foreach ($users as $user) {
        $cleanUser = new stdClass();
        $cleanUser->id = $user->id;
        $cleanUsers[] = $cleanUser;
    }

    return $cleanUsers;
}

function e4s_discCleanUpAthletes($options) {
    if (!isset($options->athletes)) {
        return array();
    }
    $cleanAthletes = array();
    $athletes = $options->athletes;
    foreach ($athletes as $athlete) {
        $cleanAthlete = new stdClass();
        $cleanAthlete->id = $athlete->id;
        $cleanAthletes[] = $cleanAthlete;
    }

    return $cleanAthletes;
}

function e4s_discAddInEventInfo($options) {
    if (!isset($options->events) or empty($options->events)) {
        return array();
    }
    $ids = array();
    $array = $options->events;
    foreach ($array as $arr) {
        $ids[] = $arr->id;
    }
    $sql = 'select id id, tf tf, Gender gender, Name name
            from ' . E4S_TABLE_EVENTS . '
            where id in (' . implode(',', $ids) . ')
    ';
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows < 1) {
        Entry4UIError(9024, 'Failed to read events for discount', 200, '');
    }
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    return $rows;
}

function e4s_discCleanUpEvents($options) {
    if (!isset($options->events)) {
        return array();
    }
    $array = $options->events;

    $cleanArray = array();
    foreach ($array as $arr) {
        $cleanArr = new stdClass();
        $cleanArr->id = $arr->id;
        $cleanArray[] = $cleanArr;
    }

    return $cleanArray;
}

function e4s_discAddInIgnoreEventInfo($options) {
    if (!isset($options->ignoreEvents) or empty($options->ignoreEvents)) {
        return array();
    }
    $ids = array();
    $array = $options->ignoreEvents;
    foreach ($array as $arr) {
        $ids[] = $arr->id;
    }
    $sql = 'select id id, tf tf, Gender gender, Name name
            from ' . E4S_TABLE_EVENTS . '
            where id in (' . implode(',', $ids) . ')
    ';
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows < 1) {
        Entry4UIError(9025, 'Failed to read ignoreEvents for discount', 200, '');
    }
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    return $rows;
}

function e4s_discCleanUpIgnoreEvents($options) {
    if (!isset($options->ignoreEvents)) {
        return array();
    }
    $array = $options->ignoreEvents;

    $cleanArray = array();
    foreach ($array as $arr) {
        $cleanArr = new stdClass();
        $cleanArr->id = $arr->id;
        $cleanArray[] = $cleanArr;
    }

    return $cleanArray;
}