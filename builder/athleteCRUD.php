<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';
function e4s_checkForRegisteredAthlete($obj) {
//    Entry4UIError(204,"URN Not found. Please confirm in source Registration system",200);
//    Entry4UIError(206,"Source registration hold different details. Please confirm in source Registration system",200);
    $aoCode = $_GET['aocode'];
    $regid = $_GET['regid'];
    $dob = $_GET['dob'];
    if ($aoCode === '') {
        Entry4UIError(1000, 'Please pass a registration code.', 200, '');
    }
    if ($regid === '' or $dob === '') {
        Entry4UIError(1001, 'Please pass a registration number and date of birth.', 200, '');
    }

    $now = date(E4S_SHORT_DATE);
    $dobSplit = explode('-', $dob);
    if (!checkdate($dobSplit[1], $dobSplit[2], $dobSplit[0])) {
        Entry4UIError(1000, 'Invalid Date of Birth given.', 200, '');
    }
    $currentAge = (int)getAgeFromText($dob, $now, E4S_SHORT_DATE);
    if ($currentAge < 6 || $currentAge > 110) {
        Entry4UIError(1000, 'Invalid Date of Birth. Age :' . $currentAge, 200, '');
    }
    $model = new stdClass();
    $model->urn = $regid;
    $model->aoCode = $aoCode;
    $model->dob = $dob;

    $aObj = new athleteClass();
    Entry4UISuccess('
        "data":' . json_encode($aObj->checkModelAgainstExtSystem($model), JSON_NUMERIC_CHECK));

}

function e4s_actionAthlete($obj, $process) {
    $model = e4s_getAthleteModel($obj, $process);
    switch ($model->process) {
        case E4S_CRUD_LIST:
            e4s_listAthletes($model, TRUE);
            break;
        case E4S_CRUD_CREATE:
            e4s_createAthlete($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readAthlete($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_updateAthlete($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteAthlete($model, TRUE);
            break;
    }
}

function e4s_getAthleteRow($key, $value, $mustExist) {
    $row = null;
    $sql = 'select a.*, c.clubname club
            from ' . E4S_TABLE_ATHLETE . ' a left join ' . E4S_TABLE_CLUBS . ' c on c.id = a.clubid
            where a.' . $key . ' = ' . $value;

    $result = e4s_queryNoLog($sql);
    if ($mustExist) {
        if ($result->num_rows === null) {
            Entry4UIError(2003, 'Invalid ' . $key, 400, '');
        }
        $row = $result->fetch_assoc();
        $row = athleteClass::normaliseArray($row);
    }

    if (!$mustExist) {
        if ($result->num_rows !== 0) {
            Entry4UIError(2004, 'Duplicate ' . $key, 400, '');
        }
    }
    $row['club2'] = '';
// if 2nd claim club
    if ((int)$row['club2Id'] !== 0) {
        $claimSQL = 'select *
                     from ' . E4S_TABLE_CLUBS . '
                     where id = ' . $row['club2Id'];
        $claimresult = e4s_queryNoLog($claimSQL);
        if ($claimresult->num_rows === 1) {
            $claimClub = $claimresult->fetch_assoc();
            $row['club2'] = $claimClub['Clubname'];
        }
    }

    $row['school'] = '';
// if 2nd claim club
    if ((int)$row['schoolId'] !== 0) {
        $claimSQL = 'select *
                     from ' . E4S_TABLE_CLUBS . '
                     where id = ' . $row['schoolId'];
        $claimresult = e4s_queryNoLog($claimSQL);
        if ($claimresult->num_rows === 1) {
            $claimClub = $claimresult->fetch_assoc();
            $row['school'] = $claimClub['Clubname'];
        }
    }

    $options = e4s_getOptionsAsObj($row['options']);
    if (!isset($options->noEntryReason)) {
        $options->noEntryReason = '';
    }
    $row['options'] = $options;
    return $row;
}
function e4s_readAthlete($model, $exit) {
    $athleteObj = athleteClass::withID($model->id);
    if (is_null($athleteObj->athleteRow)) {
        Entry4UIError(2005, 'Invalid ' . $model->id, 200, '');
    }
    $row = $athleteObj->getExtendedRow();
    if (array_key_exists('club2Id', $row)) {
        if ((int)$row['club2Id'] !== 0) {
            $row['club2id'] = (int)$row['club2Id'];
        }
    }

    $agObj = new e4sAgeClass(E4S_AGE_DEFAULT_BASE);
    $sendAGs = $agObj->getAgeGroups($model->compId);

    $meta = new stdClass();
    $meta->ageGroups = $sendAGs;

    if ($exit) {
        $meta = new stdClass();
        $meta->ageGroups = $sendAGs;

        Entry4UISuccess('
            "data": ' . json_encode($row, JSON_NUMERIC_CHECK) . ',
            "meta": ' . json_encode($meta, JSON_NUMERIC_CHECK)
        );
    }
    return $row;
}

function e4s_createAthlete($model, $exit) {
    if ((int)$model->id !== 0) {
        return null;
    }
    $athleteObj = new athleteClass();
    $athleteObj->create($model);
    $row = $athleteObj->getExtendedRow();
    Entry4UISuccess($row);
}

function e4s_updateAthlete($model, $exit) {
    $id = $model->id;
    $athleteObj = athleteClass::withID($id);
    if (is_null($athleteObj->getRow())) {
        Entry4UIError(4999, 'Athlete not found');
    }
    $activeEndDate = $athleteObj->athleteRow['activeEndDate'];

    if ($model->aoCode === E4S_AOCODE_EA) {
        $thisYear = (int)Date('Y');
        $activeEndDate = $thisYear . '-12-31';
    }

    $externalAthlete = $athleteObj->checkModelAgainstExtSystem($model, $activeEndDate);
    $externalAthlete = e4s_getDataAsType($externalAthlete, E4S_OPTIONS_OBJECT);

    $switchRecord = false;
    $urn = $model->urn;

    if ( e4s_isAAIDomain() ) {
        if ($model->aoCode === E4S_AOCODE_AAI or $model->aoCode === E4S_AOCODE_ANI) {
            if ($model->urn === $externalAthlete->URN) {
                if ($model->dob === $externalAthlete->dob) {
                    $switchRecord = TRUE;
                }
            }
        }
    }

    if ( $switchRecord ){
        $origRow = e4s_getAthleteRow('id', $model->id, TRUE);
        $id = $externalAthlete->id;
        $options = addslashes(e4s_getOptionsAsString($model->options));
        $athleteObj->moveTo($id,$options);
    }else {
        $firstName = addslashes($externalAthlete->firstName);
        $surName = addslashes($externalAthlete->surName);
        $dob = $externalAthlete->dob;
        $classification = $model->classification;
        $clubId = $externalAthlete->clubId;
        $club2Id = $externalAthlete->club2Id;
        $schoolId = $model->schoolId;
        $aoCode = $model->aoCode;
        $urn = $model->urn;
        $email = $model->email;
        $gender = $externalAthlete->gender;
        if ($model->aoCode === E4S_AOCODE_EA) {
            if (!isset($externalAthlete->status) or $externalAthlete->status !== E4S_EA_REGISTERED) {
                $activeEndDate = ($thisYear - 1) . '-12-31';
            }
            if ( !is_null($urn)){
                include_once E4S_FULL_PATH . 'classes/deDupClass.php';
                $deDupObj = new deDupClass(1, false,$firstName,$surName, $urn, $id);
            }
        }
        $origRow = e4s_getAthleteRow('id', $id, TRUE);

        if (isAppAdmin()) {
            $activeEndDate = $model->activeEndDate;
        }

        $sql = 'update ' . E4S_TABLE_ATHLETE . "
            set firstName ='" . $firstName . "',
                surName ='" . $surName . "',
                dob ='" . $dob . "',
                classification ='" . $classification . "',
                email ='" . $email . "',
                aocode ='" . $aoCode . "',";

        if ($urn === '' || is_null($urn)) {
            $sql .= 'urn = null,';
        } else {
            $sql .= "URN ='" . $urn . "',";
        }

        $sql .= " gender ='" . $gender . "',
                schoolid ='" . $schoolId . "',
                clubid ='" . $clubId . "',
                club2id ='" . $club2Id . "',
                activeEndDate ='" . $activeEndDate . "',
                options ='" . addslashes(e4s_getOptionsAsString($model->options)) . "'
            where id = " . $id;

        e4s_queryNoLog($sql);
    }
    $row = e4s_getAthleteRow('id', $id, TRUE);
    checkOpenEntries($origRow, $row);
    $athleteObj = athleteClass::withID($row['id']);
    $athleteObj->addUserAthletes();
    $row = $athleteObj->getExtendedRow();
    if ($exit) {
        Entry4UISuccess($row);
    }
    return $row;
}

function checkOpenEntries($origRow, $row) {
    $update = FALSE;
    $fromClub = $origRow['clubId'];
    $toClub = $row['clubId'];

    if ($origRow['clubId'] !== $row['clubId']) {
        $update = TRUE;
        $fromClub = $origRow['clubId'];
        $toClub = $row['clubId'];
    }

    if ($origRow['club2Id'] !== $row['club2Id']) {
        $update = TRUE;
        $fromClub = $origRow['club2Id'];
        $toClub = $row['club2Id'];
    }
    if ($origRow['firstName'] !== $row['firstName'] or $origRow['surName'] !== $row['surName']) {
        $update = TRUE;
    }
    if ($update) {
        updateOpenEntriesForAthlete($row, $fromClub, $toClub);
    }
}

function e4s_deleteAthlete($model, $exit) {
    $athleteObj = athleteClass::withID($model->id);
    if (is_null($athleteObj->athleteRow)) {
        Entry4UIError(2100, 'Athlete ' . $model->id . ' does not exist', 200, '');
    }
    $row = $athleteObj->athleteRow;
    $athleteObj->delete();
    if ($exit) {
        Entry4UISuccess('
            "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_listAthletes($model, $exit) {

    $startswith = $model->pageInfo->startswith;
    $pagesize = $model->pageInfo->pagesize;
    $page = $model->pageInfo->page;

    $usePaging = FALSE;
    if (isset($pagesize) and isset($page)) {
        $usePaging = TRUE;
    }

    $sql = 'select id, name
        from ' . E4S_TABLE_ATHLETE;

    if (isset($startswith)) {
        $sql .= " where firstname like '" . $startswith . "%' ";
        $sql .= " or surname like '" . $startswith . "%' ";
    }

    $sql .= ' order by surname ';

    if ($usePaging and $pagesize !== 0) {
        $sql .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
    }

    $result = e4s_queryNoLog($sql);
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($rows, JSON_NUMERIC_CHECK));
    }
    return $rows;
}

function updateOpenEntriesForAthlete($athleteRow, $fromID, $toID) {
    $athleteId = $athleteRow['id'];
    $ceidSQL = 'select ce.id ceid
                from ' . E4S_TABLE_COMPETITON . ' c,
                     ' . E4S_TABLE_COMPEVENTS . ' ce, 
                     ' . E4S_TABLE_ENTRIES . ' e 
                where c.date >= now()
                and c.id = ce.compid 
                and e.compeventid = ce.id
                and e.clubid = ' . $fromID . '
                and e.athleteid = ' . $athleteId;
    $ceidResults = e4s_queryNoLog($ceidSQL);
    if ($ceidResults->num_rows < 1) {
        return;
    }

    $rows = $ceidResults->fetch_all(MYSQLI_ASSOC);
    $ceids = array();
    foreach ($rows as $row) {
        $ceids[] = $row['ceid'];
    }
    $sql = 'update ' . E4S_TABLE_ENTRIES . '
            set clubid = ' . $toID . ",
                athlete = '" . addslashes($athleteRow['firstName'] . ' ' . $athleteRow['surName']) . "'
            where athleteid = " . $athleteId . '
            and compeventid in (' . implode(',', $ceids) . ')';

    e4s_queryNoLog($sql);
}
function e4s_getAAIAthlete($obj){
    $athlete = new athleteClass(false);
    $athlete->returnAthleteToEA($obj);
}
function e4s_getAthleteModel($obj, $process) {
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;

    $model->id = checkFieldForXSS($obj, 'id:Athlete ID');

    $model->firstName = checkFieldForXSS($obj, 'firstName:Athlete Firstname');
	if ( !is_null($model->firstName) ) {
		$model->firstName = ucfirst( strtolower( trim( $model->firstName, ' ' ) ) );
	}
    $model->surName = checkFieldForXSS($obj, 'surName:Athlete Surname');
	if ( !is_null($model->surName) ) {
		$model->surName = ucfirst( strtolower( trim( $model->surName, ' ' ) ) );
	}
    $model->urn = checkFieldForXSS($obj, 'URN:Athlete URN');
    $model->compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $model->aoCode = checkFieldForXSS($obj, 'aocode:Athlete AO Code');
    if ($model->urn === '' or $model->aoCode === '') {
		if ( $model->aoCode !== E4S_AOCODE_INTERNATIONAL) {
			$model->aoCode = '';
		}
        $model->urn = null;
    }
    $model->dob = checkFieldForXSS($obj, 'dob:Athlete DOB');
    $model->gender = checkFieldForXSS($obj, 'gender:Athlete Gender');
    $model->email = checkFieldForXSS($obj, 'email:Email');
    $model->clubId = checkFieldForXSS($obj, 'clubId:Athlete ClubID');
    $model->club2Id = checkFieldForXSS($obj, 'club2id:Athlete Second Claim ClubID');
    $model->schoolId = checkFieldForXSS($obj, 'schoolid:Athlete SchoolID');
    $model->activeEndDate = checkFieldForXSS($obj, 'activeEndDate:Athlete Reg End Date');
    if (is_null($model->activeEndDate)) {
        $model->activeEndDate = checkFieldForXSS($obj, 'activeenddate:Athlete Reg End Date');
    }
    if ($model->activeEndDate === '0000-00-00' || $model->activeEndDate === '' || is_null($model->activeEndDate)) {
        $model->activeEndDate = date('Y') . '-12-31';
    }
    $model->classification = checkFieldForXSS($obj, 'classification:Athlete Classification');
    $model->options = checkJSONObjForXSS($obj, 'options:Athlete Options');

    e4s_addStdPageInfo($obj, $model);
    return $model;
}
function e4s_getPayeesForResults($obj){
	$params = $obj->get_params('JSON');
	$urns = null;
	$egId = null;
	if ( array_key_exists('urns', $params )) {
		$urns = $params['urns'];
	}
	if ( array_key_exists('sourceEventGroupSummaryId', $params )) {
		$egId = $params['sourceEventGroupSummaryId'];
	}
	if ( is_null($urns) or is_null($egId) ){
		Entry4UIError(5670, 'Invalid options passed to getPayees');
	}
	$sql = "select e.entryId, e.athleteId, e.urn, e.userId, u.user_nicename userName, u.display_name userDisplayname, u.user_email userEmail
	        from " . E4S_TABLE_ENTRYINFO . " e,
	             " . E4S_TABLE_USERS . " u
	        where egid = " . $egId . "
	        and urn in ('" . implode("','", $urns) . "')
	        and u.id = e.userid";
	$result = e4s_queryNoLog($sql);
	$rows = [];
	while ( $dbRow = $result->fetch_object() ){
		$row = new stdClass();
		$row->entryId = (int)$dbRow->entryId;
		$row->athleteId = (int)$dbRow->athleteId;
		$row->urn = (int)$dbRow->urn;
		$row->user = new stdClass();
		$row->user->id = (int)$dbRow->userId;
		$row->user->name = $dbRow->userName;
		$row->user->displayName = $dbRow->userDisplayname;
		$row->user->email = $dbRow->userEmail;

		$rows[] = $row;
	}
	Entry4UISuccess('
		"data":' . json_encode($rows)
	)  ;
}
function e4s_getOwnersForResults($obj, $includeTeams = false){
    e4s_getOwnersGeneric($obj, true);
}
function e4s_getAthleteOwners($obj){
    e4s_getOwnersGeneric($obj, false);
}
function e4s_getOwnersGeneric($obj, $includeEventInfo){
    $params = $obj->get_params('JSON');
    $ownerObj = null;
    if ( array_key_exists('urns', $params )) {
        $urns = $params['urns'];
        $ownerObj = e4sOwners::withURNs($urns);
    }
    if ( array_key_exists('athleteids', $params )) {
        $athleteIds = $params['athleteids'];
        $ownerObj = e4sOwners::withAthleteIds($athleteIds);
    }
    if ( is_null($ownerObj) ){
        Entry4UIError(5670, 'Invalid options passed to getOwners');
    }
    $ownerObj->getOwners();
    $retObj = $ownerObj->getReturnObject($includeEventInfo);

    Entry4UISuccess($retObj);
}
