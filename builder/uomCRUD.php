<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

function e4s_actionUOM($obj, $process) {
    $model = e4s_getUOMModel($obj, $process);
    switch ($model->process) {
        case E4S_CRUD_LIST:
            e4s_listUOMs($model, TRUE);
            break;
        case E4S_CRUD_CREATE:
            e4s_createUOM($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readUOM($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_updateUOM($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteUOM($model, TRUE);
            break;
    }
}

function e4s_returnStdUOMRow($row) {
    if (is_null($row) === FALSE) {
        //        $row['minAtYear'] = $row['minYear'];
        //        unset ($row['minYear']);
        $row['type'] = $row['uomtype'];
        unset($row['uomtype']);
        if ($row['uomoptions'] !== '') {
            $row['uomoptions'] = json_decode($row['uomoptions']);
        }
        $row['options'] = $row['uomoptions'];
        unset($row['uomoptions']);
    }
    return $row;
}

function e4s_getUOMRowCRUD($key, $value, $mustExist) {
    $row = e4s_getUOMRow($key, $value, $mustExist);
    return e4s_returnStdUOMRow($row);
}

function e4s_getUOMRow($keys, $values, $mustExist) {
    return e4s_getCRUDRow(E4S_TABLE_UOM, $keys, $values, $mustExist);
}

function e4s_readUOM($model, $exit) {
    $row = e4s_getUOMRowCRUD('id', $model->id, TRUE);

    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_createUOM($model, $exit) {
    if ((int)$model->id !== 0) {
        Entry4UIError(2019, 'ID of non zero passed to create', 400, '');
    }
//    e4s_getUOMRowCRUD('club', '"' . $model->name . '"', false);
    $sql = 'Insert into ' . E4S_TABLE_UOM . " ( club )
            values('" . $model->name . "')";
    e4s_queryNoLog($sql);

    $row = e4s_getUOMRowCRUD('id', e4s_getLastID(), TRUE);

    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_updateUOM($model, $exit) {

    $row = e4s_getUOMRowCRUD('id', $model->id, TRUE);
    $sql = 'update ' . E4S_TABLE_UOM . "
            set uomtype ='" . $model->uomtype . "',
                uomoptions ='" . json_encode($model->options) . "'
            where id = " . $model->id;
    e4s_queryNoLog($sql);
    $row['uomtype'] = $model->uomtype;
    $row['uomoptions'] = $model->uomoptions;

    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_deleteUOM($model, $exit) {
    // check Row exists
    $row = e4s_getUOMRowCRUD('id', $model->id, TRUE);
    $sql = 'select *
            from ' . E4S_TABLE_EVENTS . '
            where  uomid = ' . $model->id;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows > 0) {
        Entry4UIError(2014, 'Unable to delete as in use', 400, '');
    }
    $sql = 'delete from ' . E4S_TABLE_UOM . '
            where id = ' . $model->id;

    e4s_queryNoLog($sql);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_listUOMs($model, $exit) {

    $usePaging = FALSE;
    if (isset($model->pageInfo->pagesize) and isset($model->pageInfo->page)) {
        $usePaging = TRUE;
    }

    $sql = 'select *
        from ' . E4S_TABLE_UOM;

    if ($model->pageInfo->startswith !== '') {
        $sql .= " where uomtype like '" . $model->pageInfo->startswith . "%' ";
    }

    $sql .= ' order by id ';

    if ($usePaging and $model->pageInfo->pagesize !== 0) {
        $sql .= ' limit ' . (($model->pageInfo->page - 1) * $model->pageInfo->pagesize) . ', ' . $model->pageInfo->pagesize;
    }

    $result = e4s_queryNoLog($sql);
    $rows = $result->fetch_all(MYSQLI_ASSOC);
    $newRows = array();
    foreach ($rows as $row) {
        $newRows[] = e4s_returnStdUOMRow($row);
    }
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($newRows, JSON_NUMERIC_CHECK));
    }
    return $rows;
}

function e4s_getUOMModel($obj, $process) {
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;
    $model->id = checkFieldForXSS($obj, 'id:UOM ID');
    $model->uomtype = checkFieldForXSS($obj, 'id:UOM Type');
    $model->uomoptions = checkFieldForXSS($obj, 'uomoptions:UOM options');

    e4s_addStdPageInfo($obj, $model);

    return $model;
}