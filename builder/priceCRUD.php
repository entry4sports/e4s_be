<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

define('E4S_PRICE_IN', 'IN');
define('E4S_PRICE_OUT', 'OUT');

function e4s_actionPrice($obj, $process) {
    $model = e4s_getPriceModel($obj, $process);

    switch ($model->process) {
        case E4S_CRUD_LIST:
            e4s_listPrices($model, TRUE);
            break;
        case E4S_CRUD_CREATE:
            e4s_createPrice($model, TRUE);
            break;
        case E4S_CRUD_READ:
            e4s_readPrice($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            e4s_updatePrice($model, TRUE);
            break;
        case E4S_CRUD_DELETE:
            e4s_deletePrice($model, TRUE);
            break;
    }
}

function e4s_returnIfR4S() {
    if (is_R4SCompetition() === 1) {
        return TRUE;
    }
    return FALSE;
}

function e4s_returnStdPriceRow($row) {
    if (is_null($row) === FALSE) {
        renameBuilderField($row, 'ID', 'id');
        renameBuilderField($row, 'compid', 'compId');
        renameBuilderField($row, 'saleprice', 'salePrice');
        renameBuilderField($row, 'salefee', 'saleFee');
        renameBuilderField($row, 'saleenddate', 'saleEndDate');

        if ($row['saleEndDate'] === null) {
            $row['saleEndDate'] = '';
        }

        $options = e4s_addDefaultPriceOptions($row['options']);
        $row['options'] = $options;

        $priceObj = e4s_calculateFees(E4S_PRICE_OUT, $row['compId'], $options, (float)$row['salePrice'], (float)$row['saleFee']);
        $row['salePrice'] = $priceObj->price;
        $row['saleFee'] = $priceObj->fee;

        $priceObj = e4s_calculateFees(E4S_PRICE_OUT, $row['compId'], $options, (float)$row['price'], (float)$row['fee']);
        $row['price'] = $priceObj->price;
        $row['fee'] = $priceObj->fee;
//
//        $row['salePrice'] = (float)$row['salePrice'] - (float)$row['saleFee'];
//        $row['price'] = (float)$row['price'] - (float)$row['fee'];
    }
    return $row;
}

function e4s_getPriceRowCRUD($key, $value, $mustExist) {
    $row = e4s_getPriceRow($key, $value, $mustExist);
    return e4s_returnStdPriceRow($row);
}

function e4s_getPriceRow($keys, $values, $mustExist) {
    return e4s_getCRUDRow(E4S_TABLE_EVENTPRICE, $keys, $values, $mustExist);
}

function e4s_readPrice($model, $exit) {
    $row = e4s_getPriceRowCRUD('id', $model->id, TRUE);

    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_checkFreeEntry($model) {
    $sql = 'select id id
            from ' . E4S_TABLE_EVENTPRICE . '
            where compid = ' . $model->compid . "
            and options like '%freeEntry\":true'";
    $result = e4s_queryNoLog($sql);

    if ($result->num_rows === 0) {
        return;
    }
    if ($model->id !== 0 and $result->num_rows === 1) {
        $row = $result->fetch_assoc();
        if ((int)$row['id'] === $model->id) {
            return;
        }
    }
    Entry4UIError(9102, 'A Free entry already exists.', 200, '');
}

function e4s_priceExist($id) {
    $sql = 'select *
            from ' . E4S_TABLE_EVENTPRICE . '
            where id = ' . $id;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows > 0) {
        return TRUE;
    }
    return FALSE;
}

function e4s_createFreeEntryForR4S($compid) {
    $model = new stdClass();
    $model->id = 0;
    $model->compid = $compid;
    $model->auditObj = null;
    $model->name = 'R4S_No_Price';
    $model->description = $model->name;
    $model->options = e4s_addDefaultPriceOptions('{}');
    $model->options->freeEntry = TRUE;
    $model->price = 0;
    $model->saleprice = 0;
    $model->salefee = 0;
    $model->fee = 0;
    $model->saleenddate = null;

    e4s_createPrice($model, TRUE);
}

function e4s_createPrice($model, $exit) {
    if ($model->id !== 0) {
        Entry4UIError(2000, 'ID of non zero passed to create', 400, '');
    }

    if ($model->options->freeEntry) {
        e4s_checkFreeEntry($model);
    } elseif (e4s_returnIfR4S()) {
        Entry4UIError(2001, 'You are not allowed to enter a price for a Results Only competition', 200, '');
    }

    $sql = 'Insert into ' . E4S_TABLE_EVENTPRICE . " (name,description,compid, price,fee,saleprice,salefee, saleenddate , options)
values(
'" . $model->name . "',
'" . $model->description . "',
" . $model->compid . ',
' . $model->price . ',
' . $model->fee . ',
' . $model->saleprice . ',
' . $model->salefee . ',';
    if (e4s_isDateEmpty($model->saleenddate)) {
        $sql .= 'null,';
    } else {
        $sql .= "'" . $model->saleenddate . "',";
    }

    $sql .= "'" . json_encode($model->options) . "'
)";

    e4s_queryNoLog($sql);
    $insertedId = e4s_getLastID();
    e4s_updateSaleDate($model);

    $row = e4s_getPriceRowCRUD('id', $insertedId, TRUE);
    $model->auditObj->writeAudit('Create Price Record : ' . $insertedId, TRUE, E4S_TABLE_EVENTPRICE, $insertedId, $row);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_updateSaleDate($model) {
    $sql = 'update ' . E4S_TABLE_EVENTPRICE . '
            set saleenddate = ';
    if (e4s_isDateEmpty($model->saleenddate)) {
        $sql .= 'null, price = saleprice, fee = salefee';
    } else {
        $sql .= "'" . $model->saleenddate . "'";
    }
    $sql .= ' where compid = ' . $model->compid ;
    e4s_queryNoLog($sql);
    $compObj = e4s_GetCompObj($model->compid);
    $compObj->getSaleEndDate(TRUE, TRUE);
}

function e4s_updatePrice($model, $exit) {
    if (e4s_returnIfR4S()) {
        Entry4UIError(2002, 'You are not allowed to update a price for a Results Only competition', 200, '');
    }

    if ($model->options->freeEntry) {
        e4s_checkFreeEntry($model);
    }
    $row = e4s_getPriceRowCRUD('id', $model->id, TRUE);
    // Check there are no Entries for this price if prices have changed
    $entryCheck = false;

    $currentDate = strtotime(date('Y-m-d'));
    if ($row['saleEndDate']=== ''){
        $oldDate = $currentDate;
        // added a saleEndDate, ignore existing
        $row['salePrice'] = $row['price'];
        $row['saleFee'] = $row['fee'];
    }else{
        $oldDate = strtotime(date('Y-m-d',strtotime($row['saleEndDate'])));
    }
    if (is_null($model->saleenddate)){
        $newDate = $currentDate;
    }else{
        $newDate = strtotime(date('Y-m-d',strtotime($model->saleenddate)));
    }
    $where = 'and   ce.priceid = ' . $model->id;
    if ( $oldDate <= $currentDate or $newDate <= $currentDate ){
//        Either old or new date are in the past, check entries
        $entryCheck = true;
        $where = ' and ce.compId = ' . $model->compid;
    }
    if (!e4s_compareDoubles((float)$row['salePrice'], '=', (float)$model->saleprice) ) {
        $entryCheck = true;
    }
    if (!e4s_compareDoubles((float)$row['saleFee'], '=', (float)$model->salefee) ) {
        $entryCheck = true;
    }

    $message = '';
    if ( $entryCheck ) {
        $entrySql = '
        select count(e.id) count
        from ' . E4S_TABLE_ENTRIES . ' e,
             ' . E4S_TABLE_COMPEVENTS . ' ce
        where e.compeventid = ce.id ' . $where;

        $entryResult = e4s_queryNoLog($entrySql);
        $entryCount = $entryResult->fetch_assoc();
        if ((int)$entryCount['count'] > 0) {
            $message = 'An audit log has been generated as existing entries exist with original prices.';
        }
    }

    $model->auditObj->writeAudit('Update Price Record : ' . $model->id, TRUE, E4S_TABLE_EVENTPRICE, $model->id, $row);
    $sql = 'update ' . E4S_TABLE_EVENTPRICE . "
            set name ='" . $model->name . "',
            description ='" . $model->description . "',";

    if (e4s_isDateEmpty($model->saleenddate)) {
        $sql .= 'saleenddate = null,';
    } else {
        $sql .= "saleenddate ='" . $model->saleenddate . "',";
    }

    $sql .= "
            options ='" . json_encode($model->options) . "',
            price = " . $model->price . ',
            fee = ' . $model->fee . ',
            saleprice = ' . $model->saleprice . ',
            salefee = ' . $model->salefee . '
            where id = ' . $model->id;
    e4s_queryNoLog($sql);
    e4s_updateSaleDate($model);
    $row = e4s_getPriceRowCRUD('id', $model->id, TRUE);
    if ($exit) {
            Entry4UISuccess('"data":' . json_encode($row, JSON_NUMERIC_CHECK) , $message);
    }
    return $row;
}

function e4s_deletePrice($model, $exit) {

    if (e4s_returnIfR4S()) {
        Entry4UIError(2003, 'You are not allowed to delete a price for a Results Only competition', 200, '');
    }
    // check Row exists
    $row = e4s_getPriceRowCRUD('id', $model->id, TRUE);
    $sql = 'select id
            from ' . E4S_TABLE_COMPEVENTS . '
            where  Priceid = ' . $model->id;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows > 0) {
        Entry4UIError(2012, 'Unable to delete as in use', 200, '');
    }

    if (is_null($model->auditObj)) {
//        e4s_dump($model,"Model", true, true);
        $model->auditObj = e4sAudit::withID($row['compId']);
    }

    $model->auditObj->writeAudit('Delete Price Record : ' . $model->id, TRUE, E4S_TABLE_EVENTPRICE, $model->id, $row);
    $sql = 'delete from ' . E4S_TABLE_EVENTPRICE . '
            where id = ' . $model->id;

    e4s_queryNoLog($sql);
    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($row, JSON_NUMERIC_CHECK));
    }
    return $row;
}

function e4s_listPrices($model, $exit) {
    try {
        $usePaging = FALSE;

        if (isset($model->pageInfo) === FALSE) {
            $model->pageInfo = new stdClass();
            $model->pageInfo->startswith = '';
            $model->pageInfo->page = 0;
            $model->pageInfo->pagesize = 0;
        }
        if (isset($model->pageInfo->pagesize) and isset($model->pageInfo->page)) {
            $usePaging = TRUE;
        }

        $sql = 'select *
        from ' . E4S_TABLE_EVENTPRICE . '
        where compid = ' . $model->compid . ' ';

        if (!is_null($model->pageInfo->startswith) and $model->pageInfo->startswith !== '') {
            $sql .= " and name like '" . $model->pageInfo->startswith . "%' ";
        }else{
            $sql .= " and name != '" . E4S_QUALIFY_PRICE . "' ";
        }

        $sql .= ' order by id ';

        if ($usePaging and $model->pageInfo->pagesize !== 0) {
            $sql .= ' limit ' . (($model->pageInfo->page - 1) * $model->pageInfo->pagesize) . ', ' . $model->pageInfo->pagesize;
        }

        $result = e4s_queryNoLog($sql);
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $newRows = array();
        foreach ($rows as $row) {
            $newRows[] = e4s_returnStdPriceRow($row);
        }
        if ($exit) {
            Entry4UISuccess('
                "data":' . json_encode($newRows, JSON_NUMERIC_CHECK));
        }
        return $newRows;
    } catch (mysqli_sql_exception $ex) {
        throw new Exception("Can't connect to the database! \n" . $ex);
    }
    return array();
}
function e4s_validatePriceInModel($model){
	$saleEndDate = $model->saleenddate;
	if ( is_null($saleEndDate) || $saleEndDate === '' || $saleEndDate === '0000-00-00' ){
		return;
	}
	$compObj = e4s_GetCompObj($model->compid);
	$entriesOpen = $compObj->getEntriesOpenDate();
	$entriesClose = $compObj->getEntriesCloseDate();
	$validMsg = priceClass::validatePriceDate($saleEndDate, $entriesOpen,$entriesClose);

	if ( $validMsg != '' ){
		Entry4UIError(5875,$validMsg,200);
	}
}
function e4s_getPriceModel($obj, $process) {
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;
    $model->id = checkFieldForXSS($obj, 'id:Price ID');
    if (!is_null($model->id)) {
        $model->id = (int)$model->id;
    }
    $model->compid = checkFieldForXSS($obj, 'compId:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    if (is_null($model->compid)) {
        $model->compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    }
    $model->auditObj = null;

    if (!is_null($model->compid)) {
        $model->auditObj = e4sAudit::withID($model->compid);
    }else {
	    if ( $model->process === E4S_CRUD_DELETE ) {
		    // deleting a price does not have a compid
		    return $model;
	    }
    }

    $model->pids = checkJSONObjForXSS($obj, 'pids:Price IDs');
    $model->name = checkFieldForXSS($obj, 'name:Price Name');
    $model->description = checkFieldForXSS($obj, 'description:Price description');
    if ( !is_null($model->description )){
        $model->description = addslashes($model->description );
    }
    $model->options = checkJSONObjForXSS($obj, 'options:Price options');
    $model->options = e4s_addDefaultPriceOptions($model->options);

    if ($model->options->freeEntry) {
        $model->price = 0;
        $model->saleprice = 0;
        $model->salefee = 0;
        $model->fee = 0;
        $model->saleenddate = null;
    } else {
        $model->price = checkFieldForXSS($obj, 'price:Standard Price');
        $model->fee = checkFieldForXSS($obj, 'fee:E4S Fee');
        $priceObj = e4s_calculateFees(E4S_PRICE_IN, $model->compid, $model->options, $model->price, $model->fee);
        $model->price = $priceObj->price;
        $model->fee = $priceObj->fee;
        $model->saleenddate = checkFieldForXSS($obj, 'saleEndDate:Sale End Date');
        if ($model->saleenddate === '' || $model->saleenddate === '0000-00-00' || is_null($model->saleenddate)) {
            // no saleenddate so ignore sale price and fee
            $model->saleprice = $model->price;
            $model->salefee = $model->fee;
            $model->saleenddate = null;
        } else {
            $model->saleprice = checkFieldForXSS($obj, 'salePrice:Sale Price');
            $model->salefee = checkFieldForXSS($obj, 'saleFee:E4S Fee');
            // prices are stored as what is charged to the athlete
            $priceObj = e4s_calculateFees(E4S_PRICE_IN, $model->compid, $model->options, $model->saleprice, $model->salefee);
            $model->saleprice = $priceObj->price;
            $model->salefee = $priceObj->fee;
        }
    }

    $model->pageInfo = new stdClass();
    $model->pageInfo->startswith = checkFieldForXSS($obj, 'startswith:Starts With');
    $model->pageInfo->page = checkFieldForXSS($obj, 'pagenumber:Page Number');
    $model->pageInfo->pagesize = checkFieldForXSS($obj, 'pagesize:Page Size');
	e4s_validatePriceInModel($model);
    return $model;
}

function e4s_calculateFees($direction, $compid, $options, $price, $fee) {
    $compObj = e4s_GetCompObj($compid, TRUE);
//    $priceObj = new priceClass($compid, $compObj->isNationalComp());
    $priceObj = $compObj->getPriceObj();
    return $priceObj->calculateFees($direction, $options, $price, $fee);
}

function e4s_isDateEmpty($date) {
    if ($date === '' || $date === '0000-00-00' || is_null($date)) {
        return TRUE;
    }
    return FALSE;
}