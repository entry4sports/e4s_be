<?php
include_once E4S_FULL_PATH . 'dbInfo.php';

function e4s_copyClubCompInfo($inboundObj){
    $fromCompId = checkFieldForXSS($inboundObj, 'fromId:Comp Club Category ID' . E4S_CHECKTYPE_NUMERIC);
    $toCompId = checkFieldForXSS($inboundObj, 'toId:Comp Club Category ID' . E4S_CHECKTYPE_NUMERIC);
    if ($fromCompId === $toCompId ){
        Entry4UIError(6800, 'Copy same competition ??');
    }

    $defObjs = e4s_cloneClubComp($fromCompId, $toCompId, true);
    Entry4UISuccess($defObjs);
}

function e4s_cloneClubComp($fromCompId, $toCompId, $exit = false){
    $defObjs = array();
    $fromCompObj = e4s_getCompObj($fromCompId,false,true);
    if (! $fromCompObj->isClubcomp()){
        if ( $exit) {
            Entry4UIError(6810, $fromCompId . ' is not a club comp');
        }
        return $defObjs;
    }
    $toCompObj = e4s_getCompObj($toCompId,false,true);
    if (! $toCompObj->isClubcomp()){
        if ($exit) {
            Entry4UIError(6820, $toCompId . ' is not a club comp');
        }
        return $defObjs;
    }
    e4s_clubCompClean($toCompId);
    $catObjs = e4s_copyClubCopyCats($fromCompId, false);
    if ( empty($catObjs) ) {
        Entry4UIError(6810, 'Competition has no Club Comp Categories');
    }
    $defObjs = e4s_copyClubCompDefs($fromCompId, $toCompId, $catObjs, false);
    return $defObjs;
}
function e4s_clubCompClean($compId){
    $sql = 'delete from ' . E4S_TABLE_CLUBCATEGORY . '
            where id in ( select distinct categoryid from ' . E4S_TABLE_CLUBCOMP . ' where compid = ' . $compId . ')';
    e4s_queryNoLog($sql);
    $sql = 'delete from ' . E4S_TABLE_CLUBCOMP . '
            where compid = ' . $compId;
    e4s_queryNoLog($sql);
}
function e4s_copyClubCompDefs($fromCompId, $toCompId, $cats, $test = true){
    $defs = e4s_listClubCompDefsForCompId($fromCompId);

    foreach($defs as $obj){
        $catId = $obj->category->id;
        foreach($cats as $catObj){
            if ( $catObj->oldId === $catId ){
                $catId = $catObj->id;
                break;
            }
        }
        $sql = 'insert into ' . E4S_TABLE_CLUBCOMP . ' (clubid, compid, categoryid, bibnos)
                values(
                    ' . $obj->club->id . ',
                    ' . $toCompId . ',
                    ' . $catId . ",
                    '" . $obj->bibNos . "'
                )";
        e4s_queryNoLog($sql);
    }
    return $defs;
}
function e4s_copyClubCopyCats($fromCompId, $test = true){
    $obj = new stdClass();
    $obj->compId = $fromCompId;
    $currentObjs = e4s_listClubCompCategories($obj);
    foreach ($currentObjs as $currentObj){
        $currentObj->oldId = $currentObj->id;
        $sql = 'insert into ' . E4S_TABLE_CLUBCATEGORY . " (categoryName,maxEntries,maxRelays)
                values(
                    '" . $currentObj->categoryName . "',
                    " . $currentObj->maxEntries . ',
                    ' . $currentObj->maxRelays . '
                )';
        if ( !$test ) {
            e4s_queryNoLog($sql);
        }
        $currentObj->id = e4s_getLastID();
    }
    return $currentObjs;
}
function e4s_amendClubCompCatDef($inboundObj){
    $obj = e4s_getClubCompCatDefObj($inboundObj);
    if ( $obj->id === 0 ){
        $sql = 'insert into ' . E4S_TABLE_CLUBCATEGORY . " (categoryName,maxEntries,maxRelays)
                values(
                    '" . $obj->name . "',
                    " . $obj->maxEntries . ',
                    ' . $obj->maxRelays . '
                )';
    }else{
        $sql = 'update ' . E4S_TABLE_CLUBCATEGORY . "
                set categoryname = '" . $obj->name . "',
                    maxEntries = " . $obj->maxEntries . ',
                    maxRelays = ' . $obj->maxRelays . '
                where id = ' . $obj->id;
    }
    e4s_queryNoLog($sql);
    $cats = e4s_listClubCompCategories($obj);
    Entry4UISuccess($cats);
}
function e4s_deleteClubCompCatDef($inboundObj){
    $obj = e4s_getClubCompCatDefObj($inboundObj);
    if ( $obj->id === 0 ){
        Entry4UIError(6940, 'Invalid parameters passed');
    }
    $sql = 'delete from ' . E4S_TABLE_CLUBCATEGORY . '
            where id = ' . $obj->id;
    e4s_queryNoLog($sql);
    $cats = e4s_listClubCompCategories($obj);
    Entry4UISuccess($cats);
}

function e4s_listClubCompCats($inboundObj){
    $obj = e4s_getClubCompCatDefObj($inboundObj);
    return e4s_listClubCompCategories($obj);
}
function e4s_listClubCompCategories($obj){
    $sql = 'select *
            from ' . E4S_TABLE_CLUBCATEGORY . '
            where id in ( select distinct categoryId 
                          from ' . E4S_TABLE_CLUBCOMP . '
                          where compid = ' . $obj->compId . ')
            order by maxEntries desc';
    $result = e4s_queryNoLog($sql);
    $retArr = array();
    while ( $obj = $result->fetch_object(E4S_CLUBCOMPCAT_OBJ)) {
        unset($obj->optionsSet);
        unset($obj->options);
        $retArr[] = $obj;
    }
    return $retArr;
}
function e4s_getClubCompCatDefObj($obj){
    $id = checkFieldForXSS($obj, 'id:Comp Club Category ID' . E4S_CHECKTYPE_NUMERIC);
    $name = checkFieldForXSS($obj, 'name:Comp Club Category Name');
    $maxEntries = checkFieldForXSS($obj, 'maxEntries:Max Entries for Category' . E4S_CHECKTYPE_NUMERIC);
    $maxRelays = checkFieldForXSS($obj, 'maxRelays:Max Relays for Category'. E4S_CHECKTYPE_NUMERIC);
    $compId = checkFieldForXSS($obj, 'compId:Competition id' . E4S_CHECKTYPE_NUMERIC);
    $obj = new stdClass();
    $obj->id = $id;
    $obj->compId = $compId;
    $obj->name = $name;
    $obj->maxEntries = $maxEntries;
    $obj->maxRelays = $maxRelays;
    return $obj;
}
function e4s_amendClubCompDef($inboundObj){
    $obj = e4s_getClubCompDefObj($inboundObj);
    if ( $obj->id === 0 ){
        $sql = 'insert into ' . E4S_TABLE_CLUBCOMP . ' (clubid, compid, categoryid, bibnos)
                values(
                    ' . $obj->clubId . ',
                    ' . $obj->compId . ',
                    ' . $obj->categoryId . ",
                    '" . $obj->bibNos . "'
                )";
    }else{
        $sql = 'update ' . E4S_TABLE_CLUBCOMP . '
                set clubid = ' . $obj->clubId . ',
                    compid = ' . $obj->compId . ',
                    categoryId = ' . $obj->categoryId . ",
                    bibNos = '" . $obj->bibNos . "'
                where id = " . $obj->id;
    }
    e4s_queryNoLog($sql);
    $defs = e4s_listClubCompDefsForCompId($obj->compId);
    Entry4UISuccess($defs);
}
function e4s_getCompIdForCatId($id){
    $sql = 'select distinct compId as compId
            from ' . E4S_TABLE_CLUBCOMP . '
            where categoryId = ' . $id;
    $result = e4s_queryNoLog($sql);
    if ( $result->num_rows === 0 ) {
        Entry4UIError(6905, 'Invalid Category passed');
    }
    $obj = $result->fetch_object();
    return (int)$obj->compId;
}
function e4s_deleteClubCompDef($inboundObj){
    $obj = e4s_getClubCompDefObj($inboundObj);
    if ( $obj->id === 0 ){
        Entry4UIError(6950, 'Invalid parameters passed');
    }
    $obj->compId = e4s_getCompIdForCatId($obj->id);
    $sql = 'delete from ' . E4S_TABLE_CLUBCOMP . '
            where id = ' . $obj->id;
    e4s_queryNoLog($sql);
    $defs = e4s_listClubCompDefsForCompId($obj->compId);
    Entry4UISuccess($defs);
}
function e4s_listClubCompDefs($inboundObj){
    $obj = e4s_getClubCompCatDefObj($inboundObj);
    return e4s_listClubCompDefsForCompId($obj->compId);
}
function e4s_listClubCompDefsForCompId($compId){
    $sql = 'select cc.*,
                   cl.clubname clubName,
                   c.name compName,
                   cat.categoryName
            from ' . E4S_TABLE_CLUBCOMP . ' cc,
                 ' . E4S_TABLE_CLUBS . ' cl,
                 ' . E4S_TABLE_COMPETITON . ' c,
                 ' . E4S_TABLE_CLUBCATEGORY . ' cat
            where c.id = cc.compid
            and   cc.clubid = cl.id
            and   cc.categoryId = cat.id
            and   c.id = ' . $compId . '
            order by clubname';
    $result = e4s_queryNoLog($sql);
    $retArr = array();
    while ( $obj = $result->fetch_object(E4S_CLUBCOMP_OBJ)) {
        unset($obj->optionsSet);
        unset($obj->options);
        $clubObj = new stdClass();
        $clubObj->id = $obj->clubId;
        $clubObj->name = $obj->clubName;
        $obj->club = $clubObj;
        unset($obj->clubId);
        unset($obj->clubName);

        $compObj = new stdClass();
        $compObj->id = $obj->compId;
        $compObj->name = $obj->compName;
        $obj->competition = $compObj;
        unset($obj->compId);
        unset($obj->compName);

        $catObj = new stdClass();
        $catObj->id = $obj->categoryId;
        $catObj->name = $obj->categoryName;
        $obj->category = $catObj;
        unset($obj->categoryId);
        unset($obj->categoryName);

        $retArr[] = $obj;
    }
    return $retArr;
}
function e4s_getClubCompDefObj($obj){
    $id = checkFieldForXSS($obj, 'id:Comp Club Def ID' . E4S_CHECKTYPE_NUMERIC);
    $clubId = checkFieldForXSS($obj, 'clubId:Club id' . E4S_CHECKTYPE_NUMERIC);
    $compId = checkFieldForXSS($obj, 'compId:Competition id' . E4S_CHECKTYPE_NUMERIC);
    $catId = checkFieldForXSS($obj, 'categoryId:Club Comp Category id' . E4S_CHECKTYPE_NUMERIC);
    $bibs = checkFieldForXSS($obj, 'bibNos:Bib Numbers');

    $obj = new stdClass();
    $obj->id = $id;
    $obj->clubId = $clubId;
    $obj->compId = $compId;
    $obj->categoryId = $catId;
    $obj->bibNos = $bibs;

    return $obj;
}