<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
include_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

function e4s_R4SScheduleEventCRUD($obj, $process) {
    $model = e4s_getR4SScheduleEventModel($obj, $process);

    switch ($model->process) {
//        case E4S_CRUD_LIST:
//            e4s_listR4SSchedules($model, true);
//            break;
        case E4S_CRUD_CREATE:
            e4s_createR4SScheduleEvent($model, TRUE);
            break;
//        case E4S_CRUD_READ:
//            e4s_readR4SSchedule($model,true);
//            break;
        case E4S_CRUD_UPDATE:
            e4s_updateR4SScheduleEvent($model, TRUE);
            break;
//        case E4S_CRUD_DELETE:
//            e4s_deleteR4SSchedule($model,true);
//            break;
    }
}

function e4s_getR4SScheduleEventModel($obj, $process) {
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;

    $model->id = checkFieldForXSS($obj, 'id:R4S Schedule Event Id');
    $model->scheduleId = checkFieldForXSS($obj, 'scheduleid:R4S Schedule Id');
    $model->eventGroupId = checkFieldForXSS($obj, 'eventgroupid:Event Group Id');

    e4s_addStdPageInfo($obj, $model);
    return $model;
}

function e4s_getR4SScheduleEventCRUD($key, $value, $mustExist) {
    $row = e4s_getR4SScheduleEvent($key, $value, $mustExist);
    return $row;
}

function e4s_getR4SScheduleEvent($key, $value, $mustExist) {
    return e4s_getCRUDRow(E4S_TABLE_R4SSCHEDULEEVENT, $key, $value, $mustExist);
}

function e4s_readR4SScheduleEvent($model, $exit) {
    $row = e4s_getR4SScheduleEvent('id', $model->id, TRUE);
    if ($exit) {
        Entry4UISuccess($row);
    }
    return e4s_getDataAsType($row, E4S_OPTIONS_OBJECT);
}

function e4s_createR4SScheduleEvent($model, $exit) {
    $id = $model->id;
    $scheduleId = $model->scheduleId;
    $eventGroupId = $model->eventGroupId;

    if ((int)$id !== 0) {
        Entry4UIError(7610, 'ID of non zero passed to create');
    }
    $sql = 'insert into ' . E4S_TABLE_R4SSCHEDULEEVENT . ' (scheduleid, eventgroupid)
            values (' . $scheduleId . ',' . $eventGroupId . ')';
    e4s_queryNoLog($sql);
    $model->id = e4s_getLastID();
    return e4s_readR4SScheduleEvent($model, $exit);
}

function e4s_updateR4SScheduleEvent($model, $exit) {
//    $id = $model->id;
//    $scheduleId = $model->scheduleId;
//    $eventGroupId = $model->eventGroupId;
//
//    e4s_getR4SSchedule("id", $id, true);
//    $sql = "update " . E4S_TABLE_R4SSCHEDULEEVENT . "
//            set scheduleid = " . $scheduleId . ",
//                eventgroupid = " . $eventGroupId . "
//            where id = " . $id;
//    e4s_queryNoLog($sql);
//    $row = e4s_getR4SSchedule("id", $id, true);
//    if ( $exit) {
//        Entry4UISuccess($row);
//    }
//    return $row;
}

function e4s_deleteR4SScheduleEvent($model, $exit) {

    $id = $model->id;
    // check Row exists
    $row = e4s_getR4SScheduleEvent('id', $id, TRUE);

    $sql = 'delete from ' . E4S_TABLE_R4SSCHEDULEEVENT . '
            where id = ' . $id;

    e4s_queryNoLog($sql);
    if ($exit) {
        Entry4UISuccess($row);
    }
    return $row;
}

function e4s_deleteR4SScheduleEventsForSchedule($id) {
    $sql = 'delete from ' . E4S_TABLE_R4SSCHEDULEEVENT . '
            where scheduleid = ' . $id;
    e4s_queryNoLog($sql);
}

function e4s_listR4SScheduleEvents($model, $exit) {

//    $pagesize = $model->pageInfo->pagesize;
//    $page = $model->pageInfo->page;
//
//    $usePaging = false;
//    if (isset($pagesize) and isset($page)) {
//        $usePaging = true;
//    }
//
//    $sql = "select id
//                    ,scheduleid
//                    ,eventgroupid
//        from " . E4S_TABLE_R4SSCHEDULEEVENT ;
//
//    if ( !is_null($model->outputId)){
//        $sql .= " and scheduleid = " . $model->scheduleid ;
//    }
//
//    $sql .= " order by scheduleid ";
//
//    if ($usePaging and $pagesize !== 0 ){
//        $sql .= " limit " . (($page - 1) * $pagesize)  . ", ". $pagesize;
//    }
//
//    $result = e4s_queryNoLog( $sql);
//    $rows = $result->fetch_all(MYSQLI_ASSOC);
//    if ( $exit) {
//        Entry4UISuccess( $rows );
//    }
//    return $rows;
}
