<?php
require_once E4S_FULL_PATH . 'dbInfo.php';
require_once E4S_FULL_PATH . 'builder/e4sCRUD.php';

function e4s_clearEventGroupTimes($obj){
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $useCompDate = checkFieldForXSS($obj, 'usecompdate:Use Competition Date');
    if ( is_null($useCompDate) ){
        $useCompDate = false;
    }
    $compObj = e4s_getCompObj($compId,false,true);
    if (! $compObj->isOrganiser() ){
        Entry4UIError(7090, 'You are not authorised to use this function');
    }

    $sql = 'update ' . E4S_TABLE_EVENTGROUPS . '
            set startdate = ';

    if ($useCompDate) {
        $sql .= " '" . $compObj->getDate() . "' ";
    }else{
        $sql .= ' date(startdate) ';
    }

    $sql .= ' where compid = ' . $compId;
    e4s_queryNoLog($sql);

    $sql = 'update ' . E4S_TABLE_COMPEVENTS . '
            set startdate = ';

    if ($useCompDate) {
        $sql .= " '" . $compObj->getDate() . "' ";
    }else{
        $sql .= ' date(startdate) ';
    }

    $sql .= ' where compid = ' . $compId;
    e4s_queryNoLog($sql);
}

function e4s_updateMaxInHeatFromCard($egId, $newMaxInHeat){
	$egObj = eventGroup::getObj($egId, FALSE);
	$egOptions = $egObj->getOptions();
	$existMaxInHeat = eventGroup::getMaxInHeat($egOptions);
	if ($existMaxInHeat === $newMaxInHeat ) {
		return;
	}else {
		// update options
		$egOptions = eventGroup::setMaxInHeat( $egOptions, $newMaxInHeat );
		$egOptions = e4s_removeDefaultEventGroupOptions( $egOptions );

		$sql = 'update ' . E4S_TABLE_EVENTGROUPS . "
            set options = '" . e4s_decode( $egOptions, E4S_OPTIONS_STRING ) . "'
            where id = " . $egId;

		e4s_queryNoLog( $sql );
		e4s_updateCeOptionsFromEg($egId, $newMaxInHeat);
	}
}
function e4s_updateLimitsFromCard($obj) {
    $egId = (int)checkFieldFromParamsForXSS($obj, 'egid:EventGroup id' . E4S_CHECKTYPE_NUMERIC);
    $options = checkJSONObjForXSS($obj, 'options:EventGroup options');
    $options = e4s_getOptionsAsObj($options);
    $reSeed = true;
    if ( isset($options->reSeed) and $options->reSeed === 'false' ){
        $reSeed = false;
    }
	$trialsDefault = false;
	$compTrials = null;
	if ( isset($options->trialsDefault) and $options->trialsDefault === 'true' ){
		$compTrials = $options->trials;
		$trialsDefault = true;
	}

    $egObj = eventGroup::getObj($egId, FALSE);
    $egOptions = $egObj->getOptions();
    $existMaxInHeat = eventGroup::getMaxInHeat($egOptions);
    $newMaxInHeat = eventGroup::getMaxInHeat($options);
    $existLaneCount = eventGroup::getLaneCount($egOptions);
    $newLaneCount = eventGroup::getLaneCount($options);
    $existFirstLane = eventGroup::getFirstLane($egOptions);
    $newFirstLane = eventGroup::getFirstLane($options);
    $existSeedAge = eventGroup::getSeedAge($egOptions);
    $newSeedAge = eventGroup::getSeedAge($options);
    $existAutoQualify = eventGroup::getAutoQualify($egOptions);
    $newAutoQualify = eventGroup::getAutoQualify($options);
    $existNonAutoQualify = eventGroup::getNonAutoQualify($egOptions);
    $newNonAutoQualify = eventGroup::getNonAutoQualify($options);

    $existDoubleUp = eventGroup::getSeedDoubleUp($egOptions);
    $newDoubleUp = eventGroup::getSeedDoubleUp($options);

	$existProgressions = eventGroup::getProgressions($egOptions);
	$newProgressions = eventGroup::getProgressions($options);

    $existSeedGender = eventGroup::getSeedGender($egOptions);
    $newSeedGender = eventGroup::getSeedGender($options);

	// trials
    $existMaxTrials = eventGroup::getMaxTrials($egOptions);
    $newMaxTrials = eventGroup::getMaxTrials($options);
	$existMinTrials = eventGroup::getMinTrials($egOptions);
    $newMinTrials = eventGroup::getMinTrials($options);
	$existAthleteCnt = eventGroup::getAthleteCnt($egOptions);
	$newAthleteCnt = eventGroup::getAthleteCnt($options);
	$existJumpOrder = eventGroup::getJumpOrder($egOptions);
	$newJumpOrder = eventGroup::getJumpOrder($options);

	$fieldsChange = true;
    if ($existMaxInHeat === $newMaxInHeat) {
	    if ( $existLaneCount === $newLaneCount ) {
		    if ( $existFirstLane === $newFirstLane ) {
			    if ( $existSeedAge === $newSeedAge ) {
				    if ( $existSeedGender === $newSeedGender ) {
					    if ( $existDoubleUp === $newDoubleUp ) {
						    if ( $existAutoQualify === $newAutoQualify ) {
							    if ( $existNonAutoQualify === $newNonAutoQualify ) {
								    if ( $existProgressions === $newProgressions ) {
									    if ( $existMaxTrials === $newMaxTrials ) {
										    if ( $existMinTrials === $newMinTrials ) {
											    if ( $existAthleteCnt === $newAthleteCnt ) {
												    if ( $existJumpOrder === $newJumpOrder ) {
													    $fieldsChange = false;
												    }
											    }
										    }
									    }
								    }
							    }
						    }
					    }
				    }
			    }
		    }
	    }
    }

	if ( $fieldsChange ){
	    // update options
	    $egOptions = eventGroup::setMaxInHeat( $egOptions, $newMaxInHeat );
	    $egOptions = eventGroup::setLaneCount( $egOptions, $newLaneCount );
	    $egOptions = eventGroup::setFirstLane( $egOptions, $newFirstLane );
	    $egOptions = eventGroup::setSeeded( $egOptions, false );
	    $egOptions = eventGroup::setSeedAge( $egOptions, $newSeedAge );
	    $egOptions = eventGroup::setSeedGender( $egOptions, $newSeedGender );
	    $egOptions = eventGroup::setSeedDoubleUp( $egOptions, $newDoubleUp );
	    $egOptions = eventGroup::setAutoQualify( $egOptions, $newAutoQualify );
	    $egOptions = eventGroup::setNonAutoQualify( $egOptions, $newNonAutoQualify );
	    $egOptions = eventGroup::setProgressions( $egOptions, $newProgressions );

		if ( $trialsDefault ){
			$egOptions = eventGroup::clearTrials( $egOptions );
		}else{
			$egOptions = eventGroup::setMaxTrials( $egOptions, $newMaxTrials );
			$egOptions = eventGroup::setMinTrials( $egOptions, $newMinTrials );
			$egOptions = eventGroup::setAthleteCnt( $egOptions, $newAthleteCnt );
			$egOptions = eventGroup::setJumpOrder( $egOptions, $newJumpOrder );
		}
	    $egOptions = e4s_removeDefaultEventGroupOptions( $egOptions );

	    $sql = 'update ' . E4S_TABLE_EVENTGROUPS . "
            set options = '" . e4s_decode( $egOptions, E4S_OPTIONS_STRING ) . "'
            where id = " . $egId;

	    e4s_queryNoLog( $sql );

	    if ( $existMaxInHeat !== $newMaxInHeat ) {
		    e4s_updateCeOptionsFromEg($egId, $newMaxInHeat);
	    }
    }
	if ( $reSeed ) {
		// update Seedings ( if required )
		$seedingObj  = new seedingV2Class( 0, $egId );
		$seedingRows = $seedingObj->getSeedings( $reSeed );

		// send Socket
		$socketData          = new stdClass();
		$socketData->egId    = $egId;
		$socketData->options = $egOptions;
		$socketData->entries = $seedingRows;
		e4s_sendSocketInfo( $egObj->compId, $socketData, R4S_SOCKET_EVENT_GROUP_OPTIONS );
	}
	if ( $trialsDefault ){
		$compObj = e4s_getCompObj($egObj->compId);
		$cOptions = $compObj->getOptions();
		$cOptions->trials = $compTrials;
		$compObj->updateCompOptions($cOptions);
	}
    Entry4UISuccess();
}
function e4s_updateCeOptionsFromEg($egId, $maxInHeat){
	// update CE Records
	$sql    = 'select id,
					   options
                from ' . E4S_TABLE_COMPEVENTS . '
				where maxgroup = ' . $egId;
	$result = e4s_queryNoLog( $sql );
	while ( $obj = $result->fetch_object() ) {
		$ceOptions = e4s_getOptionsAsObj( $obj->options );
		$ceOptions = eventGroup::setMaxInHeat( $ceOptions, $maxInHeat );
		$ceOptions = e4s_removeDefaultCompEventOptions( $ceOptions );
		$sql       = 'update ' . E4S_TABLE_COMPEVENTS . "
					set options = '" . e4s_decode( $ceOptions, E4S_OPTIONS_STRING ) . "'
					where id = " . $obj->id;
		e4s_queryNoLog( $sql );
	}
}
function e4s_updateEgNotes($obj) {
    $egId = checkFieldFromParamsForXSS($obj, 'id:EventGroup id' . E4S_CHECKTYPE_NUMERIC);
    $notes = checkFieldFromParamsForXSS($obj, 'notes:Notes');
    $sql = 'update ' . E4S_TABLE_EVENTGROUPS . "
            set notes = '" . $notes . "'
            where id = " . $egId;
    e4s_queryNoLog($sql);
    $eventGroupObj = eventGroup::getObj($egId);
    $eventGroupObj->sendNotesSocket($egId, $notes);
}
function e4s_updateResultsPossible($obj){
    $egId = checkFieldFromParamsForXSS($obj, 'egId:EventGroup id' . E4S_CHECKTYPE_NUMERIC);
    $possible = checkFieldFromParamsForXSS($obj, 'possible:EventGroup Results Possible');

    $egObj = eventGroup::getObj($egId);
    $egObj->updateResultsPossible($possible);
    Entry4UISuccess();
}

function e4s_deleteEventGroup($obj){
    $egId = (int)checkFieldFromParamsForXSS($obj, 'id:EventGroup id');
    e4s_deleteEventGroupById($egId);
    Entry4UISuccess();
}
function e4s_deleteEventGroupById($egId):void{
    $egObj = eventGroup::getObj($egId);
    if ( !$egObj->compObj->isOrganiser() ){
        Entry4UIError(9033, 'You are not authorised to perform this action');
    }
    $egObj->delete();
}
function e4s_updateEventGroup($obj) {
    $egId = (int)checkFieldFromParamsForXSS($obj, 'id:EventGroup id');
    if ($egId === 0) {
        // process Array
        $compId = e4s_updateArrayEventGroups($obj);
    } else {
        // process single
        $compId = e4s_updateSingleEventGroup($obj);
    }
    e4s_removeUnusedEventGroups($compId);
    $compObj = e4s_getCompObj($compId);
    $compObj->updateCompOptions();
    Entry4UISuccess();
}

function e4s_updateArrayEventGroups($obj) {
    $arr = $obj->get_params('JSON');
    $compId = 0;
    $egIdArr = array();
    // check eventNo is unique
    foreach ($arr as $eg) {
        $egId = (int)$eg['eventNo'];
        if ( array_key_exists($egId, $egIdArr) ){
            Entry4UIError(5310, 'Duplicate Event Numbers are not allowed ( Event No ' . $eg['eventNo'] . ' ). Nothing has been saved.');
        }
        $egIdArr[$egId] = true;
    }
    foreach ($arr as $eg) {
        $egObj = eventGroup::getObj($eg['id'], TRUE);
        $useObj = new stdClass();
        $useObj->egId = $eg['id'];
        $useObj->isOpen = $eg['isOpen'];
        $useObj->eventDateTimeISO = $eg['eventDateTime'];
        $useObj->name = e4s_allowOneBracket($eg['name']);
        $useObj->name = str_replace(',', '.',  $useObj->name);
        $useObj->notes = $eg['notes'];
        $useObj->type = $eg['type'];
        $useObj->typeNo = $eg['typeNo'];
        $useObj->eventNo = $eg['eventNo'];
        $useObj->bibSortNo = $eg['bibSortNo'];
        if ($useObj->bibSortNo === '') {
            $useObj->bibSortNo = 'null';
        }
        $useObj->options = $eg['options'];
        if (is_null($useObj->options)) {
            $useObj->options = e4s_getEventGroupV2DefaultOptions();
        } else {
            $useObj->options = e4s_mergeInOptions($egObj->egObj->options, $useObj->options);
            $useObj->options = e4s_addDefaultEventGroupV2Options($useObj->options);
        }
        $useObj->options->limits = $eg['limits'];
        $sendSocketMsg = FALSE;
        if ($useObj->notes !== $egObj->egObj->notes) {
//            trigger socketMsg on Notes ?
            $sendSocketMsg = TRUE;
        }
        $sendSocketMsg = FALSE;
        if ($useObj->notes !== $egObj->egObj->notes) {
//            trigger socketMsg on Notes ?
            $sendSocketMsg = TRUE;
        }
        $compId = $egObj->updateEG($useObj, $sendSocketMsg);

    }
    return $compId;
}

function e4s_updateSingleEventGroup($obj) {
    $model = new stdClass();
    $model->isOpen = (int)checkFieldFromParamsForXSS($obj, 'isOpen:Event Open');
    $model->egId = (int)checkFieldFromParamsForXSS($obj, 'id:EventGroup id');
    $model->eventDateTimeISO = checkFieldFromParamsForXSS($obj, 'eventDateTime:Event Date and Time');
    $model->notes = checkFieldFromParamsForXSS($obj, 'notes:Event Group Notes');
    $model->name = checkFieldFromParamsForXSS($obj, 'name:EventGroup name');
    $model->name = e4s_allowOneBracket($model->name);
    $model->name = str_replace(',', '.',  $model->name);
    $model->type = checkFieldFromParamsForXSS($obj, 'type:Event type');
    $model->typeNo = (int)checkFieldFromParamsForXSS($obj, 'typeNo:Event type number');
    $model->eventNo = (int)checkFieldFromParamsForXSS($obj, 'eventNo:Event number');
    $model->bibSortNo = checkFieldFromParamsForXSS($obj, 'bibSortNo:Bib Sorting number');

    if (!is_null($model->bibSortNo) and $model->bibSortNo !== '') {
        $model->bibSortNo = (int)$model->bibSortNo;
    } else {
        $model->bibSortNo = 'null';
    }
    $model->options = checkJSONObjForXSS($obj, 'options:Options');
    if (is_null($model->options)) {
        $model->options = e4s_getEventGroupV2DefaultOptions();
    } else {
        $model->options = e4s_addDefaultEventGroupV2Options($model->options);
    }
    $model->options->limits = checkJSONObjForXSS($obj, 'limits:Limits');

    $egObj = eventGroup::getObj($model->egId, TRUE);
    e4s_hasMaxAthletesChanged($egObj->egObj, $model->options->limits);
    $egObj->updateEG($model);
    return $egObj->compId;
}

function e4s_hasMaxAthletesChanged($egObj, $limits) {
    $maxInHeat = $egObj->options->maxInHeat;
    $newMaxInHeat = $limits['maxInHeat'];
    if ($maxInHeat !== $newMaxInHeat) {
        $egId = $egObj->id;
        $seedingObj = new seedingV2Class($egObj->compId, $egId);
        // clear the seedings for this egId
        $seedingObj->clear();
    }
}

function e4s_mergeEGs($obj){
    $model = new stdClass();
    $model->fromEgId = (int)checkFieldFromParamsForXSS($obj, 'fromEgId:From EG ID' . E4S_CHECKTYPE_NUMERIC);
    $model->toEgId = (int)checkFieldFromParamsForXSS($obj, 'toEgId:To EG ID' . E4S_CHECKTYPE_NUMERIC);
    $sql = 'select compId
            from ' . E4S_TABLE_EVENTGROUPS . '
            where id = ' . $model->toEgId;
    $result = e4s_queryNoLog($sql);
    if ($result->num_rows !== 1 ){
        Entry4UIError(6980, 'Invalid To Event Group Sent');
    }
    $obj = $result->fetch_object();
    $compId = (int)$obj->compId;
    $sql = 'update ' . E4S_TABLE_COMPEVENTS . '
            set maxgroup = ' . $model->toEgId . '
            where maxgroup = ' . $model->fromEgId;
    e4s_queryNoLog($sql);

    $sql = 'delete from ' . E4S_TABLE_EVENTGROUPS . '
            where id = ' . $model->fromEgId;
    e4s_queryNoLog($sql);

    $sql = 'delete from ' . E4S_TABLE_SEEDING . '
            where eventgroupid in (' . $model->fromEgId . ',' . $model->toEgId . ')';
    e4s_queryNoLog($sql);

    // send Socket
    $socketData = new stdClass();
    $socketData->fromEgId = $model->fromEgId;
    $socketData->toEgId = $model->toEgId;
    e4s_sendSocketInfo($compId, $socketData, R4S_SOCKET_EVENT_GROUP_MERGE);

    Entry4UISuccess($model);
}
function e4s_actionEG($obj, $process) {
    $model = e4s_getEventGroupArrModel($obj, $process);
    $egObj = new eventGroup($model->compid);
    switch ($process) {
        case E4S_CRUD_LIST:
            $egObj->listEventGroups($model, TRUE);
            break;
        case E4S_CRUD_UPDATE:
            $egObj->moveEventGroup($model);
            break;
    }
}

function e4s_getEventGroupArrModel($obj, $process) {
    if (is_null($process)) {
        Entry4UIError(9026, 'You must pass a process', 400, '');
    }
    $model = new stdClass();
    $model->process = $process;
    $model->compid = (int)checkFieldFromParamsForXSS($obj, 'compId:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    // used for the update
    $params = $obj->get_params('JSON');

    $model->ceids = [];
    if (array_key_exists('ceids', $params)) {
        $model->ceids = $params['ceids'];
    }
    $model->group = checkJSONObjForXSS($obj, 'group:Event Group Information');
    $model->group = e4s_getDataAsType($model->group, E4S_OPTIONS_OBJECT);
    $model->options = new stdClass();
    e4s_addStdPageInfo($obj, $model);

    return $model;
}