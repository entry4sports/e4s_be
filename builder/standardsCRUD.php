<?php
include_once E4S_FULL_PATH . 'dbInfo.php';

//------------------------------
//       Standard Values
//------------------------------
function e4s_actionStandardValues($obj, $process) {
    $model = e4s_getStandardValuesModel($obj, $process);

    switch ($model->process) {
        case E4S_CRUD_LIST:
            $rows = e4s_listStandardValues($model);
            Entry4UISuccess($rows);
            break;
        case E4S_CRUD_CREATE:
            $row = e4s_createStandardValue($model);
            Entry4UISuccess($row);
            break;
        case E4S_CRUD_UPDATE:
            $row = e4s_updateStandardValue($model);
            Entry4UISuccess($row);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteStandardValue($model);
            Entry4UISuccess();
            break;
    }
}
function e4s_getStandardValuesModel($obj, $process) {
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;

    $model->id = checkFieldForXSS($obj, 'id:Standard Value Id');
    if ( $process === E4S_CRUD_UPDATE and $model->id === 0){
        $model->process = E4S_CRUD_CREATE;
        $model->id = 0;
    }
    if ( $process === E4S_CRUD_CREATE and $model->id > 0){
        $model->process = E4S_CRUD_UPDATE;
    }
    $model->standardId = checkFieldForXSS($obj, 'standardId:Standard Id');
    $model->eventId = checkFieldForXSS($obj, 'eventId:Event Id');
    $model->ageGroupId = checkFieldForXSS($obj, 'ageGroupId:ageGroupId');
    $model->value = checkFieldForXSS($obj, 'value:Standard value');

    e4s_addStdPageInfo($obj, $model);
    return $model;
}
function e4s_readStandardValueById($id){
    $sql = 'select *
            from ' . E4S_TABLE_STANDARDVALUES . '
            where id = ' . $id;
    return e4s_getStandardValue($sql);
}
function e4s_readStandardValueByInfo($model){
    $sql = 'select *
            from ' . E4S_TABLE_STANDARDVALUES . '
            where standardid = ' . $model->standardId . '
            and   eventid = ' . $model->eventId . '
            and   agegroupid = ' . $model->ageGroupId;

    return e4s_getStandardValue($sql);
}
function e4s_getStandardValue($sql){
    $result = e4s_queryNoLog($sql);
    if ( $result->num_rows === 0){
        return null;
    }
    $obj = $result->fetch_object(E4S_STANDARD_VALUES_OBJ);
    return $obj;
}
function e4s_listStandardValues($model) {
//    $startswith = $model->pageInfo->startswith;
    $pagesize = $model->pageInfo->pagesize;
    $page = $model->pageInfo->page;

    $usePaging = FALSE;
    if (isset($pagesize) and isset($page)) {
        $usePaging = TRUE;
    }

    $sql = 'select id, 
                    standardId,
                    eventId,
                    ageGroupId,
                    value
        from ' . E4S_TABLE_STANDARDVALUES;
    if ( $model->id > 0 ){
        $sql .= ' where standardid = ' . $model->id;
    }

    $sql .= ' order by standardid,eventid,agegroupid ';

    if ($usePaging and $pagesize !== 0) {
        $sql .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
    }

    $result = e4s_queryNoLog($sql);
    $rows = array();
    while($obj = $result->fetch_object(E4S_STANDARD_VALUES_OBJ)){
        $rows[] = $obj->returnObj();
    }

    return $rows;
}
function e4s_updateStandardValue($model){
    $existRow = e4s_readStandardValueById($model->id);
    if ( is_null($existRow) ){
        Entry4UIError(4795, 'Standard Value does not exists');
    }
    $sql = 'update ' . E4S_TABLE_STANDARDVALUES . '
            set value = ' . $model->value . '
            where id = ' . $model->id;
    e4s_queryNoLog($sql);
    $obj = new stdClass();
    $obj->id = $model->id;
    $obj->standardId = $model->standardId;
    $obj->eventId = $model->eventId;
    $obj->ageGroupId = $model->ageGroupId;
    $obj->value = $model->value;
    return $obj;
}
function e4s_createStandardValue($model){
    $existRow = e4s_readStandardValueByInfo($model);
    if ( !is_null($existRow) ){
        Entry4UIError(4790, 'Standard Value already exists');
    }
    $sql = 'insert into ' . E4S_TABLE_STANDARDVALUES . ' ( standardId, eventId, ageGroupId, value )
            values(
               ' . $model->standardId . ',
               ' . $model->eventId . ',
               ' . $model->ageGroupId . ',
               ' . $model->value . ' 
               )';

    e4s_queryNoLog($sql);
    $obj = new stdClass();
    $obj->id = e4s_getLastID();
    $obj->standardId = $model->standardId;
    $obj->eventId = $model->eventId;
    $obj->ageGroupId = $model->ageGroupId;
    $obj->value = $model->value;
    return $obj;
}
function e4s_deleteStandardValue($model){
    $existRow = e4s_readStandardValueById($model->id);
    if ( is_null($existRow) ){
        Entry4UIError(4696, 'Standard Value does not exists');
    }
    $sql = 'delete from ' . E4S_TABLE_STANDARDVALUES . '
            where id = ' . $model->id;
    e4s_queryNoLog($sql);

    return true;
}
//------------------------------
//          Standards
//------------------------------
function e4s_actionStandards($obj, $process) {
    $model = e4s_getStandardsModel($obj, $process);

    switch ($model->process) {
        case E4S_CRUD_LIST:
            $rows = e4s_listStandards($model, TRUE);
            Entry4UISuccess($rows);
            break;
        case E4S_CRUD_CREATE:
            $row = e4s_createStandard($model);
            Entry4UISuccess($row);
            break;
        case E4S_CRUD_UPDATE:
            $row = e4s_updateStandard($model);
            Entry4UISuccess($row);
            break;
        case E4S_CRUD_DELETE:
            e4s_deleteStandard($model);
            Entry4UISuccess();
            break;
    }
}

function e4s_getStandardsModel($obj, $process) {
    $model = new stdClass();
    $model->process = $process;
    $model->public = FALSE;

    $model->id = checkFieldForXSS($obj, 'id:Standard Id');
    if ( $process === E4S_CRUD_UPDATE and $model->id === 0){
        $model->process = E4S_CRUD_CREATE;
        $model->id = 0;
    }
    if ( $process === E4S_CRUD_CREATE and $model->id > 0){
        $model->process = E4S_CRUD_UPDATE;
    }
    $model->description = checkFieldForXSS($obj, 'description:Standard Description');
    if ( is_null( $model->description) and ($process === E4S_CRUD_CREATE or $process === E4S_CRUD_UPDATE)){
        Entry4UIError(5620, 'No Standard Description');
    }

    e4s_addStdPageInfo($obj, $model);
    return $model;
}

function e4s_deleteStandard($model){
    $existRow = e4s_readStandardById($model->id);
    if ( is_null($existRow) ){
        Entry4UIError(4696, 'Standard does not exists');
    }
    $sql = 'delete from ' . E4S_TABLE_STANDARDS . '
            where id = ' . $model->id;
    e4s_queryNoLog($sql);

    return true;
}
function e4s_updateStandard($model){
    $existRow = e4s_readStandardById($model->id);
    if ( is_null($existRow) ){
        Entry4UIError(4695, 'Standard does not exists');
    }
    $sql = 'update ' . E4S_TABLE_STANDARDS . "
            set description = '" . $model->description . "'
            where id = " . $model->id;
    e4s_queryNoLog($sql);
    $obj = new stdClass();
    $obj->id = $model->id;
    $obj->description = $model->description;
    return $obj;
}
function e4s_createStandard($model){
    $existRow = e4s_readStandardByDesc($model->description);
    if ( !is_null($existRow) ){
        Entry4UIError(4690, 'Standard already exists');
    }
    $sql = 'insert into ' . E4S_TABLE_STANDARDS . " ( description )
            values('" . $model->description . "')";

    e4s_queryNoLog($sql);
    $obj = new stdClass();
    $obj->id = e4s_getLastID();
    $obj->description = $model->description;
    return $obj;
}
function e4s_readStandardById($id){
    $sql = 'select *
            from ' . E4S_TABLE_STANDARDS . '
            where id = ' . $id;
    return e4s_getStandard($sql);
}
function e4s_readStandardByDesc($description){
    $sql = 'select *
            from ' . E4S_TABLE_STANDARDS . "
            where description = '" . $description . "'";
    return e4s_getStandard($sql);
}
function e4s_getStandard($sql){
    $result = e4s_queryNoLog($sql);
    if ( $result->num_rows === 0){
        return null;
    }
    $obj = $result->fetch_object(E4S_STANDARD_OBJ);
    return $obj;
}
function e4s_listStandards($model, $exit) {
    $startswith = $model->pageInfo->startswith;
    $pagesize = $model->pageInfo->pagesize;
    $page = $model->pageInfo->page;

    $usePaging = FALSE;
    if (isset($pagesize) and isset($page)) {
        $usePaging = TRUE;
    }

    $sql = 'select id, description
        from ' . E4S_TABLE_STANDARDS;

    if (isset($startswith)) {
        $sql .= " where description like '" . $startswith . "%' ";
    }

    $sql .= ' order by description ';

    if ($usePaging and $pagesize !== 0) {
        $sql .= ' limit ' . (($page - 1) * $pagesize) . ', ' . $pagesize;
    }

    $result = e4s_queryNoLog($sql);
    $rows = array();
    while($obj = $result->fetch_object(E4S_STANDARD_OBJ)){
        $rows[] = $obj->returnObj();
    }

    if ($exit) {
        Entry4UISuccess('
        "data":' . json_encode($rows, JSON_NUMERIC_CHECK));
    }
    return $rows;
}
