<style>
    body {
        margin: 0px;
    }

    /*.woocommerce-MyAccount-navigation ul li {*/
    /*    -webkit-transition: background-color .3s;*/
    /*    transition: background-color .3s;*/
    /*    padding: 0;*/
    /*}*/

    .woocommerce-MyAccount-navigation ul a{
        color: #000 !important;
        text-decoration: underline !important;
        line-height: 25px;
    }

    nav ul a {
        -webkit-transition: background-color .3s;
        transition: background-color .3s;
        font-size: 1rem;
        color: #fff ! important;
        display: block;
        padding: 0 0 0 15px;
        cursor: pointer;
    }

    ul {
        display: block;
        list-style-type: disc;
        margin-block-start: 1em;
        margin-block-end: 1em;
        margin-inline-start: 0px;
        margin-inline-end: 0px;
        padding-inline-start: 40px;
    }

    li {
        display: list-item;
        text-align: -webkit-match-parent;
    }

    .nav {
        background-color: #f59220 !important;
        margin-bottom: 15px
    }

    nav ul li {
        -webkit-transition: background-color .3s;
        transition: background-color .3s;
        padding: 0;
    }
    nav .white-text ul li {
        float: left;
    }
    .dropdown-content {
        background-color: #fff;
        margin: 0;
        display: none;
        min-width: 100px;
        overflow-y: auto;
        opacity: 0;
        position: absolute;
        left: 0;
        top: 0;
        z-index: 9999;
        -webkit-transform-origin: 0 0;
        transform-origin: 0 0;
    }

    .nav-wrapper {
        height: 64px;
        position: relative;
    }

    .right {
        float: right;
        color: white;
    }

    ul:not(.browser-default) {
        padding-left: 0;
        list-style-type: none;
    }

    a {
        text-decoration: none;
    }
    form.woocommerce-cart-form {
        width: 60%;
        float: left;
        margin: 2%;
    }
    .cart-collaterals {
        width: 30% !important;
        float: right;
    }
    .cart_totals {
        width: 100% !important;
        margin-right: 2% !important;
    }
    .woocommerce a.button {
        background-color: #4CAF50 !important;
        color: #fff !important;
        margin-left: 5px !important;
    }
    .woocommerce {
        /*width: 30%;*/
    }
</style>
<div>
    <nav class="nav">
        <div class="nav-wrapper primary-background white-text e4s-primary-color-bg-gbr">
            <a href="#/"><img class="e4s-logo" style="height: 45px;" src="/ai_logo_orangebg.gif"></a>

            <ul class="right">
                <li><a class="" href="/#/showentries"><span>Home</span></a></li>
                <li style="display: none;">
                    <a data-target="dropdown-my-account" href="#" id="dropdown-trigger-my-account">
                        My Account
                        <i class="material-icons right">arrow_drop_down</i>
                    </a>
                    <ul class="dropdown-content dropdown-content-e4s" id="dropdown-my-account" tabindex="0">
                        <li tabindex="0"><a class="" href="/#/athletes"><span>Athletes</span></a></li>
                        <li tabindex="0"><a href="/my-account"><span>My Account</span></a></li>
                    </ul>
                </li>

                <li>
                    <a>
                        <span>
                                    Log out: <span><?php echo wp_get_current_user()->display_name; ?></span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>