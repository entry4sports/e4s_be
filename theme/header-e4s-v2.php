<?php
include_once E4S_FULL_PATH . 'dbInfo.php';
$config = e4s_getConfig();
$theme = strtolower($config['theme']);
$logo = 'e4s';
$theme_class = '';
if ( $theme === E4S_THEME_IRL ){
    $theme_class = 'ia-';
    $logo = 'aai';
}

?>
<style>
    body {
        margin: 0px;
    }

    /*.woocommerce-MyAccount-navigation ul li {*/
    /*    -webkit-transition: background-color .3s;*/
    /*    transition: background-color .3s;*/
    /*    padding: 0;*/
    /*}*/
    .woocommerce-MyAccount-content {
        padding-right: 25px;
    }

    .woocommerce-MyAccount-navigation ul a {
        color: #000 !important;
        text-decoration: underline !important;
        line-height: 25px;
    }

    nav ul a {
        -webkit-transition: background-color .3s;
        transition: background-color .3s;
        font-size: 1rem;
        color: #fff ! important;
        display: block;
        padding: 0 0 0 15px;
        cursor: pointer;
    }

    ul {
        display: block;
        list-style-type: disc;
        margin-block-start: 1em;
        margin-block-end: 1em;
        margin-inline-start: 0px;
        margin-inline-end: 0px;
        padding-inline-start: 40px;
    }

    li {
        display: list-item;
        text-align: -webkit-match-parent;
    }

    .nav {
        background-color: #1f417e !important;
        margin-bottom: 15px
    }

    nav ul li {
        -webkit-transition: background-color .3s;
        transition: background-color .3s;
        padding: 0;
    }

    nav .white-text ul li {
        float: left;
    }

    .dropdown-content {
        background-color: #fff;
        margin: 0;
        display: none;
        min-width: 100px;
        overflow-y: auto;
        opacity: 0;
        position: absolute;
        left: 0;
        top: 0;
        z-index: 9999;
        -webkit-transform-origin: 0 0;
        transform-origin: 0 0;
    }

    .nav-wrapper {
        height: 64px;
        position: relative;
    }

    .right {
        float: right;
        color: white;
    }

    ul:not(.browser-default) {
        padding-left: 0;
        list-style-type: none;
    }

    a {
        text-decoration: none;
    }

    form.woocommerce-cart-form {
        width: 60%;
        float: left;
        margin: 2%;
    }
    .cart-collaterals {
        width: 90% !important;
        padding-left: 30px;
    }

    .cart_totals {
        width: 100% !important;
        margin-right: 2% !important;
    }

    .woocommerce a.button {
        background-color: #006940 !important;
        color: #fff !important;
        margin-left: 5px !important;
    }

    .woocommerce {
        /*width: 30%;*/
    }
</style>
<div id="e4sv2Header">
    <!--    <nav class="nav">-->
    <!--        <div class="nav-wrapper secondary-background white-text e4s-primary-color-bg-gbr">-->
    <!--            <a href="#/"><img class="e4s-aai-logo" style="height: 45px;" src="/e4s_logo.png"></a>-->
    <!---->
    <!--            <ul class="right">-->
    <!--                <li><a href="/#/showentries"><span>Home</span></a></li>-->
    <!--                <li><a href="/my-account">My Account</a></li>-->
    <!--                <li><a><span>Log out: <span style="padding-right: 20px">-->
    <?php //echo wp_get_current_user()->display_name; ?><!--</span></span></a></li>-->
    <!--            </ul>-->
    <!--        </div>-->
    <!--    </nav>-->

    <div id="e4s-nav-bar" class="e4s-flex-column e4s-sticky-navigation">
        <div class="e4s-navigation-bar e4s-navigation-bar--<?php echo $theme_class?>primary">
            <div class="e4s-navigation-bar--content-wrapper">
                <div class="e4s-navigation-bar-logo--container" style="cursor: pointer;">
                    <img src="<?php echo E4S_PATH ?>css/images/<?php echo $logo ?>_logo_single.svg" class="e4s_logo_white">
                </div>
                <ul class="e4s-navigation-bar-menu">
                    <li class="e4s-navigation-bar-menu--item">
                        <a href="/" class="e4s-subheader--200">
                            <span class="e4s-subheader--200">Home</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>