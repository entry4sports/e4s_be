<?php
/**
 * Email Order Items
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-items.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined('ABSPATH') || exit();

$text_align = is_rtl() ? 'right' : 'left';
$margin_side = is_rtl() ? 'left' : 'right';
$downloads = $order->get_downloadable_items();

foreach ($items as $item_id => $item) :
    $product = $item->get_product();
    $sku = '';
    $purchase_note = '';
    $image = '';
    $download = e4sOrders::getDownloadForItem($item, $downloads);
    if (!apply_filters('woocommerce_order_item_visible', TRUE, $item)) {
        continue;
    }

    if (is_object($product)) {
        $sku = $product->get_sku();
        $purchase_note = $product->get_purchase_note();
        $image = $product->get_image($image_size);
    }

    ?>
    <tr class="<?php echo esc_attr(apply_filters('woocommerce_order_item_class', 'order_item', $item, $order)); ?>">
        <td class="td"
            style="text-align:<?php echo esc_attr($text_align); ?>; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap:break-word;">
            <?php

            // Show title/image etc.
            if ($show_image) {
                echo wp_kses_post(apply_filters('woocommerce_order_item_thumbnail', $image, $item));
            }

            // Product name.
            echo wp_kses_post(apply_filters('woocommerce_order_item_name', $item->get_name(), $item, FALSE));

            // SKU.
            if ($show_sku && $sku) {
                echo wp_kses_post(' (#' . $sku . ')');
            }

            // allow other plugins to add additional product information here.
            do_action('woocommerce_order_item_meta_start', $item_id, $item, $order, $plain_text);

            wc_display_item_meta($item, array('label_before' => '<strong class="wc-item-meta-label" style="float: ' . esc_attr($text_align) . '; margin-' . esc_attr($margin_side) . ': .25em; clear: both">',));

            // allow other plugins to add additional product information here.
            do_action('woocommerce_order_item_meta_end', $item_id, $item, $order, $plain_text);

            ?>
        </td>
        <td class="td"
            style="text-align:<?php echo esc_attr($text_align); ?>; vertical-align:middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
            <?php
            $qty = $item->get_quantity();
            $refunded_qty = $order->get_qty_refunded_for_item($item_id);

            if ($refunded_qty) {
                $qty_display = '<del>' . esc_html($qty) . '</del> <ins>' . esc_html($qty - ($refunded_qty * -1)) . '</ins>';
            } else {
                $qty_display = esc_html($qty);
            }
            echo wp_kses_post(apply_filters('woocommerce_email_order_item_quantity', $qty_display, $item));
            ?>
        </td>
        <td class="td"
            style="text-align:<?php echo esc_attr($text_align); ?>; vertical-align:middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
            <?php echo wp_kses_post($order->get_formatted_line_subtotal($item)); ?>
        </td>
        <td class="td"
            style="text-align:<?php echo esc_attr($text_align); ?>; vertical-align:middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
            <?php
            if (is_null($download)) {
                echo '';
            } else {
                $qty = $item->get_quantity();
                $cnt = 1;
                $style = '';
                $cntDisplay = '';
                if ($qty > 1) {
                    $style = ' style="margin:5px;" ';
                    $cntDisplay = e4sTicket::cntDisplay($cnt);
                }

                while ($cnt <= $qty) {
                    $ticket = new e4sTicket();
                    $download['ticket_count'] = $cnt;
//                    echo "<br>order-details-item.php<br>";
//                    var_dump($download);

                    $ticketURLs = $ticket->createTicketInfo($download, $item, TRUE);
                    if (!is_null($ticketURLs)) {
                        $http = 'http://';
                        if ($_SERVER['HTTPS'] !== '') {
                            $http = 'https://';
                        }
//                    echo ' <a href="' . esc_url($download['download_url'] . '&t=' . $cnt) . '" ' . $style . ' class="woocommerce-MyAccount-downloads-file button alt">' . esc_html($download['download_name'] . $cntDisplay ) . '</a>';
                        foreach ($ticketURLs as $sub => $ticketURL) {
                            echo ' <a href="' . $ticketURL . '" ' . $style . ' class="woocommerce-MyAccount-downloads-file button alt"><img src="' . $http . E4S_CURRENT_DOMAIN . E4S_PATH . '/css/images/qr.png"> ' . esc_html($cntDisplay . $download['download_name']) . '</a>';
                            if ($qty > 1 or $sub > 0) {
                                echo '<br>';
                            }
                        }
                    }
                    $cnt++;
                    $cntDisplay = e4sTicket::cntDisplay($cnt);
                }
            }
            ?>
        </td>
    </tr>
    <?php

    if ($show_purchase_note && $purchase_note) {
        ?>
        <tr>
            <td colspan="3"
                style="text-align:<?php echo esc_attr($text_align); ?>; vertical-align:middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                <?php
                echo wp_kses_post(wpautop(do_shortcode($purchase_note)));
                ?>
            </td>
        </tr>
        <?php
    }
    ?>

<?php endforeach; ?>
