<?php
/**
 * Order again button
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-again.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined('ABSPATH') || exit();
$entriesURL = explode('/my', $order_again_url)[0];
$orderKey = str_replace('wc_order_', '', $order->get_order_key());
?>

<span class="order-again">
	<a class="button wc-backward" href="/">
        <?php esc_html_e('Return to Entries', 'woocommerce'); ?>
    </a>
    <!--	<a href="--><?php //echo esc_url( $entriesURL ); ?><!--" class="button">-->
    <?php //esc_html_e( 'Back to Entries', 'woocommerce' ); ?><!--</a>-->
    <!--    <a href="-->
    <?php //echo esc_url( E4S_TICKET_BUTTON_ORDER_URL . $orderKey ); ?><!--" class="button" style="margin-right: 5px;float: right">-->
    <?php //esc_html_e( 'Show all Tickets', 'woocommerce' ); ?><!--</a>-->
</span>
