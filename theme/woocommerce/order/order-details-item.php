<?php
/**
 * Order Item Details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-item.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

if (!defined('ABSPATH')) {
    exit();
}

if (!apply_filters('woocommerce_order_item_visible', TRUE, $item)) {
    return;
}

?>

<tr class="<?php echo esc_attr(apply_filters('woocommerce_order_item_class', 'woocommerce-table__line-item order_item', $item, $order)); ?>">

    <td class="woocommerce-table__product-name product-name">
        <?php
        $is_visible = $product && $product->is_visible();
        $product_permalink = apply_filters('woocommerce_order_item_permalink', $is_visible ? $product->get_permalink($item) : '', $item, $order);
        //Product Name
        echo apply_filters('woocommerce_order_item_name', $product_permalink ? sprintf('<a href="%s">%s</a>', $product_permalink, $item->get_name()) : $item->get_name(), $item, $is_visible); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
        //End of Product Name
        $qty = $item->get_quantity();
        $refunded_qty = $order->get_qty_refunded_for_item($item_id);

        if ($refunded_qty) {
            $qty_display = '<del>' . esc_html($qty) . '</del> <ins>' . esc_html($qty - ($refunded_qty * -1)) . '</ins>';
        } else {
            $qty_display = esc_html($qty);
        }

        echo apply_filters('woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf('&times;&nbsp;%s', $qty_display) . '</strong>', $item); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

        do_action('woocommerce_order_item_meta_start', $item_id, $item, $order, FALSE);

        $linkedAthlete = e4s_display_item_meta($item); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

        do_action('woocommerce_order_item_meta_end', $item_id, $item, $order, FALSE);
        ?>
    </td>

    <td class="woocommerce-table__product-total product-total">
        <?php echo $order->get_formatted_line_subtotal($item); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
    </td>

    <td class="woocommerce-table__product-download product-download">
        <?php
        if (is_null($download)) {
            echo '-';
        } else {
            echo '';
            $qty = $item->get_quantity();
            $cnt = 1;
            $style = '';
            $cntDisplay = '';
            if ($qty > 1) {
                $style = ' style="margin:5px;" ';
                $cntDisplay = e4sTicket::cntDisplay($cnt);
            }
            $download['linked_athlete'] = $linkedAthlete;

            while ($cnt <= $qty) {
                $ticket = new e4sTicket();
                $download['ticket_count'] = $cnt;
                $ticketLable = $cntDisplay . $download['download_name'];
                $info = $ticket->getCarRegForItem($download, $item, $cnt);
                if ( $info !== '' ){
                    $ticketLable = $cntDisplay . $info;
                }
                $ticketURLs = $ticket->createTicketInfo($download, $item);
                if (!is_null($ticketURLs)) {
//                    echo ' <a href="' . esc_url($download['download_url'] . '&t=' . $cnt) . '" ' . $style . ' class="woocommerce-MyAccount-downloads-file button alt">' . esc_html($download['download_name'] . $cntDisplay ) . '</a>';
                    foreach ($ticketURLs as $sub => $ticketURL) {
                        echo ' <a target="_blank" style="margin:2px;" href="' . $ticketURL . '" ' . $style . ' class="e4s_75 woocommerce-MyAccount-downloads-file button alt"><img src="' . E4S_PATH . 'css/images/qr.png"> ' . esc_html($ticketLable) . '</a>';
                        echo '<br>';
                    }
                }
                $cnt++;
                $cntDisplay = e4sTicket::cntDisplay($cnt);
            }
        }
        ?>
    </td>
</tr>

<?php if ($show_purchase_note && $purchase_note) : ?>

    <tr class="woocommerce-table__product-purchase-note product-purchase-note">

        <td colspan="2"><?php echo wpautop(do_shortcode(wp_kses_post($purchase_note))); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></td>

    </tr>

<?php endif; ?>
