<?php
if ( !defined('E4S_FUNCTIONS_ROUTE_PATH') ) {

	define( "E4S_FUNCTIONS_ROUTE_PATH", '/wp-json/e4s/v5' );
	define( "E4S_NS", 'e4s/v5' );
	define( "E4S_ENTRY_NS", 'entry/v5' );
	define( "E4S_ROUTE_PATH", '/wp-json/e4s/v5' );
	define( "E4S_ADMIN_NS", E4S_NS . '/admin' );
	define( "E4S_PUBLIC_TEXT", '/public' );
	define( "E4S_PUBLIC_NS", E4S_NS . E4S_PUBLIC_TEXT );
	define( "E4S_SECURE_TEXT", '/secure' );
	define( "E4S_SECURE_NS", E4S_NS . E4S_SECURE_TEXT );
	define( "E4S_PRIVATE_TEXT", '/private' );
	define( "E4S_PRIVATE_NS", E4S_NS . E4S_PRIVATE_TEXT );
	define( "E4S_REPORT_TEXT", '/reports' );
	define( "E4S_REPORT_NS", E4S_NS . E4S_REPORT_TEXT );
	define( "E4S_R4S_NS", E4S_NS . '/r4s' );
	define( "E4S_PUBLIC_REPORT_NS", E4S_NS . E4S_PUBLIC_TEXT . E4S_REPORT_TEXT );
	define( "E4S_BUILDER_NS", E4S_NS . '/builder' );
	define( "E4S_CHECK_IN_DOMAIN", 'e4scheck.in' );
	define( "E4S_CRON_USER", 'E4S_CRON_USER' );
	define( "E4S_ATTRIB_ATHLETE", '_athlete' );
	define( "E4S_TICKET_ROUTE", 'ticket' );
	define( "E4S_TICKET", 'e4sticket' );
	define( "E4S_SECUST_LIST", 'Cust' );
	define( "E4S_NOHEADERS", 'E4S_NOHEADERS' );
	define( "E4S_OTD_DIR", 'otd' );
	define( "E4S_OTD_PATH", E4S_FULL_PATH . E4S_OTD_DIR );
	define( "E4S_ORDER_ITEM", 'e4s_order_item' );
	define( "E4S_ORDER_COMPS", 'E4S_ORDER_COMPS' );
	define( "E4S_TYPE_SEP", '~' );
	define( "E4S_TYPE_NUMERIC", 'N' );
	define( "E4S_TYPE_NUMERIC_ARRAY", 'NA' );
	define( "E4S_CHECKTYPE_NUMERIC", E4S_TYPE_SEP . E4S_TYPE_NUMERIC );
	define( "E4S_CHECKTYPE_NUMERIC_ARRAY", E4S_TYPE_SEP . E4S_TYPE_NUMERIC_ARRAY );
	define( "E4S_IN_MINI_CART", 'E4S_IN_MINI_CART' );
	define( "E4S_CAR_REG", 'e4s_car_reg' );
	define( "E4S_CAR_REG_INFO", 'e4s_car_park_info' );
	define( "E4S_CAR_REG_LABEL", 'Registration' );
	define( "E4S_CAR_PARK_TICKET", 'Car Park' );
}
if ( !defined('E4S_CURRENT_DOMAIN') ) {
	define( 'E4S_CURRENT_DOMAIN', strtolower( $_SERVER['SERVER_NAME'] ) );
	define( 'R4S_UK_DOMAIN', 'result4sports.co.uk' );
	define( 'E4S_LIVE_DOMAIN', 'entry4sports.co.uk' );
	define( 'E4S_OLDLIVE_DOMAIN', 'entry4sports.com' );
	define( 'E4S_DEV_DOMAIN', 'dev.' . E4S_LIVE_DOMAIN );
	define( 'E4S_DEMO_DOMAIN', 'demo.' . E4S_OLDLIVE_DOMAIN );
	define( 'E4S_TEST_DOMAIN', 'test.' . E4S_LIVE_DOMAIN );
	define( 'E4S_UAT_DOMAIN', 'uat.' . E4S_LIVE_DOMAIN );
	define( 'E4S_UK_DOMAIN', E4S_LIVE_DOMAIN );
	define( 'E4S_IRE_DOMAIN', 'regional.' . E4S_OLDLIVE_DOMAIN );
	define( 'E4S_AAI_DOMAIN', 'entry.athleticsireland.ie' );
}
$defaultPageSize = 20;
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
include_once 'e4s_routes.php';
$e4s_xss_fields = array();
$GLOBALS['e4s_xss_fields'] = $e4s_xss_fields;

function e4s_rest_UIIssue(){
    exit;
}

add_filter('flush_rewrite_rules_hard', '__return_false');

function e4s_rest_displayLap($obj){
    require_once E4S_FULL_PATH . 'classes/e4sLapCounter.php';
    e4s_displayLapScreen($obj);
}
function e4s_webPageHeader(){
    header('Content-Type: text/html; charset=utf-8');
}
function e4s_rest_AccountsSplitYear($obj){
    include_once E4S_FULL_PATH . 'reports/accounts/ordersbeforetax.php';
    ordersBeforeApril($obj);
}
function e4s_rest_AccountsSplitDate($obj){
	include_once E4S_FULL_PATH . 'reports/accounts/ordersbeforetax.php';
	ordersBeforeDate($obj);
}
function e4s_rest_ReportsFinance2() {
    e4s_webPageHeader();
    include_once E4S_FULL_PATH . 'reports/financeV2/financeReport.php';
    exit();
}

function e4s_rest_ReportsFinance() {
    e4s_webPageHeader();
    include_once E4S_FULL_PATH . 'reports/finance/financeReport.php';
    exit();
}

function e4s_rest_ReportsFinanceCustomers($obj) {
    include_once E4S_FULL_PATH . 'reports/finance/cust.php';
}

function e4s_rest_ReportsFinanceOrganisations($obj) {
    include_once E4S_FULL_PATH . 'reports/finance/organisations.php';
}

function checkFieldForXSS($obj, $param) {
    $fieldArr = explode(':', $param);
    $fieldName = $fieldArr[0];
    $displayField = $fieldName;
    if (sizeof($fieldArr) > 1) {
        $displayField = $fieldArr[1];
    }

    $val = $obj->get_param($fieldName);
    if (is_null($val)) {
        $val = $obj->get_param(strtolower($fieldName));
    }
    $displayField = e4s_checkDataType($displayField, $val);
    if (e4s_getDataType($displayField) === E4S_TYPE_NUMERIC_ARRAY) {
        // numeric array and its been checked so return
        $dataType = getType($val);
        if ($dataType === 'integer') {
            // array required
            $val = [$val];
        }
        return $val;
    }
    return checkValueForXSS($val, $displayField);
}

function checkValueForXSS($val, $displayField) {
    if ($val === null || $val === '') {
        return $val;
    }
    // allow & and double quotes and replace single withback quote
    $validChars = array("'", '&', '"', 'á', 'é', 'í', 'ó', 'ú');
    $useVal = str_replace($validChars, '`', $val);

//    != on purpose. Allow integers to equal string
    if ($useVal != htmlentities($useVal)) {
        $e4s_xss_fields[] = $displayField;
        $GLOBALS['e4s_xss_fields'] = $e4s_xss_fields;
        return htmlentities($useVal);
    }
    if ($val === 'true') {
        $val = TRUE;
    }
    if ($val === 'false') {
        $val = FALSE;
    }
    if (e4s_getDataType($displayField) === E4S_TYPE_NUMERIC) {
        $val = (int)$val;
    }
    return $val;
}

function checkFieldFromParamsForXSS($obj, $param) {
    $fieldArr = explode(':', $param);
    $fieldName = $fieldArr[0];
    $displayField = $fieldArr[1];
    $params = $obj->get_params('JSON');

    if (array_key_exists($fieldName, $params)) {
        $val = $params[$fieldName];
    } else {
        $val = null;
    }

    return checkValueForXSS($val, $displayField);
}

function checkJSONObjForXSS($obj, $param) {
    $fieldArr = explode(':', $param);
    $fieldName = $fieldArr[0];
    $params = $obj->get_params('JSON');

    if (array_key_exists($fieldName, $params)) {
        $val = $params[$fieldName];
    } else {
        $val = null;
    }
    e4s_checkDataType($fieldArr[1], $val);
    return $val;
}

function e4s_checkIsNumeric($value, $displayField) {
    if ($value !== '' and !is_null($value)) {
        $strValue = (int)$value . '';
        if ('' . $value !== '' . (int)$strValue) {
            $arr = preg_split('-' . E4S_TYPE_SEP . '-', $displayField);
            Entry4UIError(9504, 'Incorrect value passed for ' . $arr[0]);
        }
    }
}

function e4s_getDataType($displayField) {
    if (strpos($displayField, E4S_TYPE_SEP) > 0) {
        $arr = preg_split('-' . E4S_TYPE_SEP . '-', $displayField);
        return $arr[1];
    } else {
        return '';
    }
}

function e4s_checkDataType($displayField, $value) {
    $dataType = e4s_getDataType($displayField);
    if ($dataType !== '') {
        switch ($dataType) {
            case E4S_TYPE_NUMERIC:
                // ensure numeric
                e4s_checkIsNumeric($value, $displayField);
                break;
            case E4S_TYPE_NUMERIC_ARRAY:
                if (gettype($value) === 'integer') {
                    // integer passed instead of array
                    e4s_checkIsNumeric($value, $displayField);
                } else {
                    foreach ($value as $arrVal) {
                        e4s_checkIsNumeric($arrVal, $displayField);
                    }
                }
                break;
        }
    }

    return $displayField;
}
function e4s_rest_showESAAScores($obj) {
    include E4S_FULL_PATH . 'results/teamscores/esaa.php';
	e4s_esaaScores($obj);
}
function e4s_rest_markEntriesQualified($obj) {
    include E4S_FULL_PATH . 'competition/commonComp.php';
    e4s_markQualifiedEntries($obj);
}
function e4s_rest_updateCompetitionSettings($obj){
    include E4S_FULL_PATH . 'competition/commonComp.php';
    e4s_updateCompSettings($obj);
}
function e4s_rest_getCompSecurityKey($obj) {
    include E4S_FULL_PATH . 'competition/compStatus.php';
    getCompSecurityCode($obj);
}
function e4s_rest_getCompInfo($obj) {
    include E4S_FULL_PATH . 'competition/commonComp.php';
    e4s_getCompInfo($obj);
}

function e4s_rest_getSchedule($obj) {
    include E4S_FULL_PATH . 'competition/commonComp.php';
    e4s_getSchedule($obj);
}

function e4s_rest_getEventGroupEntries($obj) {
    include E4S_FULL_PATH . 'competition/commonComp.php';
    e4s_getEventGroupEntries($obj, false);
}
function e4s_rest_getEventGroupEntriesIncUnpaid($obj) {
	include E4S_FULL_PATH . 'competition/commonComp.php';
	e4s_getEventGroupEntries($obj, true);
}
function e4s_rest_getAthleteEntries($obj) {
    include E4S_FULL_PATH . 'entries/entries.php';
    e4s_getEntriesForAthlete($obj);
}

function e4s_rest_getCompetitionAges($obj) {
    include E4S_FULL_PATH . 'builder/ageCRUD.php';
    e4s_getCompAges($obj);
}

function e4s_waitingListCheckerUpdate() {
    include_once E4S_FULL_PATH . 'admin/cronFunctions.php';
    e4s_processWaitingLists();
}

function e4s_moveOffWaitingList($obj) {
    include_once E4S_FULL_PATH . 'entries/entries.php';
    e4s_moveEntryOffWaitingList($obj);
}

function e4s_setWaitingPosOnAComps($obj) {
//    defunct
    Entry4UISuccess();
}

// Help
function e4s_rest_GetHelpDoc($obj) {
    include E4S_FULL_PATH . 'builder/helpCRUD.php';
    e4s_actionHelp($obj, E4S_CRUD_READ);
}

function e4s_rest_GetAllHelpDocs($obj) {
    include E4S_FULL_PATH . 'builder/helpCRUD.php';
    e4s_actionHelp($obj, E4S_CRUD_LIST);
}

function e4s_rest_createHelpDoc($obj) {
    include E4S_FULL_PATH . 'builder/helpCRUD.php';
    e4s_actionHelp($obj, E4S_CRUD_CREATE);
}

function e4s_rest_updateHelpDoc($obj) {
    include E4S_FULL_PATH . 'builder/helpCRUD.php';
    e4s_actionHelp($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_deleteHelpDoc($obj)
{
    include E4S_FULL_PATH . 'builder/helpCRUD.php';
    e4s_actionHelp($obj, E4S_CRUD_DELETE);
}
function e4s_rest_updateEntryTeamBibNo($obj) {
    include E4S_FULL_PATH . 'entries/entries.php';
    e4s_updateEntryTeamBibNo($obj);
    Entry4UISuccess();
}

function e4s_rest_updateEntryTeamBibNos($obj){
    include E4S_FULL_PATH . 'entries/entries.php';
    e4s_updateEntryTeamBibNos($obj);
    exit;
}
// Competition Check List
function e4s_rest_moveCompetitionStatus($obj) {
    include E4S_FULL_PATH . 'competition/compStatus.php';
    moveToStatus($obj);
    exit();
}

function e4s_rest_checkCompetition($obj) {
    e4s_webPageHeader();
    $compid = (int)checkFieldFromParamsForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    require_once E4S_FULL_PATH . 'competition/public/checklist.php';
}

function e4s_rest_updateSchoolTeam($obj) {
    $process = E4S_CRUD_UPDATE;
    e4s_rest_processSchoolTeam($obj, $process);
}

function e4s_rest_createSchoolTeam($obj) {
    e4s_rest_processSchoolTeam($obj, E4S_CRUD_CREATE);
}

function e4s_rest_updateLeagueTeam($obj) {
    e4s_rest_processLeagueTeam($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_createLeagueTeam($obj) {
    $process = E4S_CRUD_CREATE;
    e4s_rest_processLeagueTeam($obj, $process);
}

function e4s_rest_requestSelfService($obj) {
    require_once E4S_FULL_PATH . 'eventteams/selfService.php';
    e4s_requestSelfService($obj);
}

function e4s_rest_authoriseSelfService($obj) {
    require_once E4S_FULL_PATH . 'eventteams/selfService.php';
    e4s_authoriseSelfService($obj);
}

function e4s_rest_processLeagueTeam($obj, $process) {
    $public = FALSE;
    /*
     * {"id":0,"ceid":0,"teamName":"wefdew","athletes":[{"id":1,"consent":true,"name":"Fred Bloggs"],"paid":0,"prodId":0}
    */
    $id = (int)checkFieldFromParamsForXSS($obj, 'id:Team id');
    $ceid = (int)checkFieldFromParamsForXSS($obj, 'ceid:Competition event id');
    $teamname = checkFieldFromParamsForXSS($obj, 'teamName:Team name');
    $params = $obj->get_params('JSON');
    $formrows = $params['formRows'];
    $type = 'LEAGUE';
    include E4S_FULL_PATH . 'eventteams/eventTeamCRUD.php';
}

function e4s_rest_processSchoolTeam($obj, $process) {
    $public = FALSE;
    /*
     * {"id":0,"ceid":0,"teamName":"wefdew","athletes":[{"id":1,"consent":true,"name":"Fred Bloggs"],"paid":0,"prodId":0}
    */ //    $id = (int)checkFieldFromParamsForXSS($obj, 'id:Team id');
//    $ceid = (int)checkFieldFromParamsForXSS($obj, 'ceid:Competition event id');
//    $teamname = checkFieldFromParamsForXSS($obj, 'teamName:Team name');
//    $params = $obj->get_params("JSON");
//    $athletes = $params['athletes'];
//    $type = "SCHOOL";
    include E4S_FULL_PATH . 'eventteams/eventTeamCRUD.php';
    e4s_processTeam($process, $obj, E4S_TEAM_TYPE_SCHOOL);
}

function e4s_rest_createEventTeam($obj) {
    include E4S_FULL_PATH . 'eventteams/eventTeamCRUD.php';
    e4s_processTeam(E4S_CRUD_CREATE, $obj);
}

function e4s_rest_updateEventTeam($obj) {
    include E4S_FULL_PATH . 'eventteams/eventTeamCRUD.php';
    e4s_processTeam(E4S_CRUD_UPDATE, $obj);
}

function e4s_rest_deleteEventTeam($obj) {
    include E4S_FULL_PATH . 'eventteams/eventTeamCRUD.php';
    e4s_processTeam(E4S_CRUD_DELETE, $obj);
}

function e4s_rest_listEventTeams($obj) {
    include E4S_FULL_PATH . 'eventteams/eventTeamCRUD.php';
    e4s_processTeam(E4S_CRUD_LIST, $obj);
}

// Orgs/CompClubs CRUD
function e4s_rest_builderReadOrg($obj) {
    include E4S_FULL_PATH . 'builder/compClubCRUD.php';
    e4s_actionCompClub($obj, E4S_CRUD_READ);
}
function e4s_rest_builderReadAdminOrg($obj) {
    include E4S_FULL_PATH . 'builder/compClubCRUD.php';
    e4s_actionCompClub($obj, E4S_CRUD_READ, true);
}

function e4s_rest_builderCreateOrg($obj) {
    include E4S_FULL_PATH . 'builder/compClubCRUD.php';
    e4s_actionCompClub($obj, E4S_CRUD_CREATE);
}
function e4s_rest_approveOrg($obj){
    include E4S_FULL_PATH . 'builder/compClubCRUD.php';
	e4s_actionCompClub($obj, E4S_CRUD_UPDATE . "Approve");
}
function e4s_rest_builderUpdateOrg($obj) {
    include E4S_FULL_PATH . 'builder/compClubCRUD.php';
    e4s_actionCompClub($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderDeleteOrg($obj) {
    include E4S_FULL_PATH . 'builder/compClubCRUD.php';
    e4s_actionCompClub($obj, E4S_CRUD_DELETE);
}

function e4s_rest_ListMyOrgs($obj) {
	include E4S_FULL_PATH . 'builder/compClubCRUD.php';
	e4s_actionCompClub($obj, E4S_CRUD_LIST . "My");
}

function e4s_rest_builderListOrgs($obj) {
    include E4S_FULL_PATH . 'builder/compClubCRUD.php';
    e4s_actionCompClub($obj, E4S_CRUD_LIST);
}
// Event Group CRUD
function e4s_rest_mergeEGs($obj){
    include E4S_FULL_PATH . 'builder/eventGroupCRUD.php';
    e4s_mergeEGs($obj);
}
function e4s_rest_builderListEGs($obj) {
    include E4S_FULL_PATH . 'builder/eventGroupCRUD.php';
    e4s_actionEG($obj, E4S_CRUD_LIST);
}

function e4s_rest_clearEventTimes($obj){
    include E4S_FULL_PATH . 'builder/eventGroupCRUD.php';
    e4s_clearEventGroupTimes($obj);
}
function e4s_rest_builderUpdateEG($obj) {
    include E4S_FULL_PATH . 'builder/eventGroupCRUD.php';
    e4s_updateEventGroup($obj);
}
function e4s_rest_deleteEventGroup($obj){
    include E4S_FULL_PATH . 'builder/eventGroupCRUD.php';
    e4s_deleteEventGroup($obj);
}
function e4s_rest_updateCompNotes($obj){
    include E4S_FULL_PATH . 'dbInfo.php';
    e4s_updateBibCompNotes($obj);
}
function e4s_rest_updateEGNotes($obj) {
    include E4S_FULL_PATH . 'builder/eventGroupCRUD.php';
    e4s_updateEgNotes($obj);
}
function e4s_rest_createPhotofinish($obj){
    include E4S_FULL_PATH . 'results/webresults.php';
    e4s_createPhotofinish($obj);
}
function e4s_rest_getPhotofinish($obj){
    include E4S_FULL_PATH . 'results/webresults.php';
    e4s_getPhotofinishFile($obj);
}
function e4s_rest_photofinishResponse($obj){
    include E4S_FULL_PATH . 'results/webresults.php';
    e4s_sendPhotofinishResponse($obj);
}
function e4s_rest_updateEGResultsPossible($obj) {
    include E4S_FULL_PATH . 'builder/eventGroupCRUD.php';
    e4s_updateResultsPossible($obj);
}
function e4s_rest_builderUpdateEGs($obj) {
    include E4S_FULL_PATH . 'builder/eventGroupCRUD.php';
    e4s_actionEG($obj, E4S_CRUD_UPDATE);
}

// Competition CRUD
function e4s_rest_getCompEmails($obj) {
    include E4S_FULL_PATH . 'builder/compCRUD.php';
    e4s_getCompEmails($obj);
}

function e4s_rest_contactOrganiser($obj) {
    include E4S_FULL_PATH . 'competition/contactOrganiser.php';
    e4s_contactOrganiser($obj);
}

function e4s_rest_builderReadComp($obj) {
    include E4S_FULL_PATH . 'builder/compCRUD.php';
    e4s_actionComp($obj, E4S_CRUD_READ);
}

function e4s_rest_builderCreateComp($obj) {
    include E4S_FULL_PATH . 'builder/compCRUD.php';
    e4s_actionComp($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderUpdateComp($obj) {
    include E4S_FULL_PATH . 'builder/compCRUD.php';
    e4s_actionComp($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderDeleteComp($obj) {
    include E4S_FULL_PATH . 'builder/compCRUD.php';
    e4s_actionComp($obj, E4S_CRUD_DELETE);
}

function e4s_rest_builderListComps($obj) {
    include E4S_FULL_PATH . 'builder/compCRUD.php';
    e4s_actionComp($obj, E4S_CRUD_LIST);
}

// CompEvents CRUD
function e4s_rest_resetCETimes($obj) {
    include E4S_FULL_PATH . 'builder/compEventCRUD.php';
    e4s_actionCE($obj, 'RESET_TIMES');
}

function e4s_rest_builderReadCE($obj) {
    include E4S_FULL_PATH . 'builder/compEventCRUD.php';
    e4s_actionCE($obj, E4S_CRUD_READ);
}

function e4s_rest_builderCreateCE($obj) {
    include E4S_FULL_PATH . 'builder/compEventCRUD.php';
    e4s_actionCE($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderUpdateCE($obj) {
    include E4S_FULL_PATH . 'builder/compEventCRUD.php';
    e4s_actionCE($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderBulkupdate($obj) {
    include E4S_FULL_PATH . 'builder/compEventArrCRUD.php';
    e4s_actionCEArr($obj, 'BULK_UPDATE');
}

function e4s_rest_builderDeleteCEs($obj) {
    include E4S_FULL_PATH . 'builder/compEventArrCRUD.php';
    e4s_actionCEArr($obj, E4S_CRUD_DELETE);
}

function e4s_rest_builderDeleteCE($obj) {
    include E4S_FULL_PATH . 'builder/compEventCRUD.php';
    e4s_actionCE($obj, E4S_CRUD_DELETE);
}

function e4s_rest_builderListCEs($obj) {
    include E4S_FULL_PATH . 'builder/compEventCRUD.php';
    e4s_actionCE($obj, E4S_CRUD_LIST);
}

// Price CRUD
function e4s_rest_builderReadPrice($obj) {
    include E4S_FULL_PATH . 'builder/priceCRUD.php';
    e4s_actionPrice($obj, E4S_CRUD_READ);
}

function e4s_rest_builderCreatePrice($obj) {
    include E4S_FULL_PATH . 'builder/priceCRUD.php';
    e4s_actionPrice($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderUpdatePrice($obj) {
    include E4S_FULL_PATH . 'builder/priceCRUD.php';
    e4s_actionPrice($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderUpdatePrices($obj) {
    include E4S_FULL_PATH . 'builder/priceCRUD.php';
    e4s_actionPrice($obj, E4S_CRUD_UPDATE . 'Multi');
}

function e4s_rest_builderDeletePrice($obj) {
    include E4S_FULL_PATH . 'builder/priceCRUD.php';
    e4s_actionPrice($obj, E4S_CRUD_DELETE);
}

function e4s_rest_builderListPrices($obj) {
    include E4S_FULL_PATH . 'builder/priceCRUD.php';
    e4s_actionPrice($obj, E4S_CRUD_LIST);
}

// Discount CRUD
function e4s_rest_builderReadDisc($obj) {
    include E4S_FULL_PATH . 'builder/discCRUD.php';
    e4s_actionDisc($obj, E4S_CRUD_READ);
}

function e4s_rest_builderCreateDisc($obj) {
    include E4S_FULL_PATH . 'builder/discCRUD.php';
    e4s_actionDisc($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderUpdateDisc($obj) {
    include E4S_FULL_PATH . 'builder/discCRUD.php';
    e4s_actionDisc($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderDeleteDisc($obj) {
    include E4S_FULL_PATH . 'builder/discCRUD.php';
    e4s_actionDisc($obj, E4S_CRUD_DELETE);
}

function e4s_rest_builderListDiscs($obj) {
    include E4S_FULL_PATH . 'builder/discCRUD.php';
    e4s_actionDisc($obj, E4S_CRUD_LIST);
}

// CompRules CRUD
function e4s_rest_builderReadCompRule($obj) {
    include E4S_FULL_PATH . 'builder/compRuleCRUD.php';
    e4s_actionCompRule($obj, E4S_CRUD_READ);
}

function e4s_rest_builderCreateCompRule($obj) {
    include E4S_FULL_PATH . 'builder/compRuleCRUD.php';
    e4s_actionCompRule($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderUpdateCompRule($obj) {
    include E4S_FULL_PATH . 'builder/compRuleCRUD.php';
    e4s_actionCompRule($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderDeleteCompRule($obj) {
    include E4S_FULL_PATH . 'builder/compRuleCRUD.php';
    e4s_actionCompRule($obj, E4S_CRUD_DELETE);
}

function e4s_rest_builderListCompRules($obj) {
    include E4S_FULL_PATH . 'builder/compRuleCRUD.php';
    e4s_actionCompRule($obj, E4S_CRUD_LIST);
}

// Locations CRUD
function e4s_rest_builderReadLoc($obj) {
    include E4S_FULL_PATH . 'builder/locationCRUD.php';
    e4s_actionLocation($obj, E4S_CRUD_READ);
}

function e4s_rest_builderCreateLoc($obj) {
    include E4S_FULL_PATH . 'builder/locationCRUD.php';
    e4s_actionLocation($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderUpdateLoc($obj) {
    include E4S_FULL_PATH . 'builder/locationCRUD.php';
    e4s_actionLocation($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderDeleteLoc($obj) {
    include E4S_FULL_PATH . 'builder/locationCRUD.php';
    e4s_actionLocation($obj, E4S_CRUD_DELETE);
}

function e4s_rest_builderListLocs($obj) {
    include E4S_FULL_PATH . 'builder/locationCRUD.php';
    e4s_actionLocation($obj, E4S_CRUD_LIST);
}

// Secondary CRUD
function e4s_rest_builderReadSecondary($obj) {
    include E4S_FULL_PATH . 'builder/secondaryCRUD.php';
    e4s_actionSecondary($obj, E4S_CRUD_READ);
}

function e4s_rest_builderCreateSecondary($obj) {
    include E4S_FULL_PATH . 'builder/secondaryCRUD.php';
    e4s_actionSecondary($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderUpdateSecondary($obj) {
    include E4S_FULL_PATH . 'builder/secondaryCRUD.php';
    e4s_actionSecondary($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderDeleteSecondary($obj) {
    include E4S_FULL_PATH . 'builder/secondaryCRUD.php';
    e4s_actionSecondary($obj, E4S_CRUD_DELETE);
}

function e4s_rest_builderListSecondary($obj) {
    include E4S_FULL_PATH . 'builder/secondaryCRUD.php';
    e4s_actionSecondary($obj, E4S_CRUD_LIST);
}

function e4s_rest_builderListSecondaryCust($obj) {
    include E4S_FULL_PATH . 'builder/secondaryCRUD.php';
    e4s_actionSecondary($obj, E4S_CRUD_LIST . E4S_SECUST_LIST);
}

function e4s_rest_purchaseSecondary($obj) {
    include E4S_FULL_PATH . 'builder/secondaryCRUD.php';
    e4s_purchaseProduct($obj);
}

function e4s_rest_getSecondaryPurchases($obj) {
    include E4S_FULL_PATH . 'builder/secondaryCRUD.php';
    e4s_getSecondaryPurchases($obj);
}

function e4s_rest_getSecondaryAthletePurchases($obj) {
    include E4S_FULL_PATH . 'builder/secondaryCRUD.php';
    e4s_getSecondaryAthletePurchases($obj);
}

// Secondary Variation CRUD
function e4s_rest_builderReadSecondaryVariation($obj) {
    include E4S_FULL_PATH . 'builder/secondaryCRUD.php';
    e4s_actionSecondaryVariation($obj, E4S_CRUD_READ);
}

function e4s_rest_builderCreateSecondaryVariation($obj) {
    include E4S_FULL_PATH . 'builder/secondaryCRUD.php';
    e4s_actionSecondaryVariation($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderUpdateSecondaryVariation($obj) {
    include E4S_FULL_PATH . 'builder/secondaryCRUD.php';
    e4s_actionSecondaryVariation($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderDeleteSecondaryVariation($obj) {
    include E4S_FULL_PATH . 'builder/secondaryCRUD.php';
    e4s_actionSecondaryVariation($obj, E4S_CRUD_DELETE);
}

// R4S Design CRUD
function e4s_rest_builderReadR4SDesign($obj) {
    include E4S_FULL_PATH . 'builder/r4sDesignCRUD.php';
    e4s_R4SDesignCRUD($obj, E4S_CRUD_READ);
}

function e4s_rest_builderCreateR4SDesign($obj) {
    include E4S_FULL_PATH . 'builder/r4sDesignCRUD.php';
    e4s_R4SDesignCRUD($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderUpdateR4SDesign($obj) {
    include E4S_FULL_PATH . 'builder/r4sDesignCRUD.php';
    e4s_R4SDesignCRUD($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderDeleteR4SDesign($obj) {
    include E4S_FULL_PATH . 'builder/r4sDesignCRUD.php';
    e4s_R4SDesignCRUD($obj, E4S_CRUD_DELETE);
}

function e4s_rest_builderListR4SDesigns($obj) {
    include E4S_FULL_PATH . 'builder/r4sDesignCRUD.php';
    e4s_R4SDesignCRUD($obj, E4S_CRUD_LIST);
}

// R4S Output CRUD
function e4s_rest_builderReadR4SOutput($obj) {
    include E4S_FULL_PATH . 'builder/r4sOutputCRUD.php';
    e4s_R4SOutputCRUD($obj, E4S_CRUD_READ);
}

function e4s_rest_builderCreateR4SOutput($obj) {
    include E4S_FULL_PATH . 'builder/r4sOutputCRUD.php';
    e4s_R4SOutputCRUD($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderUpdateR4SOutput($obj) {
    include E4S_FULL_PATH . 'builder/r4sOutputCRUD.php';
    e4s_R4SOutputCRUD($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderDeleteR4SOutput($obj) {
    include E4S_FULL_PATH . 'builder/r4sOutputCRUD.php';
    e4s_R4SOutputCRUD($obj, E4S_CRUD_DELETE);
}

function e4s_rest_builderListR4SOutputs($obj) {
    include E4S_FULL_PATH . 'builder/r4sOutputCRUD.php';
    e4s_R4SOutputCRUD($obj, E4S_CRUD_LIST);
}

// R4S Schedule CRUD
function e4s_rest_builderReadR4SSchedule($obj) {
    include E4S_FULL_PATH . 'builder/r4sScheduleCRUD.php';
    e4s_R4SScheduleCRUD($obj, E4S_CRUD_READ);
}

function e4s_rest_builderCreateR4SSchedule($obj) {
    include E4S_FULL_PATH . 'builder/r4sScheduleCRUD.php';
    e4s_R4SScheduleCRUD($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderUpdateR4SSchedule($obj) {
    include E4S_FULL_PATH . 'builder/r4sScheduleCRUD.php';
    e4s_R4SScheduleCRUD($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderDeleteR4SSchedule($obj) {
    include E4S_FULL_PATH . 'builder/r4sScheduleCRUD.php';
    e4s_R4SScheduleCRUD($obj, E4S_CRUD_DELETE);
}

function e4s_rest_builderListR4SSchedules($obj) {
    include E4S_FULL_PATH . 'builder/r4sScheduleCRUD.php';
    e4s_R4SScheduleCRUD($obj, E4S_CRUD_LIST);
}

// R4S Schedule Event CRUD
//function e4s_rest_builderReadR4SScheduleEvent( $obj ){
//    include E4S_FULL_PATH .'/builder/r4sScheduleEventCRUD.php';
//    e4s_R4SScheduleCRUD($obj, E4S_CRUD_READ);
//}
function e4s_rest_builderCreateR4SScheduleEvent($obj) {
    include E4S_FULL_PATH . 'builder/r4sScheduleEventCRUD.php';
    e4s_R4SScheduleEventCRUD($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderDeleteR4SScheduleEvent($obj) {
    include E4S_FULL_PATH . 'builder/r4sScheduleEventCRUD.php';
    e4s_R4SScheduleEventCRUD($obj, E4S_CRUD_DELETE);
}
// Message CRUD
const E4S_MESSAGE_TYPE_EMAIL = 'E';
const E4S_MESSAGE_TYPE_MSG = 'M';
function e4s_rest_builderReadMessage($obj) {
    include E4S_FULL_PATH . 'builder/messageCRUD.php';
    e4s_actionMessage($obj, E4S_CRUD_READ);
}

function e4s_rest_builderUnReadMessage($obj) {
    include E4S_FULL_PATH . 'builder/messageCRUD.php';
    e4s_actionMessage($obj, E4S_CRUD_UPDATE, 'UNREAD');
}

function e4s_rest_builderReadMessages($obj) {
    include E4S_FULL_PATH . 'builder/messageCRUD.php';
    e4s_actionMessage($obj, E4S_CRUD_LIST);
}

function e4s_rest_builderCreateMessage($obj) {
    include E4S_FULL_PATH . 'builder/messageCRUD.php';
    e4s_actionMessage($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderMarkMessageDeleted($obj) {
    include E4S_FULL_PATH . 'builder/messageCRUD.php';
    e4s_actionMessage($obj, E4S_CRUD_UPDATE, 'DELETE');
}

function e4s_rest_builderMarkMessageUnDeleted($obj) {
    include E4S_FULL_PATH . 'builder/messageCRUD.php';
    e4s_actionMessage($obj, E4S_CRUD_UPDATE, 'UNDELETE');
}

// Area CRUD
function e4s_rest_builderReadArea($obj) {
    include E4S_FULL_PATH . 'builder/areaCRUD.php';
    e4s_actionArea($obj, E4S_CRUD_READ);
}

function e4s_rest_builderCreateArea($obj) {
    include E4S_FULL_PATH . 'builder/areaCRUD.php';
    e4s_actionArea($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderUpdateArea($obj) {
    include E4S_FULL_PATH . 'builder/areaCRUD.php';
    e4s_actionArea($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderDeleteArea($obj) {
    include E4S_FULL_PATH . 'builder/areaCRUD.php';
    e4s_actionArea($obj, E4S_CRUD_DELETE);
}

function e4s_rest_builderListAreas($obj) {
    include E4S_FULL_PATH . 'builder/areaCRUD.php';
    e4s_actionArea($obj, E4S_CRUD_LIST);
}

// Club CRUD
function e4s_rest_builderReadSchool($obj) {
    include E4S_FULL_PATH . 'builder/clubCRUD.php';
    e4s_actionClub($obj, E4S_CRUD_READ, true);
}
function e4s_rest_builderReadClub($obj) {
    include E4S_FULL_PATH . 'builder/clubCRUD.php';
    e4s_actionClub($obj, E4S_CRUD_READ);
}
function e4s_rest_builderCreateSchool($obj) {
    include E4S_FULL_PATH . 'builder/clubCRUD.php';
    e4s_actionClub($obj, E4S_CRUD_CREATE, true);
}
function e4s_rest_builderCreateClub($obj) {
    include E4S_FULL_PATH . 'builder/clubCRUD.php';
    e4s_actionClub($obj, E4S_CRUD_CREATE);
}
function e4s_rest_builderUpdateSchool($obj) {
    include E4S_FULL_PATH . 'builder/clubCRUD.php';
    e4s_actionClub($obj, E4S_CRUD_UPDATE, true);
}
function e4s_rest_builderUpdateClub($obj) {
    include E4S_FULL_PATH . 'builder/clubCRUD.php';
    e4s_actionClub($obj, E4S_CRUD_UPDATE);
}
function e4s_rest_builderDeleteSchool($obj) {
    include E4S_FULL_PATH . 'builder/clubCRUD.php';
    e4s_actionClub($obj, E4S_CRUD_DELETE, true);
}
function e4s_rest_builderDeleteClub($obj) {
    include E4S_FULL_PATH . 'builder/clubCRUD.php';
    e4s_actionClub($obj, E4S_CRUD_DELETE);
}
function e4s_rest_builderListClubs($obj) {
    include E4S_FULL_PATH . 'builder/clubCRUD.php';
    e4s_actionClub($obj, E4S_CRUD_LIST);
}
function e4s_rest_builderListSchools($obj) {
    include E4S_FULL_PATH . 'builder/clubCRUD.php';
    e4s_actionClub($obj, E4S_CRUD_LIST, true);
}

// AgeGroup CRUD
function e4s_rest_builderReadAge($obj) {
    include E4S_FULL_PATH . 'builder/ageCRUD.php';
    e4s_actionAge($obj, E4S_CRUD_READ);
}

function e4s_rest_builderCreateAge($obj) {
    include E4S_FULL_PATH . 'builder/ageCRUD.php';
    e4s_actionAge($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderUpdateAge($obj) {
    include E4S_FULL_PATH . 'builder/ageCRUD.php';
    e4s_actionAge($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderDeleteAge($obj) {
    include E4S_FULL_PATH . 'builder/ageCRUD.php';
    e4s_actionAge($obj, E4S_CRUD_DELETE);
}

function e4s_rest_builderListAges($obj) {
    include E4S_FULL_PATH . 'builder/ageCRUD.php';
    e4s_actionAge($obj, E4S_CRUD_LIST);
}

function e4s_rest_builderListDefaultAges($obj) {
    include E4S_FULL_PATH . 'builder/ageCRUD.php';
    e4s_actionAge($obj, E4S_CRUD_LIST . 'Def');
}

function e4s_rest_openAnnouncer($obj) {
    include E4S_FULL_PATH . 'otd/tfAnnouncements.php';
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $obj = new tfAnnouncementsClass($compid);
    $obj->player();
    exit();
}

// Event CRUD
function e4s_rest_builderReadEvent($obj) {
    include E4S_FULL_PATH . 'builder/eventCRUD.php';
    e4s_actionEvent($obj, E4S_CRUD_READ);
}

function e4s_rest_builderUpdateUniqueEvents($obj) {
    include E4S_FULL_PATH . 'builder/eventCRUD.php';
    e4s_updateEventsWithUniqueCeIds($obj);
}

function e4s_rest_builderCreateEvent($obj) {
    include E4S_FULL_PATH . 'builder/eventCRUD.php';
    e4s_actionEvent($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderUpdateEvent($obj) {
    include E4S_FULL_PATH . 'builder/eventCRUD.php';
    e4s_actionEvent($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderDeleteEvent($obj) {
    include E4S_FULL_PATH . 'builder/eventCRUD.php';
    e4s_actionEvent($obj, E4S_CRUD_DELETE);
}

function e4s_rest_builderListEvent($obj) {
    include E4S_FULL_PATH . 'builder/eventCRUD.php';
    e4s_actionEvent($obj, E4S_CRUD_LIST);
}

function e4s_rest_addAthleteToEvent($obj) {
    include E4S_OTD_PATH . '/tfCards.php';
    e4s_addAthleteToEvent($obj);
}
function e4s_rest_updateEGMaxInHeat($obj){
	include E4S_FULL_PATH . 'builder/eventGroupCRUD.php';
	e4s_updateMaxInHeatFromCard($obj);
}
function e4s_rest_updateEGFromCard($obj) {
    include E4S_FULL_PATH . 'builder/eventGroupCRUD.php';
    e4s_updateLimitsFromCard($obj);
}
function e4s_rest_myEntries($obj){
    e4s_webPageHeader();
    include E4S_FULL_PATH . 'showEntries/myEntries.php';
    e4s_myEntriesReport($obj);
}
function e4s_rest_moveEntriesVertical($obj){
    include E4S_FULL_PATH . 'entries/entries.php';
    e4s_moveEntriesVertical($obj);
}
function e4s_rest_moveEntriesHorizontal($obj){
    include E4S_FULL_PATH . 'entries/entries.php';
    e4s_moveEntriesHorizontal($obj);
}
function e4s_rest_addIncludedEntries($obj){
    include E4S_FULL_PATH . 'entries/entries.php';
    e4s_addIncludedEntries($obj);
}
function e4s_rest_createEntriesForEG($obj) {
    include E4S_FULL_PATH . 'entries/entries.php';
    e4s_createEntriesForEg($obj);
}

function e4s_rest_cardEntries($obj) {
    include E4S_OTD_PATH . '/tfCards.php';
    e4s_getEntriesForEventGroupId($obj);
}

function e4s_rest_cardEntryArray($obj) {
    include E4S_OTD_PATH . '/tfCards.php';
    e4s_getEntriesForEventGroupIdArray($obj);
}

function e4s_rest_checkPriority($obj) {
    include E4S_FULL_PATH . 'builder/compCRUD.php';
    e4s_checkPriority($obj);
}

function e4s_rest_builderListEventDefs($obj) {
    include E4S_FULL_PATH . 'builder/eventCRUD.php';
    e4s_actionEvent($obj, E4S_CRUD_LIST . 'defs');
}

// Athlete CRUD
function e4s_rest_builderReadAthlete($obj) {
    include E4S_FULL_PATH . 'builder/athleteCRUD.php';
    e4s_actionAthlete($obj, E4S_CRUD_READ);
}

function e4s_rest_builderCreateAthlete($obj) {
    include E4S_FULL_PATH . 'builder/athleteCRUD.php';
    e4s_actionAthlete($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderUpdateAthlete($obj) {
    include E4S_FULL_PATH . 'builder/athleteCRUD.php';
    e4s_actionAthlete($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderDeleteAthlete($obj) {
    include E4S_FULL_PATH . 'builder/athleteCRUD.php';
    e4s_actionAthlete($obj, E4S_CRUD_DELETE);
}

function e4s_rest_builderListAthlete($obj) {
    include E4S_FULL_PATH . 'builder/athleteCRUD.php';
    e4s_actionAthlete($obj, E4S_CRUD_LIST);
}

// UOM CRUD
function e4s_rest_builderReadUOM($obj) {
    include E4S_FULL_PATH . 'builder/uomCRUD.php';
    e4s_actionUOM($obj, E4S_CRUD_READ);
}

function e4s_rest_builderCreateUOM($obj) {
    include E4S_FULL_PATH . 'builder/uomCRUD.php';
    e4s_actionUOM($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderUpdateUOM($obj) {
    include E4S_FULL_PATH . 'builder/uomCRUD.php';
    e4s_actionUOM($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderDeleteUOM($obj) {
    include E4S_FULL_PATH . 'builder/uomCRUD.php';
    e4s_actionUOM($obj, E4S_CRUD_DELETE);
}

function e4s_rest_builderListUOM($obj) {
    include E4S_FULL_PATH . 'builder/uomCRUD.php';
    e4s_actionUOM($obj, E4S_CRUD_LIST);
}

function e4s_rest_getOrder($obj) {
    include_once E4S_FULL_PATH . 'dbFix/getOrderInfo.php';
    e4s_getOrderInfo($obj);
}
function e4s_rest_checkEAAwards($obj) {
    e4s_webPageHeader();
    require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
    include_once E4S_FULL_PATH . 'dbInfo.php';
    include_once E4S_FULL_PATH . 'classes/eaAwards.php';
    $eaAwards = new eaAwards();
    $eaAwards->check();
}
// User
// set Credit
function e4s_rest_setUserCredit($obj) {
    include E4S_FULL_PATH . 'admin/userMaint.php';
    e4s_updateCredit($obj, FALSE);
}

// update Credit
function e4s_rest_updateUserCredit($obj) {
    include E4S_FULL_PATH . 'admin/userMaint.php';
    e4s_updateCredit($obj, TRUE);
}

// Permissions
// Create or update

function e4s_rest_setUserVersion ($obj){
    include E4S_FULL_PATH . 'admin/userObj.php';

    $version = checkFieldForXSS($obj, 'version:User Version');
    $toggle = checkFieldForXSS($obj, 'toggle:Toggle Version' . E4S_CHECKTYPE_NUMERIC);
    $userId = checkFieldForXSS($obj, 'userId:For User' . E4S_CHECKTYPE_NUMERIC);

    e4s_setVersion($version, $toggle, $userId);
    Entry4UISuccess();
}
function e4s_rest_addOrgToUser($obj){
	include E4S_FULL_PATH . 'builder/permCRUD.php';
	e4s_addUserToOrg($obj);
}
function e4s_rest_builderPermissionModify($obj) {
    include E4S_FULL_PATH . 'builder/permCRUD.php';
    updatePermissionID($obj);
}
// GET
function e4s_rest_getPermissions($obj) {
    include E4S_FULL_PATH . 'builder/permCRUD.php';
    e4s_returnUserPermissions($obj);
}

// Delete
function e4s_rest_builderPermissionDelete($obj) {
    include E4S_FULL_PATH . 'builder/permCRUD.php';
    deletePermission($obj);
}

function e4s_rest_viewfile($obj) {
    $public = FALSE;
    $attachment_id = checkFieldForXSS($obj, 'fileid:File ID');
    $code = checkFieldForXSS($obj, 'code:File ID');
    include E4S_FULL_PATH . 'downloads/viewfile.php';
    exit();
}

function e4s_rest_upload($obj) {
    $public = FALSE;
    $private = checkFieldForXSS($obj, 'private:Private Key');
    include E4S_FULL_PATH . 'downloads/upload.php';
    exit();
}

function e4s_rest_dedup($obj) {
    e4s_webPageHeader();
    include_once E4S_FULL_PATH . 'classes/deDupClass.php';
    e4s_dedupAthletes();
}

function e4s_rest_e4s($obj) {
    e4s_webPageHeader();
    include E4S_FULL_PATH . 'admin/e4s.php';
    e4s_process($obj);
}

function e4s_rest_e4scron($obj) {
//    e4s_webPageHeader();
    include E4S_FULL_PATH . 'admin/cronFunctions.php';
    e4s_processCron($obj);
}

function e4s_rest_checkCompPof10($obj){
	$compId = checkFieldForXSS($obj, 'compid:Comp Id');
	$egId = checkFieldForXSS($obj, 'egid:Event Group Id');
    if ( !is_numeric($egId) ){
        $egId = 0;
    }else{
        $egId = (int)$egId;
    }
    if ( is_numeric($compId) ){
        include E4S_FULL_PATH . 'competition/commonComp.php';
	    e4s_checkCompEntriesPof10($compId, $egId);
    }
    Entry4UISuccess();
}
function e4s_rest_checkPof10ForAthletes($obj){
	$URNs = checkFieldForXSS($obj, 'urns:Athlete URNs');
    if ( !is_null($URNs) ){
	    $URNs = explode('~', $URNs);
        include E4S_FULL_PATH . 'dbInfo.php';
	    e4s_updatePof10ForAthletes($URNs);
    }
    Entry4UISuccess();
}
function e4s_rest_checkPof10($obj){
	$userId = checkFieldForXSS($obj, 'userid:User Id');
	$force = checkFieldForXSS($obj, 'force:Force update');
    if ( !is_null($force) ) {
        if ( $force === true or $force === 1 or $force === 'true' or $force === '1' ){
            $force = true;
        } else {
            $force = false;
        }
    }else{
        $force = false;
    }
    if ( is_numeric($userId) ){
        if ( E4S_CURRENT_DOMAIN !== E4S_AAI_DOMAIN) {
	        include E4S_FULL_PATH . 'athlete/userAthletes.php';
	        e4s_checkUserAthletesPof10( $userId, E4S_AOCODE_EA, $force );
        }
    }
    Entry4UISuccess();
}
function e4s_rest_getRosterInfo($obj){
	include E4S_FULL_PATH . 'scoreboards/rostercommon.php';
	getRosterInfo($obj);
}
function e4s_rest_test($obj) {
    e4s_webPageHeader();
    include E4S_FULL_PATH . 'test/test.php';
}

function e4s_rest_test2($obj) {
    e4s_webPageHeader();
    $public = TRUE;
    include E4S_FULL_PATH . 'test/test2.php';
    exit();
}

//function getAndCheckField($obj, )
function e4s_rest_adminRefunds($obj) {
    include E4S_FULL_PATH . 'admin/refunds.php';
    e4s_refund($obj);
}
function e4s_rest_userRefunds($obj) {
    include E4S_FULL_PATH . 'admin/refunds.php';
    e4s_refund($obj);
}

function e4s_rest_getAdminChequeOrders($obj) {
    $public = FALSE;
    include E4S_FULL_PATH . 'cart/orders.php';
    getOnHoldOrders();
    exit();
}

function e4s_rest_setChequeOrders($obj) {
    $public = FALSE;
    include E4S_FULL_PATH . 'cart/orders.php';
    updateOrder($obj);
    exit();
}

function e4s_rest_setLocalHost($obj) {
    include E4S_FULL_PATH . 'admin/e4sConfig.php';
    e4s_setLocalHostAdmin($obj);
}

function e4s_rest_getAdminConfig($obj) {
    include E4S_FULL_PATH . 'admin/e4sConfig.php';
    e4s_restGetConfig();
}

function e4s_rest_getPublicConfig($obj) {
    include E4S_FULL_PATH . 'admin/e4sConfig.php';
    e4s_restGetConfig();
}

function e4s_rest_CompMoreInfo($obj) {
    $public = TRUE;
    include E4S_FULL_PATH . 'competition/commonComp.php';
    e4s_getCompMoreInfo($obj);
}

function e4s_rest_copyClubCompDef($obj) {
    include E4S_FULL_PATH . 'builder/clubCompCRUD.php';
    e4s_copyClubCompInfo($obj);
}
function e4s_rest_getFilteredCompsLite($obj) {
    $public = TRUE;
    include E4S_FULL_PATH . 'competition/public/getAllCompInfoV2.php';
    e4s_getLiteCompsForFilter($obj);
}
function e4s_rest_getHomePageComps($obj) {
    $public = TRUE;
    include E4S_FULL_PATH . 'competition/public/getAllCompInfoV2.php';
    e4s_getCompsForFilter($obj);
}
function e4s_rest_updateClubCompDef($obj){
    include E4S_FULL_PATH . 'builder/clubCompCRUD.php';
    e4s_amendClubCompDef($obj);
}
function e4s_rest_deleteClubCompDef($obj){
    include E4S_FULL_PATH . 'builder/clubCompCRUD.php';
    e4s_deleteClubCompDef($obj);
}
function e4s_rest_readClubCompDef($obj){
    include E4S_FULL_PATH . 'builder/clubCompCRUD.php';
    $defs = e4s_listClubCompDefs($obj);
    Entry4UISuccess($defs);
}
function e4s_rest_updateClubCompCatDef($obj){
    include E4S_FULL_PATH . 'builder/clubCompCRUD.php';
    e4s_amendClubCompCatDef($obj);
}
function e4s_rest_readClubCompCatDef($obj){
    include E4S_FULL_PATH . 'builder/clubCompCRUD.php';
    $cats = e4s_listClubCompCats($obj);
    Entry4UISuccess($cats);
}
function e4s_rest_deleteClubCompCatDef($obj){
    include E4S_FULL_PATH . 'builder/clubCompCRUD.php';
    e4s_deleteClubCompCatDef($obj);
}
function e4s_rest_updateClubComp($obj){
    include E4S_FULL_PATH . 'admin/userMaint.php';
    e4s_updateClubComp($obj);
}
function e4s_rest_deleteClubComp($obj){
    include E4S_FULL_PATH . 'admin/userMaint.php';
    e4s_deleteClubComp($obj);
}
function e4s_rest_getPublicCompInfo($obj) {
    $public = TRUE;
    $orgid = checkFieldForXSS($obj, 'orgid:Organisation ID');
    include E4S_FULL_PATH . 'competition/public/getAllCompInfo.php';
    exit();
}

function e4s_rest_getUserRecord($obj) {
    $email = checkFieldForXSS($obj, 'email:Email Address');
    $id = checkFieldForXSS($obj, 'id:User ID');
    include E4S_FULL_PATH . 'admin/userMaint.php';

    if (!is_null($email)) {
        getInformationForEmail($email);
    } else {
        getInformationForID($id);
    }
    exit();
}

function e4s_rest_deleteClubFromUser($obj) {
    $userid = checkFieldForXSS($obj, 'userid:User ID');
    $clubid = checkFieldForXSS($obj, 'clubid:Club ID');
    include E4S_FULL_PATH . 'admin/userMaint.php';
    removeUserClub($userid, $clubid);
    exit();
}

function e4s_rest_deleteAreaFromUser($obj) {
    $userid = checkFieldForXSS($obj, 'userid:User ID');
    $areaid = checkFieldForXSS($obj, 'areaid:Area ID');
    include E4S_FULL_PATH . 'admin/userMaint.php';
    removeUserArea($userid, $areaid);
    exit();
}

function e4s_rest_addUserToArea($obj) {
    $userid = checkFieldForXSS($obj, 'userid:User ID');
    $areaid = checkFieldForXSS($obj, 'areaid:Area ID');
    include E4S_FULL_PATH . 'admin/userMaint.php';
    addUserToArea($userid, $areaid);
    exit();
}

function e4s_rest_addUserToClub($obj) {
    $userid = checkFieldForXSS($obj, 'userid:User ID');
    $clubid = checkFieldForXSS($obj, 'clubid:Club ID');
    include E4S_FULL_PATH . 'admin/userMaint.php';
    addUserToClub($userid, $clubid);
    exit();
}

function e4s_rest_migrateUsers($obj) {
//    e4s_webPageHeader();
    include E4S_FULL_PATH . 'dbFix/migrateusers.php';
    exit();
}

function e4s_rest_setUserPwd($obj) {
    e4s_webPageHeader();
    include E4S_FULL_PATH . 'admin/userMaint.php';
    e4s_switchUser($obj, FALSE);
    exit();
}

function e4s_rest_switchUser($obj) {
    include E4S_FULL_PATH . 'admin/userMaint.php';
    e4s_switchUser($obj);
    exit();
}

function e4s_rest_restoreUserPWD($obj) {
    e4s_webPageHeader();
    $email = checkFieldForXSS($obj, 'email:Email Address');
    include E4S_FULL_PATH . 'admin/userMaint.php';
    e4s_restoreUserPassword($email);
    exit();
}

function e4s_rest_getUserInfo($obj) {
    $key = '';
    include E4S_FULL_PATH . 'user/getUser.php';
    exit();
}

//function e4s_rest_getInfoForUser( $obj ) {
////    Where does Key come from ?
//    $key = $obj->get_param(key);
//    include E4S_FULL_PATH .'/user/getUser.php' ;
//    exit();
//}
function e4s_rest_getFinancialReport4($obj) {
    e4s_webPageHeader();
    $echo = checkFieldForXSS($obj, 'echo:Echo Debug');
    $days = checkFieldForXSS($obj, 'days:No of Days');
    if (is_null($days)) {
        $days = 7;
    }
    include E4S_FULL_PATH . 'reports/compreport4.php';
    exit();
}

function e4s_rest_getFinancialReport3($obj) {
    e4s_webPageHeader();
    $echo = checkFieldForXSS($obj, 'echo:Echo Debug');
    $days = checkFieldForXSS($obj, 'days:No of Days');
    if (is_null($days)) {
        $days = 7;
    }
    include E4S_FULL_PATH . 'reports/compreport3.php';
    exit();
}

function e4s_rest_getFinancialByDateReport($obj) {
    e4s_webPageHeader();
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $orgid = checkFieldForXSS($obj, 'orgid:Organiser ID');
    $days = checkFieldForXSS($obj, 'days:No of Days');
    if (is_null($days)) {
        $days = 7;
    }
    include E4S_FULL_PATH . 'reports/compreport2.php';
    exit();
}
function e4s_rest_listStandardValues($obj){
    include E4S_FULL_PATH . 'builder/standardsCRUD.php';
    e4s_actionStandardValues($obj, E4S_CRUD_LIST) ;
}
function e4s_rest_modifyStandardValue($obj){
    include E4S_FULL_PATH . 'builder/standardsCRUD.php';
    e4s_actionStandardValues($obj, E4S_CRUD_UPDATE) ;
}
function e4s_rest_deleteStandardValue($obj){
    include E4S_FULL_PATH . 'builder/standardsCRUD.php';
    e4s_actionStandardValues($obj, E4S_CRUD_DELETE) ;
}
function e4s_rest_listStandards($obj){
    include E4S_FULL_PATH . 'builder/standardsCRUD.php';
    e4s_actionStandards($obj, E4S_CRUD_LIST) ;
}
function e4s_rest_modifyStandard($obj){
    include E4S_FULL_PATH . 'builder/standardsCRUD.php';
    e4s_actionStandards($obj, E4S_CRUD_UPDATE) ;
}
function e4s_rest_deleteStandard($obj){
    include E4S_FULL_PATH . 'builder/standardsCRUD.php';
    e4s_actionStandards($obj, E4S_CRUD_DELETE) ;
}
function e4s_rest_getFinancialReport($obj) {
    $GLOBALS[E4S_NOHEADERS] = TRUE;
//    var_dump("e4s_rest_getFinancialReport");
    require_once E4S_FULL_PATH . 'dbInfo.php';
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    e4s_webPageHeader();
    $obj = new financialClass($compid);
    $obj->outputReport();
    exit();
}

function e4s_rest_clearCheckins($obj) {
    include E4S_FULL_PATH . 'athlete/checkin.php';
    e4s_clear_Checkin($obj);
}

function e4s_rest_listCheckins($obj) {
    include E4S_FULL_PATH . 'athlete/checkin.php';
    e4s_list_Checkin($obj, TRUE);
}

function e4s_rest_listAthleteCheckins($obj) {
    include E4S_FULL_PATH . 'athlete/checkin.php';
    e4s_list_Checkin($obj, FALSE);
}

function e4s_rest_veryifyAthleteEmailId($obj) {
    include E4S_FULL_PATH . 'athlete/checkin.php';
    e4s_veryifyEmailIdAndCompCode($obj);
}

function e4s_rest_getCheckinSummary($obj) {
    include E4S_FULL_PATH . 'athlete/checkin.php';
    e4s_getCheckinSummary($obj);
}

function e4s_rest_updateCheckins($obj) {
    include E4S_FULL_PATH . 'athlete/checkin.php';
    e4s_updateCheckins($obj, TRUE);
}

function e4s_rest_updateAthleteCheckins($obj) {
    include E4S_FULL_PATH . 'athlete/checkin.php';
    e4s_updateCheckins($obj, FALSE);
}

function e4s_rest_validateCompetition($obj){
    include E4S_FULL_PATH . 'builder/compCRUD.php';
    e4s_actionComp($obj, 'Validate');
}
function e4s_rest_cloneCompetition($obj) {
    include E4S_FULL_PATH . 'builder/compCRUD.php';
    e4s_actionComp($obj, 'Clone');
    exit();
}

function e4s_rest_getComp($obj) {
    include E4S_FULL_PATH . 'builder/compCRUD.php';
    e4s_getCompetitionInfo($obj);
}

function e4s_rest_getCompetitionReport($obj) {
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    include E4S_FULL_PATH . 'competition/public/getCompetitionReport.php';
    exit();
}

function e4s_rest_getCompetitionReport2($obj) {
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    include E4S_FULL_PATH . 'competition/public/getCompetitionReport2.php';
    exit();
}

function e4s_rest_getCompetitionReport3($obj) {
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    include E4S_FULL_PATH . 'competition/public/getCompetitionReport3.php';
    exit();
}

// confirm Participants for a heat
function e4s_rest_confirmHeatParticipants($obj) {
    include E4S_FULL_PATH . 'seedings/seedings.php';
    e4s_confirmParticipants($obj);
}

function e4s_rest_confirmSeedingForEG($obj) {
    include E4S_FULL_PATH . 'seedings/seedings.php';
    e4s_confirmSeedingForEg($obj);
}
// update from DragDrop
function e4s_rest_updateSeedingForEG($obj) {
    include E4S_FULL_PATH . 'seedings/seedings.php';
    e4s_updateSeedingForEg($obj);
}
// Clear Seeding
function e4s_rest_clearEventSeedings($obj) {
    $eventgroupid = checkFieldForXSS($obj, 'eventgroupid:Event Group');
    include E4S_FULL_PATH . 'reports/output/outputFunctions.php';
    e4s_clearSeedings($eventgroupid);
}

function e4s_rest_dataCompSeedings($obj) {
    include E4S_FULL_PATH . 'seedings/seedings.php';
    e4s_listSeedingsForComp($obj, true);
}
function e4s_rest_listEntryStatus($obj) {
	e4s_webPageHeader();
	include E4S_FULL_PATH . 'showEntries/entryStatus.php';
	e4s_listEntryStatus($obj);
}
function e4s_rest_listCompSeedings($obj) {
    e4s_webPageHeader();
    include E4S_FULL_PATH . 'seedings/seedings.php';
	e4s_listSeedingsForComp($obj, false);
}
function e4s_rest_clearCompSeedings($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    include E4S_FULL_PATH . 'reports/output/outputFunctions.php';
    e4s_clearAllSeedings($compId);
}

function e4s_writeHeightPositions($obj){
    include E4S_FULL_PATH . 'results/webresults.php';
	e4s_writeHeightPositionsToDB($obj);
}
function e4s_rest_writeEventSeedingsV2($obj) {
    $eventGroupId = checkFieldForXSS($obj, 'eventgroupid:Event Group');
    include E4S_FULL_PATH . 'e4sObjects.php';
    $obj = new seedingV2Class(0, $eventGroupId);
    $obj->writeInboundSeedings();
}

// run the outputReport
function e4s_rest_getOutputReport($obj) {
    e4s_rest_getOutputReportGeneric($obj, '', false);
    exit();
}
function e4s_rest_getOutputPublicReport($obj) {
    e4s_rest_getOutputReportGeneric($obj, '', true);
    exit();
}
function e4s_rest_getEntryPerf($obj){
	include E4S_FULL_PATH . 'entries/entries.php';
	$retObj = e4s_getEntryPerf($obj);
    Entry4UISuccess($retObj);
}
function e4s_rest_showEntriesV2($obj){
	include E4S_FULL_PATH . 'reports/showEntriesV2.php';
    e4s_showEntriesV2($obj);
    exit;
}
function e4s_rest_getOutputPublicReportV2($obj) {
    e4s_rest_getOutputReportGeneric($obj, 'V2', true);
}
function e4s_rest_getOutputReportV2($obj) {
    e4s_rest_getOutputReportGeneric($obj, 'V2', false);
    exit();
}
function e4s_rest_getOutputReportV3($obj) {
    e4s_rest_getOutputReportGeneric($obj, 'V3', false);
    exit();
}
function e4s_rest_getOutputPublicReportV3($obj) {
    e4s_rest_getOutputReportGeneric($obj, 'V3', true);
}
function e4s_rest_getOutputReportGeneric($obj, $version, $cardOnly) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    include E4S_FULL_PATH . 'reports/output/outputReport' . $version . '.php';
    $compObj = e4s_getCompObj($compId);
    if ( $compObj->hasCardAccess() ) {
        showOutputReport($compObj->getSourceId(), $cardOnly);
    }else{
        e4s_rest_listCompSeedings($obj);
    }

    exit();
}

function e4s_rest_fullCardReload($obj) {
    $data = checkJSONObjForXSS($obj, 'data:Card Data');
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    include E4S_FULL_PATH . 'classes/socketClass.php';
    e4s_sendSocketInfo($compId, $data, R4S_SOCKET_REFRESH_CARD);
}

// Bib Number Generation
function e4s_rest_getUnused($obj) {
    include E4S_FULL_PATH . 'dbInfo.php';
    e4s_getUnusedBibNos($obj);
    Entry4UISuccess();
}

function e4s_rest_changeAthleteBib($obj) {
    include E4S_FULL_PATH . 'dbInfo.php';
    e4s_changeAthletesBib($obj);
    Entry4UISuccess();
}

function e4s_rest_clearBibs($obj) {
    include E4S_FULL_PATH . 'dbInfo.php';
    e4s_clearBibs($obj);
}

function e4s_rest_createBibs($obj) {
    include E4S_FULL_PATH . 'dbInfo.php';
    e4s_createBibs($obj);
}

function e4s_rest_addBibs($obj) {
    include E4S_FULL_PATH . 'dbInfo.php';
    e4s_addBibs($obj);
}

function e4s_rest_getCompAthleteSummary($obj) {
    include E4S_FULL_PATH . 'dbInfo.php';
    e4s_getCompetitionAthletes($obj, TRUE);
}

function e4s_rest_getCompAthletes($obj) {
    include E4S_FULL_PATH . 'dbInfo.php';
    e4s_getCompetitionAthletes($obj, FALSE);
}

// Live Feeds
function e4s_rest_getInfoSchedule($obj){
	include E4S_FULL_PATH . '/info/schedule.php';
	$compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    e4s_getInfoSchedule($compId);
    exit;
}
function e4s_rest_getInfoEvent($obj){
	include E4S_FULL_PATH . '/info/schedule.php';
	$egId = checkFieldForXSS($obj, 'egId:Event Group Id' . E4S_CHECKTYPE_NUMERIC);
    e4s_getInfoEvent($egId);
    exit;
}
function e4s_rest_getInfoResults($obj){
	include E4S_FULL_PATH . '/info/schedule.php';
	$egId = checkFieldForXSS($obj, 'egId:Event Group Id' . E4S_CHECKTYPE_NUMERIC);
    e4s_getInfoResults($egId);
    exit;
}
// results
function e4s_rest_SendResultsToDisplays($obj) {
    include E4S_FULL_PATH . 'results/webresults.php';
    e4s_publishEventResults($obj);
}

// Lynx Receiver
function e4s_rest_receiveLynx($obj){
	include E4S_FULL_PATH . 'otd/LynxController/lynxReceiver.php';
	e4s_receiveLynx($obj);
}

function accessProtected($obj, $prop) {
	$reflection = new ReflectionClass($obj);
	$property = $reflection->getProperty($prop);
	$property->setAccessible(true);
	return $property->getValue($obj);
}

// seed final
function e4s_rest_readResults($obj) {
    include E4S_FULL_PATH . 'results/webresults.php';
    e4s_readCompResults($obj);
}

// Read Results
function e4s_rest_deleteHeatResults($obj) {
    include E4S_FULL_PATH . 'results/webresults.php';
    e4s_deleteEgResults($obj);
}

function e4s_rest_writeAutoEntries($obj) {
    include E4S_FULL_PATH . 'results/webresults.php';
    e4s_writeAutoEntries($obj);
}

function e4s_rest_writeResults($obj) {
    include E4S_FULL_PATH . 'results/webresults.php';
    e4s_writeCompResults($obj);
}

// refresh Web Results
function e4s_rest_refreshWebResults($obj) {
    include E4S_FULL_PATH . 'results/webresults.php';
    e4s_refreshWebResults($obj);
}

// Scoreboard
function e4s_rest_sendScoreboardMessage($obj) {
    include_once E4S_FULL_PATH . 'dbInfo.php';
    include_once E4S_OTD_PATH . '/tfUiCommon.php';
    include_once E4S_FULL_PATH . 'classes/uploadTrackInfo.php';
    e4s_sendMessage($obj);
}

function e4s_rest_commandToScoreboard($obj) {
    include_once E4S_FULL_PATH . 'dbInfo.php';
    e4s_sendCommand($obj);
}

function e4s_rest_getScoreboardEvents($obj) {
    include E4S_FULL_PATH . 'events/eventGroups.php';
    $compId = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    e4s_getEventGroupsForComp($compId);
}
function e4s_restFeederEntries($obj){
    include E4S_FULL_PATH . 'results/results.php';
    $entriesObj = e4s_createFeederEntries($obj);
    Entry4UISuccess($entriesObj);
}
function e4s_restGetPayees($obj){
	include E4S_FULL_PATH . 'builder/athleteCRUD.php';
	$payeeObj = e4s_getPayeesForResults($obj);
	Entry4UISuccess($payeeObj);
}
function e4s_restGetOwners($obj){
    include E4S_FULL_PATH . 'builder/athleteCRUD.php';
    $ownerObj = e4s_getOwnersForResults($obj);
    Entry4UISuccess($ownerObj);
}
function e4s_restGetAthleteOwners($obj){
    include E4S_FULL_PATH . 'builder/athleteCRUD.php';
    $ownerObj = e4s_getAthleteOwners($obj);
    Entry4UISuccess($ownerObj);
}
function e4s_rest_postPFResults($obj) {
    include E4S_FULL_PATH . 'results/results.php';
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    e4s_timetronicsTrackResults($compid);
}
function e4s_rest_postLynxPFResults($obj) {
    include E4S_FULL_PATH . 'results/results.php';
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    e4s_lynxTrackResults($compid);
}

function e4s_rest_getResultsForComp($obj) {
    include E4S_FULL_PATH . 'results/results.php';
    e4s_getResultsForEvent($obj);
}

function e4s_rest_clearScoreboardForEvent($obj) {
    include E4S_FULL_PATH . 'results/results.php';
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $eventno = checkFieldForXSS($obj, 'eventno:Event number');
    e4s_clearScoreboard($compid, $eventno);
}

function e4s_rest_getScoreboardForEvent($obj) {
    include E4S_FULL_PATH . 'results/results.php';
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $eventno = checkFieldForXSS($obj, 'eventno:Event number');

    if ((int)$compid === 0 and (int)$eventno > 0) {
        //    // switch them over
        $compid = $eventno;
        e4s_getAllForComp($compid);
    } else {
        e4s_getScoreboardForBoard($compid, $eventno);
    }
}
function e4s_rest_getAllScoreboards($obj) {
    include E4S_FULL_PATH . 'results/results.php';
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    e4s_getAllForComp($compid);
}
function e4s_rest_getScoreboardEventsForArray($obj) {
    include E4S_FULL_PATH . 'results/results.php';
    e4s_getScoreboardEventsForArray($obj);
}

// Post Results
function e4s_rest_NextUpDelete($obj){
    include E4S_FULL_PATH . 'classes/e4sEventNextUp.php';
    e4s_deleteNextUp($obj);
}
function e4s_rest_NextUp($obj){
    include E4S_FULL_PATH . 'classes/e4sEventNextUp.php';
    e4s_updateNextUp($obj);
}
function e4s_rest_closeResults($obj){
	include_once E4S_FULL_PATH . 'dbInfo.php';
    e4s_closeResults($obj);
}
function e4s_rest_postResults($obj) {
    include E4S_FULL_PATH . 'results/results.php';
    e4s_postResults($obj);
}

function e4s_rest_getEntries($obj) {
    include E4S_FULL_PATH . 'entries/entries.php';
    e4s_getEntryList($obj);
    exit();
}

function e4s_rest_getWaitingEntries($obj) {
    include E4S_FULL_PATH . 'reports/reports.php';
    e4s_getWaitingAthletes($obj);
    exit();
}

function e4s_rest_getCompetitionClubs($obj) {
    $public = FALSE;

    $orgId = checkFieldForXSS($obj, 'orgId:Organisation ID');
    include E4S_FULL_PATH . 'competition/getCompetitionClubs.php';
    exit();
}

function e4s_rest_getPublicCompetitionClubs() {
    $public = TRUE;
    include E4S_FULL_PATH . 'competition/getCompetitionClubs.php';
    exit();
}

function e4s_rest_getCompTeams($obj) {
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    include E4S_FULL_PATH . 'competition/team/getCompTeams.php';
    exit();
}

function e4s_rest_checkEligable($obj) {
    $aid = checkFieldForXSS($obj, 'aid:Athlete ID');
    $ceid = checkFieldForXSS($obj, 'ceid:Competition Event ID');
    include E4S_FULL_PATH . 'athlete/athleteEligable.php';
    exit();
}

function e4s_rest_getUsersInEntities($obj) {
    include E4S_FULL_PATH . 'admin/userMaint.php';
    e4s_getUsersInEntities($obj);
}

function e4s_rest_getUsers($obj) {
    global $defaultPageSize;

    $page = checkFieldForXSS($obj, 'pagenumber:Page Number');
    if ($page === '' || !is_numeric($page)) {
        $page = 1;
    }
    $pagesize = checkFieldForXSS($obj, 'pagesize:Page size');
    if ($pagesize === '' || $pagesize === null) {
        $pagesize = $defaultPageSize;
    }
    if (!is_numeric($pagesize)) {
        $pagesize = $defaultPageSize;
    }
    if ($pagesize < 2) {
        $pagesize = $defaultPageSize;
    }
    $search = checkFieldForXSS($obj, 'search:Search Criteria');
    if ($search === '' || $search === null) {
        $search = '';
    }

    if (str_contains($search, '&lt;')) {
        $search = str_replace('&lt;', '<', $search);
    }
    if (str_contains(strtolower($search), 'mailto:')) {
        $search = str_replace('mailto:','', strtolower($search));
    }
    if (str_contains($search, '<')) {
        $temp = preg_split('~<~', $search);
        $search = $temp[1];
    }

    if (str_contains($search, '&gt;')) {
        $search = str_replace('&gt;', '>', $search);
    }
    if (str_contains($search, '>')) {
        $temp = preg_split('~>~', $search);
        $search = $temp[0];
    }
    $sort = checkFieldForXSS($obj, 'sort:Sort');
    if ($sort === null) {
        $sort = '';
    }
    $sortdesc = checkFieldForXSS($obj, 'sortdesc:Sort Order');
    if (is_null($sortdesc)) {
        $sortdesc = '';
    }
    $extra = checkFieldForXSS($obj, 'extra');
    if ($extra === '1') {
        $extra = TRUE;
    } else {
        $extra = FALSE;
    }
    include E4S_FULL_PATH . 'admin/userMaint.php';
    e4s_getUsers($search, $extra, $page, $pagesize, $sort, $sortdesc);
    exit();
}

function e4s_rest_getAthleteUsers($obj) {
    include_once E4S_FULL_PATH . 'athlete/userAthletes.php';
    e4s_getUsersForAthlete($obj);
}

function e4s_rest_getAthletesForEventGroupID($obj) {
    include_once E4S_FULL_PATH . 'dbInfo.php';
    e4s_getAthletesForEGId($obj);
}

function e4s_rest_getAthletes($obj) {
    global $defaultPageSize;
    global $echo;
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    $echo = checkFieldForXSS($obj, 'echo:Echo');
    $page = checkFieldForXSS($obj, 'pagenumber:Page Number');
    $teamid = checkFieldForXSS($obj, 'teamid:Team ID');
    $urn = checkFieldForXSS($obj, 'urn:URN');
    $ceid = checkFieldForXSS($obj, 'ceid:Competition Event ID');
    $athleteid = checkFieldForXSS($obj, 'athleteid:Athlete ID');
    $showAllAthletes = checkFieldForXSS($obj, 'showAllAthletes:Show All Athletes');
    if ($showAllAthletes === '0') {
        $showAllAthletes = FALSE;
    } else {
        $showAllAthletes = TRUE;
    }
    if ($ceid === '' || !is_numeric($ceid)) {
        $ceid = 0;
    } else {
        $ceid = (int)$ceid;
    }
    if ($page === '' || !is_numeric($page)) {
        $page = 1;
    }
    $pagesize = checkFieldForXSS($obj, 'pagesize:Page size');
    if ($pagesize === '' || $pagesize === null) {
        $pagesize = $defaultPageSize;
    }
    if (!is_numeric($pagesize)) {
        $pagesize = $defaultPageSize;
    }
    if ($pagesize < 2) {
        $pagesize = $defaultPageSize;
    }
    $search = checkFieldForXSS($obj, 'search:Search Criteria');
    if ($search === '' || $search === null) {
        $search = '';
    }
    $filterRegion = checkFieldForXSS($obj, 'region:Region');
    if ($filterRegion === null) {
        $filterRegion = '';
    } else {
        $filterRegion = trim(addslashes($filterRegion));
    }
    $filterCounty = checkFieldForXSS($obj, 'county:County');
    if ($filterCounty === null) {
        $filterCounty = '';
    } else {
        $filterCounty = addslashes($filterCounty);
    }
    $filterClub = checkFieldForXSS($obj, 'club:Club');
    if ($filterClub === null) {
        $filterClub = '';
    } else {
        $filterClub = trim(addslashes($filterClub));
    }

    $filterFirstname = checkFieldForXSS($obj, 'firstname:First name');
    if ($filterFirstname === null) {
        $filterFirstname = '';
    } else {
        $filterFirstname = trim(addslashes($filterFirstname));
    }

    $filterSurname = checkFieldForXSS($obj, 'surname:Surname');
    if ($filterSurname === null) {
        $filterSurname = '';
    } else {
        $filterSurname = trim(addslashes($filterSurname));
    }
    $filterGender = $obj->get_param('gender');
    $filterAgeGroupID = checkFieldForXSS($obj, 'ageGroupId:Age group ID');
    if ($filterAgeGroupID === null) {
        $filterAgeGroupID = '';
    }
    $sort = checkFieldForXSS($obj, 'sort:Sort');
    if ($sort === null) {
        $sort = '';
    }
    $sortdesc = checkFieldForXSS($obj, 'sortdesc:Sort Order');
    if ($sortdesc === null) {
        $sortdesc = '';
    }
    include E4S_FULL_PATH . 'athlete/getAthletes.php';
    exit();
}

function e4s_rest_getCompetitionDates($obj) {
    $club = checkFieldForXSS($obj, 'club:Club');
    $public = FALSE;
    include E4S_FULL_PATH . 'competition/getCompetitionDates.php';
    exit();
}

function e4s_rest_getPublicCompetitionDates($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID');
    $club = checkFieldForXSS($obj, 'club:Club');
    if ($club === 'undefined') {
        exit();
    }
    $public = TRUE;
    include E4S_FULL_PATH . 'competition/getCompetitionDates.php';
    exit();
}

function e4s_rest_getCompetitionTeamEvents($obj) {
    $compid = checkFieldForXSS($obj, 'compid:Competition ID');
    include E4S_FULL_PATH . 'events/getCompetitionTeamEvents.php';
    exit();
}
function e4s_rest_getCompetitionEventsV2($obj) {
    $compId = checkFieldForXSS($obj, 'compid:Competition ID');
    $athleteId = checkFieldForXSS($obj, 'aid:Athlete ID');
	$pbVersion = checkFieldForXSS($obj, 'version:Version');
    include E4S_FULL_PATH . 'events/getCompetitionEventsV2.php';
    exit();
}
function e4s_rest_getCompetitionEvents($obj) {
	$compId = checkFieldForXSS($obj, 'compid:Competition ID');
    $athleteId = checkFieldForXSS($obj, 'aid:Athlete ID');
    $clubId = checkFieldForXSS($obj, 'clubid:Club ID');
	include E4S_FULL_PATH . 'events/getCompetitionEventsV2.php';
    exit();
}

function e4s_rest_confirmWithParams($obj) {
    $regid = $_GET['regid'];
    $dob = $_GET['dob'];
    $aocode = $_GET['aocode'];
    $classification = $_GET['classification'];
    $clubid = 0;
    if (isset($_GET['clubid'])) {
        $clubid = $_GET['clubid'];
    }
    include E4S_FULL_PATH . 'athlete/confirmAthlete.php';
    exit();
}
function e4s_rest_getClubComps($obj){
    include E4S_FULL_PATH . 'competition/commonComp.php';
    e4s_getClubComps();
}
function e4s_rest_getRegIDWithParms($obj) {
    include E4S_FULL_PATH . 'builder/athleteCRUD.php';
    e4s_checkForRegisteredAthlete($obj);
}
function e4s_rest_getAAIAthlete($obj){
    include E4S_FULL_PATH . 'builder/athleteCRUD.php';
    e4s_getAAIAthlete($obj);
}
function e4s_rest_getUploadTrackInfo($obj) {
    include E4S_FULL_PATH . 'otd/uploadTrackInfo.php';
    e4s_requestTrackInfo($obj);
}

function e4s_rest_uploadTrackInfo($obj) {
    include E4S_FULL_PATH . 'otd/uploadTrackInfo.php';
    e4s_uploadTrackFile($obj, FALSE);
}

function e4s_rest_uploadTrackInfoAuto($obj) {
    include E4S_FULL_PATH . 'otd/postTrackFile.php';
    e4s_postTrackFile($obj);
}
function e4s_rest_exportResultsForPOF10($obj) {
    include E4S_FULL_PATH . 'results/webresults.php';
    e4s_exportResultsForPOF10($obj);
}

function e4s_rest_generateResultsForPOF10($obj) {
    include E4S_FULL_PATH . 'results/webresults.php';
    e4s_generatePof10File($obj);
}
function e4s_rest_clubCompReport($obj){
    e4s_webPageHeader();
    include E4S_FULL_PATH . 'reports/clubCompReport.php';
    e4s_clubCompResultReport($obj);
}
function e4s_rest_clearHeightResults($obj){
    include E4S_FULL_PATH . 'results/webresults.php';
    e4s_clearHeightResults($obj);
}
function e4s_rest_exportResultsForComp($obj) {
    e4s_webPageHeader();
    include E4S_FULL_PATH . 'results/webresults.php';
    e4s_exportResultsForComp($obj);
}

function e4s_rest_getAthletePof10Info($obj) {
    include E4S_FULL_PATH . 'performance/perf.php';
    e4s_getPof10Info($obj);
}

function e4s_rest_updateAthletePof10($obj) {
	include E4S_FULL_PATH . 'performance/perf.php';
	e4s_updateAthletesPof10($obj);
}
function e4s_rest_updateAthletePof10Info($obj) {
    include E4S_FULL_PATH . 'performance/perf.php';
    // updates the pb with the Pof10 which is not whats required
//    e4s_updatePBsFromPof10($obj);
}

function e4s_rest_InvalidURN() {
    Entry4UISuccess();
}

function e4s_rest_getAthlete($obj) {
    $id = checkFieldForXSS($obj, 'id:ID');
    $compid = checkFieldForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    include E4S_FULL_PATH . 'athlete/getAthlete.php';
    e4s_getAthlete($id, $compid);
}

function e4s_rest_getAndLinkAthlete($obj) {
    include E4S_FULL_PATH . 'athlete/getAthlete.php';
    e4s_checkAndAddAthlete($obj);
}

function e4s_rest_getMultiPb($obj) {
    $aid = checkFieldForXSS($obj, 'aid:Athlete ID');
    $ceid = checkFieldForXSS($obj, 'ceid:Ceompetition Event ID');
    Entry4UIError(9879, 'Function deprecated');
}

function e4s_rest_getCart() {
    include E4S_FULL_PATH . 'cart/getCart.php';
    getActiveUserCart();
}

function e4s_rest_getMiniCart() {
    e4s_webPageHeader();
    include E4S_FULL_PATH . 'cart/getCart.php';
    getActiveUserMiniCart();
}

function e4s_rest_CancelEntry($obj) {
    include E4S_FULL_PATH . 'entries/commonEntries.php';
    e4s_cancelEntry($obj);
}

function e4s_rest_MarkEntryPresent($obj) {
    include E4S_FULL_PATH . 'entries/commonEntries.php';
    e4s_MarkEntryPresent($obj);
}

function e4s_rest_MarkTeamEntryPresent($obj) {
	include E4S_FULL_PATH . 'entries/commonEntries.php';
	e4s_MarkTeamEntryPresent($obj);
}

function e4s_rest_MoveTrackPosition($obj) {
    include E4S_FULL_PATH . 'entries/commonEntries.php';
    e4s_moveTrackPosition($obj);
}

// Post R4S Results
function r4s_rest_postResults($obj) {
    include E4S_OTD_PATH . '/r4sResults.php';
    r4s_postResults($obj);
}

function r4s_rest_clearResults($obj) {
    include E4S_OTD_PATH . '/r4sResults.php';
    r4s_clearResults($obj);
}
function e4s_rest_SetStands($obj) {
	include E4S_FULL_PATH . 'reports/output/outputFunctions.php';
	e4s_setStands($obj);
}
function e4s_rest_SetStartHeight($obj) {
	include E4S_FULL_PATH . 'reports/output/outputFunctions.php';
    e4s_setStartHeight($obj);
}

function e4s_rest_AddToBasket($obj) {
    include E4S_FULL_PATH . 'cart/getCart.php';
    e4s_addToBasket($obj);
}

function e4s_rest_UpdatePaid($obj) {
    include E4S_FULL_PATH . 'cart/getCart.php';
    e4s_updatePaid($obj);
}

function e4s_rest_getAthleteTicket($obj) {
    include E4S_FULL_PATH . 'dbInfo.php';
    e4s_getESAthleteGuid($obj);
}

function e4s_rest_getClubs($obj) {
    global $defaultPageSize;

    $useAreaId = checkFieldForXSS($obj, 'areaid:Area ID');
    if ($useAreaId === '' || $useAreaId === null) {
        $useAreaId = 0;
    }
    $search = checkFieldForXSS($obj, 'key:Search key');
    if (is_null($search)) {
        $search = checkFieldForXSS($obj, 'startswith:Search key');
    }
    $clubtype = checkFieldForXSS($obj, 'type:Club type');

    $page = checkFieldForXSS($obj, 'pagenumber:Page Number');
    if ($page === '' || !is_numeric($page)) {
        $page = 1;
    }
    $pagesize = checkFieldForXSS($obj, 'pagesize:Page size');
    if ($pagesize === '' || $pagesize === null) {
        $pagesize = $defaultPageSize;
    }
    if (!is_numeric($pagesize)) {
        $pagesize = $defaultPageSize;
    }
    if ($pagesize < 2) {
        $pagesize = $defaultPageSize;
    }

    include E4S_FULL_PATH . 'clubs/getClubs.php';
    exit();
}

function e4s_restRelay($obj){
    include E4S_FULL_PATH . 'admin/e4sReturn.php';
    $url = checkFieldForXSS($obj, 'url:Relayed URL');

    $type = checkFieldForXSS($obj, 'type:Content Type');

    $url = urldecode($url);
	$arrContextOptions=array(
		"ssl"=>array(
			"verify_peer"=>false,
			"verify_peer_name"=>false,
		),
	);

	$x = file_get_contents($url, false, stream_context_create($arrContextOptions));

    if (!is_null($type) and strtolower($type) === 'json') {
        $content = e4s_getDataAsType($x, E4S_OPTIONS_OBJECT);
    }else{
        $content = $x;
    }
    Entry4UISuccess('"data":' . $content);
}
function e4s_rest_SwitchAthlete($obj) {
    $ceid = checkFieldForXSS($obj, 'ceid:Competition Event ID');
    $entryid = checkFieldForXSS($obj, 'entryid:Entry ID');
    $athleteid = checkFieldForXSS($obj, 'athleteid:athlete ID');

    include E4S_FULL_PATH . 'entries/switchEntry.php';
    e4s_switchAthlete($entryid, $athleteid, $ceid);
    exit();
}

function e4s_rest_SwitchEntry($obj) {
    $ceid = checkFieldForXSS($obj, 'ceid:Competition Event ID');
    $entryid = checkFieldForXSS($obj, 'entryid:Entry ID');

    include E4S_FULL_PATH . 'entries/switchEntry.php';
    e4s_switch($entryid, $ceid);
    exit();
}

function e4s_rest_createProduct($obj) {
    include E4S_FULL_PATH . 'product/createProduct.php';
    $ceId = checkFieldForXSS($obj, 'ceid:Competition Event ID');
    $athleteId = checkFieldForXSS($obj, 'aid:Athlete ID');
    $useClubId = checkFieldForXSS($obj, 'clubid:Club ID');
    $pb = checkFieldForXSS($obj, 'pb:PB');

    if (is_null($pb)) {
        $pb = 0;
    }

    if (!is_null($useClubId)) {
        $useClubId = preg_split('~/~', $useClubId);
        $useClubId = $useClubId[0];
    }
    $teamId = checkFieldForXSS($obj, 'teamid:Team ID');
    if ($teamId === '' || $teamId === null) {
        $teamId = 0;
    } else {
        $teamId = (int)$teamId;
    }

    $productObj = new stdClass();
    $productObj->ceId = $ceId;
    $productObj->athleteId = $athleteId;
    $productObj->useClubId = $useClubId;
    $productObj->teamId = $teamId;
    $productObj->perf = (float)$pb;
    e4s_createProduct($productObj);
}

function e4s_rest_postAthleteMultiPBUpdate($obj) {
    exit();
}
function e4s_rest_pushAthletePBUpdateV2($obj) {
	include E4S_FULL_PATH . 'performance/perf.php';
	e4s_updatePBFromAthleteMaintV2($obj);
}
function e4s_rest_pushAthletePBUpdate($obj) {
    include E4S_FULL_PATH . 'performance/perf.php';
    e4s_updatePBFromAthleteMaint($obj);
}

function e4s_rest_competitionPBUpdate($obj) {
    include E4S_FULL_PATH . 'performance/perf.php';
    e4s_updateCompetitionPb($obj);
}

function e4s_rest_newTeam($obj) {
    $compid = $obj->get_param('compId');
    $teamname = $obj->get_param('teamName');
    $gender = checkFieldForXSS($obj, 'gender:Gender');
    $areaid = $obj->get_param('areaId');
    include E4S_FULL_PATH . 'competition/team/newTeam.php';
    exit();
}

function e4s_rest_deleteTeam($obj) {
    $teamid = checkFieldForXSS($obj, 'teamid:Team ID');
    include E4S_FULL_PATH . 'competition/team/deleteTeam.php';
    exit();
}

function e4s_rest_newAthlete($obj) {

    $id = checkFieldForXSS($obj, 'id:ID');
    $urn = checkFieldForXSS($obj, 'URN:Registration ID');
    if ($urn === '') {
        $urn = null;
    }
    $aocode = checkFieldForXSS($obj, 'aocode:Registration Organisation');
    $gender = checkFieldForXSS($obj, 'gender:Gender');
    $firstName = checkFieldForXSS($obj, 'firstName:First name');
    $firstName = addslashes($firstName);
    $surName = checkFieldForXSS($obj, 'surName:Surname');
    $surName = addslashes($surName);
    $dob = checkFieldForXSS($obj, 'dob:Date of birth');
    $clubid = checkFieldForXSS($obj, 'clubid:Club ID');
    $class = checkFieldForXSS($obj, 'classification:Disability Classification');
    if ($class === '') {
        $class = 0;
    }
    $schoolid = checkFieldForXSS($obj, 'schoolid:School ID');
    include E4S_FULL_PATH . 'athlete/newAthlete.php';
    exit();
}

function e4s_rest_updateAthlete($obj) {

    $id = checkFieldFromParamsForXSS($obj, 'id:Athlete ID');
    $urn = checkFieldFromParamsForXSS($obj, 'URN:Athlete registration id');
    if ($urn === '') {
        $urn = null;
    }
    $aocode = checkFieldFromParamsForXSS($obj, 'aocode:Athlete registration code');
    $gender = checkFieldFromParamsForXSS($obj, 'gender:Athlete gender');
    $firstName = checkFieldFromParamsForXSS($obj, 'firstName:Athlete first name');
    $surName = checkFieldFromParamsForXSS($obj, 'surName:Athlete surname');
    $dob = checkFieldFromParamsForXSS($obj, 'dob:Athlete date of birth');
    $clubid = checkFieldFromParamsForXSS($obj, 'clubid:Club id');
    $class = checkFieldFromParamsForXSS($obj, 'classification:Disability classification');
    if ($class === '') {
        $class = 0;
    }
    $schoolid = checkFieldFromParamsForXSS($obj, 'schoolid:School ID');
    include E4S_FULL_PATH . 'updateAthlete.php';
    exit();
}

//function e4s_rest_AddToCart($obj) {
//    $products = checkFieldFromParamsForXSS($obj,'products:Products');
//    $products = [19602,19603];
//    $params = $obj->get_params("JSON");
//
//    $products = $params['products'];
//
//    include E4S_FULL_PATH . 'dbInfo.php';
//
//    foreach ( $products as $product_id ) {
//        $product_cart_id = WC()->cart->generate_cart_id( $product_id );
//        $in_cart = WC()->cart->find_product_in_cart( $product_cart_id );
//        if (! $in_cart) {
//            WC()->cart->add_to_cart($product_id, 1);
//        }
//    }
//
//    Entry4UISuccess('');
//    exit();
//}

function e4s_rest_deleteUserAthlete($obj) {
    include_once E4S_FULL_PATH . 'admin/userMaint.php';
    e4s_deleteUserAthlete($obj);
}

function e4s_rest_addUserAthlete($obj) {
    include_once E4S_FULL_PATH . 'athlete/userAthletes.php';
    e4s_addUserAthlete($obj);
}

function e4s_rest_loadFromRegistration($obj) {
    include_once E4S_FULL_PATH . 'dbInfo.php';
    $status = checkFieldForXSS($obj, 'status:Status');
    if ($status !== '') {
        $status = strtoupper($status);
    }
    $regdate = checkFieldForXSS($obj, 'regdate:Registration Date');
    if ($regdate === '') {
        $regdate = null;
    }
    $regid = checkFieldForXSS($obj, 'regid:Registration ID');
    if ($regid === '') {
        $regid = null;
    }
    $gender = checkFieldForXSS($obj, 'gender:Gender');
    if ($gender !== '') {
        $gender = strtoupper($gender[0]);
    }
    $firstname = checkFieldForXSS($obj, 'firstname:First name');
    $surname = checkFieldForXSS($obj, 'surname:Surname');
    $dob = checkFieldForXSS($obj, 'dob:Date of birth');
    $club = checkFieldForXSS($obj, 'club:Club');
    $externId = checkFieldForXSS($obj, 'clubid:Club');
    if ( is_null($externId) ){
        // EventMaster send CamelCase
        $externId = checkFieldForXSS($obj, 'clubId:Club');
    }
    $system = checkFieldForXSS($obj, 'system:System');
    if ($system === '' || is_null($system)) {
        $system = E4S_AOCODE_AAI;
    }
    $schoolId = 0;
    $class = checkFieldForXSS($obj, 'class:Disability classification');
    if ($class === '' || is_null($class)) {
        $class = 0;
    }

    include_once E4S_FULL_PATH . 'import/RegToAthlete.php';
    e4s_athleteRegistration($system, $status, $firstname, $surname, $dob, $regid, $gender, $regdate, $club, $externId, $schoolId, $class);
}

function e4s_rest_DeleteProduct($obj) {
    $prodId = checkFieldForXSS($obj, 'prodid:Product ID');
    include_once E4S_FULL_PATH . 'entries/entries.php';
    e4s_removeProductFromBasket($prodId);
}

function e4s_logout() {
    wp_logout();
    do_action('e4s_logout_action');
}

add_action('e4s_logout_action', function () {
//    array_map(function ($k) {
    //setcookie($k, FALSE, time() - (86400 * 30), '/');
//    }, array_keys($_COOKIE));
    // Redirect to 'siteurl' since by default WordPress redirects to its login
    // URL, which actually sets a new cookie
    header('Location: ' . get_option('siteurl'));
    exit();
}, 99999);

function e4s_emailIsOnBlackList($email) {
    $blacklist = array();

    $blacklist['demo.com'] = TRUE;
    $blacklist['mixwi.com'] = TRUE;
    $blacklist['meupd.com'] = TRUE;
    $blacklist['jusuk.com'] = TRUE;
    $blacklist['livingit.site'] = TRUE;

    if (strpos($email, '@') === FALSE) {
        return TRUE;
    }

    if (array_key_exists($email, $blacklist)) {
        return TRUE;
    }

    $emailArr = explode('@', $email);
    if (array_key_exists($emailArr[1], $blacklist)) {
        return TRUE;
    }

    $domainArr = explode('.', $emailArr[1]);
    $count = sizeof($domainArr);

    $tryDomain = '';
    for ($x = $count - 1; $x > -1; $x--) {
        if ($tryDomain !== '') {
            $tryDomain = '.' . $tryDomain;
        }
        $tryDomain = $domainArr[$x] . $tryDomain;
        if (array_key_exists($tryDomain, $blacklist)) {
            return TRUE;
        }
    }
    return FALSE;
}

function e4s_checkWCRegistration($errors, $sanitized_user_login, $user_email) {

    if (e4s_emailIsOnBlackList($user_email)) {
        $errors->add('demo_error', __('<strong>ERROR</strong>: Registration Failed.', 'e4s_emailValidation'));
    }
    if (!array_key_exists('E4S_ALLOW_REGISTRATION', $GLOBALS)) {
        $errors->add('demo_error', __('<strong>ERROR</strong>: Please use the authorised Entry4Sports Registration system.', 'e4s_emailValidation'));
    }
    return $errors;
}

function e4s_sociallogin(){
	if ( class_exists( 'NextendSocialLogin', FALSE ) ) {
		echo NextendSocialLogin::renderButtonsWithContainer();
	}
    exit;
}
function e4s_login_url($url) {
    return '/#/login';
}

add_filter('login_url', 'e4s_login_url', 999, 1);
function e4s_registration_url($url) {
    return '/#/register';
}

add_filter('register_url', 'e4s_registration_url', 999, 1);
add_filter('registration_errors', 'e4s_checkWCRegistration', 10, 3);
function e4s_register($obj) {
    include_once E4S_FULL_PATH . 'user/userregistration.php';
    e4s_UserRegister($obj);
}
function e4s_login() {
}

// CompEventArrs CRUD
function e4s_rest_builderReadCEArr($obj) {
    include E4S_FULL_PATH . 'builder/compEventArrCRUD.php';
    e4s_actionCEArr($obj, E4S_CRUD_READ);
}

function e4s_rest_builderCreateCEArr($obj) {
    include E4S_FULL_PATH . 'builder/compEventArrCRUD.php';
    e4s_actionCEArr($obj, E4S_CRUD_CREATE);
}

function e4s_rest_builderUpdateCEArr($obj) {
    include E4S_FULL_PATH . 'builder/compEventArrCRUD.php';
    e4s_actionCEArr($obj, E4S_CRUD_UPDATE);
}

function e4s_rest_builderDeleteCEArr($obj) {
    include E4S_FULL_PATH . 'builder/compEventArrCRUD.php';
    e4s_actionCEArr($obj, E4S_CRUD_DELETE);
}

function e4s_rest_builderListCEArrs($obj) {
    include E4S_FULL_PATH . 'builder/compEventArrCRUD.php';
    e4s_actionCEArr($obj, E4S_CRUD_LIST);
}

function v_getUrl() {
    $uri = explode('?', $_SERVER['REQUEST_URI'] . '?');
    $url = v_getUrlDomain() . $uri[0];

    return $url;
}

function v_getUrlDomain() {
    $url = isset($_SERVER['HTTPS']) && 'on' === $_SERVER['HTTPS'] ? 'https' : 'http';
    $url .= '://' . $_SERVER['SERVER_NAME'];
    $url .= in_array($_SERVER['SERVER_PORT'], array('80', '443')) ? '' : ':' . $_SERVER['SERVER_PORT'];
    return $url;
}

function v_forcelogin() {
    $GLOBALS['currentUser'] = wp_get_current_user();
    $url = v_getUrl();

    if (!is_user_logged_in() or !isPageAvailable($url)) {
        $redirect_url = apply_filters('v_forcelogin_redirect', $url);
        if (preg_replace('/\?.*/', '', $url) != preg_replace('/\?.*/', '', wp_login_url()) && !isE4SPublicPage($url)) {
            wp_safe_redirect(wp_login_url($redirect_url), 401);
            exit();
        }
    }
}
// IsPublic page ?
function isPageAvailable($url) {

    if ($url !== '') {
        $current_page = $url;
        $current_page = str_replace(v_getUrlDomain(), '', $current_page);
    }
    $current_page = explode('?', $current_page . '?');
    $current_page = strtolower($current_page[0]);
    if (strpos($current_page, E4S_PRIVATE_TEXT . '/') !== FALSE) {
        return isSecureRequestor();
    }
    // not a secure Page
    return TRUE;
}

function e4s_login_scripts() {
    wp_enqueue_style('e4s_login', v_getUrlDomain() . E4S_PATH . '/css/e4s_wplogin.css');
}

function e4s_scripts() {
    if (strtolower($_SERVER['SERVER_NAME']) !== 'dev.entry4sports.com' && strtolower($_SERVER['SERVER_NAME']) !== 'localhost') {
        add_action('init', 'v_forcelogin');
    }
    wp_enqueue_style('mat_icons', 'https://fonts.googleapis.com/icon?family=Material+Icons');
}

add_action('wp_enqueue_scripts', 'e4s_scripts');
add_action('admin_enqueue_scripts', 'e4s_scripts');
add_action('login_enqueue_scripts', 'e4s_login_scripts');

function isE4SPublicPage($url) {
    $retval = FALSE;
    $publicPages = array();
    $current_page = $_SERVER['REQUEST_URI'];
    $domain = strtolower($_SERVER['SERVER_NAME']);

    if ($domain === E4S_CHECK_IN_DOMAIN) {
        return TRUE;
    }
    if ($url !== '') {
        $current_page = $url;
        $current_page = str_replace(v_getUrlDomain(), '', $current_page);
    }
    $completeUrl = $current_page;
    $current_page = explode('?', $current_page . '?');
    $current_page = strtolower($current_page[0]);

    $publicPages[] = '/wp-json/jwt-auth/v1/token';

    if (in_array($current_page, $publicPages)) {
        $retval = TRUE;
    }
    if ($current_page === '/') {
        $retval = TRUE;
    }
    if (strpos($completeUrl, 'event/entries') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($completeUrl, 'action=settimetable') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($completeUrl, '/lap') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($completeUrl, '/touch') !== FALSE) {
        $retval = TRUE;
    }
    // all open for tickets
    if (strpos($completeUrl, 'action=ealive') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, 'user/version') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, 'athlete/getaai') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($completeUrl, 'action=awardcheck') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/cart') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/basket') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/checkout') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/secondary/cust') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/minicart') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/ticket/update') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, 'results/report') !== FALSE) {
        $retval = TRUE;
    }
    // End of open for secondary
    if (strpos($current_page, E4S_TICKET_ROUTE . '/read') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/schedule/list') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/competition/') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/results/pf') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/output/') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/results/lynxpf') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/scoreboard') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/register') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/view/') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/test') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, 'builder/comp') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, 'showinfo.php') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, 'schedule.php') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, 'getentrycounts.php') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, 'getentriestimetable') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, 'getentriestitleinfo') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, 'wp-login') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, 'lost-password') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, 'cron') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/results/read') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/public') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/admin/config') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, 'v3import') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, 'loadathletesdiffv3') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/checkin') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/webresults') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/uploadwpdoc') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, 'help') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, 'showentries') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, E4S_PRIVATE_TEXT . '/') !== FALSE) {
        // athlete registration inbound
        $retval = isSecureRequestor();
    }
    if (strpos($current_page, '/schedule') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, E4S_SECURE_TEXT . '/track/') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, '/uicard/entries') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($completeUrl, '/my-account/?password-reset=true') !== FALSE) {
        $retval = TRUE;
    }
    if (strpos($current_page, E4S_SECURE_TEXT . '/') !== FALSE) {
        $retval = false;
    }
//    if (strpos($current_page, '/event/entries') !== FALSE) {
//        $retval = FALSE;
//    }
    return $retval;
}

function isSecureRequestor() {

    $domain = strtolower($_SERVER['SERVER_NAME']);
    $secureDomains = array();
    $secureDomains[] = E4S_DEV_DOMAIN;
    $secureDomains[] = E4S_AAI_DOMAIN;
    $secureDomains[] = E4S_UK_DOMAIN;

    foreach ($secureDomains as $secureDomain) {
        if ($secureDomain === $domain) {
            return TRUE;
        }
    }

    return isSecureRequestorIP();
}

function isSecureRequestorIP() {
    if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
        $uid = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $uid = $_SERVER['REMOTE_ADDR'];
    }

    $secureUIDs = array();

    $secureUIDs[] = '82.165.80.99'; // E4S Support
    $secureUIDs[] = '217.160.0.219'; // E4S Support
    $secureUIDs[] = '2a00:23c6:b110:a901:e8d3:8ffb:fabd:3aef'; // E4S Support ip6
    $secureUIDs[] = '81.150.168.100'; // Primo
    $secureUIDs[] = '34.250.83.190'; // Event Master ?
    $secureUIDs[] = '3.251.237.247'; // Event Master 2022?
    $secureUIDs[] = '109.158.219.116'; // API

    foreach ($secureUIDs as $secureUID) {
        if ($secureUID === $uid) {
            return TRUE;
        }
    }

    return FALSE;
}

//if ( !function_exists( 'is_cart' ) ) {
//    require_once '/includes/wc-conditional-functions.php';
//}

function e4s_rest_scanTicket($obj) {
    e4s_readTicket($obj, TRUE);
}

function e4s_rest_nonScannedTicket($obj) {
    e4s_readTicket($obj, FALSE);
}

function e4s_readTicket($obj, $scanned) {
    include_once E4S_FULL_PATH . 'dbInfo.php';
    $ticket = new e4sTicket();
    $ticket->readTicket($obj, $scanned);
}

function e4s_rest_reportTicket($obj) {
    include_once E4S_FULL_PATH . 'dbInfo.php';
    $ticket = new e4sTicket();
    $ticket->report($obj);
}

function e4s_rest_listAthleteTicket($obj) {
    include_once E4S_FULL_PATH . 'dbInfo.php';
    $ticket = new e4sTicket();
    $ticket->list($obj, TRUE);
}

function e4s_rest_listTicket($obj) {
    include_once E4S_FULL_PATH . 'dbInfo.php';
    $ticket = new e4sTicket();
    $ticket->list($obj, FALSE);
}

function e4s_rest_listAllTicket($obj) {
    include_once E4S_FULL_PATH . 'dbInfo.php';
    $ticket = new e4sTicket();
    $ticket->listAll($obj);
}

function e4s_rest_updateTicket($obj) {
    include_once E4S_FULL_PATH . 'dbInfo.php';
    $ticket = new e4sTicket();
    $ticket->update($obj);
}

function e4s_rest_allocateTicket($obj) {
    include_once E4S_FULL_PATH . 'dbInfo.php';
    $ticket = new e4sTicket();
    $ticket->allocate($obj);
}

function e4s_rest_clearTicket($obj) {
    include_once E4S_FULL_PATH . 'dbInfo.php';
    $ticket = new e4sTicket();
    $ticket->clear($obj);
}

function e4s_rest_generateTicket1Off($obj) {
    include_once E4S_FULL_PATH . 'dbInfo.php';
    e4s_generateAndEmailTickets($obj);
}

//T&F Card Routes
function e4s_rest_getCardInfo($obj) {
    include_once E4S_OTD_PATH . '/tfCards.php';
    e4s_getBaseCardInfo($obj);
}

function e4s_rest_displayCard($obj) {
    include_once E4S_OTD_PATH . '/tfCards.php';
    e4s_displayCards($obj);
}

function e4s_rest_saveSignature($obj) {
    include_once E4S_OTD_PATH . '/tfCards.php';
    e4s_saveSignature($obj);
}

function e4s_add_custom_default_download($downloads) {
    $GLOBALS[E4S_NOHEADERS] = TRUE;
    require_once E4S_FULL_PATH . 'dbInfo.php';
//    var_dump($downloads);
    $ticket = new e4sTicket();
    return $ticket->setDownloadLinks($downloads);
}

function e4s_account_downloads_columns($columns) {
    $columns = array('download-product' => 'Ticket Details', 'download-e4s' => 'E4S Ticket Information', 'download-file' => 'Download');
//    { []=> string(7) "Product" ["download-remaining"]=> string(19) "Downloads remaining" ["download-expires"]=> string(7) "Expires" ["download-file"]=> string(8) "Download" ["download-actions"]=> string(6) " " }
    return $columns;
}

function e4s_setHeader() {
    $page_id = get_queried_object_id();
    $page_object = get_page($page_id);
    $content = null;
    if (isset($page_object->post_content)) {
        $content = $page_object->post_content;
    }
    if (is_page('cart') || is_cart() || has_shortcode($content, 'woocommerce_checkout') || has_shortcode($content, 'woocommerce_my_account')) {
        get_header('e4s');
    }
}

function e4s_checkForSecondary($cart_item_key, $cart) {
    $GLOBALS[E4S_NOHEADERS] = TRUE;
//    var_dump("e4s_checkForSecondary");
    require_once E4S_FULL_PATH . 'dbInfo.php';
    $cartProduct = $cart->cart_contents[$cart_item_key];

    if ($cartProduct['variation_id'] === 0) {
        $product = wc_get_product($cartProduct['product_id']);
        $obj = e4s_getWCProductDescObj($product);
        if (isset($obj->compid)) {
            include_once E4S_FULL_PATH . 'dbInfo.php';
            $model = new stdClass();
            $model->prod = new stdClass();
            $model->prod->id = $cartProduct['product_id'];
            $e4sProduct = new e4sProduct($model, $obj->compid);
            if (isset($obj->athleteid)) {
                $e4sProduct->removeSecondary($obj->athleteid);
            }
        }
    }
}

function e4s_getCartProduct($cartObj, $cart_item, $cart_item_key){
    $desc = $cartObj->get_description();
    include_once E4S_FULL_PATH . 'dbInfo.php';
    $descObj = e4s_getOptionsAsObj($desc);
    if ( isset($descObj->compid) and isset($descObj->athleteid) ){

        if ( !isset($descObj->waiting) ){
            $descObj->waiting = false;
        }
        $countObj = e4s_getEntryCountObj($descObj->compid);
        $entryObj = $countObj->getAthleteEntryForCeId($descObj->athleteid, $descObj->ceid);
        if ( $entryObj->waitingPos > 0 ){
            $onWaitingList = true;
        }else{
            $onWaitingList = false;
        }
        if ( $onWaitingList !== $descObj->waiting ) {
            $descObj->waiting = $onWaitingList;
            $cartObj->set_description(e4s_getOptionsAsString($descObj));
            $cartObj->save();
        }
    }

    return $cartObj;
}
function e4s_getCheckoutProductName($name, $cart_item, $cart_key) {
    return e4s_getCartProductName($name, $cart_item, $cart_key, 'C');
}
function e4s_getCartProductName($name, $cart_item, $cart_key, $from = 'B') {
    include_once E4S_FULL_PATH . 'dbInfo.php';
    $retVal = $name;
    $productId = $cart_item['product_id'];
    if (array_key_exists('variation_id', $cart_item)) {
        $variantId = $cart_item['variation_id'];

        if ((int)$variantId !== 0) {
            $productId = (int)$variantId;
        }
    }
    $product = wc_get_product($productId);
    $descObj = e4s_getWCProductDescObj($product);

    if (isset($descObj->ticket)) {
        $descObj->qty = $cart_item['quantity'];
        $retVal = e4s_getTicketDisplay($descObj, $from, $product);
    }
    if (strpos($name, ': ') !== FALSE) {
        // Entry
        $arr = explode('>', $name);
        if (sizeof($arr) > 1) {
            $prodName = $arr[1];
        } else {
            $prodName = $name;
        }

        $noNumber = explode(': ', $prodName);
        if (sizeof($noNumber) > 1) {
            $noNumber = $noNumber[1];
            $noNumber = str_replace('</a', '', $noNumber);
            $noNumberArr = explode('. ', $noNumber);
            $athlete = explode('(', $noNumberArr[1])[0];
            $obj = new stdClass();
            $obj->compId = $descObj->compid;
            $compObj = e4s_getCompObj($obj->compId);
            $obj->title = trim($compObj->getName());
            if (isset($descObj->teamName)) {
                $athlete = $descObj->teamName;
            }
            if (isset($descObj->athlete)) {
                $athlete = $descObj->athlete;
            }
            $obj->athlete = $athlete;
            $event = $noNumberArr[2];
            if (isset($descObj->event)) {
                $event = $descObj->event;
            }
            $obj->event = $event;
            $obj->ceId = $descObj->ceid;
            $obj->eventLabel = 'Event';
            $obj->entryObj = null;
            if (isset($descObj->athleteid)) {
                $countObj = e4s_getEntryCountObj($obj->compId);
                $obj->athleteLabel = 'Athlete';
                $entryObj = $countObj->getAthleteEntryForCeId($descObj->athleteid, $descObj->ceid);
                $obj->entryObj = $entryObj;
            } else {
                $obj->athleteLabel = 'Team';
            }
//            $product->save_meta_data();
            $retVal = e4s_showAthleteEventInfo($obj);
        } else {
            $retVal = '!-! ' . $name;
        }
    }

    return $retVal;
}
function e4s_isCarPark($name){
    $name = strtolower($name);
    $carPark = strtolower(E4S_CAR_PARK_TICKET);

    return strpos($name, $carPark) !== false;
}
function checkTicketCarPark($obj = null){
    $carPark = E4S_CAR_PARK_TICKET;
    $carParkCount = 0;

    if ( !is_null($obj)) {
        if (e4s_isCarPark($obj->ticket) ) {
            if ( isset($obj->qty) ){
                $carParkCount += $obj->qty;
            }else{
                $carParkCount++;
            }
        }
    }

    return $carParkCount;
}

add_action( 'woocommerce_checkout_create_order_line_item', 'custom_checkout_create_order_line_item', 20, 4 );
function custom_checkout_create_order_line_item( $item, $cart_item_key, $values, $order ) {
    // Update order item meta
    $product = $values['data'];
    $field = e4s_getCarRegField($product->get_id());
    if (e4s_isCarPark($product->get_name() ) ) {
        if (array_key_exists($field, $_REQUEST)) {
            $carReg = $_REQUEST[$field];
            if (!empty($carReg)) {
                $item->update_meta_data(E4S_CAR_REG, $carReg);
            }
        }
    }
}
function e4s_getTicketDisplay($obj, $from, WC_Product $product) {
    $html = '<ul class="wc-item-meta">';
    if (isset($obj->compid)) {
        $html .= '<li><span class="wc-item-meta-label">Competition :</span>';
        $html .= '<strong>' . $obj->compid . ':' . $obj->compname . '</strong></li>';
    }
    $html .= '<li><span class="wc-item-meta-label">Ticket : ';
    $html .= '</span><strong>' . $obj->ticket . '</strong></li>';
    if ($from === 'C') { // checkout
        $carParkCount = checkTicketCarPark($obj);
        if ($carParkCount > 0) {
            $txt = 'Please Enter the Registration of your car for the car park.';
            if ($carParkCount > 1) {
                $txt = 'Please Enter the Registration of the ' . $carParkCount . ' cars for the car park separated by a comma.';
            }
            $txt .= ' Failure to complete the correct registration will require further payment on the day.';
            $fieldHTML = woocommerce_form_field(e4s_getCarRegField($product->get_id()), array('type' => 'text', 'carRegCnt'  => $carParkCount,'class' => array('form-row-last'), 'label' => $txt, 'placeholder' => 'Enter Car Registration', 'required' => TRUE), '');

            $html .= $fieldHTML;
        }
    }
    $shortDesc = $product->get_short_description();
    if ( $shortDesc !== '' ) {
        $shortDesc = e4s_decodeHTML($shortDesc);
        $html .= '<p>'.$shortDesc.'</p>';
    }
    $html .= '</ul>';
    return $html;
}
function e4s_getCarRegField($id){
    return E4S_CAR_REG. '_' . $id;
}
add_filter('woocommerce_form_field_text','e4s_wc_field', 10, 4);
function e4s_wc_field($field,$key,$args, $value){
    if ( str_starts_with($key,E4S_CAR_REG )){
        $html = str_replace('/>',' onchange="checkCarReg(' . $args['carRegCnt'] . ',\'' . $key. '\');return false;" />', $field);
        $html .= "<script>function checkCarReg(carRegCnt, fieldName){
            let regValue = $('#' + fieldName).val();
            let regArr = regValue.split(',');
            let errors = false;
            let processed = [];
            let processedNoSpace = [];
            if ( regArr.length !== carRegCnt ){
                errors = true;
            }else{
                for(let r in regArr){
                    let reg = regArr[r].trim();
                    let regNoSpace = reg.replace(/ /g,'');
                    
                    if (regNoSpace.length > 7){
                        errors = true;
                    }
                    if (typeof processed[regNoSpace] !== 'undefined'){
                        errors = true;
                    }
                    processedNoSpace[regNoSpace] = reg;
                    processed.push(reg);
                }
            }
            if ( errors ){
                if ( carRegCnt === 1 ){
                    alert('Please enter a valid registration number');
                }else{
                    alert('Please enter ' + carRegCnt + ' valid distinct registration numbers separated by a comma');
                }
                $('#' + fieldName).val('');
            }else{
                $('#' + fieldName).val(processed.join().toUpperCase());
            }
        }</script>";
        return $html;
    }
    return $field;
}
add_action( 'woocommerce_checkout_process', 'privacy_checkbox_error_message' );
function privacy_checkbox_error_message() {
    foreach($_POST as $key=>$value){
        if ( str_starts_with($key, E4S_CAR_REG) ){
            if ( $value === '' ){
                wc_add_notice(__('Let us know your car registrations please before processing'), 'error');
            }
        }
    }
}

function e4s_showAthleteEventInfo($obj) {
    $html = '<ul class="wc-item-meta">';

    if (isset($obj->title)) {
        global $tz;
        $compObj = e4s_GetCompObj($obj->compId);
        if (isset($obj->ceId)) {
            $compDate = $compObj->getEventDate($obj->ceId);
        } else {
            $compDate = $compObj->getDate();
        }
        $compDate = DateTime::createFromFormat(E4S_SHORT_DATE, $compDate, $tz);
        $compDate = $compDate->format('D ' . E4S_FORMATTED_DATE);

        $html .= '<li><span class="wc-item-meta-label">Competition :</span>';
        $html .= '<strong>' . $obj->compId . ':' . $obj->title . '</strong></li>';
        $html .= '<li><span class="wc-item-meta-label">Date : ';
        $html .= '</span><strong>' . $compDate . '</strong></li>';

        if (isset($obj->entryObj) and $compObj->isWaitingListEnabled()) {
            $waitingPos = $obj->entryObj->waitingPos;
            $origWaitingPos = $waitingPos;
            if ($waitingPos === E4S_ENTRY_INVALID ){
                $obj->event = 'Invalid Event';
            }

            if ( isset($obj->entryObj->entryOptions)) {
                $eOptions = e4s_addDefaultEntryOptions($obj->entryObj->entryOptions);
                if ($eOptions->waitingInfo->originalPos !== $waitingPos) {
                    $origWaitingPos = $eOptions->waitingInfo->originalPos;
                }
            }
            if ($waitingPos > 0) {
                $html .= '<li>
                    <span class="wc-item-meta-label e4s_waiting_list" style="color:red; font-weight: bold;">Current Waiting Position:</span><strong>' . $waitingPos . '</strong>
                </li>';
                if ( $origWaitingPos !== $waitingPos){
                    $html .= '<li>
                        <span class="wc-item-meta-label e4s_waiting_list" style="color:black; font-weight: bold;">Initial Waiting Position:</span><strong>' . $origWaitingPos . '</strong>
                    </li>';
                }
            }else{
                if ( $origWaitingPos !== $waitingPos){
                    $html .= '<li>
                        <span class="wc-item-meta-label e4s_waiting_list" style="color:red; font-weight: bold;">No longer on Waiting list</span><strong>Confirmed Entry</strong>
                    </li>';
                    $html .= '<li>
                        <span class="wc-item-meta-label e4s_waiting_list" style="color:black; font-weight: bold;">Initial Waiting Position:</span><strong>' . $origWaitingPos . '</strong>
                    </li>';
                }
            }
        }
    }

    $html .= '<li>
                <span class="wc-item-meta-label">' . $obj->athleteLabel . ':</span><strong>' . $obj->athlete . '</strong>
            </li>
            <li>
                <span class="wc-item-meta-label">' . $obj->eventLabel . ':</span><strong>' . $obj->event . '</strong>
            </li>
         </ul>';
    return $html;
}

function add_attributes_to_cart_item($item_data, $cart_item) {
    $newItem_data = array();
    foreach ($item_data as $item) {
        $newItem_array = array();
        $checkElement = FALSE;
        foreach ($item as $key => $element) {
            if ($key === 'key') {
                if ($element[0] === '_') {
                    $element = ucfirst(str_replace('_', '', $element));
                    $checkElement = TRUE;
                }
            }
            if ($key === 'value' and $checkElement) {
                if (strpos($element, ': ')) {
                    $element = explode(': ', $element)[1];
                }
            }
            $newItem_array[$key] = $element;
        }
        $newItem_data[] = $newItem_array;
    }

    return $newItem_data;
}

function e4s_getPriceForCart($price, $cartItem, $cartKey) {

    if (array_key_exists(E4S_IN_MINI_CART,$GLOBALS)){
        return $price;
    }
    $priceArr = explode('</span>', $price);
    $actualPrice = (float)$priceArr[1];
    $netPrice = 0;
    if ($actualPrice > 0) {
        $netPrice = $actualPrice - .35;
        $netPrice = $netPrice / 1.05;
        $fee = $actualPrice - $netPrice;
        if ($fee < .5) {
            $netPrice = $actualPrice - 0.5;
        }
    }
    return $priceArr[0] . '</span>' . number_format($netPrice, 2) . '</span>';
}

function e4s_getFeesForCart($actualPrice, $cartItem, $cartKey) {
    $fee = 0;

    if ($actualPrice > 0) {
        $netPrice = $actualPrice - .35;
        $netPrice = $netPrice / 1.05;
        $fee = $actualPrice - $netPrice;
        if ($fee < 0.5) {
            $fee = 0.5;
        }
    }
    return '<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">' . get_woocommerce_currency_symbol() . '</span>' . number_format($fee, 2) . '</span>';
}

function e4s_afterCartNotice() {
    echo 'E4S Processing Fees are non-refundable and no entry is confirmed until paid for. <a href="/tandc">Click Here</a> to see our refunds policy.<br>';
    echo '<div id="e4sWaitingListDesc" style="display:none;">';
    echo '<span style="color:red;font-weight: bold;">Waiting Lists</span>: The system should send out emails if you are moved off the waiting list into the event but due to Spam/Lost emails, <b>IT IS YOUR RESPONSIBILTY</b> to check before the Competition to see if you have made the event or not.<br>';
    echo 'If you are still on the Waiting list after the competition, you will be refunded minus processing fees.';
    echo '</div>';
}

function e4s_display_item_meta($item) {
    $linkedAthlete = '';
    $strings = array();
    $html = '';
    $args = array('before' => '<ul class="wc-item-meta"><li>', 'after' => '</li></ul>', 'separator' => '</li><li>', 'echo' => TRUE, 'autop' => FALSE,);

    // First param is prefix to ignore. default is _ but we want _athlete
    foreach ($item->get_formatted_meta_data('-_-', TRUE) as $meta_id => $meta) {
        if ($meta->key === E4S_ATTRIB_ATHLETE or $meta->key[0] !== '_') {
            $value = $args['autop'] ? wp_kses_post($meta->display_value) : wp_kses_post(make_clickable(trim(strip_tags($meta->display_value))));
            if ($meta->key === E4S_ATTRIB_ATHLETE) {
                $meta->display_key = 'Athlete';
                $linkedAthlete = $value;
            }
            if ($meta->key === E4S_CAR_REG) {
                $meta->display_key = E4S_CAR_REG_LABEL;
                $linkedAthlete = '';
            }
            $strings[] = '<strong class="wc-item-meta-label">' . wp_kses_post($meta->display_key) . ':</strong> ' . $value;
        }
    }

    if ($strings) {
        $html = $args['before'] . implode($args['separator'], $strings) . $args['after'];
    }

    $html = apply_filters('woocommerce_display_item_meta', $html, $item, $args);

//    if ( $args['echo'] ) {
    echo $html;
//    } else {
    return $linkedAthlete;
}

function e4s_showCheckOutQty($item_qty, $cart_item, $cart_item_key) {
    return _e4s_ShowQty($item_qty, $cart_item['data']);
}

function e4s_showQty($item_qty, $item) {
    return _e4s_ShowQty($item_qty, $item->get_product());
}

function _e4s_ShowQty($item_qty, $product) {
    $GLOBALS[E4S_NOHEADERS] = TRUE;
//    var_dump("_e4s_ShowQty");
    require_once E4S_FULL_PATH . 'dbInfo.php';

    $descObj = e4s_getWCProductDescObj($product);
    if (isset($descObj->athlete)) {
        $item_qty = '';
    }
    return $item_qty;
}

function e4s_getOrderProdName($name, $item, $third) {

    $GLOBALS[E4S_NOHEADERS] = TRUE;
    require_once E4S_FULL_PATH . 'dbInfo.php';
    $varId = $item->get_variation_id();
    if ($varId !== 0) {
        $product = wc_get_product($varId);
    } else {
        $product = $item->get_product();
    }

    $descObj = e4s_getWCProductDescObj($product);

    if (isset($descObj->athlete)) {
        $obj = new stdClass();
        $obj->title = $descObj->compname;
        $obj->compId = $descObj->compid;
        $obj->ceId = $descObj->ceid;
        $obj->athlete = $descObj->athlete;
        $obj->athleteLabel = 'Athlete';
        $obj->event = $descObj->event;
        $obj->eventLabel = 'Event';

        $countObj = e4s_getEntryCountObj($obj->compId);
        $entryObj = $countObj->getAthleteEntryForCeId($descObj->athleteid, $descObj->ceid);
        $obj->entryObj = $entryObj;
        $name = e4s_showAthleteEventInfo($obj);
    }

    if (isset($descObj->ticket)) {
        $name = e4s_getTicketDisplay($descObj, 'O', $product);
    }

    $GLOBALS[E4S_ORDER_ITEM] = $descObj;

    return $name;
}

function e4s_getCartThumbnail($product_get_image, $cart_item, $cart_item_key) {
    $GLOBALS[E4S_NOHEADERS] = TRUE;
//    var_dump("e4s_getCartThumbnail");
    include_once E4S_FULL_PATH . 'dbInfo.php';
    $product = $cart_item['data'];
    $descObj = e4s_getWCProductDescObj($product);

    if (isset($descObj->athlete)) {
        // Athlete Entry
        $compObj = e4s_GetCompObj($descObj->compid, TRUE);

        $product_get_image = '<img width="300" height="300" 
                                src="' . $compObj->getLogo() . '" 
                                class="woocommerce-placeholder wp-post-image" 
                                alt="Placeholder"
                              >';
    }
    if (isset($descObj->ticket)) {
        // Secondary Ticket
        $product_get_image = '<img width="300" height="300" 
                                src="' . E4S_PATH . '/css/images/qr.png" 
                                class="woocommerce-placeholder wp-post-image" 
                                alt="Placeholder"
                              >';
    }

    return $product_get_image;
}

function e4s_getProductPermalink($product_get_permalink_cart_item, $cart_item, $cart_item_key) {
    return FALSE;
}

function e4s_woocommerce_cart_emptied() {
    if (isset($_REQUEST['empty-cart']) && $_REQUEST['empty-cart'] === 'clearcart') {
        // Only do if emptying the cart form the button
        $GLOBALS[E4S_NOHEADERS] = TRUE;
//        var_dump("e4s_woocommerce_cart_emptied");
        include_once E4S_FULL_PATH . 'dbInfo.php';
        e4sCart::cartEmptied();
    }
}

function e4s_downloadsPermitted($allowed, $instance) {
    return TRUE;
}

function e4s_Thankyou($order_id) {
    if (!$order_id) {
        return;
    }

    $order = wc_get_order($order_id);
    $order->update_status('completed');

    $processed = get_post_meta($order_id, E4S_CREDIT_KEY, TRUE);
    if (!$processed) {
        $e4sObj = new e4sCredit(get_current_user_id());
        $e4sObj->updateCreditFromOrder($order);

        $ticketObj = new e4sTicket();
        $ticketObj->emailAthletes($order_id);

        // Flag the action as done (to avoid repetitions on reload for example)
        $order->update_meta_data(E4S_CREDIT_KEY, TRUE);
        $order->save();
    }
}
function e4s_setMailCallback($mail, $instance) {
    include_once E4S_FULL_PATH . 'builder/messageCRUD.php';
    return 'e4s_mail';
}

function e4s_getCompsFromOrder($order): array {
    if (!array_key_exists(E4S_ORDER_COMPS, $GLOBALS)) {
        $items = $order->get_items();
        $compObjs = array();
        foreach ($items as $item) {
            $product = $item->get_product();
            if ($product !== FALSE) {
                $desc = $product->get_description();
                if ($desc !== '') {
                    $desc = e4s_getOptionsAsObj($desc);
                    if (isset($desc->compid)) {
                        $compId = (int)$desc->compid;
                        if (!array_key_exists($compId, $compObjs)) {
                            $compObjs[$compId] = e4s_GetCompObj($compId);
                        }
                    }
                }
            }
        }
        $GLOBALS[E4S_ORDER_COMPS] = $compObjs;
    }
    return $GLOBALS[E4S_ORDER_COMPS];
}

function e4s_order_footer($order) {
    include_once E4S_FULL_PATH . 'dbInfo.php';
    $compObjs = e4s_getCompsFromOrder($order);
    $newMessages = FALSE;
    $isTicketPrintRequired = FALSE;
    foreach ($compObjs as $compObj) {
        $msgObj = new e4SMessageClass();
        $obj = new stdClass();
        $obj->compId = $compObj->getID();
        if ($compObj->isShopOnly()) {
            $isTicketPrintRequired = TRUE;
        }
        if ($msgObj->checkNewEntry($obj)) {
            $newMessages = TRUE;
        }
    }
    if ($isTicketPrintRequired) {
        echo '<br><br><b>There are tickets enclosed in this order. Please print them off now in case you do not receive an email or have entered your email incorrectly.<br>Emails are sent for all orders and if they are not in your inbox, please check your Junk/Spam folders<br>';
        echo 'Alternatively, please bookmark this page to get to this order summary at any time.</b><br>';
    }
    if ($newMessages) {
        echo '<br><br><a href="/#/email-messages" style="background:red !important" class="button">Read Messages</a> There are unread messages for the event(s) you have entered.<br>';
    }
}

function e4s_emailCustomerFooter($order, $sent_to_admin, $plain_text, $email) {

//    echo "<b>Tickets:</b> The Entry4Sports System automatically generates a ticket for each Athlete entered alongside any purchased tickets. They may/may not be processed by the organiser of this event.<br>";

    $compObjs = e4s_getCompsFromOrder($order);
    foreach ($compObjs as $compObj) {
        $footerText = $compObj->getEmailFooter();
        if ($footerText !== '') {
            echo '<B>' . $compObj->getName() . ' Information</B>:' . $footerText . '<br>';
        }
    }
    echo '<b>Transaction Fees:</b>These are paid at source and are non-refundable.<br>';
    echo '<br>';
    echo "<b>Amend Your Entry:</b>If your need to pull out or switch to another event, please click the \"Entry Options\" link below your entry in this email or re-visit the competition in Entry4Sports.<br>";
    echo '<br>';
}

// remove columns and the modified template doesnt show the downloads section
//as we are putting the tickets in the details
function e4s_emailDownloadColumns($columns) {
    $columns = array();

    return $columns;
}

// define the woocommerce_quantity_input_max callback
function e4s_getMaxOrderQty($qtyArray, $product) {
//    $defaults = array(
//        'input_name' => 'quantity',
//        'input_value' => '1',
//        'max_value' => apply_filters( 'woocommerce_quantity_input_max', -1, $product ),
//        'min_value' => apply_filters( 'woocommerce_quantity_input_min', 0, $product ),
//        'step' => apply_filters( 'woocommerce_quantity_input_step', 1, $product ),
//        'pattern' => apply_filters( 'woocommerce_quantity_input_pattern', has_filter( 'woocommerce_stock_amount', 'intval' ) ? '[0-9]*' : '' ),
//        'inputmode' => apply_filters( 'woocommerce_quantity_input_inputmode', has_filter( 'woocommerce_stock_amount', 'intval' ) ? 'numeric' : '' ),
//        'cart_item' => cart_item . Added by E4S
//    );

    $GLOBALS[E4S_NOHEADERS] = TRUE;
//    var_dump("e4s_getMaxOrderQty", $qtyArray);
    return $qtyArray;
//    include_once E4S_FULL_PATH . 'dbInfo.php';
//    $maxValue = e4sCart::getMaxQty($qtyArray, $product);
//    $qtyArray['max_value'] = $maxValue;
//
//    return $qtyArray;
}

function e4s_billingFields($fields, $key, $value) {
//    label string
//    required boolean
//    class  array
//    autocomplete value
//    priority int
    $class = $fields['class'];
    $priority = $fields['priority'];
    $label = $fields['label'];
    switch ($label) {
        case 'First name':
            $priority = 10;
            break;
        case 'Last name':
            $priority = 20;
            break;
        case 'Street address':
            $priority = 30;
            break;
        case 'Town / City':
            $class[0] = 'form-row-first';
            $priority = 40;
            break;
        case 'State / County':
            $class[0] = 'form-row-last';
            $priority = 50;
            break;
        case 'Postcode / ZIP':
            $class[0] = 'form-row-first';
            $priority = 60;
            break;
        case 'Country / Region':
            $class[0] = 'form-row-last';
            $priority = 100;
            break;
        case 'Phone':
            $class[0] = 'form-row-first';
            $priority = 110;
            break;
        case 'Email address':
            $class[0] = 'form-row-last';
            $priority = 120;
            break;
    }

//    var_dump ($label . "->" .$priority);
    $fields['priority'] = $priority;
    $fields['class'] = $class;
    return $fields;
}

function e4s_after_remove_product_from_cart($removed_cart_item_key, $cart) {
    $line_item = $cart->removed_cart_contents[$removed_cart_item_key];
    $product_id = $line_item['product_id'];
    $GLOBALS[E4S_NOHEADERS] = TRUE;
    include_once E4S_FULL_PATH . 'dbInfo.php';
    e4sCart::itemRemovedFromCart($product_id);
}

function e4s_order_item_end($item_id, $item, $order) {
    if (array_key_exists(E4S_ORDER_ITEM, $GLOBALS)) {
        include_once E4S_FULL_PATH . 'entries/commonEntries.php';
        e4s_generateOptionsLink();
    }
}

function e4s_CompStatusEmail() {
    include_once E4S_FULL_PATH . 'admin/cronFunctions.php';
    e4s_cronCompStatusEmail();
}

function e4s_cronimport_irish_athletes_loadv3() {
    include_once E4S_FULL_PATH . 'admin/cronFunctions.php';
    import_irish_athletes_loadv3();
}

function import_irish_athletes_loadflatv3() {
    $GLOBALS['e4s_import_id'] = 1;
    $GLOBALS[E4S_CRON_USER] = TRUE;
    include_once E4S_FULL_PATH . 'import/loadAthletesManual.php';
}
function import_irish_athletes_check() {
    $GLOBALS['e4s_import_id'] = 1;
    $GLOBALS[E4S_CRON_USER] = TRUE;
    include_once E4S_FULL_PATH . 'import/loadAthletesCheck.php';
}
function e4s_cronimport_northern_irish_athletes() {
    include_once E4S_FULL_PATH . 'admin/cronFunctions.php';
    import_northern_irish_athletes();
}

function ReportOnPaymentStatus() {
    include_once E4S_FULL_PATH . 'admin/cronFunctions.php';
    e4s_reportOnCompStatus();
}

function e4s_entryCheckerUpdate() {
    include_once E4S_FULL_PATH . 'admin/cronFunctions.php';
    e4s_entryCheckUpdate();
}

function e4s_entriesClosing() {
    include_once E4S_FULL_PATH . 'admin/cronFunctions.php';
    e4s_CheckForEntriesClosing();
}

function e4s_orderCheckerUpdate() {
    include_once E4S_FULL_PATH . 'admin/cronFunctions.php';
    e4s_checkFailedOrders();
}

function e4s_testCron() {
    include_once E4S_FULL_PATH . 'admin/cronFunctions.php';
    e4s_test();
}

function e4s_healthMonitorUpdate() {
    include_once E4S_FULL_PATH . 'admin/cronFunctions.php';
    e4s_cronHealthMonitor();
}

function e4s_cronSendOutstandingEmails() {
    include_once E4S_FULL_PATH . 'admin/cronFunctions.php';
    e4s_sendOutstandingEmails();
}

// E4S Shortcuts
function e4s_isWPShortCode($shortCode) {
    $ignore = ['admin' => TRUE, 'wp-admin' => TRUE, 'login' => TRUE, 'logout' => TRUE, 'basket' => TRUE, 'cart' => TRUE, 'favicon.ico' => TRUE];
    if (array_key_exists($shortCode, $ignore)) {
        return TRUE;
    }
    if (strpos($shortCode, '.php') > 0) {
        return TRUE;
    }
    return FALSE;
}

function e4s_checkFailURIs($uri) {
    $msg = 'Sorry, try elsewhere';
    $failURIs = array();
    $failURIs[] = '/robots.txt';
    $failURIs[] = '/wp-sitemap.xml';
    $failURIs[] = '/xmlrpc.php';

    foreach ($failURIs as $failURI) {
        if ($uri === $failURI) {
            exit($msg);
        }
    }
}

function e4s_redirect_post_location() {
    if ( !array_key_exists('HTTP_HOST', $_SERVER ) or !array_key_exists('QUERY_STRING', $_SERVER )){
        return TRUE;
    }
    $domain = $_SERVER['HTTP_HOST'];
    $qs = $_SERVER['QUERY_STRING'];
    $uri = $_SERVER['REQUEST_URI'];
    $targetLocation = '';

    $uri = trim($uri);
    e4s_checkFailURIs($uri);
//    if (stripos($qs, "fbclid") !== FALSE) {
    if (true) {
        // link from facebook
        $uri = str_replace('?' . $qs, '', $uri);

        if (substr($uri, -1) === '/') {
            $uri = substr($uri, 0, strlen($uri) - 1);
        }
        $uri = explode('/', $uri);

        $rostermId = 0;
        $isRoster = false;
        if (sizeof($uri) > 1 ) {
	        if ( strtolower( $uri[1] ) === "roster" or strtolower( $uri[1] ) === "rostertouch" or strtolower( $uri[1] ) === "rostercallroom" ) {
		        $isRoster = TRUE;
	        }
        }
        if ( $isRoster and sizeof($uri) === 3){
            $rostermId = (int)$uri[2];
            unset($uri[2]);
        }
        if (sizeof($uri) === 2) {
            $shortCode = $uri[1];
            $compId = 0;
            $GLOBALS['e4s_check_shortcode'] = TRUE;
            include_once E4S_FULL_PATH . 'dbInfo.php';
            if (is_numeric($shortCode)) {
                $compId = (int)$shortCode;
                if ((string)$compId === $shortCode) {
                    $targetLocation = '/#/show-entry/' . $compId;
                }
            } else {
                if ( strtolower($shortCode) === 'cart'){
                    if ( e4s_getUserID() === E4S_USER_NOT_LOGGED_IN ) {
                        $targetLocation = '/#/cart-redirect';
                    }else{
                        $targetLocation = '/basket';
                    }
                }else {
                    if ( $isRoster ){
	                    if ( strtolower($shortCode) === "rostertouch"){
		                    $targetLocation = '/entry/v5/scoreboards/rostertouchv2.php/' . $rostermId;
	                    }else{
		                    $targetLocation = '/entry/v5/scoreboards/controllerV2.php/' . $rostermId;
                        }
                    }else {
	                    // check for comp short desc and get compid
	                    if ( ! e4s_isWPShortCode( $shortCode ) ) {
		                    $compId = e4s_getShortCode( $shortCode );
		                    if ( $compId > 0 ) {
			                    $targetLocation = '/#/show-entry/' . $compId;
		                    } else {
//                        logTxt("Shortcode return {$compId} : " . $shortCode);
		                    }
	                    }
                    }
                }
            }
        } else {
            if (sizeof($uri) === 4 and is_numeric($uri[1]) and strtolower($uri[2]) === 'today') {
                include_once E4S_FULL_PATH . 'dbInfo.php';
                $compId = e4sCompetition::getNextCompForLocation($uri[1]);
                if ( $compId > 0) {
                    // delete element from array $uri and return modified array
                    $uri[1] = $compId;
                    $uri[2] = $uri[3];
                    unset($uri[3]);
                }else{
                    Entry4UIError(4000, 'No future competitions defined.');
                }
            }
            if (sizeof($uri) === 3 and is_numeric($uri[1])) {
                $compId = (int)$uri[1];
                if ((string)$compId === (string)$uri[1]) {
                    switch ($uri[2]) {
	                    case 'getorder':
		                    $targetLocation = E4S_FUNCTIONS_ROUTE_PATH . '/getorder/' . $compId;
		                    break;
                        case 'lap':
                            $targetLocation = E4S_FUNCTIONS_ROUTE_PATH . '/lap?compid=' . $compId;
                            break;
                        case 'entries':
//                          /entry/v5/competition/showEntriesV2.php?compid=216
                            $targetLocation = E4S_PATH . 'competition/showEntriesV2.php?compid=' . $compId;
                            break;
                        case 'entrystatus':
//                          /entry/v5/competition/showEntriesV2.php?compid=216
                            $targetLocation = E4S_FUNCTIONS_ROUTE_PATH . '/competition/entrystatus/' . $compId;
                            break;
                        case 'entriesv2':
//                          /entry/v5/competition/showEntriesV2.php?compid=216
                            $targetLocation = E4S_FUNCTIONS_ROUTE_PATH . '/public/showEntriesV2/' . $compId;
                            break;
	                    case 'callroom':
//                          /entry/v5/competition/showEntriesV2.php?compid=216
		                    $targetLocation = E4S_PATH . 'scoreboards/e4scontrollerV2.php?name=CallRoom&delay=5&mid=' . $compId;
		                    break;
                        case 'sched':
                        case 'schedule':
//                          /entry/v5/competition/schedule.php?compid=216
                            $targetLocation = E4S_PATH . 'competition/schedule.php?compid=' . $compId;
                            break;
                        case 'results':
//                          /#/results/187
                            $targetLocation = '/#/results/' . $compId;
                            break;
                        case 'resultlist':
                        case 'resultslist':
                            $targetLocation = E4S_FUNCTIONS_ROUTE_PATH . '/results/report/' . $compId;
                            break;
                        case 'pof10':
//                          /187/pof10
                            $targetLocation = E4S_FUNCTIONS_ROUTE_PATH . '/results/pof10/' . $compId;
                            break;
	                    case 'myentries':
//                          /wp-json/e4s/v5/entries/myentries/617
		                    $targetLocation = E4S_FUNCTIONS_ROUTE_PATH . '/entries/myentries/' . $compId;
		                    break;

	                    case 'esaa':
//                          /wp-json/e4s/v5/public/esaascores/617
		                    $targetLocation = E4S_FUNCTIONS_ROUTE_PATH . '/esaascores/' . $compId;
		                    break;

                        case 'cards':
                        case 'card':
                        case 'resultcard':
                        case 'resultscard':
//                          /wp-json/e4s/v5/public/reports/output/177
                            $targetLocation = E4S_FUNCTIONS_ROUTE_PATH . '/public/reports/cards/' . $compId;
                            break;
//                            /wp-json/e4s/v5/uicard/177  Old Results Entry Card
//                            $targetLocation = "/wp-json/e4s/v5/uicard/" . $compId;
//                            break;
                        case 'output':
                        case 'report':
//                          /wp-json/e4s/v5/public/reports/output/177
                            $targetLocation = E4S_FUNCTIONS_ROUTE_PATH . '/public/reports/output/' . $compId;
                            break;

                        case 'checkin':
                        case 'builder':
                        case 'bibs':
                            $targetLocation = '/#/' . $uri[2] . '/' . $compId;
                            break;
                        case 'scoreboard':
                        case 'scoreboards':
                            $targetLocation = '/#/scoreboard-output-list/' . $compId;
                            break;
                        case 'display':
                            $targetLocation = E4S_PATH . 'results/touch.php?compid=' . $compId;
                            break;
                        case 'ldisplay':
                            $targetLocation = E4S_PATH . 'results/touch.php?locid=' . $compId;
                            break;
		                case 'order':
//                          https://entry4sports.co.uk/wp-admin/post.php?post=260916&action=edit
			                // $compid is actually the order id here
			                $targetLocation = '/wp-admin/post.php?post=' . $compId . '&action=edit';
//			                exit($targetLocation);
			                break;

		                case 'apisched':
//                          /wp-json/e4s/v5/public/reports/output/177
			                $targetLocation = E4S_FUNCTIONS_ROUTE_PATH . '/public/info/apisched/' . $compId;
			                break;
		                case 'apistart':
//                          /wp-json/e4s/v5/public/reports/output/177
			                $targetLocation = E4S_FUNCTIONS_ROUTE_PATH . '/public/info/apistart/' . $compId;
			                break;
		                case 'apiresults':
//                          /wp-json/e4s/v5/public/reports/output/177
			                $targetLocation = E4S_FUNCTIONS_ROUTE_PATH . '/public/info/apiresults/' . $compId;
			                break;
                    }
                }
            }
        }
        if ( $targetLocation === '' ){
            $targetLocation = e4s_routes($uri);
        }

        if ($targetLocation !== '') {
            if ( $qs !== '' ){
                if ( str_contains($targetLocation, '?') ){
                    $qs = '&' . $qs;
                }else {
                    $qs = '?' . $qs;
                }
            }
            echo "
                <script>
                    location.replace('" . 'https://' . $domain . $targetLocation . $qs . "');  
                </script>
            ";
            exit();
        }
    }

    return TRUE;
}
function e4s_routes($uri):string {
    if ( sizeof($uri) === 3 ) {
	    if ( $uri[0] === '' and $uri[1] === 'urncheck' ) {
            $urn = $uri[2];
		    include_once E4S_FULL_PATH . 'dbInfo.php';
		    $aaiObj = new aaiRegistrationClass();
		    $obj    = $aaiObj->validateURN( $urn );
		    Entry4UISuccess( $obj );
	    }
    }
    return '';
}
function e4s_rest_showEntries($obj){
    e4s_webPageHeader();
    $compId = (int)checkFieldFromParamsForXSS($obj, 'compid:Competition ID' . E4S_CHECKTYPE_NUMERIC);
    include_once E4S_FULL_PATH . 'showEntries/showEntriesV2.php';
    exit;
}
function e4s_checkCartContents() {
    $checkout = strtolower($_SERVER['REQUEST_URI']) === '/checkout/';
    include_once E4S_FULL_PATH . 'cart/getCart.php';
    // Check if there are entries to be added ( basket only )
    if (!$checkout) {
        getActiveUserCart(FALSE);
    }
    if (e4s_validateCart($checkout)) {
//        ob_start();
        wc_print_notices();
//        ob_end_clean();
        if ($checkout) {
            die();
        }
    }
}
add_filter('retrieve_password_message','e4s_retrieve_password_message', 20, 2 );
add_filter('password_reset_expiration','e4s_password_reset_expiration',10,1);
function e4s_retrieve_password_message($message, $user_data){
    $message = str_replace('wp-login.php?action=rp&', '#/v2/reset_pwd?', $message);
    $message = str_replace('<http', 'http', $message);
    $message = str_replace('>', '', $message);
    $message .= "\n\n" . 'This link will expire at ';
    $mins = e4s_password_reset_expiration();
    $time = date_create('now');
    $time->add(new DateInterval('PT' . $mins . 'M'));

    $stamp = $time->format('d/m/Y H:i');
    return $message . $stamp;
}
function e4s_password_reset_expiration($expire = 720){
    // password expiration in minutes
    return 720;
}
function e4s_pwdReset($obj){
    include_once E4S_FULL_PATH . 'dbInfo.php';
    $key = checkJSONObjForXSS($obj, 'key:User Key');
    $login = checkJSONObjForXSS($obj, 'login:User Login');
    $pwd = checkJSONObjForXSS($obj, 'pwd:Password');

    $user = check_password_reset_key( $key, $login );
    if ( is_wp_error( $user ) ) {
        Entry4UIError(8500,str_replace('_', ' ', $user->get_error_code() ), 200);
    }
    wp_set_password($pwd, $user->ID);
    Entry4UISuccess();
}
function e4s_pwdResetRequest($obj){
    include_once E4S_FULL_PATH . 'dbInfo.php';
    $user_login = checkJSONObjForXSS($obj, 'key:User Key');
    if ( is_null($user_login) ){
        Entry4UIError(8506, 'Not enough information passed.', 200);
    }

    global $wpdb, $wp_hasher;

    $user_login = sanitize_text_field($user_login);

    if ( empty( $user_login) ) {
        return false;
    } else if ( strpos( $user_login, '@' ) ) {
        $user_data = get_user_by( 'email', trim( $user_login ) );
        if ( empty( $user_data ) ) {
            $retVal = '"data":"If this email is valid, you should receive a request for password reset [8510]"';
            Entry4UISuccess($retVal);
        }
    } else {
        $login = trim($user_login);
        $user_data = get_user_by('login', $login);
    }

    do_action('lostpassword_post');

    if ( !$user_data ) return false;

    // redefining user_login ensures we return the right case in the email
    $user_login = $user_data->user_login;
    $user_email = $user_data->user_email;

//    do_action('retreive_password', $user_login);  // Misspelled and deprecated
    do_action('retrieve_password', $user_login);

    $allow = apply_filters('allow_password_reset', true, $user_data->ID);

    if ( ! $allow ) {
        $retVal = '"data":"If this email is valid, you should receive a request for password reset [8511]"';
        Entry4UISuccess($retVal);
    } else if ( is_wp_error($allow) ) {
        $retVal = '"data":"If this email is valid, you should receive a request for password reset [8512]"';
        Entry4UISuccess($retVal);
    }

    $key = wp_generate_password( 20, false );
    do_action( 'retrieve_password_key', $user_login, $key );

    if ( empty( $wp_hasher ) ) {
        require_once ABSPATH . 'wp-includes/class-phpass.php';
        $wp_hasher = new PasswordHash( 8, true );
    }
    $hashed = time() . ':' . $wp_hasher->HashPassword( $key );
    $wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user_login ) );

    $message = __('Someone requested that the password be reset for the following account:') . "\r\n\r\n";
    $message .= network_home_url( '/' ) . "\r\n\r\n";
    $message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
    $message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
    $message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
    $message .= '<' . network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . ">\r\n";

    if ( is_multisite() )
        $blogname = $GLOBALS['current_site']->site_name;
    else
        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

    $title = sprintf( __('[%s] Password Reset'), $blogname );

    $title = apply_filters('retrieve_password_title', $title);
    $message = apply_filters('retrieve_password_message', $message, $key);

    if ( $message && !wp_mail($user_email, $title, $message) )
        wp_die( __('The e-mail could not be sent.') . "<br />\n" . __('Possible reason: your host may have disabled the mail() function...') );
    $retVal = '"data":"Link for password reset has been emailed to you. Please check your email. [8500]"';
    Entry4UISuccess($retVal);
}

function e4s_rest_listStripeUsers($obj){
    include_once E4S_FULL_PATH . 'stripe/e4sStripe.php';
    e4s_listStripeConnections();
}
function e4s_rest_initStripeUsers($obj) {
    include_once E4S_FULL_PATH . 'stripe/e4sStripe.php';
    e4s_initialiseExistingUsers();
}

function e4s_rest_approveStripeUser($obj) {
    include_once E4S_FULL_PATH . 'stripe/e4sStripe.php';
    e4s_approveNewStripeUser($obj);
}

function action_wp_mail_failed($wpError) {
    include_once E4S_FULL_PATH . 'dbInfo.php';

    $emailObj = new e4SMessageClass();
    $emailObj->errorReceived($wpError);
    return error_log(print_r($wpError, TRUE));
}

function e4s_payment_complete($orderId) {
    include_once E4S_FULL_PATH . 'entries/entries.php';
    e4s_markOrderPaid($orderId);
}

function e4s_afterWPMail($is_sent, $to, $cc, $bcc, $subject, $body, $from) {
    require_once E4S_FULL_PATH . 'builder/messageCRUD.php';
    e4s_postWPMail($to[0],$subject,$body, $from);
}

function e4s_checkEmailFrom($phpMailer) {
    $replyTo = $phpMailer->getReplyToAddresses();
    foreach ($replyTo as $replyToAddr => $value) {
        if ($replyToAddr !== '') {
            $phpMailer->setFrom($replyToAddr);
        }
        break;
    }

    return $phpMailer;
}

function e4s_tandc_text() {
    if (E4S_CURRENT_DOMAIN !== E4S_AAI_DOMAIN) {
        ?>
        <div>All entrants shall be deemed to have made him/herself familiar with, and agreed to be bound by the UKA Anti-Doping Rules and to submit to the authority of UK Anti-Doping in the application and enforcement of the Anti-Doping Rules.<br>
            The UKA Anti-Doping Rules apply to entrants participating in the sport of Athletics, for 12 months from today (<?php echo date('jS M Y')?>), whether or not the entrant is a citizen of, or resident in, the UK.
        </div>
        <?php
    }
}

function e4s_display_item_meta_filter($html, $item, $args ){
    $html = str_replace(E4S_CAR_REG,E4S_CAR_REG_LABEL, $html);
    return $html;
}
add_action('woocommerce_checkout_before_terms_and_conditions', 'e4s_tandc_text');
add_filter('woocommerce_get_item_data', 'add_attributes_to_cart_item', 10, 2);
add_filter('woocommerce_customer_get_downloadable_products', 'e4s_add_custom_default_download', 9999, 1);
add_filter('woocommerce_account_downloads_columns', 'e4s_account_downloads_columns', 10, 1);

add_filter('woocommerce_display_item_meta', 'e4s_display_item_meta_filter', 10, 3);
add_filter('woocommerce_order_get_downloadable_items', 'e4s_add_custom_default_download', 9999, 1);
add_filter('woocommerce_order_item_name', 'e4s_getOrderProdName', 9999, 3);
add_filter('woocommerce_order_item_quantity_html', 'e4s_showQty', 9999, 2);
add_filter('woocommerce_order_is_download_permitted', 'e4s_downloadsPermitted', 10, 2);
add_filter('woocommerce_order_item_permalink', 'e4s_getProductPermalink', 20, 3);
add_action('woocommerce_order_item_meta_end', 'e4s_order_item_end', 10, 3);
add_action('woocommerce_order_details_after_order_table', 'e4s_order_footer', 10, 1);
add_action('woocommerce_payment_complete', 'e4s_payment_complete');
add_action('woocommerce_pre_payment_complete', 'e4s_payment_complete');
add_filter('woocommerce_cart_item_product', 'e4s_getCartProduct', 10, 3);
add_filter('woocommerce_cart_item_name', 'e4s_getCartProductName', 10, 3);
add_filter('woocommerce_checkout_item_name', 'e4s_getCheckoutProductName', 10, 3);
add_filter('woocommerce_cart_item_price', 'e4s_getPriceForCart', 10, 3);
add_filter('woocommerce_cart_item_fee', 'e4s_getFeesForCart', 10, 3);
add_filter('woocommerce_cart_item_thumbnail', 'e4s_getCartThumbnail', 10, 3);
add_filter('woocommerce_cart_item_permalink', 'e4s_getProductPermalink', 20, 3);
add_action('woocommerce_cart_emptied', 'e4s_woocommerce_cart_emptied', 10, 0);
add_action('woocommerce_after_cart_table', 'e4s_afterCartNotice');
add_action('woocommerce_remove_cart_item', 'e4s_checkForSecondary', 10, 2);
add_action('woocommerce_cart_item_removed', 'e4s_after_remove_product_from_cart', 10, 2);

add_filter('woocommerce_email_downloads_columns', 'e4s_emailDownloadColumns', 10, 1);
add_action('woocommerce_email_order_meta', 'e4s_emailCustomerFooter', 10, 4);

add_filter('woocommerce_quantity_input_args', 'e4s_getMaxOrderQty', 10, 2);
add_filter('woocommerce_form_field_args', 'e4s_billingFields', 10, 3);

add_action('wp_head', 'e4s_setHeader');
add_action('woocommerce_thankyou', 'e4s_Thankyou');
add_action('init', 'e4s_redirect_post_location', 10, 1);
//add_action( 'wp_logout', 'e4s_user_loggedout', 10, 1 );
// Mail Actions
add_filter('woocommerce_mail_callback', 'e4s_setMailCallback', 10, 2); // WC to use something other than wp_mail
add_filter('wp_mail_smtp_custom_options', 'e4s_checkEmailFrom', 10, 1);
add_action('wp_mail_failed', 'action_wp_mail_failed', 10, 1);
add_action('wp_mail_smtp_mailcatcher_smtp_send_after', 'e4s_afterWPMail', 10, 7);

add_filter('woocommerce_default_address_fields', 'e4s_disable_postcode_validation');

function e4s_disable_postcode_validation($address_fields) {
    $address_fields['postcode']['required'] = FALSE;
    return $address_fields;
}

function e4s_hookCartSummary() {
    include_once E4S_FULL_PATH . 'cart/getCart.php';
    e4s_CartSummary();
}

function e4s_woocommerce_after_checkout_validation($postedData, $errors) {
    include_once E4S_FULL_PATH . 'stripe/e4sStripe.php';
    e4s_woocommerceAfterCheckoutValidation($postedData, $errors);
}

add_action('woocommerce_after_checkout_validation', 'e4s_woocommerce_after_checkout_validation', 1, 2);
//add_action( 'woocommerce_before_cart', 'e4s_checkCartContents', 10,0 );
//add_action( 'woocommerce_before_checkout_form', 'e4s_checkCheckoutContents', 10,0 );
// vvvvvv this is called for BOTH basket and checkout vvvvvv
add_action('woocommerce_check_cart_items', 'e4s_checkCartContents', 10, 0);
add_filter('woocommerce_checkout_cart_item_quantity', 'e4s_showCheckOutQty', 9999, 3);

add_action('woocommerce_after_cart_totals', 'e4s_hookCartSummary', 10, 0);

function filter_wc_stripe_payment_metadata($metadata, $order, $source) {
    include_once E4S_FULL_PATH . 'stripe/e4sStripe.php';
    return e4s_getMetaData_Stripe($order, $metadata);
}

function filter_wc_stripeconnect_payment_metadata($meta, $action) {
    include_once E4S_FULL_PATH . 'stripe/e4sStripe.php';
    return e4s_getMetaData_StripeConnect($meta, $action);
}

function filter_wc_stripeconnect_charge_description($description, $name, $orderNo, $orderId) {
    include_once E4S_FULL_PATH . 'stripe/e4sStripe.php';
    return e4s_genericGetDescription($description, $orderId);
}

function filter_wc_stripe_update_charge_params($data, $name, $commission, $order) {
    if ($name === 'update_destination_payment') { // should be as this should be the only name that calls this function
        include_once E4S_FULL_PATH . 'stripe/e4sStripe.php';
        $data = e4s_getStripeconnect_charge_params($data, $name, $commission, $order);
    }
    return $data;
}

// Stripe Filter
add_filter('wc_stripe_payment_metadata', 'filter_wc_stripe_payment_metadata', 10, 3);

//Stripe Connect Filters
add_filter('yith_wcstripe_connect_metadata', 'filter_wc_stripeconnect_payment_metadata', 10, 2);
add_filter('yith_wcsc_charge_description', 'filter_wc_stripeconnect_charge_description', 10, 4);
add_filter('yith_wcstripe_update_charge_params', 'filter_wc_stripe_update_charge_params', 10, 4);

// E4S Cron actions
const E4S_CRON_HEALTHCHECKER_ACTION = 'e4s_healthMonitor';
const E4S_CRON_ENTRYCHECKER_ACTION = 'e4s_entryChecker';
const E4S_CRON_ORDERCHECKER_ACTION = 'e4s_orderChecker';
const E4S_CRON_COMPCLOSINGSOON_ACTION = 'e4s_entriesClosingChecker';
const E4S_CRON_WAITINGLISTCHECKER_ACTION = 'e4s_waitingListChecker';
const E4S_CRON_SCHEDULECHECKER_ACTION = 'e4s_checkSchedules';
const E4S_CRON_SENDBATCHEMAILS_ACTION = 'e4s_sendBatchEmails';
const E4S_CRON_STATUSREPORT_ACTION = 'e4s_reportOnStatus';

add_action(E4S_CRON_ENTRYCHECKER_ACTION, 'e4s_entryCheckerUpdate');
add_action(E4S_CRON_COMPCLOSINGSOON_ACTION, 'e4s_entriesClosing');
add_action(E4S_CRON_ORDERCHECKER_ACTION, 'e4s_orderCheckerUpdate');
add_action(E4S_CRON_WAITINGLISTCHECKER_ACTION, 'e4s_waitingListCheckerUpdate');
add_action(E4S_CRON_SENDBATCHEMAILS_ACTION, 'e4s_cronSendOutstandingEmails');
add_action(E4S_CRON_HEALTHCHECKER_ACTION, 'e4s_healthMonitorUpdate');
add_action('e4s_CompStatusEmail', 'e4s_CompStatusEmail');
add_action('e4s_loadathletesV3', 'e4s_cronimport_irish_athletes_loadv3');
add_action('e4s_loadNIathletesV3', 'e4s_cronimport_northern_irish_athletes');
add_action('e4s_checkFees', 'checkFeesPaid');
add_action(E4S_CRON_STATUSREPORT_ACTION, 'ReportOnPaymentStatus');
add_action('e4s_dedup', 'e4s_rest_dedup');
add_action(E4S_CRON_SCHEDULECHECKER_ACTION, 'e4s_checkSchedules');

// schedule / Cron events
function e4s_checkSchedules() {
    $e4s5mins = 'e4s5mins';
    // 5 minutes functions
    $check = E4S_CRON_HEALTHCHECKER_ACTION;
    if (!wp_next_scheduled($check)) {
        wp_schedule_event(time(), $e4s5mins, $check);
    }
    $check = E4S_CRON_ORDERCHECKER_ACTION;
    if (!wp_next_scheduled($check)) {
        wp_schedule_event(time(), $e4s5mins, $check);
    }
    $check = E4S_CRON_SENDBATCHEMAILS_ACTION;
    if (!wp_next_scheduled($check)) {
        wp_schedule_event(time(), $e4s5mins, $check);
    }
    $check = E4S_CRON_WAITINGLISTCHECKER_ACTION;
    if (!wp_next_scheduled($check)) {
        wp_schedule_event(time(), $e4s5mins, $check);
    }

    // 2 minute functions
    $e4s2mins = 'e4s2mins';
    $check = E4S_CRON_ENTRYCHECKER_ACTION;
    if (!wp_next_scheduled($check)) {
        wp_schedule_event(time(), $e4s2mins, $check);
    }

    $e4s2daily = 'e4s2daily';
    $check = E4S_CRON_STATUSREPORT_ACTION;
    if (!wp_next_scheduled($check)) {
        wp_schedule_event(time(), $e4s2daily, $check);
    }
    $check = E4S_CRON_COMPCLOSINGSOON_ACTION;
    if (!wp_next_scheduled($check)) {
        wp_schedule_event(time(), $e4s2daily, $check);
    }
}

if (!wp_next_scheduled(E4S_CRON_SCHEDULECHECKER_ACTION)) {
    wp_schedule_event(time(), 'e4s30mins', E4S_CRON_SCHEDULECHECKER_ACTION);
}
add_action('woocommerce_before_mini_cart_contents', 'addminicart');
function addminicart(){
    $GLOBALS[E4S_IN_MINI_CART] = true;
}
function filter_plugin_updates( $value ) {
	unset( $value->response['woo-conditional-product-fees-for-checkout/woocommerce-conditional-product-fees-for-checkout.php'] );
	return $value;
}
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );

//function wp_version_remove_version() {
//	return '';
//}
//add_filter('the_generator', 'wp_version_remove_version');
function keep_me_logged_in_for_1_day( $expirein ) {
	return 86400; // 1 day in seconds
}
add_filter( 'auth_cookie_expiration', 'keep_me_logged_in_for_1_day' );

//function replace_siteurl($val) {
//	return 'http://uat.entry4sports.co.uk';
//}
//add_filter('option_siteurl', 'replace_siteurl');
//add_filter('option_home', 'replace_siteurl');