<?php
include_once E4S_FULL_PATH . 'dbInfo.php';

$config = e4s_getConfig();
$theme = strtolower($config['theme']);
$logo = 'e4s';
$theme_class = '';
if ( $theme === E4S_THEME_IRL ){
    $theme_class = 'ia-';
    if ( E4S_CURRENT_DOMAIN !== E4S_EIRE_DOMAIN and E4S_CURRENT_DOMAIN !== E4S_AAI_TEST_DOMAIN) {
	    $logo = 'aai';
    }
}

?>
<style>
    .testing {

    }
<?php
include_once E4S_FULL_PATH . 'css/e4s2022-11-14.css';
?>
    body {
        margin: 0px;
    }

    .e4s_cart_waiting {
        padding: 20px 0 20px 0px;
        font-weight: bold;
        color: red;
    }

    .e4s_cart_addEntries {
        float: right !important;
    }

    .woocommerce-MyAccount-content {
        padding-right: 25px;
    }

    .woocommerce-MyAccount-navigation ul a {
        color: #000 !important;
        text-decoration: underline !important;
        line-height: 25px;
    }

    .nav {
        background-color: var(--e4s-navigation-bar--<?php echo $theme_class ?>primary__background) !important;
        margin-bottom: 15px
    }

    nav .white-text ul li {
        float: left;
    }
    nav ul li {
        -webkit-transition: background-color .3s;
        transition: background-color .3s;
        padding: 0;
    }
    nav ul a {
        -webkit-transition: background-color .3s;
        transition: background-color .3s;
        font-size: 1rem;
        color: #fff ! important;
        display: block;
        padding: 0 0 0 15px;
        cursor: pointer;
    }
    .nav-wrapper {
        height: 64px;
        position: relative;
    }
    ul {
        display: block;
        list-style-type: disc;
        margin-block-start: 1em;
        margin-block-end: 1em;
        margin-inline-start: 0px;
        margin-inline-end: 0px;
        padding-inline-start: 40px;
    }

    ul:not(.browser-default) {
        padding-left: 0;
        list-style-type: none;
    }
    li {
        display: list-item;
        text-align: -webkit-match-parent;
    }

    .dropdown-content {
        background-color: #fff;
        margin: 0;
        display: none;
        min-width: 100px;
        overflow-y: auto;
        opacity: 0;
        position: absolute;
        left: 0;
        top: 0;
        z-index: 9999;
        -webkit-transform-origin: 0 0;
        transform-origin: 0 0;
    }

    .right {
        float: right;
        color: white;
    }

    a {
        text-decoration: none;
    }

    form.woocommerce-cart-form {
        width: 60%;
        float: left;
        margin: 2%;
        width: 100% !important;
        margin:0;
    }

    .woocommerce-cart-form td {
        padding: 2px !important;
    }

    .woocommerce-cart-form td.product-thumbnail {
        width: 50px;
        padding: 2px 6px 2px 2px !important;
    }

    .woocommerce a.button {
        background-color: var(--e4s-button--<?php echo $theme_class ?>primary__background) !important;
        color: #fff !important;
        margin-left: 5px !important;
    }

    .woocommerce button.button {
        background-color: var(--e4s-button--<?php echo $theme_class ?>tertiary__background) !important;
        color:var(--e4s-button--tertiary__text-color) !important;
    }
    .woocommerce a.button, .woocommerce .place-order button.button {
        background-color: var(--e4s-button--<?php echo $theme_class ?>primary__background) !important;
        color:var(--e4s-button--primary__text-color) !important;
    }

    .woocommerce {
        max-width: 1600px;
        margin: 0 auto;
    }
    .woocommerce-cart-form td {
        border-bottom: 2px solid black ;
    }
    .cart-collaterals {
        width: 90% !important;
        padding-left: 30px;
    }

    .cart_totals {
        width: 100% !important;
        margin-right: 2% !important;
    }

    .entry-content11 {
        display: grid;
        justify-content: center;
        justify-items: center;
        grid-template-columns: repeat(auto-fit, minmax(1000px, 1400px));
    }

    .woocommerce .woocommerce-info {
        border-top: 0px;
    }
    add_payment_method table.cart img, .woocommerce-cart table.cart img, .woocommerce-checkout table.cart img {
        width: 80px;
    }

    .e4s_cart_main {
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(380px, 680px));
        gap: 20px;
        justify-content: center;
        padding-top: 20px;
    }

    .product-remove {
        width: 5px;
        padding: 1px !important;
    }

    #e4s-nav-bar {
        height: 64px;
    }
    #e4s-nav-bar .e4s-navigation-bar--ia-primary {
        background-color:  #f7921e !important;
    }
</style>
<div>
    <div id="e4s-nav-bar" class="e4s-flex-column e4s-sticky-navigation">
        <div class="e4s-navigation-bar e4s-navigation-bar--<?php echo $theme_class?>primary">
            <div class="e4s-navigation-bar--content-wrapper">
                <div class="e4s-navigation-bar-logo--container" style="cursor: pointer;">
                    <img src="<?php echo E4S_PATH ?>css/images/<?php echo $logo ?>_logo_single.svg" class="e4s_logo_white">
                </div>
                <ul class="e4s-navigation-bar-menu">
                    <li class="e4s-navigation-bar-menu--item">
                        <a href="/" class="e4s-subheader--200">
                            <span class="e4s-subheader--200">Home</span>
                        </a>
                    </li>
                    <li class="e4s-navigation-bar-menu--item">
                        <span class="e4s-subheader--200" style="font-size: small; color: white;"><?php echo wp_get_current_user()->display_name; ?></span>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</div>