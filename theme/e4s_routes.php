<?php /** @noinspection ALL */
// E4S Routes
add_action('rest_api_init', function () {
    register_rest_route(E4S_NS, '/login/', array('methods' => 'GET', 'callback' => 'e4s_login', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/sociallogin/', array('methods' => 'GET', 'callback' => 'e4s_sociallogin', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/relay', array('methods' => 'GET', 'callback' => 'e4s_restRelay', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/wtf', array('methods' => 'GET', 'callback' => 'e4s_restRelay', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/help/(?P<keyid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_GetHelpDoc', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/help/(?P<key>\S+)', array('methods' => 'GET', 'callback' => 'e4s_rest_GetHelpDoc', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/help', array('methods' => 'GET', 'callback' => 'e4s_rest_GetAllHelpDocs', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/help', array('methods' => 'POST', 'callback' => 'e4s_rest_createHelpDoc', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/help', array('methods' => 'PUT', 'callback' => 'e4s_rest_updateHelpDoc', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/help/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_deleteHelpDoc', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/agegroups/(?P<compId>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompetitionAges', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/priority/(?P<compId>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_checkPriority', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/contactOrganiser', array('methods' => 'POST', 'callback' => 'e4s_rest_contactOrganiser', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/update', array('methods' => 'POST', 'callback' => 'e4s_rest_updateCompetitionSettings', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/checkin/clear', array('methods' => 'GET', 'callback' => 'e4s_rest_clearCheckins', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/checkin', array('methods' => 'GET', 'callback' => 'e4s_rest_listCheckins', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/checkin/verify', array('methods' => 'GET', 'callback' => 'e4s_rest_veryifyAthleteEmailId', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/checkinsummary', array('methods' => 'GET', 'callback' => 'e4s_rest_getCheckinSummary', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/checkin/(?P<compid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_updateCheckins', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getComp', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/standards', array('methods' => 'GET', 'callback' => 'e4s_rest_listStandards', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/standards', array('methods' => 'POST', 'callback' => 'e4s_rest_modifyStandard', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/standards/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_deleteStandard', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/standardvalues', array('methods' => 'GET', 'callback' => 'e4s_rest_listStandardValues', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/standardvalues/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_listStandardValues', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/standardvalues', array('methods' => 'POST', 'callback' => 'e4s_rest_modifyStandardValue', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/standardvalues/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_deleteStandardValue', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/logout', array('methods' => 'GET', 'callback' => 'e4s_logout', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/register/', array('methods' => 'POST', 'callback' => 'e4s_register', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/pwdresetrequest', array('methods' => 'POST', 'callback' => 'e4s_pwdResetRequest', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/pwdreset', array('methods' => 'POST', 'callback' => 'e4s_pwdReset', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/v3import', array('methods' => 'GET', 'callback' => 'import_irish_athletes_loadv3', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/v3flatimport', array('methods' => 'GET', 'callback' => 'import_irish_athletes_loadflatv3', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athletecheck', array('methods' => 'GET', 'callback' => 'import_irish_athletes_check', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete/owners', array('methods' => 'POST', 'callback' => 'e4s_restGetAthleteOwners', 'permission_callback' => '__return_true'));
    // Comp Check List
    register_rest_route(E4S_NS, '/competition/checklist/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_checkCompetition', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/track/move', array('methods' => 'POST', 'callback' => 'e4s_rest_MoveTrackPosition', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entry/qualify', array('methods' => 'POST', 'callback' => 'e4s_rest_markEntriesQualified', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entry/present/(?P<present>\S+)/(?P<entryid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_MarkEntryPresent', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/entry/team/present/(?P<present>\S+)/(?P<entryid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_MarkTeamEntryPresent', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entry/startheight/(?P<entryid>\d+)/(?P<startheight>\S+)', array('methods' => 'POST', 'callback' => 'e4s_rest_SetStartHeight', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entry/stands/(?P<entryid>\d+)/(?P<stands>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_SetStands', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entry/cancel/(?P<entryid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_CancelEntry', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/entry/delete/(?P<entryid>\d+)/(?P<reason>\S+)', array('methods' => 'POST', 'callback' => 'e4s_rest_CancelEntry', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entry/addtobasket', array('methods' => 'POST', 'callback' => 'e4s_rest_AddToBasket', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entry/paid', array('methods' => 'POST', 'callback' => 'e4s_rest_UpdatePaid', 'permission_callback' => '__return_true'));

    // school team CRUD
    register_rest_route(E4S_NS, '/schoolteam', array('methods' => 'POST', 'callback' => 'e4s_rest_createSchoolTeam', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/schoolteam', array('methods' => 'PUT', 'callback' => 'e4s_rest_updateSchoolTeam', 'permission_callback' => '__return_true'));

    // league team CRUD
    register_rest_route(E4S_NS, '/leagueteam', array('methods' => 'POST', 'callback' => 'e4s_rest_createLeagueTeam', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/leagueteam', array('methods' => 'PUT', 'callback' => 'e4s_rest_updateLeagueTeam', 'permission_callback' => '__return_true'));

    // event teams CRUD
    register_rest_route(E4S_NS, '/selfserviceTEST', array('methods' => 'GET', 'callback' => 'e4s_rest_requestSelfService', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/selfservice', array('methods' => 'POST', 'callback' => 'e4s_rest_requestSelfService', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/selfservice', array('methods' => 'GET', 'callback' => 'e4s_rest_authoriseSelfService', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/eventteams/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_listEventTeams', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/compeventteam', array('methods' => 'POST', 'callback' => 'e4s_rest_createEventTeam', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/compeventteam', array('methods' => 'PUT', 'callback' => 'e4s_rest_updateEventTeam', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/compeventteam/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_deleteEventTeam', 'permission_callback' => '__return_true'));

    // Builder
    // Event Group CRUD
    register_rest_route(E4S_NS, '/eventgroups/merge/(?P<fromEgId>\d+)/(?P<toEgId>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_mergeEGs', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/eventgroups', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListEGs', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entries/include/(?P<compid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_addIncludedEntries', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entries/create/(?P<egid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_createEntriesForEG', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entries/create/(?P<egid>\d+)/(?P<targetegid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_createEntriesForEG', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entries/moveHoriz', array('methods' => 'POST', 'callback' => 'e4s_rest_moveEntriesHorizontal', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entries/moveVert', array('methods' => 'POST', 'callback' => 'e4s_rest_moveEntriesVertical', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entries/myentries/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_myEntries', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/eventgroup/maxinheat/(?P<egid>\d+)/(?P<maxinheat>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_updateEGMaxInHeat', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/eventgroup/cardupdate/(?P<egid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_updateEGFromCard', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/eventgroup/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_deleteEventGroup', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/eventgroup', array('methods' => 'POST', 'callback' => 'e4s_rest_builderUpdateEG', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/eventgroup/clear/(?P<compid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_clearEventTimes', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/eventgroup/clear/(?P<compid>\d+)/(?P<usecompdate>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_clearEventTimes', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/eventgroup/resultspossible', array('methods' => 'POST', 'callback' => 'e4s_rest_updateEGResultsPossible', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/eventgroup/notes', array('methods' => 'POST', 'callback' => 'e4s_rest_updateEGNotes', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/compevent/renamegroup', array('methods' => 'POST', 'callback' => 'e4s_rest_builderUpdateEGs', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/report/checkAndUpdateComp', array('methods' => 'GET', 'callback' => 'e4s_rest_checkAndUpdateComp', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/cron/checkreports', array('methods' => 'GET', 'callback' => 'e4s_updateReportInfo', 'permission_callback' => '__return_true'));

    // Competition CRUD
    register_rest_route(E4S_NS, '/builder', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListComps', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/builder/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadComp', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/builder', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateComp', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/builder', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateComp', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/builder/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteComp', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/compevent/bulkupdate', array('methods' => 'POST', 'callback' => 'e4s_rest_builderBulkupdate', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/bib/notes', array('methods' => 'POST', 'callback' => 'e4s_rest_updateCompNotes', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/bib/unused/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getUnused', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/bib/change/(?P<compid>\d+)/(?P<athleteid>\d+)/(?P<bibno>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_changeAthleteBib', 'permission_callback' => '__return_true'));

	// Orgs CRUD
    register_rest_route(E4S_NS, '/myorgs', array('methods' => 'GET', 'callback' => 'e4s_rest_ListMyOrgs', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/myorgs/(?P<linkUserId>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_ListMyOrgs', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/orgs/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadOrg', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/orgs/admin/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadAdminOrg', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/orgs/(?P<status>\S+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListOrgs', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/orgs', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateOrg', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/orgs/approve/(?P<id>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_approveOrg', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/orgs', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateOrg', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/orgs/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteOrg', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/orgs', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListOrgs', 'permission_callback' => '__return_true'));

    // CompEvents CRUD
    register_rest_route(E4S_NS, '/compevent/resettimes/(?P<compid>\d+)', array('methods' => 'PUT', 'callback' => 'e4s_rest_resetCETimes', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/compevent/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadCE', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/compevent', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateCE', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/compevent', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateCE', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/compevent/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteCE', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/compevent/delete', array('methods' => 'POST', 'callback' => 'e4s_rest_builderDeleteCEs', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/compevent', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListCEs', 'permission_callback' => '__return_true'));

    // CompEventArrs CRUD
    register_rest_route(E4S_NS, '/compeventarr/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadCEArr', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/compeventarr', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateCEArr', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/compeventarr', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateCEArr', 'permission_callback' => '__return_true'));

    register_rest_route(E4S_NS, '/compeventarr/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteCEArr', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/compeventarr', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListCEArrs', 'permission_callback' => '__return_true'));

    // Price CRUD
    register_rest_route(E4S_NS, '/price/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadPrice', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/price', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreatePrice', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/price', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdatePrice', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/prices', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdatePrices', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/price/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeletePrice', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/price', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListPrices', 'permission_callback' => '__return_true'));

    // Disc CRUD
    register_rest_route(E4S_NS, '/discount/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadDisc', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/discount', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateDisc', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/discount', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateDisc', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/discount/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteDisc', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/discount', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListDiscs', 'permission_callback' => '__return_true'));

    // Comp Rule CRUD
    register_rest_route(E4S_NS, '/rule/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadCompRule', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/rule', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateCompRule', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/rule', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateCompRule', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/rule/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteCompRule', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/rule', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListCompRules', 'permission_callback' => '__return_true'));

    // Location CRUD
    register_rest_route(E4S_NS, '/locs/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadLoc', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/locs', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateLoc', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/locs', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateLoc', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/locs/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteLoc', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/locs', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListLocs', 'permission_callback' => '__return_true'));

    // Secondary Definitions
    register_rest_route(E4S_NS, '/secondary/(?P<refid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadSecondary', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/secondary', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateSecondary', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/secondary', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateSecondary', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/secondary/(?P<refid>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteSecondary', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/secondary/list', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListSecondary', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/secondary/cust/list', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListSecondaryCust', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/secondary/cust', array('methods' => 'POST', 'callback' => 'e4s_rest_purchaseSecondary', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/secondary/cust', array('methods' => 'GET', 'callback' => 'e4s_rest_getSecondaryPurchases', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/secondary/athlete', array('methods' => 'GET', 'callback' => 'e4s_rest_getSecondaryAthletePurchases', 'permission_callback' => '__return_true'));

    // Secondary Variations Definitions
    register_rest_route(E4S_NS, '/secondary/variation/(?P<prodid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadSecondaryVariation', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/secondary/variation', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateSecondaryVariation', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/secondary/variation', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateSecondaryVariation', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/secondary/variation/(?P<prodid>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteSecondaryVariation', 'permission_callback' => '__return_true'));

    // r4s schedule config crud
    register_rest_route(E4S_R4S_NS, '/schedule/read/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadR4SSchedule', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/schedule/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadR4SSchedule', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/schedule/create', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateR4SSchedule', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/schedule', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateR4SSchedule', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/schedule/update', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateR4SSchedule', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/schedule', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateR4SSchedule', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/schedule/delete/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteR4SSchedule', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/schedule/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteR4SSchedule', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/schedule/list/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListR4SSchedules', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/schedule/event/create', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateR4SScheduleEvent', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/schedule/event', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateR4SScheduleEvent', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/schedule/event/delete/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteR4SScheduleEvent', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/schedule/event/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteR4SScheduleEvent', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/schedule/event', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateR4SScheduleEvent', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/schedule/event/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteR4SScheduleEvent', 'permission_callback' => '__return_true'));

    // r4s design config crud
    register_rest_route(E4S_R4S_NS, '/design/read/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadR4SDesign', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/design/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadR4SDesign', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/design/create', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateR4SDesign', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/design', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateR4SDesign', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/design/update', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateR4SDesign', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/design', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateR4SDesign', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/design/delete/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteR4SDesign', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/design/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteR4SDesign', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/design/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadR4SDesign', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/design', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateR4SDesign', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/design', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateR4SDesign', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/design/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteR4SDesign', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/design/list', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListR4SDesigns', 'permission_callback' => '__return_true'));

    // r4s output config crud
    register_rest_route(E4S_R4S_NS, '/output/read/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadR4SOutput', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/output/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadR4SOutput', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/output', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateR4SOutput', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/output/create', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateR4SOutput', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/output', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateR4SOutput', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/output/update', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateR4SOutput', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/output/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteR4SOutput', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/output/delete/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteR4SOutput', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_R4S_NS, '/output/list/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListR4SOutputs', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_REPORT_NS, '/finance', array('methods' => 'GET', 'callback' => 'e4s_rest_ReportsFinance', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_REPORT_NS, '/finance/cust', array('methods' => 'GET', 'callback' => 'e4s_rest_ReportsFinanceCustomers', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_REPORT_NS, '/finance/orgs', array('methods' => 'GET', 'callback' => 'e4s_rest_ReportsFinanceOrganisations', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_REPORT_NS, '/finance2', array('methods' => 'GET', 'callback' => 'e4s_rest_ReportsFinance2', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_REPORT_NS, '/accountsplit/(?P<year>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_AccountsSplitYear', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_REPORT_NS, '/accountsplit/date/(?P<date>\S+)', array('methods' => 'GET', 'callback' => 'e4s_rest_AccountsSplitDate', 'permission_callback' => '__return_true'));

    // Messages
    register_rest_route(E4S_NS, '/message/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadMessage', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/message/unread/(?P<id>\d+)', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUnReadMessage', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/message', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadMessages', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/message', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateMessage', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/message/delete/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderMarkMessageDeleted', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/message/undelete/(?P<id>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_builderMarkMessageUnDeleted', 'permission_callback' => '__return_true'));

    // Areas
    register_rest_route(E4S_NS, '/areas/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadArea', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/areas', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateArea', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/areas', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateArea', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/areas/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteArea', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/areas', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListAreas', 'permission_callback' => '__return_true'));

    // Clubs
    register_rest_route(E4S_NS, '/clubCrud/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadClub', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/clubCrud', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateClub', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/clubCrud', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateClub', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/clubCrud/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteClub', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/clubCrud', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListClubs', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/clubs', array('methods' => 'GET', 'callback' => 'e4s_rest_getClubs', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/clubs/(?P<areaId>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getClubs', 'permission_callback' => '__return_true'));

    // Schools
    register_rest_route(E4S_NS, '/schoolCrud/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadSchool', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/schoolCrud', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateSchool', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/schoolCrud', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateSchool', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/schoolCrud/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteSchool', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/schoolCrud', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListSchools', 'permission_callback' => '__return_true'));


    register_rest_route(E4S_NS, '/ag/def/(?P<aocode>\S+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListDefaultAges', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/comp/(?P<compid>\d+)/info', array('methods' => 'GET', 'callback' => 'e4s_rest_CompMoreInfo', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/ag/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadAge', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/ag', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateAge', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/ag', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateArea', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/ag/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteAge', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/ag', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListAges', 'permission_callback' => '__return_true'));

    // UOM
    register_rest_route(E4S_NS, '/uom/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadUOM', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/uom', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateUOM', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/uom', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateUOM', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/uom/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteUOM', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/uom', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListUOM', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/stripe/init', array('methods' => 'POST', 'callback' => 'e4s_rest_initStripeUsers', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/stripe/list', array('methods' => 'GET', 'callback' => 'e4s_rest_listStripeUsers', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/stripe/approve/(?P<userid>\d+)/(?P<code>\S+)', array('methods' => 'GET', 'callback' => 'e4s_rest_approveStripeUser', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/stripe/approve/(?P<userid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_approveStripeUser', 'permission_callback' => '__return_true'));

    // Credit
    register_rest_route(E4S_NS, '/user/credit/update/(?P<userid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_updateUserCredit', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/user/credit/(?P<userid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_setUserCredit', 'permission_callback' => '__return_true'));

    // Permissions
    register_rest_route(E4S_NS, '/user/permissions/addorg/(?P<orgid>\d+)/(?P<userid>\S+)', array('methods' => 'GET', 'callback' => 'e4s_rest_addOrgToUser', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/user/permissions/(?P<userid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_builderPermissionModify', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/user/permissions/(?P<userid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getPermissions', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/user/permissions/(?P<userid>\d+)', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderPermissionModify', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/user/permissions/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderPermissionDelete', 'permission_callback' => '__return_true'));
//    register_rest_route(E4S_NS, '/user/version', array('methods' => 'GET', 'callback' => 'e4s_rest_getUserVersion', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/user/version', array('methods' => 'POST', 'callback' => 'e4s_rest_setUserVersion', 'permission_callback' => '__return_true'));

    // Announcement Player
    register_rest_route(E4S_NS, '/otd/(?P<compid>\d+)/announcer', array('methods' => 'GET', 'callback' => 'e4s_rest_openAnnouncer', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/lap', array('methods' => 'GET', 'callback' => 'e4s_rest_displayLap', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/lap/(?P<screenid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_displayLap', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/otd/track/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getUploadTrackInfo', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/otd/track/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getUploadTrackInfo', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/otd/track/(?P<compid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_uploadTrackInfo', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/otd/track/(?P<compid>\d+)/(?P<outputno>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getUploadTrackInfo', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/otd/track/(?P<compid>\d+/(?P<outputno>\d+))', array('methods' => 'GET', 'callback' => 'e4s_rest_getUploadTrackInfo', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/otd/track/(?P<compid>\d+/(?P<outputno>\d+))', array('methods' => 'POST', 'callback' => 'e4s_rest_uploadTrackInfo', 'permission_callback' => '__return_true'));

    // auto file upload from Matts/uploader page
    register_rest_route(E4S_NS, '/otd' . E4S_SECURE_TEXT . '/track/(?P<compid>\d+)/(?P<key>\S+)', array('methods' => 'POST', 'callback' => 'e4s_rest_uploadTrackInfoAuto', 'permission_callback' => '__return_true'));

    // Events
    register_rest_route(E4S_NS, '/event/comp/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListEventsForComp', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/event/unique', array('methods' => 'POST', 'callback' => 'e4s_rest_builderUpdateUniqueEvents', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/event/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadEvent', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/event', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateEvent', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/event', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateEvent', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/event/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteEvent', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/event', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListEvent', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/eventdefs', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListEventDefs', 'permission_callback' => '__return_true'));

    // add athlete to event from results
    register_rest_route(E4S_NS, '/event/addAthlete', array('methods' => 'POST', 'callback' => 'e4s_rest_addAthleteToEvent', 'permission_callback' => '__return_true'));

    // Admin
    register_rest_route(E4S_ADMIN_NS, '/config', array('methods' => 'GET', 'callback' => 'e4s_rest_getAdminConfig', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_PUBLIC_NS, '/config', array('methods' => 'GET', 'callback' => 'e4s_rest_getPublicConfig', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_ADMIN_NS, '/config', array('methods' => 'POST', 'callback' => 'e4s_rest_setLocalHost', 'permission_callback' => '__return_true'));

    register_rest_route(E4S_PUBLIC_NS, '/eacheck', array('methods' => 'GET', 'callback' => 'e4s_rest_checkEAAwards', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/refund', array('methods' => 'POST', 'callback' => 'e4s_rest_userRefunds', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_ADMIN_NS, '/refund', array('methods' => 'POST', 'callback' => 'e4s_rest_adminRefunds', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_ADMIN_NS, '/cheques', array('methods' => 'GET', 'callback' => 'e4s_rest_getAdminChequeOrders', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_ADMIN_NS, '/cheques', array('methods' => 'POST', 'callback' => 'e4s_rest_setChequeOrders', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_ADMIN_NS, '/user', array('methods' => 'GET', 'callback' => 'e4s_rest_getUserRecord', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_ADMIN_NS, '/user/club', array('methods' => 'DELETE', 'callback' => 'e4s_rest_deleteClubFromUser', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_ADMIN_NS, '/user/clubcomp', array('methods' => 'POST', 'callback' => 'e4s_rest_updateClubComp', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_ADMIN_NS, '/user/clubcomp/(?P<userid>\d+)/(?P<compid>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_deleteClubComp', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_ADMIN_NS, '/user/area', array('methods' => 'DELETE', 'callback' => 'e4s_rest_deleteAreaFromUser', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_ADMIN_NS, '/user/area', array('methods' => 'POST', 'callback' => 'e4s_rest_addUserToArea', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_ADMIN_NS, '/user/club', array('methods' => 'POST', 'callback' => 'e4s_rest_addUserToClub', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/comps/lite', array('methods' => 'GET', 'callback' => 'e4s_rest_getFilteredCompsLite', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/comps/filter', array('methods' => 'GET', 'callback' => 'e4s_rest_getHomePageComps', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/get-all-comps/(?P<orgid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getPublicCompInfo', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/get-all-comps', array('methods' => 'GET', 'callback' => 'e4s_rest_getPublicCompInfo', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/user', array('methods' => 'GET', 'callback' => 'e4s_rest_getUserInfo', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete/pof10pbs/(?P<urn>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_updateAthletePof10Info', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete/pof10pbs/(?P<urn>\S+)', array('methods' => 'GET', 'callback' => 'e4s_rest_InvalidURN', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete/entries/(?P<athleteid>\S+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getAthleteEntries', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/pof10/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_exportResultsForPOF10', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/generate/pof10/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_generateResultsForPOF10', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/clearheights/(?P<egid>\d+)/(?P<fromheight>\S+)', array('methods' => 'POST', 'callback' => 'e4s_rest_clearHeightResults', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/report/(?P<compid>\d+)/(?P<fromeventno>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_exportResultsForComp', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/report/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_exportResultsForComp', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/esaascores/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_showESAAScores', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/esaascores/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_showESAAScores', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/clubreport/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_clubCompReport', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/owners', array('methods' => 'POST', 'callback' => 'e4s_restGetOwners', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/payees', array('methods' => 'POST', 'callback' => 'e4s_restGetPayees', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/entries', array('methods' => 'POST', 'callback' => 'e4s_restFeederEntries', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_REPORT_NS, '/4', array('methods' => 'GET', 'callback' => 'e4s_rest_getFinancialReport4', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_REPORT_NS, '/3', array('methods' => 'GET', 'callback' => 'e4s_rest_getFinancialReport3', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_REPORT_NS, '/2', array('methods' => 'GET', 'callback' => 'e4s_rest_getFinancialByDateReport', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_REPORT_NS, '/1/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getFinancialReport', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_REPORT_NS, '/finance/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getFinancialReport', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_BUILDER_NS, '/competition/clone', array('methods' => 'POST', 'callback' => 'e4s_rest_cloneCompetition', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/validate/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_validateCompetition', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/competition/report', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompetitionReport', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/competition/report2', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompetitionReport2', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/competition/report3', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompetitionReport3', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/clubcomps', array('methods' => 'GET', 'callback' => 'e4s_rest_getClubComps', 'permission_callback' => '__return_true'));

    register_rest_route(E4S_ADMIN_NS, '/clubcomp/copy', array('methods' => 'POST', 'callback' => 'e4s_rest_copyClubCompDef', 'permission_callback' => '__return_true'));

    register_rest_route(E4S_ADMIN_NS, '/clubcomp/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_readClubCompDef', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_ADMIN_NS, '/clubcomp', array('methods' => 'POST', 'callback' => 'e4s_rest_updateClubCompDef', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_ADMIN_NS, '/clubcomp/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_deleteClubCompDef', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_ADMIN_NS, '/clubcompcat', array('methods' => 'POST', 'callback' => 'e4s_rest_updateClubCompCatDef', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_ADMIN_NS, '/clubcompcat/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_readClubCompCatDef', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_ADMIN_NS, '/clubcompcat/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_deleteClubCompCatDef', 'permission_callback' => '__return_true'));

    // Seeding
    register_rest_route(E4S_PUBLIC_NS, '/seeding/data/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_dataCompSeedings', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/seeding/list/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_listCompSeedings', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/seeding/(?P<eventgroupid>\d+)/(?P<checkedin>\d+)/(?P<heatno>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_confirmHeatParticipants', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/seeding/clearall/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_clearCompSeedings', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/seeding/clear/(?P<eventgroupid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_clearEventSeedings', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/seeding/write/(?P<eventgroupid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_writeEventSeedingsV2', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/seeding/confirm/(?P<eventgroupid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_confirmSeedingForEG', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/seeding/update', array('methods' => 'POST', 'callback' => 'e4s_rest_updateSeedingForEG', 'permission_callback' => '__return_true'));

    register_rest_route(E4S_NS, '/photofinish', array('methods' => 'POST', 'callback' => 'e4s_rest_createPhotofinish', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/photofinish/(?P<compid>\d+)/(?P<filename>\S+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getPhotofinish', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/photofinish/response', array('methods' => 'POST', 'callback' => 'e4s_rest_photofinishResponse', 'permission_callback' => '__return_true'));

    // scoreboard
    register_rest_route(E4S_NS, '/scoreboard/message/(?P<compid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_sendScoreboardMessage', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/scoreboard/command/(?P<compid>\d+)/(?P<outputid>\d+)/(?P<action>\S+)', array('methods' => 'POST', 'callback' => 'e4s_rest_commandToScoreboard', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/socket/(?P<compid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_commandToScoreboard', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/scoreboard/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getScoreboardEvents', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/scoreboard/events/(?P<compid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_getScoreboardEventsForArray', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/scoreboard/all/(?P<compid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_getAllScoreboards', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/scoreboard/(?P<compid>\d+)/(?P<eventno>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getScoreboardForEvent', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/scoreboard/clear/(?P<compid>\d+)/(?P<eventno>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_clearScoreboardForEvent', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/read/(?P<egid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_readResults', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/autoentries', array('methods' => 'POST', 'callback' => 'e4s_rest_writeAutoEntries', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/write', array('methods' => 'POST', 'callback' => 'e4s_rest_writeResults', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/(?P<egid>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_deleteHeatResults', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/(?P<egid>\d+)/(?P<heatno>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_deleteHeatResults', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/delete/(?P<egid>\d+)/(?P<heatno>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_deleteHeatResults', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/pf/(?P<compid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_postPFResults', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/lynxpf/(?P<compid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_postLynxPFResults', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/webresults', array('methods' => 'GET', 'callback' => 'e4s_rest_refreshWebResults', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/(?P<compid>\d+)/(?P<eventno>\d+)/(?P<heatno>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getResultsForComp', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/results/(?P<compid>\d+)/(?P<egid>\d+)/(?P<heatno>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_SendResultsToDisplays', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/info/apisched/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getInfoSchedule', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/info/apistart/(?P<egid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getInfoEvent', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/info/apiresults/(?P<egid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getInfoResults', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/lynx', array('methods' => 'POST', 'callback' => 'e4s_rest_receiveLynx', 'permission_callback' => '__return_true'));

	register_rest_route(E4S_NS, '/entryperf/(?P<entryid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getEntryPerf', 'permission_callback' => '__return_true'));
	// show Entries V2
	register_rest_route(E4S_PUBLIC_REPORT_NS, '/entryperf/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_showEntriesV2', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_PUBLIC_NS, '/showentriesv2/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_showEntriesV2', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/athlete/updatepof10/(?P<urn>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_updateAthletePof10', 'permission_callback' => '__return_true'));
	// output report
    register_rest_route(E4S_PUBLIC_REPORT_NS, '/output/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getOutputReportV2', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_REPORT_NS, '/outputv2/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getOutputReportV2', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_REPORT_NS, '/outputv3/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getOutputReportV3', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/card/reload/(?P<compid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_fullCardReload', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/reports/cardsV1/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getOutputPublicReport', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/reports/cards/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getOutputPublicReportV2', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/reports/cardsV2/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getOutputPublicReportV2', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/reports/cardsV3/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getOutputPublicReportV3', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/postresult', array('methods' => 'POST', 'callback' => 'e4s_rest_postResults', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/closeresult/(?P<compid>\d+)/(?P<egid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_closeResults', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/(?P<compid>\d+)/athletes', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompAthletes', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/(?P<compid>\d+)/athletesummary', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompAthleteSummary', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/comporg/', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompetitionClubs', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/comporg/(?P<orgId>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompetitionClubs', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/comporg/', array('methods' => 'GET', 'callback' => 'e4s_rest_getPublicCompetitionClubs', 'permission_callback' => '__return_true'));
    register_rest_route('jwt-auth/v1', '/comporg/', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompetitionClubsAuth', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/compdates/(?P<club>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompetitionDates', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/compdates/(?P<club>\d+)/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompetitionDates', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/compdates/(?P<club>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getPublicCompetitionDates', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/compdates/(?P<club>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getPublicCompetitionDates', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/compdates/(?P<club>\d+)/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getPublicCompetitionDates', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/compdates/undefined', array('methods' => 'GET', 'callback' => 'e4s_rest_UIIssue', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/users/', array('methods' => 'GET', 'callback' => 'e4s_rest_getUsers', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/users/enities/', array('methods' => 'GET', 'callback' => 'e4s_rest_getUsersInEntities', 'permission_callback' => '__return_true'));

    // Next Up
    register_rest_route(E4S_NS, '/event/nextup/(?P<egid>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_NextUpDelete', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/event/nextup', array('methods' => 'POST', 'callback' => 'e4s_rest_NextUp', 'permission_callback' => '__return_true'));

    // Athlete
	register_rest_route(E4S_ADMIN_NS, '/athlete/(?P<urn>\d+)/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getAdminAthlete', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/athlete/(?P<id>\d+)', array('methods' => 'PUT', 'callback' => 'e4s_rest_updateAthlete', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/athlete/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_deleteUserAthlete', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/athlete/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getAthlete', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/athlete/(?P<id>\d+)/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getAthlete', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/athlete/(?P<id>\d+)/(?P<userid>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_deleteUserAthlete', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/athlete/(?P<athleteid>\d+)/(?P<userid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_addUserAthlete', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete/users/(?P<athleteid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getAthleteUsers', 'permission_callback' => '__return_true'));

    register_rest_route(E4S_NS, '/athlete/confirm', array('methods' => 'GET', 'callback' => 'e4s_rest_confirmWithParams', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete/getid', array('methods' => 'GET', 'callback' => 'e4s_rest_getRegIDWithParms', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete/getaai', array('methods' => 'GET', 'callback' => 'e4s_rest_getAAIAthlete', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete/check/(?P<aid>\d+)/(?P<ceid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_checkEligable', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete/link/(?P<id>\d+)/(?P<dob>\S+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getAndLinkAthlete', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete/checkin/(?P<compid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_updateAthleteCheckins', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete/checkin', array('methods' => 'GET', 'callback' => 'e4s_rest_listAthleteCheckins', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/athletes/', array('methods' => 'GET', 'callback' => 'e4s_rest_getAthletes', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/athletes/(?P<egid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getAthletesForEventGroupID', 'permission_callback' => '__return_true'));

    register_rest_route(E4S_NS, '/athlete-maint/(?P<id>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadAthlete', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete-maint/(?P<id>\d+)/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_builderReadAthlete', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete-maint', array('methods' => 'POST', 'callback' => 'e4s_rest_builderCreateAthlete', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete-maint', array('methods' => 'PUT', 'callback' => 'e4s_rest_builderUpdateAthlete', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete-maint/(?P<id>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_builderDeleteAthlete', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete-maint', array('methods' => 'GET', 'callback' => 'e4s_rest_builderListAthlete', 'permission_callback' => '__return_true'));

	register_rest_route(E4S_NS, '/events/multi/(?P<aid>\d+)/(?P<ceid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getMultiPb', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/events/(?P<compid>\d+)/(?P<aid>\d+)/(?P<clubid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompetitionEvents', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/events/(?P<compid>\d+)/(?P<aid>\d+)/(?P<clubid>\S+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompetitionEvents', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/events/(?P<compid>\d+)/(?P<aid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompetitionEventsV2', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/cart', array('methods' => 'GET', 'callback' => 'e4s_rest_getCart', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/minicart', array('methods' => 'GET', 'callback' => 'e4s_rest_getMiniCart', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/teams/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompTeams', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/event/entries/(?P<compid>\d+)/(?P<egid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getEventGroupEntries', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/event/entries/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getEventGroupEntries', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/event/allentries/(?P<compid>\d+)/(?P<egid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getEventGroupEntriesIncUnpaid', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/event/allentries/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getEventGroupEntriesIncUnpaid', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/schedule/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getSchedule', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/info/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompInfo', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/schedresults/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getScheduleAndResults', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entry/bib/(?P<entryid>\d+)/(?P<teambibno>\S+)', array('methods' => 'POST', 'callback' => 'e4s_rest_updateEntryTeamBibNo', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entry/teambibnos', array('methods' => 'POST', 'callback' => 'e4s_rest_updateEntryTeamBibNos', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/(?P<compid>\d+)/bibs', array('methods' => 'DELETE', 'callback' => 'e4s_rest_clearBibs', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/(?P<compid>\d+)/bibs', array('methods' => 'POST', 'callback' => 'e4s_rest_createBibs', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/(?P<compid>\d+)/bibs', array('methods' => 'PUT', 'callback' => 'e4s_rest_addBibs', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/emails/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompEmails', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/entries/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getEntries', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/entrystatus/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_listEntryStatus', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/showentries/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_showEntries', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_SECURE_NS, '/showentries/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_showEntries', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/entries/waiting/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getWaitingEntries', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/getorder/(?P<orderid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getOrder', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/admin/migrate', array('methods' => 'GET', 'callback' => 'e4s_rest_migrateUsers', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/socketkey/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getCompSecurityKey', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/admin/switchuser', array('methods' => 'GET', 'callback' => 'e4s_rest_switchUser', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/admin/setpwd', array('methods' => 'GET', 'callback' => 'e4s_rest_setUserPwd', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/admin/restorepwd', array('methods' => 'GET', 'callback' => 'e4s_rest_restoreUserPWD', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/checkPof10forathletes', array('methods' => 'GET', 'callback' => 'e4s_rest_checkPof10ForAthletes', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/checkPof10/(?P<userid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_checkPof10', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/checkPof10/(?P<userid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_checkPof10', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/checkCompPof10/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_checkCompPof10', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/checkCompPof10/(?P<compid>\d+)/(?P<egid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_checkCompPof10', 'permission_callback' => '__return_true'));

    // Card Routes
    register_rest_route(E4S_PUBLIC_NS, '/uicard/entries/(?P<compid>\d+)/(?P<tf>\S+)/(?P<eventgroupid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_cardEntries', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/uicard/entries/(?P<compid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_cardEntryArray', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/uicard/entries/(?P<compid>\d+)/(?P<tf>\S+)/(?P<eventgroupid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_cardEntries', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/uicard/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_displayCard', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/uicard/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_displayCard', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/uicard/(?P<compid>\d+)/(?P<device>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_displayCard', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/uicard/signature', array('methods' => 'POST', 'callback' => 'e4s_rest_saveSignature', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/r4sPostResult', array('methods' => 'POST', 'callback' => 'r4s_rest_postResults', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/temp_clearresults', array('methods' => 'GET', 'callback' => 'r4s_rest_clearResults', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/waitinglist/move/(?P<entryid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_moveOffWaitingList', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entrychecker', array('methods' => 'POST', 'callback' => 'e4s_entryCheckerUpdate', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/height/positions', array('methods' => 'POST', 'callback' => 'e4s_writeHeightPositions', 'permission_callback' => '__return_true'));

    // defunct Routes ?
    register_rest_route(E4S_NS, '/setwaitinglists', array('methods' => 'POST', 'callback' => 'e4s_waitingListCheckerUpdate', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/setwaitinglists/(?P<compid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_setWaitingPosOnAComps', 'permission_callback' => '__return_true'));


    // PUT Routes
    register_rest_route(E4S_NS, '/product/(?P<ceid>\d+)/(?P<aid>\d+)/(?P<clubid>\d+)', array('methods' => 'PUT', 'callback' => 'e4s_rest_createProduct', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/product/(?P<ceid>\d+)/(?P<aid>\d+)/(?P<clubid>\S+)', array('methods' => 'PUT', 'callback' => 'e4s_rest_createProduct', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/product/(?P<ceid>\d+)/(?P<aid>\d+)/(?P<clubid>\d+)/(?P<teamid>\d+)', array('methods' => 'PUT', 'callback' => 'e4s_rest_createProduct', 'permission_callback' => '__return_true'));

    // POST Routes
    register_rest_route(E4S_NS, '/competition/status', array('methods' => 'POST', 'callback' => 'e4s_rest_moveCompetitionStatus', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete', array('methods' => 'POST', 'callback' => 'e4s_rest_newAthlete', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/team/add', array('methods' => 'POST', 'callback' => 'e4s_rest_newTeam', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/team/(?P<teamId>\d+)', array('methods' => 'DELETE', 'callback' => 'e4s_rest_deleteTeam', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/linkathlete/(?P<athleteid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_addUserAthlete', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PRIVATE_NS, '/athlete/registration', array('methods' => 'POST', 'callback' => 'e4s_rest_loadFromRegistration', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete/pbupdatev2', array('methods' => 'POST', 'callback' => 'e4s_rest_pushAthletePBUpdateV2', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete/pbupdate', array('methods' => 'POST', 'callback' => 'e4s_rest_pushAthletePBUpdate', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/competition/pbupdate', array('methods' => 'POST', 'callback' => 'e4s_rest_competitionPBUpdate', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete/withdraw/(?P<aid>\d+)/(?P<productid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_withdrawFromEvent', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/athlete/multi/(?P<aid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_postAthleteMultiPBUpdate', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/product/delete/(?P<prodid>\d+)', array('methods' => 'POST', 'callback' => 'e4s_rest_DeleteProduct', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entry/switch', array('methods' => 'POST', 'callback' => 'e4s_rest_SwitchEntry', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/entry/athlete/switch', array('methods' => 'POST', 'callback' => 'e4s_rest_SwitchAthlete', 'permission_callback' => '__return_true'));
    register_rest_route('view', '/(?P<fileid>\d+)/(?P<code>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_viewfile', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/upload', array('methods' => 'POST', 'callback' => 'e4s_rest_upload', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/' . E4S_TICKET_ROUTE . '/athlete/(?P<athleteid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getAthleteTicket', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/' . E4S_TICKET_ROUTE . '/list/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_listTicket', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/' . E4S_TICKET_ROUTE . '/athlete/list/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_listAthleteTicket', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/' . E4S_TICKET_ROUTE . '/listall/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_listAllTicket', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/' . E4S_TICKET_ROUTE . '/generate', array('methods' => 'GET', 'callback' => 'e4s_rest_generateTicket', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/' . E4S_TICKET_ROUTE . '/(?P<compid>\d+)/generate1off', array('methods' => 'GET', 'callback' => 'e4s_rest_generateTicket1Off', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/' . E4S_TICKET_ROUTE . '/clear/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_clearTicket', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/' . E4S_TICKET_ROUTE . '/scan/(?P<compid>\d+)/(?P<guid>\S+)', array('methods' => 'GET', 'callback' => 'e4s_rest_scanTicket', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/' . E4S_TICKET_ROUTE . '/read/(?P<compid>\d+)/(?P<guid>\S+)', array('methods' => 'GET', 'callback' => 'e4s_rest_nonScannedTicket', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/' . E4S_TICKET_ROUTE . '/get/(?P<compid>\d+)/(?P<athleteid>\S+)', array('methods' => 'GET', 'callback' => 'e4s_rest_nonScannedTicket', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/' . E4S_TICKET_ROUTE . '/order/(?P<order_key>\S+)', array('methods' => 'GET', 'callback' => 'e4s_rest_listTicket', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/' . E4S_TICKET_ROUTE . '/update/(?P<guid>\S+)', array('methods' => 'POST', 'callback' => 'e4s_rest_updateTicket', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/' . E4S_TICKET_ROUTE . '/allocate/(?P<guid>\S+)', array('methods' => 'POST', 'callback' => 'e4s_rest_allocateTicket', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/' . E4S_TICKET_ROUTE . '/report/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_reportTicket', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/dedup', array('methods' => 'GET', 'callback' => 'e4s_rest_dedup', 'permission_callback' => '__return_true'));

	// Roster
	register_rest_route(E4S_PUBLIC_NS, '/roster/(?P<compid>\d+)', array('methods' => ['GET','POST'], 'callback' => 'e4s_rest_getRosterInfo', 'permission_callback' => '__return_true'));

    // test end points
    register_rest_route(E4S_NS, '/pof10Update', array('methods' => ['GET','POST'], 'callback' => 'e4s_rest_pof10Update', 'permission_callback' => '__return_true'));
	register_rest_route(E4S_NS, '/test', array('methods' => ['GET','POST'], 'callback' => 'e4s_rest_test', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/test2', array('methods' => 'GET', 'callback' => 'e4s_rest_test2', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_NS, '/card/(?P<compid>\d+)', array('methods' => 'GET', 'callback' => 'e4s_rest_getCardInfo', 'permission_callback' => '__return_true'));

    // Accounts end point
    register_rest_route(E4S_NS, '/e4s', array('methods' => 'GET', 'callback' => 'e4s_rest_e4s', 'permission_callback' => '__return_true'));
    register_rest_route(E4S_PUBLIC_NS, '/e4s', array('methods' => 'GET', 'callback' => 'e4s_rest_e4s', 'permission_callback' => '__return_true'));
    // Cron end point
    register_rest_route(E4S_NS, '/e4scron', array('methods' => 'GET', 'callback' => 'e4s_rest_e4scron', 'permission_callback' => '__return_true'));
});
