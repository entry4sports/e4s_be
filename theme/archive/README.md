# Archive old orders and products from E4S

Move orders and products that match criteria off to archive folders

# Configuration

Edit settings in Woocommerce » Settings » Advanced » Archives
