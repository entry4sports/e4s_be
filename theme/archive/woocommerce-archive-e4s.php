<?php
/*
  Plugin Name: Archive Entry4Sports
  Plugin URI: http://dev.entry4ports.co.uk/
  Description: Archive Entry4Sports data to archive tables
  Version: 1.1.0
  Author: Paul Day
  Author URI: https://entry4sports.co.uk
  Text Domain: woo-archive-e4s
  Tags: Woocommerce, Archive, orders, performances, products, Entry4Sports
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $WcAE4S;
$WcAE4S = new WooCommerceArchiveE4S;

class WooCommerceArchiveE4S {
	private $legacy_statuses = array();
	private $archived_statuses = array();
	private $textdomain = 'woo-archive-e4s';

	public function __construct() {

//		add_filter( 'woocommerce_register_shop_order_post_statuses', array(
//			&$this,
//			'register_custom_order_status'
//		), 100 );
//		add_filter( 'wc_order_statuses', array( &$this, 'add_custom_statuses_to_list' ) );
//
//		// filter on edit-shop_order page
////		add_filter( 'views_edit-shop_order', array( $this, 'shop_order_filters' ), 10, 1 );
//		add_action( 'pre_get_posts', array( $this, 'pre_get_posts' ), 10, 1 );

		// Add settings
		add_filter( 'woocommerce_get_sections_advanced', array( $this, 'woocommerce_get_sections_advanced' ), 10, 1 );
		add_filter( 'woocommerce_get_settings_advanced', array( $this, 'woocommerce_get_settings_advanced' ), 10, 2 );

		add_filter( 'plugin_action_links_woo-archive-e4s/woocommerce-archive-e4s.php', array(
			&$this,
			'settings_link'
		) );
//		add_action('admin_menu', 'e4s_admin_menu');
		// Manage cron tasks
		$this->manage_schedules();

		add_action( 'Entry4Sports_Archive', array( $this, 'cron_tasks' ) );
	}
	function manage_schedules() {
		if ( ! wp_next_scheduled( 'Entry4Sports_Archive' ) ) {
			wp_schedule_event( strtotime( '+1 hour' ), 'hourly', 'Entry4Sports_Archive' );
		}
	}
	public function register_custom_order_status( $order_statuses ) {
		$this->legacy_statuses = $order_statuses;
		foreach ( $order_statuses as $order_status => $values ) {
			$values['label']                                 = sprintf( __( '%s (archived)', $this->textdomain ), $values['label'] );
			$values['label_count']                           = _n_noop( $values['label'] . ' <span class="count">(%s)</span>', $values['label'] . ' <span class="count">(%s)</span>' );
			$values['show_in_admin_all_list']                = false;
			$values['show_in_admin_status_list']             = false;
			$order_statuses[ $order_status . '-a' ]          = $values;
			$this->archived_statuses[ $order_status . '-a' ] = $values['label'];
		}

		return $order_statuses;
	}

	function add_custom_statuses_to_list( $order_statuses ) {
		foreach ( $order_statuses as $order_status => $label ) {
			$order_statuses[ $order_status . '-a' ] = sprintf( __( '%s (archived)', $this->textdomain ), $label );
		}

		return $order_statuses;
	}

	function woocommerce_get_sections_advanced( $sections ) {
		$sections['archives'] = __( 'Entry4Sports', $this->textdomain );

		return $sections;
	}

	function woocommerce_get_settings_advanced( $settings, $current_section ) {
		if ( 'archives' === $current_section ) {
			$settings = apply_filters(
				'woocommerce_settings_archives', array(
					array(
						'title' => __( 'Entry4Sports', $this->textdomain ),
						'desc'  => __( 'Automatically archive Entry4Sports data older than X days', $this->textdomain ),
						'type'  => 'title',
						'id'    => 'archives',
					),
					array(
						'title'    => __( 'Enabled', $this->textdomain ),
						'desc'     => __( 'Enable/Disable Entry4Sports Archiving', $this->textdomain ),
						'id'       => 'woocommerce_archive_e4s_enabled',
						'type'     => 'checkbox',
						'default'  => '0',
						'desc_tip' => true,
					),
					array(
						'title'    => __( 'Max orders age (in days)', $this->textdomain ),
						'desc'     => __( '0 disables automatic archives', $this->textdomain ),
						'id'       => 'woocommerce_archive_e4s_older_than_days',
						'type'     => 'text',
						'default'  => '0',
						'desc_tip' => true,
					),
					array(
						'type' => 'sectionend',
						'id'   => 'archives',
					),
				)
			);
		}

		return $settings;
	}

	/**
	 *  Settings link on the plugins page
	 */
	public function settings_link( $links ) {
		$setting_url   = add_query_arg(
			array(
				'page'    => 'wc-settings',
				'&tab'    => 'advanced',
				'section' => 'archives',
			),
			admin_url( 'admin.php' )
		);
		$settings_link = '<a href="' . $setting_url . '">' . __( 'Settings', $this->textdomain ) . '</a>';
		array_unshift( $links, $settings_link );

		return $links;
	}

	function cron_tasks() {
		$enabled = get_option( 'woocommerce_archive_e4s_enabled' );
		if ( $enabled !== 'yes' ) {
			return;
		}

		$max_age = get_option( 'woocommerce_archive_e4s_older_than_days' );
		if ( ! $max_age ) {
			$max_age = 500;
		}

		include_once E4S_FULL_PATH . 'dbInfo.php';
		$e4sArchive = new e4sArchive( $max_age );
		$e4sArchive->process();
	}
}

// ability to call process manually
function e4s_perform_archive( $obj ) {
	global $WcAE4S;
	$WcAE4S->cron_tasks();
}
function e4s_admin_menu() {
	add_menu_page(
		'Entry4Sports Settings',
		'Entry4Sports',
		'edit_posts',
		'e4s_slug',
		'e4s_admin_page',
		'dashicons-media-spreadsheet'
	);
//		add_submenu_page( 'Entry4Sportd', __( 'Entry4Sports', $this->textdomain ), __( 'Entry4Sports', $this->textdomain ), 'manage_options', 'e4s-archive', 'e4s_archive_page' );
}
function e4s_admin_page() {
	// write the code to display the admin page with input field that is written to the options table
	// and a button to run the archive process

	?>
    <div class="wrap">
        <h2>Entry4Sports Archive</h2>
        <p>Click the button below to archive Entry4Sports data</p>
        <form method="post" action="options.php">
			<?php
			settings_fields( 'e4s_archive' );
			do_settings_sections( 'e4s_archive' );
			?>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Archive Data</th>
                    <td>
                        <input type="submit" name="e4s_archive" value="Archive"/>
                    </td>
                </tr>
            </table>
        </form>
    </div>
	<?php

}