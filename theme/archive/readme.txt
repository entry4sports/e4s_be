=== WooCommerce Archive E4S ===
Contributors: Paul Day,
Tags: woocommerce, orders, archive, performances
Requires at least: 6.4.1
Tested up to: 6.4.1
Stable tag: development
License: private

Archive Entry4Sports orders and products

== Description ==

Archive Entry4Sports orders and products

== Installation ==

1. Upload `woo-archive-e4s` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress admin
3. You can edit defaults settings in Woocommerce » Settings » Advanced » Archives

== Frequently asked questions ==

== Changelog ==

= 1.0 =

- Initial release Nov 2023
